/**
 * @defgroup Ioc_InputSR5ms_Ifa Ioc_InputSR5ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR5ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTSR5MS_IFA_H_
#define IOC_INPUTSR5MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngIdxInfo(data) do \
{ \
    *data = Ioc_InputSR5msRisngIdxInfo; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallIdxInfo(data) do \
{ \
    *data = Ioc_InputSR5msFallIdxInfo; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngTiStampInfo(data) do \
{ \
    *data = Ioc_InputSR5msRisngTiStampInfo; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallTiStampInfo(data) do \
{ \
    *data = Ioc_InputSR5msFallTiStampInfo; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngIdxInfo_FlRisngIdx(data) do \
{ \
    *data = Ioc_InputSR5msRisngIdxInfo.FlRisngIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngIdxInfo_FrRisngIdx(data) do \
{ \
    *data = Ioc_InputSR5msRisngIdxInfo.FrRisngIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngIdxInfo_RlRisngIdx(data) do \
{ \
    *data = Ioc_InputSR5msRisngIdxInfo.RlRisngIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngIdxInfo_RrRisngIdx(data) do \
{ \
    *data = Ioc_InputSR5msRisngIdxInfo.RrRisngIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallIdxInfo_FlFallIdx(data) do \
{ \
    *data = Ioc_InputSR5msFallIdxInfo.FlFallIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallIdxInfo_FrFallIdx(data) do \
{ \
    *data = Ioc_InputSR5msFallIdxInfo.FrFallIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallIdxInfo_RlFallIdx(data) do \
{ \
    *data = Ioc_InputSR5msFallIdxInfo.RlFallIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallIdxInfo_RrFallIdx(data) do \
{ \
    *data = Ioc_InputSR5msFallIdxInfo.RrFallIdx; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngTiStampInfo_FlRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngTiStampInfo_FrRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngTiStampInfo_RlRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngTiStampInfo_RrRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallTiStampInfo_FlFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallTiStampInfo_FrFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallTiStampInfo_RlFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFallTiStampInfo_RrFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[i]; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msEcuModeSts(data) do \
{ \
    *data = Ioc_InputSR5msEcuModeSts; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Ioc_InputSR5msAcmAsicInitCompleteFlag; \
}while(0);

#define Ioc_InputSR5ms_Read_Ioc_InputSR5msFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_InputSR5msFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo(data) do \
{ \
    Ioc_InputSR5msWssMonInfo = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FlRisngIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.FlRisngIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FlFallIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.FlFallIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FlRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FlFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.FlFallTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FrRisngIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.FrRisngIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FrFallIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.FrFallIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FrRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_FrFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.FrFallTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RlRisngIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.RlRisngIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RlFallIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.RlFallIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RlRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RlFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.RlFallTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RrRisngIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.RrRisngIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RrFallIdx(data) do \
{ \
    Ioc_InputSR5msWssMonInfo.RrFallIdx = *data; \
}while(0);

#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RrRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[i] = *data[i]; \
}
#define Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo_RrFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Ioc_InputSR5msWssMonInfo.RrFallTiStamp[i] = *data[i]; \
}
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTSR5MS_IFA_H_ */
/** @} */
