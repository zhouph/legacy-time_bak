/**
 * @defgroup Ioc_OutputSR1ms_Ifa Ioc_OutputSR1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputSR1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTSR1MS_IFA_H_
#define IOC_OUTPUTSR1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msBalVlvDrvInfo(data) do \
{ \
    *data = Ioc_OutputSR1msBalVlvDrvInfo; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFsrCbsDrvInfo(data) do \
{ \
    *data = Ioc_OutputSR1msFsrCbsDrvInfo; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFsrAbsDrvInfo(data) do \
{ \
    *data = Ioc_OutputSR1msFsrAbsDrvInfo; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_FlOvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_FlIvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_FrOvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_FrIvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_RlOvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_RlIvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_RrOvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo_RrIvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msBalVlvDrvInfo_PressDumpVlvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msBalVlvDrvInfo_BalVlvDrvData(data) do \
{ \
    *data = Ioc_OutputSR1msBalVlvDrvInfo.BalVlvDrvData; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFsrCbsDrvInfo_FsrCbsDrv(data) do \
{ \
    *data = Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    *data = Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msEcuModeSts(data) do \
{ \
    *data = Ioc_OutputSR1msEcuModeSts; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Ioc_OutputSR1msAcmAsicInitCompleteFlag; \
}while(0);

#define Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_OutputSR1msFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrDutyData(data) do \
{ \
    Ioc_OutputSR1msIocDcMtrDutyData = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrFreqData(data) do \
{ \
    Ioc_OutputSR1msIocDcMtrFreqData = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo0Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo0Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo1Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo1Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo2Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo2Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo3Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo3Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc0Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvTc0Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc1Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvTc1Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv0Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvEsv0Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv1Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvEsv1Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc0Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc0Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc1Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc1Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc2Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc2Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc3Data(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc3Data = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvRelayData(data) do \
{ \
    Ioc_OutputSR1msIocVlvRelayData = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocAdcSelWriteData(data) do \
{ \
    Ioc_OutputSR1msIocAdcSelWriteData = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrDutyData_MtrDutyDataFlg(data) do \
{ \
    Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrDutyData_ACH_Tx8_PUMP_DTY_PWM(data) do \
{ \
    Ioc_OutputSR1msIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrFreqData_MtrFreqDataFlg(data) do \
{ \
    Ioc_OutputSR1msIocDcMtrFreqData.MtrFreqDataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrFreqData_ACH_Tx8_PUMP_TCK_PWM(data) do \
{ \
    Ioc_OutputSR1msIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo0Data_VlvNo0DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo0Data.VlvNo0DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo0Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo1Data_VlvNo1DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo1Data.VlvNo1DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo1Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo2Data_VlvNo2DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo2Data.VlvNo2DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo2Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo2Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo3Data_VlvNo3DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo3Data.VlvNo3DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo3Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNo3Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc0Data_VlvTc0DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvTc0Data.VlvTc0DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvTc0Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc1Data_VlvTc1DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvTc1Data.VlvTc1DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvTc1Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv0Data_VlvEsv0DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvEsv0Data.VlvEsv0DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvEsv0Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv1Data_VlvEsv1DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvEsv1Data.VlvEsv1DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvEsv1Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc0Data_VlvNc0DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc0Data.VlvNc0DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc0Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc1Data_VlvNc1DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc1Data.VlvNc1DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc1Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc2Data_VlvNc2DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc2Data.VlvNc2DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc2Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc2Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc3Data_VlvNc3DataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc3Data.VlvNc3DataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc3Data_ACH_TxValve_SET_POINT(data) do \
{ \
    Ioc_OutputSR1msIocVlvNc3Data.ACH_TxValve_SET_POINT = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvRelayData_VlvRelayDataFlg(data) do \
{ \
    Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvRelayData_ACH_Tx1_FS_CMD(data) do \
{ \
    Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocAdcSelWriteData_AdcSelDataFlg(data) do \
{ \
    Ioc_OutputSR1msIocAdcSelWriteData.AdcSelDataFlg = *data; \
}while(0);

#define Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocAdcSelWriteData_ACH_Tx13_ADC_SEL(data) do \
{ \
    Ioc_OutputSR1msIocAdcSelWriteData.ACH_Tx13_ADC_SEL = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTSR1MS_IFA_H_ */
/** @} */
