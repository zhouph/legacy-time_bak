/**
 * @defgroup Ioc_InputSR50us_Ifa Ioc_InputSR50us_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR50us_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTSR50US_IFA_H_
#define IOC_INPUTSR50US_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD1IIFAngInfo(data) do \
{ \
    *data = Ioc_InputSR50usMpsD1IIFAngInfo; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD2IIFAngInfo(data) do \
{ \
    *data = Ioc_InputSR50usMpsD2IIFAngInfo; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usHwTrigMotInfo(data) do \
{ \
    *data = Ioc_InputSR50usHwTrigMotInfo; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD1IIFAngInfo_D1IIFAngRawData(data) do \
{ \
    *data = Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngRawData; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD1IIFAngInfo_D1IIFAngDegree(data) do \
{ \
    *data = Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngDegree; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD2IIFAngInfo_D2IIFAngRawData(data) do \
{ \
    *data = Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngRawData; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD2IIFAngInfo_D2IIFAngDegree(data) do \
{ \
    *data = Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngDegree; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    *data = Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    *data = Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    *data = Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    *data = Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usEcuModeSts(data) do \
{ \
    *data = Ioc_InputSR50usEcuModeSts; \
}while(0);

#define Ioc_InputSR50us_Read_Ioc_InputSR50usFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_InputSR50usFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotCurrMonInfo(data) do \
{ \
    Ioc_InputSR50usMotCurrMonInfo = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotAngleMonInfo(data) do \
{ \
    Ioc_InputSR50usMotAngleMonInfo = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Ioc_InputSR50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTSR50US_IFA_H_ */
/** @} */
