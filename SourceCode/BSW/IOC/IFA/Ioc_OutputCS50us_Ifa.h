/**
 * @defgroup Ioc_OutputCS50us_Ifa Ioc_OutputCS50us_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS50us_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTCS50US_IFA_H_
#define IOC_OUTPUTCS50US_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhUhighData(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhVhighData(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhWhighData(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhUlowData(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhVlowData(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhWlowData(data) do \
{ \
    *data = Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usEcuModeSts(data) do \
{ \
    *data = Ioc_OutputCS50usEcuModeSts; \
}while(0);

#define Ioc_OutputCS50us_Read_Ioc_OutputCS50usFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_OutputCS50usFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTCS50US_IFA_H_ */
/** @} */
