/**
 * @defgroup Ioc_InputSR1ms_Ifa Ioc_InputSR1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTSR1MS_IFA_H_
#define IOC_INPUTSR1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_InputSR1ms_Read_Ioc_InputSR1msAchAdcData(data) do \
{ \
    *data = Ioc_InputSR1msAchAdcData; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigFspInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigFspInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigTmpInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigTmpInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMotInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPdtInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPdtInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo(data) do \
{ \
    *data = Ioc_InputSR1msHwTrigVlvInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msAchAdcData_ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT(data) do \
{ \
    *data = Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msAchAdcData_ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY(data) do \
{ \
    *data = Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_Ext5vMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_CEMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.CEMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_Int5vMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.Int5vMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_CSPMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.CSPMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_Vbatt01Mon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.Vbatt01Mon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_Vbatt02Mon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.Vbatt02Mon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_VddMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.VddMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_Vdd3Mon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.Vdd3Mon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo_Vdd5Mon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPwrInfo.Vdd5Mon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigFspInfo_FspAbsHMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigFspInfo.FspAbsHMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigFspInfo_FspAbsMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigFspInfo.FspAbsMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigFspInfo_FspCbsHMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigFspInfo.FspCbsHMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigFspInfo_FspCbsMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigFspInfo.FspCbsMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigTmpInfo_LineTestMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigTmpInfo.LineTestMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigTmpInfo_TempMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigTmpInfo.TempMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo_MotPwrMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMotInfo.MotPwrMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo_StarMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMotInfo.StarMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo_UoutMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMotInfo.UoutMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo_VoutMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMotInfo.VoutMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo_WoutMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMotInfo.WoutMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPdtInfo_PdfSigMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPdtInfo.PdfSigMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPdtInfo_Pdt5vMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPdtInfo.Pdt5vMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPdtInfo_Pdf5vMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPdtInfo.Pdf5vMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPdtInfo_PdtSigMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigPdtInfo.PdtSigMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RlIMainMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RlIMainMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RlISubMonFs(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RlISubMonFs; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RlMocNegMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RlMocNegMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RlMocPosMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RlMocPosMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RrIMainMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RrIMainMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RrISubMonFs(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RrISubMonFs; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RrMocNegMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RrMocNegMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_RrMocPosMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.RrMocPosMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo_BFLMon(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigMocInfo.BFLMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo_VlvCVMon(data) do \
{ \
    *data = Ioc_InputSR1msHwTrigVlvInfo.VlvCVMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo_VlvRLVMon(data) do \
{ \
    *data = Ioc_InputSR1msHwTrigVlvInfo.VlvRLVMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo_VlvCutPMon(data) do \
{ \
    *data = Ioc_InputSR1msHwTrigVlvInfo.VlvCutPMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo_VlvCutSMon(data) do \
{ \
    *data = Ioc_InputSR1msHwTrigVlvInfo.VlvCutSMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo_VlvSimMon(data) do \
{ \
    *data = Ioc_InputSR1msHwTrigVlvInfo.VlvSimMon; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A7_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A7_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A2_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A2_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_P1_NEG_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.P1_NEG_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A6_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A6_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_MTR_FW_S_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.MTR_FW_S_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A4_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A4_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_MTP_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.MTP_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A3_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A3_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_P1_POS_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.P1_POS_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A5_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A5_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_VDD_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.VDD_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_COOL_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.COOL_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_CAN_L_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.CAN_L_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_P3_NEG_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.P3_NEG_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A1_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A1_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_P2_POS_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.P2_POS_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A8_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A8_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_IF_A9_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.IF_A9_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_P2_NEG_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.P2_NEG_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_P3_POS_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.P3_POS_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo_OP_OUT_MON(data) do \
{ \
    *data = Ioc_InputSR1msSwTrigEscInfo.OP_OUT_MON; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpPresData1(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpPresData1; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpPresData2(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpPresData2; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpSentCrc(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpSentCrc; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpSentData(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpSentData; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpSentMsgId(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpSentMsgId; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpSentSerialCrc(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpSentSerialCrc; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpSentConfig(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpSentConfig; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_McpSentInvalid(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.McpSentInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_MCPSentSerialInvalid(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.MCPSentSerialInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1PresData1(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1PresData1; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1PresData2(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1PresData2; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentCrc(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentCrc; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentData(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentData; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentMsgId(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentMsgId; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentSerialCrc(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialCrc; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentConfig(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentConfig; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentInvalid(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp1SentSerialInvalid(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2PresData1(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2PresData1; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2PresData2(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2PresData2; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentCrc(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentCrc; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentData(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentData; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentMsgId(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentMsgId; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentSerialCrc(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialCrc; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentConfig(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentConfig; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentInvalid(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo_Wlp2SentSerialInvalid(data) do \
{ \
    *data = Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msEcuModeSts(data) do \
{ \
    *data = Ioc_InputSR1msEcuModeSts; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Ioc_InputSR1msAcmAsicInitCompleteFlag; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msAdcInvalid(data) do \
{ \
    *data = Ioc_InputSR1msAdcInvalid; \
}while(0);

#define Ioc_InputSR1ms_Read_Ioc_InputSR1msFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_InputSR1msFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfoEsc(data) do \
{ \
    Ioc_InputSR1msVlvMonInfoEsc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfoEsc(data) do \
{ \
    Ioc_InputSR1msMotMonInfoEsc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc(data) do \
{ \
    Ioc_InputSR1msReservedMonInfoEsc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfo(data) do \
{ \
    Ioc_InputSR1msMotMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotVoltsMonInfo(data) do \
{ \
    Ioc_InputSR1msMotVoltsMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlSigMonInfo(data) do \
{ \
    Ioc_InputSR1msPedlSigMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlPwrMonInfo(data) do \
{ \
    Ioc_InputSR1msPedlPwrMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo(data) do \
{ \
    Ioc_InputSR1msMocMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo(data) do \
{ \
    Ioc_InputSR1msVlvMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltVBatt01Mon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt01Mon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltVBatt02Mon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt02Mon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltGio2Mon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltGio2Mon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltExt5VSupplyMon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltExt5VSupplyMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltExt12VSupplyMon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltExt12VSupplyMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltSwVpwrMon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltSwVpwrMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltVpwrMon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltVpwrMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltGio1Mon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltGio1Mon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc_VoltIgnMon_Esc(data) do \
{ \
    Ioc_InputSR1msPwrMonInfoEsc.VoltIgnMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfoEsc_VoltFspMon_Esc(data) do \
{ \
    Ioc_InputSR1msVlvMonInfoEsc.VoltFspMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfoEsc_VoltMtpMon_Esc(data) do \
{ \
    Ioc_InputSR1msMotMonInfoEsc.VoltMtpMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfoEsc_VoltMtrFwSMon_Esc(data) do \
{ \
    Ioc_InputSR1msMotMonInfoEsc.VoltMtrFwSMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbPwrMon_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbPwrMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbI1LMon_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1LMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbI1RMon_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1RMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbI2LMon_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2LMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbI2RMon_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2RMon_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbMonLPlus_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLPlus_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbMonLMinus_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLMinus_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbMonRPlus_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRPlus_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc_VoltEpbMonRMinus_Esc(data) do \
{ \
    Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRMinus_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc_VoltMcuAdc1_Esc(data) do \
{ \
    Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc1_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc_VoltMcuAdc2_Esc(data) do \
{ \
    Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc2_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc_VoltMcuAdc3_Esc(data) do \
{ \
    Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc3_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc_VoltMcuAdc4_Esc(data) do \
{ \
    Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc4_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc_VoltMcuAdc5_Esc(data) do \
{ \
    Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc5_Esc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpPresData1(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpPresData1 = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpPresData2(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpPresData2 = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpSentCrc(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpSentCrc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpSentData(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpSentData = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpSentMsgId(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpSentMsgId = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpSentSerialCrc(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_McpSentConfig(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.McpSentConfig = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1PresData1(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1PresData1 = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1PresData2(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1PresData2 = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1SentCrc(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1SentData(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1SentData = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1SentMsgId(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1SentSerialCrc(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp1SentConfig(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2PresData1(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2PresData1 = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2PresData2(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2PresData2 = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2SentCrc(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2SentData(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2SentData = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2SentMsgId(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2SentSerialCrc(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo_Wlp2SentConfig(data) do \
{ \
    Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfo_MotPwrVoltMon(data) do \
{ \
    Ioc_InputSR1msMotMonInfo.MotPwrVoltMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfo_MotStarMon(data) do \
{ \
    Ioc_InputSR1msMotMonInfo.MotStarMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotVoltsMonInfo_MotVoltPhUMon(data) do \
{ \
    Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotVoltsMonInfo_MotVoltPhVMon(data) do \
{ \
    Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMotVoltsMonInfo_MotVoltPhWMon(data) do \
{ \
    Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlSigMonInfo_PdfSigMon(data) do \
{ \
    Ioc_InputSR1msPedlSigMonInfo.PdfSigMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlSigMonInfo_PdtSigMon(data) do \
{ \
    Ioc_InputSR1msPedlSigMonInfo.PdtSigMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlPwrMonInfo_Pdt5vMon(data) do \
{ \
    Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlPwrMonInfo_Pdf5vMon(data) do \
{ \
    Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RlCurrMainMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RlCurrMainMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RlCurrSubMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RlCurrSubMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RlMocNegMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RlMocNegMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RlMocPosMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RlMocPosMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RrCurrMainMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RrCurrMainMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RrCurrSubMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RrCurrSubMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RrMocNegMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RrMocNegMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo_RrMocPosMon(data) do \
{ \
    Ioc_InputSR1msMocMonInfo.RrMocPosMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo_VlvCVMon(data) do \
{ \
    Ioc_InputSR1msVlvMonInfo.VlvCVMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo_VlvRLVMon(data) do \
{ \
    Ioc_InputSR1msVlvMonInfo.VlvRLVMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo_PrimCutVlvMon(data) do \
{ \
    Ioc_InputSR1msVlvMonInfo.PrimCutVlvMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo_SecdCutVlvMon(data) do \
{ \
    Ioc_InputSR1msVlvMonInfo.SecdCutVlvMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo_SimVlvMon(data) do \
{ \
    Ioc_InputSR1msVlvMonInfo.SimVlvMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_FlOvCurrMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.FlOvCurrMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_FlIvDutyMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.FlIvDutyMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_FrOvCurrMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.FrOvCurrMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_FrIvDutyMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.FrIvDutyMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_RlOvCurrMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.RlOvCurrMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_RlIvDutyMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.RlIvDutyMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_RrOvCurrMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.RrOvCurrMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo_RrIvDutyMon(data) do \
{ \
    Ioc_InputSR1msWhlVlvFbMonInfo.RrIvDutyMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVBatt1Mon(data) do \
{ \
    Ioc_InputSR1msVBatt1Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVBatt2Mon(data) do \
{ \
    Ioc_InputSR1msVBatt2Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msFspCbsHMon(data) do \
{ \
    Ioc_InputSR1msFspCbsHMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msFspCbsMon(data) do \
{ \
    Ioc_InputSR1msFspCbsMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msFspAbsHMon(data) do \
{ \
    Ioc_InputSR1msFspAbsHMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msFspAbsMon(data) do \
{ \
    Ioc_InputSR1msFspAbsMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msExt5vMon(data) do \
{ \
    Ioc_InputSR1msExt5vMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msCEMon(data) do \
{ \
    Ioc_InputSR1msCEMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msLineTestMon(data) do \
{ \
    Ioc_InputSR1msLineTestMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msTempMon(data) do \
{ \
    Ioc_InputSR1msTempMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd1Mon(data) do \
{ \
    Ioc_InputSR1msVdd1Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd2Mon(data) do \
{ \
    Ioc_InputSR1msVdd2Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd3Mon(data) do \
{ \
    Ioc_InputSR1msVdd3Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd4Mon(data) do \
{ \
    Ioc_InputSR1msVdd4Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd5Mon(data) do \
{ \
    Ioc_InputSR1msVdd5Mon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msVddMon(data) do \
{ \
    Ioc_InputSR1msVddMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msCspMon(data) do \
{ \
    Ioc_InputSR1msCspMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msInt5vMon(data) do \
{ \
    Ioc_InputSR1msInt5vMon = *data; \
}while(0);

#define Ioc_InputSR1ms_Write_Ioc_InputSR1msBFLMon(data) do \
{ \
    Ioc_InputSR1msBFLMon = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTSR1MS_IFA_H_ */
/** @} */
