/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Ioc_InputSR50us.h"
#include "Ioc_OutputCS50us.h"
#include "Ioc_InputSR1ms.h"
#include "Ioc_InputCS1ms.h"
#include "Ioc_OutputSR1ms.h"
#include "Ioc_OutputCS1ms.h"
#include "Ioc_InputSR5ms.h"
#include "Ioc_OutputCS5ms.h"

int main(void)
{
    Ioc_InputSR50us_Init();
    Ioc_OutputCS50us_Init();
    Ioc_InputSR1ms_Init();
    Ioc_InputCS1ms_Init();
    Ioc_OutputSR1ms_Init();
    Ioc_OutputCS1ms_Init();
    Ioc_InputSR5ms_Init();
    Ioc_OutputCS5ms_Init();

    while(1)
    {
        Ioc_InputSR50us();
        Ioc_OutputCS50us();
        Ioc_InputSR1ms();
        Ioc_InputCS1ms();
        Ioc_OutputSR1ms();
        Ioc_OutputCS1ms();
        Ioc_InputSR5ms();
        Ioc_OutputCS5ms();
    }
}