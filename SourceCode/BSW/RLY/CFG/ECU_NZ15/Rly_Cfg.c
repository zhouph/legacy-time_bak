/**
 * @defgroup Rly_Cfg Rly_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rly_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rly_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RLY_START_SEC_CONST_UNSPECIFIED
#include "Rly_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Relay routing config table */
 const Relay_ConfigType Relay_Config[RELAY_PORT_MAX_NUM] = 
 {
    /* RELAY_PORT_BRK_LAMP */
    {        
        (Relay_ActiveType)  Relay_HighActive,               /* SigActiveType */
        (Relay_ActiveType)  Relay_HighActive                /* EnActiveType */
    },
    /* RELAY_PORT_ESS_LAMP */
    {           
        (Relay_ActiveType)  Relay_HighActive,               /* SigActiveType */
        (Relay_ActiveType)  Relay_HighActive                /* EnActiveType */
    }
 };
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RLY_STOP_SEC_CONST_UNSPECIFIED
#include "Rly_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_START_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
#define RLY_START_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLY_STOP_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_START_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/** Variable Section (32BIT)**/


#define RLY_STOP_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_START_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
#define RLY_START_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLY_STOP_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_START_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/** Variable Section (32BIT)**/


#define RLY_STOP_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RLY_START_SEC_CODE
#include "Rly_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define RLY_STOP_SEC_CODE
#include "Rly_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
