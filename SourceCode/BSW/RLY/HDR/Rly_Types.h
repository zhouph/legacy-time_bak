/**
 * @defgroup Rly_Types Rly_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rly_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RLY_TYPES_H_
#define RLY_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Rly_ActrEcuModeSts;
    Eem_SuspcDetnFuncInhibitRlySts_t Rly_ActrFuncInhibitRlySts;

/* Output Data Element */
    Rly_ActrRlyDrvInfo_t Rly_ActrRlyDrvInfo;
}Rly_Actr_HdrBusType;
 /* Requesting client ID type */
 typedef uint8 Relay_ClientIdType;

 /* Requesting buffer type */
 typedef uint16 Relay_DataType;

 /* ECU Signal port type */
 typedef uint8 Relay_SigPortType;

 /* Relay enable port type */
 typedef uint8 Relay_EnPortType;

 /* Relay port type */
 typedef uint16 Relay_PortType;

  /* Relay Active Type */
 typedef enum
 {
    Relay_LowActive = 0,
    Relay_HighActive = 1
 }Relay_ActiveType;

 /* Enable status type */
 typedef enum
 {
    Relay_EnStatus_Disabled = 0,
    Relay_EnStatus_Enabled
 }Relay_EnStatusType;

  /* Running status type */
 typedef enum
 {
    Relay_RunStatus_Idle = 0,
    Relay_RunStatus_Running,
    Relay_RunStatus_Fail
 }Relay_RunStatusType;


 typedef enum
 {
    Relay_ReqStatus_Requested = 0,
    Relay_ReqStatus_NotRequested
 }Relay_ClientStsType;

 typedef enum
 {
    Relay_Error_IdxOverflow = 0,
    Relay_Error_Input,
    Relay_Error_Timing,
    Relay_Error_Config
 }Relay_ErrorType;
 /* Routing config type */
 typedef struct
 {
    Relay_ActiveType     SigActiveType;
    Relay_ActiveType     EnActiveType;
 }Relay_ConfigType;

 /* Motor actuator status type */
 typedef struct
 {
    Relay_EnStatusType  EnStatus;
    Relay_RunStatusType RunStatus;    
 }Relay_StatusType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RLY_TYPES_H_ */
/** @} */
