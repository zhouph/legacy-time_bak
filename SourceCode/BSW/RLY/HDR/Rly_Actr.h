/**
 * @defgroup Rly_Actr Rly_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rly_Actr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RLY_ACTR_H_
#define RLY_ACTR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rly_Types.h"
#include "Rly_Cfg.h"
#include "Rly_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define RLY_ACTR_MODULE_ID      (0)
 #define RLY_ACTR_MAJOR_VERSION  (2)
 #define RLY_ACTR_MINOR_VERSION  (0)
 #define RLY_ACTR_PATCH_VERSION  (0)
 #define RLY_ACTR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Rly_Actr_HdrBusType Rly_ActrBus;

/* Version Info */
extern const SwcVersionInfo_t Rly_ActrVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Rly_ActrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitRlySts_t Rly_ActrFuncInhibitRlySts;

/* Output Data Element */
extern Rly_ActrRlyDrvInfo_t Rly_ActrRlyDrvInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Rly_Actr_Init(void);
extern void Rly_Actr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RLY_ACTR_H_ */
/** @} */
