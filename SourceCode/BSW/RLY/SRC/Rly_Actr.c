/**
 * @defgroup Rly_Actr Rly_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rly_Actr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rly_Actr.h"
#include "Rly_Actr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RLY_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Rly_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RLY_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Rly_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLY_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Rly_Actr_HdrBusType Rly_ActrBus;

/* Version Info */
const SwcVersionInfo_t Rly_ActrVersionInfo = 
{   
    RLY_ACTR_MODULE_ID,           /* Rly_ActrVersionInfo.ModuleId */
    RLY_ACTR_MAJOR_VERSION,       /* Rly_ActrVersionInfo.MajorVer */
    RLY_ACTR_MINOR_VERSION,       /* Rly_ActrVersionInfo.MinorVer */
    RLY_ACTR_PATCH_VERSION,       /* Rly_ActrVersionInfo.PatchVer */
    RLY_ACTR_BRANCH_VERSION       /* Rly_ActrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Rly_ActrEcuModeSts;
Eem_SuspcDetnFuncInhibitRlySts_t Rly_ActrFuncInhibitRlySts;

/* Output Data Element */
Rly_ActrRlyDrvInfo_t Rly_ActrRlyDrvInfo;

uint32 Rly_Actr_Timer_Start;
uint32 Rly_Actr_Timer_Elapsed;

#define RLY_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLY_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
#define RLY_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLY_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_ACTR_START_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/** Variable Section (32BIT)**/


#define RLY_ACTR_STOP_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLY_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
 /* Valve actuation duty data buffer */
 static Relay_DataType Relay_DataBuf[RELAY_PORT_MAX_NUM][RELAY_CLIENT_MAX_NUM];
 /* Client status */
 static Relay_ClientStsType Relay_ClientSts[RELAY_PORT_MAX_NUM][RELAY_CLIENT_MAX_NUM];
 /* Valve port status */
 static Relay_StatusType Relay_PortSts[RELAY_PORT_MAX_NUM];

#define RLY_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLY_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Rly_MemMap.h"
#define RLY_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLY_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Rly_MemMap.h"
#define RLY_ACTR_START_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/** Variable Section (32BIT)**/


#define RLY_ACTR_STOP_SEC_VAR_32BIT
#include "Rly_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RLY_ACTR_START_SEC_CODE
#include "Rly_MemMap.h"
static void Relay_Actuation(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Rly_Actr_Init(void)
{
    /* Initialize internal bus */
    Rly_ActrBus.Rly_ActrEcuModeSts = 0;
    Rly_ActrBus.Rly_ActrFuncInhibitRlySts = 0;
    Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyDbcDrv = 0;
    Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyEssDrv = 0;
}

void Rly_Actr(void)
{
    Rly_Actr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Rly_Actr_Read_Rly_ActrEcuModeSts(&Rly_ActrBus.Rly_ActrEcuModeSts);
    Rly_Actr_Read_Rly_ActrFuncInhibitRlySts(&Rly_ActrBus.Rly_ActrFuncInhibitRlySts);

    /* Process */
	Relay_Actuation();
    /* Output */
    Rly_Actr_Write_Rly_ActrRlyDrvInfo(&Rly_ActrBus.Rly_ActrRlyDrvInfo);
    /*==============================================================================
    * Members of structure Rly_ActrRlyDrvInfo 
     : Rly_ActrRlyDrvInfo.RlyDbcDrv;
     : Rly_ActrRlyDrvInfo.RlyEssDrv;
     =============================================================================*/
    

    Rly_Actr_Timer_Elapsed = STM0_TIM0.U - Rly_Actr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/****************************************************************************
| NAME:             Relay_Actuation
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Relay actuation function
****************************************************************************/
static void Relay_Actuation(void)
 {
    Relay_PortType port;
    Relay_ClientIdType client;
    /* Input A�ˢ�E��E��IAC �ˢ碮��uE�ˢ�IiE. A�ˢ�E��E��IAC �ˢ碮��oA CaEA input Parameter �ˢ�E�ˢ�IeA�ˢ碮��u
    	BRK LAMP relay
    Relay_DataBuf[0][DIAG] = notdefined;
    Relay_DataBuf[0][FAILSAFE] = notdefined;
    Relay_DataBuf[0][LOGIC] = notdefined;
    	ESS LAMP relay
    Relay_DataBuf[1][DIAG] = notdefined;
    Relay_DataBuf[1][FAILSAFE] = notdefined;
    Relay_DataBuf[1][LOGIC] = notdefined;
    */
    Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyDbcDrv = STD_HIGH;
    Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyEssDrv = STD_HIGH;

    for(port = 0; port < RELAY_PORT_MAX_NUM; port++)
    {
        for(client = 0; client < RELAY_CLIENT_MAX_NUM; client++)
        {
            if(Relay_ClientSts[port][client] == Relay_ReqStatus_Requested)
            {            
                if(Relay_DataBuf[port][client] == STD_HIGH)
                {
					if(port == 0)
                		Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyDbcDrv= 1;
					else
						Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyEssDrv= 1;						
                }
                else if(Relay_DataBuf[port][client] == STD_LOW)
                {
					if(port == 0)
                		Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyDbcDrv= 0;
					else
						Rly_ActrBus.Rly_ActrRlyDrvInfo.RlyEssDrv= 0;
                }
                break;
            }
        }
    }
 }

#define RLY_ACTR_STOP_SEC_CODE
#include "Rly_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
