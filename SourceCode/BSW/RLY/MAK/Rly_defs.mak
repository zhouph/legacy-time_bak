# \file
#
# \brief Rly
#
# This file contains the implementation of the SWC
# module Rly.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Rly_CORE_PATH     := $(MANDO_BSW_ROOT)\Rly
Rly_CAL_PATH      := $(Rly_CORE_PATH)\CAL\$(Rly_VARIANT)
Rly_SRC_PATH      := $(Rly_CORE_PATH)\SRC
Rly_CFG_PATH      := $(Rly_CORE_PATH)\CFG\$(Rly_VARIANT)
Rly_HDR_PATH      := $(Rly_CORE_PATH)\HDR
Rly_IFA_PATH      := $(Rly_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Rly_CMN_PATH      := $(Rly_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Rly_UT_PATH		:= $(Rly_CORE_PATH)\UT
	Rly_UNITY_PATH	:= $(Rly_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Rly_UT_PATH)
	CC_INCLUDE_PATH		+= $(Rly_UNITY_PATH)
	Rly_Actr_PATH 	:= Rly_UT_PATH\Rly_Actr
endif
CC_INCLUDE_PATH    += $(Rly_CAL_PATH)
CC_INCLUDE_PATH    += $(Rly_SRC_PATH)
CC_INCLUDE_PATH    += $(Rly_CFG_PATH)
CC_INCLUDE_PATH    += $(Rly_HDR_PATH)
CC_INCLUDE_PATH    += $(Rly_IFA_PATH)
CC_INCLUDE_PATH    += $(Rly_CMN_PATH)

