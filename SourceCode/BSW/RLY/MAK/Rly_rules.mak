# \file
#
# \brief Rly
#
# This file contains the implementation of the SWC
# module Rly.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Rly_src

Rly_src_FILES        += $(Rly_SRC_PATH)\Rly_Actr.c
Rly_src_FILES        += $(Rly_IFA_PATH)\Rly_Actr_Ifa.c
Rly_src_FILES        += $(Rly_CFG_PATH)\Rly_Cfg.c
Rly_src_FILES        += $(Rly_CAL_PATH)\Rly_Cal.c

ifeq ($(ICE_COMPILE),true)
Rly_src_FILES        += $(Rly_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Rly_src_FILES        += $(Rly_UNITY_PATH)\unity.c
	Rly_src_FILES        += $(Rly_UNITY_PATH)\unity_fixture.c	
	Rly_src_FILES        += $(Rly_UT_PATH)\main.c
	Rly_src_FILES        += $(Rly_UT_PATH)\Rly_Actr\Rly_Actr_UtMain.c
endif