#include "unity.h"
#include "unity_fixture.h"
#include "Rly_Actr.h"
#include "Rly_Actr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Rly_ActrEcuModeSts[MAX_STEP] = RLY_ACTRECUMODESTS;
const Eem_SuspcDetnFuncInhibitRlySts_t UtInput_Rly_ActrFuncInhibitRlySts[MAX_STEP] = RLY_ACTRFUNCINHIBITRLYSTS;

const Saluint8 UtExpected_Rly_ActrRlyDrvInfo_RlyDbcDrv[MAX_STEP] = RLY_ACTRRLYDRVINFO_RLYDBCDRV;
const Saluint8 UtExpected_Rly_ActrRlyDrvInfo_RlyEssDrv[MAX_STEP] = RLY_ACTRRLYDRVINFO_RLYESSDRV;



TEST_GROUP(Rly_Actr);
TEST_SETUP(Rly_Actr)
{
    Rly_Actr_Init();
}

TEST_TEAR_DOWN(Rly_Actr)
{   /* Postcondition */

}

TEST(Rly_Actr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Rly_ActrEcuModeSts = UtInput_Rly_ActrEcuModeSts[i];
        Rly_ActrFuncInhibitRlySts = UtInput_Rly_ActrFuncInhibitRlySts[i];

        Rly_Actr();

        TEST_ASSERT_EQUAL(Rly_ActrRlyDrvInfo.RlyDbcDrv, UtExpected_Rly_ActrRlyDrvInfo_RlyDbcDrv[i]);
        TEST_ASSERT_EQUAL(Rly_ActrRlyDrvInfo.RlyEssDrv, UtExpected_Rly_ActrRlyDrvInfo_RlyEssDrv[i]);
    }
}

TEST_GROUP_RUNNER(Rly_Actr)
{
    RUN_TEST_CASE(Rly_Actr, All);
}
