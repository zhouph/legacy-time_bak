/**
 * @defgroup Rly_Actr_Ifa Rly_Actr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rly_Actr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RLY_ACTR_IFA_H_
#define RLY_ACTR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Rly_Actr_Read_Rly_ActrEcuModeSts(data) do \
{ \
    *data = Rly_ActrEcuModeSts; \
}while(0);

#define Rly_Actr_Read_Rly_ActrFuncInhibitRlySts(data) do \
{ \
    *data = Rly_ActrFuncInhibitRlySts; \
}while(0);


/* Set Output DE MAcro Function */
#define Rly_Actr_Write_Rly_ActrRlyDrvInfo(data) do \
{ \
    Rly_ActrRlyDrvInfo = *data; \
}while(0);

#define Rly_Actr_Write_Rly_ActrRlyDrvInfo_RlyDbcDrv(data) do \
{ \
    Rly_ActrRlyDrvInfo.RlyDbcDrv = *data; \
}while(0);

#define Rly_Actr_Write_Rly_ActrRlyDrvInfo_RlyEssDrv(data) do \
{ \
    Rly_ActrRlyDrvInfo.RlyEssDrv = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RLY_ACTR_IFA_H_ */
/** @} */
