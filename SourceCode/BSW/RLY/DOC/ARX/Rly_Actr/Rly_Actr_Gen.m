Rly_ActrEcuModeSts = Simulink.Bus;
DeList={'Rly_ActrEcuModeSts'};
Rly_ActrEcuModeSts = CreateBus(Rly_ActrEcuModeSts, DeList);
clear DeList;

Rly_ActrFuncInhibitRlySts = Simulink.Bus;
DeList={'Rly_ActrFuncInhibitRlySts'};
Rly_ActrFuncInhibitRlySts = CreateBus(Rly_ActrFuncInhibitRlySts, DeList);
clear DeList;

Rly_ActrRlyDrvInfo = Simulink.Bus;
DeList={
    'RlyDbcDrv'
    'RlyEssDrv'
    };
Rly_ActrRlyDrvInfo = CreateBus(Rly_ActrRlyDrvInfo, DeList);
clear DeList;

