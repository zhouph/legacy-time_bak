/**
 * @defgroup Mtr_Processing_Types Mtr_Processing_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTR_PROCESSING_TYPES_H_
#define MTR_PROCESSING_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t Mtr_Processing_SigEemFailData;
    AdcIf_Conv1msSwTrigMotInfo_t Mtr_Processing_SigSwTrigMotInfo;
    Wss_SenWhlSpdInfo_t Mtr_Processing_SigWhlSpdInfo;
    AdcIf_Conv50usHwTrigMotInfo_t Mtr_Processing_SigHwTrigMotInfo;
    Ioc_InputSR50usMotCurrMonInfo_t Mtr_Processing_SigMotCurrMonInfo;
    Ioc_InputSR50usMotAngleMonInfo_t Mtr_Processing_SigMotAngleMonInfo;
    Eem_MainEemEceData_t Mtr_Processing_SigEemEceData;
    Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_SigMtrProcessDataInf;
    Mom_HndlrEcuModeSts_t Mtr_Processing_SigEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Mtr_Processing_SigIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Mtr_Processing_SigIgnEdgeSts;

/* Output Data Element */
    Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_SigMtrProcessOutInfo;
}Mtr_Processing_Sig_HdrBusType;

typedef struct
{
/* Input Data Element */
    Arbitrator_VlvArbWhlVlvReqInfo_t Mtr_Processing_DiagArbWhlVlvReqInfo;
    Eem_MainEemFailData_t Mtr_Processing_DiagEemFailData;
    Eem_MainEemCtrlInhibitData_t Mtr_Processing_DiagEemCtrlInhibitData;
    Ioc_InputSR1msPwrMonInfoEsc_t Mtr_Processing_DiagPwrMonInfoEsc;
    Msp_CtrlMotRotgAgSigInfo_t Mtr_Processing_DiagMotRotgAgSigInfo;
    Wss_SenWhlSpdInfo_t Mtr_Processing_DiagWhlSpdInfo;
    Diag_HndlrDiagHndlRequest_t Mtr_Processing_DiagDiagHndlRequest;
    Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_DiagMtrProcessOutInfo;

/* Output Data Element */
    Mtr_Processing_DiagMtrProcessOutData_t Mtr_Processing_DiagMtrProcessOutData;
    Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_DiagMtrProcessDataInf;
    Mtr_Processing_DiagMtrDiagState_t Mtr_Processing_DiagMtrDiagState;
}Mtr_Processing_Diag_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTR_PROCESSING_TYPES_H_ */
/** @} */
