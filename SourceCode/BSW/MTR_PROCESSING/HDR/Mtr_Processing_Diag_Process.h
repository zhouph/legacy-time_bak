/**
 * @defgroup Mtr_Processing_Diag Mtr_Processing_Diag
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Diag.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/
#ifndef MTR_PROCESSING_DIAG_PROCESS_H_
#define MTR_PROCESSING_DIAG_PROCESS_H_
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Diag.h"
#include "Mtr_Processing_Diag_Ifa.h"
#include "Mgd_TLE9180_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define COUNT_INIT				0
#define WAITLIMITCOUNT		3000
#define DEF_DETECT				1
#define DEF_NOT_DETECT		0
/****************************************************************
	Diag Request 
	*************************************************************/
#define DEF_NOT_REQUEST		0x00
#define DEF_LAB_CALIBRATION	0x01
#define DEF_CAR_CALIBRATION	0x02
#define DEF_PWM_HOLD			0x03
#define DEF_IQ_ID_HOLD			0x04
#define DEF_STROKE_TEST		0x05
#define DEF_DIAG_STOP			0x06
/****************************************************************
	Valve Request 
	*************************************************************/
#define DEF_VALVE_CLOSE  0x00u
#define DEF_VALVE_OPEN	0x01u
/*****************************************************************
	Motor Stauts
	*************************************************************/
#define DEF_MTR_INIT 		0x00u
#define DEF_MTR_NORMAL		0x01u
#define DEF_MTR_DIAG		0x02u
#define DEF_MTR_ERROR		0x03u
/******************************************************************
	Motor Init Status
	***************************************************************/
#define DEF_INIT_START		0x00u
#define DEF_INIT_COMPLETE	0x01u
#define DEF_INIT_SKIP		0x02u
#define DEF_INIT_ERR		0x03u
#define DEF_INIT_CHECK		0x04u
#define DEF_INIT_RE_CHECK	0x05u
/******************************************************************
	Motor Init Control Status
	***************************************************************/
#define DEF_INITCTR_INIT				0x00u
#define DEF_INITCTR_U_EXCITATION		0x01u
#define DEF_INITCTR_V_EXCITATION		0x02u
#define DEF_INITCTR_W_EXCITATION		0x03u
#define DEF_INITCTR_U_2ND_EXCITATION	0x04u
#define DEF_INITCTR_EXCITATION_CHECK	0x05u
#define DEF_INITCTR_EXCITATION_END	0x06u
#define DEF_INITCTR_EXCITATION_ERR	0x07u
/******************************************************************
	Motor Init Threhsold
	***************************************************************/
#define DEF_MTR_INIT_CTR_INIT 0
#define DEF_MTR_INIT_CTR_1ST	10
#define DEF_MTR_INIT_CTR_2ND	10
/******************************************************************
	Motor Init Phase Check Result
	***************************************************************/
#define DEF_PHASE_INIT			0x00u
#define DEF_PHASE_FAIL			0x01u
#define DEF_PHASE_COMPLTE		0x02u	
/******************************************************************
	Motor Calibration Status
	***************************************************************/
#define DEF_MTR_CAL_STATE_INIT			0x00u
#define DEF_MTR_CAL_STATE_WAIT			0x02u
#define DEF_MTR_CAL_STATE_ERR				0x04u
#define DEF_MTR_CAL_STATE_CW				0x10u
#define DEF_MTR_CAL_STATE_CW_ADD			0x30u
#define DEF_MTR_CAL_STATE_CCW			0x08u
#define DEF_MTR_CAL_STATE_CCW_ADD		0x0Cu
#define DEF_MTR_CAL_STATE_IQ_CHECK		0x60u
#define DEF_MTR_CAL_STATE_RPM_CHECK		0x80u
#define DEF_MTR_CAL_STATE_COMPLETE		0x40u

#define DEF_MTR_CAL_STATE_WAIT_LIMIT  950
#define DEF_MTR_WAIT_CNT_LIMIT 1000
#define MTR_MOTION_CNT_LIMIT		4

#define DEF_OFFSETDETECT_FAIL			0x01u
#define DEF_OFFSETDETECT_COMPLETE	0x02u
#define DEF_OFFSETDETECT_RETEST		0x03u
#define DEF_OFFSETDETECT_TEST			0x04u
#define DEF_IQ_REQ_FAIL					0x01u
#define DEF_IQ_REQ_COMPLETE			0x02u
#define DEF_PRM_REQ_FAIL				0x01u
#define DEF_RPM_REQ_COMPLETE			0x02u4

#define Mtr_MECA_ANGLE_0DEG		(Saluint32)0
#define Mtr_MECA_ANGLE_90DEG		(Saluint32)2048
#define Mtr_MECA_ANGLE_180DEG		(Saluint32)4096
#define Mtr_MECA_ANGLE_270DEG		(Saluint32)6144
#define Mtr_MECA_ANGLE_360DEG		(Saluint32)8192

#define MPS_ELEC_ANGLE_5DEG		(Saluint32)29
#define MPS_ELEC_ANGLE_10DEG		(Saluint32)57	
#define MPS_ELEC_ANGLE_115DEG        (Saluint32)654//163
#define MPS_ELEC_ANGLE_125DEG		(Saluint32)712
/******************************************************************
	Motor Motor Direction Status
	***************************************************************/
#define DEF_MTR_DIR_MASK				0x18u  /*0001 1000*/
#define DEF_MTR_STOP					0x00u  /*0000 0000*/
#define DEF_MTR_CW						0x10u  /*0001 0000*/
#define DEF_MTR_CCW					0x08u  /*0000 1000*/
/******************************************************************
	Motor Calibration Excitation Status
	***************************************************************/
#define DEF_CAL_EXCITATION_PHASE_INIT				0x00u
#define DEF_CAL_EXCITATION_U_PHASE				0x01u
#define DEF_CAL_EXCITATION_U_PHASE_WAIT			0x02u
#define DEF_CAL_EXCITATION_W_PHASE				0x03u
#define DEF_CAL_EXCITATION_W_PHASE_WAIT			0x04u
#define DEF_CAL_EXCITATION_V_PHASE				0x05u
#define DEF_CAL_EXCITATION_V_PHASE_WAIT			0x06u
#define DEF_CAL_GAIN								1
/********************************************************************
 	Motor Current Request Status
 	***********************DEF_PERCENT_GAIN*****************************************/
 #define DEF_MTR_CUR_REQ_INIT			0x00u
 #define DEF_MTR_CUR_REQ_CW			0x01u
 #define DEF_MTR_CUR_REQ_WAIT			0x02u
 #define DEF_MTR_CUR_REQ_CCW			0x03u
 #define DEF_MTR_CUR_REQ_CHECK		0x04u
 #define DEF_MTR_CUR_REQ_COMPLETE_WAIT	0x05u
 #define DEF_MTR_CUR_REQ_ERR			0x06u
 #define DEF_MTR_CUR_REQ_COMPLETE		0x07u
 #define DEF_1_5_SEC_1ms				1500
 #define DEF_1_0_SEC_1ms				1000
 #define DEF_0_8_SEC_1ms				  800
 #define DEF_RPM_MINIMUM				  500
 #define DEF_RPM_LIMIT_PERCENT			    10
 #define DEF_PERCENT_GAIN				  100
/********************************************************************
 	Motor Current Value Check Define
 	****************************************************************/
#define CURRENT_BAND_LIMIT			25/* 45%*//*20150212 current unbalance problem 45-->25*/
#define CURRENT_BAND_MIN			50/*   5% /* 20150212 current unbalabce problen 5-->50*/
#define CURRENT_PHA_PLUS_MIN	       500/* 5A */
#define CURRENT_PHA_MINUS_MIN       -500/*-5A*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
 #define DEF_CAL_MAX_DUTY	(Saluint16)2000	
 #define DEF_CAL_MIN_DUTY	(Saluint16)1000
 #define DEF_INIT_GAIN			1
 #define DEF_INIT_REGAIN		2
 #define DEF_PHASE_INIT			0
 #define DEF_INIT_THRESHOLD_VOLT (Saluint16)7000
 #define DEF_ELEC_ANGLE_115deg	  (Saluint32)11500	
 #define DEF_ELEC_ANGLE_125deg (Saluint32)12500
 #define DEF_ELEC_ANGLE_10deg	(Saluint32)1000
 
#define DEF_MTR_REQUEST_NONE	       0x00 
#define DEF_MTR_REQUEST_PWM		0x01
#define DEF_MTR_REQUEST_IQ		0x02
#define DEF_MTR_REQUEST_PWM_INIT 0x03
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_DIAG_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"



extern void Mtr_Processing_Diag_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Processing_Diag);
#endif
