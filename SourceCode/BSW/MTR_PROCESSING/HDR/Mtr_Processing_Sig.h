/**
 * @defgroup Mtr_Processing_Sig Mtr_Processing_Sig
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Sig.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTR_PROCESSING_SIG_H_
#define MTR_PROCESSING_SIG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Types.h"
#include "Mtr_Processing_Cfg.h"
#include "Mtr_Processing_Cal.h"
#include "Mtr_Processing_Sig_Process.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MTR_PROCESSING_SIG_MODULE_ID      (0)
 #define MTR_PROCESSING_SIG_MAJOR_VERSION  (2)
 #define MTR_PROCESSING_SIG_MINOR_VERSION  (0)
 #define MTR_PROCESSING_SIG_PATCH_VERSION  (0)
 #define MTR_PROCESSING_SIG_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mtr_Processing_Sig_HdrBusType Mtr_Processing_SigBus;

/* Version Info */
extern const SwcVersionInfo_t Mtr_Processing_SigVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t Mtr_Processing_SigEemFailData;
extern AdcIf_Conv1msSwTrigMotInfo_t Mtr_Processing_SigSwTrigMotInfo;
extern Wss_SenWhlSpdInfo_t Mtr_Processing_SigWhlSpdInfo;
extern AdcIf_Conv50usHwTrigMotInfo_t Mtr_Processing_SigHwTrigMotInfo;
extern Ioc_InputSR50usMotCurrMonInfo_t Mtr_Processing_SigMotCurrMonInfo;
extern Ioc_InputSR50usMotAngleMonInfo_t Mtr_Processing_SigMotAngleMonInfo;
extern Eem_MainEemEceData_t Mtr_Processing_SigEemEceData;
extern Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_SigMtrProcessDataInf;
extern Mom_HndlrEcuModeSts_t Mtr_Processing_SigEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Mtr_Processing_SigIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Mtr_Processing_SigIgnEdgeSts;

/* Output Data Element */
extern Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_SigMtrProcessOutInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mtr_Processing_Sig_Init(void);
extern void Mtr_Processing_Sig(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTR_PROCESSING_SIG_H_ */
/** @} */
