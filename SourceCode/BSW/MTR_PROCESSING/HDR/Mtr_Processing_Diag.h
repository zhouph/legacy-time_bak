/**
 * @defgroup Mtr_Processing_Diag Mtr_Processing_Diag
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Diag.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTR_PROCESSING_DIAG_H_
#define MTR_PROCESSING_DIAG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Types.h"
#include "Mtr_Processing_Cfg.h"
#include "Mtr_Processing_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MTR_PROCESSING_DIAG_MODULE_ID      (0)
 #define MTR_PROCESSING_DIAG_MAJOR_VERSION  (2)
 #define MTR_PROCESSING_DIAG_MINOR_VERSION  (0)
 #define MTR_PROCESSING_DIAG_PATCH_VERSION  (0)
 #define MTR_PROCESSING_DIAG_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mtr_Processing_Diag_HdrBusType Mtr_Processing_DiagBus;

/* Version Info */
extern const SwcVersionInfo_t Mtr_Processing_DiagVersionInfo;

/* Input Data Element */
extern Arbitrator_VlvArbWhlVlvReqInfo_t Mtr_Processing_DiagArbWhlVlvReqInfo;
extern Eem_MainEemFailData_t Mtr_Processing_DiagEemFailData;
extern Eem_MainEemCtrlInhibitData_t Mtr_Processing_DiagEemCtrlInhibitData;
extern Ioc_InputSR1msPwrMonInfoEsc_t Mtr_Processing_DiagPwrMonInfoEsc;
extern Msp_CtrlMotRotgAgSigInfo_t Mtr_Processing_DiagMotRotgAgSigInfo;
extern Wss_SenWhlSpdInfo_t Mtr_Processing_DiagWhlSpdInfo;
extern Diag_HndlrDiagHndlRequest_t Mtr_Processing_DiagDiagHndlRequest;
extern Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_DiagMtrProcessOutInfo;

/* Output Data Element */
extern Mtr_Processing_DiagMtrProcessOutData_t Mtr_Processing_DiagMtrProcessOutData;
extern Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_DiagMtrProcessDataInf;
extern Mtr_Processing_DiagMtrDiagState_t Mtr_Processing_DiagMtrDiagState;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mtr_Processing_Diag_Init(void);
extern void Mtr_Processing_Diag(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTR_PROCESSING_DIAG_H_ */
/** @} */
