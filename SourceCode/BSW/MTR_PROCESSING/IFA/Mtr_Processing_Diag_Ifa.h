/**
 * @defgroup Mtr_Processing_Diag_Ifa Mtr_Processing_Diag_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Diag_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTR_PROCESSING_DIAG_IFA_H_
#define MTR_PROCESSING_DIAG_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo(data) do \
{ \
    *data = Mtr_Processing_DiagArbWhlVlvReqInfo; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagEemFailData(data) do \
{ \
    *data = Mtr_Processing_DiagEemFailData; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagEemCtrlInhibitData(data) do \
{ \
    *data = Mtr_Processing_DiagEemCtrlInhibitData; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagPwrMonInfoEsc(data) do \
{ \
    *data = Mtr_Processing_DiagPwrMonInfoEsc; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMotRotgAgSigInfo(data) do \
{ \
    *data = Mtr_Processing_DiagMotRotgAgSigInfo; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagWhlSpdInfo(data) do \
{ \
    *data = Mtr_Processing_DiagWhlSpdInfo; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo(data) do \
{ \
    *data = Mtr_Processing_DiagMtrProcessOutInfo; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[i]; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Mtr_Processing_DiagEemFailData.Eem_Fail_Motor; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    *data = Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt01Mon_Esc(data) do \
{ \
    *data = Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt02Mon_Esc(data) do \
{ \
    *data = Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt02Mon_Esc; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    *data = Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    *data = Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    *data = Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    *data = Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    *data = Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    *data = Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg; \
}while(0);

#define Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    *data = Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg; \
}while(0);


/* Set Output DE MAcro Function */
#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData(data) do \
{ \
    Mtr_Processing_DiagMtrProcessOutData = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessDataInf(data) do \
{ \
    Mtr_Processing_DiagMtrProcessDataInf = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData_MtrProIqFailsafe(data) do \
{ \
    Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData_MtrProIdFailsafe(data) do \
{ \
    Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData_MtrProUPhasePWM(data) do \
{ \
    Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData_MtrProVPhasePWM(data) do \
{ \
    Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData_MtrProWPhasePWM(data) do \
{ \
    Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessDataInf_MtrProCalibrationState(data) do \
{ \
    Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessDataInf_MtrCaliResult(data) do \
{ \
    Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult = *data; \
}while(0);

#define Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrDiagState(data) do \
{ \
    Mtr_Processing_DiagMtrDiagState = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTR_PROCESSING_DIAG_IFA_H_ */
/** @} */
