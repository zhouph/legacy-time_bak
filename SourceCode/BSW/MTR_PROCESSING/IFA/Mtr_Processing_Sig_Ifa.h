/**
 * @defgroup Mtr_Processing_Sig_Ifa Mtr_Processing_Sig_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Sig_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTR_PROCESSING_SIG_IFA_H_
#define MTR_PROCESSING_SIG_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo(data) do \
{ \
    *data = Mtr_Processing_SigSwTrigMotInfo; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigWhlSpdInfo(data) do \
{ \
    *data = Mtr_Processing_SigWhlSpdInfo; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigHwTrigMotInfo(data) do \
{ \
    *data = Mtr_Processing_SigHwTrigMotInfo; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotCurrMonInfo(data) do \
{ \
    *data = Mtr_Processing_SigMotCurrMonInfo; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotAngleMonInfo(data) do \
{ \
    *data = Mtr_Processing_SigMotAngleMonInfo; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemEceData(data) do \
{ \
    *data = Mtr_Processing_SigEemEceData; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMtrProcessDataInf(data) do \
{ \
    *data = Mtr_Processing_SigMtrProcessDataInf; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_BBSSol(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_ESCSol(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_FrontSol(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_RearSol(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_RearSol; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_Motor; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MPS(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_MPS; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MGD(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_MGD; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    *data = Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo_MotPwrMon(data) do \
{ \
    *data = Mtr_Processing_SigSwTrigMotInfo.MotPwrMon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo_StarMon(data) do \
{ \
    *data = Mtr_Processing_SigSwTrigMotInfo.StarMon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo_UoutMon(data) do \
{ \
    *data = Mtr_Processing_SigSwTrigMotInfo.UoutMon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo_VoutMon(data) do \
{ \
    *data = Mtr_Processing_SigSwTrigMotInfo.VoutMon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo_WoutMon(data) do \
{ \
    *data = Mtr_Processing_SigSwTrigMotInfo.WoutMon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Mtr_Processing_SigWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Mtr_Processing_SigWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Mtr_Processing_SigWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    *data = Mtr_Processing_SigHwTrigMotInfo.Uphase0Mon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    *data = Mtr_Processing_SigHwTrigMotInfo.Uphase1Mon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    *data = Mtr_Processing_SigHwTrigMotInfo.Vphase0Mon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    *data = Mtr_Processing_SigHwTrigMotInfo.VPhase1Mon; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    *data = Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    *data = Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    *data = Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    *data = Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    *data = Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1deg; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    *data = Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2deg; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    *data = Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    *data = Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEemEceData_Eem_Ece_Motor(data) do \
{ \
    *data = Mtr_Processing_SigEemEceData.Eem_Ece_Motor; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMtrProcessDataInf_MtrProCalibrationState(data) do \
{ \
    *data = Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigMtrProcessDataInf_MtrCaliResult(data) do \
{ \
    *data = Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigEcuModeSts(data) do \
{ \
    *data = Mtr_Processing_SigEcuModeSts; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigIgnOnOffSts(data) do \
{ \
    *data = Mtr_Processing_SigIgnOnOffSts; \
}while(0);

#define Mtr_Processing_Sig_Read_Mtr_Processing_SigIgnEdgeSts(data) do \
{ \
    *data = Mtr_Processing_SigIgnEdgeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTR_PROCESSING_SIG_IFA_H_ */
/** @} */
