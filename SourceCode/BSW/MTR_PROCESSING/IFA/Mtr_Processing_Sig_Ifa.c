/**
 * @defgroup Mtr_Processing_Sig_Ifa Mtr_Processing_Sig_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Sig_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Sig_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_SIG_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_CODE
#include "Mtr_Processing_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTR_PROCESSING_SIG_STOP_SEC_CODE
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
