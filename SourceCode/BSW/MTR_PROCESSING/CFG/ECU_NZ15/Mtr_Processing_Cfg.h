/**
 * @defgroup Mtr_Processing_Cfg Mtr_Processing_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTR_PROCESSING_CFG_H_
#define MTR_PROCESSING_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
  typedef struct
 {
     Saluint16  MulFactor;
     Saluint16  DivFactor;
     Saluint16  Offset;
 }Mtr_Processing_CurrSnsrCfgType;
 
typedef struct
{
	Saluint16 NumOfHole;
	Saluint16 FullElecAngle;
}Mtr_Processing_MtrSnsrCfgType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern const Mtr_Processing_CurrSnsrCfgType Mtr_Processing_CurSnsrConfig;
extern const Mtr_Processing_MtrSnsrCfgType Mtr_Processing_MpsSnsrConfig;
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTR_PROCESSING_CFG_H_ */
/** @} */
