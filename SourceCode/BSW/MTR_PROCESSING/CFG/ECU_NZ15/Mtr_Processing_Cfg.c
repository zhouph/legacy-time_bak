/**
 * @defgroup Mtr_Processing_Cfg Mtr_Processing_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/
const Mtr_Processing_CurrSnsrCfgType Mtr_Processing_CurSnsrConfig =
{
    (Saluint16) 100,		/* Voltage to Current Multiply factor */
    (Saluint16) 10,		/* Voltage to Current Divide factor */
    (Saluint16) 2500,	/* Offset */
};

const Mtr_Processing_MtrSnsrCfgType Mtr_Processing_MpsSnsrConfig =
{
    (Saluint16)    1,                            /* NumOfHole */
    (Saluint16) 2048                         /* FullElecAngle */
};
#define MTR_PROCESSING_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_CODE
#include "Mtr_Processing_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTR_PROCESSING_STOP_SEC_CODE
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
