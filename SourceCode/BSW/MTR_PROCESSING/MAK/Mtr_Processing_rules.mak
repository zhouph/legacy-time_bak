# \file
#
# \brief Mtr_Processing
#
# This file contains the implementation of the SWC
# module Mtr_Processing.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Mtr_Processing_src

Mtr_Processing_src_FILES        += $(Mtr_Processing_SRC_PATH)\Mtr_Processing_Sig.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_SRC_PATH)\Mtr_Processing_Sig_Process.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_IFA_PATH)\Mtr_Processing_Sig_Ifa.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_SRC_PATH)\Mtr_Processing_Diag.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_SRC_PATH)\Mtr_Processing_Diag_Process.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_IFA_PATH)\Mtr_Processing_Diag_Ifa.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_CFG_PATH)\Mtr_Processing_Cfg.c
Mtr_Processing_src_FILES        += $(Mtr_Processing_CAL_PATH)\Mtr_Processing_Cal.c

ifeq ($(ICE_COMPILE),true)
Mtr_Processing_src_FILES        += $(Mtr_Processing_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Mtr_Processing_src_FILES        += $(Mtr_Processing_UNITY_PATH)\unity.c
	Mtr_Processing_src_FILES        += $(Mtr_Processing_UNITY_PATH)\unity_fixture.c	
	Mtr_Processing_src_FILES        += $(Mtr_Processing_UT_PATH)\main.c
	Mtr_Processing_src_FILES        += $(Mtr_Processing_UT_PATH)\Mtr_Processing_Sig\Mtr_Processing_Sig_UtMain.c
	Mtr_Processing_src_FILES        += $(Mtr_Processing_UT_PATH)\Mtr_Processing_Diag\Mtr_Processing_Diag_UtMain.c
endif