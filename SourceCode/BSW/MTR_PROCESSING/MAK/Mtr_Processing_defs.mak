# \file
#
# \brief Mtr_Processing
#
# This file contains the implementation of the SWC
# module Mtr_Processing.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Mtr_Processing_CORE_PATH     := $(MANDO_BSW_ROOT)\Mtr_Processing
Mtr_Processing_CAL_PATH      := $(Mtr_Processing_CORE_PATH)\CAL\$(Mtr_Processing_VARIANT)
Mtr_Processing_SRC_PATH      := $(Mtr_Processing_CORE_PATH)\SRC
Mtr_Processing_CFG_PATH      := $(Mtr_Processing_CORE_PATH)\CFG\$(Mtr_Processing_VARIANT)
Mtr_Processing_HDR_PATH      := $(Mtr_Processing_CORE_PATH)\HDR
Mtr_Processing_IFA_PATH      := $(Mtr_Processing_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Mtr_Processing_CMN_PATH      := $(Mtr_Processing_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Mtr_Processing_UT_PATH		:= $(Mtr_Processing_CORE_PATH)\UT
	Mtr_Processing_UNITY_PATH	:= $(Mtr_Processing_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Mtr_Processing_UT_PATH)
	CC_INCLUDE_PATH		+= $(Mtr_Processing_UNITY_PATH)
	Mtr_Processing_Sig_PATH 	:= Mtr_Processing_UT_PATH\Mtr_Processing_Sig
	Mtr_Processing_Diag_PATH 	:= Mtr_Processing_UT_PATH\Mtr_Processing_Diag
endif
CC_INCLUDE_PATH    += $(Mtr_Processing_CAL_PATH)
CC_INCLUDE_PATH    += $(Mtr_Processing_SRC_PATH)
CC_INCLUDE_PATH    += $(Mtr_Processing_CFG_PATH)
CC_INCLUDE_PATH    += $(Mtr_Processing_HDR_PATH)
CC_INCLUDE_PATH    += $(Mtr_Processing_IFA_PATH)
CC_INCLUDE_PATH    += $(Mtr_Processing_CMN_PATH)

