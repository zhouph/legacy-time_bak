/**
 * @defgroup Mtr_Processing_Diag Mtr_Processing_Diag
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Diag.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Diag.h"
#include "Mtr_Processing_Diag_Ifa.h"
#include "Mtr_Processing_Diag_Process.h"
#include "Mps_TLE5012_Hndlr_1ms.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define ARB_VLV_MAX_CURRENT (2250)
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	Saluint8 Mtr_Stauts;
	Saluint32 Mtr_Offset_Position;
	Saluint32 Mtr_U_Position;
	Saluint32 Mtr_V_Position;
	Saluint32 Mtr_W_Position;
}Mtr_Processing_Diag_CommonInterType;

typedef struct
{
	Saluint8 Mtr_Init_Status;
	Saluint8 Mtr_Init_Excitation_Status;
}Mtr_Processing_DiagInit_Type;

typedef struct
{
	Saluint8 Mtr_CalState;
	Saluint8 Mtr_CalPhaseExcitation;
	Saluint8 Mtr_MtrDirectionState;
	Saluint8 Mtr_CW_PositionState;
	Saluint8 Mtr_CCW_PositionState;
	Saluint8 Mtr_Iq_Request_State;
	Saluint8 Mtr_RPM_Request_State;
}Mtr_Processing_DaigCal_Type;

typedef struct
{
	Saluint32 	Mtr_CWPhaseOffset;			/* CW Offset  저장 결과 값 */
	Saluint32 	Mtr_CWPositionAddValue;		/* CW Offset의 합 */
	Saluint8		Mtr_CWPositionAVRCnt;		/* CW Offset 저장 Count*/
	Saluint32		Mtr_CCWPhaseOffset; 		/* CCW Offset 저장 결과 값 */
	Saluint32		Mtr_CCWPositionAddValue;	/* CCW Offset의 합 */
	Saluint8		Mtr_CCWPositionAVRCnt;		/* CCW Offset 저장 Count*/
       Saluint32		Mtr_WholeCWOffsetPosition;		/* 4번의 모터 회전 중 CW Position값의 합 */
	Saluint32		Mtr_WholeCCWOffsetPosition;	/* 4번의 모터 회전 중 CCW Position값의 합 */
	Saluint8 		Mtr_CWPosiOffsetFlg;		
	Saluint8		Mtr_CCWPosiOffsetFlg;
	Saluint8		Mtr_Current_CompleteFlg;
}Mtr_Processing_DaigPhase_Type;

typedef struct
{
	Salsint32 DataBuff[10];
	Salsint32 MovAverMaxValue;
	Salsint32 MovAverMinValue;	
	Salsint32 AverValue;	
	Saluint8 SavedBufferNo;
}Mtr_Processing_DaigMtr_MovingAver_Type;

typedef struct
{
	Saluint8	Mtr_MPS_Get_Complete_Flg;
	Mtr_Processing_Diag_CommonInterType	Mtr_Inter_Common_Info;
	Mtr_Processing_DiagInit_Type			Mtr_Inter_Init_Info;
	Mtr_Processing_DaigCal_Type			Mtr_Inter_Cal_Info;
	Mtr_Processing_DaigPhase_Type		Mtr_Inter_U_Phase_Info;
	Mtr_Processing_DaigPhase_Type		Mtr_Inter_V_Phase_Info;
	Mtr_Processing_DaigPhase_Type		Mtr_Inter_W_Phase_Info;
	Mtr_Processing_DaigMtr_MovingAver_Type	Mtr_Inter_CW_RPM_Data;
	Mtr_Processing_DaigMtr_MovingAver_Type	Mtr_Inter_CCW_RPM_Data;
}Mtr_Processing_InternalVal_Info;


Mtr_Processing_InternalVal_Info Mtr_Process_Internal_Info; 

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_DIAG_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"

/* Extern Add Variables It must to change on next version */

/*************************************************/
void Mtr_Processing_Diag_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Processing_Diag_Process);
static void Mtr_Init_Check( Mtr_Processing_Diag_HdrBusType *pMtr_Init_Check,Mtr_Processing_InternalVal_Info *pMtr_Internal_Init_Check );
static void Mtr_ProcessDefine(Mtr_Processing_Diag_HdrBusType *pMtr_ProcessDefine,Mtr_Processing_InternalVal_Info *pMtr_Internal_ProcessDefine );
static void Mtr_ProcessInitExcitate(Mtr_Processing_Diag_HdrBusType *pMtr_PricessInitExcitate, Mtr_Processing_InternalVal_Info *pMtr_Internal_PricessInitExcitate);
static void Mtr_Diag_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Process,Mtr_Processing_InternalVal_Info *pMtr_Internal_Diag_Process);
static void Mtr_Diag_Calibration(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Calibration,Mtr_Processing_InternalVal_Info *pMtr_Internal_Diag_Calibration);
static void Mtr_Calibration_Current_Req(Mtr_Processing_Diag_HdrBusType *pMtr_Calibration_Current_Req ,Mtr_Processing_InternalVal_Info *pMtr_Internal_Calibration_Current_Req );
static void Mtr_Cal_CW_Position_Average(Mtr_Processing_DaigPhase_Type *PhaseInfo,Saluint8 Cnt);
static void Mtr_Cal_CCW_Position_Average(Mtr_Processing_DaigPhase_Type *PhaseInfo,Saluint8 Cnt);
static void Fif_u32MovingAverageCalc(Mtr_Processing_DaigMtr_MovingAver_Type *MoveAver,Saluint8 DataNo,Salsint32 NewData);
static void Mtr_Cal_Offset_Potion_CW_Compare(Mtr_Processing_InternalVal_Info *pCW_Compare_Internal);
static void Mtr_Cal_Offset_Potion_CCW_Compare(Mtr_Processing_InternalVal_Info *pCW_Compare_Internal);
static void Mtr_Cal_Offset_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Cal_Offset_Process ,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Offset_Process);
static void Mtr_Cal_Offset_Potion_CCW_Compare(Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Offset_Potion_CCW_Compare);
static void Mtr_Cal_Offset_Potion_CW_Compare(Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Offset_Potion_CW_Compare);
static void Mtr_Cal_Current_Sensor_Compare(Mtr_Processing_Diag_HdrBusType  *pMtr_Cal_Current_Sensor_Compare,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Current_Sensor_Compare);
static void Mtr_Cal_PhaPosition_Get(Mtr_Processing_Diag_HdrBusType *pMtr_Cal_PhaPosition_Get,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_PhaPosition_Get);
static void Mtr_Cal_PhaPosition_Calc(Mtr_Processing_Diag_HdrBusType *pMtr_Cal_PhaPosition_Calc,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_PhaPosition_Calc );
static Saluint8 Mtr_Process_MotorState(Mtr_Processing_Diag_HdrBusType *pMtr_Process_MotorState,Mtr_Processing_InternalVal_Info *pMtr_Internal_Process_MotorState);
static Saluint8 Mtr_Diag_Motor_Reqeust(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Mtr_Request,  Mtr_Processing_InternalVal_Info *pMtr_Internal_Diag_Mtr_Request);
static Saluint8 Mtr_Init_Ctr(Mtr_Processing_Diag_HdrBusType *pMtr_Init_Ctr,Mtr_Processing_InternalVal_Info *pMtr_Internal_Init_Ctr);
static Saluint8 Mtr_Init_Status(Mtr_Processing_Diag_HdrBusType *Mtr_Init_Status,Mtr_Processing_InternalVal_Info *pMtr_Internal_Init_Status);
static Saluint8 Mtr_MotInit_Phase_Compare(Saluint32 Phase_A,Saluint32 Phase_B,Saluint32 Phase_C);
static Saluint8 Mtr_Diag_CalibrationStatus(Mtr_Processing_Diag_HdrBusType *DiagInputinfo,Mtr_Processing_InternalVal_Info *Cal_StateInternal);
static Saluint8 Mtr_Diag_MtrDirection(Mtr_Processing_InternalVal_Info *Diag_ExcitationInternal);
static Saluint8 Mtr_Daig_ExcitationStatus(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Excitation,Mtr_Processing_InternalVal_Info *Diag_ExcitationInternal);
static Saluint8 Mtr_InitTestThresholdCalc(Mtr_Processing_Diag_HdrBusType *pMtr_InitTestThresholdCalc);
static Saluint8 Mtr_Vlv_Check(Mtr_Processing_Diag_HdrBusType *pMtr_Vlv_Check);

void Mtr_Processing_Diag_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Processing_Diag_Process)
{
	Mtr_Process_Internal_Info.Mtr_Inter_Common_Info.Mtr_Stauts	=Mtr_Process_MotorState(pMtr_Processing_Diag_Process,&Mtr_Process_Internal_Info);
	Mtr_ProcessDefine(pMtr_Processing_Diag_Process,&Mtr_Process_Internal_Info);
}


static Saluint8 Mtr_Process_MotorState(Mtr_Processing_Diag_HdrBusType *pMtr_Process_MotorState,Mtr_Processing_InternalVal_Info *pMtr_Internal_Process_MotorState)
{
	static Saluint8 Mtr_Status 		=DEF_MTR_INIT;
         
    	switch(Mtr_Status)
	{
		case DEF_MTR_INIT:
			if(
				(pMtr_Internal_Process_MotorState->Mtr_Inter_Init_Info.Mtr_Init_Status ==DEF_INIT_COMPLETE)
				||(pMtr_Internal_Process_MotorState->Mtr_Inter_Init_Info.Mtr_Init_Status ==DEF_INIT_SKIP)
				||(pMtr_Process_MotorState->Mtr_Processing_DiagEemFailData.Eem_Fail_Motor ==0) /* 현 전역 변수 사용 주의 */
			)
			{
				Mtr_Status =DEF_MTR_NORMAL;
			}
			else
			{
				if(pMtr_Internal_Process_MotorState->Mtr_Inter_Init_Info.Mtr_Init_Status==DEF_INIT_ERR)
				{
					Mtr_Status =DEF_MTR_ERROR;
				}
			}
			break;
		case DEF_MTR_NORMAL:
			if(pMtr_Process_MotorState->Mtr_Processing_DiagEemFailData.Eem_Fail_Motor==DEF_DETECT)/* 현 전역 변수 사용 주의 */
			{
				Mtr_Status =DEF_MTR_ERROR;
			}
			else
			{
				if(pMtr_Process_MotorState->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order != DEF_NOT_REQUEST)
				{
					Mtr_Status =DEF_MTR_DIAG;
				}
			}
			break;
		case DEF_MTR_DIAG:
			if(pMtr_Internal_Process_MotorState->Mtr_Inter_Cal_Info.Mtr_CalState==DEF_MTR_CAL_STATE_COMPLETE)
			{
				Mtr_Status = DEF_MTR_NORMAL;
			}
			break;
		case DEF_MTR_ERROR:
			if(pMtr_Process_MotorState->Mtr_Processing_DiagEemFailData.Eem_Fail_Motor==DEF_NOT_DETECT)/* 현 전역 변수 사용 주의 */
			{
				Mtr_Status =DEF_MTR_NORMAL;
			}
			break;
		default:
			break;
	}
	return Mtr_Status;
}

static void Mtr_ProcessDefine(Mtr_Processing_Diag_HdrBusType *pMtr_ProcessDefine,Mtr_Processing_InternalVal_Info *pMtr_Internal_ProcessDefine )
{
	if(pMtr_Internal_ProcessDefine->Mtr_Inter_Common_Info.Mtr_Stauts==DEF_MTR_INIT)
	{
		Mtr_Init_Check(pMtr_ProcessDefine,pMtr_Internal_ProcessDefine);
	}
	else if(pMtr_Internal_ProcessDefine->Mtr_Inter_Common_Info.Mtr_Stauts==DEF_MTR_DIAG)
	{
		Mtr_Diag_Process(pMtr_ProcessDefine,pMtr_Internal_ProcessDefine);	
	}
	else
	{
		;
	}
	pMtr_ProcessDefine->Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState =Mtr_Diag_Motor_Reqeust(pMtr_ProcessDefine,pMtr_Internal_ProcessDefine);
}
static void Mtr_Init_Check( Mtr_Processing_Diag_HdrBusType *pMtr_Init_Check,Mtr_Processing_InternalVal_Info *pMtr_Internal_Init_Check )
{
	Mtr_Process_Internal_Info.Mtr_Inter_Init_Info.Mtr_Init_Status =Mtr_Init_Status(pMtr_Init_Check,pMtr_Internal_Init_Check);
	Mtr_Process_Internal_Info.Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status =Mtr_Init_Ctr(pMtr_Init_Check,pMtr_Internal_Init_Check);
	
	Mtr_ProcessInitExcitate(pMtr_Init_Check,pMtr_Internal_Init_Check);
}
static Saluint8 Mtr_Init_Status(Mtr_Processing_Diag_HdrBusType *pMtr_Init_Status,Mtr_Processing_InternalVal_Info *pMtr_Internal_Init_Status)
{
    static Saluint8 Mtr_Init = DEF_INIT_START;
    static Saluint8 Mtr_Init_Cnt	=0;
	switch(Mtr_Init)
	{
		case DEF_INIT_START: /* Saved Position reading wait*/
			if(    
				(pMtr_Init_Status->Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail ==0)
				&&(MpsD1_Compen_SetFag_at1ms==1)
                   &&(mgd9180_configure_complete ==1)
                       )
			{
				Mtr_Init = DEF_INIT_CHECK;
			}
			break;
		case DEF_INIT_CHECK:
			if(pMtr_Internal_Init_Status->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_EXCITATION_END)
			{
				Mtr_Init =DEF_INIT_COMPLETE;
			}
			else if(pMtr_Internal_Init_Status->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_EXCITATION_ERR)
			{
				Mtr_Init =DEF_INIT_RE_CHECK;
			}
			else
			{
				;
			}
			break;
		case DEF_INIT_RE_CHECK:
			if(pMtr_Internal_Init_Status->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_EXCITATION_END)
			{
				Mtr_Init =DEF_INIT_COMPLETE;
			}
			else if(pMtr_Internal_Init_Status->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_EXCITATION_ERR)
			{
				Mtr_Init =DEF_INIT_ERR;
			}
			else
			{
				;
			}
			break;
		case DEF_INIT_ERR:
		case DEF_INIT_SKIP:
		case DEF_INIT_COMPLETE:
			break;
		default:
			break;
	}

	return Mtr_Init;
}

static Saluint8 Mtr_Init_Ctr(Mtr_Processing_Diag_HdrBusType *pMtr_Init_Ctr,Mtr_Processing_InternalVal_Info *pMtr_Internal_Init_Ctr)
{
	static Saluint8 CtrStatus 					=DEF_INITCTR_INIT;
	static Saluint8	fu8ExcitationStateThreshold	=DEF_MTR_INIT_CTR_1ST;
	static Saluint8	Init_Excitation_test_Cnt		=COUNT_INIT;
	static Saluint8 CompareSatus				=DEF_PHASE_INIT;
	static Saluint32 PositionCompare; 

	fu8ExcitationStateThreshold =Mtr_InitTestThresholdCalc(pMtr_Init_Ctr);
	
	switch(CtrStatus)
	{
		case DEF_INITCTR_INIT:
			if(pMtr_Internal_Init_Ctr->Mtr_Inter_Init_Info.Mtr_Init_Status ==DEF_INIT_CHECK)
			{
				CtrStatus	=DEF_INITCTR_U_EXCITATION;
			}
			break;
		case DEF_INITCTR_U_EXCITATION: 			
			CtrStatus =DEF_INITCTR_V_EXCITATION;
			break;
		case DEF_INITCTR_V_EXCITATION:
			if(Init_Excitation_test_Cnt==fu8ExcitationStateThreshold)
			{
				CtrStatus =DEF_INITCTR_W_EXCITATION;
				pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_V_Position = pMtr_Init_Ctr->Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
				Init_Excitation_test_Cnt =COUNT_INIT;
			}
			else
			{
				Init_Excitation_test_Cnt++;
			}
			break;
		case DEF_INITCTR_W_EXCITATION:
			if(Init_Excitation_test_Cnt==fu8ExcitationStateThreshold)
			{
				CtrStatus =DEF_INITCTR_U_2ND_EXCITATION;
				pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_W_Position = pMtr_Init_Ctr->Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
				Init_Excitation_test_Cnt =COUNT_INIT;
			}
			else
			{
				Init_Excitation_test_Cnt++;
			}
			break;
		case DEF_INITCTR_U_2ND_EXCITATION:	
			if(Init_Excitation_test_Cnt==fu8ExcitationStateThreshold)
			{
				CtrStatus =DEF_INITCTR_EXCITATION_CHECK;
				pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_U_Position = pMtr_Init_Ctr->Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
				Init_Excitation_test_Cnt =COUNT_INIT;
			}
			else
			{
				Init_Excitation_test_Cnt++;
			}
			break;
		case DEF_INITCTR_EXCITATION_CHECK:
			CompareSatus=Mtr_MotInit_Phase_Compare(pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_U_Position ,pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_V_Position ,pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_W_Position );
			if(CompareSatus==DEF_PHASE_COMPLTE)
			{
				PositionCompare=(Saluint32)abs(pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_U_Position -pMtr_Internal_Init_Ctr->Mtr_Inter_Common_Info.Mtr_Offset_Position);
				if(PositionCompare>DEF_ELEC_ANGLE_10deg)
				{
					CtrStatus =DEF_INITCTR_EXCITATION_ERR;
				}
				else
				{
					CtrStatus =DEF_INITCTR_EXCITATION_END;
				}
			}
			else
			{
				CtrStatus =DEF_INITCTR_EXCITATION_ERR;
			}
			break;
		case DEF_INITCTR_EXCITATION_ERR:
			if(pMtr_Internal_Init_Ctr->Mtr_Inter_Init_Info.Mtr_Init_Status == DEF_INIT_RE_CHECK)
			{
				CtrStatus =DEF_INITCTR_V_EXCITATION;
			}
			break;
		case DEF_INITCTR_EXCITATION_END:
			break;
		default:
			break;
	}
	return CtrStatus;
}

static Saluint8 Mtr_InitTestThresholdCalc(Mtr_Processing_Diag_HdrBusType *pMtr_InitTestThresholdCalc)
{
	Saluint16 threhsoldvoltage =pMtr_InitTestThresholdCalc->Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc;
	Saluint8  thresholdcount	 =DEF_MTR_INIT_CTR_1ST;
	if(threhsoldvoltage>DEF_INIT_THRESHOLD_VOLT)
	{
		thresholdcount = DEF_MTR_INIT_CTR_1ST;
	}
	else
	{
		thresholdcount=DEF_MTR_INIT_CTR_2ND;
	}

	return thresholdcount;
}


static Saluint8 Mtr_MotInit_Phase_Compare(Saluint32 Phase_A,Saluint32 Phase_B,Saluint32 Phase_C)
{
        Saluint32 Position[3]={0,0,0};
        Saluint32 u32_Phase_diff     =0;
        Saluint32 u32_Phase_diff_2 =0;
        Saluint8 u8_phaseinitStatus = DEF_PHASE_INIT;
        Saluint8 i =0;
        Saluint8 j=0;
        Saluint32 Temp=0;
      
        Position[0] = Phase_A;
        Position[1] = Phase_B;
        Position[2] = Phase_C;

        for(i=0;i<2;i++) /* bubble sort */
       {
            for(j=0;j<2;j++)
            {
                if (Position[j] >Position[j+1])
                {
                    Temp=Position[j];
                    Position[j] =Position[j+1];
                    Position[j+1]=Temp;
                }
            }
       }
       u32_Phase_diff       =Position[1]-Position[0];
       u32_Phase_diff_2    =Position[2]-Position[1];
	
	if(
		(u32_Phase_diff<DEF_ELEC_ANGLE_115deg)
		||(u32_Phase_diff>DEF_ELEC_ANGLE_125deg)
		||(u32_Phase_diff_2<DEF_ELEC_ANGLE_115deg)
		||(u32_Phase_diff_2>DEF_ELEC_ANGLE_125deg)
		
	)
	{
		u8_phaseinitStatus = DEF_PHASE_FAIL;
	}
	else
	{
		u8_phaseinitStatus = DEF_PHASE_COMPLTE;
	}

	return u8_phaseinitStatus;
}

static void Mtr_ProcessInitExcitate(Mtr_Processing_Diag_HdrBusType *pMtr_PricessInitExcitate, Mtr_Processing_InternalVal_Info *pMtr_Internal_PricessInitExcitate)
{
	Saluint32 U_PHASER_PWM	=DEF_PHASE_INIT;
	Saluint32	V_PHASER_PWM	=DEF_PHASE_INIT;
	Saluint32 W_PHASER_PWM	=DEF_PHASE_INIT;
	Saluint32 Gainvalue		=DEF_INIT_GAIN;
	if(pMtr_Internal_PricessInitExcitate->Mtr_Inter_Init_Info.Mtr_Init_Status==DEF_INIT_RE_CHECK )
	{
		Gainvalue	=DEF_INIT_REGAIN;
	}
	if(pMtr_Internal_PricessInitExcitate->Mtr_Inter_Common_Info.Mtr_Stauts ==DEF_MTR_INIT)
	{
		if((pMtr_Internal_PricessInitExcitate->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_U_EXCITATION)||(pMtr_Internal_PricessInitExcitate->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_U_2ND_EXCITATION))
		{
			U_PHASER_PWM =DEF_CAL_MAX_DUTY*Gainvalue;
			V_PHASER_PWM =DEF_CAL_MIN_DUTY*Gainvalue;
			W_PHASER_PWM =DEF_CAL_MIN_DUTY*Gainvalue;
		}
		else if(pMtr_Internal_PricessInitExcitate->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_V_EXCITATION)
		{
			U_PHASER_PWM =DEF_CAL_MIN_DUTY*Gainvalue;
			V_PHASER_PWM =DEF_CAL_MAX_DUTY*Gainvalue;
			W_PHASER_PWM =DEF_CAL_MIN_DUTY*Gainvalue;
			
		}
		else if(pMtr_Internal_PricessInitExcitate->Mtr_Inter_Init_Info.Mtr_Init_Excitation_Status ==DEF_INITCTR_W_EXCITATION)
		{
			U_PHASER_PWM =DEF_CAL_MIN_DUTY*Gainvalue;
			V_PHASER_PWM =DEF_CAL_MIN_DUTY*Gainvalue;
			W_PHASER_PWM =DEF_CAL_MAX_DUTY*Gainvalue;
		}
		else
		{
			;
		}
	}

	pMtr_PricessInitExcitate->Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM	=U_PHASER_PWM;
	pMtr_PricessInitExcitate->Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM	=V_PHASER_PWM;
	pMtr_PricessInitExcitate->Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM	=W_PHASER_PWM;
}


static void Mtr_Diag_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Process,Mtr_Processing_InternalVal_Info *pMtr_Internal_Diag_Process)
{
	static Saluint8 Diag_Cnt =0;
	switch (pMtr_Diag_Process->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order)
	{
		case DEF_LAB_CALIBRATION:
		case DEF_CAR_CALIBRATION:
			Mtr_Diag_Calibration(pMtr_Diag_Process,pMtr_Internal_Diag_Process);
			break;
		case DEF_PWM_HOLD:
			if(
                            (pMtr_Diag_Process->Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail ==0)
                     )
			{
				Diag_Cnt =0;
				pMtr_Diag_Process->Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM=pMtr_Diag_Process->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM;
				pMtr_Diag_Process->Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM=pMtr_Diag_Process->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM;
				pMtr_Diag_Process->Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM=pMtr_Diag_Process->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM;
				/* Diag 긍정 응답 */
			}
			else
			{
				if((Diag_Cnt >15)||(pMtr_Diag_Process->Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail ==1))
				{
					Diag_Cnt =0;
					/* Diag 부정응답 */
				}
				else
				{
					Diag_Cnt++;
				}
			}
			break;
		case DEF_IQ_ID_HOLD:
			if(
				(pMtr_Diag_Process->Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail ==0)
			)
			{
				Diag_Cnt =0;
				pMtr_Diag_Process->Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe=pMtr_Diag_Process->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id;
				pMtr_Diag_Process->Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe=pMtr_Diag_Process->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq;
			}
			else
			{
				if((Diag_Cnt >15)||(pMtr_Diag_Process->Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail ==1))
				{
					Diag_Cnt =0;
					/* Diag 부정응답 */
				}
				else
				{
					Diag_Cnt++;
				}
			}
			break;
		//case DEF_STROKE_TEST:
              case DEF_DIAG_STOP:
			break;
		default:
			break;
	}
}


static void Mtr_Diag_Calibration(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Calibration,Mtr_Processing_InternalVal_Info *pMtr_Internal_Diag_Calibration)
{
	pMtr_Internal_Diag_Calibration->Mtr_Inter_Cal_Info.Mtr_CalState =Mtr_Diag_CalibrationStatus(pMtr_Diag_Calibration,pMtr_Internal_Diag_Calibration);			/* Motor status =>order*/
	pMtr_Internal_Diag_Calibration->Mtr_Inter_Cal_Info.Mtr_MtrDirectionState = Mtr_Diag_MtrDirection(pMtr_Internal_Diag_Calibration);
	pMtr_Internal_Diag_Calibration->Mtr_Inter_Cal_Info.Mtr_CalPhaseExcitation = Mtr_Daig_ExcitationStatus(pMtr_Diag_Calibration,pMtr_Internal_Diag_Calibration);//FT_Cal_Phase_array();			/* Motor 정렬 명령 함수 =>order */
	Mtr_Cal_Offset_Process(pMtr_Diag_Calibration,pMtr_Internal_Diag_Calibration);		/* Motor Position 계산 함수=> sensing *//* Modul Error Check */
}

static Saluint8 Mtr_Diag_CalibrationStatus(Mtr_Processing_Diag_HdrBusType *DiagInputinfo,Mtr_Processing_InternalVal_Info *Cal_StateInternal)
{
	static Saluint8 MtrCalStatus = DEF_MTR_CAL_STATE_INIT;
	static Saluint16 MtrCal_WaitCnt		=0;
	static Saluint16 MtrCal_MotionCnt 	=0;
	switch(MtrCalStatus)
	{
		case DEF_MTR_CAL_STATE_INIT:
			if(
				(DiagInputinfo->Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail ==0)
			)
			{
				MtrCalStatus =DEF_MTR_CAL_STATE_WAIT;
			}
			break;
		case DEF_MTR_CAL_STATE_WAIT:
			if(MtrCal_WaitCnt >=DEF_MTR_CAL_STATE_WAIT_LIMIT)
			{
				MtrCalStatus = DEF_MTR_CAL_STATE_CW;
				MtrCal_WaitCnt =0;
			}
			else
			{
				MtrCal_WaitCnt++;
			}
			break;
		case DEF_MTR_CAL_STATE_CW:
			if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState ==DEF_OFFSETDETECT_FAIL)
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_CCW_ADD;
				MtrCal_WaitCnt	=0;
				MtrCal_MotionCnt   =0;
			}
			else if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState ==DEF_OFFSETDETECT_COMPLETE)
			{ 
				MtrCal_MotionCnt++;
				Mtr_Cal_CW_Position_Average(&(Cal_StateInternal->Mtr_Inter_U_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CW_Position_Average(&(Cal_StateInternal->Mtr_Inter_V_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CW_Position_Average(&(Cal_StateInternal->Mtr_Inter_W_Phase_Info),MtrCal_MotionCnt);

				if(MtrCal_MotionCnt >=MTR_MOTION_CNT_LIMIT)
				{ 
					MtrCalStatus		=DEF_MTR_CAL_STATE_CCW;
					MtrCal_MotionCnt 	=0;
				}
				MtrCal_WaitCnt =0;
			}
			else if(MtrCal_WaitCnt>=DEF_MTR_WAIT_CNT_LIMIT)
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_ERR;
				MtrCal_WaitCnt	=0;
				MtrCal_MotionCnt	=0;
			}
			else
			{
				MtrCal_WaitCnt++;
			}
			break;
		case DEF_MTR_CAL_STATE_CW_ADD:
			if((Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState==DEF_OFFSETDETECT_FAIL)||(MtrCal_WaitCnt>=DEF_MTR_WAIT_CNT_LIMIT))
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_ERR;
				MtrCal_WaitCnt	=0;
				MtrCal_MotionCnt  	=0;
			}
			else if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState==DEF_OFFSETDETECT_COMPLETE)
			{
				MtrCal_MotionCnt ++;
				Mtr_Cal_CW_Position_Average(&(Cal_StateInternal->Mtr_Inter_U_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CW_Position_Average(&(Cal_StateInternal->Mtr_Inter_V_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CW_Position_Average(&(Cal_StateInternal->Mtr_Inter_W_Phase_Info),MtrCal_MotionCnt);

				if(MtrCal_MotionCnt >=DEF_MTR_WAIT_CNT_LIMIT)
				{
					if(DiagInputinfo->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order ==DEF_CAR_CALIBRATION)
					{
						MtrCalStatus	=DEF_MTR_CAL_STATE_COMPLETE;
					}
					else
					{
						MtrCalStatus	=DEF_MTR_CAL_STATE_IQ_CHECK;
					}
					MtrCal_MotionCnt	=0;
				}
				MtrCal_WaitCnt	=0;
			}
			else
			{
				MtrCal_WaitCnt++;
			}
			break;
		case DEF_MTR_CAL_STATE_CCW:
			if((Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState==DEF_OFFSETDETECT_FAIL)||(MtrCal_WaitCnt>=DEF_MTR_WAIT_CNT_LIMIT))
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_ERR;
				MtrCal_WaitCnt	=0;
				MtrCal_MotionCnt  	=0;
			}
			else if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState==DEF_OFFSETDETECT_COMPLETE)
			{
				MtrCal_MotionCnt ++;
				Mtr_Cal_CCW_Position_Average(&(Cal_StateInternal->Mtr_Inter_U_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CCW_Position_Average(&(Cal_StateInternal->Mtr_Inter_V_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CCW_Position_Average(&(Cal_StateInternal->Mtr_Inter_W_Phase_Info),MtrCal_MotionCnt);
				if(MtrCal_MotionCnt >=DEF_MTR_WAIT_CNT_LIMIT)
				{

					if(DiagInputinfo->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order ==DEF_CAR_CALIBRATION)
					{
						MtrCalStatus	=DEF_MTR_CAL_STATE_COMPLETE;
					}
					else
					{
						MtrCalStatus	=DEF_MTR_CAL_STATE_IQ_CHECK;
					}
					MtrCal_MotionCnt  	=0;
				}
				MtrCal_WaitCnt=0;	
			}
			else
			{
				MtrCal_WaitCnt++;
			}
			break;
		case DEF_MTR_CAL_STATE_CCW_ADD:
			if((Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState==DEF_OFFSETDETECT_FAIL)||(MtrCal_WaitCnt>=DEF_MTR_WAIT_CNT_LIMIT))
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_ERR;
				MtrCal_WaitCnt	=0;
				MtrCal_MotionCnt  	=0;
			}
			else if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState==DEF_OFFSETDETECT_COMPLETE)
			{
				MtrCal_MotionCnt ++;
				Mtr_Cal_CCW_Position_Average(&(Cal_StateInternal->Mtr_Inter_U_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CCW_Position_Average(&(Cal_StateInternal->Mtr_Inter_V_Phase_Info),MtrCal_MotionCnt);
				Mtr_Cal_CCW_Position_Average(&(Cal_StateInternal->Mtr_Inter_W_Phase_Info),MtrCal_MotionCnt);
				if(MtrCal_MotionCnt >=MTR_MOTION_CNT_LIMIT)
				{
					MtrCalStatus		=DEF_MTR_CAL_STATE_CW_ADD;
					MtrCal_MotionCnt 	=0;
				}
				MtrCal_WaitCnt	=0;
			}
			else
			{
				MtrCal_WaitCnt++;
			}
			break;
		case DEF_MTR_CAL_STATE_ERR:
			/* 부정 응답 */
			//cdu1MSPCalEnd=ON;
			//cdu1MSPCalInhibitFlg=ON;
			//Mtr_Processing_Valve_Open_Request = DEF_VALVE_CLOSE;
			if(MtrCal_WaitCnt>=DEF_MTR_WAIT_CNT_LIMIT)
			{
				MtrCal_WaitCnt	=0;
 				MtrCalStatus		=DEF_MTR_CAL_STATE_INIT;
			}
			else
			{
				MtrCal_WaitCnt++;
			}
			break;
		case DEF_MTR_CAL_STATE_IQ_CHECK:
			//FT_Calibration_Iq_Req();/* Iq test */
			Mtr_Calibration_Current_Req(DiagInputinfo,Cal_StateInternal);
			if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_Iq_Request_State ==DEF_IQ_REQ_COMPLETE)
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_COMPLETE;//DEF_MTR_CAL_STATE_RPM_CHECK;;
			}
			else if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_Iq_Request_State==DEF_IQ_REQ_FAIL)
			{
				MtrCalStatus		=DEF_MTR_CAL_STATE_ERR;
			}
			else
			{
				;
			}
			break;
		case DEF_MTR_CAL_STATE_RPM_CHECK:
			//FT_Calibration_RPM_Req();/* RPM test */
			if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_Iq_Request_State==DEF_IQ_REQ_COMPLETE)
			 {
			 	MtrCalStatus		=DEF_MTR_CAL_STATE_COMPLETE;
			 }
			 else if(Cal_StateInternal->Mtr_Inter_Cal_Info.Mtr_Iq_Request_State==DEF_IQ_REQ_FAIL)
			 {
			 	MtrCalStatus		=DEF_MTR_CAL_STATE_ERR;
			 }
			 else
			 {
			 	;
			 }
			break;
		case DEF_MTR_CAL_STATE_COMPLETE:
                     MtrCalStatus				= DEF_MTR_CAL_STATE_INIT;
			#if 0
			cdu1MSPCalEnd=ON;
			cdu1MSPCalInhibitFlg=OFF;
			/* reposition*/
			fu16EEPMotCWOffset_a		=fu16_U_CW_PhaseOffset;
			fu16EEPMotCWOffset_b		=fu16_V_CW_PhaseOffset;
			fu16EEPMotCWOffset_c		=fu16_W_CW_PhaseOffset;
			fu16EEPMotCCWOffset_a	=fu16_U_CCW_PhaseOffset;
			fu16EEPMotCCWOffset_b	=fu16_V_CCW_PhaseOffset;
			fu16EEPMotCCWOffset_c	=fu16_W_CCW_PhaseOffset;
			#endif
			break;
		default:
			break;
	}
	return MtrCalStatus;
}
static Saluint8 Mtr_Diag_MtrDirection(Mtr_Processing_InternalVal_Info *Diag_ExcitationInternal)
{
	Saluint8 MtrDirection =(Saluint8)((Diag_ExcitationInternal->Mtr_Inter_Cal_Info.Mtr_CalState)&DEF_MTR_DIR_MASK);
	return MtrDirection;
}
static Saluint8 Mtr_Daig_ExcitationStatus(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Excitation,Mtr_Processing_InternalVal_Info *Diag_ExcitationInternal)
{
	Saluint8	fu8MotorCalPhaseState =DEF_CAL_EXCITATION_PHASE_INIT;
	Saluint8	fu8MotorDirection	=Diag_ExcitationInternal->Mtr_Inter_Cal_Info.Mtr_MtrDirectionState;
	switch(fu8MotorCalPhaseState)
	{
		case DEF_CAL_EXCITATION_PHASE_INIT:
			if(fu8MotorDirection != DEF_MTR_STOP)/* fu8MotorCalState != INIT or CAL_COMPLETE or CAL_ERROR */
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_U_PHASE;
			}
		break;
		case DEF_CAL_EXCITATION_U_PHASE:	 /* u상 정렬 */
			if((fu8MotorDirection ==DEF_MTR_CW)&&( Diag_ExcitationInternal->Mtr_Inter_U_Phase_Info.Mtr_CWPosiOffsetFlg ==ON))
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_V_PHASE_WAIT;			/* Motor Change */
			}
			else if((fu8MotorDirection ==DEF_MTR_CCW)&&( Diag_ExcitationInternal->Mtr_Inter_U_Phase_Info.Mtr_CCWPosiOffsetFlg==ON))
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_U_PHASE_WAIT;			/* Motor Change */
			}
			else if(fu8MotorDirection ==DEF_MTR_STOP)/* fu8MotorCalState = INIT or CAL_COMPLETE or CAL_ERROR */
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_PHASE_INIT;
			}
			else if((Diag_ExcitationInternal->Mtr_Inter_U_Phase_Info.Mtr_CWPosiOffsetFlg ==OFF)
				||(Diag_ExcitationInternal->Mtr_Inter_U_Phase_Info.Mtr_CCWPosiOffsetFlg==OFF)
				)
			{
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM	=DEF_CAL_MAX_DUTY*DEF_CAL_GAIN;					
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM	=DEF_CAL_MIN_DUTY*DEF_CAL_GAIN;
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM	=DEF_CAL_MIN_DUTY*DEF_CAL_GAIN;
			}
			else
			{
				; /* No Operation*/
			}
		break;
		case DEF_CAL_EXCITATION_U_PHASE_WAIT:
			if(fu8MotorDirection ==DEF_MTR_CW)
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_U_PHASE;
			}
			else if(fu8MotorDirection ==DEF_MTR_CCW)
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_W_PHASE;
			}
			else
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_PHASE_INIT;
			}
			
		break;
		case DEF_CAL_EXCITATION_V_PHASE: /* V상 정렬*/
			if((fu8MotorDirection ==DEF_MTR_CW)&&((Diag_ExcitationInternal->Mtr_Inter_V_Phase_Info.Mtr_CWPosiOffsetFlg==ON)/*||(fu8_CW_Position_Offset_State	==OFFSET_RETEST)||(fu8_CW_Position_Offset_State ==OFFSET_COMPLETE)*/))
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_W_PHASE_WAIT;
			}
			else if((fu8MotorDirection ==DEF_MTR_CCW)
				&&((Diag_ExcitationInternal->Mtr_Inter_V_Phase_Info.Mtr_CCWPosiOffsetFlg==ON)\
					||(Diag_ExcitationInternal->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState==DEF_OFFSETDETECT_RETEST)\
					||(Diag_ExcitationInternal->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState ==DEF_OFFSETDETECT_COMPLETE)
					)
			)
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_V_PHASE_WAIT;
			}
			else if(fu8MotorDirection ==DEF_MTR_STOP )
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_PHASE_INIT;
			}
			else if((Diag_ExcitationInternal->Mtr_Inter_V_Phase_Info.Mtr_CWPosiOffsetFlg==OFF)
				||(Diag_ExcitationInternal->Mtr_Inter_V_Phase_Info.Mtr_CCWPosiOffsetFlg==OFF)
				)
			{
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM	=DEF_CAL_MIN_DUTY*DEF_CAL_GAIN;
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM	=DEF_CAL_MIN_DUTY*DEF_CAL_GAIN;
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM	=DEF_CAL_MAX_DUTY*DEF_CAL_GAIN;
			}
			else
			{
				; /* No Operation*/
			}
		break;
		case DEF_CAL_EXCITATION_V_PHASE_WAIT:

			if(fu8MotorDirection ==DEF_MTR_CW)
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_V_PHASE;			/* Motor Change */
			}
			else if(fu8MotorDirection ==DEF_MTR_CCW)
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_U_PHASE;			/* Motor Change */
			}
			else
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_PHASE_INIT;
			}
		break;
		case DEF_CAL_EXCITATION_W_PHASE: /* V상 정렬 <--Lab  Test 시 W 상*/
			if((fu8MotorDirection ==DEF_MTR_CW)
				&&((Diag_ExcitationInternal->Mtr_Inter_W_Phase_Info.Mtr_CWPosiOffsetFlg==ON)\
				||(Diag_ExcitationInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState ==DEF_OFFSETDETECT_RETEST)\
				||(Diag_ExcitationInternal->Mtr_Inter_Cal_Info.Mtr_CW_PositionState ==DEF_OFFSETDETECT_COMPLETE)
				)
			)
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_U_PHASE_WAIT;
			}
			else if((fu8MotorDirection ==DEF_MTR_CCW)&&(Diag_ExcitationInternal->Mtr_Inter_W_Phase_Info.Mtr_CCWPosiOffsetFlg==ON))
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_W_PHASE_WAIT;
			}
			else if(fu8MotorDirection ==DEF_MTR_STOP)
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_PHASE_INIT;
			}
			else if((Diag_ExcitationInternal->Mtr_Inter_W_Phase_Info.Mtr_CWPosiOffsetFlg==OFF)||(Diag_ExcitationInternal->Mtr_Inter_W_Phase_Info.Mtr_CCWPosiOffsetFlg==OFF))
			{
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM	=DEF_CAL_MIN_DUTY*DEF_CAL_GAIN;
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM	=DEF_CAL_MAX_DUTY*DEF_CAL_GAIN;
					pMtr_Diag_Excitation->Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM	=DEF_CAL_MIN_DUTY*DEF_CAL_GAIN;
			}
			else
			{
				; /* No Operation*/
			}
		break;		
		case DEF_CAL_EXCITATION_W_PHASE_WAIT:
			if(fu8MotorDirection ==DEF_MTR_CW)
			{
				fu8MotorCalPhaseState =DEF_CAL_EXCITATION_W_PHASE;			/* Motor Change */
			}
			else if(fu8MotorDirection ==DEF_MTR_CCW)
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_V_PHASE;		/* Motor Change */
			}
			else
			{
				fu8MotorCalPhaseState = DEF_CAL_EXCITATION_PHASE_INIT;
			}
		break;
		
		default:/* No Operationg */
		break;
	}
	return fu8MotorCalPhaseState;
}

static void Mtr_Cal_Offset_Process(Mtr_Processing_Diag_HdrBusType *pMtr_Cal_Offset_Process ,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Offset_Process)
{
	Mtr_Cal_Current_Sensor_Compare(pMtr_Cal_Offset_Process,pMtr_Internal_Cal_Offset_Process);
	Mtr_Cal_PhaPosition_Get(pMtr_Cal_Offset_Process,pMtr_Internal_Cal_Offset_Process);
	Mtr_Cal_PhaPosition_Calc(pMtr_Cal_Offset_Process,pMtr_Internal_Cal_Offset_Process);
	Mtr_Cal_Offset_Potion_CW_Compare(pMtr_Internal_Cal_Offset_Process);
	Mtr_Cal_Offset_Potion_CCW_Compare(pMtr_Internal_Cal_Offset_Process);
}
static void Mtr_Cal_CW_Position_Average(Mtr_Processing_DaigPhase_Type *PhaseInfo,Saluint8 Cnt)
{
	PhaseInfo->Mtr_WholeCWOffsetPosition += PhaseInfo ->Mtr_CWPhaseOffset;
	if(Cnt ==MTR_MOTION_CNT_LIMIT)
	{
		PhaseInfo->Mtr_CWPhaseOffset=PhaseInfo->Mtr_WholeCWOffsetPosition/Cnt;
		PhaseInfo->Mtr_WholeCWOffsetPosition =0;
	}
}
static void Mtr_Cal_CCW_Position_Average(Mtr_Processing_DaigPhase_Type *PhaseInfo,Saluint8 Cnt)
{
	PhaseInfo->Mtr_WholeCCWOffsetPosition += PhaseInfo ->Mtr_CCWPhaseOffset;
	if(Cnt ==MTR_MOTION_CNT_LIMIT)
	{
		PhaseInfo->Mtr_CCWPhaseOffset=PhaseInfo->Mtr_WholeCCWOffsetPosition/Cnt;
		PhaseInfo->Mtr_WholeCCWOffsetPosition =0;
	}
}
static void Mtr_Cal_Current_Sensor_Compare(Mtr_Processing_Diag_HdrBusType  *pMtr_Cal_Current_Sensor_Compare,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Current_Sensor_Compare)
{
	Salsint32 fs32AdcIu				=pMtr_Cal_Current_Sensor_Compare->Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd;	/* U상 전류 */
	Salsint32 fs32AdcIv				=pMtr_Cal_Current_Sensor_Compare->Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd;	/* V상 전류 */
	Salsint32 Tempdivider				=0;
	Salsint32 fu32DiffPhaseCurrent	=0;
	
	if((fs32AdcIu>CURRENT_PHA_PLUS_MIN)&&(fs32AdcIv<CURRENT_PHA_MINUS_MIN))
	{
			fu32DiffPhaseCurrent =(Saluint32)abs(((DEF_PERCENT_GAIN*(fs32AdcIv)))/fs32AdcIu);
			if(fu32DiffPhaseCurrent>=CURRENT_BAND_LIMIT)/* 상대 전류가 45%보다 클 경우 */
			{
				pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_U_Phase_Info.Mtr_Current_CompleteFlg=ON;
			}
			else
			{
				pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_U_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			}
	}
	else if((fs32AdcIu<CURRENT_PHA_MINUS_MIN)&&(fs32AdcIv>CURRENT_PHA_PLUS_MIN))
	{
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_U_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_W_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			fu32DiffPhaseCurrent =(Saluint32)abs(((DEF_PERCENT_GAIN*(fs32AdcIu)))/fs32AdcIv);
			if(fu32DiffPhaseCurrent>=CURRENT_BAND_LIMIT)/* 상대 전류가 45%보다 클 경우 */
			{
				pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_V_Phase_Info.Mtr_Current_CompleteFlg=ON;
			}
			else
			{
				pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_V_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			}
	}
	else if((fs32AdcIu<CURRENT_PHA_MINUS_MIN)&&(fs32AdcIv<CURRENT_PHA_MINUS_MIN))
	{
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_U_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_V_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			if(fs32AdcIv>fs32AdcIu)
			{
				Tempdivider =fs32AdcIv;
			}
			else
			{
				Tempdivider =fs32AdcIu;
			}
			fu32DiffPhaseCurrent =(Saluint32)abs(DEF_PERCENT_GAIN*(fs32AdcIu-fs32AdcIv)/Tempdivider);
			if(fu32DiffPhaseCurrent<=CURRENT_BAND_MIN)/* 전류차가 5% 이하일 경우 */
			{
				pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_W_Phase_Info.Mtr_Current_CompleteFlg =ON;
			}
			else
			{
				pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_W_Phase_Info.Mtr_Current_CompleteFlg=OFF;
			}
	}
	else
	{
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_U_Phase_Info.Mtr_Current_CompleteFlg	=OFF;
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_V_Phase_Info.Mtr_Current_CompleteFlg	=OFF;
			pMtr_Internal_Cal_Current_Sensor_Compare->Mtr_Inter_W_Phase_Info.Mtr_Current_CompleteFlg	=OFF;
	}
}
static void Mtr_Cal_PhaPosition_Get(Mtr_Processing_Diag_HdrBusType *pMtr_Cal_PhaPosition_Get,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_PhaPosition_Get)
{
	static Saluint8   fu8StableCount 			=0;
	static Saluint32 fu32MotorPosition		=0; 
	static Saluint32 fu32DiffMPSPosition		=0;
	static Saluint32 fu32MotorPosition_old	=0;
	fu32MotorPosition_old    =fu32MotorPosition;
	fu32MotorPosition		 =pMtr_Cal_PhaPosition_Get->Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg;	/* Get Position */
	if(pMtr_Internal_Cal_PhaPosition_Get->Mtr_Inter_Cal_Info.Mtr_MtrDirectionState !=DEF_MTR_STOP)
	{
		fu32DiffMPSPosition 	=(Saluint32)abs(fu32MotorPosition-fu32MotorPosition_old);  		/* delta position/5ms */
		
		if(fu32DiffMPSPosition<5)
		{	
			if(fu8StableCount==2)
			{
				fu8StableCount	=0;
				pMtr_Internal_Cal_PhaPosition_Get->Mtr_MPS_Get_Complete_Flg 	=ON;
			}
			else
			{
				fu8StableCount++;
			}
		}
		else
		{
			pMtr_Internal_Cal_PhaPosition_Get->Mtr_MPS_Get_Complete_Flg 	=OFF;
			fu8StableCount	=0;
		}
	}
	else
	{
		pMtr_Internal_Cal_PhaPosition_Get->Mtr_MPS_Get_Complete_Flg 	=OFF;
		fu8StableCount	=0;
	}
}

static void Mtr_Cal_Pha_Offset_Calc(Mtr_Processing_Diag_HdrBusType *pCalPhaOffset,Mtr_Processing_InternalVal_Info *pMPSDataInfo,Mtr_Processing_DaigPhase_Type *Phase)
{

	if(pMPSDataInfo->Mtr_Inter_Cal_Info.Mtr_MtrDirectionState !=DEF_MTR_CW)
	{
		if((Phase->Mtr_Current_CompleteFlg==ON)&&(pMPSDataInfo->Mtr_MPS_Get_Complete_Flg==ON))/* 최대 전류값이고 각이 수렴할 경우 */
		{
			Phase ->Mtr_CWPositionAVRCnt++;
			Phase ->Mtr_CWPositionAddValue +=pCalPhaOffset->Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg; /* 2048*10 = 20480<2^15*/
			if(Phase ->Mtr_CWPositionAVRCnt ==30) 
			{
				Phase ->Mtr_CWPhaseOffset =(Saluint32)(Phase ->Mtr_CWPositionAddValue/Phase ->Mtr_CWPositionAVRCnt);
				Phase ->Mtr_CWPositionAddValue =0; 
				Phase ->Mtr_CWPositionAVRCnt	=0;
				Phase ->Mtr_CWPosiOffsetFlg 	=ON;
			}
		}
	}
	else if(pMPSDataInfo->Mtr_Inter_Cal_Info.Mtr_MtrDirectionState ==DEF_MTR_CCW)
	{
		if((Phase->Mtr_Current_CompleteFlg==ON)&&(pMPSDataInfo->Mtr_MPS_Get_Complete_Flg==ON))/* 최대 전류값이고 각이 수렴할 경우 */
		{
			Phase ->Mtr_CCWPositionAVRCnt++;
			Phase ->Mtr_CCWPositionAddValue +=pCalPhaOffset->Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
			if(Phase ->Mtr_CCWPositionAVRCnt ==30) 
			{
				Phase ->Mtr_CWPhaseOffset 		=(Saluint32)(Phase ->Mtr_CCWPositionAddValue/Phase ->Mtr_CCWPositionAVRCnt);
				Phase ->Mtr_CCWPositionAddValue 	=0;
				Phase ->Mtr_CCWPositionAVRCnt	=0;
				Phase ->Mtr_CCWPosiOffsetFlg 		=ON;
			}

		}
	}
	else
	{
		Phase ->Mtr_CWPositionAVRCnt	=0;
		Phase ->Mtr_CCWPositionAVRCnt	=0;
		Phase ->Mtr_CWPositionAddValue 	=0;
		Phase ->Mtr_CCWPositionAddValue	=0;
	}
}
static Saluint8 Mtr_Diag_Motor_Reqeust(Mtr_Processing_Diag_HdrBusType *pMtr_Diag_Mtr_Request,  Mtr_Processing_InternalVal_Info *pMtr_Internal_Diag_Mtr_Request)
{
	Saluint8 Mtr_request_Result = DEF_MTR_REQUEST_NONE;

	if((pMtr_Internal_Diag_Mtr_Request->Mtr_Inter_Common_Info.Mtr_Stauts==DEF_MTR_INIT)
		||(pMtr_Internal_Diag_Mtr_Request->Mtr_Inter_Common_Info.Mtr_Stauts==DEF_MTR_DIAG)
		)
	{
		if(pMtr_Internal_Diag_Mtr_Request->Mtr_Inter_Common_Info.Mtr_Stauts==DEF_MTR_INIT)
		{
                    Mtr_request_Result =DEF_MTR_REQUEST_PWM_INIT;
		}
               else if(
			(pMtr_Internal_Diag_Mtr_Request->Mtr_Inter_Cal_Info.Mtr_CalState !=DEF_MTR_CAL_STATE_IQ_CHECK)
			||(pMtr_Diag_Mtr_Request->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order ==DEF_PWM_HOLD)
		)
		{
			Mtr_request_Result =DEF_MTR_REQUEST_PWM;
		}
		else if((pMtr_Internal_Diag_Mtr_Request->Mtr_Inter_Cal_Info.Mtr_CalState ==DEF_MTR_CAL_STATE_IQ_CHECK)
			||(pMtr_Diag_Mtr_Request->Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order ==DEF_IQ_ID_HOLD)
			)
		{
			Mtr_request_Result =DEF_MTR_REQUEST_IQ;
		}
		else
		{
			;
		}
	}
	return Mtr_request_Result;
}
static void Mtr_Cal_PhaPosition_Calc(Mtr_Processing_Diag_HdrBusType *pMtr_Cal_PhaPosition_Calc,Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_PhaPosition_Calc )
{
	if(pMtr_Internal_Cal_PhaPosition_Calc->Mtr_Inter_Cal_Info.Mtr_CalPhaseExcitation  ==DEF_CAL_EXCITATION_U_PHASE)
	{
		Mtr_Cal_Pha_Offset_Calc(pMtr_Cal_PhaPosition_Calc,pMtr_Internal_Cal_PhaPosition_Calc,&(pMtr_Internal_Cal_PhaPosition_Calc->Mtr_Inter_U_Phase_Info));
	}
	else if(pMtr_Internal_Cal_PhaPosition_Calc->Mtr_Inter_Cal_Info.Mtr_CalPhaseExcitation==DEF_CAL_EXCITATION_V_PHASE)
	{
		Mtr_Cal_Pha_Offset_Calc(pMtr_Cal_PhaPosition_Calc,pMtr_Internal_Cal_PhaPosition_Calc,&(pMtr_Internal_Cal_PhaPosition_Calc->Mtr_Inter_V_Phase_Info));
	}
	else if(pMtr_Internal_Cal_PhaPosition_Calc->Mtr_Inter_Cal_Info.Mtr_CalPhaseExcitation ==DEF_CAL_EXCITATION_W_PHASE)
	{
		Mtr_Cal_Pha_Offset_Calc(pMtr_Cal_PhaPosition_Calc,pMtr_Internal_Cal_PhaPosition_Calc,&(pMtr_Internal_Cal_PhaPosition_Calc->Mtr_Inter_W_Phase_Info));
	}
	else
	{
		;
	}
}


static void Mtr_Cal_Offset_Potion_CW_Compare(Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Offset_Potion_CW_Compare)
{

	Saluint32 fu32DiffPhaseUtoV =0;
	Saluint32 fu32DiffPhaseVtoW =0;	
	if((pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_U_Phase_Info.Mtr_CWPosiOffsetFlg==ON)
	&&(pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CWPosiOffsetFlg==ON)
	&&(pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_W_Phase_Info.Mtr_CWPosiOffsetFlg==ON)
	)
	{
		fu32DiffPhaseUtoV =(Saluint16)abs(pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_U_Phase_Info.Mtr_CWPhaseOffset-pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CWPhaseOffset); 
		fu32DiffPhaseVtoW =(Saluint16)abs(pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CWPhaseOffset-pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_W_Phase_Info.Mtr_CWPhaseOffset);
	
		if(((fu32DiffPhaseUtoV>=MPS_ELEC_ANGLE_115DEG)&&(fu32DiffPhaseUtoV<=MPS_ELEC_ANGLE_125DEG))
		&&((fu32DiffPhaseVtoW>=MPS_ELEC_ANGLE_115DEG)&&(fu32DiffPhaseVtoW<=MPS_ELEC_ANGLE_125DEG))
		)
		{
			pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_Cal_Info.Mtr_CW_PositionState = DEF_OFFSETDETECT_COMPLETE;
		}
		else if((fu32DiffPhaseUtoV<MPS_ELEC_ANGLE_5DEG)||(fu32DiffPhaseVtoW<MPS_ELEC_ANGLE_5DEG))
		{
			pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_Cal_Info.Mtr_CW_PositionState = DEF_OFFSETDETECT_FAIL;
		}
		else
		{
			pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_Cal_Info.Mtr_CW_PositionState =DEF_OFFSETDETECT_RETEST;
		}
		pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_U_Phase_Info.Mtr_CWPosiOffsetFlg	=OFF;
		pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CWPosiOffsetFlg	=OFF;
		pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_W_Phase_Info.Mtr_CWPosiOffsetFlg	=OFF;
	}
	else
	{
		pMtr_Internal_Cal_Offset_Potion_CW_Compare->Mtr_Inter_Cal_Info.Mtr_CW_PositionState  = DEF_OFFSETDETECT_TEST;
	}
}
static void Mtr_Cal_Offset_Potion_CCW_Compare(Mtr_Processing_InternalVal_Info *pMtr_Internal_Cal_Offset_Potion_CCW_Compare)
{
	Saluint32 fu32DiffPhaseUtoV =0;
	Saluint32 fu32DiffPhaseVtoW =0;	
	if((pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_U_Phase_Info.Mtr_CCWPosiOffsetFlg==ON)
	&&(pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CWPosiOffsetFlg==ON)
	&&(pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_W_Phase_Info.Mtr_CWPosiOffsetFlg==ON)
	)
	{
		fu32DiffPhaseUtoV =(Saluint16)abs(pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_U_Phase_Info.Mtr_CCWPhaseOffset-pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CCWPhaseOffset); 
		fu32DiffPhaseVtoW =(Saluint16)abs(pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CCWPhaseOffset-pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_W_Phase_Info.Mtr_CCWPhaseOffset);
	
		if(((fu32DiffPhaseUtoV>=MPS_ELEC_ANGLE_115DEG)&&(fu32DiffPhaseUtoV<=MPS_ELEC_ANGLE_125DEG))
		&&((fu32DiffPhaseVtoW>=MPS_ELEC_ANGLE_115DEG)&&(fu32DiffPhaseVtoW<=MPS_ELEC_ANGLE_125DEG))
		)
		{
			pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState = DEF_OFFSETDETECT_COMPLETE;
		}
		else if((fu32DiffPhaseUtoV<MPS_ELEC_ANGLE_5DEG)||(fu32DiffPhaseVtoW<MPS_ELEC_ANGLE_5DEG))
		{
			pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_Cal_Info.Mtr_CCW_PositionState = DEF_OFFSETDETECT_FAIL;
		}
		else
		{
			pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_Cal_Info.Mtr_CW_PositionState =DEF_OFFSETDETECT_RETEST;
		}
		pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_U_Phase_Info.Mtr_CCWPosiOffsetFlg	=OFF;
		pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_V_Phase_Info.Mtr_CCWPosiOffsetFlg	=OFF;
		pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_W_Phase_Info.Mtr_CCWPosiOffsetFlg	=OFF;
	}
	else
	{
		pMtr_Internal_Cal_Offset_Potion_CCW_Compare->Mtr_Inter_Cal_Info.Mtr_CW_PositionState  = DEF_OFFSETDETECT_TEST;
	}
 
}
	
static void Mtr_Calibration_Current_Req(Mtr_Processing_Diag_HdrBusType *pMtr_Calibration_Current_Req ,Mtr_Processing_InternalVal_Info *pMtr_Internal_Calibration_Current_Req )
{
	Salsint32 Mtr_Measured_RPM =pMtr_Calibration_Current_Req->Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild; /* lamtrs16WrpmMech need feedback */
	Saluint32 Mtr_CW_RPM;
	Saluint32 Mtr_CCW_RPM;
	Saluint32 Mtr_CompareCWnCCW;
	Saluint8 Mtr_ReCal_Diff_Percentage;	
	static Saluint8 Mtr_CalibrationCurrentStatus	= DEF_MTR_CUR_REQ_INIT;
	static Saluint16 Mtr_Request_Cnt			=0;


	switch(Mtr_CalibrationCurrentStatus)
	{
		case DEF_MTR_CUR_REQ_INIT:
			if(Mtr_Request_Cnt > DEF_1_5_SEC_1ms)
			{
				Mtr_Request_Cnt =0;
				Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_CW; 
			}
			else
			{
				pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe =0;
				pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe =0;
				Mtr_Request_Cnt++;
			}
		break;
		
		case DEF_MTR_CUR_REQ_CW:
		if(Mtr_Request_Cnt > DEF_1_0_SEC_1ms)
		{
			Mtr_Request_Cnt =0;
			Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_WAIT;
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe =0;
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe =0;
		}
		else if(Mtr_Request_Cnt > DEF_0_8_SEC_1ms)
		{
			Fif_u32MovingAverageCalc(&(pMtr_Internal_Calibration_Current_Req->Mtr_Inter_CW_RPM_Data),10,Mtr_Measured_RPM);
		}
		else
		{
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe =0;
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe =1000;
		}
		Mtr_Request_Cnt++;
		break;

		case  DEF_MTR_CUR_REQ_WAIT:
		if(Mtr_Request_Cnt > DEF_1_5_SEC_1ms)
		{
			Mtr_Request_Cnt =0;
			Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_CCW;
		}
		else
		{
			Mtr_Request_Cnt++;
		}
		break;

		case DEF_MTR_CUR_REQ_CCW:
		if(Mtr_Request_Cnt > DEF_1_0_SEC_1ms)
		{
			Mtr_Request_Cnt =0;
			Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_CHECK;
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe =0;
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe =0;
		}
		else if(Mtr_Request_Cnt > DEF_0_8_SEC_1ms)
		{
			Fif_u32MovingAverageCalc(&(pMtr_Internal_Calibration_Current_Req->Mtr_Inter_CCW_RPM_Data),10,Mtr_Measured_RPM);
		}
		else
		{
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe =0;
			pMtr_Calibration_Current_Req->Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe =-1000;
			
		}
		Mtr_Request_Cnt++;
		break;

		case   DEF_MTR_CUR_REQ_CHECK:
			Mtr_CW_RPM 		=(Saluint16)abs(pMtr_Internal_Calibration_Current_Req->Mtr_Inter_CW_RPM_Data.AverValue);
			Mtr_CCW_RPM 	=(Saluint16)abs(pMtr_Internal_Calibration_Current_Req->Mtr_Inter_CCW_RPM_Data.AverValue);
			Mtr_CompareCWnCCW	=(Saluint16)abs(Mtr_CW_RPM-Mtr_CCW_RPM);
			if(Mtr_CCW_RPM>Mtr_CW_RPM)
			{
				Mtr_ReCal_Diff_Percentage = (Saluint8)((DEF_PERCENT_GAIN*Mtr_CompareCWnCCW)/Mtr_CCW_RPM);
			}
			else
			{
				Mtr_ReCal_Diff_Percentage = (Saluint8)((DEF_PERCENT_GAIN*Mtr_CompareCWnCCW)/Mtr_CW_RPM);
			}
			if((Mtr_ReCal_Diff_Percentage>DEF_RPM_LIMIT_PERCENT)
				||(Mtr_CW_RPM<=DEF_RPM_MINIMUM)
				||(Mtr_CCW_RPM<=DEF_RPM_MINIMUM) /* 500 RPM 이하일 경우 고장으로 간주 */
			)
			{
				Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_ERR;
			}
			else
			{
				Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_COMPLETE_WAIT; 
			}
		break;

		case DEF_MTR_CUR_REQ_COMPLETE_WAIT:
			if(Mtr_Request_Cnt > DEF_1_5_SEC_1ms)
			{
				Mtr_Request_Cnt =0;
				Mtr_CalibrationCurrentStatus =DEF_MTR_CUR_REQ_COMPLETE;
			}
			else
			{
				Mtr_Request_Cnt++;
			}
		break;
		
		case DEF_MTR_CUR_REQ_ERR:
		case DEF_MTR_CUR_REQ_COMPLETE:
			Mtr_CalibrationCurrentStatus=DEF_MTR_CUR_REQ_INIT;
		break;

		default:
		break;
	}
}


static void Fif_u32MovingAverageCalc(Mtr_Processing_DaigMtr_MovingAver_Type *MoveAver,Saluint8 DataNo,Salsint32 NewData)
{
	Saluint8 Count;
	Salsint32 TempSumBuffer;
	
	/* Old Data Throw */
    for(Count=1; Count<DataNo; Count++)
	{
		MoveAver->DataBuff[Count-1]=MoveAver->DataBuff[Count];		
	}

    if(MoveAver->SavedBufferNo<DataNo)
	{
		MoveAver->SavedBufferNo++;	
	}
	
	/*  New Data Input Set */ 
	MoveAver->DataBuff[DataNo-1]=NewData;	
		
	MoveAver->MovAverMaxValue=MoveAver->DataBuff[0];
	MoveAver->MovAverMinValue=MoveAver->DataBuff[0];
	TempSumBuffer=0;


	/* Check Min/Max Data */
	for(Count=0; Count<DataNo; Count++)
	{
		if(MoveAver->MovAverMaxValue<MoveAver->DataBuff[Count])
		{
			MoveAver->MovAverMaxValue=MoveAver->DataBuff[Count];
		}
		if(MoveAver->MovAverMinValue>MoveAver->DataBuff[Count])
		{
			MoveAver->MovAverMinValue=MoveAver->DataBuff[Count];
		}
		TempSumBuffer+=MoveAver->DataBuff[Count];
	}
	
	if(MoveAver->SavedBufferNo==DataNo)
	{
		MoveAver->AverValue=(Salsint32)( (TempSumBuffer-(Salsint32)MoveAver->MovAverMinValue-(Salsint32)MoveAver->MovAverMaxValue) / (DataNo-2) );	
	}
	else
	{
		MoveAver->AverValue=NewData;
	}
}


static Saluint8 Mtr_Vlv_Check(Mtr_Processing_Diag_HdrBusType *pMtr_Vlv_Check)
{
        Saluint8 Check_Result =0;
        if(
            (
               (pMtr_Vlv_Check->Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[5]==ARB_VLV_MAX_CURRENT)
               &&(pMtr_Vlv_Check->Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[5]==ARB_VLV_MAX_CURRENT))
            )
        {
            Check_Result =1;
        }
        return Check_Result;
}