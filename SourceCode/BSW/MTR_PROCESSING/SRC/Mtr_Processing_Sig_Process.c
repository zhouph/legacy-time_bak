/**
 * @defgroup Mtr_Processing_Sig Mtr_Processing_Sig
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Sig.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Sig.h"
#include "Mtr_Processing_Sig_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define DEF_ELEC_FULL_PULSE 2048
#define DEF_ELEC_GAIN            17578
#define DEF_ELEC_DIVIDE         1000
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_SIG_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"

static void Mtr_Processing_SenseCurrent(Mtr_Processing_Sig_HdrBusType *pProcessing_Sig_SensorCurrent);
static void Mtr_Processing_SenseAngle(Mtr_Processing_Sig_HdrBusType *pProcessing_Sig_MpsAngle);

void Mtr_Processing_Sig_Process(Mtr_Processing_Sig_HdrBusType *pProcessing_Sig_Process)
{
	Mtr_Processing_SenseCurrent(pProcessing_Sig_Process);
	Mtr_Processing_SenseAngle(pProcessing_Sig_Process);
}

static void Mtr_Processing_SenseCurrent(Mtr_Processing_Sig_HdrBusType *pProcessing_Sig_SensorCurrent)
 {
    Haluint16 avgU, avgV;
    Salsint32 currU, currV,currW;

    avgU = (pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0+pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1) / 2;
    avgV = (pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0+pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1) / 2;
	
    currU = (((Salsint32)avgU) - Mtr_Processing_CurSnsrConfig.Offset) * Mtr_Processing_CurSnsrConfig.MulFactor / Mtr_Processing_CurSnsrConfig.DivFactor;
    currW = (((Salsint32)avgV) - Mtr_Processing_CurSnsrConfig.Offset) * Mtr_Processing_CurSnsrConfig.MulFactor / Mtr_Processing_CurSnsrConfig.DivFactor;
    currV = -(currU + currW);

    pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = currU;
    pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = currV;
    pProcessing_Sig_SensorCurrent->Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd= currW;
 }

   /****************************************************************************
| NAME:             ACMIO_SenseAngle
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:     Motor Angle Sensing
****************************************************************************/

static void Mtr_Processing_SenseAngle(Mtr_Processing_Sig_HdrBusType *pProcessing_Sig_MpsAngle)
 {
    Saluint16 angle1=0; 
    Saluint16 angle2=0;
    Salsint32 ElecAngle1 =0;
    Salsint32 ElecAngle2 =0;

    angle1 = (Saluint16)pProcessing_Sig_MpsAngle->Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw;
    angle2 = (Saluint16)pProcessing_Sig_MpsAngle->Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw;
    
    ElecAngle1 = (Salsint32)((Salsint32)((angle1%DEF_ELEC_FULL_PULSE)*DEF_ELEC_GAIN)/DEF_ELEC_DIVIDE);
    ElecAngle2 = (Salsint32)((Salsint32)((angle2%DEF_ELEC_FULL_PULSE)*DEF_ELEC_GAIN)/DEF_ELEC_DIVIDE);

    pProcessing_Sig_MpsAngle->Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw =angle1;
    pProcessing_Sig_MpsAngle->Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw =angle2;
    pProcessing_Sig_MpsAngle->Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = ElecAngle1;
    pProcessing_Sig_MpsAngle->Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = ElecAngle2;
 }