/**
 * @defgroup Mtr_Processing_Diag Mtr_Processing_Diag
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Diag.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Diag.h"
#include "Mtr_Processing_Diag_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_DIAG_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mtr_Processing_Diag_HdrBusType Mtr_Processing_DiagBus;

/* Version Info */
const SwcVersionInfo_t Mtr_Processing_DiagVersionInfo = 
{   
    MTR_PROCESSING_DIAG_MODULE_ID,           /* Mtr_Processing_DiagVersionInfo.ModuleId */
    MTR_PROCESSING_DIAG_MAJOR_VERSION,       /* Mtr_Processing_DiagVersionInfo.MajorVer */
    MTR_PROCESSING_DIAG_MINOR_VERSION,       /* Mtr_Processing_DiagVersionInfo.MinorVer */
    MTR_PROCESSING_DIAG_PATCH_VERSION,       /* Mtr_Processing_DiagVersionInfo.PatchVer */
    MTR_PROCESSING_DIAG_BRANCH_VERSION       /* Mtr_Processing_DiagVersionInfo.BranchVer */
};
    
/* Input Data Element */
Arbitrator_VlvArbWhlVlvReqInfo_t Mtr_Processing_DiagArbWhlVlvReqInfo;
Eem_MainEemFailData_t Mtr_Processing_DiagEemFailData;
Eem_MainEemCtrlInhibitData_t Mtr_Processing_DiagEemCtrlInhibitData;
Ioc_InputSR1msPwrMonInfoEsc_t Mtr_Processing_DiagPwrMonInfoEsc;
Msp_CtrlMotRotgAgSigInfo_t Mtr_Processing_DiagMotRotgAgSigInfo;
Wss_SenWhlSpdInfo_t Mtr_Processing_DiagWhlSpdInfo;
Diag_HndlrDiagHndlRequest_t Mtr_Processing_DiagDiagHndlRequest;
Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_DiagMtrProcessOutInfo;

/* Output Data Element */
Mtr_Processing_DiagMtrProcessOutData_t Mtr_Processing_DiagMtrProcessOutData;
Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_DiagMtrProcessDataInf;
Mtr_Processing_DiagMtrDiagState_t Mtr_Processing_DiagMtrDiagState;

uint32 Mtr_Processing_Diag_Timer_Start;
uint32 Mtr_Processing_Diag_Timer_Elapsed;

#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_DIAG_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_DIAG_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_DIAG_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_DIAG_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_DIAG_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_DIAG_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_DIAG_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTR_PROCESSING_DIAG_START_SEC_CODE
#include "Mtr_Processing_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mtr_Processing_Diag_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[i] = 0;   
    Mtr_Processing_DiagBus.Mtr_Processing_DiagEemFailData.Eem_Fail_Motor = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt02Mon_Esc = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult = 0;
    Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrDiagState = 0;
}

void Mtr_Processing_Diag(void)
{
    uint16 i;
    
    Mtr_Processing_Diag_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagArbWhlVlvReqInfo(&Mtr_Processing_DiagBus.Mtr_Processing_DiagArbWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagArbWhlVlvReqInfo 
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagEemFailData_Eem_Fail_Motor(&Mtr_Processing_DiagBus.Mtr_Processing_DiagEemFailData.Eem_Fail_Motor);

    /* Decomposed structure interface */
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(&Mtr_Processing_DiagBus.Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail);

    /* Decomposed structure interface */
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt01Mon_Esc(&Mtr_Processing_DiagBus.Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc);
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt02Mon_Esc(&Mtr_Processing_DiagBus.Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt02Mon_Esc);

    /* Decomposed structure interface */
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagMotRotgAgSigInfo_MotMechAngleSpdFild(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild);

    Mtr_Processing_Diag_Read_Mtr_Processing_DiagWhlSpdInfo(&Mtr_Processing_DiagBus.Mtr_Processing_DiagWhlSpdInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagWhlSpdInfo 
     : Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagDiagHndlRequest(&Mtr_Processing_DiagBus.Mtr_Processing_DiagDiagHndlRequest);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagDiagHndlRequest 
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhUMeasd(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd);
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhVMeasd(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd);
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhWMeasd(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd);
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle1_1_100Deg(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg);
    Mtr_Processing_Diag_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle2_1_100Deg(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg);


    /* Process */
    Mtr_Processing_Diag_Process(&Mtr_Processing_DiagBus);

    /* Output */
    Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessOutData(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessOutData);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagMtrProcessOutData 
     : Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe;
     : Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe;
     : Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM;
     : Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM;
     : Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM;
     =============================================================================*/
    
    Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrProcessDataInf(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrProcessDataInf);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagMtrProcessDataInf 
     : Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState;
     : Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult;
     =============================================================================*/
    
    Mtr_Processing_Diag_Write_Mtr_Processing_DiagMtrDiagState(&Mtr_Processing_DiagBus.Mtr_Processing_DiagMtrDiagState);

    Mtr_Processing_Diag_Timer_Elapsed = STM0_TIM0.U - Mtr_Processing_Diag_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTR_PROCESSING_DIAG_STOP_SEC_CODE
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
