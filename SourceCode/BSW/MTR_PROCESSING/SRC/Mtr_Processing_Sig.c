/**
 * @defgroup Mtr_Processing_Sig Mtr_Processing_Sig
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Sig.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Sig.h"
#include "Mtr_Processing_Sig_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_SIG_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mtr_Processing_Sig_HdrBusType Mtr_Processing_SigBus;

/* Version Info */
const SwcVersionInfo_t Mtr_Processing_SigVersionInfo = 
{   
    MTR_PROCESSING_SIG_MODULE_ID,           /* Mtr_Processing_SigVersionInfo.ModuleId */
    MTR_PROCESSING_SIG_MAJOR_VERSION,       /* Mtr_Processing_SigVersionInfo.MajorVer */
    MTR_PROCESSING_SIG_MINOR_VERSION,       /* Mtr_Processing_SigVersionInfo.MinorVer */
    MTR_PROCESSING_SIG_PATCH_VERSION,       /* Mtr_Processing_SigVersionInfo.PatchVer */
    MTR_PROCESSING_SIG_BRANCH_VERSION       /* Mtr_Processing_SigVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t Mtr_Processing_SigEemFailData;
AdcIf_Conv1msSwTrigMotInfo_t Mtr_Processing_SigSwTrigMotInfo;
Wss_SenWhlSpdInfo_t Mtr_Processing_SigWhlSpdInfo;
AdcIf_Conv50usHwTrigMotInfo_t Mtr_Processing_SigHwTrigMotInfo;
Ioc_InputSR50usMotCurrMonInfo_t Mtr_Processing_SigMotCurrMonInfo;
Ioc_InputSR50usMotAngleMonInfo_t Mtr_Processing_SigMotAngleMonInfo;
Eem_MainEemEceData_t Mtr_Processing_SigEemEceData;
Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_SigMtrProcessDataInf;
Mom_HndlrEcuModeSts_t Mtr_Processing_SigEcuModeSts;
Prly_HndlrIgnOnOffSts_t Mtr_Processing_SigIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Mtr_Processing_SigIgnEdgeSts;

/* Output Data Element */
Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_SigMtrProcessOutInfo;

uint32 Mtr_Processing_Sig_Timer_Start;
uint32 Mtr_Processing_Sig_Timer_Elapsed;

#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_SIG_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_SIG_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTR_PROCESSING_SIG_START_SEC_CODE
#include "Mtr_Processing_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mtr_Processing_Sig_Init(void)
{
    /* Initialize internal bus */
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_RearSol = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_Motor = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_MPS = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_MGD = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigSwTrigMotInfo.MotPwrMon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigSwTrigMotInfo.StarMon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigSwTrigMotInfo.UoutMon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigSwTrigMotInfo.VoutMon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigSwTrigMotInfo.WoutMon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigWhlSpdInfo.FlWhlSpd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigWhlSpdInfo.FrWhlSpd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigWhlSpdInfo.RlWhlSpd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigHwTrigMotInfo.Uphase0Mon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigHwTrigMotInfo.Uphase1Mon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigHwTrigMotInfo.Vphase0Mon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigHwTrigMotInfo.VPhase1Mon = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0 = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1 = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0 = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1 = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1deg = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2deg = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEemEceData.Eem_Ece_Motor = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigEcuModeSts = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigIgnOnOffSts = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigIgnEdgeSts = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = 0;
    Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = 0;
}

void Mtr_Processing_Sig(void)
{
    Mtr_Processing_Sig_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_BBSSol(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_ESCSol(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_FrontSol(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_RearSol(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_RearSol);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_Motor(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_Motor);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MPS(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_MPS);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MGD(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_MGD);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_OverVolt(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_UnderVolt(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowVolt(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowerVolt(&Mtr_Processing_SigBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt);

    Mtr_Processing_Sig_Read_Mtr_Processing_SigSwTrigMotInfo(&Mtr_Processing_SigBus.Mtr_Processing_SigSwTrigMotInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigSwTrigMotInfo 
     : Mtr_Processing_SigSwTrigMotInfo.MotPwrMon;
     : Mtr_Processing_SigSwTrigMotInfo.StarMon;
     : Mtr_Processing_SigSwTrigMotInfo.UoutMon;
     : Mtr_Processing_SigSwTrigMotInfo.VoutMon;
     : Mtr_Processing_SigSwTrigMotInfo.WoutMon;
     =============================================================================*/
    
    Mtr_Processing_Sig_Read_Mtr_Processing_SigWhlSpdInfo(&Mtr_Processing_SigBus.Mtr_Processing_SigWhlSpdInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigWhlSpdInfo 
     : Mtr_Processing_SigWhlSpdInfo.FlWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.FrWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.RlWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Mtr_Processing_Sig_Read_Mtr_Processing_SigHwTrigMotInfo(&Mtr_Processing_SigBus.Mtr_Processing_SigHwTrigMotInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigHwTrigMotInfo 
     : Mtr_Processing_SigHwTrigMotInfo.Uphase0Mon;
     : Mtr_Processing_SigHwTrigMotInfo.Uphase1Mon;
     : Mtr_Processing_SigHwTrigMotInfo.Vphase0Mon;
     : Mtr_Processing_SigHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/
    
    Mtr_Processing_Sig_Read_Mtr_Processing_SigMotCurrMonInfo(&Mtr_Processing_SigBus.Mtr_Processing_SigMotCurrMonInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMotCurrMonInfo 
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0;
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1;
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0;
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/
    
    Mtr_Processing_Sig_Read_Mtr_Processing_SigMotAngleMonInfo(&Mtr_Processing_SigBus.Mtr_Processing_SigMotAngleMonInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMotAngleMonInfo 
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1deg;
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2deg;
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw;
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEemEceData_Eem_Ece_Motor(&Mtr_Processing_SigBus.Mtr_Processing_SigEemEceData.Eem_Ece_Motor);

    Mtr_Processing_Sig_Read_Mtr_Processing_SigMtrProcessDataInf(&Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessDataInf);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMtrProcessDataInf 
     : Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState;
     : Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult;
     =============================================================================*/
    
    Mtr_Processing_Sig_Read_Mtr_Processing_SigEcuModeSts(&Mtr_Processing_SigBus.Mtr_Processing_SigEcuModeSts);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigIgnOnOffSts(&Mtr_Processing_SigBus.Mtr_Processing_SigIgnOnOffSts);
    Mtr_Processing_Sig_Read_Mtr_Processing_SigIgnEdgeSts(&Mtr_Processing_SigBus.Mtr_Processing_SigIgnEdgeSts);

    /* Process */
    Mtr_Processing_Sig_Process(&Mtr_Processing_SigBus);
    /* Output */
    Mtr_Processing_Sig_Write_Mtr_Processing_SigMtrProcessOutInfo(&Mtr_Processing_SigBus.Mtr_Processing_SigMtrProcessOutInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMtrProcessOutInfo 
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg;
     =============================================================================*/
    

    Mtr_Processing_Sig_Timer_Elapsed = STM0_TIM0.U - Mtr_Processing_Sig_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTR_PROCESSING_SIG_STOP_SEC_CODE
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
