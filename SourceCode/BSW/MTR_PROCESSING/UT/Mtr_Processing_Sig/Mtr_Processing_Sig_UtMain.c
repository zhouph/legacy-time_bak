#include "unity.h"
#include "unity_fixture.h"
#include "Mtr_Processing_Sig.h"
#include "Mtr_Processing_Sig_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_BBSSol[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_BBSSOL;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_ESCSol[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_ESCSOL;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_FrontSol[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_FRONTSOL;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_RearSol[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_REARSOL;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_Motor[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_MOTOR;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_MPS[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_MPS;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_MGD[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_MGD;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_OverVolt[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_OVERVOLT;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_UnderVolt[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_UNDERVOLT;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_LowVolt[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_LOWVOLT;
const Saluint8 UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_LowerVolt[MAX_STEP] = MTR_PROCESSING_SIGEEMFAILDATA_EEM_FAIL_LOWERVOLT;
const Haluint16 UtInput_Mtr_Processing_SigSwTrigMotInfo_MotPwrMon[MAX_STEP] = MTR_PROCESSING_SIGSWTRIGMOTINFO_MOTPWRMON;
const Haluint16 UtInput_Mtr_Processing_SigSwTrigMotInfo_StarMon[MAX_STEP] = MTR_PROCESSING_SIGSWTRIGMOTINFO_STARMON;
const Haluint16 UtInput_Mtr_Processing_SigSwTrigMotInfo_UoutMon[MAX_STEP] = MTR_PROCESSING_SIGSWTRIGMOTINFO_UOUTMON;
const Haluint16 UtInput_Mtr_Processing_SigSwTrigMotInfo_VoutMon[MAX_STEP] = MTR_PROCESSING_SIGSWTRIGMOTINFO_VOUTMON;
const Haluint16 UtInput_Mtr_Processing_SigSwTrigMotInfo_WoutMon[MAX_STEP] = MTR_PROCESSING_SIGSWTRIGMOTINFO_WOUTMON;
const Saluint16 UtInput_Mtr_Processing_SigWhlSpdInfo_FlWhlSpd[MAX_STEP] = MTR_PROCESSING_SIGWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_Mtr_Processing_SigWhlSpdInfo_FrWhlSpd[MAX_STEP] = MTR_PROCESSING_SIGWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_Mtr_Processing_SigWhlSpdInfo_RlWhlSpd[MAX_STEP] = MTR_PROCESSING_SIGWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_Mtr_Processing_SigWhlSpdInfo_RrlWhlSpd[MAX_STEP] = MTR_PROCESSING_SIGWHLSPDINFO_RRLWHLSPD;
const Haluint16 UtInput_Mtr_Processing_SigHwTrigMotInfo_Uphase0Mon[MAX_STEP] = MTR_PROCESSING_SIGHWTRIGMOTINFO_UPHASE0MON;
const Haluint16 UtInput_Mtr_Processing_SigHwTrigMotInfo_Uphase1Mon[MAX_STEP] = MTR_PROCESSING_SIGHWTRIGMOTINFO_UPHASE1MON;
const Haluint16 UtInput_Mtr_Processing_SigHwTrigMotInfo_Vphase0Mon[MAX_STEP] = MTR_PROCESSING_SIGHWTRIGMOTINFO_VPHASE0MON;
const Haluint16 UtInput_Mtr_Processing_SigHwTrigMotInfo_VPhase1Mon[MAX_STEP] = MTR_PROCESSING_SIGHWTRIGMOTINFO_VPHASE1MON;
const Haluint16 UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhUMon0[MAX_STEP] = MTR_PROCESSING_SIGMOTCURRMONINFO_MOTCURRPHUMON0;
const Haluint16 UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhUMon1[MAX_STEP] = MTR_PROCESSING_SIGMOTCURRMONINFO_MOTCURRPHUMON1;
const Haluint16 UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhVMon0[MAX_STEP] = MTR_PROCESSING_SIGMOTCURRMONINFO_MOTCURRPHVMON0;
const Haluint16 UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhVMon1[MAX_STEP] = MTR_PROCESSING_SIGMOTCURRMONINFO_MOTCURRPHVMON1;
const Haluint16 UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle1deg[MAX_STEP] = MTR_PROCESSING_SIGMOTANGLEMONINFO_MOTPOSIANGLE1DEG;
const Haluint16 UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle2deg[MAX_STEP] = MTR_PROCESSING_SIGMOTANGLEMONINFO_MOTPOSIANGLE2DEG;
const Haluint16 UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle1raw[MAX_STEP] = MTR_PROCESSING_SIGMOTANGLEMONINFO_MOTPOSIANGLE1RAW;
const Haluint16 UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle2raw[MAX_STEP] = MTR_PROCESSING_SIGMOTANGLEMONINFO_MOTPOSIANGLE2RAW;
const Saluint8 UtInput_Mtr_Processing_SigEemEceData_Eem_Ece_Motor[MAX_STEP] = MTR_PROCESSING_SIGEEMECEDATA_EEM_ECE_MOTOR;
const Saluint8 UtInput_Mtr_Processing_SigMtrProcessDataInf_MtrProCalibrationState[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSDATAINF_MTRPROCALIBRATIONSTATE;
const Saluint32 UtInput_Mtr_Processing_SigMtrProcessDataInf_MtrCaliResult[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSDATAINF_MTRCALIRESULT;
const Mom_HndlrEcuModeSts_t UtInput_Mtr_Processing_SigEcuModeSts[MAX_STEP] = MTR_PROCESSING_SIGECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Mtr_Processing_SigIgnOnOffSts[MAX_STEP] = MTR_PROCESSING_SIGIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_Mtr_Processing_SigIgnEdgeSts[MAX_STEP] = MTR_PROCESSING_SIGIGNEDGESTS;

const Salsint32 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTCURRPHUMEASD;
const Salsint32 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTCURRPHVMEASD;
const Salsint32 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTCURRPHWMEASD;
const Saluint16 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTPOSIANGLE1RAW;
const Saluint16 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTPOSIANGLE2RAW;
const Salsint32 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTPOSIANGLE1_1_100DEG;
const Salsint32 UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg[MAX_STEP] = MTR_PROCESSING_SIGMTRPROCESSOUTINFO_MOTPOSIANGLE2_1_100DEG;



TEST_GROUP(Mtr_Processing_Sig);
TEST_SETUP(Mtr_Processing_Sig)
{
    Mtr_Processing_Sig_Init();
}

TEST_TEAR_DOWN(Mtr_Processing_Sig)
{   /* Postcondition */

}

TEST(Mtr_Processing_Sig, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_BBSSol[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_ESCSol[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_FrontSol[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_RearSol = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_RearSol[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_Motor = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_Motor[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_MPS = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_MPS[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_MGD = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_MGD[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_OverVolt[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_UnderVolt[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_LowVolt[i];
        Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt = UtInput_Mtr_Processing_SigEemFailData_Eem_Fail_LowerVolt[i];
        Mtr_Processing_SigSwTrigMotInfo.MotPwrMon = UtInput_Mtr_Processing_SigSwTrigMotInfo_MotPwrMon[i];
        Mtr_Processing_SigSwTrigMotInfo.StarMon = UtInput_Mtr_Processing_SigSwTrigMotInfo_StarMon[i];
        Mtr_Processing_SigSwTrigMotInfo.UoutMon = UtInput_Mtr_Processing_SigSwTrigMotInfo_UoutMon[i];
        Mtr_Processing_SigSwTrigMotInfo.VoutMon = UtInput_Mtr_Processing_SigSwTrigMotInfo_VoutMon[i];
        Mtr_Processing_SigSwTrigMotInfo.WoutMon = UtInput_Mtr_Processing_SigSwTrigMotInfo_WoutMon[i];
        Mtr_Processing_SigWhlSpdInfo.FlWhlSpd = UtInput_Mtr_Processing_SigWhlSpdInfo_FlWhlSpd[i];
        Mtr_Processing_SigWhlSpdInfo.FrWhlSpd = UtInput_Mtr_Processing_SigWhlSpdInfo_FrWhlSpd[i];
        Mtr_Processing_SigWhlSpdInfo.RlWhlSpd = UtInput_Mtr_Processing_SigWhlSpdInfo_RlWhlSpd[i];
        Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd = UtInput_Mtr_Processing_SigWhlSpdInfo_RrlWhlSpd[i];
        Mtr_Processing_SigHwTrigMotInfo.Uphase0Mon = UtInput_Mtr_Processing_SigHwTrigMotInfo_Uphase0Mon[i];
        Mtr_Processing_SigHwTrigMotInfo.Uphase1Mon = UtInput_Mtr_Processing_SigHwTrigMotInfo_Uphase1Mon[i];
        Mtr_Processing_SigHwTrigMotInfo.Vphase0Mon = UtInput_Mtr_Processing_SigHwTrigMotInfo_Vphase0Mon[i];
        Mtr_Processing_SigHwTrigMotInfo.VPhase1Mon = UtInput_Mtr_Processing_SigHwTrigMotInfo_VPhase1Mon[i];
        Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0 = UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhUMon0[i];
        Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1 = UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhUMon1[i];
        Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0 = UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhVMon0[i];
        Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1 = UtInput_Mtr_Processing_SigMotCurrMonInfo_MotCurrPhVMon1[i];
        Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1deg = UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle1deg[i];
        Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2deg = UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle2deg[i];
        Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw = UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle1raw[i];
        Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw = UtInput_Mtr_Processing_SigMotAngleMonInfo_MotPosiAngle2raw[i];
        Mtr_Processing_SigEemEceData.Eem_Ece_Motor = UtInput_Mtr_Processing_SigEemEceData_Eem_Ece_Motor[i];
        Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState = UtInput_Mtr_Processing_SigMtrProcessDataInf_MtrProCalibrationState[i];
        Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult = UtInput_Mtr_Processing_SigMtrProcessDataInf_MtrCaliResult[i];
        Mtr_Processing_SigEcuModeSts = UtInput_Mtr_Processing_SigEcuModeSts[i];
        Mtr_Processing_SigIgnOnOffSts = UtInput_Mtr_Processing_SigIgnOnOffSts[i];
        Mtr_Processing_SigIgnEdgeSts = UtInput_Mtr_Processing_SigIgnEdgeSts[i];

        Mtr_Processing_Sig();

        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg, UtExpected_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg[i]);
    }
}

TEST_GROUP_RUNNER(Mtr_Processing_Sig)
{
    RUN_TEST_CASE(Mtr_Processing_Sig, All);
}
