#define UT_MAX_STEP 5

#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_0   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_1   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_2   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_3   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_4   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGEEMFAILDATA_EEM_FAIL_MOTOR {0,0,0,0,0}
#define MTR_PROCESSING_DIAGEEMCTRLINHIBITDATA_EEM_BBS_DEFECTIVEMODEFAIL {0,0,0,0,0}
#define MTR_PROCESSING_DIAGPWRMONINFOESC_VOLTVBATT01MON_ESC {0,0,0,0,0}
#define MTR_PROCESSING_DIAGPWRMONINFOESC_VOLTVBATT02MON_ESC {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMOTROTGAGSIGINFO_MOTMECHANGLESPDFILD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGWHLSPDINFO_FLWHLSPD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGWHLSPDINFO_FRWHLSPD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGWHLSPDINFO_RLWHLSPD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGWHLSPDINFO_RRLWHLSPD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_ORDER {0,0,0,0,0}
#define MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_IQ {0,0,0,0,0}
#define MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_ID {0,0,0,0,0}
#define MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_U_PWM {0,0,0,0,0}
#define MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_V_PWM {0,0,0,0,0}
#define MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_W_PWM {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTCURRPHUMEASD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTCURRPHVMEASD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTCURRPHWMEASD {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTPOSIANGLE1_1_100DEG {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTPOSIANGLE2_1_100DEG {0,0,0,0,0}

#define MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROIQFAILSAFE   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROIDFAILSAFE   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROUPHASEPWM   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROVPHASEPWM   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROWPHASEPWM   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSDATAINF_MTRPROCALIBRATIONSTATE   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRPROCESSDATAINF_MTRCALIRESULT   {0,0,0,0,0}
#define MTR_PROCESSING_DIAGMTRDIAGSTATE     {0,0,0,0,0}
