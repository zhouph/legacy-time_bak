#include "unity.h"
#include "unity_fixture.h"
#include "Mtr_Processing_Diag.h"
#include "Mtr_Processing_Diag_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLOVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FLIVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FROVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_FRIVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLOVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RLIVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RROVREQDATA_4;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_0[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_0;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_1[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_1;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_2[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_2;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_3[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_3;
const Saluint16 UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_4[MAX_STEP] = MTR_PROCESSING_DIAGARBWHLVLVREQINFO_RRIVREQDATA_4;
const Saluint8 UtInput_Mtr_Processing_DiagEemFailData_Eem_Fail_Motor[MAX_STEP] = MTR_PROCESSING_DIAGEEMFAILDATA_EEM_FAIL_MOTOR;
const Saluint8 UtInput_Mtr_Processing_DiagEemCtrlInhibitData_Eem_BBS_DefectiveModeFail[MAX_STEP] = MTR_PROCESSING_DIAGEEMCTRLINHIBITDATA_EEM_BBS_DEFECTIVEMODEFAIL;
const Haluint16 UtInput_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt01Mon_Esc[MAX_STEP] = MTR_PROCESSING_DIAGPWRMONINFOESC_VOLTVBATT01MON_ESC;
const Haluint16 UtInput_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt02Mon_Esc[MAX_STEP] = MTR_PROCESSING_DIAGPWRMONINFOESC_VOLTVBATT02MON_ESC;
const Salsint32 UtInput_Mtr_Processing_DiagMotRotgAgSigInfo_MotMechAngleSpdFild[MAX_STEP] = MTR_PROCESSING_DIAGMOTROTGAGSIGINFO_MOTMECHANGLESPDFILD;
const Saluint16 UtInput_Mtr_Processing_DiagWhlSpdInfo_FlWhlSpd[MAX_STEP] = MTR_PROCESSING_DIAGWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_Mtr_Processing_DiagWhlSpdInfo_FrWhlSpd[MAX_STEP] = MTR_PROCESSING_DIAGWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_Mtr_Processing_DiagWhlSpdInfo_RlWhlSpd[MAX_STEP] = MTR_PROCESSING_DIAGWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_Mtr_Processing_DiagWhlSpdInfo_RrlWhlSpd[MAX_STEP] = MTR_PROCESSING_DIAGWHLSPDINFO_RRLWHLSPD;
const Saluint8 UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Order[MAX_STEP] = MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_ORDER;
const Saluint16 UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Iq[MAX_STEP] = MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_IQ;
const Saluint16 UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Id[MAX_STEP] = MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_ID;
const Saluint16 UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_U_PWM[MAX_STEP] = MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_U_PWM;
const Saluint16 UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_V_PWM[MAX_STEP] = MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_V_PWM;
const Saluint16 UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_W_PWM[MAX_STEP] = MTR_PROCESSING_DIAGDIAGHNDLREQUEST_DIAGREQUEST_W_PWM;
const Salsint32 UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhUMeasd[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTCURRPHUMEASD;
const Salsint32 UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhVMeasd[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTCURRPHVMEASD;
const Salsint32 UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhWMeasd[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTCURRPHWMEASD;
const Salsint32 UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle1_1_100Deg[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTPOSIANGLE1_1_100DEG;
const Salsint32 UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle2_1_100Deg[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTINFO_MOTPOSIANGLE2_1_100DEG;

const Salsint32 UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProIqFailsafe[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROIQFAILSAFE;
const Salsint32 UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProIdFailsafe[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROIDFAILSAFE;
const Saluint32 UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProUPhasePWM[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROUPHASEPWM;
const Saluint32 UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProVPhasePWM[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROVPHASEPWM;
const Saluint32 UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProWPhasePWM[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSOUTDATA_MTRPROWPHASEPWM;
const Saluint8 UtExpected_Mtr_Processing_DiagMtrProcessDataInf_MtrProCalibrationState[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSDATAINF_MTRPROCALIBRATIONSTATE;
const Saluint32 UtExpected_Mtr_Processing_DiagMtrProcessDataInf_MtrCaliResult[MAX_STEP] = MTR_PROCESSING_DIAGMTRPROCESSDATAINF_MTRCALIRESULT;
const Mtr_Processing_DiagMtrDiagState_t UtExpected_Mtr_Processing_DiagMtrDiagState[MAX_STEP] = MTR_PROCESSING_DIAGMTRDIAGSTATE;



TEST_GROUP(Mtr_Processing_Diag);
TEST_SETUP(Mtr_Processing_Diag)
{
    Mtr_Processing_Diag_Init();
}

TEST_TEAR_DOWN(Mtr_Processing_Diag)
{   /* Postcondition */

}

TEST(Mtr_Processing_Diag, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlOvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FlIvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrOvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_FrIvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlOvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RlIvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrOvReqData_4[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[0] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_0[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[1] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_1[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[2] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_2[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[3] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_3[i];
        Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[4] = UtInput_Mtr_Processing_DiagArbWhlVlvReqInfo_RrIvReqData_4[i];
        Mtr_Processing_DiagEemFailData.Eem_Fail_Motor = UtInput_Mtr_Processing_DiagEemFailData_Eem_Fail_Motor[i];
        Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = UtInput_Mtr_Processing_DiagEemCtrlInhibitData_Eem_BBS_DefectiveModeFail[i];
        Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc = UtInput_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt01Mon_Esc[i];
        Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt02Mon_Esc = UtInput_Mtr_Processing_DiagPwrMonInfoEsc_VoltVBatt02Mon_Esc[i];
        Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild = UtInput_Mtr_Processing_DiagMotRotgAgSigInfo_MotMechAngleSpdFild[i];
        Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd = UtInput_Mtr_Processing_DiagWhlSpdInfo_FlWhlSpd[i];
        Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd = UtInput_Mtr_Processing_DiagWhlSpdInfo_FrWhlSpd[i];
        Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd = UtInput_Mtr_Processing_DiagWhlSpdInfo_RlWhlSpd[i];
        Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd = UtInput_Mtr_Processing_DiagWhlSpdInfo_RrlWhlSpd[i];
        Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order = UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Order[i];
        Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq = UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Iq[i];
        Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id = UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_Id[i];
        Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM = UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_U_PWM[i];
        Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM = UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_V_PWM[i];
        Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM = UtInput_Mtr_Processing_DiagDiagHndlRequest_DiagRequest_W_PWM[i];
        Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd = UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhUMeasd[i];
        Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd = UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhVMeasd[i];
        Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd = UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhWMeasd[i];
        Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg = UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle1_1_100Deg[i];
        Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg = UtInput_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle2_1_100Deg[i];

        Mtr_Processing_Diag();

        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe, UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProIqFailsafe[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe, UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProIdFailsafe[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM, UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProUPhasePWM[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM, UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProVPhasePWM[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM, UtExpected_Mtr_Processing_DiagMtrProcessOutData_MtrProWPhasePWM[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState, UtExpected_Mtr_Processing_DiagMtrProcessDataInf_MtrProCalibrationState[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult, UtExpected_Mtr_Processing_DiagMtrProcessDataInf_MtrCaliResult[i]);
        TEST_ASSERT_EQUAL(Mtr_Processing_DiagMtrDiagState, UtExpected_Mtr_Processing_DiagMtrDiagState[i]);
    }
}

TEST_GROUP_RUNNER(Mtr_Processing_Diag)
{
    RUN_TEST_CASE(Mtr_Processing_Diag, All);
}
