/**
 * @defgroup Mtr_Processing_Cal Mtr_Processing_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mtr_Processing_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mtr_Processing_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_CALIB_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/* Global Calibration Section */


#define MTR_PROCESSING_STOP_SEC_CALIB_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"

#define MTR_PROCESSING_START_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTR_PROCESSING_STOP_SEC_CONST_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_NOINIT_32BIT
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTR_PROCESSING_STOP_SEC_VAR_UNSPECIFIED
#include "Mtr_Processing_MemMap.h"
#define MTR_PROCESSING_START_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/** Variable Section (32BIT)**/


#define MTR_PROCESSING_STOP_SEC_VAR_32BIT
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTR_PROCESSING_START_SEC_CODE
#include "Mtr_Processing_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTR_PROCESSING_STOP_SEC_CODE
#include "Mtr_Processing_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
