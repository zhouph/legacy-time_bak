/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Mtr_Processing_Sig.h"
#include "Mtr_Processing_Diag.h"

int main(void)
{
    Mtr_Processing_Sig_Init();
    Mtr_Processing_Diag_Init();

    while(1)
    {
        Mtr_Processing_Sig();
        Mtr_Processing_Diag();
    }
}