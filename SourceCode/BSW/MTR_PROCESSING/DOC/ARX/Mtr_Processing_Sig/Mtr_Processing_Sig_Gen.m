Mtr_Processing_SigEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    };
Mtr_Processing_SigEemFailData = CreateBus(Mtr_Processing_SigEemFailData, DeList);
clear DeList;

Mtr_Processing_SigSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
Mtr_Processing_SigSwTrigMotInfo = CreateBus(Mtr_Processing_SigSwTrigMotInfo, DeList);
clear DeList;

Mtr_Processing_SigWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Mtr_Processing_SigWhlSpdInfo = CreateBus(Mtr_Processing_SigWhlSpdInfo, DeList);
clear DeList;

Mtr_Processing_SigHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
Mtr_Processing_SigHwTrigMotInfo = CreateBus(Mtr_Processing_SigHwTrigMotInfo, DeList);
clear DeList;

Mtr_Processing_SigMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Mtr_Processing_SigMotCurrMonInfo = CreateBus(Mtr_Processing_SigMotCurrMonInfo, DeList);
clear DeList;

Mtr_Processing_SigMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Mtr_Processing_SigMotAngleMonInfo = CreateBus(Mtr_Processing_SigMotAngleMonInfo, DeList);
clear DeList;

Mtr_Processing_SigEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Motor'
    };
Mtr_Processing_SigEemEceData = CreateBus(Mtr_Processing_SigEemEceData, DeList);
clear DeList;

Mtr_Processing_SigMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    'MtrCaliResult'
    };
Mtr_Processing_SigMtrProcessDataInf = CreateBus(Mtr_Processing_SigMtrProcessDataInf, DeList);
clear DeList;

Mtr_Processing_SigEcuModeSts = Simulink.Bus;
DeList={'Mtr_Processing_SigEcuModeSts'};
Mtr_Processing_SigEcuModeSts = CreateBus(Mtr_Processing_SigEcuModeSts, DeList);
clear DeList;

Mtr_Processing_SigIgnOnOffSts = Simulink.Bus;
DeList={'Mtr_Processing_SigIgnOnOffSts'};
Mtr_Processing_SigIgnOnOffSts = CreateBus(Mtr_Processing_SigIgnOnOffSts, DeList);
clear DeList;

Mtr_Processing_SigIgnEdgeSts = Simulink.Bus;
DeList={'Mtr_Processing_SigIgnEdgeSts'};
Mtr_Processing_SigIgnEdgeSts = CreateBus(Mtr_Processing_SigIgnEdgeSts, DeList);
clear DeList;

Mtr_Processing_SigMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Mtr_Processing_SigMtrProcessOutInfo = CreateBus(Mtr_Processing_SigMtrProcessOutInfo, DeList);
clear DeList;

