Mtr_Processing_DiagArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Mtr_Processing_DiagArbWhlVlvReqInfo = CreateBus(Mtr_Processing_DiagArbWhlVlvReqInfo, DeList);
clear DeList;

Mtr_Processing_DiagEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_Motor'
    };
Mtr_Processing_DiagEemFailData = CreateBus(Mtr_Processing_DiagEemFailData, DeList);
clear DeList;

Mtr_Processing_DiagEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_BBS_DefectiveModeFail'
    };
Mtr_Processing_DiagEemCtrlInhibitData = CreateBus(Mtr_Processing_DiagEemCtrlInhibitData, DeList);
clear DeList;

Mtr_Processing_DiagPwrMonInfoEsc = Simulink.Bus;
DeList={
    'VoltVBatt01Mon_Esc'
    'VoltVBatt02Mon_Esc'
    };
Mtr_Processing_DiagPwrMonInfoEsc = CreateBus(Mtr_Processing_DiagPwrMonInfoEsc, DeList);
clear DeList;

Mtr_Processing_DiagMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'MotMechAngleSpdFild'
    };
Mtr_Processing_DiagMotRotgAgSigInfo = CreateBus(Mtr_Processing_DiagMotRotgAgSigInfo, DeList);
clear DeList;

Mtr_Processing_DiagWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Mtr_Processing_DiagWhlSpdInfo = CreateBus(Mtr_Processing_DiagWhlSpdInfo, DeList);
clear DeList;

Mtr_Processing_DiagDiagHndlRequest = Simulink.Bus;
DeList={
    'DiagRequest_Order'
    'DiagRequest_Iq'
    'DiagRequest_Id'
    'DiagRequest_U_PWM'
    'DiagRequest_V_PWM'
    'DiagRequest_W_PWM'
    };
Mtr_Processing_DiagDiagHndlRequest = CreateBus(Mtr_Processing_DiagDiagHndlRequest, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Mtr_Processing_DiagMtrProcessOutInfo = CreateBus(Mtr_Processing_DiagMtrProcessOutInfo, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessOutData = Simulink.Bus;
DeList={
    'MtrProIqFailsafe'
    'MtrProIdFailsafe'
    'MtrProUPhasePWM'
    'MtrProVPhasePWM'
    'MtrProWPhasePWM'
    };
Mtr_Processing_DiagMtrProcessOutData = CreateBus(Mtr_Processing_DiagMtrProcessOutData, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    'MtrCaliResult'
    };
Mtr_Processing_DiagMtrProcessDataInf = CreateBus(Mtr_Processing_DiagMtrProcessDataInf, DeList);
clear DeList;

Mtr_Processing_DiagMtrDiagState = Simulink.Bus;
DeList={'Mtr_Processing_DiagMtrDiagState'};
Mtr_Processing_DiagMtrDiagState = CreateBus(Mtr_Processing_DiagMtrDiagState, DeList);
clear DeList;

