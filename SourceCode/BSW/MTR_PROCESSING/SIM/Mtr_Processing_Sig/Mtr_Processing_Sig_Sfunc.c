#define S_FUNCTION_NAME      Mtr_Processing_Sig_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          38
#define WidthOutputPort         7

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Mtr_Processing_Sig.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol = input[0];
    Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol = input[1];
    Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol = input[2];
    Mtr_Processing_SigEemFailData.Eem_Fail_RearSol = input[3];
    Mtr_Processing_SigEemFailData.Eem_Fail_Motor = input[4];
    Mtr_Processing_SigEemFailData.Eem_Fail_MPS = input[5];
    Mtr_Processing_SigEemFailData.Eem_Fail_MGD = input[6];
    Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt = input[7];
    Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt = input[8];
    Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt = input[9];
    Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt = input[10];
    Mtr_Processing_SigSwTrigMotInfo.MotPwrMon = input[11];
    Mtr_Processing_SigSwTrigMotInfo.StarMon = input[12];
    Mtr_Processing_SigSwTrigMotInfo.UoutMon = input[13];
    Mtr_Processing_SigSwTrigMotInfo.VoutMon = input[14];
    Mtr_Processing_SigSwTrigMotInfo.WoutMon = input[15];
    Mtr_Processing_SigWhlSpdInfo.FlWhlSpd = input[16];
    Mtr_Processing_SigWhlSpdInfo.FrWhlSpd = input[17];
    Mtr_Processing_SigWhlSpdInfo.RlWhlSpd = input[18];
    Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd = input[19];
    Mtr_Processing_SigHwTrigMotInfo.Uphase0Mon = input[20];
    Mtr_Processing_SigHwTrigMotInfo.Uphase1Mon = input[21];
    Mtr_Processing_SigHwTrigMotInfo.Vphase0Mon = input[22];
    Mtr_Processing_SigHwTrigMotInfo.VPhase1Mon = input[23];
    Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0 = input[24];
    Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1 = input[25];
    Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0 = input[26];
    Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1 = input[27];
    Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1deg = input[28];
    Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2deg = input[29];
    Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw = input[30];
    Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw = input[31];
    Mtr_Processing_SigEemEceData.Eem_Ece_Motor = input[32];
    Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState = input[33];
    Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult = input[34];
    Mtr_Processing_SigEcuModeSts = input[35];
    Mtr_Processing_SigIgnOnOffSts = input[36];
    Mtr_Processing_SigIgnEdgeSts = input[37];

    Mtr_Processing_Sig();


    output[0] = Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd;
    output[1] = Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd;
    output[2] = Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd;
    output[3] = Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw;
    output[4] = Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw;
    output[5] = Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
    output[6] = Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Mtr_Processing_Sig_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
