#define S_FUNCTION_NAME      Mtr_Processing_Diag_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          60
#define WidthOutputPort         8

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Mtr_Processing_Diag.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[0] = input[0];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[1] = input[1];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[2] = input[2];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[3] = input[3];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData[4] = input[4];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[0] = input[5];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[1] = input[6];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[2] = input[7];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[3] = input[8];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData[4] = input[9];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[0] = input[10];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[1] = input[11];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[2] = input[12];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[3] = input[13];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData[4] = input[14];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[0] = input[15];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[1] = input[16];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[2] = input[17];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[3] = input[18];
    Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData[4] = input[19];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[0] = input[20];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[1] = input[21];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[2] = input[22];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[3] = input[23];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData[4] = input[24];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[0] = input[25];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[1] = input[26];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[2] = input[27];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[3] = input[28];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData[4] = input[29];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[0] = input[30];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[1] = input[31];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[2] = input[32];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[3] = input[33];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData[4] = input[34];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[0] = input[35];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[1] = input[36];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[2] = input[37];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[3] = input[38];
    Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData[4] = input[39];
    Mtr_Processing_DiagEemFailData.Eem_Fail_Motor = input[40];
    Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = input[41];
    Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc = input[42];
    Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt02Mon_Esc = input[43];
    Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild = input[44];
    Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd = input[45];
    Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd = input[46];
    Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd = input[47];
    Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd = input[48];
    Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order = input[49];
    Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq = input[50];
    Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id = input[51];
    Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM = input[52];
    Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM = input[53];
    Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM = input[54];
    Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd = input[55];
    Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd = input[56];
    Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd = input[57];
    Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg = input[58];
    Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg = input[59];

    Mtr_Processing_Diag();


    output[0] = Mtr_Processing_DiagMtrProcessOutData.MtrProIqFailsafe;
    output[1] = Mtr_Processing_DiagMtrProcessOutData.MtrProIdFailsafe;
    output[2] = Mtr_Processing_DiagMtrProcessOutData.MtrProUPhasePWM;
    output[3] = Mtr_Processing_DiagMtrProcessOutData.MtrProVPhasePWM;
    output[4] = Mtr_Processing_DiagMtrProcessOutData.MtrProWPhasePWM;
    output[5] = Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState;
    output[6] = Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult;
    output[7] = Mtr_Processing_DiagMtrDiagState;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Mtr_Processing_Diag_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
