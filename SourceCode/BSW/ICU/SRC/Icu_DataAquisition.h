/**
 * @defgroup Icu_DataAcqusition Icu_DataAcqusition
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_DataAcqusition.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ICU_DATAACQUSITION_H_
#define ICU_DATAACQUSITION_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Icu_Types.h"
#include "Icu_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Icu_DataAquisition(void);
void Icu_StartInputCapture(Icu_ChannelType channel);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ICU_DATAACQUSITION_H_ */
/** @} */
