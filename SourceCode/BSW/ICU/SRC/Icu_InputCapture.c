/**
 * @defgroup Icu_InputCapture Icu_InputCapture
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_InputCapture.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Icu_InputCapture.h"
#include "Icu_InputCapture_Ifa.h"
#include "IfxStm_reg.h"
#include "Icu_DataAquisition.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ICU_INPUTCAPTURE_START_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ICU_INPUTCAPTURE_STOP_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_INPUTCAPTURE_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Icu_InputCapture_HdrBusType Icu_InputCaptureBus;

/* Version Info */
const SwcVersionInfo_t Icu_InputCaptureVersionInfo = 
{   
    ICU_INPUTCAPTURE_MODULE_ID,           /* Icu_InputCaptureVersionInfo.ModuleId */
    ICU_INPUTCAPTURE_MAJOR_VERSION,       /* Icu_InputCaptureVersionInfo.MajorVer */
    ICU_INPUTCAPTURE_MINOR_VERSION,       /* Icu_InputCaptureVersionInfo.MinorVer */
    ICU_INPUTCAPTURE_PATCH_VERSION,       /* Icu_InputCaptureVersionInfo.PatchVer */
    ICU_INPUTCAPTURE_BRANCH_VERSION       /* Icu_InputCaptureVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Icu_InputCaptureEcuModeSts;
Eem_SuspcDetnFuncInhibitIcuSts_t Icu_InputCaptureFuncInhibitIcuSts;

/* Output Data Element */
Icu_InputCaptureRisngIdxInfo_t Icu_InputCaptureRisngIdxInfo;
Icu_InputCaptureFallIdxInfo_t Icu_InputCaptureFallIdxInfo;
Icu_InputCaptureRisngTiStampInfo_t Icu_InputCaptureRisngTiStampInfo;
Icu_InputCaptureFallTiStampInfo_t Icu_InputCaptureFallTiStampInfo;

uint32 Icu_InputCapture_Timer_Start;
uint32 Icu_InputCapture_Timer_Elapsed;

#define ICU_INPUTCAPTURE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_INPUTCAPTURE_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_INPUTCAPTURE_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_INPUTCAPTURE_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_INPUTCAPTURE_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_INPUTCAPTURE_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_INPUTCAPTURE_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_INPUTCAPTURE_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_INPUTCAPTURE_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ICU_INPUTCAPTURE_START_SEC_CODE
#include "Icu_MemMap.h"

//LEJ
#include "TimE.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Icu_InputCapture_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Icu_InputCaptureBus.Icu_InputCaptureEcuModeSts = 0;
    Icu_InputCaptureBus.Icu_InputCaptureFuncInhibitIcuSts = 0;
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.FlRisngIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.FrRisngIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.RlRisngIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.RrRisngIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.FlFallIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.FrFallIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.RlFallIdx = 0;
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.RrFallIdx = 0;
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[i] = 0;   
    for(i=0;i<32;i++) Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[i] = 0;   
    
    for(i=0;i<Icu_IcuChannel_MaxNum;i++) Icu_StartInputCapture(i);
}

void Icu_InputCapture(void)
{
    uint16 i;

    #if (ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)    
        ASresult[TIME_AS_CHECKPOINT_FOR_5MS] = SetAlivesupervisionCheckpoint(TIME_AS_CHECKPOINT_FOR_5MS);   
    #elif (ALIVESUPERVISION_INDICATION == STD_ON)        
    ASresult[TIME_AS_CHECKPOINT_FOR_5MS] = SetAlivesupervisionCheckpoint0(TIME_AS_CHECKPOINT_FOR_5MS); 
    #endif 

    Icu_InputCapture_Timer_Start = STM0_TIM0.U;	

    /* Input */
    Icu_InputCapture_Read_Icu_InputCaptureEcuModeSts(&Icu_InputCaptureBus.Icu_InputCaptureEcuModeSts);
    Icu_InputCapture_Read_Icu_InputCaptureFuncInhibitIcuSts(&Icu_InputCaptureBus.Icu_InputCaptureFuncInhibitIcuSts);

    /* Process */
    Icu_DataAquisition();

    /* Output */
    Icu_InputCapture_Write_Icu_InputCaptureRisngIdxInfo(&Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo);
    /*==============================================================================
    * Members of structure Icu_InputCaptureRisngIdxInfo 
     : Icu_InputCaptureRisngIdxInfo.FlRisngIdx;
     : Icu_InputCaptureRisngIdxInfo.FrRisngIdx;
     : Icu_InputCaptureRisngIdxInfo.RlRisngIdx;
     : Icu_InputCaptureRisngIdxInfo.RrRisngIdx;
     =============================================================================*/
    
    Icu_InputCapture_Write_Icu_InputCaptureFallIdxInfo(&Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo);
    /*==============================================================================
    * Members of structure Icu_InputCaptureFallIdxInfo 
     : Icu_InputCaptureFallIdxInfo.FlFallIdx;
     : Icu_InputCaptureFallIdxInfo.FrFallIdx;
     : Icu_InputCaptureFallIdxInfo.RlFallIdx;
     : Icu_InputCaptureFallIdxInfo.RrFallIdx;
     =============================================================================*/
    
    Icu_InputCapture_Write_Icu_InputCaptureRisngTiStampInfo(&Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo);
    /*==============================================================================
    * Members of structure Icu_InputCaptureRisngTiStampInfo 
     : Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp;
     : Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp;
     : Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp;
     : Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp;
     =============================================================================*/
    
    Icu_InputCapture_Write_Icu_InputCaptureFallTiStampInfo(&Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo);
    /*==============================================================================
    * Members of structure Icu_InputCaptureFallTiStampInfo 
     : Icu_InputCaptureFallTiStampInfo.FlFallTiStamp;
     : Icu_InputCaptureFallTiStampInfo.FrFallTiStamp;
     : Icu_InputCaptureFallTiStampInfo.RlFallTiStamp;
     : Icu_InputCaptureFallTiStampInfo.RrFallTiStamp;
     =============================================================================*/
    

    Icu_InputCapture_Timer_Elapsed = STM0_TIM0.U - Icu_InputCapture_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ICU_INPUTCAPTURE_STOP_SEC_CODE
#include "Icu_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
