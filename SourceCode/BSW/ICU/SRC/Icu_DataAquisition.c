/**
 * @defgroup Icu_DataAquisition Icu_DataAquisition
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_DataAquisition.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Icu_InputCapture.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define Icu_TIMESTAMP_BUF_SIZE       32
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ICU_STOP_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

static Icu_CntType Icu_RisingTCount[Icu_IcuChannel_MaxNum], Icu_FallingTCount[Icu_IcuChannel_MaxNum];
static Icu_CntType Icu_FirstRisingEdgeCnt[Icu_IcuChannel_MaxNum], Icu_FirstFallingEdgeCnt[Icu_IcuChannel_MaxNum];

static Icu_IdxType Icu_RisingIdxRaw[Icu_IcuChannel_MaxNum];
static Icu_IdxType Icu_FallingIdxRaw[Icu_IcuChannel_MaxNum];

#pragma align 512
static Ifx_GTM_TIM_CH_GPR1 Icu_FallingDestArray[Icu_IcuChannel_MaxNum][Icu_TIMESTAMP_BUF_SIZE];
static Ifx_GTM_TIM_CH_GPR1 Icu_RisingDestArray[Icu_IcuChannel_MaxNum][Icu_TIMESTAMP_BUF_SIZE];
#pragma align restore

#define ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ICU_START_SEC_CODE
#include "Icu_MemMap.h"

void Icu_DataAquisition(void);
void Icu_StartInputCapture(Icu_ChannelType channel);
static void Icu_ChDataAquisition(Icu_ChannelType channel, Icu_IdxType* rIdx, Icu_IdxType* fIdx, Icu_ArrayType* rTiStamp, Icu_ArrayType* fTiStamp);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Icu_DataAquisition(void)
{
    Icu_ChDataAquisition(Icu_IcuChannel_Ch_FL, &Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.FlRisngIdx, &Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.FlFallIdx, &Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[0], &Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[0]);
    Icu_ChDataAquisition(Icu_IcuChannel_Ch_FR, &Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.FrRisngIdx, &Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.FrFallIdx, &Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[0], &Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[0]);
    Icu_ChDataAquisition(Icu_IcuChannel_Ch_RL, &Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.RlRisngIdx, &Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.RlFallIdx, &Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[0], &Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[0]);
    Icu_ChDataAquisition(Icu_IcuChannel_Ch_RR, &Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.RrRisngIdx, &Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.RrFallIdx, &Icu_InputCaptureBus.Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[0], &Icu_InputCaptureBus.Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[0]);    
}

/* Set-up the Source and Destination Address and DMA Hardware Transaction Start*/
void Icu_StartInputCapture(Icu_ChannelType channel)
{
    Std_ReturnType ReturnStatus;
    Haluint32 Gpr1_addr_r, Gpr1_addr_f;

    Icu_RisingIdxRaw[channel]    = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_FallingIdxRaw[channel]   = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.FlRisngIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.FrRisngIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.RlRisngIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureRisngIdxInfo.RrRisngIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.FlFallIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.FrFallIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.RlFallIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    Icu_InputCaptureBus.Icu_InputCaptureFallIdxInfo.RrFallIdx = (Icu_TIMESTAMP_BUF_SIZE-1);
    
    Gpr1_addr_r = (Haluint32)(((Haluint8*)&GTM_TIM0_CH0_GPR1) + (Haluint32)((0x800*(Icu_ChannelConfig[channel].GtmTimChGpr_Rising / 8)) + (0x80 * (Icu_ChannelConfig[channel].GtmTimChGpr_Rising % 8))));
    Gpr1_addr_f = (Haluint32)(((Haluint8*)&GTM_TIM0_CH0_GPR1) + (Haluint32)((0x800*(Icu_ChannelConfig[channel].GtmTimChGpr_Falling / 8)) + (0x80 * (Icu_ChannelConfig[channel].GtmTimChGpr_Falling % 8))));
    ReturnStatus  = Dma_StartHwTransaction(Icu_ChannelConfig[channel].DmaCh_Rising,  (Haluint32*)(Gpr1_addr_r), (Haluint32*)&Icu_RisingDestArray[channel][0].U,  Icu_TIMESTAMP_BUF_SIZE);
    ReturnStatus =  Dma_StartHwTransaction(Icu_ChannelConfig[channel].DmaCh_Falling, (Haluint32*)(Gpr1_addr_f), (Haluint32*)&Icu_FallingDestArray[channel][0].U, Icu_TIMESTAMP_BUF_SIZE);
}

/* Not used function but only needed for Compiling because of Tresos Generation */
void Tim0_Ch1_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim0_Ch2_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim0_Ch3_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim0_Ch4_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim1_Ch1_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim1_Ch2_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim1_Ch3_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Tim1_Ch4_Notification(uint8 ModuleType, uint8 ModuleNo, uint8 ChannelNo,uint16 IrqNotifVal){;}
void Dma_ErrorCallOut(uint8 Channel, uint8 Error){;}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Icu_ChDataAquisition(Icu_ChannelType channel, Icu_IdxType* rIdx, Icu_IdxType* fIdx, Icu_ArrayType* rTiStamp, Icu_ArrayType* fTiStamp)
{
    Haluint16 i,j;

    /* Store of the First Rising and Falling Edge Counter for comparison of edge order*/
    /* The filtered ECNT(Rising/falling) is 8 bit register and need to match with Ring Buffer size. */
    /* It can seriously affect to compare with First rising edge and First falling Edge Count.      */
    Icu_FirstRisingEdgeCnt[channel]  = Icu_RisingDestArray[channel][0].B.ECNT % Icu_TIMESTAMP_BUF_SIZE;
    Icu_FirstFallingEdgeCnt[channel] = Icu_FallingDestArray[channel][0].B.ECNT % Icu_TIMESTAMP_BUF_SIZE;
    
    /* Store of the DMA Transfer Count for DMA Ring Buffer Index Set-up */
    Icu_RisingTCount[channel]  = Dma_GetTransferCount(Icu_ChannelConfig[channel].DmaCh_Rising);
    Icu_FallingTCount[channel] = Dma_GetTransferCount(Icu_ChannelConfig[channel].DmaCh_Falling);
        
    /* Calculation of the Raw value of Ring Buffer Index */
    Icu_RisingIdxRaw[channel]  = Icu_TIMESTAMP_BUF_SIZE - Icu_RisingTCount[channel]  - 1;
    Icu_FallingIdxRaw[channel] = Icu_TIMESTAMP_BUF_SIZE - Icu_FallingTCount[channel] - 1;
    

    /* Calculation and Store of the Current Buffer Index and Buffer Pointer */
    /* Shift the Rising Buffer when the buffer is filled earlier than falling buffer to arrange the edge order*/


        if(Icu_FirstRisingEdgeCnt[channel] < Icu_FirstFallingEdgeCnt[channel])
        {
            *rIdx  = Icu_RisingIdxRaw[channel];
            *fIdx = Icu_FallingIdxRaw[channel];

            for(i=0; i<Icu_TIMESTAMP_BUF_SIZE; i++)
            {
                rTiStamp[i]  = Icu_RisingDestArray[channel][i].B.GPR1;
                fTiStamp[i] = Icu_FallingDestArray[channel][i].B.GPR1;
            }
        }
        else if(Icu_FirstRisingEdgeCnt[channel] > Icu_FirstFallingEdgeCnt[channel])
        {
            *rIdx  = (Icu_RisingIdxRaw[channel] + 1) % Icu_TIMESTAMP_BUF_SIZE ;
            *fIdx = Icu_FallingIdxRaw[channel] ;
            for(j=0; j<Icu_TIMESTAMP_BUF_SIZE; j++)
            {
                if(j!=(Icu_TIMESTAMP_BUF_SIZE-1))
                {
                    rTiStamp[j+1] = Icu_RisingDestArray[channel][j].B.GPR1;
                    fTiStamp[j]  = Icu_FallingDestArray[channel][j].B.GPR1;
                }
                else
                {
                    rTiStamp[0]  = Icu_RisingDestArray[channel][j].B.GPR1;
                    fTiStamp[j] = Icu_FallingDestArray[channel][j].B.GPR1;
                }
            }
        }
        else    /*Initial Status*/
        {
            ;
        }
    
} 

#define ICU_STOP_SEC_CODE
#include "Icu_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
