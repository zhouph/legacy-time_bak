/**
 * @defgroup Icu_Cal Icu_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Icu_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_CALIB_UNSPECIFIED
#include "Icu_MemMap.h"
/* Global Calibration Section */


#define ICU_STOP_SEC_CALIB_UNSPECIFIED
#include "Icu_MemMap.h"

#define ICU_START_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ICU_STOP_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ICU_START_SEC_CODE
#include "Icu_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ICU_STOP_SEC_CODE
#include "Icu_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
