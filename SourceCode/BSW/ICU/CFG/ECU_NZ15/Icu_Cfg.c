/**
 * @defgroup Icu_Cfg Icu_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Icu_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
const Icu_ChannelConfigType Icu_ChannelConfig[] = 
{
    /* Icu Configuration for Icu Cahnnel FL */
    {
        DMA_CH11,                                       /* Dma Rising Edge Channel */
        DMA_CH12,                                       /* Dma Falling Edge Channel */
        (Icu_GtmTimChType)GTM_TIM0_CHANNEL4,         /* Gtm Tim Channel GPR for Rising Edge */
        (Icu_GtmTimChType)GTM_TIM1_CHANNEL4          /* Gtm Tim Channel GPR for Falling Edge */
    },
    /* Icu Configuration for Icu Cahnnel FR */
    {
        DMA_CH13,                                       /* Dma Rising Edge Channel */
        DMA_CH14,                                       /* Dma Falling Edge Channel */
        (Icu_GtmTimChType)GTM_TIM0_CHANNEL1,         /* Gtm Tim Channel GPR for Rising Edge */
        (Icu_GtmTimChType)GTM_TIM1_CHANNEL1          /* Gtm Tim Channel GPR for Falling Edge */
    },
    /* Icu Configuration for Icu Cahnnel RL */
    {
        DMA_CH15,                                       /* Dma Rising Edge Channel */
        DMA_CH16,                                       /* Dma Falling Edge Channel */
        (Icu_GtmTimChType)GTM_TIM0_CHANNEL3,         /* Gtm Tim Channel GPR for Rising Edge */
        (Icu_GtmTimChType)GTM_TIM1_CHANNEL3          /* Gtm Tim Channel GPR for Falling Edge */
    },
    /* Icu Configuration for Icu Cahnnel RR */
    {
        DMA_CH17,                                       /* Dma Rising Edge Channel */
        DMA_CH18,                                       /* Dma Falling Edge Channel */
        (Icu_GtmTimChType)GTM_TIM0_CHANNEL2,         /* Gtm Tim Channel GPR for Rising Edge */
        (Icu_GtmTimChType)GTM_TIM1_CHANNEL2          /* Gtm Tim Channel GPR for Falling Edge */
    }
};
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ICU_STOP_SEC_CONST_UNSPECIFIED
#include "Icu_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ICU_STOP_SEC_VAR_NOINIT_32BIT
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ICU_STOP_SEC_VAR_UNSPECIFIED
#include "Icu_MemMap.h"
#define ICU_START_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/** Variable Section (32BIT)**/


#define ICU_STOP_SEC_VAR_32BIT
#include "Icu_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ICU_START_SEC_CODE
#include "Icu_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ICU_STOP_SEC_CODE
#include "Icu_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
