/**
 * @defgroup Icu_InputCapture Icu_InputCapture
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_InputCapture.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ICU_INPUTCAPTURE_H_
#define ICU_INPUTCAPTURE_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Icu_Types.h"
#include "Icu_Cfg.h"
#include "Icu_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ICU_INPUTCAPTURE_MODULE_ID      (0)
 #define ICU_INPUTCAPTURE_MAJOR_VERSION  (2)
 #define ICU_INPUTCAPTURE_MINOR_VERSION  (0)
 #define ICU_INPUTCAPTURE_PATCH_VERSION  (0)
 #define ICU_INPUTCAPTURE_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Icu_InputCapture_HdrBusType Icu_InputCaptureBus;

/* Version Info */
extern const SwcVersionInfo_t Icu_InputCaptureVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Icu_InputCaptureEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIcuSts_t Icu_InputCaptureFuncInhibitIcuSts;

/* Output Data Element */
extern Icu_InputCaptureRisngIdxInfo_t Icu_InputCaptureRisngIdxInfo;
extern Icu_InputCaptureFallIdxInfo_t Icu_InputCaptureFallIdxInfo;
extern Icu_InputCaptureRisngTiStampInfo_t Icu_InputCaptureRisngTiStampInfo;
extern Icu_InputCaptureFallTiStampInfo_t Icu_InputCaptureFallTiStampInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Icu_InputCapture_Init(void);
extern void Icu_InputCapture(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ICU_INPUTCAPTURE_H_ */
/** @} */
