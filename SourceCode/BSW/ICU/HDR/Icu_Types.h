/**
 * @defgroup Icu_Types Icu_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ICU_TYPES_H_
#define ICU_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

#include "Gtm.h"
#include "Dma.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
/** \brief Type for Icu Channel */
typedef Haluint8 Icu_ChannelType;

/** \brief Type for Ring Buffer Index information of DMA for Wheel */
typedef Haluint16 Icu_IdxType;

/** \brief Type for Transfer Count information of DMA for Wheel */
typedef Haluint16 Icu_CntType;

/** \brief Type for Ring Buffer information of DMA for Wheel */
typedef Haluint32 Icu_ArrayType;

/** \brief Type for Gtm Tim Module Channel */
typedef Haluint16 Icu_GtmTimChType;

typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Icu_InputCaptureEcuModeSts;
    Eem_SuspcDetnFuncInhibitIcuSts_t Icu_InputCaptureFuncInhibitIcuSts;

/* Output Data Element */
    Icu_InputCaptureRisngIdxInfo_t Icu_InputCaptureRisngIdxInfo;
    Icu_InputCaptureFallIdxInfo_t Icu_InputCaptureFallIdxInfo;
    Icu_InputCaptureRisngTiStampInfo_t Icu_InputCaptureRisngTiStampInfo;
    Icu_InputCaptureFallTiStampInfo_t Icu_InputCaptureFallTiStampInfo;
}Icu_InputCapture_HdrBusType;

/** \brief Type for Icu Channel Configuration */
typedef struct 
{
    Dma_ChType              DmaCh_Rising;
    Dma_ChType              DmaCh_Falling;
    Icu_GtmTimChType     GtmTimChGpr_Rising;
    Icu_GtmTimChType     GtmTimChGpr_Falling;
}Icu_ChannelConfigType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ICU_TYPES_H_ */
/** @} */
