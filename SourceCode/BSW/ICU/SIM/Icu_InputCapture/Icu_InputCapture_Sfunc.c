#define S_FUNCTION_NAME      Icu_InputCapture_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          2
#define WidthOutputPort         264

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Icu_InputCapture.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Icu_InputCaptureEcuModeSts = input[0];
    Icu_InputCaptureFuncInhibitIcuSts = input[1];

    Icu_InputCapture();


    output[0] = Icu_InputCaptureRisngIdxInfo.FlRisngIdx;
    output[1] = Icu_InputCaptureRisngIdxInfo.FrRisngIdx;
    output[2] = Icu_InputCaptureRisngIdxInfo.RlRisngIdx;
    output[3] = Icu_InputCaptureRisngIdxInfo.RrRisngIdx;
    output[4] = Icu_InputCaptureFallIdxInfo.FlFallIdx;
    output[5] = Icu_InputCaptureFallIdxInfo.FrFallIdx;
    output[6] = Icu_InputCaptureFallIdxInfo.RlFallIdx;
    output[7] = Icu_InputCaptureFallIdxInfo.RrFallIdx;
    output[8] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[0];
    output[9] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[1];
    output[10] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[2];
    output[11] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[3];
    output[12] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[4];
    output[13] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[5];
    output[14] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[6];
    output[15] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[7];
    output[16] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[8];
    output[17] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[9];
    output[18] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[10];
    output[19] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[11];
    output[20] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[12];
    output[21] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[13];
    output[22] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[14];
    output[23] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[15];
    output[24] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[16];
    output[25] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[17];
    output[26] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[18];
    output[27] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[19];
    output[28] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[20];
    output[29] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[21];
    output[30] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[22];
    output[31] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[23];
    output[32] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[24];
    output[33] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[25];
    output[34] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[26];
    output[35] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[27];
    output[36] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[28];
    output[37] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[29];
    output[38] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[30];
    output[39] = Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[31];
    output[40] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[0];
    output[41] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[1];
    output[42] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[2];
    output[43] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[3];
    output[44] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[4];
    output[45] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[5];
    output[46] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[6];
    output[47] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[7];
    output[48] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[8];
    output[49] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[9];
    output[50] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[10];
    output[51] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[11];
    output[52] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[12];
    output[53] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[13];
    output[54] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[14];
    output[55] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[15];
    output[56] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[16];
    output[57] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[17];
    output[58] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[18];
    output[59] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[19];
    output[60] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[20];
    output[61] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[21];
    output[62] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[22];
    output[63] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[23];
    output[64] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[24];
    output[65] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[25];
    output[66] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[26];
    output[67] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[27];
    output[68] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[28];
    output[69] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[29];
    output[70] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[30];
    output[71] = Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[31];
    output[72] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[0];
    output[73] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[1];
    output[74] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[2];
    output[75] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[3];
    output[76] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[4];
    output[77] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[5];
    output[78] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[6];
    output[79] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[7];
    output[80] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[8];
    output[81] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[9];
    output[82] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[10];
    output[83] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[11];
    output[84] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[12];
    output[85] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[13];
    output[86] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[14];
    output[87] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[15];
    output[88] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[16];
    output[89] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[17];
    output[90] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[18];
    output[91] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[19];
    output[92] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[20];
    output[93] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[21];
    output[94] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[22];
    output[95] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[23];
    output[96] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[24];
    output[97] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[25];
    output[98] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[26];
    output[99] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[27];
    output[100] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[28];
    output[101] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[29];
    output[102] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[30];
    output[103] = Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[31];
    output[104] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[0];
    output[105] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[1];
    output[106] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[2];
    output[107] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[3];
    output[108] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[4];
    output[109] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[5];
    output[110] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[6];
    output[111] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[7];
    output[112] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[8];
    output[113] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[9];
    output[114] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[10];
    output[115] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[11];
    output[116] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[12];
    output[117] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[13];
    output[118] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[14];
    output[119] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[15];
    output[120] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[16];
    output[121] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[17];
    output[122] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[18];
    output[123] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[19];
    output[124] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[20];
    output[125] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[21];
    output[126] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[22];
    output[127] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[23];
    output[128] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[24];
    output[129] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[25];
    output[130] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[26];
    output[131] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[27];
    output[132] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[28];
    output[133] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[29];
    output[134] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[30];
    output[135] = Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[31];
    output[136] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[0];
    output[137] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[1];
    output[138] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[2];
    output[139] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[3];
    output[140] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[4];
    output[141] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[5];
    output[142] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[6];
    output[143] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[7];
    output[144] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[8];
    output[145] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[9];
    output[146] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[10];
    output[147] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[11];
    output[148] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[12];
    output[149] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[13];
    output[150] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[14];
    output[151] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[15];
    output[152] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[16];
    output[153] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[17];
    output[154] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[18];
    output[155] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[19];
    output[156] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[20];
    output[157] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[21];
    output[158] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[22];
    output[159] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[23];
    output[160] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[24];
    output[161] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[25];
    output[162] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[26];
    output[163] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[27];
    output[164] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[28];
    output[165] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[29];
    output[166] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[30];
    output[167] = Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[31];
    output[168] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[0];
    output[169] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[1];
    output[170] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[2];
    output[171] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[3];
    output[172] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[4];
    output[173] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[5];
    output[174] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[6];
    output[175] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[7];
    output[176] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[8];
    output[177] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[9];
    output[178] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[10];
    output[179] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[11];
    output[180] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[12];
    output[181] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[13];
    output[182] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[14];
    output[183] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[15];
    output[184] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[16];
    output[185] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[17];
    output[186] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[18];
    output[187] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[19];
    output[188] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[20];
    output[189] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[21];
    output[190] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[22];
    output[191] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[23];
    output[192] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[24];
    output[193] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[25];
    output[194] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[26];
    output[195] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[27];
    output[196] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[28];
    output[197] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[29];
    output[198] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[30];
    output[199] = Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[31];
    output[200] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[0];
    output[201] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[1];
    output[202] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[2];
    output[203] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[3];
    output[204] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[4];
    output[205] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[5];
    output[206] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[6];
    output[207] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[7];
    output[208] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[8];
    output[209] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[9];
    output[210] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[10];
    output[211] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[11];
    output[212] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[12];
    output[213] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[13];
    output[214] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[14];
    output[215] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[15];
    output[216] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[16];
    output[217] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[17];
    output[218] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[18];
    output[219] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[19];
    output[220] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[20];
    output[221] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[21];
    output[222] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[22];
    output[223] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[23];
    output[224] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[24];
    output[225] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[25];
    output[226] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[26];
    output[227] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[27];
    output[228] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[28];
    output[229] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[29];
    output[230] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[30];
    output[231] = Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[31];
    output[232] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[0];
    output[233] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[1];
    output[234] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[2];
    output[235] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[3];
    output[236] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[4];
    output[237] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[5];
    output[238] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[6];
    output[239] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[7];
    output[240] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[8];
    output[241] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[9];
    output[242] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[10];
    output[243] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[11];
    output[244] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[12];
    output[245] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[13];
    output[246] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[14];
    output[247] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[15];
    output[248] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[16];
    output[249] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[17];
    output[250] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[18];
    output[251] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[19];
    output[252] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[20];
    output[253] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[21];
    output[254] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[22];
    output[255] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[23];
    output[256] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[24];
    output[257] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[25];
    output[258] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[26];
    output[259] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[27];
    output[260] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[28];
    output[261] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[29];
    output[262] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[30];
    output[263] = Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[31];
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Icu_InputCapture_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
