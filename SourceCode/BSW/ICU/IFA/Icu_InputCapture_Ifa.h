/**
 * @defgroup Icu_InputCapture_Ifa Icu_InputCapture_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Icu_InputCapture_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ICU_INPUTCAPTURE_IFA_H_
#define ICU_INPUTCAPTURE_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Icu_InputCapture_Read_Icu_InputCaptureEcuModeSts(data) do \
{ \
    *data = Icu_InputCaptureEcuModeSts; \
}while(0);

#define Icu_InputCapture_Read_Icu_InputCaptureFuncInhibitIcuSts(data) do \
{ \
    *data = Icu_InputCaptureFuncInhibitIcuSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Icu_InputCapture_Write_Icu_InputCaptureRisngIdxInfo(data) do \
{ \
    Icu_InputCaptureRisngIdxInfo = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureFallIdxInfo(data) do \
{ \
    Icu_InputCaptureFallIdxInfo = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureRisngTiStampInfo(data) do \
{ \
    Icu_InputCaptureRisngTiStampInfo = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureFallTiStampInfo(data) do \
{ \
    Icu_InputCaptureFallTiStampInfo = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureRisngIdxInfo_FlRisngIdx(data) do \
{ \
    Icu_InputCaptureRisngIdxInfo.FlRisngIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureRisngIdxInfo_FrRisngIdx(data) do \
{ \
    Icu_InputCaptureRisngIdxInfo.FrRisngIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureRisngIdxInfo_RlRisngIdx(data) do \
{ \
    Icu_InputCaptureRisngIdxInfo.RlRisngIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureRisngIdxInfo_RrRisngIdx(data) do \
{ \
    Icu_InputCaptureRisngIdxInfo.RrRisngIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureFallIdxInfo_FlFallIdx(data) do \
{ \
    Icu_InputCaptureFallIdxInfo.FlFallIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureFallIdxInfo_FrFallIdx(data) do \
{ \
    Icu_InputCaptureFallIdxInfo.FrFallIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureFallIdxInfo_RlFallIdx(data) do \
{ \
    Icu_InputCaptureFallIdxInfo.RlFallIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureFallIdxInfo_RrFallIdx(data) do \
{ \
    Icu_InputCaptureFallIdxInfo.RrFallIdx = *data; \
}while(0);

#define Icu_InputCapture_Write_Icu_InputCaptureRisngTiStampInfo_FlRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureRisngTiStampInfo.FlRisngTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureRisngTiStampInfo_FrRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureRisngTiStampInfo.FrRisngTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureRisngTiStampInfo_RlRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureRisngTiStampInfo.RlRisngTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureRisngTiStampInfo_RrRisngTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureRisngTiStampInfo.RrRisngTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureFallTiStampInfo_FlFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureFallTiStampInfo.FlFallTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureFallTiStampInfo_FrFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureFallTiStampInfo.FrFallTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureFallTiStampInfo_RlFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureFallTiStampInfo.RlFallTiStamp[i] = *data[i]; \
}
#define Icu_InputCapture_Write_Icu_InputCaptureFallTiStampInfo_RrFallTiStamp(data) \
{ \
    for(i=0;i<32;i++) Icu_InputCaptureFallTiStampInfo.RrFallTiStamp[i] = *data[i]; \
}
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ICU_INPUTCAPTURE_IFA_H_ */
/** @} */
