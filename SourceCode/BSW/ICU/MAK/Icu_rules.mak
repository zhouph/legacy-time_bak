# \file
#
# \brief Icu
#
# This file contains the implementation of the SWC
# module Icu.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Icu_src

Icu_src_FILES        += $(Icu_SRC_PATH)\Icu_InputCapture.c
Icu_src_FILES        += $(Icu_SRC_PATH)\Icu_DataAquisition.c
Icu_src_FILES        += $(Icu_IFA_PATH)\Icu_InputCapture_Ifa.c
Icu_src_FILES        += $(Icu_CFG_PATH)\Icu_Cfg.c
Icu_src_FILES        += $(Icu_CAL_PATH)\Icu_Cal.c

ifeq ($(ICE_COMPILE),true)
Icu_src_FILES        += $(Icu_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Icu_src_FILES        += $(Icu_UNITY_PATH)\unity.c
	Icu_src_FILES        += $(Icu_UNITY_PATH)\unity_fixture.c	
	Icu_src_FILES        += $(Icu_UT_PATH)\main.c
	Icu_src_FILES        += $(Icu_UT_PATH)\Icu_InputCapture\Icu_InputCapture_UtMain.c
endif