# \file
#
# \brief Icu
#
# This file contains the implementation of the SWC
# module Icu.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Icu_CORE_PATH     := $(MANDO_BSW_ROOT)\Icu
Icu_CAL_PATH      := $(Icu_CORE_PATH)\CAL\$(Icu_VARIANT)
Icu_SRC_PATH      := $(Icu_CORE_PATH)\SRC
Icu_CFG_PATH      := $(Icu_CORE_PATH)\CFG\$(Icu_VARIANT)
Icu_HDR_PATH      := $(Icu_CORE_PATH)\HDR
Icu_IFA_PATH      := $(Icu_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Icu_CMN_PATH      := $(Icu_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Icu_UT_PATH		:= $(Icu_CORE_PATH)\UT
	Icu_UNITY_PATH	:= $(Icu_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Icu_UT_PATH)
	CC_INCLUDE_PATH		+= $(Icu_UNITY_PATH)
	Icu_InputCapture_PATH 	:= Icu_UT_PATH\Icu_InputCapture
endif
CC_INCLUDE_PATH    += $(Icu_CAL_PATH)
CC_INCLUDE_PATH    += $(Icu_SRC_PATH)
CC_INCLUDE_PATH    += $(Icu_CFG_PATH)
CC_INCLUDE_PATH    += $(Icu_HDR_PATH)
CC_INCLUDE_PATH    += $(Icu_IFA_PATH)
CC_INCLUDE_PATH    += $(Icu_CMN_PATH)

