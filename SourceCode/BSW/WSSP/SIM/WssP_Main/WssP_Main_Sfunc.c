#define S_FUNCTION_NAME      WssP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          13
#define WidthOutputPort         54

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "WssP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    WssP_MainWhlSpdInfo.FlWhlSpd = input[0];
    WssP_MainWhlSpdInfo.FrWhlSpd = input[1];
    WssP_MainWhlSpdInfo.RlWhlSpd = input[2];
    WssP_MainWhlSpdInfo.RrlWhlSpd = input[3];
    WssP_MainAbsCtrlInfo.AbsActFlg = input[4];
    WssP_MainWssMonData.WssM_Inhibit_FL = input[5];
    WssP_MainWssMonData.WssM_Inhibit_FR = input[6];
    WssP_MainWssMonData.WssM_Inhibit_RL = input[7];
    WssP_MainWssMonData.WssM_Inhibit_RR = input[8];
    WssP_MainEcuModeSts = input[9];
    WssP_MainIgnOnOffSts = input[10];
    WssP_MainIgnEdgeSts = input[11];
    WssP_MainDiagClrSrs = input[12];

    WssP_Main();


    output[0] = WssP_MainWssPlauData.WssP_AbsLongTermErr;
    output[1] = WssP_MainWssPlauData.WssP_AbsLongTerm_Ihb;
    output[2] = WssP_MainWssPlauData.WssP_FL_PhaseErr;
    output[3] = WssP_MainWssPlauData.WssP_FL_PhaseErr_Ihb;
    output[4] = WssP_MainWssPlauData.WssP_FR_PhaseErr;
    output[5] = WssP_MainWssPlauData.WssP_FR_PhaseErr_Ihb;
    output[6] = WssP_MainWssPlauData.WssP_RL_PhaseErr;
    output[7] = WssP_MainWssPlauData.WssP_RL_PhaseErr_Ihb;
    output[8] = WssP_MainWssPlauData.WssP_RR_PhaseErr;
    output[9] = WssP_MainWssPlauData.WssP_RR_PhaseErr_Ihb;
    output[10] = WssP_MainWssPlauData.WssP_FL_ExciterErr;
    output[11] = WssP_MainWssPlauData.WssP_FL_ExciterErr_Ihb;
    output[12] = WssP_MainWssPlauData.WssP_FR_ExciterErr;
    output[13] = WssP_MainWssPlauData.WssP_FR_ExciterErr_Ihb;
    output[14] = WssP_MainWssPlauData.WssP_RL_ExciterErr;
    output[15] = WssP_MainWssPlauData.WssP_RL_ExciterErr_Ihb;
    output[16] = WssP_MainWssPlauData.WssP_RR_ExciterErr;
    output[17] = WssP_MainWssPlauData.WssP_RR_ExciterErr_Ihb;
    output[18] = WssP_MainWssPlauData.WssP_FL_AirGapErr;
    output[19] = WssP_MainWssPlauData.WssP_FL_AirGapErr_Ihb;
    output[20] = WssP_MainWssPlauData.WssP_FR_AirGapErr;
    output[21] = WssP_MainWssPlauData.WssP_FR_AirGapErr_Ihb;
    output[22] = WssP_MainWssPlauData.WssP_RL_AirGapErr;
    output[23] = WssP_MainWssPlauData.WssP_RL_AirGapErr_Ihb;
    output[24] = WssP_MainWssPlauData.WssP_RR_AirGapErr;
    output[25] = WssP_MainWssPlauData.WssP_RR_AirGapErr_Ihb;
    output[26] = WssP_MainWssPlauData.WssP_FL_PJumpErr;
    output[27] = WssP_MainWssPlauData.WssP_FL_PJumpErr_Ihb;
    output[28] = WssP_MainWssPlauData.WssP_FR_PJumpErr;
    output[29] = WssP_MainWssPlauData.WssP_FR_PJumpErr_Ihb;
    output[30] = WssP_MainWssPlauData.WssP_RL_PJumpErr;
    output[31] = WssP_MainWssPlauData.WssP_RL_PJumpErr_Ihb;
    output[32] = WssP_MainWssPlauData.WssP_RR_PJumpErr;
    output[33] = WssP_MainWssPlauData.WssP_RR_PJumpErr_Ihb;
    output[34] = WssP_MainWssPlauData.WssP_FL_MJumpErr;
    output[35] = WssP_MainWssPlauData.WssP_FL_MJumpErr_Ihb;
    output[36] = WssP_MainWssPlauData.WssP_FR_MJumpErr;
    output[37] = WssP_MainWssPlauData.WssP_FR_MJumpErr_Ihb;
    output[38] = WssP_MainWssPlauData.WssP_RL_MJumpErr;
    output[39] = WssP_MainWssPlauData.WssP_RL_MJumpErr_Ihb;
    output[40] = WssP_MainWssPlauData.WssP_RR_MJumpErr;
    output[41] = WssP_MainWssPlauData.WssP_RR_MJumpErr_Ihb;
    output[42] = WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr;
    output[43] = WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr_Ihb;
    output[44] = WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr;
    output[45] = WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr_Ihb;
    output[46] = WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr;
    output[47] = WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr_Ihb;
    output[48] = WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr;
    output[49] = WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr_Ihb;
    output[50] = WssP_MainWssPlauData.WssP_max_speed;
    output[51] = WssP_MainWssPlauData.WssP_2nd_speed;
    output[52] = WssP_MainWssPlauData.WssP_3rd_speed;
    output[53] = WssP_MainWssPlauData.WssP_min_speed;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    WssP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
