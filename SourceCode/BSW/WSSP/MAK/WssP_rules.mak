# \file
#
# \brief WssP
#
# This file contains the implementation of the SWC
# module WssP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += WssP_src

WssP_src_FILES        += $(WssP_SRC_PATH)\WssP_Main.c
WssP_src_FILES        += $(WssP_IFA_PATH)\WssP_Main_Ifa.c
WssP_src_FILES        += $(WssP_CFG_PATH)\WssP_Cfg.c
WssP_src_FILES        += $(WssP_CAL_PATH)\WssP_Cal.c
WssP_src_FILES        += $(WssP_SRC_PATH)\WssP_Main_Proc.c

ifeq ($(ICE_COMPILE),true)
WssP_src_FILES        += $(WssP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	WssP_src_FILES        += $(WssP_UNITY_PATH)\unity.c
	WssP_src_FILES        += $(WssP_UNITY_PATH)\unity_fixture.c	
	WssP_src_FILES        += $(WssP_UT_PATH)\main.c
	WssP_src_FILES        += $(WssP_UT_PATH)\WssP_Main\WssP_Main_UtMain.c
endif