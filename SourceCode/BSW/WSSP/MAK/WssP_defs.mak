# \file
#
# \brief WssP
#
# This file contains the implementation of the SWC
# module WssP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

WssP_CORE_PATH     := $(MANDO_BSW_ROOT)\WssP
WssP_CAL_PATH      := $(WssP_CORE_PATH)\CAL\$(WssP_VARIANT)
WssP_SRC_PATH      := $(WssP_CORE_PATH)\SRC
WssP_CFG_PATH      := $(WssP_CORE_PATH)\CFG\$(WssP_VARIANT)
WssP_HDR_PATH      := $(WssP_CORE_PATH)\HDR
WssP_IFA_PATH      := $(WssP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    WssP_CMN_PATH      := $(WssP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	WssP_UT_PATH		:= $(WssP_CORE_PATH)\UT
	WssP_UNITY_PATH	:= $(WssP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(WssP_UT_PATH)
	CC_INCLUDE_PATH		+= $(WssP_UNITY_PATH)
	WssP_Main_PATH 	:= WssP_UT_PATH\WssP_Main
endif
CC_INCLUDE_PATH    += $(WssP_CAL_PATH)
CC_INCLUDE_PATH    += $(WssP_SRC_PATH)
CC_INCLUDE_PATH    += $(WssP_CFG_PATH)
CC_INCLUDE_PATH    += $(WssP_HDR_PATH)
CC_INCLUDE_PATH    += $(WssP_IFA_PATH)
CC_INCLUDE_PATH    += $(WssP_CMN_PATH)

