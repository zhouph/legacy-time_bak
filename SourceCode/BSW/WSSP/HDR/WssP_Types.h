/**
 * @defgroup WssP_Types WssP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSP_TYPES_H_
#define WSSP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Wss_SenWhlSpdInfo_t WssP_MainWhlSpdInfo;
    Abc_CtrlAbsCtrlInfo_t WssP_MainAbsCtrlInfo;
    WssM_MainWssMonData_t WssP_MainWssMonData;
    Mom_HndlrEcuModeSts_t WssP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t WssP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t WssP_MainIgnEdgeSts;
    Diag_HndlrDiagClr_t WssP_MainDiagClrSrs;

/* Output Data Element */
    WssP_MainWssPlauData_t WssP_MainWssPlauData;
}WssP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSP_TYPES_H_ */
/** @} */
