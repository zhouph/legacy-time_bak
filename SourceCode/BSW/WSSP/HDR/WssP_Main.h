/**
 * @defgroup WssP_Main WssP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSP_MAIN_H_
#define WSSP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssP_Types.h"
#include "WssP_Cfg.h"
#include "WssP_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define WSSP_MAIN_MODULE_ID      (0)
 #define WSSP_MAIN_MAJOR_VERSION  (2)
 #define WSSP_MAIN_MINOR_VERSION  (0)
 #define WSSP_MAIN_PATCH_VERSION  (0)
 #define WSSP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern WssP_Main_HdrBusType WssP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t WssP_MainVersionInfo;

/* Input Data Element */
extern Wss_SenWhlSpdInfo_t WssP_MainWhlSpdInfo;
extern Abc_CtrlAbsCtrlInfo_t WssP_MainAbsCtrlInfo;
extern WssM_MainWssMonData_t WssP_MainWssMonData;
extern Mom_HndlrEcuModeSts_t WssP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t WssP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t WssP_MainIgnEdgeSts;
extern Diag_HndlrDiagClr_t WssP_MainDiagClrSrs;

/* Output Data Element */
extern WssP_MainWssPlauData_t WssP_MainWssPlauData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void WssP_Main_Init(void);
extern void WssP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSP_MAIN_H_ */
/** @} */
