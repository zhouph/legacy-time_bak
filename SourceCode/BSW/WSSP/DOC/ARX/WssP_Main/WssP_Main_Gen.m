WssP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
WssP_MainWhlSpdInfo = CreateBus(WssP_MainWhlSpdInfo, DeList);
clear DeList;

WssP_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    };
WssP_MainAbsCtrlInfo = CreateBus(WssP_MainAbsCtrlInfo, DeList);
clear DeList;

WssP_MainWssMonData = Simulink.Bus;
DeList={
    'WssM_Inhibit_FL'
    'WssM_Inhibit_FR'
    'WssM_Inhibit_RL'
    'WssM_Inhibit_RR'
    };
WssP_MainWssMonData = CreateBus(WssP_MainWssMonData, DeList);
clear DeList;

WssP_MainEcuModeSts = Simulink.Bus;
DeList={'WssP_MainEcuModeSts'};
WssP_MainEcuModeSts = CreateBus(WssP_MainEcuModeSts, DeList);
clear DeList;

WssP_MainIgnOnOffSts = Simulink.Bus;
DeList={'WssP_MainIgnOnOffSts'};
WssP_MainIgnOnOffSts = CreateBus(WssP_MainIgnOnOffSts, DeList);
clear DeList;

WssP_MainIgnEdgeSts = Simulink.Bus;
DeList={'WssP_MainIgnEdgeSts'};
WssP_MainIgnEdgeSts = CreateBus(WssP_MainIgnEdgeSts, DeList);
clear DeList;

WssP_MainDiagClrSrs = Simulink.Bus;
DeList={'WssP_MainDiagClrSrs'};
WssP_MainDiagClrSrs = CreateBus(WssP_MainDiagClrSrs, DeList);
clear DeList;

WssP_MainWssPlauData = Simulink.Bus;
DeList={
    'WssP_AbsLongTermErr'
    'WssP_AbsLongTerm_Ihb'
    'WssP_FL_PhaseErr'
    'WssP_FL_PhaseErr_Ihb'
    'WssP_FR_PhaseErr'
    'WssP_FR_PhaseErr_Ihb'
    'WssP_RL_PhaseErr'
    'WssP_RL_PhaseErr_Ihb'
    'WssP_RR_PhaseErr'
    'WssP_RR_PhaseErr_Ihb'
    'WssP_FL_ExciterErr'
    'WssP_FL_ExciterErr_Ihb'
    'WssP_FR_ExciterErr'
    'WssP_FR_ExciterErr_Ihb'
    'WssP_RL_ExciterErr'
    'WssP_RL_ExciterErr_Ihb'
    'WssP_RR_ExciterErr'
    'WssP_RR_ExciterErr_Ihb'
    'WssP_FL_AirGapErr'
    'WssP_FL_AirGapErr_Ihb'
    'WssP_FR_AirGapErr'
    'WssP_FR_AirGapErr_Ihb'
    'WssP_RL_AirGapErr'
    'WssP_RL_AirGapErr_Ihb'
    'WssP_RR_AirGapErr'
    'WssP_RR_AirGapErr_Ihb'
    'WssP_FL_PJumpErr'
    'WssP_FL_PJumpErr_Ihb'
    'WssP_FR_PJumpErr'
    'WssP_FR_PJumpErr_Ihb'
    'WssP_RL_PJumpErr'
    'WssP_RL_PJumpErr_Ihb'
    'WssP_RR_PJumpErr'
    'WssP_RR_PJumpErr_Ihb'
    'WssP_FL_MJumpErr'
    'WssP_FL_MJumpErr_Ihb'
    'WssP_FR_MJumpErr'
    'WssP_FR_MJumpErr_Ihb'
    'WssP_RL_MJumpErr'
    'WssP_RL_MJumpErr_Ihb'
    'WssP_RR_MJumpErr'
    'WssP_RR_MJumpErr_Ihb'
    'WssP_FL_WheelTypeSwapErr'
    'WssP_FL_WheelTypeSwapErr_Ihb'
    'WssP_FR_WheelTypeSwapErr'
    'WssP_FR_WheelTypeSwapErr_Ihb'
    'WssP_RL_WheelTypeSwapErr'
    'WssP_RL_WheelTypeSwapErr_Ihb'
    'WssP_RR_WheelTypeSwapErr'
    'WssP_RR_WheelTypeSwapErr_Ihb'
    'WssP_max_speed'
    'WssP_2nd_speed'
    'WssP_3rd_speed'
    'WssP_min_speed'
    };
WssP_MainWssPlauData = CreateBus(WssP_MainWssPlauData, DeList);
clear DeList;

