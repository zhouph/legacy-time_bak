/**
 * @defgroup WssP_Cfg WssP_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssP_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSSP_START_SEC_CONST_UNSPECIFIED
#include "WssP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSSP_STOP_SEC_CONST_UNSPECIFIED
#include "WssP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_START_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSP_STOP_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
#define WSSP_START_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSP_STOP_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_START_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSP_STOP_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_START_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSP_STOP_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
#define WSSP_START_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSP_STOP_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_START_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSP_STOP_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSSP_START_SEC_CODE
#include "WssP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSSP_STOP_SEC_CODE
#include "WssP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
