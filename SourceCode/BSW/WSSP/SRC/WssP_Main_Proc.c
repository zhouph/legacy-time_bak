/**
 * @defgroup WssP_Main WssP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Abc_Types.h"
#include "WssP_Main.h"
#include "WssP_Main_Ifa.h"
#include "WssP_Main_Proc.h"
#include "common.h"
#include "ErP_Types.h"
#include "Erp_FsTestComplete.h"
//#include "ActiveBrakeControlInOutIfTemp.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

static void check_phase_36sec(WssP_Main_HdrBusType *pWssPInfo);
static void arrange_v_FS(WssP_Main_HdrBusType *pWssPInfo);
static void calc_spin(void);
static void airgap_error_FS(WssP_Main_HdrBusType *pWssPInfo);
static void sorting_spd_FS(Saluint16 *spd_ptr);
static void speedjump_error(WssP_Main_HdrBusType *pWssPInfo, Saluint8 mode);
static void speedjump_calc_mode(Saluint8 mode);
static void CompareToAnotherWheel(void);

struct H_STRUCT_FS *HW, HFL, HFR, HRL, HRR;
static Saluint8     fu8sensor_chk_cnt;
static Saluint8     start_flg;
static Saluint8     driving_flg;
Saluint16   max_speed_fs;
Saluint16   max_2nd_speed_fs;
Saluint16   max_3rd_speed_fs;
Saluint16   min_speed_fs;
static Saluint8     fu8duplicate_100g_mode;
static Saluint8     fu8duplicate_70g_mode;
static Saluint8     fu8NonDrvSlipDetectFlag;
static Saluint8     fu1SpinDetectFlg;
static Saluint8     spin_count_over_flg;
static Saluint16   pre_max_speed;
static Salsint16     fs16max_spin_counter;
static Saluint8     fu8airgap_4kph_counter;
static Saluint8     fu8rough_wheel_cnt;
static Saluint16   u16WrngSpeedBackUpErrCnt;
static Saluint8     fu8standstill_flg;
static Saluint16   fu16abs_on_time;
extern Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
static Salsint16     speed_tempW0, speed_tempW1, speed_tempW2;
static CalcTrac_t *CalTrac,CaltFL,CaltFR,CaltRL,CaltRR;

static void arrange_v_FS(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint16 temp_spd[4];
    temp_spd[0]=pWssPInfo->WssP_MainWhlSpdInfo.FlWhlSpd; 
    temp_spd[1]=pWssPInfo->WssP_MainWhlSpdInfo.FrWhlSpd;
    temp_spd[2]=pWssPInfo->WssP_MainWhlSpdInfo.RlWhlSpd;
    temp_spd[3]=pWssPInfo->WssP_MainWhlSpdInfo.RrlWhlSpd;

    sorting_spd_FS(&temp_spd[0]);

    min_speed_fs=temp_spd[0]; max_3rd_speed_fs=temp_spd[1];
    max_2nd_speed_fs=temp_spd[2]; max_speed_fs=temp_spd[3];
    
    pWssPInfo->WssP_MainWssPlauData.WssP_max_speed = max_speed_fs;
    pWssPInfo->WssP_MainWssPlauData.WssP_2nd_speed = max_2nd_speed_fs;
    pWssPInfo->WssP_MainWssPlauData.WssP_3rd_speed = max_3rd_speed_fs;
    pWssPInfo->WssP_MainWssPlauData.WssP_min_speed = min_speed_fs;

    if(max_speed_fs<=U8_F64SPEED_2KPH)
    {
        driving_flg=0;
        start_flg=0;

        HFL.fu1Driving_Set_Flg=0;
        HFR.fu1Driving_Set_Flg=0;
        HRL.fu1Driving_Set_Flg=0;
        HRR.fu1Driving_Set_Flg=0;
    }
    else
    {

        if(min_speed_fs>U8_F64SPEED_2KPH)
        {
            fu8airgap_4kph_counter=0;
        }
        else
        {
            ;
        }

        if(min_speed_fs>=U16_F64SPEED_10KPH)
        {
            start_flg=1;
            if(ece_flg==1)
            {
                driving_flg=1;
            }
            else
            {
                ;
            }
            fs16max_spin_counter=0;
            fu1SpinDetectFlg    =0;
        }
        else
        {
            ;
        }
    }
}
static void sorting_spd_FS(Saluint16 *spd_ptr)
{
    Saluint16 m=0;
    Saluint8 i=0,j=0,k=0;

    for(i=0;i<3;i++)
    {
        k=i;
        m=spd_ptr[i];
        for(j=i+1;j<4;j++){
            if(m>spd_ptr[j])
            {
                m=spd_ptr[j];
                k=j;
            }
            else
            {
                ;
            }   
        }
        spd_ptr[k]=spd_ptr[i];
        spd_ptr[i]=m;
    }
}

static void check_phase_36sec(WssP_Main_HdrBusType *pWssPInfo)
{
    
    if(pWssPInfo->WssP_MainAbsCtrlInfo.AbsActFlg ==1)
    {
        if(fu16abs_on_time>=U16_36SEC_TIME)
        {
            pWssPInfo->WssP_MainWssPlauData.WssP_AbsLongTermErr=ERR_FAILED;
            fu16abs_on_time=0;
        }
        fu16abs_on_time++;
    }
    else
    {
        fu16abs_on_time=0;
    }
    
}

static void check_exciter(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint8 Temp_Exciter_Check_Inhibit;
    Temp_Exciter_Check_Inhibit=0;
    speed_tempW1=(Saluint16)((HW->fsu16SpeedOfResol64/10)*5);
    if(max_speed_fs>=U16_F64SPEED_40KPH)
    {
        speed_tempW0=(Saluint16)((max_speed_fs/10)*3);
    }
    else if(max_speed_fs>=U16_F64SPEED_20KPH)
    {
        speed_tempW0=(Saluint16)((max_speed_fs/10)*2);
    }
    else
    {
        HW->fu16exciter_error_count=0;
        Temp_Exciter_Check_Inhibit=1;
    }
    if(Temp_Exciter_Check_Inhibit==0)
    {
        if(speed_tempW1<=speed_tempW0)
        {
            HW->fu16exciter_error_count++;
            if(HW->fu16exciter_error_count>=U16_120SEC_TIME)
            {
                if(max_speed_fs>=U16_F64SPEED_40KPH)
                {
                    if(fu8sensor_chk_cnt==0)
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_FL_ExciterErr=ERR_FAILED;
                    }
                    else if(fu8sensor_chk_cnt==1)
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_FR_ExciterErr=ERR_FAILED;
                    }
                    else if(fu8sensor_chk_cnt==2)
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_RL_ExciterErr=ERR_FAILED;
                    }
                    else
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_RR_ExciterErr=ERR_FAILED;
                    }
                }
                else
                {
                    if(fu8sensor_chk_cnt==0)
                    {   
                        pWssPInfo->WssP_MainWssPlauData.WssP_FL_ExciterErr=ERR_FAILED;
                    }
                    else if(fu8sensor_chk_cnt==1)
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_FR_ExciterErr=ERR_FAILED;
                    }
                    else if(fu8sensor_chk_cnt==2)
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_RL_ExciterErr=ERR_FAILED;
                    }
                    else
                    {
                        pWssPInfo->WssP_MainWssPlauData.WssP_RR_ExciterErr=ERR_FAILED;
                    }
                }
                HW->fu16exciter_error_count=0;
            }
            else
            {
                ;
            }
        }
        else 
        {
            HW->fu16exciter_error_count=0;
        }
    }
}
static void check_airgap(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint8 ft_u8return=0;

    if(HW->fsu16SpeedOfResol64<=U8_F64SPEED_0KPH25)
    {
        if(max_speed_fs>=U16_F64SPEED_10KPH)
        {
            ++HW->fu16airgap_error_count;
            if(max_3rd_speed_fs>=U16_F64SPEED_10KPH)
            {
                if((max_speed_fs-max_3rd_speed_fs)<U16_F64SPEED_4KPH)
                {
                    if(fu1SpinDetectFlg==0)/*Cycle Time 변경(7ms ->10ms):200->286*/
                    {
                        fu8airgap_4kph_counter++;
                        if(fu8airgap_4kph_counter>U8_155MSEC_TIME)
                        {
                            airgap_error_FS(&WssP_MainBus);
                        }
                        else
                        {
                            ;
                        }
                        ft_u8return=1;
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    ;
                }

                if(ft_u8return==0)
                {
                    fu8airgap_4kph_counter=0;
                    if(HW->fu16airgap_error_count>U16_180SEC_TIME)
                    {
                        airgap_error_FS(&WssP_MainBus);
                    }
                    else
                    {
                        ;
                    }
                    ft_u8return=1;
                }
            }
            else
            {
                ;
            }

            if(ft_u8return==0)
            {
                fu8airgap_4kph_counter=0;
                if((fu1SpinDetectFlg==0)&&(HW->fu16airgap_error_count>=U16_20SEC_TIME))/*Cycle Time 변경(7ms ->10ms):200->286*/
                {
                    airgap_error_FS(&WssP_MainBus);
                }
                else if(HW->fu16airgap_error_count>U16_180SEC_TIME)
                {
                    airgap_error_FS(&WssP_MainBus);
                }
                else
                {
                    ;
                }
                ft_u8return=1;
            }
        }
        else
        {
            ;
        }

        if(ft_u8return==0)
        {
            fu8airgap_4kph_counter=0;
        }
    }
    else
    {
        ;
    }
    
    if(ft_u8return==0)
    {
        HW->fu16airgap_error_count=0;
    }
}

static void check_airgap_10kph(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint16 fu16AirgapCheckTime = 0;

    #if __DRIVE_TYPE==REAR_WHEEL_DRV
    if((HFL.fsu16SpeedOfResol64 <= U8_F64SPEED_0KPH25)&&(HFR.fsu16SpeedOfResol64 <= U8_F64SPEED_0KPH25))
    {
        fu8NonDrvSlipDetectFlag=1;
    }
    #else
    if((HRL.fsu16SpeedOfResol64 <= U8_F64SPEED_0KPH25)&&(HRR.fsu16SpeedOfResol64 <= U8_F64SPEED_0KPH25))
    {
        fu8NonDrvSlipDetectFlag=1;
    }
    #endif    
    fu16AirgapCheckTime = U16_12SEC_TIME;

    if(fu8NonDrvSlipDetectFlag==1)
    {
        fu16AirgapCheckTime = U16_12SEC_TIME;        
    }    
    if(HW->fsu16SpeedOfResol64 <= U8_F64SPEED_0KPH25)
    {
        if(max_3rd_speed_fs <= U8_F64SPEED_0KPH25)
        {
            if((max_2nd_speed_fs > U16_F64SPEED_10KPH) && ((max_speed_fs-max_2nd_speed_fs) < U16_F64SPEED_4KPH))
            {
                if(HW->fu16airgap_error_count >= fu16AirgapCheckTime)
                {
                    airgap_error_FS(&WssP_MainBus);
                }
                else
                {
                    HW->fu16airgap_error_count++;
                }  
            }
            else 
            {
                HW->fu16airgap_error_count=0;
            }
        }
        else if((max_3rd_speed_fs > U16_F64SPEED_10KPH) && ((max_speed_fs-max_3rd_speed_fs) < U16_F64SPEED_4KPH))
        {
            if(HW->fu16airgap_error_count >= fu16AirgapCheckTime)
            {
                airgap_error_FS(&WssP_MainBus);
            }
            else
            {
                HW->fu16airgap_error_count++;
            }
        }
        else
        {
            HW->fu16airgap_error_count=0;
        }
    }
    else
    {
        HW->fu16airgap_error_count=0;
    }
}

static void airgap_error_FS(WssP_Main_HdrBusType *pWssPInfo)
{
    switch(fu8sensor_chk_cnt)
    {
        case 0 :
                pWssPInfo->WssP_MainWssPlauData.WssP_FL_AirGapErr=ERR_FAILED;
                break;
        case 1 :
                pWssPInfo->WssP_MainWssPlauData.WssP_FR_AirGapErr=ERR_FAILED;
                break;
        case 2 :
                pWssPInfo->WssP_MainWssPlauData.WssP_RL_AirGapErr=ERR_FAILED;
                break;
        case 3 :
                pWssPInfo->WssP_MainWssPlauData.WssP_RR_AirGapErr=ERR_FAILED;
                break;
        default : 
                break;
    }
    HW->fu16airgap_error_count=0;
    fu8airgap_4kph_counter=0;
}
static void check_phase_12sec(WssP_Main_HdrBusType *pWssPInfo)
{
    if(pWssPInfo->WssP_MainAbsCtrlInfo.AbsActFlg==1)
    {
        if(HW->fsu16SpeedOfResol64<=U8_F64SPEED_2KPH)
        {
            if(HW->fu16stand_still_count>=U16_12SEC_TIME)
            {
                switch(fu8sensor_chk_cnt)
                {
                    case 0 :
                            pWssPInfo->WssP_MainWssPlauData.WssP_FL_PhaseErr=ERR_FAILED;
                            break;
                    case 1 :
                            pWssPInfo->WssP_MainWssPlauData.WssP_FR_PhaseErr=ERR_FAILED;
                            break;
                    case 2 :
                            pWssPInfo->WssP_MainWssPlauData.WssP_RL_PhaseErr=ERR_FAILED;
                            break;
                    case 3 :
                            pWssPInfo->WssP_MainWssPlauData.WssP_RR_PhaseErr=ERR_FAILED;
                            break;
                    default : 
                            break;
                }
                HW->fu16stand_still_count=0;
            }
            HW->fu16stand_still_count++;
            /*return;*/
        }
        else
        {
            HW->fu16stand_still_count=0;
        }
    }
    else
    {
        HW->fu16stand_still_count=0;
    }
    /*HW->fu16stand_still_count=0;*/
}
static void check_speed_jump(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint8 mode;
        
    /***************** Monitor Speedjump **************************/
    speed_tempW0=(Salsint16)(HW->fsu16SpeedOfResol64 - HW->fs16w_speed_n_1);
    speed_tempW1=(Salsint16)(HW->fsu16SpeedOfResol64 - HW->fs16w_speed2_n_1);
    
    if(HW->fsu16SpeedOfResol64 <=  min_speed_fs) /*Temp Vehicle speed -> min speed*/
    {
        if(abs(speed_tempW1) <= (U16_F64SPEED_17KPH5*SPEED_JUMP_MUL_NO))   /*Loop Time변경 (7-->5ms) 적용(25kph->17.5kph)*/
        {
            speed_tempW1=0;
        }
        else
        {
            ;   
        }
        
        if(abs(speed_tempW0) <= (U16_F64SPEED_17KPH5*SPEED_JUMP_MUL_NO))   /*Loop Time변경 (7-->5ms) 적용(25kph->17.5kph)*/
        {
            speed_tempW0=0;   
        }
        else
        {
            ;   
        }
    }
    else
    {
        ;
    }    
   
    if(speed_tempW0>(U16_F64SPEED_12KPH*SPEED_JUMP_MUL_NO))    /*Loop Time변경 (7-->5ms) 적용(17.5kph->12kph)*/
    {
        HW->fu8plus_70g_delay=5;
        /*=====================================================================
          각 휠의 이전 SCAN 대비 속도차가 70G 이상났을때 Count
        =======================================================================*/
        HW->plus_70g_suspect_cnt++;
    }
    else 
    {
        if(HW->fu8plus_70g_delay>0)
        {
            HW->fu8plus_70g_delay--;
        }
        else if(speed_tempW0<-(U16_F64SPEED_12KPH*SPEED_JUMP_MUL_NO))  /*Loop Time변경 (7-->5ms) 적용(17.5kph->12kph)*/
        {
            HW->time_chk_m100g_flg=1;
        }
        else
        {
            ;
        }
    }
    
    if((speed_tempW1>(U16_F64SPEED_12KPH*SPEED_JUMP_MUL_NO))||(speed_tempW1<-(U16_F64SPEED_17KPH5*SPEED_JUMP_MUL_NO))) /*Loop Time변경 (7-->5ms) 적용(25kph->17.5kph, 17.5kph->12kph)*/
    {
        /*if(init_end_flg==0)
        {
            mode=1;
        }*/
        /*else 
        {*/
            speed_tempW2 = (char16_t)(HW->fsu16SpeedOfResol64 -  min_speed_fs); /*Temp vehicle speed -> min speed*/
            if(abs(speed_tempW2)<U16_F64SPEED_4KPH)
            {
                HW->fu8same_with_vref_cnt++;
            }               
            else
            {
                HW->fu8same_with_vref_cnt=0;
            }

            if(HW->fu8same_with_vref_cnt>5)
            {
                mode=3;
            }
            else if((speed_tempW0<-(U16_F64SPEED_12KPH*SPEED_JUMP_MUL_NO))&&(HW->fu8plus_70g_delay!=0)) /*Loop Time변경 (7-->5ms) 적용(17.5kph->12kph)*/
            {
                if(HW->fsu16SpeedOfResol64<( min_speed_fs-HW->fs16v_diff_with_vref))/*Temp vehicle speed -> min speed*/
                {
                    mode=3;
                }
                else
                {
                    mode=2;
                }
            }
            else 
            {
                if(speed_tempW1>(U16_F64SPEED_12KPH*SPEED_JUMP_MUL_NO))     /*Loop Time변경 (7-->5ms) 적용(17.5kph->12kph)*/
                {
                    /*=====================================================================
                       -. 70G 3회 이상 발생 시 Suspect Flag Set
                       -. 이전 휠속과 현재 휠속과의 차이가 5KPH 이하인 경우가 70ms 이상 지속시
                          Suspect Flag Clear
                    =======================================================================*/
                    if(HW->u1SpeedJumpSusFlg==0)
                    {
                        if(HW->plus_70g_suspect_cnt>=3) 
                        {
                            HW->fs16v_diff_with_vref=(INT)( min_speed_fs - HW->fs16w_speed_n_1); /*Temp vehicle speed -> min speed*/
                            mode=2;
                        }
                        else
                        {
                            mode=3;       
                        }
                    }
                    else
                    {
                        if(abs(speed_tempW0)<U16_F64SPEED_5KPH)
                        {
                            if(HW->plus_70g_suspect_clear_cnt>=10)
                            {
                                HW->plus_70g_suspect_clear_cnt=0;
                                mode=3;    
                            }
                            else
                            {
                                HW->plus_70g_suspect_clear_cnt++; 
                                mode=2;   
                            }
                        }
                        else
                        {
                            HW->plus_70g_suspect_clear_cnt=0;
                            mode=2;   
                        }
                        HW->plus_70g_suspect_cnt=0;
                    }
                }
                else
                {    
                    if(HW->u1SpeedJumpSusFlg==0)
                    {
                        HW->fs16v_diff_with_vref=(INT)( min_speed_fs - HW->fs16w_speed_n_1);/*Temp vehicle speed -> min speed*/
                    }
                    else
                    {
                        ;
                    }
                    mode=2;
                }               
            }
        /*}*/
    }
    else
    {
        mode=3;
    }

    /* Judge Speedjump */
    if(HW->time_chk_m100g_flg==1)
    {
        if(speed_tempW1<-(U16_F64SPEED_17KPH5*SPEED_JUMP_MUL_NO))    /*Loop Time변경 (7-->5ms) 적용(25kph->17.5kph)*/
        {
            HW->fu8minus_100g_time++;
            if(HW->fu8minus_100g_time>=U8_140MSEC_TIME)
            {
                /*=====================================================================
                  고장 검출 시 에러 Count Clear.
                =======================================================================*/
                HW->fu8minus_100g_time=0;
                speedjump_error(&WssP_MainBus,1);
            }
            else
            {
                ;
            }
        }
        else 
        {
            HW->fu8minus_100g_time=0;
            HW->time_chk_m100g_flg=0;
        }
    }
    else
    {
        ;
    }

    /* Rough Road Monitor */
    if(abs(speed_tempW0)>U8_F64SPEED_1KPH5)
    {
        HW->fu8rough_detect_delay_time=(Saluint8)U8_140MSEC_TIME;
    }
    else if (HW->fu8rough_detect_delay_time>0) 
    {
        HW->fu8rough_detect_delay_time--;
    }
    else
    {
        ;
    }

    if(HW->fu8rough_detect_delay_time>0) 
    {
        fu8rough_wheel_cnt++;
    }
    else
    {
        ;
    }

    /*=====================================================================
      고장 검출 시 에러 Count Clear.
    =======================================================================*/
    if(speed_tempW0>(U16_F64SPEED_17KPH5*SPEED_JUMP_MUL_NO)) /*Loop Time변경 (7-->5ms) 적용(25kph->17.5kph)*/
    {
        HW->fu8plus_70g_number++;
        HW->fu8plus_100g_number++;
        if(HW->fu8plus_70g_number>=126)
        {
            HW->fu8plus_70g_number=0;
            speedjump_error(&WssP_MainBus,0);
        }
        else
        {
            ;
        }
        
        if(HW->fu8plus_100g_number>=56)
        {
            HW->fu8plus_100g_number=0;
            speedjump_error(&WssP_MainBus,0);
        }
        else
        {
            ;
        }
    }
    else if(speed_tempW0>(U16_F64SPEED_12KPH*SPEED_JUMP_MUL_NO)) /*Loop Time변경 (7-->5ms) 적용(17.5kph->12kph)*/
    {
        HW->fu8plus_70g_number++;
        if(HW->fu8plus_70g_number>=WSS_JUMP_70G_DETECT_TIME)
        {
            HW->fu8plus_70g_number=0;
            speedjump_error(&WssP_MainBus,0);
        }
        else
        {
            ;
        }

    }
    else if(speed_tempW0<-(U16_F64SPEED_17KPH5*SPEED_JUMP_MUL_NO)) /*Loop Time변경 (7-->5ms) 적용(25kph->17.5kph)*/
    {
        HW->fu8minus_100g_number++;
        if(HW->fu8minus_100g_number>=WSS_JUMP_100G_DETECT_TIME)
        {
            HW->fu8minus_100g_number=0;
            speedjump_error(&WssP_MainBus,1);
        }
        else
        {
            ;
        }
    }
    else
    {
        ;                            
    }
    
    if(fu8standstill_flg==1)
    {
        /*=====================================================================
        REVISION#2 : 정차 판단시에 PLUS 70g COUNT 변수 CLEAR
        =======================================================================*/
        HW->plus_70g_suspect_cnt=0;
        HW->plus_70g_suspect_clear_cnt=0;
        speedjump_calc_mode(1);     
    }
    else
    {
        speedjump_calc_mode(mode);
        HW->fu16StandStillDetCnt=0;
    }

    if(HW->fu8plus_100g_number>=5)
    {
        fu8duplicate_100g_mode++;
    }
    else
        {
        ;
    }
    
    if(HW->fu8plus_70g_number>=20)
    {
        fu8duplicate_70g_mode++;
    }
    else
    {
        ;
     }
    
}
/*=====================================================================
        REVISION: 신규 Wheel Speed Jump 고장검출 추가 -Target : Single Wheel Speed Jump(28g) 발생 시
        =======================================================================*/
       
#define U16_WHEEL_SPEED_JUMP_THRESHOLD  (64*25)   /*25KPH : 70g*/
#define U8_WHEEL_SPEED_JUMP_INCREASE    (1000) /*카운터 증가 계수*/
#define U8_WHEEL_SPEED_JUMP_DECREASE    (10)
#define U16_WHEEL_SPEED_JUMP_ERR_DET_COUNT  (60*U8_WHEEL_SPEED_JUMP_INCREASE) /*연속발생 기준 80회 시점 혹은 간헐적 누적 카운터 1500인 경우*/ 
#define U16_WHEEL_SPEED_JUMP_SUS_DET_COUNT  (U8_WHEEL_SPEED_JUMP_INCREASE*10)   /*연속 발생 기준 3회 이상인 경우 suspect*/
/* #define U16_WHEEL_SPEED_JUMP_SUS_RECOV_COUNT    (U16_WHEEL_SPEED_JUMP_ERR_DET_COUNT/3) */ /*검출 기준 30% 이하인 경우 suspect clear*/
static void check_single_speed_jump(WssP_Main_HdrBusType *pWssPInfo)
{
    switch (fu8sensor_chk_cnt)
    {
        case 0:
            CalTrac=(CalcTrac_t *)&CaltFL;
            Fif_u16TrajectoryCalc(CalTrac,(pWssPInfo->WssP_MainWhlSpdInfo.FlWhlSpd/64));
            break;
        case 1:
            CalTrac=(CalcTrac_t *)&CaltFR;
            Fif_u16TrajectoryCalc(CalTrac,(pWssPInfo->WssP_MainWhlSpdInfo.FrWhlSpd/64));
            break;
        case 2:
            CalTrac=(CalcTrac_t *)&CaltRL;
            Fif_u16TrajectoryCalc(CalTrac,(pWssPInfo->WssP_MainWhlSpdInfo.RlWhlSpd/64));
            break;
        case 3:
            CalTrac=(CalcTrac_t *)&CaltRR;
            Fif_u16TrajectoryCalc(CalTrac,(pWssPInfo->WssP_MainWhlSpdInfo.RrlWhlSpd/64));
            break;
        default :
            break;
    }
    
    HW->fu16WheelSpeedDiff = (uint16_t)abs((INT)(HW->fsu16SpeedOfResol64) - HW->fs16w_speed_n_1);
    
    if(HW->fu16WheelSpeedDiff > U16_WHEEL_SPEED_JUMP_THRESHOLD)
    {
        HW->fu16CheckTimer = U16_3SEC_TIME;
        HW->fu16WheelJumpErrCnt_temp+=U8_WHEEL_SPEED_JUMP_INCREASE;
        HW->fu16WheelJumpErrCnt+=U8_WHEEL_SPEED_JUMP_INCREASE;               
        HW->fu16WheelJumpSusCnt+=U8_WHEEL_SPEED_JUMP_INCREASE;
    }
    else
    {
        if(HW->fu16WheelJumpErrCnt>U8_WHEEL_SPEED_JUMP_DECREASE)
        {
            HW->fu16WheelJumpErrCnt=HW->fu16WheelJumpErrCnt-U8_WHEEL_SPEED_JUMP_DECREASE;
        }
        else
        {
            HW->fu16WheelJumpErrCnt=0;
        }
    }
    
    if(HW->fu16CheckTimer >0)
    {  
        CompareToAnotherWheel();
        HW->fu16CheckTimer--;
    }
   else
   {
        HW->fu16WheelJumpErrCnt_temp=0;
   }
   
   if(HW->fu16WheelJumpErrCnt>=U16_WHEEL_SPEED_JUMP_ERR_DET_COUNT)
   {
       HW->fu16WheelJumpErrCnt=0;
       speedjump_error(&WssP_MainBus,0);
   }
   
   if(HW->fu16WheelJumpErrCnt>=U16_WHEEL_SPEED_JUMP_SUS_DET_COUNT)/*(HW->fu16WheelJumpSusCnt>=U16_WHEEL_SPEED_JUMP_SUS_DET_COUNT)*/
   {
        HW->fu1WheelSpeedJumpSus=1;
   }
   else if(HW->fu16WheelJumpErrCnt==0)
   {
        HW->fu1WheelSpeedJumpSus=0;
   }
   else
   {
        ;
   }
        
    if(fu8standstill_flg==1)
    {
        /*****신규 고장검출 Count Clear 조건 추가*****/
        if(HW->fu16StandStillDetCnt>U16_60SEC_TIME)
        {
            HW->fu16WheelJumpErrCnt=0;
            HW->fu16StandStillDetCnt=0;    
        }
        else
        {
            HW->fu16StandStillDetCnt++;
        }
    }
}

static void CompareToAnotherWheel(void)
{
    switch (fu8sensor_chk_cnt)
    {
        case 0:
            if(CaltRL.u1UnstableSigFlg==1)
            {
                if(HW->fu16WheelJumpErrCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpErrCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpErrCnt=0;
                }
                if(HW->fu16WheelJumpSusCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpSusCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpSusCnt=0;
                }
                HW->TestWSSNormalCount++;
                HW->fu16WheelJumpErrCnt_temp=0;
            }
            break;
        case 1:
            if(CaltRR.u1UnstableSigFlg==1)
            {
                if(HW->fu16WheelJumpErrCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpErrCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpErrCnt=0;
                }
                if(HW->fu16WheelJumpSusCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpSusCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpSusCnt=0;
                }
                HW->TestWSSNormalCount++;
                HW->fu16WheelJumpErrCnt_temp=0;
            }
            break;
        case 2:
            if(CaltFL.u1UnstableSigFlg==1)
            {
                if(HW->fu16WheelJumpErrCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpErrCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpErrCnt=0;
                }
                if(HW->fu16WheelJumpSusCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpSusCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpSusCnt=0;
                }
                HW->TestWSSNormalCount++;
                HW->fu16WheelJumpErrCnt_temp=0;
            }
            break;
        case 3:
            if(CaltFR.u1UnstableSigFlg==1)
            {
                if(HW->fu16WheelJumpErrCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpErrCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpErrCnt=0;
                }
                if(HW->fu16WheelJumpSusCnt>=HW->fu16WheelJumpErrCnt_temp)
                {
                    HW->fu16WheelJumpSusCnt-=HW->fu16WheelJumpErrCnt_temp;
                }
                else
                {
                    HW->fu16WheelJumpSusCnt=0;
                }
                HW->TestWSSNormalCount++;
                HW->fu16WheelJumpErrCnt_temp=0;
            }
            break;
        default :
            break;
     }
}

void Fif_u16TrajectoryCalc(CalcTrac_t *CalcTrac,uint16_t NewSig)
{
    Saluint8 Count=0;
    Saluint32 TempSumBuffer=0;
    Saluint16 TempSumCalc=0,TempDiffCalc=0;

    /* Old Data Throw */
    if(CalcTrac->u8SigSavedBufferNo<10)
    {
    CalcTrac->u8SigSavedBufferNo++;	
    }

    CalcTrac->u16SigDataBuff[0]=CalcTrac->u16SigDataBuff[1];
    CalcTrac->u16SigDataBuff[1]=CalcTrac->u16SigDataBuff[2];
    CalcTrac->u16SigDataBuff[2]=CalcTrac->u16SigDataBuff[3];
    CalcTrac->u16SigDataBuff[3]=CalcTrac->u16SigDataBuff[4];
    CalcTrac->u16SigDataBuff[4]=CalcTrac->u16SigDataBuff[5];
    CalcTrac->u16SigDataBuff[5]=CalcTrac->u16SigDataBuff[6];
    CalcTrac->u16SigDataBuff[6]=CalcTrac->u16SigDataBuff[7];
    CalcTrac->u16SigDataBuff[7]=CalcTrac->u16SigDataBuff[8];
    CalcTrac->u16SigDataBuff[8]=CalcTrac->u16SigDataBuff[9];
    CalcTrac->u16SigDataBuff[9]=(NewSig);

    CalcTrac->u16SigMaxValue=CalcTrac->u16SigDataBuff[0];
    CalcTrac->u16SigMinValue=CalcTrac->u16SigDataBuff[0];
    TempSumBuffer=0;

    /* Check Min/Max Data */
    for(Count=0; Count<10; Count++)
    {
        if(CalcTrac->u16SigMaxValue>CalcTrac->u16SigDataBuff[Count])
        {
            CalcTrac->u16SigMaxValue=CalcTrac->u16SigDataBuff[Count];
        }
        if(CalcTrac->u16SigMinValue<CalcTrac->u16SigDataBuff[Count])
        {
            CalcTrac->u16SigMinValue=CalcTrac->u16SigDataBuff[Count];
        }
        if(Count>0)
        {
            TempSumCalc=(uint16_t)(abs((CalcTrac->u16SigDataBuff[Count])-(CalcTrac->u16SigDataBuff[Count-1])));
            TempSumBuffer+=(uint32_t)TempSumCalc;    
        }
    }

    if(CalcTrac->u8SigSavedBufferNo>=10)
    {
        CalcTrac->u32SigDiffSumValue=TempSumBuffer;
        CalcTrac->u16SigStraightValue=(NewSig)-CalcTrac->u16SigDataBuff[0];
        if(CalcTrac->u16SigStraightValue>0)
        {
            CalcTrac->u16TrjRatio=(uint16_t)(((CalcTrac->u32SigDiffSumValue)/((uint32_t)(CalcTrac->u16SigStraightValue)))*100);
        }
        else
        {
            CalcTrac->u16TrjRatio=0;
        }
        TempDiffCalc=(uint16_t)(abs((CalcTrac->u16SigMaxValue)-(CalcTrac->u16SigMinValue)));
        CalcTrac->u16SigDiffValue= TempDiffCalc; 
    }
    else 
    {
        CalcTrac->u32SigDiffSumValue=0;
        CalcTrac->u16SigDiffValue=0;
        CalcTrac->u16SigStraightValue=0;
        CalcTrac->u16TrjRatio=0;      
    }

    if((CalcTrac->u16TrjRatio>1000)||(CalcTrac->u16SigDiffValue>7))
    {
        CalcTrac->u1UnstableSigFlg=1;
    }
    else
    {
        CalcTrac->u1UnstableSigFlg=0;
    }
}

static void frequency_error(WssP_Main_HdrBusType *pWssPInfo)
{
    if(HW->fsu16SpeedOfResol64>U16_F64SPEED_512KPH) 
    {
        HW->fu8frequency_error_count++;
        if(HW->fu8frequency_error_count>U8_100MSEC_TIME)
        {
            switch(fu8sensor_chk_cnt)
            {
                case 0 :
                        pWssPInfo->WssP_MainWssPlauData.WssP_FL_PhaseErr=ERR_FAILED;
                        break;
                case 1 :
                        pWssPInfo->WssP_MainWssPlauData.WssP_FR_PhaseErr=ERR_FAILED;
                        break;
                case 2 :
                        pWssPInfo->WssP_MainWssPlauData.WssP_RL_PhaseErr=ERR_FAILED;
                        break;
                case 3 :
                        pWssPInfo->WssP_MainWssPlauData.WssP_RR_PhaseErr=ERR_FAILED;
                        break;
                default : 
                        break;
            }
            /*=====================================================================
            REVISION#3 : 고장 검출 시 에러 Count Clear.
            =======================================================================*/ 
            HW->fu8frequency_error_count=0; 
        }           
    } 
    else
    {
        if(HW->fu8frequency_error_count>0)   
        {
            HW->fu8frequency_error_count--;    
        }
        else
        {
            ;   
        }
    }
}

static void calc_spin(void)
{
    Salsint16 max_wh_accel;
    
    max_wh_accel=(Salsint16)(max_speed_fs-pre_max_speed);
    
    if(max_speed_fs>=U16_F64SPEED_10KPH)
    {
        if((max_wh_accel<U16_F64SPEED_20KPH)&&(spin_count_over_flg==1))
        {
            fs16max_spin_counter+=max_wh_accel;
        }
        else
        {
            ;
        }
        spin_count_over_flg=0;
    }
    #if __BTC
    else if(fu1BTCSOn==0)
    #else
    else
    #endif
    {
        if(max_speed_fs<U16_F64SPEED_5KPH)
        {
            if(fu8standstill_flg==1) 
            {
                fs16max_spin_counter=0;
            }
            else
            {
            	;
            }
            
            if(pre_max_speed>U8_F64SPEED_2KPH)
            {
                spin_count_over_flg=1;
            }
            else
            {
                ;
            }
        }
        else if(spin_count_over_flg==1)
        {
            if(max_wh_accel!=0)
            {
                fs16max_spin_counter+=max_wh_accel;
            }
            else if(fs16max_spin_counter>0)
            {
                fs16max_spin_counter-=8;
            }
            else
            {
                ;
            }   
            
            if(fs16max_spin_counter<0)
            {
                fs16max_spin_counter=0;
            }
            else
            {
                ;
            }
            }
        else
        {
            ;            
        }
    }
    #if __BTC
    else
    {
        ;
    }
    #endif
    pre_max_speed = max_speed_fs;
    
    if(fs16max_spin_counter>=286) /* 7ms loop : 200, 10ms loop : 286 */
    {
        fu1SpinDetectFlg=1;
    }
    else if(fs16max_spin_counter==0)
    {
        fu1SpinDetectFlg=0;
    }
    else
    {
        ;
    }
}

static void FW_vWssInvalidSpeedCheck(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint16 DiffSpeed;
    Saluint16 LowLimitSpd,RefLowLimitSpd;
    Saluint8 WrongSpdErr=0,NoSpdErr=0;
            
           
    /* 1. 3바퀴의 속도 차이가 없는 상태에서 해당 바퀴의 속도가 70% 이하인 경우 고장 검출 - Invalid Low Short Term 고장 검출. */
    DiffSpeed=max_speed_fs-max_3rd_speed_fs;
    RefLowLimitSpd = (max_speed_fs*_ShortTermFaultRefPcntDiff) / 100;
    LowLimitSpd = (max_speed_fs*_ShortTermFaultFaultPcntDiff) / 100;
    if((DiffSpeed<(_ShortTermFaultRefSpdDiff*64))&&(max_3rd_speed_fs>RefLowLimitSpd)&&(max_speed_fs>(_ShortTermFaultEnableMaxSpd*64)))
    {   
        if(HW->fsu16SpeedOfResol64<LowLimitSpd)
        {
            HW->u16WrngSpeedErrCnt++;     /* 20sec */
        }
        else
        {
            HW->u16WrngSpeedErrCnt=0;
        }
    }
    else
    {
        HW->u16WrngSpeedErrCnt=0;
    }
            
     /* 2. 3바퀴의 속도 차이가 없는 상태에서 해당 바퀴의 속도가 130% 이상인 경우 고장 검출 - Invalid High Short Term 고장 검출. */
    DiffSpeed=max_2nd_speed_fs-min_speed_fs;
    RefLowLimitSpd = (min_speed_fs*_HighShortTermFaultRefPcntDiff) / 100;
    LowLimitSpd = (min_speed_fs*_HighShortTermFaultFaultPcntDiff) / 100;
    if((DiffSpeed<(_ShortTermFaultRefSpdDiff*64))&&(max_2nd_speed_fs<RefLowLimitSpd)&&(min_speed_fs>(_ShortTermFaultEnableMaxSpd*64)))
    {   
        if(HW->fsu16SpeedOfResol64>LowLimitSpd)
        {
            HW->u16WrngHighSpeedErrCnt++;     /* 20sec */
        }
        else
        {
            HW->u16WrngHighSpeedErrCnt=0;
        }
    }
    else
    {
        HW->u16WrngHighSpeedErrCnt=0;
    }
            
    /* 3. 2바퀴의 속도 차이가 없는 상태에서 해당 바퀴의 속도가 50% 이하인 경우 고장 검출 - Long Term 고장 검출 */
    DiffSpeed=max_speed_fs-max_2nd_speed_fs;
    RefLowLimitSpd = (max_speed_fs*_LongTermFaultRefPcntDiff) / 100;
    LowLimitSpd = (max_speed_fs*_LongTermFaultFaultPcntDiff) / 100;
    if((DiffSpeed<(_LongTermFaultRefSpdDiff*64))&&(max_2nd_speed_fs>RefLowLimitSpd)&&(max_speed_fs>(_LongTermFaultEnableMaxSpd*64)))
    {
        if(HW->fsu16SpeedOfResol64<LowLimitSpd)
        {
            HW->u16WrngSpeedLongTermErrCnt++;        /* 1min */
        }
        else
        {
            HW->u16WrngSpeedLongTermErrCnt=0;
        }
    } 
    else
    {
        HW->u16WrngSpeedLongTermErrCnt=0;
    }   
    
    if((HW->u16WrngSpeedErrCnt>(100*_ShortTermFaultFaultDetTime))
        ||(HW->u16WrngHighSpeedErrCnt>(100*_ShortTermFaultFaultDetTime))
        ||(HW->u16WrngSpeedLongTermErrCnt>(100*_LongTermFaultFaultDetTime)))
    {
        /* Air-Gap 고장과 중복되므로 고장 시 해당 Wheel Speed 에 의하여 DTC 결정. */
        if(HW->fsu16SpeedOfResol64>(2*64))
        {
            WrongSpdErr=1;
        }
        else
        {
            NoSpdErr=1;
        }
        HW->u16WrngSpeedErrCnt=0;
        HW->u16WrngSpeedLongTermErrCnt=0;
        HW->u16WrngHighSpeedErrCnt=0;
    }
   
    if(NoSpdErr==1)
    {
        switch(fu8sensor_chk_cnt)
        {
            case 0 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_FL_AirGapErr=ERR_FAILED;
                    break;
            case 1 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_FR_AirGapErr=ERR_FAILED;
                    break;
            case 2 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_RL_AirGapErr=ERR_FAILED;
                    break;
            case 3 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_RR_AirGapErr=ERR_FAILED;
                    break;
            default : 
                    break;
        }
    }
            
    if(WrongSpdErr==1)
    {
        switch(fu8sensor_chk_cnt)
        {
            case 0:
                pWssPInfo->WssP_MainWssPlauData.WssP_FL_ExciterErr=ERR_FAILED;
                break;
            case 1:
                pWssPInfo->WssP_MainWssPlauData.WssP_FR_ExciterErr=ERR_FAILED;
                break;
            case 2:    
                pWssPInfo->WssP_MainWssPlauData.WssP_RL_ExciterErr=ERR_FAILED;
                break;
            case 3:    
                pWssPInfo->WssP_MainWssPlauData.WssP_RR_ExciterErr=ERR_FAILED;
                break;
            default :
                break;
        }
    }
    
    if((HW->WheelErrorDetFlg==1))
    {
        HW->u16WrngSpeedErrCnt=0;
        HW->u16WrngSpeedLongTermErrCnt=0;
        u16WrngSpeedBackUpErrCnt=0;
    }
}

static void FW_vWssSwapCheck(void)
{
    #if 0
    Saluint8 WrongSnsr=0;

    if(HW->u8SensorType==WSS_SMART)
    {
        if(HW->u8SWSSDirection==SAWSS_INVALID_PULSE)/*고장검출 속도 범위 제한 해제*/
        {
            HW->fu16WrongSenSigCnt++;
            if(HW->fu16WrongSenSigCnt>200)
            {
                WrongSnsr=1;
                HW->fu16WrongSenSigCnt=0;
            }
        }
        else
        {
            HW->fu16WrongSenSigCnt=0;
        }
    }
    else if(HW->u8SensorType==WSS_ACTIVE)
    {
        if((HW->fsu16SpeedOfResol64>U16_F64SPEED_5KPH)&&(HW->fsu16SpeedOfResol64<U16_F64SPEED_60KPH))/*고장검출 범위 확장 30->60kph 이내*/
        {
            if((HW->u16ToneWheelDeviation<50)||(HW->u16ToneWheelDeviation>150))
            {
                HW->fu16WrongSenSigCnt++;
                if(HW->fu16WrongSenSigCnt>200)
                {
                    WrongSnsr=1;
                    HW->fu16WrongSenSigCnt=0;
                }
            }
            else
            {
                HW->fu16WrongSenSigCnt=0;
            }
        }
        else
        {
            HW->fu16WrongSenSigCnt=0;
        }
    }
    else
    {
        HW->fu16WrongSenSigCnt=0;
    }
    
    if(WrongSnsr==1)
    {
        switch(fu8sensor_chk_cnt)
        {
            case 0:
                WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr=ERR_FAILED;                
                break;

            case 1:
                WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr=ERR_FAILED;                
                break;

            case 2:
                WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr=ERR_FAILED;               
                break;

            case 3:
                WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr=ERR_FAILED;               
                break;

            default:
                break;
        }
    }

    if(HW->WheelErrorDetFlg==1)
    {
        HW->fu16WrongSenSigCnt=0;
    }
    #endif
}

static void FW_vWssTestCompleteDet(void)
{
    /* UINT fu16DiffSpdAvg; */
    Saluint16 fu16Avr2nd3drSpeed;
    Saluint16 fu16NormalSpdLowThre;
    Saluint16 fu16NormalSpdHighThre; 

    fu16Avr2nd3drSpeed=(max_2nd_speed_fs+max_3rd_speed_fs)/2;
    fu16NormalSpdLowThre=(fu16Avr2nd3drSpeed*6)/10;
    fu16NormalSpdHighThre=(fu16Avr2nd3drSpeed*14)/10;
    
    //if((init_end_flg==1)&&(fu16Avr2nd3drSpeed>=U8_F64SPEED_2KPH)) TO-DO
    if(fu16Avr2nd3drSpeed>=U8_F64SPEED_2KPH)
    {
        /* if((fu16VehicleSpeed*4) > (fu16DiffSpdAvg*10)) */
        if((fu16NormalSpdLowThre<HW->fsu16SpeedOfResol64)&&(HW->fsu16SpeedOfResol64<fu16NormalSpdHighThre))
        {
            
            if(HW->fu16WheelSpeedDiff < U8_F64SPEED_2KPH)   
            {
                HW->fu16WssTestCompleteCnt++;
            }
            else
            {
                HW->fu16WssTestCompleteCnt=0;    
            }
                
            if(HW->fu16WssTestCompleteCnt > U16_5SEC_TIME)
            {
                HW->fu1WheelSpeedJumpSus=0;
                HW->fu16WheelJumpSusCnt=0;
                HW->fu16WssTestCompleteCnt=0;
                switch(fu8sensor_chk_cnt)
                {
                    case 0:
                    feu1FlAirgapTestFlg=feu1FlPhaseTestFlg=feu1FlMjumpTestFlg=feu1FlPjumpTestFlg=feu1FlExciterLFlg=feu1FlExciterHFlg=1;     
                    feu1FlHwOpenTestFlg=feu1FlHwShortTestFlg=feu1FlLeakageTestFlg=feu1FlHwOverTempTestFlg=feu1FlWireTestFlg=1;
                    break;

                    case 1:
                    feu1FrAirgapTestFlg=feu1FrPhaseTestFlg=feu1FrMjumpTestFlg=feu1FrPjumpTestFlg=feu1FrExciterLFlg=feu1FrExciterHFlg=1;
                    feu1FrHwOpenTestFlg=feu1FrHwShortTestFlg=feu1FrLeakageTestFlg=feu1FrHwOverTempTestFlg=feu1FrWireTestFlg=1;
                    break;

                    case 2:
                    feu1RlAirgapTestFlg=feu1RlPhaseTestFlg=feu1RlMjumpTestFlg=feu1RlPjumpTestFlg=feu1RlExciterLFlg=feu1RlExciterHFlg=1;                              
                    feu1RlHwOpenTestFlg =feu1RlHwShortTestFlg=feu1RlLeakageTestFlg=feu1RlHwOverTempTestFlg=feu1RlWireTestFlg=1;
                    break;
            
                    case 3:
                    feu1RrAirgapTestFlg=feu1RrPhaseTestFlg=feu1RrMjumpTestFlg=feu1RrPjumpTestFlg=feu1RrExciterLFlg=feu1RrExciterHFlg=1;
                    feu1RrHwOpenTestFlg=feu1RrHwShortTestFlg=feu1RrLeakageTestFlg=feu1RrHwOverTempTestFlg=feu1RrWireTestFlg=1;
                    break;

                    default:
                    break;
                }
            }
            else if(HW->fu16WssTestCompleteCnt > U16_3SEC_TIME)
            {
                HW->fu1Driving_Set_Flg=1;   
            }
            else
            {
                ;
            }
        }
        else
        {
            HW->fu16WssTestCompleteCnt=0;
        }
    }
    else
    {
        ;
    }
        
}

static void speedjump_error(WssP_Main_HdrBusType *pWssPInfo, Saluint8 mode)
{
    if(mode==0)
    {
        switch(fu8sensor_chk_cnt)
        {
            case 0 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_FL_PJumpErr=ERR_FAILED;
                    break;
            case 1 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_FR_PJumpErr=ERR_FAILED;
                    break;
            case 2 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_RL_PJumpErr=ERR_FAILED;
                    break;
            case 3 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_RR_PJumpErr=ERR_FAILED;
                    break;
            default : 
                    break;
        } 
    }
    else if(mode==1)
    {
        switch(fu8sensor_chk_cnt)
        {
            case 0 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_FL_MJumpErr=ERR_FAILED;
                    break;
            case 1 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_FR_MJumpErr=ERR_FAILED;
                    break;
            case 2 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_RL_MJumpErr=ERR_FAILED;
                    break;
            case 3 :
                    pWssPInfo->WssP_MainWssPlauData.WssP_RR_MJumpErr=ERR_FAILED;
                    break;
            default : 
                    break;
        }
    }
    else
    {
        ;
    }
}

static void FW_vWssBackUpCheck(WssP_Main_HdrBusType *pWssPInfo)
{
    /* static uint16_t u16WrngSpeedBackUpErrCnt; */
    Saluint16 LowLimitSpd; 
    LowLimitSpd = (max_speed_fs*_BackUpFaultFaultPcntDiff) / 100;
    if(max_speed_fs>(_BackUpFaultEnableMaxSpd*64))
    {    
        if(min_speed_fs<LowLimitSpd)
        {
            u16WrngSpeedBackUpErrCnt++;      /* 2min */
        }
        else
        {
           u16WrngSpeedBackUpErrCnt=0;
        }
    }
    else
    {
       /* u16WrngSpeedBackUpErrCnt=0; */
    }
    
    if(u16WrngSpeedBackUpErrCnt>(U16_120SEC_TIME))
    {
        switch(fu8sensor_chk_cnt)
        {
            case 0:
                pWssPInfo->WssP_MainWssPlauData.WssP_FL_ExciterErr=ERR_FAILED;
                break;
            case 1:
                pWssPInfo->WssP_MainWssPlauData.WssP_FR_ExciterErr=ERR_FAILED;
                break;
            case 2:    
                pWssPInfo->WssP_MainWssPlauData.WssP_RL_ExciterErr=ERR_FAILED;
                break;
            case 3:    
                pWssPInfo->WssP_MainWssPlauData.WssP_RR_ExciterErr=ERR_FAILED;
                break;
            default :
                break;
        }
        u16WrngSpeedBackUpErrCnt=0;
        
    }
}

static void speedjump_calc_mode(Saluint8 mode)
{
    if(mode==1)
    {
        HW->fu8plus_100g_number=0;
        HW->fu8plus_70g_number=0;
        HW->fu8minus_100g_number=0;
        HW->fs16v_diff_with_vref=0;
        HW->fu8same_with_vref_cnt=0;
        HW->u1SpeedJumpSusFlg=0;
        HW->fs16w_speed2_n_1=(Salsint16)HW->fsu16SpeedOfResol64;
    }
    else if(mode==2)
    {
        HW->fs16w_speed2_n_1=(Salsint16)(min_speed_fs-HW->fs16v_diff_with_vref); /*temp vehicle speed -> min speed*/
        HW->u1SpeedJumpSusFlg=1;
        /*return;*/
    }
    else
    {
        HW->fs16v_diff_with_vref=0;
        HW->fu8same_with_vref_cnt=0;
        HW->u1SpeedJumpSusFlg=0;
        HW->fs16w_speed2_n_1=(Salsint16)HW->fsu16SpeedOfResol64;
    }
}

void WssP_Main_Proc(WssP_Main_HdrBusType *pWssPInfo)
{
    Saluint16 standstill_time;

    /* Process */
    arrange_v_FS(&WssP_MainBus);
    calc_spin();
    check_phase_36sec(&WssP_MainBus);
    /*temporally determine standstill*/
    if( (max_speed_fs<U8_F64SPEED_0KPH25)&&(min_speed_fs<=U8_F64SPEED_2KPH25) ) /*temp vehicle speed -> min speed*/
    {
        if(standstill_time < U16_2SEC_TIME)
        {
            standstill_time++;
        }
        else
        {
            fu8standstill_flg=1;
        }
    }
    else
    {
        standstill_time=0;
        fu8standstill_flg=0;
    }
    
    for(fu8sensor_chk_cnt=0;fu8sensor_chk_cnt<4;fu8sensor_chk_cnt++)
    {

        if(fu8sensor_chk_cnt==0)
        {
            HW=(struct H_STRUCT_FS *)&HFL;
            HW->fsu16SpeedOfResol64 = pWssPInfo->WssP_MainWhlSpdInfo.FlWhlSpd;
        }
        else if(fu8sensor_chk_cnt==1)
        {
            HW=(struct H_STRUCT_FS *)&HFR;
            HW->fsu16SpeedOfResol64 = pWssPInfo->WssP_MainWhlSpdInfo.FrWhlSpd;
        }
        else if(fu8sensor_chk_cnt==2)
        {
            HW=(struct H_STRUCT_FS *)&HRL;
            HW->fsu16SpeedOfResol64 = pWssPInfo->WssP_MainWhlSpdInfo.RlWhlSpd;
        }
        else
        {
            HW=(struct H_STRUCT_FS *)&HRR;
            HW->fsu16SpeedOfResol64 = pWssPInfo->WssP_MainWhlSpdInfo.RrlWhlSpd;
        }

        if(HW->WheelErrorDetFlg==0) 
        {
            check_exciter(&WssP_MainBus);

            if(start_flg==0)
            {
                check_airgap(&WssP_MainBus);
            }
            else
            {
                check_airgap_10kph(&WssP_MainBus);
            }                
                
            check_phase_12sec(&WssP_MainBus);
            check_speed_jump(&WssP_MainBus);
            check_single_speed_jump(&WssP_MainBus);
            frequency_error(&WssP_MainBus);
        }
        else
        {
            fu8duplicate_100g_mode++;
            fu8duplicate_70g_mode++;
            
            if(HW->WheelErrorDetFlg==1)
            {
                HW->suspect_flg=1;
            }
            else
            {
                ;
            }
            HW->fs16w_speed2_n_1=(Salsint16)HW->fsu16SpeedOfResol64;
            HW->fu8plus_100g_number=0;
            HW->fu8minus_100g_number=0;
            HW->fu8plus_70g_number=0;
            HW->fu8minus_100g_time=0;
        }

        if(fu8sensor_chk_cnt==3)
        {
            if((suspect_flg_fl==1)&&(suspect_flg_fr==1)&&(suspect_flg_rl==1)&&(suspect_flg_rr==1))
            {
                suspect_flg_fl=suspect_flg_fr=suspect_flg_rl=suspect_flg_rr=0;
            }
            else
            {
                ;
            }

            if(fu8duplicate_100g_mode>1)
            {
                if(fu8plus_100g_number_fl>=DUAL_WSS_JUMP_100G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_FL_PJumpErr=ERR_FAILED;
                    fu8plus_100g_number_fl=0;
                }
                if(fu8plus_100g_number_fr>=DUAL_WSS_JUMP_100G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_FR_PJumpErr=ERR_FAILED;
                    fu8plus_100g_number_fr=0;
                }
                if(fu8plus_100g_number_rl>=DUAL_WSS_JUMP_100G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_RL_PJumpErr=ERR_FAILED;
                    fu8plus_100g_number_rl=0;
                }
                if(fu8plus_100g_number_rr>=DUAL_WSS_JUMP_100G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_RR_PJumpErr=ERR_FAILED;
                    fu8plus_100g_number_rr=0;
                }
            }
            else if(fu8duplicate_70g_mode>1)
            {
                if(fu8plus_70g_number_fl>=DUAL_WSS_JUMP_70G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_FL_PJumpErr=ERR_FAILED;
                    fu8plus_70g_number_fl=0;
                }
                if(fu8plus_70g_number_fr>=DUAL_WSS_JUMP_70G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_FR_PJumpErr=ERR_FAILED;
                    fu8plus_70g_number_fr=0;
                }
                if(fu8plus_70g_number_rl>=DUAL_WSS_JUMP_70G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_RL_PJumpErr=ERR_FAILED;
                    fu8plus_70g_number_rl=0;
                }
                if(fu8plus_70g_number_rr>=DUAL_WSS_JUMP_70G_DETECT_TIME)
                {
                    WssP_MainWssPlauData.WssP_RR_PJumpErr=ERR_FAILED;
                    fu8plus_70g_number_rr=0;
                }
            }
            else
            {
                ;
            }
            fu8duplicate_100g_mode=0;
            fu8duplicate_70g_mode=0;
        }
        else
        {
            ;
        }
        
        HW->fs16w_speed_n_1=(Salsint16)HW->fsu16SpeedOfResol64;

        FW_vWssInvalidSpeedCheck(&WssP_MainBus);
        //FW_vWssSwapCheck(); To-Do
        FW_vWssTestCompleteDet();
    }  

    if((WheelErrorDetFlg_fl==0)&&(WheelErrorDetFlg_fr==0)&&(WheelErrorDetFlg_rl==0)&&(WheelErrorDetFlg_rr==0)) 
    {
        FW_vWssBackUpCheck(&WssP_MainBus);
    }

    if((HFL.fu1Driving_Set_Flg>0)&&(HFR.fu1Driving_Set_Flg>0)&&(HRL.fu1Driving_Set_Flg>0)&&(HRR.fu1Driving_Set_Flg>0))
    {
        if(ece_flg==1)
        {
            driving_flg=1;
        }
    }
    pre_max_speed = max_speed_fs;
    /*inhibit*/
    if(pWssPInfo->WssP_MainWssMonData.WssM_Inhibit_FL==1)
    {
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_PhaseErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_ExciterErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_AirGapErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_PJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_MJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr = ERR_INHIBIT;       
    }

    if(pWssPInfo->WssP_MainWssMonData.WssM_Inhibit_FR==1)
    {
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_PhaseErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_ExciterErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_AirGapErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_PJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_MJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr = ERR_INHIBIT;       
    }

    if(pWssPInfo->WssP_MainWssMonData.WssM_Inhibit_RL==1)
    {
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_PhaseErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_ExciterErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_AirGapErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_PJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_MJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr = ERR_INHIBIT;       
    }

    if(pWssPInfo->WssP_MainWssMonData.WssM_Inhibit_RR==1)
    {
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_PhaseErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_ExciterErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_AirGapErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_PJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_MJumpErr = ERR_INHIBIT;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr = ERR_INHIBIT;       
    }
    /*Clear via diag*/
    if(pWssPInfo->WssP_MainDiagClrSrs==1)
    {
        pWssPInfo->WssP_MainWssPlauData.WssP_AbsLongTermErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_PhaseErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_PhaseErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_PhaseErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_PhaseErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_ExciterErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_ExciterErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_ExciterErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_ExciterErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_AirGapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_AirGapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_AirGapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_AirGapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_PJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_PJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_PJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_PJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_MJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_MJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_MJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_MJumpErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr = ERR_NONE;
        pWssPInfo->WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr = ERR_NONE;

        fu8duplicate_100g_mode=0;
        fu8duplicate_70g_mode=0;
        u16WrngSpeedBackUpErrCnt=0;
        fs16max_spin_counter=0;
        fu8snsr_hw_open_err_count_fl=fu8snsr_hw_open_err_count_fr=fu8snsr_hw_open_err_count_rl=fu8snsr_hw_open_err_count_rr=0;
        fu8snsr_hw_short_err_count_fl=fu8snsr_hw_short_err_count_fr=fu8snsr_hw_short_err_count_rl=fu8snsr_hw_short_err_count_rr=0;
        suspect_flg_fl=suspect_flg_fr=suspect_flg_rl=suspect_flg_rr=0;
        fs16w_speed2_n_1_fl=fs16w_speed2_n_1_fr=fs16w_speed2_n_1_rl=fs16w_speed2_n_1_rr=0;
        fs16w_speed_n_1_fl=fs16w_speed2_n_1_fr=fs16w_speed2_n_1_rl=fs16w_speed2_n_1_rr=0;
        fu8plus_100g_number_fl=fu8plus_100g_number_fr=fu8plus_100g_number_rl=fu8plus_100g_number_rr=0;
        fu8minus_100g_number_fl=fu8minus_100g_number_fr=fu8minus_100g_number_rl=fu8minus_100g_number_rr=0;
        fu8plus_70g_number_fl=fu8plus_70g_number_fr=fu8plus_70g_number_rl=fu8plus_70g_number_rr=0;
        fu8minus_100g_time_fl=fu8minus_100g_time_fr=fu8minus_100g_time_rl=fu8minus_100g_time_rr=0;
        fu8plus_70g_delay_fl=fu8plus_70g_delay_fr=fu8plus_70g_delay_rl=fu8plus_70g_delay_rr=0;
        plus_70g_suspect_cnt_fl=plus_70g_suspect_cnt_fr=plus_70g_suspect_cnt_rl=plus_70g_suspect_cnt_rr=0;
        time_chk_m100g_flg_fl=time_chk_m100g_flg_fr=time_chk_m100g_flg_rl=time_chk_m100g_flg_rr=0;
        fu8same_with_vref_cnt_fl=fu8same_with_vref_cnt_fr=fu8same_with_vref_cnt_rr=fu8same_with_vref_cnt_rl=0;
        fs16v_diff_with_vref_fl=fs16v_diff_with_vref_fr=fs16v_diff_with_vref_rl=fs16v_diff_with_vref_rr=0;
        fu1SpeedJumpSusFlg_fl=fu1SpeedJumpSusFlg_fr=fu1SpeedJumpSusFlg_rl=fu1SpeedJumpSusFlg_rr=0;
        plus_70g_suspect_clear_cnt_fl=plus_70g_suspect_clear_cnt_fr=plus_70g_suspect_clear_cnt_rl=plus_70g_suspect_clear_cnt_rr=0;
        fu16airgap_error_count_fl=fu16airgap_error_count_fr=fu16airgap_error_count_rl=fu16airgap_error_count_rr=0;
        fu8airgap_4kph_counter=0;
        fu16exciter_error_count_fl=fu16exciter_error_count_fr=fu16exciter_error_count_rl=fu16exciter_error_count_rr=0;
        fu16stand_still_count_fl=fu16stand_still_count_fr=fu16stand_still_count_rl=fu16stand_still_count_rr=0;
        fu16abs_on_time=0;
        fu8frequency_error_count_fl=fu8frequency_error_count_fr=fu8frequency_error_count_rl=fu8frequency_error_count_rr=0;
        fu16ABSOn_count_fl=fu16ABSOn_count_fr=fu16ABSOn_count_rl=fu16ABSOn_count_rr=0;
        fu16WheelJumpErrCnt_fl=fu16WheelJumpErrCnt_rl=fu16WheelJumpErrCnt_fr=fu16WheelJumpErrCnt_rr=0;
        fu16WheelJumpSusCnt_fl=fu16WheelJumpSusCnt_rl=fu16WheelJumpSusCnt_fr=fu16WheelJumpSusCnt_rr=0;
        fu1WheelSpeedJumpSus_fl=fu1WheelSpeedJumpSus_fr=fu1WheelSpeedJumpSus_rl=fu1WheelSpeedJumpSus_rr=0;
        (void)memset(&HFL, 0, sizeof(HFL));
        (void)memset(&HFR, 0, sizeof(HFR));
        (void)memset(&HRL, 0, sizeof(HRL));
        (void)memset(&HRR, 0, sizeof(HRR));

        if(clear_all_flg==0)
        {
            fu8NonDrvSlipDetectFlag=0;
        }
    }
}   
