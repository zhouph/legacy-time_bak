/**
 * @defgroup WssP_Main WssP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssP_Main.h"
#include "WssP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSSP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "WssP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSSP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "WssP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
WssP_Main_HdrBusType WssP_MainBus;

/* Version Info */
const SwcVersionInfo_t WssP_MainVersionInfo = 
{   
    WSSP_MAIN_MODULE_ID,           /* WssP_MainVersionInfo.ModuleId */
    WSSP_MAIN_MAJOR_VERSION,       /* WssP_MainVersionInfo.MajorVer */
    WSSP_MAIN_MINOR_VERSION,       /* WssP_MainVersionInfo.MinorVer */
    WSSP_MAIN_PATCH_VERSION,       /* WssP_MainVersionInfo.PatchVer */
    WSSP_MAIN_BRANCH_VERSION       /* WssP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Wss_SenWhlSpdInfo_t WssP_MainWhlSpdInfo;
Abc_CtrlAbsCtrlInfo_t WssP_MainAbsCtrlInfo;
WssM_MainWssMonData_t WssP_MainWssMonData;
Mom_HndlrEcuModeSts_t WssP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t WssP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t WssP_MainIgnEdgeSts;
Diag_HndlrDiagClr_t WssP_MainDiagClrSrs;

/* Output Data Element */
WssP_MainWssPlauData_t WssP_MainWssPlauData;

uint32 WssP_Main_Timer_Start;
uint32 WssP_Main_Timer_Elapsed;

#define WSSP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
#define WSSP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_MAIN_START_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSP_MAIN_STOP_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssP_MemMap.h"
#define WSSP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssP_MemMap.h"
#define WSSP_MAIN_START_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSP_MAIN_STOP_SEC_VAR_32BIT
#include "WssP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSSP_MAIN_START_SEC_CODE
#include "WssP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void WssP_Main_Init(void)
{
    /* Initialize internal bus */
    WssP_MainBus.WssP_MainWhlSpdInfo.FlWhlSpd = 0;
    WssP_MainBus.WssP_MainWhlSpdInfo.FrWhlSpd = 0;
    WssP_MainBus.WssP_MainWhlSpdInfo.RlWhlSpd = 0;
    WssP_MainBus.WssP_MainWhlSpdInfo.RrlWhlSpd = 0;
    WssP_MainBus.WssP_MainAbsCtrlInfo.AbsActFlg = 0;
    WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_FL = 0;
    WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_FR = 0;
    WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_RL = 0;
    WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_RR = 0;
    WssP_MainBus.WssP_MainEcuModeSts = 0;
    WssP_MainBus.WssP_MainIgnOnOffSts = 0;
    WssP_MainBus.WssP_MainIgnEdgeSts = 0;
    WssP_MainBus.WssP_MainDiagClrSrs = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_AbsLongTermErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_AbsLongTerm_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_PhaseErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_PhaseErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_PhaseErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_PhaseErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_PhaseErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_PhaseErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_PhaseErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_PhaseErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_ExciterErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_ExciterErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_ExciterErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_ExciterErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_ExciterErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_ExciterErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_ExciterErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_ExciterErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_AirGapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_AirGapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_AirGapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_AirGapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_AirGapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_AirGapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_AirGapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_AirGapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_PJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_PJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_PJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_PJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_PJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_PJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_PJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_PJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_MJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_MJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_MJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_MJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_MJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_MJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_MJumpErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_MJumpErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr_Ihb = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_max_speed = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_2nd_speed = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_3rd_speed = 0;
    WssP_MainBus.WssP_MainWssPlauData.WssP_min_speed = 0;
}

void WssP_Main(void)
{
    WssP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    WssP_Main_Read_WssP_MainWhlSpdInfo(&WssP_MainBus.WssP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure WssP_MainWhlSpdInfo 
     : WssP_MainWhlSpdInfo.FlWhlSpd;
     : WssP_MainWhlSpdInfo.FrWhlSpd;
     : WssP_MainWhlSpdInfo.RlWhlSpd;
     : WssP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    WssP_Main_Read_WssP_MainAbsCtrlInfo_AbsActFlg(&WssP_MainBus.WssP_MainAbsCtrlInfo.AbsActFlg);

    /* Decomposed structure interface */
    WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_FL(&WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_FL);
    WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_FR(&WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_FR);
    WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_RL(&WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_RL);
    WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_RR(&WssP_MainBus.WssP_MainWssMonData.WssM_Inhibit_RR);

    WssP_Main_Read_WssP_MainEcuModeSts(&WssP_MainBus.WssP_MainEcuModeSts);
    WssP_Main_Read_WssP_MainIgnOnOffSts(&WssP_MainBus.WssP_MainIgnOnOffSts);
    WssP_Main_Read_WssP_MainIgnEdgeSts(&WssP_MainBus.WssP_MainIgnEdgeSts);
    WssP_Main_Read_WssP_MainDiagClrSrs(&WssP_MainBus.WssP_MainDiagClrSrs);

    /* Process */
    WssP_Main_Proc(&WssP_MainBus);

    /* Output */
    WssP_Main_Write_WssP_MainWssPlauData(&WssP_MainBus.WssP_MainWssPlauData);
    /*==============================================================================
    * Members of structure WssP_MainWssPlauData 
     : WssP_MainWssPlauData.WssP_AbsLongTermErr;
     : WssP_MainWssPlauData.WssP_AbsLongTerm_Ihb;
     : WssP_MainWssPlauData.WssP_FL_PhaseErr;
     : WssP_MainWssPlauData.WssP_FL_PhaseErr_Ihb;
     : WssP_MainWssPlauData.WssP_FR_PhaseErr;
     : WssP_MainWssPlauData.WssP_FR_PhaseErr_Ihb;
     : WssP_MainWssPlauData.WssP_RL_PhaseErr;
     : WssP_MainWssPlauData.WssP_RL_PhaseErr_Ihb;
     : WssP_MainWssPlauData.WssP_RR_PhaseErr;
     : WssP_MainWssPlauData.WssP_RR_PhaseErr_Ihb;
     : WssP_MainWssPlauData.WssP_FL_ExciterErr;
     : WssP_MainWssPlauData.WssP_FL_ExciterErr_Ihb;
     : WssP_MainWssPlauData.WssP_FR_ExciterErr;
     : WssP_MainWssPlauData.WssP_FR_ExciterErr_Ihb;
     : WssP_MainWssPlauData.WssP_RL_ExciterErr;
     : WssP_MainWssPlauData.WssP_RL_ExciterErr_Ihb;
     : WssP_MainWssPlauData.WssP_RR_ExciterErr;
     : WssP_MainWssPlauData.WssP_RR_ExciterErr_Ihb;
     : WssP_MainWssPlauData.WssP_FL_AirGapErr;
     : WssP_MainWssPlauData.WssP_FL_AirGapErr_Ihb;
     : WssP_MainWssPlauData.WssP_FR_AirGapErr;
     : WssP_MainWssPlauData.WssP_FR_AirGapErr_Ihb;
     : WssP_MainWssPlauData.WssP_RL_AirGapErr;
     : WssP_MainWssPlauData.WssP_RL_AirGapErr_Ihb;
     : WssP_MainWssPlauData.WssP_RR_AirGapErr;
     : WssP_MainWssPlauData.WssP_RR_AirGapErr_Ihb;
     : WssP_MainWssPlauData.WssP_FL_PJumpErr;
     : WssP_MainWssPlauData.WssP_FL_PJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_FR_PJumpErr;
     : WssP_MainWssPlauData.WssP_FR_PJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_RL_PJumpErr;
     : WssP_MainWssPlauData.WssP_RL_PJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_RR_PJumpErr;
     : WssP_MainWssPlauData.WssP_RR_PJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_FL_MJumpErr;
     : WssP_MainWssPlauData.WssP_FL_MJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_FR_MJumpErr;
     : WssP_MainWssPlauData.WssP_FR_MJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_RL_MJumpErr;
     : WssP_MainWssPlauData.WssP_RL_MJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_RR_MJumpErr;
     : WssP_MainWssPlauData.WssP_RR_MJumpErr_Ihb;
     : WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr;
     : WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr_Ihb;
     : WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr;
     : WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr_Ihb;
     : WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr;
     : WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr_Ihb;
     : WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr;
     : WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr_Ihb;
     : WssP_MainWssPlauData.WssP_max_speed;
     : WssP_MainWssPlauData.WssP_2nd_speed;
     : WssP_MainWssPlauData.WssP_3rd_speed;
     : WssP_MainWssPlauData.WssP_min_speed;
     =============================================================================*/
    

    WssP_Main_Timer_Elapsed = STM0_TIM0.U - WssP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSSP_MAIN_STOP_SEC_CODE
#include "WssP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
