/**
 * @defgroup WssP_Main WssP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Main_Proc.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSP_MAIN_PROC_H_
#define WSSP_MAIN_PROC_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssP_Types.h"
#include "WssP_Cfg.h"
#include "WssP_Main.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 //#define WSSP_MAIN_MODULE_ID      (0)
 //#define WSSP_MAIN_MAJOR_VERSION  (2)
 //#define WSSP_MAIN_MINOR_VERSION  (0)
 //#define WSSP_MAIN_PATCH_VERSION  (0)
 //#define WSSP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
#define U8_100MSEC_TIME      20
#define U8_140MSEC_TIME      28
#define U8_155MSEC_TIME      31
#define U16_2SEC_TIME      400
#define U16_3SEC_TIME      600
#define U16_5SEC_TIME      1000
#define U16_12SEC_TIME      2400
#define U16_20SEC_TIME      4000
#define U16_36SEC_TIME      7200
#define U16_60SEC_TIME      12000
#define U16_120SEC_TIME      24000
#define U16_180SEC_TIME      36000

#define U8_F64SPEED_0KPH25 16
#define U8_F64SPEED_1KPH5 1.5*64
#define U8_F64SPEED_2KPH 2*64
#define U16_F64SPEED_4KPH 4*64
#define U16_F64SPEED_5KPH 5*64
#define U16_F64SPEED_10KPH 10*64
#define U16_F64SPEED_12KPH 12*64
#define U16_F64SPEED_17KPH5 17.5*64
#define U16_F64SPEED_20KPH 20*64
#define U16_F64SPEED_40KPH 40*64
#define U16_F64SPEED_60KPH 60*64
#define U16_F64SPEED_512KPH 512*64

#define WSS_JUMP_70G_DETECT_TIME                 126
#define WSS_JUMP_100G_DETECT_TIME                56
#define DUAL_WSS_JUMP_70G_DETECT_TIME            20
#define DUAL_WSS_JUMP_100G_DETECT_TIME           5

#define _ShortTermFaultRefSpdDiff              3            /* 3kph */
#define _ShortTermFaultRefPcntDiff             90           /* 90%  */
#define _ShortTermFaultFaultPcntDiff           70           /* 60%  */
#define _ShortTermFaultFaultDetTime            20           /* 20sec */
#define _ShortTermFaultEnableMaxSpd            15           /* 15kph */
                        
#define _HighShortTermFaultRefPcntDiff             110           /* 110% */
#define _HighShortTermFaultFaultPcntDiff           130           /* 130% */
                        
#define _LongTermFaultRefSpdDiff               2            /* 5kph */
#define _LongTermFaultRefPcntDiff              95           /* 90%  */
#define _LongTermFaultFaultPcntDiff            50           /* 60%  */
#define _LongTermFaultFaultDetTime             60           /* 60sec */
#define _LongTermFaultEnableMaxSpd             15           /* 15kph */

#define SPEED_JUMP_MUL_NO 2

#define _BackUpFaultFaultPcntDiff              40           /* 40%    */
#define _BackUpFaultEnableMaxSpd               20           /* 20kph  */

#define SAWSS_STANDSTILL_PULSE          0x01
#define SAWSS_WARNING_PULSE             0x02
#define SAWSS_DRL_PULSE                 0x03
#define SAWSS_DRR_PULSE                 0x04
#define SAWSS_DRL_EL_PULSE              0x05
#define SAWSS_DRR_EL_PULSE              0x06
#define SAWSS_INVALID_PULSE             0x07

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void WssP_Main_Proc(WssP_Main_HdrBusType *WssP_MainBus);

struct H_STRUCT_FS
{
    
    Saluint16  fu16exciter_error_count;  
    Saluint16  fu16airgap_error_count;
    Saluint8 fu8snsr_hw_open_err_count;/* Sensor H/W */
    Saluint8 fu8snsr_hw_short_err_count;/* Sensor H/W */
    Salsint16   fs16w_speed_n_1;
    Salsint16   fs16w_speed2_n_1;
    Saluint16  fu16stand_still_count;
    Saluint8 fu8plus_100g_number;
    Saluint8 fu8minus_100g_number;
    Saluint8 fu8plus_70g_number;
    Saluint8 fu8minus_100g_time;
    Saluint8 fu8plus_70g_delay;
    Saluint8 fu8same_with_vref_cnt;
    Salsint16   fs16v_diff_with_vref;
    Saluint8 fu8rough_detect_delay_time;
    Saluint8 fu8frequency_error_count;
    Saluint16  fu16ABSOn_count;
    Saluint8 plus_70g_suspect_cnt;
    Saluint8 plus_70g_suspect_clear_cnt;
    Saluint8 TestWSSNormalCount;
    Saluint16  fu16CheckTimer;
    Saluint16  fu16WheelJumpErrCnt_temp;
    Saluint16  fu16StandStillDetCnt;
    Saluint16  fu16WrongSenSigCnt;
    Saluint16  u16WrngSpeedErrCnt;
    Saluint16  u16WrngHighSpeedErrCnt;
    Saluint16  u16WrngSpeedLongTermErrCnt;
    Saluint16  fu16WssTestCompleteCnt;  
    Saluint8 fu8NormalSenSigCnt;
    Saluint16 fsu16SpeedOfResol64;
    Saluint16 fu16WheelSpeedDiff;
    
    /******************************New Wheel Speed Jump Detection Concept************************************/    
    Saluint16 fu16WheelJumpErrCnt;
    Saluint16 fu16WheelJumpSusCnt;
    /********************************************************************************************************/
    Saluint8 suspect_flg;
    Saluint8 time_chk_m100g_flg;
    Saluint8 WheelErrorDetFlg;       
    Saluint8 u1SpeedJumpSusFlg;        
    Saluint8 fu1WheelSpeedJumpSus;
    Saluint8 fu1Driving_Set_Flg;
    Saluint8 u8SensorType;
    Saluint8 u8SWSSDirection;
};

typedef struct
{
    Saluint16 u16SigDataBuff[10];
    Saluint16 u16SigMaxValue;
    Saluint16 u16SigMinValue;
    Saluint32 u32SigDiffSumValue;
    Saluint16 u16SigDiffValue;		
    Saluint8   u8SigSavedBufferNo;
    Saluint16 u16TrjRatio;
    Saluint16 u16SigStraightValue;
    Saluint8   u1UnstableSigFlg;
}CalcTrac_t;
extern void    Fif_u16TrajectoryCalc(CalcTrac_t *CalcTrac,Saluint16 NewSig);

#define fsu16SpeedOfResol64_FL    HFL.fsu16SpeedOfResol64
#define fsu16SpeedOfResol64_FR    HFR.fsu16SpeedOfResol64
#define fsu16SpeedOfResol64_RL    HRL.fsu16SpeedOfResol64
#define fsu16SpeedOfResol64_RR    HRR.fsu16SpeedOfResol64
#define fu16exciter_error_count_fl          HFL.fu16exciter_error_count
#define fu16airgap_error_count_fl           HFL.fu16airgap_error_count
#define fu8snsr_hw_open_err_count_fl        HFL.fu8snsr_hw_open_err_count         /* Sensor H/W */
#define fu8snsr_hw_short_err_count_fl       HFL.fu8snsr_hw_short_err_count        /* Sensor H/W */
#define fs16w_speed_n_1_fl                  HFL.fs16w_speed_n_1
#define fs16w_speed2_n_1_fl                 HFL.fs16w_speed2_n_1
#define fu16stand_still_count_fl            HFL.fu16stand_still_count
#define fu8plus_100g_number_fl              HFL.fu8plus_100g_number
#define fu8minus_100g_number_fl             HFL.fu8minus_100g_number
#define fu8plus_70g_number_fl               HFL.fu8plus_70g_number
#define fu8minus_100g_time_fl               HFL.fu8minus_100g_time
#define fu8plus_70g_delay_fl                HFL.fu8plus_70g_delay
#define fs16v_diff_with_vref_fl             HFL.fs16v_diff_with_vref
#define fu8same_with_vref_cnt_fl            HFL.fu8same_with_vref_cnt 
#define fu8rough_detect_delay_time_fl       HFL.fu8rough_detect_delay_time
#define fu8frequency_error_count_fl         HFL.fu8frequency_error_count
#define fu16ABSOn_count_fl                  HFL.fu16ABSOn_count
#define plus_70g_suspect_cnt_fl          HFL.plus_70g_suspect_cnt
#define plus_70g_suspect_clear_cnt_fl       HFL.plus_70g_suspect_clear_cnt
#define fu16WheelJumpErrCnt_fl              HFL.fu16WheelJumpErrCnt
#define fu16WheelJumpSusCnt_fl              HFL.fu16WheelJumpSusCnt

#define suspect_flg_fl                        HFL.suspect_flg
#define w_sen_fail_flg_fl                   HFL.w_sen_fail_flg                                     /* Sensor H/W */
#define time_chk_m100g_flg_fl               HFL.time_chk_m100g_flg
#define sub_senAD_fl_open_flg               HFL.sub_senAD_open_flg                     /* Sensor H/W */
#define WheelErrorDetFlg_fl                 HFL.WheelErrorDetFlg                          /* Sensor H/W */
#define fu1SpeedJumpSusFlg_fl               HFL.u1SpeedJumpSusFlg   
#define fu1WheelSpeedJumpSus_fl             HFL.fu1WheelSpeedJumpSus
#define fu16CheckTimer_fl                   HFL.fu16CheckTimer
#define fu16WheelJumpErrCnt_temp_fl         HFL.fu16WheelJumpErrCnt_temp
#define fu16StandStillDetCnt_fl             HFL.fu16StandStillDetCnt

// FR
#define fu16exciter_error_count_fr          HFR.fu16exciter_error_count
#define fu16airgap_error_count_fr           HFR.fu16airgap_error_count
#define fu8snsr_hw_open_err_count_fr        HFR.fu8snsr_hw_open_err_count       /* Sensor H/W */
#define fu8snsr_hw_short_err_count_fr       HFR.fu8snsr_hw_short_err_count      /* Sensor H/W */
#define fs16w_speed_n_1_fr                  HFR.fs16w_speed_n_1
#define fs16w_speed2_n_1_fr                 HFR.fs16w_speed2_n_1
#define fu16stand_still_count_fr            HFR.fu16stand_still_count
#define fu8plus_100g_number_fr              HFR.fu8plus_100g_number
#define fu8minus_100g_number_fr             HFR.fu8minus_100g_number
#define fu8plus_70g_number_fr               HFR.fu8plus_70g_number
#define fu8minus_100g_time_fr               HFR.fu8minus_100g_time
#define fu8plus_70g_delay_fr                HFR.fu8plus_70g_delay
#define fs16v_diff_with_vref_fr             HFR.fs16v_diff_with_vref
#define fu8same_with_vref_cnt_fr            HFR.fu8same_with_vref_cnt 
#define fu8rough_detect_delay_time_fr       HFR.fu8rough_detect_delay_time
#define fu8frequency_error_count_fr         HFR.fu8frequency_error_count
#define fu16ABSOn_count_fr                  HFR.fu16ABSOn_count
#define plus_70g_suspect_cnt_fr          HFR.plus_70g_suspect_cnt
#define	plus_70g_suspect_clear_cnt_fr       HFR.plus_70g_suspect_clear_cnt
#define fu16WheelJumpErrCnt_fr              HFR.fu16WheelJumpErrCnt
#define fu16WheelJumpSusCnt_fr              HFR.fu16WheelJumpSusCnt

#define suspect_flg_fr                         HFR.suspect_flg
#define w_sen_fail_flg_fr                   HFR.w_sen_fail_flg
#define time_chk_m100g_flg_fr               HFR.time_chk_m100g_flg
#define sub_senAD_fr_open_flg               HFR.sub_senAD_open_flg                    /* Sensor H/W */
#define WheelErrorDetFlg_fr                 HFR.WheelErrorDetFlg                           /* Sensor H/W */
#define fu1SpeedJumpSusFlg_fr               HFR.u1SpeedJumpSusFlg
#define fu1WheelSpeedJumpSus_fr             HFR.fu1WheelSpeedJumpSus
#define fu16CheckTimer_fr                   HFR.fu16CheckTimer
#define fu16WheelJumpErrCnt_temp_fr         HFR.fu16WheelJumpErrCnt_temp
#define fu16StandStillDetCnt_fr             HFR.fu16StandStillDetCnt

// RL
#define fu16exciter_error_count_rl          HRL.fu16exciter_error_count
#define fu16airgap_error_count_rl           HRL.fu16airgap_error_count
#define fu8snsr_hw_open_err_count_rl        HRL.fu8snsr_hw_open_err_count       /* Sensor H/W */
#define fu8snsr_hw_short_err_count_rl       HRL.fu8snsr_hw_short_err_count      /* Sensor H/W */
#define fs16w_speed_n_1_rl                  HRL.fs16w_speed_n_1
#define fs16w_speed2_n_1_rl                 HRL.fs16w_speed2_n_1
#define fu16stand_still_count_rl            HRL.fu16stand_still_count
#define fu8plus_100g_number_rl              HRL.fu8plus_100g_number
#define fu8minus_100g_number_rl             HRL.fu8minus_100g_number
#define fu8plus_70g_number_rl               HRL.fu8plus_70g_number
#define fu8minus_100g_time_rl               HRL.fu8minus_100g_time
#define fu8plus_70g_delay_rl                HRL.fu8plus_70g_delay
#define fs16v_diff_with_vref_rl             HRL.fs16v_diff_with_vref
#define fu8same_with_vref_cnt_rl            HRL.fu8same_with_vref_cnt 
#define fu8rough_detect_delay_time_rl       HRL.fu8rough_detect_delay_time
#define fu8frequency_error_count_rl         HRL.fu8frequency_error_count
#define fu16ABSOn_count_rl                  HRL.fu16ABSOn_count
#define plus_70g_suspect_cnt_rl          HRL.plus_70g_suspect_cnt
#define	plus_70g_suspect_clear_cnt_rl       HRL.plus_70g_suspect_clear_cnt
#define fu16WheelJumpErrCnt_rl              HRL.fu16WheelJumpErrCnt
#define fu16WheelJumpSusCnt_rl              HRL.fu16WheelJumpSusCnt

#define suspect_flg_rl                         HRL.suspect_flg
#define w_sen_fail_flg_rl                   HRL.w_sen_fail_flg
#define time_chk_m100g_flg_rl               HRL.time_chk_m100g_flg
#define sub_senAD_rl_open_flg               HRL.sub_senAD_open_flg                   /* Sensor H/W */
#define WheelErrorDetFlg_rl                 HRL.WheelErrorDetFlg                          /* Sensor H/W */
#define fu1SpeedJumpSusFlg_rl               HRL.u1SpeedJumpSusFlg
#define fu1WheelSpeedJumpSus_rl             HRL.fu1WheelSpeedJumpSus
#define fu16CheckTimer_rl                   HRL.fu16CheckTimer
#define fu16WheelJumpErrCnt_temp_rl         HRL.fu16WheelJumpErrCnt_temp
#define fu16StandStillDetCnt_rl             HRL.fu16StandStillDetCnt

// RR
#define fu16exciter_error_count_rr          HRR.fu16exciter_error_count
#define fu16airgap_error_count_rr           HRR.fu16airgap_error_count
#define fu8snsr_hw_open_err_count_rr        HRR.fu8snsr_hw_open_err_count        /* Sensor H/W */
#define fu8snsr_hw_short_err_count_rr       HRR.fu8snsr_hw_short_err_count       /* Sensor H/W */
#define fs16w_speed_n_1_rr                  HRR.fs16w_speed_n_1
#define fs16w_speed2_n_1_rr                 HRR.fs16w_speed2_n_1
#define fu16stand_still_count_rr            HRR.fu16stand_still_count
#define fu8plus_100g_number_rr              HRR.fu8plus_100g_number
#define fu8minus_100g_number_rr             HRR.fu8minus_100g_number
#define fu8plus_70g_number_rr               HRR.fu8plus_70g_number
#define fu8minus_100g_time_rr               HRR.fu8minus_100g_time
#define fu8plus_70g_delay_rr                HRR.fu8plus_70g_delay
#define fs16v_diff_with_vref_rr             HRR.fs16v_diff_with_vref
#define fu8same_with_vref_cnt_rr            HRR.fu8same_with_vref_cnt 
#define fu8rough_detect_delay_time_rr       HRR.fu8rough_detect_delay_time
#define fu8frequency_error_count_rr         HRR.fu8frequency_error_count
#define fu16ABSOn_count_rr                  HRR.fu16ABSOn_count
#define plus_70g_suspect_cnt_rr          HRR.plus_70g_suspect_cnt
#define	plus_70g_suspect_clear_cnt_rr       HRR.plus_70g_suspect_clear_cnt
#define fu16WheelJumpErrCnt_rr              HRR.fu16WheelJumpErrCnt
#define fu16WheelJumpSusCnt_rr              HRR.fu16WheelJumpSusCnt

#define suspect_flg_rr                         HRR.suspect_flg
#define w_sen_fail_flg_rr                   HRR.w_sen_fail_flg
#define time_chk_m100g_flg_rr               HRR.time_chk_m100g_flg
#define sub_senAD_rr_open_flg               HRR.sub_senAD_open_flg               /* Sensor H/W */
#define WheelErrorDetFlg_rr                 HRR.WheelErrorDetFlg                      /* Sensor H/W */
#define fu1SpeedJumpSusFlg_rr               HRR.u1SpeedJumpSusFlg
#define fu1WheelSpeedJumpSus_rr             HRR.fu1WheelSpeedJumpSus
#define fu16CheckTimer_rr                   HRR.fu16CheckTimer
#define fu16WheelJumpErrCnt_temp_rr         HRR.fu16WheelJumpErrCnt_temp
#define fu16StandStillDetCnt_rr             HRR.fu16StandStillDetCnt

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSP_MAIN_PROC_H_ */
/** @} */
