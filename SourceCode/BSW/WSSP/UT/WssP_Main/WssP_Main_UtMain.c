#include "unity.h"
#include "unity_fixture.h"
#include "WssP_Main.h"
#include "WssP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_WssP_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = WSSP_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_WssP_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = WSSP_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_WssP_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = WSSP_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_WssP_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = WSSP_MAINWHLSPDINFO_RRLWHLSPD;
const Rtesint32 UtInput_WssP_MainAbsCtrlInfo_AbsActFlg[MAX_STEP] = WSSP_MAINABSCTRLINFO_ABSACTFLG;
const Saluint8 UtInput_WssP_MainWssMonData_WssM_Inhibit_FL[MAX_STEP] = WSSP_MAINWSSMONDATA_WSSM_INHIBIT_FL;
const Saluint8 UtInput_WssP_MainWssMonData_WssM_Inhibit_FR[MAX_STEP] = WSSP_MAINWSSMONDATA_WSSM_INHIBIT_FR;
const Saluint8 UtInput_WssP_MainWssMonData_WssM_Inhibit_RL[MAX_STEP] = WSSP_MAINWSSMONDATA_WSSM_INHIBIT_RL;
const Saluint8 UtInput_WssP_MainWssMonData_WssM_Inhibit_RR[MAX_STEP] = WSSP_MAINWSSMONDATA_WSSM_INHIBIT_RR;
const Mom_HndlrEcuModeSts_t UtInput_WssP_MainEcuModeSts[MAX_STEP] = WSSP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_WssP_MainIgnOnOffSts[MAX_STEP] = WSSP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_WssP_MainIgnEdgeSts[MAX_STEP] = WSSP_MAINIGNEDGESTS;
const Diag_HndlrDiagClr_t UtInput_WssP_MainDiagClrSrs[MAX_STEP] = WSSP_MAINDIAGCLRSRS;

const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_AbsLongTermErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_ABSLONGTERMERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_AbsLongTerm_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_ABSLONGTERM_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_PhaseErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_PHASEERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_PhaseErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_PHASEERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_PhaseErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_PHASEERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_PhaseErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_PHASEERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_PhaseErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_PHASEERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_PhaseErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_PHASEERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_PhaseErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_PHASEERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_PhaseErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_PHASEERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_ExciterErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_EXCITERERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_ExciterErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_EXCITERERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_ExciterErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_EXCITERERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_ExciterErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_EXCITERERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_ExciterErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_EXCITERERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_ExciterErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_EXCITERERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_ExciterErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_EXCITERERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_ExciterErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_EXCITERERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_AirGapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_AIRGAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_AirGapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_AIRGAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_AirGapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_AIRGAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_AirGapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_AIRGAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_AirGapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_AIRGAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_AirGapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_AIRGAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_AirGapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_AIRGAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_AirGapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_AIRGAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_PJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_PJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_PJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_PJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_PJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_PJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_PJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_PJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_PJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_PJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_PJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_PJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_PJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_PJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_PJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_PJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_MJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_MJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_MJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_MJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_MJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_MJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_MJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_MJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_MJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_MJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_MJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_MJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_MJumpErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_MJUMPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_MJumpErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_MJUMPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_WheelTypeSwapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_WHEELTYPESWAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FL_WheelTypeSwapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FL_WHEELTYPESWAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_WheelTypeSwapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_WHEELTYPESWAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_FR_WheelTypeSwapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_FR_WHEELTYPESWAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_WheelTypeSwapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_WHEELTYPESWAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RL_WheelTypeSwapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RL_WHEELTYPESWAPERR_IHB;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_WheelTypeSwapErr[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_WHEELTYPESWAPERR;
const Saluint8 UtExpected_WssP_MainWssPlauData_WssP_RR_WheelTypeSwapErr_Ihb[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_RR_WHEELTYPESWAPERR_IHB;
const Saluint16 UtExpected_WssP_MainWssPlauData_WssP_max_speed[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_MAX_SPEED;
const Saluint16 UtExpected_WssP_MainWssPlauData_WssP_2nd_speed[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_2ND_SPEED;
const Saluint16 UtExpected_WssP_MainWssPlauData_WssP_3rd_speed[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_3RD_SPEED;
const Saluint16 UtExpected_WssP_MainWssPlauData_WssP_min_speed[MAX_STEP] = WSSP_MAINWSSPLAUDATA_WSSP_MIN_SPEED;



TEST_GROUP(WssP_Main);
TEST_SETUP(WssP_Main)
{
    WssP_Main_Init();
}

TEST_TEAR_DOWN(WssP_Main)
{   /* Postcondition */

}

TEST(WssP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        WssP_MainWhlSpdInfo.FlWhlSpd = UtInput_WssP_MainWhlSpdInfo_FlWhlSpd[i];
        WssP_MainWhlSpdInfo.FrWhlSpd = UtInput_WssP_MainWhlSpdInfo_FrWhlSpd[i];
        WssP_MainWhlSpdInfo.RlWhlSpd = UtInput_WssP_MainWhlSpdInfo_RlWhlSpd[i];
        WssP_MainWhlSpdInfo.RrlWhlSpd = UtInput_WssP_MainWhlSpdInfo_RrlWhlSpd[i];
        WssP_MainAbsCtrlInfo.AbsActFlg = UtInput_WssP_MainAbsCtrlInfo_AbsActFlg[i];
        WssP_MainWssMonData.WssM_Inhibit_FL = UtInput_WssP_MainWssMonData_WssM_Inhibit_FL[i];
        WssP_MainWssMonData.WssM_Inhibit_FR = UtInput_WssP_MainWssMonData_WssM_Inhibit_FR[i];
        WssP_MainWssMonData.WssM_Inhibit_RL = UtInput_WssP_MainWssMonData_WssM_Inhibit_RL[i];
        WssP_MainWssMonData.WssM_Inhibit_RR = UtInput_WssP_MainWssMonData_WssM_Inhibit_RR[i];
        WssP_MainEcuModeSts = UtInput_WssP_MainEcuModeSts[i];
        WssP_MainIgnOnOffSts = UtInput_WssP_MainIgnOnOffSts[i];
        WssP_MainIgnEdgeSts = UtInput_WssP_MainIgnEdgeSts[i];
        WssP_MainDiagClrSrs = UtInput_WssP_MainDiagClrSrs[i];

        WssP_Main();

        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_AbsLongTermErr, UtExpected_WssP_MainWssPlauData_WssP_AbsLongTermErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_AbsLongTerm_Ihb, UtExpected_WssP_MainWssPlauData_WssP_AbsLongTerm_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_PhaseErr, UtExpected_WssP_MainWssPlauData_WssP_FL_PhaseErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_PhaseErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FL_PhaseErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_PhaseErr, UtExpected_WssP_MainWssPlauData_WssP_FR_PhaseErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_PhaseErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FR_PhaseErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_PhaseErr, UtExpected_WssP_MainWssPlauData_WssP_RL_PhaseErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_PhaseErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RL_PhaseErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_PhaseErr, UtExpected_WssP_MainWssPlauData_WssP_RR_PhaseErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_PhaseErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RR_PhaseErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_ExciterErr, UtExpected_WssP_MainWssPlauData_WssP_FL_ExciterErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_ExciterErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FL_ExciterErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_ExciterErr, UtExpected_WssP_MainWssPlauData_WssP_FR_ExciterErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_ExciterErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FR_ExciterErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_ExciterErr, UtExpected_WssP_MainWssPlauData_WssP_RL_ExciterErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_ExciterErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RL_ExciterErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_ExciterErr, UtExpected_WssP_MainWssPlauData_WssP_RR_ExciterErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_ExciterErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RR_ExciterErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_AirGapErr, UtExpected_WssP_MainWssPlauData_WssP_FL_AirGapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_AirGapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FL_AirGapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_AirGapErr, UtExpected_WssP_MainWssPlauData_WssP_FR_AirGapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_AirGapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FR_AirGapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_AirGapErr, UtExpected_WssP_MainWssPlauData_WssP_RL_AirGapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_AirGapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RL_AirGapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_AirGapErr, UtExpected_WssP_MainWssPlauData_WssP_RR_AirGapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_AirGapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RR_AirGapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_PJumpErr, UtExpected_WssP_MainWssPlauData_WssP_FL_PJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_PJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FL_PJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_PJumpErr, UtExpected_WssP_MainWssPlauData_WssP_FR_PJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_PJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FR_PJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_PJumpErr, UtExpected_WssP_MainWssPlauData_WssP_RL_PJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_PJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RL_PJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_PJumpErr, UtExpected_WssP_MainWssPlauData_WssP_RR_PJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_PJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RR_PJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_MJumpErr, UtExpected_WssP_MainWssPlauData_WssP_FL_MJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_MJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FL_MJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_MJumpErr, UtExpected_WssP_MainWssPlauData_WssP_FR_MJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_MJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FR_MJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_MJumpErr, UtExpected_WssP_MainWssPlauData_WssP_RL_MJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_MJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RL_MJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_MJumpErr, UtExpected_WssP_MainWssPlauData_WssP_RR_MJumpErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_MJumpErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RR_MJumpErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr, UtExpected_WssP_MainWssPlauData_WssP_FL_WheelTypeSwapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FL_WheelTypeSwapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr, UtExpected_WssP_MainWssPlauData_WssP_FR_WheelTypeSwapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_FR_WheelTypeSwapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr, UtExpected_WssP_MainWssPlauData_WssP_RL_WheelTypeSwapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RL_WheelTypeSwapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr, UtExpected_WssP_MainWssPlauData_WssP_RR_WheelTypeSwapErr[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr_Ihb, UtExpected_WssP_MainWssPlauData_WssP_RR_WheelTypeSwapErr_Ihb[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_max_speed, UtExpected_WssP_MainWssPlauData_WssP_max_speed[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_2nd_speed, UtExpected_WssP_MainWssPlauData_WssP_2nd_speed[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_3rd_speed, UtExpected_WssP_MainWssPlauData_WssP_3rd_speed[i]);
        TEST_ASSERT_EQUAL(WssP_MainWssPlauData.WssP_min_speed, UtExpected_WssP_MainWssPlauData_WssP_min_speed[i]);
    }
}

TEST_GROUP_RUNNER(WssP_Main)
{
    RUN_TEST_CASE(WssP_Main, All);
}
