/**
 * @defgroup WssP_Main_Ifa WssP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSP_MAIN_IFA_H_
#define WSSP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define WssP_Main_Read_WssP_MainWhlSpdInfo(data) do \
{ \
    *data = WssP_MainWhlSpdInfo; \
}while(0);

#define WssP_Main_Read_WssP_MainAbsCtrlInfo(data) do \
{ \
    *data = WssP_MainAbsCtrlInfo; \
}while(0);

#define WssP_Main_Read_WssP_MainWssMonData(data) do \
{ \
    *data = WssP_MainWssMonData; \
}while(0);

#define WssP_Main_Read_WssP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = WssP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define WssP_Main_Read_WssP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = WssP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define WssP_Main_Read_WssP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = WssP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define WssP_Main_Read_WssP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = WssP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define WssP_Main_Read_WssP_MainAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = WssP_MainAbsCtrlInfo.AbsActFlg; \
}while(0);

#define WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_FL(data) do \
{ \
    *data = WssP_MainWssMonData.WssM_Inhibit_FL; \
}while(0);

#define WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_FR(data) do \
{ \
    *data = WssP_MainWssMonData.WssM_Inhibit_FR; \
}while(0);

#define WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_RL(data) do \
{ \
    *data = WssP_MainWssMonData.WssM_Inhibit_RL; \
}while(0);

#define WssP_Main_Read_WssP_MainWssMonData_WssM_Inhibit_RR(data) do \
{ \
    *data = WssP_MainWssMonData.WssM_Inhibit_RR; \
}while(0);

#define WssP_Main_Read_WssP_MainEcuModeSts(data) do \
{ \
    *data = WssP_MainEcuModeSts; \
}while(0);

#define WssP_Main_Read_WssP_MainIgnOnOffSts(data) do \
{ \
    *data = WssP_MainIgnOnOffSts; \
}while(0);

#define WssP_Main_Read_WssP_MainIgnEdgeSts(data) do \
{ \
    *data = WssP_MainIgnEdgeSts; \
}while(0);

#define WssP_Main_Read_WssP_MainDiagClrSrs(data) do \
{ \
    *data = WssP_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define WssP_Main_Write_WssP_MainWssPlauData(data) do \
{ \
    WssP_MainWssPlauData = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_AbsLongTermErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_AbsLongTermErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_AbsLongTerm_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_AbsLongTerm_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_PhaseErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_PhaseErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_PhaseErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_PhaseErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_PhaseErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_PhaseErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_PhaseErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_PhaseErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_PhaseErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_PhaseErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_PhaseErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_PhaseErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_PhaseErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_PhaseErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_PhaseErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_PhaseErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_ExciterErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_ExciterErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_ExciterErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_ExciterErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_ExciterErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_ExciterErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_ExciterErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_ExciterErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_ExciterErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_ExciterErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_ExciterErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_ExciterErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_ExciterErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_ExciterErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_ExciterErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_ExciterErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_AirGapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_AirGapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_AirGapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_AirGapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_AirGapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_AirGapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_AirGapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_AirGapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_AirGapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_AirGapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_AirGapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_AirGapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_AirGapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_AirGapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_AirGapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_AirGapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_PJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_PJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_PJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_PJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_PJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_PJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_PJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_PJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_PJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_PJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_PJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_PJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_PJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_PJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_PJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_PJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_MJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_MJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_MJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_MJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_MJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_MJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_MJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_MJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_MJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_MJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_MJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_MJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_MJumpErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_MJumpErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_MJumpErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_MJumpErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_WheelTypeSwapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FL_WheelTypeSwapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FL_WheelTypeSwapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_WheelTypeSwapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_FR_WheelTypeSwapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_FR_WheelTypeSwapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_WheelTypeSwapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RL_WheelTypeSwapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RL_WheelTypeSwapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_WheelTypeSwapErr(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_RR_WheelTypeSwapErr_Ihb(data) do \
{ \
    WssP_MainWssPlauData.WssP_RR_WheelTypeSwapErr_Ihb = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_max_speed(data) do \
{ \
    WssP_MainWssPlauData.WssP_max_speed = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_2nd_speed(data) do \
{ \
    WssP_MainWssPlauData.WssP_2nd_speed = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_3rd_speed(data) do \
{ \
    WssP_MainWssPlauData.WssP_3rd_speed = *data; \
}while(0);

#define WssP_Main_Write_WssP_MainWssPlauData_WssP_min_speed(data) do \
{ \
    WssP_MainWssPlauData.WssP_min_speed = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSP_MAIN_IFA_H_ */
/** @} */
