# \file
#
# \brief Diag
#
# This file contains the implementation of the SWC
# module Diag.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Diag_src

Diag_src_FILES        += $(Diag_SRC_PATH)\Diag_Hndlr.c
Diag_src_FILES        += $(Diag_IFA_PATH)\Diag_Hndlr_Ifa.c
Diag_src_FILES        += $(Diag_SRC_PATH)\CCDrvCanDiag.c
Diag_src_FILES        += $(Diag_SRC_PATH)\CDCL_LinkData.c
Diag_src_FILES        += $(Diag_SRC_PATH)\CDS_ReadRecordValue.c
Diag_src_FILES        += $(Diag_SRC_PATH)\CDCS_DiagService.c
Diag_src_FILES        += $(Diag_CFG_PATH)\Diag_Cfg.c
Diag_src_FILES        += $(Diag_CAL_PATH)\Diag_Cal.c

ifeq ($(ICE_COMPILE),true)
Diag_src_FILES        += $(Diag_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Diag_src_FILES        += $(Diag_UNITY_PATH)\unity.c
	Diag_src_FILES        += $(Diag_UNITY_PATH)\unity_fixture.c	
	Diag_src_FILES        += $(Diag_UT_PATH)\main.c
	Diag_src_FILES        += $(Diag_UT_PATH)\Diag_Hndlr\Diag_Hndlr_UtMain.c
endif