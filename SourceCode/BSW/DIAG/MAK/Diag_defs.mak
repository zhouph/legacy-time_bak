# \file
#
# \brief Diag
#
# This file contains the implementation of the SWC
# module Diag.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Diag_CORE_PATH     := $(MANDO_BSW_ROOT)\Diag
Diag_CAL_PATH      := $(Diag_CORE_PATH)\CAL\$(Diag_VARIANT)
Diag_SRC_PATH      := $(Diag_CORE_PATH)\SRC
Diag_CFG_PATH      := $(Diag_CORE_PATH)\CFG\$(Diag_VARIANT)
Diag_HDR_PATH      := $(Diag_CORE_PATH)\HDR
Diag_IFA_PATH      := $(Diag_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Diag_CMN_PATH      := $(Diag_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Diag_UT_PATH		:= $(Diag_CORE_PATH)\UT
	Diag_UNITY_PATH	:= $(Diag_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Diag_UT_PATH)
	CC_INCLUDE_PATH		+= $(Diag_UNITY_PATH)
	Diag_Hndlr_PATH 	:= Diag_UT_PATH\Diag_Hndlr
endif
CC_INCLUDE_PATH    += $(Diag_CAL_PATH)
CC_INCLUDE_PATH    += $(Diag_SRC_PATH)
CC_INCLUDE_PATH    += $(Diag_CFG_PATH)
CC_INCLUDE_PATH    += $(Diag_HDR_PATH)
CC_INCLUDE_PATH    += $(Diag_IFA_PATH)
CC_INCLUDE_PATH    += $(Diag_CMN_PATH)

