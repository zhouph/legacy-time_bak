Diag_HndlrWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Diag_HndlrWhlSpdInfo = CreateBus(Diag_HndlrWhlSpdInfo, DeList);
clear DeList;

Diag_HndlrIdbSnsrEolOfsCalcInfo = Simulink.Bus;
DeList={
    'PedalEolOfsCalcOk'
    'PedalEolOfsCalcEnd'
    'PressEolOfsCalcOk'
    'PressEolOfsCalcEnd'
    };
Diag_HndlrIdbSnsrEolOfsCalcInfo = CreateBus(Diag_HndlrIdbSnsrEolOfsCalcInfo, DeList);
clear DeList;

Diag_HndlrEcuModeSts = Simulink.Bus;
DeList={'Diag_HndlrEcuModeSts'};
Diag_HndlrEcuModeSts = CreateBus(Diag_HndlrEcuModeSts, DeList);
clear DeList;

Diag_HndlrIgnOnOffSts = Simulink.Bus;
DeList={'Diag_HndlrIgnOnOffSts'};
Diag_HndlrIgnOnOffSts = CreateBus(Diag_HndlrIgnOnOffSts, DeList);
clear DeList;

Diag_HndlrVBatt1Mon = Simulink.Bus;
DeList={'Diag_HndlrVBatt1Mon'};
Diag_HndlrVBatt1Mon = CreateBus(Diag_HndlrVBatt1Mon, DeList);
clear DeList;

Diag_HndlrVBatt2Mon = Simulink.Bus;
DeList={'Diag_HndlrVBatt2Mon'};
Diag_HndlrVBatt2Mon = CreateBus(Diag_HndlrVBatt2Mon, DeList);
clear DeList;

Diag_HndlrFspCbsMon = Simulink.Bus;
DeList={'Diag_HndlrFspCbsMon'};
Diag_HndlrFspCbsMon = CreateBus(Diag_HndlrFspCbsMon, DeList);
clear DeList;

Diag_HndlrCEMon = Simulink.Bus;
DeList={'Diag_HndlrCEMon'};
Diag_HndlrCEMon = CreateBus(Diag_HndlrCEMon, DeList);
clear DeList;

Diag_HndlrVddMon = Simulink.Bus;
DeList={'Diag_HndlrVddMon'};
Diag_HndlrVddMon = CreateBus(Diag_HndlrVddMon, DeList);
clear DeList;

Diag_HndlrCspMon = Simulink.Bus;
DeList={'Diag_HndlrCspMon'};
Diag_HndlrCspMon = CreateBus(Diag_HndlrCspMon, DeList);
clear DeList;

Diag_HndlrFuncInhibitDiagSts = Simulink.Bus;
DeList={'Diag_HndlrFuncInhibitDiagSts'};
Diag_HndlrFuncInhibitDiagSts = CreateBus(Diag_HndlrFuncInhibitDiagSts, DeList);
clear DeList;

Diag_HndlrMtrDiagState = Simulink.Bus;
DeList={'Diag_HndlrMtrDiagState'};
Diag_HndlrMtrDiagState = CreateBus(Diag_HndlrMtrDiagState, DeList);
clear DeList;

Diag_HndlrDiagVlvActrInfo = Simulink.Bus;
DeList={
    'SolenoidDrvDuty'
    'FlOvReqData'
    'FlIvReqData'
    'FrOvReqData'
    'FrIvReqData'
    'RlOvReqData'
    'RlIvReqData'
    'RrOvReqData'
    'RrIvReqData'
    'PrimCutVlvReqData'
    'SecdCutVlvReqData'
    'SimVlvReqData'
    'ResPVlvReqData'
    'BalVlvReqData'
    'CircVlvReqData'
    'PressDumpReqData'
    'RelsVlvReqData'
    };
Diag_HndlrDiagVlvActrInfo = CreateBus(Diag_HndlrDiagVlvActrInfo, DeList);
clear DeList;

Diag_HndlrMotReqDataDiagInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Diag_HndlrMotReqDataDiagInfo = CreateBus(Diag_HndlrMotReqDataDiagInfo, DeList);
clear DeList;

Diag_HndlrSasCalInfo = Simulink.Bus;
DeList={
    'DiagSasCaltoAppl'
    };
Diag_HndlrSasCalInfo = CreateBus(Diag_HndlrSasCalInfo, DeList);
clear DeList;

Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = Simulink.Bus;
DeList={
    'PedlTrvlOffsEolCalcReqFlg'
    'PSnsrOffsEolCalcReqFlg'
    };
Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = CreateBus(Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo, DeList);
clear DeList;

Diag_HndlrDiagHndlRequest = Simulink.Bus;
DeList={
    'DiagRequest_Order'
    'DiagRequest_Iq'
    'DiagRequest_Id'
    'DiagRequest_U_PWM'
    'DiagRequest_V_PWM'
    'DiagRequest_W_PWM'
    };
Diag_HndlrDiagHndlRequest = CreateBus(Diag_HndlrDiagHndlRequest, DeList);
clear DeList;

Diag_HndlrDiagClrSrs = Simulink.Bus;
DeList={'Diag_HndlrDiagClrSrs'};
Diag_HndlrDiagClrSrs = CreateBus(Diag_HndlrDiagClrSrs, DeList);
clear DeList;

Diag_HndlrDiagSci = Simulink.Bus;
DeList={'Diag_HndlrDiagSci'};
Diag_HndlrDiagSci = CreateBus(Diag_HndlrDiagSci, DeList);
clear DeList;

Diag_HndlrDiagAhbSci = Simulink.Bus;
DeList={'Diag_HndlrDiagAhbSci'};
Diag_HndlrDiagAhbSci = CreateBus(Diag_HndlrDiagAhbSci, DeList);
clear DeList;

