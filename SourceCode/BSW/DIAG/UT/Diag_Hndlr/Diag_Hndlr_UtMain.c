#include "unity.h"
#include "unity_fixture.h"
#include "Diag_Hndlr.h"
#include "Diag_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Diag_HndlrWhlSpdInfo_FlWhlSpd[MAX_STEP] = DIAG_HNDLRWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_Diag_HndlrWhlSpdInfo_FrWhlSpd[MAX_STEP] = DIAG_HNDLRWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_Diag_HndlrWhlSpdInfo_RlWhlSpd[MAX_STEP] = DIAG_HNDLRWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_Diag_HndlrWhlSpdInfo_RrlWhlSpd[MAX_STEP] = DIAG_HNDLRWHLSPDINFO_RRLWHLSPD;
const Rteuint8 UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk[MAX_STEP] = DIAG_HNDLRIDBSNSREOLOFSCALCINFO_PEDALEOLOFSCALCOK;
const Rteuint8 UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd[MAX_STEP] = DIAG_HNDLRIDBSNSREOLOFSCALCINFO_PEDALEOLOFSCALCEND;
const Rteuint8 UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk[MAX_STEP] = DIAG_HNDLRIDBSNSREOLOFSCALCINFO_PRESSEOLOFSCALCOK;
const Rteuint8 UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd[MAX_STEP] = DIAG_HNDLRIDBSNSREOLOFSCALCINFO_PRESSEOLOFSCALCEND;
const Mom_HndlrEcuModeSts_t UtInput_Diag_HndlrEcuModeSts[MAX_STEP] = DIAG_HNDLRECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Diag_HndlrIgnOnOffSts[MAX_STEP] = DIAG_HNDLRIGNONOFFSTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_Diag_HndlrVBatt1Mon[MAX_STEP] = DIAG_HNDLRVBATT1MON;
const Ioc_InputSR1msVBatt2Mon_t UtInput_Diag_HndlrVBatt2Mon[MAX_STEP] = DIAG_HNDLRVBATT2MON;
const Ioc_InputSR1msFspCbsMon_t UtInput_Diag_HndlrFspCbsMon[MAX_STEP] = DIAG_HNDLRFSPCBSMON;
const Ioc_InputSR1msCEMon_t UtInput_Diag_HndlrCEMon[MAX_STEP] = DIAG_HNDLRCEMON;
const Ioc_InputSR1msVddMon_t UtInput_Diag_HndlrVddMon[MAX_STEP] = DIAG_HNDLRVDDMON;
const Ioc_InputSR1msCspMon_t UtInput_Diag_HndlrCspMon[MAX_STEP] = DIAG_HNDLRCSPMON;
const Eem_SuspcDetnFuncInhibitDiagSts_t UtInput_Diag_HndlrFuncInhibitDiagSts[MAX_STEP] = DIAG_HNDLRFUNCINHIBITDIAGSTS;
const Mtr_Processing_DiagMtrDiagState_t UtInput_Diag_HndlrMtrDiagState[MAX_STEP] = DIAG_HNDLRMTRDIAGSTATE;

const Saluint16 UtExpected_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_SOLENOIDDRVDUTY;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_FlOvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_FLOVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_FlIvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_FLIVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_FrOvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_FROVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_FrIvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_FRIVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_RlOvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_RLOVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_RlIvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_RLIVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_RrOvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_RROVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_RrIvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_RRIVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_PRIMCUTVLVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_SECDCUTVLVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_SimVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_SIMVLVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_RESPVLVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_BalVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_BALVLVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_CircVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_CIRCVLVREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_PressDumpReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_PRESSDUMPREQDATA;
const Saluint8 UtExpected_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData[MAX_STEP] = DIAG_HNDLRDIAGVLVACTRINFO_RELSVLVREQDATA;
const Salsint32 UtExpected_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData[MAX_STEP] = DIAG_HNDLRMOTREQDATADIAGINFO_MOTPWMPHUDATA;
const Salsint32 UtExpected_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData[MAX_STEP] = DIAG_HNDLRMOTREQDATADIAGINFO_MOTPWMPHVDATA;
const Salsint32 UtExpected_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData[MAX_STEP] = DIAG_HNDLRMOTREQDATADIAGINFO_MOTPWMPHWDATA;
const Saluint8 UtExpected_Diag_HndlrMotReqDataDiagInfo_MotReq[MAX_STEP] = DIAG_HNDLRMOTREQDATADIAGINFO_MOTREQ;
const Salsint32 UtExpected_Diag_HndlrSasCalInfo_DiagSasCaltoAppl[MAX_STEP] = DIAG_HNDLRSASCALINFO_DIAGSASCALTOAPPL;
const Salsint32 UtExpected_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg[MAX_STEP] = DIAG_HNDLRIDBSNSROFFSEOLCALCREQBYDIAGINFO_PEDLTRVLOFFSEOLCALCREQFLG;
const Salsint32 UtExpected_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg[MAX_STEP] = DIAG_HNDLRIDBSNSROFFSEOLCALCREQBYDIAGINFO_PSNSROFFSEOLCALCREQFLG;
const Saluint8 UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_Order[MAX_STEP] = DIAG_HNDLRDIAGHNDLREQUEST_DIAGREQUEST_ORDER;
const Saluint16 UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_Iq[MAX_STEP] = DIAG_HNDLRDIAGHNDLREQUEST_DIAGREQUEST_IQ;
const Saluint16 UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_Id[MAX_STEP] = DIAG_HNDLRDIAGHNDLREQUEST_DIAGREQUEST_ID;
const Saluint16 UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM[MAX_STEP] = DIAG_HNDLRDIAGHNDLREQUEST_DIAGREQUEST_U_PWM;
const Saluint16 UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM[MAX_STEP] = DIAG_HNDLRDIAGHNDLREQUEST_DIAGREQUEST_V_PWM;
const Saluint16 UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM[MAX_STEP] = DIAG_HNDLRDIAGHNDLREQUEST_DIAGREQUEST_W_PWM;
const Diag_HndlrDiagClr_t UtExpected_Diag_HndlrDiagClrSrs[MAX_STEP] = DIAG_HNDLRDIAGCLRSRS;
const Diag_HndlrDiagSci_t UtExpected_Diag_HndlrDiagSci[MAX_STEP] = DIAG_HNDLRDIAGSCI;
const Diag_HndlrDiagAhbSci_t UtExpected_Diag_HndlrDiagAhbSci[MAX_STEP] = DIAG_HNDLRDIAGAHBSCI;



TEST_GROUP(Diag_Hndlr);
TEST_SETUP(Diag_Hndlr)
{
    Diag_Hndlr_Init();
}

TEST_TEAR_DOWN(Diag_Hndlr)
{   /* Postcondition */

}

TEST(Diag_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Diag_HndlrWhlSpdInfo.FlWhlSpd = UtInput_Diag_HndlrWhlSpdInfo_FlWhlSpd[i];
        Diag_HndlrWhlSpdInfo.FrWhlSpd = UtInput_Diag_HndlrWhlSpdInfo_FrWhlSpd[i];
        Diag_HndlrWhlSpdInfo.RlWhlSpd = UtInput_Diag_HndlrWhlSpdInfo_RlWhlSpd[i];
        Diag_HndlrWhlSpdInfo.RrlWhlSpd = UtInput_Diag_HndlrWhlSpdInfo_RrlWhlSpd[i];
        Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk = UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk[i];
        Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd = UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd[i];
        Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk = UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk[i];
        Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd = UtInput_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd[i];
        Diag_HndlrEcuModeSts = UtInput_Diag_HndlrEcuModeSts[i];
        Diag_HndlrIgnOnOffSts = UtInput_Diag_HndlrIgnOnOffSts[i];
        Diag_HndlrVBatt1Mon = UtInput_Diag_HndlrVBatt1Mon[i];
        Diag_HndlrVBatt2Mon = UtInput_Diag_HndlrVBatt2Mon[i];
        Diag_HndlrFspCbsMon = UtInput_Diag_HndlrFspCbsMon[i];
        Diag_HndlrCEMon = UtInput_Diag_HndlrCEMon[i];
        Diag_HndlrVddMon = UtInput_Diag_HndlrVddMon[i];
        Diag_HndlrCspMon = UtInput_Diag_HndlrCspMon[i];
        Diag_HndlrFuncInhibitDiagSts = UtInput_Diag_HndlrFuncInhibitDiagSts[i];
        Diag_HndlrMtrDiagState = UtInput_Diag_HndlrMtrDiagState[i];

        Diag_Hndlr();

        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty, UtExpected_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.FlOvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_FlOvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.FlIvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_FlIvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.FrOvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_FrOvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.FrIvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_FrIvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.RlOvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_RlOvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.RlIvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_RlIvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.RrOvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_RrOvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.RrIvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_RrIvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.SimVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_SimVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.ResPVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.BalVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_BalVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.CircVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_CircVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.PressDumpReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_PressDumpReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagVlvActrInfo.RelsVlvReqData, UtExpected_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData, UtExpected_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData, UtExpected_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData, UtExpected_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrMotReqDataDiagInfo.MotReq, UtExpected_Diag_HndlrMotReqDataDiagInfo_MotReq[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrSasCalInfo.DiagSasCaltoAppl, UtExpected_Diag_HndlrSasCalInfo_DiagSasCaltoAppl[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg, UtExpected_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg, UtExpected_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagHndlRequest.DiagRequest_Order, UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_Order[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagHndlRequest.DiagRequest_Iq, UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_Iq[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagHndlRequest.DiagRequest_Id, UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_Id[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM, UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM, UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM, UtExpected_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagClrSrs, UtExpected_Diag_HndlrDiagClrSrs[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagSci, UtExpected_Diag_HndlrDiagSci[i]);
        TEST_ASSERT_EQUAL(Diag_HndlrDiagAhbSci, UtExpected_Diag_HndlrDiagAhbSci[i]);
    }
}

TEST_GROUP_RUNNER(Diag_Hndlr)
{
    RUN_TEST_CASE(Diag_Hndlr, All);
}
