/**
 * @defgroup Diag_Types Diag_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Diag_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DIAG_TYPES_H_
#define DIAG_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h"
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Wss_SenWhlSpdInfo_t Diag_HndlrWhlSpdInfo;
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Diag_HndlrIdbSnsrEolOfsCalcInfo;
    Mom_HndlrEcuModeSts_t Diag_HndlrEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Diag_HndlrIgnOnOffSts;
    Ioc_InputSR1msVBatt1Mon_t Diag_HndlrVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t Diag_HndlrVBatt2Mon;
    Ioc_InputSR1msFspCbsMon_t Diag_HndlrFspCbsMon;
    Ioc_InputSR1msCEMon_t Diag_HndlrCEMon;
    Ioc_InputSR1msVddMon_t Diag_HndlrVddMon;
    Ioc_InputSR1msCspMon_t Diag_HndlrCspMon;
    Eem_SuspcDetnFuncInhibitDiagSts_t Diag_HndlrFuncInhibitDiagSts;
    Mtr_Processing_DiagMtrDiagState_t Diag_HndlrMtrDiagState;

/* Output Data Element */
    Diag_HndlrDiagVlvActr_t Diag_HndlrDiagVlvActrInfo;
    Diag_HndlrMotReqDataDiagInfo_t Diag_HndlrMotReqDataDiagInfo;
    Diag_HndlrSasCalInfo_t Diag_HndlrSasCalInfo;
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
    Diag_HndlrDiagHndlRequest_t Diag_HndlrDiagHndlRequest;
    Diag_HndlrDiagClr_t Diag_HndlrDiagClrSrs;
    Diag_HndlrDiagSci_t Diag_HndlrDiagSci;
    Diag_HndlrDiagAhbSci_t Diag_HndlrDiagAhbSci;
}Diag_Hndlr_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DIAG_TYPES_H_ */
/** @} */
