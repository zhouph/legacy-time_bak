/**
 * @defgroup Diag_Hndlr Diag_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Diag_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DIAG_HNDLR_H_
#define DIAG_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Diag_Types.h"
#include "Diag_Cfg.h"
#include "Diag_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define DIAG_HNDLR_MODULE_ID      (0)
 #define DIAG_HNDLR_MAJOR_VERSION  (2)
 #define DIAG_HNDLR_MINOR_VERSION  (0)
 #define DIAG_HNDLR_PATCH_VERSION  (0)
 #define DIAG_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Diag_Hndlr_HdrBusType Diag_HndlrBus;

/* Version Info */
//extern const SwcVersionInfo_t Diag_HndlrVersionInfo;

/* Input Data Element */
extern Wss_SenWhlSpdInfo_t Diag_HndlrWhlSpdInfo;
extern Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Diag_HndlrIdbSnsrEolOfsCalcInfo;
extern Mom_HndlrEcuModeSts_t Diag_HndlrEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Diag_HndlrIgnOnOffSts;
extern Ioc_InputSR1msVBatt1Mon_t Diag_HndlrVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Diag_HndlrVBatt2Mon;
extern Ioc_InputSR1msFspCbsMon_t Diag_HndlrFspCbsMon;
extern Ioc_InputSR1msCEMon_t Diag_HndlrCEMon;
extern Ioc_InputSR1msVddMon_t Diag_HndlrVddMon;
extern Ioc_InputSR1msCspMon_t Diag_HndlrCspMon;
extern Eem_SuspcDetnFuncInhibitDiagSts_t Diag_HndlrFuncInhibitDiagSts;
extern Mtr_Processing_DiagMtrDiagState_t Diag_HndlrMtrDiagState;

/* Output Data Element */
extern Diag_HndlrDiagVlvActr_t Diag_HndlrDiagVlvActrInfo;
extern Diag_HndlrMotReqDataDiagInfo_t Diag_HndlrMotReqDataDiagInfo;
extern Diag_HndlrSasCalInfo_t Diag_HndlrSasCalInfo;
extern Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
extern Diag_HndlrDiagHndlRequest_t Diag_HndlrDiagHndlRequest;
extern Diag_HndlrDiagClr_t Diag_HndlrDiagClrSrs;
extern Diag_HndlrDiagSci_t Diag_HndlrDiagSci;
extern Diag_HndlrDiagAhbSci_t Diag_HndlrDiagAhbSci;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Diag_Hndlr_Init(void);
extern void Diag_Hndlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DIAG_HNDLR_H_ */
/** @} */
