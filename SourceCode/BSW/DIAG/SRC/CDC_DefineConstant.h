#ifndef CDC_DEFINECONSTANT_H
#define CDC_DEFINECONSTANT_H

#define HMC_DIAG    1
#define CHERY_DIAG  2
#define SYMC_DIAG   3

#if 0
    #define __DIAG_SPEC  CHERY_DIAG
#elif 0
    #define __DIAG_SPEC  SYMC_DIAG
#else
    #define __DIAG_SPEC  HMC_DIAG
#endif


#define  U8_DIAG_STANDARD_MODE    0x81
#define  U8_DIAG_PROGRAM_MODE     0x85
#if __DIAG_SPEC==CHERY_DIAG
    #define  U8_DIAG_EXTENDED_MODE    0x83
    #define  U8_DIAG_SUPPLIER_MODE    0x90
#elif __DIAG_SPEC==SYMC_DIAG
    #define  U8_DIAG_EXTENDED_MODE    0x92
    #define  U8_DIAG_SUPPLIER_MODE    0x90
#else
    #define  U8_DIAG_EXTENDED_MODE    0x90
    #define  U8_DIAG_SUPPLIER_MODE    0x77
#endif

#if __DIAG_SPEC==SYMC_DIAG
    #define U8_DIAG_EMISSION_MODE     0x87
#endif 

#define U8_DIAG_SHUTDOWN_READY       0xD0
#define U8_DIAG_SHUTDOWN_REQUEST     0xD1
         
#define  U8_DIAG_STOP_DIAG_SESSION   0x02
#define  U8_DIAG_POWER_ON_RESET      0x01        
#define  U8_DIAG_CAN_TX_DISABLE      0x03
#define  U8_DIAG_JUMP_PRGM_MODE      0x04
#define  U8_TX_RESPONSE_NO_REQ       0xEE

#define  U8_DIAG_LOOP_TIME  10

#define  U8_DIAG_T_10MS    1
#define  U8_DIAG_T_20MS    2
#define  U8_DIAG_T_30MS    3
#define  U8_DIAG_T_35MS    4
#define  U8_DIAG_T_40MS    4
#define  U8_DIAG_T_50MS    5
#define  U8_DIAG_T_100MS   10
#define  U8_DIAG_T_120MS   12
#define  U8_DIAG_T_140MS   14
#define  U8_DIAG_T_400MS   40
#define  U8_DIAG_T_500MS   50
#define  U8_DIAG_T_510MS   51
#define  U8_DIAG_T_520MS   52
#define  U16_DIAG_T_5S     500
#define  U16_DIAG_T_10S    1000
#define  U16_DIAG_T_60S    6000


#define  U16_DIAG_AX_OFS_SPEED_LIMIT 1920

#define U8_INLINE_CODE_IGNORE  0
#define U8_INLINE_CODE_ENABLE  1
#define U8_INLINE_CODE_DISABLE 2

/* Local Inline Define */
#define U8_LOCAL_INLINE_DOM_SET 1
#define U8_LOCAL_INLINE_NA_SET  2
#define U8_LOCAL_INLINE_EU_SET  3

#define NO_SUPPORT  0
#define BY_SELF     1
#define BY_DIAG     2

#if 0
#if 1
    #if __CAR==CHINA_B12 /*#if __STEER_SENSOR_MAKER==TRW_SAS */
    	#define __SAS_CAL_TYPE  BY_SELF
    #elif defined (__RUF_CAN_Type_SAS)
    	#define __SAS_CAL_TYPE  BY_DIAG
    #endif
#else
    #define __SAS_CAL_TYPE  NO_SUPPORT
#endif
#endif

#define U8_MEC_MAX_VAL  60

#endif
