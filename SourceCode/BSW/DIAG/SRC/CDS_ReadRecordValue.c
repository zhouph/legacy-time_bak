/*

File    : CDS_ReadRecordValue.c
Author  : PK Kang

Last Rev No : 1.0
Last Rev Date   : 2006/08/10 (YY/MM/DD)

SRS of CUSTOMER : HMC Diagnostic Communication on CAN Specification
                  

Target ECU  : MGH-40 ABS
              MGH-40 ESP

*/
#include "Asw_Types.h"
#include "Common.h"
//#include "../CAN/CAN_CHK/CCChkCan.h" //0919 PJS
//#include "../Com/SRC/CanChk.h" //0919 PJS 추가
#include "CDC_DiagHeader.h"
#include "CDS_ReadRecordValue.h"
#include "IoHwAb.h" /* KCLim : To get voltage */
//#include "../Failsafe/fs_macro_set.h"
//#include "../Failsafe/F_CommFunc.h"
//#include "../Failsafe/F_VariableLinkSet.h"
//#include "../Failsafe/F_ActuatorLinkSet.h"
//#include "Wss.h"
//#include "../Failsafe/FEErrorProcessing.h"
//#include "../Failsafe/FsDtcInformIndex.h"
//#include "../Failsafe/FRValveRelayCheck.h"
//#include "../Failsafe/FTMotorCheck.h"
//#include "../Failsafe/FVSolAtvCheck.h"
//#include "../Failsafe/FLLampRequest.h"
//#include "../Failsafe/FsErrDefine.h"
//#include "../Failsafe/FCVacumnSenCheck.h"
//#include "../Failsafe/FCPedalSenCheck.h"
#include "ErP_DtcInformIndex.h"
#include "Diag_Hndlr.h"

#if __ECU==ESP_ECU_1
//#include "../Failsafe/ESC/FYLgYawSensorCalc.h"
#endif

uint8_t  CDS_u8GetWheelSpeed(uint8_t u8SelWss);
uint8_t  CDS_u8CalcVehicleSpeed(uint8_t SelDtype);
uint8_t  CDS_u8GetActuatorStatus(uint8_t u8SelAct);
uint8_t  CDS_u8GetLampStatus(uint8_t u8SelLP);
uint8_t  CDS_u8GetLampStatus(uint8_t u8SelLP);
uint8_t  CDS_u8GetSwitchStatus(uint8_t u8SelSW);
#if ((__G_SENSOR_TYPE!=NONE)||(__4WD_VARIANT_CODE==ENABLE))  
uint8_t  CDS_u8GetAxSnsrData(void);
#endif
#if __FULL_ESC_SYSTEM==1
uint16_t CDS_u16GetYawAySnsrData(uint8_t u8SelSnsr);
uint8_t  CDS_u8GetMcpSnsrData(uint8_t u8SelSnsr);
uint16_t CDS_u16GetSteeringData(void);
    #if 0
    uint8_t CDS_u8GetSteerPhaseData(void);
    #endif
#endif
uint16_t CDS_u16AnalizeHmcDtc(uint8_t u8ErrBuff);
uint8_t  CDS_u8DirveActuatorData(uint8_t u8SelAct);
uint8_t  CDS_u8GetPowerAdData(uint8_t u8SelPwr);
uint16_t CDS_u16GetVacuumSnsrData(void);
uint8_t  CDS_u16GetPedalTravelData(uint8_t u8SelPedal);

#define CDS_mClearBit(var,bit)    ((var) &= (uint8_t)(~((0x01) << (bit))))

#define U16_DIAG_MAX_SPEED     16000
#define U8_DIAG_V_250KPH       0xFA

#if 0
    #define DIAG_PEDAL_PDT_BY_CAN
#endif

#define __DIAG_NO_ASIC 1

/* For Compile : MSW */
uint8_t	Diag_feu1FlWssErrFlg;
uint8_t	Diag_feu1FrWssErrFlg;
uint8_t	Diag_feu1RlWssErrFlg;
uint8_t	Diag_feu1RrWssErrFlg;
uint8_t	Diag_motor_mon_flg;
uint8_t	Diag_motor_error_flg;
uint8_t	Diag_mr_error_flg;
uint8_t	Diag_abs_fsr_mon_flg;
uint8_t	Diag_ahb_fsr_mon_flg;
uint8_t	Diag_fu1FLNODriveReqSet;
uint8_t	Diag_fiu1SolFLNOShortFail;
uint8_t	Diag_fiu1SolFLNOOpenFail;
uint8_t	Diag_fvu1SolFLNO200usDrive;
uint8_t	Diag_wsu1FlnoShort;
uint8_t	Diag_wsu1FlnoOpen;
uint8_t	Diag_fu1FRNODriveReqSet;
uint8_t	Diag_fiu1SolFRNOShortFail;
uint8_t	Diag_fiu1SolFRNOOpenFail;
uint8_t	Diag_fu1RLNODriveReqSet;
uint8_t	Diag_fiu1SolRLNOShortFail;
uint8_t	Diag_fiu1SolRLNOOpenFail;
uint8_t	Diag_fu1RRNODriveReqSet;
uint8_t	Diag_fiu1SolRRNOShortFail;
uint8_t	Diag_fiu1SolRRNOOpenFail;
uint8_t	Diag_fu1FLNCDriveReqSet;
uint8_t	Diag_fiu1SolFLNCShortFail;
uint8_t	Diag_fiu1SolFLNCOpenFail;
uint8_t	Diag_fu1FRNCDriveReqSet;
uint8_t	Diag_fiu1SolFRNCShortFail;
uint8_t	Diag_fiu1SolFRNCOpenFail;
uint8_t	Diag_fu1RLNCDriveReqSet;
uint8_t	Diag_fiu1SolRLNCShortFail;
uint8_t	Diag_fiu1SolRLNCOpenFail;
uint8_t	Diag_fu1RRNCDriveReqSet;
uint8_t	Diag_fiu1SolRRNCShortFail;
uint8_t	Diag_fiu1SolRRNCOpenFail;
uint8_t	Diag_fu1APV1DriveReqSet;
uint8_t	Diag_fu1APV2DriveReqSet;
uint8_t	Diag_fu1RLV1DriveReqSet;
uint8_t	Diag_fu1RLV2DriveReqSet;
uint8_t	Diag_fu1CUT1DriveReqSet;
uint8_t	Diag_fu1CUT2DriveReqSet;
uint8_t	Diag_fu1SIMVDriveReqSet;
uint8_t	Diag_fcu1ABSLampDrive;
uint8_t	Diag_fcu1EBDLampDrive;

uint8_t	Diag_fcu1VDCLampDrive;
uint8_t	fcu2FunctionLampDrive;
uint8_t	Diag_fcu1VDCOffLampDrive;
uint8_t	Diag_fsu1AhbWLampOnReq;
uint8_t	Diag_fsu1RBCSWLampOnReq;
uint8_t	Diag_fsu8ServiceLampOnReq;
uint8_t	Diag_fu1ESCSwitchSignal;
extern uint8_t	Diag_fu1BLSSignal;
uint8_t	Diag_fu1PBSignal;
uint8_t	Diag_a_long_1_1000g;
uint8_t	Diag_feu1AX_HW_Line_err_flg;
uint8_t	Diag_SEN_BUS_off_err_flg;
uint8_t	Diag_SEN_hw_err_flg;
uint8_t	Diag_feu1IMU_HW_Line_err_flg;
uint8_t	Diag_fs16PosPress2ndSignal_1_100Bar;
uint8_t	Diag_fs16NegPress2ndSignal_1_100Bar;
uint8_t	Diag_fs16PosPress3rdSignal_1_100Bar;
uint8_t	Diag_fs16NegPress3rdSignal_1_100Bar;
uint8_t	Diag_fs16PosPress1stSignal_1_100Bar;
uint8_t	Diag_fs16NegPress1stSignal_1_100Bar;
uint8_t	Diag_fs16PosPress4thSignal_1_100Bar;
uint8_t	Diag_fs16NegPress4thSignal_1_100Bar;
uint8_t	Diag_steer_1_10deg;
uint8_t	Diag_str_hw_err_flg;
//extern uint8_t	Diag_fu16CalVoltVDD;
uint8_t	Diag_fu16CalVoltPower5V;
uint8_t	Diag_fu16CalASIC_PDT_5V_MON;
uint8_t	Diag_fu16CalASIC_PDF_5V_MON;
uint8_t	Diag_pdt_pot1_1_10mm;
int32_t Diag_fys16EstimatedYaw; // PJS for compile
int32_t Diag_fys16EstimatedAY; // PJS for compile

uint16_t Diag_fsu16SpeedOfResol64_fl; // PJS for compile
uint16_t Diag_fsu16SpeedOfResol64_fr; // PJS for compile
uint16_t Diag_fsu16SpeedOfResol64_rl; // PJS for compile
uint16_t Diag_fsu16SpeedOfResol64_rr; // PJS for compile

/**************************************/

uint8_t CDS_u8GetWheelSpeed(uint8_t u8SelWss)
{
    uint8_t u8WssSpeed;
    
    if(u8SelWss==U8_FL_WSS_RV)
    {
    	//Diag_fsu16SpeedOfResol64_fl = Wss_GetSpeedFL();
    	Diag_fsu16SpeedOfResol64_fl = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.FlWhlSpd;
        if(Diag_fsu16SpeedOfResol64_fl<U16_DIAG_MAX_SPEED)
        {
            u8WssSpeed=(uint8_t)(Diag_fsu16SpeedOfResol64_fl/64);
        }
        else
        {
            u8WssSpeed=U8_DIAG_V_250KPH;    
        }
    }
    else if(u8SelWss==U8_FR_WSS_RV)
    {
    	//Diag_fsu16SpeedOfResol64_fr = Wss_GetSpeedFR();
		Diag_fsu16SpeedOfResol64_fr = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.FrWhlSpd;
        if(Diag_fsu16SpeedOfResol64_fr<U16_DIAG_MAX_SPEED)
        {
            u8WssSpeed=(uint8_t)(Diag_fsu16SpeedOfResol64_fr/64);
        }
        else
        {
            u8WssSpeed=U8_DIAG_V_250KPH;    
        }
    }
    else if(u8SelWss==U8_RL_WSS_RV)
    {
    	//Diag_fsu16SpeedOfResol64_rl = Wss_GetSpeedRL();
		Diag_fsu16SpeedOfResol64_rl = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.RlWhlSpd;
        if(Diag_fsu16SpeedOfResol64_rl<U16_DIAG_MAX_SPEED)
        {
            u8WssSpeed=(uint8_t)(Diag_fsu16SpeedOfResol64_rl/64);
        }
        else
        {
            u8WssSpeed=U8_DIAG_V_250KPH;    
        }
    }
    else if(u8SelWss==U8_RR_WSS_RV)
    {
		
    	//Diag_fsu16SpeedOfResol64_rr = Wss_GetSpeedRR();
    	Diag_fsu16SpeedOfResol64_rr = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.RrlWhlSpd;
        if(Diag_fsu16SpeedOfResol64_rr<U16_DIAG_MAX_SPEED)
        {
            u8WssSpeed=(uint8_t)(Diag_fsu16SpeedOfResol64_rr/64);
        }
        else
        {
            u8WssSpeed=U8_DIAG_V_250KPH;    
        }
    }
    else
    {
            u8WssSpeed=0xFF;
    }
    
    return u8WssSpeed;
}


uint8_t CDS_u8CalcVehicleSpeed(uint8_t SelDtype)
{
    uint8_t WSSCNT, u8Vref;
    uint16_t DiagVehicleSpeed, DiagVehicleSpeedsum;
    
    DiagVehicleSpeed   =0;
    DiagVehicleSpeedsum=0;
    WSSCNT=0;
    
    //Diag_fsu16SpeedOfResol64_fl = Wss_GetSpeedFL();
    //Diag_fsu16SpeedOfResol64_fr = Wss_GetSpeedFR();
    //Diag_fsu16SpeedOfResol64_rl = Wss_GetSpeedRL();
    //Diag_fsu16SpeedOfResol64_rr = Wss_GetSpeedRR();
	Diag_fsu16SpeedOfResol64_fl = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.FlWhlSpd;
	Diag_fsu16SpeedOfResol64_fr = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.FrWhlSpd;
	Diag_fsu16SpeedOfResol64_rl = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.RlWhlSpd;
	Diag_fsu16SpeedOfResol64_rr = Diag_HndlrBus.Diag_HndlrWhlSpdInfo.RrlWhlSpd;
	
    if(((Diag_fsu16SpeedOfResol64_fl==0)&&(Diag_fsu16SpeedOfResol64_fr==0)&&(Diag_fsu16SpeedOfResol64_rl==0))||
    ((Diag_fsu16SpeedOfResol64_fl==0)&&(Diag_fsu16SpeedOfResol64_fr==0)&&(Diag_fsu16SpeedOfResol64_rr==0))||
    ((Diag_fsu16SpeedOfResol64_fl==0)&&(Diag_fsu16SpeedOfResol64_rl==0)&&(Diag_fsu16SpeedOfResol64_rr==0))||
    ((Diag_fsu16SpeedOfResol64_fr==0)&&(Diag_fsu16SpeedOfResol64_rl==0)&&(Diag_fsu16SpeedOfResol64_rr==0)))
    {
        DiagVehicleSpeed=0;
    }
    else
    {
        if(SelDtype==U8_FRONT_MEAN_VREF)
        {
            DiagVehicleSpeed=(Diag_fsu16SpeedOfResol64_fl+Diag_fsu16SpeedOfResol64_fr)/2;
        }
        else if(SelDtype==U8_REAR_MEAN_VREF)
        {
            DiagVehicleSpeed=(Diag_fsu16SpeedOfResol64_rl+Diag_fsu16SpeedOfResol64_rr)/2;
        }
        else if(SelDtype==U8_4WHL_MEAN_VREF)
        {
            if(Diag_feu1FlWssErrFlg==0)
            {
                WSSCNT++;
                DiagVehicleSpeedsum+=Diag_fsu16SpeedOfResol64_fl;
            }
            else
            {
                ;
            }
            if(Diag_feu1FrWssErrFlg==0)
            {
                WSSCNT++;
                DiagVehicleSpeedsum+=Diag_fsu16SpeedOfResol64_fr;
            }
            else
            {
                ;
            }
            if(Diag_feu1RlWssErrFlg==0)
            {
                WSSCNT++;
                DiagVehicleSpeedsum+=Diag_fsu16SpeedOfResol64_rl;
            }
            else
            {
                ;
            }
            if(Diag_feu1RrWssErrFlg==0)
            {
                WSSCNT++;
                DiagVehicleSpeedsum+=Diag_fsu16SpeedOfResol64_rr;
            }
            else
            {
                ;
            }
            
            if (WSSCNT>=2) 
            {
                DiagVehicleSpeed= DiagVehicleSpeedsum/WSSCNT;
            }   
            else 
            {
                DiagVehicleSpeed=0;
            }
        }
        else
        {
            u8Vref=0xFF;
        }
    }
    
    if(DiagVehicleSpeed<U16_DIAG_MAX_SPEED)
    {
        u8Vref=(uint8_t)(DiagVehicleSpeed/64); 
    }
    else
    {
        u8Vref=U8_DIAG_V_250KPH;       
    }
    
    return u8Vref;
}



uint8_t CDS_u8GetActuatorStatus(uint8_t u8SelAct)
{
    /* Relay (00 = off, 01 = on, 11 = not supported)
       Bit 1,0 : Motor     
       Bit 3,2 : Valve  */
        
    /* Valve-1 (00 = off, 01 = on, 11 = not supported)
       Bit 1,0 : Front left (IN)
       Bit 3,2 : Front right (IN)
       Bit 5,4 : Rear left (IN)
       Bit 7,6 : Rear right (IN)
    
       Valve-2 (00 = off, 01 = on, 11 = not supported)
       Bit 1,0 : Front left (OUT)
       Bit 3,2 : Front right (OUT)
       Bit 5,4 : Rear left (OUT)
       Bit 7,6 : Rear right (OUT)
       
       Valve-3 (00 = off, 01 = on, 11 = not supported)
       Bit 1,0 : TCS front left  - (TCS only)
       Bit 3,2 : TCS front right - (TCS only)
       Bit 5,4 : ESV front left
       Bit 7,6 : ESV front right */
       
    
    uint8_t u8ActStatus;
        
    if(u8SelAct==U8_MOT_STATUS)
    {   
        u8ActStatus=0x00;
        
            if((Diag_motor_mon_flg==1)&&(Diag_motor_error_flg==0)&&(Diag_mr_error_flg==0))
            {
                 u8ActStatus = 0x01;
            }
            else 
            {
                 u8ActStatus = 0x00;
            }
        }
    else if(u8SelAct==U8_RELAY_STATUS)
    {
        u8ActStatus = 0xF0;
    
        if((Diag_abs_fsr_mon_flg==1)||(Diag_ahb_fsr_mon_flg==1))
        {
             u8ActStatus |= 0x04;
        }
        else 
        {
            ;
        }        
        if((Diag_motor_mon_flg==1)&&(Diag_motor_error_flg==0)&&(Diag_mr_error_flg==0))
            {
            u8ActStatus |= 0x01;
            }
            else
            {
                ;
            }   
        
        #if 0
            #if defined(DIAG_HSA_CODING_ENABLE)
                if(ccu1HsaVcSetFlg==1)
                {
                    if(fu1RSMMon==1)
                    {
                        CDS_mClearBit(u8ActStatus, 5);
                    }
                    else
                    {
                        CDS_mClearBit(u8ActStatus, 5);
                        CDS_mClearBit(u8ActStatus, 4);
                    }
                }
                else
                {
                    ;
                }
            #else
                if(fu1RSMMon==1)
                {
                    CDS_mClearBit(u8ActStatus, 5);
                }
                else
                {
                    CDS_mClearBit(u8ActStatus, 5);
                    CDS_mClearBit(u8ActStatus, 4);
                }
            #endif
        #endif
        
        #if 0
            #if __ESS_RELAY_ENABLE==ENABLE
                if(ccu1EssVcSetFlg==1)
                {
                    if(flu1ESSRSMMon==1)
                    {
                        CDS_mClearBit(u8ActStatus, 7);
                    }
                    else
                    {
                        CDS_mClearBit(u8ActStatus, 7);
                        CDS_mClearBit(u8ActStatus, 6);
                    }
                }
                else
                {
                    ;
                }
            #endif
        #endif
    }
    else if(u8SelAct==U8_INVALVE_STATUS)
    {
        /*===========================================================
        Version_1.2
        1. Actuator 상태에 대한 정보 구현 (ON or OFF)
        ===========================================================*/
        if(Diag_abs_fsr_mon_flg==1)
        {
            u8ActStatus = 0xFF;
            #if (1)
                if((Diag_fu1FLNODriveReqSet==0)||(Diag_fiu1SolFLNOShortFail==1)||(Diag_fiu1SolFLNOOpenFail==1))
            #else
                if((Diag_fvu1SolFLNO200usDrive==0)||(Diag_wsu1FlnoShort==1)||(Diag_wsu1FlnoOpen==1))
            #endif
            {
                CDS_mClearBit(u8ActStatus, 1);
                CDS_mClearBit(u8ActStatus, 0);
            }
            else            
            {
                CDS_mClearBit(u8ActStatus, 1);
            }
            #if (1)
                if((Diag_fu1FRNODriveReqSet==0)||(Diag_fiu1SolFRNOShortFail==1)||(Diag_fiu1SolFRNOOpenFail==1))
            #else
                if((fvu1SolFRNO200usDrive==0)||(wsu1FrnoShort==1)||(wsu1FrnoOpen==1))
            #endif
            {
                CDS_mClearBit(u8ActStatus, 3);
                CDS_mClearBit(u8ActStatus, 2);
            }
            else            
            {
                CDS_mClearBit(u8ActStatus, 3);
            }
            #if (__DIAG_NO_ASIC==1)
                if((Diag_fu1RLNODriveReqSet==0)||(Diag_fiu1SolRLNOShortFail==1)||(Diag_fiu1SolRLNOOpenFail==1))
            #else
                if((fvu1SolRLNO200usDrive==0)||(wsu1RlnoShort==1)||(wsu1RlnoOpen==1))  
            #endif
            {
                CDS_mClearBit(u8ActStatus, 5);
                CDS_mClearBit(u8ActStatus, 4);
            }
            else            
            {
                CDS_mClearBit(u8ActStatus, 5);
            }
            #if (__DIAG_NO_ASIC==1)
                if((Diag_fu1RRNODriveReqSet==0)||(Diag_fiu1SolRRNOShortFail==1)||(Diag_fiu1SolRRNOOpenFail==1))
            #else
                if((fvu1SolRRNO200usDrive==0)||(wsu1RrnoShort==1)||(wsu1RrnoOpen==1))  
            #endif
            {
                CDS_mClearBit(u8ActStatus, 7);
                CDS_mClearBit(u8ActStatus, 6);
            }
            else            
            {
                CDS_mClearBit(u8ActStatus, 7);
            }
        }
        else
        {
            u8ActStatus=0x00;   
        }
    }
    else if(u8SelAct==U8_OUTVALVE_STATUS)
    {
        if(Diag_abs_fsr_mon_flg==1)
        {
            u8ActStatus = 0xFF;
            #if (__DIAG_NO_ASIC==1)
                if((Diag_fu1FLNCDriveReqSet==0)||(Diag_fiu1SolFLNCShortFail==1)||(Diag_fiu1SolFLNCOpenFail==1))
            #else
                if((fvu1SolFLNC200usDrive==0)||(wsu1FlncShort==1)||(wsu1FlncOpen==1)) 
            #endif
            {
                CDS_mClearBit(u8ActStatus, 1);
                CDS_mClearBit(u8ActStatus, 0);
            }
            else                
            {
                CDS_mClearBit(u8ActStatus, 1);
            }
            #if (__DIAG_NO_ASIC==1)
            if((Diag_fu1FRNCDriveReqSet==0)||(Diag_fiu1SolFRNCShortFail==1)||(Diag_fiu1SolFRNCOpenFail==1))
            #else
            if((fvu1SolFRNC200usDrive==0)||(wsu1FrncShort==1)||(wsu1FrncOpen==1)) 
            #endif
            {
                CDS_mClearBit(u8ActStatus, 3);
                CDS_mClearBit(u8ActStatus, 2);
            }
            else                
            {
                CDS_mClearBit(u8ActStatus, 3);
            }
            #if (__DIAG_NO_ASIC==1)
                if((Diag_fu1RLNCDriveReqSet==0)||(Diag_fiu1SolRLNCShortFail==1)||(Diag_fiu1SolRLNCOpenFail==1))
            #else
                if((fvu1SolRLNC200usDrive==0)||(wsu1RlncShort==1)||(wsu1RlncOpen==1))
            #endif
            {
                CDS_mClearBit(u8ActStatus, 5);
                CDS_mClearBit(u8ActStatus, 4);
            }
            else                
            {
                CDS_mClearBit(u8ActStatus, 5);
            }
            #if (__DIAG_NO_ASIC==1)
                if((Diag_fu1RRNCDriveReqSet==0)||(Diag_fiu1SolRRNCShortFail==1)||(Diag_fiu1SolRRNCOpenFail==1))
            #else
                if((fvu1SolRRNC200usDrive==0)||(wsu1RrncShort==1)||(wsu1RrncOpen==1)) 
            #endif
            {
                CDS_mClearBit(u8ActStatus, 7);
                CDS_mClearBit(u8ActStatus, 6);
            }
            else                
            {
                CDS_mClearBit(u8ActStatus, 7);
            }
        }
        else
        {
            u8ActStatus=0;
        }
    }
	#if 0
	    #if __ECU==ESP_ECU_1
		    else if(u8SelAct==U8_ESPVALVE_STATUS)
		    {
		        if(Diag_abs_fsr_mon_flg==1)
		        {
		            u8ActStatus = 0xFF;
		            #if (__DIAG_NO_ASIC==1)
		                if((fu1STCDriveReqSet==0)||(fiu1SolSTCShortFail==1)||(fiu1SolSTCOpenFail==1)) 
		            #else
		                if(fvu1SolStcno200usMonitor==1)
		            #endif
		            {
		                    CDS_mClearBit(u8ActStatus, 1);
		                    CDS_mClearBit(u8ActStatus, 0);
		            }
		            else                
		            {
		                CDS_mClearBit(u8ActStatus, 1);
		            }
		            #if (__DIAG_NO_ASIC==1)
		                if((fu1PTCDriveReqSet ==0)||(fiu1SolPTCShortFail==1)||(fiu1SolPTCOpenFail==1)) 
		            #else
		                if(fvu1SolPtcno200usMonitor==1)    
		            #endif
		            {
		                    CDS_mClearBit(u8ActStatus, 3);
		                    CDS_mClearBit(u8ActStatus, 2);
		                }
		            else                
		            {
		                CDS_mClearBit(u8ActStatus, 3);
		            }
		            
		            #if (__DIAG_NO_ASIC==1)
		                if((fu1SSVDriveReqSet==0)||(fiu1SolSESVShortFail==1)||(fiu1SolSESVOpenFail==1)) 
		            #else
		                if(fvu1SolSsnc200usMonitor==1)     
		            #endif
		            {
		                CDS_mClearBit(u8ActStatus, 5);
		                CDS_mClearBit(u8ActStatus, 4);
		            }
		            else                
		            {
		                CDS_mClearBit(u8ActStatus, 5);
		            }
		            #if (__DIAG_NO_ASIC==1)
		                if((fu1PSVDriveReqSet==0)||(fiu1SolPESVShortFail==1)||(fiu1SolPESVOpenFail==1)) 
		            #else
		                if(fvu1SolPsnc200usMonitor==1)     
		            #endif
		            {
		                CDS_mClearBit(u8ActStatus, 7);
		                CDS_mClearBit(u8ActStatus, 6);
		            }
		            else                
		            {
		                CDS_mClearBit(u8ActStatus, 7);
		            }
		        }
		        else
		        {
		            u8ActStatus=0;
		        }
		    }
	    #endif
	    #if __DIAG_PREMIUM_14VV_ECU==1
		    else if(u8SelAct==U8_RSVVALVE_STATUS)
		    {
		        if(Diag_abs_fsr_mon_flg==1)
		        {
		            u8ActStatus = 0xFF;
		
		            if(fvu1SolPrsnc200usMonitor==1)
		            {
		                    CDS_mClearBit(u8ActStatus, 1);
		                    CDS_mClearBit(u8ActStatus, 0);
		            }
		            else                
		            {
		                CDS_mClearBit(u8ActStatus, 1);
		            }
		            
		            if(fvu1SolSrsnc200usMonitor==1)    
		            {
		                    CDS_mClearBit(u8ActStatus, 3);
		                    CDS_mClearBit(u8ActStatus, 2);
		                }
		            else                
		            {
		                CDS_mClearBit(u8ActStatus, 3);
		            }
		        }
		        else
		        {
		            u8ActStatus=0xF0;
		        }
		    }
	    #endif
    #else
	    else if(u8SelAct==U8_AHBVALVE1_STATUS)
	    {
	        if(Diag_ahb_fsr_mon_flg==1)
	        {
	            u8ActStatus = 0xFF;
	            if(Diag_fu1APV1DriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 1);
	                CDS_mClearBit(u8ActStatus, 0);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 1);
	            }
	            
	            if(Diag_fu1APV2DriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 3);
	                CDS_mClearBit(u8ActStatus, 2);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 3);
	            }
	            
	            if(Diag_fu1RLV1DriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 5);
	                CDS_mClearBit(u8ActStatus, 4);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 5);
	            }
	            
	            if(Diag_fu1RLV2DriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 7);
	                CDS_mClearBit(u8ActStatus, 6);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 7);
	            }
	        }
	        else
	        {
	            u8ActStatus=0;
	        }   
	    }
	    else if(u8SelAct==U8_AHBVALVE2_STATUS)
	    {
	        if(Diag_ahb_fsr_mon_flg==1)
	        {
	            u8ActStatus = 0xFF;
	            if(Diag_fu1CUT1DriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 1);
	                CDS_mClearBit(u8ActStatus, 0);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 1);
	            }
	            
	            if(Diag_fu1CUT2DriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 3);
	                CDS_mClearBit(u8ActStatus, 2);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 3);
	            }
	            
	            if(Diag_fu1SIMVDriveReqSet==0)
	            {
	                CDS_mClearBit(u8ActStatus, 5);
	                CDS_mClearBit(u8ActStatus, 4);
	            }
	            else                
	            {
	                CDS_mClearBit(u8ActStatus, 5);
	            }
	        }
	        else
	        {
	            u8ActStatus=0;
	        }   
	    }
	    #endif /* __AHB_GEN3_SYSTEM==ENABLE */
    else
    {
        u8ActStatus=0xFF;
    }
    
    return u8ActStatus;
}

uint8_t CDS_u8GetLampStatus(uint8_t u8SelLP)
{
    uint8_t u8LampStatus;
    
 /* Warning lamp (00 = off, 01 = on, 11 = not supported)
       Bit 1,0 : ABS                       
       Bit 3,2 : EBD                       
       Bit 5,4 : TCS(or ESP)             
       Bit 7,6 : TCS OFF(or ESP OFF)*/
        
    u8LampStatus=0xFF;
    
    if(u8SelLP==U8_BASIC_LAMP)
    {    
        if(Diag_fcu1ABSLampDrive==1)
        {
            CDS_mClearBit(u8LampStatus, 1);    
        }
        else              
        {
            CDS_mClearBit(u8LampStatus, 1);    
            CDS_mClearBit(u8LampStatus, 0);    
        }
        
        if(Diag_fcu1EBDLampDrive==1)
        {
            CDS_mClearBit(u8LampStatus, 3);    
        }
        else              
        {
            CDS_mClearBit(u8LampStatus, 3);    
            CDS_mClearBit(u8LampStatus, 2);    
        }
        
        #if 1
            #if 1 
            #if 1
                if(Diag_fcu1VDCLampDrive==1)
                {
                    CDS_mClearBit(u8LampStatus, 5);
                }
                else
                {
                    if(fcu2FunctionLampDrive==1)
                    {
                        CDS_mClearBit(u8LampStatus, 5);
                    }
                    else
                    {
                        CDS_mClearBit(u8LampStatus, 5);    
                        CDS_mClearBit(u8LampStatus, 4);
                    }
                }
            #else
                if(fcu2FunctionLampDrive==1)
                {   
                    CDS_mClearBit(u8LampStatus, 5); 
                }   
                else                
                {
                    CDS_mClearBit(u8LampStatus, 5);    
                    CDS_mClearBit(u8LampStatus, 4);    
                }
            #endif
        
            #if 1
            if(Diag_fcu1VDCOffLampDrive==1)
            #else
            if(Diag_fcu1VDCLampDrive==1)
            #endif 
            {
                CDS_mClearBit(u8LampStatus, 7);
            } 
            else
            {
                CDS_mClearBit(u8LampStatus, 7);    
                CDS_mClearBit(u8LampStatus, 6);    
            }
            #else
                if(fcu2VDCLampDrive_SYC==1)
                {
                    CDS_mClearBit(u8LampStatus, 5);
                }
                else if(fcu2VDCLampDrive_SYC==3)
                {
                    CDS_mClearBit(u8LampStatus, 5);
                }
                else
                {
                    CDS_mClearBit(u8LampStatus, 5);    
                    CDS_mClearBit(u8LampStatus, 4);
                }
                
                if(fcu2VDCLampDrive_SYC==2)
                {
                    CDS_mClearBit(u8LampStatus, 7);
                } 
                else
                {
                    CDS_mClearBit(u8LampStatus, 7);    
                    CDS_mClearBit(u8LampStatus, 6);    
                }
            #endif        
        #endif
    }
    else if(u8SelLP==U8_FUNC_LAMP)
    {
        #if ((__BTC==ENABLE) || (__TCS==ENABLE) || (__VDC==ENABLE))
        if(fcu2FunctionLampDrive==1)
        {
            CDS_mClearBit(u8LampStatus, 1);
        }
        else
        {
            CDS_mClearBit(u8LampStatus, 1);
            CDS_mClearBit(u8LampStatus, 0);
        }
        #endif
         
        #if 0
        if(fcu3HDCLampDrive==1)
        {
            CDS_mClearBit(u8LampStatus, 3);
        }
        else
        {
            CDS_mClearBit(u8LampStatus, 3);
            CDS_mClearBit(u8LampStatus, 2);
        }
        #endif
    }
    /* AHB / RBC Lamp Diag Spec 추가 */
    else if(u8SelLP==U8_AHB_LAMP)
    {
         if(Diag_fsu1AhbWLampOnReq==1)
        {
            CDS_mClearBit(u8LampStatus, 1);
        }
        else
        {
            CDS_mClearBit(u8LampStatus, 1);
            CDS_mClearBit(u8LampStatus, 0);
        }
        
        if(Diag_fsu1RBCSWLampOnReq==1)
        {
            CDS_mClearBit(u8LampStatus, 3); 
        }
        else
        {
            CDS_mClearBit(u8LampStatus, 3);
            CDS_mClearBit(u8LampStatus, 2);
        }
        
        if(Diag_fsu8ServiceLampOnReq==1)
            {
        	CDS_mClearBit(u8LampStatus, 5);
            }
        else if(Diag_fsu8ServiceLampOnReq==2)
            {
        	CDS_mClearBit(u8LampStatus, 4);       	
            }
        else
        {
        	CDS_mClearBit(u8LampStatus, 4);
        	CDS_mClearBit(u8LampStatus, 5);        	
        }
    }
    else
    {
        u8LampStatus=0xFF;
    }
    
    return u8LampStatus;
}





uint8_t CDS_u8GetSwitchStatus(uint8_t u8SelSW)
{    
    uint8_t u8SwitchStatus;
    /* Switch-1 (00 = off, 01 = on, 11 = not supported)
       Bit 1,0 : TCS/ESP switch               
       Bit 3,2 : Brake Light switch         
                 (BLS_PIN)                  
       Bit 5,4 : Brake switch
                 (fu1BSSignal)*/
    
    u8SwitchStatus=0xFF;
     
    if((u8SelSW==U8_BASIC_SWT)||(u8SelSW==U8_SUPP_SWT))
    {
        #if __FULL_ESC_SYSTEM==1
            #if __ESC_OFF_SWITCH_TYPE!=NONE
                if(Diag_fu1ESCSwitchSignal==1)
                {
                    CDS_mClearBit(u8SwitchStatus, 1);    
                }
                else                
                {
                    CDS_mClearBit(u8SwitchStatus, 1);    
                    CDS_mClearBit(u8SwitchStatus, 0);    
                }
            #endif
        #endif

        if(Diag_fu1BLSSignal==1)
        {
            CDS_mClearBit(u8SwitchStatus, 3);    
        }
        else                
        {
            CDS_mClearBit(u8SwitchStatus, 3);    
            CDS_mClearBit(u8SwitchStatus, 2);    
        }
    
        #if __FULL_ESC_SYSTEM==1
        /*
        #if __BRAKE_SWITCH_ENABLE
            if(fu1BSSignal==1)     
            {
                CDS_mClearBit(u8SwitchStatus, 5);    
            }
            else                
            {
                CDS_mClearBit(u8SwitchStatus, 5);    
                CDS_mClearBit(u8SwitchStatus, 4);    
            }
        #endif
        */
        
        if(u8SelSW==U8_SUPP_SWT)
        {
            if(Diag_fu1PBSignal==1)
            {
                CDS_mClearBit(u8SwitchStatus, 7);    
            }
            else                
            {
                CDS_mClearBit(u8SwitchStatus, 7);    
                CDS_mClearBit(u8SwitchStatus, 6);    
            }
        }
        else
        {
            ;
        }     
        #endif
        
        #if 0
            if(u8SelSW==U8_BASIC_SWT)
            {
                if(fu1HazardSw==1)
                {
                    CDS_mClearBit(u8SwitchStatus, 7);    
                }
                else                
                {
                    CDS_mClearBit(u8SwitchStatus, 7);    
                    CDS_mClearBit(u8SwitchStatus, 6);    
                }
            }
            else
            {
                ;
            }
        #endif 
    }
    else if(u8SelSW==U8_HDC_SWT)
    {
         u8SwitchStatus = 0xFF;
    
        #if 0
        if(fu1HDCSwitchSignal==1)
        {
            CDS_mClearBit(u8SwitchStatus, 1);
        }
        else
        {
            CDS_mClearBit(u8SwitchStatus, 1);
            CDS_mClearBit(u8SwitchStatus, 0);
        }
        /*
        if(down_sw_on_flg == 1)
        {
            CDS_mClearBit(u8SwitchStatus, 3);
        }
        else
        {
            CDS_mClearBit(u8SwitchStatus, 3);
            CDS_mClearBit(u8SwitchStatus, 2);
        }

        if(up_sw_on_flg == 1)
        {
            CDS_mClearBit(u8SwitchStatus, 5);
        }
        else
        {
            CDS_mClearBit(u8SwitchStatus, 5);
            CDS_mClearBit(u8SwitchStatus, 4);
        }
        */
        #endif
        
        #if 0
            #if defined(DIAG_HSA_CODING_ENABLE)
                if(ccu1HsaVcSetFlg==1)
                {
                    if(fu1RSMMon==1)
                    {
                        CDS_mClearBit(u8SwitchStatus, 3);
                    }
                    else
                    {
                        CDS_mClearBit(u8SwitchStatus, 3);
                        CDS_mClearBit(u8SwitchStatus, 2);
                    }
                }
                else
                {
                    ;
                }
            #else
                if(fu1RSMMon==1)
                {
                    CDS_mClearBit(u8SwitchStatus, 3);
                }
                else
                {
                    CDS_mClearBit(u8SwitchStatus, 3);
                    CDS_mClearBit(u8SwitchStatus, 2);
                }
            #endif
        #endif
        
        #if 0
            #if __ESS_RELAY_ENABLE==ENABLE
                if(ccu1EssVcSetFlg==1)
                {
                    if(flu1ESSRSMMon==1)
                    {
                        CDS_mClearBit(u8SwitchStatus, 5);
                    }
                    else
                    {
                        CDS_mClearBit(u8SwitchStatus, 5);
                        CDS_mClearBit(u8SwitchStatus, 4);
                    }
                }
                else
                {
                    ;
                }
            #endif
        #endif
        
        /*
        #if __FULL_ESC_SYSTEM==1
        if(FOUR_WD_SW_MON == 1)
        {
            CDS_mClearBit(u8SwitchStatus, 7);
        }
        else
        {
            CDS_mClearBit(u8SwitchStatus, 7);
            CDS_mClearBit(u8SwitchStatus, 6);
        }
        #endif
    */
    }
    #if 0
    else if(u8SelSW==U8_HDC_STATUS)
    {
        u8SwitchStatus=((uint8_t)fcu3HDCLampDrive)|(uint8_t)0xF0;
        
        if(fu1HDCSwitchSignal==1)
        {
            CDS_mClearBit(u8SwitchStatus, 5);
        }
        else
        {
            CDS_mClearBit(u8SwitchStatus, 5);
            CDS_mClearBit(u8SwitchStatus, 4);
        }
    }
    #endif
    else
    {
        u8SwitchStatus=0xFF;
    }   
    
    return u8SwitchStatus;
}
    
    
    
    

#if ((__G_SENSOR_TYPE!=NONE)||(__4WD_VARIANT_CODE==ENABLE))    
uint8_t CDS_u8GetAxSnsrData(void)
{
    uint8_t u8GenTemp;
    uint8_t u8ReturnAx;
    int16_t s16GsenData;
        
    //if(ccu1AxSnsrSetFlg==1)
    if(1)
    {
        s16GsenData = (int16_t)(((int32_t)((((int32_t)Diag_a_long_1_1000g)*255L)/4L))/1000L);
        u8GenTemp = (uint8_t)s16GsenData + 127; 
            
        #if 1
            if((Diag_feu1AX_HW_Line_err_flg==1)&&(Diag_SEN_BUS_off_err_flg==1)&&(Diag_SEN_hw_err_flg==1))
            {
                u8ReturnAx=u8GenTemp;
            }
            else
            {
                u8ReturnAx=127;
            }
        #else
            u8ReturnAx=u8GenTemp;
        #endif 
    }
    else
    {
        u8ReturnAx=0xFF;
    }
    return u8ReturnAx;
}
#endif

#if __FULL_ESC_SYSTEM==1
uint16_t CDS_u16GetYawAySnsrData(uint8_t u8SelSnsr)
{
    uint16_t u16YawAyData;
    uint16_t u16ReturnYawAy;
    uint16_t u16TempYaw,u16TempAy;

	#if 0
    #if defined(__SVDO_IMU) || defined(__HMC_IMU)   
        u16TempYaw=(uint16_t)(((int32_t)Diag_fys16EstimatedYaw*4)/25)&0x0FFF;
        u16TempAy= (uint16_t)(((int32_t)Diag_fys16EstimatedAY*63)/100)&0x0FFF;
    #elif defined (__BOSCH_IMU)               /* Bosch(GM) IMU */
        u16TempYaw=0;
        u16TempAy=0;
    #endif
    #endif
	
    if(u8SelSnsr==U8_YAW_RV)
    {
        u16YawAyData=(0x0800+(uint16_t)u16TempYaw)&0x0FFF; 
    }
    else if(u8SelSnsr==U8_AY_RV)
    {
        u16YawAyData=(0x0800+(uint16_t)u16TempAy)&0x0FFF;
    }
    else
    {
        u16YawAyData=0xFF;
    }
    
    if((Diag_feu1IMU_HW_Line_err_flg==1)&&(Diag_SEN_BUS_off_err_flg==1)&&(Diag_SEN_hw_err_flg==1))
    {
        u16ReturnYawAy=u16YawAyData;
    }
    else
    {
        u16ReturnYawAy=0x0800;
    }
    return u16ReturnYawAy;
}


uint8_t CDS_u8GetMcpSnsrData(uint8_t u8SelSnsr)
{ 
    uint8_t  u8Pressure;
    uint16_t u16Pressure;
    
    if(u8SelSnsr==U8_POS_MP_RV)
    {
#if 1
        u8Pressure=0xFF;
#else
		if(pos_mc_1_100bar<=0)
		{
		    u8Pressure=0;
		}
		else 
		{
		    u16Pressure=(uint16_t)(pos_mc_1_100bar/100);
		
		    if(u16Pressure>=0xFE)
		    {
		        u8Pressure=0xFE;
		    }
		    else
		    {
		        u8Pressure=(uint8_t)u16Pressure;
		    }
		}
#endif        
    }
    else if(u8SelSnsr==U8_NEG_MP_RV)
    {    
#if 1
        u8Pressure=0xFF;
#else    
		if(neg_mc_1_100bar<=0)
		{
		    u8Pressure=0;
		}
		else 
		{
		    u16Pressure=(uint16_t)(neg_mc_1_100bar/100);
		
		    if(u16Pressure>=0xFE)
		    {
		        u8Pressure=0xFE;
		    }
		    else
		    {
		        u8Pressure=(uint8_t)u16Pressure;
		    }
		}
#endif		
    }
    /*===========================================================
    Version_1.4
    1. Service Data 추가
       >. FL/FR/RL/RR Wheel Pressure sensor
    ===========================================================*/
    else if(u8SelSnsr==U8_FL_POS_MP_RV)
    {
#if 1
            u8Pressure=0xFF;
#else
            if(pos_fl_1_100bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(pos_fl_1_100bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }
    else if(u8SelSnsr==U8_FR_POS_MP_RV)
    {
#if 1
        u8Pressure=0xFF;
#else
        if(pos_fr_1_100bar<=0)
        {
            u8Pressure=0;
        }
        else 
        {
            u16Pressure=(uint16_t)(pos_fr_1_100bar/100);
        
            if(u16Pressure>=0xFE)
            {
                u8Pressure=0xFE;
            }
            else
            {
                u8Pressure=(uint8_t)u16Pressure;
            }
        }
#endif
    }
    else if(u8SelSnsr==U8_RL_POS_MP_RV)
    {
#if 1
        u8Pressure=0xFF;
#else
        if(pos_rl_1_100bar<=0)
        {
            u8Pressure=0;
        }
        else 
        {
            u16Pressure=(uint16_t)(pos_rl_1_100bar/100);
        
            if(u16Pressure>=0xFE)
            {
                u8Pressure=0xFE;
            }
            else
            {
                u8Pressure=(uint8_t)u16Pressure;
            }
        }
#endif
    }
    else if(u8SelSnsr==U8_RR_POS_MP_RV)
    {
#if 1
        u8Pressure=0xFF;
#else
        if(pos_rr_1_100bar<=0)
        {
            u8Pressure=0;
        }
        else 
        {
            u16Pressure=(uint16_t)(pos_rr_1_100bar/100);
        
            if(u16Pressure>=0xFE)
            {
                u8Pressure=0xFE;
            }
            else
            {
                u8Pressure=(uint8_t)u16Pressure;
            }
        }
#endif
    }
    else if(u8SelSnsr==U8_PRM_POS_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16PosPress2ndSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16PosPress2ndSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }   
    else if(u8SelSnsr==U8_PRM_NEG_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16NegPress2ndSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16NegPress2ndSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }  
    else if(u8SelSnsr==U8_SEC_POS_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16PosPress3rdSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16PosPress3rdSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }  
    else if(u8SelSnsr==U8_SEC_NEG_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16NegPress3rdSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16NegPress3rdSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }  
    else if(u8SelSnsr==U8_HPA_POS_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16PosPress1stSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16PosPress1stSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }    
    else if(u8SelSnsr==U8_HPA_NEG_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16NegPress1stSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16NegPress1stSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }    
    else if(u8SelSnsr==U8_SIM_POS_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16PosPress4thSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16PosPress4thSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }  
    else if(u8SelSnsr==U8_SIM_NEG_MP_RV)
    {
#if 0
            u8Pressure=0xFF;
#else
            if(Diag_fs16NegPress4thSignal_1_100Bar<=0)
            {
                u8Pressure=0;
            }
            else 
            {
                u16Pressure=(uint16_t)(Diag_fs16NegPress4thSignal_1_100Bar/100);
            
                if(u16Pressure>=0xFE)
                {
                    u8Pressure=0xFE;
                }
                else
                {
                    u8Pressure=(uint8_t)u16Pressure;
                }
            }
#endif   
    }                        
    else
    {
        u8Pressure=0xFF;
    }
    
    return u8Pressure;
}       
    
    
    
uint16_t CDS_u16GetSteeringData(void)
{
    uint16_t u16Steer;
    
    #if 0
        if (wbu1StrCenterDet==1)
        {
            u16Steer=(uint16_t)((wbs16CountSteer_2deg-2000)+0x8000);
        }
        else 
        {
            u16Steer=0xFFFF;
        }
        
        return u16Steer;
    #elif __STEER_SENSOR_TYPE==CAN_TYPE
        u16Steer=(uint16_t)(Diag_steer_1_10deg+0x8000)&(uint16_t)0xFFFF;
        if(Diag_str_hw_err_flg==1)
        {
            /*return u16Steer;*/
        }
        else
        {
            u16Steer = 0x8000; /*return 0x8000;*/
        }
	 return u16Steer;
    #endif
}

#if 0
uint8_t CDS_u8GetSteerPhaseData(void)
{
    uint8_t u8SteerPos;
     
    u8SteerPos=0xFF;
     
    if(PHA_PIN_MON==1)     
    {
        CDS_mClearBit(u8SteerPos, 1);
    }
    else                
    {
        CDS_mClearBit(u8SteerPos, 1);    
        CDS_mClearBit(u8SteerPos, 0);    
    }

    if(PHB_PIN_MON==1)     
    {
        CDS_mClearBit(u8SteerPos, 3);
    }
    else                
    {
        CDS_mClearBit(u8SteerPos, 3);    
        CDS_mClearBit(u8SteerPos, 2);    
    }

    if(PHZ_PIN_MON==1)     
    {
        CDS_mClearBit(u8SteerPos, 5);
    }
    else                
    {
        CDS_mClearBit(u8SteerPos, 5);    
        CDS_mClearBit(u8SteerPos, 4);    
    }
    
    return u8SteerPos;
}
#endif
#endif

uint16_t CDS_u16AnalizeHmcDtc(uint8_t u8ErrBuff)
{
    uint16_t u16DTC;
    
    u16DTC=0;

    switch(u8ErrBuff)
    {
        case U8_FL_HW_FAULT:
            u16DTC=WHL_FL_HW_ERROR;
        break;
        
        case U8_FR_HW_FAULT:
            u16DTC=WHL_FR_HW_ERROR;
        break;
        
        case U8_RL_HW_FAULT:
            u16DTC=WHL_RL_HW_ERROR;
        break;
        
        case U8_RR_HW_FAULT:
            u16DTC=WHL_RR_HW_ERROR;
        break;
        
        case U8_FL_AIRGAP:
            u16DTC=WHL_FL_AIRGAP_ERROR;
        break;
        
        case U8_FR_AIRGAP:
            u16DTC=WHL_FR_AIRGAP_ERROR;
        break;
        
        case U8_RL_AIRGAP:
            u16DTC=WHL_RL_AIRGAP_ERROR;
        break;
        
        case U8_RR_AIRGAP:
            u16DTC=WHL_RR_AIRGAP_ERROR;
        break;
        
        case U8_FL_SPD_JUMP:
            u16DTC=WHL_FL_SPDJUMP_ERROR;
        break;
        
        case U8_FR_SPD_JUMP:
            u16DTC=WHL_FR_SPDJUMP_ERROR;
        break;
        
        case U8_RL_SPD_JUMP:
            u16DTC=WHL_RL_SPDJUMP_ERROR;
        break;
        
        case U8_RR_SPD_JUMP:
            u16DTC=WHL_RR_SPDJUMP_ERROR;
        break;
        
        case U8_SOLENOID_FAIL:
            u16DTC=SOLENOID_COIL_ERROR;
        break;
        
        case U8_MOTOR_FAIL:
            u16DTC=MOTOR_ERROR;
        break;
        
        case U8_VALVE_RALEY_FAIL:
            u16DTC=VALVE_RELAY_ERROR;
        break;
        
        case U8_ECU_HW_FAIL:
            u16DTC=ECU_HW_ERROR;
        break;
        
        case U8_OVER_POWER:
            u16DTC=OVER_VOLT_ERROR;
        break;
        
        case U8_UNDER_POWER:
            u16DTC=UNDER_VOLT_ERROR;
        break;
        
        case U8_CAN_OVERRUN:
            u16DTC=CAN_HW_ERROR;
        break;
        
        case U8_CAN_BUS_OFF:
            u16DTC=CAN_BUS_OFF_ERROR;
        break;
        
        case U8_EMS_TIMEOUT:
            u16DTC=EMS_TIME_OUT_ERROR;
        break;
        
        case U8_TCU_TIMEOUT:
            u16DTC=TCU_TIME_OUT_ERROR;
        break;
        
        case U8_CAN_MSG_UNMATCH:
            u16DTC=CAN_DATA_UNMATCH_ERROR;
        break;
                
        case U8_G_SEN_HW_FAIL:
            u16DTC=G_SEN_HW_ERROR;
        break;
        
        case U8_G_SEN_SIGNAL_FAIL:
            u16DTC=G_SEN_SIGNAL_ERROR;
        break;
        
        case U8_G_SEN_CAL_FAIL:
            u16DTC=G_SEN_CAL_ERROR;
        break; 
        
        case U8_12VSEN_PWR_FAIL:
            u16DTC=SEN12V_PWR_ERROR;
        break;
        
        case U8_5VSEN_PWR_FAIL:
            u16DTC=SEN5V_PWR_ERROR;
        break;
        
        case U8_TEMPERATURE_ERROR:
            u16DTC=TEMPERATURE_ERROR;
        break;
                
        case U8_HDC_SW_FAIL:
            u16DTC=HDC_SWITCH_ERROR;
        break;
    
        case U8_VDC_SW_FAIL:
            u16DTC=VDC_SWITCH_ERROR;
        break;
        
        #if (0)   
            case U8_MCP_HW_FAIL:
                u16DTC=MCP_HW_ERROR;
            break;
        
            case U8_MCP_SIGNAL_FAIL:
                u16DTC=MCP_SIGNAL_ERROR;
            break;
        #else
            case U8_HPA_SHORTTERM_CHARGE_FAIL:
        	 u16DTC=HPA_SHORTTERM_CHARGE_ERROR;	
    	    break;
    		
    	    case U8_HPA_LONGTERM_CHARGE_FAIL:
    		u16DTC=HPA_LONGTERM_CHARGE_ERROR;	
            break;
        
            case U8_HPA_HW_FAIL:
                u16DTC=HPA_HW_ERROR;
            break;
        
            case U8_HPA_SIGNAL_FAIL:
                u16DTC=HPA_SIGNAL_ERROR;
            break;
            
            case U8_HPA_LOW_PRESSURE_FAIL:
                u16DTC=HPA_LOW_PRESSURE_ERROR;
            break;  
            
            case U8_BCP_SEN_HW_FAIL:
                u16DTC=BCP_SEN_HW_ERROR;
            break;
        
            case U8_BCP_ABNORMAL_PRESSURE_FAIL:
                u16DTC=BCP_ABNORMAL_PRESSURE_ERROR;
            break;
            
            case U8_PSP_SEN_HW_FAIL:
                u16DTC=PSP_SEN_HW_ERROR;
            break; 
            
            case U8_PSP_ABNORMAL_PRESSURE_FAIL:
                u16DTC=PSP_ABNORMAL_PRESSURE_ERROR;
            break;             
        #endif    
    
        case U8_BLS_FAIL:
            u16DTC=BLS_BS_ERROR;
        break;
    
        case U8_YAW_LG_OVERRUN:
            u16DTC=YAW_LG_OVERRUN_ERROR;
        break;
    
        case U8_YAW_LG_BUS_OFF_FAIL:
            u16DTC=YAW_LG_BUS_OFF_ERROR;
        break;
        
        case U8_YAW_LG_ELECTIC_FAIL:
            u16DTC=YAW_LG_HW_ERROR;
        break;        
    
        case U8_YAW_LG_ACU_FAIL:
            u16DTC=YAW_LG_ACU_SIGNAL_ERROR;        
        break;
    
        case U8_YAW_LG_SIGNAL_FAIL:
            u16DTC=YAW_LG_SIGNAL_ERROR;
        break;
    
        case U8_STR_HW_FAIL:
            u16DTC=STR_HW_ERROR;
        break;
    
        case U8_STR_CAL_ERROR:
            u16DTC=STR_CAL_ERROR;
        break;
            
        case U8_STR_SIGNAL_FAIL:
            u16DTC=STR_SIGNAL_ERROR;
        break;
            
        case U8_FWD1_TIMEOUT:
            u16DTC=FWD1_TIME_OUT_ERROR;
        break;
           
        case U8_VARINAT_CODING_FAIL:
            u16DTC=VARIANT_CODING_ERROR;
		break;

        case U8_EMSINVALID_FAIL:
            u16DTC=EMS_SIGNAL_ERROR;
        break;
            
        #if 0
            case U8_CDC1_TIMEOUT:
                u16DTC=CDC1_TIME_OUT_ERROR;
            break;
        
            case U8_EPS1_TIMEOUT:
                u16DTC=EPS1_TIME_OUT_ERROR;
            break;
        
            case U8_AFS1_TIMEOUT:
                u16DTC=AFS1_TIME_OUT_ERROR;
            break;
        #endif
    
        #if 0    
            case U8_ACC240_TIMEOUT:
                u16DTC=ACC240_TIME_OUT_ERROR;
            break;
        #endif
               
        case U8_BRK_LP_RELAY_FAIL:
            u16DTC=BRK_LAMP_RELAY_ERROR;
        break;
                
        case U8_HCU_TIMEOUT:
            u16DTC=HCU_TIME_OUT_ERROR;
        break;
        
        case U8_GEAR_SWITCH_FAIL:
            u16DTC=GEAR_SWITCH_ERR;
        break;
        
        case U8_CLUTCH_SWITCH_FAIL:
            u16DTC=CLUTCH_SWITCH_ERR;
        break;
                
        case U8_VSM2_INVALID_FAIL:
            u16DTC=VSM2_INVALID_DATA_ERROR;
        break;
        
        case U8_VSM2_TIMEOUT:
            u16DTC=VSM2_TIME_OUT_ERROR;
        break;
        
        case U8_ESS_LP_ERR:
            u16DTC=ESS_LP_ERR_DTC_CODE;
        break;
                               
        case U8_VACUUM_PUMP_RELAY_FAIL :
            u16DTC=VACUUM_PUMP_RELAY_ERROR;
        break;
        
        case U8_VACUUM_PUMP_SYSTEM_FAIL:
            u16DTC=VACUUM_PUMP_SYSTEM_ERROR;
        break;
        
        case U8_VACUUM_SENSOR_HW_FAIL:
            u16DTC=VACUUM_SENSOR_HW_ERROR;
        break;
        
        case U8_VACUUM_SENSOR_SW_FAIL:
            u16DTC=VACUUM_SENSOR_SW_ERROR;
        break;         
        
        case U8_PEDAL_SENSOR_HW_FAIL:
            u16DTC=PEDAL_SENSOR_HW_ERROR;
        break;
        
        case U8_PEDAL_SENSOR_SW_FAIL:
        case U8_PEDAL_SIGNAL_ABNORMAL:
            u16DTC=PEDAL_SENSOR_SIGNAL_ERROR;
        break;
        
        case U8_PEDAL_SENSOR_CAL_FAIL:
            u16DTC=PEDAL_SENSOR_CAL_ERROR;
        break;
        case U8_PRESSURE_SENSOR_CAL_FAIL:
            u16DTC=PRESSURE_SENSOR_CAL_ERROR;
        break;

        case U8_IGN_LINE_OPEN_FAIL:
            u16DTC=DTC_IGN_LINE;
        break;

        case U8_BOOSTER_CIRCUIT_LEAK_FAIL:
            u16DTC=BOOSTER_CIRCUIT_LEAK_ERROR;
        break;    
        
        
        case U8_SCC_ACCEL_ABNORM_FAIL:  
            u16DTC=SCC_ACCEL_ABNORM_DTC;
        break;
        
        
        //case U8_SCC_ECU_FAULT_FAIL:
        //    u16DTC=SCC_ECU_FAULT_DTC;
        //break;


        case U8_EMS1_CAN_ABNORM_FAIL:  
            u16DTC=EMS1_EMS2_CAN_ABNORM_DTC;
        break;
    
        case U8_SCC1_SCC2_CAN_TIMEOUT_FAIL: 
            u16DTC=SCC1_SCC2_CAN_TIMEOUT_DTC;
        break;
    
        case U8_EMS5_CAN_ABNORM_FAIL:   
        case U8_EMS5_CAN_TIMEOUT_FAIL:  
            u16DTC=EMS5_CAN_TIMEOUT_DTC;
        break;
    
        case U8_SCC1_SCC2_CAN_ABNORM_FAIL:  
            u16DTC=SCC1_SCC2_CAN_ABNORM_DTC;
        break;
    
        case U8_TCU_SCC_CAN_FAIL_FAIL:  
            u16DTC=TCU_SCC_CAN_FAIL_DTC;
		break;
        case U8_TCUGEARINVALID_FAIL:  
            u16DTC=TCU_GEAR_INVALID_FAIL_DTC;
        break; 

        break;  
        
        case U8_AHB_TIMEOUT:  
            u16DTC=AHB_TIME_OUT_ERROR;
        break;
        
        case U8_BMS_TIMEOUT:  
            u16DTC=BMS_TIME_OUT_ERROR;
        break;
        
        case U8_MCU_TIMEOUT:  
            u16DTC=MCU_TIME_OUT_ERROR;
        break;
        
        case U8_EPB1_TIMEOUT:
            u16DTC=EPB1_TIMEOUT_ERROR;
        break;
        
        case U8_EPB1_SIGNAL_FAIL:
            u16DTC=EPB1_SIGNAL_ERROR;
        break;
        
        case U8_AVH_SW_FAIL:
            u16DTC=AVH_SW_ERROR;
        break;
        
        case U8_CLU2_TIMEOUT:
            u16DTC=CLU2_TIMEOUT_ERROR;
        break;
        
        case U8_CLU2_SIGNAL_FAIL:
            u16DTC=CLU2_SIGNAL_ERROR;
        break; 

        //MOC DTC
        case U8_MOC_SWITCH_ERR:
            u16DTC=DTC_APPLY_SWITCH;
        break;
        
        case U8_MOC_FORCE_ERR:
            u16DTC=DTC_FORCE_SEN;
        break;
        
        case U8_MOC_CURRENT_ERR:
            u16DTC=DTC_CURRENT_SEN;
        break;
        
        case U8_MOC_ACTUATOR_L_LINE_ERR:
            u16DTC=DTC_MOTOR_SHORT_OPEN_L;
        break;
        
        case U8_MOC_ACTUATOR_R_LINE_ERR:
            u16DTC=DTC_MOTOR_SHORT_OPEN_R;
        break;        

        case U8_MOC_ACTUATOR_L_TIMEOUT_ERR:
            u16DTC=DTC_MOTOR_TIMEOUT_L;
        break;            
            
        case U8_MOC_ACTUATOR_R_TIMEOUT_ERR:
            u16DTC=DTC_MOTOR_TIMEOUT_R;
        break;
            
        case U8_MOC_CONTROL_NOT_FINISHED:
            u16DTC=DTC_CONTROL_NOT_FINISHED;
        break;

        case U8_MOC_HALLPWR_ERR:
            u16DTC=DTC_HALLPWR_OPEN_SHORT;
            break;
        
        default:
        break;
    }
    
    return u16DTC;
}

uint8_t CDS_u8DirveActuatorData(uint8_t u8SelAct)
{
    uint8_t u8ActSf=U8_DIAG_ACT_FAILURE;
    
    /*===========================================================
    Version_1.2
    1. Actuator 강제 구동 시 구동 성공 여부에 대한 정보 구현 
       (Driving Success or Failure)
    ===========================================================*/
        if(u8SelAct==U8_DIAG_MOTOR) 
        {
            if((Diag_motor_mon_flg==1)&&(Diag_motor_error_flg==0)&&(Diag_mr_error_flg==0))
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else 
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }  
    else
    {  
        if(Diag_abs_fsr_mon_flg==1)
        {
            if(u8SelAct==U8_DIAG_FLI)
        {
            #if (1)             
                if((Diag_fu1FLNODriveReqSet==1)&&(Diag_fiu1SolFLNOShortFail==0)&&(Diag_fiu1SolFLNOOpenFail==0))
            #else
                if((Diag_fvu1SolFLNO200usDrive==1)&&(Diag_wsu1FlnoShort==0)&&(Diag_wsu1FlnoOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else 
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else if(u8SelAct==U8_DIAG_FRI)
        {
            #if (1)
                if((Diag_fu1FRNODriveReqSet==1)&&(Diag_fiu1SolFRNOShortFail==0)&&(Diag_fiu1SolFRNOOpenFail==0))
            #else
                if((fvu1SolFRNO200usDrive==1)&&(wsu1FrnoShort==0)&&(wsu1FrnoOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }  
            else
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }    
        }
        else if(u8SelAct==U8_DIAG_RLI)
        {
            #if (1)
                if((Diag_fu1RLNODriveReqSet==1)&&(Diag_fiu1SolRLNOShortFail==0)&&(Diag_fiu1SolRLNOOpenFail==0))
            #else
                if((fvu1SolRLNO200usDrive==1)&&(wsu1RlnoShort==0)&&(wsu1RlnoOpen==0))
            #endif          
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else if(u8SelAct==U8_DIAG_RRI)
        {
            #if (1)
                if((Diag_fu1RRNODriveReqSet==1)&&(Diag_fiu1SolRRNOShortFail==0)&&(Diag_fiu1SolRRNOOpenFail==0))
            #else
                if((fvu1SolRRNO200usDrive==1)&&(wsu1RrnoShort==0)&&(wsu1RrnoOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else 
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else if(u8SelAct==U8_DIAG_FLO)
        {
            #if (1)
                if((Diag_fu1FLNCDriveReqSet==1)&&(Diag_fiu1SolFLNCShortFail==0)&&(Diag_fiu1SolFLNCOpenFail==0))
            #else
                if((fvu1SolFLNC200usDrive==1)&&(wsu1FlncShort==0)&&(wsu1FlncOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else 
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else if(u8SelAct==U8_DIAG_FRO)
        {
            #if (1)
                if((Diag_fu1FRNCDriveReqSet==1)&&(Diag_fiu1SolFRNCShortFail==0)&&(Diag_fiu1SolFRNCOpenFail==0))
            #else
                if((fvu1SolFRNC200usDrive==1)&&(wsu1FrncShort==0)&&(wsu1FrncOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else 
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else if(u8SelAct==U8_DIAG_RLO)
        {
            #if (1)
                if((Diag_fu1RLNCDriveReqSet==1)&&(Diag_fiu1SolRLNCShortFail==0)&&(Diag_fiu1SolRLNCOpenFail==0))
            #else
                if((fvu1SolRLNC200usDrive==1)&&(wsu1RlncShort==0)&&(wsu1RlncOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else if(u8SelAct==U8_DIAG_RRO)
        {
            #if (1)
                if((Diag_fu1RRNCDriveReqSet==1)&&(Diag_fiu1SolRRNCShortFail==0)&&(Diag_fiu1SolRRNCOpenFail==0))
            #else
                if((fvu1SolRRNC200usDrive==1)&&(wsu1RrncShort==0)&&(wsu1RrncOpen==0))
            #endif
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        } 
		#if 0  
	        #if __ECU==ESP_ECU_1
	        else if(u8SelAct==U8_DIAG_TCL)
	        {
	            #if (__DIAG_NO_ASIC==1)
	                if((fu1STCDriveReqSet==1)&&(fiu1SolSTCShortFail==0)&&(fiu1SolSTCOpenFail==0))
	            #else
	                if(fvu1SolStcno200usMonitor==0) 
	            #endif
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }
	        }
	        else if(u8SelAct==U8_DIAG_TCR)
	        {
	            #if (__DIAG_NO_ASIC==1)
	                if((fu1PTCDriveReqSet==1)&&(fiu1SolPTCShortFail==0)&&(fiu1SolPTCOpenFail==0))
	            #else
	                if(fvu1SolPtcno200usMonitor==0) 
	            #endif
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }
	        }               
	        else if(u8SelAct==U8_DIAG_ESVP)
	        {
	            #if (__DIAG_NO_ASIC==1)
	                if((fu1PSVDriveReqSet==1)&&(fiu1SolPESVShortFail==0)&&(fiu1SolPESVOpenFail==0))
	            #else
	                if(fvu1SolPsnc200usMonitor==0)  
	            #endif
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }
	        }
	        else if(u8SelAct==U8_DIAG_ESVS)
	        {
	            #if (__DIAG_NO_ASIC==1)
	                if((fu1SSVDriveReqSet==1)&&(fiu1SolSESVShortFail==0)&&(fiu1SolSESVOpenFail==0))
	            #else
	                if(fvu1SolSsnc200usMonitor==0) 
	            #endif
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }
	        }
	            #if __DIAG_PREMIUM_14VV_ECU==1
	            else if(u8SelAct==U8_DIAG_PRSV)
	            {
	                if(fvu1SolPrsnc200usMonitor==0) 
	                {
	                    u8ActSf=U8_DIAG_ACT_SUCCESS;
	                }
	                else 
	                {
	                    u8ActSf=U8_DIAG_ACT_FAILURE;
	                }
	            }
	            else if(u8SelAct==U8_DIAG_SRSV)
	            {
	                if(fvu1SolSrsnc200usMonitor==0) 
	                {
	                    u8ActSf=U8_DIAG_ACT_SUCCESS;
	                }
	                else 
	                {
	                    u8ActSf=U8_DIAG_ACT_FAILURE;
	                }
	            }
	            #endif
	        #endif
            #endif
        }
        #if __AHB_GEN3_SYSTEM==ENABLE  
        if(Diag_ahb_fsr_mon_flg==1)
        {
	        if(u8SelAct==U8_DIAG_APV01)
	        {
	            if(Diag_fu1APV1DriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	        else if(u8SelAct==U8_DIAG_APV02)
	        {
	            if(Diag_fu1APV2DriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	        else if(u8SelAct==U8_DIAG_RLV01)
	        {
	            if(Diag_fu1RLV1DriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	        else if(u8SelAct==U8_DIAG_RLV02)
	        {
	            if(Diag_fu1RLV2DriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	        else if(u8SelAct==U8_DIAG_CUT1)
	        {
	            if(Diag_fu1CUT1DriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	        else if(u8SelAct==U8_DIAG_CUT2)
	        {
	            if(Diag_fu1CUT2DriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	        else if(u8SelAct==U8_DIAG_SIMV)
	        {
	            if(Diag_fu1SIMVDriveReqSet==1)
	            {
	                u8ActSf=U8_DIAG_ACT_SUCCESS;
	            }
	            else 
	            {
	                u8ActSf=U8_DIAG_ACT_FAILURE;
	            }       
	        }
	    }	    
        #endif /* __AHB_GEN3_SYSTEM==ENABLE */
    }
    if(u8SelAct==U8_DIAG_MULTI)
        {
            u8ActSf=U8_DIAG_ACT_SUCCESS;
    }
    
    #if 0    
        if(u8SelAct==U8_DIAG_ESSRELAY)
        {
            /*
            if(flu1ESSLampRelayMon==1)
            {
                if(flu1ESSRSMMon==0)
                {
                    u8ActSf=U8_DIAG_ACT_SUCCESS;
                }
                else
                {
                    u8ActSf=U8_DIAG_ACT_FAILURE;
                }
            }
            else
            {
                if(flu1ESSRSMMon==Diag_fu1BLSSignal)
                {
                    u8ActSf=U8_DIAG_ACT_SUCCESS;
                }
                else
                {
                    u8ActSf=U8_DIAG_ACT_FAILURE;
                }
            }
            */
            
            #if __ESS_RELAY_ENABLE==ENABLE
                if(flu1ESSLampDriveOKflg==1)
                {
                    u8ActSf=U8_DIAG_ACT_SUCCESS;
                }
                else 
                {
                    u8ActSf=U8_DIAG_ACT_FAILURE;
                }
            #else
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            #endif
        }
        else
        {
            ;
        }
    #endif
   
    #if 0
        if(u8SelAct==U8_DIAG_HDCRELAY)
        {
            if((fu1BrakeLampRequest==1)&&(fu1RSMMon==1))
            {
                u8ActSf=U8_DIAG_ACT_SUCCESS;
            }
            else 
            {
                u8ActSf=U8_DIAG_ACT_FAILURE;
            }
        }
        else
        {
            ;
        }
    #endif 
    
    return u8ActSf;
}

uint8_t CDS_u8GetPowerAdData(uint8_t u8SelPwr)
{
    uint16_t u16CalPwr_1;
    uint8_t  u8CalPwr;
    uint16 Diag_fu16CalVoltVDD;
    
    u8CalPwr=0xff;
    
    switch(u8SelPwr)
    {
       case U8_DIAG_BATT:
       {
           /*u16CalPwr_1=(uint16_t)(((((uint32_t)fu16IGNVoltage*20)*255)/16)/1023);*/
    	  // Diag_fu16CalVoltVDD = IoHwAb_GetVoltage (IOHWAB_IN_PORT_VDD_MON);
    	   Diag_fu16CalVoltVDD = Diag_HndlrBus.Diag_HndlrVddMon;
           u16CalPwr_1=(uint16_t)(((((uint32_t)Diag_fu16CalVoltVDD)*255)/16)/1000);
           if(u16CalPwr_1>=0xff) 
           {
               u8CalPwr=0xff;
           }
           else 
           {
               u8CalPwr=(uint8_t)u16CalPwr_1;
           }
       }
       break;
       
       case U8_DIAG_5VREF:
       {
           /*u16CalPwr_1=(uint16_t)(((((uint32_t)fu16Power5VVoltage*10)*255)/6)/1023);*/
           u16CalPwr_1=(uint16_t)(((((uint32_t)Diag_fu16CalVoltPower5V)*255)/6)/1000);
           if(u16CalPwr_1>=0xff) 
           {
               u8CalPwr=0xff;
           }
           else 
           {
               u8CalPwr=(uint8_t)u16CalPwr_1;
           }
       }
       break;
       case U8_DIAG_5VPDT:
       {
           /*u16CalPwr_1=(uint16_t)(((((uint32_t)fu16Power5VVoltage*10)*255)/6)/1023);*/
           u16CalPwr_1=(uint16_t)(((((uint32_t)Diag_fu16CalASIC_PDT_5V_MON)*255)/6)/1000);
           if(u16CalPwr_1>=0xff) 
           {
               u8CalPwr=0xff;
           }
           else 
           {
               u8CalPwr=(uint8_t)u16CalPwr_1;
           }
       }
       break;
       case U8_DIAG_5VPDF:
       {
           /*u16CalPwr_1=(uint16_t)(((((uint32_t)fu16Power5VVoltage*10)*255)/6)/1023);*/
           u16CalPwr_1=(uint16_t)(((((uint32_t)Diag_fu16CalASIC_PDF_5V_MON)*255)/6)/1000);
           if(u16CalPwr_1>=0xff) 
           {
               u8CalPwr=0xff;
           }
           else 
           {
               u8CalPwr=(uint8_t)u16CalPwr_1;
           }
       }
       break;        
       
       default:
       break;
    }
    
    return u8CalPwr;
}

/*===========================================================
Version_1.2
1. Service Data 추가
   >. Vacuum sensor
===========================================================*/
uint16_t CDS_u16GetVacuumSnsrData(void)
{
    uint16_t u16Vacuum;
    
    /* Unit:KPA, Range:0~120, Conversion:X/10 */
    
    #if 0
        if(vacuum_1_10kpa>=1200)
        {
            u16Vacuum=1200;
        }
        else if(vacuum_1_10kpa<=0)
        {
            u16Vacuum=0;
        }
        else
        {
            u16Vacuum=(uint16_t)vacuum_1_10kpa;
        }
    #else
        u16Vacuum=0xFFFF;
    #endif
    
    return u16Vacuum;
}


/*===========================================================
Version_1.4
1. Service Data 추가
   >. Pedal travel sensor
===========================================================*/
uint8_t CDS_u16GetPedalTravelData(uint8_t u8SelPedal)
{
    #if ((__USE_PDT_SIGNAL==ENABLE) || (__USE_PDF_SIGNAL==ENABLE))
    uint16_t u16PedalTravel=0;
    #endif
    uint8_t  u8PedalReturn=0xFF;
    
    /* Unit:mm, Range:0~135, Conversion:X/10 */
    
    #if __PEDAL_SENSOR_TYPE!=NONE
    	#if __USE_PDT_SIGNAL==ENABLE
        if(u8SelPedal==U8_PEDAL0_PDT_RV)
        {
            if(Diag_pdt_pot1_1_10mm>=1350)
            {
                u16PedalTravel=1350;
            }
            else if(Diag_pdt_pot1_1_10mm<=0)
            {
                u16PedalTravel=0;
            }
            else
            {
                u16PedalTravel=(uint16_t)Diag_pdt_pot1_1_10mm;
            }
            
            #if 0
                if((AHBSUB2_ToChk.u1RxErrFlg==1)||(Diag_SEN_BUS_off_err_flg==0)||(Diag_SEN_hw_err_flg==0))
                {
                    u16PedalTravel=0;
                }
                else
                {
                    ;
                }
            #endif
            u8PedalReturn=(uint8_t)(u16PedalTravel/10);
        }
        #endif
        #if 0
        if(u8SelPedal==U8_PEDAL1_PDF_RV)
        {
            if(pdf_pot2_1_10mm>=1350)
            {
                u16PedalTravel=1350;
            }
            else if(pdf_pot2_1_10mm<=0)
            {
                u16PedalTravel=0;
            }
            else
            {
                u16PedalTravel=(uint16_t)pdf_pot2_1_10mm;
            }
            u8PedalReturn=(uint8_t)(u16PedalTravel/10);
        }
        #endif
    #else
        u8PedalReturn=0xFF;
    #endif
    
    return u8PedalReturn;
}

