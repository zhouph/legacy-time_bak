#ifndef CDC_DIAGHEADER_H
#define CDC_DIAGHEADER_H

typedef struct
{
    uBitType diag_mr_demand   :1;
    uBitType fast_mode        :1;
    uBitType mat_mr_demand    :1;
    uBitType diag_sci         :1;
	uBitType diag_ahb_sci     :1;/* AHB와 ESC Extend Mode를 나누기 위한 Flag */
    uBitType mat_mode         :1;
    uBitType mat_flash        :1;
    #if 1
    uBitType ResetSas         :1;
    uBitType StartChangeSas   :1;
    #endif
    uBitType cdu1AHBControlFlg :1;
}CDC_DIAG_t;

extern CDC_DIAG_t DIAG1;        
#define diag_sci_flg        DIAG1.diag_sci
#define Diag_ext_sci_flg    diag_sci_flg   

#define diag_mr_demand_flg  DIAG1.diag_mr_demand 
#define fast_mode_flg       DIAG1.fast_mode 
#define mat_mr_demand_flg   DIAG1.mat_mr_demand 

#define diag_ahb_sci_flg    DIAG1.diag_ahb_sci
#define mat_mode_flg        DIAG1.mat_mode 
#define mat_flash_flg       DIAG1.mat_flash
/* AHB Control Encoding Value */
#define cdu1AHBControlFlg   DIAG1.cdu1AHBControlFlg
#if ((__ECU==ESP_ECU_1)&&(__STEER_SENSOR_TYPE==CAN_TYPE))
#define ctu1ResetSas        DIAG1.ResetSas
#define ctu1StartChangeSas  DIAG1.StartChangeSas
#endif

#if 1
/*#if ((__ECU==ESP_ECU_1)&&(__HMC_CAN_VER!=CAN_HD_HEV))  */
    #define __FULL_ESC_SYSTEM 1
#else
    #define __FULL_ESC_SYSTEM 0
#endif

/* EV 차량 분리를 위한 매크로 처리 구문 */
#if 0
#if __HMC_CAN_VER==CAN_EV||__HMC_CAN_VER==CAN_FCEV
#define ELECTRIC_VEHICLE_ENABLE
#endif
#endif

#if 1
extern void Diag_10ms_EnterDiagOnCAN(void);
extern void CDCS_vInitDiagOnCAN(void);
#elif __DIAG_TYPE==K_LINE
extern void diag(void);
#endif
#endif 

