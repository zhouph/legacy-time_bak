/*

File    : CDiagOnCANService.c
Author  : PK Kang

Last Rev No : 1.0
Last Rev Date   : 2006/04/11 (YY/MM/DD)

SRS of CUSTOMER : HMC Diagnostic Communication on CAN Specification
                  

Target ECU  : MGH-40 ABS
              MGH-40 ESP

*/
#include "Asw_Types.h"
#include "Common.h"
#include "Car.par.h" // KHJ
#include "version.par.h"
#include "IoHwAb.h" /* KCLim : To get voltage */
//#include "SpiIf.h"    /* KCLim : To get voltage */
#include "../../NVM/HDR/NvMIf.h"/* msw : KCLim NvM*/
#if __DIAG_TYPE==CAN_LINE
//#include "../Failsafe/F_MasterHeader.h"
#include "CDC_DiagService.h"
#include "CDC_LinkData.h"
#include "CDC_DefineConstant.h"
#include "CDC_DefineServiceId.h"    
#include "CDC_DiagHeader.h"
#include "CDS_ReadRecordValue.h"
#include "CDS_ActControl.h"
//#include "Motor.h"
//#include "../Failsafe/F_VariableLinkSet.h"
//#include "../CAN/CAN_CHK/CCChkCan.h" //0919 PJS
//#include "../Com/SRC/CanChk.h" //0919 PJS 추가
//#include "PwrM.h" //0919 PJS 추가
//#include "SAL.h"
#include "ErP.h"
#include "Diag_Hndlr.h"
//#include "../System/WContlPwrSgnl.h"
/*#if __CAR_MAKER==SSANGYONG
#error "Ssang yong"
#elif __CAR_MAKER==HMC_KMC
#error "HKMC"
#else
#error "aaa"
#endif*/	// KHJ
//#include "../Failsafe/fs_macro_set.h"
//#include "../Failsafe/FEErrorProcessing.h"
//#include "../Failsafe/ErrorHandling.h"
//#include "../Failsafe/F_VariableLinkSet.h"
//#include "../Failsafe/FsErrDefine.h"
//#include "../Failsafe/FCPedalSenCheck.h" /*Pressure, Pedal Calibration NG 변수 적용*/
//#include "../Failsafe/FIAsicCheck.h"
//#include "../System/WContlEEP.h"
//#include "../Failsafe/FRValveRelayCheck.h"
//#include "../Failsafe/FTMotorCheck.h"
//#include "../Failsafe/FVSolAtvCheck.h"
//#include "../Failsafe/FWSpeedCheck.h"
//#include "../Failsafe/FGAXSensorCheck.h"
//#include "../Failsafe/ESC/FYLgYawHWCheck.h"
//#include "../Failsafe/F_VariableLinkSet.h"
/*#include "../System/WcontlSPI.h"*/
//#include "../Logic/LSABSEstLongAccSensorOffsetMon.h"
//#include "../Logic/AHB/LCAHBCallControl.h"	    	/* AHB Control Flag 사용을 위한 것 */
//#include "../Logic/AHB/LSAHBCallSensorSignal.h"     /* Pedel, Pressure Calibration Information을 위한 것 */
//#include "../System/WContlSenSig.h"
//#include "../System/WContlVariantCode.h"

//#include "../System/Drivers/WDrv_ADC.h"	/* HSH_COMPILE */	/* Need to be deleted */
//#include "../System/WContlPwrSgnl.h"
//#include "../System/WCommSubMCU.h"	/* HSH_COMPILE */	/* Need to be deleted */
#if __ECU==ESP_ECU_1
//#include "../Failsafe/ESC/FYLgYawSensorCalc.h"
#endif

#define __SUPPLIER_DTC_ENABLE 0
#define __G_SENSOR_TYPE 2
#define __4WD_VARIANT_CODE 0
#define __ECU 2
#define __FREEZE_FRAME_ENABLE 0

#define BOOT_ENTRY_CAN                          0x01
#define BOOT_ENTRY_KWP2000                      0x02
#define BOOT_ENTRY_BETWEEN_PROGRAMMING_SERV     0x10
#define BOOT_ENTRY_AFTER_SECURITY_SERV          0x20

void Diag_10ms_EnterDiagOnCAN(void);
void CDCS_vInitDiagOnCAN(void);

void CDCS_vRequestCheck(void);

static void CDCS_vReadWriteEEPROM(void);
static uint8_t CDCS_u8WriteEEPROM(void);

static void CDCS_vSetMec(void);


static void CDCS_vNegativeResponse(uint8_t REQID, uint8_t RSPCode);

static void CDCS_vSTDSRequest(void); /* Start diag session */

static void CDCS_vSPDSRequest(void); /* Stop Diagnostic Session */
static void CDCS_vSPDSResponse(void); 

static void CDCS_vExtendMode(void);
static void CDCS_vStandardMode(void);
static void CDCS_vSupplierMode(void);
static void CDCS_vProgrammingMode(void);

#if __DIAG_SPEC==SYMC_DIAG
static void CDCS_vEmissionMode(void);
uint8_t CDCS_vCheckFuncAddr(void);
#endif

static void CDCS_vShutDownMode(uint8_t cdu8_Mode);

static void CDCS_vDNMTRequest(void); /* Disable Normal Tx */
static void CDCS_vDNMTResponse(void);

static void CDCS_vENMTRequest(void); /* Enable Normal Tx */
static void CDCS_vENMTResponse(void);

static void CDCS_vTPRequest(void); /* Tester Present */
static void CDCS_vTPResponse(void); 

static void CDCS_vSARequest(void); /* Security Access */
static void CDCS_vSAResponseSeed(void); /* Response Seed */
static void CDCS_vSAResponseKey(void); /* Response Key */

static void CDCS_vERRequest(void); /* ECU Reset */
static void CDCS_vERResponse(void);
static void CDCS_vJumpExternalFunction(void);

static void CDCS_vRDBLIRequest(void); /* Read record value by local Id */
static void CDCS_vRDBLIResponse(void);
static void CDCS_vRDBLIResponsePREMIUM(void);
#if __ECU==ESP_ECU_1
static void CDCS_vRDBLIResponseESP(void);
#endif
static void CDCS_vReadDataWheelSpeed(void);
static void CDCS_vReadDataWarningLamp(void);
static void CDCS_vReadDataSwitch1(void);
static void CDCS_vReadDataRelay(void);
static void CDCS_vReadDataMotor(void);
static void CDCS_vReadDataValve(void);

/* Pedal, Pressure Sensor Calibration Funtion */
#if ((__PEDAL_TRAVLE_SENSOR_DIAG_CAL == 1) || (__PRESSURE_SENSOR_DIAG_CAL == 1) || (__MOTOR_POSITION_SENSOR_DIAG_CAL_OFFLINE == ENABLE))
	static void CDCS_vSetSnrCalStatus(CDC_SnsrCalOfs_t *SnsrCalPtr);
	static void CDCS_vCalSnsrReqChk(CDC_SnsrCalOfs_t *SnsrCalPtr, uint8_t CAL_INDEX);
	//////////////////////////////////////////////////////////F0119 Taeho: SWD V0
	static void CDCS_vCalMPSReqChk(CDC_SnsrCalOfs_t *SnsrCalPtr, uint8_t CAL_INDEX);
	/////////////////////////////////////////////////////////////////////////////
	static void CDCS_vCalSnsrResponse(uint8_t u8RspLid, uint8_t u8Result);
	static void CDCS_vClearEepRom(void);
	static void Diag_vCopySensorCalibrationInform(void);
	/* Pedal, Pressure Sensor Calibration Variable */
	static uint8_t cdu8SnsrCalFailStat;
#endif

#if ((__G_SENSOR_TYPE!=0)||(__4WD_VARIANT_CODE==1)||(__ECU==2))
static void CDCS_vReadDataGsnsr(void);
#endif

static void CDCS_vReadDataADvalues(void);
static void CDCS_vReadDataADvalues_Internal(void);
static void CDCS_vReadDataInternalError(void);
#if __SUPPLIER_DTC_ENABLE==1
    static void CDCS_vReadSIDTCRequest(void);
    static void CDCS_vReadSIDTCResponse(void);
#endif
static void CDCS_vReadDataTestComplete(void);

#if __FULL_ESC_SYSTEM==1
static void CDCS_vReadDataYaw(void);
static void CDCS_vReadDataSteering(void);
static void CDCS_vReadDataPressure(void);
static void CDCS_vReadDataSwitch2(void);
static void CDCS_vReadDataFunctionLamp(void);
#endif

static void CDCS_vReadDataMec(void);

static void CDCS_vRDTCBSRequest(void); /* Read DTC */
static void CDCS_vRDTCBSResponse(void);

#if __FREEZE_FRAME_ENABLE==1
static void CDCS_vRSODTCRequest(void); /* Read Freeze Frame */
static void CDCS_vRSODTCResponse(void); 
#endif

static void CDCS_vCDIRequest(void); /* Clear DTC */
static void CDCS_vCDIResponse(void);
#ifdef CD_VERIFY_CLR_DTC
static void CDCS_vChkClearDtc(void);
#endif
static void CDCS_vREIRequest(void); /* Read ECU ID */
static void CDCS_vREIResponse(void);
static void CDCS_vREIResponseInternalVer(void);
#ifdef DIAG_INLINE_CODING_ENABLE
static void CDCS_vREIResponseInLineCode(void);
#endif

static void CDCS_vSetTimeDrivingIO(void);
static void CDCS_vIOCBLIRequest(void); /* IO Control */
static void CDCS_vIOCBLIResponse(uint8_t ActCode);
static void CDCS_vIOCBLIResponseStop(uint8_t ActCode);
static void CDCS_vIOCBLISupportedPID(void);
static void CDCS_vIOCBLIActSingle(void);
static void CDCS_vIOCBLIActMultiple(void);

static void CDCS_vWMBARequest(void); /* Write Memory */
static void CDCS_vWMBAResponse(void);

#if ((__DIAG_CAL_AX_ENABLE==1)||(__DIAG_CAL_AX_SAS_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == 1) || (__PRESSURE_SENSOR_DIAG_CAL == 1))
static void CDCS_vClearEepRequest(void);
static void CDCS_vClearEepResponse(void);
#endif

static void CDCS_vRMBARequest(void); /* Read Memory */
static void CDCS_vRMBAResponse(void);
static void CDCS_vRMBAResponseFlash(void);

void CDCS_vStopActuation(void);
static void CDCS_vDriveActSet(void);
void CDCS_vSolSetOnTime(void);
static void CDCS_vCrcCheck(void);
static void CDCS_vGetSeed(void);
void CDCS_vInitSecurity(void);

#if 0
    static void CDCS_vSetTimeCalSas(void);
    static void CDCS_vCalSasResponse(void);
#endif

#if ((__SAS_CAL_TYPE==BY_DIAG)||(__DIAG_CAL_AX_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))
    static void CDCS_vCalSasRequest(void);
#endif

#if ((__DIAG_VARIANR_CODE_SET_SYS==1)||(defined(DIAG_INLINE_CODING_ENABLE)))
    static void CDCS_vResetVariantCode(void);
    static void CDCS_vRVCRequest(void);
    static void CDCS_vRVCResponse(void);
    uint8_t CDCS_u8ChkInLineCode(void);
#endif

#if __DIAG_CAL_AX_ENABLE==1
    static void CDCS_vSetCalAxSnsrStatus(void);
    static void CDCS_vCalAxSnsrRequest(void);
    static void CDCS_vCalAxSnsrResponse(uint8_t u8Result);
#endif 

#if __DIAG_SPEC==SYMC_DIAG
    static void CDCS_vNegativeResByFuncReq(uint8_t REQID, uint8_t RSPCode);
    static void CDCS_vWriteDataEEP(void);
    static void CDCS_vWDLIDRequest(void);
    static void CDCS_vWDLIDResponse(void);

    static void CDCS_vCNTDTCRequest(void);
    static void CDCS_vCNTDTCResponse(void);
#endif

#if defined(__FAILSAFE_SIGNAL_TEST)
static void CDCS_vFsTestRequest(void);
static void CDCS_vFsTestResponse(void);
#endif

static void CDCS_vAHBInputInterface(void);

/*Declare Constant For CAN Diag Service*/

#define CDCS_mSetBit(var,bit)      do{(var) |= (0x01 << (bit));}while(0)
#define CDCS_mClearBit(var,bit)    do{(var) &= (~(0x01 << (bit)));}while(0)

static void CDCS_vSendData(uint8_t Data, uint8_t Position);

/* Temp DTC Process */
uint16_t CDS_u16TempDtc(uint8_t index);

void WF_JumpCanDownload(void);

/*#define CD_VERIFY_CLR_DTC*/
#define U8_DIAG_AHB_EXTENDED_SPEED      320
#define U8_DIAG_MIN_SPEED      640  /* 640/64bit = 10Kph */
#define U8_DIAG_NOT_SUPPORTED  0xFF 

#if __DIAG_SPEC==CHERY_DIAG
    #define U8_DIAG_ACT_OFF      0x00
    #define U8_DIAG_ACT_ON       0x11 
#elif __DIAG_SPEC==SYMC_DIAG
    #define U8_DIAG_ACT_OFF      0x00
    #define U8_DIAG_ACT_ON       0x07
#else    
    #define U8_DIAG_ACT_OFF      0x11
    #define U8_DIAG_ACT_ON       0x00 
#endif

#define U16_SOL_DRV_MAX_DUTY      1000
#define U16_SOL_DRV_MAX_AHB_DUTY  2250  /* AHB Max Duty 값 변경에 따른 이원화 */
#define U16_MOT_DRV_MAX_DUTY      200

#define U8_IO_DRV_RSP_DELAY_TIME  20/*10*/
#define U8_JUMP_FUNC_DELAY        U8_DIAG_T_20MS
#define U8_CAL_SAS_WAIT_INIT      U8_DIAG_T_520MS/*U8_DIAG_T_510MS*/
#define U8_RCRRP_WAIT_TIME        U8_DIAG_T_20MS/*U8_DIAG_T_10MS*/

/* For Compile MSW */
#define 	U16_CALCVOLT_9V0	9000
#define 	U16_CALCVOLT_16V0	16000

/*Motor Calibration*/
#define MOTOR_CAL_COMPLETE	0x40u  /*0100 0000*/
#define MOTOR_CAL_ERROR		0x04u  /*0000 0100*/
/********************/

/************************************/

CDS_ABSSOL_t  CDABSFIRST, CDABSNEXT;
#if __ECU==ESP_ECU_1
CDS_ESPSOL_t  CDESPFIRST, CDESPNEXT;
#endif
CDS_AHBSOL_t  CDAHBFIRST, CDAHBNEXT;
CDS_LAMPRELAY_t CDLPRELAY;
CDS_ACTMISC_t CDMISC;
CDC_EEP_t   DCS3;
CDC_DRV_t   DCS4;
CDC_Serv_t  DCS5;
CDC_VC_t    DCS6;
CDC_MSPOfs_t  DCS9;

#if ((__ECU==ESP_ECU_1)&&(__STEER_SENSOR_TYPE==CAN_TYPE))
CDC_CalSas_t  DCS7;
#endif
CDC_DIAG_t  DIAG1;
CDC_AxOfs_t  DCS8;

CDC_SnsrCalOfs_t DiagCalPdl, DiagCalPrs; 	/* Pressure, Pedal Sensor Offset Address */ 

//////////////////////////////////////////////////////////F0119 Taeho: SWD_V0
CDC_SnsrCalOfs_t DiagCalMPS_Offline;
/////////////////////////////////////////////////////////////////////////////

extern const uint8_t VERSION[2][16];
extern uint8_t ELECTRIC_SW_DLS[1];
extern uint8_t MANDO_SW_Release_Counter;

uint8_t   cdcsu8TempB0;
uint8_t   cdu8Mec;
uint16_t  cdu16ActDrvTimer;
uint16_t  cdu16ActDrvTimerAHB;
uint16_t  cdcsu16SaveActTimer;
uint8_t   cdcsu8ActCode;
uint16_t  cdu16MotorDrvTimer;
uint16_t  cdu16ActDrvNextTimer;
uint16_t  cdu16DrvInterTimer;
uint16_t  cdcsu16ActTimer;
uint8_t   cdcsu8SaveActMultiTimer;
uint16_t  cdcsu16WriteAddress;
uint8_t   cdcsu8WriteData;
uint8_t   cdcsu8EepromData[4];
uint8_t   cdcsu8RequestID;
uint16_t  cdcsu16SecurityDelayTimer;
uint8_t   cdcsu8Seed;
uint8_t   cdcsu8CrcKey1;
uint8_t   cdcsu8CrcKey2;
uint8_t   cdcsu8JumpDelayTimer;

uint8_t MANDO_SW_Release_Counter;

uint8_t   cdcsu8CanDiagMode; 
uint8_t   cdcsu8RxFrameLength; 
uint8_t   cdcsu8SendData[100];
uint16_t  cdu16MotorDrvDuty;
uint16_t  cdu16SolenoidDrvDuty;
uint16_t  cdu16SolenoidDrvDutyAHB;
uint8_t   cdcsu8ActResult;
uint32_t  cdcsu32FlashAddr;

#if __DIAG_VARIANR_CODE_SET_SYS==1
uint8_t   cdcsu8WaitVcTimer;
#endif

#if __SAS_CAL_TYPE==BY_DIAG
uint8_t cdcsu8WaitCalSasTimer;
#endif

uint8_t  cdcsu8CalAxOfsStat;
uint16_t cdcsu16CalAxTimer;

uint8_t cdcsu8EcuIdOption;
#if __DIAG_SPEC==SYMC_DIAG
    #if __ECU==ABS_ECU_1
        uint8_t SYMC_HECU_HW_PN[8] ={"50005250"};
    #else
        uint8_t SYMC_HECU_HW_PN[8] ={"50005251"};
    #endif
    #define SYMC_DIAG_SPEC_REVISION  1
    uint8_t cdu8WrEepTrailCnt;
    uint8_t cdu8WrEepDataCnt;
    uint8_t cdu8WrEepBuffer[12];
#endif

#if defined(__FAILSAFE_SIGNAL_TEST)
uint8_t  cdu8FsMode_rx_ok, cdu8FsMode_2, cdu8FsMode_3, cdu8FsMode_4, cdu8FsMode_5, cdu8FsMode_6, cdu8FsMode_7;
#endif

#if defined(__LOGGER)
uint8_t cdu8TxLogEnableFlg;
#endif

/* For Compile MSW */
uint8_t	Diag_fu1AllControl;
uint8_t	Diag_min_speed;
uint8_t	Diag_standstill_flg;
uint8_t	Diag_fu16LoopCounter;
//uint16_t	Diag_fu16CalVoltVDD; /* KCLim : Change datatype uint8 --> uint16 */
uint8_t	Diag_fsu1PedalEOLOffsetWriteOk;
uint8_t	Diag_lcahbu1AHBOn;
uint8_t	Diag_fu1BLSErrDet;
uint8_t	Diag_fu1BLSSignal;
uint8_t	Diag_number_of_error;
uint8_t	Diag_clear_all_flg;
uint8_t	Diag_fcu1ATDriveType;
uint8_t	Diag_fcu1CVTDriveType;
uint16_t	Diag_fcu16EngRpm;

extern uint8_t	Diag_feu1RlWssErrFlg;
extern uint8_t	Diag_feu1RrWssErrFlg;
uint8_t	Diag_fcu8TPS;
uint8_t	Diag_fcu8GearPosition;
uint8_t	Diag_fu1BSSignal;
uint8_t	Diag_feu8TestComplete[ERROR_BYTE_NO];
uint8_t	Diag_wmu8EcuHwVersion;
uint8_t	Diag_asic_version;
uint8_t	Diag_wu1PedalPdt1EepEraseOk;
uint8_t	Diag_wu1PedalPdt2EepEraseOk;
uint8_t	Diag_wu1PressureBcEepEraseOk;
uint8_t	Diag_wu1PressurePsEepEraseOk;
uint8_t	Diag_fsu1PressureCalNGState;
uint8_t	Diag_lsahbu1PressEEPofsCalcEND;
uint8_t	Diag_lsahbu1PressEEPofsCalcOK;
uint8_t	Diag_wu1PressurePsEepWrtOk;
uint8_t	Diag_wu1PressureBcEepWrtOk;
uint8_t	Diag_fsu1PedalCalNGState;
uint8_t	Diag_lsahbu1PedalEEPofsCalcEND;
uint8_t	Diag_lsahbu1PedalEEPofsCalcOK;
uint8_t	Diag_wu1PedalPdt1EepWrtOk;
uint8_t	Diag_wu1PedalPdt2EepWrtOk;

uint8_t	Diag_fu1HDCSwitchSignal;

/* Motor Forced Drive */
uint16 MotorForcedDrv_CtrlMode;
//MotDutyInfo_t MotorForcedDrv;
/******************************/

/*Motor Calibration*/
uint8 fu8MotorCalState;/*Calibration 시 모터 여자 상태*/
/********************/

void CDCS_vInitDiagOnCAN(void)
{
	cdcsu8CanDiagMode=0x81;	//PJS
}

void Diag_10ms_EnterDiagOnCAN(void)
{
    uint16 Diag_fu16CalVoltVDD;
    #if defined(__FAILSAFE_SIGNAL_TEST)
    if(((cdclu8RxBuffer[0]&0xF0)==0)&&(cdclu1RxOkFlg==1))
    {
        cdcsu8RequestID=cdclu8RxBuffer[1];
        if(cdcsu8RequestID==U8_DIAG_FS_TEST_REQUEST_ID)
        {
            cdcsu8RxFrameLength=0x07;
            CDCS_vFsTestRequest();
        }
    }
    #endif
    CDCS_vAHBInputInterface();
    #if ((__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))      
    Diag_vCopySensorCalibrationInform(); /* Diag Pedal, Pressure Calibration Information by AHB Gen3 */
    #endif
    
    #if __PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE
        CDCS_vSetSnrCalStatus(&DiagCalPdl);
    #endif
    
    #if __PRESSURE_SENSOR_DIAG_CAL == ENABLE
        CDCS_vSetSnrCalStatus(&DiagCalPrs);
    #endif   

    if(Diag_fu1AllControl==0)

    if(1)
    {
        if(cdcsu1EnterCANDiag==1)
        { 
           CDCS_vReadWriteEEPROM();
		   #if ((__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))
            CDCS_vClearEepRom();
 		   #endif
            CDCS_vSetTimeDrivingIO();
           
            /* AHB Gen3 System Roll&Brake Test 과정을 위해 
               Extend Mode 상태에서 Ign On && 최소 Speed가 5Kph이상일 경우
               Extend Mode의 AHB 관련 Flag를 1로 Set 해줌
               du8MinSpeed과 du8IgnState에 du1AHBControlFlg 상응하는 변수 확인*/
	        if((Diag_min_speed>=U8_DIAG_AHB_EXTENDED_SPEED)||(Diag_HndlrBus.Diag_HndlrIgnOnOffSts == OFF)||(cdu1AHBControlFlg==1))
            {
                diag_ahb_sci_flg=0;
				Diag_HndlrBus.Diag_HndlrDiagAhbSci = 0;
            }
            else
            {
                if((Diag_standstill_flg==1)&&(cdu1AHBControlFlg==0))
                {
                    diag_ahb_sci_flg=1;
					Diag_HndlrBus.Diag_HndlrDiagAhbSci = 1;
                }
                else
                {   
                    ;
                }
            }
        }
        else
        {
            ;
        }
        #ifdef CD_VERIFY_CLR_DTC
            CDCS_vChkClearDtc();
        #endif
        
        #if __SAS_CAL_TYPE==BY_DIAG
            CDCS_vSetTimeCalSas();
        #endif

        #if __DIAG_CAL_AX_ENABLE==1
            #if defined(DIAG_HSA_CODING_ENABLE)
                if(ccu1HsaVcSetFlg==1)
                {
                    CDCS_vSetCalAxSnsrStatus();
                }
                else
                {
                    ;
                }
            #else

                CDCS_vSetCalAxSnsrStatus();
            #endif
        #endif

        CDCS_vJumpExternalFunction();
        
        #if ((__DIAG_VARIANR_CODE_SET_SYS==1)||(defined(DIAG_INLINE_CODING_ENABLE)))
            CDCS_vResetVariantCode();
        #endif

        
        CDCL_vLinkData();
    }
    else
    {
         cdclu8DiagStatus=7;
    }

    if(Diag_fu16LoopCounter>=U16_DIAG_T_10S)
    {
        cdcsu1SATimeOkflg=1;
    }
    else
    {
        ;
    }
    
    if(cdcsu16SecurityDelayTimer>0)
    {
         cdcsu16SecurityDelayTimer--;
    }
    else
    {
        ;
    }
    
    #if __DIAG_SPEC==SYMC_DIAG
        CDCS_vWriteDataEEP();
    #endif

    #if __SUPPLIER_DTC_ENABLE==1
        CDCS_vSetMec();
    #endif
    
    if((Diag_HndlrBus.Diag_HndlrIgnOnOffSts == OFF)/*||(ccu1TxDisableFlg == 1)*/)
    {
        cdu1DisabledNetFlg=1;
    }
    else
    {
        cdu1DisabledNetFlg=0;
    }   

    //Diag_fu16CalVoltVDD             = IoHwAb_GetVoltage(IOHWAB_IN_PORT_VDD_MON);
    Diag_fu16CalVoltVDD = Diag_HndlrBus.Diag_HndlrCEMon;
    if((Diag_fu16CalVoltVDD < U16_CALCVOLT_9V0)||(Diag_fu16CalVoltVDD > U16_CALCVOLT_16V0))
    {
        cdu1WrongSysVoltFlg=1;   
    }
    else
    {
        cdu1WrongSysVoltFlg=0;
    }

    /* for valve actuation on diag mode */
	#if 0 /* ~ IDB 15NZ ECU B*/
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FlIvReqData = cdu1FLNOFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FrIvReqData = cdu1FRNOFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RlIvReqData = cdu1RLNOFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RrIvReqData = cdu1RRNOFirstDrv;

    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FlOvReqData = cdu1FLNCFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FrOvReqData = cdu1FRNCFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RlOvReqData = cdu1RLNCFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RrOvReqData = cdu1RRNCFirstDrv;

    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = cdu1CUT1VFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = cdu1CUT2VFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PrimBalVlvReqData = cdu1APV01FirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SecdBalVlvReqData = cdu1APV02FirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SimVlvReqData = cdu1SIMVFirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.ChmbRespBalVlvReqData = cdu1RLV01FirstDrv;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SimPVlvReqData = cdu1RLV02FirstDrv;
	#endif
	/* IDB 15NZ ECU Quick C */ /* Asic TC.1 BAL */ /* Asic TC.2 PD */ /* MCU GD.Ch0 CV */ /* MCU GD.Ch1 RLV */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FlIvReqData = cdu1FLNOFirstDrv; /* Asic NO.4 FLNO */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FrIvReqData = cdu1FRNOFirstDrv; /* Asic NO.3 FRNO */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RlIvReqData = cdu1RLNOFirstDrv; /* Asic NO.1 RLNO */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RrIvReqData = cdu1RRNOFirstDrv; /* Asic NO.2 RRNO */

    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FlOvReqData = cdu1FLNCFirstDrv; /* Asic NC.3 FLNC */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FrOvReqData = cdu1FRNCFirstDrv; /* Asic Nc.1 FRNC */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RlOvReqData = cdu1RLNCFirstDrv; /* Asic NC.4 RLNC */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RrOvReqData = cdu1RRNCFirstDrv; /* Asic NC.2 RRNC */

    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = cdu1CUT1VFirstDrv; /* MCU GD.Ch3 CutP */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = cdu1CUT2VFirstDrv; /* MCU GD.Ch2 CutS */
    //Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PrimBalVlvReqData = cdu1APV01FirstDrv; 
#if 0 //comfile for PJS    
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SecdBalVlvReqData = cdu1APV02FirstDrv; 
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SimVlvReqData = cdu1SIMVFirstDrv;	   /* MCU GD.Ch4 Sim */
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.ChmbRespBalVlvReqData = cdu1RLV01FirstDrv;
    //Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SimPVlvReqData = cdu1RLV02FirstDrv;
	Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PrimCircuitVlvReqData = cdu1APV01FirstDrv;
	Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SecdCircuitVlvReqData = cdu1RLV02FirstDrv;
#endif //comfile for PJS	
	/* 
	SecdBalVlvReqData = PD
	ChmbRespBalVlvReqData = BAL 
	PrimCircVlvReqData = RLV
	SecdCircVlvReqData = CV
	*/

    Diag_HndlrBus.Diag_HndlrDiagClrSrs = clear_all_flg;
}


static void CDCS_vAHBInputInterface(void)
{   
    /*AHB Control Flag*/
    if(Diag_fsu1PedalEOLOffsetWriteOk==0)
    {
        cdu1AHBControlFlg=0;
    }
    else
    {
        if(Diag_lcahbu1AHBOn==1)
        {
            if(Diag_fu1BLSErrDet==0)
            {
                cdu1AHBControlFlg=Diag_fu1BLSSignal;
            }
            else
            {
                cdu1AHBControlFlg=1;
            }
        }
        else
        {
            cdu1AHBControlFlg=0;
        }
    }
}

static void CDCS_vReadWriteEEPROM(void)
{
    if(cdcsu1ReadProdFlg==1)
    {
        /* msw : KCLim NvM */if(WE_u8ReadEEP_Byte(cdcsu16WriteAddress,&cdcsu8EepromData[0],4)==1)
        {
            cdcsu1ReadProdFlg=0;
            CDCS_vRMBAResponse();
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }

    if(cdcsu1WriteProdFlg == 1)
    {
        if(cdcsu1WriteProdFirstFlg == 1)
        {
            if(CDCS_u8WriteEEPROM() == 1 )
            {
                cdcsu1WriteProdFirstFlg = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    
        /* msw : KCLim NvM*/ if(WE_u8ReadEEP_Byte(cdcsu16WriteAddress, (uint8_t*)&cdcsu8TempB0, 1)==1)
        {
            if(cdcsu8TempB0==cdcsu8WriteData)
            {
                CDCS_vWMBAResponse();
                cdcsu1WriteProdFlg = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
    
        /*===========================================================
        Version_1.1
        1. Ax snsr Offset 보정 서비스 신규 추가
        2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
           신규 서비스로 분리함.
        ===========================================================*/
    #if ((__DIAG_CAL_AX_ENABLE==1)||(__DIAG_CAL_AX_SAS_ENABLE==1))
        if(cdcsu1EraseEepFlg==1)
        {
             if((weu1EraseSector==0)&&(weu1WriteSector==0))
             {
                weu16Eep_addr=U16_EEP_AX_OFS_EEP_ADDR;
                weu1EraseSector=1;
                cdcsu1EraseEepFlg=0;
             }
             else
             {
                ;
             }
        }
        else
        {
            ;
        }
    #endif
}




static uint8_t CDCS_u8WriteEEPROM(void)
{
	uint8_t returnValue;
    /*msw : KCLim NvM*/if((weu1EraseSector==0)&&(weu1WriteSector==0))
    {
        if(WE_u8ReadEEP_Byte(cdcsu16WriteAddress&(uint16_t)0xfffc, &cdcsu8EepromData[0], 4)==1)
        {
            if(cdcsu8EepromData[cdcsu16WriteAddress&0x0003]!=cdcsu8WriteData)
            {
                cdcsu8EepromData[cdcsu16WriteAddress&0x0003]=cdcsu8WriteData;
                weu16EepBuff[0]=*(uint16_t*)(void *)&cdcsu8EepromData[0];
                weu16EepBuff[1]=*(uint16_t*)(void *)&cdcsu8EepromData[2];

                weu16Eep_addr=cdcsu16WriteAddress&(uint16_t)0xfffc;
                weu1WriteSector=1;

                returnValue=1;
            }
            else
            {
                returnValue=1;
            }
        }
        else
        {
            returnValue = 0;
    }
    }
    else
    {
        returnValue = 0;
    }
    return returnValue;
}


#if __DIAG_SPEC==SYMC_DIAG
void CDCS_vWriteDataEEP(void)
{
    if(cdu8WrEepDataCnt>0)
    {
        if(cdu8WrEepTrailCnt<cdu8WrEepDataCnt)
        {
            if((weu1EraseSector==0)&&(weu1WriteSector==0))
            {
                weu16EepBuff[0]=*(uint16_t*)&cdu8WrEepBuffer[0+cdu8WrEepTrailCnt];
                weu16EepBuff[1]=*(uint16_t*)&cdu8WrEepBuffer[2+cdu8WrEepTrailCnt];
                weu16Eep_addr=cdcsu16WriteAddress+cdu8WrEepTrailCnt;
                weu1WriteSector=1;
                cdu8WrEepTrailCnt+=4;
            }
            else
            {
                ;
            }
        }
        else
        {
            cdu8WrEepTrailCnt=0;
            cdu8WrEepDataCnt=0;
        }
    }
    else
    {
        ;
    }
}
#endif /* __DIAG_SPEC==SYMC_DIAG */ 

/*===========================================================
Version_1.3
1. BBOX 기능 구현
===========================================================*/

static void CDCS_vSetMec(void)
{
    uchar8_t meceep[4];
    
    if(cdu1ChkMecFlg==0)
    {
        if(cdu1ReadMecFlg==0)
        {
            /*msw  : KCLim NvM */ if((WE_u8ReadEEP_Byte(U16_EEP_BBOX_CNT_ADDR,&meceep[0],U16_EEP_BBOX_CNT_SIZE))==1)
            {
                cdu8Mec=meceep[0];
                
                if(cdu8Mec>U8_MEC_MAX_VAL) 
                {
                    cdu8Mec=U8_MEC_MAX_VAL;
                }
                else if(cdu8Mec==0) 
                {
                    cdu8Mec=0;
                }
                else
                {
                    cdu8Mec--;
                }
                
                cdu1ReadMecFlg=1;
            }
            else
            {
                ;
            }
        }
        else
        {
            meceep[0] = cdu8Mec;
            meceep[1] = 0;
            meceep[2] = 0;
            meceep[3] = 0;  
            /* msw  : KCLim NvM */
            if((weu1EraseSector==0)&&(weu1WriteSector==0))
            {
                weu16EepBuff[0]=*(uchar16_t*)(void *)&meceep[0];
                weu16EepBuff[1]=*(uchar16_t*)(void *)&meceep[2];
                weu16Eep_addr=U16_EEP_BBOX_CNT_ADDR;
                weu1WriteSector=1;
                
                cdu1ChkMecFlg=1;
            }
            else
            {
                ;
            }
        }
    }
    else
    {
        ;
    }
}


#if __DIAG_SPEC!=SYMC_DIAG
void CDCS_vRequestCheck(void)
{
    uint8_t u8IgnoreRequestFlg=0;
    if((cdclu8RxBuffer[0]&0xF0)==0)    
    {
        cdcsu8RequestID=cdclu8RxBuffer[1];
        
        /* For Carb OBD2 Test */
        #if __CAR_MAKER==HMC_KMC
            if(cdclu1RxFuncReqFlg==1)
            {
                cdclu1RxFuncReqFlg=0;
                
                if(cdcsu8RequestID<=0x0F)
                {
                    cdclu8DiagStatus=0;
                    cdclu1RequestChkFlg=0;
                    cdclu1RxOkFlg=0;
                    cdclu1TxEndFlg=0;
                    u8IgnoreRequestFlg=1;
                }
                else
                {
                    ;
                }
            }
            else
            {
                ;
            }
        #endif
        if(u8IgnoreRequestFlg==0)
        {                  
        if(cdcsu8CanDiagMode==U8_DIAG_STANDARD_MODE)
        {
            if(cdcsu8RequestID==U8_DIAG_STDS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vSTDSRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_SPDS_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x01;
                CDCS_vSPDSRequest();
            }
            #if __DIAG_SPEC!=CHERY_DIAG
            else if(cdcsu8RequestID==U8_DIAG_SA_REQUEST_ID) 
            {
                CDCS_vSARequest();
            }
            #endif
            else if(cdcsu8RequestID==U8_DIAG_REI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vREIRequest();
            }
            #if __DIAG_SPEC!=CHERY_DIAG
            else if(cdcsu8RequestID==U8_DIAG_ENMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vENMTRequest();
            }
            #endif  
            else if(cdcsu8RequestID==U8_DIAG_RDBLI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vRDBLIRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_RDTCBS_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x04;
                CDCS_vRDTCBSRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_CDI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x03;
                CDCS_vCDIRequest();
            }

            /*===========================================================
            Version_1.1
            1. Ax snsr Offset 보정 서비스 신규 추가
            2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
               신규 서비스로 분리함.
            ===========================================================*/
            #if ((__SAS_CAL_TYPE==BY_DIAG)||(__DIAG_CAL_AX_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE) ||\
				(__MOTOR_POSITION_SENSOR_DIAG_CAL_OFFLINE == ENABLE))
            else if(cdcsu8RequestID==U8_DIAG_CALSAS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x04;
                CDCS_vCalSasRequest();
            } 
            #endif
            #if ((__DIAG_VARIANR_CODE_SET_SYS==1)||(defined(DIAG_INLINE_CODING_ENABLE)))
            else if(cdcsu8RequestID==U8_DIAG_RVC_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x03; /* Variant Service DLC, Inline DLC 함수 Call 하는 곳에서 처리  */              
                CDCS_vRVCRequest();
            }
            #endif
            #if defined(__FAILSAFE_SIGNAL_TEST)
            else if(cdcsu8RequestID==U8_DIAG_FS_TEST_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x07;
                CDCS_vFsTestRequest();
            }
            #endif
            else 
            {
                #if __DIAG_SPEC==CHERY_DIAG
                switch(cdcsu8RequestID) 
                {
                    case U8_DIAG_DNMT_REQUEST_ID:
                    case U8_DIAG_ENMT_REQUEST_ID:
                    case U8_DIAG_TP_REQUEST_ID:
                    case U8_DIAG_IOCBLI_REQUEST_ID:
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                    break;
                  
                    default:
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
                    break;
                }
                #else 
                CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
                #endif
            }
        }                              
        else if(cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)    
        {
            if(cdcsu8RequestID==U8_DIAG_STDS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vSTDSRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_SPDS_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x01;
                CDCS_vSPDSRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_TP_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vTPRequest();
            } 
            #if __DIAG_SPEC!=CHERY_DIAG
            else if(cdcsu8RequestID==U8_DIAG_SA_REQUEST_ID) 
            {
                /*
                if(cdcsu1AccesModeFlg==0)
                {
                    cdcsu8RxFrameLength=0x02;
                }
                else
                {
                    cdcsu8RxFrameLength=0x04;
                }
                */
                CDCS_vSARequest();
            }
            #endif
            /*===========================================================
            Version_1.1
            1. ECU reset service를 Extended Mode에서 수행할 수 있도록 변경함.
            ===========================================================*/
            else if(cdcsu8RequestID==U8_DIAG_ER_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vERRequest();
            }  
            else if(cdcsu8RequestID==U8_DIAG_REI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vREIRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_DNMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vDNMTRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_ENMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vENMTRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_RDBLI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vRDBLIRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_RDTCBS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x04;
                CDCS_vRDTCBSRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_CDI_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x03;
                CDCS_vCDIRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_IOCBLI_REQUEST_ID) 
            {
                CDCS_vIOCBLIRequest();
            }
            /*===========================================================
            Version_1.1
            1. Ax snsr Offset 보정 서비스 신규 추가
            2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
               신규 서비스로 분리함.
            ===========================================================*/
            #if ((__SAS_CAL_TYPE==BY_DIAG)||(__DIAG_CAL_AX_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))
            else if(cdcsu8RequestID==U8_DIAG_CALSAS_REQUEST_ID)
            {
                
                cdcsu8RxFrameLength=0x04;
                CDCS_vCalSasRequest();
            }
            #endif
            #if ((__DIAG_VARIANR_CODE_SET_SYS==1)||(defined(DIAG_INLINE_CODING_ENABLE)))
            else if(cdcsu8RequestID==U8_DIAG_RVC_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x03; /* Variant Service DLC, Inline DLC 함수 Call 하는 곳에서 처리  */ 
                CDCS_vRVCRequest();
            }
            #endif
            else 
            {
                CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
            }
        }        
        else if(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)     
        {
            if(cdcsu8RequestID==U8_DIAG_STDS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vSTDSRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_SPDS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x01;
                CDCS_vSPDSRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_TP_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vTPRequest();
            }  
            else if(cdcsu8RequestID==U8_DIAG_SA_REQUEST_ID)
            {
                /*
                if(cdcsu1AccesModeFlg==0)
                {
                    cdcsu8RxFrameLength=0x02;
                }
                else
                {
                    cdcsu8RxFrameLength=0x04;
                }
                */
                CDCS_vSARequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_ER_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vERRequest();  
            } 
            else if(cdcsu8RequestID==U8_DIAG_REI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vREIRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_DNMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vDNMTRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_ENMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vENMTRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_RDBLI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vRDBLIRequest();
            }
            else 
            {
                CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
            }
        }            
        else if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
        {
            if(cdcsu8RequestID==U8_DIAG_STDS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vSTDSRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_SPDS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x01;
                CDCS_vSPDSRequest();
            }  
            else if(cdcsu8RequestID==U8_DIAG_TP_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vTPRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_SA_REQUEST_ID) 
            {
                /*
                if(cdcsu1AccesModeFlg==0)
                {
                    cdcsu8RxFrameLength=0x02;
                }
                else
                {
                    cdcsu8RxFrameLength=0x04;
                }
                */
                CDCS_vSARequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_ER_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vERRequest();
            }   
            else if(cdcsu8RequestID==U8_DIAG_REI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vREIRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_DNMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02; 
                CDCS_vDNMTRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_ENMT_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vENMTRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_RDBLI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x02;
                CDCS_vRDBLIRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_RMBA_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x05;
                CDCS_vRMBARequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_WMBA_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x06;
                CDCS_vWMBARequest();
            }

            /*===========================================================
            Version_1.1
            1. Ax snsr Offset 보정 서비스 신규 추가
            2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
               신규 서비스로 분리함.
            ===========================================================*/
            #if ((__DIAG_CAL_AX_ENABLE==1)||(__DIAG_CAL_AX_SAS_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))
            else if(cdcsu8RequestID==U8_DIAG_CLEEP_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x01;
                CDCS_vClearEepRequest();  
            }
            #endif  
            #if __SUPPLIER_DTC_ENABLE==1
            else if(cdcsu8RequestID==U8_DIAG_SMINTERNAL_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x01;
                CDCS_vReadSIDTCRequest();  
            }
            #endif              
            else if(cdcsu8RequestID==U8_DIAG_RDTCBS_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x04;
                CDCS_vRDTCBSRequest();
            } 
            else if(cdcsu8RequestID==U8_DIAG_CDI_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x03;
                CDCS_vCDIRequest();
            }
            else if(cdcsu8RequestID==U8_DIAG_IOCBLI_REQUEST_ID) 
            {
                CDCS_vIOCBLIRequest();
            }
            /*===========================================================
            Version_1.1
            1. Ax snsr Offset 보정 서비스 신규 추가
            2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
               신규 서비스로 분리함.
            ===========================================================*/
            #if ((__SAS_CAL_TYPE==BY_DIAG)||(__DIAG_CAL_AX_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))
            else if(cdcsu8RequestID==U8_DIAG_CALSAS_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x04;
                CDCS_vCalSasRequest();
            } 
            #endif
            #if __FREEZE_FRAME_ENABLE==1
            else if(cdcsu8RequestID==U8_DIAG_RSODTC_REQUEST_ID) 
            {
                cdcsu8RxFrameLength=0x03;
                CDCS_vRSODTCRequest();
            }
            #endif
            #if ((__DIAG_VARIANR_CODE_SET_SYS==1)||(defined(DIAG_INLINE_CODING_ENABLE)))
            else if(cdcsu8RequestID==U8_DIAG_RVC_REQUEST_ID)
            {
                cdcsu8RxFrameLength=0x03; /* Variant Service DLC, Inline DLC 함수 Call 하는 곳에서 처리  */ 
                CDCS_vRVCRequest();
            }
            #endif
            else 
            {
                CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
            }
        }    
        else
        {
            ;
        } 
        cdclu1RequestChkFlg=1; 
        }    
             
    }
    else   
    {
        cdclu8DiagStatus=7;
    }     

}

#else /* __DIAG_SPEC!=SYMC_DIAG */  

void CDCS_vRequestCheck(void)
{
    uint8_t u8SunFunctionId;
    
    if((cdclu8RxBuffer[0]&0xF0)==0)    
    {
        cdcsu8RequestID=cdclu8RxBuffer[1];
        u8SunFunctionId=cdclu8RxBuffer[2];
        
        switch(cdcsu8RequestID)
        {
            /* Start Diag Session : 02 10 XX */
            case U8_DIAG_STDS_REQUEST_ID:
                cdcsu8RxFrameLength=0x02;
                CDCS_vSTDSRequest();
            break;
            
            /* Stop Diag Session : 01 02 */
            case U8_DIAG_SPDS_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    cdcsu8RxFrameLength=0x01;
                    CDCS_vSPDSRequest();
                }
            break;
            
            /* ECU Reset : 02 11 01*/
            case U8_DIAG_ER_REQUEST_ID: 
                if(cdcsu8CanDiagMode!=U8_DIAG_STANDARD_MODE)
                {
                    cdcsu8RxFrameLength=0x02;
                    CDCS_vERRequest();
                }
                else
                {
                    CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }   
            break;
            
            
            /* Clear DTC : 03 14 FF 00 */
            case U8_DIAG_CDI_REQUEST_ID:
                if(cdcsu8CanDiagMode!=U8_DIAG_PROGRAM_MODE)
                {
                    cdcsu8RxFrameLength=0x03;
                    CDCS_vCDIRequest();
                }
                else
                {
                    CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }
            break;
            
            /* Read DTC By Status : 04 18 00 FF 00 */
            case U8_DIAG_RDTCBS_REQUEST_ID: 
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    if(cdcsu8CanDiagMode!=U8_DIAG_PROGRAM_MODE)
                    {
                        cdcsu8RxFrameLength=0x04;
                        CDCS_vRDTCBSRequest();
                    }
                    else
                    {
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                    }   
                }
            break;
            
            /* Read ID : 02 1A XX */
            case U8_DIAG_REI_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    cdcsu8RxFrameLength=0x02;
                    CDCS_vREIRequest();
                }
            break;
            
            /* Read Data By LID : 02 21 XX */
            case U8_DIAG_RDBLI_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    cdcsu8RxFrameLength=0x02;
                    CDCS_vRDBLIRequest();
                }
            break;
            
            /* Disable Normal Comm. : 02 28 XX */
            case U8_DIAG_DNMT_REQUEST_ID: 
                if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                {
                    cdcsu8RxFrameLength=0x02;
                    CDCS_vDNMTRequest();
                }
                else
                {
                    CDCS_vNegativeResByFuncReq(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }
            break;
            
            /* Enable Normal Comm. : 02 29 XX */
            case U8_DIAG_ENMT_REQUEST_ID: 
                if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                {
                    cdcsu8RxFrameLength=0x02;
                    CDCS_vENMTRequest();
                }
                else
                {
                    CDCS_vNegativeResByFuncReq(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }
            break;
            
            
            /* Security Access : 02 27 01 or 04 24 02 XX XX */
            case U8_DIAG_SA_REQUEST_ID:
                if((cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                {
                    CDCS_vSARequest();
                }
                else
                {
                    CDCS_vNegativeResByFuncReq(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }
            break; 
            
            /* IO control : 03 30 XX XX or 06 30 F0 XX XX XX XX */
            case U8_DIAG_IOCBLI_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                    {
                        CDCS_vIOCBLIRequest();
                    }
                    else
                    {
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                    }
                }
            break;
            
            /* Cal SAS : 04 31 01 00 XX */
            #if __SAS_CAL_TYPE==BY_DIAG
                case U8_DIAG_CALSAS_REQUEST_ID: 
                    if(cdclu1RxFuncReqFlg==1)
                    {
                        cdclu1RxFuncReqFlg=0;
                        cdclu8DiagStatus=7;
                    }
                    else
                    {
                        cdcsu8RxFrameLength=0x04;
                        CDCS_vCalSasRequest();
                    }
                break;
            #endif
            
            /* Write Data VC : 03 3B 04 FF */ 
            case U8_DIAG_RVC_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                    {    
                        switch(u8SunFunctionId)
                        {
                            
                            #if __DIAG_VARIANR_CODE_SET_SYS==1
                                case U8_DIAG_RVC_ROUTINE_CONT:
                                    cdcsu8RxFrameLength=0x03;
                                    CDCS_vRVCRequest();
                                    
                                    /*
                                    if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                                    {
                                        cdcsu8RxFrameLength=0x03;
                                        CDCS_vRVCRequest();
                                    }
                                    else
                                    {
                                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                                    } 
                                    */
                                break;
                            #endif
                            
                            case U8_DIAG_ID_SYMC_OP_CODE_FP:
                            case U8_DIAG_ID_SYMC_OP_DATA_FP:
                            case U8_DIAG_ID_SYMC_OP_BOOT_FP:
                                CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_INCORRECT_BYTE_COUNT);
                            break;
                            
                            default:
                                CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                            break;   
                         }
                    }
                    else
                    {
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                    }          
                }
            break;
            
                        
            /* Tester Present : 02 3E XX */
            case U8_DIAG_TP_REQUEST_ID:
                if(cdu1EmissionTestMode==0)
                {
                    cdcsu8RxFrameLength=0x02;
                    CDCS_vTPRequest();
                }
                else
                {
                    CDCS_vNegativeResByFuncReq(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }
            break;
            
            
            /* Control DTC Setting : 05 85 XX FF 00 XX */
            case U8_DIAG_CNTDTC_REQUEST_ID: 
                if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                {
                    cdcsu8RxFrameLength=0x05;
                    CDCS_vCNTDTCRequest();
                }
                else
                {
                    CDCS_vNegativeResByFuncReq(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                }
            break;
            
            
            /* Read EEPROM : 05 23 00 00 XX 01 */
            case U8_DIAG_RMBA_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
                    {
                        cdcsu8RxFrameLength=0x05;
                        CDCS_vRMBARequest();
                    }
                    else
                    {
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
                    }
                }
            break;
            
            /* Write EEPROM : 06 3D 00 00 XX 01 XX */
            case U8_DIAG_WMBA_REQUEST_ID:
                if(cdclu1RxFuncReqFlg==1)
                {
                    cdclu1RxFuncReqFlg=0;
                    cdclu8DiagStatus=7;
                }
                else
                {
                    if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
                    {
                        cdcsu8RxFrameLength=0x06;
                        CDCS_vWMBARequest();
                    }
                    else
                    {
                        CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
                    }
                }
            break;
            
            /* Clear Ax Offset */
            #if ((__DIAG_CAL_AX_ENABLE==1)||(__DIAG_CAL_AX_SAS_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))
                case U8_DIAG_CLEEP_REQUEST_ID:
                    if(cdclu1RxFuncReqFlg==1)
                    {
                        cdclu1RxFuncReqFlg=0;
                        cdclu8DiagStatus=7;
                    }
                    else
                    {
                        if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
                        {
                            cdcsu8RxFrameLength=0x01;
                            CDCS_vClearEepRequest();
                        }
                        else
                        {
                            CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_SERVICE_NOT_SUPPORTED);
                        }
                    }
                break;
            #endif
            
            default:
                CDCS_vNegativeResByFuncReq(cdcsu8RequestID,U8_DIAG_SERVICE_NOT_SUPPORTED);      
            break; 
        }       
        cdclu1RequestChkFlg=1;
    }
    else   
    {
        if(cdclu8DiagStatus==10)
        {
            cdcsu8RequestID=cdclu8RxReqFrame[0];
                          
            switch(cdcsu8RequestID)
            {
                case U8_DIAG_RVC_REQUEST_ID:
                    if(cdclu1RxFuncReqFlg==1)
                    {
                        cdclu1RxFuncReqFlg=0;
                        cdclu8DiagStatus=7;
                    }
                    else
                    {
                        if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE))
                        {
                            cdcsu8RxFrameLength=0x0C;
                            CDCS_vWDLIDRequest();
                        }
                        else
                        {
                            CDCS_vNegativeResponse(cdcsu8RequestID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                        }
                    }
                break;
                
                default:
                    CDCS_vNegativeResByFuncReq(cdcsu8RequestID,U8_DIAG_SERVICE_NOT_SUPPORTED);
                break;
            } 
            cdclu1RequestChkFlg=1;
        }   
        else    
        {
            cdclu8DiagStatus=7;
        }
    }     

}



uint8_t CDCS_vCheckFuncAddr(void)
{
    if (cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_REQUIRED) 
    {
        if(cdclu1RxFuncReqFlg==1)
        {
            cdclu1RxFuncReqFlg=0;
            cdclu8DiagStatus=7;
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else if (cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_NO_REQUIRED) 
    {
        if(cdclu1RxFuncReqFlg==0)
        {
            cdclu8DiagStatus=7;
            return 1;
        }
        else
        {
            cdclu1RxFuncReqFlg=0;
            return 0;
        }
    }
    else
    {
        return 0;
    }
}
#endif /* __DIAG_SPEC!=SYMC_DIAG */

static void CDCS_vSendData(uint8_t Data, uint8_t Position)
{
    cdcsu8SendData[Position] = Data; 
}

static void CDCS_vNegativeResponse(uint8_t REQID, uint8_t RSPCode)
{
    CDCS_vSendData(U8_DIAG_NEGATIVE_RESPONSE_ID, 0); 
    CDCS_vSendData(REQID, 1);
    CDCS_vSendData(RSPCode, 2);
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
}


#if __DIAG_SPEC==SYMC_DIAG 
static void CDCS_vNegativeResByFuncReq(uint8_t REQID, uint8_t RSPCode)
{
    if(cdclu1RxFuncReqFlg==1)
    {
        cdclu1RxFuncReqFlg=0;
        cdclu8DiagStatus=7;
    }
    else
    {
        CDCS_vSendData(U8_DIAG_NEGATIVE_RESPONSE_ID, 0); 
        CDCS_vSendData(REQID, 1);
        CDCS_vSendData(RSPCode, 2);
        
        cdclu16ServiceDataSum=0x03;
        cdclu1SendReadyFlg=1;
    }
}
#endif


static void CDCS_vSTDSRequest(void)
{
    uint8_t u8RxFrameLen, u8RxFrameLenChk;
    
    u8RxFrameLen=cdclu8RxBuffer[0]&0x0F;
    
    #if ((__DIAG_SPEC==CHERY_DIAG)||(__DIAG_SPEC==SYMC_DIAG))
        if(cdclu8RxBuffer[2]==U8_DIAG_PROGRAM_MODE)
        {
            if((u8RxFrameLen!=0x02)&&(u8RxFrameLen!=0x03))
            {
                u8RxFrameLenChk=0;
            }
            else
            {
                u8RxFrameLenChk=1;
            }   
        }
        else
        {
            if(u8RxFrameLen!=0x02)
            {
                u8RxFrameLenChk=0;
            }
            else
            {
                u8RxFrameLenChk=1;
            }
        }
    #else
        if((u8RxFrameLen!=0x02)&&(u8RxFrameLen!=0x03))
        {
            u8RxFrameLenChk=0;
        }
        else
        {
            u8RxFrameLenChk=1;
        }
    #endif
    
    #if __DIAG_SPEC==SYMC_DIAG
        if(u8RxFrameLenChk==1)
        {
            if(cdu1EmissionTestMode==1)
            {
                CDCS_vNegativeResByFuncReq(U8_DIAG_STDS_REQUEST_ID,U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
                return;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    #endif
    
    if(u8RxFrameLenChk==0)
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResByFuncReq(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        #else
            CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        #endif
    }
    else
    {
        if(cdclu8RxBuffer[2]==U8_DIAG_STANDARD_MODE) 
        {
             CDCS_vStandardMode();
        }
        else if(cdclu8RxBuffer[2]==U8_DIAG_EXTENDED_MODE) 
        {
            if(Diag_min_speed<U8_DIAG_MIN_SPEED)
            {
                CDCS_vExtendMode();
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }

        }
        else if(cdclu8RxBuffer[2]==U8_DIAG_PROGRAM_MODE)
        {
            #if __DIAG_SPEC!=CHERY_DIAG
                if(Diag_min_speed<U8_DIAG_MIN_SPEED)
                {
                    CDCS_vProgrammingMode();
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                }
                
                if(cdclu8RxBuffer[3]==U8_DIAG_SA_DELAY_BKDOOR)
                {
                    cdcsu1SATimeOkflg=1;
                }
                else
                {
                    ;
                }
            #else
                if((cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)||(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE))
                {
                    CDCS_vProgrammingMode();
                    
                    cdcsu1SATimeOkflg=1;
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                }
            #endif
        }
        else if(cdclu8RxBuffer[2]==U8_DIAG_SUPPLIER_MODE) 
        {
            if(Diag_min_speed<U8_DIAG_MIN_SPEED)
            {
                CDCS_vSupplierMode(); 
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }
        }
        #if __DIAG_SPEC==SYMC_DIAG
        else if(cdclu8RxBuffer[2]==U8_DIAG_EMISSION_MODE) 
        {
            if(Diag_min_speed<U8_DIAG_MIN_SPEED)
            {
                CDCS_vEmissionMode(); 
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }
        }
        #endif
        #if __CAR_MAKER == HMC_KMC
        else if((cdclu8RxBuffer[2]==U8_DIAG_SHUTDOWN_READY)||(cdclu8RxBuffer[2]==U8_DIAG_SHUTDOWN_REQUEST))
        {
            CDCS_vShutDownMode(cdclu8RxBuffer[2]);
        }
        #endif
        else 
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResByFuncReq(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #else
                CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #endif
        }
    } 
}

static void CDCS_vStandardMode(void)
{
    CDCS_vSendData(U8_DIAG_STDS_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_STANDARD_MODE, 1);
    cdcsu8CanDiagMode=U8_DIAG_STANDARD_MODE;
	/* Standard 모드 진입할 때, 구분 Flag 0으로 Set 시킴 */
    diag_ahb_sci_flg=0;
    cdcsu1EnterCANDiag=diag_sci_flg=0;
	Diag_HndlrBus.Diag_HndlrDiagSci = 0;
	Diag_HndlrBus.Diag_HndlrDiagAhbSci = 0;
    CDCS_vInitSecurity();
    // ccu1TxDisableFlg=0;
    #if __DIAG_SPEC==SYMC_DIAG
        cdu1InhibitFsChk=0;
        
        if(cdclu1RxFuncReqFlg==1)
        {
            cdclu1RxFuncReqFlg=0;
            cdclu8DiagStatus=7;
        }
        else
        {
            cdclu16ServiceDataSum=0x02;
            cdclu1SendReadyFlg=1;
        }
    #else
         cdclu16ServiceDataSum=0x02;
         cdclu1SendReadyFlg=1;
    #endif 
}

static void CDCS_vExtendMode(void)
{
    CDCS_vSendData(U8_DIAG_STDS_RESPONSE_ID, 0); 
    CDCS_vSendData(U8_DIAG_EXTENDED_MODE, 1);
    cdcsu8CanDiagMode=U8_DIAG_EXTENDED_MODE;
	/* AHB Gen3 Extended Mode 진입 조건 변경 5Kph 이하, IGN On, AHB Control이 아닐때.*/
    if((Diag_min_speed<=U8_DIAG_AHB_EXTENDED_SPEED) && (Diag_HndlrBus.Diag_HndlrIgnOnOffSts == ON) && (cdu1AHBControlFlg==0))
    {
        diag_ahb_sci_flg=1;
		Diag_HndlrBus.Diag_HndlrDiagAhbSci = 1;
    }
    cdcsu1EnterCANDiag=diag_sci_flg=1;
	Diag_HndlrBus.Diag_HndlrDiagSci = 1;
    CDCS_vInitSecurity();
    cdclu16ServiceDataSum=0x02;
    cdclu1SendReadyFlg=1;
}

static void CDCS_vSupplierMode(void)
{
    CDCS_vSendData(U8_DIAG_STDS_RESPONSE_ID, 0); 
    CDCS_vSendData(U8_DIAG_SUPPLIER_MODE, 1);
    cdcsu8CanDiagMode=U8_DIAG_SUPPLIER_MODE;
    /* Supplier Mode 진입 시 구분 Flag 1로 Set */
	diag_ahb_sci_flg=1;
    cdcsu1EnterCANDiag=diag_sci_flg=1;
	Diag_HndlrBus.Diag_HndlrDiagAhbSci = 1;
	Diag_HndlrBus.Diag_HndlrDiagSci = 1;
    CDCS_vInitSecurity();
    cdclu16ServiceDataSum=0x02;
    cdclu1SendReadyFlg=1;
}

static void CDCS_vProgrammingMode(void)
{
    CDCS_vSendData(U8_DIAG_STDS_RESPONSE_ID, 0); 
    CDCS_vSendData(U8_DIAG_PROGRAM_MODE, 1);
    cdcsu8CanDiagMode=U8_DIAG_PROGRAM_MODE;
    /* AHB Gen3 Extended Mode 진입 조건 변경 5Kph 이하, IGN On, AHB Control이 아닐때.*/
    if((Diag_min_speed<=U8_DIAG_AHB_EXTENDED_SPEED) && (Diag_HndlrBus.Diag_HndlrIgnOnOffSts == ON) && (cdu1AHBControlFlg==0))
    {
        diag_ahb_sci_flg=1;
		Diag_HndlrBus.Diag_HndlrDiagAhbSci = 1;
    }
    cdcsu1EnterCANDiag=diag_sci_flg=1;
	Diag_HndlrBus.Diag_HndlrDiagSci = 1;
    CDCS_vInitSecurity();
    cdclu16ServiceDataSum=0x02;
    cdclu1SendReadyFlg=1;
}

#if __DIAG_SPEC==SYMC_DIAG
static void CDCS_vEmissionMode(void)
{
    if(cdclu1RxFuncReqFlg==0)
    {
        CDCS_vSendData(U8_DIAG_STDS_RESPONSE_ID, 0); 
        CDCS_vSendData(U8_DIAG_EMISSION_MODE, 1);
        cdcsu8CanDiagMode=U8_DIAG_STANDARD_MODE;
        diag_sci_flg=1;
		Diag_HndlrBus.Diag_HndlrDiagSci = 1;
        cdu1EmissionTestMode=1;
        CDCS_vInitSecurity();
        cdclu16ServiceDataSum=0x02;
        cdclu1SendReadyFlg=1;
    }
    else
    {
        cdclu1RxFuncReqFlg=0;
        cdclu8DiagStatus=7;
    }
}
#endif

static void CDCS_vShutDownMode(uint8_t cdu8_Mode)
{
    #ifdef DIAG_ECU_SHUTDOWN_ENABLE
        if(cdu8_Mode==U8_DIAG_SHUTDOWN_READY)
        {
            if(Diag_HndlrBus.Diag_HndlrIgnOnOffSts == ON)
            {
                cdu1SysShutDownReady=1;
                cdclu8DiagStatus=7;
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }   
        }
        else if(cdu8_Mode==U8_DIAG_SHUTDOWN_REQUEST)
        {
            if((Diag_HndlrBus.Diag_HndlrIgnOnOffSts == OFF)&&(cdu1SysShutDownReady==1))
            {
                cdu1SysShutDownReq=1;
                cdclu8DiagStatus=7;
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_STDS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }   
        }
        else
        {
            ;
        }
    #else
        cdclu8DiagStatus=7;
    #endif
}

static void CDCS_vSPDSRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
         CDCS_vNegativeResponse(U8_DIAG_SPDS_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    { 
        if((cdcsu1WriteProdFlg==1)||(cdcsu1ActuationFlg==1)||(cdcsu1ActuationMultiFlg==1))
        {
            CDCS_vNegativeResponse(U8_DIAG_SPDS_REQUEST_ID, U8_DIAG_GENERAL_REJECT);
        }
        else 
        {
            #if __DIAG_SPEC==SYMC_DIAG
            if(cdu1EmissionTestMode==1)
            {
                CDCS_vNegativeResponse(U8_DIAG_SPDS_REQUEST_ID, U8_DIAG_NOT_SUPPORT_IN_ACTIVE_DCM);
            }
            else    
            {
                CDCS_vSPDSResponse();
            }
            #else
                CDCS_vSPDSResponse();
            #endif
        }
    }   
}

static void CDCS_vSPDSResponse(void)
{
    CDCS_vSendData(U8_DIAG_SPDS_RESPONSE_ID, 0);
    cdclu8TerminationStatus=U8_DIAG_STOP_DIAG_SESSION;
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vTPRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F))
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResByFuncReq(U8_DIAG_TP_REQUEST_ID,U8_DIAG_INCORRECT_BYTE_COUNT);
        #else
            CDCS_vNegativeResponse(U8_DIAG_TP_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        #endif
    }
    else
    {
        #if __DIAG_SPEC==SYMC_DIAG 
            if(CDCS_vCheckFuncAddr()==1) 
            {
                return;
            }
            else
            {
                ;
            }
        #endif 
    
        if (cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_REQUIRED) 
        {
            CDCS_vTPResponse();
        }
        else if (cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_NO_REQUIRED) 
        {
            cdclu8DiagStatus=7;
        }   
        else 
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResByFuncReq(U8_DIAG_TP_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #else
                CDCS_vNegativeResponse(U8_DIAG_TP_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #endif
        }
    } 
}

static void CDCS_vTPResponse(void)
{
    CDCS_vSendData(U8_DIAG_TP_RESPONSE_ID, 0);
    #if ((__DIAG_SPEC==CHERY_DIAG)||(__DIAG_SPEC==SYMC_DIAG))
        cdclu16ServiceDataSum=0x01;
    #else
        CDCS_vSendData(U8_DIAG_TP_ZERO_SUB_FUNC, 1); 
        cdclu16ServiceDataSum=0x02;
    #endif
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vERRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F))
    {
          CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
        {
            if(cdclu8RxBuffer[2]==U8_DIAG_POWER_ON_RESET)
            {
                CDCS_vERResponse();
            }
            else 
            {
                CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        }
        #if __DIAG_SPEC!=SYMC_DIAG
        /*===========================================================
        Version_1.1
        1. ECU reset service를 Extended Mode에서 수행할 수 있도록 변경함.
        ===========================================================*/
        else if(cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)
        {
            if(cdclu8RxBuffer[2]==U8_DIAG_POWER_ON_RESET)
            {
                CDCS_vERResponse();
            }
            else 
            {
                CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        }
        else if(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)    
        {
            if(cdcsu1SecurityOkFlg==1)
            {
                if(cdclu8RxBuffer[2]==U8_DIAG_POWER_ON_RESET)
                {
                    CDCS_vERResponse();
                }
                else 
                {
                    CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                }
            }
            else
            { 
                CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_SECURITY_ACCESS_DENIED);
            }
        }
        #else
        else if((cdcsu8CanDiagMode==U8_DIAG_EXTENDED_MODE)||(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE))
        {
            if(cdclu8RxBuffer[2]==U8_DIAG_POWER_ON_RESET)
            {
                CDCS_vERResponse();
            }
            else 
            {
                CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        }
        #endif
        else
        {
            CDCS_vNegativeResponse(U8_DIAG_ER_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
        }
            
    }
}


static void CDCS_vERResponse(void)
{
    CDCS_vSendData(U8_DIAG_ER_RESPONSE_ID, 0);
  
    if(cdclu8RxBuffer[2]==U8_DIAG_POWER_ON_RESET)
    {
        cdclu8TerminationStatus=U8_DIAG_POWER_ON_RESET;
        cdcsu1WaitingFuncFlg=1;
    }
    else
    {
        ;
    }
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;  
}

static void CDCS_vJumpExternalFunction(void)
{
    if(cdcsu1WaitingFuncFlg==1)
    {
        if(cdclu8TerminationStatus==U8_DIAG_POWER_ON_RESET)
        {
            if(cdcsu8JumpDelayTimer==U8_JUMP_FUNC_DELAY)
            {
                cdclu8TerminationStatus=0;
                cdcsu8JumpDelayTimer=0;
                cdcsu1WaitingFuncFlg=0;
                CDCL_vReset();
            }
            else
            {
                cdcsu8JumpDelayTimer++;
            }
        }
        else if(cdclu8TerminationStatus==U8_DIAG_JUMP_PRGM_MODE)
        {
            if(cdcsu8JumpDelayTimer==U8_JUMP_FUNC_DELAY)
            {
                cdclu8TerminationStatus=0;
                cdcsu8JumpDelayTimer=0;
                cdcsu1WaitingFuncFlg=0;
                WF_JumpCanDownload();	//DIAG PJS
            }
            else
            {
                cdcsu8JumpDelayTimer++;
            }   
        }
        else
        {
            cdcsu1WaitingFuncFlg=0;
        }
    }
    else
    {
        ;
    }
}

            

static void CDCS_vRDTCBSRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
          CDCS_vNegativeResponse(U8_DIAG_RDTCBS_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if(cdclu8RxBuffer[2]==U8_DIAG_RDTCBS_STATUS_OF_DTC)
        {
            if(((cdclu8RxBuffer[3]==U8_DIAG_GROUP_OF_DTC_HIGH_BYTE)||(cdclu8RxBuffer[3]==U8_DIAG_ALL_GROUP_OF_DTC_HIGH_BYTE))&&(cdclu8RxBuffer[4]==U8_DIAG_GROUP_OF_DTC_LOW_BYTE))
            {
                 CDCS_vRDTCBSResponse();
            }
            else 
            {
                 CDCS_vNegativeResponse(U8_DIAG_RDTCBS_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
            }   
        }           
        else 
        {
             CDCS_vNegativeResponse(U8_DIAG_RDTCBS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
        }
    } 
}



static void CDCS_vRDTCBSResponse(void)
{
    uint8_t  DTCNum, du8length, i, u8ErrBuff, u8ErrInfo;
    uint16_t u16ErrCode;
   /* msw  : KCLim NvM */
    DTCNum = number_of_error;
    du8length = 2 + (3*DTCNum);
    
    CDCS_vSendData(U8_DIAG_RDTCBS_RESPONSE_ID, 0);  
    CDCS_vSendData(DTCNum, 1);
    
    if(DTCNum != 0)
    {
        for(i=0; i <= DTCNum; i++)
        {
            u8ErrBuff=ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_ERRCODE];

            u16ErrCode=CDS_u16TempDtc(i); /* Temp */
            /*u16ErrCode=CDS_u16AnalizeHmcDtc(u8ErrBuff);*/
            u8ErrInfo=(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD]&(uint8_t)0xF0)|(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_FTB]&0x0F);
            
            CDCS_vSendData(((uint8_t)(u16ErrCode>>8)), (2+(3*i))); // DTC High Bytes
            CDCS_vSendData(((uint8_t)u16ErrCode), (3+(3*i)));      // DTC Low Bytes
            CDCS_vSendData(u8ErrInfo, (4+(3*i))); // SOD
            
        }
    }
    else
    {
        ;
    }

    cdclu16ServiceDataSum=du8length;
    cdclu1SendReadyFlg=1; 
}

/* Temporary DTC Process */
uint16_t CDS_u16TempDtc(uint8_t index)
{
    uint16_t dtc = 0;

    dtc = (ErrBuffer[index].ErrMember[U8_DTC_INFORM_ERRCODE] |
           (ErrBuffer[index].ErrMember[U8_DTC_RESERVED] << 8));

    return dtc;
}

#if __FREEZE_FRAME_ENABLE==1
static void CDCS_vRSODTCRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
          CDCS_vNegativeResponse(U8_DIAG_RSODTC_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        CDCS_vRSODTCResponse();                
    }
}   
                
static void CDCS_vRSODTCResponse(void)
{
    uint8_t i_cnt;
    uint8_t u8FailureDataCnt;
    uint8_t ByteCnt=0;
    uint8_t ReqMatchedFlg=0;
    uint8_t u8ErrBuff=0;
    uint8_t u8ErrInfo=0;
    uint16_t u16RxErrCode,u16ErrCode; 
        
    CDCS_vSendData(U8_DIAG_RSODTC_RESPONSE_ID, ByteCnt); 
    ByteCnt++;
    
    u16RxErrCode=(uint16_t)(((uint16_t)cdclu8RxBuffer[2]<<8)|(uint16_t)cdclu8RxBuffer[3]);
    
    if(number_of_error!=0)
    {
        for ( i_cnt = 0 ; i_cnt < number_of_error ; i_cnt++ )
        {
            u8ErrBuff=ErrBuffer[feu8ErrorPointer[i_cnt]].ErrMember[U8_DTC_INFORM_ERRCODE];
            
            u16ErrCode=CDS_u16TempDtc(i); /* Temp */
            /*u16ErrCode=CDS_u16AnalizeHmcDtc(u8ErrBuff); /*Call DTC No.*/*/
            
            if(u16ErrCode==u16RxErrCode)
            {
                CDCS_vSendData(0x01, ByteCnt);
                ByteCnt++; 
                u8ErrInfo=(uint8_t)((ErrBuffer[feu8ErrorPointer[i_cnt]].ErrMember[U8_DTC_INFORM_SOD]&(uint8_t)0xF0)|(ErrBuffer[feu8ErrorPointer[i_cnt]].ErrMember[U8_DTC_INFORM_FTB]&0x0F));    
                CDCS_vSendData((uint8_t)((u16ErrCode>>8)&0xFF), ByteCnt); /* DTC High Bytes */
                ByteCnt++;
                CDCS_vSendData((uint8_t)(u16ErrCode&0xFF), ByteCnt);      /* DTC Low Bytes */
                ByteCnt++;
                CDCS_vSendData(u8ErrInfo, ByteCnt); /* SOD */
                ByteCnt++;
                
                for(u8FailureDataCnt=(U8_DTC_INFORM_SOD+1); u8FailureDataCnt <U8NUM_DTC_INFORM;u8FailureDataCnt++)
                {
                    CDCS_vSendData(ErrBuffer[feu8ErrorPointer[i_cnt]].ErrMember[u8FailureDataCnt],ByteCnt);
                    ByteCnt++;      
                }
                ReqMatchedFlg=1;
                break;       
            }         
        }
    }
    if(ReqMatchedFlg==0)
    {
        CDCS_vSendData(0x00, ByteCnt); 
        ByteCnt++;
    }
    cdclu16ServiceDataSum=ByteCnt;
    cdclu1SendReadyFlg=1;    
}
#endif

#ifdef CD_VERIFY_CLR_DTC
static void CDCS_vChkClearDtc(void)
{
    static uint16_t cdu16_WaitCdtEndTimer=0;
    
    if(cdu1ClearDtcReq==1)
    {
        if(Diag_clear_all_flg==0)
        {
            if(cdclu8TerminationStatus==0)
            {
            CDCS_vCDIResponse();
            cdu1ClearDtcReq      =0;
            cdu16_WaitCdtEndTimer=0;    
        	}
        }
        else
        {
            cdu16_WaitCdtEndTimer=cdu16_WaitCdtEndTimer+1;
            
            if(cdu16_WaitCdtEndTimer==U8_RCRRP_WAIT_TIME)
            {
                cdclu8TerminationStatus=U8_TX_RESPONSE_NO_REQ;
                CDCS_vNegativeResponse(U8_DIAG_CDI_REQUEST_ID, U8_DIAG_REQ_CORRECT_RSP_PENDING);    
            }
            else if(cdu16_WaitCdtEndTimer==U16_DIAG_T_5S)
            {
                CDCS_vNegativeResponse(U8_DIAG_CDI_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                cdu1ClearDtcReq      =0;
                cdu16_WaitCdtEndTimer=0;    
            }
            else
            {
                ;   
            }
        }
    }
    else
    {
        ;
    }
}
#endif


static void CDCS_vCDIRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
          CDCS_vNegativeResponse(U8_DIAG_CDI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
            
        if(((cdclu8RxBuffer[2]==U8_DIAG_GROUP_OF_DTC_HIGH_BYTE)||(cdclu8RxBuffer[2]==U8_DIAG_ALL_GROUP_OF_DTC_HIGH_BYTE))&&(cdclu8RxBuffer[3]==U8_DIAG_GROUP_OF_DTC_LOW_BYTE))
        {
            CDCS_vStopActuation();
            clear_all_flg  =1;
  
            if(cdu8Mec!=0) 
            {
                cdu1ClearDtcSupplier=1;
            }
            else
            {
                if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
                {
                    cdu1ClearDtcSupplier=1;
                }
                else
                {
                    ;
                }
            }
            
            #ifdef CD_VERIFY_CLR_DTC
                cdu1ClearDtcReq=1;
            #else
                CDCS_vCDIResponse();
            #endif
        }
        else 
        {
             CDCS_vNegativeResponse(U8_DIAG_CDI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
        }   
                    
    }

}


static void CDCS_vCDIResponse(void)
{
    CDCS_vSendData(U8_DIAG_CDI_RESPONSE_ID, 0); 
    
    if(cdclu8RxBuffer[2]==U8_DIAG_ALL_GROUP_OF_DTC_HIGH_BYTE)
    {
        CDCS_vSendData(U8_DIAG_ALL_GROUP_OF_DTC_HIGH_BYTE, 1);
    }
    else
    {
        CDCS_vSendData(U8_DIAG_GROUP_OF_DTC_HIGH_BYTE, 1);
    }
    
    CDCS_vSendData(U8_DIAG_GROUP_OF_DTC_LOW_BYTE, 2);
  
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1;
}   

static void CDCS_vRDBLIRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
          CDCS_vNegativeResponse(U8_DIAG_RDBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
        {
            if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_STANDARD_LOCAL_ID)
            {
                CDCS_vRDBLIResponse();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_PREMIUM_LOCAL_ID)
            {
                CDCS_vRDBLIResponsePREMIUM();
            }
            #if __FULL_ESC_SYSTEM==1
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_SUPPLIER_LOCAL_ID) 
                {
                    CDCS_vRDBLIResponseESP();
                }
            #endif /* ESP_ECU_1 */
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_WHEEL_SPEED) 
            {
                CDCS_vReadDataWheelSpeed();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_WARNING_LAMP) 
            {
                CDCS_vReadDataWarningLamp();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_SWITCH_1) 
            {
                CDCS_vReadDataSwitch1(); 
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_RELAY) 
            {
                CDCS_vReadDataRelay();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_MOTOR) 
            {
                CDCS_vReadDataMotor();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_VALVE) 
            {
                CDCS_vReadDataValve();
            }
            #if ((__G_SENSOR_TYPE!=NONE)||(__ECU==ESP_ECU_1)||(__4WD_VARIANT_CODE==ENABLE))
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_G_SNSR) 
            {
                 CDCS_vReadDataGsnsr();
            }
            #endif /*((__G_SENSOR_TYPE!=NONE)||(__ECU==ESP_ECU_1)||(__4WD_VARIANT_CODE==ENABLE))*/
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_AD) 
            {
                CDCS_vReadDataADvalues();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_AD2) 
            {
                CDCS_vReadDataADvalues_Internal();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_INTERNAL_ERROR) 
            {
                CDCS_vReadDataInternalError();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_TEST_COMPLETE)
            {
                CDCS_vReadDataTestComplete();
            }
            #if __FULL_ESC_SYSTEM==1
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_YAW) 
                {
                    CDCS_vReadDataYaw();
                }
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_STEERING) 
                {
                    CDCS_vReadDataSteering();
                }    
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_PRESSURE) 
                {
                    CDCS_vReadDataPressure();
                }
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_SWITCH_2) 
                {
                    CDCS_vReadDataSwitch2();
                }
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_FUNC_LAMP) 
                {
                    CDCS_vReadDataFunctionLamp();
                }
            #endif /* ESP_ECU_1 */
            /*===========================================================
            Version_1.3
            1. BBOX 기능 구현
               >. MEC Read Service 추가
            ===========================================================*/
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_MEC)
            {
                CDCS_vReadDataMec();
            }   
            else 
            {
                CDCS_vNegativeResponse(U8_DIAG_RDBLI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        }                                                           
        else
        {
            if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_STANDARD_LOCAL_ID) 
            {
                CDCS_vRDBLIResponse();
            }
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_PREMIUM_LOCAL_ID)
            {
                CDCS_vRDBLIResponsePREMIUM();
            }
            #if __FULL_ESC_SYSTEM==1
                else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_SUPPLIER_LOCAL_ID) 
                {
                    CDCS_vRDBLIResponseESP();
                }
            #endif /* ESP_ECU_1 */
            else if(cdclu8RxBuffer[2]==U8_DIAG_RDBLI_WHEEL_SPEED) 
            {
                CDCS_vReadDataWheelSpeed();
            }
            else 
            {
                CDCS_vNegativeResponse(U8_DIAG_RDBLI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        }
    }
}

 

static void CDCS_vRDBLIResponse(void)
{
    uint8_t  u8Gear, u8DiagGear, u8Voltage, u8DsRefV;
    uint16_t u16LG, u16Yaw, u16Steer;
    
    #if (defined (HMC_CAN)||defined(GMDW_CAN)) && !defined(ELECTRIC_VEHICLE_ENABLE)
        uint8_t  u8TPS;
        uint16_t    u16EngRpm;
    #endif
        
    uint8_t  cu8_5vSenPwrEquip=0;
    uint32_t cdu32_SIDtemp=0;
   
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_STANDARD_LOCAL_ID, 1);
    
    #if (__G_SENSOR_TYPE==ANALOG_TYPE)
        cu8_5vSenPwrEquip=1;
    #endif
    
    /* 2~5 : Supported PID */
    #if __ECU==ESP_ECU_1
        #if defined (HMC_CAN)||defined(GMDW_CAN)
        if((Diag_fcu1ATDriveType==1)||(Diag_fcu1CVTDriveType==1))
        {
            cdcsu1AtmChkFlg=1;
        }
        else
        {
            ;
        }
        #endif
    #endif
    
    /* 6~7 : Engine RPM */
    #if __ECU==ESP_ECU_1
        #if ((defined (HMC_CAN)||defined(GMDW_CAN)) && !defined(ELECTRIC_VEHICLE_ENABLE))
            cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_RPM;
            u16EngRpm=(uint16_t)(Diag_fcu16EngRpm*4);
            CDCS_vSendData((uint8_t)u16EngRpm, 6);        
            CDCS_vSendData((uint8_t)(u16EngRpm>>8), 7); 
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 6);      
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 7); 
        #endif 
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 6);       
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 7);
    #endif    

    //if(ccu14wdVcSetFlg==0)
    if(1)
    {
        #if __DRIVE_TYPE==REAR_WHEEL_DRV
        if((feu1FlWssErrFlg==0)&&(feu1FrWssErrFlg==0))
        {
            u8DsRefV=CDS_u8CalcVehicleSpeed(U8_FRONT_MEAN_VREF);
        }
        #else
        if((Diag_feu1RlWssErrFlg==0)&&(Diag_feu1RrWssErrFlg==0))
        { 
            u8DsRefV=CDS_u8CalcVehicleSpeed(U8_REAR_MEAN_VREF);
        }
        #endif
        else
        {
            u8DsRefV=CDS_u8CalcVehicleSpeed(U8_4WHL_MEAN_VREF);
        }
    }
    else
    {
        u8DsRefV=CDS_u8CalcVehicleSpeed(U8_4WHL_MEAN_VREF);    
    }
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_VEHSPD;
    CDCS_vSendData(u8DsRefV, 8); 
     

    /*Absolute Throttle Position sensor*/
    #if __ECU==ESP_ECU_1
        /* EV 차량 TPS 값 잘못 출력에 관하여 EV 차량일 경우 Not Supported 출력 */
        #if (defined (HMC_CAN)||defined(GMDW_CAN)) && !defined(ELECTRIC_VEHICLE_ENABLE)
            /*
            if(Diag_fcu8TPS<0x20)
            {
                u8TPS=0;
            }
            else if(Diag_fcu8TPS==0xFF)
            {           
                u8TPS=U8_DIAG_NOT_SUPPORTED;
            }
            else
            {
                u8TPS=((((Diag_fcu8TPS-0x20)*100)/213)*255)/100;
            }*/
            cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_TPS;
            u8TPS=(uint8_t)(((uint16_t)Diag_fcu8TPS*255)/100);
            
            CDCS_vSendData(u8TPS, 9);
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 9);
        #endif
    #else 
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 9);
    #endif   

    /* Shift lever position*/
    #if __ECU==ESP_ECU_1
        #if defined (HMC_CAN)||defined(GMDW_CAN)
        if(cdcsu1AtmChkFlg==1)
        {
            u8DiagGear=0x00;
            u8Gear=(uint8_t)(Diag_fcu8GearPosition);
            if((u8Gear==0x00)||(u8Gear==0x06))
            {
                CDCS_mSetBit(u8DiagGear,0);
            }
            else if (u8Gear==0x07)
            {
                CDCS_mSetBit(u8DiagGear,1);
            }
            else if (u8Gear==0x05)
            {
                CDCS_mSetBit(u8DiagGear,2);
            }
            else if (u8Gear==0x03)
            {
                CDCS_mSetBit(u8DiagGear,4);
            }
            else if (u8Gear==0x02)
            {
                CDCS_mSetBit(u8DiagGear,5);
            }
            else if (u8Gear==0x01)
            {
                CDCS_mSetBit(u8DiagGear,6);
            }
            else if (u8Gear==0x08)
            {
                CDCS_mSetBit(u8DiagGear,7);
            }
            else
            {
                ;
            }
            cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_SHFTPOS;
            CDCS_vSendData(u8DiagGear, 10);
        }
        else
        {
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 10);
        }
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 10);
        #endif      
    #else 
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 10);
    #endif 
    
    /* Battery voltage: $xx * 16/255 ( V )*/
    u8Voltage=CDS_u8GetPowerAdData(U8_DIAG_BATT);
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_BATTERY;
    CDCS_vSendData(u8Voltage, 11);
       
    /* 5 volt reference = $xx * 6/255 V*/
    if(cu8_5vSenPwrEquip==1)
    {
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_5VPWR;
        u8Voltage=CDS_u8GetPowerAdData(U8_DIAG_5VREF);  
        CDCS_vSendData(u8Voltage, 12);
    }
    else
    {
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 12);
    }

    /* FL/FR/RL/RR Wheel Speed */
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_WSPD_FL;
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_FL_WSS_RV), 13);
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_WSPD_FR;
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_FR_WSS_RV), 14);
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_WSPD_RL;
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_RL_WSS_RV), 15);
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_WSPD_RR;
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_RR_WSS_RV), 16);
   
    
    /* Steering sensor */
    #if ((__FULL_ESC_SYSTEM==1)&&(__STEER_SENSOR_TYPE==ANALOG_TYPE))
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_ANLG_SAS_ST;
        CDCS_vSendData(CDS_u8GetSteerPhaseData(), 17);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 17);      
    #endif

    /* Longitudinal (4WD only) G sensor &
       Lateral (ESP) G Sensor */
    #if ((__G_SENSOR_TYPE!=NONE)||(__4WD_VARIANT_CODE==ENABLE))
        //if(ccu1AxSnsrSetFlg==1)
        if(1)
        {
            cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_AX;
            CDCS_vSendData(CDS_u8GetAxSnsrData(), 18);
        }
        else
        {
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 18);
        }
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 18);
    #endif
       
    CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 19);
    
    /* Warning Lamp */
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_WLP;
    CDCS_vSendData(CDS_u8GetLampStatus(U8_BASIC_LAMP), 20);    
                      
    /* Switch */
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_SWITCH;
    CDCS_vSendData(CDS_u8GetSwitchStatus(U8_BASIC_SWT), 21);

    
    /* Relay */
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_MOTVVLP;
    CDCS_vSendData(CDS_u8GetActuatorStatus(U8_RELAY_STATUS), 22);    

    /* Motor */
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_MOTOR;
    CDCS_vSendData(CDS_u8GetActuatorStatus(U8_MOT_STATUS), 23);    

    /* ABS Valve */
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_ABSIN;
    CDCS_vSendData(CDS_u8GetActuatorStatus(U8_INVALVE_STATUS), 24);    
    cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_ABSOUT;
    CDCS_vSendData(CDS_u8GetActuatorStatus(U8_OUTVALVE_STATUS), 25);    

    /* TC/ESV Valve */
    #if __AHB_GEN3_SYSTEM==DISABLE
    #if __ECU==ESP_ECU_1  
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_ESCVV;
        CDCS_vSendData(CDS_u8GetActuatorStatus(U8_ESPVALVE_STATUS), 26);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 26);
    #endif /* #if __ECU==ESP_ECU_1 */
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 26);
    #endif /* #if __ECU==ESP_ECU_1 */
    
    /*===========================================================
    Version_1.1
    1. RSV valves에 대한 service data 출력 추가
    ===========================================================*/
    /* RSV Valve */
    #if __DIAG_PREMIUM_14VV_ECU==1
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_USVHSV;
        CDCS_vSendData(CDS_u8GetActuatorStatus(U8_RSVVALVE_STATUS), 27);    
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 27);
    #endif
    
    #if __FULL_ESC_SYSTEM==1
        #if __STEER_SENSOR_TYPE==CAN_TYPE
            cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_SAS;
            u16Steer=CDS_u16GetSteeringData();
            CDCS_vSendData((uint8_t)(u16Steer>>8), 28);
            CDCS_vSendData((uint8_t)u16Steer, 29);
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 28);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 29);
        #endif
    
        /* Lateral G Sensor */   
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_CANAY;  
        u16LG=CDS_u16GetYawAySnsrData(U8_AY_RV);  
        CDCS_vSendData((uint8_t)(u16LG>>8), 30);     
        CDCS_vSendData((uint8_t)u16LG, 31);        
    
        /* Yaw Sensor */
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_CANYAW; 
        u16Yaw=CDS_u16GetYawAySnsrData(U8_YAW_RV);     
        CDCS_vSendData((uint8_t)(u16Yaw>>8), 32);  
        CDCS_vSendData((uint8_t)u16Yaw, 33);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 28);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 29);  
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 30);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 31);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 32);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 33);
    #endif
    #if __AHB_GEN3_SYSTEM==DISABLE 
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_MCP;        
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_POS_MP_RV), 34);
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_NEG_MP_RV), 35);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 34);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 35);
    #endif
    
    /*===========================================================
    Version_1.3
    1. Service Data 추가
       >. Pedal travel sensor
    2. Pedal travel sensor 위치 변경
    ===========================================================*/
    #if __PEDAL_SENSOR_TYPE!=NONE
		#if __USE_PDT_SIGNAL==ENABLE
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_PDT;        
        CDCS_vSendData((uint8_t)(CDS_u16GetPedalTravelData(U8_PEDAL0_PDT_RV)), 36);  
		#else
			CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 36); /* HAS Not Supported. */                
		#endif
		#if __USE_PDF_SIGNAL==ENABLE          
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_PDF;        
        CDCS_vSendData((uint8_t)(CDS_u16GetPedalTravelData(U8_PEDAL1_PDF_RV)), 37);
    #else
        	CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 37); /* HAS Not Supported. */                
        #endif
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 36); /* HAS Not Supported. */                
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 37); /* Longitudinal Sesnor Not Supported. */
    #endif
    
    /*===========================================================
    Version_1.1
    1. HDC Lamp 및 Switch에 대하 service data 출력 추가
    2. Service data 위치 변경 
    ===========================================================*/
    if(CDS_u8GetSwitchStatus(U8_HDC_STATUS)!=0xFF)
    {
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_DBC;        
        CDCS_vSendData(CDS_u8GetSwitchStatus(U8_HDC_STATUS), 38); /* HDC Lamp & Swtich Status*/     
    }
    else
    {
        CDCS_vSendData(0xFF, 38); /* HDC Lamp & Swtich Status*/
    }
          
    #if ((__FULL_ESC_SYSTEM==1) && (__STEER_SENSOR_TYPE==ANALOG_TYPE))
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID01_ANALG_SAS;    
        u16Steer=CDS_u16GetSteeringData();
        CDCS_vSendData((uint8_t)(u16Steer>>8), 39); 
        CDCS_vSendData((uint8_t)u16Steer, 40);      
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 39);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 40);
    #endif /* __ECU==ESP_ECU_1 */

    CDCS_vSendData(((uint8_t)cdu32_SIDtemp), 2);
    CDCS_vSendData(((uint8_t)(cdu32_SIDtemp>>8)), 3);
    CDCS_vSendData(((uint8_t)(cdu32_SIDtemp>>16)), 4);
    CDCS_vSendData(((uint8_t)(cdu32_SIDtemp>>24)), 5);
    
    cdclu16ServiceDataSum=41;
    cdclu1SendReadyFlg=1; 
    
}


static void CDCS_vRDBLIResponsePREMIUM(void)
{
    uint32_t cdu32_SIDtemp=0;
	uint8_t cdu8SupportVacuumtemp=0;
    
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_PREMIUM_LOCAL_ID, 1);
                
    #if __FL_PRESS==ENABLE 
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_WHL_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_FL_POS_MP_RV), 6); 
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 6);
    #endif  
    
    #if __FR_PRESS==ENABLE 
		cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_WHL_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_FR_POS_MP_RV), 7); 
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 7);
    #endif  
    
    #if __RL_PRESS==ENABLE 
		cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_WHL_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_RL_POS_MP_RV), 8); 
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 8);
    #endif  
    
    #if __RR_PRESS==ENABLE 
		cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_WHL_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_RR_POS_MP_RV), 9); 
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 9);
    #endif    
                
    #if __VACUUM_SENSOR_ENABLE==ENABLE
        #ifdef CC_LVBA_CAN_ENABLE
            if(ccu1LvbaEmsTypeFlg==1)
            {
                cdu8SupportVacuumtemp++;
            }
            else
            {
                ;
            }
        #else
            cdu8SupportVacuumtemp++;
        #endif
    #endif   
    
    if(cdu8SupportVacuumtemp!=0)
    {
		cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_VACUUM;
        CDCS_vSendData((uint8_t)((CDS_u16GetVacuumSnsrData())>>8), 10);
        CDCS_vSendData((uint8_t)(CDS_u16GetVacuumSnsrData()), 11);
    }
    else
    {
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 10);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 11);
    }
    
    #if __AHB_GEN3_SYSTEM==ENABLE
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_PRMR_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_PRM_POS_MP_RV), 12);
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_PRM_NEG_MP_RV), 13);
        
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_SCND_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_SEC_POS_MP_RV), 14);
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_SEC_NEG_MP_RV), 15);
        
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_HPA_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_HPA_POS_MP_RV), 16);
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_HPA_NEG_MP_RV), 17);
        
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_SIM_PRES;
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_SIM_POS_MP_RV), 18);
        CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_SIM_NEG_MP_RV), 19);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 12);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 13);  
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 14);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 15);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 16);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 17);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 18);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 19);
    #endif  
    
    #if __AHB_GEN3_SYSTEM==ENABLE   
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_AHBVALVE1;     
        CDCS_vSendData(CDS_u8GetActuatorStatus(U8_AHBVALVE1_STATUS), 20);
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_AHBVALVE2;     
        CDCS_vSendData(CDS_u8GetActuatorStatus(U8_AHBVALVE2_STATUS), 21);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 20);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 21);
    #endif

    /* AHB/RBC WLamp Spec 추가 */
    #if __AHB_GEN3_SYSTEM==ENABLE  
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_AHBWLP;
        CDCS_vSendData(CDS_u8GetLampStatus(U8_AHB_LAMP), 22);  
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 22);
    #endif
    #if __PEDAL_SENSOR_TYPE==ANALOG_TYPE
		#if __USE_PDT_SIGNAL==ENABLE
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_5VPDT;        
        CDCS_vSendData((uint8_t)(CDS_u8GetPowerAdData(U8_DIAG_5VPDT)), 23);
		#else
			CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 23); /* HAS Not Supported. */                
		#endif
		#if __USE_PDF_SIGNAL==ENABLE          
        cdu32_SIDtemp|=U8_DIAG_SPPRTD_LID02_5VPDF;        
        CDCS_vSendData((uint8_t)(CDS_u8GetPowerAdData(U8_DIAG_5VPDT)), 24);
        #else
        	CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 24); /* HAS Not Supported. */                
        #endif
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 23); /* HAS Not Supported. */                
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 24); /* Longitudinal Sesnor Not Supported. */
    #endif     

    CDCS_vSendData(((uint8_t)cdu32_SIDtemp), 2);
    CDCS_vSendData(((uint8_t)(cdu32_SIDtemp>>8)), 3);
    CDCS_vSendData(((uint8_t)(cdu32_SIDtemp>>16)), 4);
    CDCS_vSendData(((uint8_t)(cdu32_SIDtemp>>24)), 5);    
        
    cdclu16ServiceDataSum=25;
    cdclu1SendReadyFlg=1; 
}

#if __FULL_ESC_SYSTEM==1
static void CDCS_vRDBLIResponseESP(void)
{
    uint16_t u16YAW;
        
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_SUPPLIER_LOCAL_ID, 1);    
    
    CDCS_vSendData(0x03, 2);
    CDCS_vSendData(0, 3);
    CDCS_vSendData(0, 4);
    CDCS_vSendData(0, 5);
    
    /* Yaw Rate */
    u16YAW=CDS_u16GetYawAySnsrData(U8_YAW_RV); /* YAW */
    CDCS_vSendData((uint8_t)u16YAW, 6); 
    CDCS_vSendData((uint8_t)(u16YAW>>8) , 7);   
        
    /* Pressure */
     CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_POS_MP_RV), 8);
                  
    cdclu16ServiceDataSum=0x09;
    cdclu1SendReadyFlg=1;      
}
#endif


static void CDCS_vReadDataWheelSpeed(void)
{
    uint8_t   u8DsRefV;
            
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_WHEEL_SPEED, 1);
    
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_FL_WSS_RV), 2);
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_FR_WSS_RV), 3);
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_RL_WSS_RV), 4);
    CDCS_vSendData(CDS_u8GetWheelSpeed(U8_RR_WSS_RV), 5);
    
    //if(ccu14wdVcSetFlg==0)
    if(1)
    {
        #if __DRIVE_TYPE==REAR_WHEEL_DRV
        if((feu1FlWssErrFlg==0)&&(feu1FrWssErrFlg==0))
        {
            u8DsRefV=CDS_u8CalcVehicleSpeed(U8_FRONT_MEAN_VREF);
        }
        #else
        if((Diag_feu1RlWssErrFlg==0)&&(Diag_feu1RrWssErrFlg==0))
        { 
            u8DsRefV=CDS_u8CalcVehicleSpeed(U8_REAR_MEAN_VREF);
        }
        #endif
        else
        {
            u8DsRefV=CDS_u8CalcVehicleSpeed(U8_4WHL_MEAN_VREF);
        }
    }
    else
    {
        u8DsRefV=CDS_u8CalcVehicleSpeed(U8_4WHL_MEAN_VREF);    
    }
    
    CDCS_vSendData(u8DsRefV, 6); 
              
    cdclu16ServiceDataSum=0x07;
    cdclu1SendReadyFlg=1; 
}


static void CDCS_vReadDataWarningLamp(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_WARNING_LAMP, 1);

    CDCS_vSendData(CDS_u8GetLampStatus(U8_BASIC_LAMP), 2);
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vReadDataSwitch1(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_SWITCH_1, 1);

     CDCS_vSendData(CDS_u8GetSwitchStatus(U8_SUPP_SWT), 2);
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vReadDataRelay(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_RELAY, 1);

     CDCS_vSendData(CDS_u8GetActuatorStatus(U8_RELAY_STATUS) , 2);
        
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1;  
}

static void CDCS_vReadDataMotor(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_MOTOR, 1);

     CDCS_vSendData(CDS_u8GetActuatorStatus(U8_MOT_STATUS), 2);
        
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vReadDataValve(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_VALVE, 1);

    CDCS_vSendData(CDS_u8GetActuatorStatus(U8_INVALVE_STATUS), 2);
    CDCS_vSendData(CDS_u8GetActuatorStatus(U8_OUTVALVE_STATUS), 3);
    #if __ECU==ESP_ECU_1
        CDCS_vSendData(CDS_u8GetActuatorStatus(U8_ESPVALVE_STATUS), 4);
        /*===========================================================
        Version_1.1
        1. RSV valves에 대한 service data 출력 추가
        ===========================================================*/
        #if __DIAG_PREMIUM_14VV_ECU==1
            CDCS_vSendData(CDS_u8GetActuatorStatus(U8_RSVVALVE_STATUS), 5);
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 5);
        #endif
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 4);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 5);
    #endif
    
    cdclu16ServiceDataSum=0x06;
    cdclu1SendReadyFlg=1; 
}

#if ((__G_SENSOR_TYPE!=NONE)||(__4WD_VARIANT_CODE==ENABLE)||(__ECU==ESP_ECU_1))
static void CDCS_vReadDataGsnsr(void)
{
    uint16_t  u16LG;
   
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_G_SNSR, 1);
    
    #if ((__G_SENSOR_TYPE!=NONE)||(__4WD_VARIANT_CODE==ENABLE))
        //if(ccu1AxSnsrSetFlg==1)
    if(1)
        {
             CDCS_vSendData(CDS_u8GetAxSnsrData(), 2);
        }
        else
        {
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 2);
        }
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 2);
    #endif
       
    
    #if __FULL_ESC_SYSTEM==1
        u16LG=CDS_u16GetYawAySnsrData(U8_AY_RV); /* AY */
                 
        CDCS_vSendData((uint8_t)(u16LG>>8), 3);
        CDCS_vSendData((uint8_t)u16LG, 4);
    #else   
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 3);
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 4);
    #endif
    
    cdclu16ServiceDataSum=0x05;
    cdclu1SendReadyFlg=1; 
}
#endif


static void CDCS_vReadDataADvalues(void)
{
    uint16 Diag_fu16CalVoltVDD;
    uint16 Diag_fu16CalVoltPower12V;
    uint16 Diag_fu16CalVoltMOTOR;
    uint16 Diag_fu16CalVoltAhbFSR;
    uint16 Diag_fu16CalVoltAbsFSR;
    uint16 Diag_fu16CalVoltPosBCPPVoltage;
    uint16 Diag_fu16CalVoltNegBCPPVoltage;
    uint16 Diag_fu16CalVoltPosBCPSVoltage;
    uint16 Diag_fu16CalVoltNegBCPSVoltage;
    uint16 Diag_fu16CalVoltPosHPAVoltage;
    uint16 Diag_fu16CalVoltNegHPAVoltage;
    uint16 Diag_fu16CalVoltPosPSPVoltage;
    uint16 Diag_fu16CalVoltNegPSPVoltage;
    uint16 Diag_fu16CalVoltPDT;
    uint16 Diag_fu16CalVoltPDF;
    #if __AHB_GEN3_SYSTEM==DISABLE
        #if __FULL_ESC_SYSTEM==1
        uint16_t u16YawCanData, u16AyCanData;
        #endif
        #if __G_SENSOR_TYPE==CAN_TYPE
        uint16_t u16AxCanData;
        #endif
    #endif
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_AD, 1);
    
    #if __AHB_GEN3_SYSTEM==DISABLE
        CDCS_vSendData((uint8_t)(fu16CompensatedVDD>>8), 2);
        CDCS_vSendData((uint8_t)fu16CompensatedVDD, 3);  
        
        CDCS_vSendData((uint8_t)(ref_mon_ad>>8), 4);
        CDCS_vSendData((uint8_t)ref_mon_ad, 5);
        
        CDCS_vSendData((uint8_t)(motor_mon_ad>>8), 6);
        CDCS_vSendData((uint8_t)motor_mon_ad, 7);
        
        #if __G_SENSOR_TYPE==ANALOG_TYPE
            CDCS_vSendData((uint8_t)(sen_5V_mon_ad>>8), 8);
            CDCS_vSendData((uint8_t)sen_5V_mon_ad, 9);
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 8);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 9);
        #endif
        
        
        #if ((__FULL_ESC_SYSTEM==1) && (__MGH80_MOCi == DISABLE)) /*yunhanwoo don't use CSP_MON in MOCi*/
            CDCS_vSendData((uint8_t)(sen_12V_mon_ad>>8), 10);
            CDCS_vSendData((uint8_t)sen_12V_mon_ad, 11);
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 10);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 11);
        #endif
        
        if(ccu1AxSnsrSetFlg==1)
        {
            #if __G_SENSOR_TYPE==CAN_TYPE
                #if defined(__SVDO_IMU) || defined(__HMC_IMU)  
                u16AxCanData=((uint16_t)((((int32_t)a_long_1_1000g)*392)/625))&0x0FFF;  /*u16AxCanData/64 = a_long_100_g *0.0098*/
                #else
                u16AxCanData=((uint16_t)fs12IMULonAcc)&0x0FFF;
                #endif
                CDCS_vSendData((uint8_t)(u16AxCanData>>8), 12);    
                CDCS_vSendData((uint8_t)u16AxCanData, 13);
            #elif __G_SENSOR_TYPE==ANALOG_TYPE
                CDCS_vSendData((uint8_t)(G_SEN_SIG_MON>>8), 12);    
                CDCS_vSendData((uint8_t)G_SEN_SIG_MON, 13);
            #endif 
        }
        else
        {
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 12);  
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 13);
        }  
      
        #if __FULL_ESC_SYSTEM==1
            CDCS_vSendData((uint8_t)(mp_pos_mon_ad>>8), 14);
            CDCS_vSendData((uint8_t)mp_pos_mon_ad, 15);
            CDCS_vSendData((uint8_t)(mp_neg_mon_ad>>8), 16);
            CDCS_vSendData((uint8_t)mp_neg_mon_ad, 17);   
    
            #if defined(__SVDO_IMU) || defined(__HMC_IMU) 
                u16YawCanData=(uint16_t)((((int32_t)fys16EstimatedYaw)*4)/25)&0x0FFF;
                u16AyCanData= (uint16_t)((((int32_t)fys16EstimatedAY)*63)/100)&0x0FFF;
            #endif
            
            CDCS_vSendData((uint8_t)(u16YawCanData>>8), 18);
            CDCS_vSendData((uint8_t)u16YawCanData, 19);    
            CDCS_vSendData((uint8_t)(u16AyCanData>>8), 20);  
            CDCS_vSendData((uint8_t)u16AyCanData, 21); 
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 14);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 15);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 16);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 17);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 18);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 19);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 20);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 21);
        #endif
        
        #if __PEDAL_SENSOR_TYPE!=NONE
            CDCS_vSendData((uint8_t)(PEDAL_SIG_MON>>8), 22);
            CDCS_vSendData((uint8_t)PEDAL_SIG_MON, 23);
            #ifdef _USE_2_PEDAL_SIGNAL
                CDCS_vSendData((uint8_t)(SUB_PEDAL_SIG_MON>>8), 24);
                CDCS_vSendData((uint8_t)SUB_PEDAL_SIG_MON, 25);
            #else
                CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 24);
                CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 25);
            #endif
        #else
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 22);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 23);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 24);
            CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 25);
        #endif 
        
        #if __VACUUM_SENSOR_ENABLE==ENABLE                 
            CDCS_vSendData((uint8_t)(vacumn_mon_ad >>8), 26);        
            CDCS_vSendData((uint8_t)vacumn_mon_ad , 27); 
        #else                                              
            CDCS_vSendData((uint8_t)0xFF , 26);                 
            CDCS_vSendData((uint8_t)0xFF , 27);                
        #endif     
        
        cdclu16ServiceDataSum=28;
    #else
        
        /* For Compile MSW : Compile KCLim To do */
#if 0
        Diag_fu16CalVoltVDD             = IoHwAb_GetVoltage(IOHWAB_IN_PORT_VDD_MON);
        Diag_fu16CalVoltPower12V        = IoHwAb_GetVoltage(IOHWAB_IN_PORT_CSP_MON);
        // ?? Diag_fu16CalVoltMOTOR           = IoHwAb_GetVoltage();
        Diag_fu16CalVoltAhbFSR          = IoHwAb_GetVoltage(IOHWAB_IN_PORT_FSP_CBS_MON);
        // ?? Diag_fu16CalVoltAbsFSR          = ??
        // ?? Diag_fu16CalVoltPosBCPPVoltage  = IoHwAb_GetVoltage();
        // ?? Diag_fu16CalVoltNegBCPPVoltage  = IoHwAb_GetVoltage();
        // ?? Diag_fu16CalVoltPosBCPSVoltage  = IoHwAb_GetVoltage();
        // ?? Diag_fu16CalVoltNegBCPSVoltage  = IoHwAb_GetVoltage();
        // ?? Diag_fu16CalVoltPosHPAVoltage   = IoHwAb_GetVoltage();
        // ?? Diag_fu16CalVoltNegHPAVoltage   = IoHwAb_GetVoltage();
        Diag_fu16CalVoltPosPSPVoltage   = IoHwAb_GetVoltage(IOHWAB_IN_PORT_PSIMP_POS_MON);
        Diag_fu16CalVoltNegPSPVoltage   = IoHwAb_GetVoltage(IOHWAB_IN_PORT_PSIMP_NEG_MON);
        Diag_fu16CalVoltPDT             = IoHwAb_GetVoltage(IOHWAB_IN_PORT_PDT_5V_MON);
        Diag_fu16CalVoltPDF             = IoHwAb_GetVoltage(IOHWAB_IN_PORT_PDF_5V_MON);

        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltVDD>>8), 2);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltVDD, 3);
        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPower12V>>8), 4);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltPower12V, 5);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltMOTOR>>8), 6);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltMOTOR, 7);
        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltAhbFSR>>8), 8);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltAhbFSR, 9);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltAbsFSR>>8), 10);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltAbsFSR, 11);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPosBCPPVoltage>>8), 12);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltPosBCPPVoltage, 13);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltNegBCPPVoltage>>8), 14);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltNegBCPPVoltage, 15);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPosBCPSVoltage>>8), 16);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltPosBCPSVoltage, 17);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltNegBCPSVoltage>>8), 18);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltNegBCPSVoltage, 19);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPosHPAVoltage>>8), 20);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltPosHPAVoltage, 21);
        //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltNegHPAVoltage>>8), 22);
        //CDCS_vSendData((uint8_t)Diag_fu16CalVoltNegHPAVoltage, 23);
        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPosPSPVoltage>>8), 24);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltPosPSPVoltage, 25);
        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltNegPSPVoltage>>8), 26);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltNegPSPVoltage, 27);
        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPDT>>8), 28);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltPDT, 29);
        CDCS_vSendData((uint8_t)(Diag_fu16CalVoltPDF>>8), 30);
        CDCS_vSendData((uint8_t)Diag_fu16CalVoltPDF, 31);
        cdclu16ServiceDataSum=32;
#endif
    #endif
    cdclu1SendReadyFlg=1; 
}




static void CDCS_vReadDataADvalues_Internal(void)
{

    uint16 Diag_fu16CalVoltBATT1;
    uint16 Diag_fu16CalVoltBATT2;
    uint16 Diag_fu16CalVoltVBB;
    uint16 Diag_fu16CalVolt5v_Vcc;
    uint16 Diag_fu16CalVolt5VPPowMon;
    uint16 Diag_fu16CalVolt3p3VVccMon;
    uint16 Diag_fu16CalVoltVDD;
    uint16 Diag_fu16CalVoltCut1Neg ;
    uint16 Diag_fu16CalVoltCut2Neg ;
    uint16 Diag_fu16CalVoltSimVNeg ;
    uint16 Diag_fu16CalVoltCE      ;
    uint16 Diag_wu16AsicTemp       ;

    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0); 
    CDCS_vSendData(U8_DIAG_RDBLI_AD2, 1);
    
    CDCS_vSendData((uint8_t)Diag_fu1HDCSwitchSignal, 2);
    #if __BRAKE_SWITCH_ENABLE==ENABLE
        CDCS_vSendData((uint8_t)Diag_fu1BSSignal, 3);
    #else
        CDCS_vSendData((uint8_t)0xFF, 3);      
    #endif    
    
    #if __AVH==ENABLE
        CDCS_vSendData((uint8_t)fu1AVHSwitchSignal, 4);  
        CDCS_vSendData((uint8_t)fu1PBSignal, 5); 
        CDCS_vSendData((uint8_t)fu1HazardSw, 6); 
        CDCS_vSendData((uint8_t)fu1DoorSwSignal, 7); 
    #else
        CDCS_vSendData((uint8_t)0xFF, 4);   
        CDCS_vSendData((uint8_t)0xFF, 5);
        CDCS_vSendData((uint8_t)0xFF, 6);
        CDCS_vSendData((uint8_t)0xFF, 7);      
    #endif
    /* For Compile MSW : Compile KCLim To do */
#if 0
	Diag_fu16CalVoltBATT1           = IoHwAb_GetVoltage (IOHWAB_IN_PORT_VBATT01_MON);
	Diag_fu16CalVoltBATT2           = IoHwAb_GetVoltage (IOHWAB_IN_PORT_VBATT02_MON);
	// ?? Diag_fu16CalVoltVBB             = IoHwAb_GetVoltage ();
	Diag_fu16CalVolt5v_Vcc          = IoHwAb_GetVoltage (IOHWAB_IN_PORT_5V_VCC_MON);
	Diag_fu16CalVolt5VPPowMon       = IoHwAb_GetVoltage (IOHWAB_IN_PORT_5V_P_POW_MON);
	Diag_fu16CalVolt3p3VVccMon      = IoHwAb_GetVoltage (IOHWAB_IN_PORT_3p3V_VCC_MON);
	Diag_fu16CalVoltVDD             = IoHwAb_GetVoltage (IOHWAB_IN_PORT_VDD_MON);
	// ?? Diag_fu16CalVoltCut1Neg         = IoHwAb_GetVoltage ();
	// ?? Diag_fu16CalVoltCut2Neg         = IoHwAb_GetVoltage ();
	// ?? Diag_fu16CalVoltSimVNeg         = IoHwAb_GetVoltage ();
	Diag_fu16CalVoltCE              = IoHwAb_GetVoltage (IOHWAB_IN_PORT_CE_MON);
	Diag_wu16AsicTemp               = (uint16)SpiIf_ReceiveSignal (SPI_SIG_ASIC_RX_TEMP);

    CDCS_vSendData((uint8_t)(Diag_fu16CalVoltBATT1>>8), 8);
    CDCS_vSendData((uint8_t)Diag_fu16CalVoltBATT1, 9);
    CDCS_vSendData((uint8_t)(Diag_fu16CalVoltBATT2>>8), 10);
    CDCS_vSendData((uint8_t)Diag_fu16CalVoltBATT2, 11);
    //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltVBB>>8), 12);
    //CDCS_vSendData((uint8_t)Diag_fu16CalVoltVBB, 13);
    CDCS_vSendData((uint8_t)(Diag_fu16CalVolt5v_Vcc>>8), 14);
    CDCS_vSendData((uint8_t)Diag_fu16CalVolt5v_Vcc, 15);
    CDCS_vSendData((uint8_t)(Diag_fu16CalVolt5VPPowMon>>8), 16);
    CDCS_vSendData((uint8_t)Diag_fu16CalVolt5VPPowMon, 17);
    CDCS_vSendData((uint8_t)(Diag_fu16CalVolt3p3VVccMon>>8), 18);
    CDCS_vSendData((uint8_t)Diag_fu16CalVolt3p3VVccMon, 19);
    CDCS_vSendData((uint8_t)(Diag_fu16CalVoltVDD>>8), 20);
    CDCS_vSendData((uint8_t)Diag_fu16CalVoltVDD, 21);
    //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltCut1Neg>>8), 22);
    //CDCS_vSendData((uint8_t)Diag_fu16CalVoltCut1Neg, 23);
    //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltCut2Neg>>8), 24);
    //CDCS_vSendData((uint8_t)Diag_fu16CalVoltCut2Neg, 25);
    //CDCS_vSendData((uint8_t)(Diag_fu16CalVoltSimVNeg>>8), 26);
    //CDCS_vSendData((uint8_t)Diag_fu16CalVoltSimVNeg, 27);
    CDCS_vSendData((uint8_t)(Diag_fu16CalVoltCE>>8), 28);
    CDCS_vSendData((uint8_t)Diag_fu16CalVoltCE, 29);
    CDCS_vSendData((uint8_t)(Diag_wu16AsicTemp>>8), 30);
    CDCS_vSendData((uint8_t)Diag_wu16AsicTemp, 31);
#endif
    
    cdclu16ServiceDataSum=32;
    cdclu1SendReadyFlg=1; 
}



#if __SUPPLIER_DTC_ENABLE==1
static void CDCS_vReadSIDTCRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_SMINTERNAL_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        cdu1ReadIntDtcSupplier=1;
        CDCS_vReadSIDTCResponse();
    }   
}
static void CDCS_vReadSIDTCResponse(void)
{
    CDCS_vSendData(U8_DIAG_SMINTERNAL_RESPONSE_ID, 0);
    
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}
#endif


static void CDCS_vReadDataInternalError(void)
{
    uint8_t   Temp_1;
    uint8_t   cdu8_Byte;
    
    /* FOR Compile MSW : KCLim NvM */
    cdu8_Byte=ERROR_BYTE_NO;
    
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_INTERNAL_ERROR, 1);
    #if __SUPPLIER_DTC_ENABLE==1
        if(cdu1ReadIntDtcSupplier==1)
        {
            for(Temp_1=0; Temp_1 < cdu8_Byte; Temp_1++)
            {
                CDCS_vSendData(weu8CopySuppEepBuff[Temp_1], 2 + Temp_1);
            }
            cdu1ReadIntDtcSupplier=0; 
        }
        else
    #endif
        {
            for(Temp_1=0; Temp_1 < cdu8_Byte; Temp_1++)
            {
                CDCS_vSendData(weu8CopyEepBuff[Temp_1], 2 + Temp_1);
            }       
        }
    
    cdclu16ServiceDataSum=cdu8_Byte+2;
   
    cdclu1SendReadyFlg=1;
}

static void CDCS_vReadDataTestComplete(void)
{
    uint8_t   Temp_1;
    
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_TEST_COMPLETE, 1);
    /* FOR Compile MSW : KCLim NvM */
    for(Temp_1=0; Temp_1 < ERROR_BYTE_NO; Temp_1++)
    {
        CDCS_vSendData(Diag_feu8TestComplete[Temp_1], 2 + Temp_1);
    }
    
    cdclu16ServiceDataSum=ERROR_BYTE_NO+2;

    cdclu1SendReadyFlg=1; 
}



#if __FULL_ESC_SYSTEM==1
static void CDCS_vReadDataYaw(void)
{
    uint16_t  u16YAW;
    
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_YAW, 1);
        
    u16YAW=CDS_u16GetYawAySnsrData(U8_YAW_RV); /* YAW */

    CDCS_vSendData((uint8_t)(u16YAW>>8), 2); 
    CDCS_vSendData((uint8_t)u16YAW, 3); 
    
    cdclu16ServiceDataSum=0x04; 
    cdclu1SendReadyFlg=1; 
}

#if __STEER_SENSOR_TYPE!=NONE
static void CDCS_vReadDataSteering(void)
{
    uint16_t u16Steer;

    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_STEERING, 1);

    #if __STEER_SENSOR_TYPE==ANALOG_TYPE
        CDCS_vSendData(CDS_u8GetSteerPhaseData(), 2);
    #else
        CDCS_vSendData(U8_DIAG_NOT_SUPPORTED, 2);
    #endif
    
     u16Steer=CDS_u16GetSteeringData();
    
    CDCS_vSendData((uint8_t)(u16Steer>>8), 3);
    CDCS_vSendData((uint8_t)u16Steer, 4);
    cdclu16ServiceDataSum=0x05;
    cdclu1SendReadyFlg=1; 
}
#endif

static void CDCS_vReadDataPressure(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_PRESSURE, 1);

    CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_POS_MP_RV), 2); /* Positive */
     CDCS_vSendData(CDS_u8GetMcpSnsrData(U8_NEG_MP_RV), 3); /* Negative */
        
    cdclu16ServiceDataSum=0x04;  
    cdclu1SendReadyFlg=1;  
}

static void CDCS_vReadDataSwitch2(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_SWITCH_2, 1);

     CDCS_vSendData(CDS_u8GetSwitchStatus(U8_HDC_SWT), 2);
        
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vReadDataFunctionLamp(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_FUNC_LAMP, 1);

     CDCS_vSendData(CDS_u8GetLampStatus(U8_FUNC_LAMP), 2);
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
 }
#endif /* #if __FULL_ESC_SYSTEM==1 */

/*===========================================================
Version_1.3
1. BBOX 기능 구현
   >. MEC Read Service 추가
===========================================================*/
static void CDCS_vReadDataMec(void)
{
    CDCS_vSendData(U8_DIAG_RDBLI_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RDBLI_MEC, 1);

    CDCS_vSendData(cdu8Mec, 2);
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1; 
}   

static void CDCS_vSetTimeDrivingIO(void)
{
    if((cdcsu1ActuationFlg==1)||(cdcsu1ActuationMultiFlg==1))
    {
        cdcsu8ActResult=CDS_u8DirveActuatorData(cdcsu8ActCode);
    }
    else
    {
        cdcsu8ActResult=0;
    }
    
    if(cdcsu1ActuationFlg==1)
    {
        if(cdcsu16ActTimer>U8_DIAG_LOOP_TIME)
        {
            cdcsu16ActTimer-=U8_DIAG_LOOP_TIME;
        }
        else
        {
            cdcsu16ActTimer=0;
        }
        
        if(cdcsu16ActTimer==(cdcsu16SaveActTimer-U8_IO_DRV_RSP_DELAY_TIME))
        {
            CDCS_vIOCBLIResponse(cdcsu8ActCode);
        }
        else
        {
            
            ;
        }
        
        if(cdcsu16ActTimer==0)
        {
            CDCS_vStopActuation();
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
    
    if(cdcsu1ActuationMultiFlg==1)
    {
        if(cdcsu16ActTimer>U8_DIAG_LOOP_TIME)
        {
            cdcsu16ActTimer-=U8_DIAG_LOOP_TIME;
        }
        else
        {
            cdcsu16ActTimer=0;
        }
        
        if(cdcsu16SaveActTimer>=U8_IO_DRV_RSP_DELAY_TIME)
        {
            if(cdcsu16ActTimer==(cdcsu16SaveActTimer-U8_IO_DRV_RSP_DELAY_TIME))
            {
                CDCS_vIOCBLIResponse(cdcsu8ActCode);
            }
            else
            {
                ;
            }
        }
        else
        {
            if(cdcsu16ActTimer<=U8_DIAG_LOOP_TIME)
            {
                CDCS_vIOCBLIResponse(cdcsu8ActCode);
            }
            else
            {
                ;
            }
        }
                
        if(cdcsu16ActTimer==0)
        {
            CDCS_vStopActuation();
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
}

static void CDCS_vIOCBLIRequest(void) 
{
    uint8_t u8BytesLen;
    #if ((__DIAG_SPEC==CHERY_DIAG)||(__DIAG_SPEC==SYMC_DIAG))
        uint8_t u8BytesSpec;
    #endif
    
    u8BytesLen=cdclu8RxBuffer[0]&0x0F;
    
    if((u8BytesLen!=0x03)&&(u8BytesLen!=0x06)) 
    {
        if(cdcsu8CanDiagMode!=U8_DIAG_SUPPLIER_MODE)
        {
            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        }
        else
        {
            ;
        }
    }
    else
    {
        cdcsu8ActCode = cdclu8RxBuffer[2];
        
        if(cdcsu8ActCode==U8_DIAG_IOLI) 
        {
            #if __DIAG_SPEC==CHERY_DIAG
                if(u8BytesLen==0x03)
                {
                    CDCS_vIOCBLISupportedPID();
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
                }
            #elif __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);    
            #else
                CDCS_vIOCBLISupportedPID();
            #endif
        }
        else if((cdcsu8ActCode==U8_DIAG_FLI)||(cdcsu8ActCode==U8_DIAG_FRI)
        ||(cdcsu8ActCode==U8_DIAG_RLI)||(cdcsu8ActCode==U8_DIAG_FLO)||(cdcsu8ActCode==U8_DIAG_FRO)
        ||(cdcsu8ActCode==U8_DIAG_RLO)||(cdcsu8ActCode==U8_DIAG_RRI)||(cdcsu8ActCode==U8_DIAG_RRO)
        #if __AHB_GEN3_SYSTEM==DISABLE
            ||(cdcsu8ActCode==U8_DIAG_MOTOR)
            #if __ECU==ESP_ECU_1
                 ||(cdcsu8ActCode==U8_DIAG_TCL)||(cdcsu8ActCode==U8_DIAG_TCR)
                 ||(cdcsu8ActCode==U8_DIAG_ESVP)||(cdcsu8ActCode==U8_DIAG_ESVS)
                /*===========================================================
                Version_1.1
                1. PRSV, SRSV 밸브 강제 구동 서비스 추가
                ===========================================================*/
                 #if __DIAG_PREMIUM_14VV_ECU==1
                 ||(cdcsu8ActCode==U8_DIAG_PRSV)||(cdcsu8ActCode==U8_DIAG_SRSV)
                 #endif
            #endif
        #endif
        /*===========================================================
        Version_1.1
        1. DBC, ESS BRAKE LAMP RELAY 강제 구동 서비스 추가
        ===========================================================*/
        #if __DIAG_DBC_RELAY_SET==1 
            ||(cdcsu8ActCode==U8_DIAG_HDCRELAY)
        #endif
        #if __DIAG_ESS_RELAY_SET==1 
            ||(cdcsu8ActCode==U8_DIAG_ESSRELAY)
        #endif
        ) 
        {
            #if __DIAG_SPEC==CHERY_DIAG
                if(u8BytesLen==0x03)
                {
                    CDCS_vIOCBLIActSingle();
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
                }
            #elif __DIAG_SPEC==SYMC_DIAG 
                if(u8BytesLen==0x03)
                {
                    if(cdclu8RxBuffer[3]==0x01)
                    {
                        CDCS_vIOCBLIResponse(cdcsu8ActCode);
                    }
                    else
                    {
                        CDCS_vIOCBLIActSingle();
                    }
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
                }
            #else
                #if defined(DIAG_HSA_CODING_ENABLE) || (ESS_INLINE_ENABLE==ENABLE)
                    if(cdcsu8ActCode==U8_DIAG_HDCRELAY)
                    {
                        if(ccu1HsaVcSetFlg==1)
                        {
                            CDCS_vIOCBLIActSingle();   
                        }
                        else
                        {
                            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
                        }
                    }
                    else if(cdcsu8ActCode==U8_DIAG_ESSRELAY)
                    {
                        if(ccu1EssVcSetFlg==1)
                        {
                            CDCS_vIOCBLIActSingle();   
                        }
                        else
                        {
                            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
                        }     
                    }
                    else
                    {
                        CDCS_vIOCBLIActSingle();   
                    }
                #else
                    CDCS_vIOCBLIActSingle();
                #endif  
            #endif
        }
		/* 강제 구동 구문 AHB Valve는 구분 Flag를 보고 돌리거나 Nagative 거나 */
		/* Release Valve와 Apply Valve 순서 변경 */
        #if __AHB_GEN3_SYSTEM==ENABLE
        else if((cdcsu8ActCode==U8_DIAG_MOTOR)||(cdcsu8ActCode==U8_DIAG_RLV02)||(cdcsu8ActCode==U8_DIAG_RLV01)
            ||(cdcsu8ActCode==U8_DIAG_APV01)||(cdcsu8ActCode==U8_DIAG_APV02)    
            ||(cdcsu8ActCode==U8_DIAG_CUT1)||(cdcsu8ActCode==U8_DIAG_CUT2)
            ||(cdcsu8ActCode==U8_DIAG_SIMV))
        {
            if(diag_ahb_sci_flg==1)
            {
                CDCS_vIOCBLIActSingle();   
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }
        }    
        #endif    
        else if(cdcsu8ActCode==U8_DIAG_MULTI) 
        {
            #if ((__DIAG_SPEC==CHERY_DIAG)||(__DIAG_SPEC==SYMC_DIAG))
                if(cdclu8RxBuffer[3]==U8_DIAG_ACT_ON)
                {
                    u8BytesSpec=0x06;
                }
                else if(cdclu8RxBuffer[3]==U8_DIAG_ACT_OFF)
                {
                    u8BytesSpec=0x03;
                }
                else
                {
                    u8BytesSpec=u8BytesLen;
                }
                
                if(u8BytesLen==u8BytesSpec)
                {
                    CDCS_vIOCBLIActMultiple();
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
                }
            #else
                CDCS_vIOCBLIActMultiple();
            #endif
        }
        #if ((__DIAG_SPEC==SYMC_DIAG)&&(__DIAG_CAL_AX_ENABLE==1))
        else if(cdcsu8ActCode==U8_DIAG_CALAXOFS)
        {
            if(u8BytesLen==0x03)    
            {
                if((cdu1DisabledNetFlg==0)&&(cdu1WrongSysVoltFlg==0))
                {
                    
                    CDCS_vCalAxSnsrRequest();
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                }
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
            }
        }
        #endif
        else 
        {
            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
        }
    }
}

static void CDCS_vIOCBLIActSingle(void)
{
    CDCS_vStopActuation();
    
    if(cdclu8RxBuffer[3]==U8_DIAG_ACT_ON)
    {
        if (cdcsu8ActCode==U8_DIAG_MOTOR) 
        {
            if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
            {
                cdu16MotorDrvDuty = (((uint16_t)cdclu8RxBuffer[4])<<8) | ((uint16_t)cdclu8RxBuffer[5]);
                MotorForcedDrv_CtrlMode = (uint16_t)cdclu8RxBuffer[4];
							
                            if(MotorForcedDrv_CtrlMode == MOTOR_A_DUTY_ON)
                            {
								#if 0
                            	MotorForcedDrv.MotRequest = 1;
                            	MotorForcedDrv.MotPwmDutyA = 4000;
                            	MotorForcedDrv.MotPwmDutyB = 2000;
                            	MotorForcedDrv.MotPwmDutyC = 2000;
                            	SAL_Write_Diag_MotDutyInfo(&MotorForcedDrv);
								#endif
								#if 0
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 1;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = 4000;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = 2000;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = 2000;
								#endif
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 3;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = 4000;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = 2000;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = 2000;
                            }
                            else if(MotorForcedDrv_CtrlMode == MOTOR_B_DUTY_ON)
                            {
								#if 0
                            	MotorForcedDrv.MotRequest = 1;
                            	MotorForcedDrv.MotPwmDutyA = 2000;
                            	MotorForcedDrv.MotPwmDutyB = 4000;
                            	MotorForcedDrv.MotPwmDutyC = 2000;
                            	SAL_Write_Diag_MotDutyInfo(&MotorForcedDrv);
								#endif
								#if 0
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 1;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = 2000;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = 4000;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = 2000;								
								#endif
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 3;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = 2000;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = 4000;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = 2000;								
                            }
                            else if(MotorForcedDrv_CtrlMode == MOTOR_C_DUTY_ON)
                            {
								#if 0
                            	MotorForcedDrv.MotRequest = 1;
                            	MotorForcedDrv.MotPwmDutyA = 2000;
                            	MotorForcedDrv.MotPwmDutyB = 2000;
                            	MotorForcedDrv.MotPwmDutyC = 4000;
                            	SAL_Write_Diag_MotDutyInfo(&MotorForcedDrv);
								#endif
								#if 0
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 1;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = 2000;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = 2000;
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = 4000;								
								#endif
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 3;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = 2000;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = 2000;
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = 4000;								
                            }
                            else
                            {
								#if 0
                            	MotorForcedDrv.MotRequest = 0;
                            	SAL_Write_Diag_MotDutyInfo(&MotorForcedDrv);
								#endif
								#if 0
								Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 0;
								#endif
								Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 7;
                            }
                			
                if((cdu16MotorDrvDuty==0)||(cdu16MotorDrvDuty>=U16_MOT_DRV_MAX_DUTY))
                {
                    cdu16MotorDrvDuty=U16_MOT_DRV_MAX_DUTY; 
                }
                else
                {
                    ;
                }
            }
            else
            {
                cdu16MotorDrvDuty=U16_MOT_DRV_MAX_DUTY; 
            }

            cdu16MotorDrvTimer=2000;
        }
        else 
        {
            if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
            {
                cdu16SolenoidDrvDuty = (((uint16_t)cdclu8RxBuffer[4])<<8) | ((uint16_t)cdclu8RxBuffer[5]);
                                 
                if((cdu16SolenoidDrvDuty==0)||(cdu16SolenoidDrvDuty>=U16_SOL_DRV_MAX_DUTY))
                {
                    cdu16SolenoidDrvDuty=U16_SOL_DRV_MAX_DUTY;  
                }
                else
                {
                    ;
                }
                
                #if __AHB_GEN3_SYSTEM==ENABLE
                    cdu16SolenoidDrvDutyAHB = (((uint16_t)cdclu8RxBuffer[4])<<8) | ((uint16_t)cdclu8RxBuffer[5]);
                    
                    if((cdu16SolenoidDrvDutyAHB==0)||(cdu16SolenoidDrvDutyAHB>=U16_SOL_DRV_MAX_AHB_DUTY))
                    {
                        cdu16SolenoidDrvDutyAHB=U16_SOL_DRV_MAX_AHB_DUTY;  
                    }
                    else
                    {
                        ;
                    }
                #endif
            }
            else
            {
                cdu16SolenoidDrvDuty=U16_SOL_DRV_MAX_DUTY;  
                #if __AHB_GEN3_SYSTEM==ENABLE
                    cdu16SolenoidDrvDutyAHB=U16_SOL_DRV_MAX_AHB_DUTY;
                #endif
            }
            
            if (cdcsu8ActCode==U8_DIAG_FLI) 
            {
                cdu1FLNOFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_FRI) 
            {
                cdu1FRNOFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_RLI) 
            {
                cdu1RLNOFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_RRI) 
            {
                cdu1RRNOFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_FLO) 
            {
                cdu1FLNCFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_FRO) 
            {
                cdu1FRNCFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_RLO) 
            {
                cdu1RLNCFirstDrv=1;
            }
            else if (cdcsu8ActCode==U8_DIAG_RRO) 
            {
                cdu1RRNCFirstDrv=1;
            }
            #if __AHB_GEN3_SYSTEM==DISABLE
                #if __ECU==ESP_ECU_1
                    else if (cdcsu8ActCode==U8_DIAG_TCL) 
                    {
                        cdu1STCFirstDrv=1;
                    } 
                    else if (cdcsu8ActCode==U8_DIAG_TCR) 
                    {
                        cdu1PTCFirstDrv=1;
                    }    
                    else if (cdcsu8ActCode==U8_DIAG_ESVP) 
                    {
                        cdu1ESVPFirstDrv=1;
                    }
                    else if (cdcsu8ActCode==U8_DIAG_ESVS) 
                    {
                        cdu1ESVSFirstDrv=1;
                    }
                    /*===========================================================
                    Version_1.1
                    1. PRSV, SRSV 밸브 강제 구동 서비스 추가
                    ===========================================================*/
                    #if __DIAG_PREMIUM_14VV_ECU==1
                        else if (cdcsu8ActCode==U8_DIAG_PRSV) 
                        {
                            cdu1PRSVFirstDrv=1;
                        }
                        else if (cdcsu8ActCode==U8_DIAG_SRSV) 
                        {
                            cdu1SRSVFirstDrv=1;
                        }
                    #endif
                #endif
            #else
                else if (cdcsu8ActCode==U8_DIAG_RLV02) 
                {
                    cdu1RLV02FirstDrv=1;
                }     
                else if (cdcsu8ActCode==U8_DIAG_RLV01) 
                {
                    cdu1RLV01FirstDrv=1;
                }
                else if (cdcsu8ActCode==U8_DIAG_APV01) 
                {
                    cdu1APV01FirstDrv=1;
                }   
                else if (cdcsu8ActCode==U8_DIAG_APV02) 
                {
                    cdu1APV02FirstDrv=1;
                }                   
                else if (cdcsu8ActCode==U8_DIAG_CUT1) 
                {
                    cdu1CUT1VFirstDrv=1;
                }
                else if (cdcsu8ActCode==U8_DIAG_CUT2) 
                {
                    cdu1CUT2VFirstDrv=1;
                }
                else if (cdcsu8ActCode==U8_DIAG_SIMV) 
                {
                    cdu1SIMVFirstDrv=1;
                }
            #endif
            /*===========================================================
            Version_1.1
            1. DBC, ESS BRAKE LAMP RELAY 강제 구동 서비스 추가
            ===========================================================*/
            #if __DIAG_DBC_RELAY_SET==1
                else if (cdcsu8ActCode==U8_DIAG_HDCRELAY)
                {
                    cdu1HdcRelayDrv=1;
                }
            #endif
            
            #if __DIAG_ESS_RELAY_SET==1
                else if (cdcsu8ActCode==U8_DIAG_ESSRELAY)
                {
                    cdu1EssRelayDrv=1;
                }
            #endif
            else 
            {
                ;
            }
        }
        
        cdcsu16SaveActTimer=cdcsu16ActTimer=cdu16ActDrvTimer=2000;
        cdu16ActDrvTimerAHB=cdu16ActDrvTimer;
        
        cdcsu1FSRDemandFlg=1;
        cdcsu1ActuationFlg=1;
    }   
    else if(cdclu8RxBuffer[3]==U8_DIAG_ACT_OFF)
    {
		#if 0
    	MotorForcedDrv.MotRequest = 0;
    	SAL_Write_Diag_MotDutyInfo(&MotorForcedDrv);
		#endif
		//Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 0;
		Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 7;
        CDCS_vStopActuation();
        CDCS_vIOCBLIResponseStop(cdcsu8ActCode);
    }   
    else 
    {
        CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
    }
    
    CDCS_vDriveActSet();
}

static void CDCS_vIOCBLIActMultiple(void)
{
    CDCS_vStopActuation();
    
    if(cdclu8RxBuffer[3]==U8_DIAG_ACT_ON)
    {
        /*===========================================================
        Version_1.1
        1. Multiple IO control시 각 Valve에 대한 Duty입력부 추가
        ===========================================================*/
        cdu16SolenoidDrvDuty=U16_SOL_DRV_MAX_DUTY;
        #if __AHB_GEN3_SYSTEM==ENABLE
            cdu16SolenoidDrvDutyAHB=U16_SOL_DRV_MAX_AHB_DUTY;
        #endif

        if((cdclu8RxBuffer[4] & 0x01) == 0x01) 
        {
            cdu1FLNOFirstDrv=1;
        }
        else
        {
            ;
        }

        if((cdclu8RxBuffer[4] & 0x02) == 0x02) 
        {
            cdu1FRNOFirstDrv=1;
        }
        else
        {
            ;
        }   

        if((cdclu8RxBuffer[4] & 0x04) == 0x04) 
        {
            cdu1RLNOFirstDrv=1;
        }
        else
        {
            ;
        }   

        if((cdclu8RxBuffer[4] & 0x08) == 0x08) 
        {
            cdu1RRNOFirstDrv=1;
        }
        else
        {
            ;
        }   
        
        if((cdclu8RxBuffer[4] & 0x10) == 0x10) 
        {
            cdu1FLNCFirstDrv=1;
        }
        else
        {
            ;
        }   

        if((cdclu8RxBuffer[4] & 0x20) == 0x20) 
        {
             cdu1FRNCFirstDrv=1;
        }
        else
        {
            ;
        }   

        if((cdclu8RxBuffer[4] & 0x40) == 0x40) 
        {
            cdu1RLNCFirstDrv=1;
        }
        else
        {
            ;
        } 

        if((cdclu8RxBuffer[4] & 0x80) == 0x80) 
        {
            cdu1RRNCFirstDrv=1;
        }
        else
        {
            ;
        }   
        
        #if __AHB_GEN3_SYSTEM==DISABLE
            if((cdclu8RxBuffer[5] & 0x01) == 0x01) 
            {
                /*===========================================================
                Version_1.1
                1. Multiple IO control시 Motor에 대한 Duty입력부 추가
                ===========================================================*/
                cdu16MotorDrvDuty=U16_MOT_DRV_MAX_DUTY; 
                cdu16MotorDrvTimer=((uint16_t)cdclu8RxBuffer[6])*10;
            }
            else
            {
                ;
            }                        
            #if __ECU==ESP_ECU_1
                if((cdclu8RxBuffer[5] & 0x02) == 0x02) 
                {
                    cdu1STCFirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x04) == 0x04) 
                {
                    cdu1PTCFirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x08) == 0x08) 
                {
                    cdu1ESVPFirstDrv=1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x10) == 0x10) 
                {
                    cdu1ESVSFirstDrv=1;
                }
                else
                {
                    ;
                }
                /*===========================================================
                Version_1.1
                1. PRSV, SRSV 밸브 강제 구동 서비스 추가 (Multiple)
                ===========================================================*/
                #if __DIAG_PREMIUM_14VV_ECU==1
                    if((cdclu8RxBuffer[5] & 0x20) == 0x20) 
                    {
                        cdu1PRSVFirstDrv=1;
                    }
                    else
                    {
                        ;
                    }
                    if((cdclu8RxBuffer[5] & 0x40) == 0x40) 
                    {
                        cdu1SRSVFirstDrv=1;
                    }
                    else
                    {
                        ;
                    }
                #endif   
            #endif
        #else /* __AHB_GEN3_SYSTEM==ENABLE */
            if(diag_ahb_sci_flg==1)
            {
                if((cdclu8RxBuffer[5] & 0x01) == 0x01) 
                {
                    /*===========================================================
                    Version_1.1
                    1. Multiple IO control시 Motor에 대한 Duty입력부 추가
                    ===========================================================*/
                    cdu16MotorDrvDuty=U16_MOT_DRV_MAX_DUTY; 
                    cdu16MotorDrvTimer=((uint16_t)cdclu8RxBuffer[6])*10;
                }
                else
                {
                    ;
                }                
                if((cdclu8RxBuffer[5] & 0x02) == 0x02) 
                {
                    cdu1RLV02FirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x04) == 0x04) 
                {
                    cdu1RLV01FirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x08) == 0x08) 
                {
                    cdu1APV01FirstDrv = 1;
                    
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x10) == 0x10) 
                {
                    cdu1APV02FirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x20) == 0x20) 
                {
                    cdu1CUT1VFirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x40) == 0x40) 
                {
                    cdu1CUT2VFirstDrv = 1;
                }
                else
                {
                    ;
                }
                if((cdclu8RxBuffer[5] & 0x80) == 0x80) 
                {
                    cdu1SIMVFirstDrv = 1;
                }
                else
                {
                    ;
                }
            } /* if(diag_ahb_sci_flg==1) */
            else
            {
                /* AHB Diag가 Set이 안되면, 모든 Drv를 0으로 Set하고 Negative Response를 날려준다 */
                if(cdclu8RxBuffer[5]!=0)
                {
                    CDCS_vStopActuation();
                    CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                }
            }            
        #endif /* __AHB_GEN3_SYSTEM==ENABLE */
        
        cdcsu1FSRDemandFlg=1;
        
        cdcsu16SaveActTimer=cdcsu16ActTimer=cdu16ActDrvTimer=((uint16_t)cdclu8RxBuffer[6])*10;
        cdu16ActDrvTimerAHB=cdu16ActDrvTimer;
            
        if(cdclu8RxBuffer[6] == 0)
        {
            CDCS_vStopActuation();
            CDCS_vIOCBLIResponse(cdcsu8ActCode);
        }
        else 
        {
            cdcsu1ActuationMultiFlg=1;
        }       
    } /*if(cdclu8RxBuffer[3]==U8_DIAG_ACT_ON) */
    else if(cdclu8RxBuffer[3]==U8_DIAG_ACT_OFF)
    {
        CDCS_vStopActuation();
        CDCS_vIOCBLIResponseStop(cdcsu8ActCode);
    }   
    else 
    {
        CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
    } 
    CDCS_vDriveActSet();
}

static void CDCS_vDriveActSet(void)
{   
    if(cdcsu1MRDemandFlg==1)
    {
        diag_mr_demand_flg=1;
    }
    else
    {
        diag_mr_demand_flg=0;
    }
}

void CDCS_vSolSetOnTime(void)
{
   /* if(cdu1FLNOFirstDrv==1)
    {
        on_time_inlet_fl=7;  
    }
    else
    {
        on_time_inlet_fl=0;
    }
    
    if(cdu1FRNOFirstDrv==1)
    {
        on_time_inlet_fr=7;
    }
    else
    {
        on_time_inlet_fr=0;
    }
    
    if(cdu1RLNOFirstDrv==1)
    {
        on_time_inlet_rl=7;  
    }
    else
    {
        on_time_inlet_rl=0;
    }

    if(cdu1FLNCFirstDrv==1)
    {
        on_time_outlet_fl=7; 
    }
    else 
    {
        on_time_outlet_fl=0;
    }
    
    if(cdu1FRNCFirstDrv==1)
    {
        on_time_outlet_fr=7; 
    }
    else
    {
        on_time_outlet_fr=0;
    }
    
    if(cdu1RLNCFirstDrv==1) 
    {
        on_time_outlet_rl=7; 
    }
    else 
    {
        on_time_outlet_rl=0;
    }

    if(cdu1RRNOFirstDrv==1) 
    {
        on_time_inlet_rr =7; 
    }
    else 
    {
        on_time_inlet_rr =0;
    }
    
    if(cdu1RRNCFirstDrv==1)
    {
        on_time_outlet_rr=7; 
    }
    else 
    {
        on_time_outlet_rr=0;
    }
    
    #if (__BTC==ENABLE) &&(__ECU!=ESP_ECU_1)
        if(cdcsu1TCLOnFlg==1) 
        {
            on_time_stcno=7; 
        }
        else 
        {
            on_time_stcno=0;
        }
  
        if(cdcsu1TCROnFlg==1) 
        {
            on_time_ptcno=7; 
        }
        else 
        {
            on_time_ptcno=0;
        }
    #endif

    #if __ECU==ESP_ECU_1
        if(cdu1PTCFirstDrv ==1) 
        {
            on_time_ptcno =7;
        }
        else 
        {
            on_time_ptcno =0;
        }
        if(cdu1STCFirstDrv ==1) 
        {
            on_time_stcno =7; 
        }
        else 
        {
            on_time_stcno =0;
        }
        if(cdu1ESVPFirstDrv  ==1) 
        {
            on_time_psnc  =7; 
        }
        else 
        {
            on_time_psnc  =0;
        }
        if(cdu1ESVSFirstDrv  ==1) 
        {
            on_time_ssnc  =7; 
        }
        else 
        {
            on_time_ssnc  =0;
        }
    #endif*/
}

void CDCS_vStopActuation(void)
{
    
    CDSACTFLAG0=0;
    #if __ECU==ESP_ECU_1
        cdu1PTCFirstDrv=0; 
        cdu1STCFirstDrv=0; 
        cdu1ESVPFirstDrv=0;
        cdu1ESVSFirstDrv=0;
    /*===========================================================
    Version_1.1
    1. PRSV, SRSV 밸브 강제 구동 서비스 추가
    ===========================================================*/
        #if __DIAG_PREMIUM_14VV_ECU==1
            cdu1PRSVFirstDrv=0;
            cdu1SRSVFirstDrv=0;
        #endif
    #endif
    
    #if __AHB_GEN3_SYSTEM==ENABLE
        cdu1APV01FirstDrv = 0;
        cdu1APV02FirstDrv = 0;
        cdu1RLV01FirstDrv = 0;
        cdu1RLV02FirstDrv = 0;
        cdu1CUT1VFirstDrv = 0;
        cdu1CUT2VFirstDrv = 0;
        cdu1SIMVFirstDrv  = 0;
    #endif
    
    cdcsu1FSRDemandFlg=0;     
    cdcsu1MRDemandFlg=0;      
    cdcsu1ActuationFlg=0;     
    cdcsu1ActuationMultiFlg=0;
    cdu1MultiDrvMode=0;       

    
    cdu16ActDrvTimer=0;
    cdu16ActDrvTimerAHB=0;
    cdu16MotorDrvTimer=0;
    cdcsu16ActTimer=0;
    
    cdu16MotorDrvDuty=0;
    cdu16SolenoidDrvDuty=0;
    cdu16SolenoidDrvDutyAHB=0;
    CDCS_vDriveActSet();
    
    /*===========================================================
    Version_1.1
    1. DBC, ESS BRAKE LAMP RELAY 강제 구동 서비스 추가
    ===========================================================*/
    #if __DIAG_DBC_RELAY_SET==1
        cdu1HdcRelayDrv=0;
    #endif
    
    #if __DIAG_ESS_RELAY_SET==1
        cdu1EssRelayDrv=0;
    #endif
	#if 0
    /* Motor Forced Drive Request Off */
    MotorForcedDrv.MotRequest = 0;
    SAL_Write_Diag_MotDutyInfo(&MotorForcedDrv);
	#endif
	//Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 0;
	Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 7;
}

static void CDCS_vIOCBLISupportedPID(void)
{
    uint32_t TmpSupported_Pid=0;
        
    CDCS_vSendData(U8_DIAG_IOCBLI_RESPONSE_ID, 0);
    CDCS_vSendData(cdcsu8ActCode, 1);
    
    /*===========================================================
    Version_1.1
    1. DBC, ESS BRAKE LAMP RELAY 강제 구동 서비스 추가
    ===========================================================*/
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_MOTOR; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_FLI; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_FRI; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_RLI; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_RRI; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_FLO; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_FRO; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_RLO; 
    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_RRO; 
    #if __AHB_GEN3_SYSTEM==DISABLE
        #if __ECU==ESP_ECU_1
            TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_TCL; 
            TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_TCR; 
            #if __DIAG_PREMIUM_14VV_ECU==1
                TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_PRSV; 
                TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_SRSV; 
            #endif
            TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_ESVP; 
            TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_ESVS;
        #endif
         #if __DIAG_DBC_RELAY_SET==1
            #if defined(DIAG_HSA_CODING_ENABLE)
                if(ccu1HsaVcSetFlg==1)    
                {
                    TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_HDCRELAY; 
                }
                else
                {
                    ;
                }
            #else
                TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_HDCRELAY; 
            #endif
        #endif
    #endif
    #if __DIAG_ESS_RELAY_SET==1
        #if ESS_INLINE_ENABLE==ENABLE
            if(ccu1EssVcSetFlg==1)
            {   
                TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_ESSRELAY; 
            }
            else
            {
                ;
            }
        #else
            TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_ESSRELAY; 
        #endif
    #endif      
    #if __AHB_GEN3_SYSTEM==ENABLE    
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_APV01; 
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_APV02; 
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_RLV01; 
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_RLV02; 
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_CUT1; 
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_CUT2; 
        TmpSupported_Pid|=U8_DIAG_SPPRTD_PID_SIMV; 
    #endif
    
    CDCS_vSendData(((uint8_t)TmpSupported_Pid), 2);
    CDCS_vSendData(((uint8_t)(TmpSupported_Pid>>8)), 3);
    CDCS_vSendData(((uint8_t)(TmpSupported_Pid>>16)), 4);
    CDCS_vSendData(((uint8_t)(TmpSupported_Pid>>24)), 5);
      
    cdclu16ServiceDataSum=0x06;
    cdclu1SendReadyFlg=1;             
}
        
static void CDCS_vIOCBLIResponse(uint8_t ActCode)
{
    #if __DIAG_SPEC==SYMC_DIAG
    uint8_t IoStatus;
    #endif
    
    CDCS_vSendData(U8_DIAG_IOCBLI_RESPONSE_ID, 0);
    CDCS_vSendData(ActCode, 1);
    
    #if __DIAG_SPEC==SYMC_DIAG
        CDCS_vSendData(cdclu8RxBuffer[3], 2);
        
        if(cdclu8RxBuffer[3]==0x01)
        {
            IoStatus=CDS_u8DirveActuatorData(ActCode);
            CDCS_vSendData(IoStatus, 3);
            cdclu16ServiceDataSum=0x04;
        }
        else
        {
            if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE) 
            {
                CDCS_vSendData(cdcsu8ActResult, 3);
                cdclu16ServiceDataSum=0x04;   
            }
            else    
            {
                cdclu16ServiceDataSum=0x03;
            }
        }
    
    #else
        CDCS_vSendData(cdcsu8ActResult, 2);
        cdclu16ServiceDataSum=0x03; 
    #endif
    cdclu1SendReadyFlg=1; 
}

static void CDCS_vIOCBLIResponseStop(uint8_t ActCode)
{
    CDCS_vSendData(U8_DIAG_IOCBLI_RESPONSE_ID, 0);
    CDCS_vSendData(ActCode, 1);
    
     #if __DIAG_SPEC==SYMC_DIAG
        CDCS_vSendData(cdclu8RxBuffer[3], 2);
        
        if(cdclu8RxBuffer[3]==0x01)
        {
            CDCS_vSendData(U8_DIAG_ACT_SUCCESS, 3);
            cdclu16ServiceDataSum=0x04;
        }
        else
        {
            if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE) 
            {
                CDCS_vSendData(U8_DIAG_ACT_SUCCESS, 3);
                cdclu16ServiceDataSum=0x04;   
            }
            else    
            {
                cdclu16ServiceDataSum=0x03;
            }
        }
    
    #else
        CDCS_vSendData(U8_DIAG_ACT_SUCCESS, 2);
        cdclu16ServiceDataSum=0x03; 
    #endif
        
    cdclu1SendReadyFlg=1; 
}      

static void CDCS_vREIRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_REI_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        cdcsu8EcuIdOption=cdclu8RxBuffer[2];
        
        #if __DIAG_SPEC==SYMC_DIAG
            switch(cdcsu8EcuIdOption)
            {
                case U8_DIAG_ID_SYMC_OP_ECUID:
                case U8_DIAG_ID_SYMC_OP_CODE_FP:
                case U8_DIAG_ID_SYMC_OP_DATA_FP:
                case U8_DIAG_ID_SYMC_OP_BOOT_FP:
                case U8_DIAG_ID_SYMC_OP_CODE_SWID:
                case U8_DIAG_ID_SYMC_OP_DATA_SWID:
                case U8_DIAG_ID_SYMC_OP_BOOT_SWID:
                   CDCS_vREIResponse();
                break;
                
                case U8_DIAG_ID_OPTION_ECUIDT:
                    if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
                    {
                        CDCS_vREIResponse();
                    }
                    else
                    {
                        CDCS_vNegativeResponse(U8_DIAG_REI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                    }
                break;
                
                default:
                   CDCS_vNegativeResponse(U8_DIAG_REI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                break;
            }
        #else
            if(cdcsu8EcuIdOption==U8_DIAG_ID_OPTION_ECUIDT) 
            {
                CDCS_vREIResponse();
            }
            #ifdef DIAG_INLINE_CODING_ENABLE
            else if(cdcsu8EcuIdOption==0xE0) 
            {
                CDCS_vREIResponseInLineCode();
            }
            #endif
            else if(cdcsu8EcuIdOption==0xFE)
            {
                CDCS_vREIResponseInternalVer(); 
            }            
            else 
            {
                CDCS_vNegativeResponse(U8_DIAG_REI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        #endif
    }
}

static void CDCS_vREIResponse(void)
{
    #if __DIAG_SPEC==SYMC_DIAG
    uint8_t i, FingerPrint[12];
    uint8_t u8DrvMode;
    #endif
         
    CDCS_vSendData(U8_DIAG_REI_RESPONSE_ID, 0);     
    CDCS_vSendData(cdcsu8EcuIdOption, 1);    

    #if __DIAG_SPEC==SYMC_DIAG
        switch(cdcsu8EcuIdOption)
        {
            case U8_DIAG_ID_SYMC_OP_ECUID:
                CDCS_vSendData(0x06, 2); /* ECU origin */
                CDCS_vSendData(0xFC, 3); /* ECU supplier ID */ 
                CDCS_vSendData(0x00, 4); /* Diag Infor - Variant */
                CDCS_vSendData(SYMC_DIAG_SPEC_REVISION, 5); /* Diag Infor - Diag Version */
                CDCS_vSendData(0xFF, 6); /* (reserved) */
                
                CDCS_vSendData(0x00, 7); /* H/W version */
                CDCS_vSendData(Diag_wmu8EcuHwVersion-0x40, 8); /* H/W version */
                CDCS_vSendData(0x00, 9); /* S/W version */
                CDCS_vSendData(0x00, 10);
                CDCS_vSendData(SYMC_SW_Release_Counter, 11);
                CDCS_vSendData(0xFF, 12);
                CDCS_vSendData(0xFF, 13);
                for(i=0;i<8;i++)
                {
                    CDCS_vSendData(SYMC_HECU_HW_PN[i],14+i);
                }
                
                cdclu16ServiceDataSum = 22;
            break;
            
            case U8_DIAG_ID_SYMC_OP_CODE_SWID:
            case U8_DIAG_ID_SYMC_OP_DATA_SWID:
            case U8_DIAG_ID_SYMC_OP_BOOT_SWID:
                CDCS_vSendData(0x01, 2); /* Number of BLOCK */
                CDCS_vSendData(0x06, 3); /* ECU origin */
                CDCS_vSendData(0xFC, 4); /* ECU supplier ID */ 
                CDCS_vSendData(0x00, 5); /* Diag Infor - Variant */
                CDCS_vSendData(SYMC_DIAG_SPEC_REVISION, 6); /* Diag Infor - Diag Version */
                CDCS_vSendData(0xFF, 7); /* (reserved) */
                
                CDCS_vSendData(0x00, 8); /* S/W version */
                CDCS_vSendData(0x00, 9);
                CDCS_vSendData(SYMC_SW_Release_Counter, 10);
                CDCS_vSendData(0xFF, 11);
                CDCS_vSendData(0xFF, 12);
                for(i=0;i<8;i++)
                {
                    CDCS_vSendData(SYMC_HECU_HW_PN[i],13+i);
                }
                
                cdclu16ServiceDataSum = 21;
            break;
            
            case U8_DIAG_ID_SYMC_OP_CODE_FP:
                if(WE_u8ReadEEP_Byte(U16_CODE_FINGER_EEP_ADDR, &FingerPrint[0], 12)==1)
                {
                    
                    CDCS_vSendData(0x01, 2); /* Number of BLOCK */
                    CDCS_vSendData(0x00, 3); /* Active Logical BLOCK */
                                      
                    for(i=0;i<8;i++)
                    {
                        CDCS_vSendData(FingerPrint[2+i], 4+i);
                    }
                }
                cdclu16ServiceDataSum=12;
            break; 
             
            case U8_DIAG_ID_SYMC_OP_DATA_FP:
                if(WE_u8ReadEEP_Byte(U16_DATA_FINGER_EEP_ADDR, &FingerPrint[0], 12)==1)
                {
                    CDCS_vSendData(0x01, 2); /* Number of BLOCK */
                    CDCS_vSendData(0x00, 3); /* Active Logical BLOCK */
                    
                    for(i=0;i<8;i++)
                    {
                        CDCS_vSendData(FingerPrint[2+i], 4+i);
                    }
                }
                cdclu16ServiceDataSum=12;
            break; 
             
            case U8_DIAG_ID_SYMC_OP_BOOT_FP:
                if(WE_u8ReadEEP_Byte(U16_BOOT_FINGER_EEP_ADDR, &FingerPrint[0], 12)==1)
                {
                    CDCS_vSendData(0x01, 2); /* Number of BLOCK */
                    CDCS_vSendData(0x00, 3); /* Active Logical BLOCK */
                    
                    for(i=0;i<8;i++)
                    {
                        CDCS_vSendData(FingerPrint[2+i], 4+i);
                    }
                }
                cdclu16ServiceDataSum=12;
            break;
            
            case U8_DIAG_ID_OPTION_ECUIDT:
                CDCS_vSendData(VERSION[0][0], 2);
                CDCS_vSendData(VERSION[0][1], 3);
                CDCS_vSendData(VERSION[0][2], 4);
                CDCS_vSendData(VERSION[0][3], 5);
                CDCS_vSendData(VERSION[0][4], 6);
                CDCS_vSendData(VERSION[0][5], 7);
                CDCS_vSendData(VERSION[0][6], 8);
  /*===============================================================
        Variant : 2WDE : 2WD EPS, 2WDH : 2WD HPS
                  4WDE : 4WD EPS, 4WDH : 4WD HPS
    ================================================================*/ 
                CDCS_vSendData(VERSION[0][7], 9);
                CDCS_vSendData(VERSION[0][8], 10);
                #if __4WD_VARIANT_CODE==ENABLE
                if(fsu1VarintSetEnd==1)
                {
                    if(fsu14WDDriveType==1)
                    {
                        CDCS_vSendData(0x34, 11);
                    }
                    else
                    {
                        CDCS_vSendData(0x32, 11);
                    }
                    CDCS_vSendData(0x57, 12);
                    CDCS_vSendData(0x44, 13);
                    
                    u8DrvMode=wmu8ReadEepDrvMode&0x07;
                    if((u8DrvMode==DM_2WD_EPS)||(u8DrvMode==DM_4WD_EPS))
                    {
                        CDCS_vSendData(0x45, 14);
                    }
                    else
                    {
                        CDCS_vSendData(0x48, 14);
                    }
                }
                else
                {
                    CDCS_vSendData(0x20, 11);
                    CDCS_vSendData(0x20, 12);
                    CDCS_vSendData(0x20, 13); 
                    CDCS_vSendData(0x20, 14); 
                }
                #else 
                CDCS_vSendData(VERSION[0][9], 11);
                CDCS_vSendData(VERSION[0][10], 12);
                CDCS_vSendData(VERSION[0][11], 13);
                CDCS_vSendData(0x20, 14);
                #endif
               /* CDCS_vSendData(VERSION[0][10], 12);*/
               /* CDCS_vSendData(VERSION[0][11], 13);*/
                    
                CDCS_vSendData(VERSION[1][0], 15);
                CDCS_vSendData(VERSION[1][1], 16);
                CDCS_vSendData(VERSION[1][2], 17);
                CDCS_vSendData(VERSION[1][3], 18);
                CDCS_vSendData(VERSION[1][4], 19);
                CDCS_vSendData(VERSION[1][5], 20);
                CDCS_vSendData(VERSION[1][6], 21);
                CDCS_vSendData(VERSION[1][7], 22);
                CDCS_vSendData(VERSION[1][8], 23);
                CDCS_vSendData(VERSION[1][9], 24);
                CDCS_vSendData(VERSION[1][10], 25);

                CDCS_vSendData(Diag_asic_version, 26);       /* ASIC Version : 2.2='0' , 3.0='1' , 3.1='2' */
                
                CDCS_vSendData(0x2D, 27); /* '-' */
                
                CDCS_vSendData(VERSION[1][12], 28); /* Compile date (Year) */
                CDCS_vSendData(VERSION[1][13], 29); /* Compile date (Month) */
                CDCS_vSendData(VERSION[1][14], 30); /* Compile date (Date10) */
                CDCS_vSendData(VERSION[1][15], 31); /* Compile date (Date1) */
                
                cdclu16ServiceDataSum = 32;
            break;
            
            default:
            break;
        }
   #else    
        CDCS_vSendData(VERSION[0][0], 2);
        CDCS_vSendData(VERSION[0][1], 3); 
              
         if(VERSION[0][2] == 0x20)
         {     
            CDCS_vSendData(0x20, 4);
            CDCS_vSendData(VERSION[0][2], 5);           
            CDCS_vSendData(VERSION[0][3], 6);
            CDCS_vSendData(VERSION[0][4], 7);
            CDCS_vSendData(VERSION[0][5], 8);           
            CDCS_vSendData(VERSION[0][6], 9);
        }
        else
        {
            CDCS_vSendData(VERSION[0][2], 4);   
            CDCS_vSendData(VERSION[0][3], 5);
            CDCS_vSendData(VERSION[0][4], 6);
            CDCS_vSendData(VERSION[0][5], 7);
            CDCS_vSendData(VERSION[0][6], 8);
            CDCS_vSendData(VERSION[0][7], 9);
         }
        #if __4WD_VARIANT_CODE==ENABLE
        if(fsu1VarintSetEnd==1)
        {
            if(fsu14WDDriveType==1)
            {
                CDCS_vSendData(0x34, 10);
            }
            else
            {
                CDCS_vSendData(0x32, 10);
            }
            CDCS_vSendData(0x57, 11);
            CDCS_vSendData(0x44, 12);
        }
        else
        {
            CDCS_vSendData(0x20, 10);
            CDCS_vSendData(0x20, 11);
            CDCS_vSendData(0x20, 12); 
        }
        #else 
        CDCS_vSendData(VERSION[0][8], 10);
        CDCS_vSendData(VERSION[0][9], 11);
        CDCS_vSendData(VERSION[0][10], 12);
        #endif
        
        #if __DIAG_NEW_VERSION==1
            CDCS_vSendData(VERSION[0][11], 13);
            
            CDCS_vSendData(Diag_wmu8EcuHwVersion, 14);
            CDCS_vSendData(0x2E, 15);
            CDCS_vSendData(ELECTRIC_SW_DLS[0], 16);
            
            CDCS_vSendData(0x20, 17);
              
            CDCS_vSendData(VERSION[1][0],  18);
            CDCS_vSendData(VERSION[1][1],  19);
            CDCS_vSendData(VERSION[1][2],  20);
            CDCS_vSendData(VERSION[1][3],  21);
            CDCS_vSendData(VERSION[1][4],  22);
            CDCS_vSendData(VERSION[1][5],  23);
            CDCS_vSendData(VERSION[1][6],  24);
            CDCS_vSendData(VERSION[1][7],  25);
            CDCS_vSendData(VERSION[1][8],  26);
            CDCS_vSendData(VERSION[1][9],  27);
            CDCS_vSendData(VERSION[1][10], 28);

            CDCS_vSendData(Diag_asic_version, 29);       /* ASIC Version : 2.2='0' , 3.0='1' , 3.1='2' */
            
            CDCS_vSendData(0x2D, 30); /* '-' */
            
            CDCS_vSendData(VERSION[1][12], 31); /* Compile date (Year) */
            CDCS_vSendData(VERSION[1][13], 32); /* Compile date (Month) */
            CDCS_vSendData(VERSION[1][14], 33); /* Compile date (Date10) */
            CDCS_vSendData(VERSION[1][15], 34); /* Compile date (Date1) */
            
            cdclu16ServiceDataSum = 0x23;
        #else
            CDCS_vSendData(VERSION[0][11], 13);
            CDCS_vSendData(VERSION[0][12], 14);
                
            CDCS_vSendData(VERSION[1][0], 15);
            CDCS_vSendData(VERSION[1][1], 16);
            CDCS_vSendData(VERSION[1][2], 17);
            CDCS_vSendData(VERSION[1][3], 18);
            CDCS_vSendData(VERSION[1][4], 19);
            CDCS_vSendData(VERSION[1][5], 20);
            CDCS_vSendData(VERSION[1][6], 21);
            CDCS_vSendData(VERSION[1][7], 22);
            CDCS_vSendData(VERSION[1][8], 23);
            CDCS_vSendData(VERSION[1][9], 24);
            CDCS_vSendData(VERSION[1][10], 25);

            CDCS_vSendData(Diag_asic_version, 26);       /* ASIC Version : 2.2='0' , 3.0='1' , 3.1='2' */
            
            CDCS_vSendData(0x2D, 27); /* '-' */
            
            CDCS_vSendData(VERSION[1][12], 28); /* Compile date (Year) */
            CDCS_vSendData(VERSION[1][13], 29); /* Compile date (Month) */
            CDCS_vSendData(VERSION[1][14], 30); /* Compile date (Date10) */
            CDCS_vSendData(VERSION[1][15], 31); /* Compile date (Date1) */
            
            cdclu16ServiceDataSum = 32;
        #endif
    #endif

    cdclu1SendReadyFlg=1; 
}
#ifdef DIAG_INLINE_CODING_ENABLE
static void CDCS_vREIResponseInLineCode(void)
{
    #ifdef DIAG_INLINE_CODING_ENABLE
    uint8_t cdu8_VarTemp=0;   /* recordValue#1 */
    uint8_t cdu8_VarTemp1=0;  /* recordValue#2 */  
    uint8_t cdu8_VarTemp4=0;  /* recordValue#5 */
    #endif
    
    #ifdef DIAG_INLINE_CODING_ENABLE
        CDCS_vSendData(U8_DIAG_REI_RESPONSE_ID, 0);     
        CDCS_vSendData(0xE0, 1);
        
        #if ESS_INLINE_ENABLE==ENABLE
            if(fu2EssInlineState==0) 
            {
                cdu8_VarTemp|=0x01;
            }
            else if(fu2EssInlineState==1)
            {
                cdu8_VarTemp|=0x03;
            }
            else if(fu2EssInlineState==2)
            {
                cdu8_VarTemp|=0x02;   
            }
            else
            {
                ;
            }
        #endif
        
        #if HSA_INLINE_ENABLE==ENABLE
            if(fu2HsaInlineState==0) 
            {
                cdu8_VarTemp|=0x04;
            }
            else if(fu2HsaInlineState==1)
            {
                cdu8_VarTemp|=0x0C;
            }
            else if(fu2HsaInlineState==2)
            {
                cdu8_VarTemp|=0x08;   
            }
            else
            {
                ;
            }
        #endif
        
        #if SPAS_INLINE_ENABLE==ENABLE
            if(fu2SpasInlineState==0) 
            {
                cdu8_VarTemp|=0x10;
            }
            else if(fu2SpasInlineState==1)
            {
                cdu8_VarTemp|=0x30;
            }
            else if(fu2SpasInlineState==2)
            {
                cdu8_VarTemp|=0x20;   
            }
            else
            {
                ;
            }
        #endif
        
        #if AVH_INLINE_ENABLE==ENABLE
            if(fu2AvhInlineState==0) 
            {
                cdu8_VarTemp|=0x40;
            }
            else if(fu2AvhInlineState==1)
            {
                cdu8_VarTemp|=0xC0;
            }
            else if(fu2AvhInlineState==2)
            {
                cdu8_VarTemp|=0x80;   
            }
            else
            {
                ;
            }
        #endif
        
        #if EPB_INLINE_ENABLE==ENABLE 
            if(fu2EpbInlineState==0) 
            {
                cdu8_VarTemp1|=0x01;
            }
            else if(fu2EpbInlineState==1)
            {
                cdu8_VarTemp1|=0x03;
            }
            else if(fu2EpbInlineState==2)
            {
                cdu8_VarTemp1|=0x02;   
            }
            else
            {
                ;
            }
        #endif
        
        /*------------------- Local Inline Coding Add -------------------*/
        
        #if REGION_INLINE_ENABLE==ENABLE
            if(fu2LocalInlineState==1) 
            {
                cdu8_VarTemp4=0x01;
            }
            else if(fu2LocalInlineState==2)
            {
                cdu8_VarTemp4=0x02;  
            }
            else if(fu2LocalInlineState==3)
            {
                cdu8_VarTemp4=0x03;   
            }
            else
            {
                ;
            }
        #endif
        
        /*---------------------------------------------------------------*/
        
        CDCS_vSendData(cdu8_VarTemp,  2);
        CDCS_vSendData(cdu8_VarTemp1, 3);
        CDCS_vSendData(0, 4);
        CDCS_vSendData(0, 5);
        CDCS_vSendData(cdu8_VarTemp4, 6);
        
        
        cdclu16ServiceDataSum=7;
        cdclu1SendReadyFlg=1;
    
    #endif
}
#endif
static void CDCS_vREIResponseInternalVer(void)
{
    if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
    {
        CDCS_vSendData(U8_DIAG_REI_RESPONSE_ID, 0);     
        CDCS_vSendData(0xFE, 1);
        CDCS_vSendData(MANDO_SW_Release_Counter, 2);
        
        cdclu16ServiceDataSum=3;
        cdclu1SendReadyFlg=1;
    }
    else
    {
        CDCS_vNegativeResponse(U8_DIAG_REI_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
    }   
}

static void CDCS_vDNMTRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResByFuncReq(U8_DIAG_DNMT_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        #else
            CDCS_vNegativeResponse(U8_DIAG_DNMT_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
        #endif
    }
    else
    {
        #if __DIAG_SPEC==SYMC_DIAG 
            if(CDCS_vCheckFuncAddr()==1) 
            {
                return;
            }
            else
            {
                ;
            }
        #endif
    
        if(cdclu8RxBuffer[2]==U8_DIAG_DNMT_RESPONSE_REQUIRED)
        {
            CDCS_vDNMTResponse();
            //ccu1TxDisableFlg=1;
        }
        else if (cdclu8RxBuffer[2]==U8_DIAG_DNMT_RESPONSE_NO_REQUIRED)
        {
            cdclu8DiagStatus=7;
            //ccu1TxDisableFlg=1;
        }
        else
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResByFuncReq(U8_DIAG_DNMT_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #else 
                CDCS_vNegativeResponse(U8_DIAG_DNMT_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            #endif
        }
    }
}

static void CDCS_vDNMTResponse(void)
{
    CDCS_vSendData(U8_DIAG_DNMT_RESPONSE_ID, 0);
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}  

static void CDCS_vENMTRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResByFuncReq(U8_DIAG_ENMT_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        #else
            CDCS_vNegativeResponse(U8_DIAG_ENMT_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
        #endif
    }
    else
    {
        #if __DIAG_SPEC==SYMC_DIAG 
            if(CDCS_vCheckFuncAddr()==1) 
            {
                return;
            }
            else
            {
                ;
            }
        #endif
        
        if(cdclu8RxBuffer[2]==U8_DIAG_ENMT_RESPONSE_REQUIRED)
        {
            CDCS_vENMTResponse();
           // ccu1TxDisableFlg=0;
        }
        else if (cdclu8RxBuffer[2]==U8_DIAG_ENMT_RESPONSE_NO_REQUIRED)
        {
            cdclu8DiagStatus=7;
            //ccu1TxDisableFlg=0;
        }
        else
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResByFuncReq(U8_DIAG_ENMT_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #else 
                CDCS_vNegativeResponse(U8_DIAG_ENMT_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            #endif
        }
    }
}

static void CDCS_vENMTResponse(void)
{
    CDCS_vSendData(U8_DIAG_ENMT_RESPONSE_ID, 0);
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}

static void CDCS_vSARequest(void)
{
    uint8_t u8BytesLen;
    
    u8BytesLen=cdclu8RxBuffer[0]&0x0F;
    
    if((u8BytesLen!=0x02)&&(u8BytesLen!=0x04)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if(cdcsu1SATimeOkflg==0)
        {
            CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_REQUIRED_TIME_DALAY);
        }
        else
        {
            if(cdcsu16SecurityDelayTimer==0)
            {   
                if(cdcsu1AccesModeFlg==0)
                {
                    if(cdcsu1SecurityOkFlg==0)
                    {
                        if(cdclu8RxBuffer[2]==U8_DIAG_RequestSeed)
                        {
                            CDCS_vGetSeed();
                            CDCS_vSAResponseSeed();
                            cdcsu1AccesModeFlg=1;
                        }
                        else
                        {
                            CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                        }
                    }
                    else
                    {
                        if(cdclu8RxBuffer[2]==U8_DIAG_RequestSeed)
                        {
                            CDCS_vSAResponseSeed();
                        }
                        else if(cdclu8RxBuffer[2]==U8_DIAG_SendKey)
                        {
                            CDCS_vSAResponseKey();
                        }
                        else
                        {
                            CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                        }
                    }   
                }
                else
                {
                    if(cdclu8RxBuffer[2]==U8_DIAG_SendKey)
                    {
                        CDCS_vCrcCheck();
                      
                        if((cdclu8RxBuffer[3]==cdcsu8CrcKey1)&&(cdclu8RxBuffer[4]==cdcsu8CrcKey2))
                        {
                            cdcsu1SecurityOkFlg=1;
                            CDCS_vSAResponseKey();
                        }
                        else
                        {
                            if(cdcsu1SecurityNgFlg==1)
                            {
                                cdcsu16SecurityDelayTimer=U16_DIAG_T_10S;
                                cdcsu1SecurityNgFlg=0;
                            }
                            else
                            {
                                cdcsu1SecurityNgFlg=1;
                            }
                            
                            CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_INVALID_KEY);
                        }
                        cdcsu1AccesModeFlg=0;
                    }
                    else
                    {
                        CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_SECURITY_ACCESS_DENIED);
                    }
                }   
            
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_SA_REQUEST_ID, U8_DIAG_REQUIRED_TIME_DALAY);
            }
        }
    }   
}
    
static void CDCS_vSAResponseSeed(void)
{
    CDCS_vSendData(U8_DIAG_SA_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_RequestSeed, 1);
    
    if(cdcsu1SecurityOkFlg==0)
    {   
        CDCS_vSendData(0x00, 2);
        CDCS_vSendData(cdcsu8Seed, 3);
    }
    else
    {
        CDCS_vSendData(0, 2);
        CDCS_vSendData(0, 3);
    }
    
    cdclu16ServiceDataSum=0x04;
    cdclu1SendReadyFlg=1;
}

static void CDCS_vSAResponseKey(void)
{
    CDCS_vSendData(U8_DIAG_SA_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_SendKey, 1);
    CDCS_vSendData(0x34, 2);
    
    if(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)
    {
        if(Diag_HndlrBus.Diag_HndlrIgnOnOffSts == ON)
        {
            cdclu8TerminationStatus=U8_DIAG_JUMP_PRGM_MODE;
            cdcsu1WaitingFuncFlg=1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1;
}


static void CDCS_vGetSeed(void)
{
    uint8_t u8GenSeed;
    
    u8GenSeed=((uint8_t)Diag_fu16LoopCounter)&0x07;
    
    if(u8GenSeed==0)
    {
        cdcsu8Seed=0x03;
    }
    else if(u8GenSeed==1)
    {
        cdcsu8Seed=0x17;
    }
    else if(u8GenSeed==2)
    {
        cdcsu8Seed=0x28;
    }
    else if(u8GenSeed==3)
    {
        cdcsu8Seed=0x3B;
    }
    else if(u8GenSeed==4)
    {
        cdcsu8Seed=0x4F;
    }
    else if(u8GenSeed==5)
    {
        cdcsu8Seed=0x55;
    }
    else if(u8GenSeed==6)
    {
        cdcsu8Seed=0x69;
    }
    else if(u8GenSeed==7)
    {
        cdcsu8Seed=0x7A;
    }
    else
    {
        ;
    }
    
}

static void CDCS_vCrcCheck(void)
{
    uint16_t  u16GenCrcKey, u16FcsKey;
               
    u16GenCrcKey=((uint16_t)(cdcsu8Seed))<<3;
    u16FcsKey=u16GenCrcKey;

    /*U8_DIAG_CRC_GEN_CODE = 1101(0x0D)*/

    if(u16FcsKey>=1024)
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*128); 
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=512)&&(u16FcsKey<1024))
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*64);
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=256)&&(u16FcsKey<512))
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*32);
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=128)&&(u16FcsKey<256))
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*16);
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=64)&&(u16FcsKey<128))
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*8);
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=32)&&(u16FcsKey<64))
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*4);
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=16)&&(u16FcsKey<32))
    {
        u16FcsKey^=(U8_DIAG_CRC_GEN_CODE*2);
    }
    else
    {
        ;
    }
        
    if((u16FcsKey>=8)&&(u16FcsKey<16))
    {
        u16FcsKey^=U8_DIAG_CRC_GEN_CODE;
    }
    else
    {
        ;
    }
      
    cdcsu8CrcKey1=(uint8_t)((u16GenCrcKey+u16FcsKey)>>8);
    cdcsu8CrcKey2=(uint8_t)(u16GenCrcKey+u16FcsKey);
}               

void CDCS_vInitSecurity(void)
{
    cdcsu1SecurityOkFlg=0;
    cdcsu1AccesModeFlg=0;
}


static void CDCS_vRMBARequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_RMBA_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        /*===========================================================
        Version_1.1
        1. EEPROM 전영역을 Read할 수 있도록 변경함.
        ===========================================================*/
    	/* For Compile MSW : KCLim NvM */
    	if(cdclu8RxBuffer[5]==0x04)
        {
            cdcsu16WriteAddress=cdclu8RxBuffer[3];
            cdcsu16WriteAddress<<=8;
            cdcsu16WriteAddress+=(cdclu8RxBuffer[4]+EEP_FW_SEGMENT);
            cdcsu1ReadProdFlg=1;
        }
        else if(cdclu8RxBuffer[5]==0xF0)
        {
            CDCS_vRMBAResponseFlash();
        }
        else
        {
            CDCS_vNegativeResponse(U8_DIAG_RMBA_REQUEST_ID, U8_DIAG_NOT_UPLOAD_NUMBER);
        }
    }
}



static void CDCS_vRMBAResponse(void)
{
    CDCS_vSendData(U8_DIAG_RMBA_RESPONSE_ID, 0);
    CDCS_vSendData(cdcsu8EepromData[0], 1);
    CDCS_vSendData(cdcsu8EepromData[1], 2);
    CDCS_vSendData(cdcsu8EepromData[2], 3);
    CDCS_vSendData(cdcsu8EepromData[3], 4);
  
    cdclu16ServiceDataSum=0x05;
    cdclu1SendReadyFlg=1;
}

static void CDCS_vRMBAResponseFlash(void)
{
    CDCS_vSendData(U8_DIAG_RMBA_RESPONSE_ID, 0);
    cdclu1UploadFlashFlg=1;
    cdcsu32FlashAddr=(((uint32_t)cdclu8RxBuffer[2])<<16)+(((uint32_t)cdclu8RxBuffer[3])<<8)+(uint32_t)cdclu8RxBuffer[4];
    
    cdclu16ServiceDataSum=cdclu8RxBuffer[5]+1;
    cdclu1SendReadyFlg=1;
}


static void CDCS_vWMBARequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_WMBA_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if((cdclu8RxBuffer[2]==0x00)&&(cdclu8RxBuffer[3]==0x00))
        {
            if(cdclu8RxBuffer[5]==0x01)
            {
                if((cdclu8RxBuffer[4]>=U8_DIAG_START_EE_ADDR)&&(cdclu8RxBuffer[4]<=U8_DIAG_END_EE_ADDR))
                {
                    cdcsu16WriteAddress=cdclu8RxBuffer[3];
                    cdcsu16WriteAddress<<=8;
                    /* For Compile MSW  : KCLim NvM */cdcsu16WriteAddress+=(cdclu8RxBuffer[4]+EEP_FW_SEGMENT);
                    cdcsu8WriteData=cdclu8RxBuffer[6];
                    cdcsu1WriteProdFirstFlg=1;
                    cdcsu1WriteProdFlg=1;
                }
                /*===========================================================
                Version_1.3
                1. BBOX 기능 구현
                   >. MEC Write 기능 추가
                ===========================================================*/
                else if(cdclu8RxBuffer[4]==0xEE)
                {
                    cdu8Mec=cdclu8RxBuffer[6];
                    cdu1ChkMecFlg=0;
                    CDCS_vWMBAResponse();
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_WMBA_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                }
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_WMBA_REQUEST_ID, U8_DIAG_NOT_DOWNLOAD_NUMBER);
            }
        }
        else
        {
            CDCS_vNegativeResponse(U8_DIAG_WMBA_REQUEST_ID, U8_DIAG_NOT_DOWNLOAD_ADDR);
        }
    }
}



static void CDCS_vWMBAResponse(void)
{
    CDCS_vSendData(U8_DIAG_WMBA_RESPONSE_ID, 0);
    
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}

/*===========================================================
Version_1.1
1. Ax snsr Offset 보정 서비스 신규 추가
2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
   신규 서비스로 분리함.
3. Pedal, Pressure Calibration 구문 추가함.
===========================================================*/
#if ((__DIAG_CAL_AX_ENABLE==1)||(__DIAG_CAL_AX_SAS_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL==ENABLE)||(__PRESSURE_SENSOR_DIAG_CAL==ENABLE))
static void CDCS_vClearEepRequest(void)
{
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_CLEEP_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        cdcsu1EraseEepFlg=1;
        /* Pedal, Pressure Clear E2P Request */
        #if __PRESSURE_SENSOR_DIAG_CAL == ENABLE
        DiagCalPrs.u1CalOfsEraseFlg=1;
        #endif
        #if __PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE
        DiagCalPdl.u1CalOfsEraseFlg=1;
        #endif

        
        CDCS_vClearEepResponse();
    }
}


static void CDCS_vClearEepResponse(void)
{
    CDCS_vSendData(U8_DIAG_CLEEP_RESPONSE_ID, 0);
    
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}

#endif

#if __SAS_CAL_TYPE==BY_DIAG
static void CDCS_vSetTimeCalSas(void)
{
    /*===========================================================
    Version_1.4
    1. MGH-60 수평 전개
       : 종G센서 고장 시 SAS Cal을 통한 종G 센서 Offset 보정 
         서비스 금지
    ===========================================================*/
    #if __DIAG_CAL_AX_SAS_ENABLE==1
        if(fgu1AxHwErrDet==0)
        {
            cdcsu1AxCalInhibitFlg=fu1AxErrorDet;
        }
        else
        {
            #if __G_SENSOR_TYPE==CAN_TYPE
            if((fu1ESCSensorPowerErrDet==1)||(fu1SubCanLineErrDet==1)||(fgu1AxHwErrDetRef==1))
            #else
            if((fu1Power5VErrDet==1)||(fgu1AxHwErrDetRef==1))
            #endif
            {
                cdcsu1AxCalInhibitFlg=1;
            }
            else
            {
                cdcsu1AxCalInhibitFlg=0;
            }
        }
        
        if(cdcsu1AxOfsEepStart==1)
        {
            if(lsabsu1AxOfsEepCalEndFlg==1) 
            {
                cdcsu1AxOfsEepStart=0;
                cdcsu1AxOfsEepCalEndFlg=1;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    #endif

    if((cdcsu1CalSasFlg==1)||(cdcsu1DeCalSasFlg==1))
    {
        if(cdcsu8WaitCalSasTimer!=0)
        {
            if(cdcsu1DeCalSasFlg==1)
            {
                if(fcu1LWSCal==0)
                {
                    cdcsu8WaitCalSasTimer=0;
                }
                else 
                {
                     cdcsu8WaitCalSasTimer=cdcsu8WaitCalSasTimer-1;
                     
                    if(cdcsu8WaitCalSasTimer==U8_DIAG_T_500MS)
                    {
                        cdclu8TerminationStatus=U8_TX_RESPONSE_NO_REQ;
                        CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQ_CORRECT_RSP_PENDING);
                    }
                    else
                    {
                        ;
                    }
                    /*cdcsu8WaitCalSasTimer--;*/
                }
            }
            else
            {
                ;
            }
            
            if(cdcsu1CalSasFlg==1)
            {
                if(fcu1LWSCal==1)
                {
                /*===========================================================
                Version_1.1
                1. Ax snsr Offset 보정 서비스 신규 추가
                2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
                   신규 서비스로 분리함.
                ===========================================================*/
                /*===========================================================
                Version_1.4
                1. MGH-60 수평 전개
                   : 종G센서 Offset 보정 성공 시 보정치를 EEPROM에 기록하도록 
                     변경함.
                ===========================================================*/    
                    #if __DIAG_CAL_AX_SAS_ENABLE==1
                        if(cdcsu1AxCalInhibitFlg==0)
                        {
                            if(cdcsu1AxOfsEepCalEndFlg==1) 
                            {
                                WS_vWriteAxOfsToEEP();
                                    
                                if(wbu1WriteAxOfsEepEnd==1)
                                {
                                    cdcsu8WaitCalSasTimer=0;
                                    cdcsu1AxOfsWEepReqFlg=0;
                                    cdcsu1AxOfsEepCalEndFlg=0;
                                }
                                else
                                {
                                    ;
                                }
                            }
                            else 
                            {
                                if(cdcsu1AxOfsWEepReqFlg==0)
                                {
                                    cdclu8TerminationStatus=U8_TX_RESPONSE_NO_REQ;
                                    CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQ_CORRECT_RSP_PENDING);
                                    cdcsu1AxOfsWEepReqFlg=1;
                                }
                                else
                                {
                                    ;
                                }
                            }
                        }
                        else
                        {
                            cdcsu8WaitCalSasTimer=0;
                        }
                    #else
                        cdcsu8WaitCalSasTimer=0;
                    #endif
                }
                else 
                {
                    if(cdcsu8WaitCalSasTimer>0)
                    {
                        cdcsu8WaitCalSasTimer=cdcsu8WaitCalSasTimer-1;
                    }
                    
                    if(cdcsu8WaitCalSasTimer==U8_DIAG_T_500MS)
                    {
                        cdclu8TerminationStatus=U8_TX_RESPONSE_NO_REQ;
                        CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQ_CORRECT_RSP_PENDING);
                        cdcsu1AxOfsWEepReqFlg=1;
                    }
                    else
                    {
                        ;
                    }
                    /*cdcsu8WaitCalSasTimer--;*/
                }
            }
            else
            {
                ;
            }
        }
        else
        {
        	;
        }
        if(cdcsu8WaitCalSasTimer==0)
        {
            if(cdclu8TerminationStatus==0)
            {
		        cdcsu1CalSasFlg=cdcsu1DeCalSasFlg=0;
	            CDCS_vCalSasResponse();             	
            }      	
        }
        else
        {
        	;
        }       
    }
    else
    {
        ;
    }
}
#endif

/*===========================================================
Version_1.1
1. Ax snsr Offset 보정 서비스 신규 추가
2. SAS Calibration service 시 수행하던 Ax Offset 보정 서비스
   신규 서비스로 분리함.
===========================================================*/
#if ((__SAS_CAL_TYPE==BY_DIAG)||(__DIAG_CAL_AX_ENABLE==1)||(__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))

static void CDCS_vCalSasRequest(void)
{ 
    if (cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F))
    {
          CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if((cdu1DisabledNetFlg==0)&&(cdu1WrongSysVoltFlg==0))
        {
            switch(cdclu8RxBuffer[2])
            {
                #if __SAS_CAL_TYPE==BY_DIAG
                    case U8_DIAG_CALSAS_RLID:
                        if(cdclu8RxBuffer[3]==U8_DIAG_START_CALSAD)
                        {
                            if((cdclu8RxBuffer[4]==U8_DIAG_SAS_CAL)||(cdclu8RxBuffer[4]==U8_DIAG_SAS_DECAL))
                            {
                                if(cdclu8RxBuffer[4]==U8_DIAG_SAS_DECAL)
                                {
                                    ctu1ResetSas=1;
                                    cdcsu1DeCalSasFlg=1;
                                    cdcsu1DeCalSasChkFlg=1;
                                    #if __DIAG_CAL_AX_SAS_ENABLE==1
                                        if((lsabsu1AxOfsEepCalEndFlg==0)&&(cdcsu1AxCalInhibitFlg==0))
                                        {
                                            cdcsu1AxOfsEepStart=1;
                                        }
                                        else
                                        {
                                            ;
                                        }
                                    #endif  
                                }
                                else if(cdclu8RxBuffer[4]==U8_DIAG_SAS_CAL)
                                {
                                    if(fcu1LWSCal==1)
                                    {
                                        cdcsu1OverrunCalSasFlg=1;
                                        CDCS_vCalSasResponse();
                                    }
                                    else
                                    {
                                        ctu1StartChangeSas=1;
                                        cdcsu1CalSasFlg=1;
                                        cdcsu1CalSasChkFlg=1;
                                        cdu1DiagSasCaltoCL=1;
                                    }
                                }
                                else
                                {
                                    ;
                                }
                                cdcsu8WaitCalSasTimer=U8_CAL_SAS_WAIT_INIT;
                            }
                            else
                            {
                                 CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                            }
                        }
                        else
                        {
                            CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                        }
                    break;
                #endif
                
                #if __DIAG_CAL_AX_ENABLE==1
              
                    case U8_DIAG_CALAXSNSR_RLID:
                     
                        #if defined(DIAG_HSA_CODING_ENABLE)

                            if(ccu1HsaVcSetFlg==1)
                            {
                                
                                CDCS_vCalAxSnsrRequest();
                            }
                            else
                            {
                               
                                
                                CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                            }
                        #else

                            CDCS_vCalAxSnsrRequest();
                        #endif
                    break;
                #endif
                
                #if (__PRESSURE_SENSOR_DIAG_CAL == ENABLE)
                case U8_DIAG_CAL_PRS_RLID:
                    CDCS_vCalSnsrReqChk(&DiagCalPrs, U8_DIAG_CAL_PRS_RLID);
                break;
                #endif
                
                #if (__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE)
                case U8_DIAG_CAL_PDL_RLID:
                    CDCS_vCalSnsrReqChk(&DiagCalPdl, U8_DIAG_CAL_PDL_RLID);
                break;       
                #endif         
                //////////////////////////////////////////////////////////F0119 Taeho: SWD_V0
                #if(__MOTOR_POSITION_SENSOR_DIAG_CAL_OFFLINE == ENABLE)
                case U8_DIAG_CAL_MPS_OFFLINE_RLID:
                	CDCS_vCalMPSReqChk(&DiagCalMPS_Offline, U8_DIAG_CAL_MPS_OFFLINE_RLID);
                break;
				#endif
                /////////////////////////////////////////////////////////////////////////////

                
                default:
                    CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
                break;
            }
        }
        else
        {
            CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
        }   
    }
 
}
#endif

#if __SAS_CAL_TYPE==BY_DIAG
static void CDCS_vCalSasResponse(void)
{
    CDCS_vSendData(U8_DIAG_CALSAS_RESPONSE_ID, 0);
    CDCS_vSendData(U8_DIAG_CALSAS_RLID, 1);
    
    if((feu1SAS_HW_Line_err_flg==1)&&(BUS_off_err_flg==1)&&(CAN_hw_err_flg==1)&&(CAN_bus_off_flg==0)&&(cdcsu1OverrunCalSasFlg==0))
    {
        if(cdclu8RxBuffer[4]==U8_DIAG_SAS_DECAL)
        {
            if(fcu1LWSCal==0)
            {
                CDCS_vSendData(U8_DIAG_CAL_OK, 2);
            }
            else
            {
                CDCS_vSendData(U8_DIAG_CAL_NG, 2);
            }
        }
        else if(cdclu8RxBuffer[4]==U8_DIAG_SAS_CAL)
        {
            if(fcu1LWSCal==1)
            {
                CDCS_vSendData(U8_DIAG_CAL_OK, 2);
                cdu1DiagSasCaltoCL=0;
            }
            else
            {
                CDCS_vSendData(U8_DIAG_CAL_NG, 2);
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        CDCS_vSendData(U8_DIAG_CAL_NG, 2);
        cdcsu1OverrunCalSasFlg=0;
    }
    
    cdclu16ServiceDataSum=0x03;
    cdclu1SendReadyFlg=1;
}
#endif /* __SAS_CAL_TYPE==BY_DIAG */

#if ((__DIAG_VARIANR_CODE_SET_SYS==1)||(defined(DIAG_INLINE_CODING_ENABLE)))
static void CDCS_vResetVariantCode(void)
{
	#ifdef DIAG_INLINE_CODING_ENABLE
    static uint16_t cdu16_InCodeWaitTimer=0;
    uint8_t cdu8_ChkCodeStat1=0;
    #endif
    
    #if __DIAG_VARIANR_CODE_SET_SYS==1
    if(cdcsu1VcCompleteFlg==1)
    {
        cdcsu1WaitVcFlg=0;
        cdcsu1ResetVcFlg=0;
        cdcsu1ClrVcDtcFlg=1;
        if(cdclu8TerminationStatus==0)
        {
            cdcsu1VcCompleteFlg=0;
            cdcsu8WaitVcTimer=0;            
            CDCS_vRVCResponse();
        }
        else
        {
            ;
        }
    }
    else
    {
        if(cdcsu1WaitVcFlg==1)
        {
            cdcsu8WaitVcTimer=cdcsu8WaitVcTimer-1;
            
            if(cdcsu8WaitVcTimer==0)
            {
                cdcsu1WaitVcFlg=0;
                cdclu8TerminationStatus=U8_TX_RESPONSE_NO_REQ;
                CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_REQ_CORRECT_RSP_PENDING);
            }
            else
            {
                ;/*cdcsu8WaitVcTimer--;*/
            }
        }
        else
        {
            ;
        }
    }
    #endif
    
    #ifdef DIAG_INLINE_CODING_ENABLE
        if(cdu1InlineCodeRequest==1)
        {
            cdu8_ChkCodeStat1=CDCS_u8ChkInLineCode();
            
            if(cdu8_ChkCodeStat1!=0)
            {
            	/* Inline Coding Response 수정 */
            	cdu16_InCodeWaitTimer=cdu16_InCodeWaitTimer+1;
            	
                if(cdu16_InCodeWaitTimer==U8_RCRRP_WAIT_TIME)
                {
                    cdclu8TerminationStatus=U8_TX_RESPONSE_NO_REQ;
                    CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_REQ_CORRECT_RSP_PENDING);
                }
                else if(cdu16_InCodeWaitTimer==U16_DIAG_T_5S)
                {
                    cdu16_InCodeWaitTimer=0;
                    cdu1InlineCodeRequest=0;
                    CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                }
                else
                {
                    ;
                }              
            }
            else
            {
               cdcsu1ClrInlineDtcFlg=1;
               if(cdclu8TerminationStatus==0)
               {
               cdu16_InCodeWaitTimer=0;
               cdu1InlineCodeRequest=0;
               CDCS_vRVCResponse();
            }
        }
        }
        else
        {
            ;
        }
    #endif
        
    if(cdu1ClearVarCodeRequest==1)
    {
        if(cdu1ClearVarCodeStart==0)
        {
            cdu1ClearVarCodeRequest=0;
            
            CDCS_vRVCResponse();   
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
    
    #if ((__DIAG_VARIANR_CODE_SET_SYS==1)&&(defined(DIAG_INLINE_CODING_ENABLE)))
    if(((cdcsu1ClrVcDtcFlg==1)||(cdcsu1ClrInlineDtcFlg==1))&&(ccu1SysCodingOkFlg==1))
    #else
        #if __DIAG_VARIANR_CODE_SET_SYS==1
        if(cdcsu1ClrVcDtcFlg==1)
        #else
            #if defined(DIAG_INLINE_CODING_ENABLE)
            if(cdcsu1ClrInlineDtcFlg==1)
            #endif
        #endif
    #endif
    {
        cdcsu1ClearVcErrFlg=1; 
        cdcsu1ClrVcDtcFlg=0;
        cdcsu1ClrInlineDtcFlg=0;  
    }
    else
    {
        ;
    }
}

static void CDCS_vRVCRequest(void)
{
    uint8_t cdu8VcPreCondi=0;
    #if (__CAR_MAKER==SSANGYONG)
        uint8_t cdcsu8TempDrvMode=0;
    #endif
    #ifdef DIAG_INLINE_CODING_ENABLE
        uint8_t cdu8_ChkCodeStat0;
    #endif
    
    #if __DIAG_VARIANR_CODE_SET_SYS==1
    #if __CAR_MAKER==SSANGYONG
        if(ccu1Variant_decode_ok_flg==1)
        {
        cdcsu8TempDrvMode=wbu8FwdDrvMode&0x07;
        
        if((cdcsu8TempDrvMode==DM_2WD)||(cdcsu8TempDrvMode==DM_2WD_EPS)||(cdcsu8TempDrvMode==DM_4WD)||(cdcsu8TempDrvMode==DM_4WD_EPS))
        {
            cdu8VcPreCondi=0;    
        }
        else
        {
            cdu8VcPreCondi=1;     
            }
        }
        else
        {
            cdu8VcPreCondi=1;     
            
        }
    #endif
    
    #if defined(DCT_TM_VARIANT_ENABLE)
        if(wu8CanDctTmVariantData==0)
        {
            cdu8VcPreCondi=1; 
        }
        else
        {
            cdu8VcPreCondi=0;
        }
    #endif
    #endif
    
    if((cdu1DisabledNetFlg==1)||(cdu1WrongSysVoltFlg==1))
    {
        cdu8VcPreCondi=1;
    }
    else
    {
        ;
    }   
        
    if((cdclu8RxBuffer[2]==U8_DIAG_RVC_ROUTINE_CONT)&&(cdclu8RxBuffer[3]==U8_DIAG_RVC_RESET_INDEX))
    {
    if(cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
            #if __DIAG_VARIANR_CODE_SET_SYS==1
            if(cdu8VcPreCondi==1)
            {
                CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
            }
            else
            {
                cdcsu1ResetVcFlg=1;
                cdcsu1WaitVcFlg=1;
                cdcsu8WaitVcTimer=U8_RCRRP_WAIT_TIME;
            }
            #else
                CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #endif
        }
    }
    else
    {
        if(cdclu8RxBuffer[2]==0xF0)
        {
            if(cdcsu8CanDiagMode==U8_DIAG_SUPPLIER_MODE)
            {
                cdu1ClearVarCodeStart  =1;
                
                cdu1ClearVarCodeRequest=1;
            }
            else
            {
                CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            }
        }
        else
        {
            #ifdef DIAG_INLINE_CODING_ENABLE
            if(cdclu8RxBuffer[2]==0xE0)
            {
                if((cdclu8RxBuffer[0]&0x0F)==0x07) /* Inline Coding DLC */
                {
                    #if ESS_INLINE_ENABLE==ENABLE
                    if((cdclu8RxBuffer[3]&0x02)==0)
                    {
                        cdu2EssInlineCode=U8_INLINE_CODE_IGNORE;    
                    }
                    else
                    {
                        if((cdclu8RxBuffer[3]&0x01)==0)
                        {
                            cdu2EssInlineCode=U8_INLINE_CODE_DISABLE;
                        }
                        else 
                        {
                            cdu2EssInlineCode=U8_INLINE_CODE_ENABLE;
                        }   
                    }
                    #endif

                    #if HSA_INLINE_ENABLE==ENABLE
                    if((cdclu8RxBuffer[3]&0x08)==0)
                    {
                        cdu2HsaInlineCode=U8_INLINE_CODE_IGNORE;    
                    }
                    else
                    {
                        if((cdclu8RxBuffer[3]&0x04)==0)
                        {
                            cdu2HsaInlineCode=U8_INLINE_CODE_DISABLE;
                        }
                        else 
                        {
                            cdu2HsaInlineCode=U8_INLINE_CODE_ENABLE;
                        }   
                    }
                    #endif
                 
                    #if SPAS_INLINE_ENABLE==ENABLE
                    if((cdclu8RxBuffer[3]&0x20)==0)
                    {
                        cdu2SpasInlineCode=U8_INLINE_CODE_IGNORE;    
                    }
                    else
                    {
                        if((cdclu8RxBuffer[3]&0x10)==0)
                        {
                            cdu2SpasInlineCode=U8_INLINE_CODE_DISABLE;
                        }
                        else 
                        {
                            cdu2SpasInlineCode=U8_INLINE_CODE_ENABLE;
                        }   
                    }
                    #endif

                    #if AVH_INLINE_ENABLE==ENABLE
                    if((cdclu8RxBuffer[3]&0x80)==0)
                    {
                        cdu2AvhInlineCode=U8_INLINE_CODE_IGNORE;    
                    }
                    else
                    {
                        if((cdclu8RxBuffer[3]&0x40)==0)
                        {
                            cdu2AvhInlineCode=U8_INLINE_CODE_DISABLE;
                        }
                        else 
                        {
                            cdu2AvhInlineCode=U8_INLINE_CODE_ENABLE;
                        }   
                    }
                    #endif
                    
                    #if EPB_INLINE_ENABLE==ENABLE 
                    if((cdclu8RxBuffer[4]&0x02)==0)
                    {
                        cdu2EpbInlineCode=U8_INLINE_CODE_IGNORE;    
                    }
                    else
                    {
                        if((cdclu8RxBuffer[4]&0x01)==0)
                        {
                            cdu2EpbInlineCode=U8_INLINE_CODE_DISABLE;
                        }
                        else 
                        {
                            cdu2EpbInlineCode=U8_INLINE_CODE_ENABLE;
                        }   
                    }
                    #endif

                    /*-------------- Local Inline Coding --------------*/
                    if((cdclu8RxBuffer[7]==1)||(cdclu8RxBuffer[7]==2)||(cdclu8RxBuffer[7]==3))
                    {
                    #if REGION_INLINE_ENABLE == ENABLE
                        
                        if(cdclu8RxBuffer[7]==0)
                        {
                            cdu2LocalInlineCode=U8_INLINE_CODE_IGNORE;
                        }                       
                        #if DOM_LOCAL_INLINE_ENABLE==ENABLE
                        else if(cdclu8RxBuffer[7]==1) /* 03 */
                        {
                            /*cdu2DomInlineCode=U8_INLINE_CODE_ENABLE;  03 */
                            cdu2LocalInlineCode=U8_LOCAL_INLINE_DOM_SET; /* 1 */   
                        }
                        #endif
                        
                        #if NA_LOCAL_INLINE_ENABLE==ENABLE
                        else if(cdclu8RxBuffer[7]==2) /* 0C*/
                        {
                            /*cdu2NaInlineCode=U8_INLINE_CODE_ENABLE;  0C */
                            cdu2LocalInlineCode=U8_LOCAL_INLINE_NA_SET; /* 2 */   
                        }                    
                        #endif
                        
                        #if EU_LOCAL_INLINE_ENABLE==ENABLE
                        else if(cdclu8RxBuffer[7]==3) /* 30 */
                        {
                            /*cdu2EuInlineCode=U8_INLINE_CODE_ENABLE;  30 */
                            cdu2LocalInlineCode=U8_LOCAL_INLINE_EU_SET; /* 3 */ 
                        }  
                        #endif                 
                        else
                        {
                            CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
                        } 
                    #endif
                    }
                    /*-------------------------------------------------*/
                        
                    cdu8_ChkCodeStat0=CDCS_u8ChkInLineCode();

                    if(cdu8_ChkCodeStat0==0)
                    {
                        CDCS_vRVCResponse();
                    }
                    else
                    {
                        cdu1InlineCodeStart  =1;
                        cdu1InlineCodeRequest=1;
                    }
                }
                else
                {
                    CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);    
            }
        }
        else
        {
            CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
        }
            #else
                CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #endif
    }
}

}

static void CDCS_vRVCResponse(void)
{
    CDCS_vSendData(U8_DIAG_RVC_RESPONSE_ID, 0);
    CDCS_vSendData(cdclu8RxBuffer[2], 1);
    
    cdclu16ServiceDataSum=0x02;
    cdclu1SendReadyFlg=1;
}



uint8_t CDCS_u8ChkInLineCode(void)
{
    uint8_t cdu8_unmatchCnt;
    
    cdu8_unmatchCnt=0;
    
    #ifdef DIAG_INLINE_CODING_ENABLE
        #if ESS_INLINE_ENABLE==ENABLE
            if(cdu2EssInlineCode==U8_INLINE_CODE_DISABLE)
            {
                if(fu2EssInlineState!=2)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                }
            }
            else if(cdu2EssInlineCode==U8_INLINE_CODE_ENABLE)
            {
                if(fu2EssInlineState!=1)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                } 
            }
            else
            {
                ;
            }
        #endif
        
        #if HSA_INLINE_ENABLE==ENABLE
            if(cdu2HsaInlineCode==U8_INLINE_CODE_DISABLE)
            {
                if(fu2HsaInlineState!=2)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                }
            }
            else if(cdu2HsaInlineCode==U8_INLINE_CODE_ENABLE)
            {
                if(fu2HsaInlineState!=1)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                } 
            }
            else
            {
                ;
            }
        #endif
        
        #if SPAS_INLINE_ENABLE==ENABLE
            if(cdu2SpasInlineCode==U8_INLINE_CODE_DISABLE)
            {
                if(fu2SpasInlineState!=2)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                }
            }
            else if(cdu2SpasInlineCode==U8_INLINE_CODE_ENABLE)
            {
                if(fu2SpasInlineState!=1)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                } 
            }
            else
            {
                ;
            }
        #endif
        
        #if AVH_INLINE_ENABLE==ENABLE
            if(cdu2AvhInlineCode==U8_INLINE_CODE_DISABLE)
            {
                if(fu2AvhInlineState!=2)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                }
            }
            else if(cdu2AvhInlineCode==U8_INLINE_CODE_ENABLE)
            {
                if(fu2AvhInlineState!=1)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                } 
            }
            else
            {
                ;
            }
        #endif
        
        #if EPB_INLINE_ENABLE==ENABLE 
            if(cdu2EpbInlineCode==U8_INLINE_CODE_DISABLE)
            {
                if(fu2EpbInlineState!=2)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                }
            }
            else if(cdu2EpbInlineCode==U8_INLINE_CODE_ENABLE)
            {
                if(fu2EpbInlineState!=1)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                } 
            }
            else
            {
                ;
            }
        #endif
        
        /********************* Local Inline Code *****************************/
        #if (REGION_INLINE_ENABLE == ENABLE)
            if((cdu2LocalInlineCode==1)||(cdu2LocalInlineCode==2)||(cdu2LocalInlineCode==3))
            {
                if(fu2LocalInlineState!=cdu2LocalInlineCode)
                {
                    cdu8_unmatchCnt=cdu8_unmatchCnt+1;
                }
                else
                {
                    ;
                } 
            }
            else
            {
                ;
            }
        #endif
        /*********************************************************************/
    #endif
    
    return cdu8_unmatchCnt;
}
    
#endif /* __DIAG_VARIANR_CODE_SET_SYS==1 */

#if ((__PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE) || (__PRESSURE_SENSOR_DIAG_CAL == ENABLE))

static void CDCS_vSetSnrCalStatus(CDC_SnsrCalOfs_t *SnsrCalPtr)
{ 
    /*** Check Ax Offset Cal Complete ***/ 
    if(SnsrCalPtr->u1CalOfsEepStart==1)    
    {
        if(SnsrCalPtr->u1CalOfsEndByCl==1) 
        {
            SnsrCalPtr->u1CalOfsEepStart =0;
            SnsrCalPtr->u1CalOfsEepCalEnd=1;
        }
        else 
        {
            ;
        }
    }
    else
    {
        ;
    }
    
    /*** Write EEPROM & Termination service ***/
    if(SnsrCalPtr->u1CalOfsEepCalEnd==1)
    {
        if(SnsrCalPtr->u1CalOfsEndOk==1)
        {
            //if(SnsrCalPtr->u1CalOfsEepWrtEnd==1)
            if((weu1WriteSector==0)&&(weu1EraseSector==0))
            {
                SnsrCalPtr->u1CalOfsEepCalEnd =0;
                SnsrCalPtr->u16CalOfsWaitTimer=0;
                SnsrCalPtr->u1CalOfsResultOk  =1;                
                SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_SUCCESS;
            }
            else
            {
                SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_BUSY;
            }
        }
        else
        {
            SnsrCalPtr->u1CalOfsEepCalEnd =0;
            SnsrCalPtr->u16CalOfsWaitTimer=0;
            
            SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_FAIL;
            
            if(SnsrCalPtr->u1CalOfsInhibitFlg==1)
            {
                cdu8SnsrCalFailStat=U8_CAL_SNSR_FAIL_SYS_FAULT;
            }
            else
            {              
                if(SnsrCalPtr->u1CalOfsWrongCondi==1)
                {
                    cdu8SnsrCalFailStat=U8_CAL_SNSR_FAIL_WRONG_CONDI;
                }
                else
                {
                    cdu8SnsrCalFailStat=U8_CAL_SNSR_FAIL_OFFSET_NOISE;
                }
            }       
        }
    }
    else
    {
        ;
    }
    
    /*** Check Calibration service Time over ***/
    if(SnsrCalPtr->u8CalOfsStat==U8_CAL_SNSR_BUSY)
    {
        if(SnsrCalPtr->u16CalOfsWaitTimer>=U16_DIAG_T_5S)
        {
            SnsrCalPtr->u16CalOfsWaitTimer=0;
            SnsrCalPtr->u1CalOfsEepStart  =0;
            SnsrCalPtr->u1CalOfsEepCalEnd =0;
            
            SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_FAIL;
            cdu8SnsrCalFailStat=U8_CAL_SNSR_FAIL_EEP_FAULT;
        }
        else
        {
            SnsrCalPtr->u16CalOfsWaitTimer++;
        }
    }
    else
    {
        ;
    }
}

static void CDCS_vCalSnsrReqChk(CDC_SnsrCalOfs_t *SnsrCalPtr, uint8_t CAL_INDEX)
{ 
    uint8_t u8ConParam, u8Action;
    /*uint8_t cdu8_CalWrongCondi=0;*/
    /*uint8_t cdu8_RspLid;*/

    #if __DIAG_SPEC==SYMC_DIAG
        u8ConParam=cdclu8RxBuffer[3];
        u8Action  =0xFF;
    #else
        u8ConParam=cdclu8RxBuffer[3];
        u8Action  =cdclu8RxBuffer[4];
    #endif
    
    if(u8ConParam==0x00)
    {
        if(u8Action==0xFF)
        {
            if(SnsrCalPtr->u1CalOfsInhibitFlg==1)
            {
                cdu8SnsrCalFailStat=U8_CAL_SNSR_FAIL_SYS_FAULT;
                CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_FAIL);
                SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_INIT;
            }
            else
            {
                if(SnsrCalPtr->u8CalOfsStat==U8_CAL_SNSR_INIT)
                {                       
                    if(SnsrCalPtr->u1CalOfsWrongCondi==0)
                    {       
                        SnsrCalPtr->u1CalOfsEepStart=1;
                        SnsrCalPtr->u8CalOfsStat    =U8_CAL_SNSR_BUSY;
                        SnsrCalPtr->u1CalOfsResultOk=0;
                       
                        CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_BUSY);                                           
                    }
                    else
                    {
                        cdu8SnsrCalFailStat=U8_CAL_SNSR_FAIL_WRONG_CONDI;                        
                        CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_FAIL);
                        SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_INIT;
                    }
                }
                else if(SnsrCalPtr->u8CalOfsStat==U8_CAL_SNSR_SUCCESS)
                {
                    CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_SUCCESS);
                    SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_INIT;
                }
                else if(SnsrCalPtr->u8CalOfsStat==U8_CAL_SNSR_FAIL)
                {
                    CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_FAIL);
                    SnsrCalPtr->u8CalOfsStat=U8_CAL_SNSR_INIT;
                }
                else
                {                      
                    CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_BUSY);
                   
                }
            }
        }
        else
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
            #else
                CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
            #endif 
        }
    }
    else
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
        #else
            CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
        #endif 
    }
}

///////////////////////////////////////////////////////// F0119 Taeho: SWD_V0
static void CDCS_vCalMPSReqChk(CDC_SnsrCalOfs_t *SnsrCalPtr, uint8_t CAL_INDEX)
{
	uint8_t u8ConParam, u8Action;
	/*uint8_t cdu8_CalWrongCondi=0;*/
	/*uint8_t cdu8_RspLid;*/

	u8ConParam = cdclu8RxBuffer[3];
	u8Action   = cdclu8RxBuffer[4];
	//if(() || ((cdcsu8CanDiagMode == U8_DIAG_SUPPLIER_MODE))
	if((cdcsu8CanDiagMode == U8_DIAG_EXTENDED_MODE) || (cdcsu8CanDiagMode == U8_DIAG_SUPPLIER_MODE))
	{
		if(u8ConParam==0x00)
		{
			if(u8Action==0xFF)
			{
            			cdu1MSPCalStart = 1;/* request flag set*/
						Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 2;
				//F0119 Taeho: Define MPS calibration
				if(fu8MotorCalState==MOTOR_CAL_COMPLETE)
				{
					CDCS_vCalSnsrResponse(CAL_INDEX, U8_CAL_SNSR_SUCCESS);
				}
				else if(fu8MotorCalState==MOTOR_CAL_ERROR)
				{
					CDCS_vCalSnsrResponse(CAL_INDEX,U8_CAL_SNSR_FAIL);
				}
				else
				{
					CDCS_vCalSnsrResponse(CAL_INDEX,U8_CAL_SNSR_BUSY);
				}
			}
			else
			{
				//F0119 Taeho: OFF MPS calibration
				CDCS_vCalSnsrResponse(CAL_INDEX, U8_DIAG_SERVICE_NOT_SUPPORTED);
			}
		}
		else
		{
			CDCS_vNegativeResponse(CAL_INDEX, U8_DIAG_SERVICE_NOT_SUPPORTED);//<-- 
		}
	}
	else
	{
		CDCS_vNegativeResponse(CAL_INDEX, U8_DIAG_SERVICE_NOT_SUPPORTED);
	}

}
/////////////////////////////////////////////////////////////////////////////

static void CDCS_vCalSnsrResponse(uint8_t u8RspLid, uint8_t u8Result)
{
    #if __DIAG_SPEC==SYMC_DIAG
        CDCS_vSendData(U8_DIAG_IOCBLI_RESPONSE_ID, 0);
        CDCS_vSendData(u8RspLid, 1);
        CDCS_vSendData(0x00, 2);
        
        if(u8Result==0xFF)
        {
            CDCS_vSendData(cdu8SnsrCalFailStat, 3);
        }
        else
        {
            CDCS_vSendData(u8Result, 3);
        }
        
        cdclu16ServiceDataSum=0x04;
    #else
        CDCS_vSendData(U8_DIAG_CALSAS_RESPONSE_ID, 0);
        CDCS_vSendData(u8RspLid, 1);
        
        if(u8Result==0xFF)
        {
            CDCS_vSendData(cdu8SnsrCalFailStat, 2);
        }
        else
        {
            CDCS_vSendData(u8Result, 2);
        }
        
        cdclu16ServiceDataSum=0x03;
    #endif
    cdclu1SendReadyFlg=1;
}
/* Clear EEPROM about Pedal, Pressure Sensor */
/* Firmware Test 용도 */

static void CDCS_vClearEepRom(void)
{
	#if __PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE
    if(DiagCalPdl.u1CalOfsEraseFlg==1)
    {   
        if((Diag_wu1PedalPdt1EepEraseOk==1) &&(Diag_wu1PedalPdt2EepEraseOk==1))
        {
            DiagCalPdl.u1CalOfsEraseFlg=0;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
    #endif
    #if __PRESSURE_SENSOR_DIAG_CAL == ENABLE
    if(DiagCalPrs.u1CalOfsEraseFlg==1)
    {
        if((Diag_wu1PressureBcEepEraseOk==1)&&(Diag_wu1PressurePsEepEraseOk==1))
        {
            DiagCalPrs.u1CalOfsEraseFlg=0;
        }
        else
        {
            ;
        }
    }
    #endif
}

/* Pedal, Pressure Sensor Calibration Information */
static void Diag_vCopySensorCalibrationInform(void)
{    
    static uint8_t cdu8_PdlEepWrtOk=0;
    static uint8_t cdu8_PrsEepWrtOk=0;
    uint8_t cdu8_CalInhibit;
    
    if((Diag_min_speed>=U8_DIAG_MIN_SPEED)||(Diag_HndlrBus.Diag_HndlrIgnOnOffSts == OFF)/*||((fs1ExBlsSig==1)&&(fsu1BLSFaultDet==0))*/)
    {
        cdu8_CalInhibit=1;
    }
    else
    {
        cdu8_CalInhibit=0;
    }
    #if __PRESSURE_SENSOR_DIAG_CAL == ENABLE 
    DiagCalPrs.u1CalOfsInhibitFlg=Diag_fsu1PressureCalNGState;
    
    if(cdu8_CalInhibit==1)
    {
        DiagCalPrs.u1CalOfsWrongCondi=1;
    }
    else
    {
        DiagCalPrs.u1CalOfsWrongCondi=0;
    }
    
    DiagCalPrs.u1CalOfsEndByCl   =Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;//Diag_lsahbu1PressEEPofsCalcEND;
    DiagCalPrs.u1CalOfsEndOk     =Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;//Diag_lsahbu1PressEEPofsCalcOK;
    //DiagCalPrs.u1CalOfsEepWrtEnd =1; /* Eep Write End Flag Set */
    
    if(DiagCalPrs.u8CalOfsStat!=U8_CAL_SNSR_INIT)
    {
        if(Diag_wu1PressurePsEepWrtOk==1)
        {
            cdu8_PrsEepWrtOk|=0x01;
        }
        else
        {
            ;
        }
        
        if(Diag_wu1PressureBcEepWrtOk==1)
        {
            cdu8_PrsEepWrtOk|=0x02;
        }
        else
        {
            ;
        }
    }
    else
    {
        cdu8_PrsEepWrtOk=0;
    }

      
    if(cdu8_PrsEepWrtOk==0x03)
    {
        DiagCalPrs.u1CalOfsEepWrtEnd=1; 
        cdu8_PrsEepWrtOk=0;
    }
    else
    {
        DiagCalPrs.u1CalOfsEepWrtEnd=0;
    }
    #endif
    #if __PEDAL_TRAVLE_SENSOR_DIAG_CAL == ENABLE  
    DiagCalPdl.u1CalOfsInhibitFlg=Diag_fsu1PedalCalNGState;
     
    if(cdu8_CalInhibit==1)
    {
        DiagCalPdl.u1CalOfsWrongCondi=1;
    }
    else
    {
        DiagCalPdl.u1CalOfsWrongCondi=0;
    }
    
    DiagCalPdl.u1CalOfsEndByCl   =Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;//Diag_lsahbu1PedalEEPofsCalcEND;
    DiagCalPdl.u1CalOfsEndOk     =Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;//Diag_lsahbu1PedalEEPofsCalcOK;
    //DiagCalPdl.u1CalOfsEepWrtEnd =1;/* Eep Write End Flag Set */
    
    if(DiagCalPdl.u8CalOfsStat!=U8_CAL_SNSR_INIT)
    {
        if(Diag_wu1PedalPdt1EepWrtOk==1)
        {
            cdu8_PdlEepWrtOk|=0x01;
        }
        if(Diag_wu1PedalPdt2EepWrtOk==1)
        {
            cdu8_PdlEepWrtOk|=0x02;
        }
    }
    else
    {
        cdu8_PdlEepWrtOk=0;
    }
    
      
    if(cdu8_PdlEepWrtOk==0x03)
    {
        DiagCalPdl.u1CalOfsEepWrtEnd=1;
        cdu8_PdlEepWrtOk=0;
    }
    else
    {
        DiagCalPdl.u1CalOfsEepWrtEnd=0;
    }
    #endif
	fu8MotorCalState = Diag_HndlrBus.Diag_HndlrMtrDiagState;
}



#endif
/*===========================================================
Version_1.1
1. Ax snsr Offset 보정 서비스 신규 추가
===========================================================*/
#if __DIAG_CAL_AX_ENABLE==1
static void CDCS_vSetCalAxSnsrStatus(void)
{
    /*** Check Ax Snsr Fault ***/ 
    /*===========================================================
    Version_1.4
    1. MGH-60 수평 전개
       : 종G센서 고장 판단 조건에 Sensor Can-Lien Bus Off및 
         Sensor Power 고장 추가
    ===========================================================*/
       
    if(fgu1AxHwErrDet==0)
    {
        cdcsu1AxCalInhibitFlg=fu1AxErrorDet;
    }
    else
    {
        #if __G_SENSOR_TYPE==CAN_TYPE
        if((fu1ESCSensorPowerErrDet==1)||(fu1SubCanLineErrDet==1)||(fgu1AxHwErrDetRef==1))
        #else
        if((fu1Power5VErrDet==1)||(fgu1AxHwErrDetRef==1))
        #endif
        {
            cdcsu1AxCalInhibitFlg=1;
        }
        else
        {
            cdcsu1AxCalInhibitFlg=0;
        }
    }
    
    if((cdu1DisabledNetFlg==1)||(cdu1WrongSysVoltFlg==1))
    {
        cdcsu1AxCalInhibitFlg=1;
    }
    else
    {
        ;
    }   
    /*** Check Ax Offset Cal Complete ***/ 
    if(cdcsu1AxOfsEepStart==1)    
    {
        if(lsabsu1AxOfsEepCalEndFlg==1) 
        {
            cdcsu1AxOfsEepStart=0;
            cdcsu1AxOfsEepCalEndFlg=1;
        }
        else 
        {
            ;
        }
    }
    else
    {
        ;
    }
        
    
    
    /*** Write EEPROM & Termination service ***/
    if(cdcsu1AxOfsEepCalEndFlg==1)
    {
        if(lsabsu1AxOfsEepCalOkFlg==1)
        {
            WS_vWriteAxOfsToEEP();
                        
            if(wbu1WriteAxOfsEepEnd==1)
            {
                cdcsu1AxOfsEepCalEndFlg=0;
                cdcsu16CalAxTimer=0;
                cdcsu8CalAxOfsStat=U8_CALAX_SUCCESS;
            }
            else
            {
                cdcsu8CalAxOfsStat=U8_CALAX_BUSY;
            }
        }
        else
        {
            cdcsu1AxOfsEepCalEndFlg=0;
            cdcsu16CalAxTimer=0;
            cdcsu8CalAxOfsStat=U8_CALAX_FAILURE;
        }
    }
    else
    {
        ;
    }
    
    /*** Check Calibration service Time over ***/
    if(cdcsu8CalAxOfsStat==U8_CALAX_BUSY)
    {
        if(cdcsu16CalAxTimer>=U16_DIAG_T_5S)
        {
            cdcsu16CalAxTimer=0;
            
            cdcsu1AxOfsEepStart=0;
            cdcsu1AxOfsEepCalEndFlg=0;
            
            cdcsu8CalAxOfsStat=U8_CALAX_FAILURE;
        }
        else
        {
            cdcsu16CalAxTimer++;
        }
    }
    else
    {
        ;
    }
}
    
        
static void CDCS_vCalAxSnsrRequest(void)
{ 
   uint8_t u8ConParam, u8Action;
    
    /*===========================================================
    KR_Version_1.5
    1. SYMC C200 사양에 Ax Sensor Offset Calibration 서비스 추가
    ===========================================================*/
    #if __DIAG_SPEC==SYMC_DIAG
        u8ConParam=cdclu8RxBuffer[3];
        u8Action  =0xFF;
    #else
        u8ConParam=cdclu8RxBuffer[3];
        u8Action  =cdclu8RxBuffer[4];
    #endif       
    
    if(u8ConParam==0x00)
    {
        if(u8Action==0xFF)
        {
            /*===========================================================
                Version_1.11
                1. Ax Calibration Error를 제외한 나머지 Ax Error에 대해서만
                   Ax Calibration service를 제한하도록 변경함.
            ===========================================================*/
            if(cdcsu1AxCalInhibitFlg==1)
            {
                CDCS_vCalAxSnsrResponse(U8_CALAX_FAILURE);
            }
            else
            {
                if(cdcsu8CalAxOfsStat==U8_CALAX_INIT)
                {
                    /*===========================================================
                    KR_Version_1.5
                    1. AX sensor Offset 보정 속도 조건 변경
                       stand still --> Max Speed < 30KPH
                    2. MACRO에 의하여 동일 IGN 사이클에서 여러번 AX Cal Service
                       를 수행할 수 있도록 변경
                    ===========================================================*/
                    #if __DIAG_CONSECUTIVE_CAL_AX_ENABLE==1
                    if(max_speed<U16_DIAG_AX_OFS_SPEED_LIMIT)
                    #else
                    if((lsabsu1AxOfsEepCalEndFlg==0)&&(max_speed<U16_DIAG_AX_OFS_SPEED_LIMIT))
                    #endif
                    {
                        cdcsu1AxOfsEepStart=1;
                        cdcsu8CalAxOfsStat =U8_CALAX_BUSY;
                        CDCS_vCalAxSnsrResponse(U8_CALAX_BUSY);
                    }
                    else
                    {
                        #if __DIAG_SPEC==SYMC_DIAG
                            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                        #else
                            CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_CONDITIONS_NOT_CORRECT);
                        #endif 
                    }
                }
                else if(cdcsu8CalAxOfsStat==U8_CALAX_SUCCESS)
                {
                    CDCS_vCalAxSnsrResponse(U8_CALAX_SUCCESS);
                    cdcsu8CalAxOfsStat=U8_CALAX_INIT;
                }
                else if(cdcsu8CalAxOfsStat==U8_CALAX_FAILURE)
                {
                    CDCS_vCalAxSnsrResponse(U8_CALAX_FAILURE);
                    cdcsu8CalAxOfsStat=U8_CALAX_INIT;
                }
                else
                {   
                    CDCS_vCalAxSnsrResponse(U8_CALAX_BUSY);
                }
            }
        }
        else
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
            #else
                CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
            #endif 
        }
    }
    else
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResponse(U8_DIAG_IOCBLI_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
        #else
            CDCS_vNegativeResponse(U8_DIAG_CALSAS_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
        #endif 
    }
}


static void CDCS_vCalAxSnsrResponse(uint8_t u8Result)
{
    /*===========================================================
    KR_Version_1.5
    1. SYMC C200 사양에 Ax Sensor Offset Calibration 서비스 추가
    ===========================================================*/
    #if __DIAG_SPEC==SYMC_DIAG
        CDCS_vSendData(U8_DIAG_IOCBLI_RESPONSE_ID, 0);
        CDCS_vSendData(U8_DIAG_CALAXOFS, 1);
        CDCS_vSendData(0x00, 2);
        
        if(u8Result==0xFF)
        {
            CDCS_vSendData(0, 3);
        }
        else
        {
            CDCS_vSendData(u8Result, 3);
        }
        
        cdclu16ServiceDataSum=0x04;
    #else
        CDCS_vSendData(U8_DIAG_CALSAS_RESPONSE_ID, 0);
        CDCS_vSendData(U8_DIAG_CALAXSNSR_RLID, 1);
        
        if(u8Result==0xFF)
        {
            CDCS_vSendData(0, 2);
        }
        else
        {
            CDCS_vSendData(u8Result, 2);
        }
        
        cdclu16ServiceDataSum=0x03;
    #endif
    cdclu1SendReadyFlg=1;
}
#endif /* __DIAG_CAL_AX_ENABLE==1 */



#if __DIAG_SPEC==SYMC_DIAG
static void CDCS_vWDLIDRequest(void)
{
    uint8_t i;
    
    if(cdcsu8RxFrameLength!=cdcl8LenReqData) 
    {
        CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
    }
    else
    {
        if((cdclu8RxReqFrame[1]==U8_DIAG_ID_SYMC_OP_CODE_FP)||(cdclu8RxReqFrame[1]==U8_DIAG_ID_SYMC_OP_DATA_FP)||(cdclu8RxReqFrame[1]==U8_DIAG_ID_SYMC_OP_BOOT_FP))
        {
            cdcsu8EcuIdOption=cdclu8RxReqFrame[1];
            
            cdu8WrEepDataCnt=12;
            
            for(i=0;i<10;i++)
            {
                cdu8WrEepBuffer[i]=cdclu8RxReqFrame[i+2];
            }
            
            switch(cdcsu8EcuIdOption)
            {
                case U8_DIAG_ID_SYMC_OP_CODE_FP:
                    cdcsu16WriteAddress=U16_CODE_FINGER_EEP_ADDR;
                break;
                
                case U8_DIAG_ID_SYMC_OP_DATA_FP:
                    cdcsu16WriteAddress=U16_DATA_FINGER_EEP_ADDR;
                break;
                
                case U8_DIAG_ID_SYMC_OP_BOOT_FP:
                    cdcsu16WriteAddress=U16_BOOT_FINGER_EEP_ADDR;
                break;
                
                default:
                break;
            }
                                  
            CDCS_vWDLIDResponse(); 
        }
        else
        {
            CDCS_vNegativeResponse(U8_DIAG_RVC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
        }
    }
}

static void CDCS_vWDLIDResponse(void)
{
    uint8_t i;
    
    CDCS_vSendData(U8_DIAG_RVC_RESPONSE_ID, 0);
    CDCS_vSendData(cdcsu8EcuIdOption, 1);
    
    cdclu16ServiceDataSum=2;
    cdclu1SendReadyFlg=1;   
}

static void CDCS_vCNTDTCRequest(void)
{ 
    uint8_t u8FsChkEn;
    
    if(cdcsu8RxFrameLength!=(cdclu8RxBuffer[0]&0x0F)) 
    {
        #if __DIAG_SPEC==SYMC_DIAG
            CDCS_vNegativeResByFuncReq(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT); 
        #else
            CDCS_vNegativeResponse(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_INCORRECT_BYTE_COUNT);
        #endif
    }
    else
    {
        if((cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_REQUIRED)||(cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_NO_REQUIRED))
        {
            if((cdclu8RxBuffer[3]==0xFF)&&(cdclu8RxBuffer[4]==0x00))
            {
                if((cdclu8RxBuffer[5]==0x01)||(cdclu8RxBuffer[5]==0x02))
                {
                    if(cdclu8RxBuffer[5]==0x01)
                    {
                        u8FsChkEn=1;
                    }
                    else
                    {
                        u8FsChkEn=0;
                    }
                    
                    #if __DIAG_SPEC==SYMC_DIAG 
                        if(CDCS_vCheckFuncAddr()==1) 
                        {
                            return;
                        }
                        else
                        {
                            ;
                        }
                    #endif
                    
                    if (cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_REQUIRED) 
                    {
                        cdu1InhibitFsChk=u8FsChkEn;
                        CDCS_vCNTDTCResponse();
                    }
                    else if (cdclu8RxBuffer[2]==U8_DIAG_TP_RESPONSE_NO_REQUIRED) 
                    {
                        cdu1InhibitFsChk=u8FsChkEn;
                        cdclu8DiagStatus=7;
                    }   
                    else 
                    {
                        ;
                    }
                }
                else
                {
                    #if __DIAG_SPEC==SYMC_DIAG
                        CDCS_vNegativeResByFuncReq(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE); 
                    #else
                        CDCS_vNegativeResponse(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
                    #endif
                }   
            }
            else
            {
                #if __DIAG_SPEC==SYMC_DIAG
                    CDCS_vNegativeResByFuncReq(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE); 
                #else
                    CDCS_vNegativeResponse(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_REQUEST_OUT_RANGE);
                #endif
            }
        }
        else
        {
            #if __DIAG_SPEC==SYMC_DIAG
                CDCS_vNegativeResByFuncReq(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED); 
            #else
                CDCS_vNegativeResponse(U8_DIAG_CNTDTC_REQUEST_ID, U8_DIAG_SUB_FUNC_NOT_SUPPORTED);
            #endif
        }
    }
}
    
static void CDCS_vCNTDTCResponse(void)
{
    CDCS_vSendData(U8_DIAG_CNTDTC_RESPONSE_ID, 0);
    
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}
#endif /* __DIAG_SPEC==SYMC_DIAG */

#if defined(__FAILSAFE_SIGNAL_TEST)
static void CDCS_vFsTestRequest(void)
{
    cdu8FsMode_2 = cdclu8RxBuffer[2];
    cdu8FsMode_3 = cdclu8RxBuffer[3];
    cdu8FsMode_4 = cdclu8RxBuffer[4];
    cdu8FsMode_5 = cdclu8RxBuffer[5];
    cdu8FsMode_6 = cdclu8RxBuffer[6];
    cdu8FsMode_7 = cdclu8RxBuffer[7];
    
    cdu8FsMode_rx_ok=1;
    
    CDCS_vFsTestResponse();
}

static void CDCS_vFsTestResponse(void)
{
    CDCS_vSendData(U8_DIAG_FS_TEST_RESPONSE_ID, 0);
    
    cdclu16ServiceDataSum=0x01;
    cdclu1SendReadyFlg=1;
}
#endif
#include "Mcal.h"
#include "IfxScu_reg.h"
uint8_t SwResetPerformed_by_Appl;
uint8_t Parameter_by_Appl;
#define RESET_BY_APPL            0x5A
extern uint32 Mcal_SafetyResetEndInitCounter;
void WF_JumpCanDownload(void)
{
	/* step1.Leave message before application reset execution */
	SwResetPerformed_by_Appl = RESET_BY_APPL;
    Parameter_by_Appl = (BOOT_ENTRY_CAN | BOOT_ENTRY_AFTER_SECURITY_SERV); // parameter for BOOT_EntryPointFromEcuApplication function in Boot

    /* Step2.Reset Safety ENDINIT protection before accessing RSTCON2 and SWRSTCON register */
    Mcal_ResetSafetyENDINIT_Timed((uint32)150000U);
    Mcal_SafetyResetEndInitCounter--;
    //safety_clear = 0;

    /* Step3.RSTCON2 register will be preserved after reset. */
    //SCU_RSTCON.B.SW = 1; // software reset will generate system reset. RAM will be cleared.
    SCU_RSTCON2.B.USRINFO = (uint16_t)(((uint16_t)(SwResetPerformed_by_Appl<<8))|(Parameter_by_Appl));
    SCU_SWRSTCON.B.SWRSTREQ = ENABLE;

    /* Step4. Add some delay for HW to reset. But this shall not be met by s/w flow. */
	for(uint32_t i = 0U ; i< (uint32_t)90000; i++)
	{
	}
	Mcal_SetSafetyENDINIT(); // SW should not meet this line
}
#endif /* __CAN_DIAG_ENABLE==1 */

/*
=> Revision Note

=========================================================================================
Version_1.1 [2008.04.14] [PK Kang]
=========================================================================================

=========================================================================================
Version_1.3 [2009.09.04] [PK Kang]
=========================================================================================
변경 내역
  1. BBOX 기능 구현
     > MEC == 0이고, Supplier Mode일 경우에 Internal Error가 삭제.
     > MEC > 0 이면, 모든 Diag Mode에서 Internal Error가 삭제. 
  
  2. 추가 센서에 대한 service data 출력 기능 추가
     > FL/FR/RL/RR wheel pressure sensor
     > Vacuum sensor
     > Pedal travel sensor

=========================================================================================
Version_1.4 [2009.10.22] [PK Kang]
=========================================================================================
변경 내역
  1. 종G 센서 Offset 보정 서비스관련 고객 요구 사항 반영
     > 별동 보정 서비스 수행 시 
       -. Ax Error 조건에 Sensor CAN Bus-off, Sensor Power error 추가
       -. 진입 속도 조건 변경 : Stand still --> Max wheel speed < 30KPH
     > SAS Cal 서비스 수행시 종G 보정
       -. 종G 보정 완료 시 계산된 종G값을 EEPROM에 기록할 수 있도록 변경
       -. Ax Error 시 종G 보정 금지




*/

