/*
 * ErP_FsTestComplete.h
 *
 *  Created on: 2015. 1. 26.
 *      Author: jinsu.park
 */

#ifndef ERP_FSTESTCOMPLETE_H_
#define ERP_FSTESTCOMPLETE_H_

/* FsTest Byte 0 */
#if __CUSTOMER_DTC==HMC_DTC
    #define MASK_WHEEL_FL_ERROR_BYTE  0x03
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_WHEEL_FL_ERROR_BYTE  0x0F
#endif

#define feu1FlAirgapTestFlg      FsTest->InternalDTCByte0_Bit0
#define feu1FlPhaseTestFlg       FsTest->InternalDTCByte0_Bit1
#define feu1FlMjumpTestFlg       FsTest->InternalDTCByte0_Bit2
#define feu1FlPjumpTestFlg       FsTest->InternalDTCByte0_Bit3
#define feu1FlExciterLFlg        FsTest->InternalDTCByte0_Bit4
#define feu1FlExciterHFlg        FsTest->InternalDTCByte0_Bit5
#define feu1FlBrokenTTestFlg     FsTest->InternalDTCByte0_Bit6
#define feu1FlSplitTTestFlg      FsTest->InternalDTCByte0_Bit7

/* FsTest Byte 1 */
#if __CUSTOMER_DTC==HMC_DTC
    #define MASK_WHEEL_FR_ERROR_BYTE  0x03
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_WHEEL_FR_ERROR_BYTE  0x0F
#endif

#define feu1FrAirgapTestFlg       FsTest->InternalDTCByte1_Bit0
#define feu1FrPhaseTestFlg        FsTest->InternalDTCByte1_Bit1
#define feu1FrMjumpTestFlg        FsTest->InternalDTCByte1_Bit2
#define feu1FrPjumpTestFlg        FsTest->InternalDTCByte1_Bit3
#define feu1FrExciterLFlg         FsTest->InternalDTCByte1_Bit4
#define feu1FrExciterHFlg         FsTest->InternalDTCByte1_Bit5
#define feu1FrBrokenTTestFlg      FsTest->InternalDTCByte1_Bit6
#define feu1FrSplitTTestFlg       FsTest->InternalDTCByte1_Bit7

/* FsTest Byte 2 */
#if __CUSTOMER_DTC==HMC_DTC
    #define MASK_WHEEL_RL_ERROR_BYTE  0x03
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_WHEEL_RL_ERROR_BYTE  0x0F
#endif

#define feu1RlAirgapTestFlg       FsTest->InternalDTCByte2_Bit0
#define feu1RlPhaseTestFlg        FsTest->InternalDTCByte2_Bit1
#define feu1RlMjumpTestFlg        FsTest->InternalDTCByte2_Bit2
#define feu1RlPjumpTestFlg        FsTest->InternalDTCByte2_Bit3
#define feu1RlExciterLFlg         FsTest->InternalDTCByte2_Bit4
#define feu1RlExciterHFlg         FsTest->InternalDTCByte2_Bit5
#define feu1RlBrokenTTestFlg      FsTest->InternalDTCByte2_Bit6
#define feu1RlSplitTTestFlg       FsTest->InternalDTCByte2_Bit7

/* FsTest Byte 3 */
#if __CUSTOMER_DTC==HMC_DTC
    #define MASK_WHEEL_RR_ERROR_BYTE  0x03
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_WHEEL_RR_ERROR_BYTE  0x0F
#endif

#define feu1RrAirgapTestFlg      FsTest->InternalDTCByte3_Bit0
#define feu1RrPhaseTestFlg       FsTest->InternalDTCByte3_Bit1
#define feu1RrMjumpTestFlg       FsTest->InternalDTCByte3_Bit2
#define feu1RrPjumpTestFlg       FsTest->InternalDTCByte3_Bit3
#define feu1RrExciterLFlg        FsTest->InternalDTCByte3_Bit4
#define feu1RrExciterHFlg        FsTest->InternalDTCByte3_Bit5
#define feu1RrBrokenTTestFlg     FsTest->InternalDTCByte3_Bit6
#define feu1RrSplitTTestFlg      FsTest->InternalDTCByte3_Bit7

/* FsTest Byte 4 */
#define feu1FlHwOpenTestFlg       FsTest->InternalDTCByte4_Bit0
#define feu1FlHwShortTestFlg      FsTest->InternalDTCByte4_Bit1
#define feu1FrHwOpenTestFlg       FsTest->InternalDTCByte4_Bit2
#define feu1FrHwShortTestFlg      FsTest->InternalDTCByte4_Bit3
#define feu1RlHwOpenTestFlg       FsTest->InternalDTCByte4_Bit4
#define feu1RlHwShortTestFlg      FsTest->InternalDTCByte4_Bit5
#define feu1RrHwOpenTestFlg       FsTest->InternalDTCByte4_Bit6
#define feu1RrHwShortTestFlg      FsTest->InternalDTCByte4_Bit7

/* FsTest Byte 5 */
#define feu1FlLeakageTestFlg      FsTest->InternalDTCByte5_Bit0
#define feu1FlHwOverTempTestFlg   FsTest->InternalDTCByte5_Bit1
#define feu1FrLeakageTestFlg      FsTest->InternalDTCByte5_Bit2
#define feu1FrHwOverTempTestFlg   FsTest->InternalDTCByte5_Bit3
#define feu1RlLeakageTestFlg      FsTest->InternalDTCByte5_Bit4
#define feu1RlHwOverTempTestFlg   FsTest->InternalDTCByte5_Bit5
#define feu1RrLeakageTestFlg      FsTest->InternalDTCByte5_Bit6
#define feu1RrHwOverTempTestFlg   FsTest->InternalDTCByte5_Bit7

/* FsTest Byte 6 */
#define feu1FlWireTestFlg         FsTest->InternalDTCByte6_Bit0
#define feu1FrWireTestFlg         FsTest->InternalDTCByte6_Bit1
#define feu1RlWireTestFlg         FsTest->InternalDTCByte6_Bit2
#define feu1RrWireTestFlg         FsTest->InternalDTCByte6_Bit3
#define feu1FlTypeSwapTestFlg     FsTest->InternalDTCByte6_Bit4
#define feu1FrTypeSwapTestFlg     FsTest->InternalDTCByte6_Bit5
#define feu1RlTypeSwapTestFlg     FsTest->InternalDTCByte6_Bit6
#define feu1RrTypeSwapTestFlg     FsTest->InternalDTCByte6_Bit7

/* FsTest Byte 7*/
#define feu1FroOpenTestFlg        FsTest->InternalDTCByte7_Bit0
#define feu1FroShortTestFlg       FsTest->InternalDTCByte7_Bit1
#define feu1FriOpenTestFlg        FsTest->InternalDTCByte7_Bit2
#define feu1FriShortTestFlg       FsTest->InternalDTCByte7_Bit3
#define feu1FloOpenTestFlg        FsTest->InternalDTCByte7_Bit4
#define feu1FloShortTestFlg       FsTest->InternalDTCByte7_Bit5
#define feu1FliOpenTestFlg        FsTest->InternalDTCByte7_Bit6
#define feu1FliShortTestFlg       FsTest->InternalDTCByte7_Bit7

/* FsTest Byte 8 */
#define feu1RroOpenTestFlg        FsTest->InternalDTCByte8_Bit0
#define feu1RroShortTestFlg       FsTest->InternalDTCByte8_Bit1
#define feu1RriOpenTestFlg        FsTest->InternalDTCByte8_Bit2
#define feu1RriShortTestFlg       FsTest->InternalDTCByte8_Bit3
#define feu1RloOpenTestFlg        FsTest->InternalDTCByte8_Bit4
#define feu1RloShortTestFlg       FsTest->InternalDTCByte8_Bit5
#define feu1RliOpenTestFlg        FsTest->InternalDTCByte8_Bit6
#define feu1RliShortTestFlg       FsTest->InternalDTCByte8_Bit7

/* FsTest Byte 9 */
#define feu1Apv1OpenTestFlg       FsTest->InternalDTCByte9_Bit0
#define feu1Apv1ShortTestFlg      FsTest->InternalDTCByte9_Bit1
#define feu1Apv2OpenTestFlg       FsTest->InternalDTCByte9_Bit2
#define feu1Apv2ShortTestFlg      FsTest->InternalDTCByte9_Bit3
#define feu1Rlv1OpenTestFlg       FsTest->InternalDTCByte9_Bit4
#define feu1Rlv1ShortTestFlg      FsTest->InternalDTCByte9_Bit5
#define feu1Rlv2OpenTestFlg       FsTest->InternalDTCByte9_Bit6
#define feu1Rlv2ShortTestFlg      FsTest->InternalDTCByte9_Bit7

/* FsTest Byte 10 */
#define feu1FliOverTempTestFlg    FsTest->InternalDTCByte10_Bit0
#define feu1FriOverTempTestFlg    FsTest->InternalDTCByte10_Bit1
#define feu1FloOverTempTestFlg    FsTest->InternalDTCByte10_Bit2
#define feu1FroOverTempTestFlg    FsTest->InternalDTCByte10_Bit3
#define feu1RliOverTempTestFlg    FsTest->InternalDTCByte10_Bit4
#define feu1RriOverTempTestFlg    FsTest->InternalDTCByte10_Bit5
#define feu1RloOverTempTestFlg    FsTest->InternalDTCByte10_Bit6
#define feu1RroOverTempTestFlg    FsTest->InternalDTCByte10_Bit7

/* FsTest Byte 11 */
#if __CUSTOMER_DTC==HMC_DTC
    #define MASK_MOTOR_ERROR_BYTE    0xC2
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_MOTOR_ERROR_BYTE    0xC3
#endif

#define feu1MotFuseOpenTestFlg    FsTest->InternalDTCByte11_Bit0
#define feu1MotShortTestFlg       FsTest->InternalDTCByte11_Bit1 /* Not Used  */
#define feu1MotOpenTestFlg        FsTest->InternalDTCByte11_Bit2
#define feu1MrOpenTestFlg         FsTest->InternalDTCByte11_Bit3
#define feu1MotLockTestFlg        FsTest->InternalDTCByte11_Bit4
#define feu1MotOverTempFlg        FsTest->InternalDTCByte11_Bit5
#define feu1VBB_PowerTestflg	  FsTest->InternalDTCByte11_Bit6
#define feu1TCbyte11_bit7_err     FsTest->InternalDTCByte11_Bit7

/* FsTest Byte 12 */
#define MASK_VALVERELAY_ERROR_BYTE_0  0xE0
#define feu1VrBypassTestFlg       FsTest->InternalDTCByte12_Bit0
#define feu1VrOpenTestFlg         FsTest->InternalDTCByte12_Bit1
#define feu1VrShortGndTestFlg     FsTest->InternalDTCByte12_Bit2
#define feu1VrLeakageTestFlg      FsTest->InternalDTCByte12_Bit3
#define feu1VrShutdownTestFlg     FsTest->InternalDTCByte12_Bit4
#define feu1TCbyte12_bit5_err     FsTest->InternalDTCByte12_Bit5
#define feu1TCbyte12_bit6_err     FsTest->InternalDTCByte12_Bit6
#define feu1TCbyte12_bit7_err     FsTest->InternalDTCByte12_Bit7

/* FsTest Byte 13 */
#define feu1UnderPwrTestFlg       FsTest->InternalDTCByte13_Bit0
#define feu1OverPwrTestFlg        FsTest->InternalDTCByte13_Bit1
#define feu1LowPwrTestFlg         FsTest->InternalDTCByte13_Bit2
#define feu1HigherPwrTestFlg      FsTest->InternalDTCByte13_Bit3
#define feu1TCbyte13_bit4_err     FsTest->InternalDTCByte13_Bit4
#define feu1TCbyte13_bit5_err     FsTest->InternalDTCByte13_Bit5
#define feu1TCbyte13_bit6_err     FsTest->InternalDTCByte13_Bit6
#define feu1TCbyte13_bit7_err     FsTest->InternalDTCByte13_Bit7

/* FsTest Byte 14 */
#define feu15VSenPwrTestFlg       FsTest->InternalDTCByte14_Bit0
#define feu112VSenOpenTestFlg     FsTest->InternalDTCByte14_Bit1
#define feu112VSenShortTestFlg    FsTest->InternalDTCByte14_Bit2
#define feu1sen_5VPDFTestFlg	  FsTest->InternalDTCByte14_Bit3
#define feu1sen_5VPDTTestFlg	  FsTest->InternalDTCByte14_Bit4
#define feu1sen_5VP_PSNSRTestFlg  FsTest->InternalDTCByte14_Bit5
#define feu1ign_line_openTestFlg  FsTest->InternalDTCByte14_Bit6
#define feu1AbsLongTermTestFlg    FsTest->InternalDTCByte14_Bit7

/* FsTest Byte 15 */
#define MASK_ECU_ERROR_BYTE_0     0x00
#define feu1AdcTestFlg            FsTest->InternalDTCByte15_Bit0
#define feu1RomTestFlg            FsTest->InternalDTCByte15_Bit1
#define feu1RamTestFlg            FsTest->InternalDTCByte15_Bit2
#define feu1EepromTestFlg         FsTest->InternalDTCByte15_Bit3
#define feu1CycleTimeTestFlg      FsTest->InternalDTCByte15_Bit4
#define feu1EccTestFlg            FsTest->InternalDTCByte15_Bit5
#define feu1ValveInitTestFlg      FsTest->InternalDTCByte15_Bit6
#define feu1InvalidTestflg        FsTest->InternalDTCByte15_Bit7

/* FsTest Byte 16 */
#if __ECU==ABS_ECU_1
    #define MASK_ECU_ERROR_BYTE_1     0x08
#else
    #define MASK_ECU_ERROR_BYTE_1     0xE8
#endif
#define feu1VddIcTestFlg          FsTest->InternalDTCByte16_Bit0
#define feu1CommAsicInitTestFlg   FsTest->InternalDTCByte16_Bit1
#define feu1CommAsicParityTestFlg FsTest->InternalDTCByte16_Bit2
#define feu1TCbyte16_bit3_err     FsTest->InternalDTCByte16_Bit3
#define feu1TCbyte16_bit4_err     FsTest->InternalDTCByte16_Bit4
#define feu1RollCntTestFlg        FsTest->InternalDTCByte16_Bit5
#define feu1SubMcuTestFlg         FsTest->InternalDTCByte16_Bit6
#define feu1CheckSumTestFlg       FsTest->InternalDTCByte16_Bit7

/* FsTest Byte 17 */
#define feu1Ems1TimeoutTestFlg       FsTest->InternalDTCByte17_Bit0
#define feu1Ems2TimeoutTestFlg       FsTest->InternalDTCByte17_Bit1
#define feu1TcuTimeoutTestFlg        FsTest->InternalDTCByte17_Bit2
#define feu1TcuUnmatchTestFlg        FsTest->InternalDTCByte17_Bit3
#define feu1Fwd1TimeoutTestFlg       FsTest->InternalDTCByte17_Bit4
#define feu1VariantFailTestFlg       FsTest->InternalDTCByte17_Bit5
#define feu1CanBusOffTestFlg         FsTest->InternalDTCByte17_Bit6
#define feu1CanOverrunTestFlg        FsTest->InternalDTCByte17_Bit7

/* FsTest Byte 18 */
#define feu1BodyInfTimeoutTestFlg      FsTest->InternalDTCByte18_Bit0
#define feu1BlsTimeoutTestFlg          FsTest->InternalDTCByte18_Bit1
#define feu1HCUTimeoutTestFlg          FsTest->InternalDTCByte18_Bit2
#define feu1SadsTimeoutTestFlg         FsTest->InternalDTCByte18_Bit3
#define feu1EpbTimeoutTestFlg          FsTest->InternalDTCByte18_Bit4
#define feu1SwtchTimeoutTestFlg        FsTest->InternalDTCByte18_Bit5
#define feu1SysPwrTimeoutTestFlg       FsTest->InternalDTCByte18_Bit6
#define feu1EPSTimeoutTestFlg          FsTest->InternalDTCByte18_Bit7

/* FsTest Byte 19 */
#define feu1VaccumTimeoutTestFlg     FsTest->InternalDTCByte19_Bit0
#define feu1VaccumSignalTestFlg      FsTest->InternalDTCByte19_Bit1
#define feu1Afs1TimeoutTestFlg       FsTest->InternalDTCByte19_Bit2
#define feu1Acc240TimeoutTestFlg     FsTest->InternalDTCByte19_Bit3
#define feu1EpbSerialTestFlg         FsTest->InternalDTCByte19_Bit4
#define feu1EcmSerialTestFlg         FsTest->InternalDTCByte19_Bit5
#define feu1TcmSerialTestFlg         FsTest->InternalDTCByte19_Bit6
#define feu1BcmSerialTestFlg         FsTest->InternalDTCByte19_Bit7

/* FsTest Byte 20 */
#define feu1EngTorqTestFlg           FsTest->InternalDTCByte20_Bit0
#define feu1EngAccelTestFlg          FsTest->InternalDTCByte20_Bit1
#define feu1TransmisTestFlg          FsTest->InternalDTCByte20_Bit2
#define feu1AwdCntrlTestFlg          FsTest->InternalDTCByte20_Bit3
#define feu1EngTorqRedcTestFlg       FsTest->InternalDTCByte20_Bit4
#define feu1BlsInvalidTestFlg        FsTest->InternalDTCByte20_Bit5
#define feu1DisSwInvalidTestFlg      FsTest->InternalDTCByte20_Bit6
#define feu1TCbyte20_bit7_err        FsTest->InternalDTCByte20_Bit7

/* FsTest Byte 21 */
#define feu1TCU5TimeoutTestFlg       FsTest->InternalDTCByte21_Bit0
#define feu1HCU1TimeoutTestFlg       FsTest->InternalDTCByte21_Bit1
#define feu1HCU2TimeoutTestFlg       FsTest->InternalDTCByte21_Bit2
#define feu1HCU3TimeoutTestFlg       FsTest->InternalDTCByte21_Bit3
#define feu1BmsTimeoutTestFlg        FsTest->InternalDTCByte21_Bit4
#define feu1BmsInvalidSigTestFlg     FsTest->InternalDTCByte21_Bit5
#define feu1McuTimeoutTestFlg        FsTest->InternalDTCByte21_Bit6
#define feu1McuInvalidSigTestFlg     FsTest->InternalDTCByte21_Bit7

/* FsTest Byte 22 */
#define MASK_ASIC_ERROR_BYTE_0  0x18
#define feu1CommAsicUnderVTestFlg    FsTest->InternalDTCByte22_Bit0
#define feu1CommAsicOverVTestFlg     FsTest->InternalDTCByte22_Bit1
#define feu1CommAsicFGNDTestFlg      FsTest->InternalDTCByte22_Bit2
#define feu1TCbyte22_bit3_err        FsTest->InternalDTCByte22_Bit3
#define feu1TCbyte22_bit4_err        FsTest->InternalDTCByte22_Bit4
#define feu1CommAsicFreqTestFlg      FsTest->InternalDTCByte22_Bit5
#define feu1CommAsicOstdTestFlg      FsTest->InternalDTCByte22_Bit6
#define feu1CommAsicVersionTestFlg   FsTest->InternalDTCByte22_Bit7

/* FsTest Byte 23 */
#define MASK_ASIC_ERROR_BYTE_1  0xFF
#define feu1TCbyte23_bit0_err        FsTest->InternalDTCByte23_Bit0
#define feu1TCbyte23_bit1_err        FsTest->InternalDTCByte23_Bit1
#define feu1TCbyte23_bit2_err        FsTest->InternalDTCByte23_Bit2
#define feu1TCbyte23_bit3_err        FsTest->InternalDTCByte23_Bit3
#define feu1TCbyte23_bit4_err        FsTest->InternalDTCByte23_Bit4
#define feu1TCbyte23_bit5_err        FsTest->InternalDTCByte23_Bit5
#define feu1TCbyte23_bit6_err        FsTest->InternalDTCByte23_Bit6
#define feu1TCbyte23_bit7_err        FsTest->InternalDTCByte23_Bit7

/* FsTest Byte 24 */
#if __CUSTOMER_DTC==HMC_DTC
    #if __G_SENSOR_TYPE==CAN_TYPE
        #if (YAW_SNSR_ACU_TYPE==ENABLE)
            #define MASK_AX_ERROR_BYTE_0  0x5F
        #else
        #define MASK_AX_ERROR_BYTE_0  0x45
        #endif
    #else
        #define MASK_AX_ERROR_BYTE_0  0x7F
    #endif
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_AX_ERROR_BYTE_0      0x71
#endif

#define feu1AxHwLineTestFlg             FsTest->InternalDTCByte24_Bit0
#define feu1CanAxRollTestFlg            FsTest->InternalDTCByte24_Bit1 /* Not Used */
#define feu1CanAxCheckSumTestFlg        FsTest->InternalDTCByte24_Bit2 /* Not Used */
#define feu1CanAxSwInternalTestFlg      FsTest->InternalDTCByte24_Bit3 /* Not Used */
#define feu1AxCANFuncTestFlg            FsTest->InternalDTCByte24_Bit4
#define feu1AxCbitTestFlg               FsTest->InternalDTCByte24_Bit5
#define feu1AXCalibrationTestflg        FsTest->InternalDTCByte24_Bit6
#define feu1AXNoiseTestflg              FsTest->InternalDTCByte24_Bit7

/* FsTest Byte 25 */
#if __CUSTOMER_DTC==HMC_DTC
    #define MASK_AX_ERROR_BYTE_1             0xC0
#elif __CUSTOMER_DTC==GM_DTC
    #define MASK_AX_ERROR_BYTE_1             0xC0
#endif

#define feu1AxStandoffsetTestFlg         FsTest->InternalDTCByte25_Bit0
#define feu1AxDrvShortoffsetTestFlg      FsTest->InternalDTCByte25_Bit1
#define feu1AxDrvLongoffsetTestFlg       FsTest->InternalDTCByte25_Bit2/* Not Used */
#define feu1AxStickTestFlg               FsTest->InternalDTCByte25_Bit3
#define feu1AxReverseTestFlg             FsTest->InternalDTCByte25_Bit4/* Not Used */
#define feu1AxRangeTestFlg               FsTest->InternalDTCByte25_Bit5/* Not Used */
#define feu1AxInitRunTestFlg             FsTest->InternalDTCByte25_Bit6/* dual can */
#define feu1SenTypeTestFlg               FsTest->InternalDTCByte25_Bit7/* dual can */

/* FsTest Byte 26 */
#define MASK_BLS_ERROR_BYTE_0      0xC0
#define feu1BlsBsTestFlg           FsTest->InternalDTCByte26_Bit0
#define feu1BlsHighTestFlg         FsTest->InternalDTCByte26_Bit1
#define feu1BlsLowTestFlg          FsTest->InternalDTCByte26_Bit2
#define feu1BlsVrefTestFlg         FsTest->InternalDTCByte26_Bit3
#define feu1BlsNoisTestFlg         FsTest->InternalDTCByte26_Bit4
#define feu1BsNoiseTestFlg         FsTest->InternalDTCByte26_Bit5
#define feu1BasNotLearnTestFlg     FsTest->InternalDTCByte26_Bit6
#define feu1TCbyte26_bit7_err      FsTest->InternalDTCByte26_Bit7

/* FsTest Byte 27 */
#define feu1TcoffLampTestFlg      FsTest->InternalDTCByte27_Bit0
#define feu1FuncLampTestFlg       FsTest->InternalDTCByte27_Bit1
#define feu1VDCLampTestFlg        FsTest->InternalDTCByte27_Bit2
#define feu1HDCLampTestFlg        FsTest->InternalDTCByte27_Bit3
#define feu1AbsLampTestFlg        FsTest->InternalDTCByte27_Bit4
#define feu1VDCOffLampTestFlg     FsTest->InternalDTCByte27_Bit5
#define feu1TCbyte27_bit6_err     FsTest->InternalDTCByte27_Bit6
#define feu1TCbyte27_bit7_err     FsTest->InternalDTCByte27_Bit7

/* FsTest Byte 28 */
#define feu1VdcSwTestFlg             FsTest->InternalDTCByte28_Bit0
#define feu1HdcSwTestFlg             FsTest->InternalDTCByte28_Bit1
#define feu1CalNotProgTestFlg        FsTest->InternalDTCByte28_Bit2
#define feu1HDCRelayOffTestOkFlg     FsTest->InternalDTCByte28_Bit3
#define feu1ESSDriveOnTestOkFlg      FsTest->InternalDTCByte28_Bit4
#define feu1ESSDriveOffTestOkFlg     FsTest->InternalDTCByte28_Bit5
#define feu1ESSRelayOnTestOkFlg      FsTest->InternalDTCByte28_Bit6
#define feu1ESSRelayOffTestOkFlg     FsTest->InternalDTCByte28_Bit7

/* FsTest Byte 29 */
#define feu1TCEcebyte29_bit0_err        FsTest->InternalDTCByte29_Bit0
#define feu1TCEcebyte29_bit1_err        FsTest->InternalDTCByte29_Bit1
#define feu1TCEcebyte29_bit2_err        FsTest->InternalDTCByte29_Bit2
#define feu1TCEcebyte29_bit3_err        FsTest->InternalDTCByte29_Bit3
#define feu1TCEcebyte29_bit4_err        FsTest->InternalDTCByte29_Bit4
#define feu1TCEcebyte29_bit5_err        FsTest->InternalDTCByte29_Bit5
#define feu1TCEcebyte29_bit6_err        FsTest->InternalDTCByte29_Bit6
#define feu1TCEcebyte29_bit7_err        FsTest->InternalDTCByte29_Bit7

/* FsTest Byte 30 */
#define feu1TCEcebyte30_bit0_err        FsTest->InternalDTCByte30_Bit0
#define feu1TCEcebyte30_bit1_err        FsTest->InternalDTCByte30_Bit1
#define feu1TCEcebyte30_bit2_err        FsTest->InternalDTCByte30_Bit2
#define feu1TCEcebyte30_bit3_err        FsTest->InternalDTCByte30_Bit3
#define feu1TCEcebyte30_bit4_err        FsTest->InternalDTCByte30_Bit4
#define feu1TCEcebyte30_bit5_err        FsTest->InternalDTCByte30_Bit5
#define feu1TCEcebyte30_bit6_err        FsTest->InternalDTCByte30_Bit6
#define feu1TCEcebyte30_bit7_err        FsTest->InternalDTCByte30_Bit7

/* FsTest Byte 31 */
#define feu1TcTempTestFlg               FsTest->InternalDTCByte31_Bit0
#define feu1Bat1FuseTestFlg             FsTest->InternalDTCByte31_Bit1
#define feu1WrongEbdTestFlg             FsTest->InternalDTCByte31_Bit2
#define feu1LowBrkFluidTestFlg          FsTest->InternalDTCByte31_Bit3
#define feu1WrongImmoIdTestFlg          FsTest->InternalDTCByte31_Bit4
#define feu1EnvIdNotProgTestFlg         FsTest->InternalDTCByte31_Bit5
#define feu1VinNotProgTestFlg           FsTest->InternalDTCByte31_Bit6
#define feu1HsSubConfigTestFlg          FsTest->InternalDTCByte31_Bit7

/* FsTest Byte 32 */
#define feu1VacuumOpenTestFlg             FsTest->InternalDTCByte32_Bit0
#define feu1VacuumShortTestFlg            FsTest->InternalDTCByte32_Bit1
#define feu1VacuumStickTestFlg            FsTest->InternalDTCByte32_Bit2
#define feu1VacuumLeakTestFlg             FsTest->InternalDTCByte32_Bit3
#define feu1VacuumNoiseTestFlg            FsTest->InternalDTCByte32_Bit4
#define feu1VacuumPumpRelayShortTestFlg   FsTest->InternalDTCByte32_Bit5
#define feu1VacuumPumpRelayOpenTestFlg    FsTest->InternalDTCByte32_Bit6
#define feu1VacuumPumpSystemTestFlg       FsTest->InternalDTCByte32_Bit7

/* FsTest Byte 33 */
#define feu1FliPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit0
#define feu1FloPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit1
#define feu1FriPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit2
#define feu1FroPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit3
#define feu1RliPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit4
#define feu1RloPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit5
#define feu1RriPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit6
#define feu1RroPsvOpenTestFlg           FsTest->InternalDTCByte33_Bit7

/* FsTest Byte 34 */
#define feu1TCbyte34_bit0_err           FsTest->InternalDTCByte34_Bit0
#define feu1TCbyte34_bit1_err           FsTest->InternalDTCByte34_Bit1
#define feu1TCbyte34_bit2_err           FsTest->InternalDTCByte34_Bit2
#define feu1TCbyte34_bit3_err           FsTest->InternalDTCByte34_Bit3
#define feu1TCbyte34_bit4_err           FsTest->InternalDTCByte34_Bit4
#define feu1TCbyte34_bit5_err           FsTest->InternalDTCByte34_Bit5
#define feu1TCbyte34_bit6_err           FsTest->InternalDTCByte34_Bit6
#define feu1TCbyte34_bit7_err           FsTest->InternalDTCByte34_Bit7

/* FsTest Byte 35 */
#define feu1SecureCodeProgTestFlg       FsTest->InternalDTCByte35_Bit0
#define feu1WrongCalTestFlg             FsTest->InternalDTCByte35_Bit1
#define feu1TCbyte35_bit2_err           FsTest->InternalDTCByte35_Bit2
#define feu1TCbyte35_bit3_err           FsTest->InternalDTCByte35_Bit3
#define feu1TCbyte35_bit4_err           FsTest->InternalDTCByte35_Bit4
#define feu1TCbyte35_bit5_err           FsTest->InternalDTCByte35_Bit5
#define feu1TCbyte35_bit6_err           FsTest->InternalDTCByte35_Bit6
#define feu1TCbyte35_bit7_err           FsTest->InternalDTCByte35_Bit7

#if __ECU==ESP_ECU_1

    #if __STEER_SENSOR_TYPE==CAN_TYPE
        /* FsTest Byte 36 */
        #if __CUSTOMER_DTC==HMC_DTC
            #define MASK_STEER_ERROR_BYTE_0  0xA3
        #elif __CUSTOMER_DTC==GM_DTC
            #define MASK_STEER_ERROR_BYTE_0  0x83
        #endif

        #define feu1StgTimeoutTestFlg      FsTest->InternalDTCByte36_Bit0
        #define feu1StgCalTestFlg          FsTest->InternalDTCByte36_Bit1
        #define feu1StgChecksumTestFlg     FsTest->InternalDTCByte36_Bit2
        #define feu1StgInternalErrTestFlg  FsTest->InternalDTCByte36_Bit3
        #define feu1StgRollingTestFlg      FsTest->InternalDTCByte36_Bit4
        #define feu1StgProtectionTestFlg   FsTest->InternalDTCByte36_Bit5
        #define feu1StgRangeTestFlg        FsTest->InternalDTCByte36_Bit6
        #define feu1TCbyte36_bit7_err      FsTest->InternalDTCByte36_Bit7

        /* FsTest Byte 37 */
        #define MASK_STEER_ERROR_BYTE_1    0x80
        #define feu1StgShortOffsetTestFlg  FsTest->InternalDTCByte37_Bit0
        #define feu1StgLongOffsetTestFlg   FsTest->InternalDTCByte37_Bit1/* Not Used */
        #define feu1StgModelMayTestFlg     FsTest->InternalDTCByte37_Bit2
        #define feu1StgShockTestFlg        FsTest->InternalDTCByte37_Bit3/* Not Used */
        #define feu1StgStickTestFlg        FsTest->InternalDTCByte37_Bit4/* Not Used */
        #define feu1StgReverseTestFlg      FsTest->InternalDTCByte37_Bit5/* Not Used */
        #define feu1StgRateTestFlg         FsTest->InternalDTCByte37_Bit6/* Not Used */
        #define feu1TCbyte37_bit7_err      FsTest->InternalDTCByte37_Bit7
    #else /* __STEER_SENSOR_TYPE==CAN_TYPE */

        /* FsTest Byte 36 */
        #define feu1StrNlowTestFlg         FsTest->InternalDTCByte36_Bit0
        #define feu1StrNhighTestFlg        FsTest->InternalDTCByte36_Bit1
        #define feu1StrConstTestFlg        FsTest->InternalDTCByte36_Bit2
        #define feu1StrOffsetTestFlg       FsTest->InternalDTCByte36_Bit3
        #define feu1StrNhighPhbTestFlg     FsTest->InternalDTCByte36_Bit4
        #define feu1StrGradientTestFlg     FsTest->InternalDTCByte36_Bit5
        #define feu1StrRangeTestFlg        FsTest->InternalDTCByte36_Bit6
        #define feu1StrNoPhaTestFlg        FsTest->InternalDTCByte36_Bit7

        /* FsTest Byte 37 */
        #define feu1StrConstPhbTestFlg     FsTest->InternalDTCByte37_Bit0
        #define feu1StrNoPhbTestFlg        FsTest->InternalDTCByte37_Bit1
        #define feu1StrCrossTestFlg        FsTest->InternalDTCByte37_Bit2
        #define feu1StrNhighPhaTestFlg     FsTest->InternalDTCByte37_Bit3
        #define feu1StrConstPhaTestFlg     FsTest->InternalDTCByte37_Bit4
        #define feu1TCbyte37_bit5_err      FsTest->InternalDTCByte37_Bit5
        #define feu1SasHwLineTestFlg       FsTest->InternalDTCByte37_Bit6
        #define feu1TCbyte37_bit7_err      FsTest->InternalDTCByte37_Bit7

    #endif

    /* FsTest Byte 38 */
    #if __CUSTOMER_DTC==HMC_DTC
        #if (YAW_SNSR_ACU_TYPE==ENABLE)
            #define MASK_IMU_ERROR_BYTE  0xBF
        #else
        #define MASK_IMU_ERROR_BYTE  0x83
        #endif
    #elif __CUSTOMER_DTC==GM_DTC
        #define MASK_IMU_ERROR_BYTE  0x81
    #endif

    #define feu1ImuHwLineTestFlg        FsTest->InternalDTCByte38_Bit0
    #define feu1ImuCheckSumTestFlg      FsTest->InternalDTCByte38_Bit1
    #define feu1YawInternalErrTestFlg   FsTest->InternalDTCByte38_Bit2
    #define feu1ImuRollingTestFlg       FsTest->InternalDTCByte38_Bit3
    #define feu1ImuCANFuncTestFlg       FsTest->InternalDTCByte38_Bit4
    #define feu1ImuInvalidMsgTestFlg    FsTest->InternalDTCByte38_Bit5
    #define feu1ImuRasterTestFlg        FsTest->InternalDTCByte38_Bit6
    #define feu1AyInternalErrTestFlg    FsTest->InternalDTCByte38_Bit7

    /* FsTest Byte 39 */
    #define MASK_YAW_ERROR_BYTE_0            0x80
    #define feu1YawShortDrvOffsetTestFlg     FsTest->InternalDTCByte39_Bit0/* Not Used */
    #define feu1YawLongDrvOffsetTestFlg      FsTest->InternalDTCByte39_Bit1/* Not Used */
    #define feu1YawModelMayTestFlg           FsTest->InternalDTCByte39_Bit2/* Not Used */
    #define feu1YawModelMustTestFlg          FsTest->InternalDTCByte39_Bit3
    #define feu1YawShockTestFlg              FsTest->InternalDTCByte39_Bit4
    #define feu1YawStickTestFlg              FsTest->InternalDTCByte39_Bit5/* Not Used */
    #define feu1YawReverseTestFlg            FsTest->InternalDTCByte39_Bit6/* Not Used */
    #define feu1TCbyte39_bit7_err            FsTest->InternalDTCByte39_Bit7

    /* FsTest Byte 40 */
    #if __CUSTOMER_DTC==HMC_DTC
        #define MASK_YAW_ERROR_BYTE_1  0xF0
    #elif __CUSTOMER_DTC==GM_DTC
        #define MASK_YAW_ERROR_BYTE_1  0xF8
    #endif

    #define feu1YawStandOffsetTestFlg        FsTest->InternalDTCByte40_Bit0
    #define feu1YawRangeTestFlg              FsTest->InternalDTCByte40_Bit1
    #define feu1YawNoiseTestFlg              FsTest->InternalDTCByte40_Bit2
    #define feu1YawCbitTestFlg               FsTest->InternalDTCByte40_Bit3
    #define feu1SENVoltTestFlg               FsTest->InternalDTCByte40_Bit4
    #define feu1YawInitRunTestFlg            FsTest->InternalDTCByte40_Bit5
    #define feu1SenMCUVoltTestFlg            FsTest->InternalDTCByte40_Bit6
    #define feu1SenMCUSenTestFlg             FsTest->InternalDTCByte40_Bit7

    /* FsTest Byte 41 */
    #if __CUSTOMER_DTC==HMC_DTC
        #define MASK_AY_ERROR_BYTE_0  0xF0
    #elif __CUSTOMER_DTC==GM_DTC
        #define MASK_AY_ERROR_BYTE_0  0xF8
    #endif

    #define feu1AyStandOffsetTestFlg         FsTest->InternalDTCByte41_Bit0
    #define feu1AyRangeTestFlg               FsTest->InternalDTCByte41_Bit1
    #define feu1AyNoiseTestFlg               FsTest->InternalDTCByte41_Bit2
    #define feu1AyCbitTestFlg                FsTest->InternalDTCByte41_Bit3
    #define feu1AyWrongInstalledTestFlg      FsTest->InternalDTCByte41_Bit4
    #define feu1AYInitRunTestFlg             FsTest->InternalDTCByte41_Bit5
    #define feu1TCbyte41_bit6_err            FsTest->InternalDTCByte41_Bit6
    #define feu1TCbyte41_bit7_err            FsTest->InternalDTCByte41_Bit7

    /* FsTest Byte 42 */
    #define MASK_AY_ERROR_BYTE_1             0x80
    #define feu1AyShortDrvOffsetTestFlg      FsTest->InternalDTCByte42_Bit0
    #define feu1AyLongDrvOffsetTestFlg       FsTest->InternalDTCByte42_Bit1
    #define feu1AyModelMayTestFlg            FsTest->InternalDTCByte42_Bit2
    #define feu1AyModelMustTestFlg           FsTest->InternalDTCByte42_Bit3
    #define feu1AyShockTestFlg               FsTest->InternalDTCByte42_Bit4
    #define feu1AyStickTestFlg               FsTest->InternalDTCByte42_Bit5
    #define feu1AyReverseTestFlg             FsTest->InternalDTCByte42_Bit6
    #define feu1TCbyte42_bit7_err            FsTest->InternalDTCByte42_Bit7

    /* FsTest Byte 43 */
    #define feu1MpAverageTestFlg             FsTest->InternalDTCByte43_Bit0
    #define feu1MpVTestFlg                   FsTest->InternalDTCByte43_Bit1
    #define feu1MpOffsetTestFlg              FsTest->InternalDTCByte43_Bit2
    #define feu1MpNoiseTestFlg               FsTest->InternalDTCByte43_Bit3
    #define feu1MpVrefTestFlg                FsTest->InternalDTCByte43_Bit4
    #define feu1MpOffset10BarTestFlg         FsTest->InternalDTCByte43_Bit5
    #define feu1MpStateTestFlg               FsTest->InternalDTCByte43_Bit6
    #define feu1TCbyte43_bit7_err            FsTest->InternalDTCByte43_Bit7

    /* FsTest Byte 44 */
    #define feu1FlpSignalTestFlg             FsTest->InternalDTCByte44_Bit0
    #define feu1FrpSignalTestFlg             FsTest->InternalDTCByte44_Bit1
    #define feu1RlpSignalTestFlg             FsTest->InternalDTCByte44_Bit2
    #define feu1RrpSignalTestFlg             FsTest->InternalDTCByte44_Bit3
    #define feu1FlpVTestFlg                  FsTest->InternalDTCByte44_Bit4
    #define feu1FrpVTestFlg                  FsTest->InternalDTCByte44_Bit5
    #define feu1RlpVTestFlg                  FsTest->InternalDTCByte44_Bit6
    #define feu1RrpVTestFlg                  FsTest->InternalDTCByte44_Bit7

    /* FsTest Byte 45 */
    #define feu1YawBusOffTestFlg             FsTest->InternalDTCByte45_Bit0
    #define feu1YawOverrunTestFlg            FsTest->InternalDTCByte45_Bit1
    #define feu1VSM2TimeoutTestFlg           FsTest->InternalDTCByte45_Bit2
    #define feu1VSM2ChecksumTestFlg          FsTest->InternalDTCByte45_Bit3
    #define feu1VSM2InternalTestFlg          FsTest->InternalDTCByte45_Bit4
    #define feu1VSM2RollingTestFlg           FsTest->InternalDTCByte45_Bit5
    #define feu1Clu2TimeoutTestFlg           FsTest->InternalDTCByte45_Bit6
    #define feu1CLU2SignalTestFlg            FsTest->InternalDTCByte45_Bit7

    /* FsTest Byte 46 */
    #define feu1CUT1OpenTestFlg              FsTest->InternalDTCByte46_Bit0
    #define feu1CUT1ShortTestFlg             FsTest->InternalDTCByte46_Bit1
    #define feu1CUT2OpenTestFlg              FsTest->InternalDTCByte46_Bit2
    #define feu1CUT2ShortTestFlg             FsTest->InternalDTCByte46_Bit3
    #define feu1SIMOpenTestFlg               FsTest->InternalDTCByte46_Bit4
    #define feu1SIMShortTestFlg              FsTest->InternalDTCByte46_Bit5
    #define feu1CUT1LeakTestFlg            FsTest->InternalDTCByte46_Bit6
    #define feu1TCbyte46_bit7_err            FsTest->InternalDTCByte46_Bit7

    /* FsTest Byte 47 */
    #define feu1TCbyte47_bit0_err            FsTest->InternalDTCByte47_Bit0
    #define feu1TCbyte47_bit1_err            FsTest->InternalDTCByte47_Bit1
    #define feu1TCbyte47_bit2_err            FsTest->InternalDTCByte47_Bit2
    #define feu1TCbyte47_bit3_err            FsTest->InternalDTCByte47_Bit3
    #define feu1TCbyte47_bit4_err            FsTest->InternalDTCByte47_Bit4
    #define feu1TCbyte47_bit5_err            FsTest->InternalDTCByte47_Bit5
    #define feu1simVstuckTestFlg             FsTest->InternalDTCByte47_Bit6
    #define feu1TCbyte47_bit7_err            FsTest->InternalDTCByte47_Bit7

    /* FsTest Byte 48 */
    #define feu1FlpAverageTestFlg            FsTest->InternalDTCByte48_Bit0
    #define feu1FrpAverageTestFlg            FsTest->InternalDTCByte48_Bit1
    #define feu1RlpAverageTestFlg            FsTest->InternalDTCByte48_Bit2
    #define feu1RrpAverageTestFlg            FsTest->InternalDTCByte48_Bit3
    #define feu1FlpStateTestFlg              FsTest->InternalDTCByte48_Bit4
    #define feu1FrpStateTestFlg              FsTest->InternalDTCByte48_Bit5
    #define feu1RlpStateTestFlg              FsTest->InternalDTCByte48_Bit6
    #define feu1RrpStateTestFlg              FsTest->InternalDTCByte48_Bit7

    /* FsTest Byte 49 */
    #define feu1FlpNoiseTestFlg              FsTest->InternalDTCByte49_Bit0
    #define feu1FrpNoiseTestFlg              FsTest->InternalDTCByte49_Bit1
    #define feu1RlpNoiseTestFlg              FsTest->InternalDTCByte49_Bit2
    #define feu1RrpNoiseTestFlg              FsTest->InternalDTCByte49_Bit3
    #define feu1BlrFetOpenTest               FsTest->InternalDTCByte49_Bit4
    #define feu1BlrContactOpenTest           FsTest->InternalDTCByte49_Bit5
    #define feu1BlrCoilOpenTest              FsTest->InternalDTCByte49_Bit6
    #define pedal_signal_test_flg            FsTest->InternalDTCByte49_Bit7

    /* FsTest Byte 50 */
    #define feu1WrongEscTestFlg              FsTest->InternalDTCByte50_Bit0
    #define feu1CeSubConfigTestFlg           FsTest->InternalDTCByte50_Bit1
    #define feu1HdcTemperatureTestFlg        FsTest->InternalDTCByte50_Bit2
    #define pedal_pdt_pot1_open_test_flg     FsTest->InternalDTCByte50_Bit3
    #define pedal_pdf_pot2_open_test_flg     FsTest->InternalDTCByte50_Bit4
    #define pedal_pdt_pot1_short_test_flg    FsTest->InternalDTCByte50_Bit5
    #define pedal_pdf_pot2_short_test_flg    FsTest->InternalDTCByte50_Bit6
    #define pedal_mismatch_test_flg          FsTest->InternalDTCByte50_Bit7

    /* FsTest Byte 51 */
    #define feu1GearRSwitchShortTest         FsTest->InternalDTCByte51_Bit0
    #define feu1GearRSwitchOpenTest          FsTest->InternalDTCByte51_Bit1
    #define feu1GearRSwitchNoiseTest         FsTest->InternalDTCByte51_Bit2
    #define feu1ClutchSwitchOpenShortTest    FsTest->InternalDTCByte51_Bit3
    #define feu1ClutchSwitchNoiseTest        FsTest->InternalDTCByte51_Bit4
    #define feu1AvhSwTestFlg           		 FsTest->InternalDTCByte51_Bit5
	#define feu1EPB1SignalTestFlg            FsTest->InternalDTCByte51_Bit6
    #define feu1Epb1TimeoutTestFlg           FsTest->InternalDTCByte51_Bit7

    /* FsTest Byte 52 */
    #define feu1SCCTIMEOUT_Test_flg          FsTest->InternalDTCByte52_Bit0
    #define feu1SCCMSGINVALID_Test_flg       FsTest->InternalDTCByte52_Bit1
    #define feu1SCC_Accel_Invalid_test_flg   FsTest->InternalDTCByte52_Bit2
    #define feu1EMS1MSGINVALID_Test_flg      FsTest->InternalDTCByte52_Bit3
    #define feu1EMS5MSGINVALID_Test_flg      FsTest->InternalDTCByte52_Bit4
    #define feu1TCU1MSGINVALID_Test_flg      FsTest->InternalDTCByte52_Bit5
    #define feu1EMS5TIMEOUT_Test_flg         FsTest->InternalDTCByte52_Bit6
    #define feu1AvsmSwTestFlg                FsTest->InternalDTCByte52_Bit7

    /* FsTest Byte 53 */
    #define feu1TCbyte53_bit0_err            FsTest->InternalDTCByte53_Bit0
    #define feu1TCbyte53_bit1_err            FsTest->InternalDTCByte53_Bit1
    #define feu1TCbyte53_bit2_err            FsTest->InternalDTCByte53_Bit2
    #define feu1TCbyte53_bit3_err            FsTest->InternalDTCByte53_Bit3
    #define feu1TCbyte53_bit4_err            FsTest->InternalDTCByte53_Bit4
    #define feu1TCbyte53_bit5_err            FsTest->InternalDTCByte53_Bit5
    #define feu1TCbyte53_bit6_err            FsTest->InternalDTCByte53_Bit6
    #define feu1TCbyte53_bit7_err            FsTest->InternalDTCByte53_Bit7

    /* FsTest Byte 54 */
    #define feu1PdtSigOffsetTestFlg          FsTest->InternalDTCByte54_Bit0
    #define feu1PdfSigOffsetTestFlg          FsTest->InternalDTCByte54_Bit1
    #define feu1PdtSigStickTestFlg           FsTest->InternalDTCByte54_Bit2
    #define feu1PdfSigStickTestFlg           FsTest->InternalDTCByte54_Bit3
    #define feu1PdtSigNoiseTestFlg           FsTest->InternalDTCByte54_Bit4
    #define feu1PdfSigNoiseTestFlg           FsTest->InternalDTCByte54_Bit5
    #define feu1PdtSigNoChangeTestFlg        FsTest->InternalDTCByte54_Bit6
    #define feu1PedalCalibrationTestFlg      FsTest->InternalDTCByte54_Bit7

    /* FsTest Byte 55 */
    #define MASK_VALVERELAY_ERROR_BYTE_1     0xE0
    #define feu1AhbVrBypassTestFlg           FsTest->InternalDTCByte55_Bit0
    #define feu1AhbVrOpenTestFlg             FsTest->InternalDTCByte55_Bit1
    #define feu1AhbVrShortGndTestFlg         FsTest->InternalDTCByte55_Bit2
    #define feu1AhbVrLeakageTestFlg          FsTest->InternalDTCByte55_Bit3
    #define feu1AhbVrShutdownTestFlg         FsTest->InternalDTCByte55_Bit4
    #define feu1TCbyte55_bit5_err            FsTest->InternalDTCByte55_Bit5
    #define feu1TCbyte55_bit6_err            FsTest->InternalDTCByte55_Bit6
    #define feu1TCbyte55_bit7_err            FsTest->InternalDTCByte55_Bit7

    #if  __AHB_GEN3_SYSTEM == ENABLE

    /* FsTest Byte 70 */
    #define feu1Hpa_V_TestFlg                FsTest->InternalDTCByte70_Bit0
    #define feu1HpaAvrTestFlg                FsTest->InternalDTCByte70_Bit1
    #define feu1BcpPri_V_TestFlg             FsTest->InternalDTCByte70_Bit2
    #define feu1BcpPriAvrTestFlg             FsTest->InternalDTCByte70_Bit3
    #define feu1BcpSec_V_TestFlg             FsTest->InternalDTCByte70_Bit4
    #define feu1BcpSecAvrTestFlg             FsTest->InternalDTCByte70_Bit5
    #define feu1Psp_V_TestFlg                FsTest->InternalDTCByte70_Bit6
    #define feu1PspAvrTestFlg                FsTest->InternalDTCByte70_Bit7

    /* FsTest Byte 71 */
    #define MASK_HPA_ERROR_BYTE              0xC0
    #define feu1HpaLowStickTestFlg           FsTest->InternalDTCByte71_Bit0
    #define feu1HpaLeakTestFlg               FsTest->InternalDTCByte71_Bit1
    #define feu1HpaLowPreTestFlg             FsTest->InternalDTCByte71_Bit2
    #define feu1HpaHighStickTestFlg          FsTest->InternalDTCByte71_Bit3
    #define feu1HpaOffsetTestFlg             FsTest->InternalDTCByte71_Bit4
    #define feu1HpaNoiseTestFlg              FsTest->InternalDTCByte71_Bit5
    #define feu1TCbyte71_bit6_err            FsTest->InternalDTCByte71_Bit6
    #define feu1TCbyte71_bit7_err            FsTest->InternalDTCByte71_Bit7

    /* FsTest Byte 72*/
    #define MASK_BCP_ERROR_BYTE              0xC0
    #define feu1BcpPriLowStickTestFlg        FsTest->InternalDTCByte72_Bit0
    #define feu1BcpPriOffsetTestFlg          FsTest->InternalDTCByte72_Bit1
    #define feu1BcpPriNoiseTestFlg           FsTest->InternalDTCByte72_Bit2
    #define feu1BcpSecLowStickTestFlg        FsTest->InternalDTCByte72_Bit3
    #define feu1BcpSecOffsetTestFlg          FsTest->InternalDTCByte72_Bit4
    #define feu1BcpSecNoiseTestFlg           FsTest->InternalDTCByte72_Bit5
    #define feu1TCbyte72_bit6_err            FsTest->InternalDTCByte72_Bit6
    #define feu1TCbyte72_bit7_err            FsTest->InternalDTCByte72_Bit7

    /* FsTest Byte 73*/
    #define MASK_PSP_ERROR_BYTE              0xE0
    #define feu1PspStickTestFlg              FsTest->InternalDTCByte73_Bit0
    #define feu1PspOffsetTestFlg             FsTest->InternalDTCByte73_Bit1
    #define feu1PspNoiseTestFlg              FsTest->InternalDTCByte73_Bit2
    #define feu1PressureCalibrationTestFlg   FsTest->InternalDTCByte73_Bit3
    #define feu1BcPriLeakTestFlg             FsTest->InternalDTCByte73_Bit4
    #define feu1BcSecLeakTestFlg             FsTest->InternalDTCByte73_Bit5
    #define feu1TCbyte73_bit6_err            FsTest->InternalDTCByte73_Bit6
    #define feu1TCbyte73_bit7_err            FsTest->InternalDTCByte73_Bit7

    /* FsTest Byte 74*/
    #define feu1EMSSIGINVALID_Test_flg            FsTest->InternalDTCByte74_Bit0
    #define feu1TCUGearInvalidTest_flg            FsTest->InternalDTCByte74_Bit1
    #define feu1TCbyte74_bit2_err            FsTest->InternalDTCByte74_Bit2
    #define feu1TCbyte74_bit3_err            FsTest->InternalDTCByte74_Bit3
    #define feu1TCbyte74_bit4_err            FsTest->InternalDTCByte74_Bit4
    #define feu1TCbyte74_bit5_err            FsTest->InternalDTCByte74_Bit5
    #define feu1TCbyte74_bit6_err            FsTest->InternalDTCByte74_Bit6
    #define feu1TCbyte74_bit7_err            FsTest->InternalDTCByte74_Bit7

    #endif

#endif /* __ECU==ESP_ECU_1 */

extern struct ERROR_FLAG *FsTest;


#endif /* ERP_FSTESTCOMPLETE_H_ */
