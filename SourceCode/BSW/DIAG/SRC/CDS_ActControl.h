#ifndef CDS_ACTCONTROL_H
#define CDS_ACTCONTROL_H

typedef struct
{
    uBitType u1cNoFL :1;
    uBitType u1cNoFR :1;
    uBitType u1cNoRL :1;
    uBitType u1cNoRR :1;
    uBitType u1cNcFL :1;
    uBitType u1cNcFR :1;
    uBitType u1cNcRL :1;
    uBitType u1cNcRR :1;
}CDS_ABSSOL_t;

extern CDS_ABSSOL_t CDABSFIRST, CDABSNEXT;

#define CDSACTFLAG0  *(uint8_t*)(void *)&CDABSFIRST
#define cdu1FLNOFirstDrv      CDABSFIRST.u1cNoFL
#define cdu1FRNOFirstDrv      CDABSFIRST.u1cNoFR
#define cdu1RLNOFirstDrv      CDABSFIRST.u1cNoRL
#define cdu1RRNOFirstDrv      CDABSFIRST.u1cNoRR
#define cdu1FLNCFirstDrv      CDABSFIRST.u1cNcFL
#define cdu1FRNCFirstDrv      CDABSFIRST.u1cNcFR
#define cdu1RLNCFirstDrv      CDABSFIRST.u1cNcRL
#define cdu1RRNCFirstDrv      CDABSFIRST.u1cNcRR

#define cdu1FLNONextDrv       CDABSNEXT.u1cNoFL 
#define cdu1FRNONextDrv       CDABSNEXT.u1cNoFR 
#define cdu1RLNONextDrv       CDABSNEXT.u1cNoRL 
#define cdu1RRNONextDrv       CDABSNEXT.u1cNoRR 
#define cdu1FLNCNextDrv       CDABSNEXT.u1cNcFL 
#define cdu1FRNCNextDrv       CDABSNEXT.u1cNcFR 
#define cdu1RLNCNextDrv       CDABSNEXT.u1cNcRL 
#define cdu1RRNCNextDrv       CDABSNEXT.u1cNcRR 

#if __ECU==ESP_ECU_1
typedef struct
{
  uBitType   u1cPTCNoOn           :1;                          
  uBitType   u1cSTCNoOn           :1;                          
  uBitType   u1cPSNcOn            :1;                          
  uBitType   u1cSSNcOn            :1;
  uBitType   u1cPRSVOn            :1;                          
  uBitType   u1cSRSVOn            :1;                          
}CDS_ESPSOL_t;

extern CDS_ESPSOL_t CDESPFIRST, CDESPNEXT;

#define cdu1PTCFirstDrv         CDESPFIRST.u1cPTCNoOn           
#define cdu1STCFirstDrv         CDESPFIRST.u1cSTCNoOn           
#define cdu1ESVPFirstDrv        CDESPFIRST.u1cPSNcOn            
#define cdu1ESVSFirstDrv        CDESPFIRST.u1cSSNcOn 
#define cdu1PRSVFirstDrv        CDESPFIRST.u1cPRSVOn
#define cdu1SRSVFirstDrv        CDESPFIRST.u1cSRSVOn

#define cdu1PTCNextDrv          CDESPNEXT.u1cPTCNoOn           
#define cdu1STCNextDrv          CDESPNEXT.u1cSTCNoOn           
#define cdu1ESVPNextDrv         CDESPNEXT.u1cPSNcOn            
#define cdu1ESVSNextDrv         CDESPNEXT.u1cSSNcOn 
#define cdu1PRSVNextDrv         CDESPNEXT.u1cPRSVOn
#define cdu1SRSVNextDrv         CDESPNEXT.u1cSRSVOn 
#endif

typedef struct
{
  uBitType   u1cAPV01NoOn        :1;                          
  uBitType   u1cAPV02NoOn        :1;                          
  uBitType   u1cRLV01cOn         :1;                          
  uBitType   u1cRLV02cOn         :1;
  uBitType   u1cPCUT1VOn         :1;                          
  uBitType   u1cCUT2VOn          :1;  
  uBitType   u1cSIMVOn           :1;                        
}CDS_AHBSOL_t;

extern CDS_AHBSOL_t CDAHBFIRST, CDAHBNEXT;

#define cdu1APV01FirstDrv     CDAHBFIRST.u1cAPV01NoOn           
#define cdu1APV02FirstDrv     CDAHBFIRST.u1cAPV02NoOn           
#define cdu1RLV01FirstDrv     CDAHBFIRST.u1cRLV01cOn            
#define cdu1RLV02FirstDrv     CDAHBFIRST.u1cRLV02cOn 
#define cdu1CUT1VFirstDrv     CDAHBFIRST.u1cPCUT1VOn
#define cdu1CUT2VFirstDrv     CDAHBFIRST.u1cCUT2VOn
#define cdu1SIMVFirstDrv      CDAHBFIRST.u1cSIMVOn

#define cdu1APV01NextDrv      CDAHBNEXT.u1cAPV01NoOn           
#define cdu1APV02NextDrv      CDAHBNEXT.u1cAPV02NoOn           
#define cdu1RLV01PNextDrv     CDAHBNEXT.u1cRLV01cOn            
#define cdu1RLV02SNextDrv     CDAHBNEXT.u1cRLV02cOn 
#define cdu1CUT1VNextDrv      CDAHBNEXT.u1cCUT1VOn
#define cdu1SCUT1VNextDrv     CDAHBNEXT.u1cCUT2VOn 
#define cdu1SIMVNextDrv       CDAHBNEXT.u1cSIMVOn 

typedef struct
{
    uBitType   u1cHdcRelay          :1;                          
    uBitType   u1cEssRelay          :1;                          
}CDS_LAMPRELAY_t;

extern CDS_LAMPRELAY_t CDLPRELAY;

#define cdu1HdcRelayDrv         CDLPRELAY.u1cHdcRelay           
#define cdu1EssRelayDrv         CDLPRELAY.u1cEssRelay

typedef struct
{
  uBitType   u1MultiDrvMode        :1; 
  uBitType   DiagCopReset          :1; 
  uBitType   u1EmissionTestMode    :1;
  uBitType   u1InhibitFsChk        :1; 
  uBitType   SasCalReqToCl         :1; 
  uBitType   ClearDtcSupplier      :1; 
  uBitType   ChkMec                :1;
  uBitType   ReadMec               :1; 
  uBitType   ClearDtcReq           :1;   
  uBitType   ReadIntDtcSupplier    :1;  
  uBitType   SysShutDownReady      :1;
  uBitType   SysShutDownReq        :1;
}CDS_ACTMISC_t;

extern CDS_ACTMISC_t CDMISC;

#define cdu1MultiDrvMode         CDMISC.u1MultiDrvMode 
#define cdu8DiagCopReset         CDMISC.DiagCopReset
#define cdu1EmissionTestMode     CDMISC.u1EmissionTestMode
#define cdu1InhibitFsChk         CDMISC.u1InhibitFsChk
#define cdu1DiagSasCaltoCL       CDMISC.SasCalReqToCl
#define cdu1ClearDtcSupplier     CDMISC.ClearDtcSupplier
#define cdu1ChkMecFlg            CDMISC.ChkMec
#define cdu1ReadMecFlg           CDMISC.ReadMec  
#define cdu1ClearDtcReq          CDMISC.ClearDtcReq  
#define cdu1ReadIntDtcSupplier   CDMISC.ReadIntDtcSupplier
#define cdu1SysShutDownReady     CDMISC.SysShutDownReady
#define cdu1SysShutDownReq       CDMISC.SysShutDownReq

extern uint16_t  cdu16ActDrvTimer;
extern uint16_t  cdu16ActDrvTimerAHB;
extern uint16_t  cdu16MotorDrvTimer;
extern uint16_t  cdu16DrvInterTimer;
extern uint16_t  cdu16ActDrvNextTimer; 

extern uint16_t   cdu16SolenoidDrvDuty;
extern uint16_t   cdu16SolenoidDrvDutyAHB;
extern uint16_t   cdu16MotorDrvDuty;

#if 0
extern uchar8_t  cdu8FsMode_rx_ok, cdu8FsMode_2, cdu8FsMode_3, cdu8FsMode_4, cdu8FsMode_5, cdu8FsMode_6, cdu8FsMode_7;
#endif

#endif
