#ifndef CDS_READRECORDVALUE_H
#define CDS_READRECORDVALUE_H


/*** Declare MACRO for Diag ***/
#if 1
    #define __DIAG_NEW_VERSION  1 
#else
    #define __DIAG_NEW_VERSION  0
#endif  

#define __DIAG_NO_ASIC            1 

#define __DIAG_PREMIUM_14VV_ECU   0
/*#define __PRESSURE_5EA*/

#if 0
    #define __DIAG_ESS_RELAY_SET   1
#else
    #define __DIAG_ESS_RELAY_SET   0
#endif


#if 0
    #define __DIAG_DBC_RELAY_SET   1
#else
    #define __DIAG_DBC_RELAY_SET   0
#endif


#if 1
    #if 0
        #if ((__CAR==KMC_HM) || (__CAR==KMC_XM))
            #define __DIAG_CAL_AX_ENABLE     0
            #define __DIAG_CAL_AX_SAS_ENABLE 1
        #else
            #define __DIAG_CAL_AX_ENABLE     1
            #define __DIAG_CAL_AX_SAS_ENABLE 0
        #endif
    #else 
        #define __DIAG_CAL_AX_ENABLE     0
        #define __DIAG_CAL_AX_SAS_ENABLE 0
    #endif
#else
    #define __DIAG_CAL_AX_ENABLE     0
    #define __DIAG_CAL_AX_SAS_ENABLE 0        
#endif

/* Pedal, Pressure Sensor Macro */
#define __PEDAL_TRAVLE_SENSOR_DIAG_CAL 1
#define __PRESSURE_SENSOR_DIAG_CAL     1

//////////////////////////////////////////////////////////F0119 Taeho: SWD_V0
#define __MOTOR_POSITION_SENSOR_DIAG_CAL_OFFLINE 1
/////////////////////////////////////////////////////////////////////////////


#define __DIAG_CONSECUTIVE_CAL_AX_ENABLE 1

#if 0
    #define __DIAG_VARIANR_CODE_SET_SYS  1
#else
    #define __DIAG_VARIANR_CODE_SET_SYS  0
#endif

#if 0
    #define DIAG_INLINE_CODING_ENABLE
#endif

#if 0
    #define DIAG_HSA_CODING_ENABLE
#endif

    #define DIAG_ECU_SHUTDOWN_ENABLE

#define U8_FL_WSS_RV   1
#define U8_FR_WSS_RV   2
#define U8_RL_WSS_RV   3
#define U8_RR_WSS_RV   4

#define U8_FRONT_MEAN_VREF  1
#define U8_REAR_MEAN_VREF   2
#define U8_4WHL_MEAN_VREF   3

#define U8_MOT_STATUS       1
#define U8_RELAY_STATUS     2
#define U8_INVALVE_STATUS   3
#define U8_OUTVALVE_STATUS  4
#define U8_ESPVALVE_STATUS  5
#define U8_RSVVALVE_STATUS  6
#define U8_AHBVALVE1_STATUS 7
#define U8_AHBVALVE2_STATUS 8

#define U8_BASIC_LAMP    1
#define U8_FUNC_LAMP     2
#define U8_AHB_LAMP      3   /* AHB/RBC Lamp Diag 분리용 */

#define U8_BASIC_SWT    1
#define U8_SUPP_SWT     2
#define U8_HDC_SWT      3
#define U8_HDC_STATUS   4

#define U8_YAW_RV       1
#define U8_AY_RV        2

#define U8_POS_MP_RV    1 
#define U8_NEG_MP_RV    2
#define U8_FL_POS_MP_RV 3
#define U8_FR_POS_MP_RV 4
#define U8_RL_POS_MP_RV 5
#define U8_RR_POS_MP_RV 6
#define U8_PRM_POS_MP_RV 7
#define U8_PRM_NEG_MP_RV 8
#define U8_SEC_POS_MP_RV 9
#define U8_SEC_NEG_MP_RV 10
#define U8_HPA_POS_MP_RV 11
#define U8_HPA_NEG_MP_RV 12
#define U8_SIM_POS_MP_RV 13
#define U8_SIM_NEG_MP_RV 14

#define U8_PEDAL0_PDT_RV 1
#define U8_PEDAL1_PDF_RV 2

/*SUPPORTED LID01 */ 
#define U8_DIAG_SPPRTD_LID01_RPM  			0x00000001
#define U8_DIAG_SPPRTD_LID01_VEHSPD  		0x00000002
#define U8_DIAG_SPPRTD_LID01_TPS 			0x00000004
#define U8_DIAG_SPPRTD_LID01_SHFTPOS 		0x00000008
#define U8_DIAG_SPPRTD_LID01_BATTERY 		0x00000010
#define U8_DIAG_SPPRTD_LID01_5VPWR			0x00000020
#define U8_DIAG_SPPRTD_LID01_WSPD_FL 		0x00000040
#define U8_DIAG_SPPRTD_LID01_WSPD_FR 		0x00000080
#define U8_DIAG_SPPRTD_LID01_WSPD_RL 		0x00000100
#define U8_DIAG_SPPRTD_LID01_WSPD_RR 		0x00000200
#define U8_DIAG_SPPRTD_LID01_ANLG_SAS_ST 	0x00000400
#define U8_DIAG_SPPRTD_LID01_AX				0x00000800
#define U8_DIAG_SPPRTD_LID01_ANALOGAY		0x00001000
#define U8_DIAG_SPPRTD_LID01_WLP			0x00002000
#define U8_DIAG_SPPRTD_LID01_SWITCH			0x00004000
#define U8_DIAG_SPPRTD_LID01_MOTVVLP		0x00008000
#define U8_DIAG_SPPRTD_LID01_MOTOR			0x00010000
#define U8_DIAG_SPPRTD_LID01_ABSIN			0x00020000
#define U8_DIAG_SPPRTD_LID01_ABSOUT			0x00040000
#define U8_DIAG_SPPRTD_LID01_ESCVV			0x00080000
#define U8_DIAG_SPPRTD_LID01_USVHSV			0x00100000
#define U8_DIAG_SPPRTD_LID01_SAS			0x00200000
#define U8_DIAG_SPPRTD_LID01_CANAY			0x00400000
#define U8_DIAG_SPPRTD_LID01_CANYAW			0x00800000
#define U8_DIAG_SPPRTD_LID01_MCP			0x01000000
#define U8_DIAG_SPPRTD_LID01_PDT			0x02000000
#define U8_DIAG_SPPRTD_LID01_PDF			0x04000000
#define U8_DIAG_SPPRTD_LID01_DBC			0x08000000
#define U8_DIAG_SPPRTD_LID01_ANALG_SAS		0x10000000

/*SUPPORTED LID02 */ 
#define U8_DIAG_SPPRTD_LID02_WHL_PRES  	0x00000001
#define U8_DIAG_SPPRTD_LID02_VACUUM    	0x00000002
#define U8_DIAG_SPPRTD_LID02_PRMR_PRES 	0x00000004
#define U8_DIAG_SPPRTD_LID02_SCND_PRES 	0x00000008
#define U8_DIAG_SPPRTD_LID02_HPA_PRES 	0x00000010
#define U8_DIAG_SPPRTD_LID02_SIM_PRES	0x00000020
#define U8_DIAG_SPPRTD_LID02_AHBVALVE1 	0x00000040
#define U8_DIAG_SPPRTD_LID02_AHBVALVE2 	0x00000080
#define U8_DIAG_SPPRTD_LID02_AHBWLP 	0x00000100
#define U8_DIAG_SPPRTD_LID02_5VPDT 	    0x00000200
#define U8_DIAG_SPPRTD_LID02_5VPDF 	    0x00000400

#define U8_DIAG_ACT_SUCCESS  0x01
#define U8_DIAG_ACT_FAILURE  0x00
#define U8_DIAG_IOLI      0x00
#define U8_DIAG_MOTOR     0x01
#define U8_DIAG_FLI       0x02
#define U8_DIAG_FRI       0x03
#define U8_DIAG_RLI       0x04
#define U8_DIAG_RRI       0x05
#define U8_DIAG_FLO       0x06
#define U8_DIAG_FRO       0x07
#define U8_DIAG_RLO       0x08
#define U8_DIAG_RRO       0x09

#if 0
#define U8_DIAG_TCL       0x0A
#define U8_DIAG_TCR       0x0B
#define U8_DIAG_PRSV      0x0C
#define U8_DIAG_SRSV      0x0D 
#define U8_DIAG_ESVP      0x0E
#define U8_DIAG_ESVS      0x0F
#define U8_DIAG_HDCRELAY  0x11
#define U8_DIAG_ESSRELAY  0x12
/* AHB Gen3 Bleeding 작업을 위한 Valve 순서 변경 */
#else
#define U8_DIAG_RLV02     0x0A
#define U8_DIAG_RLV01     0x0F
#define U8_DIAG_APV01     0x0E/*0x15*/ 
#define U8_DIAG_APV02     0x0B/*0x16*/ 
#define U8_DIAG_CUT1      0x10
#define U8_DIAG_CUT2      0x11
#define U8_DIAG_SIMV      0x12
#endif

#define U8_DIAG_CALAXOFS  0x20
#define U8_DIAG_MULTI     0xF0
/*SUPPORTED IO CONTROL*/ 
#define U8_DIAG_SPPRTD_PID_MOTOR     0x00000001
#define U8_DIAG_SPPRTD_PID_FLI       0x00000002
#define U8_DIAG_SPPRTD_PID_FRI       0x00000004
#define U8_DIAG_SPPRTD_PID_RLI       0x00000008
#define U8_DIAG_SPPRTD_PID_RRI       0x00000010
#define U8_DIAG_SPPRTD_PID_FLO       0x00000020
#define U8_DIAG_SPPRTD_PID_FRO       0x00000040
#define U8_DIAG_SPPRTD_PID_RLO       0x00000080
#define U8_DIAG_SPPRTD_PID_RRO       0x00000100
#define U8_DIAG_SPPRTD_PID_TCL       0x00000200
#define U8_DIAG_SPPRTD_PID_TCR       0x00000400
#define U8_DIAG_SPPRTD_PID_PRSV      0x00000800
#define U8_DIAG_SPPRTD_PID_SRSV      0x00001000 
#define U8_DIAG_SPPRTD_PID_ESVP      0x00002000
#define U8_DIAG_SPPRTD_PID_ESVS      0x00004000

#define U8_DIAG_SPPRTD_PID_HDCRELAY  0x00010000
#define U8_DIAG_SPPRTD_PID_ESSRELAY  0x00020000
/* AHB Gen3 Bleeding 작업을 위한 Valve 순서 변경 */
#define U8_DIAG_SPPRTD_PID_RLV02     0x00040000
#define U8_DIAG_SPPRTD_PID_RLV01     0x00080000
#define U8_DIAG_SPPRTD_PID_APV01     0x00100000 
#define U8_DIAG_SPPRTD_PID_APV02     0x00200000

#define U8_DIAG_SPPRTD_PID_CUT1      0x00400000
#define U8_DIAG_SPPRTD_PID_CUT2      0x00800000
#define U8_DIAG_SPPRTD_PID_SIMV      0x01000000

#define U8_DIAG_BATT   1
#define U8_DIAG_5VREF  2
#define U8_DIAG_5VPDT  3
#define U8_DIAG_5VPDF  4

#if 0
    #define WHL_RR_HW_ERROR         0x4054
    #define WHL_RL_HW_ERROR         0x4049
    #define WHL_FR_HW_ERROR         0x4044
    #define WHL_FL_HW_ERROR         0x4039

    #define WHL_RR_AIRGAP_ERROR     0x4050
    #define WHL_RL_AIRGAP_ERROR     0x4045
    #define WHL_FR_AIRGAP_ERROR     0x4040
    #define WHL_FL_AIRGAP_ERROR     0x4035

    #define WHL_RR_SPDJUMP_ERROR    0x4051
    #define WHL_RL_SPDJUMP_ERROR    0x4046
    #define WHL_FR_SPDJUMP_ERROR    0x4041
    #define WHL_FL_SPDJUMP_ERROR    0x4036
    
    #define SOLENOID_COIL_ERROR     0x4060
    #define MOTOR_ERROR             0x4110
    #define VALVE_RELAY_ERROR       0x4121
    #define ECU_HW_ERROR            0x4550

    #define OVER_VOLT_ERROR         0x4900
    #define UNDER_VOLT_ERROR        0x4899
    
    #define G_SEN_HW_ERROR          0x5274
    #define G_SEN_SIGNAL_ERROR      0x5275
    #define SEN_PWR_ERROR           0x5112
#else
    #define WHL_RR_HW_ERROR         0x5209
    #define WHL_RL_HW_ERROR         0x5206
    #define WHL_FR_HW_ERROR         0x5203
    #define WHL_FL_HW_ERROR         0x5200

    #define WHL_RR_AIRGAP_ERROR     0x5211
    #define WHL_RL_AIRGAP_ERROR     0x5208
    #define WHL_FR_AIRGAP_ERROR     0x5205
    #define WHL_FL_AIRGAP_ERROR     0x5202

    #define WHL_RR_SPDJUMP_ERROR    0x5210
    #define WHL_RL_SPDJUMP_ERROR    0x5207
    #define WHL_FR_SPDJUMP_ERROR    0x5204
    #define WHL_FL_SPDJUMP_ERROR    0x5201

    #define SOLENOID_COIL_ERROR     0x6380
    #define MOTOR_ERROR             0x6402
    #define VALVE_RELAY_ERROR       0x6112
    #define ECU_HW_ERROR            0x5604

    #define OVER_VOLT_ERROR         0x5101
    #define UNDER_VOLT_ERROR        0x5102
    
    #define G_SEN_HW_ERROR          0x5274
    #define G_SEN_SIGNAL_ERROR      0x5275
    #define SEN_PWR_ERROR           0x5112
#endif

    #define CAN_HW_ERROR            0x5605
    #if 0
        #define CAN_BUS_OFF_ERROR       0xC073
        #define EMS_TIME_OUT_ERROR      0xC100
        #define TCU_TIME_OUT_ERROR      0xC101
    #else
        #define CAN_BUS_OFF_ERROR       0x5616
        #define EMS_TIME_OUT_ERROR      0x5611
        #define TCU_TIME_OUT_ERROR      0x5612
    #endif
    #define CAN_DATA_UNMATCH_ERROR  0x5613
	#define EMS_SIGNAL_ERROR        0x6229 /*Actuator Failure - EMS*/

    #define G_SEN_HW_ERROR          0x5274
    #define G_SEN_SIGNAL_ERROR      0x5275
    #define SEN12V_PWR_ERROR       0x5114
    #define SEN5V_PWR_ERROR        0x5113
    
    #define G_SEN_CAL_ERROR         0x5285

    #define HDC_SWITCH_ERROR        0x5526
    #define VDC_SWITCH_ERROR        0x5503
    #define TEMPERATURE_ERROR       0x6227

    #if (0)
    	#define MCP_HW_ERROR            0x5235
        #define MCP_SIGNAL_ERROR        0x5237 	
    #else           
        #define HPA_SHORTTERM_CHARGE_ERROR	0x5233
        #define HPA_LONGTERM_CHARGE_ERROR	0x5234
        #define HPA_HW_ERROR      		0x5235           
        #define HPA_SIGNAL_ERROR                0x5237  
        
        #define HPA_LOW_PRESSURE_ERROR          0x5236
        #define BCP_SEN_HW_ERROR                0x5353
        #define BCP_ABNORMAL_PRESSURE_ERROR     0x5354
        #define PSP_SEN_HW_ERROR                0x5355
        #define PSP_ABNORMAL_PRESSURE_ERROR     0x5356
        #define BOOSTER_CIRCUIT_LEAK_ERROR      0x5268
    #endif
    
    #define BLS_BS_ERROR            0x5513 
           
    #define YAW_LG_OVERRUN_ERROR    0x5647
    #if 0
        #define YAW_LG_HW_ERROR         0x5643      /*IMU SPEC Only*/
    #else
        #define YAW_LG_HW_ERROR         0x5282      /*Internal SPEC Only*/
    #endif
    
    #define YAW_LG_BUS_OFF_ERROR    0x5807      /*T.B.D*/
    
    #define YAW_LG_SIGNAL_ERROR     0x5283
    #define YAW_LG_ACU_SIGNAL_ERROR     0x5286 /*ACU와 YAW&G센서 구분 위함(HMC요청)*/
            
    #if 0 
        #define STR_HW_ERROR            0x5259
    #elif 1
        #if 1
            #define STR_HW_ERROR            0x5623
        #else
            #define STR_HW_ERROR            0x5259
        #endif
    #else
    	#define STR_HW_ERROR            0x5259
	#endif
     
    #define STR_CAL_ERROR           0x5261
    #define STR_SIGNAL_ERROR        0x5260

    #define FWD1_TIME_OUT_ERROR     0x5627
    
    #define VARIANT_CODING_ERROR    0x5702
    
    #if 0
	    #define CDC1_TIME_OUT_ERROR    0x5617
	    #define EPS1_TIME_OUT_ERROR    0x5618
	    #define AFS1_TIME_OUT_ERROR    0x5619
    #endif

    #if 0
    	#define ACC240_TIME_OUT_ERROR  0x5620
    #endif

    #define BRK_LAMP_RELAY_ERROR   0x6130
    
    #define HCU_TIME_OUT_ERROR     0x5614
    
    #define GEAR_SWITCH_ERR         0x5527
   
    #define CLUTCH_SWITCH_ERR       0x5520
    
    #define VSM2_INVALID_DATA_ERROR 0x5688
    #define VSM2_TIME_OUT_ERROR     0x5687
    
    #define ESS_LP_ERR_DTC_CODE         0x6131     

    #define VACUUM_PUMP_RELAY_ERROR    0x6126
    #define VACUUM_PUMP_SYSTEM_ERROR   0x6231
   	#define VACUUM_SENSOR_HW_ERROR     0x5385
    #define VACUUM_SENSOR_SW_ERROR     0x5386
    
    #define PEDAL_SENSOR_HW_ERROR     0x5378 
    #define PEDAL_SENSOR_SIGNAL_ERROR 0x5379 
    #define PEDAL_SENSOR_CAL_ERROR    0x5380 
    #define PRESSURE_SENSOR_CAL_ERROR    0x5384

    
    #define SCC_ACCEL_ABNORM_DTC      0x5357 /*Not used*/
    #define SCC_ECU_FAULT_DTC         0x5359 /*Not used*/
    #define EMS1_EMS2_CAN_ABNORM_DTC  0x5648 /*Invalid CAN data EMS1*/
    #define SCC1_SCC2_CAN_TIMEOUT_DTC 0x5638 /*CAN time-out SCC*/
    #define EMS5_CAN_TIMEOUT_DTC      0x5649 /*Invalid CAN data EMS5 + Time-out*/
    #define SCC1_SCC2_CAN_ABNORM_DTC  0x5650 /*Invalid CAN data SCC*/
    #define TCU_SCC_CAN_FAIL_DTC      0x6228 /*Invalid CAN data TCU1*/
    #define TCU_GEAR_INVALID_FAIL_DTC 0x6243 /*Invalid CAN data GEAR*/
    
    #define AHB_TIME_OUT_ERROR            0x5633
    #define BMS_TIME_OUT_ERROR            0x5635
    #define MCU_TIME_OUT_ERROR            0x5806
    
    #define EPB1_TIMEOUT_ERROR    0x5651
    #define EPB1_SIGNAL_ERROR     0x5652
    #define AVH_SW_ERROR          0x5358 
    
    #if 1
	    #define CLU2_TIMEOUT_ERROR    0x5628
	    #define CLU2_SIGNAL_ERROR     0x5656         
    #else
	    #define CLU2_TIMEOUT_ERROR    0x5812
	    #define CLU2_SIGNAL_ERROR     0x580E         
	#endif    

/* MOC DTC List */
#define DTC_BAT_RESET						0x5100
#define DTC_IGN_LINE						0x5103
#define DTC_APPLY_SWITCH					0x5501
#define DTC_RELEASE_SWITCH					0x5502
#define DTC_SWITCH_POWER					0x5503
#define DTC_SWITCH_COMMON					0x5505
#define DTC_FORCE_SEN						0x5506
#define DTC_CURRENT_SEN						0x5507
#define DTC_ACTUAR_DRV						0x5508
#define DTC_ECUHW							0x5604
#define DTC_CAN_OVERRUN						0x5604
#define DTC_HALL_COUNT						0x5509
#define DTC_BLS_SWITCH						0x5513 
#define DTC_CAN_BUSOFF						0x5616
#define DTC_EMS_TIMEOUT						0x5611
#define DTC_TCU_TIMEOUT						0x5612
#define DTC_BCM_TIMEOUT					    0x5555/* 0xC140*/
#define DTC_BSM_TIMEOUT						0x5625
#define DTC_VICM_TIMEOUT					0x6666/* 0xC167*/
#define DTC_CLU_TIMEOUT						0x5628
#define DTC_MOTOR_SHORT_OPEN_L              0x6416
#define DTC_MOTOR_SHORT_OPEN_R              0x6417	
#define DTC_MOTOR_TIMEOUT_L                 0x6220
#define DTC_MOTOR_TIMEOUT_R                 0x6224	
#define DTC_CALERR                          0x5710
#define DTC_CONTROL_NOT_FINISHED            0x5100
#define DTC_HALLPWR_OPEN_SHORT              0x4444

#define DTC_INVMSG_GSEL						0x5646	
#define DTC_INVMSG_WHEEL					0x5642
#define DTC_INVMSG_DRVD						0x5656
#define DTC_INVMSG_SEATBELT					0x5656
#define DTC_INVMSG_MISSION					0x5613
#define DTC_INVMSG_ENGINE					0xFFFF
#define DTC_INVMSG_BLS						0xFFFF
#define DTC_INVMSG_CLUTCH					0xFFFF
#define DTC_INVMSG_AX						0xFFFF
#define DTC_INVMSG_LDMS						0xFFFF

extern uint8_t  CDS_u8GetWheelSpeed(uint8_t u8SelWss);
extern uint8_t  CDS_u8CalcVehicleSpeed(uint8_t SelDtype);
extern uint8_t  CDS_u8GetActuatorStatus(uint8_t u8SelAct);
extern uint8_t  CDS_u8GetLampStatus(uint8_t u8SelLP);
extern uint8_t  CDS_u8GetSwitchStatus(uint8_t u8SelSW);
#if ((__G_SENSOR_TYPE!=NONE)||(__4WD_VARIANT_CODE==ENABLE))  
extern uint8_t  CDS_u8GetAxSnsrData(void);
#endif
#if __FULL_ESC_SYSTEM==1
extern uint16_t CDS_u16GetYawAySnsrData(uint8_t u8SelSnsr);
extern uint8_t  CDS_u8GetMcpSnsrData(uint8_t u8SelSnsr);
extern uint16_t CDS_u16GetSteeringData(void);
    #if 0
    extern uint8_t CDS_u8GetSteerPhaseData(void);
    #endif
#endif
extern uint16_t CDS_u16AnalizeHmcDtc(uint8_t u8ErrBuff);
extern uint8_t  CDS_u8DirveActuatorData(uint8_t u8SelAct);
extern uint8_t  CDS_u8GetPowerAdData(uint8_t u8SelPwr);
extern uint16_t CDS_u16GetVacuumSnsrData(void);
extern uint8_t  CDS_u16GetPedalTravelData(uint8_t u8SelPedal);

#endif
