/**
 * @defgroup Diag_Hndlr Diag_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Diag_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Diag_Hndlr.h"
#include "Diag_Hndlr_Ifa.h"
#include "IfxStm_reg.h"
#include "CDC_DiagService.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Diag_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define DIAG_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Diag_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Diag_Hndlr_HdrBusType Diag_HndlrBus;

/* Version Info */
const SwcVersionInfo_t Diag_HndlrVersionInfo = 
{   
    DIAG_HNDLR_MODULE_ID,           /* Diag_HndlrVersionInfo.ModuleId */
    DIAG_HNDLR_MAJOR_VERSION,       /* Diag_HndlrVersionInfo.MajorVer */
    DIAG_HNDLR_MINOR_VERSION,       /* Diag_HndlrVersionInfo.MinorVer */
    DIAG_HNDLR_PATCH_VERSION,       /* Diag_HndlrVersionInfo.PatchVer */
    DIAG_HNDLR_BRANCH_VERSION       /* Diag_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Wss_SenWhlSpdInfo_t Diag_HndlrWhlSpdInfo;
Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Diag_HndlrIdbSnsrEolOfsCalcInfo;
Mom_HndlrEcuModeSts_t Diag_HndlrEcuModeSts;
Prly_HndlrIgnOnOffSts_t Diag_HndlrIgnOnOffSts;
Ioc_InputSR1msVBatt1Mon_t Diag_HndlrVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Diag_HndlrVBatt2Mon;
Ioc_InputSR1msFspCbsMon_t Diag_HndlrFspCbsMon;
Ioc_InputSR1msCEMon_t Diag_HndlrCEMon;
Ioc_InputSR1msVddMon_t Diag_HndlrVddMon;
Ioc_InputSR1msCspMon_t Diag_HndlrCspMon;
Eem_SuspcDetnFuncInhibitDiagSts_t Diag_HndlrFuncInhibitDiagSts;
Mtr_Processing_DiagMtrDiagState_t Diag_HndlrMtrDiagState;

/* Output Data Element */
Diag_HndlrDiagVlvActr_t Diag_HndlrDiagVlvActrInfo;
Diag_HndlrMotReqDataDiagInfo_t Diag_HndlrMotReqDataDiagInfo;
Diag_HndlrSasCalInfo_t Diag_HndlrSasCalInfo;
Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
Diag_HndlrDiagHndlRequest_t Diag_HndlrDiagHndlRequest;
Diag_HndlrDiagClr_t Diag_HndlrDiagClrSrs;
Diag_HndlrDiagSci_t Diag_HndlrDiagSci;
Diag_HndlrDiagAhbSci_t Diag_HndlrDiagAhbSci;

uint32 Diag_Hndlr_Timer_Start;
uint32 Diag_Hndlr_Timer_Elapsed;

#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/** Variable Section (32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/** Variable Section (32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_CODE
#include "Diag_MemMap.h"

//LEJ
#include "TimE.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Diag_Hndlr_Init(void)
{
    /* Initialize internal bus */
    Diag_HndlrBus.Diag_HndlrWhlSpdInfo.FlWhlSpd = 0;
    Diag_HndlrBus.Diag_HndlrWhlSpdInfo.FrWhlSpd = 0;
    Diag_HndlrBus.Diag_HndlrWhlSpdInfo.RlWhlSpd = 0;
    Diag_HndlrBus.Diag_HndlrWhlSpdInfo.RrlWhlSpd = 0;
    Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk = 0;
    Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd = 0;
    Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk = 0;
    Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd = 0;
    Diag_HndlrBus.Diag_HndlrEcuModeSts = 0;
    Diag_HndlrBus.Diag_HndlrIgnOnOffSts = 0;
    Diag_HndlrBus.Diag_HndlrVBatt1Mon = 0;
    Diag_HndlrBus.Diag_HndlrVBatt2Mon = 0;
    Diag_HndlrBus.Diag_HndlrFspCbsMon = 0;
    Diag_HndlrBus.Diag_HndlrCEMon = 0;
    Diag_HndlrBus.Diag_HndlrVddMon = 0;
    Diag_HndlrBus.Diag_HndlrCspMon = 0;
    Diag_HndlrBus.Diag_HndlrFuncInhibitDiagSts = 0;
    Diag_HndlrBus.Diag_HndlrMtrDiagState = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FlOvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FlIvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FrOvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.FrIvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RlOvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RlIvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RrOvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RrIvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.SimVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.BalVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.CircVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.PressDumpReqData = 0;
    Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = 0;
    Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = 0;
    Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = 0;
    Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = 0;
    Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo.MotReq = 0;
    Diag_HndlrBus.Diag_HndlrSasCalInfo.DiagSasCaltoAppl = 0;
    Diag_HndlrBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = 0;
    Diag_HndlrBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = 0;
    Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Order = 0;
    Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Iq = 0;
    Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_Id = 0;
    Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = 0;
    Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = 0;
    Diag_HndlrBus.Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = 0;
    Diag_HndlrBus.Diag_HndlrDiagClrSrs = 0;
    Diag_HndlrBus.Diag_HndlrDiagSci = 0;
    Diag_HndlrBus.Diag_HndlrDiagAhbSci = 0;
}
uint8 Stop_counter; 
void Diag_Hndlr(void)
{

    #if (ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)    	
        ASresult[TIME_AS_CHECKPOINT_FOR_10MS] = SetAlivesupervisionCheckpoint(TIME_AS_CHECKPOINT_FOR_10MS); 
    #elif (ALIVESUPERVISION_INDICATION == STD_ON)    
        ASresult[TIME_AS_CHECKPOINT_FOR_10MS] = SetAlivesupervisionCheckpoint0(TIME_AS_CHECKPOINT_FOR_10MS);    
    #endif 
        
    Diag_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Diag_Hndlr_Read_Diag_HndlrWhlSpdInfo(&Diag_HndlrBus.Diag_HndlrWhlSpdInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrWhlSpdInfo 
     : Diag_HndlrWhlSpdInfo.FlWhlSpd;
     : Diag_HndlrWhlSpdInfo.FrWhlSpd;
     : Diag_HndlrWhlSpdInfo.RlWhlSpd;
     : Diag_HndlrWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Diag_Hndlr_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo(&Diag_HndlrBus.Diag_HndlrIdbSnsrEolOfsCalcInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrIdbSnsrEolOfsCalcInfo 
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
     =============================================================================*/
    
    Diag_Hndlr_Read_Diag_HndlrEcuModeSts(&Diag_HndlrBus.Diag_HndlrEcuModeSts);
    Diag_Hndlr_Read_Diag_HndlrIgnOnOffSts(&Diag_HndlrBus.Diag_HndlrIgnOnOffSts);
    Diag_Hndlr_Read_Diag_HndlrVBatt1Mon(&Diag_HndlrBus.Diag_HndlrVBatt1Mon);
    Diag_Hndlr_Read_Diag_HndlrVBatt2Mon(&Diag_HndlrBus.Diag_HndlrVBatt2Mon);
    Diag_Hndlr_Read_Diag_HndlrFspCbsMon(&Diag_HndlrBus.Diag_HndlrFspCbsMon);
    Diag_Hndlr_Read_Diag_HndlrCEMon(&Diag_HndlrBus.Diag_HndlrCEMon);
    Diag_Hndlr_Read_Diag_HndlrVddMon(&Diag_HndlrBus.Diag_HndlrVddMon);
    Diag_Hndlr_Read_Diag_HndlrCspMon(&Diag_HndlrBus.Diag_HndlrCspMon);
    Diag_Hndlr_Read_Diag_HndlrFuncInhibitDiagSts(&Diag_HndlrBus.Diag_HndlrFuncInhibitDiagSts);
    Diag_Hndlr_Read_Diag_HndlrMtrDiagState(&Diag_HndlrBus.Diag_HndlrMtrDiagState);

    /* Process */
	Diag_10ms_EnterDiagOnCAN();
    /* Output */
    Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo(&Diag_HndlrBus.Diag_HndlrDiagVlvActrInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrDiagVlvActrInfo 
     : Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty;
     : Diag_HndlrDiagVlvActrInfo.FlOvReqData;
     : Diag_HndlrDiagVlvActrInfo.FlIvReqData;
     : Diag_HndlrDiagVlvActrInfo.FrOvReqData;
     : Diag_HndlrDiagVlvActrInfo.FrIvReqData;
     : Diag_HndlrDiagVlvActrInfo.RlOvReqData;
     : Diag_HndlrDiagVlvActrInfo.RlIvReqData;
     : Diag_HndlrDiagVlvActrInfo.RrOvReqData;
     : Diag_HndlrDiagVlvActrInfo.RrIvReqData;
     : Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.SimVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.ResPVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.BalVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.CircVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.PressDumpReqData;
     : Diag_HndlrDiagVlvActrInfo.RelsVlvReqData;
     =============================================================================*/
    
    Diag_Hndlr_Write_Diag_HndlrMotReqDataDiagInfo(&Diag_HndlrBus.Diag_HndlrMotReqDataDiagInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrMotReqDataDiagInfo 
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData;
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData;
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData;
     : Diag_HndlrMotReqDataDiagInfo.MotReq;
     =============================================================================*/
    
    Diag_Hndlr_Write_Diag_HndlrSasCalInfo(&Diag_HndlrBus.Diag_HndlrSasCalInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrSasCalInfo 
     : Diag_HndlrSasCalInfo.DiagSasCaltoAppl;
     =============================================================================*/
    Diag_HndlrBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = cdu1DiagPedalTravelSnrCalReq;
	Diag_HndlrBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = cdu1DiagPressureSnrCalReq;
    Diag_Hndlr_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo(&Diag_HndlrBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo 
     : Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
     : Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
     =============================================================================*/
    
    Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest(&Diag_HndlrBus.Diag_HndlrDiagHndlRequest);
    /*==============================================================================
    * Members of structure Diag_HndlrDiagHndlRequest 
     : Diag_HndlrDiagHndlRequest.DiagRequest_Order;
     : Diag_HndlrDiagHndlRequest.DiagRequest_Iq;
     : Diag_HndlrDiagHndlRequest.DiagRequest_Id;
     : Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM;
     : Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM;
     : Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM;
     =============================================================================*/
    
    Diag_Hndlr_Write_Diag_HndlrDiagClrSrs(&Diag_HndlrBus.Diag_HndlrDiagClrSrs);
    Diag_Hndlr_Write_Diag_HndlrDiagSci(&Diag_HndlrBus.Diag_HndlrDiagSci);
    Diag_Hndlr_Write_Diag_HndlrDiagAhbSci(&Diag_HndlrBus.Diag_HndlrDiagAhbSci);

    Diag_Hndlr_Timer_Elapsed = STM0_TIM0.U - Diag_Hndlr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define DIAG_HNDLR_STOP_SEC_CODE
#include "Diag_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
