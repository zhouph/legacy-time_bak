/*
 * Diag_Ext.h
 *
 *  Created on: 2014. 11. 27.
 *      Author: jinsu.park
 */

#ifndef DIAG_EXT_H_
#define DIAG_EXT_H_

/*==============================================================================
 *                  INCLUDE FILES
 ==============================================================================*/
#include "Diag_ExtTypes.h"
#include "ComStack_Types.h"
#include "Can_GeneralTypes.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/*CDCS_DiagService.c ����*/
extern uint8_t	Diag_fu1AllControl;
extern uint8_t	Diag_min_speed;
extern uint8_t	Diag_standstill_flg;
extern uint8_t	Diag_fu16LoopCounter;
//extern uint8_t	Diag_fu16CalVoltVDD;
extern uint8_t	Diag_fsu1PedalEOLOffsetWriteOk;
extern uint8_t	Diag_lcahbu1AHBOn;
extern uint8_t	Diag_fu1BLSErrDet;
extern uint8_t	Diag_fu1BLSSignal;
extern uint8_t	Diag_number_of_error;
extern uint8_t	Diag_clear_all_flg;
extern uint8_t	Diag_fcu1ATDriveType;
extern uint8_t	Diag_fcu1CVTDriveType;
extern uint16_t	Diag_fcu16EngRpm;
extern uint8_t	Diag_feu1RlWssErrFlg;
extern uint8_t	Diag_feu1RrWssErrFlg;
extern uint8_t	Diag_fcu8TPS;
extern uint8_t	Diag_fcu8GearPosition;
extern uint8_t	Diag_fu1BSSignal;
//extern uint8_t	Diag_feu8TestComplete[ERROR_BYTE_NO];
extern uint8_t	Diag_wmu8EcuHwVersion;
extern uint8_t	Diag_asic_version;
extern uint8_t	Diag_wu1PedalPdt1EepEraseOk;
extern uint8_t	Diag_wu1PedalPdt2EepEraseOk;
extern uint8_t	Diag_wu1PressureBcEepEraseOk;
extern uint8_t	Diag_wu1PressurePsEepEraseOk;
extern uint8_t	Diag_fsu1PressureCalNGState;
extern uint8_t	Diag_lsahbu1PressEEPofsCalcEND;
extern uint8_t	Diag_lsahbu1PressEEPofsCalcOK;
extern uint8_t	Diag_wu1PressurePsEepWrtOk;
extern uint8_t	Diag_wu1PressureBcEepWrtOk;
extern uint8_t	Diag_fsu1PedalCalNGState;
extern uint8_t	Diag_lsahbu1PedalEEPofsCalcEND;
extern uint8_t	Diag_lsahbu1PedalEEPofsCalcOK;
extern uint8_t	Diag_wu1PedalPdt1EepWrtOk;
extern uint8_t	Diag_wu1PedalPdt2EepWrtOk;
extern uint8_t	Diag_fu1HDCSwitchSignal;
/*CDS_ReadRecordValue.c ����*/
extern uint8_t	Diag_feu1FlWssErrFlg;
extern uint8_t	Diag_feu1FrWssErrFlg;
extern uint8_t	Diag_feu1RlWssErrFlg;
extern uint8_t	Diag_feu1RrWssErrFlg;
extern uint8_t	Diag_motor_mon_flg;
extern uint8_t	Diag_motor_error_flg;
extern uint8_t	Diag_mr_error_flg;
extern uint8_t	Diag_abs_fsr_mon_flg;
extern uint8_t	Diag_ahb_fsr_mon_flg;
extern uint8_t	Diag_fu1FLNODriveReqSet;
extern uint8_t	Diag_fiu1SolFLNOShortFail;
extern uint8_t	Diag_fiu1SolFLNOOpenFail;
extern uint8_t	Diag_fvu1SolFLNO200usDrive;
extern uint8_t	Diag_wsu1FlnoShort;
extern uint8_t	Diag_wsu1FlnoOpen;
extern uint8_t	Diag_fu1FRNODriveReqSet;
extern uint8_t	Diag_fiu1SolFRNOShortFail;
extern uint8_t	Diag_fiu1SolFRNOOpenFail;
extern uint8_t	Diag_fu1RLNODriveReqSet;
extern uint8_t	Diag_fiu1SolRLNOShortFail;
extern uint8_t	Diag_fiu1SolRLNOOpenFail;
extern uint8_t	Diag_fu1RRNODriveReqSet;
extern uint8_t	Diag_fiu1SolRRNOShortFail;
extern uint8_t	Diag_fiu1SolRRNOOpenFail;
extern uint8_t	Diag_fu1FLNCDriveReqSet;
extern uint8_t	Diag_fiu1SolFLNCShortFail;
extern uint8_t	Diag_fiu1SolFLNCOpenFail;
extern uint8_t	Diag_fu1FRNCDriveReqSet;
extern uint8_t	Diag_fiu1SolFRNCShortFail;
extern uint8_t	Diag_fiu1SolFRNCOpenFail;
extern uint8_t	Diag_fu1RLNCDriveReqSet;
extern uint8_t	Diag_fiu1SolRLNCShortFail;
extern uint8_t	Diag_fiu1SolRLNCOpenFail;
extern uint8_t	Diag_fu1RRNCDriveReqSet;
extern uint8_t	Diag_fiu1SolRRNCShortFail;
extern uint8_t	Diag_fiu1SolRRNCOpenFail;
extern uint8_t	Diag_fu1APV1DriveReqSet;
extern uint8_t	Diag_fu1APV2DriveReqSet;
extern uint8_t	Diag_fu1RLV1DriveReqSet;
extern uint8_t	Diag_fu1RLV2DriveReqSet;
extern uint8_t	Diag_fu1CUT1DriveReqSet;
extern uint8_t	Diag_fu1CUT2DriveReqSet;
extern uint8_t	Diag_fu1SIMVDriveReqSet;
extern uint8_t	Diag_fcu1ABSLampDrive;
extern uint8_t	Diag_fcu1EBDLampDrive;
extern uint8_t	Diag_fcu1VDCLampDrive;
extern uint8_t	fcu2FunctionLampDrive;
extern uint8_t	Diag_fcu1VDCOffLampDrive;
extern uint8_t	Diag_fsu1AhbWLampOnReq;
extern uint8_t	Diag_fsu1RBCSWLampOnReq;
extern uint8_t	Diag_fsu8ServiceLampOnReq;
extern uint8_t	Diag_fu1ESCSwitchSignal;
extern uint8_t	Diag_fu1BLSSignal;
extern uint8_t	Diag_fu1PBSignal;
extern uint8_t	Diag_a_long_1_1000g;
extern uint8_t	Diag_feu1AX_HW_Line_err_flg;
extern uint8_t	Diag_SEN_BUS_off_err_flg;
extern uint8_t	Diag_SEN_hw_err_flg;
extern uint8_t	Diag_feu1IMU_HW_Line_err_flg;
extern uint8_t	Diag_fs16PosPress2ndSignal_1_100Bar;
extern uint8_t	Diag_fs16NegPress2ndSignal_1_100Bar;
extern uint8_t	Diag_fs16PosPress3rdSignal_1_100Bar;
extern uint8_t	Diag_fs16NegPress3rdSignal_1_100Bar;
extern uint8_t	Diag_fs16PosPress1stSignal_1_100Bar;
extern uint8_t	Diag_fs16NegPress1stSignal_1_100Bar;
extern uint8_t	Diag_fs16PosPress4thSignal_1_100Bar;
extern uint8_t	Diag_fs16NegPress4thSignal_1_100Bar;
extern uint8_t	Diag_steer_1_10deg;
extern uint8_t	Diag_str_hw_err_flg;
//extern uint8_t	Diag_fu16CalVoltVDD;
extern uint8_t	Diag_fu16CalVoltPower5V;
extern uint8_t	Diag_fu16CalASIC_PDT_5V_MON;
extern uint8_t	Diag_fu16CalASIC_PDF_5V_MON;
extern uint8_t	Diag_pdt_pot1_1_10mm;
extern int32_t	Diag_fys16EstimatedYaw;
extern int32_t 	Diag_fys16EstimatedAY;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
void Diag_CanIfRxIndication(Can_IdType CanId, uint8 CanDlc, const uint8 * PduInfoPtr);
/*==============================================================================
 *                  END OF FILE
 ==============================================================================*/


#endif /* DIAG_EXT_H_ */
