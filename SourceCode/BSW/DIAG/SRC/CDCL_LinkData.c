/*

File    : CDCL_v_LinkData.c
Author  : PK Kang    

Last Rev No : 1.0
Last Rev Date   : 2006/04/11 (YY/MM/DD)

SRS of CUSTOMER : HMC Diagnostic Communication on CAN Specification
                  

Target ECU  : MGH-40 ABS
              MGH-40 ESP

*/
#include "Asw_Types.h"
#include "Common.h"
//CDC_CANCHK_t DCL0;
//CDC_MISC_t   DCL1;
#define __DIAG_TYPE CAN_LINE
#if __DIAG_TYPE==CAN_LINE

#include "CDC_LinkData.h"
#include "CDC_DiagService.h"
#include "CDC_DefineConstant.h"
#include "CDC_DiagHeader.h"
#include "CDS_ActControl.h"
#include "Diag_Hndlr.h"

/* Declare Function For CAN Diag*/
void CDCL_vLinkData(void);
void CDCL_vReset(void);
static void CDCL_vArrangeTxSingleFrame(void);
static void CDCL_vArrangeTxFirstFrame(void);
static void CDCL_vCheckFlowControl(void);
static void CDCL_vArrangeTxConsecFrame(void);
static void CDCL_vSetDiagStatus(void);
static void CDCL_vClearTxBuffer(void);
void CDCL_vStopDiagSession(void);

static void CDCL_vArrangeTxFlowControl(void);
static void CDCL_vRxConsecFrame(void);



/*Declare Variable For CAN Diag*/
uint8_t cdclu8DiagStatus; /* Diag 상태 설정 */
/* Diag Status (cdclu8DiagStatus)
   0 = Ready Request
   1 = Tx Single Frame
   2 = Tx First Frame
   3 = Check Flow Control
   4 = Tx Consecutive Frame
   5 = End Tx One Block
   6 = End Tx Diag Service Data */
uint8_t  cdclu8TxBuffer[8];
uint8_t  cdclu8RxBuffer[8];
uint8_t  cdclu8TerminationStatus;
uint16_t cdclu16ServiceDataSum; /* 한 서비스에 대한 Service Data에 대한 전체 길이 */

uint16_t cdclu16ReceiveTimer; /* 한 서비스가 끝나고 다음 Request를 기다리는 타이머 5초 */
uint8_t  cdclu8OneTxFrameTotalBytes; /* 송신할 한 Frame의 전체 바이트 길이 */
uint16_t cdclu16WaitFlowControlTimer; /* Flow Control을 기다리는 타이머 100ms */
uint8_t  cdclu8FlowStatus; /* Flow 제어 변수 */
uint8_t  cdclu8BlockSize; /* Flow Control이 지정하는 송신해야 할 Block Size */ 
uint8_t  cdclu8STmin; /* Consecutive Frame간의 시간 Max 4ms*/

uint8_t  cdclu8SequeceNumber; /* Consecutive Frame의 순번 */
uint8_t  cdclu8SpareBytes; /* (cdclu16ServiceDataSum-6)/7의 나머지 - FF 별개*/ 
uint8_t  cdclu8TotalCframe; /* FF송신 후 CF로 보내야 할 Frame의 총 갯수 - FF 별개*/
uint8_t  cdclu8RemainCframe; /* 보낼 Frame의 남은 갯수 */
uint8_t  cdclu8SeqenceInUnit; /* BS내에서 카운트되는 Frame 순선 (Block) */
uint16_t cdclu16TxedBytes;  /* 보낸 Data의 갯수 */

uint8_t cdclu8RxReqFrame[20];
uint8_t cdcl8LenReqConsecFrm;
uint8_t cdcl8LenReqData;
uint8_t cdclu8WaitCosecFrmTimer;
uint8_t cdclu8ConsecCopyCnt;
uint8_t cdclu8TxCosecFrmDelay;

CDC_CANCHK_t DCL0;
CDC_MISC_t   DCL1;

uint32_t *pUpload;

#define PPAGE_TO_GPAGE(x) ((x&0x3FFF)|((x>>2)&~0x3FFF)|(0x400000))

/*Declare Constant For CAN Diag*/
#define U8_DIAG_NPCI_TYPE_SF   0x00
#define U8_DIAG_NPCI_TYPE_FF   0x10
#define U8_DIAG_NPCI_TYPE_CF   0x20
#define U8_DIAG_NPCI_TYPE_FC   0x30

#if 0
        #define U8_TIMEOUT_N_BRS  20 /* 200ms */
        #define U8_TIMEOUT_N_CRS  20 /* 200ms */
#else
        #define U8_TIMEOUT_N_BRS  15 /* 150ms */
        #define U8_TIMEOUT_N_CRS  15 /* 150ms */
#endif
    

#if 0
    #define U8_TX_STMIN       1  /* 20ms */ 
#else
    #define U8_TX_STMIN       0  /* 10ms */
#endif 






/* Diag Status (cdclu8DiagStatus)
   0  = Ready Request
   1  = Tx Single Frame
   2  = Tx First Frame
   3  = Check Flow Control
   4  = Tx Consecutive Frame
   5  = End Tx One Block
   6  = End Tx Diag Service Data 
   7  = Terminate Diag Service without response 
   8  = Tx Flow Control
   9  = Check&Copy Consecutive Frame
   10 = Ready Response to Mulitiple Fram Request*/
   
void CDCL_vLinkData(void)
{
    if(cdclu8DiagStatus==0)
    {   
        if(cdclu1RxOkFlg==0) 
        {
            if(cdcsu1EnterCANDiag==1)
            {
                if(cdclu16ReceiveTimer==0) 
                {
                    if(cdcsu8CanDiagMode!=U8_DIAG_SUPPLIER_MODE)
                    {
                        CDCL_vStopDiagSession();
                    }
                    else
                    {
                        ;
                    }
                }   
                else  
                {
                    cdclu16ReceiveTimer--;
                }   
            }
            else
            {
                ;
            }
        }    
        else 
        {    
            if(cdclu1RequestChkFlg==0)
            {
                #if 0
                    if((cdclu8RxBuffer[0]&0xF0)==0x10)
                    {
                        cdcl8LenReqData=cdclu8RxBuffer[1];
                        cdcl8LenReqConsecFrm=cdclu8RxBuffer[1]-6;
                        cdclu8RxReqFrame[0]=cdclu8RxBuffer[2];
                        cdclu8RxReqFrame[1]=cdclu8RxBuffer[3];
                        cdclu8RxReqFrame[2]=cdclu8RxBuffer[4];
                        cdclu8RxReqFrame[3]=cdclu8RxBuffer[5];
                        cdclu8RxReqFrame[4]=cdclu8RxBuffer[6];
                        cdclu8RxReqFrame[5]=cdclu8RxBuffer[7];
                        cdclu8DiagStatus=8;
                        cdclu1RxOkFlg=0;
                    }
                    else
                    {
                        CDCS_vRequestCheck();
                    }
                #else
                    CDCS_vRequestCheck();
                #endif
            }
            else
            {
                ;
            }
                       
            if(cdclu1SendReadyFlg==1)   
            {    
                if (cdclu16ServiceDataSum<=7) 
                {
                    cdclu8DiagStatus=1;
                }   
                else 
                {
                    cdclu8DiagStatus=2;
                    cdclu1SendReadyFlg=0;
                }
                    cdclu1RequestChkFlg=0;
                    cdclu1RxOkFlg=0;
            }
            else
            {
                ;
            }
        }
    }
    else if(cdclu8DiagStatus==1) 
    {
        if(cdclu1SendReadyFlg==1)
        {
            CDCL_vArrangeTxSingleFrame();
            cdclu1SendReadyFlg=0;
        }
        else
        {
            ;
        }
    }   
    else if(cdclu8DiagStatus==2) 
    {
        if(cdclu1TxEndFlg==1)
        {
            CDCL_vSetDiagStatus();
            cdclu1TxEndFlg=0;
        }
        else
        {
            CDCL_vArrangeTxFirstFrame();
        }
    }   
    else if(cdclu8DiagStatus==3) 
    {
        CDCL_vCheckFlowControl();
    }   
    else if(cdclu8DiagStatus==4) 
    {
        if(cdclu8TxCosecFrmDelay==U8_TX_STMIN)
        {
            cdclu8TxCosecFrmDelay=0;
            CDCL_vArrangeTxConsecFrame();
        }
        else
        {
            cdclu8TxCosecFrmDelay++;
        }
    }       
    else if(cdclu8DiagStatus==6)
    {
        if(cdclu1TxEndFlg==1)
        {
            CDCL_vSetDiagStatus();
            cdclu1TxEndFlg=0;
        }
        else
        {
            ;
        }
    }
    else if(cdclu8DiagStatus==7)
    {
        cdclu8DiagStatus=0;
        cdclu1RequestChkFlg=0;
        cdclu1RxOkFlg=0;
        cdclu1TxEndFlg=0;
        if(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)
        {
            cdclu16ReceiveTimer=U16_DIAG_T_60S;
        }
        else
        {
            cdclu16ReceiveTimer=U16_DIAG_T_5S;
        }
    }
    else if(cdclu8DiagStatus==8)
    {
        if(cdclu1TxEndFlg==1)
        {
            CDCL_vSetDiagStatus();
            cdclu1TxEndFlg=0;
        }
        else
        {
            CDCL_vArrangeTxFlowControl();
        }
    }
    else if(cdclu8DiagStatus==9)
    {
        CDCL_vRxConsecFrame();
    }
    else if(cdclu8DiagStatus==10)
    {
        if(cdclu1RequestChkFlg==0)
        {
            CDCS_vRequestCheck();
        }
        else
        {
            ;
        }
                   
        if(cdclu1SendReadyFlg==1)   
        {    
            if (cdclu16ServiceDataSum<=7) 
            {
                cdclu8DiagStatus=1;
            }   
            else 
            {
                cdclu8DiagStatus=2;
                cdclu1SendReadyFlg=0;
            }
            
            cdclu1RequestChkFlg=0;
        }
        else
        {
            ;
        }
    }   
    else 
    {
        ;
    }   
                
}

static void CDCL_vArrangeTxSingleFrame(void)
{
    uint8_t u8TxUnit;
    
    u8TxUnit=(uint8_t)cdclu16ServiceDataSum;
    cdclu8TxBuffer[0]=(u8TxUnit)|U8_DIAG_NPCI_TYPE_SF;
    
    if(cdclu1UploadFlashFlg==1)
    {
        cdclu8TxBuffer[1]=cdcsu8SendData[0];
    }
    else
    {
        WF_vMemCopy(&cdcsu8SendData[0], &cdclu8TxBuffer[1], u8TxUnit);
    }
      
    cdclu8OneTxFrameTotalBytes=u8TxUnit+1;
    cdclu8DiagStatus=6;
    cdclu1TxEnableFlg=1;

}

static void CDCL_vArrangeTxFirstFrame(void)
{
    uint8_t u8TxUnit;
       
    u8TxUnit=6;
    cdclu8TxBuffer[0]=(((uint8_t)(cdclu16ServiceDataSum>>8))&0x0F)|U8_DIAG_NPCI_TYPE_FF;
    cdclu8TxBuffer[1]=(uint8_t)cdclu16ServiceDataSum;
        
    if(cdclu1UploadFlashFlg==1)
    {
        cdclu8TxBuffer[2]=cdcsu8SendData[0];
    }
    else
    {
        WF_vMemCopy(&cdcsu8SendData[0], &cdclu8TxBuffer[2], u8TxUnit);
    }
    
    cdclu8OneTxFrameTotalBytes=u8TxUnit+2;
    cdclu8SpareBytes=(uint8_t)((cdclu16ServiceDataSum-6)%7);
    if(cdclu8SpareBytes==0) 
    {
        cdclu8TotalCframe=(uint8_t)((cdclu16ServiceDataSum-6)/7);
    }
    else 
    {
        cdclu8TotalCframe=(uint8_t)(((cdclu16ServiceDataSum-6)/7) + 1);
    }
    cdclu8RemainCframe=cdclu8TotalCframe;
    cdclu1TxEnableFlg=1;
}  
          

static void CDCL_vCheckFlowControl(void)
{
    uint8_t u8Rx1stByte;
    
    if(cdclu1RxOkFlg==0)
    {
        if(cdclu16WaitFlowControlTimer==0) 
        {
            cdclu8SeqenceInUnit=0;
            cdclu8SequeceNumber=0;
            cdclu1UploadFlashFlg=0;
            cdclu8DiagStatus=7;
        }
        else
        {
            cdclu16WaitFlowControlTimer--;
        }
    }
    else                   
    {
        u8Rx1stByte=cdclu8RxBuffer[0]&(uint8_t)0xF0;
        if(u8Rx1stByte==U8_DIAG_NPCI_TYPE_FC)
        {
            cdclu8FlowStatus=cdclu8RxBuffer[0]&0x0F;
            if(cdclu8RxBuffer[1]==0)
            {
                cdclu8BlockSize=cdclu8RemainCframe;
            }
            else
            {
                cdclu8BlockSize=cdclu8RxBuffer[1];
            }
                
            cdclu8STmin=cdclu8RxBuffer[2];
            cdclu8DiagStatus=4;
        }
        else
        {
            if(cdclu16WaitFlowControlTimer==0) 
            {
                cdclu8SeqenceInUnit=0;
                cdclu8SequeceNumber=0;
                cdclu1UploadFlashFlg=0;
                cdclu8DiagStatus=7;
            }  
            else 
            {
                cdclu16WaitFlowControlTimer--;
            }
        }
        cdclu1RxOkFlg=0;       
    }        
} 

static void CDCL_vArrangeTxConsecFrame(void)      
{
    uint8_t u8TxUnit;
           
    if(cdclu8FlowStatus==0)
    {
        if(cdclu8RemainCframe!=0)
        {
            if(cdclu1TxOkFlg==0)
            {
                if(cdclu8SeqenceInUnit<cdclu8BlockSize)       
                {
                    if(cdclu8RemainCframe>1) 
                    {
                        u8TxUnit = 7;
                    }   
                    else  
                    {
                        if(cdclu8SpareBytes==0) 
                        {
                            u8TxUnit = 7;
                        }
                        else 
                        {
                            u8TxUnit = cdclu8SpareBytes;
                            CDCL_vClearTxBuffer();
                        }
                    }                             
                                              
                    cdclu8TxBuffer[0]=((cdclu8SequeceNumber+1)&0x0F)|U8_DIAG_NPCI_TYPE_CF;
                    if(cdclu1UploadFlashFlg==0)
                    {
                        cdclu16TxedBytes = 6+(7*((uint16_t)cdclu8SequeceNumber));
                        WF_vMemCopy(&cdcsu8SendData[cdclu16TxedBytes], &cdclu8TxBuffer[1], u8TxUnit);
                    }
                    else
                    {
                        ;
                    }
                   
                    cdclu8OneTxFrameTotalBytes=u8TxUnit+1;
                    cdclu8SequeceNumber++;
                    cdclu8SeqenceInUnit++;
                    cdclu8RemainCframe = cdclu8TotalCframe-cdclu8SequeceNumber;
                    cdclu1TxEnableFlg=1;
                }
                else
                {
                    cdclu8SeqenceInUnit=0;
                    cdclu8DiagStatus=3;
                    cdclu16WaitFlowControlTimer=U8_TIMEOUT_N_BRS;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            cdclu8SeqenceInUnit=0;
            cdclu8SequeceNumber=0;
            cdclu1UploadFlashFlg=0;
            cdclu8DiagStatus=6;
        }
    }         
    else if(cdclu8FlowStatus==1)
    { 
        cdclu8DiagStatus=3;
        cdclu16WaitFlowControlTimer=U8_TIMEOUT_N_BRS;
    }
    else if(cdclu8FlowStatus==2)
    {
        cdclu8DiagStatus=0;
        if(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)
        {
            cdclu16ReceiveTimer=U16_DIAG_T_60S;
        }
        else
        {
            cdclu16ReceiveTimer=U16_DIAG_T_5S;
        }

    }
    else
    {
        ;
    }   
                     
            
}


static void CDCL_vArrangeTxFlowControl(void)
{
    cdclu8TxBuffer[0]=0x30;
    cdclu8TxBuffer[1]=0x00;
    cdclu8TxBuffer[2]=0x00;
    cdclu8TxBuffer[3]=0x00;
    cdclu8TxBuffer[4]=0x00;
    cdclu8TxBuffer[5]=0x00;
    cdclu8TxBuffer[6]=0x00;
    cdclu8TxBuffer[7]=0x00;
    
    cdclu1TxEnableFlg=1;
}


static void CDCL_vRxConsecFrame(void)
{
    uint8_t i, u8ForCnt, u8Rx1stByte, u8GetEnd;
    
    u8GetEnd=u8ForCnt=0;
    
    if(cdclu1RxOkFlg==0)
    {
        if(cdclu8WaitCosecFrmTimer==0) 
        {
            cdclu8DiagStatus=7;
        }
        else
        {
            cdclu8WaitCosecFrmTimer--;
        }
    }
    else                   
    {
        u8Rx1stByte=cdclu8RxBuffer[0]&(uint8_t)0xF0;
        
        if(u8Rx1stByte==U8_DIAG_NPCI_TYPE_CF)
        {
            cdclu8WaitCosecFrmTimer=U8_TIMEOUT_N_CRS;
            
            if(cdcl8LenReqConsecFrm<=7)
            {
                u8ForCnt=cdcl8LenReqConsecFrm;
                u8GetEnd=1;
            }
            else
            {
                u8ForCnt=7;
                cdcl8LenReqConsecFrm-=7;
            }
            
            for(i=0;i<u8ForCnt;i++)
            {
                cdclu8RxReqFrame[6+cdclu8ConsecCopyCnt+i]=cdclu8RxBuffer[1+i];
            }
            
            if(u8GetEnd==1)
            {
                cdclu8ConsecCopyCnt=0;
                cdclu8DiagStatus=10;
            }
            else
            {   
                cdclu8ConsecCopyCnt+=i;
            }  
        }
        else
        {
            if(cdclu8WaitCosecFrmTimer==0) 
            {
                cdclu8DiagStatus=7;
            }  
            else 
            {
                cdclu8WaitCosecFrmTimer--;
            }
        }
        cdclu1RxOkFlg=0;       
    }        
}
    

static void CDCL_vSetDiagStatus(void)
{
    if (cdclu8DiagStatus==6) 
    {
        if(cdclu8TerminationStatus==U8_DIAG_STOP_DIAG_SESSION) 
        {
            CDCL_vStopDiagSession();
        }
        else if (cdclu8TerminationStatus==U8_TX_RESPONSE_NO_REQ)
        {
            cdclu8DiagStatus=1;
            cdclu8TerminationStatus=0;
        }   
        else 
        {
            cdclu8DiagStatus=0;
            if(cdcsu8CanDiagMode==U8_DIAG_PROGRAM_MODE)
            {
                cdclu16ReceiveTimer=U16_DIAG_T_60S;
            }
            else
            {
                cdclu16ReceiveTimer=U16_DIAG_T_5S;
            }
        }  
    }    
    else if(cdclu8DiagStatus==2)
    {
        cdclu8DiagStatus=3;
        cdclu16WaitFlowControlTimer=U8_TIMEOUT_N_BRS;
    }
    else if(cdclu8DiagStatus==8)
    {
        cdclu8DiagStatus=9;
        cdclu8WaitCosecFrmTimer=U8_TIMEOUT_N_CRS;
    }
    else
    {
        ;
    }
    
    CDCL_vClearTxBuffer();
}

static void CDCL_vClearTxBuffer(void)
{
    uint8_t i;
    
    for(i=0;i<8;i++)
    {
        cdclu8TxBuffer[i]=0;
    }
}

void CDCL_vReset(void)
{
    CDCS_vStopActuation();
    cdu8DiagCopReset = 1;
}

void CDCL_vStopDiagSession(void)  /* not reset, diag_sci_flg=0, Diag Mode=Standard */ 
{
    CDCS_vStopActuation();
    CDCS_vInitSecurity();
        
    cdcsu8CanDiagMode=U8_DIAG_STANDARD_MODE;
    /* Diag Tool에서 Enable Normal Communication 을 선택하도록 
       체크 설정하여 Download 실시할 경우 통신 안됨 현상 발생 수정 */
    cdclu8DiagStatus=0;
    cdclu1RequestChkFlg=0;   /*IGN Riging Edge에서 Reqeust시 통신 불가 현상 개선*/
    cdcsu1EnterCANDiag=0;
    /* Stop Diag일 경우 AHB와 ESC 구분 Flag 0으로 Set */
    diag_ahb_sci_flg=0;
    diag_sci_flg=0;
	Diag_HndlrBus.Diag_HndlrDiagSci = 0;
	Diag_HndlrBus.Diag_HndlrDiagAhbSci = 0;
    cdclu8TerminationStatus=0;
     //ccu1TxDisableFlg=0;	// DIAG PJS
    
    #if 0
        cdu1InhibitFsChk=0;
    #endif
    
    cdu1SysShutDownReady=0;
    cdu1SysShutDownReq  =0;
}       


#if 0
void CDCL_vCopyFlash(uint32_t src_ptr, uint8_t* des_ptr, uint8_t number)
{
    uint16_t i;
    uint32_t tempAddr;

   /* pFwData= *(uint8_t *)(PPAGE_TO_GPAGE(0xF9B000+i));*/
    for(i=0;i<number;i++)
    {
        tempAddr= src_ptr+i;
        *(des_ptr+i)=*(uint8_t *)(PPAGE_TO_GPAGE(tempAddr));
    }
}     
#endif

#endif /* __DIAG_TYPE==CAN_LINE */

/*
=> Revision Note
=========================================================================================
Version_1.0 [2007.08.07] [PK Kang]
=========================================================================================
1. Initail Release
   *. CDCL_v = Communication Diag on CAN Link data


=========================================================================================
Version_1.1 [2008.03.12] [PK Kang]
=========================================================================================
변경 내역 1 : CDCL_vReset
  1. Register를 직접 Access하지 않고 Flag를 띄워 F/W Module에서 Reset을 취할 수 
     있도록 변경함.


=========================================================================================
Version_1.2 [2008.08.08] [PK Kang]
=========================================================================================
변경 내역 1 : SYMC CAN Diagnosis Spec 적용
  1. Functional Request 시 No Response
  2. Mulitiple Frame Request 처리 (First Frame -> Flow Control -> Consec Frame)

     

*/

