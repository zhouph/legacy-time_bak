#ifndef CDC_LINKDATA_H
#define CDC_LINKDATA_H

typedef struct
{
	uBitType u1cRxOk        :1;
	uBitType u1cTxOk        :1;
	uBitType u1cRxEnable    :1;
	uBitType u1cTxEnable    :1;
	uBitType u1cUploadFlash :1;
	uBitType u1cSendReady   :1;
	uBitType u1cRequestChk  :1;
	uBitType u1cTxEnd       :1;	
}CDC_CANCHK_t;

extern CDC_CANCHK_t DCL0;

#define cdclu1RxOkFlg         DCL0.u1cRxOk
#define cdclu1TxOkFlg         DCL0.u1cTxOk
#define cdclu1RxEnableFlg     DCL0.u1cRxEnable
#define cdclu1TxEnableFlg     DCL0.u1cTxEnable
#define cdclu1UploadFlashFlg  DCL0.u1cUploadFlash
#define cdclu1SendReadyFlg    DCL0.u1cSendReady	     
#define cdclu1RequestChkFlg   DCL0.u1cRequestChk
#define cdclu1TxEndFlg        DCL0.u1cTxEnd

typedef struct
{
	uBitType u1cRxFuncReq       :1;
	
}CDC_MISC_t;

extern CDC_MISC_t DCL1;

#define cdclu1RxFuncReqFlg         DCL1.u1cRxFuncReq


extern uint8_t  cdclu8RxBuffer[8];
extern uint8_t  cdclu8TxBuffer[8];
extern uint8_t  cdclu8DiagStatus;
extern uint8_t  cdclu8TerminationStatus;
extern uint16_t cdclu16ServiceDataSum; 

extern uint8_t  cdclu8RxReqFrame[20];
extern uint8_t  cdcl8LenReqData;

extern void CDCL_vLinkData(void);
extern void CDCL_vReset(void); 

extern void CDCL_vStopDiagSession(void);

#endif               