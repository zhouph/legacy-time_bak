#ifndef CDC_DIAGSERVICE_H
#define CDC_DIAGSERVICE_H



typedef struct 
{
    uBitType u1cReadProd        :1; 
    uBitType u1cWriteProdFirst  :1; 
    uBitType u1cWriteProd       :1; 
    uBitType u1cAccesMode       :1;
    uBitType u1cSecurityOk      :1;
    uBitType u1cSecurityNg      :1;
    uBitType u1cSATimeOk        :1; 
}CDC_EEP_t;
#define cdcsu1ReadProdFlg              DCS3.u1cReadProd 
#define cdcsu1WriteProdFirstFlg        DCS3.u1cWriteProdFirst 
#define cdcsu1WriteProdFlg             DCS3.u1cWriteProd
#define cdcsu1AccesModeFlg             DCS3.u1cAccesMode
#define cdcsu1SecurityOkFlg            DCS3.u1cSecurityOk
#define cdcsu1SecurityNgFlg            DCS3.u1cSecurityNg
#define cdcsu1SATimeOkflg              DCS3.u1cSATimeOk 
extern CDC_EEP_t   DCS3;

typedef struct 
{
    uBitType   u1cFSRDemand         :1;   
    uBitType   u1cMRDemand          :1;   
    uBitType   u1cActuation         :1;   
    uBitType   u1cActuationMulti    :1;

}CDC_DRV_t;
#define cdcsu1FSRDemandFlg             DCS4.u1cFSRDemand                  
#define cdcsu1MRDemandFlg              DCS4.u1cMRDemand                   
#define cdcsu1ActuationFlg             DCS4.u1cActuation              
#define cdcsu1ActuationMultiFlg        DCS4.u1cActuationMulti
extern CDC_DRV_t  DCS4; 

typedef struct 
{
    uBitType   u1cOverrunCalSas     :1;
    uBitType   u1cAtmChk            :1;
    uBitType   u1cEnterCANDiag      :1;
    uBitType   u1WaitingFunc        :1;
    uBitType   u1DisabledNet        :1;
    uBitType   u1WrongSysVolt       :1;
}CDC_Serv_t;

#define cdcsu1OverrunCalSasFlg   DCS5.u1cOverrunCalSas 
#define cdcsu1AtmChkFlg          DCS5.u1cAtmChk 
#define cdcsu1EnterCANDiag       DCS5.u1cEnterCANDiag 
#define cdcsu1WaitingFuncFlg     DCS5.u1WaitingFunc
#define cdu1DisabledNetFlg       DCS5.u1DisabledNet
#define cdu1WrongSysVoltFlg      DCS5.u1WrongSysVolt
extern CDC_Serv_t  DCS5;

typedef struct 
{
    uBitType   u1ClearVcEr          :1;
    uBitType   u1ResetVc            :1;
    uBitType   u1WaitVc             :1;
    uBitType   u1VcComplete         :1;
    uBitType   u1InlineCodeStart    :1;
    uBitType   u1InlineCodeRequest  :1;
    uBitType   u1ClearVarCodeStart  :1;
    uBitType   u1ClearVarCodeRequest:1;
    uBitType   u1ClrVcDtcFlg        :1;
    uBitType   u1ClrInlineDtcFlg    :1;
    uBitType   u2EssInlineCode      :2;
    uBitType   u2SpasInlineCode     :2;
    uBitType   u2AvhInlineCode      :2;
    uBitType   u2HsaInlineCode      :2;
    uBitType   u2EpbInlineCode      :2;
    /* Local Inline Set Add */
    uBitType   u2LocalInlineCode    :2;
    uBitType   u2DomInlineCode      :2;
    uBitType   u2NaInlineCode       :2;
    uBitType   u2EuInlineCode       :2;
}CDC_VC_t;

#define cdcsu1ClearVcErrFlg      DCS6.u1ClearVcEr
#define cdcsu1ResetVcFlg         DCS6.u1ResetVc
#define cdcsu1WaitVcFlg          DCS6.u1WaitVc
#define cdcsu1VcCompleteFlg      DCS6.u1VcComplete
#define cdu1InlineCodeStart      DCS6.u1InlineCodeStart
#define cdu1InlineCodeRequest    DCS6.u1InlineCodeRequest
#define cdu1ClearVarCodeStart    DCS6.u1ClearVarCodeStart
#define cdu1ClearVarCodeRequest  DCS6.u1ClearVarCodeRequest
#define cdcsu1ClrVcDtcFlg        DCS6.u1ClrVcDtcFlg
#define cdcsu1ClrInlineDtcFlg    DCS6.u1ClrInlineDtcFlg
#define cdu2EssInlineCode        DCS6.u2EssInlineCode
#define cdu2SpasInlineCode       DCS6.u2SpasInlineCode
#define cdu2AvhInlineCode        DCS6.u2AvhInlineCode
#define cdu2HsaInlineCode        DCS6.u2HsaInlineCode
#define cdu2EpbInlineCode        DCS6.u2EpbInlineCode
/* Local Inline Set Add */
#define cdu2LocalInlineCode      DCS6.u2LocalInlineCode
#define cdu2DomInlineCode        DCS6.u2DomInlineCode
#define cdu2NaInlineCode         DCS6.u2NaInlineCode
#define cdu2EuInlineCode         DCS6.u2EuInlineCode

extern CDC_VC_t  DCS6;
/*for MPS(Motor Position Sensor) by jungse*/
typedef struct 
{
    uBitType   u1MSPCalStart       	:1;
    uBitType   u1MSPCalEnd         	:1;
    uBitType   u1MSPCalInhibitFlg     :1;

}CDC_MSPOfs_t;

#define cdu1MSPCalStart		DCS9.u1MSPCalStart 
#define cdu1MSPCalEnd		DCS9.u1MSPCalEnd
#define cdu1MSPCalInhibitFlg	DCS9.u1MSPCalInhibitFlg
extern CDC_MSPOfs_t DCS9;
/***********************************/

#if ((__ECU==ESP_ECU_1)&&(__STEER_SENSOR_TYPE==CAN_TYPE))
typedef struct 
{
    uBitType   u1cCalSas            :1;
    uBitType   u1cDeCalSas          :1;
    uBitType   u1cCalSasChk         :1;
    uBitType   u1cDeCalSasChk       :1; 
}CDC_CalSas_t;

#define cdcsu1CalSasFlg          DCS7.u1cCalSas
#define cdcsu1DeCalSasFlg        DCS7.u1cDeCalSas
#define cdcsu1CalSasChkFlg       DCS7.u1cCalSasChk 
#define cdcsu1DeCalSasChkFlg     DCS7.u1cDeCalSasChk
extern CDC_CalSas_t  DCS7;
#endif

typedef struct 
{
    uBitType   u1AxOfsEepStart       :1;
    uBitType   u1EraseEepFlg         :1;
    uBitType   u1AxCalInhibitFlg     :1;
    uBitType   u1AxOfsEepCalEnd      :1;
    uBitType   u1CalAxStart          :1;
    uBitType   u1AxOfsWEepReq        :1;

}CDC_AxOfs_t;
#define cdcsu1AxOfsEepStart     DCS8.u1AxOfsEepStart
#define cdcsu1EraseEepFlg       DCS8.u1EraseEepFlg
#define cdcsu1AxCalInhibitFlg   DCS8.u1AxCalInhibitFlg
#define cdcsu1AxOfsEepCalEndFlg DCS8.u1AxOfsEepCalEnd
#define cdcsu1AxOfsWEepReqFlg   DCS8.u1AxOfsWEepReq
extern CDC_AxOfs_t  DCS8;

/* Pedal, Pressure Sensor Calibration을 위한 Struct 구문 처리 */
typedef struct 
{
    uBitType   u1CalOfsEepStart       :1;
    uBitType   u1CalOfsInhibitFlg     :1;
    uBitType   u1CalOfsEepCalEnd      :1;
    uBitType   u1CalOfsEraseFlg       :1;
    uBitType   u1CalOfsEndByCl        :1;
    uBitType   u1CalOfsEndOk          :1;
    uBitType   u1CalOfsEepWrtEnd      :1;
    uBitType   u1CalOfsWrongCondi     :1;
    uBitType   u8CalOfsStat           :8;
    uBitType   u16CalOfsWaitTimer     :16;
    uBitType   u1CalOfsResultOk       :1;
    uBitType   unused0                :7;    /* Byte 단위로 맞추기 위한 unsued */
}CDC_SnsrCalOfs_t;

extern CDC_SnsrCalOfs_t DiagCalPrs, DiagCalPdl;  /* Pedal, Pressure Sensor Offset Address */

//////////////////////////////////////////////////////////F0119 Taeho: SWD_V0
extern CDC_SnsrCalOfs_t DiagCalMPS_Offline;
/////////////////////////////////////////////////////////////////////////////


#define cdu1DiagPedalTravelSnrCalReq  DiagCalPdl.u1CalOfsEepStart /* Pedal Travel Sensor Calibration Request*/
#define cdu1DiagPressureSnrCalReq     DiagCalPrs.u1CalOfsEepStart /* Pressure Sensor Calibration Request*/

extern uint8_t  cdcsu8CanDiagMode; 
extern uint8_t  cdcsu8SendData[100];
extern uint32_t cdcsu32FlashAddr;

extern void CDCS_vRequestCheck(void);
extern void CDCS_vStopActuation(void);
extern void CDCS_vInitSecurity(void);

#define MOTOR_A_DUTY_ON 1
#define MOTOR_B_DUTY_ON 2
#define MOTOR_C_DUTY_ON 3

#endif                 
