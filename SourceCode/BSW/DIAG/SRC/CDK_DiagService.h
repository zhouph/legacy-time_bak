#ifndef CDK_DIAGSERVICE_H
#define CDK_DIAGSERVICE_H

typedef struct
{
    uBitType u1FsrDemand    :1;
    uBitType u1Actuation    :1;
}CDK_ACTCNTRL_t;

extern CDK_ACTCNTRL_t CDK1;

#define cdku1FsrDemandFlg    CDK1.u1FsrDemand   
#define cdku1ActuationFlg    CDK1.u1Actuation   

typedef struct
{
    uBitType u1ReadProd        :1;
    uBitType u1WriteFirst      :1;
    uBitType u1WriteProd       :1;
    uBitType u1RxEnd           :1;
    uBitType u1TxEnd           :1;
    uBitType u1RxDummy         :1;
    uBitType u1ActuationMulti  :1;
}CDK_DRVC_t;

extern CDK_DRVC_t CDK2;

#define cdku1ReadProdFlg         CDK2.u1ReadProd      
#define cdku1WriteFirstFlg       CDK2.u1WriteFirst
#define cdku1WriteProdFlg        CDK2.u1WriteProd    
#define cdku1RxEndFlg            CDK2.u1RxEnd        
#define cdku1TxEndFlg            CDK2.u1TxEnd        
#define cdku1RxDummyFlg          CDK2.u1RxDummy 
#define cdku1ActuationMultiFlg   CDK2.u1ActuationMulti 

typedef struct
{
    uBitType uk1SATimeOk        :1;
    uBitType uk1AccesMode       :1;
    uBitType uk1SecurityOk      :1;
    uBitType uk1SecurityNg      :1;
    
}CDK_SAFLG_t;

extern CDK_SAFLG_t CDKSA;

#define cdku1SATimeOkflg       CDKSA.uk1SATimeOk  
#define cdku1AccesModeFlg      CDKSA.uk1AccesMode    
#define cdku1SecurityOkFlg     CDKSA.uk1SecurityOk   
#define cdku1SecurityNgFlg     CDKSA.uk1SecurityNg   

#endif
