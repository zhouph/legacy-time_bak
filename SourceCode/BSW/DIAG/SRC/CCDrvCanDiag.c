/*******************************************************************************
* Project Name: CAN DIAG DRV
* File Name: CCDrvCanDiag.c
* Description: Hnadle CAN Diag messages
+----------------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|----------------------------------------------------------------------------------
| Date        Version  Author  Description
| ----------  -------  ------  ----------------------------------------------------
| 2007-08-16   1.1      KKS     - First Registration
| 2007-10-08   1.2      KKS     - Add SSANGYOUNG CAN
| 2008-08-08   1.3      KJH     - new SSANGYONG CAN diag 적용 
| 2009-04-15   1.4      KKY     - REVISION#01 DIAG_TX1 CAN BUF위치 수정                              
******************************************************************************/

/* Includes ******************************************************************/
#include "Diag_Hndlr.h"
#include "Common.h"
#include "ComStack_Types.h"// DIAG PJS
#include "Can_GeneralTypes.h"// DIAG PJS

#include "CDC_LinkData.h"

#if __DIAG_TYPE==CAN_LINE


/* Logcal Definiton  **********************************************************/
/*===========================================================
REVISION#01: DIAG_TX1 CAN BUF위치 수정(22-->15)
===========================================================*/
const uint16_t	DIAG_TX1[2] = {21,0x7D9};
/* Variables Definition********************************************************/
extern uint8_t cdclu8TxBuffer[8];
uint8_t ccu8DiagTxBuffer[8];
uint8_t wu8CandiagEnable;



/* LocalFunction prototype ***************************************************/
extern void CC_vSetTransRegChannel(uint8_t id0,uint8_t id1,uint8_t *trans_ptr,uint8_t prio,uint8_t size,uint8_t ch);
//void CC_vTransDiagData(void);
void CC_vSetDiagCan(void);

/* Implementation*************************************************************/



/************************************************************************
* FUNCTION NAME:               void CC_vTransDiagTxData(void)
* CALLED BY:          Main()
* Preconditions:      none 
* PARAMETER:          Handle of CAN Diag
* RETURN VALUE:       none
* Description:        Transmit CAN Diag Data
*************************************************************************/

void CC_vTransDiagData(void)
{	
	Can_PduType Can_Pdu;
	Can_ReturnType Retvalue;

#if __DIAG_TYPE==CAN_LINE
	if(wu8CandiagEnable==1)
	{
		Can_Pdu.swPduHandle = 10;
		Can_Pdu.id = 0x7D9;
		Can_Pdu.length = 8 ;
		Can_Pdu.sdu = (uint8_t*)&ccu8DiagTxBuffer;

		Retvalue=Can_17_MCanP_Write(38,  &Can_Pdu);
		cdclu1TxOkFlg=1;
	}
	else
	{
		;
	}

	if(cdclu1TxOkFlg==1)
	{
		cdclu1TxOkFlg   =0;
		wu8CandiagEnable=0;
	}
	else
	{
		;
	}
#endif

#if 0 // DIAG PJS
	CAN_Buff_Struct	can_buff_struct;
	#if __DIAG_TYPE==CAN_LINE
		if(wu8CandiagEnable==1)
		{
			can_buff_struct.id=DIAG_TX1[1];
			can_buff_struct.length=8;
			WF_vMemCopy((uint8_t*)&ccu8DiagTxBuffer[0],(uint8_t*)((void *)can_buff_struct.data),8);
			#if defined(__CAN_DIAG_SUB_CHANNEL_ENABLE)
			WC_TxCAN(FLEXCAN_MODULE_C,DIAG_TX1[0],&can_buff_struct);			
			#else
/*===========================================================
REVISION#01: DIAG_TX1 CAN BUF위치 수정
===========================================================*/
					WC_TxCAN(FLEXCAN_MODULE_A,DIAG_TX1[0],&can_buff_struct);
			#endif
			cdclu1TxOkFlg=1;
		}
		else
		{
			;
		}
		
		if(cdclu1TxOkFlg==1)
		{
			cdclu1TxOkFlg   =0;
			wu8CandiagEnable=0;
		}
		else
		{
			;
		}
	#endif
#endif
}
/************************************************************************
* FUNCTION NAME:               void CC_vSetDiagCan(void)
* CALLED BY:          Main()
* Preconditions:      none 
* PARAMETER:          Handle of CAN Diag
* RETURN VALUE:       none
* Description:        Start CAN Diag 
*************************************************************************/
void CC_vSetDiagCan(void)
{
/* DIAG TX LOAD*/
    if(cdclu1TxEnableFlg==1)
    {
        ccu8DiagTxBuffer[0]=cdclu8TxBuffer[0];
        ccu8DiagTxBuffer[1]=cdclu8TxBuffer[1];
        ccu8DiagTxBuffer[2]=cdclu8TxBuffer[2];
        ccu8DiagTxBuffer[3]=cdclu8TxBuffer[3];
        ccu8DiagTxBuffer[4]=cdclu8TxBuffer[4];
        ccu8DiagTxBuffer[5]=cdclu8TxBuffer[5];
        ccu8DiagTxBuffer[6]=cdclu8TxBuffer[6];
        ccu8DiagTxBuffer[7]=cdclu8TxBuffer[7];
        
        cdclu1TxEnableFlg=0;
        wu8CandiagEnable=1;
        cdclu1TxEndFlg=1;
    }
    else 
    {
        ;
    }
}

void CC_vSetCalSasMsg(void)
{
	#ifdef __SAS_CAL_ENA_ENABLE
#if 0 // 향후 기능 추가 필요 PJS
		CAN_Buff_Struct can_buff_struct;
		Can_PduType Can_Pdu;
		Can_ReturnType Retvalue;

				if(ctu1ResetSas==1)
		{

			can_buff_struct.id=SAS_CAL[1];
			can_buff_struct.length=2;
			STG_CAL[0] = 0x05;
			STG_CAL[1] = 0x00;
			WF_vMemCopy((uint8_t*)&STG_CAL[0],(uint8_t*)((void *)can_buff_struct.data),2);
			WC_TxCAN(ccu8_CanCH,SAS_CAL[0],&can_buff_struct);
			ctu1ResetSas = 0;

		}
		else if(ctu1StartChangeSas==1)
		{
			can_buff_struct.id=SAS_CAL[1];
			can_buff_struct.length=2;
			STG_CAL[0] = 0x03;
			STG_CAL[1] = 0x2B;
			WF_vMemCopy((uint8_t*)&STG_CAL[0],(uint8_t*)((void *)can_buff_struct.data),2);
			WC_TxCAN(ccu8_CanCH,SAS_CAL[0],&can_buff_struct);
			ctu1StartChangeSas = 0;
		}
		else
		{
			;
		}
#endif	// 향후 기능 추가 필요 PJS
	#endif
}

void Diag_CanIfRxIndication(Can_IdType CanId, uint8 CanDlc, const uint8 * PduInfoPtr)
{
	uint8 i;
	for(i=0;i<CanDlc;i++)
    {
		cdclu8RxBuffer[i] = (uint8_t)(PduInfoPtr[i]);
    }
	if(CanId == 0x7df)
	{
        cdclu1RxOkFlg=1;
        cdclu1RxFuncReqFlg=1;
	}
	else
	{
		cdclu1RxOkFlg=1;
	}
}
#endif
