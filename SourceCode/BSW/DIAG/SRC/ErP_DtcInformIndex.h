/*
 * ErP_DtcInformIndex.h
 *
 *  Created on: 2015. 1. 26.
 *      Author: jinsu.park
 */

#ifndef ERP_DTCINFORMINDEX_H_
#define ERP_DTCINFORMINDEX_H_

/* DTC Index */
    #define U8_FL_HW_FAULT                 1
    #define U8_FR_HW_FAULT                 2
    #define U8_RL_HW_FAULT                 3
    #define U8_RR_HW_FAULT                 4
    #define U8_FL_AIRGAP                   5
    #define U8_FR_AIRGAP                   6
    #define U8_RL_AIRGAP                   7
    #define U8_RR_AIRGAP                   8
    #define U8_FL_SPD_JUMP                 9
    #define U8_FR_SPD_JUMP                10
    #define U8_RL_SPD_JUMP                11
    #define U8_RR_SPD_JUMP                12
    #define U8_SOLENOID_FAIL              13
    #define U8_MOTOR_FAIL                 14
    #define U8_VALVE_RALEY_FAIL           15
    #define U8_ECU_HW_FAIL                16
    #define U8_OVER_POWER                 17
    #define U8_UNDER_POWER                18
    #define U8_CAN_OVERRUN                19
    #define U8_CAN_BUS_OFF                20
    #define U8_EMS_TIMEOUT                21
    #define U8_TCU_TIMEOUT                22
    #define U8_CAN_MSG_UNMATCH            23
    #define U8_G_SEN_HW_FAIL              24
    #define U8_G_SEN_SIGNAL_FAIL          25
    #define U8_12VSEN_PWR_FAIL            26

    #define U8_HDC_SW_FAIL                27
    #define U8_VDC_SW_FAIL                28
    #define U8_TEMPERATURE_ERROR          29
    #define U8_MCP_HW_FAIL                30
    #define U8_MCP_SIGNAL_FAIL            31
    #define U8_BLS_FAIL                   32
    #define U8_YAW_LG_OVERRUN             33
    #define U8_YAW_LG_ELECTIC_FAIL        34
    #define U8_YAW_LG_BUS_OFF_FAIL        35
    #define U8_YAW_LG_SIGNAL_FAIL         37
    #define U8_STR_HW_FAIL                38
    #define U8_STR_CAL_ERROR              39
    #define U8_STR_SIGNAL_FAIL            40
    #define U8_VARINAT_CODING_FAIL        41
    #define U8_FWD1_TIMEOUT               42
    #define U8_CDC1_TIMEOUT               43
    #define U8_EPS1_TIMEOUT               44
    #define U8_AFS1_TIMEOUT               45
    #define U8_ACC240_TIMEOUT             46
    #define U8_BRK_LP_RELAY_FAIL          47
    #define U8_HCU_TIMEOUT                48
    #define U8_VACUUM_PUMP_RELAY_FAIL     49
    #define U8_VACUUM_PUMP_SYSTEM_FAIL    50
    #define U8_PEDAL_SENSOR_HW_FAIL       51
    #define U8_PEDAL_SENSOR_SW_FAIL       52
    #define U8_PEDAL_SENSOR_CAL_FAIL      53
    #define U8_SCC_ACCEL_ABNORM_FAIL      54
    #define U8_EMS1_CAN_ABNORM_FAIL       55
    #define U8_SCC1_SCC2_CAN_TIMEOUT_FAIL 56
    #define U8_EMS5_CAN_ABNORM_FAIL       57
    #define U8_EMS5_CAN_TIMEOUT_FAIL      58
    #define U8_SCC1_SCC2_CAN_ABNORM_FAIL  59
    #define U8_TCU_SCC_CAN_FAIL_FAIL      60
    #define U8_ESS_LP_ERR                 61
    #define U8_ESS_HDC_RELAY_FAIL_ERR     62
    #define U8_G_SEN_CAL_FAIL             63
    #define U8_VSM2_INVALID_FAIL          64
    #define U8_VSM2_TIMEOUT               65
    #define U8_AHB_TIMEOUT                66
    #define U8_BMS_TIMEOUT                67
    #define U8_MCU_TIMEOUT                68
    #define U8_AVH_SW_FAIL                69
    #define U8_EPB1_TIMEOUT               70
    #define U8_EPB1_SIGNAL_FAIL           71
    #define U8_GEAR_SWITCH_FAIL           72
    #define U8_CLUTCH_SWITCH_FAIL         73
    #define U8_CLU2_TIMEOUT               74
    #define U8_CLU2_SIGNAL_FAIL           75
    #define U8_VACUUM_SENSOR_HW_FAIL      76
    #define U8_VACUUM_SENSOR_SW_FAIL      77
    #define U8_5VSEN_PWR_FAIL             78
    #define U8_AVSM_SW_FAIL               79
	#define U8_YAW_LG_ACU_FAIL            80
    #define U8_PEDAL_SIGNAL_ABNORMAL      81
    #define U8_PRESSURE_SENSOR_CAL_FAIL   82
    #define U8_HPA_SHORTTERM_CHARGE_FAIL  83
    #define U8_HPA_LONGTERM_CHARGE_FAIL   84
    #define U8_HPA_HW_FAIL                85
    #define U8_HPA_SIGNAL_FAIL            86
    #define U8_HPA_LOW_PRESSURE_FAIL      87
    #define U8_BCP_SEN_HW_FAIL            88
    #define U8_BCP_ABNORMAL_PRESSURE_FAIL 89
    #define U8_PSP_SEN_HW_FAIL            90
    #define U8_PSP_ABNORMAL_PRESSURE_FAIL 91
    #define U8_IGN_LINE_OPEN_FAIL         92
    #define U8_BOOSTER_CIRCUIT_LEAK_FAIL  93

    #define U8_EMSINVALID_FAIL            94
    #define U8_TCUGEARINVALID_FAIL        95

	#define U8_SENSOR_TIMEOUT			  400


    /* MOC DTC Index */
    #define U8_MOC_SWITCH_ERR             150
    #define U8_MOC_FORCE_ERR              151
    #define U8_MOC_CURRENT_ERR            152
    #define U8_MOC_ACTUATOR_L_LINE_ERR    153
    #define U8_MOC_ACTUATOR_R_LINE_ERR    154
    #define U8_MOC_ACTUATOR_L_TIMEOUT_ERR 155
    #define U8_MOC_ACTUATOR_R_TIMEOUT_ERR 156
    #define U8_MOC_CONTROL_NOT_FINISHED   157
    #define U8_MOC_HALLPWR_ERR            158
/* FTB */
#define U8_MAX_THRD           0x01
#define U8_MIN_THRD           0x02
#define U8_NO_SIGNAL          0x04
#define U8_INVALID_SIGNAL     0x08

/************ Error Index **********/
#define U16_FL_OPEN_ERR           1
#define U16_FR_OPEN_ERR           2
#define U16_RL_OPEN_ERR           3
#define U16_RR_OPEN_ERR           4

#define U16_FL_SHORT_ERR          5
#define U16_FR_SHORT_ERR          6
#define U16_RL_SHORT_ERR          7
#define U16_RR_SHORT_ERR          8

#define U16_FL_AIRGAP_ERR         9
#define U16_FR_AIRGAP_ERR         10
#define U16_RL_AIRGAP_ERR         11
#define U16_RR_AIRGAP_ERR         12

#define U16_FL_PHASE_ERR          13
#define U16_FR_PHASE_ERR          14
#define U16_RL_PHASE_ERR          15
#define U16_RR_PHASE_ERR          16

#define U16_FL_PJUMP_ERR          17
#define U16_FR_PJUMP_ERR          18
#define U16_RL_PJUMP_ERR          19
#define U16_RR_PJUMP_ERR          20

#define U16_FL_MJUMP_ERR          21
#define U16_FR_MJUMP_ERR          22
#define U16_RL_MJUMP_ERR          23
#define U16_RR_MJUMP_ERR          24

#define U16_FL_EXCITER_H_ERR      25
#define U16_FR_EXCITER_H_ERR      26
#define U16_RL_EXCITER_H_ERR      27
#define U16_RR_EXCITER_H_ERR      28

#define U16_FL_EXCITER_L_ERR      29
#define U16_FR_EXCITER_L_ERR      30
#define U16_RL_EXCITER_L_ERR      31
#define U16_RR_EXCITER_L_ERR      32

#define U16_FL_BROKEN_T_ERR       33
#define U16_FR_BROKEN_T_ERR       34
#define U16_RL_BROKEN_T_ERR       35
#define U16_RR_BROKEN_T_ERR       36

#define U16_FL_SPLIT_T_ERR        37
#define U16_FR_SPLIT_T_ERR        38
#define U16_RL_SPLIT_T_ERR        39
#define U16_RR_SPLIT_T_ERR        40

#define U16_FLO_OPEN_ERR          41
#define U16_FLO_SHORT_ERR         42
#define U16_FRO_OPEN_ERR          43
#define U16_FRO_SHORT_ERR         44
#define U16_RLO_OPEN_ERR          45
#define U16_RLO_SHORT_ERR         46
#define U16_RRO_OPEN_ERR          47
#define U16_RRO_SHORT_ERR         48

#define U16_FLI_OPEN_ERR          49
#define U16_FLI_SHORT_ERR         50
#define U16_FRI_OPEN_ERR          51
#define U16_FRI_SHORT_ERR         52
#define U16_RLI_OPEN_ERR          53
#define U16_RLI_SHORT_ERR         54
#define U16_RRI_OPEN_ERR          55
#define U16_RRI_SHORT_ERR         56

#define U16_APV1_OPEN_ERR         57
#define U16_APV1_SHORT_ERR        58
#define U16_APV2_OPEN_ERR         59
#define U16_APV2_SHORT_ERR        60
#define U16_RLV1_OPEN_ERR         61
#define U16_RLV1_SHORT_ERR        62
#define U16_RLV2_OPEN_ERR         63
#define U16_RLV2_SHORT_ERR        64

#define U16_FLSOL_CHANNEL_ERR     65
#define U16_FRSOL_CHANNEL_ERR     66
#define U16_RLSOL_CHANNEL_ERR     67
#define U16_RRSOL_CHANNEL_ERR     68
#define U16_WRONG_EBD_ERR         69
#define U16_WRONG_ESC_ERR         70

#define U16_VR_BYPASS_ERR         71
#define U16_VR_OPEN_ERR           72
#define U16_VR_TO_GND_ERR         73
#define U16_BAT1_FUSE_OPEN        74

#define U16_MOT_FUSE_OPEN_ERR     75
#define U16_MOT_SHORT_ERR         76
#define U16_MOT_OPEN_ERR          77
#define U16_MR_OPEN_ERR           78
#define U16_MOT_LOCK_ERR          79

#define U16_UNDER_PWR_ERR         80
#define U16_OVER_PWR_ERR          81
#define U16_LOW_PWR_ERR           82

#define U16_TEMPERATURE_ERR       83

#define U16_LOW_FLUID_ERR         84

#define U16_ADC_ERR               85
#define U16_ROM_ERR               86
#define U16_RAM_ERR               87
#define U16_CHECKSUM_ERR          88
#define U16_CYCLETIME_ERR         89
#define U16_COM_ERR               90
#define U16_EEPROM_ERR            91
#define U16_VDD_IC_ERR            92

#define U16_ASIC_UNDER_V_ERR      93
#define U16_ASIC_OVER_V_ERR       94
#define U16_ASIC_FREQ_ERR         95
#define U16_ASIC_OSTD_ERR         96
#define U16_ASIC_VERSION_ERR      97
#define U16_ASIC_LD_ERR           98
#define U16_ASIC_FGND_ERR         99

#define U16_AX_HW_LINE_ERR        100

        #define U16_AX_CAN_INTERNAL_ERR    101
        #define U16_AX_CAN_CHECKSUM_ERR    102
        #define U16_AX_CAN_ROLLING_CNT_ERR 103
    #define U16_AX_STD_OFFSET_ERR       104
    #define U16_AX_SHORT_DRV_OFFSET_ERR 105
    #define U16_AX_LONG_DRV_OFFSET_ERR  106
    #define U16_AX_STICK_ERR            107
    #define U16_AX_REVERSE_ERR          108
    #define U16_AX_RANGE_ERR            109
#define U16_VDC_SW_ERR            110
#define U16_HDC_SW_ERR            111
#define U16_5V_PWR_ERR            112
#define U16_BLS_ERR               113

#define U16_EMS1_TIMEOUT_ERR      114
#define U16_EMS2_TIMEOUT_ERR      115
#define U16_TCU1_TIMEOUT_ERR      116
#define U16_TCU1_UNMATCH_ERR      117
#define U16_BUS_OFF_ERR           118
#define U16_CAN_OVERRUN_ERR       119
#define U16_FWD1_TIMEOUT_ERR      120

#define U16_VARIANT_CODE_ERR      121
#define U16_BODYINFORM_TIMEOUT_ERR 126
#define U16_BLS_TIMEOUT_ERR        127
#define U16_SWTCH_TIMEOUT_ERR      128
#define U16_SYSPWR_TIMEOUT_ERR     129
#define U16_HCU_TIMEOUT_ERR        130
#define U16_SADS_TIMEOUT_ERR       131
#define U16_EPB_TIMEOUT_ERR        132

#define U16_ECM_TORQ_ERR         133
#define U16_ECM_ACCEL_ERR        134
#define U16_TCM_TRANS_ERR        135
#define U16_AWD_CNTRL_ERR        136
#define U16_TORQ_REDC_ERR        137

#define U16_ECM_SIG_VERI_ERR     138
#define U16_TCM_SIG_VERI_ERR     139
#define U16_BCM_SIG_VERI_ERR     140

#define U16_VIN_NOT_PROG_ERR     141
#define U16_WRONG_IMMO_ID_ERR    142
#define U16_ENV_ID_NOT_PROG_ERR  143
    #define U16_SAS_HW_LINE_ERR           144
        #define U16_SAD_CHECKSUM_ERR      145
        #define U16_SAD_ROLLING_CNT_ERR   146
        #define U16_SAD_INTERNAL_ERR      147
        #define U16_SAD_CALIBRATION_ERR   148
        #define U16_SAD_PROTECTION_ERR    149

        #define U16_SAD_SHORT_DRV_OFFSET_ERR 150
        #define U16_SAD_LONG_DRV_OFFSET_ERR  151
        #define U16_SAD_MDL_MAY_ERR          152
        #define U16_SAD_SHOCK_ERR            153
        #define U16_SAD_STICK_ERR            154
        #define U16_SAD_RATE_ERR             155
        #define U16_SAD_REVERSE_ERR          156
        #define U16_SAD_RANGE_ERR            157
    #define U16_IMU_HW_LINE_ERR       171
    #define U16_YAW_INTERNAL_ERR      172
    #define U16_IMU_CAN_FUNC_ERR      173
    #define U16_IMU_RASTER_FREQ_ERR   174
    #define U16_IMU_CHECKSUM_ERR      175
    #define U16_IMU_ROLLING_CNT_ERR   176

    #define U16_YAW_CBIT_ERR          177
    #define U16_LG_CBIT_ERR           178

    #define U16_YAW_SEN_BUS_OFF_ERR   179
    #define U16_YAW_SEN_OVERRUN_ERR   180

    #define U16_YAW_STD_OFFSET_ERR       181
    #define U16_YAW_SHORT_DRV_OFFSET_ERR 182
    #define U16_YAW_LONG_DRV_OFFSET_ERR  183
    #define U16_YAW_MDL_MAY_ERR          184
    #define U16_YAW_MDL_MUST_ERR         185
    #define U16_YAW_SHOCK_ERR            186
    #define U16_YAW_STICK_ERR            187
    #define U16_YAW_NOISE_ERR            188
    #define U16_YAW_REVERSE_ERR          189
    #define U16_YAW_RANGE_ERR            190

    #define U16_LG_STD_OFFSET_ERR       191
    #define U16_LG_SHORT_DRV_OFFSET_ERR 192
    #define U16_LG_LONG_DRV_OFFSET_ERR  193
    #define U16_LG_MDL_MAY_ERR          194
    #define U16_LG_MDL_MUST_ERR         195
    #define U16_LG_SHOCK_ERR            196
    #define U16_LG_STICK_ERR            197
    #define U16_LG_NOISE_ERR            198
    #define U16_LG_REVERSE_ERR          199
    #define U16_LG_RANGE_ERR            200

    #define U16_MP_AVERAGE_ERR        201
    #define U16_MP_HW_ERR             202
    #define U16_MP_OFFSET_ERR         203
    #define U16_MP_NOISE_ERR          204
    #define U16_MP_VREF_ERR           205
    #define U16_MP_OFFSET_10BAR_ERR   206

#define U16_BLS_BS_ERR            207
#define U16_BLS_HIGH_ERR          208
#define U16_BLS_LOW_ERR           209
#define U16_BLS_VREF_ERR          210
#define U16_BLS_NOISE_ERR         211
#define U16_BS_NOISE_ERR          212

#define U16_12V_SEN_OPEN_ERR      213
#define U16_12V_SEN_SHORT_ERR     214

#define U16_BLR_FET_OPEN_ERR      215
#define U16_BLR_CONTACT_OPEN_ERR  216
#define U16_BLR_COIL_OPEN_ERR     217

#define U16_HS_SUBNET_CONFIG_ERR  218
#define U16_CE_SUBNET_CONFIG_ERR  219

#define U16_EPB_SIG_VERI_ERR      220

#define U16_ECM_DLC_ERR           221
#define U16_TCM_DLC_ERR           222
#define U16_BCM_DLC_ERR           223

    #define U16_AY_INTERNAL_ERR       224
    #define U16_AX_CANFUNC_ERR        225
    #define U16_AX_CBIT_ERR           226
#define U16_BLS_INVALID_DATA_ERR    235
#define U16_DISSW_INVALID_DATA_ERR  236

#define U16_IL_ISR_ERR              237

#define U16_FL_WIRE_ERR             238
#define U16_FR_WIRE_ERR             239
#define U16_RL_WIRE_ERR             240
#define U16_RR_WIRE_ERR             241

#define U16_HDC_TEMPERATURE_ERR     242

#define U16_CAL_NOT_PROG_ERR        243
#define U16_HDC_RELAY_OFF_ERR               249
#define U16_ESS_DRIVE_ON_ERR                250
#define U16_ESS_RELAY_ON_ERR                251
#define U16_ESS_DRIVE_OFF_ERR               252
#define U16_ESS_RELAY_OFF_ERR               253

#define U16_AX_CALIBRATION_ERR              254

#define U16_VSM2_MSG_CS_ERR                 255
#define U16_VSM2_MSG_INTERNAL_ERR           256
#define U16_VSM2_MSG_ROLL_ERR               257
#define U16_VSM2_TIMEOUT_ERR                258

#define U16_AVH_SW_ERR                      259
#define U16_EPB1_TIMEOUT_ERR                260
#define U16_EPB1_SIGNAL_ERR                 261

#define U16_PEDAL_PDF_P1_OPEN_ERR           262
#define U16_PEDAL_PDF_P2_OPEN_ERR           263
#define U16_PEDAL_PDF_P1_SHORT_ERR          264
#define U16_PEDAL_PDF_P2_SHORT_ERR          265
#define U16_PEDAL_PDF_MISMATCH_ERR          266
#define U16_PEDAL_PDF_SIGNAL_ERR            267

#define U16_MAIN_AHB_TIMEOUT_ERR            268
#define U16_MAIN_AHB_INVALID_SIG_ERR        269
#define U16_AHB_TIMEOUT_ERR                 270
#define U16_AHB_INVALID_SIG_ERR             271

#define U16_SECURE_CODE_NOT_PROG_ERR        272
#define U16_WRONG_CAL_ERR                   273

#define U16_SCC_ACCEL_ABNORM_ERR            274
#define U16_EMS1MSGINVALID_ERR              275
#define U16_SCCTIMEOUT_ERR                  276
#define U16_EMS5MSGINVALID_ERR              277
#define U16_EMS5TIMEOUT_ERR                 278
#define U16_SCCMSGINVALID_ERR               279
#define U16_TCU1MSGINVALID_ERR              280
#define U16_TCUGEARINVALID_ERR              281
#define U16_EMSSIGINVALID_ERR               282


#define U16_MCU_TIMEOUT_ERR                 285

#define U16_BAS_CAL_ERR                     286

#define U16_COMMASIC_INIT_ERR               287
#define U16_ESCASIC_INIT_ERR                288

#define U16_SUBMCU_ERR                      289
#define U16_ROLLING_CNT_ERR                 290
#define U16_SEN_PPOWER_ERR				    291

#define U16_FLO_OVERTEMP_ERR                292
#define U16_FRO_OVERTEMP_ERR                293
#define U16_RLO_OVERTEMP_ERR                294
#define U16_RRO_OVERTEMP_ERR                295
#define U16_FLI_OVERTEMP_ERR                296
#define U16_FRI_OVERTEMP_ERR                297
#define U16_RLI_OVERTEMP_ERR                298
#define U16_RRI_OVERTEMP_ERR                299

#define U16_CUT1_OPEN_ERR                   300
#define U16_CUT1_SHORT_ERR                  301
#define U16_CUT2_OPEN_ERR                   302
#define U16_CUT2_SHORT_ERR                  303

#define U16_FLI_CURREG_ERR                  304
#define U16_FRI_CURREG_ERR                  305
#define U16_RLI_CURREG_ERR                  306
#define U16_RRI_CURREG_ERR                  307

#define U16_SIM_OPEN_ERR                    308
#define U16_SIM_SHORT_ERR                   309
#define U16_PSNC_CURREG_ERR                 310
#define U16_SSNC_CURREG_ERR                 311

#define U16_MOT_OVERTEMP_ERR                312

#define U16_FL_OVERTEMP_ERR                 313
#define U16_FR_OVERTEMP_ERR                 314
#define U16_RL_OVERTEMP_ERR                 315
#define U16_RR_OVERTEMP_ERR                 316

#define U16_FL_LEAKAGE_ERR                  317
#define U16_FR_LEAKAGE_ERR                  318
#define U16_RL_LEAKAGE_ERR                  319
#define U16_RR_LEAKAGE_ERR                  320

#define U16_VR_TO_LKG_ERR                   321

#define U16_CLU2_TIMEOUT_ERR                322
#define U16_CLU2_SIGNAL_ERR                 323

#define U16_FL_SWAP_ERR                     324
#define U16_FR_SWAP_ERR                     325
#define U16_RL_SWAP_ERR                     326
#define U16_RR_SWAP_ERR                     327

#define U16_LG_WRONG_INSTALLED_ERR          328
#define U16_AVSM_SW_ERR                     329

#define U16_VACCUM_TIMEOUT_ERR              330
#define U16_VACCUM_INV_SIG_ERR				331
#define U16_AX_NOISE_ERR                    332

/* Add PEDAL ERROR Indices */
#define U16_PDT_OFFSET_ERR                  333
#define U16_PDF_OFFSET_ERR                  334
#define U16_PDT_STICK_ERR                   335
#define U16_PDF_STICK_ERR                   336
#define U16_PDT_NOISE_ERR                   337
#define U16_PDF_NOISE_ERR                   338
#define U16_5V_PDF_PWR_ERR                  343
#define U16_5V_PDT_PWR_ERR                  344
#define U16_VBB_PWR_ERR               		345
#define U16_MOT_LSD_OPEN_ERR                346

#define U16_PEDAL_CAL_ERR                   347
#define U16_PRS_CAL_ERR                     348
#define U16_MOT_LSD_SHORT_ERR               349

#define U16_TVBB_TEMPERATURE_ERR            350
#define U16_SCC_TEMPERATURE_ERR             351
#define U16_HPA_SHORTTERM_CHARGE_ERR 352
#define U16_HPA_LONGTERM_CHARGE_ERR   353
#define U16_HPA_LOW_PRESSURE_ERR          354
#define U16_BCP_ABNORMAL_PRESSURE_ERR 355
#define U16_PSP_ABNORMAL_PRESSURE_ERR  356
#define U16_HPA_HW_ERR                                357
#define U16_BCP_HW_ERR                                 358
#define U16_PSP_HW_ERR                                 359
#define U16_IGN_LINE_OPEN_ERR               360
#define U16_BCP_LEAK_ERR                    361
#define U16_BCS_LEAK_ERR                    362

/* MOC Error Index BASE : 500*/
#define U16_MOC_APPLY_SWITCH_OPEN           500
#define U16_MOC_APPLY_SWITCH_SHORT          501
#define U16_MOC_APPLY_SWITCH_BLOCK          502
#define U16_MOC_RELEASE_SWITCH_OPEN         503
#define U16_MOC_RELEASE_SWITCH_SHORT        504
#define U16_MOC_RELEASE_SWITCH_BLOCK        505
#define U16_MOC_SWITCH_LINE_ERR             506
#define U16_MOC_SWITCH_INVALID_ERR          507
#define U16_MOC_SWITCH_DOUBLE_ON_ERR        508

#define U16_MOC_CURRENT_L_HW_ERR            509
#define U16_MOC_CURRENT_L_OFFSET_ERR        510
#define U16_MOC_CURRENT_L_OVER_VOLTAGE      511
#define U16_MOC_CURRENT_L_STICK_ERR         512
#define U16_MOC_CURRENT_L_LIMIT_ERR         513

#define U16_MOC_CURRENT_R_HW_ERR            514
#define U16_MOC_CURRENT_R_OFFSET_ERR        515
#define U16_MOC_CURRENT_R_OVER_VOLTAGE      516
#define U16_MOC_CURRENT_R_STICK_ERR         517
#define U16_MOC_CURRENT_R_LIMIT_ERR         518

#define U16_MOC_FORCE_CAL_ERR               519
#define U16_MOC_FORCE_INVALID_ERR           520
#define U16_MOC_FORCE_STICK_ERR             521
#define U16_MOC_FORCE_GRAD_ERR              522

#define U16_MOC_ACTUATOR_L_MTR_FET_ERR      523
#define U16_MOC_ACTUATOR_L_APPLY_DRV_ERR    524
#define U16_MOC_ACTUATOR_L_RELEASE_DRV_ERR  525
#define U16_MOC_ACTUATOR_L_NON_DRV_ERR      526
#define U16_MOC_ACTUATOR_L_UNDER_CTRL_ERR   527
#define U16_MOC_ACTUATOR_L_LONG_CONTROL     528

#define U16_MOC_ACTUATOR_R_MTR_FET_ERR      529
#define U16_MOC_ACTUATOR_R_APPLY_DRV_ERR    530
#define U16_MOC_ACTUATOR_R_RELEASE_DRV_ERR  531
#define U16_MOC_ACTUATOR_R_NON_DRV_ERR      532
#define U16_MOC_ACTUATOR_R_UNDER_CTRL_ERR   533
#define U16_MOC_ACTUATOR_R_LONG_CONTROL     534

#define U16_MOC_CONTROL_NOT_FINISHED        535
#define U16_MOC_ACTUATOR_VDD_TOGGLE_ERR     536

#define U16_MOC_HALL_PWR_OPEN_SHORT_ERR     537
#define U16_PDT_NOCHANGE_ERR                538
#endif /* ERP_DTCINFORMINDEX_H_ */
