#define S_FUNCTION_NAME      Diag_Hndlr_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          18
#define WidthOutputPort         33

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Diag_Hndlr.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Diag_HndlrWhlSpdInfo.FlWhlSpd = input[0];
    Diag_HndlrWhlSpdInfo.FrWhlSpd = input[1];
    Diag_HndlrWhlSpdInfo.RlWhlSpd = input[2];
    Diag_HndlrWhlSpdInfo.RrlWhlSpd = input[3];
    Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk = input[4];
    Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd = input[5];
    Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk = input[6];
    Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd = input[7];
    Diag_HndlrEcuModeSts = input[8];
    Diag_HndlrIgnOnOffSts = input[9];
    Diag_HndlrVBatt1Mon = input[10];
    Diag_HndlrVBatt2Mon = input[11];
    Diag_HndlrFspCbsMon = input[12];
    Diag_HndlrCEMon = input[13];
    Diag_HndlrVddMon = input[14];
    Diag_HndlrCspMon = input[15];
    Diag_HndlrFuncInhibitDiagSts = input[16];
    Diag_HndlrMtrDiagState = input[17];

    Diag_Hndlr();


    output[0] = Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty;
    output[1] = Diag_HndlrDiagVlvActrInfo.FlOvReqData;
    output[2] = Diag_HndlrDiagVlvActrInfo.FlIvReqData;
    output[3] = Diag_HndlrDiagVlvActrInfo.FrOvReqData;
    output[4] = Diag_HndlrDiagVlvActrInfo.FrIvReqData;
    output[5] = Diag_HndlrDiagVlvActrInfo.RlOvReqData;
    output[6] = Diag_HndlrDiagVlvActrInfo.RlIvReqData;
    output[7] = Diag_HndlrDiagVlvActrInfo.RrOvReqData;
    output[8] = Diag_HndlrDiagVlvActrInfo.RrIvReqData;
    output[9] = Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData;
    output[10] = Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData;
    output[11] = Diag_HndlrDiagVlvActrInfo.SimVlvReqData;
    output[12] = Diag_HndlrDiagVlvActrInfo.ResPVlvReqData;
    output[13] = Diag_HndlrDiagVlvActrInfo.BalVlvReqData;
    output[14] = Diag_HndlrDiagVlvActrInfo.CircVlvReqData;
    output[15] = Diag_HndlrDiagVlvActrInfo.PressDumpReqData;
    output[16] = Diag_HndlrDiagVlvActrInfo.RelsVlvReqData;
    output[17] = Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData;
    output[18] = Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData;
    output[19] = Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData;
    output[20] = Diag_HndlrMotReqDataDiagInfo.MotReq;
    output[21] = Diag_HndlrSasCalInfo.DiagSasCaltoAppl;
    output[22] = Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
    output[23] = Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
    output[24] = Diag_HndlrDiagHndlRequest.DiagRequest_Order;
    output[25] = Diag_HndlrDiagHndlRequest.DiagRequest_Iq;
    output[26] = Diag_HndlrDiagHndlRequest.DiagRequest_Id;
    output[27] = Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM;
    output[28] = Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM;
    output[29] = Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM;
    output[30] = Diag_HndlrDiagClrSrs;
    output[31] = Diag_HndlrDiagSci;
    output[32] = Diag_HndlrDiagAhbSci;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Diag_Hndlr_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
