/**
 * @defgroup Diag_Hndlr_Ifa Diag_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Diag_Hndlr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DIAG_HNDLR_IFA_H_
#define DIAG_HNDLR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Diag_Hndlr_Read_Diag_HndlrWhlSpdInfo(data) do \
{ \
    *data = Diag_HndlrWhlSpdInfo; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo(data) do \
{ \
    *data = Diag_HndlrIdbSnsrEolOfsCalcInfo; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Diag_HndlrWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Diag_HndlrWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Diag_HndlrWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Diag_HndlrWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk(data) do \
{ \
    *data = Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd(data) do \
{ \
    *data = Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk(data) do \
{ \
    *data = Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd(data) do \
{ \
    *data = Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrEcuModeSts(data) do \
{ \
    *data = Diag_HndlrEcuModeSts; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrIgnOnOffSts(data) do \
{ \
    *data = Diag_HndlrIgnOnOffSts; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrVBatt1Mon(data) do \
{ \
    *data = Diag_HndlrVBatt1Mon; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrVBatt2Mon(data) do \
{ \
    *data = Diag_HndlrVBatt2Mon; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrFspCbsMon(data) do \
{ \
    *data = Diag_HndlrFspCbsMon; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrCEMon(data) do \
{ \
    *data = Diag_HndlrCEMon; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrVddMon(data) do \
{ \
    *data = Diag_HndlrVddMon; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrCspMon(data) do \
{ \
    *data = Diag_HndlrCspMon; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrFuncInhibitDiagSts(data) do \
{ \
    *data = Diag_HndlrFuncInhibitDiagSts; \
}while(0);

#define Diag_Hndlr_Read_Diag_HndlrMtrDiagState(data) do \
{ \
    *data = Diag_HndlrMtrDiagState; \
}while(0);


/* Set Output DE MAcro Function */
#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrMotReqDataDiagInfo(data) do \
{ \
    Diag_HndlrMotReqDataDiagInfo = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrSasCalInfo(data) do \
{ \
    Diag_HndlrSasCalInfo = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo(data) do \
{ \
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest(data) do \
{ \
    Diag_HndlrDiagHndlRequest = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagClrSrs(data) do \
{ \
    Diag_HndlrDiagClrSrs = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagSci(data) do \
{ \
    Diag_HndlrDiagSci = *data; \
}while(0);

#define Diag_Hndlr_Write_Diag_HndlrDiagAhbSci(data) do \
{ \
    Diag_HndlrDiagAhbSci = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DIAG_HNDLR_IFA_H_ */
/** @} */
