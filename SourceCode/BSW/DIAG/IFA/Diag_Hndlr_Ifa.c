/**
 * @defgroup Diag_Hndlr_Ifa Diag_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Diag_Hndlr_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Diag_Hndlr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Diag_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define DIAG_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Diag_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/** Variable Section (32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DIAG_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Diag_MemMap.h"
#define DIAG_HNDLR_START_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/** Variable Section (32BIT)**/


#define DIAG_HNDLR_STOP_SEC_VAR_32BIT
#include "Diag_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DIAG_HNDLR_START_SEC_CODE
#include "Diag_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define DIAG_HNDLR_STOP_SEC_CODE
#include "Diag_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
