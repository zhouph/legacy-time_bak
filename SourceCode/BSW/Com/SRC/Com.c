/*
 * Com.c
 *
 *  Created on: 2014. 12. 24.
 *      Author: jinsu.park
 */


/*==============================================================================
 *                  INCLUDE FILES
==============================================================================*/
#include "Com.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
==============================================================================*/

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define COMMCAN_START_SEC_CONST_UNSPECIFIED
#include "Com_MemMap.h"
/** Constant Section (UNSPECIFIED)**/


#define COMMCAN_STOP_SEC_CONST_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_CONST_32BIT
#include "Com_MemMap.h"
/** Constant Section (32BIT)**/


#define COMMCAN_STOP_SEC_CONST_32BIT
#include "Com_MemMap.h"
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define COMMCAN_START_SEC_CONST_UNSPECIFIED
#include "Com_MemMap.h"
/** Constant Section (UNSPECIFIED)**/


#define COMMCAN_STOP_SEC_CONST_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_CONST_32BIT
#include "Com_MemMap.h"
/** Constant Section (32BIT)**/


#define COMMCAN_STOP_SEC_CONST_32BIT
#include "Com_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
==============================================================================*/
#define COMMCAN_START_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
/** Variable Section (UNSPECIFIED)**/

Can_PduType Can_Pdu;

#define COMMCAN_STOP_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_32BIT
#include "Com_MemMap.h"
/** Variable Section (32BIT)**/


#define COMMCAN_STOP_SEC_VAR_32BIT
#include "Com_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
==============================================================================*/
#define COMMCAN_START_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define COMMCAN_STOP_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_32BIT
#include "Com_MemMap.h"
/** Variable Section (32BIT)**/


#define COMMCAN_STOP_SEC_VAR_32BIT
#include "Com_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
==============================================================================*/
#define COMMCAN_START_SEC_CODE
#include "Com_MemMap.h"
static uint32 GetDataFromPDU(uint8 startbit, uint8 size, uint8* pduBuff);
static void Com_TxMainFunction(void);
static uint8 getHid(uint8 curr);
static Std_ReturnType updateMsgTimer(Com_FrameType *f);
static uint8 getMsgHit(Com_FrameType *f, Com_FrameConfigType *v);
static Std_ReturnType setMsgTimer(Com_FrameType *f, uint16 v);

static void Com_RxMsgFrameBufferCopy(Com_FrameIdType RxFrameID, const uint8 * ComSduPtr)
{
	uint8 MsgDlc = Com_FrameConfig[RxFrameID].MsgDlc;
	uint8 idx;
	for(idx=0;idx<MsgDlc;idx++)
    {
		Com_FrameBuf[RxFrameID].Data.B[idx] = (uint8_t)(ComSduPtr[idx]);
    }

}

static void Com_CheckSigData(uint8 BitLength, void* SignalDataPtr)
{
	uint8 leftShift = 32-BitLength;
	*(uint32*)SignalDataPtr = ((*(uint32*)SignalDataPtr << leftShift) >> leftShift);
}

static void Com_GetMask(uint8 startBit, uint8 BitLength, uint32* mask)
{
	uint8 endBit = startBit + BitLength;
	if((startBit<=31)&&(endBit >= 33))
	{
    	mask[0] = (0xffffffff >> (32 - startBit));
    	mask[1] = (0xffffffff << (endBit - 32));
	}
	else if(startBit>=32)
	{
		mask[0] = 0xffffffff;
		mask[1] = ~((0xffffffff<<(endBit - 32))^(0xffffffff<<(startBit - 32)));
	}
	else
	{
		mask[0] = ~((0xffffffff<<endBit)^(0xffffffff<<startBit));
		mask[1] = 0xffffffff;
	}
}
/*==============================================================================
 *                  GLOBAL FUNCTIONS
==============================================================================*/
void Can_Initialization(void)
{
	Can_17_MCanP_SetControllerMode(Can_17_MCanPConf_CanController_CanController_0, CAN_T_START); // �߰� 0813 PJS
	Can_17_MCanP_SetControllerMode(Can_17_MCanPConf_CanController_CanController_1, CAN_T_START); // �߰� 0813 PJS
}

void CanIf_RxIndication(Can_HwHandleType Hrh, Can_IdType CanId, uint8 CanDlc, const uint8 * CanSduPtr)
{
	uint16 i;
	Can_IdType RxPduId;
	PduInfoType PduInfo;
	RxPduId = CanId;

	for(i=0;i<COM_NUMBER_OF_RX_MSG;i++)
	{
		if((Com_FrameConfig[i].MsgId==CanId) /*&& (Com_FrameConfig[i].ComHrhId==Hrh)*/)
		{
			PduInfo.SduDataPtr = CanSduPtr;
			PduInfo.SduLength = CanDlc;

			Com_RxMsgFrameBufferCopy(i, CanSduPtr);
			Proxy_CanIfRxIndication(RxPduId,&PduInfo);
			return;
		}
	}
}
void Com_MainFunction(void)
{
	Com_TxMainFunction();
}
/*
#if defined(DEBUG)
#define PS2CONST	volitail
#else
PS2CONST	static
#endif
*/
static void Com_TxMainFunction(void)
{
	uint8 i;
	static uint8 hId;
	static uint8 endpos=COM_NUMBER_OF_RX_MSG+COM_NUMBER_OF_TX_MSG;
	static Can_ReturnType Retvalue;
	for(i=COM_NUMBER_OF_RX_MSG; i<endpos ;i++)
	{
		hId = getHid(i); /*warning offset*/
		if(updateMsgTimer(&Com_FrameBuf[hId])==E_NOT_OK) return;
		if(getMsgHit(&Com_FrameBuf[hId],&Com_FrameConfig[hId])==1&&(Com_FrameConfig[hId].MsgPeriod!=0))
		{
			if(setMsgTimer(&Com_FrameBuf[hId], 0)==E_NOT_OK) return;

	    		Can_Pdu.swPduHandle = i;
			Can_Pdu.id = Com_FrameConfig[hId].MsgId;
			Can_Pdu.length = Com_FrameConfig[hId].MsgDlc;
			Can_Pdu.sdu = (uint8_t*)&Com_FrameBuf[hId].Data;

			Retvalue=Can_17_MCanP_Write(Com_FrameConfig[hId].ComHrhId,  &Can_Pdu);
			}
		}
}

static uint8 getHid(uint8 curr)
{
	return (curr);
}

static Std_ReturnType updateMsgTimer(Com_FrameType *f)
{
	uint16 prev=0;
	uint16 curr=0;

	prev = f->PrevMsgTimer;
	curr = prev + 5;
	f->MsgTimer = curr;
	f->PrevMsgTimer = curr;
	if(curr <= prev) return E_NOT_OK;
	else return E_OK;
}
static Std_ReturnType setMsgTimer(Com_FrameType *f, uint16 v)
{
	f->MsgTimer=v;
	f->PrevMsgTimer=v;
	if(f->MsgTimer != v) return E_NOT_OK;
	else return E_OK;
}
static uint8 getMsgHit(Com_FrameType *f, Com_FrameConfigType *v)
{
	uint8 hit=0;

	if(f->MsgTimer >= v->MsgPeriod) hit=1;
	else hit=0;

	return hit;
	}
Std_ReturnType getPduDeExplicit(Com_SigConfigType *s, void* ptr,Com_SignalDataType *De)
{
	Std_ReturnType ret=E_OK;

	*De=0;
	if(s->DataType == DataType8bit) {*De=*(uint8*)ptr; return E_OK;}
	if(s->DataType == DataType16bit) {*De=*(uint16*)ptr; return E_OK;}
	if(s->DataType == DataType32bit) {*De=*(uint32*)ptr; return E_OK;}

	return E_NOT_OK;
}
Std_ReturnType setPduDe2AppDe(Com_SignalDataType rDe, uint8 dlc, Com_SignalDataType *wDe)
{
	uint32 appmask=0;
	appmask = ((0xffffffff >> dlc) << dlc);
	*wDe = (appmask & rDe);
	return E_OK;
}

uint8 getStartNibble(uint8 startBit)
{
	if(startBit < 32 )	return 0;
	return 1;
}


Std_ReturnType Com_ReceiveSignal(Com_SignalIdType SignalId, void* SignalDataPtr)
{
	Com_FrameIdType frameId = Com_SigConfig[SignalId].FrameId;
    uint8 startBit = Com_SigConfig[SignalId].StartBit;
    uint8 bitLength = Com_SigConfig[SignalId].BitLength;
    uint8 DataLength =  Com_SigConfig[SignalId].DataType;
    Com_FrameDataType* frameDataPtr = &Com_FrameBuf[frameId].Data;

    uint32 TempSignalData;
    TempSignalData = GetDataFromPDU(startBit, bitLength, Com_FrameBuf[frameId].Data.B);

    switch(Com_SigConfig[SignalId].DataType)
    {
        case DataType8bit:
        	*(uint8*)SignalDataPtr = TempSignalData;
                break;
        case DataType16bit:
        	*(uint16*)SignalDataPtr = TempSignalData;
                break;
        case DataType32bit:
        	*(uint32*)SignalDataPtr = TempSignalData;
                break;
    }
	return E_OK;
}

Std_ReturnType Com_SendSignal(Com_SignalIdType SignalId, void* SignalDataPtr)
{
	Com_FrameIdType frameId = Com_SigConfig[SignalId].FrameId;
    uint8 startBit = Com_SigConfig[SignalId].StartBit;
    uint8 bitLength = Com_SigConfig[SignalId].BitLength;
    Com_FrameDataType* frameDataPtr = &Com_FrameBuf[frameId].Data;
    Com_SignalDataType CheckSigData;
    switch(Com_SigConfig[SignalId].DataType)
    {
        case DataType8bit:
                CheckSigData = *(uint8*)SignalDataPtr;
                break;
        case DataType16bit:
                CheckSigData = *(uint16*)SignalDataPtr;
                break;
        case DataType32bit:
                CheckSigData = *(uint32*)SignalDataPtr;
                break;
    }
    uint32 mask[2];
    Com_CheckSigData(bitLength, &CheckSigData);
    Com_GetMask(startBit, bitLength, mask);
    if(startBit < 32)
    {
    	frameDataPtr->W[0] = (frameDataPtr->W[0] & mask[0])+(CheckSigData<<startBit);
    	frameDataPtr->W[1] = (frameDataPtr->W[1] & mask[1])+(CheckSigData>>(32-startBit));
    }
    else
    {
    	frameDataPtr->W[0] = (frameDataPtr->W[0] & mask[0]);
    	frameDataPtr->W[1] = (frameDataPtr->W[1] & mask[1])+(CheckSigData<<(startBit-32));
    }

	return E_OK;
}


boolean	Can_ReceiveCallOut (uint8 Hrh, Can_IdType CanId, uint8 CanDlc, const uint8 *CanSduPtr)
{
	return(1);
}

static uint32 GetDataFromPDU(uint8 startbit, uint8 size, uint8* pduBuff)
{
        uint32 value = 0;
        uint8 byteStart = 0;   // starting byte
        uint8 bitStart = 0;    // starting bit
        sint8 step = 0;                       // index for byte step (big-endian:1/ little-endian:-1)
        uint8 mask;

    startbit =     startbit + size - 1;

        byteStart = (startbit / 8U);
        bitStart = (startbit % 8U);
    step = -1; // setting for little-endian

        /* head */
    if (bitStart != 7U)
        {
           uint8 space = bitStart + 1;
           mask = ((uint8) 0xFFU) >> (7U-bitStart);
           value = (uint32) (pduBuff[byteStart] & mask);

           if (space >= size)
               {
                   value >>= (space - size);
                   size = 0U;
               }
               else
               {
                   byteStart = byteStart + step;
                   size -= space;
               }
        }

    /* body */
        while (size > 7U)
        {
               value <<= 8U;
               value += pduBuff[byteStart];
               size -= 8U;
               byteStart = byteStart + step;
        }

        /* tail */
        if (size > 0U)
        {
               value <<= size;
     value += (uint8) (pduBuff[byteStart] >> (8U - size));
        }

        return value;

}


#define COMMCAN_STOP_SEC_CODE
#include "Com_MemMap.h"
