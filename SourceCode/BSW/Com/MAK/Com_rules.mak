# \file
#
# \brief Com
#
# This file contains the implementation of the BSW
# module Com.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Com_src

Com_src_FILES       += $(Com_SRC_PATH)\Com.c
Com_src_FILES       += $(Com_CFG_PATH)\Com_Cfg.c





#################################################################
