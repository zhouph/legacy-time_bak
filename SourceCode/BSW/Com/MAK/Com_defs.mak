# \file
#
# \brief Com
#
# This file contains the implementation of the BSW
# module Com.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

Com_CORE_PATH     := $(MANDO_BSW_ROOT)\Com
Com_SRC_PATH      := $(Com_CORE_PATH)\SRC
Com_CFG_PATH      := $(Com_CORE_PATH)\CFG\$(Com_VARIANT)
Com_HDR_PATH      := $(Com_CORE_PATH)\HDR
Com_CMN_PATH      := $(Com_CORE_PATH)\CMN
Com_IFA_PATH      := $(Com_CORE_PATH)\IFA

CC_INCLUDE_PATH    += $(Com_CFG_PATH)
CC_INCLUDE_PATH    += $(Com_HDR_PATH)
CC_INCLUDE_PATH    += $(Com_CMN_PATH)
CC_INCLUDE_PATH    += $(Com_IFA_PATH)


