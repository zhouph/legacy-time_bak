/*
 * Com_Cfg.h
 *
 *  Created on: 2014. 12. 24.
 *      Author: jinsu.park
 */

#ifndef COM_CFG_H_
#define COM_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
===========================================================================*/
#include "Com_Types.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

#define COM_SIG_RX_EMS1_FUECUT					(Com_SignalIdType) 0
#define COM_SIG_RX_EMS1_FUECINH					(Com_SignalIdType) 1
#define COM_SIG_RX_EMS1_TQSTD_NM				(Com_SignalIdType) 2
#define COM_SIG_RX_EMS1_ACTINDTQ_PC				(Com_SignalIdType) 3
#define COM_SIG_RX_EMS1_ENGSPD_RPM				(Com_SignalIdType) 4
#define COM_SIG_RX_EMS1_INDTQ_PC				(Com_SignalIdType) 5
#define COM_SIG_RX_EMS1_FRICTTQ_PC				(Com_SignalIdType) 6
#define COM_SIG_RX_EMS1_VEHSPD_KMH				(Com_SignalIdType) 7
#define COM_SIG_RX_EMS1_MSGCHKSUM1				(Com_SignalIdType) 8

#define COM_SIG_RX_EMS2_FRTCMBCMP				(Com_SignalIdType) 9
#define COM_SIG_RX_EMS2_ENGSTAT					(Com_SignalIdType) 10
#define COM_SIG_RX_EMS2_FULLLOAD				(Com_SignalIdType) 11
#define COM_SIG_RX_EMS2_DC						(Com_SignalIdType) 12
#define COM_SIG_RX_EMS2_WARMUPCYC				(Com_SignalIdType) 13
#define COM_SIG_RX_EMS2_RBM_DC					(Com_SignalIdType) 14
#define COM_SIG_RX_EMS2_IDLTGTSPD_RPM			(Com_SignalIdType) 15
#define COM_SIG_RX_EMS2_OBDFRFACK				(Com_SignalIdType) 16
#define COM_SIG_RX_EMS2_O2_PHST					(Com_SignalIdType) 17
#define COM_SIG_RX_EMS2_ENGSPDERR				(Com_SignalIdType) 18
#define COM_SIG_RX_EMS2_FUECON_UL				(Com_SignalIdType) 19
#define COM_SIG_RX_EMS2_TPS_PC					(Com_SignalIdType) 20
#define COM_SIG_RX_EMS2_ACCPEDDEP_PC			(Com_SignalIdType) 21

#define COM_SIG_RX_EMS3_VBOFFACT				(Com_SignalIdType) 22
#define COM_SIG_RX_EMS3_ACNCOMPOFFREQ			(Com_SignalIdType) 23
#define COM_SIG_RX_EMS3_SOAKTERR				(Com_SignalIdType) 24
#define COM_SIG_RX_EMS3_CONFMILFMEMMNG			(Com_SignalIdType) 25
#define COM_SIG_RX_EMS3_PGMRUN3					(Com_SignalIdType) 26
#define COM_SIG_RX_EMS3_ENGCOLTEMP_C			(Com_SignalIdType) 27
#define COM_SIG_RX_EMS3_IMMOLAMP				(Com_SignalIdType) 28
#define COM_SIG_RX_EMS3_WIPER					(Com_SignalIdType) 29
#define COM_SIG_RX_EMS3_FANONF					(Com_SignalIdType) 30
#define COM_SIG_RX_EMS3_FANSTAT					(Com_SignalIdType) 31
#define COM_SIG_RX_EMS3_AAFOPENREQ				(Com_SignalIdType) 32
#define COM_SIG_RX_EMS3_IGCYCCTR				(Com_SignalIdType) 33
#define COM_SIG_RX_EMS3_SOAKT_MIN				(Com_SignalIdType) 34
#define COM_SIG_RX_EMS3_BRKFORACT				(Com_SignalIdType) 35
#define COM_SIG_RX_EMS3_TEMPFUELRUNOUT			(Com_SignalIdType) 36
#define COM_SIG_RX_EMS3_SOAKT480OVER			(Com_SignalIdType) 37
#define COM_SIG_RX_EMS3_FUELRUNOUT				(Com_SignalIdType) 38
#define COM_SIG_RX_EMS3_ENGCOLTEMPSUB_C			(Com_SignalIdType) 39

#define COM_SIG_RX_TCU1_TARGE					(Com_SignalIdType) 40
#define COM_SIG_RX_TCU1_RDY						(Com_SignalIdType) 41
#define COM_SIG_RX_TCU1_GARCHANGE				(Com_SignalIdType) 42
#define COM_SIG_RX_TCU1_FLT						(Com_SignalIdType) 43
#define COM_SIG_RX_TCU1_GARSELDISP				(Com_SignalIdType) 44
#define COM_SIG_RX_TCU1_FANCTRLREQ				(Com_SignalIdType) 45
#define COM_SIG_RX_TCU1_TQREDREQ_PC				(Com_SignalIdType) 46
#define COM_SIG_RX_TCU1_TQREDREQSLW_PC			(Com_SignalIdType) 47
#define COM_SIG_RX_TCU1_VEHSPD_KPH				(Com_SignalIdType) 48
#define COM_SIG_RX_TCU1_VEHSPDDEC_KPH			(Com_SignalIdType) 49
#define COM_SIG_RX_TCU1_OPUSPDCMD_RPM			(Com_SignalIdType) 50
#define COM_SIG_RX_TCU1_TQINCREQ_PC				(Com_SignalIdType) 51

#define COM_SIG_RX_TCU5_TYP						(Com_SignalIdType) 52
#define COM_SIG_RX_TCU5_GEARTYP					(Com_SignalIdType) 53
#define COM_SIG_RX_TCU5_OPUPWRSAVREQ			(Com_SignalIdType) 54
#define COM_SIG_RX_TCU5_ATFLUTEMP_C				(Com_SignalIdType) 55
#define COM_SIG_RX_TCU5_SERIVCELAMP				(Com_SignalIdType) 56
#define COM_SIG_RX_TCU5_MILREQ					(Com_SignalIdType) 57
#define COM_SIG_RX_TCU5_LMPSLPLMTSTS			(Com_SignalIdType) 58
#define COM_SIG_RX_TCU5_OPUSTAT					(Com_SignalIdType) 59
#define COM_SIG_RX_TCU5_TEMPTFSTS				(Com_SignalIdType) 60
#define COM_SIG_RX_TCU5_TMOUTSPD_RPM			(Com_SignalIdType) 61

#define COM_SIG_RX_TCU6_CURPROGRAM				(Com_SignalIdType) 62
#define COM_SIG_RX_TCU6_SHIFTCLASS_CCAN			(Com_SignalIdType) 63
#define COM_SIG_RX_TCU6_CLTLIMACT_FS			(Com_SignalIdType) 64
#define COM_SIG_RX_TCU6_REQIDLESPD_RPM_FS		(Com_SignalIdType) 65
#define COM_SIG_RX_TCU6_ADDTQLOSS_PC_FS			(Com_SignalIdType) 66
#define COM_SIG_RX_TCU6_ENGTQRED_PC_FS			(Com_SignalIdType) 67

#define COM_SIG_RX_HCU1_INJENB					(Com_SignalIdType) 68
#define COM_SIG_RX_HCU1_ECUWARMUPCTRREQ			(Com_SignalIdType) 69
#define COM_SIG_RX_HCU1_ENGTQGENDCS				(Com_SignalIdType) 70
#define COM_SIG_RX_HCU1_MILREQ					(Com_SignalIdType) 71
#define COM_SIG_RX_HCU1_TQINTSTAT				(Com_SignalIdType) 72
#define COM_SIG_RX_HCU1_ENGCLTSTAT				(Com_SignalIdType) 73
#define COM_SIG_RX_HCU1_ENGTQCMDBINV_PC			(Com_SignalIdType) 74
#define COM_SIG_RX_HCU1_ENGTQCMD_PC				(Com_SignalIdType) 75
#define COM_SIG_RX_HCU1_REQSPDUP_RPM			(Com_SignalIdType) 76
#define COM_SIG_RX_HCU1_PTOPERMODEFORECU		(Com_SignalIdType) 77
#define COM_SIG_RX_HCU1_ECUANTIJERKINH			(Com_SignalIdType) 78
#define COM_SIG_RX_HCU1_ENGCLTCLASS				(Com_SignalIdType) 79
#define COM_SIG_RX_HCU1_VEH_ST					(Com_SignalIdType) 80
#define COM_SIG_RX_HCU1_CLTCTRPHASE				(Com_SignalIdType) 81
#define COM_SIG_RX_HCU1_TQINTMOD				(Com_SignalIdType) 82
#define COM_SIG_RX_HCU1_RDY						(Com_SignalIdType) 83
#define COM_SIG_RX_HCU1_HEVRDY					(Com_SignalIdType) 84
#define COM_SIG_RX_HCU1_HEVRDYCLU				(Com_SignalIdType) 85
#define COM_SIG_RX_HCU1_ENGTRANSCTLSTAT			(Com_SignalIdType) 86
#define COM_SIG_RX_HCU1_PGMRUN1					(Com_SignalIdType) 87
#define COM_SIG_RX_HCU1_SLWDWNSTAT				(Com_SignalIdType) 88
#define COM_SIG_RX_HCU1_STINHSTAT				(Com_SignalIdType) 89
#define COM_SIG_RX_HCU1_MSGCHKSUM1				(Com_SignalIdType) 90

#define COM_SIG_RX_HCU2_REGENENA				(Com_SignalIdType) 91
#define COM_SIG_RX_HCU2_TQREDSTAT				(Com_SignalIdType) 92
#define COM_SIG_RX_HCU2_TQINCSTAT				(Com_SignalIdType) 93
#define COM_SIG_RX_HCU2_SERIVCEMOD				(Com_SignalIdType) 94
#define COM_SIG_RX_HCU2_PGMRUN2					(Com_SignalIdType) 95
#define COM_SIG_RX_HCU2_CRPTQ_NM				(Com_SignalIdType) 96
#define COM_SIG_RX_HCU2_WHLDEMTQ_NM				(Com_SignalIdType) 97
#define COM_SIG_RX_HCU2_REGENBRKTQ_NM			(Com_SignalIdType) 98
#define COM_SIG_RX_HCU2_MSGCHKSUM2				(Com_SignalIdType) 99

#define COM_SIG_RX_HCU3_EWPDIAGENA				(Com_SignalIdType) 100
#define COM_SIG_RX_HCU3_MOTPWRENA				(Com_SignalIdType) 101
#define COM_SIG_RX_HCU3_GENPWRENA				(Com_SignalIdType) 102
#define COM_SIG_RX_HCU3_MOTCTLMOD				(Com_SignalIdType) 103
#define COM_SIG_RX_HCU3_ISGCTLMOD				(Com_SignalIdType) 104
#define COM_SIG_RX_HCU3_PGMRUN3					(Com_SignalIdType) 105
#define COM_SIG_RX_HCU3_MCUANTIJERKINH			(Com_SignalIdType) 106
#define COM_SIG_RX_HCU3_MOTTQCMC_PC				(Com_SignalIdType) 107
#define COM_SIG_RX_HCU3_MOTSPDCMD_RPM			(Com_SignalIdType) 108
#define COM_SIG_RX_HCU3_MOTTQCMDBINV_PC			(Com_SignalIdType) 109
#define COM_SIG_RX_HCU3_TMINTQCMDBINV_PC		(Com_SignalIdType) 110
#define COM_SIG_RX_HCU3_LIMPOFF					(Com_SignalIdType) 111
#define COM_SIG_RX_HCU3_CHGREQ					(Com_SignalIdType) 112
#define COM_SIG_RX_HCU3_STARTINH				(Com_SignalIdType) 113
#define COM_SIG_RX_HCU3_MSGCHKSUM3				(Com_SignalIdType) 114

#define COM_SIG_RX_HCU5_ACNOPENPERM				(Com_SignalIdType) 115
#define COM_SIG_RX_HCU5_BLROFFRQ				(Com_SignalIdType) 116
#define COM_SIG_RX_HCU5_COOLSTAT				(Com_SignalIdType) 117
#define COM_SIG_RX_HCU5_SERLAMP					(Com_SignalIdType) 118
#define COM_SIG_RX_HCU5_ECOSCO					(Com_SignalIdType) 119
#define COM_SIG_RX_HCU5_COMPERM_W				(Com_SignalIdType) 120
#define COM_SIG_RX_HCU5_FUELECO_MPG				(Com_SignalIdType) 121
#define COM_SIG_RX_HCU5_ECOLVL					(Com_SignalIdType) 122
#define COM_SIG_RX_HCU5_BLUEMODE				(Com_SignalIdType) 123
#define COM_SIG_RX_HCU5_CRUONLAMP				(Com_SignalIdType) 124
#define COM_SIG_RX_HCU5_CRUSETLAMP				(Com_SignalIdType) 125
#define COM_SIG_RX_HCU5_THIENGTIR				(Com_SignalIdType) 126
#define COM_SIG_RX_HCU5_THIBATTIR				(Com_SignalIdType) 127
#define COM_SIG_RX_HCU5_THIENGBAT				(Com_SignalIdType) 128
#define COM_SIG_RX_HCU5_MOTCHA					(Com_SignalIdType) 129
#define COM_SIG_RX_HCU5_ENGBRK					(Com_SignalIdType) 130
#define COM_SIG_RX_HCU5_POWRAT_PC				(Com_SignalIdType) 131
#define COM_SIG_RX_HCU5_LDCVREF_V				(Com_SignalIdType) 132
#define COM_SIG_RX_HCU5_LDCINH					(Com_SignalIdType) 133
#define COM_SIG_RX_HCU5_LDCUVC					(Com_SignalIdType) 134
#define COM_SIG_RX_HCU5_HEVMOD					(Com_SignalIdType) 135
#define COM_SIG_RX_HCU5_PGMRUN5					(Com_SignalIdType) 136

#define COM_SIG_RX_MCU1_MOTESTTQ_PC				(Com_SignalIdType) 137
#define COM_SIG_RX_MCU1_PGMRUN1					(Com_SignalIdType) 138
#define COM_SIG_RX_MCU1_MOTACTROTSPD_RPM		(Com_SignalIdType) 139
#define COM_SIG_RX_MCU1_MOTDEFACT_PC			(Com_SignalIdType) 140
#define COM_SIG_RX_MCU1_MOTTQUPLMTM_PC			(Com_SignalIdType) 141
#define COM_SIG_RX_MCU1_MOTTQUPLMTG_PC			(Com_SignalIdType) 142
#define COM_SIG_RX_MCU1_MSGCHKSUM1				(Com_SignalIdType) 143

#define COM_SIG_RX_MCU2_FLT						(Com_SignalIdType) 144
#define COM_SIG_RX_MCU2_WRN						(Com_SignalIdType) 145
#define COM_SIG_RX_MCU2_ACTTEST					(Com_SignalIdType) 146
#define COM_SIG_RX_MCU2_MILREQ					(Com_SignalIdType) 147
#define COM_SIG_RX_MCU2_SERVICELAMP				(Com_SignalIdType) 148
#define COM_SIG_RX_MCU2_INVRDY					(Com_SignalIdType) 149
#define COM_SIG_RX_MCU2_INVCTRABLE				(Com_SignalIdType) 150
#define COM_SIG_RX_MCU2_RELAYOFFREQ				(Com_SignalIdType) 151
#define COM_SIG_RX_MCU2_MOTRADFANONFREQ			(Com_SignalIdType) 152
#define COM_SIG_RX_MCU2_AJACTIVATE				(Com_SignalIdType) 153
#define COM_SIG_RX_MCU2_ENGRPMLMT				(Com_SignalIdType) 154
#define COM_SIG_RX_MCU2_GCU_ENGRPMLMT			(Com_SignalIdType) 155
#define COM_SIG_RX_MCU2_EWPENABLE				(Com_SignalIdType) 156
#define COM_SIG_RX_MCU2_RLYOFFREQDELAYED		(Com_SignalIdType) 157
#define COM_SIG_RX_MCU2_GCU_RLYOFFREQDELAYED	(Com_SignalIdType) 158
#define COM_SIG_RX_MCU2_GCU_FLT					(Com_SignalIdType) 159
#define COM_SIG_RX_MCU2_GCU_WRN					(Com_SignalIdType) 160
#define COM_SIG_RX_MCU2_GCU_ACTTEST				(Com_SignalIdType) 161
#define COM_SIG_RX_MCU2_GCU_MILREQ				(Com_SignalIdType) 162
#define COM_SIG_RX_MCU2_GCU_SERVICELAMP			(Com_SignalIdType) 163
#define COM_SIG_RX_MCU2_GCU_INVRDY				(Com_SignalIdType) 164
#define COM_SIG_RX_MCU2_GCU_INVCTRABLE			(Com_SignalIdType) 165
#define COM_SIG_RX_MCU2_GCU_RELAYOFFREQ			(Com_SignalIdType) 166
#define COM_SIG_RX_MCU2_INVTEMP_C				(Com_SignalIdType) 167
#define COM_SIG_RX_MCU2_MOTTEMP_C				(Com_SignalIdType) 168
#define COM_SIG_RX_MCU2_GCU_INVTEMP_C			(Com_SignalIdType) 169
#define COM_SIG_RX_MCU2_GCU_HSGTEMP_C			(Com_SignalIdType) 170
#define COM_SIG_RX_MCU2_MSGCHKSUM2				(Com_SignalIdType) 171

#define COM_SIG_RX_BMS1_BATCHALMT_W				(Com_SignalIdType) 172
#define COM_SIG_RX_BMS1_BATDCHLMT_W				(Com_SignalIdType) 173
#define COM_SIG_RX_BMS1_OVERCHALMT				(Com_SignalIdType) 174
#define COM_SIG_RX_BMS1_SERVICELAMP				(Com_SignalIdType) 175
#define COM_SIG_RX_BMS1_MAINRLYONSTAT			(Com_SignalIdType) 176
#define COM_SIG_RX_BMS1_RLYOFF_BYREQ			(Com_SignalIdType) 177
#define COM_SIG_RX_BMS1_FLT						(Com_SignalIdType) 178
#define COM_SIG_RX_BMS1_WRN						(Com_SignalIdType) 179
#define COM_SIG_RX_BMS1_CTRB					(Com_SignalIdType) 180
#define COM_SIG_RX_BMS1_RDY						(Com_SignalIdType) 181
#define COM_SIG_RX_BMS1_SOC_PC					(Com_SignalIdType) 182
#define COM_SIG_RX_BMS1_AUXTEMP_C				(Com_SignalIdType) 183
#define COM_SIG_RX_BMS1_MILREQ					(Com_SignalIdType) 184
#define COM_SIG_RX_BMS1_BATCOOLPWRPREM			(Com_SignalIdType) 185
#define COM_SIG_RX_BMS1_MAINRLYOPERTMP			(Com_SignalIdType) 186
#define COM_SIG_RX_BMS1_RLYWELD					(Com_SignalIdType) 187

#define COM_SIG_RX_SAS1_ANGLE					(Com_SignalIdType) 188
#define COM_SIG_RX_SAS1_SPEED					(Com_SignalIdType) 189
#define COM_SIG_RX_SAS1_OK						(Com_SignalIdType) 190
#define COM_SIG_RX_SAS1_CAL						(Com_SignalIdType) 191
#define COM_SIG_RX_SAS1_TRIM					(Com_SignalIdType) 192
#define COM_SIG_RX_SAS1_SF						(Com_SignalIdType) 193
#define COM_SIG_RX_SAS1_MSGCOUNT				(Com_SignalIdType) 194
#define COM_SIG_RX_SAS1_CHECKSUM				(Com_SignalIdType) 195

#define COM_SIG_RX_FATC1_ACNREQSWI				(Com_SignalIdType) 196
#define COM_SIG_RX_FATC1_BLRON					(Com_SignalIdType) 197
#define COM_SIG_RX_FATC1_ENGONREQ				(Com_SignalIdType) 198
#define COM_SIG_RX_FATC1_HVACPMPOPERSTAT		(Com_SignalIdType) 199
#define COM_SIG_RX_FATC1_ACNCOMFLT				(Com_SignalIdType) 200
#define COM_SIG_RX_FATC1_COMCANFLT				(Com_SignalIdType) 201
#define COM_SIG_RX_FATC1_REARDEF				(Com_SignalIdType) 202
#define COM_SIG_RX_FATC1_BLWRMAX				(Com_SignalIdType) 203
#define COM_SIG_RX_FATC1_ACNCOMREQ_W			(Com_SignalIdType) 204
#define COM_SIG_RX_FATC1_ACNCOMMAXREQ_W			(Com_SignalIdType) 205
#define COM_SIG_RX_FATC1_ACNCOMCST_W			(Com_SignalIdType) 206
#define COM_SIG_RX_FATC1_MSGCNT					(Com_SignalIdType) 207
#define COM_SIG_RX_FATC1_OUTTEMP_SNR_C			(Com_SignalIdType) 208
#define COM_SIG_RX_FATC1_CHKSUM					(Com_SignalIdType) 209

#define COM_SIG_RX_CLU1_CRUISESWSTATUS			(Com_SignalIdType) 210
#define COM_SIG_RX_CLU1_CF_CLU_PARITYBIT1       (Com_SignalIdType) 211
#define COM_SIG_RX_CLU1_SPEED_UNIT              (Com_SignalIdType) 212
#define COM_SIG_RX_CLU1_P_BRAKE_ACT             (Com_SignalIdType) 213
#define COM_SIG_RX_CLU1_VANZ                    (Com_SignalIdType) 214
#define COM_SIG_RX_CLU1_ALIVECOUNTERCLU1        (Com_SignalIdType) 215
#define COM_SIG_RX_CLU1_CRUISESW_MAIN           (Com_SignalIdType) 216
#define COM_SIG_RX_CLU1_DASHACCFAIL             (Com_SignalIdType) 217
#define COM_SIG_RX_CLU1_DLSFAIL                 (Com_SignalIdType) 218
#define COM_SIG_RX_CLU1_CF_CLU_BRAKEFLUIDSW     (Com_SignalIdType) 219
#define COM_SIG_RX_CLU1_CF_STRRLYSTAT           (Com_SignalIdType) 220
#define COM_SIG_RX_CLU1_CF_BUTSYSVARIND         (Com_SignalIdType) 221
#define COM_SIG_RX_CLU1_R_TQACNOUTC             (Com_SignalIdType) 222
#define COM_SIG_RX_CLU1_ODOMETER                (Com_SignalIdType) 223

#define COM_SIG_RX_CLU2_IGN_SW              	(Com_SignalIdType) 224
#define COM_SIG_RX_CLU2_RKE_CMD             	(Com_SignalIdType) 225
#define COM_SIG_RX_CLU2_DRV_DR_SW               (Com_SignalIdType) 226
#define COM_SIG_RX_CLU2_DRV_KEYLOCK             (Com_SignalIdType) 227
#define COM_SIG_RX_CLU2_DRV_KEYUNLOCK           (Com_SignalIdType) 228
#define COM_SIG_RX_CLU2_PIC_LOCK                (Com_SignalIdType) 229
#define COM_SIG_RX_CLU2_PIC_UNLOCK              (Com_SignalIdType) 230
#define COM_SIG_RX_CLU2_DRV_SEAT_BELT           (Com_SignalIdType) 231
#define COM_SIG_RX_CLU2_TRUNK_OPEN_STATUS       (Com_SignalIdType) 232
#define COM_SIG_RX_CLU2_PAS_SEAT_BELT           (Com_SignalIdType) 233
#define COM_SIG_RX_CLU2_CF_HOODSTAT             (Com_SignalIdType) 234
#define COM_SIG_RX_CLU2_CF_CLU_TUMSIGLH         (Com_SignalIdType) 235
#define COM_SIG_RX_CLU2_CF_CLU_TUMSIGRH         (Com_SignalIdType) 236
#define COM_SIG_RX_CLU2_CF_CLU_LDWSSW           (Com_SignalIdType) 237
#define COM_SIG_RX_CLU2_CF_CLU_WIPERINTT        (Com_SignalIdType) 238
#define COM_SIG_RX_CLU2_CF_CLU_WIPERINTSW       (Com_SignalIdType) 239
#define COM_SIG_RX_CLU2_CF_CLU_WIPERLOW         (Com_SignalIdType) 240
#define COM_SIG_RX_CLU2_CF_CLU_WIPERHIGH        (Com_SignalIdType) 241
#define COM_SIG_RX_CLU2_CF_CLU_WIPERAUTO        (Com_SignalIdType) 242
#define COM_SIG_RX_CLU2_CF_CLU_RAINSNSSTAT      (Com_SignalIdType) 243
#define COM_SIG_RX_CLU2_CF_CLU_GEARRSTAT        (Com_SignalIdType) 244
#define COM_SIG_RX_CLU2_HS_STAT             	(Com_SignalIdType) 245
#define COM_SIG_RX_CLU2_CF_CLU_ACTIVEECOSW      (Com_SignalIdType) 246

#define COM_SIG_RX_YAWACC_ROLLINGCOUNTER        (Com_SignalIdType) 247
#define COM_SIG_RX_YAWACC_LATACCSELFTESTSTATUS  (Com_SignalIdType) 248
#define COM_SIG_RX_YAWACC_YAWRATESELFTESTSTATUS (Com_SignalIdType) 249
#define COM_SIG_RX_YAWACC_LATACCVALIDDATA     	(Com_SignalIdType) 250
#define COM_SIG_RX_YAWACC_YAWRATEVAILDDATA      (Com_SignalIdType) 251
#define COM_SIG_RX_YAWACC_YAWRATESIGNAL_0       (Com_SignalIdType) 252
#define COM_SIG_RX_YAWACC_ACCELERATORRATESIG_0  (Com_SignalIdType) 253
#define COM_SIG_RX_YAWACC_YAWRATESIGNAL_1       (Com_SignalIdType) 254
#define COM_SIG_RX_YAWACC_ACCELERATORRATESIG_1  (Com_SignalIdType) 255
#define COM_SIG_RX_YAWACC_SENSOROSCFREQDEV      (Com_SignalIdType) 256
#define COM_SIG_RX_YAWACC_AD_FAIL           	(Com_SignalIdType) 257
#define COM_SIG_RX_YAWACC_ROM_FAIL          	(Com_SignalIdType) 258
#define COM_SIG_RX_YAWACC_RAM_FAIL          	(Com_SignalIdType) 259
#define COM_SIG_RX_YAWACC_ACCEL_FAIL            (Com_SignalIdType) 260
#define COM_SIG_RX_YAWACC_ASIC_FAIL         	(Com_SignalIdType) 261
#define COM_SIG_RX_YAWACC_BATT_RANGE_ERR        (Com_SignalIdType) 262
#define COM_SIG_RX_YAWACC_EEP_FAIL          	(Com_SignalIdType) 263
#define COM_SIG_RX_YAWACC_GYRO_FAIL         	(Com_SignalIdType) 264
#define COM_SIG_RX_YAWACC_CAN_FUNC_ERR         	(Com_SignalIdType) 265
#define COM_SIG_RX_YAWACC_PLAUS_ERR_PST         (Com_SignalIdType) 266
#define COM_SIG_RX_YAWACC_RASTER_FAIL           (Com_SignalIdType) 267
#define COM_SIG_RX_YAWACC_WATCHDOG_RST          (Com_SignalIdType) 268
#define COM_SIG_RX_YAWACC_OSC_FAIL         		(Com_SignalIdType) 269

#define COM_SIG_RX_LONGACC_LONGROLLINGCOUNTER   (Com_SignalIdType) 270
#define COM_SIG_RX_LONGACC_LONGACCINVALIDDATA   (Com_SignalIdType) 271
#define COM_SIG_RX_LONGACC_INTSENFLTSYMTMACTIVE (Com_SignalIdType) 272
#define COM_SIG_RX_LONGACC_LONGACCSELFTSTSTATUS (Com_SignalIdType) 273
#define COM_SIG_RX_LONGACC_INTSENFAULTPRESENT   (Com_SignalIdType) 274
#define COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_0  (Com_SignalIdType) 275
#define COM_SIG_RX_LONGACC_LONGACCSENCIRERRPRE  (Com_SignalIdType) 276
#define COM_SIG_RX_LONGACC_INTTEMPSENSORFAULT   (Com_SignalIdType) 277
#define COM_SIG_RX_LONGACC_LONACSENRANCHKERRPRE	(Com_SignalIdType) 278
#define COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_1  (Com_SignalIdType) 279

#define COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_0     (Com_SignalIdType) 280
#define COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_1     (Com_SignalIdType) 281
#define COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_2     (Com_SignalIdType) 282
#define COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_3     (Com_SignalIdType) 283
#define COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_4     (Com_SignalIdType) 284
#define COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_5     (Com_SignalIdType) 285

#define COM_SIG_TX_TCS1_CF_BRK_TCSREQ           (Com_SignalIdType) 286
#define COM_SIG_TX_TCS1_CF_BRK_TCSPAS           (Com_SignalIdType) 287
#define COM_SIG_TX_TCS1_CF_BRK_TCSDEF           (Com_SignalIdType) 288
#define COM_SIG_TX_TCS1_CF_BRK_TCSCTL           (Com_SignalIdType) 289
#define COM_SIG_TX_TCS1_CF_BRK_ABSACT           (Com_SignalIdType) 290
#define COM_SIG_TX_TCS1_CF_BRK_ABSDEF           (Com_SignalIdType) 291
#define COM_SIG_TX_TCS1_CF_BRK_EBDDEF           (Com_SignalIdType) 292
#define COM_SIG_TX_TCS1_CF_BRK_TCSGSC           (Com_SignalIdType) 293
#define COM_SIG_TX_TCS1_CF_BRK_ESPPAS           (Com_SignalIdType) 294
#define COM_SIG_TX_TCS1_CF_BRK_ESPDEF           (Com_SignalIdType) 295
#define COM_SIG_TX_TCS1_HAC_CTL             	(Com_SignalIdType) 296
#define COM_SIG_TX_TCS1_CF_BRK_MSRREQ           (Com_SignalIdType) 297
#define COM_SIG_TX_TCS1_CF_BRK_TCSMFRN          (Com_SignalIdType) 298
#define COM_SIG_TX_TCS1_HAC_PAS             	(Com_SignalIdType) 299
#define COM_SIG_TX_TCS1_CF_BRK_ESPCTL           (Com_SignalIdType) 300
#define COM_SIG_TX_TCS1_HAC_DEF             	(Com_SignalIdType) 301
#define COM_SIG_TX_TCS1_CF_BRK_MINGEAR          (Com_SignalIdType) 302
#define COM_SIG_TX_TCS1_CF_BRK_MAXGEAR          (Com_SignalIdType) 303
#define COM_SIG_TX_TCS1_CF_BRK_ESS_STAT         (Com_SignalIdType) 304
#define COM_SIG_TX_TCS1_CR_BRK_TQI_PC           (Com_SignalIdType) 305
#define COM_SIG_TX_TCS1_BRAKELIGHT              (Com_SignalIdType) 306
#define COM_SIG_TX_TCS1_CR_BRK_TQISLW_PC        (Com_SignalIdType) 307
#define COM_SIG_TX_TCS1_CR_BRK_TQIMSR_PC        (Com_SignalIdType) 308
#define COM_SIG_TX_TCS1_ESC_OFF_STEP            (Com_SignalIdType) 309
#define COM_SIG_TX_TCS1_CF_BRK_ABSNREQ          (Com_SignalIdType) 310
#define COM_SIG_TX_TCS1_CR_EBS_MSGCHKSUM        (Com_SignalIdType) 311

#define COM_SIG_TX_AHB1_CF_AHB_SLMP             (Com_SignalIdType) 312
#define COM_SIG_TX_AHB1_CF_AHB_DEF              (Com_SignalIdType) 313
#define COM_SIG_TX_AHB1_CF_AHB_ACT              (Com_SignalIdType) 314
#define COM_SIG_TX_AHB1_CF_AHB_DIAG             (Com_SignalIdType) 315
#define COM_SIG_TX_AHB1_CF_AHB_WLMP             (Com_SignalIdType) 316
#define COM_SIG_TX_AHB1_CF_BRK_AHBSTDEP_PC      (Com_SignalIdType) 317
#define COM_SIG_TX_AHB1_CF_BRK_AHBSNSFAIL       (Com_SignalIdType) 318
#define COM_SIG_TX_AHB1_CF_BRK_PEDALCALSTATUS   (Com_SignalIdType) 319
#define COM_SIG_TX_AHB1_CF_BRK_BUZZR            (Com_SignalIdType) 320
#define COM_SIG_TX_AHB1_WHL_SPD_FL_AHB          (Com_SignalIdType) 321
#define COM_SIG_TX_AHB1_WHL_SPD_FR_AHB          (Com_SignalIdType) 322
#define COM_SIG_TX_AHB1_CR_AHB_MSGCHKSUM        (Com_SignalIdType) 323

#define COM_SIG_TX_TCS5_CF_BRK_ABSWLMP          (Com_SignalIdType) 324
#define COM_SIG_TX_TCS5_CF_BRK_EBDWLMP          (Com_SignalIdType) 325
#define COM_SIG_TX_TCS5_CF_BRK_TCSWLMP          (Com_SignalIdType) 326
#define COM_SIG_TX_TCS5_CF_BRK_TCSFLMP          (Com_SignalIdType) 327
#define COM_SIG_TX_TCS5_CF_BRK_ABSDIAG          (Com_SignalIdType) 328
#define COM_SIG_TX_TCS5_CR_BRK_WHEELFL_KMH      (Com_SignalIdType) 329
#define COM_SIG_TX_TCS5_CR_BRK_WHEELFR_KMH      (Com_SignalIdType) 330
#define COM_SIG_TX_TCS5_CR_BRK_WHEELRL_KMH      (Com_SignalIdType) 331
#define COM_SIG_TX_TCS5_CR_BRK_WHEELRR_KMH      (Com_SignalIdType) 332

#define COM_SIG_TX_ESP2_LAT_ACCEL               (Com_SignalIdType) 333
#define COM_SIG_TX_ESP2_LAT_ACCEL_STAT          (Com_SignalIdType) 334
#define COM_SIG_TX_ESP2_LAT_ACCEL_DIAG          (Com_SignalIdType) 335
#define COM_SIG_TX_ESP2_LONG_ACCEL              (Com_SignalIdType) 336
#define COM_SIG_TX_ESP2_LONG_ACCEL_STAT         (Com_SignalIdType) 337
#define COM_SIG_TX_ESP2_LONG_ACCEL_DIAG         (Com_SignalIdType) 338
#define COM_SIG_TX_ESP2_CYL_PRES                (Com_SignalIdType) 339
#define COM_SIG_TX_ESP2_CYL_PRES_STAT           (Com_SignalIdType) 340
#define COM_SIG_TX_ESP2_CYL_PRES_DIAG           (Com_SignalIdType) 341
#define COM_SIG_TX_ESP2_YAW_RATE                (Com_SignalIdType) 342
#define COM_SIG_TX_ESP2_YAW_RATE_STAT           (Com_SignalIdType) 343
#define COM_SIG_TX_ESP2_YAW_RATE_DIAG           (Com_SignalIdType) 344

#define COM_SIG_TX_EBS1_CF_BRK_PGMRUN1          (Com_SignalIdType) 345
#define COM_SIG_TX_EBS1_CF_BRK_EBSSTAT          (Com_SignalIdType) 346
#define COM_SIG_TX_EBS1_CF_BRK_SNSFAIL          (Com_SignalIdType) 347
#define COM_SIG_TX_EBS1_CF_BRK_RBSWLAMP         (Com_SignalIdType) 348
#define COM_SIG_TX_EBS1_CF_BRK_VACUUMSYSDEF     (Com_SignalIdType) 349
#define COM_SIG_TX_EBS1_CR_BRK_REGENTQLIMIT_NM  (Com_SignalIdType) 350
#define COM_SIG_TX_EBS1_CR_BRK_ESTTOT_NM        (Com_SignalIdType) 351
#define COM_SIG_TX_EBS1_CR_BRK_ESTHYD_NM        (Com_SignalIdType) 352
#define COM_SIG_TX_EBS1_CR_BRK_STKDEP_PC        (Com_SignalIdType) 353

#define COM_SIG_TX_WHL_PUL_FL               	(Com_SignalIdType) 354
#define COM_SIG_TX_WHL_PUL_FR               	(Com_SignalIdType) 355
#define COM_SIG_TX_WHL_PUL_RL               	(Com_SignalIdType) 356
#define COM_SIG_TX_WHL_PUL_RR               	(Com_SignalIdType) 357
#define COM_SIG_TX_WHL_DIR_FL               	(Com_SignalIdType) 358
#define COM_SIG_TX_WHL_DIR_FR               	(Com_SignalIdType) 359
#define COM_SIG_TX_WHL_DIR_RL               	(Com_SignalIdType) 360
#define COM_SIG_TX_WHL_DIR_RR               	(Com_SignalIdType) 361

#define COM_SIG_TX_WHL_PUL_CHKSUM               (Com_SignalIdType) 362

#define COM_SIG_TX_WHLSPD_WHL_SPD_FL            (Com_SignalIdType) 363
#define COM_SIG_TX_WHLSPD_ALIVECNT_0            (Com_SignalIdType) 364
#define COM_SIG_TX_WHLSPD_WHL_SPD_FR            (Com_SignalIdType) 365
#define COM_SIG_TX_WHLSPD_ALIVECNT_1            (Com_SignalIdType) 366
#define COM_SIG_TX_WHLSPD_WHL_SPD_RL            (Com_SignalIdType) 367
#define COM_SIG_TX_WHLSPD_WHL_SPD_RR            (Com_SignalIdType) 368

#define COM_SIG_TX_YAWCBIT_LONGACCSENSELFTSTREQ (Com_SignalIdType) 369
#define COM_SIG_TX_YAWCBIT_YAWSERIALNUMBERREQ   (Com_SignalIdType) 370
#define COM_SIG_TX_YAWCBIT_TESTYAWRATESENSOR    (Com_SignalIdType) 371
#define COM_SIG_TX_YAWCBIT_TESTACCLSENSOR       (Com_SignalIdType) 372
#define COM_SIG_TX_YAWCBIT_COMMANDEDSILENCEREQ  (Com_SignalIdType) 373

#define COM_SIG_TX_CALSAS_CCW               	(Com_SignalIdType) 374
#define COM_SIG_TX_CALSAS_LWS_CID               (Com_SignalIdType) 375

#define	COM_SIG_TX_AHBLOG0_0	(Com_SignalIdType)	376
#define	COM_SIG_TX_AHBLOG0_1	(Com_SignalIdType)	377
#define	COM_SIG_TX_AHBLOG0_2	(Com_SignalIdType)	378
#define	COM_SIG_TX_AHBLOG0_3	(Com_SignalIdType)	379
#define	COM_SIG_TX_AHBLOG0_4	(Com_SignalIdType)	380
#define	COM_SIG_TX_AHBLOG0_5	(Com_SignalIdType)	381
#define	COM_SIG_TX_AHBLOG0_6	(Com_SignalIdType)	382
#define	COM_SIG_TX_AHBLOG0_7	(Com_SignalIdType)	383
#define	COM_SIG_TX_AHBLOG1_0	(Com_SignalIdType)	384
#define	COM_SIG_TX_AHBLOG1_1	(Com_SignalIdType)	385
#define	COM_SIG_TX_AHBLOG1_2	(Com_SignalIdType)	386
#define	COM_SIG_TX_AHBLOG1_3	(Com_SignalIdType)	387
#define	COM_SIG_TX_AHBLOG1_4	(Com_SignalIdType)	388
#define	COM_SIG_TX_AHBLOG1_5	(Com_SignalIdType)	389
#define	COM_SIG_TX_AHBLOG1_6	(Com_SignalIdType)	390
#define	COM_SIG_TX_AHBLOG1_7	(Com_SignalIdType)	391
#define	COM_SIG_TX_AHBLOG2_0	(Com_SignalIdType)	392
#define	COM_SIG_TX_AHBLOG2_1	(Com_SignalIdType)	393
#define	COM_SIG_TX_AHBLOG2_2	(Com_SignalIdType)	394
#define	COM_SIG_TX_AHBLOG2_3	(Com_SignalIdType)	395
#define	COM_SIG_TX_AHBLOG2_4	(Com_SignalIdType)	396
#define	COM_SIG_TX_AHBLOG2_5	(Com_SignalIdType)	397
#define	COM_SIG_TX_AHBLOG2_6	(Com_SignalIdType)	398
#define	COM_SIG_TX_AHBLOG2_7	(Com_SignalIdType)	399
#define	COM_SIG_TX_AHBLOG3_0	(Com_SignalIdType)	400
#define	COM_SIG_TX_AHBLOG3_1	(Com_SignalIdType)	401
#define	COM_SIG_TX_AHBLOG3_2	(Com_SignalIdType)	402
#define	COM_SIG_TX_AHBLOG3_3	(Com_SignalIdType)	403
#define	COM_SIG_TX_AHBLOG3_4	(Com_SignalIdType)	404
#define	COM_SIG_TX_AHBLOG3_5	(Com_SignalIdType)	405
#define	COM_SIG_TX_AHBLOG3_6	(Com_SignalIdType)	406
#define	COM_SIG_TX_AHBLOG3_7	(Com_SignalIdType)	407
#define	COM_SIG_TX_AHBLOG4_0	(Com_SignalIdType)	408
#define	COM_SIG_TX_AHBLOG4_1	(Com_SignalIdType)	409
#define	COM_SIG_TX_AHBLOG4_2	(Com_SignalIdType)	410
#define	COM_SIG_TX_AHBLOG4_3	(Com_SignalIdType)	411
#define	COM_SIG_TX_AHBLOG4_4	(Com_SignalIdType)	412
#define	COM_SIG_TX_AHBLOG4_5	(Com_SignalIdType)	413
#define	COM_SIG_TX_AHBLOG4_6	(Com_SignalIdType)	414
#define	COM_SIG_TX_AHBLOG4_7	(Com_SignalIdType)	415
#define	COM_SIG_TX_AHBLOG5_0	(Com_SignalIdType)	416
#define	COM_SIG_TX_AHBLOG5_1	(Com_SignalIdType)	417
#define	COM_SIG_TX_AHBLOG5_2	(Com_SignalIdType)	418
#define	COM_SIG_TX_AHBLOG5_3	(Com_SignalIdType)	419
#define	COM_SIG_TX_AHBLOG5_4	(Com_SignalIdType)	420
#define	COM_SIG_TX_AHBLOG5_5	(Com_SignalIdType)	421
#define	COM_SIG_TX_AHBLOG5_6	(Com_SignalIdType)	422
#define	COM_SIG_TX_AHBLOG5_7	(Com_SignalIdType)	423
#define	COM_SIG_TX_AHBLOG6_0	(Com_SignalIdType)	424
#define	COM_SIG_TX_AHBLOG6_1	(Com_SignalIdType)	425
#define	COM_SIG_TX_AHBLOG6_2	(Com_SignalIdType)	426
#define	COM_SIG_TX_AHBLOG6_3	(Com_SignalIdType)	427
#define	COM_SIG_TX_AHBLOG6_4	(Com_SignalIdType)	428
#define	COM_SIG_TX_AHBLOG6_5	(Com_SignalIdType)	429
#define	COM_SIG_TX_AHBLOG6_6	(Com_SignalIdType)	430
#define	COM_SIG_TX_AHBLOG6_7	(Com_SignalIdType)	431
#define	COM_SIG_TX_AHBLOG7_0	(Com_SignalIdType)	432
#define	COM_SIG_TX_AHBLOG7_1	(Com_SignalIdType)	433
#define	COM_SIG_TX_AHBLOG7_2	(Com_SignalIdType)	434
#define	COM_SIG_TX_AHBLOG7_3	(Com_SignalIdType)	435
#define	COM_SIG_TX_AHBLOG7_4	(Com_SignalIdType)	436
#define	COM_SIG_TX_AHBLOG7_5	(Com_SignalIdType)	437
#define	COM_SIG_TX_AHBLOG7_6	(Com_SignalIdType)	438
#define	COM_SIG_TX_AHBLOG7_7	(Com_SignalIdType)	439
#define	COM_SIG_TX_AHBLOG8_0	(Com_SignalIdType)	440
#define	COM_SIG_TX_AHBLOG8_1	(Com_SignalIdType)	441
#define	COM_SIG_TX_AHBLOG8_2	(Com_SignalIdType)	442
#define	COM_SIG_TX_AHBLOG8_3	(Com_SignalIdType)	443
#define	COM_SIG_TX_AHBLOG8_4	(Com_SignalIdType)	444
#define	COM_SIG_TX_AHBLOG8_5	(Com_SignalIdType)	445
#define	COM_SIG_TX_AHBLOG8_6	(Com_SignalIdType)	446
#define	COM_SIG_TX_AHBLOG8_7	(Com_SignalIdType)	447
#define	COM_SIG_TX_AHBLOG9_0	(Com_SignalIdType)	448
#define	COM_SIG_TX_AHBLOG9_1	(Com_SignalIdType)	449
#define	COM_SIG_TX_AHBLOG9_2	(Com_SignalIdType)	450
#define	COM_SIG_TX_AHBLOG9_3	(Com_SignalIdType)	451
#define	COM_SIG_TX_AHBLOG9_4	(Com_SignalIdType)	452
#define	COM_SIG_TX_AHBLOG9_5	(Com_SignalIdType)	453
#define	COM_SIG_TX_AHBLOG9_6	(Com_SignalIdType)	454
#define	COM_SIG_TX_AHBLOG9_7	(Com_SignalIdType)	455

#define	COM_SIG_TX_ESCLOG0_0	(Com_SignalIdType)	456
#define	COM_SIG_TX_ESCLOG0_1	(Com_SignalIdType)	457
#define	COM_SIG_TX_ESCLOG0_2	(Com_SignalIdType)	458
#define	COM_SIG_TX_ESCLOG0_3	(Com_SignalIdType)	459
#define	COM_SIG_TX_ESCLOG0_4	(Com_SignalIdType)	460
#define	COM_SIG_TX_ESCLOG0_5	(Com_SignalIdType)	461
#define	COM_SIG_TX_ESCLOG0_6	(Com_SignalIdType)	462
#define	COM_SIG_TX_ESCLOG0_7	(Com_SignalIdType)	463
#define	COM_SIG_TX_ESCLOG1_0	(Com_SignalIdType)	464
#define	COM_SIG_TX_ESCLOG1_1	(Com_SignalIdType)	465
#define	COM_SIG_TX_ESCLOG1_2	(Com_SignalIdType)	466
#define	COM_SIG_TX_ESCLOG1_3	(Com_SignalIdType)	467
#define	COM_SIG_TX_ESCLOG1_4	(Com_SignalIdType)	468
#define	COM_SIG_TX_ESCLOG1_5	(Com_SignalIdType)	469
#define	COM_SIG_TX_ESCLOG1_6	(Com_SignalIdType)	470
#define	COM_SIG_TX_ESCLOG1_7	(Com_SignalIdType)	471
#define	COM_SIG_TX_ESCLOG2_0	(Com_SignalIdType)	472
#define	COM_SIG_TX_ESCLOG2_1	(Com_SignalIdType)	473
#define	COM_SIG_TX_ESCLOG2_2	(Com_SignalIdType)	474
#define	COM_SIG_TX_ESCLOG2_3	(Com_SignalIdType)	475
#define	COM_SIG_TX_ESCLOG2_4	(Com_SignalIdType)	476
#define	COM_SIG_TX_ESCLOG2_5	(Com_SignalIdType)	477
#define	COM_SIG_TX_ESCLOG2_6	(Com_SignalIdType)	478
#define	COM_SIG_TX_ESCLOG2_7	(Com_SignalIdType)	479
#define	COM_SIG_TX_ESCLOG3_0	(Com_SignalIdType)	480
#define	COM_SIG_TX_ESCLOG3_1	(Com_SignalIdType)	481
#define	COM_SIG_TX_ESCLOG3_2	(Com_SignalIdType)	482
#define	COM_SIG_TX_ESCLOG3_3	(Com_SignalIdType)	483
#define	COM_SIG_TX_ESCLOG3_4	(Com_SignalIdType)	484
#define	COM_SIG_TX_ESCLOG3_5	(Com_SignalIdType)	485
#define	COM_SIG_TX_ESCLOG3_6	(Com_SignalIdType)	486
#define	COM_SIG_TX_ESCLOG3_7	(Com_SignalIdType)	487
#define	COM_SIG_TX_ESCLOG4_0	(Com_SignalIdType)	488
#define	COM_SIG_TX_ESCLOG4_1	(Com_SignalIdType)	489
#define	COM_SIG_TX_ESCLOG4_2	(Com_SignalIdType)	490
#define	COM_SIG_TX_ESCLOG4_3	(Com_SignalIdType)	491
#define	COM_SIG_TX_ESCLOG4_4	(Com_SignalIdType)	492
#define	COM_SIG_TX_ESCLOG4_5	(Com_SignalIdType)	493
#define	COM_SIG_TX_ESCLOG4_6	(Com_SignalIdType)	494
#define	COM_SIG_TX_ESCLOG4_7	(Com_SignalIdType)	495
#define	COM_SIG_TX_ESCLOG5_0	(Com_SignalIdType)	496
#define	COM_SIG_TX_ESCLOG5_1	(Com_SignalIdType)	497
#define	COM_SIG_TX_ESCLOG5_2	(Com_SignalIdType)	498
#define	COM_SIG_TX_ESCLOG5_3	(Com_SignalIdType)	499
#define	COM_SIG_TX_ESCLOG5_4	(Com_SignalIdType)	500
#define	COM_SIG_TX_ESCLOG5_5	(Com_SignalIdType)	501
#define	COM_SIG_TX_ESCLOG5_6	(Com_SignalIdType)	502
#define	COM_SIG_TX_ESCLOG5_7	(Com_SignalIdType)	503
#define	COM_SIG_TX_ESCLOG6_0	(Com_SignalIdType)	504
#define	COM_SIG_TX_ESCLOG6_1	(Com_SignalIdType)	505
#define	COM_SIG_TX_ESCLOG6_2	(Com_SignalIdType)	506
#define	COM_SIG_TX_ESCLOG6_3	(Com_SignalIdType)	507
#define	COM_SIG_TX_ESCLOG6_4	(Com_SignalIdType)	508
#define	COM_SIG_TX_ESCLOG6_5	(Com_SignalIdType)	509
#define	COM_SIG_TX_ESCLOG6_6	(Com_SignalIdType)	510
#define	COM_SIG_TX_ESCLOG6_7	(Com_SignalIdType)	511
#define	COM_SIG_TX_ESCLOG7_0	(Com_SignalIdType)	512
#define	COM_SIG_TX_ESCLOG7_1	(Com_SignalIdType)	513
#define	COM_SIG_TX_ESCLOG7_2	(Com_SignalIdType)	514
#define	COM_SIG_TX_ESCLOG7_3	(Com_SignalIdType)	515
#define	COM_SIG_TX_ESCLOG7_4	(Com_SignalIdType)	516
#define	COM_SIG_TX_ESCLOG7_5	(Com_SignalIdType)	517
#define	COM_SIG_TX_ESCLOG7_6	(Com_SignalIdType)	518
#define	COM_SIG_TX_ESCLOG7_7	(Com_SignalIdType)	519
#define	COM_SIG_TX_ESCLOG8_0	(Com_SignalIdType)	520
#define	COM_SIG_TX_ESCLOG8_1	(Com_SignalIdType)	521
#define	COM_SIG_TX_ESCLOG8_2	(Com_SignalIdType)	522
#define	COM_SIG_TX_ESCLOG8_3	(Com_SignalIdType)	523
#define	COM_SIG_TX_ESCLOG8_4	(Com_SignalIdType)	524
#define	COM_SIG_TX_ESCLOG8_5	(Com_SignalIdType)	525
#define	COM_SIG_TX_ESCLOG8_6	(Com_SignalIdType)	526
#define	COM_SIG_TX_ESCLOG8_7	(Com_SignalIdType)	527
#define	COM_SIG_TX_ESCLOG9_0	(Com_SignalIdType)	528
#define	COM_SIG_TX_ESCLOG9_1	(Com_SignalIdType)	529
#define	COM_SIG_TX_ESCLOG9_2	(Com_SignalIdType)	530
#define	COM_SIG_TX_ESCLOG9_3	(Com_SignalIdType)	531
#define	COM_SIG_TX_ESCLOG9_4	(Com_SignalIdType)	532
#define	COM_SIG_TX_ESCLOG9_5	(Com_SignalIdType)	533
#define	COM_SIG_TX_ESCLOG9_6	(Com_SignalIdType)	534
#define	COM_SIG_TX_ESCLOG9_7	(Com_SignalIdType)	535


#define COM_SIG_MAX_NUM                    		(Com_SignalIdType) 536

 /* Frame ID symbolic name */
 #define CAN_FRAME_EMS1_MSG_RX              	(Com_FrameIdType) 0
 #define CAN_FRAME_EMS2_MSG_RX              	(Com_FrameIdType) 1
 #define CAN_FRAME_EMS6_MSG_RX                  (Com_FrameIdType) 2
 #define CAN_FRAME_TCU1_MSG_RX                  (Com_FrameIdType) 3
 #define CAN_FRAME_TCU2_MSG_RX                  (Com_FrameIdType) 4
 #define CAN_FRAME_TCU5_MSG_RX                  (Com_FrameIdType) 5
 #define CAN_FRAME_TCU6_MSG_RX                  (Com_FrameIdType) 6
 #define CAN_FRAME_HCU1_MSG_RX                  (Com_FrameIdType) 7
 #define CAN_FRAME_HCU2_MSG_RX                  (Com_FrameIdType) 8
 #define CAN_FRAME_HCU3_MSG_RX                  (Com_FrameIdType) 9
 #define CAN_FRAME_HCU5_MSG_RX                  (Com_FrameIdType) 10
 #define CAN_FRAME_MCU1_MSG_RX                  (Com_FrameIdType) 11
 #define CAN_FRAME_MCU2_MSG_RX                  (Com_FrameIdType) 12
 #define CAN_FRAME_BMS1_MSG_RX                  (Com_FrameIdType) 13
 #define CAN_FRAME_VSM2_MSG_RX                  (Com_FrameIdType) 14
 #define CAN_FRAME_SAS1_MSG_RX                  (Com_FrameIdType) 15
 #define CAN_FRAME_FATC1_MSG_RX                 (Com_FrameIdType) 16
 #define CAN_FRAME_CLU1_MSG_RX                  (Com_FrameIdType) 17
 #define CAN_FRAME_CGW1_MSG_RX                  (Com_FrameIdType) 18
 #define CAN_FRAME_EMS3_MSG_RX              	(Com_FrameIdType) 19
 #define CAN_FRAME_DIAG_FUNC_MSG_RX             (Com_FrameIdType) 20
 #define CAN_FRAME_DIAG_PHY_MSG_RX              (Com_FrameIdType) 21
 #define CAN_FRAME_CLU2_MSG_RX              	(Com_FrameIdType) 22
 #define CAN_FRAME_EPB1_MSG_RX                  (Com_FrameIdType) 23
 #define CAN_FRAME_YAWACC_MSG_RX            	(Com_FrameIdType) 24
 #define CAN_FRAME_LONGACC_MSG_RX            	(Com_FrameIdType) 25
 #define CAN_FRAME_YAWSERIAL_MSG_RX          	(Com_FrameIdType) 26
 #define CAN_FRAME_TCS1_MSG_TX              	(Com_FrameIdType) 27
 #define CAN_FRAME_TCS5_MSG_TX                  (Com_FrameIdType) 28
 #define CAN_FRAME_ESP2_MSG_TX                  (Com_FrameIdType) 29
 #define CAN_FRAME_CALSAS_MSG_TX                (Com_FrameIdType) 30
 #define CAN_FRAME_ESP1_MSG_TX                  (Com_FrameIdType) 31
 #define CAN_FRAME_VSM1_MSG_TX                  (Com_FrameIdType) 32
 #define CAN_FRAME_WHLPUL_MSG_TX                (Com_FrameIdType) 33
 #define CAN_FRAME_AHB1_MSG_TX              	(Com_FrameIdType) 34
 #define CAN_FRAME_EBS1_MSG_TX              	(Com_FrameIdType) 35
 #define CAN_FRAME_WHLSPD_MSG_TX             	(Com_FrameIdType) 36
 #define CAN_FRAME_WHLSPD_HEV_MSG_TX            (Com_FrameIdType) 37
 #define CAN_FRAME_DIAG_TX_MSG_TX               (Com_FrameIdType) 38
 #define CAN_FRAME_YAWCBIT_MSG_TX            	(Com_FrameIdType) 39
 #define CAN_FRAME_LOGGER0_MSG_TX             	(Com_FrameIdType) 40
 #define CAN_FRAME_LOGGER1_MSG_TX               (Com_FrameIdType) 41
 #define CAN_FRAME_LOGGER2_MSG_TX               (Com_FrameIdType) 42
 #define CAN_FRAME_LOGGER3_MSG_TX               (Com_FrameIdType) 43
 #define CAN_FRAME_LOGGER4_MSG_TX               (Com_FrameIdType) 44
 #define CAN_FRAME_LOGGER5_MSG_TX               (Com_FrameIdType) 45
 #define CAN_FRAME_LOGGER6_MSG_TX               (Com_FrameIdType) 46
 #define CAN_FRAME_LOGGER7_MSG_TX               (Com_FrameIdType) 47
 #define CAN_FRAME_LOGGER8_MSG_TX               (Com_FrameIdType) 48
 #define CAN_FRAME_LOGGER9_MSG_TX               (Com_FrameIdType) 49
 #define CAN_FRAME_LOGGER10_MSG_TX              (Com_FrameIdType) 50
 #define CAN_FRAME_LOGGER11_MSG_TX              (Com_FrameIdType) 51
 #define CAN_FRAME_LOGGER12_MSG_TX              (Com_FrameIdType) 52
 #define CAN_FRAME_LOGGER13_MSG_TX              (Com_FrameIdType) 53
 #define CAN_FRAME_LOGGER14_MSG_TX              (Com_FrameIdType) 54
 #define CAN_FRAME_LOGGER15_MSG_TX              (Com_FrameIdType) 55
 #define CAN_FRAME_LOGGER16_MSG_TX              (Com_FrameIdType) 56
 #define CAN_FRAME_LOGGER17_MSG_TX              (Com_FrameIdType) 57
 #define CAN_FRAME_LOGGER18_MSG_TX              (Com_FrameIdType) 58
 #define CAN_FRAME_LOGGER19_MSG_TX              (Com_FrameIdType) 59
 #define CAN_FRAME_LOGGER20_MSG_TX              (Com_FrameIdType) 60

 #define CAN_FRAME_MAX_NUM                    	(Com_FrameIdType) 60
 #define COM_NUMBER_OF_RX_MSG					27
 #define COM_NUMBER_OF_TX_MSG					33

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
extern const Com_SigConfigType	 Com_SigConfig[COM_SIG_MAX_NUM];
extern const Com_FrameConfigType Com_FrameConfig[CAN_FRAME_MAX_NUM];

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
 extern Com_FrameType  Com_FrameBuf[CAN_FRAME_MAX_NUM];

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
 /* User Callback */
 extern void Proxy_CanIfRxIndication(Can_IdType RxPduId, PduInfoType* PduInfoPtr);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/

#endif /* COM_CFG_H_ */
