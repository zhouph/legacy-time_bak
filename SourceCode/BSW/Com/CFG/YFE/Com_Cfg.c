/*
 * Com_Cfg.c
 *
 *  Created on: 2014. 12. 26.
 *      Author: jinsu.park
 */


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Com.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define COMMCAN_START_SEC_CONST_UNSPECIFIED
#include "Com_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
const Com_SigConfigType	 Com_SigConfig[COM_SIG_MAX_NUM] =
{
    /* COM_SIG_RX_EMS1_FUECUT */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        0,						        /* Signal Start Bit */
        1,						        /* Signal Length */
        DataType8bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_FUECINH */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        1,						        /* Signal Start Bit */
        1,						        /* Signal Length */
        DataType8bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_TQSTD_NM */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        2,						        /* Signal Start Bit */
        6,						        /* Signal Length */
        DataType8bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_ACTINDTQ_PC */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        8,						        /* Signal Start Bit */
        8,						        /* Signal Length */
        DataType8bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_ENGSPD_RPM */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        16,						        /* Signal Start Bit */
        16,						        /* Signal Length */
        DataType16bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_INDTQ_PC */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        32,						        /* Signal Start Bit */
        8,						        /* Signal Length */
        DataType8bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_FRICTTQ_PC */
    {
    	CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
        40,						        /* Signal Start Bit */
        8,						        /* Signal Length */
        DataType8bit					/* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_EMS1_VEHSPD_KMH */
	{
		CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS1_MSGCHKSUM1 */
	{
		CAN_FRAME_EMS1_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_FRTCMBCMP */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_ENGSTAT */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_FULLLOAD */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_DC */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_WARMUPCYC */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		6,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_RBM_DC */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		7,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_IDLTGTSPD_RPM */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_OBDFRFACK */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		6,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_O2_PHST */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		22,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_ENGSPDERR */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		23,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_FUECON_UL */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_TPS_PC */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS2_ACCPEDDEP_PC */
	{
		CAN_FRAME_EMS2_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_VBOFFACT */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_ACNCOMPOFFREQ */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_SOAKTERR */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_CONFMILFMEMMNG */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_PGMRUN3 */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		6,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_ENGCOLTEMP_C */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_IMMOLAMP */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_MILREQ */
	//{
	//	CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
	//	18,						        /* Signal Start Bit */
	//	1,						        /* Signal Length */
	//	DataType8bit					/* Data Type : 8, 16, 32 bit */
	//},
    /* COM_SIG_RX_EMS3_WIPER */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		19,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_FANONF */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		20,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_FANSTAT */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		21,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_AAFOPENREQ */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		23,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_IGCYCCTR */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_SOAKT_MIN */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_BRKFORACT */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_TEMPFUELRUNOUT */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		50,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_SOAKT480OVER */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		51,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_FUELRUNOUT */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		52,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_EMS3_ENGCOLTEMPSUB_C */
	{
		CAN_FRAME_EMS3_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_TARGE */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_RDY */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_GARCHANGE */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_FLT */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_GARSELDISP */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_FANCTRLREQ */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		12,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_TQREDREQ_PC */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		14,						        /* Signal Start Bit */
		9,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_TQREDREQSLW_PC */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		23,						        /* Signal Start Bit */
		9,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_VEHSPD_KPH */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_VEHSPDDEC_KPH */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_OPUSPDCMD_RPM */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU1_TQINCREQ_PC */
	{
		CAN_FRAME_TCU1_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_TYP */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_GEARTYP */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_OPUPWRSAVREQ */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		7,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_ATFLUTEMP_C */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_SERIVCELAMP */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_MILREQ */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		17,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_LMPSLPLMTSTS */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		18,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_OPUSTAT */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		19,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_TEMPTFSTS */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		20,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU5_TMOUTSPD_RPM */
	{
		CAN_FRAME_TCU5_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU6_CURPROGRAM */
	{
		CAN_FRAME_TCU6_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		5,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU6_SHIFTCLASS_CCAN */
	{
		CAN_FRAME_TCU6_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		5,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU6_CLTLIMACT_FS */
	{
		CAN_FRAME_TCU6_MSG_RX,		/* Frame Id */
		20,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU6_REQIDLESPD_RPM_FS */
	{
		CAN_FRAME_TCU6_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU6_ADDTQLOSS_PC_FS */
	{
		CAN_FRAME_TCU6_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_TCU6_ENGTQRED_PC_FS */
	{
		CAN_FRAME_TCU6_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_INJENB */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ECUWARMUPCTRREQ */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ENGTQGENDCS */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_MILREQ */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_TQINTSTAT */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ENGCLTSTAT */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ENGTQCMDBINV_PC */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ENGTQCMD_PC */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_REQSPDUP_RPM */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_PTOPERMODEFORECU */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ECUANTIJERKINH */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		35,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ENGCLTCLASS */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		36,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_VEH_ST */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		39,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_CLTCTRPHASE */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_TQINTMOD */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		43,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_RDY */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		44,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_HEVRDY */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		45,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_HEVRDYCLU */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		46,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_ENGTRANSCTLSTAT */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_PGMRUN1 */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		49,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_SLWDWNSTAT */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		53,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_STINHSTAT */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		54,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU1_MSGCHKSUM1 */
	{
		CAN_FRAME_HCU1_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_REGENENA */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_TQREDSTAT */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_TQINCSTAT */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_SERIVCEMOD */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_PGMRUN2 */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		6,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_CRPTQ_NM */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_WHLDEMTQ_NM */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_REGENBRKTQ_NM */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU2_MSGCHKSUM2 */
	{
		CAN_FRAME_HCU2_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_EWPDIAGENA */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MOTPWRENA */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_GENPWRENA */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MOTCTLMOD */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_ISGCTLMOD */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_PGMRUN3 */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MCUANTIJERKINH */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		7,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MOTTQCMC_PC */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		10,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MOTSPDCMD_RPM */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		18,						        /* Signal Start Bit */
		14,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MOTTQCMDBINV_PC */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		10,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_TMINTQCMDBINV_PC */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		42,						        /* Signal Start Bit */
		11,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_LIMPOFF */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		53,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_CHGREQ */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		54,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_STARTINH */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		55,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU3_MSGCHKSUM3 */
	{
		CAN_FRAME_HCU3_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_ACNOPENPERM */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_BLROFFRQ */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_COOLSTAT */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_SERLAMP */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_ECOSCO */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_COMPERM_W */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_FUELECO_MPG */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_ECOLVL */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_BLUEMODE */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_CRUONLAMP */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		33,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_CRUSETLAMP */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		34,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_THIENGTIR */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		35,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_THIBATTIR */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		36,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_THIENGBAT */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		37,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_MOTCHA */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		38,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_ENGBRK */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		39,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_POWRAT_PC */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_LDCVREF_V */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_LDCINH */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_LDCUVC */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		57,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_HEVMOD */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		58,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_HCU5_PGMRUN5 */
	{
		CAN_FRAME_HCU5_MSG_RX,		/* Frame Id */
		62,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_MOTESTTQ_PC */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		10,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_PGMRUN1 */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		14,						        /* Signal Start Bit */
		2,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_MOTACTROTSPD_RPM */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_MOTDEFACT_PC */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_MOTTQUPLMTM_PC */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_MOTTQUPLMTG_PC */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU1_MSGCHKSUM1 */
	{
		CAN_FRAME_MCU1_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_FLT */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_WRN */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_ACTTEST */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_MILREQ */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_SERVICELAMP */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_INVRDY */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_INVCTRABLE */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		6,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_RELAYOFFREQ */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		7,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_MOTRADFANONFREQ */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_AJACTIVATE */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		9,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_ENGRPMLMT */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		10,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_ENGRPMLMT */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		11,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_EWPENABLE */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		12,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_RLYOFFREQDELAYED */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		13,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_RLYOFFREQDELAYED */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		14,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_FLT */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_WRN */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		17,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_ACTTEST */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		18,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_MILREQ */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		19,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_SERVICELAMP */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		20,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_INVRDY */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		21,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_INVCTRABLE */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		22,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_RELAYOFFREQ */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		23,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_INVTEMP_C */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_MOTTEMP_C */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_INVTEMP_C */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_GCU_HSGTEMP_C */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_MCU2_MSGCHKSUM2 */
	{
		CAN_FRAME_MCU2_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_BATCHALMT_W */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_BATDCHLMT_W */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_OVERCHALMT */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_SERVICELAMP */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		33,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_MAINRLYONSTAT */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		34,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_RLYOFF_BYREQ */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		35,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_FLT */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		36,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_WRN */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		37,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_CTRB */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		38,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_RDY */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		39,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_SOC_PC */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		40,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_AUXTEMP_C */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_MILREQ */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_BATCOOLPWRPREM */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		57,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_MAINRLYOPERTMP */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		58,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_BMS1_RLYWELD */
	{
		CAN_FRAME_BMS1_MSG_RX,		/* Frame Id */
		59,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_ANGLE */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		16,						        /* Signal Length */
		DataType16bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_SPEED */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_OK */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_CAL */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		25,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_TRIM */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		26,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_SF */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		27,						        /* Signal Start Bit */
		5,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_MSGCOUNT */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_SAS1_CHECKSUM */
	{
		CAN_FRAME_SAS1_MSG_RX,		/* Frame Id */
		36,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_ACNREQSWI */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_BLRON */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		1,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_ENGONREQ */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		2,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_HVACPMPOPERSTAT */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		3,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_ACNCOMFLT */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		4,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_COMCANFLT */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		5,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_REARDEF */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		6,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_BLWRMAX */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		7,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_ACNCOMREQ_W */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		8,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_ACNCOMMAXREQ_W */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		16,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_ACNCOMCST_W */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		24,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_MSGCNT */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		32,						        /* Signal Start Bit */
		4,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_OUTTEMP_SNR_C */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		48,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_FATC1_CHKSUM */
	{
		CAN_FRAME_FATC1_MSG_RX,		/* Frame Id */
		56,						        /* Signal Start Bit */
		8,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_CLU1_CRUISESWSTATUS */
	{
		CAN_FRAME_CLU1_MSG_RX,		/* Frame Id */
		0,						        /* Signal Start Bit */
		3,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /* COM_SIG_RX_CLU1_CF_CLU_PARITYBIT1 */
	{
		CAN_FRAME_CLU1_MSG_RX,		    /* Frame Id */
		5,						        /* Signal Start Bit */
		1,						        /* Signal Length */
		DataType8bit					/* Data Type : 8, 16, 32 bit */
	},
    /*COM_SIG_RX_CLU1_SPEED_UNIT */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        6,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_P_BRAKE_ACT */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        7,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_VANZ */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        9,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_ALIVECOUNTERCLU1 */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        17,                              /* Signal Start Bit */
        7,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_CRUISESW_MAIN */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_DASHACCFAIL */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        25,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_DLSFAIL */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        26,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_CF_CLU_BRAKEFLUIDSW */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        27,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },    
    /*COM_SIG_RX_CLU1_CF_STRRLYSTAT */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        30,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_CF_BUTSYSVARIND */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        31,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_R_TQACNOUTC */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU1_ODOMETER */
    {
        CAN_FRAME_CLU1_MSG_RX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        24,                              /* Signal Length */
        DataType32bit                    /* Data Type : 8, 16, 32 bit */
    },
    /*COM_SIG_RX_CLU2_IGN_SW */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_RKE_CMD */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        3,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_DRV_DR_SW */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        6,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_DRV_KeyLock */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_DRV_KeyUnlock */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        9,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_PIC_Lock */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        10,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_PIC_Unlock */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        13,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_DRV_Seat_Belt */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_TRUNK_OPEN_STATUS */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        18,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_PAS_Seat_Belt */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        20,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_HoodStat */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        22,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_TumSigLh */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_TumSigRh */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        25,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_LdwsSW */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        26,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_WiperIntT */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        27,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_WiperIntSW */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        30,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_WiperLow */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        31,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_WiperHigh */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_WiperAuto */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        33,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_RainSnsStat */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        34,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_GearRStat */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_HS_STAT */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        44,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_CLU2_CF_Clu_ActiveEcoSW     */
    {
        CAN_FRAME_CLU2_MSG_RX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },

    /* COM_SIG_RX_YAWACC_ROLLINGCOUNTER */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        4,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_LATACCSELFTESTSTATUS */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        4,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_YAWRATESELFTESTSTATUS */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        5,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_LATACCVALIDDATA */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        6,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_YAWRATEVAILDDATA */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        7,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_YAWRATESIGNAL_0 */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_ACCELERATORRATESIG_0 */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        4,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_YAWRATESIGNAL_1 */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        20,                              /* Signal Start Bit */
        4,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_ACCELERATORRATESIG_1 */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_SENSOROSCFREQDEV */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_AD_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_ROM_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        41,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_RAM_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        42,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_ACCEL_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        43,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_ASIC_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        44,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_BATT_RANGE_ERR */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        45,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_EEP_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        46,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_GYRO_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        47,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_CAN_FUNC_ERR */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        51,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_PLAUS_ERR_PST */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        52,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_RASTER_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        53,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_WATCHDOG_RST */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        54,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWACC_OSC_FAIL */
    {
        CAN_FRAME_YAWACC_MSG_RX,          /* Frame Id */
        55,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },

    /* COM_SIG_RX_LONGACC_LONGROLLINGCOUNTER */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        4,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },    
    /* COM_SIG_RX_LONGACC_LONGACCINVALIDDATA */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        4,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_INTSENFLTSYMTMACTIVE */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        5,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_LONGACCSELFTSTSTATUS */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        6,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_INTSENFAULTPRESENT */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        7,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_0 */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        4,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_LONGACCSENCIRERRPRE */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        12,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_INTTEMPSENSORFAULT */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        14,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_LONACSENRANCHKERRPRE */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        15,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_1 */
    {
        CAN_FRAME_LONGACC_MSG_RX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_0 */
    {
    	CAN_FRAME_YAWSERIAL_MSG_RX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },           
    /* COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_1 */
    {
    	CAN_FRAME_YAWSERIAL_MSG_RX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_2 */
    {
    	CAN_FRAME_YAWSERIAL_MSG_RX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_3 */
    {
    	CAN_FRAME_YAWSERIAL_MSG_RX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_4 */
    {
    	CAN_FRAME_YAWSERIAL_MSG_RX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_5 */
    {
    	CAN_FRAME_YAWSERIAL_MSG_RX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },

    /* COM_SIG_TX_TCS1_CF_BRK_TCSREQ */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_TCSPAS */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        1,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_TCSDEF */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        2,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_TCSCTL */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        3,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ABSACT */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        4,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ABSDEF */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        5,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_EBDDEF */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        6,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_TCSGSC */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        7,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ESPPAS */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ESPDEF */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        9,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_HAC_CTL */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        10,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_MSRREQ */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        11,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_TCSMFRN */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        12,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_HAC_PAS */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        13,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ESPCTL */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        14,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_HAC_DEF */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        15,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_MINGEAR */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_MAXGEAR */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        19,                              /* Signal Start Bit */
        3,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ESS_STAT */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        22,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CR_BRK_TQI_PC */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        9,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_BRAKELIGHT */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        33,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CR_BRK_TQISLW_PC */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        34,                              /* Signal Start Bit */
        9,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CR_BRK_TQIMSR_PC */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        44,                              /* Signal Start Bit */
        9,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_ESC_OFF_STEP */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        53,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CF_BRK_ABSNREQ */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        55,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS1_CR_EBS_MSGCHKSUM */
    {
        CAN_FRAME_TCS1_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },

    /* COM_SIG_TX_AHB1_CF_AHB_SLMP */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_AHB_DEF */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        2,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_AHB_ACT */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        4,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_AHB_DIAG */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        6,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_AHB_WLMP */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        7,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_BRK_AHBSTDEP_PC */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        16,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_BRK_AHBSNSFAIL */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_BRK_PEDALCALSTATUS */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        25,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CF_BRK_BUZZR */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        26,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_WHL_SPD_FL_AHB */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        28,                              /* Signal Start Bit */
        14,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_WHL_SPD_FR_AHB */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        42,                              /* Signal Start Bit */
        14,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHB1_CR_AHB_MSGCHKSUM */
    {
        CAN_FRAME_AHB1_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CF_BRK_ABSWLMP */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CF_BRK_EBDWLMP */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        1,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CF_BRK_TCSWLMP */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        2,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CF_BRK_TCSFLMP */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        3,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CF_BRK_ABSDIAG */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        5,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CR_BRK_WHEELFL_KMH */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        12,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CR_BRK_WHEELFR_KMH */    
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        28,                              /* Signal Start Bit */
        12,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CR_BRK_WHEELRL_KMH */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        12,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_TCS5_CR_BRK_WHEELRR_KMH */
    {
        CAN_FRAME_TCS5_MSG_TX,          /* Frame Id */
        52,                              /* Signal Start Bit */
        12,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_LAT_ACCEL */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        11,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_LAT_ACCEL_STAT */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        14,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_LAT_ACCEL_DIAG */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        15,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_LONG_ACCEL */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        11,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_LONG_ACCEL_STAT */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        30,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_LONG_ACCEL_DIAG */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        31,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_CYL_PRES */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        12,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_CYL_PRES_STAT */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        46,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_CYL_PRES_DIAG */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        47,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_YAW_RATE */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        13,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_YAW_RATE_STAT */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        62,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESP2_YAW_RATE_DIAG */
    {
        CAN_FRAME_ESP2_MSG_TX,          /* Frame Id */
        63,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CF_BRK_PGMRUN1 */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CF_BRK_EBSSTAT */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        2,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CF_BRK_SNSFAIL */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        3,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CF_BRK_RBSWLAMP */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        4,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CF_BRK_VACUUMSYSDEF */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        5,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CR_BRK_REGENTQLIMIT_NM */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        16,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CR_BRK_ESTTOT_NM */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        16,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CR_BRK_ESTHYD_NM */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        16,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_EBS1_CR_BRK_STKDEP_PC */
    {
        CAN_FRAME_EBS1_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_PUL_FL */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_PUL_FR */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_PUL_RL */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_PUL_RR */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_DIR_FL */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_DIR_FR */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        34,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_DIR_RL */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        36,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_DIR_RR */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        38,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHL_PUL_CHKSUM */
    {
        CAN_FRAME_WHLPUL_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHLSPD_WHL_SPD_FL */
    {
        CAN_FRAME_WHLSPD_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        14,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHLSPD_ALIVECNT_0 */
    {
        CAN_FRAME_WHLSPD_MSG_TX,          /* Frame Id */
        14,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHLSPD_WHL_SPD_FR */
    {
        CAN_FRAME_WHLSPD_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        14,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHLSPD_ALIVECNT_1 */
    {
        CAN_FRAME_WHLSPD_MSG_TX,          /* Frame Id */
        30,                              /* Signal Start Bit */
        2,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHLSPD_WHL_SPD_RL */
    {
        CAN_FRAME_WHLSPD_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        14,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_WHLSPD_WHL_SPD_RR */
    {
        CAN_FRAME_WHLSPD_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        14,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_YAWCBIT_LONGACCSENSELFTSTREQ */
    {
    	CAN_FRAME_YAWCBIT_MSG_TX,          /* Frame Id */
        11,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_YAWCBIT_YAWSERIALNUMBERREQ */
    {
    	CAN_FRAME_YAWCBIT_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_YAWCBIT_TESTYAWRATESENSOR */
    {
    	CAN_FRAME_YAWCBIT_MSG_TX,          /* Frame Id */
        17,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_YAWCBIT_TESTACCLSENSOR */
    {
    	CAN_FRAME_YAWCBIT_MSG_TX,          /* Frame Id */
        18,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_YAWCBIT_COMMANDEDSILENCEREQ */
    {
    	CAN_FRAME_YAWCBIT_MSG_TX,          /* Frame Id */
        31,                              /* Signal Start Bit */
        1,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_CALSAS_CCW */
    {
    	CAN_FRAME_CALSAS_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        4,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_CALSAS_LWS_CID */
    {
    	CAN_FRAME_CALSAS_MSG_TX,          /* Frame Id */
        4,                              /* Signal Start Bit */
        11,                              /* Signal Length */
        DataType16bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_0 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_1 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_2 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_3 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_4 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_5 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_6 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG0_7 */
    {
    	CAN_FRAME_LOGGER0_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_0 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_1 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_2 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_3 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_4 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_5 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_6 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG1_7 */
    {
    	CAN_FRAME_LOGGER1_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_0 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_1 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_2 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_3 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_4 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_5 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_6 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG2_7 */
    {
    	CAN_FRAME_LOGGER2_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_0 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_1 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_2 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_3 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_4 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_5 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_6 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG3_7 */
    {
    	CAN_FRAME_LOGGER3_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_0 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_1 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_2 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_3 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_4 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_5 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_6 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG4_7 */
    {
    	CAN_FRAME_LOGGER4_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_0 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_1 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_2 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_3 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_4 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_5 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_6 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG5_7 */
    {
    	CAN_FRAME_LOGGER5_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_0 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_1 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_2 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_3 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_4 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_5 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_6 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG6_7 */
    {
    	CAN_FRAME_LOGGER6_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_0 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_1 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_2 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_3 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_4 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_5 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_6 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG7_7 */
    {
    	CAN_FRAME_LOGGER7_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_0 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_1 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_2 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_3 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_4 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_5 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_6 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG8_7 */
    {
    	CAN_FRAME_LOGGER8_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_0 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_1 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_2 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_3 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_4 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_5 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_6 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_AHBLOG9_7 */
    {
    	CAN_FRAME_LOGGER9_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_0 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_1 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_2 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_3 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_4 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_5 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_6 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG0_7 */
    {
    	CAN_FRAME_LOGGER10_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_0 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_1 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_2 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_3 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_4 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_5 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_6 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG1_7 */
    {
    	CAN_FRAME_LOGGER11_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_0 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_1 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_2 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_3 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_4 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_5 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_6 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG2_7 */
    {
    	CAN_FRAME_LOGGER12_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_0 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_1 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_2 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_3 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_4 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_5 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_6 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG3_7 */
    {
    	CAN_FRAME_LOGGER13_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_0 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_1 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_2 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_3 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_4 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_5 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_6 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG4_7 */
    {
    	CAN_FRAME_LOGGER14_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_0 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_1 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_2 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_3 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_4 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_5 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_6 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG5_7 */
    {
    	CAN_FRAME_LOGGER15_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_0 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_1 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_2 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_3 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_4 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_5 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_6 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG6_7 */
    {
    	CAN_FRAME_LOGGER16_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_0 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_1 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_2 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_3 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_4 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_5 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_6 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG7_7 */
    {
    	CAN_FRAME_LOGGER17_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_0 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_1 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_2 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_3 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_4 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_5 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_6 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG8_7 */
    {
    	CAN_FRAME_LOGGER18_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_0 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        0,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_1 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        8,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_2 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        16,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_3 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        24,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_4 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        32,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_5 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        40,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_6 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        48,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    },
    /* COM_SIG_TX_ESCLOG9_7 */
    {
    	CAN_FRAME_LOGGER19_MSG_TX,          /* Frame Id */
        56,                              /* Signal Start Bit */
        8,                              /* Signal Length */
        DataType8bit                    /* Data Type : 8, 16, 32 bit */
    }
};

const Com_FrameConfigType Com_FrameConfig[CAN_FRAME_MAX_NUM] =
 {
	/* CAN_FRAME_EMS1_MSG_RX */
	{
	    0x316,					        /* Message Id */
	    8,						        /* Message Length */
	    10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_EMS1 								/* HwHandleType */
	},
    /* CAN_FRAME_EMS2_MSG_RX */
    {
        0x329,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_EMS2 								/* HwHandleType */
    },
    /* CAN_FRAME_EMS6_MSG_RX */
    {
        0x260,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_EMS6 								/* HwHandleType */
    },
    /* CAN_FRAME_TCU1_MSG_RX */
    {
        0x43f,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_TCU1 								/* HwHandleType */
    },
    /* CAN_FRAME_TCU2_MSG_RX */
    {
        0x440,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_TCU2 								/* HwHandleType */
    },
    /* CAN_FRAME_TCU5_MSG_RX */
    {
        0x575,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_TCU5 								/* HwHandleType */
    },
    /* CAN_FRAME_TCU6_MSG_RX */
    {
        0x100,					        /* Message Id */
        2,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_TCU6 								/* HwHandleType */
    },
    /* CAN_FRAME_HCU1_MSG_RX */
    {
        0x200,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_HCU1 								/* HwHandleType */
    },
    /* CAN_FRAME_HCU2_MSG_RX */
    {
        0x201,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_HCU2 								/* HwHandleType */
    },
    /* CAN_FRAME_HCU3_MSG_RX */
    {
        0x202,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_HCU3 								/* HwHandleType */
    },
    /* CAN_FRAME_HCU5_MSG_RX */
    {
        0x592,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_HCU5 								/* HwHandleType */
    },
    /* CAN_FRAME_MCU1_MSG_RX */
    {
        0x291,					        /* Message Id */
        8,						        /* Message Length */
        10, 					        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_MCU1 								/* HwHandleType */
    },
    /* CAN_FRAME_MCU2_MSG_RX */
    {
        0x523,					        /* Message Id */
        8,						        /* Message Length */
        100,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_MCU2 								/* HwHandleType */
    },
    /* CAN_FRAME_BMS1_MSG_RX */
    {
        0x670,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_BMS1 								/* HwHandleType */
    },
    /* CAN_FRAME_VSM2_MSG_RX */
    {
        0x165,					        /* Message Id */
        8,						        /* Message Length */
        10,					        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_VSM2 								/* HwHandleType */
    },
    /* CAN_FRAME_SAS1_MSG_RX */
    {
        0x2B0,					        /* Message Id */
        5,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_SAS 								/* HwHandleType */
    },
    /* CAN_FRAME_FATC1_MSG_RX */
    {
        0x651,					        /* Message Id */
        8,						        /* Message Length */
        100,					        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_FATC1 								/* HwHandleType */
    },
    /* CAN_FRAME_CLU1_MSG_RX */
    {
        0x4F0,					        /* Message Id */
        8,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_CLU1 								/* HwHandleType */
    },
    /* CAN_FRAME_CGW1_MSG_RX */
    {
        0x541,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_CGW1 								/* HwHandleType */
    },
    /* CAN_FRAME_EMS3_MSG_RX */
    {
        0x560,					        /* Message Id */
        8,						        /* Message Length */
        100,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_EMS3 								/* HwHandleType */
    },
    /* CAN_FRAME_DIAG_FUNC_MSG_RX */
    {
        0x7df,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
    	Can_17_MCanPConf_CanHardwareObject_DIAG_RX1 								/* HwHandleType */
    },
    /* CAN_FRAME_DIAG_PHY_MSG_RX */
    {
        0x7d1,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_DIAG_RX2 								/* HwHandleType */
    },
    /* CAN_FRAME_CLU2_MSG_RX */
    {
        0x690,					        /* Message Id */
        8,						        /* Message Length */
        100,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_CLU2 								/* HwHandleType */
    },
    /* CAN_FRAME_EPB1_MSG_RX */
    {
        0x433,					        /* Message Id */
        8,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_EPB1 								/* HwHandleType */
    },
    /* CAN_FRAME_YAWACC_MSG_RX */
    {
        0x188,					        /* Message Id */
        7,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_YAW_ACC 								/* HwHandleType */
    },
    /* CAN_FRAME_LONGACC_MSG_RX */
    {
        0x189,					        /* Message Id */
        3,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LONG_ACC 								/* HwHandleType */
    },
    /* CAN_FRAME_YAWSERIAL_MSG_RX */
    {
        0x528,					        /* Message Id */
        6,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_YAW_SERIAL 								/* HwHandleType */
    },
    /* CAN_FRAME_TCS1_MSG_TX */
    {
        0x153,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_TCS1 								/* HwHandleType */
    },
    /* CAN_FRAME_TCS5_MSG_TX */
    {
        0x1f1,					        /* Message Id */
        8,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_TCS5 								/* HwHandleType */
    },
    /* CAN_FRAME_ESP2_MSG_TX */
    {
        0x220,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_ESP2 								/* HwHandleType */
    },
    /* CAN_FRAME_CALSAS_MSG_TX */
    {
    	0x7c0,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_SAS_CAL 								/* HwHandleType */
    },
    /* CAN_FRAME_ESP1_MSG_TX */
    {
        0x47f,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_ESP1 								/* HwHandleType */
    },
    /* CAN_FRAME_VSM1_MSG_TX */
    {
        0x164,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_VSM1 								/* HwHandleType */
    },
    /* CAN_FRAME_WHLPUL_MSG_TX */
    {
        0x4b1,					        /* Message Id */
        8,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_WHL_PUL 								/* HwHandleType */
    },
    /* CAN_FRAME_AHB1_MSG_TX */
    {
        0x160,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_AHB1 								/* HwHandleType */
    },
    /* CAN_FRAME_EBS1_MSG_TX */
    {
        0x2a2,					        /* Message Id */
        8,						        /* Message Length */
        10,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_EBS1 								/* HwHandleType */
    },
    /* CAN_FRAME_WHLSPD_MSG_TX */
    {
        0x4b0,					        /* Message Id */
        8,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_WHL_SPD 								/* HwHandleType */
    },
    /* CAN_FRAME_WHLSPD_HEV_MSG_TX */
    {
        0xf00,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_WHL_SPD_HEV 								/* HwHandleType */
    },
    /* CAN_FRAME_DIAG_TX_MSG_TX */
    {
        0x7d9,					        /* Message Id */
        8,						        /* Message Length */
        0,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_DIAG_TX1 								/* HwHandleType */
    },
    /* CAN_FRAME_YAWCBIT_MSG_TX */
    {
        0x2f0,					        /* Message Id */
        8,						        /* Message Length */
        20,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_YAW_CBIT 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER0_MSG_TX */
    {
        0x6dc,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA0 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER1_MSG_TX */
    {
        0x6dd,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA1 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER2_MSG_TX */
    {
        0x6de,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA2 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER3_MSG_TX */
    {
        0x6df,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA3 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER4_MSG_TX */
    {
        0x6e0,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA4 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER5_MSG_TX */
    {
        0x6e1,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA5 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER6_MSG_TX */
    {
        0x6e2,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA6 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER7_MSG_TX */
    {
        0x6e3,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA7 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER8_MSG_TX */
    {
        0x6e4,					        /* Message Id */
        8,						        /* Message Length */
        5,						        /* Message Period */
	    Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA8 								/* HwHandleType */
    },
    /* CAN_FRAME_LOGGER9_MSG_TX */
    {
        0x6e5,                          /* Message Id */
        8,                              /* Message Length */
        5,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA9                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER10_MSG_TX */
    {
        0x6e6,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA10                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER11_MSG_TX */
    {
        0x6e7,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA11                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER12_MSG_TX */
    {
        0x6e8,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA12                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER13_MSG_TX */
    {
        0x6e9,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA13                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER14_MSG_TX */
    {
        0x6ea,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA14                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER15_MSG_TX */
    {
        0x6eb,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA15                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER16_MSG_TX */
    {
        0x6ec,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA16                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER17_MSG_TX */
    {
        0x6ed,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA17                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER18_MSG_TX */
    {
        0x6ee,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA18                                 /* HwHandleType */
    },
    /* CAN_FRAME_LOGGER19_MSG_TX */
    {
        0x6ef,                          /* Message Id */
        8,                              /* Message Length */
        10,                             /* Message Period */
        Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA19                                 /* HwHandleType */
    }
 };
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define COMMCAN_STOP_SEC_CONST_UNSPECIFIED
#include "Com_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define COMMCAN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Com_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
Com_FrameType  Com_FrameBuf[CAN_FRAME_MAX_NUM];

#define COMMCAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_NOINIT_32BIT
#include "Com_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define COMMCAN_STOP_SEC_VAR_NOINIT_32BIT
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define COMMCAN_STOP_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_32BIT
#include "Com_MemMap.h"
/** Variable Section (32BIT)**/


#define COMMCAN_STOP_SEC_VAR_32BIT
#include "Com_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define COMMCAN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Com_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define COMMCAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_NOINIT_32BIT
#include "Com_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define COMMCAN_STOP_SEC_VAR_NOINIT_32BIT
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define COMMCAN_STOP_SEC_VAR_UNSPECIFIED
#include "Com_MemMap.h"
#define COMMCAN_START_SEC_VAR_32BIT
#include "Com_MemMap.h"
/** Variable Section (32BIT)**/


#define COMMCAN_STOP_SEC_VAR_32BIT
#include "Com_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define COMMCAN_START_SEC_CODE
#include "Com_MemMap.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#define COMMCAN_STOP_SEC_CODE
#include "Com_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
