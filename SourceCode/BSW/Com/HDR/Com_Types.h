/*
 * Com_Types.h
 *
 *  Created on: 2014. 12. 24.
 *      Author: jinsu.park
 */

#ifndef COM_TYPES_H_
#define COM_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define COMMCAN_PATH_BSWINCLUDE( _FILENAME_ ) STRINGIFY(../../include/_FILENAME_)
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"

 #include "Common.h" //PJS
 #include "ComStack_Types.h"
 #include "Can_GeneralTypes.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
 typedef uint16 Com_SignalIdType;
 typedef uint32 Com_SignalDataType;
 typedef uint16 Com_FrameIdType;

 typedef union
 {
     uint8  B[8];     /* Data buffer in Bytes (8 bits) */
     uint16 H[4]; 	  /* Data buffer in Half-words (16 bits) */
     uint32 W[2];     /* Data buffer in words (32 bits) */
 }Com_FrameDataType;

 typedef struct
 {
	 uint16 MsgTimer;
	 uint16 PrevMsgTimer;
	 Com_FrameDataType Data;
 }Com_FrameType;

 /* Signal config type structure */
 typedef struct
 {
	 uint16			FrameId;
	 uint8			StartBit;
	 uint8			BitLength;
	 uint8			DataType;
 }Com_SigConfigType;

 /* Frame config type structure */
 typedef struct
 {
	 uint16			MsgId;
	 uint8			MsgDlc;
	 uint8			MsgPeriod;
	 uint8			ComHrhId;
 }Com_FrameConfigType;

 typedef enum
 {
	 DataType8bit = 8,
	 DataType16bit = 16,
	 DataType32bit = 32,
 };

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* COM_TYPES_H_ */
