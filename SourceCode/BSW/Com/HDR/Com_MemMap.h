/**
 * @defgroup CommCan_MemMap CommCan_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CommCan_MemMap.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef COMMCAN_MEMMAP_H_
#define COMMCAN_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (COMMCAN_START_SEC_CODE)
  #undef COMMCAN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_CODE)
  #undef COMMCAN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_CODE_STARTED
    #error "COMMCAN_SEC_CODE not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (COMMCAN_START_SEC_CONST_UNSPECIFIED)
  #undef COMMCAN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_CONST_UNSPECIFIED)
  #undef COMMCAN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_CONST_UNSPECIFIED_STARTED
    #error "COMMCAN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_START_SEC_CONST_32BIT)
  #undef COMMCAN_START_SEC_CONST_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_CONST_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_CONST_32BIT)
  #undef COMMCAN_STOP_SEC_CONST_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_CONST_32BIT_STARTED
    #error "COMMCAN_SEC_CONST_32BIT not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_CONST_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (COMMCAN_START_SEC_VAR_UNSPECIFIED)
  #undef COMMCAN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_VAR_UNSPECIFIED)
  #undef COMMCAN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_VAR_UNSPECIFIED_STARTED /* EB_BACFIX BAC-1046 */
    #error "COMMCAN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_START_SEC_VAR_32BIT)
  #undef COMMCAN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_VAR_32BIT)
  #undef COMMCAN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_VAR_32BIT_STARTED /* EB_BACFIX BAC-1046 */
    #error "COMMCAN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (COMMCAN_START_SEC_CALIB_UNSPECIFIED)
  #undef COMMCAN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef COMMCAN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_CALIB_UNSPECIFIED_STARTED /* EB_BACFIX BAC-1046 */
    #error "COMMCAN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_START_SEC_CALIB_32BIT)
  #undef COMMCAN_START_SEC_CALIB_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_COMMCAN_SEC_STARTED
    #error "COMMCAN section not closed"
  #endif
  #define CHK_COMMCAN_SEC_STARTED
  #define CHK_COMMCAN_SEC_CALIB_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (COMMCAN_STOP_SEC_CALIB_32BIT)
  #undef COMMCAN_STOP_SEC_CALIB_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_COMMCAN_SEC_CALIB_32BIT_STARTED /* EB_BACFIX BAC-1046 */
    #error "COMMCAN_SEC_CALIB_32BIT not opened"
  #endif
  #undef CHK_COMMCAN_SEC_STARTED
  #undef CHK_COMMCAN_SEC_CALIB_32BIT_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* COMMCAN_MEMMAP_H_ */
/** @} */
