#include "unity.h"
#include "unity_fixture.h"
#include "AxM_Main.h"
#include "AxM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_AxM_MainCanRxEscInfo_Ax[MAX_STEP] = AXM_MAINCANRXESCINFO_AX;
const Saluint8 UtInput_AxM_MainRxYawSerialInfo_YawSerialNum_0[MAX_STEP] = AXM_MAINRXYAWSERIALINFO_YAWSERIALNUM_0;
const Saluint8 UtInput_AxM_MainRxYawSerialInfo_YawSerialNum_1[MAX_STEP] = AXM_MAINRXYAWSERIALINFO_YAWSERIALNUM_1;
const Saluint8 UtInput_AxM_MainRxYawSerialInfo_YawSerialNum_2[MAX_STEP] = AXM_MAINRXYAWSERIALINFO_YAWSERIALNUM_2;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_YawRateValidData[MAX_STEP] = AXM_MAINRXYAWACCINFO_YAWRATEVALIDDATA;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_YawRateSelfTestStatus[MAX_STEP] = AXM_MAINRXYAWACCINFO_YAWRATESELFTESTSTATUS;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_SensorOscFreqDev[MAX_STEP] = AXM_MAINRXYAWACCINFO_SENSOROSCFREQDEV;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Gyro_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_GYRO_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Raster_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_RASTER_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Eep_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_EEP_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Batt_Range_Err[MAX_STEP] = AXM_MAINRXYAWACCINFO_BATT_RANGE_ERR;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Asic_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_ASIC_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Accel_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_ACCEL_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Ram_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_RAM_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Rom_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_ROM_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Ad_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_AD_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Osc_Fail[MAX_STEP] = AXM_MAINRXYAWACCINFO_OSC_FAIL;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Watchdog_Rst[MAX_STEP] = AXM_MAINRXYAWACCINFO_WATCHDOG_RST;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Plaus_Err_Pst[MAX_STEP] = AXM_MAINRXYAWACCINFO_PLAUS_ERR_PST;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_RollingCounter[MAX_STEP] = AXM_MAINRXYAWACCINFO_ROLLINGCOUNTER;
const Saluint8 UtInput_AxM_MainRxYawAccInfo_Can_Func_Err[MAX_STEP] = AXM_MAINRXYAWACCINFO_CAN_FUNC_ERR;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_IntSenFltSymtmActive[MAX_STEP] = AXM_MAINRXLONGACCINFO_INTSENFLTSYMTMACTIVE;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_IntSenFaultPresent[MAX_STEP] = AXM_MAINRXLONGACCINFO_INTSENFAULTPRESENT;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_LongAccSenCirErrPre[MAX_STEP] = AXM_MAINRXLONGACCINFO_LONGACCSENCIRERRPRE;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_LonACSenRanChkErrPre[MAX_STEP] = AXM_MAINRXLONGACCINFO_LONACSENRANCHKERRPRE;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_LongRollingCounter[MAX_STEP] = AXM_MAINRXLONGACCINFO_LONGROLLINGCOUNTER;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_IntTempSensorFault[MAX_STEP] = AXM_MAINRXLONGACCINFO_INTTEMPSENSORFAULT;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_LongAccInvalidData[MAX_STEP] = AXM_MAINRXLONGACCINFO_LONGACCINVALIDDATA;
const Saluint8 UtInput_AxM_MainRxLongAccInfo_LongAccSelfTstStatus[MAX_STEP] = AXM_MAINRXLONGACCINFO_LONGACCSELFTSTSTATUS;
const Saluint8 UtInput_AxM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[MAX_STEP] = AXM_MAINRXMSGOKFLGINFO_YAWSERIALMSGOKFLG;
const Saluint8 UtInput_AxM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[MAX_STEP] = AXM_MAINRXMSGOKFLGINFO_YAWACCMSGOKFLG;
const Saluint8 UtInput_AxM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[MAX_STEP] = AXM_MAINRXMSGOKFLGINFO_LONGACCMSGOKFLG;
const Saluint8 UtInput_AxM_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = AXM_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_AxM_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = AXM_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_AxM_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = AXM_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_AxM_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = AXM_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_AxM_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = AXM_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_AxM_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = AXM_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Mom_HndlrEcuModeSts_t UtInput_AxM_MainEcuModeSts[MAX_STEP] = AXM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_AxM_MainIgnOnOffSts[MAX_STEP] = AXM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_AxM_MainIgnEdgeSts[MAX_STEP] = AXM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_AxM_MainVBatt1Mon[MAX_STEP] = AXM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_AxM_MainDiagClrSrs[MAX_STEP] = AXM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_AxM_MainAXMOutInfo_AXM_Timeout_Err[MAX_STEP] = AXM_MAINAXMOUTINFO_AXM_TIMEOUT_ERR;
const Saluint8 UtExpected_AxM_MainAXMOutInfo_AXM_Invalid_Err[MAX_STEP] = AXM_MAINAXMOUTINFO_AXM_INVALID_ERR;
const Saluint8 UtExpected_AxM_MainAXMOutInfo_AXM_CRC_Err[MAX_STEP] = AXM_MAINAXMOUTINFO_AXM_CRC_ERR;
const Saluint8 UtExpected_AxM_MainAXMOutInfo_AXM_Rolling_Err[MAX_STEP] = AXM_MAINAXMOUTINFO_AXM_ROLLING_ERR;
const Saluint8 UtExpected_AxM_MainAXMOutInfo_AXM_Temperature_Err[MAX_STEP] = AXM_MAINAXMOUTINFO_AXM_TEMPERATURE_ERR;
const Saluint8 UtExpected_AxM_MainAXMOutInfo_AXM_Range_Err[MAX_STEP] = AXM_MAINAXMOUTINFO_AXM_RANGE_ERR;



TEST_GROUP(AxM_Main);
TEST_SETUP(AxM_Main)
{
    AxM_Main_Init();
}

TEST_TEAR_DOWN(AxM_Main)
{   /* Postcondition */

}

TEST(AxM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        AxM_MainCanRxEscInfo.Ax = UtInput_AxM_MainCanRxEscInfo_Ax[i];
        AxM_MainRxYawSerialInfo.YawSerialNum_0 = UtInput_AxM_MainRxYawSerialInfo_YawSerialNum_0[i];
        AxM_MainRxYawSerialInfo.YawSerialNum_1 = UtInput_AxM_MainRxYawSerialInfo_YawSerialNum_1[i];
        AxM_MainRxYawSerialInfo.YawSerialNum_2 = UtInput_AxM_MainRxYawSerialInfo_YawSerialNum_2[i];
        AxM_MainRxYawAccInfo.YawRateValidData = UtInput_AxM_MainRxYawAccInfo_YawRateValidData[i];
        AxM_MainRxYawAccInfo.YawRateSelfTestStatus = UtInput_AxM_MainRxYawAccInfo_YawRateSelfTestStatus[i];
        AxM_MainRxYawAccInfo.SensorOscFreqDev = UtInput_AxM_MainRxYawAccInfo_SensorOscFreqDev[i];
        AxM_MainRxYawAccInfo.Gyro_Fail = UtInput_AxM_MainRxYawAccInfo_Gyro_Fail[i];
        AxM_MainRxYawAccInfo.Raster_Fail = UtInput_AxM_MainRxYawAccInfo_Raster_Fail[i];
        AxM_MainRxYawAccInfo.Eep_Fail = UtInput_AxM_MainRxYawAccInfo_Eep_Fail[i];
        AxM_MainRxYawAccInfo.Batt_Range_Err = UtInput_AxM_MainRxYawAccInfo_Batt_Range_Err[i];
        AxM_MainRxYawAccInfo.Asic_Fail = UtInput_AxM_MainRxYawAccInfo_Asic_Fail[i];
        AxM_MainRxYawAccInfo.Accel_Fail = UtInput_AxM_MainRxYawAccInfo_Accel_Fail[i];
        AxM_MainRxYawAccInfo.Ram_Fail = UtInput_AxM_MainRxYawAccInfo_Ram_Fail[i];
        AxM_MainRxYawAccInfo.Rom_Fail = UtInput_AxM_MainRxYawAccInfo_Rom_Fail[i];
        AxM_MainRxYawAccInfo.Ad_Fail = UtInput_AxM_MainRxYawAccInfo_Ad_Fail[i];
        AxM_MainRxYawAccInfo.Osc_Fail = UtInput_AxM_MainRxYawAccInfo_Osc_Fail[i];
        AxM_MainRxYawAccInfo.Watchdog_Rst = UtInput_AxM_MainRxYawAccInfo_Watchdog_Rst[i];
        AxM_MainRxYawAccInfo.Plaus_Err_Pst = UtInput_AxM_MainRxYawAccInfo_Plaus_Err_Pst[i];
        AxM_MainRxYawAccInfo.RollingCounter = UtInput_AxM_MainRxYawAccInfo_RollingCounter[i];
        AxM_MainRxYawAccInfo.Can_Func_Err = UtInput_AxM_MainRxYawAccInfo_Can_Func_Err[i];
        AxM_MainRxLongAccInfo.IntSenFltSymtmActive = UtInput_AxM_MainRxLongAccInfo_IntSenFltSymtmActive[i];
        AxM_MainRxLongAccInfo.IntSenFaultPresent = UtInput_AxM_MainRxLongAccInfo_IntSenFaultPresent[i];
        AxM_MainRxLongAccInfo.LongAccSenCirErrPre = UtInput_AxM_MainRxLongAccInfo_LongAccSenCirErrPre[i];
        AxM_MainRxLongAccInfo.LonACSenRanChkErrPre = UtInput_AxM_MainRxLongAccInfo_LonACSenRanChkErrPre[i];
        AxM_MainRxLongAccInfo.LongRollingCounter = UtInput_AxM_MainRxLongAccInfo_LongRollingCounter[i];
        AxM_MainRxLongAccInfo.IntTempSensorFault = UtInput_AxM_MainRxLongAccInfo_IntTempSensorFault[i];
        AxM_MainRxLongAccInfo.LongAccInvalidData = UtInput_AxM_MainRxLongAccInfo_LongAccInvalidData[i];
        AxM_MainRxLongAccInfo.LongAccSelfTstStatus = UtInput_AxM_MainRxLongAccInfo_LongAccSelfTstStatus[i];
        AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = UtInput_AxM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[i];
        AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = UtInput_AxM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[i];
        AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = UtInput_AxM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[i];
        AxM_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_AxM_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_AxM_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        AxM_MainEemFailData.Eem_Fail_WssFL = UtInput_AxM_MainEemFailData_Eem_Fail_WssFL[i];
        AxM_MainEemFailData.Eem_Fail_WssFR = UtInput_AxM_MainEemFailData_Eem_Fail_WssFR[i];
        AxM_MainEemFailData.Eem_Fail_WssRL = UtInput_AxM_MainEemFailData_Eem_Fail_WssRL[i];
        AxM_MainEemFailData.Eem_Fail_WssRR = UtInput_AxM_MainEemFailData_Eem_Fail_WssRR[i];
        AxM_MainEcuModeSts = UtInput_AxM_MainEcuModeSts[i];
        AxM_MainIgnOnOffSts = UtInput_AxM_MainIgnOnOffSts[i];
        AxM_MainIgnEdgeSts = UtInput_AxM_MainIgnEdgeSts[i];
        AxM_MainVBatt1Mon = UtInput_AxM_MainVBatt1Mon[i];
        AxM_MainDiagClrSrs = UtInput_AxM_MainDiagClrSrs[i];

        AxM_Main();

        TEST_ASSERT_EQUAL(AxM_MainAXMOutInfo.AXM_Timeout_Err, UtExpected_AxM_MainAXMOutInfo_AXM_Timeout_Err[i]);
        TEST_ASSERT_EQUAL(AxM_MainAXMOutInfo.AXM_Invalid_Err, UtExpected_AxM_MainAXMOutInfo_AXM_Invalid_Err[i]);
        TEST_ASSERT_EQUAL(AxM_MainAXMOutInfo.AXM_CRC_Err, UtExpected_AxM_MainAXMOutInfo_AXM_CRC_Err[i]);
        TEST_ASSERT_EQUAL(AxM_MainAXMOutInfo.AXM_Rolling_Err, UtExpected_AxM_MainAXMOutInfo_AXM_Rolling_Err[i]);
        TEST_ASSERT_EQUAL(AxM_MainAXMOutInfo.AXM_Temperature_Err, UtExpected_AxM_MainAXMOutInfo_AXM_Temperature_Err[i]);
        TEST_ASSERT_EQUAL(AxM_MainAXMOutInfo.AXM_Range_Err, UtExpected_AxM_MainAXMOutInfo_AXM_Range_Err[i]);
    }
}

TEST_GROUP_RUNNER(AxM_Main)
{
    RUN_TEST_CASE(AxM_Main, All);
}
