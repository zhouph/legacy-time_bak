# \file
#
# \brief AxM
#
# This file contains the implementation of the SWC
# module AxM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += AxM_src

AxM_src_FILES        += $(AxM_SRC_PATH)\AxM_Main.c
AxM_src_FILES        += $(AxM_SRC_PATH)\AxM_Processing.c
AxM_src_FILES        += $(AxM_IFA_PATH)\AxM_Main_Ifa.c
AxM_src_FILES        += $(AxM_CFG_PATH)\AxM_Cfg.c
AxM_src_FILES        += $(AxM_CAL_PATH)\AxM_Cal.c

ifeq ($(ICE_COMPILE),true)
AxM_src_FILES        += $(AxM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	AxM_src_FILES        += $(AxM_UNITY_PATH)\unity.c
	AxM_src_FILES        += $(AxM_UNITY_PATH)\unity_fixture.c	
	AxM_src_FILES        += $(AxM_UT_PATH)\main.c
	AxM_src_FILES        += $(AxM_UT_PATH)\AxM_Main\AxM_Main_UtMain.c
endif