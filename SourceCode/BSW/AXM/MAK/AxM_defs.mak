# \file
#
# \brief AxM
#
# This file contains the implementation of the SWC
# module AxM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

AxM_CORE_PATH     := $(MANDO_BSW_ROOT)\AxM
AxM_CAL_PATH      := $(AxM_CORE_PATH)\CAL\$(AxM_VARIANT)
AxM_SRC_PATH      := $(AxM_CORE_PATH)\SRC
AxM_CFG_PATH      := $(AxM_CORE_PATH)\CFG\$(AxM_VARIANT)
AxM_HDR_PATH      := $(AxM_CORE_PATH)\HDR
AxM_IFA_PATH      := $(AxM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    AxM_CMN_PATH      := $(AxM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	AxM_UT_PATH		:= $(AxM_CORE_PATH)\UT
	AxM_UNITY_PATH	:= $(AxM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(AxM_UT_PATH)
	CC_INCLUDE_PATH		+= $(AxM_UNITY_PATH)
	AxM_Main_PATH 	:= AxM_UT_PATH\AxM_Main
endif
CC_INCLUDE_PATH    += $(AxM_CAL_PATH)
CC_INCLUDE_PATH    += $(AxM_SRC_PATH)
CC_INCLUDE_PATH    += $(AxM_CFG_PATH)
CC_INCLUDE_PATH    += $(AxM_HDR_PATH)
CC_INCLUDE_PATH    += $(AxM_IFA_PATH)
CC_INCLUDE_PATH    += $(AxM_CMN_PATH)

