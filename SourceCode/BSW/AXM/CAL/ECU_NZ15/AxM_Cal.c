/**
 * @defgroup AxM_Cal AxM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AXM_START_SEC_CALIB_UNSPECIFIED
#include "AxM_MemMap.h"
/* Global Calibration Section */


#define AXM_STOP_SEC_CALIB_UNSPECIFIED
#include "AxM_MemMap.h"

#define AXM_START_SEC_CONST_UNSPECIFIED
#include "AxM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AXM_STOP_SEC_CONST_UNSPECIFIED
#include "AxM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_START_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXM_STOP_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
#define AXM_START_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXM_STOP_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_START_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/** Variable Section (32BIT)**/


#define AXM_STOP_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_START_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXM_STOP_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
#define AXM_START_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXM_STOP_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_START_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/** Variable Section (32BIT)**/


#define AXM_STOP_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AXM_START_SEC_CODE
#include "AxM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AXM_STOP_SEC_CODE
#include "AxM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
