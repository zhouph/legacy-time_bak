#define S_FUNCTION_NAME      AxM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          43
#define WidthOutputPort         6

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "AxM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    AxM_MainCanRxEscInfo.Ax = input[0];
    AxM_MainRxYawSerialInfo.YawSerialNum_0 = input[1];
    AxM_MainRxYawSerialInfo.YawSerialNum_1 = input[2];
    AxM_MainRxYawSerialInfo.YawSerialNum_2 = input[3];
    AxM_MainRxYawAccInfo.YawRateValidData = input[4];
    AxM_MainRxYawAccInfo.YawRateSelfTestStatus = input[5];
    AxM_MainRxYawAccInfo.SensorOscFreqDev = input[6];
    AxM_MainRxYawAccInfo.Gyro_Fail = input[7];
    AxM_MainRxYawAccInfo.Raster_Fail = input[8];
    AxM_MainRxYawAccInfo.Eep_Fail = input[9];
    AxM_MainRxYawAccInfo.Batt_Range_Err = input[10];
    AxM_MainRxYawAccInfo.Asic_Fail = input[11];
    AxM_MainRxYawAccInfo.Accel_Fail = input[12];
    AxM_MainRxYawAccInfo.Ram_Fail = input[13];
    AxM_MainRxYawAccInfo.Rom_Fail = input[14];
    AxM_MainRxYawAccInfo.Ad_Fail = input[15];
    AxM_MainRxYawAccInfo.Osc_Fail = input[16];
    AxM_MainRxYawAccInfo.Watchdog_Rst = input[17];
    AxM_MainRxYawAccInfo.Plaus_Err_Pst = input[18];
    AxM_MainRxYawAccInfo.RollingCounter = input[19];
    AxM_MainRxYawAccInfo.Can_Func_Err = input[20];
    AxM_MainRxLongAccInfo.IntSenFltSymtmActive = input[21];
    AxM_MainRxLongAccInfo.IntSenFaultPresent = input[22];
    AxM_MainRxLongAccInfo.LongAccSenCirErrPre = input[23];
    AxM_MainRxLongAccInfo.LonACSenRanChkErrPre = input[24];
    AxM_MainRxLongAccInfo.LongRollingCounter = input[25];
    AxM_MainRxLongAccInfo.IntTempSensorFault = input[26];
    AxM_MainRxLongAccInfo.LongAccInvalidData = input[27];
    AxM_MainRxLongAccInfo.LongAccSelfTstStatus = input[28];
    AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = input[29];
    AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = input[30];
    AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = input[31];
    AxM_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[32];
    AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[33];
    AxM_MainEemFailData.Eem_Fail_WssFL = input[34];
    AxM_MainEemFailData.Eem_Fail_WssFR = input[35];
    AxM_MainEemFailData.Eem_Fail_WssRL = input[36];
    AxM_MainEemFailData.Eem_Fail_WssRR = input[37];
    AxM_MainEcuModeSts = input[38];
    AxM_MainIgnOnOffSts = input[39];
    AxM_MainIgnEdgeSts = input[40];
    AxM_MainVBatt1Mon = input[41];
    AxM_MainDiagClrSrs = input[42];

    AxM_Main();


    output[0] = AxM_MainAXMOutInfo.AXM_Timeout_Err;
    output[1] = AxM_MainAXMOutInfo.AXM_Invalid_Err;
    output[2] = AxM_MainAXMOutInfo.AXM_CRC_Err;
    output[3] = AxM_MainAXMOutInfo.AXM_Rolling_Err;
    output[4] = AxM_MainAXMOutInfo.AXM_Temperature_Err;
    output[5] = AxM_MainAXMOutInfo.AXM_Range_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    AxM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
