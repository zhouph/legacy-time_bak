/**
 * @defgroup AxM_Types AxM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXM_TYPES_H_
#define AXM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxEscInfo_t AxM_MainCanRxEscInfo;
    Proxy_RxByComRxYawSerialInfo_t AxM_MainRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t AxM_MainRxYawAccInfo;
    Proxy_RxByComRxLongAccInfo_t AxM_MainRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t AxM_MainRxMsgOkFlgInfo;
    SenPwrM_MainSenPwrMonitor_t AxM_MainSenPwrMonitorData;
    Eem_MainEemFailData_t AxM_MainEemFailData;
    Mom_HndlrEcuModeSts_t AxM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AxM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AxM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AxM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t AxM_MainDiagClrSrs;

/* Output Data Element */
    AxM_MainAXMOutInfo_t AxM_MainAXMOutInfo;
}AxM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AXM_TYPES_H_ */
/** @} */
