/**
 * @defgroup AxM_Main AxM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXM_MAIN_H_
#define AXM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxM_Types.h"
#include "AxM_Cfg.h"
#include "AxM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define AXM_MAIN_MODULE_ID      (0)
 #define AXM_MAIN_MAJOR_VERSION  (2)
 #define AXM_MAIN_MINOR_VERSION  (0)
 #define AXM_MAIN_PATCH_VERSION  (0)
 #define AXM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AxM_Main_HdrBusType AxM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t AxM_MainVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxEscInfo_t AxM_MainCanRxEscInfo;
extern Proxy_RxByComRxYawSerialInfo_t AxM_MainRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t AxM_MainRxYawAccInfo;
extern Proxy_RxByComRxLongAccInfo_t AxM_MainRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t AxM_MainRxMsgOkFlgInfo;
extern SenPwrM_MainSenPwrMonitor_t AxM_MainSenPwrMonitorData;
extern Eem_MainEemFailData_t AxM_MainEemFailData;
extern Mom_HndlrEcuModeSts_t AxM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t AxM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t AxM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t AxM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t AxM_MainDiagClrSrs;

/* Output Data Element */
extern AxM_MainAXMOutInfo_t AxM_MainAXMOutInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AxM_Main_Init(void);
extern void AxM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AXM_MAIN_H_ */
/** @} */
