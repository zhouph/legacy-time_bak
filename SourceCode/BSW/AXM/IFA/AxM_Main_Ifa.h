/**
 * @defgroup AxM_Main_Ifa AxM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXM_MAIN_IFA_H_
#define AXM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AxM_Main_Read_AxM_MainCanRxEscInfo(data) do \
{ \
    *data = AxM_MainCanRxEscInfo; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawSerialInfo(data) do \
{ \
    *data = AxM_MainRxYawSerialInfo; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo(data) do \
{ \
    *data = AxM_MainRxYawAccInfo; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo(data) do \
{ \
    *data = AxM_MainRxLongAccInfo; \
}while(0);

#define AxM_Main_Read_AxM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = AxM_MainRxMsgOkFlgInfo; \
}while(0);

#define AxM_Main_Read_AxM_MainSenPwrMonitorData(data) do \
{ \
    *data = AxM_MainSenPwrMonitorData; \
}while(0);

#define AxM_Main_Read_AxM_MainEemFailData(data) do \
{ \
    *data = AxM_MainEemFailData; \
}while(0);

#define AxM_Main_Read_AxM_MainCanRxEscInfo_Ax(data) do \
{ \
    *data = AxM_MainCanRxEscInfo.Ax; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = AxM_MainRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = AxM_MainRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = AxM_MainRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.YawRateValidData; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Gyro_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Raster_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Eep_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Asic_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Accel_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Ram_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Rom_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Ad_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Osc_Fail; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.RollingCounter; \
}while(0);

#define AxM_Main_Read_AxM_MainRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = AxM_MainRxYawAccInfo.Can_Func_Err; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.LongRollingCounter; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_LongAccInvalidData(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.LongAccInvalidData; \
}while(0);

#define AxM_Main_Read_AxM_MainRxLongAccInfo_LongAccSelfTstStatus(data) do \
{ \
    *data = AxM_MainRxLongAccInfo.LongAccSelfTstStatus; \
}while(0);

#define AxM_Main_Read_AxM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define AxM_Main_Read_AxM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define AxM_Main_Read_AxM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define AxM_Main_Read_AxM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = AxM_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define AxM_Main_Read_AxM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = AxM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = AxM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = AxM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = AxM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define AxM_Main_Read_AxM_MainEcuModeSts(data) do \
{ \
    *data = AxM_MainEcuModeSts; \
}while(0);

#define AxM_Main_Read_AxM_MainIgnOnOffSts(data) do \
{ \
    *data = AxM_MainIgnOnOffSts; \
}while(0);

#define AxM_Main_Read_AxM_MainIgnEdgeSts(data) do \
{ \
    *data = AxM_MainIgnEdgeSts; \
}while(0);

#define AxM_Main_Read_AxM_MainVBatt1Mon(data) do \
{ \
    *data = AxM_MainVBatt1Mon; \
}while(0);

#define AxM_Main_Read_AxM_MainDiagClrSrs(data) do \
{ \
    *data = AxM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define AxM_Main_Write_AxM_MainAXMOutInfo(data) do \
{ \
    AxM_MainAXMOutInfo = *data; \
}while(0);

#define AxM_Main_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define AxM_Main_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define AxM_Main_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define AxM_Main_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define AxM_Main_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define AxM_Main_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AXM_MAIN_IFA_H_ */
/** @} */
