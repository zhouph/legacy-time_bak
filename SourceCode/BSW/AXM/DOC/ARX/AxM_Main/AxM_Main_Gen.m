AxM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    };
AxM_MainCanRxEscInfo = CreateBus(AxM_MainCanRxEscInfo, DeList);
clear DeList;

AxM_MainRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
AxM_MainRxYawSerialInfo = CreateBus(AxM_MainRxYawSerialInfo, DeList);
clear DeList;

AxM_MainRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    };
AxM_MainRxYawAccInfo = CreateBus(AxM_MainRxYawAccInfo, DeList);
clear DeList;

AxM_MainRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    };
AxM_MainRxLongAccInfo = CreateBus(AxM_MainRxLongAccInfo, DeList);
clear DeList;

AxM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'LongAccMsgOkFlg'
    };
AxM_MainRxMsgOkFlgInfo = CreateBus(AxM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

AxM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AxM_MainSenPwrMonitorData = CreateBus(AxM_MainSenPwrMonitorData, DeList);
clear DeList;

AxM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
AxM_MainEemFailData = CreateBus(AxM_MainEemFailData, DeList);
clear DeList;

AxM_MainEcuModeSts = Simulink.Bus;
DeList={'AxM_MainEcuModeSts'};
AxM_MainEcuModeSts = CreateBus(AxM_MainEcuModeSts, DeList);
clear DeList;

AxM_MainIgnOnOffSts = Simulink.Bus;
DeList={'AxM_MainIgnOnOffSts'};
AxM_MainIgnOnOffSts = CreateBus(AxM_MainIgnOnOffSts, DeList);
clear DeList;

AxM_MainIgnEdgeSts = Simulink.Bus;
DeList={'AxM_MainIgnEdgeSts'};
AxM_MainIgnEdgeSts = CreateBus(AxM_MainIgnEdgeSts, DeList);
clear DeList;

AxM_MainVBatt1Mon = Simulink.Bus;
DeList={'AxM_MainVBatt1Mon'};
AxM_MainVBatt1Mon = CreateBus(AxM_MainVBatt1Mon, DeList);
clear DeList;

AxM_MainDiagClrSrs = Simulink.Bus;
DeList={'AxM_MainDiagClrSrs'};
AxM_MainDiagClrSrs = CreateBus(AxM_MainDiagClrSrs, DeList);
clear DeList;

AxM_MainAXMOutInfo = Simulink.Bus;
DeList={
    'AXM_Timeout_Err'
    'AXM_Invalid_Err'
    'AXM_CRC_Err'
    'AXM_Rolling_Err'
    'AXM_Temperature_Err'
    'AXM_Range_Err'
    };
AxM_MainAXMOutInfo = CreateBus(AxM_MainAXMOutInfo, DeList);
clear DeList;

