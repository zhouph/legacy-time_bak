/**
 * @defgroup AxM_Main AxM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxM_Processing.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxM_Main.h"
#include "AxM_Processing.h"

#include "common.h"

#include <stdlib.h>

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AXM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AxM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AXM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AxM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/** Variable Section (32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_32BIT
#include "AxM_MemMap.h"

extern signed int Proxy_a_long_1_1000g; /* TODO */
extern Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo; /* TODO */

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/** Variable Section (32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_32BIT
#include "AxM_MemMap.h"

boolean gAxM_bInit;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AXM_MAIN_START_SEC_CODE
#include "AxM_MemMap.h"

static boolean AxM_CheckInhibit(AxM_Main_HdrBusType *pAxMInfo);
static void AxM_CheckTimeout(AxM_Main_HdrBusType *pAxMInfo);
static void AxM_CheckSum(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit);
static void AxM_RollingCnt(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit);
static void AxM_Invalid(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit);
static void AxM_Temperature(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit);
static void AxM_Range(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AxM_Processing(AxM_Main_HdrBusType *pAxMInfo)
{
    boolean bInhibit = FALSE;

    if(gAxM_bInit == TRUE)
    {
        AxM_CheckTimeout(pAxMInfo);
        
        bInhibit = AxM_CheckInhibit(pAxMInfo);

        AxM_CheckSum(pAxMInfo, bInhibit);
        AxM_RollingCnt(pAxMInfo, bInhibit);
        AxM_Invalid(pAxMInfo, bInhibit);
        AxM_Temperature(pAxMInfo, bInhibit);
        AxM_Range(pAxMInfo, bInhibit);
    }
}

void AxM_Initialize(void)
{
    gAxM_bInit = TRUE;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static boolean AxM_CheckInhibit(AxM_Main_HdrBusType *pAxMInfo)
{
    boolean bInhibit = FALSE;
    
    if((pAxMInfo->AxM_MainAXMOutInfo.AXM_Timeout_Err == ERR_PREFAILED) ||
        (pAxMInfo->AxM_MainAXMOutInfo.AXM_Timeout_Err == ERR_INHIBIT)
      )
    {
        bInhibit = TRUE;
    }

    return bInhibit;
}

static void AxM_CheckTimeout(AxM_Main_HdrBusType *pAxMInfo)
{
    boolean inhibit = FALSE;

    if((pAxMInfo->AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable == FALSE) ||
       (Proxy_RxCanRxInfo.SenBusOffFlag == TRUE)
      )
    {
        inhibit = TRUE;
    }
    
    if(pAxMInfo->AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg == 1)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Timeout_Err = ERR_PREPASSED;
    }
    else
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Timeout_Err = ERR_PREFAILED;
    }
    
    if(inhibit==TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Timeout_Err = ERR_INHIBIT;
    }
}

static void AxM_CheckSum(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit)
{
    pAxMInfo->AxM_MainAXMOutInfo.AXM_CRC_Err = NONE; /* for GM Variant */

    if(inhibit==TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_CRC_Err = ERR_INHIBIT;
    }
}

static void AxM_RollingCnt(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit)
{
    pAxMInfo->AxM_MainAXMOutInfo.AXM_Rolling_Err = NONE; /* TODO New Concept */
    
    if(inhibit==TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Rolling_Err = ERR_INHIBIT;
    }
}

static void AxM_Invalid(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit)
{
    boolean bAxInvalid = FALSE;

    if((pAxMInfo->AxM_MainRxLongAccInfo.IntTempSensorFault == 1) ||
       (pAxMInfo->AxM_MainRxLongAccInfo.IntSenFaultPresent == 1) ||
       (pAxMInfo->AxM_MainRxLongAccInfo.IntSenFltSymtmActive == 1) ||
       (pAxMInfo->AxM_MainRxLongAccInfo.LongAccSenCirErrPre == 1) ||
       (pAxMInfo->AxM_MainRxLongAccInfo.LonACSenRanChkErrPre == 1) ||
       (pAxMInfo->AxM_MainRxLongAccInfo.LongAccInvalidData == 1) 
      )
    {
        bAxInvalid = TRUE;
    }

    if(bAxInvalid == TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Invalid_Err = ERR_PREFAILED;
    }
    else
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Invalid_Err = ERR_PREPASSED;
    }
    
    if(inhibit==TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Invalid_Err = ERR_INHIBIT;
    }
}

static void AxM_Temperature(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit)
{
    if(pAxMInfo->AxM_MainRxLongAccInfo.IntTempSensorFault == 1)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Temperature_Err = ERR_PREFAILED;
    }
    else
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Temperature_Err = ERR_PREPASSED;
    }
    
    if(inhibit==TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Temperature_Err = ERR_INHIBIT;
    }
}

static void AxM_Range(AxM_Main_HdrBusType *pAxMInfo, boolean inhibit)
{
    unsigned int abs_u16EstimatedAx = 0;

    abs_u16EstimatedAx = abs(Proxy_a_long_1_1000g);

    /* TODO : New Concept
                    if((rev_for_judge_time<=0)&&(fu1CLSTEEROK==1)&&(Temp_Abs_STR_Mdl_0_<U16_STEER_300DEG))
    */
    
    if(abs_u16EstimatedAx >= AX_RANGE_ERR_DET_THRES)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Range_Err = ERR_PREFAILED;
    }
    else
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Range_Err = ERR_PREPASSED;
    }
    
    if(inhibit==TRUE)
    {
        pAxMInfo->AxM_MainAXMOutInfo.AXM_Range_Err = ERR_INHIBIT;
    }
}

#define AXM_MAIN_STOP_SEC_CODE
#include "AxM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
