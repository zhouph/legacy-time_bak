/**
 * @defgroup AxM_Main AxM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxM_Main.h"
#include "AxM_Main_Ifa.h"
#include "AxM_Processing.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AXM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AxM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AXM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AxM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AxM_Main_HdrBusType AxM_MainBus;

/* Version Info */
const SwcVersionInfo_t AxM_MainVersionInfo = 
{   
    AXM_MAIN_MODULE_ID,           /* AxM_MainVersionInfo.ModuleId */
    AXM_MAIN_MAJOR_VERSION,       /* AxM_MainVersionInfo.MajorVer */
    AXM_MAIN_MINOR_VERSION,       /* AxM_MainVersionInfo.MinorVer */
    AXM_MAIN_PATCH_VERSION,       /* AxM_MainVersionInfo.PatchVer */
    AXM_MAIN_BRANCH_VERSION       /* AxM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxEscInfo_t AxM_MainCanRxEscInfo;
Proxy_RxByComRxYawSerialInfo_t AxM_MainRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t AxM_MainRxYawAccInfo;
Proxy_RxByComRxLongAccInfo_t AxM_MainRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t AxM_MainRxMsgOkFlgInfo;
SenPwrM_MainSenPwrMonitor_t AxM_MainSenPwrMonitorData;
Eem_MainEemFailData_t AxM_MainEemFailData;
Mom_HndlrEcuModeSts_t AxM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t AxM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t AxM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t AxM_MainVBatt1Mon;
Diag_HndlrDiagClr_t AxM_MainDiagClrSrs;

/* Output Data Element */
AxM_MainAXMOutInfo_t AxM_MainAXMOutInfo;

uint32 AxM_Main_Timer_Start;
uint32 AxM_Main_Timer_Elapsed;

#define AXM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/** Variable Section (32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxM_MemMap.h"
#define AXM_MAIN_START_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/** Variable Section (32BIT)**/


#define AXM_MAIN_STOP_SEC_VAR_32BIT
#include "AxM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AXM_MAIN_START_SEC_CODE
#include "AxM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AxM_Main_Init(void)
{
    /* Initialize internal bus */
    AxM_MainBus.AxM_MainCanRxEscInfo.Ax = 0;
    AxM_MainBus.AxM_MainRxYawSerialInfo.YawSerialNum_0 = 0;
    AxM_MainBus.AxM_MainRxYawSerialInfo.YawSerialNum_1 = 0;
    AxM_MainBus.AxM_MainRxYawSerialInfo.YawSerialNum_2 = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.YawRateValidData = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.YawRateSelfTestStatus = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.SensorOscFreqDev = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Gyro_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Raster_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Eep_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Batt_Range_Err = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Asic_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Accel_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Ram_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Rom_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Ad_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Osc_Fail = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Watchdog_Rst = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Plaus_Err_Pst = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.RollingCounter = 0;
    AxM_MainBus.AxM_MainRxYawAccInfo.Can_Func_Err = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.IntSenFltSymtmActive = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.IntSenFaultPresent = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.LongAccSenCirErrPre = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.LonACSenRanChkErrPre = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.LongRollingCounter = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.IntTempSensorFault = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.LongAccInvalidData = 0;
    AxM_MainBus.AxM_MainRxLongAccInfo.LongAccSelfTstStatus = 0;
    AxM_MainBus.AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = 0;
    AxM_MainBus.AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = 0;
    AxM_MainBus.AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = 0;
    AxM_MainBus.AxM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    AxM_MainBus.AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssFL = 0;
    AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssFR = 0;
    AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssRL = 0;
    AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssRR = 0;
    AxM_MainBus.AxM_MainEcuModeSts = 0;
    AxM_MainBus.AxM_MainIgnOnOffSts = 0;
    AxM_MainBus.AxM_MainIgnEdgeSts = 0;
    AxM_MainBus.AxM_MainVBatt1Mon = 0;
    AxM_MainBus.AxM_MainDiagClrSrs = 0;
    AxM_MainBus.AxM_MainAXMOutInfo.AXM_Timeout_Err = 0;
    AxM_MainBus.AxM_MainAXMOutInfo.AXM_Invalid_Err = 0;
    AxM_MainBus.AxM_MainAXMOutInfo.AXM_CRC_Err = 0;
    AxM_MainBus.AxM_MainAXMOutInfo.AXM_Rolling_Err = 0;
    AxM_MainBus.AxM_MainAXMOutInfo.AXM_Temperature_Err = 0;
    AxM_MainBus.AxM_MainAXMOutInfo.AXM_Range_Err = 0;
}

void AxM_Main(void)
{
    AxM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    AxM_Main_Read_AxM_MainCanRxEscInfo_Ax(&AxM_MainBus.AxM_MainCanRxEscInfo.Ax);

    AxM_Main_Read_AxM_MainRxYawSerialInfo(&AxM_MainBus.AxM_MainRxYawSerialInfo);
    /*==============================================================================
    * Members of structure AxM_MainRxYawSerialInfo 
     : AxM_MainRxYawSerialInfo.YawSerialNum_0;
     : AxM_MainRxYawSerialInfo.YawSerialNum_1;
     : AxM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/
    
    /* Decomposed structure interface */
    AxM_Main_Read_AxM_MainRxYawAccInfo_YawRateValidData(&AxM_MainBus.AxM_MainRxYawAccInfo.YawRateValidData);
    AxM_Main_Read_AxM_MainRxYawAccInfo_YawRateSelfTestStatus(&AxM_MainBus.AxM_MainRxYawAccInfo.YawRateSelfTestStatus);
    AxM_Main_Read_AxM_MainRxYawAccInfo_SensorOscFreqDev(&AxM_MainBus.AxM_MainRxYawAccInfo.SensorOscFreqDev);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Gyro_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Gyro_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Raster_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Raster_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Eep_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Eep_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Batt_Range_Err(&AxM_MainBus.AxM_MainRxYawAccInfo.Batt_Range_Err);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Asic_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Asic_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Accel_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Accel_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Ram_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Ram_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Rom_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Rom_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Ad_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Ad_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Osc_Fail(&AxM_MainBus.AxM_MainRxYawAccInfo.Osc_Fail);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Watchdog_Rst(&AxM_MainBus.AxM_MainRxYawAccInfo.Watchdog_Rst);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Plaus_Err_Pst(&AxM_MainBus.AxM_MainRxYawAccInfo.Plaus_Err_Pst);
    AxM_Main_Read_AxM_MainRxYawAccInfo_RollingCounter(&AxM_MainBus.AxM_MainRxYawAccInfo.RollingCounter);
    AxM_Main_Read_AxM_MainRxYawAccInfo_Can_Func_Err(&AxM_MainBus.AxM_MainRxYawAccInfo.Can_Func_Err);

    /* Decomposed structure interface */
    AxM_Main_Read_AxM_MainRxLongAccInfo_IntSenFltSymtmActive(&AxM_MainBus.AxM_MainRxLongAccInfo.IntSenFltSymtmActive);
    AxM_Main_Read_AxM_MainRxLongAccInfo_IntSenFaultPresent(&AxM_MainBus.AxM_MainRxLongAccInfo.IntSenFaultPresent);
    AxM_Main_Read_AxM_MainRxLongAccInfo_LongAccSenCirErrPre(&AxM_MainBus.AxM_MainRxLongAccInfo.LongAccSenCirErrPre);
    AxM_Main_Read_AxM_MainRxLongAccInfo_LonACSenRanChkErrPre(&AxM_MainBus.AxM_MainRxLongAccInfo.LonACSenRanChkErrPre);
    AxM_Main_Read_AxM_MainRxLongAccInfo_LongRollingCounter(&AxM_MainBus.AxM_MainRxLongAccInfo.LongRollingCounter);
    AxM_Main_Read_AxM_MainRxLongAccInfo_IntTempSensorFault(&AxM_MainBus.AxM_MainRxLongAccInfo.IntTempSensorFault);
    AxM_Main_Read_AxM_MainRxLongAccInfo_LongAccInvalidData(&AxM_MainBus.AxM_MainRxLongAccInfo.LongAccInvalidData);
    AxM_Main_Read_AxM_MainRxLongAccInfo_LongAccSelfTstStatus(&AxM_MainBus.AxM_MainRxLongAccInfo.LongAccSelfTstStatus);

    /* Decomposed structure interface */
    AxM_Main_Read_AxM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(&AxM_MainBus.AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg);
    AxM_Main_Read_AxM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(&AxM_MainBus.AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg);
    AxM_Main_Read_AxM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(&AxM_MainBus.AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg);

    /* Decomposed structure interface */
    AxM_Main_Read_AxM_MainSenPwrMonitorData_SenPwrM_5V_Stable(&AxM_MainBus.AxM_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    AxM_Main_Read_AxM_MainSenPwrMonitorData_SenPwrM_12V_Stable(&AxM_MainBus.AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssFL(&AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssFL);
    AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssFR(&AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssFR);
    AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssRL(&AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssRL);
    AxM_Main_Read_AxM_MainEemFailData_Eem_Fail_WssRR(&AxM_MainBus.AxM_MainEemFailData.Eem_Fail_WssRR);

    AxM_Main_Read_AxM_MainEcuModeSts(&AxM_MainBus.AxM_MainEcuModeSts);
    AxM_Main_Read_AxM_MainIgnOnOffSts(&AxM_MainBus.AxM_MainIgnOnOffSts);
    AxM_Main_Read_AxM_MainIgnEdgeSts(&AxM_MainBus.AxM_MainIgnEdgeSts);
    AxM_Main_Read_AxM_MainVBatt1Mon(&AxM_MainBus.AxM_MainVBatt1Mon);
    AxM_Main_Read_AxM_MainDiagClrSrs(&AxM_MainBus.AxM_MainDiagClrSrs);

    /* Process */
    AxM_Processing(&AxM_MainBus);

    /* Output */
    AxM_Main_Write_AxM_MainAXMOutInfo(&AxM_MainBus.AxM_MainAXMOutInfo);
    /*==============================================================================
    * Members of structure AxM_MainAXMOutInfo 
     : AxM_MainAXMOutInfo.AXM_Timeout_Err;
     : AxM_MainAXMOutInfo.AXM_Invalid_Err;
     : AxM_MainAXMOutInfo.AXM_CRC_Err;
     : AxM_MainAXMOutInfo.AXM_Rolling_Err;
     : AxM_MainAXMOutInfo.AXM_Temperature_Err;
     : AxM_MainAXMOutInfo.AXM_Range_Err;
     =============================================================================*/
    

    AxM_Main_Timer_Elapsed = STM0_TIM0.U - AxM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AXM_MAIN_STOP_SEC_CODE
#include "AxM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
