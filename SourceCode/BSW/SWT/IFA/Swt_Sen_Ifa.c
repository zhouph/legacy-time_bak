/**
 * @defgroup Swt_Sen_Ifa Swt_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Swt_Sen_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Swt_Sen_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWT_SEN_START_SEC_CONST_UNSPECIFIED
#include "Swt_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWT_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Swt_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWT_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWT_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWT_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/** Variable Section (32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWT_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWT_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWT_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/** Variable Section (32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWT_SEN_START_SEC_CODE
#include "Swt_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWT_SEN_STOP_SEC_CODE
#include "Swt_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
