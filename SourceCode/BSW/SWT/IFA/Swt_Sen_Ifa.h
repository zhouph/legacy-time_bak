/**
 * @defgroup Swt_Sen_Ifa Swt_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Swt_Sen_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWT_SEN_IFA_H_
#define SWT_SEN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Swt_Sen_Read_Swt_SenSwtMonInfo(data) do \
{ \
    *data = Swt_SenSwtMonInfo; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_AvhSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.AvhSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_BlsSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.BlsSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_BsSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.BsSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_DoorSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.DoorSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_EscSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.EscSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_FlexBrkASwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.FlexBrkASwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_FlexBrkBSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.FlexBrkBSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_HzrdSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.HzrdSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_HdcSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.HdcSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfo_PbSwtMon(data) do \
{ \
    *data = Swt_SenSwtMonInfo.PbSwtMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_BlsSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_AvhSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_EscSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.EscSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_HdcSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_PbSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.PbSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_GearRSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_BlfSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_ClutchSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenSwtMonInfoEsc_ItpmsSwtMon_Esc(data) do \
{ \
    *data = Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc; \
}while(0);

#define Swt_Sen_Read_Swt_SenEcuModeSts(data) do \
{ \
    *data = Swt_SenEcuModeSts; \
}while(0);

#define Swt_Sen_Read_Swt_SenRsmDbcMon(data) do \
{ \
    *data = Swt_SenRsmDbcMon; \
}while(0);

#define Swt_Sen_Read_Swt_SenFuncInhibitSwtSts(data) do \
{ \
    *data = Swt_SenFuncInhibitSwtSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Swt_Sen_Write_Swt_SenSwtStsFlexBrkInfo(data) do \
{ \
    Swt_SenSwtStsFlexBrkInfo = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc(data) do \
{ \
    Swt_SenSwtInfoEsc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenEscSwtStInfo(data) do \
{ \
    Swt_SenEscSwtStInfo = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtStsFlexBrkInfo_FlexBrkASwt(data) do \
{ \
    Swt_SenSwtStsFlexBrkInfo.FlexBrkASwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtStsFlexBrkInfo_FlexBrkBSwt(data) do \
{ \
    Swt_SenSwtStsFlexBrkInfo.FlexBrkBSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_BlsSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.BlsSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_AvhSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.AvhSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_EscSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.EscSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_HdcSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.HdcSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_PbSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.PbSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_GearRSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.GearRSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_BlfSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.BlfSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_ClutchSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.ClutchSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenSwtInfoEsc_ItpmsSwt_Esc(data) do \
{ \
    Swt_SenSwtInfoEsc.ItpmsSwt_Esc = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    Swt_SenEscSwtStInfo.EscDisabledBySwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenEscSwtStInfo_TcsDisabledBySwt(data) do \
{ \
    Swt_SenEscSwtStInfo.TcsDisabledBySwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenEscSwtStInfo_HdcEnabledBySwt(data) do \
{ \
    Swt_SenEscSwtStInfo.HdcEnabledBySwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenBlsSwt(data) do \
{ \
    Swt_SenBlsSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenEscSwt(data) do \
{ \
    Swt_SenEscSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenAvhSwt(data) do \
{ \
    Swt_SenAvhSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenBsSwt(data) do \
{ \
    Swt_SenBsSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenDoorSwt(data) do \
{ \
    Swt_SenDoorSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenHzrdSwt(data) do \
{ \
    Swt_SenHzrdSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenHdcSwt(data) do \
{ \
    Swt_SenHdcSwt = *data; \
}while(0);

#define Swt_Sen_Write_Swt_SenPbSwt(data) do \
{ \
    Swt_SenPbSwt = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWT_SEN_IFA_H_ */
/** @} */
