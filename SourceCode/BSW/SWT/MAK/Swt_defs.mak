# \file
#
# \brief Swt
#
# This file contains the implementation of the SWC
# module Swt.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Swt_CORE_PATH     := $(MANDO_BSW_ROOT)\Swt
Swt_CAL_PATH      := $(Swt_CORE_PATH)\CAL\$(Swt_VARIANT)
Swt_SRC_PATH      := $(Swt_CORE_PATH)\SRC
Swt_CFG_PATH      := $(Swt_CORE_PATH)\CFG\$(Swt_VARIANT)
Swt_HDR_PATH      := $(Swt_CORE_PATH)\HDR
Swt_IFA_PATH      := $(Swt_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Swt_CMN_PATH      := $(Swt_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Swt_UT_PATH		:= $(Swt_CORE_PATH)\UT
	Swt_UNITY_PATH	:= $(Swt_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Swt_UT_PATH)
	CC_INCLUDE_PATH		+= $(Swt_UNITY_PATH)
	Swt_Sen_PATH 	:= Swt_UT_PATH\Swt_Sen
endif
CC_INCLUDE_PATH    += $(Swt_CAL_PATH)
CC_INCLUDE_PATH    += $(Swt_SRC_PATH)
CC_INCLUDE_PATH    += $(Swt_CFG_PATH)
CC_INCLUDE_PATH    += $(Swt_HDR_PATH)
CC_INCLUDE_PATH    += $(Swt_IFA_PATH)
CC_INCLUDE_PATH    += $(Swt_CMN_PATH)

