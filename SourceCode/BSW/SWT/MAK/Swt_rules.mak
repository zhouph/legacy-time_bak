# \file
#
# \brief Swt
#
# This file contains the implementation of the SWC
# module Swt.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Swt_src

Swt_src_FILES        += $(Swt_SRC_PATH)\Swt_Sen.c
Swt_src_FILES        += $(Swt_IFA_PATH)\Swt_Sen_Ifa.c
Swt_src_FILES        += $(Swt_CFG_PATH)\Swt_Cfg.c
Swt_src_FILES        += $(Swt_CAL_PATH)\Swt_Cal.c

ifeq ($(ICE_COMPILE),true)
Swt_src_FILES        += $(Swt_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Swt_src_FILES        += $(Swt_UNITY_PATH)\unity.c
	Swt_src_FILES        += $(Swt_UNITY_PATH)\unity_fixture.c	
	Swt_src_FILES        += $(Swt_UT_PATH)\main.c
	Swt_src_FILES        += $(Swt_UT_PATH)\Swt_Sen\Swt_Sen_UtMain.c
endif