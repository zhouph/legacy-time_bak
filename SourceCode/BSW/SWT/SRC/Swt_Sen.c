/**
 * @defgroup Swt_Sen Swt_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Swt_Sen.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Swt_Sen.h"
#include "Swt_Sen_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 #define SW_LOOP_TIME   5 /* ms */
#define	F_SWITCH_OFF_TIME					(150/SW_LOOP_TIME)	/* 150ms */
#define	F_TCS_DISABLED_SWITCH_TIME			(150/SW_LOOP_TIME)	/* 150ms */
#define	F_ESC_DISABLED_SWITCH_TIME			(150/SW_LOOP_TIME)	/* 150ms */
#define ESC_SWITCH_ERROR_TIME               (60000/SW_LOOP_TIME)	/* 60s */
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWT_SEN_START_SEC_CONST_UNSPECIFIED
#include "Swt_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWT_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Swt_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWT_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Swt_Sen_HdrBusType Swt_SenBus;

/* Version Info */
const SwcVersionInfo_t Swt_SenVersionInfo = 
{   
    SWT_SEN_MODULE_ID,           /* Swt_SenVersionInfo.ModuleId */
    SWT_SEN_MAJOR_VERSION,       /* Swt_SenVersionInfo.MajorVer */
    SWT_SEN_MINOR_VERSION,       /* Swt_SenVersionInfo.MinorVer */
    SWT_SEN_PATCH_VERSION,       /* Swt_SenVersionInfo.PatchVer */
    SWT_SEN_BRANCH_VERSION       /* Swt_SenVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputCS1msSwtMonInfo_t Swt_SenSwtMonInfo;
Ioc_InputCS1msSwtMonInfoEsc_t Swt_SenSwtMonInfoEsc;
Mom_HndlrEcuModeSts_t Swt_SenEcuModeSts;
Ioc_InputCS1msRsmDbcMon_t Swt_SenRsmDbcMon;
Eem_SuspcDetnFuncInhibitSwtSts_t Swt_SenFuncInhibitSwtSts;

/* Output Data Element */
Swt_SenSwtStsFlexBrkInfo_t Swt_SenSwtStsFlexBrkInfo;
Swt_SenSwtInfoEsc_t Swt_SenSwtInfoEsc;
Swt_SenEscSwtStInfo_t Swt_SenEscSwtStInfo;
Swt_SenBlsSwt_t Swt_SenBlsSwt;
Swt_SenEscSwt_t Swt_SenEscSwt;
Swt_SenAvhSwt_t Swt_SenAvhSwt;
Swt_SenBsSwt_t Swt_SenBsSwt;
Swt_SenDoorSwt_t Swt_SenDoorSwt;
Swt_SenHzrdSwt_t Swt_SenHzrdSwt;
Swt_SenHdcSwt_t Swt_SenHdcSwt;
Swt_SenPbSwt_t Swt_SenPbSwt;

uint32 Swt_Sen_Timer_Start;
uint32 Swt_Sen_Timer_Elapsed;

#define SWT_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWT_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/** Variable Section (32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWT_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
static Sw_FiltTimeType SwOnTime[SW_IN_PORT_MAX_NUM];
static Sw_FiltTimeType SwOffTime[SW_IN_PORT_MAX_NUM];
static uint8 Sw_LowValue[SW_IN_PORT_MAX_NUM];
static uint8 Sw_Status[SW_IN_PORT_MAX_NUM];
static uint8	fu1TcsDriverIntent;
static uint8	fu1EscDriverIntent;
static uint8	fu8TcsOffTimer;
static uint16	fu16EscOffTimer;
static uint8	fu1TcsSwitchAct;
static uint8	fu1EscSwitchAct;
static uint8	fu1TcsSwitchToggleEnd;
static uint8	fu1EscSwitchToggleEnd;
static uint16	fsu16SwitchErrorCnt;

#define SWT_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWT_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_SEN_START_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/** Variable Section (32BIT)**/


#define SWT_SEN_STOP_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWT_SEN_START_SEC_CODE
#include "Swt_MemMap.h"
static void SWT_DisablebySwDetect(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Swt_Sen_Init(void)
{
    uint8 i;
    /* Initialize internal bus */
    Swt_SenBus.Swt_SenSwtMonInfo.AvhSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.BlsSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.BsSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.DoorSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.EscSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.FlexBrkASwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.FlexBrkBSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.HzrdSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.HdcSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfo.PbSwtMon = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.EscSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.PbSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc = 0;
    Swt_SenBus.Swt_SenEcuModeSts = 0;
    Swt_SenBus.Swt_SenRsmDbcMon = 0;
    Swt_SenBus.Swt_SenFuncInhibitSwtSts = 0;
    Swt_SenBus.Swt_SenSwtStsFlexBrkInfo.FlexBrkASwt = 0;
    Swt_SenBus.Swt_SenSwtStsFlexBrkInfo.FlexBrkBSwt = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.BlsSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.AvhSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.EscSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.HdcSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.PbSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.GearRSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.BlfSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.ClutchSwt_Esc = 0;
    Swt_SenBus.Swt_SenSwtInfoEsc.ItpmsSwt_Esc = 0;
    Swt_SenBus.Swt_SenEscSwtStInfo.EscDisabledBySwt = 0;
    Swt_SenBus.Swt_SenEscSwtStInfo.TcsDisabledBySwt = 0;
    Swt_SenBus.Swt_SenEscSwtStInfo.HdcEnabledBySwt = 0;
    Swt_SenBus.Swt_SenBlsSwt = 0;
    Swt_SenBus.Swt_SenEscSwt = 0;
    Swt_SenBus.Swt_SenAvhSwt = 0;
    Swt_SenBus.Swt_SenBsSwt = 0;
    Swt_SenBus.Swt_SenDoorSwt = 0;
    Swt_SenBus.Swt_SenHzrdSwt = 0;
    Swt_SenBus.Swt_SenHdcSwt = 0;
    Swt_SenBus.Swt_SenPbSwt = 0;

    for(i=0;i<11;i++) SwOnTime[i]=0;
    for(i=0;i<11;i++) SwOffTime[i]=0;
    for(i=0;i<11;i++) Sw_LowValue[i]=0;
    for(i=0;i<11;i++) Sw_Status[i]=0;
}

void Swt_Sen(void)
{
    Sw_PortType PortNum = 0;
    Sw_LevelType PortLevel;

    Swt_Sen_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Swt_Sen_Read_Swt_SenSwtMonInfo_AvhSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.AvhSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_BlsSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.BlsSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_BsSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.BsSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_DoorSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.DoorSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_EscSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.EscSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_FlexBrkASwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.FlexBrkASwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_FlexBrkBSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.FlexBrkBSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_HzrdSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.HzrdSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_HdcSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.HdcSwtMon);
    Swt_Sen_Read_Swt_SenSwtMonInfo_PbSwtMon(&Swt_SenBus.Swt_SenSwtMonInfo.PbSwtMon);

    Swt_Sen_Read_Swt_SenSwtMonInfoEsc(&Swt_SenBus.Swt_SenSwtMonInfoEsc);
    /*==============================================================================
    * Members of structure Swt_SenSwtMonInfoEsc 
     : Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.EscSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.PbSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc;
     =============================================================================*/
    
    Swt_Sen_Read_Swt_SenEcuModeSts(&Swt_SenBus.Swt_SenEcuModeSts);
    Swt_Sen_Read_Swt_SenRsmDbcMon(&Swt_SenBus.Swt_SenRsmDbcMon);
    Swt_Sen_Read_Swt_SenFuncInhibitSwtSts(&Swt_SenBus.Swt_SenFuncInhibitSwtSts);

    /* Process */

    Sw_LowValue[SW_IN_PORT_BLS] = Swt_SenBus.Swt_SenSwtMonInfo.BlsSwtMon;
    Sw_LowValue[SW_IN_PORT_FLEX_BRAKE_A] = Swt_SenBus.Swt_SenSwtMonInfo.FlexBrkASwtMon;
    Sw_LowValue[SW_IN_PORT_FLEX_BRAKE_B] = Swt_SenBus.Swt_SenSwtMonInfo.FlexBrkBSwtMon;
    Sw_LowValue[SW_IN_PORT_RSM_DBC] = Swt_SenBus.Swt_SenRsmDbcMon;
    Sw_LowValue[SW_IN_PORT_AVH_SW] = Swt_SenBus.Swt_SenSwtMonInfo.AvhSwtMon;
    Sw_LowValue[SW_IN_PORT_BS_SW] = Swt_SenBus.Swt_SenSwtMonInfo.BsSwtMon;
    Sw_LowValue[SW_IN_PORT_DOOR_SW] = Swt_SenBus.Swt_SenSwtMonInfo.DoorSwtMon;
    Sw_LowValue[SW_IN_PORT_ESC_SW] = Swt_SenBus.Swt_SenSwtMonInfo.EscSwtMon;
    Sw_LowValue[SW_IN_PORT_HAZARD_SW] = Swt_SenBus.Swt_SenSwtMonInfo.HzrdSwtMon;
    Sw_LowValue[SW_IN_PORT_HDC_SW] = Swt_SenBus.Swt_SenSwtMonInfo.HdcSwtMon;
    Sw_LowValue[SW_IN_PORT_PB_SW] = Swt_SenBus.Swt_SenSwtMonInfo.PbSwtMon;

    for(PortNum = 0 ; PortNum < SW_IN_PORT_MAX_NUM ; PortNum++)
    {
        if(Sw_LowValue[PortNum] == Sw_Config[PortNum].ActType) /* Switch Pressed */
        {
            if(Sw_Status[PortNum] == SwOff)
            {
                if(SwOnTime[PortNum] >= Sw_Config[PortNum].OnFiltTime/SW_LOOP_TIME)
                {
                    Sw_Status[PortNum] = SwOn;
                    SwOnTime[PortNum] = 0;
                }
                else
                {
                    SwOnTime[PortNum]++;
                }
            }
        }
        else /* Switch Not pressed */
        {
            if(Sw_Status[PortNum] == SwOn)
            {
                if(SwOffTime[PortNum] >= Sw_Config[PortNum].OffFiltTime/SW_LOOP_TIME)
                {
                    Sw_Status[PortNum] = SwOff;
                    SwOffTime[PortNum] = 0;
                }
                else
                {
                    SwOffTime[PortNum]++;
                }
            }   
        }       
    }
    
    Swt_SenBus.Swt_SenBlsSwt = Sw_Status[SW_IN_PORT_BLS];
    Swt_SenBus.Swt_SenSwtStsFlexBrkInfo.FlexBrkASwt = Sw_Status[SW_IN_PORT_FLEX_BRAKE_A];
    Swt_SenBus.Swt_SenSwtStsFlexBrkInfo.FlexBrkBSwt = Sw_Status[SW_IN_PORT_FLEX_BRAKE_B];
    // = Sw_Status[SW_IN_PORT_RSM_DBC];  Rsm_Dbc Interface is not matched
    Swt_SenBus.Swt_SenAvhSwt = Sw_Status[SW_IN_PORT_AVH_SW];
    Swt_SenBus.Swt_SenBsSwt = Sw_Status[SW_IN_PORT_BS_SW];
    Swt_SenBus.Swt_SenDoorSwt = Sw_Status[SW_IN_PORT_DOOR_SW];
    Swt_SenBus.Swt_SenEscSwt = Sw_Status[SW_IN_PORT_ESC_SW];
    Swt_SenBus.Swt_SenHzrdSwt = Sw_Status[SW_IN_PORT_HAZARD_SW];
    Swt_SenBus.Swt_SenHdcSwt = Sw_Status[SW_IN_PORT_HDC_SW];
    Swt_SenBus.Swt_SenPbSwt = Sw_Status[SW_IN_PORT_PB_SW];

	SWT_DisablebySwDetect();
	
    /* Output */
    Swt_Sen_Write_Swt_SenSwtStsFlexBrkInfo(&Swt_SenBus.Swt_SenSwtStsFlexBrkInfo);
    /*==============================================================================
    * Members of structure Swt_SenSwtStsFlexBrkInfo 
     : Swt_SenSwtStsFlexBrkInfo.FlexBrkASwt;
     : Swt_SenSwtStsFlexBrkInfo.FlexBrkBSwt;
     =============================================================================*/
    
    Swt_Sen_Write_Swt_SenSwtInfoEsc(&Swt_SenBus.Swt_SenSwtInfoEsc);
    /*==============================================================================
    * Members of structure Swt_SenSwtInfoEsc 
     : Swt_SenSwtInfoEsc.BlsSwt_Esc;
     : Swt_SenSwtInfoEsc.AvhSwt_Esc;
     : Swt_SenSwtInfoEsc.EscSwt_Esc;
     : Swt_SenSwtInfoEsc.HdcSwt_Esc;
     : Swt_SenSwtInfoEsc.PbSwt_Esc;
     : Swt_SenSwtInfoEsc.GearRSwt_Esc;
     : Swt_SenSwtInfoEsc.BlfSwt_Esc;
     : Swt_SenSwtInfoEsc.ClutchSwt_Esc;
     : Swt_SenSwtInfoEsc.ItpmsSwt_Esc;
     =============================================================================*/
    
    Swt_Sen_Write_Swt_SenEscSwtStInfo(&Swt_SenBus.Swt_SenEscSwtStInfo);
    /*==============================================================================
    * Members of structure Swt_SenEscSwtStInfo 
     : Swt_SenEscSwtStInfo.EscDisabledBySwt;
     : Swt_SenEscSwtStInfo.TcsDisabledBySwt;
     : Swt_SenEscSwtStInfo.HdcEnabledBySwt;
     =============================================================================*/
    
    Swt_Sen_Write_Swt_SenBlsSwt(&Swt_SenBus.Swt_SenBlsSwt);
    Swt_Sen_Write_Swt_SenEscSwt(&Swt_SenBus.Swt_SenEscSwt);
    Swt_Sen_Write_Swt_SenAvhSwt(&Swt_SenBus.Swt_SenAvhSwt);
    Swt_Sen_Write_Swt_SenBsSwt(&Swt_SenBus.Swt_SenBsSwt);
    Swt_Sen_Write_Swt_SenDoorSwt(&Swt_SenBus.Swt_SenDoorSwt);
    Swt_Sen_Write_Swt_SenHzrdSwt(&Swt_SenBus.Swt_SenHzrdSwt);
    Swt_Sen_Write_Swt_SenHdcSwt(&Swt_SenBus.Swt_SenHdcSwt);
    Swt_Sen_Write_Swt_SenPbSwt(&Swt_SenBus.Swt_SenPbSwt);

    Swt_Sen_Timer_Elapsed = STM0_TIM0.U - Swt_Sen_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void SWT_DisablebySwDetect(void)
{
	uint8  TempSwitchTimeTCS;
    uint16 TempSwitchTimeECS;
    uint16 TempSwitchTime;

	if(fu1TcsDriverIntent==1)
	{
		TempSwitchTimeTCS=(uint8)F_SWITCH_OFF_TIME;
	}
	else
	{
		TempSwitchTimeTCS=(uint8)F_TCS_DISABLED_SWITCH_TIME;
	}

	if(fu1EscDriverIntent==1)
	{
		TempSwitchTimeECS=F_SWITCH_OFF_TIME;
	}
	else
	{
		TempSwitchTimeECS=F_ESC_DISABLED_SWITCH_TIME;
	}

	if(Swt_SenBus.Swt_SenEscSwt==1)
	{
		if(fu8TcsOffTimer>TempSwitchTimeTCS)
		{
			fu1TcsSwitchAct=1;
		}
		else
		{
			fu8TcsOffTimer++;
		}

		/* ESC Switch ON 시에는 Tuning 값을 적용하나, OFF 시에는 일괄적으로 150ms 를 적용함 */
		if(fu16EscOffTimer>TempSwitchTimeECS)
		{
			fu1EscSwitchAct=1;
		}
		else
		{
			fu16EscOffTimer++;
		}
	}
	else
	{
		fu1TcsSwitchAct=fu8TcsOffTimer=0;
		fu16EscOffTimer=fu1EscSwitchAct=0;
	}

    if(fu1TcsSwitchAct==1) // switched to IGN
    {

        if(fu1TcsSwitchToggleEnd==0)
        {
            fu1TcsDriverIntent^=1;
            fu1TcsSwitchToggleEnd=1;
        }
    }
    else
    {
        fu1TcsSwitchToggleEnd=0;
    }

    if(fu1EscSwitchAct==1) // switched to IGN
    {
        if(fu1EscSwitchToggleEnd==0)
        {
            fu1EscDriverIntent^=1;
            fu1EscSwitchToggleEnd=1;
        }
    }
    else
	{
		fu1EscSwitchToggleEnd=0;
	}

    if (Swt_SenBus.Swt_SenEscSwt == 1)
    {
    	Swt_SenBus.Swt_SenEscSwtStInfo.EscDisabledBySwt = OFF;
    	Swt_SenBus.Swt_SenEscSwtStInfo.TcsDisabledBySwt = OFF;

    }
    else
    {
    	Swt_SenBus.Swt_SenEscSwtStInfo.EscDisabledBySwt = fu1EscDriverIntent;
    	Swt_SenBus.Swt_SenEscSwtStInfo.TcsDisabledBySwt = fu1TcsDriverIntent;
    }	
}

#define SWT_SEN_STOP_SEC_CODE
#include "Swt_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
