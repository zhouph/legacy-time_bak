#include "unity.h"
#include "unity_fixture.h"
#include "Swt_Sen.h"
#include "Swt_Sen_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_Swt_SenSwtMonInfo_AvhSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_AVHSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_BlsSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_BLSSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_BsSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_BSSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_DoorSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_DOORSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_EscSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_ESCSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_FlexBrkASwtMon[MAX_STEP] = SWT_SENSWTMONINFO_FLEXBRKASWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_FlexBrkBSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_FLEXBRKBSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_HzrdSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_HZRDSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_HdcSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_HDCSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfo_PbSwtMon[MAX_STEP] = SWT_SENSWTMONINFO_PBSWTMON;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_BlsSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_BLSSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_AvhSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_AVHSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_EscSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_ESCSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_HdcSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_HDCSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_PbSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_PBSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_GearRSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_GEARRSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_BlfSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_BLFSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_ClutchSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_CLUTCHSWTMON_ESC;
const Haluint8 UtInput_Swt_SenSwtMonInfoEsc_ItpmsSwtMon_Esc[MAX_STEP] = SWT_SENSWTMONINFOESC_ITPMSSWTMON_ESC;
const Mom_HndlrEcuModeSts_t UtInput_Swt_SenEcuModeSts[MAX_STEP] = SWT_SENECUMODESTS;
const Ioc_InputCS1msRsmDbcMon_t UtInput_Swt_SenRsmDbcMon[MAX_STEP] = SWT_SENRSMDBCMON;
const Eem_SuspcDetnFuncInhibitSwtSts_t UtInput_Swt_SenFuncInhibitSwtSts[MAX_STEP] = SWT_SENFUNCINHIBITSWTSTS;

const Saluint8 UtExpected_Swt_SenSwtStsFlexBrkInfo_FlexBrkASwt[MAX_STEP] = SWT_SENSWTSTSFLEXBRKINFO_FLEXBRKASWT;
const Saluint8 UtExpected_Swt_SenSwtStsFlexBrkInfo_FlexBrkBSwt[MAX_STEP] = SWT_SENSWTSTSFLEXBRKINFO_FLEXBRKBSWT;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_BlsSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_BLSSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_AvhSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_AVHSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_EscSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_ESCSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_HdcSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_HDCSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_PbSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_PBSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_GearRSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_GEARRSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_BlfSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_BLFSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_ClutchSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_CLUTCHSWT_ESC;
const Saluint8 UtExpected_Swt_SenSwtInfoEsc_ItpmsSwt_Esc[MAX_STEP] = SWT_SENSWTINFOESC_ITPMSSWT_ESC;
const Saluint8 UtExpected_Swt_SenEscSwtStInfo_EscDisabledBySwt[MAX_STEP] = SWT_SENESCSWTSTINFO_ESCDISABLEDBYSWT;
const Saluint8 UtExpected_Swt_SenEscSwtStInfo_TcsDisabledBySwt[MAX_STEP] = SWT_SENESCSWTSTINFO_TCSDISABLEDBYSWT;
const Saluint8 UtExpected_Swt_SenEscSwtStInfo_HdcEnabledBySwt[MAX_STEP] = SWT_SENESCSWTSTINFO_HDCENABLEDBYSWT;
const Swt_SenBlsSwt_t UtExpected_Swt_SenBlsSwt[MAX_STEP] = SWT_SENBLSSWT;
const Swt_SenEscSwt_t UtExpected_Swt_SenEscSwt[MAX_STEP] = SWT_SENESCSWT;
const Swt_SenAvhSwt_t UtExpected_Swt_SenAvhSwt[MAX_STEP] = SWT_SENAVHSWT;
const Swt_SenBsSwt_t UtExpected_Swt_SenBsSwt[MAX_STEP] = SWT_SENBSSWT;
const Swt_SenDoorSwt_t UtExpected_Swt_SenDoorSwt[MAX_STEP] = SWT_SENDOORSWT;
const Swt_SenHzrdSwt_t UtExpected_Swt_SenHzrdSwt[MAX_STEP] = SWT_SENHZRDSWT;
const Swt_SenHdcSwt_t UtExpected_Swt_SenHdcSwt[MAX_STEP] = SWT_SENHDCSWT;
const Swt_SenPbSwt_t UtExpected_Swt_SenPbSwt[MAX_STEP] = SWT_SENPBSWT;



TEST_GROUP(Swt_Sen);
TEST_SETUP(Swt_Sen)
{
    Swt_Sen_Init();
}

TEST_TEAR_DOWN(Swt_Sen)
{   /* Postcondition */

}

TEST(Swt_Sen, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Swt_SenSwtMonInfo.AvhSwtMon = UtInput_Swt_SenSwtMonInfo_AvhSwtMon[i];
        Swt_SenSwtMonInfo.BlsSwtMon = UtInput_Swt_SenSwtMonInfo_BlsSwtMon[i];
        Swt_SenSwtMonInfo.BsSwtMon = UtInput_Swt_SenSwtMonInfo_BsSwtMon[i];
        Swt_SenSwtMonInfo.DoorSwtMon = UtInput_Swt_SenSwtMonInfo_DoorSwtMon[i];
        Swt_SenSwtMonInfo.EscSwtMon = UtInput_Swt_SenSwtMonInfo_EscSwtMon[i];
        Swt_SenSwtMonInfo.FlexBrkASwtMon = UtInput_Swt_SenSwtMonInfo_FlexBrkASwtMon[i];
        Swt_SenSwtMonInfo.FlexBrkBSwtMon = UtInput_Swt_SenSwtMonInfo_FlexBrkBSwtMon[i];
        Swt_SenSwtMonInfo.HzrdSwtMon = UtInput_Swt_SenSwtMonInfo_HzrdSwtMon[i];
        Swt_SenSwtMonInfo.HdcSwtMon = UtInput_Swt_SenSwtMonInfo_HdcSwtMon[i];
        Swt_SenSwtMonInfo.PbSwtMon = UtInput_Swt_SenSwtMonInfo_PbSwtMon[i];
        Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_BlsSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_AvhSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.EscSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_EscSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_HdcSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.PbSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_PbSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_GearRSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_BlfSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_ClutchSwtMon_Esc[i];
        Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc = UtInput_Swt_SenSwtMonInfoEsc_ItpmsSwtMon_Esc[i];
        Swt_SenEcuModeSts = UtInput_Swt_SenEcuModeSts[i];
        Swt_SenRsmDbcMon = UtInput_Swt_SenRsmDbcMon[i];
        Swt_SenFuncInhibitSwtSts = UtInput_Swt_SenFuncInhibitSwtSts[i];

        Swt_Sen();

        TEST_ASSERT_EQUAL(Swt_SenSwtStsFlexBrkInfo.FlexBrkASwt, UtExpected_Swt_SenSwtStsFlexBrkInfo_FlexBrkASwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtStsFlexBrkInfo.FlexBrkBSwt, UtExpected_Swt_SenSwtStsFlexBrkInfo_FlexBrkBSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.BlsSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_BlsSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.AvhSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_AvhSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.EscSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_EscSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.HdcSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_HdcSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.PbSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_PbSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.GearRSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_GearRSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.BlfSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_BlfSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.ClutchSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_ClutchSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenSwtInfoEsc.ItpmsSwt_Esc, UtExpected_Swt_SenSwtInfoEsc_ItpmsSwt_Esc[i]);
        TEST_ASSERT_EQUAL(Swt_SenEscSwtStInfo.EscDisabledBySwt, UtExpected_Swt_SenEscSwtStInfo_EscDisabledBySwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenEscSwtStInfo.TcsDisabledBySwt, UtExpected_Swt_SenEscSwtStInfo_TcsDisabledBySwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenEscSwtStInfo.HdcEnabledBySwt, UtExpected_Swt_SenEscSwtStInfo_HdcEnabledBySwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenBlsSwt, UtExpected_Swt_SenBlsSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenEscSwt, UtExpected_Swt_SenEscSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenAvhSwt, UtExpected_Swt_SenAvhSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenBsSwt, UtExpected_Swt_SenBsSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenDoorSwt, UtExpected_Swt_SenDoorSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenHzrdSwt, UtExpected_Swt_SenHzrdSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenHdcSwt, UtExpected_Swt_SenHdcSwt[i]);
        TEST_ASSERT_EQUAL(Swt_SenPbSwt, UtExpected_Swt_SenPbSwt[i]);
    }
}

TEST_GROUP_RUNNER(Swt_Sen)
{
    RUN_TEST_CASE(Swt_Sen, All);
}
