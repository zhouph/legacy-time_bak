Swt_SenSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'DoorSwtMon'
    'EscSwtMon'
    'FlexBrkASwtMon'
    'FlexBrkBSwtMon'
    'HzrdSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
Swt_SenSwtMonInfo = CreateBus(Swt_SenSwtMonInfo, DeList);
clear DeList;

Swt_SenSwtMonInfoEsc = Simulink.Bus;
DeList={
    'BlsSwtMon_Esc'
    'AvhSwtMon_Esc'
    'EscSwtMon_Esc'
    'HdcSwtMon_Esc'
    'PbSwtMon_Esc'
    'GearRSwtMon_Esc'
    'BlfSwtMon_Esc'
    'ClutchSwtMon_Esc'
    'ItpmsSwtMon_Esc'
    };
Swt_SenSwtMonInfoEsc = CreateBus(Swt_SenSwtMonInfoEsc, DeList);
clear DeList;

Swt_SenEcuModeSts = Simulink.Bus;
DeList={'Swt_SenEcuModeSts'};
Swt_SenEcuModeSts = CreateBus(Swt_SenEcuModeSts, DeList);
clear DeList;

Swt_SenRsmDbcMon = Simulink.Bus;
DeList={'Swt_SenRsmDbcMon'};
Swt_SenRsmDbcMon = CreateBus(Swt_SenRsmDbcMon, DeList);
clear DeList;

Swt_SenFuncInhibitSwtSts = Simulink.Bus;
DeList={'Swt_SenFuncInhibitSwtSts'};
Swt_SenFuncInhibitSwtSts = CreateBus(Swt_SenFuncInhibitSwtSts, DeList);
clear DeList;

Swt_SenSwtStsFlexBrkInfo = Simulink.Bus;
DeList={
    'FlexBrkASwt'
    'FlexBrkBSwt'
    };
Swt_SenSwtStsFlexBrkInfo = CreateBus(Swt_SenSwtStsFlexBrkInfo, DeList);
clear DeList;

Swt_SenSwtInfoEsc = Simulink.Bus;
DeList={
    'BlsSwt_Esc'
    'AvhSwt_Esc'
    'EscSwt_Esc'
    'HdcSwt_Esc'
    'PbSwt_Esc'
    'GearRSwt_Esc'
    'BlfSwt_Esc'
    'ClutchSwt_Esc'
    'ItpmsSwt_Esc'
    };
Swt_SenSwtInfoEsc = CreateBus(Swt_SenSwtInfoEsc, DeList);
clear DeList;

Swt_SenEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
Swt_SenEscSwtStInfo = CreateBus(Swt_SenEscSwtStInfo, DeList);
clear DeList;

Swt_SenBlsSwt = Simulink.Bus;
DeList={'Swt_SenBlsSwt'};
Swt_SenBlsSwt = CreateBus(Swt_SenBlsSwt, DeList);
clear DeList;

Swt_SenEscSwt = Simulink.Bus;
DeList={'Swt_SenEscSwt'};
Swt_SenEscSwt = CreateBus(Swt_SenEscSwt, DeList);
clear DeList;

Swt_SenAvhSwt = Simulink.Bus;
DeList={'Swt_SenAvhSwt'};
Swt_SenAvhSwt = CreateBus(Swt_SenAvhSwt, DeList);
clear DeList;

Swt_SenBsSwt = Simulink.Bus;
DeList={'Swt_SenBsSwt'};
Swt_SenBsSwt = CreateBus(Swt_SenBsSwt, DeList);
clear DeList;

Swt_SenDoorSwt = Simulink.Bus;
DeList={'Swt_SenDoorSwt'};
Swt_SenDoorSwt = CreateBus(Swt_SenDoorSwt, DeList);
clear DeList;

Swt_SenHzrdSwt = Simulink.Bus;
DeList={'Swt_SenHzrdSwt'};
Swt_SenHzrdSwt = CreateBus(Swt_SenHzrdSwt, DeList);
clear DeList;

Swt_SenHdcSwt = Simulink.Bus;
DeList={'Swt_SenHdcSwt'};
Swt_SenHdcSwt = CreateBus(Swt_SenHdcSwt, DeList);
clear DeList;

Swt_SenPbSwt = Simulink.Bus;
DeList={'Swt_SenPbSwt'};
Swt_SenPbSwt = CreateBus(Swt_SenPbSwt, DeList);
clear DeList;

