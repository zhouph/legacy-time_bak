#define S_FUNCTION_NAME      Swt_Sen_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          22
#define WidthOutputPort         22

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Swt_Sen.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Swt_SenSwtMonInfo.AvhSwtMon = input[0];
    Swt_SenSwtMonInfo.BlsSwtMon = input[1];
    Swt_SenSwtMonInfo.BsSwtMon = input[2];
    Swt_SenSwtMonInfo.DoorSwtMon = input[3];
    Swt_SenSwtMonInfo.EscSwtMon = input[4];
    Swt_SenSwtMonInfo.FlexBrkASwtMon = input[5];
    Swt_SenSwtMonInfo.FlexBrkBSwtMon = input[6];
    Swt_SenSwtMonInfo.HzrdSwtMon = input[7];
    Swt_SenSwtMonInfo.HdcSwtMon = input[8];
    Swt_SenSwtMonInfo.PbSwtMon = input[9];
    Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc = input[10];
    Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc = input[11];
    Swt_SenSwtMonInfoEsc.EscSwtMon_Esc = input[12];
    Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc = input[13];
    Swt_SenSwtMonInfoEsc.PbSwtMon_Esc = input[14];
    Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc = input[15];
    Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc = input[16];
    Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc = input[17];
    Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc = input[18];
    Swt_SenEcuModeSts = input[19];
    Swt_SenRsmDbcMon = input[20];
    Swt_SenFuncInhibitSwtSts = input[21];

    Swt_Sen();


    output[0] = Swt_SenSwtStsFlexBrkInfo.FlexBrkASwt;
    output[1] = Swt_SenSwtStsFlexBrkInfo.FlexBrkBSwt;
    output[2] = Swt_SenSwtInfoEsc.BlsSwt_Esc;
    output[3] = Swt_SenSwtInfoEsc.AvhSwt_Esc;
    output[4] = Swt_SenSwtInfoEsc.EscSwt_Esc;
    output[5] = Swt_SenSwtInfoEsc.HdcSwt_Esc;
    output[6] = Swt_SenSwtInfoEsc.PbSwt_Esc;
    output[7] = Swt_SenSwtInfoEsc.GearRSwt_Esc;
    output[8] = Swt_SenSwtInfoEsc.BlfSwt_Esc;
    output[9] = Swt_SenSwtInfoEsc.ClutchSwt_Esc;
    output[10] = Swt_SenSwtInfoEsc.ItpmsSwt_Esc;
    output[11] = Swt_SenEscSwtStInfo.EscDisabledBySwt;
    output[12] = Swt_SenEscSwtStInfo.TcsDisabledBySwt;
    output[13] = Swt_SenEscSwtStInfo.HdcEnabledBySwt;
    output[14] = Swt_SenBlsSwt;
    output[15] = Swt_SenEscSwt;
    output[16] = Swt_SenAvhSwt;
    output[17] = Swt_SenBsSwt;
    output[18] = Swt_SenDoorSwt;
    output[19] = Swt_SenHzrdSwt;
    output[20] = Swt_SenHdcSwt;
    output[21] = Swt_SenPbSwt;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Swt_Sen_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
