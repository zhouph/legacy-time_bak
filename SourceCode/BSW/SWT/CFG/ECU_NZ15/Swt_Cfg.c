/**
 * @defgroup Swt_Cfg Swt_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Swt_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Swt_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWT_START_SEC_CONST_UNSPECIFIED
#include "Swt_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWT_STOP_SEC_CONST_UNSPECIFIED
#include "Swt_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
 const Sw_ConfigType Sw_Config[SW_IN_PORT_MAX_NUM] = 
 {
 	/* SW_IN_PORT_BLS */
 	{
 		(Sw_FiltTimeType) 0, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 0, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_BLS /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_FLEX_BRAKE_A */
 	{
 		(Sw_FiltTimeType) 0, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 0, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_FLEX_BRAKE_A /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_FLEX_BRAKE_B */
 	{
 		(Sw_FiltTimeType) 0, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 0, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_FLEX_BRAKE_B /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_RSM_DBC */
 	{
 		(Sw_FiltTimeType) 0, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 0, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_RSM_DBC /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_AVH_SW */
 	{
 		(Sw_FiltTimeType) 150, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 150, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_AVH_SW /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_BS_SW */
 	{
 		(Sw_FiltTimeType) 0, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 0, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_BS_SW /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_DOOR_SW */
 	{
 		(Sw_FiltTimeType) 150, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 150, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_DOOR_SW /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_ESC_SW */
 	{
 		(Sw_FiltTimeType) 150, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 150, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_ESC_SW /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_HAZARD_SW */
 	{
 		(Sw_FiltTimeType) 150, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 150, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwLowActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_HAZARD_SW /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_HDC_SW */
 	{
 		(Sw_FiltTimeType) 150, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 150, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwHighActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_HDC_SW /*Ecu Signal Port */
 	},
 	/* SW_IN_PORT_PB_SW */
 	{
 		(Sw_FiltTimeType) 0, 	   				 /* SW On Filtering Time (ms) */
 		(Sw_FiltTimeType) 0, 	   				 /* SW Off Filtering Time (ms) */
 		(Sw_ActiveType)	  SwLowActive,			 /* when the switch pressed, "High" : 1 / "Low" : 0 */
 		(Sw_PortType)  SW_IN_PORT_PB_SW /*Ecu Signal Port */
 	}
 };

#define SWT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_START_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWT_STOP_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
#define SWT_START_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWT_STOP_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_START_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/** Variable Section (32BIT)**/


#define SWT_STOP_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_START_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWT_STOP_SEC_VAR_NOINIT_32BIT
#include "Swt_MemMap.h"
#define SWT_START_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWT_STOP_SEC_VAR_UNSPECIFIED
#include "Swt_MemMap.h"
#define SWT_START_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/** Variable Section (32BIT)**/


#define SWT_STOP_SEC_VAR_32BIT
#include "Swt_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWT_START_SEC_CODE
#include "Swt_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWT_STOP_SEC_CODE
#include "Swt_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
