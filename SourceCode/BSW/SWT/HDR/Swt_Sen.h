/**
 * @defgroup Swt_Sen Swt_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Swt_Sen.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWT_SEN_H_
#define SWT_SEN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Swt_Types.h"
#include "Swt_Cfg.h"
#include "Swt_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SWT_SEN_MODULE_ID      (0)
 #define SWT_SEN_MAJOR_VERSION  (2)
 #define SWT_SEN_MINOR_VERSION  (0)
 #define SWT_SEN_PATCH_VERSION  (0)
 #define SWT_SEN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Swt_Sen_HdrBusType Swt_SenBus;

/* Version Info */
extern const SwcVersionInfo_t Swt_SenVersionInfo;

/* Input Data Element */
extern Ioc_InputCS1msSwtMonInfo_t Swt_SenSwtMonInfo;
extern Ioc_InputCS1msSwtMonInfoEsc_t Swt_SenSwtMonInfoEsc;
extern Mom_HndlrEcuModeSts_t Swt_SenEcuModeSts;
extern Ioc_InputCS1msRsmDbcMon_t Swt_SenRsmDbcMon;
extern Eem_SuspcDetnFuncInhibitSwtSts_t Swt_SenFuncInhibitSwtSts;

/* Output Data Element */
extern Swt_SenSwtStsFlexBrkInfo_t Swt_SenSwtStsFlexBrkInfo;
extern Swt_SenSwtInfoEsc_t Swt_SenSwtInfoEsc;
extern Swt_SenEscSwtStInfo_t Swt_SenEscSwtStInfo;
extern Swt_SenBlsSwt_t Swt_SenBlsSwt;
extern Swt_SenEscSwt_t Swt_SenEscSwt;
extern Swt_SenAvhSwt_t Swt_SenAvhSwt;
extern Swt_SenBsSwt_t Swt_SenBsSwt;
extern Swt_SenDoorSwt_t Swt_SenDoorSwt;
extern Swt_SenHzrdSwt_t Swt_SenHzrdSwt;
extern Swt_SenHdcSwt_t Swt_SenHdcSwt;
extern Swt_SenPbSwt_t Swt_SenPbSwt;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Swt_Sen_Init(void);
extern void Swt_Sen(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWT_SEN_H_ */
/** @} */
