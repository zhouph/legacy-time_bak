/**
 * @defgroup Swt_Types Swt_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Swt_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWT_TYPES_H_
#define SWT_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputCS1msSwtMonInfo_t Swt_SenSwtMonInfo;
    Ioc_InputCS1msSwtMonInfoEsc_t Swt_SenSwtMonInfoEsc;
    Mom_HndlrEcuModeSts_t Swt_SenEcuModeSts;
    Ioc_InputCS1msRsmDbcMon_t Swt_SenRsmDbcMon;
    Eem_SuspcDetnFuncInhibitSwtSts_t Swt_SenFuncInhibitSwtSts;

/* Output Data Element */
    Swt_SenSwtStsFlexBrkInfo_t Swt_SenSwtStsFlexBrkInfo;
    Swt_SenSwtInfoEsc_t Swt_SenSwtInfoEsc;
    Swt_SenEscSwtStInfo_t Swt_SenEscSwtStInfo;
    Swt_SenBlsSwt_t Swt_SenBlsSwt;
    Swt_SenEscSwt_t Swt_SenEscSwt;
    Swt_SenAvhSwt_t Swt_SenAvhSwt;
    Swt_SenBsSwt_t Swt_SenBsSwt;
    Swt_SenDoorSwt_t Swt_SenDoorSwt;
    Swt_SenHzrdSwt_t Swt_SenHzrdSwt;
    Swt_SenHdcSwt_t Swt_SenHdcSwt;
    Swt_SenPbSwt_t Swt_SenPbSwt;
}Swt_Sen_HdrBusType;

/* Switch Port Type */
typedef uint16 Sw_PortType;

/* Switch Level Type */
typedef uint8 Sw_LevelType;

/* Switch Filtering Time Type */
typedef uint16 Sw_FiltTimeType;

/* Switch Active Type when the Switch pressed */
typedef enum
{
	SwLowActive = 0,
	SwHighActive
}Sw_ActiveType;

/* Switch Status Type*/
typedef enum
{
	SwOff = 0,
	SwOn,
	SwInvalid
}Sw_StatusType;

/* Swtch Config type */
typedef struct
{
	Sw_FiltTimeType	OnFiltTime;
	Sw_FiltTimeType	OffFiltTime;
	Sw_ActiveType	ActType;
	Sw_PortType	SigPort;
}Sw_ConfigType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWT_TYPES_H_ */
/** @} */
