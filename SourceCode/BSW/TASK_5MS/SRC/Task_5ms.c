/**
 * @defgroup Task_5ms Task_5ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_5ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_5ms.h"
#include "Task_5ms_Ifa.h"
#include "Task_1ms.h"
#include "Task_10ms.h"
#include "Task_50us.h"
#include "Brake_Main.h"
#include "Ses_Ctrl.h"
/* Include runnables mapped to this task */
#include "Proxy_RxByCom.h"
#include "Proxy_Rx.h"
#include "Icu_InputCapture.h"
#include "Ioc_InputSR5ms.h"
#include "Nvm_Hndlr.h"
#include "Prly_Hndlr.h"
#include "Mom_Hndlr.h"
#include "Wss_Sen.h"
#include "Pedal_SenSync.h"
#include "Press_SenSync.h"
#include "Swt_Sen.h"
#include "Spc_5msCtrl.h"
#include "Det_5msCtrl.h"
#include "Bbc_Ctrl.h"
#include "Rbc_Ctrl.h"
#include "Abc_Ctrl.h"
#include "Arb_Ctrl.h"
#include "Pct_5msCtrl.h"
#include "Vat_Ctrl.h"
#include "SysPwrM_Main.h"
#include "EcuHwCtrlM_Main.h"
#include "WssM_Main.h"
#include "PressM_Main.h"
#include "PedalM_Main.h"
#include "WssP_Main.h"
#include "PressP_Main.h"
#include "PedalP_Main.h"
#include "BbsVlvM_Main.h"
#include "AbsVlvM_Main.h"
#include "Mps_TLE5012M_Main.h"
#include "MgdM_Main.h"
#include "MtrM_Main.h"
#include "Watchdog_Main.h"
#include "Eem_Main.h"
#include "Eem_SuspcDetn.h"
#include "Arbitrator_Vlv.h"
#include "Arbitrator_Rly.h"
#include "Fsr_Actr.h"
#include "Rly_Actr.h"
#include "Vlv_ActrSync.h"
#include "Ioc_OutputCS5ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define U8_SYSTEM_STABILIZATION_TIME 400

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TASK_5MS_START_SEC_CONST_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define TASK_5MS_STOP_SEC_CONST_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"

 //TimE
 #include "TimE.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Task_5ms_HdrBusType Task_5msBus;


/*motor Internal flag*/
sint32 Int_5ms_IdbMotOrgSetFlg;

/* Version Info */
const SwcVersionInfo_t Task_5msVersionInfo = 
{
    TASK_5MS_MODULE_ID,       /* Task_5msVersionInfo.ModuleId */
    TASK_5MS_MAJOR_VERSION,   /* Task_5msVersionInfo.MajorVer */
    TASK_5MS_MINOR_VERSION,   /* Task_5msVersionInfo.MinorVer */
    TASK_5MS_PATCH_VERSION,   /* Task_5msVersionInfo.PatchVer */
    TASK_5MS_BRANCH_VERSION   /* Task_5msVersionInfo.BranchVer */
};

    
/* Input Data Element */
SenPwrM_MainSenPwrMonitor_t Task_5ms_Prly_HndlrSenPwrMonitorData;
Ach_InputAchWssPort0AsicInfo_t Task_5ms_Wss_SenAchWssPort0AsicInfo;
Ach_InputAchWssPort1AsicInfo_t Task_5ms_Wss_SenAchWssPort1AsicInfo;
Ach_InputAchWssPort2AsicInfo_t Task_5ms_Wss_SenAchWssPort2AsicInfo;
Ach_InputAchWssPort3AsicInfo_t Task_5ms_Wss_SenAchWssPort3AsicInfo;
Pedal_SenPdtBufInfo_t Task_5ms_Pedal_SenSyncPdtBufInfo;
Pedal_SenPdfBufInfo_t Task_5ms_Pedal_SenSyncPdfBufInfo;
Press_SenCircPBufInfo_t Task_5ms_Press_SenSyncCircPBufInfo;
Press_SenPistPBufInfo_t Task_5ms_Press_SenSyncPistPBufInfo;
Press_SenPspBufInfo_t Task_5ms_Press_SenSyncPspBufInfo;
Ioc_InputCS1msSwtMonInfo_t Task_5ms_Swt_SenSwtMonInfo;
Ioc_InputCS1msSwtMonInfoEsc_t Task_5ms_Swt_SenSwtMonInfoEsc;
Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Task_5ms_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
Msp_CtrlMotRotgAgSigInfo_t Task_5ms_Det_5msCtrlMotRotgAgSigInfo;
Det_1msCtrlBrkPedlStatus1msInfo_t Task_5ms_Bbc_CtrlBrkPedlStatus1msInfo;
Msp_CtrlMotRotgAgSigInfo_t Task_5ms_Abc_CtrlMotRotgAgSigInfo;
Diag_HndlrSasCalInfo_t Task_5ms_Abc_CtrlSasCalInfo;
Proxy_TxTxESCSensorInfo_t Task_5ms_Abc_CtrlTxESCSensorInfo;
SenPwrM_MainSenPwrMonitor_t Task_5ms_Abc_CtrlSenPwrMonitorData;
YawM_MainYAWMSerialInfo_t Task_5ms_Abc_CtrlYAWMSerialInfo;
YawM_MainYAWMOutInfo_t Task_5ms_Abc_CtrlYAWMOutInfo;
Spc_1msCtrlPedlTrvlFild1msInfo_t Task_5ms_Pct_5msCtrlPedlTrvlFild1msInfo;
Msp_CtrlMotRotgAgSigInfo_t Task_5ms_Pct_5msCtrlMotRotgAgSigInfo;
Det_1msCtrlBrkPedlStatus1msInfo_t Task_5ms_Pct_5msCtrlBrkPedlStatus1msInfo;
Det_1msCtrlPedlTrvlRate1msInfo_t Task_5ms_Pct_5msCtrlPedlTrvlRate1msInfo;
Mcc_1msCtrlIdbMotPosnInfo_t Task_5ms_Pct_5msCtrlIdbMotPosnInfo;
Ach_InputAchSysPwrAsicInfo_t Task_5ms_SysPwrM_MainAchSysPwrAsicInfo;
Ach_InputAchSysPwrAsicInfo_t Task_5ms_EcuHwCtrlM_MainAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t Task_5ms_EcuHwCtrlM_MainAchValveAsicInfo;
Ach_InputAchSysPwrAsicInfo_t Task_5ms_WssM_MainAchSysPwrAsicInfo;
Ach_InputAchWssPort0AsicInfo_t Task_5ms_WssM_MainAchWssPort0AsicInfo;
Ach_InputAchWssPort1AsicInfo_t Task_5ms_WssM_MainAchWssPort1AsicInfo;
Ach_InputAchWssPort2AsicInfo_t Task_5ms_WssM_MainAchWssPort2AsicInfo;
Ach_InputAchWssPort3AsicInfo_t Task_5ms_WssM_MainAchWssPort3AsicInfo;
Ioc_InputSR1msHalPressureInfo_t Task_5ms_PressM_MainHalPressureInfo;
SentH_MainSentHPressureInfo_t Task_5ms_PressM_MainSentHPressureInfo;
Ach_InputAchSysPwrAsicInfo_t Task_5ms_PedalM_MainAchSysPwrAsicInfo;
Ioc_InputSR1msPedlSigMonInfo_t Task_5ms_PedalM_MainPedlSigMonInfo;
Ioc_InputSR1msPedlPwrMonInfo_t Task_5ms_PedalM_MainPedlPwrMonInfo;
Press_SenPressCalcInfo_t Task_5ms_PressP_MainPressCalcInfo;
Ach_InputAchSysPwrAsicInfo_t Task_5ms_BbsVlvM_MainAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t Task_5ms_BbsVlvM_MainAchValveAsicInfo;
Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Task_5ms_BbsVlvM_MainVlvdFF0DcdInfo;
Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Task_5ms_BbsVlvM_MainVlvdFF1DcdInfo;
Ach_InputAchSysPwrAsicInfo_t Task_5ms_AbsVlvM_MainAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t Task_5ms_AbsVlvM_MainAchValveAsicInfo;
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Task_5ms_Mps_TLE5012M_MainMpsD1SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Task_5ms_Mps_TLE5012M_MainMpsD2SpiDcdInfo;
Mgd_TLE9180_HndlrMgdDcdInfo_t Task_5ms_MgdM_MainMgdDcdInfo;
Ioc_InputSR1msMotMonInfo_t Task_5ms_MtrM_MainMotMonInfo;
Ioc_InputSR1msMotVoltsMonInfo_t Task_5ms_MtrM_MainMotVoltsMonInfo;
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Task_5ms_MtrM_MainMpsD1SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Task_5ms_MtrM_MainMpsD2SpiDcdInfo;
Mgd_TLE9180_HndlrMgdDcdInfo_t Task_5ms_MtrM_MainMgdDcdInfo;
Mcc_1msCtrlMotDqIRefMccInfo_t Task_5ms_MtrM_MainMotDqIRefMccInfo;
AdcIf_Conv50usHwTrigMotInfo_t Task_5ms_MtrM_MainHwTrigMotInfo;
Ioc_InputSR50usMotCurrMonInfo_t Task_5ms_MtrM_MainMotCurrMonInfo;
Ioc_InputSR50usMotAngleMonInfo_t Task_5ms_MtrM_MainMotAngleMonInfo;
Mtr_Processing_SigMtrProcessOutInfo_t Task_5ms_MtrM_MainMtrProcessOutInfo;
RlyM_MainRelayMonitor_t Task_5ms_Eem_MainRelayMonitorData;
SenPwrM_MainSenPwrMonitor_t Task_5ms_Eem_MainSenPwrMonitorData;
SwtM_MainSwtMonFaultInfo_t Task_5ms_Eem_MainSwtMonFaultInfo;
SwtP_MainSwtPFaultInfo_t Task_5ms_Eem_MainSwtPFaultInfo;
YawM_MainYAWMOutInfo_t Task_5ms_Eem_MainYAWMOutInfo;
AyM_MainAYMOutInfo_t Task_5ms_Eem_MainAYMOuitInfo;
AxM_MainAXMOutInfo_t Task_5ms_Eem_MainAXMOutInfo;
SasM_MainSASMOutInfo_t Task_5ms_Eem_MainSASMOutInfo;
CanM_MainCanMonData_t Task_5ms_Eem_MainCanMonData;
YawP_MainYawPlauOutput_t Task_5ms_Eem_MainYawPlauOutput;
AxP_MainAxPlauOutput_t Task_5ms_Eem_MainAxPlauOutput;
AyP_MainAyPlauOutput_t Task_5ms_Eem_MainAyPlauOutput;
SasP_MainSasPlauOutput_t Task_5ms_Eem_MainSasPlauOutput;
Ses_CtrlNormVlvReqSesInfo_t Task_5ms_Arbitrator_VlvNormVlvReqSesInfo;
Ses_CtrlWhlVlvReqSesInfo_t Task_5ms_Arbitrator_VlvWhlVlvReqSesInfo;
Diag_HndlrDiagVlvActr_t Task_5ms_Arbitrator_VlvDiagVlvActrInfo;
Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Ioc_InputSR5msAcmAsicInitCompleteFlag;
Ioc_InputSR1msCEMon_t Task_5ms_Prly_HndlrCEMon;
Ioc_InputCS1msRsmDbcMon_t Task_5ms_Swt_SenRsmDbcMon;
Mcc_1msCtrlMotCtrlState_t Task_5ms_Spc_5msCtrlMotCtrlState;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_Abc_CtrlVBatt1Mon;
Diag_HndlrDiagSci_t Task_5ms_Abc_CtrlDiagSci;
Mcc_1msCtrlMotCtrlMode_t Task_5ms_Pct_5msCtrlMotCtrlMode;
Mcc_1msCtrlMotICtrlFadeOutState_t Task_5ms_Pct_5msCtrlMotICtrlFadeOutState;
Pct_1msCtrlPreBoostMod_t Task_5ms_Pct_5msCtrlPreBoostMod;
Mcc_1msCtrlStkRecvryStabnEndOK_t Task_5ms_Pct_5msCtrlStkRecvryStabnEndOK;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_SysPwrM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Task_5ms_SysPwrM_MainVBatt2Mon;
Diag_HndlrDiagClr_t Task_5ms_SysPwrM_MainDiagClrSrs;
Ioc_InputSR1msVdd1Mon_t Task_5ms_SysPwrM_MainVdd1Mon;
Ioc_InputSR1msVdd2Mon_t Task_5ms_SysPwrM_MainVdd2Mon;
Ioc_InputSR1msVdd3Mon_t Task_5ms_SysPwrM_MainVdd3Mon;
Ioc_InputSR1msVdd4Mon_t Task_5ms_SysPwrM_MainVdd4Mon;
Ioc_InputSR1msVdd5Mon_t Task_5ms_SysPwrM_MainVdd5Mon;
Ioc_InputSR1msVddMon_t Task_5ms_SysPwrM_MainVddMon;
Arbitrator_MtrMtrArbDriveState_t Task_5ms_SysPwrM_MainMtrArbDriveState;
Ach_InputAchAsicInvalidInfo_t Task_5ms_EcuHwCtrlM_MainAchAsicInvalid;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_EcuHwCtrlM_MainVBatt1Mon;
Diag_HndlrDiagClr_t Task_5ms_EcuHwCtrlM_MainDiagClrSrs;
Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
Ioc_InputSR1msVdd1Mon_t Task_5ms_EcuHwCtrlM_MainVdd1Mon;
Ioc_InputSR1msVdd2Mon_t Task_5ms_EcuHwCtrlM_MainVdd2Mon;
Ioc_InputSR1msVdd3Mon_t Task_5ms_EcuHwCtrlM_MainVdd3Mon;
Ioc_InputSR1msVdd4Mon_t Task_5ms_EcuHwCtrlM_MainVdd4Mon;
Ioc_InputSR1msVdd5Mon_t Task_5ms_EcuHwCtrlM_MainVdd5Mon;
AdcIf_Conv1msAdcInvalid_t Task_5ms_EcuHwCtrlM_MainAdcInvalid;
Ach_InputAchAsicInvalidInfo_t Task_5ms_WssM_MainAchAsicInvalid;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_WssM_MainVBatt1Mon;
Diag_HndlrDiagClr_t Task_5ms_WssM_MainDiagClrSrs;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_PressM_MainVBatt1Mon;
Diag_HndlrDiagClr_t Task_5ms_PressM_MainDiagClrSrs;
Ach_InputAchAsicInvalidInfo_t Task_5ms_PedalM_MainAchAsicInvalid;
Diag_HndlrDiagClr_t Task_5ms_PedalM_MainDiagClrSrs;
Ioc_InputSR1msVdd3Mon_t Task_5ms_PedalM_MainVdd3Mon;
Diag_HndlrDiagClr_t Task_5ms_WssP_MainDiagClrSrs;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_PressP_MainVBatt1Mon;
Diag_HndlrDiagClr_t Task_5ms_PressP_MainDiagClrSrs;
Diag_HndlrDiagClr_t Task_5ms_PedalP_MainDiagClrSrs;
Ach_InputAchAsicInvalidInfo_t Task_5ms_BbsVlvM_MainAchAsicInvalid;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_BbsVlvM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Task_5ms_BbsVlvM_MainVBatt2Mon;
Diag_HndlrDiagClr_t Task_5ms_BbsVlvM_MainDiagClrSrs;
Ioc_InputSR1msFspCbsHMon_t Task_5ms_BbsVlvM_MainFspCbsHMon;
Ioc_InputSR1msFspCbsMon_t Task_5ms_BbsVlvM_MainFspCbsMon;
Vlvd_A3944_HndlrVlvdInvalid_t Task_5ms_BbsVlvM_MainVlvdInvalid;
Ach_InputAchAsicInvalidInfo_t Task_5ms_AbsVlvM_MainAchAsicInvalid;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_AbsVlvM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Task_5ms_AbsVlvM_MainVBatt2Mon;
Diag_HndlrDiagClr_t Task_5ms_AbsVlvM_MainDiagClrSrs;
Ioc_InputSR1msFspAbsHMon_t Task_5ms_AbsVlvM_MainFspAbsHMon;
Ioc_InputSR1msFspAbsMon_t Task_5ms_AbsVlvM_MainFspAbsMon;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_Mps_TLE5012M_MainVBatt1Mon;
Diag_HndlrDiagClr_t Task_5ms_Mps_TLE5012M_MainDiagClrSrs;
Mps_TLE5012_Hndlr_1msMpsInvalid_t Task_5ms_Mps_TLE5012M_MainMpsInvalid;
Arbitrator_MtrMtrArbDriveState_t Task_5ms_Mps_TLE5012M_MainMtrArbDriveState;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_MgdM_MainVBatt1Mon;
Diag_HndlrDiagClr_t Task_5ms_MgdM_MainDiagClrSrs;
Mgd_TLE9180_HndlrMgdInvalid_t Task_5ms_MgdM_MainMgdInvalid;
Arbitrator_MtrMtrArbDriveState_t Task_5ms_MgdM_MainMtrArbDriveState;
Ioc_InputSR1msVBatt1Mon_t Task_5ms_MtrM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Task_5ms_MtrM_MainVBatt2Mon;
Diag_HndlrDiagClr_t Task_5ms_MtrM_MainDiagClrSrs;
Mgd_TLE9180_HndlrMgdInvalid_t Task_5ms_MtrM_MainMgdInvalid;
Arbitrator_MtrMtrArbDriveState_t Task_5ms_MtrM_MainMtrArbDriveState;
Diag_HndlrDiagClr_t Task_5ms_Watchdog_MainDiagClrSrs;
Diag_HndlrDiagClr_t Task_5ms_Eem_MainDiagClrSrs;
Diag_HndlrDiagSci_t Task_5ms_Eem_MainDiagSci;
Diag_HndlrDiagAhbSci_t Task_5ms_Eem_MainDiagAhbSci;
Diag_HndlrDiagClr_t Task_5ms_Eem_SuspcDetnDiagClrSrs;
Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Arbitrator_VlvAcmAsicInitCompleteFlag;
Diag_HndlrDiagSci_t Task_5ms_Arbitrator_VlvDiagSci;
Diag_HndlrDiagAhbSci_t Task_5ms_Arbitrator_VlvDiagAhbSci;
Diag_HndlrDiagSci_t Task_5ms_Arbitrator_RlyDiagSci;
Diag_HndlrDiagAhbSci_t Task_5ms_Arbitrator_RlyDiagAhbSci;
Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Fsr_ActrAcmAsicInitCompleteFlag;
Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Vlv_ActrSyncAcmAsicInitCompleteFlag;
Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Ioc_OutputCS5msAcmAsicInitCompleteFlag;

/* Output Data Element */
Proxy_RxByComRxYawSerialInfo_t Task_5ms_Proxy_RxByComRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t Task_5ms_Proxy_RxByComRxYawAccInfo;
Proxy_RxByComRxSasInfo_t Task_5ms_Proxy_RxByComRxSasInfo;
Proxy_RxByComRxLongAccInfo_t Task_5ms_Proxy_RxByComRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
Proxy_RxCanRxRegenInfo_t Task_5ms_Proxy_RxCanRxRegenInfo;
Proxy_RxCanRxAccelPedlInfo_t Task_5ms_Proxy_RxCanRxAccelPedlInfo;
Proxy_RxCanRxIdbInfo_t Task_5ms_Proxy_RxCanRxIdbInfo;
Proxy_RxCanRxEngTempInfo_t Task_5ms_Proxy_RxCanRxEngTempInfo;
Proxy_RxCanRxEscInfo_t Task_5ms_Proxy_RxCanRxEscInfo;
Proxy_RxCanRxEemInfo_t Task_5ms_Proxy_RxCanRxEemInfo;
Proxy_RxCanRxInfo_t Task_5ms_Proxy_RxCanRxInfo;
Proxy_RxIMUCalc_t Task_5ms_Proxy_RxIMUCalcInfo;
Nvm_HndlrLogicEepDataInfo_t Task_5ms_Nvm_HndlrLogicEepDataInfo;
Wss_SenWhlSpdInfo_t Task_5ms_Wss_SenWhlSpdInfo;
Wss_SenWhlEdgeCntInfo_t Task_5ms_Wss_SenWhlEdgeCntInfo;
Wss_SenWssSpeedOut_t Task_5ms_Wss_SenWssSpeedOut;
Wss_SenWssCalc_t Task_5ms_Wss_SenWssCalcInfo;
Pedal_SenSyncPdt5msRawInfo_t Task_5ms_Pedal_SenSyncPdt5msRawInfo;
Pedal_SenSyncPdf5msRawInfo_t Task_5ms_Pedal_SenSyncPdf5msRawInfo;
Press_SenSyncPistP5msRawInfo_t Task_5ms_Press_SenSyncPistP5msRawInfo;
Swt_SenEscSwtStInfo_t Task_5ms_Swt_SenEscSwtStInfo;
Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Task_5ms_Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t Task_5ms_Bbc_CtrlBaseBrkCtrlModInfo;
Rbc_CtrlTarRgnBrkTqInfo_t Task_5ms_Rbc_CtrlTarRgnBrkTqInfo;
Abc_CtrlCanTxInfo_t Task_5ms_Abc_CtrlCanTxInfo;
Abc_CtrlAbsCtrlInfo_t Task_5ms_Abc_CtrlAbsCtrlInfo;
Abc_CtrlEscCtrlInfo_t Task_5ms_Abc_CtrlEscCtrlInfo;
Abc_CtrlTcsCtrlInfo_t Task_5ms_Abc_CtrlTcsCtrlInfo;
Pct_5msCtrlStkRecvryActnIfInfo_t Task_5ms_Pct_5msCtrlStkRecvryActnIfInfo;
Vat_CtrlIdbVlvActInfo_t Task_5ms_Vat_CtrlIdbVlvActInfo;
Eem_MainEemFailData_t Task_5ms_Eem_MainEemFailData;
Eem_MainEemCtrlInhibitData_t Task_5ms_Eem_MainEemCtrlInhibitData;
Eem_MainEemSuspectData_t Task_5ms_Eem_MainEemSuspectData;
Eem_MainEemEceData_t Task_5ms_Eem_MainEemEceData;
Eem_MainEemLampData_t Task_5ms_Eem_MainEemLampData;
Eem_SuspcDetnCanTimeOutStInfo_t Task_5ms_Eem_SuspcDetnCanTimeOutStInfo;
Arbitrator_VlvArbWhlVlvReqInfo_t Task_5ms_Arbitrator_VlvArbWhlVlvReqInfo;
Fsr_ActrFsrAbsDrvInfo_t Task_5ms_Fsr_ActrFsrAbsDrvInfo;
Fsr_ActrFsrCbsDrvInfo_t Task_5ms_Fsr_ActrFsrCbsDrvInfo;
Rly_ActrRlyDrvInfo_t Task_5ms_Rly_ActrRlyDrvInfo;
Vlv_ActrSyncNormVlvReqInfo_t Task_5ms_Vlv_ActrSyncNormVlvReqInfo;
Vlv_ActrSyncWhlVlvReqInfo_t Task_5ms_Vlv_ActrSyncWhlVlvReqInfo;
Vlv_ActrSyncBalVlvReqInfo_t Task_5ms_Vlv_ActrSyncBalVlvReqInfo;
Proxy_RxCanRxGearSelDispErrInfo_t Task_5ms_Proxy_RxCanRxGearSelDispErrInfo;
Prly_HndlrIgnOnOffSts_t Task_5ms_Prly_HndlrIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_5ms_Prly_HndlrIgnEdgeSts;
Prly_HndlrPrlyEcuInhibit_t Task_5ms_Prly_HndlrPrlyEcuInhibit;
Mom_HndlrEcuModeSts_t Task_5ms_Mom_HndlrEcuModeSts;
Det_5msCtrlVehSpdFild_t Task_5ms_Det_5msCtrlVehSpdFild;
Abc_CtrlVehSpd_t Task_5ms_Abc_CtrlVehSpd;
Pct_5msCtrlTgtDeltaStkType_t Task_5ms_Pct_5msCtrlTgtDeltaStkType;
Pct_5msCtrlPCtrlBoostMod_t Task_5ms_Pct_5msCtrlPCtrlBoostMod;
Pct_5msCtrlPCtrlFadeoutSt_t Task_5ms_Pct_5msCtrlPCtrlFadeoutSt;
Pct_5msCtrlPCtrlSt_t Task_5ms_Pct_5msCtrlPCtrlSt;
Pct_5msCtrlInVlvAllCloseReq_t Task_5ms_Pct_5msCtrlInVlvAllCloseReq;
Pct_5msCtrlPChamberVolume_t Task_5ms_Pct_5msCtrlPChamberVolume;
Pct_5msCtrlTarDeltaStk_t Task_5ms_Pct_5msCtrlTarDeltaStk;
Pct_5msCtrlFinalTarPFromPCtrl_t Task_5ms_Pct_5msCtrlFinalTarPFromPCtrl;
Eem_SuspcDetnFuncInhibitVlvSts_t Task_5ms_Eem_SuspcDetnFuncInhibitVlvSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
Eem_SuspcDetnFuncInhibitProxySts_t Task_5ms_Eem_SuspcDetnFuncInhibitProxySts;
Eem_SuspcDetnFuncInhibitDiagSts_t Task_5ms_Eem_SuspcDetnFuncInhibitDiagSts;
Eem_SuspcDetnFuncInhibitAcmioSts_t Task_5ms_Eem_SuspcDetnFuncInhibitAcmioSts;
Eem_SuspcDetnFuncInhibitAcmctlSts_t Task_5ms_Eem_SuspcDetnFuncInhibitAcmctlSts;
Eem_SuspcDetnFuncInhibitMspSts_t Task_5ms_Eem_SuspcDetnFuncInhibitMspSts;
Eem_SuspcDetnFuncInhibitPedalSts_t Task_5ms_Eem_SuspcDetnFuncInhibitPedalSts;
Eem_SuspcDetnFuncInhibitPressSts_t Task_5ms_Eem_SuspcDetnFuncInhibitPressSts;
Eem_SuspcDetnFuncInhibitSesSts_t Task_5ms_Eem_SuspcDetnFuncInhibitSesSts;
Eem_SuspcDetnFuncInhibitVlvdSts_t Task_5ms_Eem_SuspcDetnFuncInhibitVlvdSts;
Eem_SuspcDetnFuncInhibitAdcifSts_t Task_5ms_Eem_SuspcDetnFuncInhibitAdcifSts;
Eem_SuspcDetnFuncInhibitCanTrcvSts_t Task_5ms_Eem_SuspcDetnFuncInhibitCanTrcvSts;
Eem_SuspcDetnFuncInhibitMccSts_t Task_5ms_Eem_SuspcDetnFuncInhibitMccSts;
Arbitrator_VlvArbVlvDriveState_t Task_5ms_Arbitrator_VlvArbVlvDriveState;
Fsr_ActrFsrDcMtrShutDwn_t Task_5ms_Fsr_ActrFsrDcMtrShutDwn;
Fsr_ActrFsrEnDrDrv_t Task_5ms_Fsr_ActrFsrEnDrDrv;
Vlv_ActrSyncVlvSync_t Task_5ms_Vlv_ActrSyncVlvSync;

#define TASK_5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
static uint16 SystemStabilzationTimeCnt;
 
#define TASK_5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TASK_5MS_START_SEC_CODE
#include "Task_5ms_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Task_5ms_Init(void)
{    
    Proxy_Rx_Init();
    Proxy_RxByCom_Init();
    Icu_InputCapture_Init();
    Ioc_InputSR5ms_Init();
    Nvm_Hndlr_Init();
    Prly_Hndlr_Init();
    Mom_Hndlr_Init();
    Wss_Sen_Init();
    Pedal_SenSync_Init();
    Press_SenSync_Init();
    Swt_Sen_Init();
    Spc_5msCtrl_Init();
    Det_5msCtrl_Init();
    Bbc_Ctrl_Init();
    Rbc_Ctrl_Init();
    Abc_Ctrl_Init();
    Arb_Ctrl_Init();
    Pct_5msCtrl_Init();
    Vat_Ctrl_Init();
    SysPwrM_Main_Init();
    EcuHwCtrlM_Main_Init();
    WssM_Main_Init();
    PressM_Main_Init();
    PedalM_Main_Init();
    WssP_Main_Init();
    PressP_Main_Init();
    PedalP_Main_Init();
    BbsVlvM_Main_Init();
    AbsVlvM_Main_Init();
    Mps_TLE5012M_Main_Init();
    MgdM_Main_Init();
    MtrM_Main_Init();
    Watchdog_Main_Init();
    Eem_Main_Init();
    Eem_SuspcDetn_Init();
    Arbitrator_Vlv_Init();
    Arbitrator_Rly_Init();
    Fsr_Actr_Init();
    Rly_Actr_Init();
    Vlv_ActrSync_Init();
    Ioc_OutputCS5ms_Init();

    //TimE
    TimE_init();
    
    /* Initialize internal bus */
}

void Task_5ms(void)
{
    uint16 i;
    /* "Read Interfaces" from <other Task> or <ISR Event>
    Assembly connection event between Tasks or Task and ISR
    for example #1) Task1msInterface1 = Task10msInterface1;
    for example #2) Task1msInterface1.Signal1 = Task10msInterface1.Signal1;
    for example #3) In case of Queue at <Shared RAM>, Task1msInterface1 = deQueue(&q);
    Therefore, it's possible to avoid memory violation.*/
    /* Sample interface */
    /* Structure interface */
    Task_5ms_Prly_HndlrSenPwrMonitorData = Task_10ms_SenPwrM_MainSenPwrMonitorData;
    Task_5ms_Wss_SenAchWssPort0AsicInfo = Task_1ms_Ach_InputAchWssPort0AsicInfo;
    Task_5ms_Wss_SenAchWssPort1AsicInfo = Task_1ms_Ach_InputAchWssPort1AsicInfo;
    Task_5ms_Wss_SenAchWssPort2AsicInfo = Task_1ms_Ach_InputAchWssPort2AsicInfo;
    Task_5ms_Wss_SenAchWssPort3AsicInfo = Task_1ms_Ach_InputAchWssPort3AsicInfo;
    Task_5ms_Pedal_SenSyncPdtBufInfo = Task_1ms_Pedal_SenPdtBufInfo;
    Task_5ms_Pedal_SenSyncPdfBufInfo = Task_1ms_Pedal_SenPdfBufInfo;
    Task_5ms_Press_SenSyncCircPBufInfo = Task_1ms_Press_SenCircPBufInfo;
    Task_5ms_Press_SenSyncPistPBufInfo = Task_1ms_Press_SenPistPBufInfo;
    Task_5ms_Press_SenSyncPspBufInfo = Task_1ms_Press_SenPspBufInfo;
    Task_5ms_Swt_SenSwtMonInfo = Task_1ms_Ioc_InputCS1msSwtMonInfo;
    Task_5ms_Swt_SenSwtMonInfoEsc = Task_1ms_Ioc_InputCS1msSwtMonInfoEsc;
    Task_5ms_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo = Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
    Task_5ms_Det_5msCtrlMotRotgAgSigInfo = Task_50us_Msp_CtrlMotRotgAgSigInfo;
    Task_5ms_Bbc_CtrlBrkPedlStatus1msInfo = Task_1ms_Det_1msCtrlBrkPedlStatus1msInfo;
    Task_5ms_Abc_CtrlMotRotgAgSigInfo = Task_50us_Msp_CtrlMotRotgAgSigInfo;
    Task_5ms_Abc_CtrlSasCalInfo = Task_10ms_Diag_HndlrSasCalInfo;
    Task_5ms_Abc_CtrlTxESCSensorInfo = Task_10ms_Proxy_TxTxESCSensorInfo;
    Task_5ms_Abc_CtrlSenPwrMonitorData = Task_10ms_SenPwrM_MainSenPwrMonitorData;
    Task_5ms_Abc_CtrlYAWMSerialInfo = Task_10ms_YawM_MainYAWMSerialInfo;
    Task_5ms_Abc_CtrlYAWMOutInfo = Task_10ms_YawM_MainYAWMOutInfo;
    Task_5ms_Pct_5msCtrlPedlTrvlFild1msInfo = Task_1ms_Spc_1msCtrlPedlTrvlFild1msInfo;
    Task_5ms_Pct_5msCtrlMotRotgAgSigInfo = Task_50us_Msp_CtrlMotRotgAgSigInfo;
    Task_5ms_Pct_5msCtrlBrkPedlStatus1msInfo = Task_1ms_Det_1msCtrlBrkPedlStatus1msInfo;
    Task_5ms_Pct_5msCtrlPedlTrvlRate1msInfo = Task_1ms_Det_1msCtrlPedlTrvlRate1msInfo;
    Task_5ms_Pct_5msCtrlIdbMotPosnInfo = Task_1ms_Mcc_1msCtrlIdbMotPosnInfo;
    Task_5ms_SysPwrM_MainAchSysPwrAsicInfo = Task_1ms_Ach_InputAchSysPwrAsicInfo;
    Task_5ms_EcuHwCtrlM_MainAchSysPwrAsicInfo = Task_1ms_Ach_InputAchSysPwrAsicInfo;
    Task_5ms_EcuHwCtrlM_MainAchValveAsicInfo = Task_1ms_Ach_InputAchValveAsicInfo;
    Task_5ms_WssM_MainAchSysPwrAsicInfo = Task_1ms_Ach_InputAchSysPwrAsicInfo;
    Task_5ms_WssM_MainAchWssPort0AsicInfo = Task_1ms_Ach_InputAchWssPort0AsicInfo;
    Task_5ms_WssM_MainAchWssPort1AsicInfo = Task_1ms_Ach_InputAchWssPort1AsicInfo;
    Task_5ms_WssM_MainAchWssPort2AsicInfo = Task_1ms_Ach_InputAchWssPort2AsicInfo;
    Task_5ms_WssM_MainAchWssPort3AsicInfo = Task_1ms_Ach_InputAchWssPort3AsicInfo;
    Task_5ms_PressM_MainHalPressureInfo = Task_1ms_Ioc_InputSR1msHalPressureInfo;
    Task_5ms_PressM_MainSentHPressureInfo = Task_1ms_SentH_MainSentHPressureInfo;
    Task_5ms_PedalM_MainAchSysPwrAsicInfo = Task_1ms_Ach_InputAchSysPwrAsicInfo;
    Task_5ms_PedalM_MainPedlSigMonInfo = Task_1ms_Ioc_InputSR1msPedlSigMonInfo;
    Task_5ms_PedalM_MainPedlPwrMonInfo = Task_1ms_Ioc_InputSR1msPedlPwrMonInfo;
    Task_5ms_PressP_MainPressCalcInfo = Task_1ms_Press_SenPressCalcInfo;
    Task_5ms_BbsVlvM_MainAchSysPwrAsicInfo = Task_1ms_Ach_InputAchSysPwrAsicInfo;
    Task_5ms_BbsVlvM_MainAchValveAsicInfo = Task_1ms_Ach_InputAchValveAsicInfo;
    Task_5ms_BbsVlvM_MainVlvdFF0DcdInfo = Task_1ms_Vlvd_A3944_HndlrVlvdFF0DcdInfo;
    Task_5ms_BbsVlvM_MainVlvdFF1DcdInfo = Task_1ms_Vlvd_A3944_HndlrVlvdFF1DcdInfo;
    Task_5ms_AbsVlvM_MainAchSysPwrAsicInfo = Task_1ms_Ach_InputAchSysPwrAsicInfo;
    Task_5ms_AbsVlvM_MainAchValveAsicInfo = Task_1ms_Ach_InputAchValveAsicInfo;
    Task_5ms_Mps_TLE5012M_MainMpsD1SpiDcdInfo = Task_1ms_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
    Task_5ms_Mps_TLE5012M_MainMpsD2SpiDcdInfo = Task_1ms_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
    Task_5ms_MgdM_MainMgdDcdInfo = Task_1ms_Mgd_TLE9180_HndlrMgdDcdInfo;
    Task_5ms_MtrM_MainMotMonInfo = Task_1ms_Ioc_InputSR1msMotMonInfo;
    Task_5ms_MtrM_MainMotVoltsMonInfo = Task_1ms_Ioc_InputSR1msMotVoltsMonInfo;
    Task_5ms_MtrM_MainMpsD1SpiDcdInfo = Task_1ms_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
    Task_5ms_MtrM_MainMpsD2SpiDcdInfo = Task_1ms_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
    Task_5ms_MtrM_MainMgdDcdInfo = Task_1ms_Mgd_TLE9180_HndlrMgdDcdInfo;
    Task_5ms_MtrM_MainMotDqIRefMccInfo = Task_1ms_Mcc_1msCtrlMotDqIRefMccInfo;
    Task_5ms_MtrM_MainHwTrigMotInfo = Task_50us_AdcIf_Conv50usHwTrigMotInfo;
    Task_5ms_MtrM_MainMotCurrMonInfo = Task_50us_Ioc_InputSR50usMotCurrMonInfo;
    Task_5ms_MtrM_MainMotAngleMonInfo = Task_50us_Ioc_InputSR50usMotAngleMonInfo;
    Task_5ms_MtrM_MainMtrProcessOutInfo = Task_50us_Mtr_Processing_SigMtrProcessOutInfo;
    Task_5ms_Eem_MainRelayMonitorData = Task_10ms_RlyM_MainRelayMonitorData;
    Task_5ms_Eem_MainSenPwrMonitorData = Task_10ms_SenPwrM_MainSenPwrMonitorData;
    Task_5ms_Eem_MainSwtMonFaultInfo = Task_10ms_SwtM_MainSwtMonFaultInfo;
    Task_5ms_Eem_MainSwtPFaultInfo = Task_10ms_SwtP_MainSwtPFaultInfo;
    Task_5ms_Eem_MainYAWMOutInfo = Task_10ms_YawM_MainYAWMOutInfo;
    Task_5ms_Eem_MainAYMOuitInfo = Task_10ms_AyM_MainAYMOuitInfo;
    Task_5ms_Eem_MainAXMOutInfo = Task_10ms_AxM_MainAXMOutInfo;
    Task_5ms_Eem_MainSASMOutInfo = Task_10ms_SasM_MainSASMOutInfo;
    Task_5ms_Eem_MainCanMonData = Task_10ms_CanM_MainCanMonData;
    Task_5ms_Eem_MainYawPlauOutput = Task_10ms_YawP_MainYawPlauOutput;
    Task_5ms_Eem_MainAxPlauOutput = Task_10ms_AxP_MainAxPlauOutput;
    Task_5ms_Eem_MainAyPlauOutput = Task_10ms_AyP_MainAyPlauOutput;
    Task_5ms_Eem_MainSasPlauOutput = Task_10ms_SasP_MainSasPlauOutput;
    Task_5ms_Arbitrator_VlvNormVlvReqSesInfo = Task_1ms_Ses_CtrlNormVlvReqSesInfo;
    Task_5ms_Arbitrator_VlvWhlVlvReqSesInfo = Task_1ms_Ses_CtrlWhlVlvReqSesInfo;
    Task_5ms_Arbitrator_VlvDiagVlvActrInfo = Task_10ms_Diag_HndlrDiagVlvActrInfo;

    /* Single interface */
    Task_5ms_Ioc_InputSR5msAcmAsicInitCompleteFlag = Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
    Task_5ms_Prly_HndlrCEMon = Task_1ms_Ioc_InputSR1msCEMon;
    Task_5ms_Swt_SenRsmDbcMon = Task_1ms_Ioc_InputCS1msRsmDbcMon;
    Task_5ms_Spc_5msCtrlMotCtrlState = Task_1ms_Mcc_1msCtrlMotCtrlState;
    Task_5ms_Abc_CtrlVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_Abc_CtrlDiagSci = Task_10ms_Diag_HndlrDiagSci;
    Task_5ms_Pct_5msCtrlMotCtrlMode = Task_1ms_Mcc_1msCtrlMotCtrlMode;
    Task_5ms_Pct_5msCtrlMotICtrlFadeOutState = Task_1ms_Mcc_1msCtrlMotICtrlFadeOutState;
    Task_5ms_Pct_5msCtrlPreBoostMod = Task_1ms_Pct_1msCtrlPreBoostMod;
    Task_5ms_Pct_5msCtrlStkRecvryStabnEndOK = Task_1ms_Mcc_1msCtrlStkRecvryStabnEndOK;
    Task_5ms_SysPwrM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_SysPwrM_MainVBatt2Mon = Task_1ms_Ioc_InputSR1msVBatt2Mon;
    Task_5ms_SysPwrM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_SysPwrM_MainVdd1Mon = Task_1ms_Ioc_InputSR1msVdd1Mon;
    Task_5ms_SysPwrM_MainVdd2Mon = Task_1ms_Ioc_InputSR1msVdd2Mon;
    Task_5ms_SysPwrM_MainVdd3Mon = Task_1ms_Ioc_InputSR1msVdd3Mon;
    Task_5ms_SysPwrM_MainVdd4Mon = Task_1ms_Ioc_InputSR1msVdd4Mon;
    Task_5ms_SysPwrM_MainVdd5Mon = Task_1ms_Ioc_InputSR1msVdd5Mon;
    Task_5ms_SysPwrM_MainVddMon = Task_1ms_Ioc_InputSR1msVddMon;
    Task_5ms_SysPwrM_MainMtrArbDriveState = Task_1ms_Arbitrator_MtrMtrArbDriveState;
    Task_5ms_EcuHwCtrlM_MainAchAsicInvalid = Task_1ms_Ach_InputAchAsicInvalid;
    Task_5ms_EcuHwCtrlM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_EcuHwCtrlM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_EcuHwCtrlM_MainAcmAsicInitCompleteFlag = Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
    Task_5ms_EcuHwCtrlM_MainVdd1Mon = Task_1ms_Ioc_InputSR1msVdd1Mon;
    Task_5ms_EcuHwCtrlM_MainVdd2Mon = Task_1ms_Ioc_InputSR1msVdd2Mon;
    Task_5ms_EcuHwCtrlM_MainVdd3Mon = Task_1ms_Ioc_InputSR1msVdd3Mon;
    Task_5ms_EcuHwCtrlM_MainVdd4Mon = Task_1ms_Ioc_InputSR1msVdd4Mon;
    Task_5ms_EcuHwCtrlM_MainVdd5Mon = Task_1ms_Ioc_InputSR1msVdd5Mon;
    Task_5ms_EcuHwCtrlM_MainAdcInvalid = Task_1ms_AdcIf_Conv1msAdcInvalid;
    Task_5ms_WssM_MainAchAsicInvalid = Task_1ms_Ach_InputAchAsicInvalid;
    Task_5ms_WssM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_WssM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_PressM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_PressM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_PedalM_MainAchAsicInvalid = Task_1ms_Ach_InputAchAsicInvalid;
    Task_5ms_PedalM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_PedalM_MainVdd3Mon = Task_1ms_Ioc_InputSR1msVdd3Mon;
    Task_5ms_WssP_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_PressP_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_PressP_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_PedalP_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_BbsVlvM_MainAchAsicInvalid = Task_1ms_Ach_InputAchAsicInvalid;
    Task_5ms_BbsVlvM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_BbsVlvM_MainVBatt2Mon = Task_1ms_Ioc_InputSR1msVBatt2Mon;
    Task_5ms_BbsVlvM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_BbsVlvM_MainFspCbsHMon = Task_1ms_Ioc_InputSR1msFspCbsHMon;
    Task_5ms_BbsVlvM_MainFspCbsMon = Task_1ms_Ioc_InputSR1msFspCbsMon;
    Task_5ms_BbsVlvM_MainVlvdInvalid = Task_1ms_Vlvd_A3944_HndlrVlvdInvalid;
    Task_5ms_AbsVlvM_MainAchAsicInvalid = Task_1ms_Ach_InputAchAsicInvalid;
    Task_5ms_AbsVlvM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_AbsVlvM_MainVBatt2Mon = Task_1ms_Ioc_InputSR1msVBatt2Mon;
    Task_5ms_AbsVlvM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_AbsVlvM_MainFspAbsHMon = Task_1ms_Ioc_InputSR1msFspAbsHMon;
    Task_5ms_AbsVlvM_MainFspAbsMon = Task_1ms_Ioc_InputSR1msFspAbsMon;
    Task_5ms_Mps_TLE5012M_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_Mps_TLE5012M_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_Mps_TLE5012M_MainMpsInvalid = Task_1ms_Mps_TLE5012_Hndlr_1msMpsInvalid;
    Task_5ms_Mps_TLE5012M_MainMtrArbDriveState = Task_1ms_Arbitrator_MtrMtrArbDriveState;
    Task_5ms_MgdM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_MgdM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_MgdM_MainMgdInvalid = Task_1ms_Mgd_TLE9180_HndlrMgdInvalid;
    Task_5ms_MgdM_MainMtrArbDriveState = Task_1ms_Arbitrator_MtrMtrArbDriveState;
    Task_5ms_MtrM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_5ms_MtrM_MainVBatt2Mon = Task_1ms_Ioc_InputSR1msVBatt2Mon;
    Task_5ms_MtrM_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_MtrM_MainMgdInvalid = Task_1ms_Mgd_TLE9180_HndlrMgdInvalid;
    Task_5ms_MtrM_MainMtrArbDriveState = Task_1ms_Arbitrator_MtrMtrArbDriveState;
    Task_5ms_Watchdog_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_Eem_MainDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_Eem_MainDiagSci = Task_10ms_Diag_HndlrDiagSci;
    Task_5ms_Eem_MainDiagAhbSci = Task_10ms_Diag_HndlrDiagAhbSci;
    Task_5ms_Eem_SuspcDetnDiagClrSrs = Task_10ms_Diag_HndlrDiagClrSrs;
    Task_5ms_Arbitrator_VlvAcmAsicInitCompleteFlag = Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
    Task_5ms_Arbitrator_VlvDiagSci = Task_10ms_Diag_HndlrDiagSci;
    Task_5ms_Arbitrator_VlvDiagAhbSci = Task_10ms_Diag_HndlrDiagAhbSci;
    Task_5ms_Arbitrator_RlyDiagSci = Task_10ms_Diag_HndlrDiagSci;
    Task_5ms_Arbitrator_RlyDiagAhbSci = Task_10ms_Diag_HndlrDiagAhbSci;
    Task_5ms_Fsr_ActrAcmAsicInitCompleteFlag = Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
    Task_5ms_Vlv_ActrSyncAcmAsicInitCompleteFlag = Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
    Task_5ms_Ioc_OutputCS5msAcmAsicInitCompleteFlag = Task_1ms_Acm_MainAcmAsicInitCompleteFlag;

    /* End of Sample interface

    /* Input */
    /* Structure interface */
    /* Decomposed structure interface */
    Task_5ms_Read_Prly_HndlrSenPwrMonitorData_SenPwrM_5V_DriveReq(&Task_5msBus.Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq);
    Task_5ms_Read_Prly_HndlrSenPwrMonitorData_SenPwrM_12V_Drive_Req(&Task_5msBus.Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req);

    Task_5ms_Read_Wss_SenAchWssPort0AsicInfo(&Task_5msBus.Wss_SenAchWssPort0AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort0AsicInfo 
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_Wss_SenAchWssPort1AsicInfo(&Task_5msBus.Wss_SenAchWssPort1AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort1AsicInfo 
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_Wss_SenAchWssPort2AsicInfo(&Task_5msBus.Wss_SenAchWssPort2AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort2AsicInfo 
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_Wss_SenAchWssPort3AsicInfo(&Task_5msBus.Wss_SenAchWssPort3AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort3AsicInfo 
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_Pedal_SenSyncPdtBufInfo(&Task_5msBus.Pedal_SenSyncPdtBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdtBufInfo 
     : Pedal_SenSyncPdtBufInfo.PdtRaw;
     =============================================================================*/

    Task_5ms_Read_Pedal_SenSyncPdfBufInfo(&Task_5msBus.Pedal_SenSyncPdfBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdfBufInfo 
     : Pedal_SenSyncPdfBufInfo.PdfRaw;
     =============================================================================*/

    Task_5ms_Read_Press_SenSyncCircPBufInfo(&Task_5msBus.Press_SenSyncCircPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncCircPBufInfo 
     : Press_SenSyncCircPBufInfo.PrimCircPRaw;
     : Press_SenSyncCircPBufInfo.SecdCircPRaw;
     =============================================================================*/

    Task_5ms_Read_Press_SenSyncPistPBufInfo(&Task_5msBus.Press_SenSyncPistPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPistPBufInfo 
     : Press_SenSyncPistPBufInfo.PistPRaw;
     =============================================================================*/

    Task_5ms_Read_Press_SenSyncPspBufInfo(&Task_5msBus.Press_SenSyncPspBufInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPspBufInfo 
     : Press_SenSyncPspBufInfo.PedlSimPRaw;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Swt_SenSwtMonInfo_AvhSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.AvhSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_BlsSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.BlsSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_BsSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.BsSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_DoorSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.DoorSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_EscSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.EscSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_FlexBrkASwtMon(&Task_5msBus.Swt_SenSwtMonInfo.FlexBrkASwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_FlexBrkBSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.FlexBrkBSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_HzrdSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.HzrdSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_HdcSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.HdcSwtMon);
    Task_5ms_Read_Swt_SenSwtMonInfo_PbSwtMon(&Task_5msBus.Swt_SenSwtMonInfo.PbSwtMon);

    Task_5ms_Read_Swt_SenSwtMonInfoEsc(&Task_5msBus.Swt_SenSwtMonInfoEsc);
    /*==============================================================================
    * Members of structure Swt_SenSwtMonInfoEsc 
     : Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.EscSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.PbSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc;
     =============================================================================*/

    Task_5ms_Read_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo(&Task_5msBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo 
     : Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
     : Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Det_5msCtrlMotRotgAgSigInfo_StkPosnMeasd(&Task_5msBus.Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd);

    Task_5ms_Read_Bbc_CtrlBrkPedlStatus1msInfo(&Task_5msBus.Bbc_CtrlBrkPedlStatus1msInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlBrkPedlStatus1msInfo 
     : Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Abc_CtrlMotRotgAgSigInfo_StkPosnMeasd(&Task_5msBus.Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd);

    Task_5ms_Read_Abc_CtrlSasCalInfo(&Task_5msBus.Abc_CtrlSasCalInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlSasCalInfo 
     : Abc_CtrlSasCalInfo.DiagSasCaltoAppl;
     =============================================================================*/

    Task_5ms_Read_Abc_CtrlTxESCSensorInfo(&Task_5msBus.Abc_CtrlTxESCSensorInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlTxESCSensorInfo 
     : Abc_CtrlTxESCSensorInfo.EstimatedYaw;
     : Abc_CtrlTxESCSensorInfo.EstimatedAY;
     : Abc_CtrlTxESCSensorInfo.A_long_1_1000g;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Abc_CtrlSenPwrMonitorData_SenPwrM_12V_Stable(&Task_5msBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable);

    Task_5ms_Read_Abc_CtrlYAWMSerialInfo(&Task_5msBus.Abc_CtrlYAWMSerialInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlYAWMSerialInfo 
     : Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg;
     : Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Abc_CtrlYAWMOutInfo_YAWM_Temperature_Err(&Task_5msBus.Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err);

    Task_5ms_Read_Pct_5msCtrlPedlTrvlFild1msInfo(&Task_5msBus.Pct_5msCtrlPedlTrvlFild1msInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlPedlTrvlFild1msInfo 
     : Pct_5msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Pct_5msCtrlMotRotgAgSigInfo_StkPosnMeasd(&Task_5msBus.Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd);

    Task_5ms_Read_Pct_5msCtrlBrkPedlStatus1msInfo(&Task_5msBus.Pct_5msCtrlBrkPedlStatus1msInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlBrkPedlStatus1msInfo 
     : Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/

    Task_5ms_Read_Pct_5msCtrlPedlTrvlRate1msInfo(&Task_5msBus.Pct_5msCtrlPedlTrvlRate1msInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlPedlTrvlRate1msInfo 
     : Pct_5msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/

    Task_5ms_Read_Pct_5msCtrlIdbMotPosnInfo(&Task_5msBus.Pct_5msCtrlIdbMotPosnInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlIdbMotPosnInfo 
     : Pct_5msCtrlIdbMotPosnInfo.MotTarPosn;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd);
    Task_5ms_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg(&Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg);

    Task_5ms_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo(&Task_5msBus.EcuHwCtrlM_MainAchSysPwrAsicInfo);
    /*==============================================================================
    * Members of structure EcuHwCtrlM_MainAchSysPwrAsicInfo 
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_OV(&Task_5msBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV);
    Task_5ms_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_UV(&Task_5msBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV);
    Task_5ms_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(&Task_5msBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT);

    /* Decomposed structure interface */
    Task_5ms_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    Task_5ms_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    Task_5ms_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    Task_5ms_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    Task_5ms_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);

    Task_5ms_Read_WssM_MainAchWssPort0AsicInfo(&Task_5msBus.WssM_MainAchWssPort0AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort0AsicInfo 
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_WssM_MainAchWssPort1AsicInfo(&Task_5msBus.WssM_MainAchWssPort1AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort1AsicInfo 
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_WssM_MainAchWssPort2AsicInfo(&Task_5msBus.WssM_MainAchWssPort2AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort2AsicInfo 
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_WssM_MainAchWssPort3AsicInfo(&Task_5msBus.WssM_MainAchWssPort3AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort3AsicInfo 
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_5ms_Read_PressM_MainHalPressureInfo(&Task_5msBus.PressM_MainHalPressureInfo);
    /*==============================================================================
    * Members of structure PressM_MainHalPressureInfo 
     : PressM_MainHalPressureInfo.McpPresData1;
     : PressM_MainHalPressureInfo.McpPresData2;
     : PressM_MainHalPressureInfo.McpSentCrc;
     : PressM_MainHalPressureInfo.McpSentData;
     : PressM_MainHalPressureInfo.McpSentMsgId;
     : PressM_MainHalPressureInfo.McpSentSerialCrc;
     : PressM_MainHalPressureInfo.McpSentConfig;
     : PressM_MainHalPressureInfo.Wlp1PresData1;
     : PressM_MainHalPressureInfo.Wlp1PresData2;
     : PressM_MainHalPressureInfo.Wlp1SentCrc;
     : PressM_MainHalPressureInfo.Wlp1SentData;
     : PressM_MainHalPressureInfo.Wlp1SentMsgId;
     : PressM_MainHalPressureInfo.Wlp1SentSerialCrc;
     : PressM_MainHalPressureInfo.Wlp1SentConfig;
     : PressM_MainHalPressureInfo.Wlp2PresData1;
     : PressM_MainHalPressureInfo.Wlp2PresData2;
     : PressM_MainHalPressureInfo.Wlp2SentCrc;
     : PressM_MainHalPressureInfo.Wlp2SentData;
     : PressM_MainHalPressureInfo.Wlp2SentMsgId;
     : PressM_MainHalPressureInfo.Wlp2SentSerialCrc;
     : PressM_MainHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_PressM_MainSentHPressureInfo_McpSentInvalid(&Task_5msBus.PressM_MainSentHPressureInfo.McpSentInvalid);
    Task_5ms_Read_PressM_MainSentHPressureInfo_MCPSentSerialInvalid(&Task_5msBus.PressM_MainSentHPressureInfo.MCPSentSerialInvalid);
    Task_5ms_Read_PressM_MainSentHPressureInfo_Wlp1SentInvalid(&Task_5msBus.PressM_MainSentHPressureInfo.Wlp1SentInvalid);
    Task_5ms_Read_PressM_MainSentHPressureInfo_Wlp1SentSerialInvalid(&Task_5msBus.PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid);
    Task_5ms_Read_PressM_MainSentHPressureInfo_Wlp2SentInvalid(&Task_5msBus.PressM_MainSentHPressureInfo.Wlp2SentInvalid);
    Task_5ms_Read_PressM_MainSentHPressureInfo_Wlp2SentSerialInvalid(&Task_5msBus.PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid);

    /* Decomposed structure interface */
    Task_5ms_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&Task_5msBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    Task_5ms_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&Task_5msBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    Task_5ms_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&Task_5msBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);

    Task_5ms_Read_PedalM_MainPedlSigMonInfo(&Task_5msBus.PedalM_MainPedlSigMonInfo);
    /*==============================================================================
    * Members of structure PedalM_MainPedlSigMonInfo 
     : PedalM_MainPedlSigMonInfo.PdfSigMon;
     : PedalM_MainPedlSigMonInfo.PdtSigMon;
     =============================================================================*/

    Task_5ms_Read_PedalM_MainPedlPwrMonInfo(&Task_5msBus.PedalM_MainPedlPwrMonInfo);
    /*==============================================================================
    * Members of structure PedalM_MainPedlPwrMonInfo 
     : PedalM_MainPedlPwrMonInfo.Pdt5vMon;
     : PedalM_MainPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_PressP_MainPressCalcInfo_PressP_SimP_1_100_bar(&Task_5msBus.PressP_MainPressCalcInfo.PressP_SimP_1_100_bar);
    Task_5ms_Read_PressP_MainPressCalcInfo_PressP_CirP1_1_100_bar(&Task_5msBus.PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar);
    Task_5ms_Read_PressP_MainPressCalcInfo_PressP_CirP2_1_100_bar(&Task_5msBus.PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar);

    /* Decomposed structure interface */
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    Task_5ms_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);

    /* Decomposed structure interface */
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_OV(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_UV(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_UV(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_OV(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_PMP_LD_ACT(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_ON(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_OFF(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VHD_OV(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV);
    Task_5ms_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(&Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT);

    Task_5ms_Read_BbsVlvM_MainVlvdFF0DcdInfo(&Task_5msBus.BbsVlvM_MainVlvdFF0DcdInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainVlvdFF0DcdInfo 
     : BbsVlvM_MainVlvdFF0DcdInfo.C0ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C0sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C0sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.Fr;
     : BbsVlvM_MainVlvdFF0DcdInfo.Ot;
     : BbsVlvM_MainVlvdFF0DcdInfo.Lr;
     : BbsVlvM_MainVlvdFF0DcdInfo.Uv;
     : BbsVlvM_MainVlvdFF0DcdInfo.Ff;
     =============================================================================*/

    Task_5ms_Read_BbsVlvM_MainVlvdFF1DcdInfo(&Task_5msBus.BbsVlvM_MainVlvdFF1DcdInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainVlvdFF1DcdInfo 
     : BbsVlvM_MainVlvdFF1DcdInfo.C3ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C3sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C3sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.Fr;
     : BbsVlvM_MainVlvdFF1DcdInfo.Ot;
     : BbsVlvM_MainVlvdFF1DcdInfo.Lr;
     : BbsVlvM_MainVlvdFF1DcdInfo.Uv;
     : BbsVlvM_MainVlvdFF1DcdInfo.Ff;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    Task_5ms_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);

    Task_5ms_Read_AbsVlvM_MainAchValveAsicInfo(&Task_5msBus.AbsVlvM_MainAchValveAsicInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainAchValveAsicInfo 
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
     =============================================================================*/

    Task_5ms_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo(&Task_5msBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD1SpiDcdInfo 
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV;
     =============================================================================*/

    Task_5ms_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo(&Task_5msBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD2SpiDcdInfo 
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV;
     =============================================================================*/

    Task_5ms_Read_MgdM_MainMgdDcdInfo(&Task_5msBus.MgdM_MainMgdDcdInfo);
    /*==============================================================================
    * Members of structure MgdM_MainMgdDcdInfo 
     : MgdM_MainMgdDcdInfo.mgdIdleM;
     : MgdM_MainMgdDcdInfo.mgdConfM;
     : MgdM_MainMgdDcdInfo.mgdConfLock;
     : MgdM_MainMgdDcdInfo.mgdSelfTestM;
     : MgdM_MainMgdDcdInfo.mgdSoffM;
     : MgdM_MainMgdDcdInfo.mgdErrM;
     : MgdM_MainMgdDcdInfo.mgdRectM;
     : MgdM_MainMgdDcdInfo.mgdNormM;
     : MgdM_MainMgdDcdInfo.mgdOsf;
     : MgdM_MainMgdDcdInfo.mgdOp;
     : MgdM_MainMgdDcdInfo.mgdScd;
     : MgdM_MainMgdDcdInfo.mgdSd;
     : MgdM_MainMgdDcdInfo.mgdIndiag;
     : MgdM_MainMgdDcdInfo.mgdOutp;
     : MgdM_MainMgdDcdInfo.mgdExt;
     : MgdM_MainMgdDcdInfo.mgdInt12;
     : MgdM_MainMgdDcdInfo.mgdRom;
     : MgdM_MainMgdDcdInfo.mgdLimpOn;
     : MgdM_MainMgdDcdInfo.mgdStIncomplete;
     : MgdM_MainMgdDcdInfo.mgdApcAct;
     : MgdM_MainMgdDcdInfo.mgdGtm;
     : MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid;
     : MgdM_MainMgdDcdInfo.mgdLfw;
     : MgdM_MainMgdDcdInfo.mgdErrOtW;
     : MgdM_MainMgdDcdInfo.mgdErrOvReg1;
     : MgdM_MainMgdDcdInfo.mgdErrUvVccRom;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg4;
     : MgdM_MainMgdDcdInfo.mgdErrOvReg6;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg6;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg5;
     : MgdM_MainMgdDcdInfo.mgdErrUvCb;
     : MgdM_MainMgdDcdInfo.mgdErrClkTrim;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs3;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs2;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs1;
     : MgdM_MainMgdDcdInfo.mgdErrCp2;
     : MgdM_MainMgdDcdInfo.mgdErrCp1;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs3;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs2;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs1;
     : MgdM_MainMgdDcdInfo.mgdErrOvVdh;
     : MgdM_MainMgdDcdInfo.mgdErrUvVdh;
     : MgdM_MainMgdDcdInfo.mgdErrOvVs;
     : MgdM_MainMgdDcdInfo.mgdErrUvVs;
     : MgdM_MainMgdDcdInfo.mgdErrUvVcc;
     : MgdM_MainMgdDcdInfo.mgdErrOvVcc;
     : MgdM_MainMgdDcdInfo.mgdErrOvLdVdh;
     : MgdM_MainMgdDcdInfo.mgdSdDdpStuck;
     : MgdM_MainMgdDcdInfo.mgdSdCp1;
     : MgdM_MainMgdDcdInfo.mgdSdOvCp;
     : MgdM_MainMgdDcdInfo.mgdSdClkfail;
     : MgdM_MainMgdDcdInfo.mgdSdUvCb;
     : MgdM_MainMgdDcdInfo.mgdSdOvVdh;
     : MgdM_MainMgdDcdInfo.mgdSdOvVs;
     : MgdM_MainMgdDcdInfo.mgdSdOt;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs3;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs2;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs1;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs3;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs2;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs1;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs3;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs2;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs1;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs3;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs2;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs2;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs3;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs2;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs3;
     : MgdM_MainMgdDcdInfo.mgdErrSpiFrame;
     : MgdM_MainMgdDcdInfo.mgdErrSpiTo;
     : MgdM_MainMgdDcdInfo.mgdErrSpiWd;
     : MgdM_MainMgdDcdInfo.mgdErrSpiCrc;
     : MgdM_MainMgdDcdInfo.mgdEpiAddInvalid;
     : MgdM_MainMgdDcdInfo.mgdEonfTo;
     : MgdM_MainMgdDcdInfo.mgdEonfSigInvalid;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp1;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp2;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp3;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOutpErrn;
     : MgdM_MainMgdDcdInfo.mgdErrOutpMiso;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB1;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB2;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB3;
     : MgdM_MainMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_MtrM_MainMotMonInfo_MotPwrVoltMon(&Task_5msBus.MtrM_MainMotMonInfo.MotPwrVoltMon);

    Task_5ms_Read_MtrM_MainMotVoltsMonInfo(&Task_5msBus.MtrM_MainMotVoltsMonInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMotVoltsMonInfo 
     : MtrM_MainMotVoltsMonInfo.MotVoltPhUMon;
     : MtrM_MainMotVoltsMonInfo.MotVoltPhVMon;
     : MtrM_MainMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_MtrM_MainMpsD1SpiDcdInfo_S_MAGOL(&Task_5msBus.MtrM_MainMpsD1SpiDcdInfo.S_MAGOL);

    /* Decomposed structure interface */
    Task_5ms_Read_MtrM_MainMpsD2SpiDcdInfo_S_MAGOL(&Task_5msBus.MtrM_MainMpsD2SpiDcdInfo.S_MAGOL);

    Task_5ms_Read_MtrM_MainMgdDcdInfo(&Task_5msBus.MtrM_MainMgdDcdInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMgdDcdInfo 
     : MtrM_MainMgdDcdInfo.mgdIdleM;
     : MtrM_MainMgdDcdInfo.mgdConfM;
     : MtrM_MainMgdDcdInfo.mgdConfLock;
     : MtrM_MainMgdDcdInfo.mgdSelfTestM;
     : MtrM_MainMgdDcdInfo.mgdSoffM;
     : MtrM_MainMgdDcdInfo.mgdErrM;
     : MtrM_MainMgdDcdInfo.mgdRectM;
     : MtrM_MainMgdDcdInfo.mgdNormM;
     : MtrM_MainMgdDcdInfo.mgdOsf;
     : MtrM_MainMgdDcdInfo.mgdOp;
     : MtrM_MainMgdDcdInfo.mgdScd;
     : MtrM_MainMgdDcdInfo.mgdSd;
     : MtrM_MainMgdDcdInfo.mgdIndiag;
     : MtrM_MainMgdDcdInfo.mgdOutp;
     : MtrM_MainMgdDcdInfo.mgdExt;
     : MtrM_MainMgdDcdInfo.mgdInt12;
     : MtrM_MainMgdDcdInfo.mgdRom;
     : MtrM_MainMgdDcdInfo.mgdLimpOn;
     : MtrM_MainMgdDcdInfo.mgdStIncomplete;
     : MtrM_MainMgdDcdInfo.mgdApcAct;
     : MtrM_MainMgdDcdInfo.mgdGtm;
     : MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid;
     : MtrM_MainMgdDcdInfo.mgdLfw;
     : MtrM_MainMgdDcdInfo.mgdErrOtW;
     : MtrM_MainMgdDcdInfo.mgdErrOvReg1;
     : MtrM_MainMgdDcdInfo.mgdErrUvVccRom;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg4;
     : MtrM_MainMgdDcdInfo.mgdErrOvReg6;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg6;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg5;
     : MtrM_MainMgdDcdInfo.mgdErrUvCb;
     : MtrM_MainMgdDcdInfo.mgdErrClkTrim;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs3;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs2;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs1;
     : MtrM_MainMgdDcdInfo.mgdErrCp2;
     : MtrM_MainMgdDcdInfo.mgdErrCp1;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs3;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs2;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs1;
     : MtrM_MainMgdDcdInfo.mgdErrOvVdh;
     : MtrM_MainMgdDcdInfo.mgdErrUvVdh;
     : MtrM_MainMgdDcdInfo.mgdErrOvVs;
     : MtrM_MainMgdDcdInfo.mgdErrUvVs;
     : MtrM_MainMgdDcdInfo.mgdErrUvVcc;
     : MtrM_MainMgdDcdInfo.mgdErrOvVcc;
     : MtrM_MainMgdDcdInfo.mgdErrOvLdVdh;
     : MtrM_MainMgdDcdInfo.mgdSdDdpStuck;
     : MtrM_MainMgdDcdInfo.mgdSdCp1;
     : MtrM_MainMgdDcdInfo.mgdSdOvCp;
     : MtrM_MainMgdDcdInfo.mgdSdClkfail;
     : MtrM_MainMgdDcdInfo.mgdSdUvCb;
     : MtrM_MainMgdDcdInfo.mgdSdOvVdh;
     : MtrM_MainMgdDcdInfo.mgdSdOvVs;
     : MtrM_MainMgdDcdInfo.mgdSdOt;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs3;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs2;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs1;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs3;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs2;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs1;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs3;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs2;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs1;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs3;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs2;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs2;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs3;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs2;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs3;
     : MtrM_MainMgdDcdInfo.mgdErrSpiFrame;
     : MtrM_MainMgdDcdInfo.mgdErrSpiTo;
     : MtrM_MainMgdDcdInfo.mgdErrSpiWd;
     : MtrM_MainMgdDcdInfo.mgdErrSpiCrc;
     : MtrM_MainMgdDcdInfo.mgdEpiAddInvalid;
     : MtrM_MainMgdDcdInfo.mgdEonfTo;
     : MtrM_MainMgdDcdInfo.mgdEonfSigInvalid;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp1;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp2;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp3;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOutpErrn;
     : MtrM_MainMgdDcdInfo.mgdErrOutpMiso;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB1;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB2;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB3;
     : MtrM_MainMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/

    Task_5ms_Read_MtrM_MainMotDqIRefMccInfo(&Task_5msBus.MtrM_MainMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMotDqIRefMccInfo 
     : MtrM_MainMotDqIRefMccInfo.IdRef;
     : MtrM_MainMotDqIRefMccInfo.IqRef;
     =============================================================================*/

    Task_5ms_Read_MtrM_MainHwTrigMotInfo(&Task_5msBus.MtrM_MainHwTrigMotInfo);
    /*==============================================================================
    * Members of structure MtrM_MainHwTrigMotInfo 
     : MtrM_MainHwTrigMotInfo.Uphase0Mon;
     : MtrM_MainHwTrigMotInfo.Uphase1Mon;
     : MtrM_MainHwTrigMotInfo.Vphase0Mon;
     : MtrM_MainHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/

    Task_5ms_Read_MtrM_MainMotCurrMonInfo(&Task_5msBus.MtrM_MainMotCurrMonInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMotCurrMonInfo 
     : MtrM_MainMotCurrMonInfo.MotCurrPhUMon0;
     : MtrM_MainMotCurrMonInfo.MotCurrPhUMon1;
     : MtrM_MainMotCurrMonInfo.MotCurrPhVMon0;
     : MtrM_MainMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_MtrM_MainMotAngleMonInfo_MotPosiAngle1deg(&Task_5msBus.MtrM_MainMotAngleMonInfo.MotPosiAngle1deg);
    Task_5ms_Read_MtrM_MainMotAngleMonInfo_MotPosiAngle2deg(&Task_5msBus.MtrM_MainMotAngleMonInfo.MotPosiAngle2deg);

    /* Decomposed structure interface */
    Task_5ms_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhUMeasd(&Task_5msBus.MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd);
    Task_5ms_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhVMeasd(&Task_5msBus.MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd);
    Task_5ms_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhWMeasd(&Task_5msBus.MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd);
    Task_5ms_Read_MtrM_MainMtrProcessOutInfo_MotPosiAngle1_1_100Deg(&Task_5msBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg);
    Task_5ms_Read_MtrM_MainMtrProcessOutInfo_MotPosiAngle2_1_100Deg(&Task_5msBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg);

    Task_5ms_Read_Eem_MainRelayMonitorData(&Task_5msBus.Eem_MainRelayMonitorData);
    /*==============================================================================
    * Members of structure Eem_MainRelayMonitorData 
     : Eem_MainRelayMonitorData.RlyM_HDCRelay_Open_Err;
     : Eem_MainRelayMonitorData.RlyM_HDCRelay_Short_Err;
     : Eem_MainRelayMonitorData.RlyM_ESSRelay_Open_Err;
     : Eem_MainRelayMonitorData.RlyM_ESSRelay_Short_Err;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_5ms_Read_Eem_MainSenPwrMonitorData_SenPwrM_5V_Stable(&Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    Task_5ms_Read_Eem_MainSenPwrMonitorData_SenPwrM_5V_Err(&Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Err);
    Task_5ms_Read_Eem_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(&Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err);
    Task_5ms_Read_Eem_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(&Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err);

    Task_5ms_Read_Eem_MainSwtMonFaultInfo(&Task_5msBus.Eem_MainSwtMonFaultInfo);
    /*==============================================================================
    * Members of structure Eem_MainSwtMonFaultInfo 
     : Eem_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_HDC_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_AVH_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_BFL_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_PB_Sw_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainSwtPFaultInfo(&Task_5msBus.Eem_MainSwtPFaultInfo);
    /*==============================================================================
    * Members of structure Eem_MainSwtPFaultInfo 
     : Eem_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainYAWMOutInfo(&Task_5msBus.Eem_MainYAWMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainYAWMOutInfo 
     : Eem_MainYAWMOutInfo.YAWM_Timeout_Err;
     : Eem_MainYAWMOutInfo.YAWM_Invalid_Err;
     : Eem_MainYAWMOutInfo.YAWM_CRC_Err;
     : Eem_MainYAWMOutInfo.YAWM_Rolling_Err;
     : Eem_MainYAWMOutInfo.YAWM_Temperature_Err;
     : Eem_MainYAWMOutInfo.YAWM_Range_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainAYMOuitInfo(&Task_5msBus.Eem_MainAYMOuitInfo);
    /*==============================================================================
    * Members of structure Eem_MainAYMOuitInfo 
     : Eem_MainAYMOuitInfo.AYM_Timeout_Err;
     : Eem_MainAYMOuitInfo.AYM_Invalid_Err;
     : Eem_MainAYMOuitInfo.AYM_CRC_Err;
     : Eem_MainAYMOuitInfo.AYM_Rolling_Err;
     : Eem_MainAYMOuitInfo.AYM_Temperature_Err;
     : Eem_MainAYMOuitInfo.AYM_Range_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainAXMOutInfo(&Task_5msBus.Eem_MainAXMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainAXMOutInfo 
     : Eem_MainAXMOutInfo.AXM_Timeout_Err;
     : Eem_MainAXMOutInfo.AXM_Invalid_Err;
     : Eem_MainAXMOutInfo.AXM_CRC_Err;
     : Eem_MainAXMOutInfo.AXM_Rolling_Err;
     : Eem_MainAXMOutInfo.AXM_Temperature_Err;
     : Eem_MainAXMOutInfo.AXM_Range_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainSASMOutInfo(&Task_5msBus.Eem_MainSASMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainSASMOutInfo 
     : Eem_MainSASMOutInfo.SASM_Timeout_Err;
     : Eem_MainSASMOutInfo.SASM_Invalid_Err;
     : Eem_MainSASMOutInfo.SASM_CRC_Err;
     : Eem_MainSASMOutInfo.SASM_Rolling_Err;
     : Eem_MainSASMOutInfo.SASM_Range_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainCanMonData(&Task_5msBus.Eem_MainCanMonData);
    /*==============================================================================
    * Members of structure Eem_MainCanMonData 
     : Eem_MainCanMonData.CanM_MainBusOff_Err;
     : Eem_MainCanMonData.CanM_SubBusOff_Err;
     : Eem_MainCanMonData.CanM_MainOverRun_Err;
     : Eem_MainCanMonData.CanM_SubOverRun_Err;
     : Eem_MainCanMonData.CanM_EMS1_Tout_Err;
     : Eem_MainCanMonData.CanM_EMS2_Tout_Err;
     : Eem_MainCanMonData.CanM_TCU1_Tout_Err;
     : Eem_MainCanMonData.CanM_TCU5_Tout_Err;
     : Eem_MainCanMonData.CanM_FWD1_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU1_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU2_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU3_Tout_Err;
     : Eem_MainCanMonData.CanM_VSM2_Tout_Err;
     : Eem_MainCanMonData.CanM_EMS_Invalid_Err;
     : Eem_MainCanMonData.CanM_TCU_Invalid_Err;
     =============================================================================*/

    Task_5ms_Read_Eem_MainYawPlauOutput(&Task_5msBus.Eem_MainYawPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainYawPlauOutput 
     : Eem_MainYawPlauOutput.YawPlauModelErr;
     : Eem_MainYawPlauOutput.YawPlauNoisekErr;
     : Eem_MainYawPlauOutput.YawPlauShockErr;
     : Eem_MainYawPlauOutput.YawPlauRangeErr;
     : Eem_MainYawPlauOutput.YawPlauStandStillErr;
     =============================================================================*/

    Task_5ms_Read_Eem_MainAxPlauOutput(&Task_5msBus.Eem_MainAxPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainAxPlauOutput 
     : Eem_MainAxPlauOutput.AxPlauOffsetErr;
     : Eem_MainAxPlauOutput.AxPlauDrivingOffsetErr;
     : Eem_MainAxPlauOutput.AxPlauStickErr;
     : Eem_MainAxPlauOutput.AxPlauNoiseErr;
     =============================================================================*/

    Task_5ms_Read_Eem_MainAyPlauOutput(&Task_5msBus.Eem_MainAyPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainAyPlauOutput 
     : Eem_MainAyPlauOutput.AyPlauNoiselErr;
     : Eem_MainAyPlauOutput.AyPlauModelErr;
     : Eem_MainAyPlauOutput.AyPlauShockErr;
     : Eem_MainAyPlauOutput.AyPlauRangeErr;
     : Eem_MainAyPlauOutput.AyPlauStandStillErr;
     =============================================================================*/

    Task_5ms_Read_Eem_MainSasPlauOutput(&Task_5msBus.Eem_MainSasPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainSasPlauOutput 
     : Eem_MainSasPlauOutput.SasPlauModelErr;
     : Eem_MainSasPlauOutput.SasPlauOffsetErr;
     : Eem_MainSasPlauOutput.SasPlauStickErr;
     =============================================================================*/

    Task_5ms_Read_Arbitrator_VlvNormVlvReqSesInfo(&Task_5msBus.Arbitrator_VlvNormVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvNormVlvReqSesInfo 
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvDataLen;
     =============================================================================*/

    Task_5ms_Read_Arbitrator_VlvWhlVlvReqSesInfo(&Task_5msBus.Arbitrator_VlvWhlVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqSesInfo 
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvDataLen;
     =============================================================================*/

    Task_5ms_Read_Arbitrator_VlvDiagVlvActrInfo(&Task_5msBus.Arbitrator_VlvDiagVlvActrInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvDiagVlvActrInfo 
     : Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
     : Arbitrator_VlvDiagVlvActrInfo.FlOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FlIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FrOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FrIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RlOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RlIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RrOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RrIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData;
     =============================================================================*/

    /* Single interface */
    Task_5ms_Read_Ioc_InputSR5msAcmAsicInitCompleteFlag(&Task_5msBus.Ioc_InputSR5msAcmAsicInitCompleteFlag);
    Task_5ms_Read_Prly_HndlrCEMon(&Task_5msBus.Prly_HndlrCEMon);
    Task_5ms_Read_Swt_SenRsmDbcMon(&Task_5msBus.Swt_SenRsmDbcMon);
    Task_5ms_Read_Spc_5msCtrlMotCtrlState(&Task_5msBus.Spc_5msCtrlMotCtrlState);
    Task_5ms_Read_Abc_CtrlVBatt1Mon(&Task_5msBus.Abc_CtrlVBatt1Mon);
    Task_5ms_Read_Abc_CtrlDiagSci(&Task_5msBus.Abc_CtrlDiagSci);
    Task_5ms_Read_Pct_5msCtrlMotCtrlMode(&Task_5msBus.Pct_5msCtrlMotCtrlMode);
    Task_5ms_Read_Pct_5msCtrlMotICtrlFadeOutState(&Task_5msBus.Pct_5msCtrlMotICtrlFadeOutState);
    Task_5ms_Read_Pct_5msCtrlPreBoostMod(&Task_5msBus.Pct_5msCtrlPreBoostMod);
    Task_5ms_Read_Pct_5msCtrlStkRecvryStabnEndOK(&Task_5msBus.Pct_5msCtrlStkRecvryStabnEndOK);
    Task_5ms_Read_SysPwrM_MainVBatt1Mon(&Task_5msBus.SysPwrM_MainVBatt1Mon);
    Task_5ms_Read_SysPwrM_MainVBatt2Mon(&Task_5msBus.SysPwrM_MainVBatt2Mon);
    Task_5ms_Read_SysPwrM_MainDiagClrSrs(&Task_5msBus.SysPwrM_MainDiagClrSrs);
    Task_5ms_Read_SysPwrM_MainVdd1Mon(&Task_5msBus.SysPwrM_MainVdd1Mon);
    Task_5ms_Read_SysPwrM_MainVdd2Mon(&Task_5msBus.SysPwrM_MainVdd2Mon);
    Task_5ms_Read_SysPwrM_MainVdd3Mon(&Task_5msBus.SysPwrM_MainVdd3Mon);
    Task_5ms_Read_SysPwrM_MainVdd4Mon(&Task_5msBus.SysPwrM_MainVdd4Mon);
    Task_5ms_Read_SysPwrM_MainVdd5Mon(&Task_5msBus.SysPwrM_MainVdd5Mon);
    Task_5ms_Read_SysPwrM_MainVddMon(&Task_5msBus.SysPwrM_MainVddMon);
    Task_5ms_Read_SysPwrM_MainMtrArbDriveState(&Task_5msBus.SysPwrM_MainMtrArbDriveState);
    Task_5ms_Read_EcuHwCtrlM_MainAchAsicInvalid(&Task_5msBus.EcuHwCtrlM_MainAchAsicInvalid);
    Task_5ms_Read_EcuHwCtrlM_MainVBatt1Mon(&Task_5msBus.EcuHwCtrlM_MainVBatt1Mon);
    Task_5ms_Read_EcuHwCtrlM_MainDiagClrSrs(&Task_5msBus.EcuHwCtrlM_MainDiagClrSrs);
    Task_5ms_Read_EcuHwCtrlM_MainAcmAsicInitCompleteFlag(&Task_5msBus.EcuHwCtrlM_MainAcmAsicInitCompleteFlag);
    Task_5ms_Read_EcuHwCtrlM_MainVdd1Mon(&Task_5msBus.EcuHwCtrlM_MainVdd1Mon);
    Task_5ms_Read_EcuHwCtrlM_MainVdd2Mon(&Task_5msBus.EcuHwCtrlM_MainVdd2Mon);
    Task_5ms_Read_EcuHwCtrlM_MainVdd3Mon(&Task_5msBus.EcuHwCtrlM_MainVdd3Mon);
    Task_5ms_Read_EcuHwCtrlM_MainVdd4Mon(&Task_5msBus.EcuHwCtrlM_MainVdd4Mon);
    Task_5ms_Read_EcuHwCtrlM_MainVdd5Mon(&Task_5msBus.EcuHwCtrlM_MainVdd5Mon);
    Task_5ms_Read_EcuHwCtrlM_MainAdcInvalid(&Task_5msBus.EcuHwCtrlM_MainAdcInvalid);
    Task_5ms_Read_WssM_MainAchAsicInvalid(&Task_5msBus.WssM_MainAchAsicInvalid);
    Task_5ms_Read_WssM_MainVBatt1Mon(&Task_5msBus.WssM_MainVBatt1Mon);
    Task_5ms_Read_WssM_MainDiagClrSrs(&Task_5msBus.WssM_MainDiagClrSrs);
    Task_5ms_Read_PressM_MainVBatt1Mon(&Task_5msBus.PressM_MainVBatt1Mon);
    Task_5ms_Read_PressM_MainDiagClrSrs(&Task_5msBus.PressM_MainDiagClrSrs);
    Task_5ms_Read_PedalM_MainAchAsicInvalid(&Task_5msBus.PedalM_MainAchAsicInvalid);
    Task_5ms_Read_PedalM_MainDiagClrSrs(&Task_5msBus.PedalM_MainDiagClrSrs);
    Task_5ms_Read_PedalM_MainVdd3Mon(&Task_5msBus.PedalM_MainVdd3Mon);
    Task_5ms_Read_WssP_MainDiagClrSrs(&Task_5msBus.WssP_MainDiagClrSrs);
    Task_5ms_Read_PressP_MainVBatt1Mon(&Task_5msBus.PressP_MainVBatt1Mon);
    Task_5ms_Read_PressP_MainDiagClrSrs(&Task_5msBus.PressP_MainDiagClrSrs);
    Task_5ms_Read_PedalP_MainDiagClrSrs(&Task_5msBus.PedalP_MainDiagClrSrs);
    Task_5ms_Read_BbsVlvM_MainAchAsicInvalid(&Task_5msBus.BbsVlvM_MainAchAsicInvalid);
    Task_5ms_Read_BbsVlvM_MainVBatt1Mon(&Task_5msBus.BbsVlvM_MainVBatt1Mon);
    Task_5ms_Read_BbsVlvM_MainVBatt2Mon(&Task_5msBus.BbsVlvM_MainVBatt2Mon);
    Task_5ms_Read_BbsVlvM_MainDiagClrSrs(&Task_5msBus.BbsVlvM_MainDiagClrSrs);
    Task_5ms_Read_BbsVlvM_MainFspCbsHMon(&Task_5msBus.BbsVlvM_MainFspCbsHMon);
    Task_5ms_Read_BbsVlvM_MainFspCbsMon(&Task_5msBus.BbsVlvM_MainFspCbsMon);
    Task_5ms_Read_BbsVlvM_MainVlvdInvalid(&Task_5msBus.BbsVlvM_MainVlvdInvalid);
    Task_5ms_Read_AbsVlvM_MainAchAsicInvalid(&Task_5msBus.AbsVlvM_MainAchAsicInvalid);
    Task_5ms_Read_AbsVlvM_MainVBatt1Mon(&Task_5msBus.AbsVlvM_MainVBatt1Mon);
    Task_5ms_Read_AbsVlvM_MainVBatt2Mon(&Task_5msBus.AbsVlvM_MainVBatt2Mon);
    Task_5ms_Read_AbsVlvM_MainDiagClrSrs(&Task_5msBus.AbsVlvM_MainDiagClrSrs);
    Task_5ms_Read_AbsVlvM_MainFspAbsHMon(&Task_5msBus.AbsVlvM_MainFspAbsHMon);
    Task_5ms_Read_AbsVlvM_MainFspAbsMon(&Task_5msBus.AbsVlvM_MainFspAbsMon);
    Task_5ms_Read_Mps_TLE5012M_MainVBatt1Mon(&Task_5msBus.Mps_TLE5012M_MainVBatt1Mon);
    Task_5ms_Read_Mps_TLE5012M_MainDiagClrSrs(&Task_5msBus.Mps_TLE5012M_MainDiagClrSrs);
    Task_5ms_Read_Mps_TLE5012M_MainMpsInvalid(&Task_5msBus.Mps_TLE5012M_MainMpsInvalid);
    Task_5ms_Read_Mps_TLE5012M_MainMtrArbDriveState(&Task_5msBus.Mps_TLE5012M_MainMtrArbDriveState);
    Task_5ms_Read_MgdM_MainVBatt1Mon(&Task_5msBus.MgdM_MainVBatt1Mon);
    Task_5ms_Read_MgdM_MainDiagClrSrs(&Task_5msBus.MgdM_MainDiagClrSrs);
    Task_5ms_Read_MgdM_MainMgdInvalid(&Task_5msBus.MgdM_MainMgdInvalid);
    Task_5ms_Read_MgdM_MainMtrArbDriveState(&Task_5msBus.MgdM_MainMtrArbDriveState);
    Task_5ms_Read_MtrM_MainVBatt1Mon(&Task_5msBus.MtrM_MainVBatt1Mon);
    Task_5ms_Read_MtrM_MainVBatt2Mon(&Task_5msBus.MtrM_MainVBatt2Mon);
    Task_5ms_Read_MtrM_MainDiagClrSrs(&Task_5msBus.MtrM_MainDiagClrSrs);
    Task_5ms_Read_MtrM_MainMgdInvalid(&Task_5msBus.MtrM_MainMgdInvalid);
    Task_5ms_Read_MtrM_MainMtrArbDriveState(&Task_5msBus.MtrM_MainMtrArbDriveState);
    Task_5ms_Read_Watchdog_MainDiagClrSrs(&Task_5msBus.Watchdog_MainDiagClrSrs);
    Task_5ms_Read_Eem_MainDiagClrSrs(&Task_5msBus.Eem_MainDiagClrSrs);
    Task_5ms_Read_Eem_MainDiagSci(&Task_5msBus.Eem_MainDiagSci);
    Task_5ms_Read_Eem_MainDiagAhbSci(&Task_5msBus.Eem_MainDiagAhbSci);
    Task_5ms_Read_Eem_SuspcDetnDiagClrSrs(&Task_5msBus.Eem_SuspcDetnDiagClrSrs);
    Task_5ms_Read_Arbitrator_VlvAcmAsicInitCompleteFlag(&Task_5msBus.Arbitrator_VlvAcmAsicInitCompleteFlag);
    Task_5ms_Read_Arbitrator_VlvDiagSci(&Task_5msBus.Arbitrator_VlvDiagSci);
    Task_5ms_Read_Arbitrator_VlvDiagAhbSci(&Task_5msBus.Arbitrator_VlvDiagAhbSci);
    Task_5ms_Read_Arbitrator_RlyDiagSci(&Task_5msBus.Arbitrator_RlyDiagSci);
    Task_5ms_Read_Arbitrator_RlyDiagAhbSci(&Task_5msBus.Arbitrator_RlyDiagAhbSci);
    Task_5ms_Read_Fsr_ActrAcmAsicInitCompleteFlag(&Task_5msBus.Fsr_ActrAcmAsicInitCompleteFlag);
    Task_5ms_Read_Vlv_ActrSyncAcmAsicInitCompleteFlag(&Task_5msBus.Vlv_ActrSyncAcmAsicInitCompleteFlag);
    Task_5ms_Read_Ioc_OutputCS5msAcmAsicInitCompleteFlag(&Task_5msBus.Ioc_OutputCS5msAcmAsicInitCompleteFlag);

    /* Process */
    /**********************************************************************************************
     Proxy_RxByCom start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Proxy_RxByCom();

    /* Structure interface */
    Task_5msBus.Proxy_RxByComRxYawSerialInfo = Proxy_RxByComRxYawSerialInfo;
    /*==============================================================================
    * Members of structure Proxy_RxByComRxYawSerialInfo 
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_0;
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_1;
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    Task_5msBus.Proxy_RxByComRxYawAccInfo = Proxy_RxByComRxYawAccInfo;
    /*==============================================================================
    * Members of structure Proxy_RxByComRxYawAccInfo 
     : Proxy_RxByComRxYawAccInfo.YawRateValidData;
     : Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus;
     : Proxy_RxByComRxYawAccInfo.YawRateSignal_0;
     : Proxy_RxByComRxYawAccInfo.YawRateSignal_1;
     : Proxy_RxByComRxYawAccInfo.SensorOscFreqDev;
     : Proxy_RxByComRxYawAccInfo.Gyro_Fail;
     : Proxy_RxByComRxYawAccInfo.Raster_Fail;
     : Proxy_RxByComRxYawAccInfo.Eep_Fail;
     : Proxy_RxByComRxYawAccInfo.Batt_Range_Err;
     : Proxy_RxByComRxYawAccInfo.Asic_Fail;
     : Proxy_RxByComRxYawAccInfo.Accel_Fail;
     : Proxy_RxByComRxYawAccInfo.Ram_Fail;
     : Proxy_RxByComRxYawAccInfo.Rom_Fail;
     : Proxy_RxByComRxYawAccInfo.Ad_Fail;
     : Proxy_RxByComRxYawAccInfo.Osc_Fail;
     : Proxy_RxByComRxYawAccInfo.Watchdog_Rst;
     : Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst;
     : Proxy_RxByComRxYawAccInfo.RollingCounter;
     : Proxy_RxByComRxYawAccInfo.Can_Func_Err;
     : Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0;
     : Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1;
     : Proxy_RxByComRxYawAccInfo.LatAccValidData;
     : Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus;
     =============================================================================*/

    Task_5msBus.Proxy_RxByComRxSasInfo = Proxy_RxByComRxSasInfo;
    /*==============================================================================
    * Members of structure Proxy_RxByComRxSasInfo 
     : Proxy_RxByComRxSasInfo.Angle;
     : Proxy_RxByComRxSasInfo.Speed;
     : Proxy_RxByComRxSasInfo.Ok;
     : Proxy_RxByComRxSasInfo.Cal;
     : Proxy_RxByComRxSasInfo.Trim;
     : Proxy_RxByComRxSasInfo.CheckSum;
     : Proxy_RxByComRxSasInfo.MsgCount;
     =============================================================================*/

    Task_5msBus.Proxy_RxByComRxLongAccInfo = Proxy_RxByComRxLongAccInfo;
    /*==============================================================================
    * Members of structure Proxy_RxByComRxLongAccInfo 
     : Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive;
     : Proxy_RxByComRxLongAccInfo.IntSenFaultPresent;
     : Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre;
     : Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre;
     : Proxy_RxByComRxLongAccInfo.LongRollingCounter;
     : Proxy_RxByComRxLongAccInfo.IntTempSensorFault;
     : Proxy_RxByComRxLongAccInfo.LongAccInvalidData;
     : Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus;
     : Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0;
     : Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1;
     =============================================================================*/

    Task_5msBus.Proxy_RxByComRxMsgOkFlgInfo = Proxy_RxByComRxMsgOkFlgInfo;
    /*==============================================================================
    * Members of structure Proxy_RxByComRxMsgOkFlgInfo 
     : Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Proxy_RxByCom end
     **********************************************************************************************/

    /**********************************************************************************************
     Proxy_Rx start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    Proxy_RxRxBms1Info = Proxy_RxByComRxBms1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxBms1Info 
     : Proxy_RxRxBms1Info.Bms1SocPc;
     =============================================================================*/

    Proxy_RxRxYawSerialInfo = Proxy_RxByComRxYawSerialInfo;
    /*==============================================================================
    * Members of structure Proxy_RxRxYawSerialInfo 
     : Proxy_RxRxYawSerialInfo.YawSerialNum_0;
     : Proxy_RxRxYawSerialInfo.YawSerialNum_1;
     : Proxy_RxRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    Proxy_RxRxYawAccInfo = Proxy_RxByComRxYawAccInfo;
    /*==============================================================================
    * Members of structure Proxy_RxRxYawAccInfo 
     : Proxy_RxRxYawAccInfo.YawRateValidData;
     : Proxy_RxRxYawAccInfo.YawRateSelfTestStatus;
     : Proxy_RxRxYawAccInfo.YawRateSignal_0;
     : Proxy_RxRxYawAccInfo.YawRateSignal_1;
     : Proxy_RxRxYawAccInfo.SensorOscFreqDev;
     : Proxy_RxRxYawAccInfo.Gyro_Fail;
     : Proxy_RxRxYawAccInfo.Raster_Fail;
     : Proxy_RxRxYawAccInfo.Eep_Fail;
     : Proxy_RxRxYawAccInfo.Batt_Range_Err;
     : Proxy_RxRxYawAccInfo.Asic_Fail;
     : Proxy_RxRxYawAccInfo.Accel_Fail;
     : Proxy_RxRxYawAccInfo.Ram_Fail;
     : Proxy_RxRxYawAccInfo.Rom_Fail;
     : Proxy_RxRxYawAccInfo.Ad_Fail;
     : Proxy_RxRxYawAccInfo.Osc_Fail;
     : Proxy_RxRxYawAccInfo.Watchdog_Rst;
     : Proxy_RxRxYawAccInfo.Plaus_Err_Pst;
     : Proxy_RxRxYawAccInfo.RollingCounter;
     : Proxy_RxRxYawAccInfo.Can_Func_Err;
     : Proxy_RxRxYawAccInfo.AccelEratorRateSig_0;
     : Proxy_RxRxYawAccInfo.AccelEratorRateSig_1;
     : Proxy_RxRxYawAccInfo.LatAccValidData;
     : Proxy_RxRxYawAccInfo.LatAccSelfTestStatus;
     =============================================================================*/

    Proxy_RxRxTcu6Info = Proxy_RxByComRxTcu6Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxTcu6Info 
     : Proxy_RxRxTcu6Info.ShiftClass_Ccan;
     =============================================================================*/

    Proxy_RxRxTcu5Info = Proxy_RxByComRxTcu5Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxTcu5Info 
     : Proxy_RxRxTcu5Info.Typ;
     : Proxy_RxRxTcu5Info.GearTyp;
     =============================================================================*/

    Proxy_RxRxTcu1Info = Proxy_RxByComRxTcu1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxTcu1Info 
     : Proxy_RxRxTcu1Info.Targe;
     : Proxy_RxRxTcu1Info.GarChange;
     : Proxy_RxRxTcu1Info.Flt;
     : Proxy_RxRxTcu1Info.GarSelDisp;
     : Proxy_RxRxTcu1Info.TQRedReq_PC;
     : Proxy_RxRxTcu1Info.TQRedReqSlw_PC;
     : Proxy_RxRxTcu1Info.TQIncReq_PC;
     =============================================================================*/

    Proxy_RxRxSasInfo = Proxy_RxByComRxSasInfo;
    /*==============================================================================
    * Members of structure Proxy_RxRxSasInfo 
     : Proxy_RxRxSasInfo.Angle;
     : Proxy_RxRxSasInfo.Speed;
     : Proxy_RxRxSasInfo.Ok;
     : Proxy_RxRxSasInfo.Cal;
     : Proxy_RxRxSasInfo.Trim;
     : Proxy_RxRxSasInfo.CheckSum;
     : Proxy_RxRxSasInfo.MsgCount;
     =============================================================================*/

    Proxy_RxRxMcu2Info = Proxy_RxByComRxMcu2Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxMcu2Info 
     : Proxy_RxRxMcu2Info.Flt;
     =============================================================================*/

    Proxy_RxRxMcu1Info = Proxy_RxByComRxMcu1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxMcu1Info 
     : Proxy_RxRxMcu1Info.MoTestTQ_PC;
     : Proxy_RxRxMcu1Info.MotActRotSpd_RPM;
     =============================================================================*/

    Proxy_RxRxLongAccInfo = Proxy_RxByComRxLongAccInfo;
    /*==============================================================================
    * Members of structure Proxy_RxRxLongAccInfo 
     : Proxy_RxRxLongAccInfo.IntSenFltSymtmActive;
     : Proxy_RxRxLongAccInfo.IntSenFaultPresent;
     : Proxy_RxRxLongAccInfo.LongAccSenCirErrPre;
     : Proxy_RxRxLongAccInfo.LonACSenRanChkErrPre;
     : Proxy_RxRxLongAccInfo.LongRollingCounter;
     : Proxy_RxRxLongAccInfo.IntTempSensorFault;
     : Proxy_RxRxLongAccInfo.LongAccInvalidData;
     : Proxy_RxRxLongAccInfo.LongAccSelfTstStatus;
     : Proxy_RxRxLongAccInfo.LongAccRateSignal_0;
     : Proxy_RxRxLongAccInfo.LongAccRateSignal_1;
     =============================================================================*/

    Proxy_RxRxHcu5Info = Proxy_RxByComRxHcu5Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu5Info 
     : Proxy_RxRxHcu5Info.HevMod;
     =============================================================================*/

    Proxy_RxRxHcu3Info = Proxy_RxByComRxHcu3Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu3Info 
     : Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC;
     : Proxy_RxRxHcu3Info.MotTQCMC_PC;
     : Proxy_RxRxHcu3Info.MotTQCMDBINV_PC;
     =============================================================================*/

    Proxy_RxRxHcu2Info = Proxy_RxByComRxHcu2Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu2Info 
     : Proxy_RxRxHcu2Info.ServiceMod;
     : Proxy_RxRxHcu2Info.RegenENA;
     : Proxy_RxRxHcu2Info.RegenBRKTQ_NM;
     : Proxy_RxRxHcu2Info.CrpTQ_NM;
     : Proxy_RxRxHcu2Info.WhlDEMTQ_NM;
     =============================================================================*/

    Proxy_RxRxHcu1Info = Proxy_RxByComRxHcu1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu1Info 
     : Proxy_RxRxHcu1Info.EngCltStat;
     : Proxy_RxRxHcu1Info.HEVRDY;
     : Proxy_RxRxHcu1Info.EngTQCmdBinV_PC;
     : Proxy_RxRxHcu1Info.EngTQCmd_PC;
     =============================================================================*/

    Proxy_RxRxFact1Info = Proxy_RxByComRxFact1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxFact1Info 
     : Proxy_RxRxFact1Info.OutTemp_SNR_C;
     =============================================================================*/

    Proxy_RxRxEms3Info = Proxy_RxByComRxEms3Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxEms3Info 
     : Proxy_RxRxEms3Info.EngColTemp_C;
     =============================================================================*/

    Proxy_RxRxEms2Info = Proxy_RxByComRxEms2Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxEms2Info 
     : Proxy_RxRxEms2Info.EngSpdErr;
     : Proxy_RxRxEms2Info.AccPedDep_PC;
     : Proxy_RxRxEms2Info.Tps_PC;
     =============================================================================*/

    Proxy_RxRxEms1Info = Proxy_RxByComRxEms1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxEms1Info 
     : Proxy_RxRxEms1Info.TqStd_NM;
     : Proxy_RxRxEms1Info.ActINDTQ_PC;
     : Proxy_RxRxEms1Info.EngSpd_RPM;
     : Proxy_RxRxEms1Info.IndTQ_PC;
     : Proxy_RxRxEms1Info.FrictTQ_PC;
     =============================================================================*/

    Proxy_RxRxClu2Info = Proxy_RxByComRxClu2Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxClu2Info 
     : Proxy_RxRxClu2Info.IGN_SW;
     =============================================================================*/

    Proxy_RxRxClu1Info = Proxy_RxByComRxClu1Info;
    /*==============================================================================
    * Members of structure Proxy_RxRxClu1Info 
     : Proxy_RxRxClu1Info.P_Brake_Act;
     : Proxy_RxRxClu1Info.Cf_Clu_BrakeFluIDSW;
     =============================================================================*/

    Proxy_RxRxMsgOkFlgInfo = Proxy_RxByComRxMsgOkFlgInfo;
    /*==============================================================================
    * Members of structure Proxy_RxRxMsgOkFlgInfo 
     : Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX;
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT;
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX;
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX;

    /* Decomposed structure interface */
    Proxy_RxAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX = Proxy_RxByComAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX;
    Proxy_RxAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX = Proxy_RxByComAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX;
    Proxy_RxAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX = Proxy_RxByComAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX;

    /* Decomposed structure interface */
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2 = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD;
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY;

    /* Decomposed structure interface */
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5 = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4 = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4;
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH;

    /* Decomposed structure interface */
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1 = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2 = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2;
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT;

    /* Decomposed structure interface */
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH;
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH;
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH;
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH;
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH;
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH;
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH;
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH;

    /* Decomposed structure interface */
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI;
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP;
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP;
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI;
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP;
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI;
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI;

    /* Decomposed structure interface */
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG;
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP;
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG;
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG;
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP;
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG;

    /* Decomposed structure interface */
    Proxy_RxV_VEH_V_VEH_2Info.QU_V_VEH_COG = Proxy_RxByComV_VEH_V_VEH_2Info.QU_V_VEH_COG;
    Proxy_RxV_VEH_V_VEH_2Info.DVCO_VEH = Proxy_RxByComV_VEH_V_VEH_2Info.DVCO_VEH;
    Proxy_RxV_VEH_V_VEH_2Info.ST_V_VEH_NSS = Proxy_RxByComV_VEH_V_VEH_2Info.ST_V_VEH_NSS;
    Proxy_RxV_VEH_V_VEH_2Info.V_VEH_COG = Proxy_RxByComV_VEH_V_VEH_2Info.V_VEH_COG;

    /* Decomposed structure interface */
    Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP;
    Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH;
    Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH;

    /* Decomposed structure interface */
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW;
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW;
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST;
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW;
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW;

    /* Decomposed structure interface */
    Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI = Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI;
    Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI = Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI;
    Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL = Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL;
    Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL = Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL;
    Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP = Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP;

    /* Decomposed structure interface */
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC;
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM;

    /* Decomposed structure interface */
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT;
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX;
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS;
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP;
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6 = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6;

    /* Decomposed structure interface */
    Proxy_RxWMOM_DRV_7Info.ST_EL_DRVG = Proxy_RxByComWMOM_DRV_7Info.ST_EL_DRVG;
    Proxy_RxWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX = Proxy_RxByComWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX;
    Proxy_RxWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7 = Proxy_RxByComWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7;
    Proxy_RxWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP = Proxy_RxByComWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP;

    /* Decomposed structure interface */
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR;
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR;
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR;
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR;

    /* Decomposed structure interface */
    Proxy_RxDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA = Proxy_RxByComDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA;
    Proxy_RxDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA = Proxy_RxByComDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA;

    Proxy_RxERRM_BN_UInfo = Proxy_RxByComERRM_BN_UInfo;
    /*==============================================================================
    * Members of structure Proxy_RxERRM_BN_UInfo 
     : Proxy_RxERRM_BN_UInfo.CTR_ERRM_BN_U;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_RxCTR_CRInfo.ST_EXCE_ACLN_THRV = Proxy_RxByComCTR_CRInfo.ST_EXCE_ACLN_THRV;
    Proxy_RxCTR_CRInfo.CTR_PHTR_CR = Proxy_RxByComCTR_CRInfo.CTR_PHTR_CR;
    Proxy_RxCTR_CRInfo.CTR_ITLI_CR = Proxy_RxByComCTR_CRInfo.CTR_ITLI_CR;
    Proxy_RxCTR_CRInfo.CTR_CLSY_CR = Proxy_RxByComCTR_CRInfo.CTR_CLSY_CR;
    Proxy_RxCTR_CRInfo.CTR_AUTOM_ECAL_CR = Proxy_RxByComCTR_CRInfo.CTR_AUTOM_ECAL_CR;
    Proxy_RxCTR_CRInfo.CTR_SWO_EKP_CR = Proxy_RxByComCTR_CRInfo.CTR_SWO_EKP_CR;
    Proxy_RxCTR_CRInfo.CTR_PCSH_MST = Proxy_RxByComCTR_CRInfo.CTR_PCSH_MST;
    Proxy_RxCTR_CRInfo.CTR_HAZW_CR = Proxy_RxByComCTR_CRInfo.CTR_HAZW_CR;

    /* Decomposed structure interface */
    Proxy_RxKLEMMENInfo.ST_OP_MSA = Proxy_RxByComKLEMMENInfo.ST_OP_MSA;
    Proxy_RxKLEMMENInfo.RQ_DRVG_RDI = Proxy_RxByComKLEMMENInfo.RQ_DRVG_RDI;
    Proxy_RxKLEMMENInfo.ST_KL_DBG = Proxy_RxByComKLEMMENInfo.ST_KL_DBG;
    Proxy_RxKLEMMENInfo.ST_KL_50_MSA = Proxy_RxByComKLEMMENInfo.ST_KL_50_MSA;
    Proxy_RxKLEMMENInfo.ST_SSP = Proxy_RxByComKLEMMENInfo.ST_SSP;
    Proxy_RxKLEMMENInfo.RWDT_BLS = Proxy_RxByComKLEMMENInfo.RWDT_BLS;
    Proxy_RxKLEMMENInfo.ST_STCD_PENG = Proxy_RxByComKLEMMENInfo.ST_STCD_PENG;
    Proxy_RxKLEMMENInfo.ST_KL = Proxy_RxByComKLEMMENInfo.ST_KL;
    Proxy_RxKLEMMENInfo.ST_KL_DIV = Proxy_RxByComKLEMMENInfo.ST_KL_DIV;
    Proxy_RxKLEMMENInfo.ST_VEH_CON = Proxy_RxByComKLEMMENInfo.ST_VEH_CON;
    Proxy_RxKLEMMENInfo.ST_KL_30B = Proxy_RxByComKLEMMENInfo.ST_KL_30B;
    Proxy_RxKLEMMENInfo.CON_CLT_SW = Proxy_RxByComKLEMMENInfo.CON_CLT_SW;
    Proxy_RxKLEMMENInfo.CTR_ENG_STOP = Proxy_RxByComKLEMMENInfo.CTR_ENG_STOP;
    Proxy_RxKLEMMENInfo.ST_PLK = Proxy_RxByComKLEMMENInfo.ST_PLK;
    Proxy_RxKLEMMENInfo.ST_KL_15N = Proxy_RxByComKLEMMENInfo.ST_KL_15N;
    Proxy_RxKLEMMENInfo.ST_KL_KEY_VLD = Proxy_RxByComKLEMMENInfo.ST_KL_KEY_VLD;

    /* Decomposed structure interface */
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3;
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2 = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2;

    /* Decomposed structure interface */
    Proxy_RxBEDIENUNG_WISCHERInfo.OP_WISW = Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WISW;
    Proxy_RxBEDIENUNG_WISCHERInfo.OP_WIPO = Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WIPO;

    /* Decomposed structure interface */
    Proxy_RxDT_PT_2Info.ST_GRSEL_DRV = Proxy_RxByComDT_PT_2Info.ST_GRSEL_DRV;
    Proxy_RxDT_PT_2Info.TEMP_EOI_DRV = Proxy_RxByComDT_PT_2Info.TEMP_EOI_DRV;
    Proxy_RxDT_PT_2Info.RLS_ENGSTA = Proxy_RxByComDT_PT_2Info.RLS_ENGSTA;
    Proxy_RxDT_PT_2Info.RPM_ENG_MAX_ALW = Proxy_RxByComDT_PT_2Info.RPM_ENG_MAX_ALW;
    Proxy_RxDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2 = Proxy_RxByComDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2;
    Proxy_RxDT_PT_2Info.TEMP_ENG_DRV = Proxy_RxByComDT_PT_2Info.TEMP_ENG_DRV;
    Proxy_RxDT_PT_2Info.ST_DRV_VEH = Proxy_RxByComDT_PT_2Info.ST_DRV_VEH;
    Proxy_RxDT_PT_2Info.ST_ECU_DT_PT_2 = Proxy_RxByComDT_PT_2Info.ST_ECU_DT_PT_2;
    Proxy_RxDT_PT_2Info.ST_IDLG_ENG_DRV = Proxy_RxByComDT_PT_2Info.ST_IDLG_ENG_DRV;
    Proxy_RxDT_PT_2Info.ST_ILK_STRT_DRV = Proxy_RxByComDT_PT_2Info.ST_ILK_STRT_DRV;
    Proxy_RxDT_PT_2Info.ST_SW_CLT_DRV = Proxy_RxByComDT_PT_2Info.ST_SW_CLT_DRV;
    Proxy_RxDT_PT_2Info.ST_ENG_RUN_DRV = Proxy_RxByComDT_PT_2Info.ST_ENG_RUN_DRV;

    /* Decomposed structure interface */
    Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1 = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2 = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0 = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_FN_MSA = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_FN_MSA;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG;
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME;

    Proxy_RxST_ENERG_GENInfo = Proxy_RxByComST_ENERG_GENInfo;
    /*==============================================================================
    * Members of structure Proxy_RxST_ENERG_GENInfo 
     : Proxy_RxST_ENERG_GENInfo.ST_LDST_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.DT_PCU_SCP;
     : Proxy_RxST_ENERG_GENInfo.ST_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.AVL_I_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.LDST_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.ST_LDREL_GEN;
     : Proxy_RxST_ENERG_GENInfo.ST_CHG_STOR;
     : Proxy_RxST_ENERG_GENInfo.TEMP_BT_14V;
     : Proxy_RxST_ENERG_GENInfo.ST_I_IBS;
     : Proxy_RxST_ENERG_GENInfo.ST_SEP_STOR;
     : Proxy_RxST_ENERG_GENInfo.ST_BN2_SCP;
     =============================================================================*/

    Proxy_RxDT_PT_1Info = Proxy_RxByComDT_PT_1Info;
    /*==============================================================================
    * Members of structure Proxy_RxDT_PT_1Info 
     : Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI;
     : Proxy_RxDT_PT_1Info.ST_RTIR_DRV;
     : Proxy_RxDT_PT_1Info.CTR_SLCK_DRV;
     : Proxy_RxDT_PT_1Info.RQ_STASS_ENGMG;
     : Proxy_RxDT_PT_1Info.ST_CAT_HT;
     : Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_CHGBLC;
     : Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT;
     : Proxy_RxDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB;
     : Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV;
     : Proxy_RxDT_PT_1Info.ST_INFS_DRV;
     : Proxy_RxDT_PT_1Info.ST_SW_WAUP_DRV;
     : Proxy_RxDT_PT_1Info.AIP_ENG_DRV;
     : Proxy_RxDT_PT_1Info.RDUC_DOCTR_RPM_DRV;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_RxDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV = Proxy_RxByComDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV;
    Proxy_RxDT_GRDT_DRVInfo.ST_RSTA_GRDT = Proxy_RxByComDT_GRDT_DRVInfo.ST_RSTA_GRDT;

    Proxy_RxDIAG_OBD_ENGMG_ELInfo = Proxy_RxByComDIAG_OBD_ENGMG_ELInfo;
    /*==============================================================================
    * Members of structure Proxy_RxDIAG_OBD_ENGMG_ELInfo 
     : Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL;
     : Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG;
     =============================================================================*/

    Proxy_RxSTAT_CT_HABRInfo = Proxy_RxByComSTAT_CT_HABRInfo;
    /*==============================================================================
    * Members of structure Proxy_RxSTAT_CT_HABRInfo 
     : Proxy_RxSTAT_CT_HABRInfo.ST_CT_HABR;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_RxDT_PT_3Info.TRNRAO_BAX = Proxy_RxByComDT_PT_3Info.TRNRAO_BAX;
    Proxy_RxDT_PT_3Info.QU_TRNRAO_BAX = Proxy_RxByComDT_PT_3Info.QU_TRNRAO_BAX;

    Proxy_RxEINHEITEN_BN2020Info = Proxy_RxByComEINHEITEN_BN2020Info;
    /*==============================================================================
    * Members of structure Proxy_RxEINHEITEN_BN2020Info 
     : Proxy_RxEINHEITEN_BN2020Info.UN_TORQ_S_MOD;
     : Proxy_RxEINHEITEN_BN2020Info.UN_PWR_S_MOD;
     : Proxy_RxEINHEITEN_BN2020Info.UN_DATE_EXT;
     : Proxy_RxEINHEITEN_BN2020Info.UN_COSP_EL;
     : Proxy_RxEINHEITEN_BN2020Info.UN_TEMP;
     : Proxy_RxEINHEITEN_BN2020Info.UN_AIP;
     : Proxy_RxEINHEITEN_BN2020Info.LANG;
     : Proxy_RxEINHEITEN_BN2020Info.UN_DATE;
     : Proxy_RxEINHEITEN_BN2020Info.UN_T;
     : Proxy_RxEINHEITEN_BN2020Info.UN_SPDM_DGTL;
     : Proxy_RxEINHEITEN_BN2020Info.UN_MILE;
     : Proxy_RxEINHEITEN_BN2020Info.UN_FU;
     : Proxy_RxEINHEITEN_BN2020Info.UN_COSP;
     =============================================================================*/

    Proxy_RxA_TEMPInfo = Proxy_RxByComA_TEMPInfo;
    /*==============================================================================
    * Members of structure Proxy_RxA_TEMPInfo 
     : Proxy_RxA_TEMPInfo.TEMP_EX_UNFILT;
     : Proxy_RxA_TEMPInfo.TEMP_EX;
     =============================================================================*/

    Proxy_RxWISCHERGESCHWINDIGKEITInfo = Proxy_RxByComWISCHERGESCHWINDIGKEITInfo;
    /*==============================================================================
    * Members of structure Proxy_RxWISCHERGESCHWINDIGKEITInfo 
     : Proxy_RxWISCHERGESCHWINDIGKEITInfo.ST_RNSE;
     : Proxy_RxWISCHERGESCHWINDIGKEITInfo.INT_RN;
     : Proxy_RxWISCHERGESCHWINDIGKEITInfo.V_WI;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD;
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD;

    /* Decomposed structure interface */
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB;
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC;

    Proxy_RxSTAT_ANHAENGERInfo = Proxy_RxByComSTAT_ANHAENGERInfo;
    /*==============================================================================
    * Members of structure Proxy_RxSTAT_ANHAENGERInfo 
     : Proxy_RxSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_DI_DF_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_PO_AHV;
     =============================================================================*/

    Proxy_RxFZZSTDInfo = Proxy_RxByComFZZSTDInfo;
    /*==============================================================================
    * Members of structure Proxy_RxFZZSTDInfo 
     : Proxy_RxFZZSTDInfo.ST_ILK_ERRM_FZM;
     : Proxy_RxFZZSTDInfo.ST_ENERG_FZM;
     : Proxy_RxFZZSTDInfo.ST_BT_PROTE_WUP;
     =============================================================================*/

    Proxy_RxBEDIENUNG_FAHRWERKInfo = Proxy_RxByComBEDIENUNG_FAHRWERKInfo;
    /*==============================================================================
    * Members of structure Proxy_RxBEDIENUNG_FAHRWERKInfo 
     : Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC;
     : Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_TPCT;
     =============================================================================*/

    Proxy_RxFAHRZEUGTYPInfo = Proxy_RxByComFAHRZEUGTYPInfo;
    /*==============================================================================
    * Members of structure Proxy_RxFAHRZEUGTYPInfo 
     : Proxy_RxFAHRZEUGTYPInfo.QUAN_CYL;
     : Proxy_RxFAHRZEUGTYPInfo.QUAN_GR;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_VEH;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_BODY;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_ENG;
     : Proxy_RxFAHRZEUGTYPInfo.CLAS_PWR;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_CNT;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_STE;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_GRB;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_CAPA;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS;
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS;

    Proxy_RxFAHRGESTELLNUMMERInfo = Proxy_RxByComFAHRGESTELLNUMMERInfo;
    /*==============================================================================
    * Members of structure Proxy_RxFAHRGESTELLNUMMERInfo 
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_1;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_2;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_3;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_6;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_7;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_4;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_5;
     =============================================================================*/

    Proxy_RxRELATIVZEITInfo = Proxy_RxByComRELATIVZEITInfo;
    /*==============================================================================
    * Members of structure Proxy_RxRELATIVZEITInfo 
     : Proxy_RxRELATIVZEITInfo.T_SEC_COU_REL;
     : Proxy_RxRELATIVZEITInfo.T_DAY_COU_ABSL;
     =============================================================================*/

    Proxy_RxKILOMETERSTANDInfo = Proxy_RxByComKILOMETERSTANDInfo;
    /*==============================================================================
    * Members of structure Proxy_RxKILOMETERSTANDInfo 
     : Proxy_RxKILOMETERSTANDInfo.RNG;
     : Proxy_RxKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR;
     : Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_RH;
     : Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_LH;
     : Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA;
     : Proxy_RxKILOMETERSTANDInfo.MILE_KM;
     =============================================================================*/

    Proxy_RxUHRZEIT_DATUMInfo = Proxy_RxByComUHRZEIT_DATUMInfo;
    /*==============================================================================
    * Members of structure Proxy_RxUHRZEIT_DATUMInfo 
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_WDAY;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_DAY;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_MON;
     : Proxy_RxUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_YR;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_HR;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_SEC;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_MN;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Proxy_RxEcuModeSts = Mom_HndlrEcuModeSts;
    Proxy_RxPCtrlAct = Pct_5msCtrlPCtrlAct;
    //Proxy_RxFuncInhibitProxySts = Eem_SuspcDetnFuncInhibitProxySts;

    /* Execute  runnable */
    Proxy_Rx();

    /* Structure interface */
    Task_5msBus.Proxy_RxCanRxRegenInfo = Proxy_RxCanRxRegenInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxRegenInfo 
     : Proxy_RxCanRxRegenInfo.HcuRegenEna;
     : Proxy_RxCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/

    Task_5msBus.Proxy_RxCanRxAccelPedlInfo = Proxy_RxCanRxAccelPedlInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxAccelPedlInfo 
     : Proxy_RxCanRxAccelPedlInfo.AccelPedlVal;
     : Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    Task_5msBus.Proxy_RxCanRxIdbInfo = Proxy_RxCanRxIdbInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxIdbInfo 
     : Proxy_RxCanRxIdbInfo.EngMsgFault;
     : Proxy_RxCanRxIdbInfo.TarGearPosi;
     : Proxy_RxCanRxIdbInfo.GearSelDisp;
     : Proxy_RxCanRxIdbInfo.TcuSwiGs;
     : Proxy_RxCanRxIdbInfo.HcuServiceMod;
     : Proxy_RxCanRxIdbInfo.SubCanBusSigFault;
     : Proxy_RxCanRxIdbInfo.CoolantTemp;
     : Proxy_RxCanRxIdbInfo.CoolantTempErr;
     =============================================================================*/

    Task_5msBus.Proxy_RxCanRxEngTempInfo = Proxy_RxCanRxEngTempInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEngTempInfo 
     : Proxy_RxCanRxEngTempInfo.EngTemp;
     : Proxy_RxCanRxEngTempInfo.EngTempErr;
     =============================================================================*/

    Task_5msBus.Proxy_RxCanRxEscInfo = Proxy_RxCanRxEscInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEscInfo 
     : Proxy_RxCanRxEscInfo.Ax;
     : Proxy_RxCanRxEscInfo.YawRate;
     : Proxy_RxCanRxEscInfo.SteeringAngle;
     : Proxy_RxCanRxEscInfo.Ay;
     : Proxy_RxCanRxEscInfo.PbSwt;
     : Proxy_RxCanRxEscInfo.ClutchSwt;
     : Proxy_RxCanRxEscInfo.GearRSwt;
     : Proxy_RxCanRxEscInfo.EngActIndTq;
     : Proxy_RxCanRxEscInfo.EngRpm;
     : Proxy_RxCanRxEscInfo.EngIndTq;
     : Proxy_RxCanRxEscInfo.EngFrictionLossTq;
     : Proxy_RxCanRxEscInfo.EngStdTq;
     : Proxy_RxCanRxEscInfo.TurbineRpm;
     : Proxy_RxCanRxEscInfo.ThrottleAngle;
     : Proxy_RxCanRxEscInfo.TpsResol1000;
     : Proxy_RxCanRxEscInfo.PvAvCanResol1000;
     : Proxy_RxCanRxEscInfo.EngChr;
     : Proxy_RxCanRxEscInfo.EngVol;
     : Proxy_RxCanRxEscInfo.GearType;
     : Proxy_RxCanRxEscInfo.EngClutchState;
     : Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv;
     : Proxy_RxCanRxEscInfo.MotEstTq;
     : Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv;
     : Proxy_RxCanRxEscInfo.TqIntvTCU;
     : Proxy_RxCanRxEscInfo.TqIntvSlowTCU;
     : Proxy_RxCanRxEscInfo.TqIncReq;
     : Proxy_RxCanRxEscInfo.DecelReq;
     : Proxy_RxCanRxEscInfo.RainSnsStat;
     : Proxy_RxCanRxEscInfo.EngSpdErr;
     : Proxy_RxCanRxEscInfo.AtType;
     : Proxy_RxCanRxEscInfo.MtType;
     : Proxy_RxCanRxEscInfo.CvtType;
     : Proxy_RxCanRxEscInfo.DctType;
     : Proxy_RxCanRxEscInfo.HevAtTcu;
     : Proxy_RxCanRxEscInfo.TurbineRpmErr;
     : Proxy_RxCanRxEscInfo.ThrottleAngleErr;
     : Proxy_RxCanRxEscInfo.TopTrvlCltchSwtAct;
     : Proxy_RxCanRxEscInfo.TopTrvlCltchSwtActV;
     : Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtAct;
     : Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtActV;
     : Proxy_RxCanRxEscInfo.WiperIntSW;
     : Proxy_RxCanRxEscInfo.WiperLow;
     : Proxy_RxCanRxEscInfo.WiperHigh;
     : Proxy_RxCanRxEscInfo.WiperValid;
     : Proxy_RxCanRxEscInfo.WiperAuto;
     : Proxy_RxCanRxEscInfo.TcuFaultSts;
     =============================================================================*/

    Task_5msBus.Proxy_RxCanRxEemInfo = Proxy_RxCanRxEemInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEemInfo 
     : Proxy_RxCanRxEemInfo.YawRateInvld;
     : Proxy_RxCanRxEemInfo.AyInvld;
     : Proxy_RxCanRxEemInfo.SteeringAngleRxOk;
     : Proxy_RxCanRxEemInfo.AxInvldData;
     : Proxy_RxCanRxEemInfo.Ems1RxErr;
     : Proxy_RxCanRxEemInfo.Ems2RxErr;
     : Proxy_RxCanRxEemInfo.Tcu1RxErr;
     : Proxy_RxCanRxEemInfo.Tcu5RxErr;
     : Proxy_RxCanRxEemInfo.Hcu1RxErr;
     : Proxy_RxCanRxEemInfo.Hcu2RxErr;
     : Proxy_RxCanRxEemInfo.Hcu3RxErr;
     =============================================================================*/

    Task_5msBus.Proxy_RxCanRxInfo = Proxy_RxCanRxInfo;
    /*==============================================================================
    * Members of structure Proxy_RxCanRxInfo 
     : Proxy_RxCanRxInfo.MainBusOffFlag;
     : Proxy_RxCanRxInfo.SenBusOffFlag;
     =============================================================================*/

    Task_5msBus.Proxy_RxIMUCalcInfo = Proxy_RxIMUCalcInfo;
    /*==============================================================================
    * Members of structure Proxy_RxIMUCalcInfo 
     : Proxy_RxIMUCalcInfo.Reverse_Gear_flg;
     : Proxy_RxIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Proxy_RxCanRxGearSelDispErrInfo = Proxy_RxCanRxGearSelDispErrInfo;

    /**********************************************************************************************
     Proxy_Rx end
     **********************************************************************************************/

    /**********************************************************************************************
     Icu_InputCapture start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Icu_InputCaptureEcuModeSts = Mom_HndlrEcuModeSts;
    //Icu_InputCaptureFuncInhibitIcuSts = Eem_SuspcDetnFuncInhibitIcuSts;

    /* Execute  runnable */
    Icu_InputCapture();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Icu_InputCapture end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_InputSR5ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Ioc_InputSR5msAcmAsicInitCompleteFlag = Task_5msBus.Ioc_InputSR5msAcmAsicInitCompleteFlag;

    /* Inter-Runnable structure interface */
    Ioc_InputSR5msRisngIdxInfo = Icu_InputCaptureRisngIdxInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR5msRisngIdxInfo 
     : Ioc_InputSR5msRisngIdxInfo.FlRisngIdx;
     : Ioc_InputSR5msRisngIdxInfo.FrRisngIdx;
     : Ioc_InputSR5msRisngIdxInfo.RlRisngIdx;
     : Ioc_InputSR5msRisngIdxInfo.RrRisngIdx;
     =============================================================================*/

    Ioc_InputSR5msFallIdxInfo = Icu_InputCaptureFallIdxInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR5msFallIdxInfo 
     : Ioc_InputSR5msFallIdxInfo.FlFallIdx;
     : Ioc_InputSR5msFallIdxInfo.FrFallIdx;
     : Ioc_InputSR5msFallIdxInfo.RlFallIdx;
     : Ioc_InputSR5msFallIdxInfo.RrFallIdx;
     =============================================================================*/

    Ioc_InputSR5msRisngTiStampInfo = Icu_InputCaptureRisngTiStampInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR5msRisngTiStampInfo 
     : Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp;
     : Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp;
     : Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp;
     : Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp;
     =============================================================================*/

    Ioc_InputSR5msFallTiStampInfo = Icu_InputCaptureFallTiStampInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR5msFallTiStampInfo 
     : Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp;
     : Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp;
     : Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp;
     : Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Ioc_InputSR5msEcuModeSts = Mom_HndlrEcuModeSts;
    //Ioc_InputSR5msFuncInhibitIocSts = Eem_SuspcDetnFuncInhibitIocSts;

    /* Execute  runnable */
    Ioc_InputSR5ms();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Ioc_InputSR5ms end
     **********************************************************************************************/

    /**********************************************************************************************
     Nvm_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Nvm_HndlrEcuModeSts = Mom_HndlrEcuModeSts;
    //Nvm_HndlrFuncInhibitNvmSts = Eem_SuspcDetnFuncInhibitNvmSts;

    /* Execute  runnable */
    Nvm_Hndlr();

    /* Structure interface */
    Task_5msBus.Nvm_HndlrLogicEepDataInfo = Nvm_HndlrLogicEepDataInfo;
    /*==============================================================================
    * Members of structure Nvm_HndlrLogicEepDataInfo 
     : Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KLatEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawEepMax;
     : Nvm_HndlrLogicEepDataInfo.KYawEepMin;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepMax;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepMin;
     : Nvm_HndlrLogicEepDataInfo.KLatEepMax;
     : Nvm_HndlrLogicEepDataInfo.KLatEepMin;
     : Nvm_HndlrLogicEepDataInfo.KYawStillEepMax;
     : Nvm_HndlrLogicEepDataInfo.KYawStillEepMin;
     : Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap;
     : Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep;
     : Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep;
     : Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep;
     : Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs;
     : Nvm_HndlrLogicEepDataInfo.ReadInvld;
     : Nvm_HndlrLogicEepDataInfo.WrReqCmpld;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Nvm_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     Prly_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq = Task_5msBus.Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq;
    Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req = Task_5msBus.Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req;

    /* Single interface */
    Prly_HndlrCEMon = Task_5msBus.Prly_HndlrCEMon;

    /* Inter-Runnable structure interface */
    Prly_HndlrCanRxClu2Info = Proxy_RxCanRxClu2Info;
    /*==============================================================================
    * Members of structure Prly_HndlrCanRxClu2Info 
     : Prly_HndlrCanRxClu2Info.IgnRun;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Prly_HndlrEcuModeSts = Mom_HndlrEcuModeSts;
    //Prly_HndlrMomEcuInhibit = Mom_HndlrMomEcuInhibit;
    //Prly_HndlrFuncInhibitPrlySts = Eem_SuspcDetnFuncInhibitPrlySts;

    /* Execute  runnable */
    Prly_Hndlr();

    /* Structure interface */
    /* Single interface */
    Task_5msBus.Prly_HndlrIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Task_5msBus.Prly_HndlrIgnEdgeSts = Prly_HndlrIgnEdgeSts;
    //Task_5msBus.Prly_HndlrPrlyEcuInhibit = Prly_HndlrPrlyEcuInhibit;

    /**********************************************************************************************
     Prly_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     Mom_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Mom_HndlrFuncInhibitMomSts = Eem_SuspcDetnFuncInhibitMomSts;

    /* Execute  runnable */
    Mom_Hndlr();

    /* Structure interface */
    /* Single interface */
    //Task_5msBus.Mom_HndlrEcuModeSts = Mom_HndlrEcuModeSts;

    /**********************************************************************************************
     Mom_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     Wss_Sen start
     **********************************************************************************************/

     /* Structure interface */
    Wss_SenAchWssPort0AsicInfo = Task_5msBus.Wss_SenAchWssPort0AsicInfo;
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort0AsicInfo 
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Wss_SenAchWssPort1AsicInfo = Task_5msBus.Wss_SenAchWssPort1AsicInfo;
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort1AsicInfo 
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Wss_SenAchWssPort2AsicInfo = Task_5msBus.Wss_SenAchWssPort2AsicInfo;
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort2AsicInfo 
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Wss_SenAchWssPort3AsicInfo = Task_5msBus.Wss_SenAchWssPort3AsicInfo;
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort3AsicInfo 
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    /* Single interface */

    /* Inter-Runnable structure interface */
    Wss_SenWssMonInfo = Ioc_InputSR5msWssMonInfo;
    /*==============================================================================
    * Members of structure Wss_SenWssMonInfo 
     : Wss_SenWssMonInfo.FlRisngIdx;
     : Wss_SenWssMonInfo.FlFallIdx;
     : Wss_SenWssMonInfo.FlRisngTiStamp;
     : Wss_SenWssMonInfo.FlFallTiStamp;
     : Wss_SenWssMonInfo.FrRisngIdx;
     : Wss_SenWssMonInfo.FrFallIdx;
     : Wss_SenWssMonInfo.FrRisngTiStamp;
     : Wss_SenWssMonInfo.FrFallTiStamp;
     : Wss_SenWssMonInfo.RlRisngIdx;
     : Wss_SenWssMonInfo.RlFallIdx;
     : Wss_SenWssMonInfo.RlRisngTiStamp;
     : Wss_SenWssMonInfo.RlFallTiStamp;
     : Wss_SenWssMonInfo.RrRisngIdx;
     : Wss_SenWssMonInfo.RrFallIdx;
     : Wss_SenWssMonInfo.RrRisngTiStamp;
     : Wss_SenWssMonInfo.RrFallTiStamp;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Wss_SenEcuModeSts = Mom_HndlrEcuModeSts;
    //Wss_SenFuncInhibitWssSts = Eem_SuspcDetnFuncInhibitWssSts;

    /* Execute  runnable */
    Wss_Sen();

    /* Structure interface */
    Task_5msBus.Wss_SenWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure Wss_SenWhlSpdInfo 
     : Wss_SenWhlSpdInfo.FlWhlSpd;
     : Wss_SenWhlSpdInfo.FrWhlSpd;
     : Wss_SenWhlSpdInfo.RlWhlSpd;
     : Wss_SenWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Task_5msBus.Wss_SenWhlEdgeCntInfo = Wss_SenWhlEdgeCntInfo;
    /*==============================================================================
    * Members of structure Wss_SenWhlEdgeCntInfo 
     : Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt;
     =============================================================================*/

    Task_5msBus.Wss_SenWssSpeedOut = Wss_SenWssSpeedOut;
    /*==============================================================================
    * Members of structure Wss_SenWssSpeedOut 
     : Wss_SenWssSpeedOut.WssMax;
     : Wss_SenWssSpeedOut.WssMin;
     =============================================================================*/

    Task_5msBus.Wss_SenWssCalcInfo = Wss_SenWssCalcInfo;
    /*==============================================================================
    * Members of structure Wss_SenWssCalcInfo 
     : Wss_SenWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Wss_Sen end
     **********************************************************************************************/

    /**********************************************************************************************
     Pedal_SenSync start
     **********************************************************************************************/

     /* Structure interface */
    Pedal_SenSyncPdtBufInfo = Task_5msBus.Pedal_SenSyncPdtBufInfo;
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdtBufInfo 
     : Pedal_SenSyncPdtBufInfo.PdtRaw;
     =============================================================================*/

    Pedal_SenSyncPdfBufInfo = Task_5msBus.Pedal_SenSyncPdfBufInfo;
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdfBufInfo 
     : Pedal_SenSyncPdfBufInfo.PdfRaw;
     =============================================================================*/

    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Pedal_SenSyncEcuModeSts = Mom_HndlrEcuModeSts;
    //Pedal_SenSyncFuncInhibitPedalSts = Eem_SuspcDetnFuncInhibitPedalSts;

    /* Execute  runnable */
    Pedal_SenSync();

    /* Structure interface */
    Task_5msBus.Pedal_SenSyncPdt5msRawInfo = Pedal_SenSyncPdt5msRawInfo;
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdt5msRawInfo 
     : Pedal_SenSyncPdt5msRawInfo.PdtSig;
     =============================================================================*/

    Task_5msBus.Pedal_SenSyncPdf5msRawInfo = Pedal_SenSyncPdf5msRawInfo;
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdf5msRawInfo 
     : Pedal_SenSyncPdf5msRawInfo.PdfSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Pedal_SenSync end
     **********************************************************************************************/

    /**********************************************************************************************
     Press_SenSync start
     **********************************************************************************************/

     /* Structure interface */
    Press_SenSyncCircPBufInfo = Task_5msBus.Press_SenSyncCircPBufInfo;
    /*==============================================================================
    * Members of structure Press_SenSyncCircPBufInfo 
     : Press_SenSyncCircPBufInfo.PrimCircPRaw;
     : Press_SenSyncCircPBufInfo.SecdCircPRaw;
     =============================================================================*/

    Press_SenSyncPistPBufInfo = Task_5msBus.Press_SenSyncPistPBufInfo;
    /*==============================================================================
    * Members of structure Press_SenSyncPistPBufInfo 
     : Press_SenSyncPistPBufInfo.PistPRaw;
     =============================================================================*/

    Press_SenSyncPspBufInfo = Task_5msBus.Press_SenSyncPspBufInfo;
    /*==============================================================================
    * Members of structure Press_SenSyncPspBufInfo 
     : Press_SenSyncPspBufInfo.PedlSimPRaw;
     =============================================================================*/

    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Press_SenSyncEcuModeSts = Mom_HndlrEcuModeSts;
    //Press_SenSyncFuncInhibitPressSts = Eem_SuspcDetnFuncInhibitPressSts;

    /* Execute  runnable */
    Press_SenSync();

    /* Structure interface */
    Task_5msBus.Press_SenSyncPistP5msRawInfo = Press_SenSyncPistP5msRawInfo;
    /*==============================================================================
    * Members of structure Press_SenSyncPistP5msRawInfo 
     : Press_SenSyncPistP5msRawInfo.PistPSig;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Press_SenSync end
     **********************************************************************************************/

    /**********************************************************************************************
     Swt_Sen start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Swt_SenSwtMonInfo.AvhSwtMon = Task_5msBus.Swt_SenSwtMonInfo.AvhSwtMon;
    Swt_SenSwtMonInfo.BlsSwtMon = Task_5msBus.Swt_SenSwtMonInfo.BlsSwtMon;
    Swt_SenSwtMonInfo.BsSwtMon = Task_5msBus.Swt_SenSwtMonInfo.BsSwtMon;
    Swt_SenSwtMonInfo.DoorSwtMon = Task_5msBus.Swt_SenSwtMonInfo.DoorSwtMon;
    Swt_SenSwtMonInfo.EscSwtMon = Task_5msBus.Swt_SenSwtMonInfo.EscSwtMon;
    Swt_SenSwtMonInfo.FlexBrkASwtMon = Task_5msBus.Swt_SenSwtMonInfo.FlexBrkASwtMon;
    Swt_SenSwtMonInfo.FlexBrkBSwtMon = Task_5msBus.Swt_SenSwtMonInfo.FlexBrkBSwtMon;
    Swt_SenSwtMonInfo.HzrdSwtMon = Task_5msBus.Swt_SenSwtMonInfo.HzrdSwtMon;
    Swt_SenSwtMonInfo.HdcSwtMon = Task_5msBus.Swt_SenSwtMonInfo.HdcSwtMon;
    Swt_SenSwtMonInfo.PbSwtMon = Task_5msBus.Swt_SenSwtMonInfo.PbSwtMon;

    Swt_SenSwtMonInfoEsc = Task_5msBus.Swt_SenSwtMonInfoEsc;
    /*==============================================================================
    * Members of structure Swt_SenSwtMonInfoEsc 
     : Swt_SenSwtMonInfoEsc.BlsSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.AvhSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.EscSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.HdcSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.PbSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.GearRSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.BlfSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.ClutchSwtMon_Esc;
     : Swt_SenSwtMonInfoEsc.ItpmsSwtMon_Esc;
     =============================================================================*/

    /* Single interface */
    Swt_SenRsmDbcMon = Task_5msBus.Swt_SenRsmDbcMon;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Swt_SenEcuModeSts = Mom_HndlrEcuModeSts;
    //Swt_SenFuncInhibitSwtSts = Eem_SuspcDetnFuncInhibitSwtSts;

    /* Execute  runnable */
    Swt_Sen();

    /* Structure interface */
    Task_5msBus.Swt_SenEscSwtStInfo = Swt_SenEscSwtStInfo;
    /*==============================================================================
    * Members of structure Swt_SenEscSwtStInfo 
     : Swt_SenEscSwtStInfo.EscDisabledBySwt;
     : Swt_SenEscSwtStInfo.TcsDisabledBySwt;
     : Swt_SenEscSwtStInfo.HdcEnabledBySwt;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Swt_Sen end
     **********************************************************************************************/

    /**********************************************************************************************
     Spc_5msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo = Task_5msBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo 
     : Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
     : Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
     =============================================================================*/

    /* Single interface */
    Spc_5msCtrlMotCtrlState = Task_5msBus.Spc_5msCtrlMotCtrlState;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
    Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq;
    Spc_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq;
    Spc_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq;
    Spc_5msCtrlNormVlvReqVlvActInfo.SimVlvReq = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq;

    /* Decomposed structure interface */
    Spc_5msCtrlEemFailData.Eem_Fail_CirP1 = Eem_MainEemFailData.Eem_Fail_CirP1;
    Spc_5msCtrlEemFailData.Eem_Fail_CirP2 = Eem_MainEemFailData.Eem_Fail_CirP2;
    Spc_5msCtrlEemFailData.Eem_Fail_SimP = Eem_MainEemFailData.Eem_Fail_SimP;
    Spc_5msCtrlEemFailData.Eem_Fail_BLS = Eem_MainEemFailData.Eem_Fail_BLS;
    Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT = Eem_MainEemFailData.Eem_Fail_PedalPDT;
    Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF = Eem_MainEemFailData.Eem_Fail_PedalPDF;

    Spc_5msCtrlCanRxAccelPedlInfo = Proxy_RxCanRxAccelPedlInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlCanRxAccelPedlInfo 
     : Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    Spc_5msCtrlPdt5msRawInfo = Pedal_SenSyncPdt5msRawInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlPdt5msRawInfo 
     : Spc_5msCtrlPdt5msRawInfo.PdtSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Spc_5msCtrlPdf5msRawInfo.PdfSig = Pedal_SenSyncPdf5msRawInfo.PdfSig;

    Spc_5msCtrlSwtStsFlexBrkInfo = Swt_SenSwtStsFlexBrkInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlSwtStsFlexBrkInfo 
     : Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt;
     : Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt;
     =============================================================================*/

    Spc_5msCtrlWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlWhlSpdInfo 
     : Spc_5msCtrlWhlSpdInfo.FlWhlSpd;
     : Spc_5msCtrlWhlSpdInfo.FrWhlSpd;
     : Spc_5msCtrlWhlSpdInfo.RlWhlSpd;
     : Spc_5msCtrlWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Spc_5msCtrlCircP5msRawInfo = Press_SenSyncCircP5msRawInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlCircP5msRawInfo 
     : Spc_5msCtrlCircP5msRawInfo.PrimCircPSig;
     : Spc_5msCtrlCircP5msRawInfo.SecdCircPSig;
     =============================================================================*/

    Spc_5msCtrlPistP5msRawInfo = Press_SenSyncPistP5msRawInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlPistP5msRawInfo 
     : Spc_5msCtrlPistP5msRawInfo.PistPSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Spc_5msCtrlAbsCtrlInfo.AbsActFlg = Abc_CtrlAbsCtrlInfo.AbsActFlg;
    Spc_5msCtrlAbsCtrlInfo.AbsDefectFlg = Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
    Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
    Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
    Spc_5msCtrlAbsCtrlInfo.AbsTarPReLe = Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
    Spc_5msCtrlAbsCtrlInfo.AbsTarPReRi = Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
    Spc_5msCtrlAbsCtrlInfo.FrntWhlSlip = Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
    Spc_5msCtrlAbsCtrlInfo.AbsDesTarP = Abc_CtrlAbsCtrlInfo.AbsDesTarP;
    Spc_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
    Spc_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;

    /* Decomposed structure interface */
    Spc_5msCtrlEscCtrlInfo.EscActFlg = Abc_CtrlEscCtrlInfo.EscActFlg;
    Spc_5msCtrlEscCtrlInfo.EscDefectFlg = Abc_CtrlEscCtrlInfo.EscDefectFlg;
    Spc_5msCtrlEscCtrlInfo.EscTarPFrntLe = Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
    Spc_5msCtrlEscCtrlInfo.EscTarPFrntRi = Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
    Spc_5msCtrlEscCtrlInfo.EscTarPReLe = Abc_CtrlEscCtrlInfo.EscTarPReLe;
    Spc_5msCtrlEscCtrlInfo.EscTarPReRi = Abc_CtrlEscCtrlInfo.EscTarPReRi;

    Spc_5msCtrlSwtStsFSInfo = Eem_SuspcDetnSwtStsFSInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlSwtStsFSInfo 
     : Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet;
     =============================================================================*/

    /* Decomposed structure interface */
    Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH = Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH;
    Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag = Det_5msCtrlPedlTrvlTagInfo.PdfSigTag;
    Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH = Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH;
    Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag = Det_5msCtrlPedlTrvlTagInfo.PdtSigTag;

    /* Decomposed structure interface */
    Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1 = Eem_MainEemSuspectData.Eem_Suspect_CirP1;
    Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2 = Eem_MainEemSuspectData.Eem_Suspect_CirP2;
    Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP = Eem_MainEemSuspectData.Eem_Suspect_SimP;
    Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS = Eem_MainEemSuspectData.Eem_Suspect_BLS;
    Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = Eem_MainEemSuspectData.Eem_Suspect_PedalPDT;
    Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = Eem_MainEemSuspectData.Eem_Suspect_PedalPDF;

    /* Inter-Runnable single interface */
    Spc_5msCtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Spc_5msCtrlPCtrlAct = Pct_5msCtrlPCtrlAct;
    Spc_5msCtrlBlsSwt = Swt_SenBlsSwt;
    Spc_5msCtrlPedlSimPRaw = Press_SenSyncPedlSimPRaw;
    Spc_5msCtrlActvBrkCtrlrActFlg = Abc_CtrlActvBrkCtrlrActFlg;
    Spc_5msCtrlPedlTrvlFinal = Det_5msCtrlPedlTrvlFinal;
    Spc_5msCtrlVehSpdFild = Det_5msCtrlVehSpdFild;

    /* Execute  runnable */
    Int_5ms_IdbMotOrgSetFlg = Ses_CtrlMotOrgSetStInfo.MotOrgSet;
    if (Int_5ms_IdbMotOrgSetFlg == 1) Spc_5msCtrl();

    /* Structure interface */
    Task_5msBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo = Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
    /*==============================================================================
    * Members of structure Spc_5msCtrlIdbSnsrEolOfsCalcInfo 
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Spc_5msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Det_5msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd = Task_5msBus.Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd;

    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[i];
    for(i=0; i<10; i++) Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[i];

    Det_5msCtrlNormVlvReqVlvActInfo = Vat_CtrlNormVlvReqVlvActInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlNormVlvReqVlvActInfo 
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.SimVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.CircVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen;
     =============================================================================*/

    Det_5msCtrlWhlVlvReqIdbInfo = Vat_CtrlWhlVlvReqIdbInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlWhlVlvReqIdbInfo 
     : Det_5msCtrlWhlVlvReqIdbInfo.FlIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData;
     =============================================================================*/

    Det_5msCtrlIdbBalVlvReqInfo = Vat_CtrlIdbBalVlvReqInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlIdbBalVlvReqInfo 
     : Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData;
     : Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData;
     : Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData;
     : Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReq;
     : Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReq;
     : Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReq;
     : Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvDataLen;
     : Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvDataLen;
     : Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen;
     =============================================================================*/

    /* Decomposed structure interface */
    Det_5msCtrlEemFailData.Eem_Fail_SimP = Eem_MainEemFailData.Eem_Fail_SimP;
    Det_5msCtrlEemFailData.Eem_Fail_PedalPDT = Eem_MainEemFailData.Eem_Fail_PedalPDT;
    Det_5msCtrlEemFailData.Eem_Fail_PedalPDF = Eem_MainEemFailData.Eem_Fail_PedalPDF;
    Det_5msCtrlEemFailData.Eem_Fail_WssFL = Eem_MainEemFailData.Eem_Fail_WssFL;
    Det_5msCtrlEemFailData.Eem_Fail_WssFR = Eem_MainEemFailData.Eem_Fail_WssFR;
    Det_5msCtrlEemFailData.Eem_Fail_WssRL = Eem_MainEemFailData.Eem_Fail_WssRL;
    Det_5msCtrlEemFailData.Eem_Fail_WssRR = Eem_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    Det_5msCtrlCanRxIdbInfo.HcuServiceMod = Proxy_RxCanRxIdbInfo.HcuServiceMod;

    Det_5msCtrlPdt5msRawInfo = Pedal_SenSyncPdt5msRawInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlPdt5msRawInfo 
     : Det_5msCtrlPdt5msRawInfo.PdtSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Det_5msCtrlPdf5msRawInfo.PdfSig = Pedal_SenSyncPdf5msRawInfo.PdfSig;

    /* Decomposed structure interface */
    Det_5msCtrlAbsCtrlInfo.AbsActFlg = Abc_CtrlAbsCtrlInfo.AbsActFlg;
    Det_5msCtrlAbsCtrlInfo.AbsDefectFlg = Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
    Det_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
    Det_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
    Det_5msCtrlAbsCtrlInfo.AbsTarPReLe = Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
    Det_5msCtrlAbsCtrlInfo.AbsTarPReRi = Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
    Det_5msCtrlAbsCtrlInfo.FrntWhlSlip = Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
    Det_5msCtrlAbsCtrlInfo.AbsDesTarP = Abc_CtrlAbsCtrlInfo.AbsDesTarP;
    Det_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
    Det_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;

    Det_5msCtrlCircPFildInfo = Spc_5msCtrlCircPFildInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlCircPFildInfo 
     : Det_5msCtrlCircPFildInfo.PrimCircPFild;
     : Det_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
     : Det_5msCtrlCircPFildInfo.SecdCircPFild;
     : Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
     =============================================================================*/

    /* Decomposed structure interface */
    Det_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;
    Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;

    /* Decomposed structure interface */
    Det_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;

    /* Decomposed structure interface */
    Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;

    Det_5msCtrlPedlTrvlFildInfo = Spc_5msCtrlPedlTrvlFildInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlPedlTrvlFildInfo 
     : Det_5msCtrlPedlTrvlFildInfo.PdfFild;
     : Det_5msCtrlPedlTrvlFildInfo.PdtFild;
     =============================================================================*/

    Det_5msCtrlPistPFildInfo = Spc_5msCtrlPistPFildInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlPistPFildInfo 
     : Det_5msCtrlPistPFildInfo.PistPFild;
     : Det_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
     =============================================================================*/

    /* Decomposed structure interface */
    Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd = Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;

    Det_5msCtrlWhlSpdFildInfo = Spc_5msCtrlWhlSpdFildInfo;
    /*==============================================================================
    * Members of structure Det_5msCtrlWhlSpdFildInfo 
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe;
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi;
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;
     =============================================================================*/

    /* Decomposed structure interface */
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssFL = Eem_MainEemSuspectData.Eem_Suspect_WssFL;
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssFR = Eem_MainEemSuspectData.Eem_Suspect_WssFR;
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssRL = Eem_MainEemSuspectData.Eem_Suspect_WssRL;
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssRR = Eem_MainEemSuspectData.Eem_Suspect_WssRR;
    Det_5msCtrlEemSuspectData.Eem_Suspect_SimP = Eem_MainEemSuspectData.Eem_Suspect_SimP;
    Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = Eem_MainEemSuspectData.Eem_Suspect_PedalPDT;
    Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = Eem_MainEemSuspectData.Eem_Suspect_PedalPDF;

    /* Inter-Runnable single interface */
    //Det_5msCtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Det_5msCtrlPCtrlAct = Pct_5msCtrlPCtrlAct;
    Det_5msCtrlActvBrkCtrlrActFlg = Abc_CtrlActvBrkCtrlrActFlg;
    Det_5msCtrlVehSpd = Abc_CtrlVehSpd;
    Det_5msCtrlBlsFild = Spc_5msCtrlBlsFild;
    Det_5msCtrlPCtrlBoostMod = Pct_5msCtrlPCtrlBoostMod;

    /* Execute  runnable */
    if (Int_5ms_IdbMotOrgSetFlg == 1)     Det_5msCtrl();

    /* Structure interface */
    /* Single interface */
    Task_5msBus.Det_5msCtrlVehSpdFild = Det_5msCtrlVehSpdFild;

    /**********************************************************************************************
     Det_5msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Bbc_Ctrl start
     **********************************************************************************************/

     /* Structure interface */
    Bbc_CtrlBrkPedlStatus1msInfo = Task_5msBus.Bbc_CtrlBrkPedlStatus1msInfo;
    /*==============================================================================
    * Members of structure Bbc_CtrlBrkPedlStatus1msInfo 
     : Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/

    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    Bbc_CtrlEemFailData.Eem_Fail_BLS = Eem_MainEemFailData.Eem_Fail_BLS;
    Bbc_CtrlEemFailData.Eem_Fail_WssFL = Eem_MainEemFailData.Eem_Fail_WssFL;
    Bbc_CtrlEemFailData.Eem_Fail_WssFR = Eem_MainEemFailData.Eem_Fail_WssFR;
    Bbc_CtrlEemFailData.Eem_Fail_WssRL = Eem_MainEemFailData.Eem_Fail_WssRL;
    Bbc_CtrlEemFailData.Eem_Fail_WssRR = Eem_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    Bbc_CtrlCanRxIdbInfo.GearSelDisp = Proxy_RxCanRxIdbInfo.GearSelDisp;
    Bbc_CtrlCanRxIdbInfo.TcuSwiGs = Proxy_RxCanRxIdbInfo.TcuSwiGs;

    /* Decomposed structure interface */
    Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg = Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg;
    Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1 = Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1;
    Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2 = Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2;

    /* Decomposed structure interface */
    Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;

    Bbc_CtrlPedlTrvlRateInfo = Det_5msCtrlPedlTrvlRateInfo;
    /*==============================================================================
    * Members of structure Bbc_CtrlPedlTrvlRateInfo 
     : Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate;
     : Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;
     =============================================================================*/

    /* Decomposed structure interface */
    Bbc_CtrlVehStopStInfo.VehStopStFlg = Det_5msCtrlVehStopStInfo.VehStopStFlg;

    /* Decomposed structure interface */
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssFL = Eem_MainEemSuspectData.Eem_Suspect_WssFL;
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssFR = Eem_MainEemSuspectData.Eem_Suspect_WssFR;
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssRL = Eem_MainEemSuspectData.Eem_Suspect_WssRL;
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssRR = Eem_MainEemSuspectData.Eem_Suspect_WssRR;

    /* Inter-Runnable single interface */
    //Bbc_CtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Bbc_CtrlIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    //Bbc_CtrlFuncInhibitBbcSts = Eem_SuspcDetnFuncInhibitBbcSts;
    Bbc_CtrlBlsFild = Spc_5msCtrlBlsFild;
    Bbc_CtrlBrkPRednForBaseBrkCtrlr = Rbc_CtrlBrkPRednForBaseBrkCtrlr;
    Bbc_CtrlFlexBrkSwtFild = Spc_5msCtrlFlexBrkSwtFild;
    Bbc_CtrlPedlTrvlFinal = Det_5msCtrlPedlTrvlFinal;
    Bbc_CtrlRgnBrkCtrlrActStFlg = Rbc_CtrlRgnBrkCtrlrActStFlg;
    Bbc_CtrlVehSpdFild = Det_5msCtrlVehSpdFild;

    /* Execute  runnable */
    if (Int_5ms_IdbMotOrgSetFlg == 1) Bbc_Ctrl();

    /* Structure interface */
    Task_5msBus.Bbc_CtrlBaseBrkCtrlModInfo = Bbc_CtrlBaseBrkCtrlModInfo;
    /*==============================================================================
    * Members of structure Bbc_CtrlBaseBrkCtrlModInfo 
     : Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
     : Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg;
     : Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Bbc_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Rbc_Ctrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    Rbc_CtrlCanRxRegenInfo = Proxy_RxCanRxRegenInfo;
    /*==============================================================================
    * Members of structure Rbc_CtrlCanRxRegenInfo 
     : Rbc_CtrlCanRxRegenInfo.HcuRegenEna;
     : Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/

    Rbc_CtrlCanRxAccelPedlInfo = Proxy_RxCanRxAccelPedlInfo;
    /*==============================================================================
    * Members of structure Rbc_CtrlCanRxAccelPedlInfo 
     : Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    /* Decomposed structure interface */
    Rbc_CtrlCanRxIdbInfo.EngMsgFault = Proxy_RxCanRxIdbInfo.EngMsgFault;
    Rbc_CtrlCanRxIdbInfo.GearSelDisp = Proxy_RxCanRxIdbInfo.GearSelDisp;
    Rbc_CtrlCanRxIdbInfo.TcuSwiGs = Proxy_RxCanRxIdbInfo.TcuSwiGs;
    Rbc_CtrlCanRxIdbInfo.HcuServiceMod = Proxy_RxCanRxIdbInfo.HcuServiceMod;
    Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault = Proxy_RxCanRxIdbInfo.SubCanBusSigFault;
    Rbc_CtrlCanRxIdbInfo.CoolantTemp = Proxy_RxCanRxIdbInfo.CoolantTemp;
    Rbc_CtrlCanRxIdbInfo.CoolantTempErr = Proxy_RxCanRxIdbInfo.CoolantTempErr;

    Rbc_CtrlCanRxEngTempInfo = Proxy_RxCanRxEngTempInfo;
    /*==============================================================================
    * Members of structure Rbc_CtrlCanRxEngTempInfo 
     : Rbc_CtrlCanRxEngTempInfo.EngTemp;
     : Rbc_CtrlCanRxEngTempInfo.EngTempErr;
     =============================================================================*/

    /* Decomposed structure interface */
    Rbc_CtrlAbsCtrlInfo.AbsActFlg = Abc_CtrlAbsCtrlInfo.AbsActFlg;
    Rbc_CtrlAbsCtrlInfo.AbsDefectFlg = Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
    Rbc_CtrlAbsCtrlInfo.AbsTarPFrntLe = Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
    Rbc_CtrlAbsCtrlInfo.AbsTarPFrntRi = Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
    Rbc_CtrlAbsCtrlInfo.AbsTarPReLe = Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
    Rbc_CtrlAbsCtrlInfo.AbsTarPReRi = Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
    Rbc_CtrlAbsCtrlInfo.FrntWhlSlip = Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
    Rbc_CtrlAbsCtrlInfo.AbsDesTarP = Abc_CtrlAbsCtrlInfo.AbsDesTarP;
    Rbc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
    Rbc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;

    Rbc_CtrlAvhCtrlInfo = Abc_CtrlAvhCtrlInfo;
    /*==============================================================================
    * Members of structure Rbc_CtrlAvhCtrlInfo 
     : Rbc_CtrlAvhCtrlInfo.AvhActFlg;
     : Rbc_CtrlAvhCtrlInfo.AvhDefectFlg;
     : Rbc_CtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/

    /* Decomposed structure interface */
    Rbc_CtrlEscCtrlInfo.EscActFlg = Abc_CtrlEscCtrlInfo.EscActFlg;
    Rbc_CtrlEscCtrlInfo.EscDefectFlg = Abc_CtrlEscCtrlInfo.EscDefectFlg;
    Rbc_CtrlEscCtrlInfo.EscTarPFrntLe = Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
    Rbc_CtrlEscCtrlInfo.EscTarPFrntRi = Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
    Rbc_CtrlEscCtrlInfo.EscTarPReLe = Abc_CtrlEscCtrlInfo.EscTarPReLe;
    Rbc_CtrlEscCtrlInfo.EscTarPReRi = Abc_CtrlEscCtrlInfo.EscTarPReRi;

    Rbc_CtrlSccCtrlInfo = Abc_CtrlSccCtrlInfo;
    /*==============================================================================
    * Members of structure Rbc_CtrlSccCtrlInfo 
     : Rbc_CtrlSccCtrlInfo.SccActFlg;
     : Rbc_CtrlSccCtrlInfo.SccDefectFlg;
     : Rbc_CtrlSccCtrlInfo.SccTarP;
     =============================================================================*/

    /* Decomposed structure interface */
    Rbc_CtrlTcsCtrlInfo.TcsActFlg = Abc_CtrlTcsCtrlInfo.TcsActFlg;
    Rbc_CtrlTcsCtrlInfo.TcsDefectFlg = Abc_CtrlTcsCtrlInfo.TcsDefectFlg;
    Rbc_CtrlTcsCtrlInfo.TcsTarPFrntLe = Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe;
    Rbc_CtrlTcsCtrlInfo.TcsTarPFrntRi = Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi;
    Rbc_CtrlTcsCtrlInfo.TcsTarPReLe = Abc_CtrlTcsCtrlInfo.TcsTarPReLe;
    Rbc_CtrlTcsCtrlInfo.TcsTarPReRi = Abc_CtrlTcsCtrlInfo.TcsTarPReRi;
    Rbc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;

    /* Decomposed structure interface */
    Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
    Rbc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;

    /* Decomposed structure interface */
    Rbc_CtrlPedlTrvlFildInfo.PdtFild = Spc_5msCtrlPedlTrvlFildInfo.PdtFild;

    /* Decomposed structure interface */
    Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;

    /* Decomposed structure interface */
    Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar = Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar;

    /* Decomposed structure interface */
    Rbc_CtrlEemFailData.Eem_Fail_WssFL = Eem_MainEemFailData.Eem_Fail_WssFL;
    Rbc_CtrlEemFailData.Eem_Fail_WssFR = Eem_MainEemFailData.Eem_Fail_WssFR;
    Rbc_CtrlEemFailData.Eem_Fail_WssRL = Eem_MainEemFailData.Eem_Fail_WssRL;
    Rbc_CtrlEemFailData.Eem_Fail_WssRR = Eem_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssFL = Eem_MainEemSuspectData.Eem_Suspect_WssFL;
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssFR = Eem_MainEemSuspectData.Eem_Suspect_WssFR;
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssRL = Eem_MainEemSuspectData.Eem_Suspect_WssRL;
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssRR = Eem_MainEemSuspectData.Eem_Suspect_WssRR;

    /* Inter-Runnable single interface */
    //Rbc_CtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Rbc_CtrlIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Rbc_CtrlPCtrlAct = Pct_5msCtrlPCtrlAct;
    //Rbc_CtrlFuncInhibitRbcSts = Eem_SuspcDetnFuncInhibitRbcSts;
    Rbc_CtrlAy = Abc_CtrlAy;
    Rbc_CtrlBlsFild = Spc_5msCtrlBlsFild;
    Rbc_CtrlPedlTrvlFinal = Det_5msCtrlPedlTrvlFinal;
    Rbc_CtrlTarPFromBaseBrkCtrlr = Bbc_CtrlTarPFromBaseBrkCtrlr;
    Rbc_CtrlTarPRateFromBaseBrkCtrlr = Bbc_CtrlTarPRateFromBaseBrkCtrlr;
    Rbc_CtrlTarPFromBrkPedl = Bbc_CtrlTarPFromBrkPedl;
    Rbc_CtrlVehSpdFild = Det_5msCtrlVehSpdFild;

    /* Execute  runnable */
    Rbc_Ctrl();

    /* Structure interface */
    Task_5msBus.Rbc_CtrlTarRgnBrkTqInfo = Rbc_CtrlTarRgnBrkTqInfo;
    /*==============================================================================
    * Members of structure Rbc_CtrlTarRgnBrkTqInfo 
     : Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq;
     : Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep;
     : Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid;
     : Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce;
     : Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce;
     : Rbc_CtrlTarRgnBrkTqInfo.EhbStat;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Rbc_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Abc_Ctrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd = Task_5msBus.Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd;

    Abc_CtrlSasCalInfo = Task_5msBus.Abc_CtrlSasCalInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlSasCalInfo 
     : Abc_CtrlSasCalInfo.DiagSasCaltoAppl;
     =============================================================================*/

    Abc_CtrlTxESCSensorInfo = Task_5msBus.Abc_CtrlTxESCSensorInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlTxESCSensorInfo 
     : Abc_CtrlTxESCSensorInfo.EstimatedYaw;
     : Abc_CtrlTxESCSensorInfo.EstimatedAY;
     : Abc_CtrlTxESCSensorInfo.A_long_1_1000g;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable = Task_5msBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable;

    Abc_CtrlYAWMSerialInfo = Task_5msBus.Abc_CtrlYAWMSerialInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlYAWMSerialInfo 
     : Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg;
     : Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err = Task_5msBus.Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err;

    /* Single interface */
    Abc_CtrlVBatt1Mon = Task_5msBus.Abc_CtrlVBatt1Mon;
    Abc_CtrlDiagSci = Task_5msBus.Abc_CtrlDiagSci;

    /* Inter-Runnable structure interface */
    Abc_CtrlEemFailData = Eem_MainEemFailData;
    /*==============================================================================
    * Members of structure Abc_CtrlEemFailData 
     : Abc_CtrlEemFailData.Eem_Fail_BBSSol;
     : Abc_CtrlEemFailData.Eem_Fail_ESCSol;
     : Abc_CtrlEemFailData.Eem_Fail_FrontSol;
     : Abc_CtrlEemFailData.Eem_Fail_RearSol;
     : Abc_CtrlEemFailData.Eem_Fail_Motor;
     : Abc_CtrlEemFailData.Eem_Fail_MPS;
     : Abc_CtrlEemFailData.Eem_Fail_MGD;
     : Abc_CtrlEemFailData.Eem_Fail_BBSValveRelay;
     : Abc_CtrlEemFailData.Eem_Fail_ESCValveRelay;
     : Abc_CtrlEemFailData.Eem_Fail_ECUHw;
     : Abc_CtrlEemFailData.Eem_Fail_ASIC;
     : Abc_CtrlEemFailData.Eem_Fail_OverVolt;
     : Abc_CtrlEemFailData.Eem_Fail_UnderVolt;
     : Abc_CtrlEemFailData.Eem_Fail_LowVolt;
     : Abc_CtrlEemFailData.Eem_Fail_LowerVolt;
     : Abc_CtrlEemFailData.Eem_Fail_SenPwr_12V;
     : Abc_CtrlEemFailData.Eem_Fail_SenPwr_5V;
     : Abc_CtrlEemFailData.Eem_Fail_Yaw;
     : Abc_CtrlEemFailData.Eem_Fail_Ay;
     : Abc_CtrlEemFailData.Eem_Fail_Ax;
     : Abc_CtrlEemFailData.Eem_Fail_Str;
     : Abc_CtrlEemFailData.Eem_Fail_CirP1;
     : Abc_CtrlEemFailData.Eem_Fail_CirP2;
     : Abc_CtrlEemFailData.Eem_Fail_SimP;
     : Abc_CtrlEemFailData.Eem_Fail_BLS;
     : Abc_CtrlEemFailData.Eem_Fail_ESCSw;
     : Abc_CtrlEemFailData.Eem_Fail_HDCSw;
     : Abc_CtrlEemFailData.Eem_Fail_AVHSw;
     : Abc_CtrlEemFailData.Eem_Fail_BrakeLampRelay;
     : Abc_CtrlEemFailData.Eem_Fail_EssRelay;
     : Abc_CtrlEemFailData.Eem_Fail_GearR;
     : Abc_CtrlEemFailData.Eem_Fail_Clutch;
     : Abc_CtrlEemFailData.Eem_Fail_ParkBrake;
     : Abc_CtrlEemFailData.Eem_Fail_PedalPDT;
     : Abc_CtrlEemFailData.Eem_Fail_PedalPDF;
     : Abc_CtrlEemFailData.Eem_Fail_BrakeFluid;
     : Abc_CtrlEemFailData.Eem_Fail_TCSTemp;
     : Abc_CtrlEemFailData.Eem_Fail_HDCTemp;
     : Abc_CtrlEemFailData.Eem_Fail_SCCTemp;
     : Abc_CtrlEemFailData.Eem_Fail_TVBBTemp;
     : Abc_CtrlEemFailData.Eem_Fail_MainCanLine;
     : Abc_CtrlEemFailData.Eem_Fail_SubCanLine;
     : Abc_CtrlEemFailData.Eem_Fail_EMSTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_FWDTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_TCUTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_HCUTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_MCUTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_VariantCoding;
     : Abc_CtrlEemFailData.Eem_Fail_WssFL;
     : Abc_CtrlEemFailData.Eem_Fail_WssFR;
     : Abc_CtrlEemFailData.Eem_Fail_WssRL;
     : Abc_CtrlEemFailData.Eem_Fail_WssRR;
     : Abc_CtrlEemFailData.Eem_Fail_SameSideWss;
     : Abc_CtrlEemFailData.Eem_Fail_DiagonalWss;
     : Abc_CtrlEemFailData.Eem_Fail_FrontWss;
     : Abc_CtrlEemFailData.Eem_Fail_RearWss;
     =============================================================================*/

    Abc_CtrlEemCtrlInhibitData = Eem_MainEemCtrlInhibitData;
    /*==============================================================================
    * Members of structure Abc_CtrlEemCtrlInhibitData 
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/

    Abc_CtrlCanRxAccelPedlInfo = Proxy_RxCanRxAccelPedlInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlCanRxAccelPedlInfo 
     : Abc_CtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Abc_CtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlCanRxIdbInfo.TarGearPosi = Proxy_RxCanRxIdbInfo.TarGearPosi;
    Abc_CtrlCanRxIdbInfo.GearSelDisp = Proxy_RxCanRxIdbInfo.GearSelDisp;
    Abc_CtrlCanRxIdbInfo.TcuSwiGs = Proxy_RxCanRxIdbInfo.TcuSwiGs;

    Abc_CtrlCanRxEngTempInfo = Proxy_RxCanRxEngTempInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlCanRxEngTempInfo 
     : Abc_CtrlCanRxEngTempInfo.EngTemp;
     : Abc_CtrlCanRxEngTempInfo.EngTempErr;
     =============================================================================*/

    Abc_CtrlCanRxEscInfo = Proxy_RxCanRxEscInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlCanRxEscInfo 
     : Abc_CtrlCanRxEscInfo.Ax;
     : Abc_CtrlCanRxEscInfo.YawRate;
     : Abc_CtrlCanRxEscInfo.SteeringAngle;
     : Abc_CtrlCanRxEscInfo.Ay;
     : Abc_CtrlCanRxEscInfo.PbSwt;
     : Abc_CtrlCanRxEscInfo.ClutchSwt;
     : Abc_CtrlCanRxEscInfo.GearRSwt;
     : Abc_CtrlCanRxEscInfo.EngActIndTq;
     : Abc_CtrlCanRxEscInfo.EngRpm;
     : Abc_CtrlCanRxEscInfo.EngIndTq;
     : Abc_CtrlCanRxEscInfo.EngFrictionLossTq;
     : Abc_CtrlCanRxEscInfo.EngStdTq;
     : Abc_CtrlCanRxEscInfo.TurbineRpm;
     : Abc_CtrlCanRxEscInfo.ThrottleAngle;
     : Abc_CtrlCanRxEscInfo.TpsResol1000;
     : Abc_CtrlCanRxEscInfo.PvAvCanResol1000;
     : Abc_CtrlCanRxEscInfo.EngChr;
     : Abc_CtrlCanRxEscInfo.EngVol;
     : Abc_CtrlCanRxEscInfo.GearType;
     : Abc_CtrlCanRxEscInfo.EngClutchState;
     : Abc_CtrlCanRxEscInfo.EngTqCmdBeforeIntv;
     : Abc_CtrlCanRxEscInfo.MotEstTq;
     : Abc_CtrlCanRxEscInfo.MotTqCmdBeforeIntv;
     : Abc_CtrlCanRxEscInfo.TqIntvTCU;
     : Abc_CtrlCanRxEscInfo.TqIntvSlowTCU;
     : Abc_CtrlCanRxEscInfo.TqIncReq;
     : Abc_CtrlCanRxEscInfo.DecelReq;
     : Abc_CtrlCanRxEscInfo.RainSnsStat;
     : Abc_CtrlCanRxEscInfo.EngSpdErr;
     : Abc_CtrlCanRxEscInfo.AtType;
     : Abc_CtrlCanRxEscInfo.MtType;
     : Abc_CtrlCanRxEscInfo.CvtType;
     : Abc_CtrlCanRxEscInfo.DctType;
     : Abc_CtrlCanRxEscInfo.HevAtTcu;
     : Abc_CtrlCanRxEscInfo.TurbineRpmErr;
     : Abc_CtrlCanRxEscInfo.ThrottleAngleErr;
     : Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtAct;
     : Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtActV;
     : Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtAct;
     : Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtActV;
     : Abc_CtrlCanRxEscInfo.WiperIntSW;
     : Abc_CtrlCanRxEscInfo.WiperLow;
     : Abc_CtrlCanRxEscInfo.WiperHigh;
     : Abc_CtrlCanRxEscInfo.WiperValid;
     : Abc_CtrlCanRxEscInfo.WiperAuto;
     : Abc_CtrlCanRxEscInfo.TcuFaultSts;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlEscSwtStInfo.EscDisabledBySwt = Swt_SenEscSwtStInfo.EscDisabledBySwt;
    Abc_CtrlEscSwtStInfo.TcsDisabledBySwt = Swt_SenEscSwtStInfo.TcsDisabledBySwt;

    Abc_CtrlWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlWhlSpdInfo 
     : Abc_CtrlWhlSpdInfo.FlWhlSpd;
     : Abc_CtrlWhlSpdInfo.FrWhlSpd;
     : Abc_CtrlWhlSpdInfo.RlWhlSpd;
     : Abc_CtrlWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk;

    /* Decomposed structure interface */
    Abc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
    Abc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;

    /* Decomposed structure interface */
    Abc_CtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;
    Abc_CtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;

    Abc_CtrlEstimdWhlPInfo = Det_5msCtrlEstimdWhlPInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlEstimdWhlPInfo 
     : Abc_CtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
     : Abc_CtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
     : Abc_CtrlEstimdWhlPInfo.ReLeEstimdWhlP;
     : Abc_CtrlEstimdWhlPInfo.ReRiEstimdWhlP;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;

    /* Decomposed structure interface */
    Abc_CtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd = Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd;
    Abc_CtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd = Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd;

    /* Decomposed structure interface */
    Abc_CtrlPistPFildInfo.PistPFild_1_100Bar = Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar;

    /* Decomposed structure interface */
    Abc_CtrlPistPOffsCorrdInfo.PistPOffsCorrd = Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;

    Abc_CtrlRgnBrkCoopWithAbsInfo = Rbc_CtrlRgnBrkCoopWithAbsInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlRgnBrkCoopWithAbsInfo 
     : Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip;
     : Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl = Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl;
    Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState = Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;

    Abc_CtrlMuxCmdExecStInfo = Pct_5msCtrlMuxCmdExecStInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlMuxCmdExecStInfo 
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe;
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi;
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe;
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi;
     =============================================================================*/

    Abc_CtrlEemSuspectData = Eem_MainEemSuspectData;
    /*==============================================================================
    * Members of structure Abc_CtrlEemSuspectData 
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssFL;
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssFR;
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssRL;
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssRR;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SameSideWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_DiagonalWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_FrontWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_RearWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BBSSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ESCSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_FrontSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_RearSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Motor;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MPS;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MGD;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BBSValveRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ESCValveRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ECUHw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ASIC;
     : Abc_CtrlEemSuspectData.Eem_Suspect_OverVolt;
     : Abc_CtrlEemSuspectData.Eem_Suspect_UnderVolt;
     : Abc_CtrlEemSuspectData.Eem_Suspect_LowVolt;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_12V;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_5V;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Yaw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Ay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Ax;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Str;
     : Abc_CtrlEemSuspectData.Eem_Suspect_CirP1;
     : Abc_CtrlEemSuspectData.Eem_Suspect_CirP2;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SimP;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BS;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BLS;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ESCSw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_HDCSw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_AVHSw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BrakeLampRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_EssRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_GearR;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Clutch;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ParkBrake;
     : Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDT;
     : Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDF;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BrakeFluid;
     : Abc_CtrlEemSuspectData.Eem_Suspect_TCSTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_HDCTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SCCTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_TVBBTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MainCanLine;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SubCanLine;
     : Abc_CtrlEemSuspectData.Eem_Suspect_EMSTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_FWDTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_TCUTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_HCUTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MCUTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_VariantCoding;
     =============================================================================*/

    Abc_CtrlEemEceData = Eem_MainEemEceData;
    /*==============================================================================
    * Members of structure Abc_CtrlEemEceData 
     : Abc_CtrlEemEceData.Eem_Ece_Wss;
     : Abc_CtrlEemEceData.Eem_Ece_Yaw;
     : Abc_CtrlEemEceData.Eem_Ece_Ay;
     : Abc_CtrlEemEceData.Eem_Ece_Ax;
     : Abc_CtrlEemEceData.Eem_Ece_Cir1P;
     : Abc_CtrlEemEceData.Eem_Ece_Cir2P;
     : Abc_CtrlEemEceData.Eem_Ece_SimP;
     : Abc_CtrlEemEceData.Eem_Ece_Bls;
     : Abc_CtrlEemEceData.Eem_Ece_Pedal;
     : Abc_CtrlEemEceData.Eem_Ece_Motor;
     : Abc_CtrlEemEceData.Eem_Ece_Vdc_Sw;
     : Abc_CtrlEemEceData.Eem_Ece_Hdc_Sw;
     : Abc_CtrlEemEceData.Eem_Ece_GearR_Sw;
     : Abc_CtrlEemEceData.Eem_Ece_Clutch_Sw;
     =============================================================================*/

    /* Decomposed structure interface */
    Abc_CtrlFinalTarPInfo.FinalMaxCircuitTp = Arb_CtrlFinalTarPInfo.FinalMaxCircuitTp;

    /* Inter-Runnable single interface */
    Abc_CtrlFSBbsVlvInitEndFlg = BbsVlvM_MainFSBbsVlvInitEndFlg;
    Abc_CtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Abc_CtrlIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Abc_CtrlFSEscVlvInitEndFlg = AbsVlvM_MainFSEscVlvInitEndFlg;
    Abc_CtrlCanRxGearSelDispErrInfo = Proxy_RxCanRxGearSelDispErrInfo;
    Abc_CtrlPCtrlAct = Pct_5msCtrlPCtrlAct;
    Abc_CtrlBlsSwt = Swt_SenBlsSwt;
    Abc_CtrlEscSwt = Swt_SenEscSwt;
    Abc_CtrlBrkPRednForBaseBrkCtrlr = Rbc_CtrlBrkPRednForBaseBrkCtrlr;
    Abc_CtrlPedlTrvlFinal = Det_5msCtrlPedlTrvlFinal;
    Abc_CtrlRgnBrkCtrlrActStFlg = Rbc_CtrlRgnBrkCtrlrActStFlg;
    Abc_CtrlFinalTarPFromPCtrl = Pct_5msCtrlFinalTarPFromPCtrl;
    Abc_CtrlTarPFromBaseBrkCtrlr = Bbc_CtrlTarPFromBaseBrkCtrlr;
    Abc_CtrlTarPFromBrkPedl = Bbc_CtrlTarPFromBrkPedl;
    Abc_CtrlMTRInitEndFlg = MtrM_MainMTRInitEndFlg;

    /* Execute  runnable */
	if (Int_5ms_IdbMotOrgSetFlg == 1 && wmu1LoopTime10ms_0 == 1)
	{
	    Abc_Ctrl();
	}
    /* Structure interface */
    Task_5msBus.Abc_CtrlCanTxInfo = Abc_CtrlCanTxInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlCanTxInfo 
     : Abc_CtrlCanTxInfo.TqIntvTCS;
     : Abc_CtrlCanTxInfo.TqIntvMsr;
     : Abc_CtrlCanTxInfo.TqIntvSlowTCS;
     : Abc_CtrlCanTxInfo.MinGear;
     : Abc_CtrlCanTxInfo.MaxGear;
     : Abc_CtrlCanTxInfo.TcsReq;
     : Abc_CtrlCanTxInfo.TcsCtrl;
     : Abc_CtrlCanTxInfo.AbsAct;
     : Abc_CtrlCanTxInfo.TcsGearShiftChr;
     : Abc_CtrlCanTxInfo.EspCtrl;
     : Abc_CtrlCanTxInfo.MsrReq;
     : Abc_CtrlCanTxInfo.TcsProductInfo;
     =============================================================================*/

    Task_5msBus.Abc_CtrlAbsCtrlInfo = Abc_CtrlAbsCtrlInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlAbsCtrlInfo 
     : Abc_CtrlAbsCtrlInfo.AbsActFlg;
     : Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
     : Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
     : Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
     : Abc_CtrlAbsCtrlInfo.AbsDesTarP;
     : Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi;
     : Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsPrioReLe;
     : Abc_CtrlAbsCtrlInfo.AbsPrioReRi;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi;
     =============================================================================*/

    Task_5msBus.Abc_CtrlEscCtrlInfo = Abc_CtrlEscCtrlInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlEscCtrlInfo 
     : Abc_CtrlEscCtrlInfo.EscActFlg;
     : Abc_CtrlEscCtrlInfo.EscDefectFlg;
     : Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
     : Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
     : Abc_CtrlEscCtrlInfo.EscTarPReLe;
     : Abc_CtrlEscCtrlInfo.EscTarPReRi;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeReLe;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeReRi;
     : Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe;
     : Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi;
     : Abc_CtrlEscCtrlInfo.EscTarPRateReLe;
     : Abc_CtrlEscCtrlInfo.EscTarPRateReRi;
     : Abc_CtrlEscCtrlInfo.EscPrioFrntLe;
     : Abc_CtrlEscCtrlInfo.EscPrioFrntRi;
     : Abc_CtrlEscCtrlInfo.EscPrioReLe;
     : Abc_CtrlEscCtrlInfo.EscPrioReRi;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi;
     : Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe;
     : Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi;
     : Abc_CtrlEscCtrlInfo.EscDelTarPReLe;
     : Abc_CtrlEscCtrlInfo.EscDelTarPReRi;
     =============================================================================*/

    Task_5msBus.Abc_CtrlTcsCtrlInfo = Abc_CtrlTcsCtrlInfo;
    /*==============================================================================
    * Members of structure Abc_CtrlTcsCtrlInfo 
     : Abc_CtrlTcsCtrlInfo.TcsActFlg;
     : Abc_CtrlTcsCtrlInfo.TcsDefectFlg;
     : Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe;
     : Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi;
     : Abc_CtrlTcsCtrlInfo.TcsTarPReLe;
     : Abc_CtrlTcsCtrlInfo.TcsTarPReRi;
     : Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Abc_CtrlVehSpd = Abc_CtrlVehSpd;

    /**********************************************************************************************
     Abc_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Arb_Ctrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    for(i=0; i<10; i++) Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[i];
    for(i=0; i<10; i++) Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[i];
    for(i=0; i<10; i++) Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[i];
    for(i=0; i<10; i++) Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[i];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReq = Abc_CtrlWhlVlvReqAbcInfo.FlOvReq;
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReq = Abc_CtrlWhlVlvReqAbcInfo.FrOvReq;
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReq = Abc_CtrlWhlVlvReqAbcInfo.RlOvReq;
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReq = Abc_CtrlWhlVlvReqAbcInfo.RrOvReq;

    Arb_CtrlAbsCtrlInfo = Abc_CtrlAbsCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlAbsCtrlInfo 
     : Arb_CtrlAbsCtrlInfo.AbsActFlg;
     : Arb_CtrlAbsCtrlInfo.AbsDefectFlg;
     : Arb_CtrlAbsCtrlInfo.AbsTarPFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsTarPReLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPReRi;
     : Arb_CtrlAbsCtrlInfo.FrntWhlSlip;
     : Arb_CtrlAbsCtrlInfo.AbsDesTarP;
     : Arb_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeReRi;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateReLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateReRi;
     : Arb_CtrlAbsCtrlInfo.AbsPrioFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsPrioFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsPrioReLe;
     : Arb_CtrlAbsCtrlInfo.AbsPrioReRi;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPReLe;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPReRi;
     =============================================================================*/

    Arb_CtrlAvhCtrlInfo = Abc_CtrlAvhCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlAvhCtrlInfo 
     : Arb_CtrlAvhCtrlInfo.AvhActFlg;
     : Arb_CtrlAvhCtrlInfo.AvhDefectFlg;
     : Arb_CtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/

    Arb_CtrlBaCtrlInfo = Abc_CtrlBaCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlBaCtrlInfo 
     : Arb_CtrlBaCtrlInfo.BaActFlg;
     : Arb_CtrlBaCtrlInfo.BaCDefectFlg;
     : Arb_CtrlBaCtrlInfo.BaTarP;
     =============================================================================*/

    Arb_CtrlEbdCtrlInfo = Abc_CtrlEbdCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlEbdCtrlInfo 
     : Arb_CtrlEbdCtrlInfo.EbdActFlg;
     : Arb_CtrlEbdCtrlInfo.EbdDefectFlg;
     : Arb_CtrlEbdCtrlInfo.EbdTarPFrntLe;
     : Arb_CtrlEbdCtrlInfo.EbdTarPFrntRi;
     : Arb_CtrlEbdCtrlInfo.EbdTarPReLe;
     : Arb_CtrlEbdCtrlInfo.EbdTarPReRi;
     : Arb_CtrlEbdCtrlInfo.EbdTarPRateReLe;
     : Arb_CtrlEbdCtrlInfo.EbdTarPRateReRi;
     : Arb_CtrlEbdCtrlInfo.EbdCtrlModeReLe;
     : Arb_CtrlEbdCtrlInfo.EbdCtrlModeReRi;
     : Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe;
     : Arb_CtrlEbdCtrlInfo.EbdDelTarPReRi;
     =============================================================================*/

    Arb_CtrlEbpCtrlInfo = Abc_CtrlEbpCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlEbpCtrlInfo 
     : Arb_CtrlEbpCtrlInfo.EbpActFlg;
     : Arb_CtrlEbpCtrlInfo.EbpDefectFlg;
     : Arb_CtrlEbpCtrlInfo.EbpTarP;
     =============================================================================*/

    Arb_CtrlEpbiCtrlInfo = Abc_CtrlEpbiCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlEpbiCtrlInfo 
     : Arb_CtrlEpbiCtrlInfo.EpbiActFlg;
     : Arb_CtrlEpbiCtrlInfo.EpbiDefectFlg;
     : Arb_CtrlEpbiCtrlInfo.EpbiTarP;
     =============================================================================*/

    Arb_CtrlEscCtrlInfo = Abc_CtrlEscCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlEscCtrlInfo 
     : Arb_CtrlEscCtrlInfo.EscActFlg;
     : Arb_CtrlEscCtrlInfo.EscDefectFlg;
     : Arb_CtrlEscCtrlInfo.EscTarPFrntLe;
     : Arb_CtrlEscCtrlInfo.EscTarPFrntRi;
     : Arb_CtrlEscCtrlInfo.EscTarPReLe;
     : Arb_CtrlEscCtrlInfo.EscTarPReRi;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeReLe;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeReRi;
     : Arb_CtrlEscCtrlInfo.EscTarPRateFrntLe;
     : Arb_CtrlEscCtrlInfo.EscTarPRateFrntRi;
     : Arb_CtrlEscCtrlInfo.EscTarPRateReLe;
     : Arb_CtrlEscCtrlInfo.EscTarPRateReRi;
     : Arb_CtrlEscCtrlInfo.EscPrioFrntLe;
     : Arb_CtrlEscCtrlInfo.EscPrioFrntRi;
     : Arb_CtrlEscCtrlInfo.EscPrioReLe;
     : Arb_CtrlEscCtrlInfo.EscPrioReRi;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeReLe;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeReRi;
     : Arb_CtrlEscCtrlInfo.EscDelTarPFrntLe;
     : Arb_CtrlEscCtrlInfo.EscDelTarPFrntRi;
     : Arb_CtrlEscCtrlInfo.EscDelTarPReLe;
     : Arb_CtrlEscCtrlInfo.EscDelTarPReRi;
     =============================================================================*/

    Arb_CtrlHsaCtrlInfo = Abc_CtrlHsaCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlHsaCtrlInfo 
     : Arb_CtrlHsaCtrlInfo.HsaActFlg;
     : Arb_CtrlHsaCtrlInfo.HsaDefectFlg;
     : Arb_CtrlHsaCtrlInfo.HsaTarP;
     =============================================================================*/

    Arb_CtrlSccCtrlInfo = Abc_CtrlSccCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlSccCtrlInfo 
     : Arb_CtrlSccCtrlInfo.SccActFlg;
     : Arb_CtrlSccCtrlInfo.SccDefectFlg;
     : Arb_CtrlSccCtrlInfo.SccTarP;
     =============================================================================*/

    Arb_CtrlTcsCtrlInfo = Abc_CtrlTcsCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlTcsCtrlInfo 
     : Arb_CtrlTcsCtrlInfo.TcsActFlg;
     : Arb_CtrlTcsCtrlInfo.TcsDefectFlg;
     : Arb_CtrlTcsCtrlInfo.TcsTarPFrntLe;
     : Arb_CtrlTcsCtrlInfo.TcsTarPFrntRi;
     : Arb_CtrlTcsCtrlInfo.TcsTarPReLe;
     : Arb_CtrlTcsCtrlInfo.TcsTarPReRi;
     : Arb_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPReLe;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPReRi;
     =============================================================================*/

    Arb_CtrlTvbbCtrlInfo = Abc_CtrlTvbbCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlTvbbCtrlInfo 
     : Arb_CtrlTvbbCtrlInfo.TvbbActFlg;
     : Arb_CtrlTvbbCtrlInfo.TvbbDefectFlg;
     : Arb_CtrlTvbbCtrlInfo.TvbbTarP;
     =============================================================================*/

    /* Decomposed structure interface */
    Arb_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg = Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;

    Arb_CtrlBBCCtrlInfo = Bbc_CtrlBBCCtrlInfo;
    /*==============================================================================
    * Members of structure Arb_CtrlBBCCtrlInfo 
     : Arb_CtrlBBCCtrlInfo.BBCCtrlSt;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Arb_CtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Arb_CtrlIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    //Arb_CtrlFuncInhibitArbiSts = Eem_SuspcDetnFuncInhibitArbiSts;
    Arb_CtrlBaseBrkCtrlrActFlg = Bbc_CtrlBaseBrkCtrlrActFlg;
    Arb_CtrlBrkPRednForBaseBrkCtrlr = Rbc_CtrlBrkPRednForBaseBrkCtrlr;
    Arb_CtrlPCtrlSt = Pct_5msCtrlPCtrlSt;
    Arb_CtrlRgnBrkCtrlrActStFlg = Rbc_CtrlRgnBrkCtrlrActStFlg;
    Arb_CtrlTarPFromBaseBrkCtrlr = Bbc_CtrlTarPFromBaseBrkCtrlr;
    Arb_CtrlTarPRateFromBaseBrkCtrlr = Bbc_CtrlTarPRateFromBaseBrkCtrlr;
    Arb_CtrlVehSpdFild = Det_5msCtrlVehSpdFild;

    /* Execute  runnable */
    if (Int_5ms_IdbMotOrgSetFlg == 1) Arb_Ctrl();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Arb_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Pct_5msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    Pct_5msCtrlPedlTrvlFild1msInfo = Task_5msBus.Pct_5msCtrlPedlTrvlFild1msInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlPedlTrvlFild1msInfo 
     : Pct_5msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd = Task_5msBus.Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd;

    Pct_5msCtrlBrkPedlStatus1msInfo = Task_5msBus.Pct_5msCtrlBrkPedlStatus1msInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlBrkPedlStatus1msInfo 
     : Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/

    Pct_5msCtrlPedlTrvlRate1msInfo = Task_5msBus.Pct_5msCtrlPedlTrvlRate1msInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlPedlTrvlRate1msInfo 
     : Pct_5msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/

    Pct_5msCtrlIdbMotPosnInfo = Task_5msBus.Pct_5msCtrlIdbMotPosnInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlIdbMotPosnInfo 
     : Pct_5msCtrlIdbMotPosnInfo.MotTarPosn;
     =============================================================================*/

    /* Single interface */
    Pct_5msCtrlMotCtrlMode = Task_5msBus.Pct_5msCtrlMotCtrlMode;
    Pct_5msCtrlMotICtrlFadeOutState = Task_5msBus.Pct_5msCtrlMotICtrlFadeOutState;
    Pct_5msCtrlPreBoostMod = Task_5msBus.Pct_5msCtrlPreBoostMod;
    Pct_5msCtrlStkRecvryStabnEndOK = Task_5msBus.Pct_5msCtrlStkRecvryStabnEndOK;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    for(i=0; i<10; i++) Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[i];
    for(i=0; i<10; i++) Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[i];
    for(i=0; i<10; i++) Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[i];
    for(i=0; i<10; i++) Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[i] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[i];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReq = Abc_CtrlWhlVlvReqAbcInfo.FlIvReq;
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReq = Abc_CtrlWhlVlvReqAbcInfo.FrIvReq;
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReq = Abc_CtrlWhlVlvReqAbcInfo.RlIvReq;
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReq = Abc_CtrlWhlVlvReqAbcInfo.RrIvReq;

    /* Decomposed structure interface */
    Pct_5msCtrlEemFailData.Eem_Fail_SimP = Eem_MainEemFailData.Eem_Fail_SimP;
    Pct_5msCtrlEemFailData.Eem_Fail_BLS = Eem_MainEemFailData.Eem_Fail_BLS;

    Pct_5msCtrlCanRxAccelPedlInfo = Proxy_RxCanRxAccelPedlInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlCanRxAccelPedlInfo 
     : Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlAbsCtrlInfo.AbsActFlg = Abc_CtrlAbsCtrlInfo.AbsActFlg;
    Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg = Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
    Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
    Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
    Pct_5msCtrlAbsCtrlInfo.AbsTarPReLe = Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
    Pct_5msCtrlAbsCtrlInfo.AbsTarPReRi = Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
    Pct_5msCtrlAbsCtrlInfo.FrntWhlSlip = Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
    Pct_5msCtrlAbsCtrlInfo.AbsDesTarP = Abc_CtrlAbsCtrlInfo.AbsDesTarP;
    Pct_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
    Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;

    Pct_5msCtrlAvhCtrlInfo = Abc_CtrlAvhCtrlInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlAvhCtrlInfo 
     : Pct_5msCtrlAvhCtrlInfo.AvhActFlg;
     : Pct_5msCtrlAvhCtrlInfo.AvhDefectFlg;
     : Pct_5msCtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlEscCtrlInfo.EscActFlg = Abc_CtrlEscCtrlInfo.EscActFlg;
    Pct_5msCtrlEscCtrlInfo.EscDefectFlg = Abc_CtrlEscCtrlInfo.EscDefectFlg;
    Pct_5msCtrlEscCtrlInfo.EscTarPFrntLe = Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
    Pct_5msCtrlEscCtrlInfo.EscTarPFrntRi = Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
    Pct_5msCtrlEscCtrlInfo.EscTarPReLe = Abc_CtrlEscCtrlInfo.EscTarPReLe;
    Pct_5msCtrlEscCtrlInfo.EscTarPReRi = Abc_CtrlEscCtrlInfo.EscTarPReRi;

    Pct_5msCtrlHsaCtrlInfo = Abc_CtrlHsaCtrlInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlHsaCtrlInfo 
     : Pct_5msCtrlHsaCtrlInfo.HsaActFlg;
     : Pct_5msCtrlHsaCtrlInfo.HsaDefectFlg;
     : Pct_5msCtrlHsaCtrlInfo.HsaTarP;
     =============================================================================*/

    Pct_5msCtrlStkRecvryCtrlIfInfo = Abc_CtrlStkRecvryCtrlIfInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlStkRecvryCtrlIfInfo 
     : Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime;
     : Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime;
     : Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlTcsCtrlInfo.TcsActFlg = Abc_CtrlTcsCtrlInfo.TcsActFlg;
    Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg = Abc_CtrlTcsCtrlInfo.TcsDefectFlg;
    Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntLe = Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe;
    Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntRi = Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi;
    Pct_5msCtrlTcsCtrlInfo.TcsTarPReLe = Abc_CtrlTcsCtrlInfo.TcsTarPReLe;
    Pct_5msCtrlTcsCtrlInfo.TcsTarPReRi = Abc_CtrlTcsCtrlInfo.TcsTarPReRi;
    Pct_5msCtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;

    /* Decomposed structure interface */
    Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg = Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;

    /* Decomposed structure interface */
    Pct_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk;
    Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg = Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg;
    Pct_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2 = Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2;

    /* Decomposed structure interface */
    Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
    Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;

    /* Decomposed structure interface */
    Pct_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms = Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms;
    Pct_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms = Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms;

    Pct_5msCtrlEstimdWhlPInfo = Det_5msCtrlEstimdWhlPInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlEstimdWhlPInfo 
     : Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
     : Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
     : Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;
     : Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe;
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi;
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe;
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi;
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe;
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi;
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReLe;
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReRi;
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe;
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi;
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReLe = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReLe;
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReRi = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReRi;
    Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe = Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe;
    Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi = Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi;
    Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe = Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe;
    Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi = Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi;
    Pct_5msCtrlFinalTarPInfo.ReqdPCtrlBoostMod = Arb_CtrlFinalTarPInfo.ReqdPCtrlBoostMod;
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe;
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi;
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReLe = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReLe;
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReRi = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReRi;

    Pct_5msCtrlWhlOutVlvCtrlTarInfo = Arb_CtrlWhlOutVlvCtrlTarInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlWhlOutVlvCtrlTarInfo 
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar;
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar;
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar;
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar;
     =============================================================================*/

    Pct_5msCtrlWhlPreFillInfo = Arb_CtrlWhlPreFillInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlWhlPreFillInfo 
     : Pct_5msCtrlWhlPreFillInfo.FlPreFillActFlg;
     : Pct_5msCtrlWhlPreFillInfo.FrPreFillActFlg;
     : Pct_5msCtrlWhlPreFillInfo.RlPreFillActFlg;
     : Pct_5msCtrlWhlPreFillInfo.RrPreFillActFlg;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK = Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK;
    Pct_5msCtrlIdbVlvActInfo.CutVlvOpenFlg = Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg;

    /* Decomposed structure interface */
    Pct_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;

    /* Decomposed structure interface */
    Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;

    /* Decomposed structure interface */
    Pct_5msCtrlPedlTrvlTagInfo.PedlSigTag = Det_5msCtrlPedlTrvlTagInfo.PedlSigTag;

    /* Decomposed structure interface */
    Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar = Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar;

    /* Decomposed structure interface */
    Pct_5msCtrlPistPRateInfo.PistPChgDurg10ms = Det_5msCtrlPistPRateInfo.PistPChgDurg10ms;

    Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo = Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo 
     : Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
     =============================================================================*/

    /* Decomposed structure interface */
    Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
    Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;

    /* Decomposed structure interface */
    Pct_5msCtrlEemSuspectData.Eem_Suspect_SimP = Eem_MainEemSuspectData.Eem_Suspect_SimP;

    /* Inter-Runnable single interface */
    Pct_5msCtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Pct_5msCtrlFuncInhibitPctrlSts = Eem_SuspcDetnFuncInhibitPctrlSts;
    Pct_5msCtrlBaseBrkCtrlrActFlg = Bbc_CtrlBaseBrkCtrlrActFlg;
    Pct_5msCtrlBlsFild = Spc_5msCtrlBlsFild;
    Pct_5msCtrlBrkPRednForBaseBrkCtrlr = Rbc_CtrlBrkPRednForBaseBrkCtrlr;
    Pct_5msCtrlRgnBrkCtlrBlendgFlg = Rbc_CtrlRgnBrkCtlrBlendgFlg;
    Pct_5msCtrlVehSpdFild = Det_5msCtrlVehSpdFild;

    /* Execute  runnable */
    if (Int_5ms_IdbMotOrgSetFlg == 1) Pct_5msCtrl();

    /* Structure interface */
    Task_5msBus.Pct_5msCtrlStkRecvryActnIfInfo = Pct_5msCtrlStkRecvryActnIfInfo;
    /*==============================================================================
    * Members of structure Pct_5msCtrlStkRecvryActnIfInfo 
     : Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl;
     : Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
     : Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Pct_5msCtrlTgtDeltaStkType = Pct_5msCtrlTgtDeltaStkType;
    Task_5msBus.Pct_5msCtrlPCtrlBoostMod = Pct_5msCtrlPCtrlBoostMod;
    Task_5msBus.Pct_5msCtrlPCtrlFadeoutSt = Pct_5msCtrlPCtrlFadeoutSt;
    Task_5msBus.Pct_5msCtrlPCtrlSt = Pct_5msCtrlPCtrlSt;
    Task_5msBus.Pct_5msCtrlInVlvAllCloseReq = Pct_5msCtrlInVlvAllCloseReq;
    Task_5msBus.Pct_5msCtrlPChamberVolume = Pct_5msCtrlPChamberVolume;
    Task_5msBus.Pct_5msCtrlTarDeltaStk = Pct_5msCtrlTarDeltaStk;
    Task_5msBus.Pct_5msCtrlFinalTarPFromPCtrl = Pct_5msCtrlFinalTarPFromPCtrl;

    /**********************************************************************************************
     Pct_5msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Vat_Ctrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    Vat_CtrlWhlOutVlvCtrlTarInfo = Arb_CtrlWhlOutVlvCtrlTarInfo;
    /*==============================================================================
    * Members of structure Vat_CtrlWhlOutVlvCtrlTarInfo 
     : Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar;
     : Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar;
     : Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar;
     : Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar;
     =============================================================================*/

    Vat_CtrlIdbPCtrllVlvCtrlStInfo = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo;
    /*==============================================================================
    * Members of structure Vat_CtrlIdbPCtrllVlvCtrlStInfo 
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime;
     =============================================================================*/

    Vat_CtrlIdbPedlFeelVlvCtrlStInfo = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo;
    /*==============================================================================
    * Members of structure Vat_CtrlIdbPedlFeelVlvCtrlStInfo 
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen;
     =============================================================================*/

    Vat_CtrlWhlnVlvCtrlTarInfo = Pct_5msCtrlWhlnVlvCtrlTarInfo;
    /*==============================================================================
    * Members of structure Vat_CtrlWhlnVlvCtrlTarInfo 
     : Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar;
     : Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar;
     : Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar;
     : Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar;
     =============================================================================*/

    /* Decomposed structure interface */
    Vat_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState = Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
    Vat_CtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;

    Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo = Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
    /*==============================================================================
    * Members of structure Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo 
     : Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
     =============================================================================*/

    /* Inter-Runnable single interface */
    Vat_CtrlEcuModeSts = Mom_HndlrEcuModeSts;
    Vat_CtrlPCtrlAct = Pct_5msCtrlPCtrlAct;
    Vat_CtrlFuncInhibitVlvActSts = Eem_SuspcDetnFuncInhibitVlvActSts;
    Vat_CtrlPCtrlBoostMod = Pct_5msCtrlPCtrlBoostMod;

    /* Execute  runnable */
    if (Int_5ms_IdbMotOrgSetFlg == 1) Vat_Ctrl();

    /* Structure interface */
    Task_5msBus.Vat_CtrlIdbVlvActInfo = Vat_CtrlIdbVlvActInfo;
    /*==============================================================================
    * Members of structure Vat_CtrlIdbVlvActInfo 
     : Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK;
     : Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk;
     : Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Vat_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     SysPwrM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = Task_5msBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;

    /* Single interface */
    SysPwrM_MainVBatt1Mon = Task_5msBus.SysPwrM_MainVBatt1Mon;
    SysPwrM_MainVBatt2Mon = Task_5msBus.SysPwrM_MainVBatt2Mon;
    SysPwrM_MainDiagClrSrs = Task_5msBus.SysPwrM_MainDiagClrSrs;
    SysPwrM_MainVdd1Mon = Task_5msBus.SysPwrM_MainVdd1Mon;
    SysPwrM_MainVdd2Mon = Task_5msBus.SysPwrM_MainVdd2Mon;
    SysPwrM_MainVdd3Mon = Task_5msBus.SysPwrM_MainVdd3Mon;
    SysPwrM_MainVdd4Mon = Task_5msBus.SysPwrM_MainVdd4Mon;
    SysPwrM_MainVdd5Mon = Task_5msBus.SysPwrM_MainVdd5Mon;
    SysPwrM_MainVddMon = Task_5msBus.SysPwrM_MainVddMon;
    SysPwrM_MainMtrArbDriveState = Task_5msBus.SysPwrM_MainMtrArbDriveState;

    /* Inter-Runnable structure interface */
    SysPwrM_MainWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure SysPwrM_MainWhlSpdInfo 
     : SysPwrM_MainWhlSpdInfo.FlWhlSpd;
     : SysPwrM_MainWhlSpdInfo.FrWhlSpd;
     : SysPwrM_MainWhlSpdInfo.RlWhlSpd;
     : SysPwrM_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    SysPwrM_MainRxClu2Info = Proxy_RxByComRxClu2Info;
    /*==============================================================================
    * Members of structure SysPwrM_MainRxClu2Info 
     : SysPwrM_MainRxClu2Info.IGN_SW;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //SysPwrM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    SysPwrM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    SysPwrM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;
    SysPwrM_MainArbVlvDriveState = Arbitrator_VlvArbVlvDriveState;

    /* Execute  runnable */
    SysPwrM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     SysPwrM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     EcuHwCtrlM_Main start
     **********************************************************************************************/

     /* Structure interface */
    EcuHwCtrlM_MainAchSysPwrAsicInfo = Task_5msBus.EcuHwCtrlM_MainAchSysPwrAsicInfo;
    /*==============================================================================
    * Members of structure EcuHwCtrlM_MainAchSysPwrAsicInfo 
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
     =============================================================================*/

    /* Decomposed structure interface */
    EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV = Task_5msBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV;
    EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV = Task_5msBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV;
    EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = Task_5msBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT;

    /* Single interface */
    EcuHwCtrlM_MainAchAsicInvalid = Task_5msBus.EcuHwCtrlM_MainAchAsicInvalid;
    EcuHwCtrlM_MainVBatt1Mon = Task_5msBus.EcuHwCtrlM_MainVBatt1Mon;
    EcuHwCtrlM_MainDiagClrSrs = Task_5msBus.EcuHwCtrlM_MainDiagClrSrs;
    EcuHwCtrlM_MainAcmAsicInitCompleteFlag = Task_5msBus.EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
    EcuHwCtrlM_MainVdd1Mon = Task_5msBus.EcuHwCtrlM_MainVdd1Mon;
    EcuHwCtrlM_MainVdd2Mon = Task_5msBus.EcuHwCtrlM_MainVdd2Mon;
    EcuHwCtrlM_MainVdd3Mon = Task_5msBus.EcuHwCtrlM_MainVdd3Mon;
    EcuHwCtrlM_MainVdd4Mon = Task_5msBus.EcuHwCtrlM_MainVdd4Mon;
    EcuHwCtrlM_MainVdd5Mon = Task_5msBus.EcuHwCtrlM_MainVdd5Mon;
    EcuHwCtrlM_MainAdcInvalid = Task_5msBus.EcuHwCtrlM_MainAdcInvalid;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //EcuHwCtrlM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    EcuHwCtrlM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    EcuHwCtrlM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    EcuHwCtrlM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     EcuHwCtrlM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     WssM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = Task_5msBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;

    WssM_MainAchWssPort0AsicInfo = Task_5msBus.WssM_MainAchWssPort0AsicInfo;
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort0AsicInfo 
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    WssM_MainAchWssPort1AsicInfo = Task_5msBus.WssM_MainAchWssPort1AsicInfo;
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort1AsicInfo 
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    WssM_MainAchWssPort2AsicInfo = Task_5msBus.WssM_MainAchWssPort2AsicInfo;
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort2AsicInfo 
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    WssM_MainAchWssPort3AsicInfo = Task_5msBus.WssM_MainAchWssPort3AsicInfo;
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort3AsicInfo 
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    /* Single interface */
    WssM_MainAchAsicInvalid = Task_5msBus.WssM_MainAchAsicInvalid;
    WssM_MainVBatt1Mon = Task_5msBus.WssM_MainVBatt1Mon;
    WssM_MainDiagClrSrs = Task_5msBus.WssM_MainDiagClrSrs;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    WssM_MainWssSpeedOut.WssMin = Wss_SenWssSpeedOut.WssMin;

    /* Inter-Runnable single interface */
    //WssM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    WssM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    WssM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    WssM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     WssM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     PressM_Main start
     **********************************************************************************************/

     /* Structure interface */
    PressM_MainHalPressureInfo = Task_5msBus.PressM_MainHalPressureInfo;
    /*==============================================================================
    * Members of structure PressM_MainHalPressureInfo 
     : PressM_MainHalPressureInfo.McpPresData1;
     : PressM_MainHalPressureInfo.McpPresData2;
     : PressM_MainHalPressureInfo.McpSentCrc;
     : PressM_MainHalPressureInfo.McpSentData;
     : PressM_MainHalPressureInfo.McpSentMsgId;
     : PressM_MainHalPressureInfo.McpSentSerialCrc;
     : PressM_MainHalPressureInfo.McpSentConfig;
     : PressM_MainHalPressureInfo.Wlp1PresData1;
     : PressM_MainHalPressureInfo.Wlp1PresData2;
     : PressM_MainHalPressureInfo.Wlp1SentCrc;
     : PressM_MainHalPressureInfo.Wlp1SentData;
     : PressM_MainHalPressureInfo.Wlp1SentMsgId;
     : PressM_MainHalPressureInfo.Wlp1SentSerialCrc;
     : PressM_MainHalPressureInfo.Wlp1SentConfig;
     : PressM_MainHalPressureInfo.Wlp2PresData1;
     : PressM_MainHalPressureInfo.Wlp2PresData2;
     : PressM_MainHalPressureInfo.Wlp2SentCrc;
     : PressM_MainHalPressureInfo.Wlp2SentData;
     : PressM_MainHalPressureInfo.Wlp2SentMsgId;
     : PressM_MainHalPressureInfo.Wlp2SentSerialCrc;
     : PressM_MainHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/

    /* Decomposed structure interface */
    PressM_MainSentHPressureInfo.McpSentInvalid = Task_5msBus.PressM_MainSentHPressureInfo.McpSentInvalid;
    PressM_MainSentHPressureInfo.MCPSentSerialInvalid = Task_5msBus.PressM_MainSentHPressureInfo.MCPSentSerialInvalid;
    PressM_MainSentHPressureInfo.Wlp1SentInvalid = Task_5msBus.PressM_MainSentHPressureInfo.Wlp1SentInvalid;
    PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid = Task_5msBus.PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid;
    PressM_MainSentHPressureInfo.Wlp2SentInvalid = Task_5msBus.PressM_MainSentHPressureInfo.Wlp2SentInvalid;
    PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid = Task_5msBus.PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid;

    /* Single interface */
    PressM_MainVBatt1Mon = Task_5msBus.PressM_MainVBatt1Mon;
    PressM_MainDiagClrSrs = Task_5msBus.PressM_MainDiagClrSrs;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //PressM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    PressM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    PressM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    PressM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     PressM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     PedalM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = Task_5msBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
    PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = Task_5msBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
    PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = Task_5msBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;

    PedalM_MainPedlSigMonInfo = Task_5msBus.PedalM_MainPedlSigMonInfo;
    /*==============================================================================
    * Members of structure PedalM_MainPedlSigMonInfo 
     : PedalM_MainPedlSigMonInfo.PdfSigMon;
     : PedalM_MainPedlSigMonInfo.PdtSigMon;
     =============================================================================*/

    PedalM_MainPedlPwrMonInfo = Task_5msBus.PedalM_MainPedlPwrMonInfo;
    /*==============================================================================
    * Members of structure PedalM_MainPedlPwrMonInfo 
     : PedalM_MainPedlPwrMonInfo.Pdt5vMon;
     : PedalM_MainPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/

    /* Single interface */
    PedalM_MainAchAsicInvalid = Task_5msBus.PedalM_MainAchAsicInvalid;
    PedalM_MainDiagClrSrs = Task_5msBus.PedalM_MainDiagClrSrs;
    PedalM_MainVdd3Mon = Task_5msBus.PedalM_MainVdd3Mon;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //PedalM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    PedalM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    PedalM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    PedalM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     PedalM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     WssP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    WssP_MainDiagClrSrs = Task_5msBus.WssP_MainDiagClrSrs;

    /* Inter-Runnable structure interface */
    WssP_MainWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure WssP_MainWhlSpdInfo 
     : WssP_MainWhlSpdInfo.FlWhlSpd;
     : WssP_MainWhlSpdInfo.FrWhlSpd;
     : WssP_MainWhlSpdInfo.RlWhlSpd;
     : WssP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    WssP_MainAbsCtrlInfo.AbsActFlg = Abc_CtrlAbsCtrlInfo.AbsActFlg;

    /* Decomposed structure interface */
    //WssP_MainWssMonData.WssM_Inhibit_FL = WssM_MainWssMonData.WssM_Inhibit_FL;
    //WssP_MainWssMonData.WssM_Inhibit_FR = WssM_MainWssMonData.WssM_Inhibit_FR;
    //WssP_MainWssMonData.WssM_Inhibit_RL = WssM_MainWssMonData.WssM_Inhibit_RL;
    //WssP_MainWssMonData.WssM_Inhibit_RR = WssM_MainWssMonData.WssM_Inhibit_RR;

    /* Inter-Runnable single interface */
    //WssP_MainEcuModeSts = Mom_HndlrEcuModeSts;
    WssP_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    WssP_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    WssP_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     WssP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     PressP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    PressP_MainPressCalcInfo.PressP_SimP_1_100_bar = Task_5msBus.PressP_MainPressCalcInfo.PressP_SimP_1_100_bar;
    PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar = Task_5msBus.PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar;
    PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar = Task_5msBus.PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar;

    /* Single interface */
    PressP_MainVBatt1Mon = Task_5msBus.PressP_MainVBatt1Mon;
    PressP_MainDiagClrSrs = Task_5msBus.PressP_MainDiagClrSrs;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    PressP_MainPdf5msRawInfo.MoveAvrPdfSig = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig;
    PressP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
    PressP_MainPdf5msRawInfo.MoveAvrPdtSig = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig;
    PressP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;

    PressP_MainPressSenCalcInfo = Press_SenSyncPressSenCalcInfo;
    /*==============================================================================
    * Members of structure PressP_MainPressSenCalcInfo 
     : PressP_MainPressSenCalcInfo.PressP_SimPMoveAve;
     : PressP_MainPressSenCalcInfo.PressP_CirP1MoveAve;
     : PressP_MainPressSenCalcInfo.PressP_CirP2MoveAve;
     : PressP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs;
     : PressP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs;
     : PressP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //PressP_MainEcuModeSts = Mom_HndlrEcuModeSts;
    PressP_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    PressP_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    PressP_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     PressP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     PedalP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    PedalP_MainDiagClrSrs = Task_5msBus.PedalP_MainDiagClrSrs;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    PedalP_MainCanRxAccelPedlInfo.AccelPedlVal = Proxy_RxCanRxAccelPedlInfo.AccelPedlVal;

    /* Decomposed structure interface */
    PedalP_MainPdf5msRawInfo.MoveAvrPdfSig = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig;
    PedalP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
    PedalP_MainPdf5msRawInfo.MoveAvrPdtSig = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig;
    PedalP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;

    PedalP_MainPressSenCalcInfo = Press_SenSyncPressSenCalcInfo;
    /*==============================================================================
    * Members of structure PedalP_MainPressSenCalcInfo 
     : PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve;
     : PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve;
     : PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve;
     : PedalP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs;
     : PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs;
     : PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs;
     =============================================================================*/

    /* Decomposed structure interface */
    PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb = PedalM_MainPedalMData.PedalM_PTS1_Check_Ihb;
    PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb = PedalM_MainPedalMData.PedalM_PTS2_Check_Ihb;

    /* Decomposed structure interface */
    PedalP_MainWssPlauData.WssP_min_speed = WssP_MainWssPlauData.WssP_min_speed;

    /* Inter-Runnable single interface */
    //PedalP_MainEcuModeSts = Mom_HndlrEcuModeSts;
    PedalP_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    PedalP_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;
    PedalP_MainBlsSwt = Swt_SenBlsSwt;

    /* Execute  runnable */
    PedalP_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     PedalP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     BbsVlvM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = Task_5msBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;

    /* Decomposed structure interface */
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV;
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = Task_5msBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT;

    BbsVlvM_MainVlvdFF0DcdInfo = Task_5msBus.BbsVlvM_MainVlvdFF0DcdInfo;
    /*==============================================================================
    * Members of structure BbsVlvM_MainVlvdFF0DcdInfo 
     : BbsVlvM_MainVlvdFF0DcdInfo.C0ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C0sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C0sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.Fr;
     : BbsVlvM_MainVlvdFF0DcdInfo.Ot;
     : BbsVlvM_MainVlvdFF0DcdInfo.Lr;
     : BbsVlvM_MainVlvdFF0DcdInfo.Uv;
     : BbsVlvM_MainVlvdFF0DcdInfo.Ff;
     =============================================================================*/

    BbsVlvM_MainVlvdFF1DcdInfo = Task_5msBus.BbsVlvM_MainVlvdFF1DcdInfo;
    /*==============================================================================
    * Members of structure BbsVlvM_MainVlvdFF1DcdInfo 
     : BbsVlvM_MainVlvdFF1DcdInfo.C3ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C3sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C3sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.Fr;
     : BbsVlvM_MainVlvdFF1DcdInfo.Ot;
     : BbsVlvM_MainVlvdFF1DcdInfo.Lr;
     : BbsVlvM_MainVlvdFF1DcdInfo.Uv;
     : BbsVlvM_MainVlvdFF1DcdInfo.Ff;
     =============================================================================*/

    /* Single interface */
    BbsVlvM_MainAchAsicInvalid = Task_5msBus.BbsVlvM_MainAchAsicInvalid;
    BbsVlvM_MainVBatt1Mon = Task_5msBus.BbsVlvM_MainVBatt1Mon;
    BbsVlvM_MainVBatt2Mon = Task_5msBus.BbsVlvM_MainVBatt2Mon;
    BbsVlvM_MainDiagClrSrs = Task_5msBus.BbsVlvM_MainDiagClrSrs;
    BbsVlvM_MainFspCbsHMon = Task_5msBus.BbsVlvM_MainFspCbsHMon;
    BbsVlvM_MainFspCbsMon = Task_5msBus.BbsVlvM_MainFspCbsMon;
    BbsVlvM_MainVlvdInvalid = Task_5msBus.BbsVlvM_MainVlvdInvalid;

    /* Inter-Runnable structure interface */
    BbsVlvM_MainArbFsrBbsDrvInfo = Arbitrator_VlvArbFsrBbsDrvInfo;
    /*==============================================================================
    * Members of structure BbsVlvM_MainArbFsrBbsDrvInfo 
     : BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv;
     : BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/

    /* Decomposed structure interface */
    for(i=0; i<5; i++) BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[i] = Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i];
    for(i=0; i<5; i++) BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[i] = Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i];
    for(i=0; i<5; i++) BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[i] = Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i];
    for(i=0; i<5; i++) BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[i] = Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i];
    for(i=0; i<5; i++) BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[i] = Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i];

    /* Inter-Runnable single interface */
    //BbsVlvM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    BbsVlvM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    BbsVlvM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;
    BbsVlvM_MainAsicEnDrDrv = Arbitrator_VlvAsicEnDrDrv;
    BbsVlvM_MainArbVlvDriveState = Arbitrator_VlvArbVlvDriveState;

    /* Execute  runnable */
    BbsVlvM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     BbsVlvM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     AbsVlvM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = Task_5msBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;

    AbsVlvM_MainAchValveAsicInfo = Task_5msBus.AbsVlvM_MainAchValveAsicInfo;
    /*==============================================================================
    * Members of structure AbsVlvM_MainAchValveAsicInfo 
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
     =============================================================================*/

    /* Single interface */
    AbsVlvM_MainAchAsicInvalid = Task_5msBus.AbsVlvM_MainAchAsicInvalid;
    AbsVlvM_MainVBatt1Mon = Task_5msBus.AbsVlvM_MainVBatt1Mon;
    AbsVlvM_MainVBatt2Mon = Task_5msBus.AbsVlvM_MainVBatt2Mon;
    AbsVlvM_MainDiagClrSrs = Task_5msBus.AbsVlvM_MainDiagClrSrs;
    AbsVlvM_MainFspAbsHMon = Task_5msBus.AbsVlvM_MainFspAbsHMon;
    AbsVlvM_MainFspAbsMon = Task_5msBus.AbsVlvM_MainFspAbsMon;

    /* Inter-Runnable structure interface */
    AbsVlvM_MainArbFsrAbsDrvInfo = Arbitrator_VlvArbFsrAbsDrvInfo;
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbFsrAbsDrvInfo 
     : AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv;
     : AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/

    AbsVlvM_MainArbWhlVlvReqInfo = Arbitrator_VlvArbWhlVlvReqInfo;
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbWhlVlvReqInfo 
     : AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    /* Decomposed structure interface */
    for(i=0; i<5; i++) AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[i] = Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i];

    AbsVlvM_MainArbBalVlvReqInfo = Arbitrator_VlvArbBalVlvReqInfo;
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbBalVlvReqInfo 
     : AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/

    AbsVlvM_MainArbResPVlvReqInfo = Arbitrator_VlvArbResPVlvReqInfo;
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbResPVlvReqInfo 
     : AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //AbsVlvM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    AbsVlvM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    AbsVlvM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;
    AbsVlvM_MainAsicEnDrDrv = Arbitrator_VlvAsicEnDrDrv;
    AbsVlvM_MainArbVlvDriveState = Arbitrator_VlvArbVlvDriveState;

    /* Execute  runnable */
    AbsVlvM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     AbsVlvM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Mps_TLE5012M_Main start
     **********************************************************************************************/

     /* Structure interface */
    Mps_TLE5012M_MainMpsD1SpiDcdInfo = Task_5msBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo;
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD1SpiDcdInfo 
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV;
     =============================================================================*/

    Mps_TLE5012M_MainMpsD2SpiDcdInfo = Task_5msBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo;
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD2SpiDcdInfo 
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV;
     =============================================================================*/

    /* Single interface */
    Mps_TLE5012M_MainVBatt1Mon = Task_5msBus.Mps_TLE5012M_MainVBatt1Mon;
    Mps_TLE5012M_MainDiagClrSrs = Task_5msBus.Mps_TLE5012M_MainDiagClrSrs;
    Mps_TLE5012M_MainMpsInvalid = Task_5msBus.Mps_TLE5012M_MainMpsInvalid;
    Mps_TLE5012M_MainMtrArbDriveState = Task_5msBus.Mps_TLE5012M_MainMtrArbDriveState;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Mps_TLE5012M_MainEcuModeSts = Mom_HndlrEcuModeSts;
    Mps_TLE5012M_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Mps_TLE5012M_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    Mps_TLE5012M_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Mps_TLE5012M_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     MgdM_Main start
     **********************************************************************************************/

     /* Structure interface */
    MgdM_MainMgdDcdInfo = Task_5msBus.MgdM_MainMgdDcdInfo;
    /*==============================================================================
    * Members of structure MgdM_MainMgdDcdInfo 
     : MgdM_MainMgdDcdInfo.mgdIdleM;
     : MgdM_MainMgdDcdInfo.mgdConfM;
     : MgdM_MainMgdDcdInfo.mgdConfLock;
     : MgdM_MainMgdDcdInfo.mgdSelfTestM;
     : MgdM_MainMgdDcdInfo.mgdSoffM;
     : MgdM_MainMgdDcdInfo.mgdErrM;
     : MgdM_MainMgdDcdInfo.mgdRectM;
     : MgdM_MainMgdDcdInfo.mgdNormM;
     : MgdM_MainMgdDcdInfo.mgdOsf;
     : MgdM_MainMgdDcdInfo.mgdOp;
     : MgdM_MainMgdDcdInfo.mgdScd;
     : MgdM_MainMgdDcdInfo.mgdSd;
     : MgdM_MainMgdDcdInfo.mgdIndiag;
     : MgdM_MainMgdDcdInfo.mgdOutp;
     : MgdM_MainMgdDcdInfo.mgdExt;
     : MgdM_MainMgdDcdInfo.mgdInt12;
     : MgdM_MainMgdDcdInfo.mgdRom;
     : MgdM_MainMgdDcdInfo.mgdLimpOn;
     : MgdM_MainMgdDcdInfo.mgdStIncomplete;
     : MgdM_MainMgdDcdInfo.mgdApcAct;
     : MgdM_MainMgdDcdInfo.mgdGtm;
     : MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid;
     : MgdM_MainMgdDcdInfo.mgdLfw;
     : MgdM_MainMgdDcdInfo.mgdErrOtW;
     : MgdM_MainMgdDcdInfo.mgdErrOvReg1;
     : MgdM_MainMgdDcdInfo.mgdErrUvVccRom;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg4;
     : MgdM_MainMgdDcdInfo.mgdErrOvReg6;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg6;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg5;
     : MgdM_MainMgdDcdInfo.mgdErrUvCb;
     : MgdM_MainMgdDcdInfo.mgdErrClkTrim;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs3;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs2;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs1;
     : MgdM_MainMgdDcdInfo.mgdErrCp2;
     : MgdM_MainMgdDcdInfo.mgdErrCp1;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs3;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs2;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs1;
     : MgdM_MainMgdDcdInfo.mgdErrOvVdh;
     : MgdM_MainMgdDcdInfo.mgdErrUvVdh;
     : MgdM_MainMgdDcdInfo.mgdErrOvVs;
     : MgdM_MainMgdDcdInfo.mgdErrUvVs;
     : MgdM_MainMgdDcdInfo.mgdErrUvVcc;
     : MgdM_MainMgdDcdInfo.mgdErrOvVcc;
     : MgdM_MainMgdDcdInfo.mgdErrOvLdVdh;
     : MgdM_MainMgdDcdInfo.mgdSdDdpStuck;
     : MgdM_MainMgdDcdInfo.mgdSdCp1;
     : MgdM_MainMgdDcdInfo.mgdSdOvCp;
     : MgdM_MainMgdDcdInfo.mgdSdClkfail;
     : MgdM_MainMgdDcdInfo.mgdSdUvCb;
     : MgdM_MainMgdDcdInfo.mgdSdOvVdh;
     : MgdM_MainMgdDcdInfo.mgdSdOvVs;
     : MgdM_MainMgdDcdInfo.mgdSdOt;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs3;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs2;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs1;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs3;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs2;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs1;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs3;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs2;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs1;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs3;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs2;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs2;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs3;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs2;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs3;
     : MgdM_MainMgdDcdInfo.mgdErrSpiFrame;
     : MgdM_MainMgdDcdInfo.mgdErrSpiTo;
     : MgdM_MainMgdDcdInfo.mgdErrSpiWd;
     : MgdM_MainMgdDcdInfo.mgdErrSpiCrc;
     : MgdM_MainMgdDcdInfo.mgdEpiAddInvalid;
     : MgdM_MainMgdDcdInfo.mgdEonfTo;
     : MgdM_MainMgdDcdInfo.mgdEonfSigInvalid;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp1;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp2;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp3;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOutpErrn;
     : MgdM_MainMgdDcdInfo.mgdErrOutpMiso;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB1;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB2;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB3;
     : MgdM_MainMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/

    /* Single interface */
    MgdM_MainVBatt1Mon = Task_5msBus.MgdM_MainVBatt1Mon;
    MgdM_MainDiagClrSrs = Task_5msBus.MgdM_MainDiagClrSrs;
    MgdM_MainMgdInvalid = Task_5msBus.MgdM_MainMgdInvalid;
    MgdM_MainMtrArbDriveState = Task_5msBus.MgdM_MainMtrArbDriveState;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //MgdM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    MgdM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    MgdM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    MgdM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     MgdM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     MtrM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    MtrM_MainMotMonInfo.MotPwrVoltMon = Task_5msBus.MtrM_MainMotMonInfo.MotPwrVoltMon;

    MtrM_MainMotVoltsMonInfo = Task_5msBus.MtrM_MainMotVoltsMonInfo;
    /*==============================================================================
    * Members of structure MtrM_MainMotVoltsMonInfo 
     : MtrM_MainMotVoltsMonInfo.MotVoltPhUMon;
     : MtrM_MainMotVoltsMonInfo.MotVoltPhVMon;
     : MtrM_MainMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/

    /* Decomposed structure interface */
    MtrM_MainMpsD1SpiDcdInfo.S_MAGOL = Task_5msBus.MtrM_MainMpsD1SpiDcdInfo.S_MAGOL;

    /* Decomposed structure interface */
    MtrM_MainMpsD2SpiDcdInfo.S_MAGOL = Task_5msBus.MtrM_MainMpsD2SpiDcdInfo.S_MAGOL;

    MtrM_MainMgdDcdInfo = Task_5msBus.MtrM_MainMgdDcdInfo;
    /*==============================================================================
    * Members of structure MtrM_MainMgdDcdInfo 
     : MtrM_MainMgdDcdInfo.mgdIdleM;
     : MtrM_MainMgdDcdInfo.mgdConfM;
     : MtrM_MainMgdDcdInfo.mgdConfLock;
     : MtrM_MainMgdDcdInfo.mgdSelfTestM;
     : MtrM_MainMgdDcdInfo.mgdSoffM;
     : MtrM_MainMgdDcdInfo.mgdErrM;
     : MtrM_MainMgdDcdInfo.mgdRectM;
     : MtrM_MainMgdDcdInfo.mgdNormM;
     : MtrM_MainMgdDcdInfo.mgdOsf;
     : MtrM_MainMgdDcdInfo.mgdOp;
     : MtrM_MainMgdDcdInfo.mgdScd;
     : MtrM_MainMgdDcdInfo.mgdSd;
     : MtrM_MainMgdDcdInfo.mgdIndiag;
     : MtrM_MainMgdDcdInfo.mgdOutp;
     : MtrM_MainMgdDcdInfo.mgdExt;
     : MtrM_MainMgdDcdInfo.mgdInt12;
     : MtrM_MainMgdDcdInfo.mgdRom;
     : MtrM_MainMgdDcdInfo.mgdLimpOn;
     : MtrM_MainMgdDcdInfo.mgdStIncomplete;
     : MtrM_MainMgdDcdInfo.mgdApcAct;
     : MtrM_MainMgdDcdInfo.mgdGtm;
     : MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid;
     : MtrM_MainMgdDcdInfo.mgdLfw;
     : MtrM_MainMgdDcdInfo.mgdErrOtW;
     : MtrM_MainMgdDcdInfo.mgdErrOvReg1;
     : MtrM_MainMgdDcdInfo.mgdErrUvVccRom;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg4;
     : MtrM_MainMgdDcdInfo.mgdErrOvReg6;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg6;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg5;
     : MtrM_MainMgdDcdInfo.mgdErrUvCb;
     : MtrM_MainMgdDcdInfo.mgdErrClkTrim;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs3;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs2;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs1;
     : MtrM_MainMgdDcdInfo.mgdErrCp2;
     : MtrM_MainMgdDcdInfo.mgdErrCp1;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs3;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs2;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs1;
     : MtrM_MainMgdDcdInfo.mgdErrOvVdh;
     : MtrM_MainMgdDcdInfo.mgdErrUvVdh;
     : MtrM_MainMgdDcdInfo.mgdErrOvVs;
     : MtrM_MainMgdDcdInfo.mgdErrUvVs;
     : MtrM_MainMgdDcdInfo.mgdErrUvVcc;
     : MtrM_MainMgdDcdInfo.mgdErrOvVcc;
     : MtrM_MainMgdDcdInfo.mgdErrOvLdVdh;
     : MtrM_MainMgdDcdInfo.mgdSdDdpStuck;
     : MtrM_MainMgdDcdInfo.mgdSdCp1;
     : MtrM_MainMgdDcdInfo.mgdSdOvCp;
     : MtrM_MainMgdDcdInfo.mgdSdClkfail;
     : MtrM_MainMgdDcdInfo.mgdSdUvCb;
     : MtrM_MainMgdDcdInfo.mgdSdOvVdh;
     : MtrM_MainMgdDcdInfo.mgdSdOvVs;
     : MtrM_MainMgdDcdInfo.mgdSdOt;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs3;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs2;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs1;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs3;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs2;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs1;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs3;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs2;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs1;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs3;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs2;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs2;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs3;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs2;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs3;
     : MtrM_MainMgdDcdInfo.mgdErrSpiFrame;
     : MtrM_MainMgdDcdInfo.mgdErrSpiTo;
     : MtrM_MainMgdDcdInfo.mgdErrSpiWd;
     : MtrM_MainMgdDcdInfo.mgdErrSpiCrc;
     : MtrM_MainMgdDcdInfo.mgdEpiAddInvalid;
     : MtrM_MainMgdDcdInfo.mgdEonfTo;
     : MtrM_MainMgdDcdInfo.mgdEonfSigInvalid;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp1;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp2;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp3;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOutpErrn;
     : MtrM_MainMgdDcdInfo.mgdErrOutpMiso;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB1;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB2;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB3;
     : MtrM_MainMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/

    MtrM_MainMotDqIRefMccInfo = Task_5msBus.MtrM_MainMotDqIRefMccInfo;
    /*==============================================================================
    * Members of structure MtrM_MainMotDqIRefMccInfo 
     : MtrM_MainMotDqIRefMccInfo.IdRef;
     : MtrM_MainMotDqIRefMccInfo.IqRef;
     =============================================================================*/

    MtrM_MainHwTrigMotInfo = Task_5msBus.MtrM_MainHwTrigMotInfo;
    /*==============================================================================
    * Members of structure MtrM_MainHwTrigMotInfo 
     : MtrM_MainHwTrigMotInfo.Uphase0Mon;
     : MtrM_MainHwTrigMotInfo.Uphase1Mon;
     : MtrM_MainHwTrigMotInfo.Vphase0Mon;
     : MtrM_MainHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/

    MtrM_MainMotCurrMonInfo = Task_5msBus.MtrM_MainMotCurrMonInfo;
    /*==============================================================================
    * Members of structure MtrM_MainMotCurrMonInfo 
     : MtrM_MainMotCurrMonInfo.MotCurrPhUMon0;
     : MtrM_MainMotCurrMonInfo.MotCurrPhUMon1;
     : MtrM_MainMotCurrMonInfo.MotCurrPhVMon0;
     : MtrM_MainMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/

    /* Decomposed structure interface */
    MtrM_MainMotAngleMonInfo.MotPosiAngle1deg = Task_5msBus.MtrM_MainMotAngleMonInfo.MotPosiAngle1deg;
    MtrM_MainMotAngleMonInfo.MotPosiAngle2deg = Task_5msBus.MtrM_MainMotAngleMonInfo.MotPosiAngle2deg;

    /* Decomposed structure interface */
    MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd = Task_5msBus.MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd;
    MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd = Task_5msBus.MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd;
    MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd = Task_5msBus.MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd;
    MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg = Task_5msBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
    MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg = Task_5msBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg;

    /* Single interface */
    MtrM_MainVBatt1Mon = Task_5msBus.MtrM_MainVBatt1Mon;
    MtrM_MainVBatt2Mon = Task_5msBus.MtrM_MainVBatt2Mon;
    MtrM_MainDiagClrSrs = Task_5msBus.MtrM_MainDiagClrSrs;
    MtrM_MainMgdInvalid = Task_5msBus.MtrM_MainMgdInvalid;
    MtrM_MainMtrArbDriveState = Task_5msBus.MtrM_MainMtrArbDriveState;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //MtrM_MainEcuModeSts = Mom_HndlrEcuModeSts;
    MtrM_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    MtrM_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    MtrM_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     MtrM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Watchdog_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Watchdog_MainDiagClrSrs = Task_5msBus.Watchdog_MainDiagClrSrs;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Watchdog_MainEcuModeSts = Mom_HndlrEcuModeSts;
    Watchdog_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Watchdog_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    Watchdog_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Watchdog_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Eem_Main start
     **********************************************************************************************/

     /* Structure interface */
    Eem_MainRelayMonitorData = Task_5msBus.Eem_MainRelayMonitorData;
    /*==============================================================================
    * Members of structure Eem_MainRelayMonitorData 
     : Eem_MainRelayMonitorData.RlyM_HDCRelay_Open_Err;
     : Eem_MainRelayMonitorData.RlyM_HDCRelay_Short_Err;
     : Eem_MainRelayMonitorData.RlyM_ESSRelay_Open_Err;
     : Eem_MainRelayMonitorData.RlyM_ESSRelay_Short_Err;
     =============================================================================*/

    /* Decomposed structure interface */
    Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable = Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    Eem_MainSenPwrMonitorData.SenPwrM_5V_Err = Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Err;
    Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err;
    Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = Task_5msBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err;

    Eem_MainSwtMonFaultInfo = Task_5msBus.Eem_MainSwtMonFaultInfo;
    /*==============================================================================
    * Members of structure Eem_MainSwtMonFaultInfo 
     : Eem_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_HDC_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_AVH_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_BFL_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_PB_Sw_Err;
     =============================================================================*/

    Eem_MainSwtPFaultInfo = Task_5msBus.Eem_MainSwtPFaultInfo;
    /*==============================================================================
    * Members of structure Eem_MainSwtPFaultInfo 
     : Eem_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
     =============================================================================*/

    Eem_MainYAWMOutInfo = Task_5msBus.Eem_MainYAWMOutInfo;
    /*==============================================================================
    * Members of structure Eem_MainYAWMOutInfo 
     : Eem_MainYAWMOutInfo.YAWM_Timeout_Err;
     : Eem_MainYAWMOutInfo.YAWM_Invalid_Err;
     : Eem_MainYAWMOutInfo.YAWM_CRC_Err;
     : Eem_MainYAWMOutInfo.YAWM_Rolling_Err;
     : Eem_MainYAWMOutInfo.YAWM_Temperature_Err;
     : Eem_MainYAWMOutInfo.YAWM_Range_Err;
     =============================================================================*/

    Eem_MainAYMOuitInfo = Task_5msBus.Eem_MainAYMOuitInfo;
    /*==============================================================================
    * Members of structure Eem_MainAYMOuitInfo 
     : Eem_MainAYMOuitInfo.AYM_Timeout_Err;
     : Eem_MainAYMOuitInfo.AYM_Invalid_Err;
     : Eem_MainAYMOuitInfo.AYM_CRC_Err;
     : Eem_MainAYMOuitInfo.AYM_Rolling_Err;
     : Eem_MainAYMOuitInfo.AYM_Temperature_Err;
     : Eem_MainAYMOuitInfo.AYM_Range_Err;
     =============================================================================*/

    Eem_MainAXMOutInfo = Task_5msBus.Eem_MainAXMOutInfo;
    /*==============================================================================
    * Members of structure Eem_MainAXMOutInfo 
     : Eem_MainAXMOutInfo.AXM_Timeout_Err;
     : Eem_MainAXMOutInfo.AXM_Invalid_Err;
     : Eem_MainAXMOutInfo.AXM_CRC_Err;
     : Eem_MainAXMOutInfo.AXM_Rolling_Err;
     : Eem_MainAXMOutInfo.AXM_Temperature_Err;
     : Eem_MainAXMOutInfo.AXM_Range_Err;
     =============================================================================*/

    Eem_MainSASMOutInfo = Task_5msBus.Eem_MainSASMOutInfo;
    /*==============================================================================
    * Members of structure Eem_MainSASMOutInfo 
     : Eem_MainSASMOutInfo.SASM_Timeout_Err;
     : Eem_MainSASMOutInfo.SASM_Invalid_Err;
     : Eem_MainSASMOutInfo.SASM_CRC_Err;
     : Eem_MainSASMOutInfo.SASM_Rolling_Err;
     : Eem_MainSASMOutInfo.SASM_Range_Err;
     =============================================================================*/

    Eem_MainCanMonData = Task_5msBus.Eem_MainCanMonData;
    /*==============================================================================
    * Members of structure Eem_MainCanMonData 
     : Eem_MainCanMonData.CanM_MainBusOff_Err;
     : Eem_MainCanMonData.CanM_SubBusOff_Err;
     : Eem_MainCanMonData.CanM_MainOverRun_Err;
     : Eem_MainCanMonData.CanM_SubOverRun_Err;
     : Eem_MainCanMonData.CanM_EMS1_Tout_Err;
     : Eem_MainCanMonData.CanM_EMS2_Tout_Err;
     : Eem_MainCanMonData.CanM_TCU1_Tout_Err;
     : Eem_MainCanMonData.CanM_TCU5_Tout_Err;
     : Eem_MainCanMonData.CanM_FWD1_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU1_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU2_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU3_Tout_Err;
     : Eem_MainCanMonData.CanM_VSM2_Tout_Err;
     : Eem_MainCanMonData.CanM_EMS_Invalid_Err;
     : Eem_MainCanMonData.CanM_TCU_Invalid_Err;
     =============================================================================*/

    Eem_MainYawPlauOutput = Task_5msBus.Eem_MainYawPlauOutput;
    /*==============================================================================
    * Members of structure Eem_MainYawPlauOutput 
     : Eem_MainYawPlauOutput.YawPlauModelErr;
     : Eem_MainYawPlauOutput.YawPlauNoisekErr;
     : Eem_MainYawPlauOutput.YawPlauShockErr;
     : Eem_MainYawPlauOutput.YawPlauRangeErr;
     : Eem_MainYawPlauOutput.YawPlauStandStillErr;
     =============================================================================*/

    Eem_MainAxPlauOutput = Task_5msBus.Eem_MainAxPlauOutput;
    /*==============================================================================
    * Members of structure Eem_MainAxPlauOutput 
     : Eem_MainAxPlauOutput.AxPlauOffsetErr;
     : Eem_MainAxPlauOutput.AxPlauDrivingOffsetErr;
     : Eem_MainAxPlauOutput.AxPlauStickErr;
     : Eem_MainAxPlauOutput.AxPlauNoiseErr;
     =============================================================================*/

    Eem_MainAyPlauOutput = Task_5msBus.Eem_MainAyPlauOutput;
    /*==============================================================================
    * Members of structure Eem_MainAyPlauOutput 
     : Eem_MainAyPlauOutput.AyPlauNoiselErr;
     : Eem_MainAyPlauOutput.AyPlauModelErr;
     : Eem_MainAyPlauOutput.AyPlauShockErr;
     : Eem_MainAyPlauOutput.AyPlauRangeErr;
     : Eem_MainAyPlauOutput.AyPlauStandStillErr;
     =============================================================================*/

    Eem_MainSasPlauOutput = Task_5msBus.Eem_MainSasPlauOutput;
    /*==============================================================================
    * Members of structure Eem_MainSasPlauOutput 
     : Eem_MainSasPlauOutput.SasPlauModelErr;
     : Eem_MainSasPlauOutput.SasPlauOffsetErr;
     : Eem_MainSasPlauOutput.SasPlauStickErr;
     =============================================================================*/

    /* Single interface */
    Eem_MainDiagClrSrs = Task_5msBus.Eem_MainDiagClrSrs;
    Eem_MainDiagSci = Task_5msBus.Eem_MainDiagSci;
    Eem_MainDiagAhbSci = Task_5msBus.Eem_MainDiagAhbSci;

    /* Inter-Runnable structure interface */
    Eem_MainBBSVlvErrInfo = BbsVlvM_MainBBSVlvErrInfo;
    /*==============================================================================
    * Members of structure Eem_MainBBSVlvErrInfo 
     : Eem_MainBBSVlvErrInfo.Batt1Fuse_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err;
     =============================================================================*/

    Eem_MainAbsVlvMData = AbsVlvM_MainAbsVlvMData;
    /*==============================================================================
    * Members of structure Eem_MainAbsVlvMData 
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err;
     =============================================================================*/

    Eem_MainEscSwtStInfo = Swt_SenEscSwtStInfo;
    /*==============================================================================
    * Members of structure Eem_MainEscSwtStInfo 
     : Eem_MainEscSwtStInfo.EscDisabledBySwt;
     : Eem_MainEscSwtStInfo.TcsDisabledBySwt;
     : Eem_MainEscSwtStInfo.HdcEnabledBySwt;
     =============================================================================*/

    Eem_MainWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure Eem_MainWhlSpdInfo 
     : Eem_MainWhlSpdInfo.FlWhlSpd;
     : Eem_MainWhlSpdInfo.FrWhlSpd;
     : Eem_MainWhlSpdInfo.RlWhlSpd;
     : Eem_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Eem_MainAbsCtrlInfo.AbsActFlg = Abc_CtrlAbsCtrlInfo.AbsActFlg;

    /* Decomposed structure interface */
    Eem_MainEscCtrlInfo.EscActFlg = Abc_CtrlEscCtrlInfo.EscActFlg;

    /* Decomposed structure interface */
    Eem_MainHdcCtrlInfo.HdcActFlg = Abc_CtrlHdcCtrlInfo.HdcActFlg;

    /* Decomposed structure interface */
    Eem_MainTcsCtrlInfo.TcsActFlg = Abc_CtrlTcsCtrlInfo.TcsActFlg;

    /* Decomposed structure interface */
    Eem_MainPedalMData.PedalM_PTS1_Open_Err = PedalM_MainPedalMData.PedalM_PTS1_Open_Err;
    Eem_MainPedalMData.PedalM_PTS1_Short_Err = PedalM_MainPedalMData.PedalM_PTS1_Short_Err;
    Eem_MainPedalMData.PedalM_PTS2_Open_Err = PedalM_MainPedalMData.PedalM_PTS2_Open_Err;
    Eem_MainPedalMData.PedalM_PTS2_Short_Err = PedalM_MainPedalMData.PedalM_PTS2_Short_Err;
    Eem_MainPedalMData.PedalM_PTS1_SupplyPower_Err = PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err;
    Eem_MainPedalMData.PedalM_PTS2_SupplyPower_Err = PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err;
    Eem_MainPedalMData.PedalM_VDD3_OverVolt_Err = PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err;
    Eem_MainPedalMData.PedalM_VDD3_UnderVolt_Err = PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err;
    Eem_MainPedalMData.PedalM_VDD3_OverCurrent_Err = PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err;

    Eem_MainPresMonFaultInfo = PressM_MainPresMonFaultInfo;
    /*==============================================================================
    * Members of structure Eem_MainPresMonFaultInfo 
     : Eem_MainPresMonFaultInfo.CIRP1_FC_Fault_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_FC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_Temp_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_Diag_Info;
     : Eem_MainPresMonFaultInfo.CIRP2_FC_Fault_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_FC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_Temp_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_Diag_Info;
     : Eem_MainPresMonFaultInfo.SIMP_FC_Fault_Err;
     : Eem_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.SIMP_FC_CRC_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_Temp_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_CRC_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_Diag_Info;
     =============================================================================*/

    Eem_MainAsicMonFaultInfo = EcuHwCtrlM_MainAsicMonFaultInfo;
    /*==============================================================================
    * Members of structure Eem_MainAsicMonFaultInfo 
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicComm_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOsc_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicNvm_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err;
     =============================================================================*/

    Eem_MainMTRMOutInfo = MtrM_MainMTRMOutInfo;
    /*==============================================================================
    * Members of structure Eem_MainMTRMOutInfo 
     : Eem_MainMTRMOutInfo.MTRM_Power_Open_Err;
     : Eem_MainMTRMOutInfo.MTRM_Open_Err;
     : Eem_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err;
     : Eem_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err;
     : Eem_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err;
     : Eem_MainMTRMOutInfo.MTRM_Calibration_Err;
     : Eem_MainMTRMOutInfo.MTRM_Short_Err;
     : Eem_MainMTRMOutInfo.MTRM_Driver_Err;
     =============================================================================*/

    Eem_MainSysPwrMonData = SysPwrM_MainSysPwrMonData;
    /*==============================================================================
    * Members of structure Eem_MainSysPwrMonData 
     : Eem_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_Comm_Err;
     : Eem_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err;
     =============================================================================*/

    /* Decomposed structure interface */
    Eem_MainWssMonData.WssM_FlOpen_Err = WssM_MainWssMonData.WssM_FlOpen_Err;
    Eem_MainWssMonData.WssM_FlShort_Err = WssM_MainWssMonData.WssM_FlShort_Err;
    Eem_MainWssMonData.WssM_FlOverTemp_Err = WssM_MainWssMonData.WssM_FlOverTemp_Err;
    Eem_MainWssMonData.WssM_FlLeakage_Err = WssM_MainWssMonData.WssM_FlLeakage_Err;
    Eem_MainWssMonData.WssM_FrOpen_Err = WssM_MainWssMonData.WssM_FrOpen_Err;
    Eem_MainWssMonData.WssM_FrShort_Err = WssM_MainWssMonData.WssM_FrShort_Err;
    Eem_MainWssMonData.WssM_FrOverTemp_Err = WssM_MainWssMonData.WssM_FrOverTemp_Err;
    Eem_MainWssMonData.WssM_FrLeakage_Err = WssM_MainWssMonData.WssM_FrLeakage_Err;
    Eem_MainWssMonData.WssM_RlOpen_Err = WssM_MainWssMonData.WssM_RlOpen_Err;
    Eem_MainWssMonData.WssM_RlShort_Err = WssM_MainWssMonData.WssM_RlShort_Err;
    Eem_MainWssMonData.WssM_RlOverTemp_Err = WssM_MainWssMonData.WssM_RlOverTemp_Err;
    Eem_MainWssMonData.WssM_RlLeakage_Err = WssM_MainWssMonData.WssM_RlLeakage_Err;
    Eem_MainWssMonData.WssM_RrOpen_Err = WssM_MainWssMonData.WssM_RrOpen_Err;
    Eem_MainWssMonData.WssM_RrShort_Err = WssM_MainWssMonData.WssM_RrShort_Err;
    Eem_MainWssMonData.WssM_RrOverTemp_Err = WssM_MainWssMonData.WssM_RrOverTemp_Err;
    Eem_MainWssMonData.WssM_RrLeakage_Err = WssM_MainWssMonData.WssM_RrLeakage_Err;
    Eem_MainWssMonData.WssM_Asic_Comm_Err = WssM_MainWssMonData.WssM_Asic_Comm_Err;

    Eem_MainMgdErrInfo = MtrM_MainMgdErrInfo;
    /*==============================================================================
    * Members of structure Eem_MainMgdErrInfo 
     : Eem_MainMgdErrInfo.MgdInterPwrSuppMonErr;
     : Eem_MainMgdErrInfo.MgdClkMonErr;
     : Eem_MainMgdErrInfo.MgdBISTErr;
     : Eem_MainMgdErrInfo.MgdFlashMemoryErr;
     : Eem_MainMgdErrInfo.MgdRAMErr;
     : Eem_MainMgdErrInfo.MgdConfigRegiErr;
     : Eem_MainMgdErrInfo.MgdInputPattMonErr;
     : Eem_MainMgdErrInfo.MgdOverTempErr;
     : Eem_MainMgdErrInfo.MgdCPmpVMonErr;
     : Eem_MainMgdErrInfo.MgdHBuffCapVErr;
     : Eem_MainMgdErrInfo.MgdInOutPlauErr;
     : Eem_MainMgdErrInfo.MgdCtrlSigMonErr;
     =============================================================================*/

    Eem_MainMpsD1ErrInfo = Mps_TLE5012M_MainMpsD1ErrInfo;
    /*==============================================================================
    * Members of structure Eem_MainMpsD1ErrInfo 
     : Eem_MainMpsD1ErrInfo.ExtPWRSuppMonErr;
     : Eem_MainMpsD1ErrInfo.IntPWRSuppMonErr;
     : Eem_MainMpsD1ErrInfo.WatchDogErr;
     : Eem_MainMpsD1ErrInfo.FlashMemoryErr;
     : Eem_MainMpsD1ErrInfo.StartUpTestErr;
     : Eem_MainMpsD1ErrInfo.ConfigRegiTestErr;
     : Eem_MainMpsD1ErrInfo.HWIntegConsisErr;
     : Eem_MainMpsD1ErrInfo.MagnetLossErr;
     =============================================================================*/

    Eem_MainMpsD2ErrInfo = Mps_TLE5012M_MainMpsD2ErrInfo;
    /*==============================================================================
    * Members of structure Eem_MainMpsD2ErrInfo 
     : Eem_MainMpsD2ErrInfo.ExtPWRSuppMonErr;
     : Eem_MainMpsD2ErrInfo.IntPWRSuppMonErr;
     : Eem_MainMpsD2ErrInfo.WatchDogErr;
     : Eem_MainMpsD2ErrInfo.FlashMemoryErr;
     : Eem_MainMpsD2ErrInfo.StartUpTestErr;
     : Eem_MainMpsD2ErrInfo.ConfigRegiTestErr;
     : Eem_MainMpsD2ErrInfo.HWIntegConsisErr;
     : Eem_MainMpsD2ErrInfo.MagnetLossErr;
     =============================================================================*/

    Eem_MainPressPInfo = PressP_MainPressPInfo;
    /*==============================================================================
    * Members of structure Eem_MainPressPInfo 
     : Eem_MainPressPInfo.SimP_Noise_Err;
     : Eem_MainPressPInfo.CirP1_Noise_Err;
     : Eem_MainPressPInfo.CirP2_Noise_Err;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Eem_MainEcuModeSts = Mom_HndlrEcuModeSts;
    Eem_MainIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Eem_MainIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    if (SystemStabilzationTimeCnt >= U8_SYSTEM_STABILIZATION_TIME)
	    Eem_Main();

    /* Structure interface */
    Task_5msBus.Eem_MainEemFailData = Eem_MainEemFailData;
    /*==============================================================================
    * Members of structure Eem_MainEemFailData 
     : Eem_MainEemFailData.Eem_Fail_BBSSol;
     : Eem_MainEemFailData.Eem_Fail_ESCSol;
     : Eem_MainEemFailData.Eem_Fail_FrontSol;
     : Eem_MainEemFailData.Eem_Fail_RearSol;
     : Eem_MainEemFailData.Eem_Fail_Motor;
     : Eem_MainEemFailData.Eem_Fail_MPS;
     : Eem_MainEemFailData.Eem_Fail_MGD;
     : Eem_MainEemFailData.Eem_Fail_BBSValveRelay;
     : Eem_MainEemFailData.Eem_Fail_ESCValveRelay;
     : Eem_MainEemFailData.Eem_Fail_ECUHw;
     : Eem_MainEemFailData.Eem_Fail_ASIC;
     : Eem_MainEemFailData.Eem_Fail_OverVolt;
     : Eem_MainEemFailData.Eem_Fail_UnderVolt;
     : Eem_MainEemFailData.Eem_Fail_LowVolt;
     : Eem_MainEemFailData.Eem_Fail_LowerVolt;
     : Eem_MainEemFailData.Eem_Fail_SenPwr_12V;
     : Eem_MainEemFailData.Eem_Fail_SenPwr_5V;
     : Eem_MainEemFailData.Eem_Fail_Yaw;
     : Eem_MainEemFailData.Eem_Fail_Ay;
     : Eem_MainEemFailData.Eem_Fail_Ax;
     : Eem_MainEemFailData.Eem_Fail_Str;
     : Eem_MainEemFailData.Eem_Fail_CirP1;
     : Eem_MainEemFailData.Eem_Fail_CirP2;
     : Eem_MainEemFailData.Eem_Fail_SimP;
     : Eem_MainEemFailData.Eem_Fail_BLS;
     : Eem_MainEemFailData.Eem_Fail_ESCSw;
     : Eem_MainEemFailData.Eem_Fail_HDCSw;
     : Eem_MainEemFailData.Eem_Fail_AVHSw;
     : Eem_MainEemFailData.Eem_Fail_BrakeLampRelay;
     : Eem_MainEemFailData.Eem_Fail_EssRelay;
     : Eem_MainEemFailData.Eem_Fail_GearR;
     : Eem_MainEemFailData.Eem_Fail_Clutch;
     : Eem_MainEemFailData.Eem_Fail_ParkBrake;
     : Eem_MainEemFailData.Eem_Fail_PedalPDT;
     : Eem_MainEemFailData.Eem_Fail_PedalPDF;
     : Eem_MainEemFailData.Eem_Fail_BrakeFluid;
     : Eem_MainEemFailData.Eem_Fail_TCSTemp;
     : Eem_MainEemFailData.Eem_Fail_HDCTemp;
     : Eem_MainEemFailData.Eem_Fail_SCCTemp;
     : Eem_MainEemFailData.Eem_Fail_TVBBTemp;
     : Eem_MainEemFailData.Eem_Fail_MainCanLine;
     : Eem_MainEemFailData.Eem_Fail_SubCanLine;
     : Eem_MainEemFailData.Eem_Fail_EMSTimeOut;
     : Eem_MainEemFailData.Eem_Fail_FWDTimeOut;
     : Eem_MainEemFailData.Eem_Fail_TCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_HCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_MCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_VariantCoding;
     : Eem_MainEemFailData.Eem_Fail_WssFL;
     : Eem_MainEemFailData.Eem_Fail_WssFR;
     : Eem_MainEemFailData.Eem_Fail_WssRL;
     : Eem_MainEemFailData.Eem_Fail_WssRR;
     : Eem_MainEemFailData.Eem_Fail_SameSideWss;
     : Eem_MainEemFailData.Eem_Fail_DiagonalWss;
     : Eem_MainEemFailData.Eem_Fail_FrontWss;
     : Eem_MainEemFailData.Eem_Fail_RearWss;
     =============================================================================*/

    //Task_5msBus.Eem_MainEemCtrlInhibitData = Eem_MainEemCtrlInhibitData;
    /*==============================================================================
    * Members of structure Eem_MainEemCtrlInhibitData 
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/

    Task_5msBus.Eem_MainEemSuspectData = Eem_MainEemSuspectData;
    /*==============================================================================
    * Members of structure Eem_MainEemSuspectData 
     : Eem_MainEemSuspectData.Eem_Suspect_WssFL;
     : Eem_MainEemSuspectData.Eem_Suspect_WssFR;
     : Eem_MainEemSuspectData.Eem_Suspect_WssRL;
     : Eem_MainEemSuspectData.Eem_Suspect_WssRR;
     : Eem_MainEemSuspectData.Eem_Suspect_SameSideWss;
     : Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss;
     : Eem_MainEemSuspectData.Eem_Suspect_FrontWss;
     : Eem_MainEemSuspectData.Eem_Suspect_RearWss;
     : Eem_MainEemSuspectData.Eem_Suspect_BBSSol;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCSol;
     : Eem_MainEemSuspectData.Eem_Suspect_FrontSol;
     : Eem_MainEemSuspectData.Eem_Suspect_RearSol;
     : Eem_MainEemSuspectData.Eem_Suspect_Motor;
     : Eem_MainEemSuspectData.Eem_Suspect_MPS;
     : Eem_MainEemSuspectData.Eem_Suspect_MGD;
     : Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_ECUHw;
     : Eem_MainEemSuspectData.Eem_Suspect_ASIC;
     : Eem_MainEemSuspectData.Eem_Suspect_OverVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_UnderVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_LowVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V;
     : Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V;
     : Eem_MainEemSuspectData.Eem_Suspect_Yaw;
     : Eem_MainEemSuspectData.Eem_Suspect_Ay;
     : Eem_MainEemSuspectData.Eem_Suspect_Ax;
     : Eem_MainEemSuspectData.Eem_Suspect_Str;
     : Eem_MainEemSuspectData.Eem_Suspect_CirP1;
     : Eem_MainEemSuspectData.Eem_Suspect_CirP2;
     : Eem_MainEemSuspectData.Eem_Suspect_SimP;
     : Eem_MainEemSuspectData.Eem_Suspect_BS;
     : Eem_MainEemSuspectData.Eem_Suspect_BLS;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCSw;
     : Eem_MainEemSuspectData.Eem_Suspect_HDCSw;
     : Eem_MainEemSuspectData.Eem_Suspect_AVHSw;
     : Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_EssRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_GearR;
     : Eem_MainEemSuspectData.Eem_Suspect_Clutch;
     : Eem_MainEemSuspectData.Eem_Suspect_ParkBrake;
     : Eem_MainEemSuspectData.Eem_Suspect_PedalPDT;
     : Eem_MainEemSuspectData.Eem_Suspect_PedalPDF;
     : Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid;
     : Eem_MainEemSuspectData.Eem_Suspect_TCSTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_HDCTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_SCCTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_TVBBTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_MainCanLine;
     : Eem_MainEemSuspectData.Eem_Suspect_SubCanLine;
     : Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_VariantCoding;
     =============================================================================*/

    Task_5msBus.Eem_MainEemEceData = Eem_MainEemEceData;
    /*==============================================================================
    * Members of structure Eem_MainEemEceData 
     : Eem_MainEemEceData.Eem_Ece_Wss;
     : Eem_MainEemEceData.Eem_Ece_Yaw;
     : Eem_MainEemEceData.Eem_Ece_Ay;
     : Eem_MainEemEceData.Eem_Ece_Ax;
     : Eem_MainEemEceData.Eem_Ece_Cir1P;
     : Eem_MainEemEceData.Eem_Ece_Cir2P;
     : Eem_MainEemEceData.Eem_Ece_SimP;
     : Eem_MainEemEceData.Eem_Ece_Bls;
     : Eem_MainEemEceData.Eem_Ece_Pedal;
     : Eem_MainEemEceData.Eem_Ece_Motor;
     : Eem_MainEemEceData.Eem_Ece_Vdc_Sw;
     : Eem_MainEemEceData.Eem_Ece_Hdc_Sw;
     : Eem_MainEemEceData.Eem_Ece_GearR_Sw;
     : Eem_MainEemEceData.Eem_Ece_Clutch_Sw;
     =============================================================================*/

    Task_5msBus.Eem_MainEemLampData = Eem_MainEemLampData;
    /*==============================================================================
    * Members of structure Eem_MainEemLampData 
     : Eem_MainEemLampData.Eem_Lamp_EBDLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ABSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_HDCLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_BBSBuzzorRequest;
     : Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AHBLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSFuncLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AVHLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AVHILampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ACCEnable;
     : Eem_MainEemLampData.Eem_Lamp_CDMEnable;
     : Eem_MainEemLampData.Eem_Buzzor_On;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Eem_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Eem_SuspcDetn start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Eem_SuspcDetnDiagClrSrs = Task_5msBus.Eem_SuspcDetnDiagClrSrs;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Eem_SuspcDetnEcuModeSts = Mom_HndlrEcuModeSts;

    /* Execute  runnable */
    Eem_SuspcDetn();

    /* Structure interface */
    Task_5msBus.Eem_SuspcDetnCanTimeOutStInfo = Eem_SuspcDetnCanTimeOutStInfo;
    /*==============================================================================
    * Members of structure Eem_SuspcDetnCanTimeOutStInfo 
     : Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet;
     : Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet;
     : Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Eem_SuspcDetnFuncInhibitVlvSts = Eem_SuspcDetnFuncInhibitVlvSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitIocSts = Eem_SuspcDetnFuncInhibitIocSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitProxySts = Eem_SuspcDetnFuncInhibitProxySts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitDiagSts = Eem_SuspcDetnFuncInhibitDiagSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitAcmioSts = Eem_SuspcDetnFuncInhibitAcmioSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitAcmctlSts = Eem_SuspcDetnFuncInhibitAcmctlSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitMspSts = Eem_SuspcDetnFuncInhibitMspSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitPedalSts = Eem_SuspcDetnFuncInhibitPedalSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitPressSts = Eem_SuspcDetnFuncInhibitPressSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitSesSts = Eem_SuspcDetnFuncInhibitSesSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitVlvdSts = Eem_SuspcDetnFuncInhibitVlvdSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitAdcifSts = Eem_SuspcDetnFuncInhibitAdcifSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitCanTrcvSts = Eem_SuspcDetnFuncInhibitCanTrcvSts;
    Task_5msBus.Eem_SuspcDetnFuncInhibitMccSts = Eem_SuspcDetnFuncInhibitMccSts;

    /**********************************************************************************************
     Eem_SuspcDetn end
     **********************************************************************************************/

    /**********************************************************************************************
     Arbitrator_Vlv start
     **********************************************************************************************/

     /* Structure interface */
    Arbitrator_VlvNormVlvReqSesInfo = Task_5msBus.Arbitrator_VlvNormVlvReqSesInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvNormVlvReqSesInfo 
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvDataLen;
     =============================================================================*/

    Arbitrator_VlvWhlVlvReqSesInfo = Task_5msBus.Arbitrator_VlvWhlVlvReqSesInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqSesInfo 
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvDataLen;
     =============================================================================*/

    Arbitrator_VlvDiagVlvActrInfo = Task_5msBus.Arbitrator_VlvDiagVlvActrInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvDiagVlvActrInfo 
     : Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
     : Arbitrator_VlvDiagVlvActrInfo.FlOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FlIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FrOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FrIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RlOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RlIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RrOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RrIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData;
     =============================================================================*/

    /* Single interface */
    Arbitrator_VlvAcmAsicInitCompleteFlag = Task_5msBus.Arbitrator_VlvAcmAsicInitCompleteFlag;
    Arbitrator_VlvDiagSci = Task_5msBus.Arbitrator_VlvDiagSci;
    Arbitrator_VlvDiagAhbSci = Task_5msBus.Arbitrator_VlvDiagAhbSci;

    /* Inter-Runnable structure interface */
    Arbitrator_VlvFSBbsVlvActrInfo = BbsVlvM_MainFSBbsVlvActrInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSBbsVlvActrInfo 
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_sim_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cutp_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cuts_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_rlv_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cv_time;
     =============================================================================*/

    Arbitrator_VlvFSFsrBbsDrvInfo = BbsVlvM_MainFSFsrBbsDrvInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSFsrBbsDrvInfo 
     : Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv;
     : Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/

    Arbitrator_VlvFSEscVlvActrInfo = AbsVlvM_MainFSEscVlvActrInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSEscVlvActrInfo 
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_flno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_frno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_bal_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_pressdump_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_resp_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_flnc_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_frnc_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlnc_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrnc_time;
     =============================================================================*/

    Arbitrator_VlvFSFsrAbsDrvInfo = AbsVlvM_MainFSFsrAbsDrvInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSFsrAbsDrvInfo 
     : Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsDrv;
     : Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/

    Arbitrator_VlvWhlVlvReqAbcInfo = Abc_CtrlWhlVlvReqAbcInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqAbcInfo 
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlOvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrOvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlOvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrOvDataLen;
     =============================================================================*/

    Arbitrator_VlvNormVlvReqVlvActInfo = Vat_CtrlNormVlvReqVlvActInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvNormVlvReqVlvActInfo 
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvDataLen;
     =============================================================================*/

    Arbitrator_VlvWhlVlvReqIdbInfo = Vat_CtrlWhlVlvReqIdbInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqIdbInfo 
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData;
     =============================================================================*/

    Arbitrator_VlvIdbBalVlvReqInfo = Vat_CtrlIdbBalVlvReqInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvIdbBalVlvReqInfo 
     : Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData;
     : Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData;
     : Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData;
     : Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReq;
     : Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReq;
     : Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq;
     : Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvDataLen;
     : Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvDataLen;
     : Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvDataLen;
     =============================================================================*/

    /* Decomposed structure interface */
    Arbitrator_VlvEemFailData.Eem_Fail_BBSSol = Eem_MainEemFailData.Eem_Fail_BBSSol;
    Arbitrator_VlvEemFailData.Eem_Fail_ESCSol = Eem_MainEemFailData.Eem_Fail_ESCSol;
    Arbitrator_VlvEemFailData.Eem_Fail_FrontSol = Eem_MainEemFailData.Eem_Fail_FrontSol;
    Arbitrator_VlvEemFailData.Eem_Fail_RearSol = Eem_MainEemFailData.Eem_Fail_RearSol;
    Arbitrator_VlvEemFailData.Eem_Fail_Motor = Eem_MainEemFailData.Eem_Fail_Motor;
    Arbitrator_VlvEemFailData.Eem_Fail_MPS = Eem_MainEemFailData.Eem_Fail_MPS;
    Arbitrator_VlvEemFailData.Eem_Fail_MGD = Eem_MainEemFailData.Eem_Fail_MGD;
    Arbitrator_VlvEemFailData.Eem_Fail_BBSValveRelay = Eem_MainEemFailData.Eem_Fail_BBSValveRelay;
    Arbitrator_VlvEemFailData.Eem_Fail_ESCValveRelay = Eem_MainEemFailData.Eem_Fail_ESCValveRelay;
    Arbitrator_VlvEemFailData.Eem_Fail_ECUHw = Eem_MainEemFailData.Eem_Fail_ECUHw;
    Arbitrator_VlvEemFailData.Eem_Fail_ASIC = Eem_MainEemFailData.Eem_Fail_ASIC;
    Arbitrator_VlvEemFailData.Eem_Fail_OverVolt = Eem_MainEemFailData.Eem_Fail_OverVolt;
    Arbitrator_VlvEemFailData.Eem_Fail_UnderVolt = Eem_MainEemFailData.Eem_Fail_UnderVolt;
    Arbitrator_VlvEemFailData.Eem_Fail_LowVolt = Eem_MainEemFailData.Eem_Fail_LowVolt;
    Arbitrator_VlvEemFailData.Eem_Fail_LowerVolt = Eem_MainEemFailData.Eem_Fail_LowerVolt;
    Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_12V = Eem_MainEemFailData.Eem_Fail_SenPwr_12V;
    Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_5V = Eem_MainEemFailData.Eem_Fail_SenPwr_5V;
    Arbitrator_VlvEemFailData.Eem_Fail_Yaw = Eem_MainEemFailData.Eem_Fail_Yaw;
    Arbitrator_VlvEemFailData.Eem_Fail_Ay = Eem_MainEemFailData.Eem_Fail_Ay;
    Arbitrator_VlvEemFailData.Eem_Fail_Ax = Eem_MainEemFailData.Eem_Fail_Ax;
    Arbitrator_VlvEemFailData.Eem_Fail_Str = Eem_MainEemFailData.Eem_Fail_Str;
    Arbitrator_VlvEemFailData.Eem_Fail_CirP1 = Eem_MainEemFailData.Eem_Fail_CirP1;
    Arbitrator_VlvEemFailData.Eem_Fail_CirP2 = Eem_MainEemFailData.Eem_Fail_CirP2;
    Arbitrator_VlvEemFailData.Eem_Fail_SimP = Eem_MainEemFailData.Eem_Fail_SimP;
    Arbitrator_VlvEemFailData.Eem_Fail_BLS = Eem_MainEemFailData.Eem_Fail_BLS;
    Arbitrator_VlvEemFailData.Eem_Fail_ESCSw = Eem_MainEemFailData.Eem_Fail_ESCSw;
    Arbitrator_VlvEemFailData.Eem_Fail_HDCSw = Eem_MainEemFailData.Eem_Fail_HDCSw;
    Arbitrator_VlvEemFailData.Eem_Fail_AVHSw = Eem_MainEemFailData.Eem_Fail_AVHSw;
    Arbitrator_VlvEemFailData.Eem_Fail_BrakeLampRelay = Eem_MainEemFailData.Eem_Fail_BrakeLampRelay;
    Arbitrator_VlvEemFailData.Eem_Fail_EssRelay = Eem_MainEemFailData.Eem_Fail_EssRelay;
    Arbitrator_VlvEemFailData.Eem_Fail_GearR = Eem_MainEemFailData.Eem_Fail_GearR;
    Arbitrator_VlvEemFailData.Eem_Fail_Clutch = Eem_MainEemFailData.Eem_Fail_Clutch;
    Arbitrator_VlvEemFailData.Eem_Fail_ParkBrake = Eem_MainEemFailData.Eem_Fail_ParkBrake;
    Arbitrator_VlvEemFailData.Eem_Fail_PedalPDT = Eem_MainEemFailData.Eem_Fail_PedalPDT;
    Arbitrator_VlvEemFailData.Eem_Fail_PedalPDF = Eem_MainEemFailData.Eem_Fail_PedalPDF;
    Arbitrator_VlvEemFailData.Eem_Fail_BrakeFluid = Eem_MainEemFailData.Eem_Fail_BrakeFluid;
    Arbitrator_VlvEemFailData.Eem_Fail_TCSTemp = Eem_MainEemFailData.Eem_Fail_TCSTemp;
    Arbitrator_VlvEemFailData.Eem_Fail_HDCTemp = Eem_MainEemFailData.Eem_Fail_HDCTemp;
    Arbitrator_VlvEemFailData.Eem_Fail_SCCTemp = Eem_MainEemFailData.Eem_Fail_SCCTemp;
    Arbitrator_VlvEemFailData.Eem_Fail_TVBBTemp = Eem_MainEemFailData.Eem_Fail_TVBBTemp;
    Arbitrator_VlvEemFailData.Eem_Fail_MainCanLine = Eem_MainEemFailData.Eem_Fail_MainCanLine;
    Arbitrator_VlvEemFailData.Eem_Fail_SubCanLine = Eem_MainEemFailData.Eem_Fail_SubCanLine;
    Arbitrator_VlvEemFailData.Eem_Fail_EMSTimeOut = Eem_MainEemFailData.Eem_Fail_EMSTimeOut;
    Arbitrator_VlvEemFailData.Eem_Fail_FWDTimeOut = Eem_MainEemFailData.Eem_Fail_FWDTimeOut;
    Arbitrator_VlvEemFailData.Eem_Fail_TCUTimeOut = Eem_MainEemFailData.Eem_Fail_TCUTimeOut;
    Arbitrator_VlvEemFailData.Eem_Fail_HCUTimeOut = Eem_MainEemFailData.Eem_Fail_HCUTimeOut;
    Arbitrator_VlvEemFailData.Eem_Fail_MCUTimeOut = Eem_MainEemFailData.Eem_Fail_MCUTimeOut;
    Arbitrator_VlvEemFailData.Eem_Fail_VariantCoding = Eem_MainEemFailData.Eem_Fail_VariantCoding;

    //Arbitrator_VlvEemCtrlInhibitData = Eem_MainEemCtrlInhibitData;
    /*==============================================================================
    * Members of structure Arbitrator_VlvEemCtrlInhibitData 
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/

    Arbitrator_VlvWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlSpdInfo 
     : Arbitrator_VlvWhlSpdInfo.FlWhlSpd;
     : Arbitrator_VlvWhlSpdInfo.FrWhlSpd;
     : Arbitrator_VlvWhlSpdInfo.RlWhlSpd;
     : Arbitrator_VlvWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Arbitrator_VlvEcuModeSts = Mom_HndlrEcuModeSts;
    Arbitrator_VlvIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Arbitrator_VlvIgnEdgeSts = Prly_HndlrIgnEdgeSts;
    //Arbitrator_VlvFuncInhibitVlvSts = Eem_SuspcDetnFuncInhibitVlvSts;

    /* Execute  runnable */
    Arbitrator_Vlv();

    /* Structure interface */
    Task_5msBus.Arbitrator_VlvArbWhlVlvReqInfo = Arbitrator_VlvArbWhlVlvReqInfo;
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbWhlVlvReqInfo 
     : Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Arbitrator_VlvArbVlvDriveState = Arbitrator_VlvArbVlvDriveState;

    /**********************************************************************************************
     Arbitrator_Vlv end
     **********************************************************************************************/

    /**********************************************************************************************
     Arbitrator_Rly start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Arbitrator_RlyDiagSci = Task_5msBus.Arbitrator_RlyDiagSci;
    Arbitrator_RlyDiagAhbSci = Task_5msBus.Arbitrator_RlyDiagAhbSci;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    Arbitrator_RlyEemFailData.Eem_Fail_Motor = Eem_MainEemFailData.Eem_Fail_Motor;
    Arbitrator_RlyEemFailData.Eem_Fail_MPS = Eem_MainEemFailData.Eem_Fail_MPS;
    Arbitrator_RlyEemFailData.Eem_Fail_MGD = Eem_MainEemFailData.Eem_Fail_MGD;
    Arbitrator_RlyEemFailData.Eem_Fail_BBSValveRelay = Eem_MainEemFailData.Eem_Fail_BBSValveRelay;
    Arbitrator_RlyEemFailData.Eem_Fail_ESCValveRelay = Eem_MainEemFailData.Eem_Fail_ESCValveRelay;
    Arbitrator_RlyEemFailData.Eem_Fail_ECUHw = Eem_MainEemFailData.Eem_Fail_ECUHw;
    Arbitrator_RlyEemFailData.Eem_Fail_ASIC = Eem_MainEemFailData.Eem_Fail_ASIC;
    Arbitrator_RlyEemFailData.Eem_Fail_OverVolt = Eem_MainEemFailData.Eem_Fail_OverVolt;
    Arbitrator_RlyEemFailData.Eem_Fail_UnderVolt = Eem_MainEemFailData.Eem_Fail_UnderVolt;
    Arbitrator_RlyEemFailData.Eem_Fail_LowVolt = Eem_MainEemFailData.Eem_Fail_LowVolt;
    Arbitrator_RlyEemFailData.Eem_Fail_LowerVolt = Eem_MainEemFailData.Eem_Fail_LowerVolt;
    Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_12V = Eem_MainEemFailData.Eem_Fail_SenPwr_12V;
    Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_5V = Eem_MainEemFailData.Eem_Fail_SenPwr_5V;
    Arbitrator_RlyEemFailData.Eem_Fail_Yaw = Eem_MainEemFailData.Eem_Fail_Yaw;
    Arbitrator_RlyEemFailData.Eem_Fail_Ay = Eem_MainEemFailData.Eem_Fail_Ay;
    Arbitrator_RlyEemFailData.Eem_Fail_Ax = Eem_MainEemFailData.Eem_Fail_Ax;
    Arbitrator_RlyEemFailData.Eem_Fail_Str = Eem_MainEemFailData.Eem_Fail_Str;
    Arbitrator_RlyEemFailData.Eem_Fail_CirP1 = Eem_MainEemFailData.Eem_Fail_CirP1;
    Arbitrator_RlyEemFailData.Eem_Fail_CirP2 = Eem_MainEemFailData.Eem_Fail_CirP2;
    Arbitrator_RlyEemFailData.Eem_Fail_SimP = Eem_MainEemFailData.Eem_Fail_SimP;
    Arbitrator_RlyEemFailData.Eem_Fail_BLS = Eem_MainEemFailData.Eem_Fail_BLS;
    Arbitrator_RlyEemFailData.Eem_Fail_ESCSw = Eem_MainEemFailData.Eem_Fail_ESCSw;
    Arbitrator_RlyEemFailData.Eem_Fail_HDCSw = Eem_MainEemFailData.Eem_Fail_HDCSw;
    Arbitrator_RlyEemFailData.Eem_Fail_AVHSw = Eem_MainEemFailData.Eem_Fail_AVHSw;
    Arbitrator_RlyEemFailData.Eem_Fail_BrakeLampRelay = Eem_MainEemFailData.Eem_Fail_BrakeLampRelay;
    Arbitrator_RlyEemFailData.Eem_Fail_EssRelay = Eem_MainEemFailData.Eem_Fail_EssRelay;
    Arbitrator_RlyEemFailData.Eem_Fail_GearR = Eem_MainEemFailData.Eem_Fail_GearR;
    Arbitrator_RlyEemFailData.Eem_Fail_Clutch = Eem_MainEemFailData.Eem_Fail_Clutch;
    Arbitrator_RlyEemFailData.Eem_Fail_ParkBrake = Eem_MainEemFailData.Eem_Fail_ParkBrake;
    Arbitrator_RlyEemFailData.Eem_Fail_PedalPDT = Eem_MainEemFailData.Eem_Fail_PedalPDT;
    Arbitrator_RlyEemFailData.Eem_Fail_PedalPDF = Eem_MainEemFailData.Eem_Fail_PedalPDF;
    Arbitrator_RlyEemFailData.Eem_Fail_BrakeFluid = Eem_MainEemFailData.Eem_Fail_BrakeFluid;
    Arbitrator_RlyEemFailData.Eem_Fail_TCSTemp = Eem_MainEemFailData.Eem_Fail_TCSTemp;
    Arbitrator_RlyEemFailData.Eem_Fail_HDCTemp = Eem_MainEemFailData.Eem_Fail_HDCTemp;
    Arbitrator_RlyEemFailData.Eem_Fail_SCCTemp = Eem_MainEemFailData.Eem_Fail_SCCTemp;
    Arbitrator_RlyEemFailData.Eem_Fail_TVBBTemp = Eem_MainEemFailData.Eem_Fail_TVBBTemp;
    Arbitrator_RlyEemFailData.Eem_Fail_MainCanLine = Eem_MainEemFailData.Eem_Fail_MainCanLine;
    Arbitrator_RlyEemFailData.Eem_Fail_SubCanLine = Eem_MainEemFailData.Eem_Fail_SubCanLine;
    Arbitrator_RlyEemFailData.Eem_Fail_EMSTimeOut = Eem_MainEemFailData.Eem_Fail_EMSTimeOut;
    Arbitrator_RlyEemFailData.Eem_Fail_FWDTimeOut = Eem_MainEemFailData.Eem_Fail_FWDTimeOut;
    Arbitrator_RlyEemFailData.Eem_Fail_TCUTimeOut = Eem_MainEemFailData.Eem_Fail_TCUTimeOut;
    Arbitrator_RlyEemFailData.Eem_Fail_HCUTimeOut = Eem_MainEemFailData.Eem_Fail_HCUTimeOut;
    Arbitrator_RlyEemFailData.Eem_Fail_MCUTimeOut = Eem_MainEemFailData.Eem_Fail_MCUTimeOut;
    Arbitrator_RlyEemFailData.Eem_Fail_VariantCoding = Eem_MainEemFailData.Eem_Fail_VariantCoding;

    Arbitrator_RlyWhlSpdInfo = Wss_SenWhlSpdInfo;
    /*==============================================================================
    * Members of structure Arbitrator_RlyWhlSpdInfo 
     : Arbitrator_RlyWhlSpdInfo.FlWhlSpd;
     : Arbitrator_RlyWhlSpdInfo.FrWhlSpd;
     : Arbitrator_RlyWhlSpdInfo.RlWhlSpd;
     : Arbitrator_RlyWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Arbitrator_RlyEcuModeSts = Mom_HndlrEcuModeSts;
    Arbitrator_RlyIgnOnOffSts = Prly_HndlrIgnOnOffSts;
    Arbitrator_RlyIgnEdgeSts = Prly_HndlrIgnEdgeSts;

    /* Execute  runnable */
    Arbitrator_Rly();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Arbitrator_Rly end
     **********************************************************************************************/

    /**********************************************************************************************
     Fsr_Actr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Fsr_ActrAcmAsicInitCompleteFlag = Task_5msBus.Fsr_ActrAcmAsicInitCompleteFlag;

    /* Inter-Runnable structure interface */
    Fsr_ActrArbFsrBbsDrvInfo = Arbitrator_VlvArbFsrBbsDrvInfo;
    /*==============================================================================
    * Members of structure Fsr_ActrArbFsrBbsDrvInfo 
     : Fsr_ActrArbFsrBbsDrvInfo.FsrCbsDrv;
     : Fsr_ActrArbFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/

    Fsr_ActrArbFsrAbsDrvInfo = Arbitrator_VlvArbFsrAbsDrvInfo;
    /*==============================================================================
    * Members of structure Fsr_ActrArbFsrAbsDrvInfo 
     : Fsr_ActrArbFsrAbsDrvInfo.FsrAbsDrv;
     : Fsr_ActrArbFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Fsr_ActrEcuModeSts = Mom_HndlrEcuModeSts;
    Fsr_ActrAsicEnDrDrv = Arbitrator_VlvAsicEnDrDrv;
    //Fsr_ActrFuncInhibitFsrSts = Eem_SuspcDetnFuncInhibitFsrSts;

    /* Execute  runnable */
    Fsr_Actr();

    /* Structure interface */
    Task_5msBus.Fsr_ActrFsrAbsDrvInfo = Fsr_ActrFsrAbsDrvInfo;
    /*==============================================================================
    * Members of structure Fsr_ActrFsrAbsDrvInfo 
     : Fsr_ActrFsrAbsDrvInfo.FsrAbsOff;
     : Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv;
     =============================================================================*/

    Task_5msBus.Fsr_ActrFsrCbsDrvInfo = Fsr_ActrFsrCbsDrvInfo;
    /*==============================================================================
    * Members of structure Fsr_ActrFsrCbsDrvInfo 
     : Fsr_ActrFsrCbsDrvInfo.FsrCbsOff;
     : Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Fsr_ActrFsrDcMtrShutDwn = Fsr_ActrFsrDcMtrShutDwn;
    Task_5msBus.Fsr_ActrFsrEnDrDrv = Fsr_ActrFsrEnDrDrv;

    /**********************************************************************************************
     Fsr_Actr end
     **********************************************************************************************/

    /**********************************************************************************************
     Rly_Actr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Rly_ActrEcuModeSts = Mom_HndlrEcuModeSts;
    //Rly_ActrFuncInhibitRlySts = Eem_SuspcDetnFuncInhibitRlySts;

    /* Execute  runnable */
    Rly_Actr();

    /* Structure interface */
    Task_5msBus.Rly_ActrRlyDrvInfo = Rly_ActrRlyDrvInfo;
    /*==============================================================================
    * Members of structure Rly_ActrRlyDrvInfo 
     : Rly_ActrRlyDrvInfo.RlyDbcDrv;
     : Rly_ActrRlyDrvInfo.RlyEssDrv;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Rly_Actr end
     **********************************************************************************************/

    /**********************************************************************************************
     Vlv_ActrSync start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Vlv_ActrSyncAcmAsicInitCompleteFlag = Task_5msBus.Vlv_ActrSyncAcmAsicInitCompleteFlag;

    /* Inter-Runnable structure interface */
    Vlv_ActrSyncArbWhlVlvReqInfo = Arbitrator_VlvArbWhlVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbWhlVlvReqInfo 
     : Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    Vlv_ActrSyncArbNormVlvReqInfo = Arbitrator_VlvArbNormVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbNormVlvReqInfo 
     : Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData;
     =============================================================================*/

    Vlv_ActrSyncArbBalVlvReqInfo = Arbitrator_VlvArbBalVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbBalVlvReqInfo 
     : Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/

    Vlv_ActrSyncArbResPVlvReqInfo = Arbitrator_VlvArbResPVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbResPVlvReqInfo 
     : Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData;
     =============================================================================*/

    /* Inter-Runnable single interface */
    //Vlv_ActrSyncEcuModeSts = Mom_HndlrEcuModeSts;
    Vlv_ActrSyncArbVlvSync = Arbitrator_VlvArbVlvSync;
    //Vlv_ActrSyncFuncInhibitVlvSts = Eem_SuspcDetnFuncInhibitVlvSts;

    /* Execute  runnable */
    Vlv_ActrSync();

    /* Structure interface */
    Task_5msBus.Vlv_ActrSyncNormVlvReqInfo = Vlv_ActrSyncNormVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncNormVlvReqInfo 
     : Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData;
     =============================================================================*/

    Task_5msBus.Vlv_ActrSyncWhlVlvReqInfo = Vlv_ActrSyncWhlVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncWhlVlvReqInfo 
     : Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    Task_5msBus.Vlv_ActrSyncBalVlvReqInfo = Vlv_ActrSyncBalVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrSyncBalVlvReqInfo 
     : Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData;
     : Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData;
     : Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData;
     =============================================================================*/

    /* Single interface */
    Task_5msBus.Vlv_ActrSyncVlvSync = Vlv_ActrSyncVlvSync;

    /**********************************************************************************************
     Vlv_ActrSync end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_OutputCS5ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Ioc_OutputCS5msAcmAsicInitCompleteFlag = Task_5msBus.Ioc_OutputCS5msAcmAsicInitCompleteFlag;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    //Ioc_OutputCS5msEcuModeSts = Mom_HndlrEcuModeSts;
    //Ioc_OutputCS5msFuncInhibitIocSts = Eem_SuspcDetnFuncInhibitIocSts;

    /* Execute  runnable */
    Ioc_OutputCS5ms();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Ioc_OutputCS5ms end
     **********************************************************************************************/


    /* Output */
    Task_5ms_Write_Proxy_RxByComRxYawSerialInfo(&Task_5msBus.Proxy_RxByComRxYawSerialInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxYawSerialInfo 
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_0;
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_1;
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxByComRxYawAccInfo(&Task_5msBus.Proxy_RxByComRxYawAccInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxYawAccInfo 
     : Proxy_RxByComRxYawAccInfo.YawRateValidData;
     : Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus;
     : Proxy_RxByComRxYawAccInfo.YawRateSignal_0;
     : Proxy_RxByComRxYawAccInfo.YawRateSignal_1;
     : Proxy_RxByComRxYawAccInfo.SensorOscFreqDev;
     : Proxy_RxByComRxYawAccInfo.Gyro_Fail;
     : Proxy_RxByComRxYawAccInfo.Raster_Fail;
     : Proxy_RxByComRxYawAccInfo.Eep_Fail;
     : Proxy_RxByComRxYawAccInfo.Batt_Range_Err;
     : Proxy_RxByComRxYawAccInfo.Asic_Fail;
     : Proxy_RxByComRxYawAccInfo.Accel_Fail;
     : Proxy_RxByComRxYawAccInfo.Ram_Fail;
     : Proxy_RxByComRxYawAccInfo.Rom_Fail;
     : Proxy_RxByComRxYawAccInfo.Ad_Fail;
     : Proxy_RxByComRxYawAccInfo.Osc_Fail;
     : Proxy_RxByComRxYawAccInfo.Watchdog_Rst;
     : Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst;
     : Proxy_RxByComRxYawAccInfo.RollingCounter;
     : Proxy_RxByComRxYawAccInfo.Can_Func_Err;
     : Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0;
     : Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1;
     : Proxy_RxByComRxYawAccInfo.LatAccValidData;
     : Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxByComRxSasInfo(&Task_5msBus.Proxy_RxByComRxSasInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxSasInfo 
     : Proxy_RxByComRxSasInfo.Angle;
     : Proxy_RxByComRxSasInfo.Speed;
     : Proxy_RxByComRxSasInfo.Ok;
     : Proxy_RxByComRxSasInfo.Cal;
     : Proxy_RxByComRxSasInfo.Trim;
     : Proxy_RxByComRxSasInfo.CheckSum;
     : Proxy_RxByComRxSasInfo.MsgCount;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxByComRxLongAccInfo(&Task_5msBus.Proxy_RxByComRxLongAccInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxLongAccInfo 
     : Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive;
     : Proxy_RxByComRxLongAccInfo.IntSenFaultPresent;
     : Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre;
     : Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre;
     : Proxy_RxByComRxLongAccInfo.LongRollingCounter;
     : Proxy_RxByComRxLongAccInfo.IntTempSensorFault;
     : Proxy_RxByComRxLongAccInfo.LongAccInvalidData;
     : Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus;
     : Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0;
     : Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxByComRxMsgOkFlgInfo(&Task_5msBus.Proxy_RxByComRxMsgOkFlgInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxMsgOkFlgInfo 
     : Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxRegenInfo(&Task_5msBus.Proxy_RxCanRxRegenInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxRegenInfo 
     : Proxy_RxCanRxRegenInfo.HcuRegenEna;
     : Proxy_RxCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxAccelPedlInfo(&Task_5msBus.Proxy_RxCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxAccelPedlInfo 
     : Proxy_RxCanRxAccelPedlInfo.AccelPedlVal;
     : Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxIdbInfo(&Task_5msBus.Proxy_RxCanRxIdbInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxIdbInfo 
     : Proxy_RxCanRxIdbInfo.EngMsgFault;
     : Proxy_RxCanRxIdbInfo.TarGearPosi;
     : Proxy_RxCanRxIdbInfo.GearSelDisp;
     : Proxy_RxCanRxIdbInfo.TcuSwiGs;
     : Proxy_RxCanRxIdbInfo.HcuServiceMod;
     : Proxy_RxCanRxIdbInfo.SubCanBusSigFault;
     : Proxy_RxCanRxIdbInfo.CoolantTemp;
     : Proxy_RxCanRxIdbInfo.CoolantTempErr;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxEngTempInfo(&Task_5msBus.Proxy_RxCanRxEngTempInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEngTempInfo 
     : Proxy_RxCanRxEngTempInfo.EngTemp;
     : Proxy_RxCanRxEngTempInfo.EngTempErr;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxEscInfo(&Task_5msBus.Proxy_RxCanRxEscInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEscInfo 
     : Proxy_RxCanRxEscInfo.Ax;
     : Proxy_RxCanRxEscInfo.YawRate;
     : Proxy_RxCanRxEscInfo.SteeringAngle;
     : Proxy_RxCanRxEscInfo.Ay;
     : Proxy_RxCanRxEscInfo.PbSwt;
     : Proxy_RxCanRxEscInfo.ClutchSwt;
     : Proxy_RxCanRxEscInfo.GearRSwt;
     : Proxy_RxCanRxEscInfo.EngActIndTq;
     : Proxy_RxCanRxEscInfo.EngRpm;
     : Proxy_RxCanRxEscInfo.EngIndTq;
     : Proxy_RxCanRxEscInfo.EngFrictionLossTq;
     : Proxy_RxCanRxEscInfo.EngStdTq;
     : Proxy_RxCanRxEscInfo.TurbineRpm;
     : Proxy_RxCanRxEscInfo.ThrottleAngle;
     : Proxy_RxCanRxEscInfo.TpsResol1000;
     : Proxy_RxCanRxEscInfo.PvAvCanResol1000;
     : Proxy_RxCanRxEscInfo.EngChr;
     : Proxy_RxCanRxEscInfo.EngVol;
     : Proxy_RxCanRxEscInfo.GearType;
     : Proxy_RxCanRxEscInfo.EngClutchState;
     : Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv;
     : Proxy_RxCanRxEscInfo.MotEstTq;
     : Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv;
     : Proxy_RxCanRxEscInfo.TqIntvTCU;
     : Proxy_RxCanRxEscInfo.TqIntvSlowTCU;
     : Proxy_RxCanRxEscInfo.TqIncReq;
     : Proxy_RxCanRxEscInfo.DecelReq;
     : Proxy_RxCanRxEscInfo.RainSnsStat;
     : Proxy_RxCanRxEscInfo.EngSpdErr;
     : Proxy_RxCanRxEscInfo.AtType;
     : Proxy_RxCanRxEscInfo.MtType;
     : Proxy_RxCanRxEscInfo.CvtType;
     : Proxy_RxCanRxEscInfo.DctType;
     : Proxy_RxCanRxEscInfo.HevAtTcu;
     : Proxy_RxCanRxEscInfo.TurbineRpmErr;
     : Proxy_RxCanRxEscInfo.ThrottleAngleErr;
     : Proxy_RxCanRxEscInfo.TopTrvlCltchSwtAct;
     : Proxy_RxCanRxEscInfo.TopTrvlCltchSwtActV;
     : Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtAct;
     : Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtActV;
     : Proxy_RxCanRxEscInfo.WiperIntSW;
     : Proxy_RxCanRxEscInfo.WiperLow;
     : Proxy_RxCanRxEscInfo.WiperHigh;
     : Proxy_RxCanRxEscInfo.WiperValid;
     : Proxy_RxCanRxEscInfo.WiperAuto;
     : Proxy_RxCanRxEscInfo.TcuFaultSts;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxEemInfo(&Task_5msBus.Proxy_RxCanRxEemInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEemInfo 
     : Proxy_RxCanRxEemInfo.YawRateInvld;
     : Proxy_RxCanRxEemInfo.AyInvld;
     : Proxy_RxCanRxEemInfo.SteeringAngleRxOk;
     : Proxy_RxCanRxEemInfo.AxInvldData;
     : Proxy_RxCanRxEemInfo.Ems1RxErr;
     : Proxy_RxCanRxEemInfo.Ems2RxErr;
     : Proxy_RxCanRxEemInfo.Tcu1RxErr;
     : Proxy_RxCanRxEemInfo.Tcu5RxErr;
     : Proxy_RxCanRxEemInfo.Hcu1RxErr;
     : Proxy_RxCanRxEemInfo.Hcu2RxErr;
     : Proxy_RxCanRxEemInfo.Hcu3RxErr;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxInfo(&Task_5msBus.Proxy_RxCanRxInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxInfo 
     : Proxy_RxCanRxInfo.MainBusOffFlag;
     : Proxy_RxCanRxInfo.SenBusOffFlag;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxIMUCalcInfo(&Task_5msBus.Proxy_RxIMUCalcInfo);
    /*==============================================================================
    * Members of structure Proxy_RxIMUCalcInfo 
     : Proxy_RxIMUCalcInfo.Reverse_Gear_flg;
     : Proxy_RxIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/

    Task_5ms_Write_Nvm_HndlrLogicEepDataInfo(&Task_5msBus.Nvm_HndlrLogicEepDataInfo);
    /*==============================================================================
    * Members of structure Nvm_HndlrLogicEepDataInfo 
     : Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KLatEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawEepMax;
     : Nvm_HndlrLogicEepDataInfo.KYawEepMin;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepMax;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepMin;
     : Nvm_HndlrLogicEepDataInfo.KLatEepMax;
     : Nvm_HndlrLogicEepDataInfo.KLatEepMin;
     : Nvm_HndlrLogicEepDataInfo.KYawStillEepMax;
     : Nvm_HndlrLogicEepDataInfo.KYawStillEepMin;
     : Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap;
     : Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep;
     : Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep;
     : Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep;
     : Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs;
     : Nvm_HndlrLogicEepDataInfo.ReadInvld;
     : Nvm_HndlrLogicEepDataInfo.WrReqCmpld;
     =============================================================================*/

    Task_5ms_Write_Wss_SenWhlSpdInfo(&Task_5msBus.Wss_SenWhlSpdInfo);
    /*==============================================================================
    * Members of structure Wss_SenWhlSpdInfo 
     : Wss_SenWhlSpdInfo.FlWhlSpd;
     : Wss_SenWhlSpdInfo.FrWhlSpd;
     : Wss_SenWhlSpdInfo.RlWhlSpd;
     : Wss_SenWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Task_5ms_Write_Wss_SenWhlEdgeCntInfo(&Task_5msBus.Wss_SenWhlEdgeCntInfo);
    /*==============================================================================
    * Members of structure Wss_SenWhlEdgeCntInfo 
     : Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt;
     =============================================================================*/

    Task_5ms_Write_Wss_SenWssSpeedOut(&Task_5msBus.Wss_SenWssSpeedOut);
    /*==============================================================================
    * Members of structure Wss_SenWssSpeedOut 
     : Wss_SenWssSpeedOut.WssMax;
     : Wss_SenWssSpeedOut.WssMin;
     =============================================================================*/

    Task_5ms_Write_Wss_SenWssCalcInfo(&Task_5msBus.Wss_SenWssCalcInfo);
    /*==============================================================================
    * Members of structure Wss_SenWssCalcInfo 
     : Wss_SenWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/

    Task_5ms_Write_Pedal_SenSyncPdt5msRawInfo(&Task_5msBus.Pedal_SenSyncPdt5msRawInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdt5msRawInfo 
     : Pedal_SenSyncPdt5msRawInfo.PdtSig;
     =============================================================================*/

    Task_5ms_Write_Pedal_SenSyncPdf5msRawInfo(&Task_5msBus.Pedal_SenSyncPdf5msRawInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdf5msRawInfo 
     : Pedal_SenSyncPdf5msRawInfo.PdfSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;
     =============================================================================*/

    Task_5ms_Write_Press_SenSyncPistP5msRawInfo(&Task_5msBus.Press_SenSyncPistP5msRawInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPistP5msRawInfo 
     : Press_SenSyncPistP5msRawInfo.PistPSig;
     =============================================================================*/

    Task_5ms_Write_Swt_SenEscSwtStInfo(&Task_5msBus.Swt_SenEscSwtStInfo);
    /*==============================================================================
    * Members of structure Swt_SenEscSwtStInfo 
     : Swt_SenEscSwtStInfo.EscDisabledBySwt;
     : Swt_SenEscSwtStInfo.TcsDisabledBySwt;
     : Swt_SenEscSwtStInfo.HdcEnabledBySwt;
     =============================================================================*/

    Task_5ms_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo(&Task_5msBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlIdbSnsrEolOfsCalcInfo 
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
     =============================================================================*/

    Task_5ms_Write_Bbc_CtrlBaseBrkCtrlModInfo(&Task_5msBus.Bbc_CtrlBaseBrkCtrlModInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlBaseBrkCtrlModInfo 
     : Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
     : Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg;
     : Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;
     =============================================================================*/

    Task_5ms_Write_Rbc_CtrlTarRgnBrkTqInfo(&Task_5msBus.Rbc_CtrlTarRgnBrkTqInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlTarRgnBrkTqInfo 
     : Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq;
     : Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep;
     : Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid;
     : Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce;
     : Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce;
     : Rbc_CtrlTarRgnBrkTqInfo.EhbStat;
     =============================================================================*/

    Task_5ms_Write_Abc_CtrlCanTxInfo(&Task_5msBus.Abc_CtrlCanTxInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlCanTxInfo 
     : Abc_CtrlCanTxInfo.TqIntvTCS;
     : Abc_CtrlCanTxInfo.TqIntvMsr;
     : Abc_CtrlCanTxInfo.TqIntvSlowTCS;
     : Abc_CtrlCanTxInfo.MinGear;
     : Abc_CtrlCanTxInfo.MaxGear;
     : Abc_CtrlCanTxInfo.TcsReq;
     : Abc_CtrlCanTxInfo.TcsCtrl;
     : Abc_CtrlCanTxInfo.AbsAct;
     : Abc_CtrlCanTxInfo.TcsGearShiftChr;
     : Abc_CtrlCanTxInfo.EspCtrl;
     : Abc_CtrlCanTxInfo.MsrReq;
     : Abc_CtrlCanTxInfo.TcsProductInfo;
     =============================================================================*/

    Task_5ms_Write_Abc_CtrlAbsCtrlInfo(&Task_5msBus.Abc_CtrlAbsCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlAbsCtrlInfo 
     : Abc_CtrlAbsCtrlInfo.AbsActFlg;
     : Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
     : Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
     : Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
     : Abc_CtrlAbsCtrlInfo.AbsDesTarP;
     : Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi;
     : Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsPrioReLe;
     : Abc_CtrlAbsCtrlInfo.AbsPrioReRi;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi;
     =============================================================================*/

    Task_5ms_Write_Abc_CtrlEscCtrlInfo(&Task_5msBus.Abc_CtrlEscCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlEscCtrlInfo 
     : Abc_CtrlEscCtrlInfo.EscActFlg;
     : Abc_CtrlEscCtrlInfo.EscDefectFlg;
     : Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
     : Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
     : Abc_CtrlEscCtrlInfo.EscTarPReLe;
     : Abc_CtrlEscCtrlInfo.EscTarPReRi;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeReLe;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeReRi;
     : Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe;
     : Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi;
     : Abc_CtrlEscCtrlInfo.EscTarPRateReLe;
     : Abc_CtrlEscCtrlInfo.EscTarPRateReRi;
     : Abc_CtrlEscCtrlInfo.EscPrioFrntLe;
     : Abc_CtrlEscCtrlInfo.EscPrioFrntRi;
     : Abc_CtrlEscCtrlInfo.EscPrioReLe;
     : Abc_CtrlEscCtrlInfo.EscPrioReRi;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi;
     : Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe;
     : Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi;
     : Abc_CtrlEscCtrlInfo.EscDelTarPReLe;
     : Abc_CtrlEscCtrlInfo.EscDelTarPReRi;
     =============================================================================*/

    Task_5ms_Write_Abc_CtrlTcsCtrlInfo(&Task_5msBus.Abc_CtrlTcsCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlTcsCtrlInfo 
     : Abc_CtrlTcsCtrlInfo.TcsActFlg;
     : Abc_CtrlTcsCtrlInfo.TcsDefectFlg;
     : Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe;
     : Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi;
     : Abc_CtrlTcsCtrlInfo.TcsTarPReLe;
     : Abc_CtrlTcsCtrlInfo.TcsTarPReRi;
     : Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi;
     =============================================================================*/

    Task_5ms_Write_Pct_5msCtrlStkRecvryActnIfInfo(&Task_5msBus.Pct_5msCtrlStkRecvryActnIfInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlStkRecvryActnIfInfo 
     : Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl;
     : Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
     : Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;
     =============================================================================*/

    Task_5ms_Write_Vat_CtrlIdbVlvActInfo(&Task_5msBus.Vat_CtrlIdbVlvActInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlIdbVlvActInfo 
     : Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK;
     : Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk;
     : Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg;
     =============================================================================*/

    Task_5ms_Write_Eem_MainEemFailData(&Task_5msBus.Eem_MainEemFailData);
    /*==============================================================================
    * Members of structure Eem_MainEemFailData 
     : Eem_MainEemFailData.Eem_Fail_BBSSol;
     : Eem_MainEemFailData.Eem_Fail_ESCSol;
     : Eem_MainEemFailData.Eem_Fail_FrontSol;
     : Eem_MainEemFailData.Eem_Fail_RearSol;
     : Eem_MainEemFailData.Eem_Fail_Motor;
     : Eem_MainEemFailData.Eem_Fail_MPS;
     : Eem_MainEemFailData.Eem_Fail_MGD;
     : Eem_MainEemFailData.Eem_Fail_BBSValveRelay;
     : Eem_MainEemFailData.Eem_Fail_ESCValveRelay;
     : Eem_MainEemFailData.Eem_Fail_ECUHw;
     : Eem_MainEemFailData.Eem_Fail_ASIC;
     : Eem_MainEemFailData.Eem_Fail_OverVolt;
     : Eem_MainEemFailData.Eem_Fail_UnderVolt;
     : Eem_MainEemFailData.Eem_Fail_LowVolt;
     : Eem_MainEemFailData.Eem_Fail_LowerVolt;
     : Eem_MainEemFailData.Eem_Fail_SenPwr_12V;
     : Eem_MainEemFailData.Eem_Fail_SenPwr_5V;
     : Eem_MainEemFailData.Eem_Fail_Yaw;
     : Eem_MainEemFailData.Eem_Fail_Ay;
     : Eem_MainEemFailData.Eem_Fail_Ax;
     : Eem_MainEemFailData.Eem_Fail_Str;
     : Eem_MainEemFailData.Eem_Fail_CirP1;
     : Eem_MainEemFailData.Eem_Fail_CirP2;
     : Eem_MainEemFailData.Eem_Fail_SimP;
     : Eem_MainEemFailData.Eem_Fail_BLS;
     : Eem_MainEemFailData.Eem_Fail_ESCSw;
     : Eem_MainEemFailData.Eem_Fail_HDCSw;
     : Eem_MainEemFailData.Eem_Fail_AVHSw;
     : Eem_MainEemFailData.Eem_Fail_BrakeLampRelay;
     : Eem_MainEemFailData.Eem_Fail_EssRelay;
     : Eem_MainEemFailData.Eem_Fail_GearR;
     : Eem_MainEemFailData.Eem_Fail_Clutch;
     : Eem_MainEemFailData.Eem_Fail_ParkBrake;
     : Eem_MainEemFailData.Eem_Fail_PedalPDT;
     : Eem_MainEemFailData.Eem_Fail_PedalPDF;
     : Eem_MainEemFailData.Eem_Fail_BrakeFluid;
     : Eem_MainEemFailData.Eem_Fail_TCSTemp;
     : Eem_MainEemFailData.Eem_Fail_HDCTemp;
     : Eem_MainEemFailData.Eem_Fail_SCCTemp;
     : Eem_MainEemFailData.Eem_Fail_TVBBTemp;
     : Eem_MainEemFailData.Eem_Fail_MainCanLine;
     : Eem_MainEemFailData.Eem_Fail_SubCanLine;
     : Eem_MainEemFailData.Eem_Fail_EMSTimeOut;
     : Eem_MainEemFailData.Eem_Fail_FWDTimeOut;
     : Eem_MainEemFailData.Eem_Fail_TCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_HCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_MCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_VariantCoding;
     : Eem_MainEemFailData.Eem_Fail_WssFL;
     : Eem_MainEemFailData.Eem_Fail_WssFR;
     : Eem_MainEemFailData.Eem_Fail_WssRL;
     : Eem_MainEemFailData.Eem_Fail_WssRR;
     : Eem_MainEemFailData.Eem_Fail_SameSideWss;
     : Eem_MainEemFailData.Eem_Fail_DiagonalWss;
     : Eem_MainEemFailData.Eem_Fail_FrontWss;
     : Eem_MainEemFailData.Eem_Fail_RearWss;
     =============================================================================*/

    //Task_5ms_Write_Eem_MainEemCtrlInhibitData(&Task_5msBus.Eem_MainEemCtrlInhibitData);
    /*==============================================================================
    * Members of structure Eem_MainEemCtrlInhibitData 
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/

    Task_5ms_Write_Eem_MainEemSuspectData(&Task_5msBus.Eem_MainEemSuspectData);
    /*==============================================================================
    * Members of structure Eem_MainEemSuspectData 
     : Eem_MainEemSuspectData.Eem_Suspect_WssFL;
     : Eem_MainEemSuspectData.Eem_Suspect_WssFR;
     : Eem_MainEemSuspectData.Eem_Suspect_WssRL;
     : Eem_MainEemSuspectData.Eem_Suspect_WssRR;
     : Eem_MainEemSuspectData.Eem_Suspect_SameSideWss;
     : Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss;
     : Eem_MainEemSuspectData.Eem_Suspect_FrontWss;
     : Eem_MainEemSuspectData.Eem_Suspect_RearWss;
     : Eem_MainEemSuspectData.Eem_Suspect_BBSSol;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCSol;
     : Eem_MainEemSuspectData.Eem_Suspect_FrontSol;
     : Eem_MainEemSuspectData.Eem_Suspect_RearSol;
     : Eem_MainEemSuspectData.Eem_Suspect_Motor;
     : Eem_MainEemSuspectData.Eem_Suspect_MPS;
     : Eem_MainEemSuspectData.Eem_Suspect_MGD;
     : Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_ECUHw;
     : Eem_MainEemSuspectData.Eem_Suspect_ASIC;
     : Eem_MainEemSuspectData.Eem_Suspect_OverVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_UnderVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_LowVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V;
     : Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V;
     : Eem_MainEemSuspectData.Eem_Suspect_Yaw;
     : Eem_MainEemSuspectData.Eem_Suspect_Ay;
     : Eem_MainEemSuspectData.Eem_Suspect_Ax;
     : Eem_MainEemSuspectData.Eem_Suspect_Str;
     : Eem_MainEemSuspectData.Eem_Suspect_CirP1;
     : Eem_MainEemSuspectData.Eem_Suspect_CirP2;
     : Eem_MainEemSuspectData.Eem_Suspect_SimP;
     : Eem_MainEemSuspectData.Eem_Suspect_BS;
     : Eem_MainEemSuspectData.Eem_Suspect_BLS;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCSw;
     : Eem_MainEemSuspectData.Eem_Suspect_HDCSw;
     : Eem_MainEemSuspectData.Eem_Suspect_AVHSw;
     : Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_EssRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_GearR;
     : Eem_MainEemSuspectData.Eem_Suspect_Clutch;
     : Eem_MainEemSuspectData.Eem_Suspect_ParkBrake;
     : Eem_MainEemSuspectData.Eem_Suspect_PedalPDT;
     : Eem_MainEemSuspectData.Eem_Suspect_PedalPDF;
     : Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid;
     : Eem_MainEemSuspectData.Eem_Suspect_TCSTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_HDCTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_SCCTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_TVBBTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_MainCanLine;
     : Eem_MainEemSuspectData.Eem_Suspect_SubCanLine;
     : Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_VariantCoding;
     =============================================================================*/

    Task_5ms_Write_Eem_MainEemEceData(&Task_5msBus.Eem_MainEemEceData);
    /*==============================================================================
    * Members of structure Eem_MainEemEceData 
     : Eem_MainEemEceData.Eem_Ece_Wss;
     : Eem_MainEemEceData.Eem_Ece_Yaw;
     : Eem_MainEemEceData.Eem_Ece_Ay;
     : Eem_MainEemEceData.Eem_Ece_Ax;
     : Eem_MainEemEceData.Eem_Ece_Cir1P;
     : Eem_MainEemEceData.Eem_Ece_Cir2P;
     : Eem_MainEemEceData.Eem_Ece_SimP;
     : Eem_MainEemEceData.Eem_Ece_Bls;
     : Eem_MainEemEceData.Eem_Ece_Pedal;
     : Eem_MainEemEceData.Eem_Ece_Motor;
     : Eem_MainEemEceData.Eem_Ece_Vdc_Sw;
     : Eem_MainEemEceData.Eem_Ece_Hdc_Sw;
     : Eem_MainEemEceData.Eem_Ece_GearR_Sw;
     : Eem_MainEemEceData.Eem_Ece_Clutch_Sw;
     =============================================================================*/

    Task_5ms_Write_Eem_MainEemLampData(&Task_5msBus.Eem_MainEemLampData);
    /*==============================================================================
    * Members of structure Eem_MainEemLampData 
     : Eem_MainEemLampData.Eem_Lamp_EBDLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ABSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_HDCLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_BBSBuzzorRequest;
     : Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AHBLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSFuncLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AVHLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AVHILampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ACCEnable;
     : Eem_MainEemLampData.Eem_Lamp_CDMEnable;
     : Eem_MainEemLampData.Eem_Buzzor_On;
     =============================================================================*/

    Task_5ms_Write_Eem_SuspcDetnCanTimeOutStInfo(&Task_5msBus.Eem_SuspcDetnCanTimeOutStInfo);
    /*==============================================================================
    * Members of structure Eem_SuspcDetnCanTimeOutStInfo 
     : Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet;
     : Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet;
     : Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet;
     =============================================================================*/

    Task_5ms_Write_Arbitrator_VlvArbWhlVlvReqInfo(&Task_5msBus.Arbitrator_VlvArbWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbWhlVlvReqInfo 
     : Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    Task_5ms_Write_Fsr_ActrFsrAbsDrvInfo(&Task_5msBus.Fsr_ActrFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure Fsr_ActrFsrAbsDrvInfo 
     : Fsr_ActrFsrAbsDrvInfo.FsrAbsOff;
     : Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv;
     =============================================================================*/

    Task_5ms_Write_Fsr_ActrFsrCbsDrvInfo(&Task_5msBus.Fsr_ActrFsrCbsDrvInfo);
    /*==============================================================================
    * Members of structure Fsr_ActrFsrCbsDrvInfo 
     : Fsr_ActrFsrCbsDrvInfo.FsrCbsOff;
     : Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv;
     =============================================================================*/

    Task_5ms_Write_Rly_ActrRlyDrvInfo(&Task_5msBus.Rly_ActrRlyDrvInfo);
    /*==============================================================================
    * Members of structure Rly_ActrRlyDrvInfo 
     : Rly_ActrRlyDrvInfo.RlyDbcDrv;
     : Rly_ActrRlyDrvInfo.RlyEssDrv;
     =============================================================================*/

    Task_5ms_Write_Vlv_ActrSyncNormVlvReqInfo(&Task_5msBus.Vlv_ActrSyncNormVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncNormVlvReqInfo 
     : Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData;
     =============================================================================*/

    Task_5ms_Write_Vlv_ActrSyncWhlVlvReqInfo(&Task_5msBus.Vlv_ActrSyncWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncWhlVlvReqInfo 
     : Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    Task_5ms_Write_Vlv_ActrSyncBalVlvReqInfo(&Task_5msBus.Vlv_ActrSyncBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncBalVlvReqInfo 
     : Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData;
     : Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData;
     : Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData;
     =============================================================================*/

    Task_5ms_Write_Proxy_RxCanRxGearSelDispErrInfo(&Task_5msBus.Proxy_RxCanRxGearSelDispErrInfo);
    Task_5ms_Write_Prly_HndlrIgnOnOffSts(&Task_5msBus.Prly_HndlrIgnOnOffSts);
    Task_5ms_Write_Prly_HndlrIgnEdgeSts(&Task_5msBus.Prly_HndlrIgnEdgeSts);
    //Task_5ms_Write_Prly_HndlrPrlyEcuInhibit(&Task_5msBus.Prly_HndlrPrlyEcuInhibit);
    //Task_5ms_Write_Mom_HndlrEcuModeSts(&Task_5msBus.Mom_HndlrEcuModeSts);
    Task_5ms_Write_Det_5msCtrlVehSpdFild(&Task_5msBus.Det_5msCtrlVehSpdFild);
    Task_5ms_Write_Abc_CtrlVehSpd(&Task_5msBus.Abc_CtrlVehSpd);
    Task_5ms_Write_Pct_5msCtrlTgtDeltaStkType(&Task_5msBus.Pct_5msCtrlTgtDeltaStkType);
    Task_5ms_Write_Pct_5msCtrlPCtrlBoostMod(&Task_5msBus.Pct_5msCtrlPCtrlBoostMod);
    Task_5ms_Write_Pct_5msCtrlPCtrlFadeoutSt(&Task_5msBus.Pct_5msCtrlPCtrlFadeoutSt);
    Task_5ms_Write_Pct_5msCtrlPCtrlSt(&Task_5msBus.Pct_5msCtrlPCtrlSt);
    Task_5ms_Write_Pct_5msCtrlInVlvAllCloseReq(&Task_5msBus.Pct_5msCtrlInVlvAllCloseReq);
    Task_5ms_Write_Pct_5msCtrlPChamberVolume(&Task_5msBus.Pct_5msCtrlPChamberVolume);
    Task_5ms_Write_Pct_5msCtrlTarDeltaStk(&Task_5msBus.Pct_5msCtrlTarDeltaStk);
    Task_5ms_Write_Pct_5msCtrlFinalTarPFromPCtrl(&Task_5msBus.Pct_5msCtrlFinalTarPFromPCtrl);
   /* Task_5ms_Write_Eem_SuspcDetnFuncInhibitVlvSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitVlvSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitIocSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitIocSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitProxySts(&Task_5msBus.Eem_SuspcDetnFuncInhibitProxySts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitDiagSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitDiagSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitAcmioSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitAcmioSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitAcmctlSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitAcmctlSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitMspSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitMspSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitPedalSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitPedalSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitPressSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitPressSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitSesSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitSesSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitVlvdSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitVlvdSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitAdcifSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitAdcifSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitCanTrcvSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitCanTrcvSts);
    Task_5ms_Write_Eem_SuspcDetnFuncInhibitMccSts(&Task_5msBus.Eem_SuspcDetnFuncInhibitMccSts);*/
    Task_5ms_Write_Arbitrator_VlvArbVlvDriveState(&Task_5msBus.Arbitrator_VlvArbVlvDriveState);
    Task_5ms_Write_Fsr_ActrFsrDcMtrShutDwn(&Task_5msBus.Fsr_ActrFsrDcMtrShutDwn);
    Task_5ms_Write_Fsr_ActrFsrEnDrDrv(&Task_5msBus.Fsr_ActrFsrEnDrDrv);
    Task_5ms_Write_Vlv_ActrSyncVlvSync(&Task_5msBus.Vlv_ActrSyncVlvSync);

    if (SystemStabilzationTimeCnt < U8_SYSTEM_STABILIZATION_TIME) // SystemStabilzationTimeCnt increase to U8_SYSTEM_STABILIZATION_TIME in maximum.
    	SystemStabilzationTimeCnt++;	
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TASK_5MS_STOP_SEC_CODE
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
