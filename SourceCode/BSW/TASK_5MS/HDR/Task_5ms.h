/**
 * @defgroup Task_5ms Task_5ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_5ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_5MS_H_
#define TASK_5MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_5ms_Types.h"
#include "Task_5ms_Cfg.h"
#include "Task_5ms_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define TASK_5MS_MODULE_ID      (0)
 #define TASK_5MS_MAJOR_VERSION  (2)
 #define TASK_5MS_MINOR_VERSION  (0)
 #define TASK_5MS_PATCH_VERSION  (0)
 #define TASK_5MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Task_5ms_HdrBusType Task_5msBus;

/* Version Info */
extern const SwcVersionInfo_t Task_5msVersionInfo;

/* Input Data Element */
extern SenPwrM_MainSenPwrMonitor_t Task_5ms_Prly_HndlrSenPwrMonitorData;
extern Ach_InputAchWssPort0AsicInfo_t Task_5ms_Wss_SenAchWssPort0AsicInfo;
extern Ach_InputAchWssPort1AsicInfo_t Task_5ms_Wss_SenAchWssPort1AsicInfo;
extern Ach_InputAchWssPort2AsicInfo_t Task_5ms_Wss_SenAchWssPort2AsicInfo;
extern Ach_InputAchWssPort3AsicInfo_t Task_5ms_Wss_SenAchWssPort3AsicInfo;
extern Pedal_SenPdtBufInfo_t Task_5ms_Pedal_SenSyncPdtBufInfo;
extern Pedal_SenPdfBufInfo_t Task_5ms_Pedal_SenSyncPdfBufInfo;
extern Press_SenCircPBufInfo_t Task_5ms_Press_SenSyncCircPBufInfo;
extern Press_SenPistPBufInfo_t Task_5ms_Press_SenSyncPistPBufInfo;
extern Press_SenPspBufInfo_t Task_5ms_Press_SenSyncPspBufInfo;
extern Ioc_InputCS1msSwtMonInfo_t Task_5ms_Swt_SenSwtMonInfo;
extern Ioc_InputCS1msSwtMonInfoEsc_t Task_5ms_Swt_SenSwtMonInfoEsc;
extern Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Task_5ms_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Task_5ms_Det_5msCtrlMotRotgAgSigInfo;
extern Det_1msCtrlBrkPedlStatus1msInfo_t Task_5ms_Bbc_CtrlBrkPedlStatus1msInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Task_5ms_Abc_CtrlMotRotgAgSigInfo;
extern Diag_HndlrSasCalInfo_t Task_5ms_Abc_CtrlSasCalInfo;
extern Proxy_TxTxESCSensorInfo_t Task_5ms_Abc_CtrlTxESCSensorInfo;
extern SenPwrM_MainSenPwrMonitor_t Task_5ms_Abc_CtrlSenPwrMonitorData;
extern YawM_MainYAWMSerialInfo_t Task_5ms_Abc_CtrlYAWMSerialInfo;
extern YawM_MainYAWMOutInfo_t Task_5ms_Abc_CtrlYAWMOutInfo;
extern Spc_1msCtrlPedlTrvlFild1msInfo_t Task_5ms_Pct_5msCtrlPedlTrvlFild1msInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Task_5ms_Pct_5msCtrlMotRotgAgSigInfo;
extern Det_1msCtrlBrkPedlStatus1msInfo_t Task_5ms_Pct_5msCtrlBrkPedlStatus1msInfo;
extern Det_1msCtrlPedlTrvlRate1msInfo_t Task_5ms_Pct_5msCtrlPedlTrvlRate1msInfo;
extern Mcc_1msCtrlIdbMotPosnInfo_t Task_5ms_Pct_5msCtrlIdbMotPosnInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_5ms_SysPwrM_MainAchSysPwrAsicInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_5ms_EcuHwCtrlM_MainAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t Task_5ms_EcuHwCtrlM_MainAchValveAsicInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_5ms_WssM_MainAchSysPwrAsicInfo;
extern Ach_InputAchWssPort0AsicInfo_t Task_5ms_WssM_MainAchWssPort0AsicInfo;
extern Ach_InputAchWssPort1AsicInfo_t Task_5ms_WssM_MainAchWssPort1AsicInfo;
extern Ach_InputAchWssPort2AsicInfo_t Task_5ms_WssM_MainAchWssPort2AsicInfo;
extern Ach_InputAchWssPort3AsicInfo_t Task_5ms_WssM_MainAchWssPort3AsicInfo;
extern Ioc_InputSR1msHalPressureInfo_t Task_5ms_PressM_MainHalPressureInfo;
extern SentH_MainSentHPressureInfo_t Task_5ms_PressM_MainSentHPressureInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_5ms_PedalM_MainAchSysPwrAsicInfo;
extern Ioc_InputSR1msPedlSigMonInfo_t Task_5ms_PedalM_MainPedlSigMonInfo;
extern Ioc_InputSR1msPedlPwrMonInfo_t Task_5ms_PedalM_MainPedlPwrMonInfo;
extern Press_SenPressCalcInfo_t Task_5ms_PressP_MainPressCalcInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_5ms_BbsVlvM_MainAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t Task_5ms_BbsVlvM_MainAchValveAsicInfo;
extern Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Task_5ms_BbsVlvM_MainVlvdFF0DcdInfo;
extern Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Task_5ms_BbsVlvM_MainVlvdFF1DcdInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_5ms_AbsVlvM_MainAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t Task_5ms_AbsVlvM_MainAchValveAsicInfo;
extern Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Task_5ms_Mps_TLE5012M_MainMpsD1SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Task_5ms_Mps_TLE5012M_MainMpsD2SpiDcdInfo;
extern Mgd_TLE9180_HndlrMgdDcdInfo_t Task_5ms_MgdM_MainMgdDcdInfo;
extern Ioc_InputSR1msMotMonInfo_t Task_5ms_MtrM_MainMotMonInfo;
extern Ioc_InputSR1msMotVoltsMonInfo_t Task_5ms_MtrM_MainMotVoltsMonInfo;
extern Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Task_5ms_MtrM_MainMpsD1SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Task_5ms_MtrM_MainMpsD2SpiDcdInfo;
extern Mgd_TLE9180_HndlrMgdDcdInfo_t Task_5ms_MtrM_MainMgdDcdInfo;
extern Mcc_1msCtrlMotDqIRefMccInfo_t Task_5ms_MtrM_MainMotDqIRefMccInfo;
extern AdcIf_Conv50usHwTrigMotInfo_t Task_5ms_MtrM_MainHwTrigMotInfo;
extern Ioc_InputSR50usMotCurrMonInfo_t Task_5ms_MtrM_MainMotCurrMonInfo;
extern Ioc_InputSR50usMotAngleMonInfo_t Task_5ms_MtrM_MainMotAngleMonInfo;
extern Mtr_Processing_SigMtrProcessOutInfo_t Task_5ms_MtrM_MainMtrProcessOutInfo;
extern RlyM_MainRelayMonitor_t Task_5ms_Eem_MainRelayMonitorData;
extern SenPwrM_MainSenPwrMonitor_t Task_5ms_Eem_MainSenPwrMonitorData;
extern SwtM_MainSwtMonFaultInfo_t Task_5ms_Eem_MainSwtMonFaultInfo;
extern SwtP_MainSwtPFaultInfo_t Task_5ms_Eem_MainSwtPFaultInfo;
extern YawM_MainYAWMOutInfo_t Task_5ms_Eem_MainYAWMOutInfo;
extern AyM_MainAYMOutInfo_t Task_5ms_Eem_MainAYMOuitInfo;
extern AxM_MainAXMOutInfo_t Task_5ms_Eem_MainAXMOutInfo;
extern SasM_MainSASMOutInfo_t Task_5ms_Eem_MainSASMOutInfo;
extern CanM_MainCanMonData_t Task_5ms_Eem_MainCanMonData;
extern YawP_MainYawPlauOutput_t Task_5ms_Eem_MainYawPlauOutput;
extern AxP_MainAxPlauOutput_t Task_5ms_Eem_MainAxPlauOutput;
extern AyP_MainAyPlauOutput_t Task_5ms_Eem_MainAyPlauOutput;
extern SasP_MainSasPlauOutput_t Task_5ms_Eem_MainSasPlauOutput;
extern Ses_CtrlNormVlvReqSesInfo_t Task_5ms_Arbitrator_VlvNormVlvReqSesInfo;
extern Ses_CtrlWhlVlvReqSesInfo_t Task_5ms_Arbitrator_VlvWhlVlvReqSesInfo;
extern Diag_HndlrDiagVlvActr_t Task_5ms_Arbitrator_VlvDiagVlvActrInfo;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Ioc_InputSR5msAcmAsicInitCompleteFlag;
extern Ioc_InputSR1msCEMon_t Task_5ms_Prly_HndlrCEMon;
extern Ioc_InputCS1msRsmDbcMon_t Task_5ms_Swt_SenRsmDbcMon;
extern Mcc_1msCtrlMotCtrlState_t Task_5ms_Spc_5msCtrlMotCtrlState;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_Abc_CtrlVBatt1Mon;
extern Diag_HndlrDiagSci_t Task_5ms_Abc_CtrlDiagSci;
extern Mcc_1msCtrlMotCtrlMode_t Task_5ms_Pct_5msCtrlMotCtrlMode;
extern Mcc_1msCtrlMotICtrlFadeOutState_t Task_5ms_Pct_5msCtrlMotICtrlFadeOutState;
extern Pct_1msCtrlPreBoostMod_t Task_5ms_Pct_5msCtrlPreBoostMod;
extern Mcc_1msCtrlStkRecvryStabnEndOK_t Task_5ms_Pct_5msCtrlStkRecvryStabnEndOK;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_SysPwrM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Task_5ms_SysPwrM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t Task_5ms_SysPwrM_MainDiagClrSrs;
extern Ioc_InputSR1msVdd1Mon_t Task_5ms_SysPwrM_MainVdd1Mon;
extern Ioc_InputSR1msVdd2Mon_t Task_5ms_SysPwrM_MainVdd2Mon;
extern Ioc_InputSR1msVdd3Mon_t Task_5ms_SysPwrM_MainVdd3Mon;
extern Ioc_InputSR1msVdd4Mon_t Task_5ms_SysPwrM_MainVdd4Mon;
extern Ioc_InputSR1msVdd5Mon_t Task_5ms_SysPwrM_MainVdd5Mon;
extern Ioc_InputSR1msVddMon_t Task_5ms_SysPwrM_MainVddMon;
extern Arbitrator_MtrMtrArbDriveState_t Task_5ms_SysPwrM_MainMtrArbDriveState;
extern Ach_InputAchAsicInvalidInfo_t Task_5ms_EcuHwCtrlM_MainAchAsicInvalid;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_EcuHwCtrlM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Task_5ms_EcuHwCtrlM_MainDiagClrSrs;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
extern Ioc_InputSR1msVdd1Mon_t Task_5ms_EcuHwCtrlM_MainVdd1Mon;
extern Ioc_InputSR1msVdd2Mon_t Task_5ms_EcuHwCtrlM_MainVdd2Mon;
extern Ioc_InputSR1msVdd3Mon_t Task_5ms_EcuHwCtrlM_MainVdd3Mon;
extern Ioc_InputSR1msVdd4Mon_t Task_5ms_EcuHwCtrlM_MainVdd4Mon;
extern Ioc_InputSR1msVdd5Mon_t Task_5ms_EcuHwCtrlM_MainVdd5Mon;
extern AdcIf_Conv1msAdcInvalid_t Task_5ms_EcuHwCtrlM_MainAdcInvalid;
extern Ach_InputAchAsicInvalidInfo_t Task_5ms_WssM_MainAchAsicInvalid;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_WssM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Task_5ms_WssM_MainDiagClrSrs;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_PressM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Task_5ms_PressM_MainDiagClrSrs;
extern Ach_InputAchAsicInvalidInfo_t Task_5ms_PedalM_MainAchAsicInvalid;
extern Diag_HndlrDiagClr_t Task_5ms_PedalM_MainDiagClrSrs;
extern Ioc_InputSR1msVdd3Mon_t Task_5ms_PedalM_MainVdd3Mon;
extern Diag_HndlrDiagClr_t Task_5ms_WssP_MainDiagClrSrs;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_PressP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Task_5ms_PressP_MainDiagClrSrs;
extern Diag_HndlrDiagClr_t Task_5ms_PedalP_MainDiagClrSrs;
extern Ach_InputAchAsicInvalidInfo_t Task_5ms_BbsVlvM_MainAchAsicInvalid;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_BbsVlvM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Task_5ms_BbsVlvM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t Task_5ms_BbsVlvM_MainDiagClrSrs;
extern Ioc_InputSR1msFspCbsHMon_t Task_5ms_BbsVlvM_MainFspCbsHMon;
extern Ioc_InputSR1msFspCbsMon_t Task_5ms_BbsVlvM_MainFspCbsMon;
extern Vlvd_A3944_HndlrVlvdInvalid_t Task_5ms_BbsVlvM_MainVlvdInvalid;
extern Ach_InputAchAsicInvalidInfo_t Task_5ms_AbsVlvM_MainAchAsicInvalid;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_AbsVlvM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Task_5ms_AbsVlvM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t Task_5ms_AbsVlvM_MainDiagClrSrs;
extern Ioc_InputSR1msFspAbsHMon_t Task_5ms_AbsVlvM_MainFspAbsHMon;
extern Ioc_InputSR1msFspAbsMon_t Task_5ms_AbsVlvM_MainFspAbsMon;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_Mps_TLE5012M_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Task_5ms_Mps_TLE5012M_MainDiagClrSrs;
extern Mps_TLE5012_Hndlr_1msMpsInvalid_t Task_5ms_Mps_TLE5012M_MainMpsInvalid;
extern Arbitrator_MtrMtrArbDriveState_t Task_5ms_Mps_TLE5012M_MainMtrArbDriveState;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_MgdM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Task_5ms_MgdM_MainDiagClrSrs;
extern Mgd_TLE9180_HndlrMgdInvalid_t Task_5ms_MgdM_MainMgdInvalid;
extern Arbitrator_MtrMtrArbDriveState_t Task_5ms_MgdM_MainMtrArbDriveState;
extern Ioc_InputSR1msVBatt1Mon_t Task_5ms_MtrM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Task_5ms_MtrM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t Task_5ms_MtrM_MainDiagClrSrs;
extern Mgd_TLE9180_HndlrMgdInvalid_t Task_5ms_MtrM_MainMgdInvalid;
extern Arbitrator_MtrMtrArbDriveState_t Task_5ms_MtrM_MainMtrArbDriveState;
extern Diag_HndlrDiagClr_t Task_5ms_Watchdog_MainDiagClrSrs;
extern Diag_HndlrDiagClr_t Task_5ms_Eem_MainDiagClrSrs;
extern Diag_HndlrDiagSci_t Task_5ms_Eem_MainDiagSci;
extern Diag_HndlrDiagAhbSci_t Task_5ms_Eem_MainDiagAhbSci;
extern Diag_HndlrDiagClr_t Task_5ms_Eem_SuspcDetnDiagClrSrs;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Arbitrator_VlvAcmAsicInitCompleteFlag;
extern Diag_HndlrDiagSci_t Task_5ms_Arbitrator_VlvDiagSci;
extern Diag_HndlrDiagAhbSci_t Task_5ms_Arbitrator_VlvDiagAhbSci;
extern Diag_HndlrDiagSci_t Task_5ms_Arbitrator_RlyDiagSci;
extern Diag_HndlrDiagAhbSci_t Task_5ms_Arbitrator_RlyDiagAhbSci;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Fsr_ActrAcmAsicInitCompleteFlag;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Vlv_ActrSyncAcmAsicInitCompleteFlag;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_5ms_Ioc_OutputCS5msAcmAsicInitCompleteFlag;

/* Output Data Element */
extern Proxy_RxByComRxYawSerialInfo_t Task_5ms_Proxy_RxByComRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t Task_5ms_Proxy_RxByComRxYawAccInfo;
extern Proxy_RxByComRxSasInfo_t Task_5ms_Proxy_RxByComRxSasInfo;
extern Proxy_RxByComRxLongAccInfo_t Task_5ms_Proxy_RxByComRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
extern Proxy_RxCanRxRegenInfo_t Task_5ms_Proxy_RxCanRxRegenInfo;
extern Proxy_RxCanRxAccelPedlInfo_t Task_5ms_Proxy_RxCanRxAccelPedlInfo;
extern Proxy_RxCanRxIdbInfo_t Task_5ms_Proxy_RxCanRxIdbInfo;
extern Proxy_RxCanRxEngTempInfo_t Task_5ms_Proxy_RxCanRxEngTempInfo;
extern Proxy_RxCanRxEscInfo_t Task_5ms_Proxy_RxCanRxEscInfo;
extern Proxy_RxCanRxEemInfo_t Task_5ms_Proxy_RxCanRxEemInfo;
extern Proxy_RxCanRxInfo_t Task_5ms_Proxy_RxCanRxInfo;
extern Proxy_RxIMUCalc_t Task_5ms_Proxy_RxIMUCalcInfo;
extern Nvm_HndlrLogicEepDataInfo_t Task_5ms_Nvm_HndlrLogicEepDataInfo;
extern Wss_SenWhlSpdInfo_t Task_5ms_Wss_SenWhlSpdInfo;
extern Wss_SenWhlEdgeCntInfo_t Task_5ms_Wss_SenWhlEdgeCntInfo;
extern Wss_SenWssSpeedOut_t Task_5ms_Wss_SenWssSpeedOut;
extern Wss_SenWssCalc_t Task_5ms_Wss_SenWssCalcInfo;
extern Pedal_SenSyncPdt5msRawInfo_t Task_5ms_Pedal_SenSyncPdt5msRawInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Task_5ms_Pedal_SenSyncPdf5msRawInfo;
extern Press_SenSyncPistP5msRawInfo_t Task_5ms_Press_SenSyncPistP5msRawInfo;
extern Swt_SenEscSwtStInfo_t Task_5ms_Swt_SenEscSwtStInfo;
extern Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Task_5ms_Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t Task_5ms_Bbc_CtrlBaseBrkCtrlModInfo;
extern Rbc_CtrlTarRgnBrkTqInfo_t Task_5ms_Rbc_CtrlTarRgnBrkTqInfo;
extern Abc_CtrlCanTxInfo_t Task_5ms_Abc_CtrlCanTxInfo;
extern Abc_CtrlAbsCtrlInfo_t Task_5ms_Abc_CtrlAbsCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Task_5ms_Abc_CtrlEscCtrlInfo;
extern Abc_CtrlTcsCtrlInfo_t Task_5ms_Abc_CtrlTcsCtrlInfo;
extern Pct_5msCtrlStkRecvryActnIfInfo_t Task_5ms_Pct_5msCtrlStkRecvryActnIfInfo;
extern Vat_CtrlIdbVlvActInfo_t Task_5ms_Vat_CtrlIdbVlvActInfo;
extern Eem_MainEemFailData_t Task_5ms_Eem_MainEemFailData;
extern Eem_MainEemCtrlInhibitData_t Task_5ms_Eem_MainEemCtrlInhibitData;
extern Eem_MainEemSuspectData_t Task_5ms_Eem_MainEemSuspectData;
extern Eem_MainEemEceData_t Task_5ms_Eem_MainEemEceData;
extern Eem_MainEemLampData_t Task_5ms_Eem_MainEemLampData;
extern Eem_SuspcDetnCanTimeOutStInfo_t Task_5ms_Eem_SuspcDetnCanTimeOutStInfo;
extern Arbitrator_VlvArbWhlVlvReqInfo_t Task_5ms_Arbitrator_VlvArbWhlVlvReqInfo;
extern Fsr_ActrFsrAbsDrvInfo_t Task_5ms_Fsr_ActrFsrAbsDrvInfo;
extern Fsr_ActrFsrCbsDrvInfo_t Task_5ms_Fsr_ActrFsrCbsDrvInfo;
extern Rly_ActrRlyDrvInfo_t Task_5ms_Rly_ActrRlyDrvInfo;
extern Vlv_ActrSyncNormVlvReqInfo_t Task_5ms_Vlv_ActrSyncNormVlvReqInfo;
extern Vlv_ActrSyncWhlVlvReqInfo_t Task_5ms_Vlv_ActrSyncWhlVlvReqInfo;
extern Vlv_ActrSyncBalVlvReqInfo_t Task_5ms_Vlv_ActrSyncBalVlvReqInfo;
extern Proxy_RxCanRxGearSelDispErrInfo_t Task_5ms_Proxy_RxCanRxGearSelDispErrInfo;
extern Prly_HndlrIgnOnOffSts_t Task_5ms_Prly_HndlrIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_5ms_Prly_HndlrIgnEdgeSts;
extern Prly_HndlrPrlyEcuInhibit_t Task_5ms_Prly_HndlrPrlyEcuInhibit;
extern Mom_HndlrEcuModeSts_t Task_5ms_Mom_HndlrEcuModeSts;
extern Det_5msCtrlVehSpdFild_t Task_5ms_Det_5msCtrlVehSpdFild;
extern Abc_CtrlVehSpd_t Task_5ms_Abc_CtrlVehSpd;
extern Pct_5msCtrlTgtDeltaStkType_t Task_5ms_Pct_5msCtrlTgtDeltaStkType;
extern Pct_5msCtrlPCtrlBoostMod_t Task_5ms_Pct_5msCtrlPCtrlBoostMod;
extern Pct_5msCtrlPCtrlFadeoutSt_t Task_5ms_Pct_5msCtrlPCtrlFadeoutSt;
extern Pct_5msCtrlPCtrlSt_t Task_5ms_Pct_5msCtrlPCtrlSt;
extern Pct_5msCtrlInVlvAllCloseReq_t Task_5ms_Pct_5msCtrlInVlvAllCloseReq;
extern Pct_5msCtrlPChamberVolume_t Task_5ms_Pct_5msCtrlPChamberVolume;
extern Pct_5msCtrlTarDeltaStk_t Task_5ms_Pct_5msCtrlTarDeltaStk;
extern Pct_5msCtrlFinalTarPFromPCtrl_t Task_5ms_Pct_5msCtrlFinalTarPFromPCtrl;
extern Eem_SuspcDetnFuncInhibitVlvSts_t Task_5ms_Eem_SuspcDetnFuncInhibitVlvSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
extern Eem_SuspcDetnFuncInhibitProxySts_t Task_5ms_Eem_SuspcDetnFuncInhibitProxySts;
extern Eem_SuspcDetnFuncInhibitDiagSts_t Task_5ms_Eem_SuspcDetnFuncInhibitDiagSts;
extern Eem_SuspcDetnFuncInhibitAcmioSts_t Task_5ms_Eem_SuspcDetnFuncInhibitAcmioSts;
extern Eem_SuspcDetnFuncInhibitAcmctlSts_t Task_5ms_Eem_SuspcDetnFuncInhibitAcmctlSts;
extern Eem_SuspcDetnFuncInhibitMspSts_t Task_5ms_Eem_SuspcDetnFuncInhibitMspSts;
extern Eem_SuspcDetnFuncInhibitPedalSts_t Task_5ms_Eem_SuspcDetnFuncInhibitPedalSts;
extern Eem_SuspcDetnFuncInhibitPressSts_t Task_5ms_Eem_SuspcDetnFuncInhibitPressSts;
extern Eem_SuspcDetnFuncInhibitSesSts_t Task_5ms_Eem_SuspcDetnFuncInhibitSesSts;
extern Eem_SuspcDetnFuncInhibitVlvdSts_t Task_5ms_Eem_SuspcDetnFuncInhibitVlvdSts;
extern Eem_SuspcDetnFuncInhibitAdcifSts_t Task_5ms_Eem_SuspcDetnFuncInhibitAdcifSts;
extern Eem_SuspcDetnFuncInhibitCanTrcvSts_t Task_5ms_Eem_SuspcDetnFuncInhibitCanTrcvSts;
extern Eem_SuspcDetnFuncInhibitMccSts_t Task_5ms_Eem_SuspcDetnFuncInhibitMccSts;
extern Arbitrator_VlvArbVlvDriveState_t Task_5ms_Arbitrator_VlvArbVlvDriveState;
extern Fsr_ActrFsrDcMtrShutDwn_t Task_5ms_Fsr_ActrFsrDcMtrShutDwn;
extern Fsr_ActrFsrEnDrDrv_t Task_5ms_Fsr_ActrFsrEnDrDrv;
extern Vlv_ActrSyncVlvSync_t Task_5ms_Vlv_ActrSyncVlvSync;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Task_5ms_Init(void);
extern void Task_5ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_5MS_H_ */
/** @} */
