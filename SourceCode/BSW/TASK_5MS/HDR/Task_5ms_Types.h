/**
 * @defgroup Task_5ms_Types Task_5ms_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_5ms_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_5MS_TYPES_H_
#define TASK_5MS_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    SenPwrM_MainSenPwrMonitor_t Prly_HndlrSenPwrMonitorData;
    Ach_InputAchWssPort0AsicInfo_t Wss_SenAchWssPort0AsicInfo;
    Ach_InputAchWssPort1AsicInfo_t Wss_SenAchWssPort1AsicInfo;
    Ach_InputAchWssPort2AsicInfo_t Wss_SenAchWssPort2AsicInfo;
    Ach_InputAchWssPort3AsicInfo_t Wss_SenAchWssPort3AsicInfo;
    Pedal_SenPdtBufInfo_t Pedal_SenSyncPdtBufInfo;
    Pedal_SenPdfBufInfo_t Pedal_SenSyncPdfBufInfo;
    Press_SenCircPBufInfo_t Press_SenSyncCircPBufInfo;
    Press_SenPistPBufInfo_t Press_SenSyncPistPBufInfo;
    Press_SenPspBufInfo_t Press_SenSyncPspBufInfo;
    Ioc_InputCS1msSwtMonInfo_t Swt_SenSwtMonInfo;
    Ioc_InputCS1msSwtMonInfoEsc_t Swt_SenSwtMonInfoEsc;
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
    Msp_CtrlMotRotgAgSigInfo_t Det_5msCtrlMotRotgAgSigInfo;
    Det_1msCtrlBrkPedlStatus1msInfo_t Bbc_CtrlBrkPedlStatus1msInfo;
    Msp_CtrlMotRotgAgSigInfo_t Abc_CtrlMotRotgAgSigInfo;
    Diag_HndlrSasCalInfo_t Abc_CtrlSasCalInfo;
    Proxy_TxTxESCSensorInfo_t Abc_CtrlTxESCSensorInfo;
    SenPwrM_MainSenPwrMonitor_t Abc_CtrlSenPwrMonitorData;
    YawM_MainYAWMSerialInfo_t Abc_CtrlYAWMSerialInfo;
    YawM_MainYAWMOutInfo_t Abc_CtrlYAWMOutInfo;
    Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_5msCtrlPedlTrvlFild1msInfo;
    Msp_CtrlMotRotgAgSigInfo_t Pct_5msCtrlMotRotgAgSigInfo;
    Det_1msCtrlBrkPedlStatus1msInfo_t Pct_5msCtrlBrkPedlStatus1msInfo;
    Det_1msCtrlPedlTrvlRate1msInfo_t Pct_5msCtrlPedlTrvlRate1msInfo;
    Mcc_1msCtrlIdbMotPosnInfo_t Pct_5msCtrlIdbMotPosnInfo;
    Ach_InputAchSysPwrAsicInfo_t SysPwrM_MainAchSysPwrAsicInfo;
    Ach_InputAchSysPwrAsicInfo_t EcuHwCtrlM_MainAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t EcuHwCtrlM_MainAchValveAsicInfo;
    Ach_InputAchSysPwrAsicInfo_t WssM_MainAchSysPwrAsicInfo;
    Ach_InputAchWssPort0AsicInfo_t WssM_MainAchWssPort0AsicInfo;
    Ach_InputAchWssPort1AsicInfo_t WssM_MainAchWssPort1AsicInfo;
    Ach_InputAchWssPort2AsicInfo_t WssM_MainAchWssPort2AsicInfo;
    Ach_InputAchWssPort3AsicInfo_t WssM_MainAchWssPort3AsicInfo;
    Ioc_InputSR1msHalPressureInfo_t PressM_MainHalPressureInfo;
    SentH_MainSentHPressureInfo_t PressM_MainSentHPressureInfo;
    Ach_InputAchSysPwrAsicInfo_t PedalM_MainAchSysPwrAsicInfo;
    Ioc_InputSR1msPedlSigMonInfo_t PedalM_MainPedlSigMonInfo;
    Ioc_InputSR1msPedlPwrMonInfo_t PedalM_MainPedlPwrMonInfo;
    Press_SenPressCalcInfo_t PressP_MainPressCalcInfo;
    Ach_InputAchSysPwrAsicInfo_t BbsVlvM_MainAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t BbsVlvM_MainAchValveAsicInfo;
    Vlvd_A3944_HndlrVlvdFF0DcdInfo_t BbsVlvM_MainVlvdFF0DcdInfo;
    Vlvd_A3944_HndlrVlvdFF1DcdInfo_t BbsVlvM_MainVlvdFF1DcdInfo;
    Ach_InputAchSysPwrAsicInfo_t AbsVlvM_MainAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t AbsVlvM_MainAchValveAsicInfo;
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012M_MainMpsD1SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012M_MainMpsD2SpiDcdInfo;
    Mgd_TLE9180_HndlrMgdDcdInfo_t MgdM_MainMgdDcdInfo;
    Ioc_InputSR1msMotMonInfo_t MtrM_MainMotMonInfo;
    Ioc_InputSR1msMotVoltsMonInfo_t MtrM_MainMotVoltsMonInfo;
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t MtrM_MainMpsD1SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t MtrM_MainMpsD2SpiDcdInfo;
    Mgd_TLE9180_HndlrMgdDcdInfo_t MtrM_MainMgdDcdInfo;
    Mcc_1msCtrlMotDqIRefMccInfo_t MtrM_MainMotDqIRefMccInfo;
    AdcIf_Conv50usHwTrigMotInfo_t MtrM_MainHwTrigMotInfo;
    Ioc_InputSR50usMotCurrMonInfo_t MtrM_MainMotCurrMonInfo;
    Ioc_InputSR50usMotAngleMonInfo_t MtrM_MainMotAngleMonInfo;
    Mtr_Processing_SigMtrProcessOutInfo_t MtrM_MainMtrProcessOutInfo;
    RlyM_MainRelayMonitor_t Eem_MainRelayMonitorData;
    SenPwrM_MainSenPwrMonitor_t Eem_MainSenPwrMonitorData;
    SwtM_MainSwtMonFaultInfo_t Eem_MainSwtMonFaultInfo;
    SwtP_MainSwtPFaultInfo_t Eem_MainSwtPFaultInfo;
    YawM_MainYAWMOutInfo_t Eem_MainYAWMOutInfo;
    AyM_MainAYMOutInfo_t Eem_MainAYMOuitInfo;
    AxM_MainAXMOutInfo_t Eem_MainAXMOutInfo;
    SasM_MainSASMOutInfo_t Eem_MainSASMOutInfo;
    CanM_MainCanMonData_t Eem_MainCanMonData;
    YawP_MainYawPlauOutput_t Eem_MainYawPlauOutput;
    AxP_MainAxPlauOutput_t Eem_MainAxPlauOutput;
    AyP_MainAyPlauOutput_t Eem_MainAyPlauOutput;
    SasP_MainSasPlauOutput_t Eem_MainSasPlauOutput;
    Ses_CtrlNormVlvReqSesInfo_t Arbitrator_VlvNormVlvReqSesInfo;
    Ses_CtrlWhlVlvReqSesInfo_t Arbitrator_VlvWhlVlvReqSesInfo;
    Diag_HndlrDiagVlvActr_t Arbitrator_VlvDiagVlvActrInfo;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR5msAcmAsicInitCompleteFlag;
    Ioc_InputSR1msCEMon_t Prly_HndlrCEMon;
    Ioc_InputCS1msRsmDbcMon_t Swt_SenRsmDbcMon;
    Mcc_1msCtrlMotCtrlState_t Spc_5msCtrlMotCtrlState;
    Ioc_InputSR1msVBatt1Mon_t Abc_CtrlVBatt1Mon;
    Diag_HndlrDiagSci_t Abc_CtrlDiagSci;
    Mcc_1msCtrlMotCtrlMode_t Pct_5msCtrlMotCtrlMode;
    Mcc_1msCtrlMotICtrlFadeOutState_t Pct_5msCtrlMotICtrlFadeOutState;
    Pct_1msCtrlPreBoostMod_t Pct_5msCtrlPreBoostMod;
    Mcc_1msCtrlStkRecvryStabnEndOK_t Pct_5msCtrlStkRecvryStabnEndOK;
    Ioc_InputSR1msVBatt1Mon_t SysPwrM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t SysPwrM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t SysPwrM_MainDiagClrSrs;
    Ioc_InputSR1msVdd1Mon_t SysPwrM_MainVdd1Mon;
    Ioc_InputSR1msVdd2Mon_t SysPwrM_MainVdd2Mon;
    Ioc_InputSR1msVdd3Mon_t SysPwrM_MainVdd3Mon;
    Ioc_InputSR1msVdd4Mon_t SysPwrM_MainVdd4Mon;
    Ioc_InputSR1msVdd5Mon_t SysPwrM_MainVdd5Mon;
    Ioc_InputSR1msVddMon_t SysPwrM_MainVddMon;
    Arbitrator_MtrMtrArbDriveState_t SysPwrM_MainMtrArbDriveState;
    Ach_InputAchAsicInvalidInfo_t EcuHwCtrlM_MainAchAsicInvalid;
    Ioc_InputSR1msVBatt1Mon_t EcuHwCtrlM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t EcuHwCtrlM_MainDiagClrSrs;
    Acm_MainAcmAsicInitCompleteFlag_t EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
    Ioc_InputSR1msVdd1Mon_t EcuHwCtrlM_MainVdd1Mon;
    Ioc_InputSR1msVdd2Mon_t EcuHwCtrlM_MainVdd2Mon;
    Ioc_InputSR1msVdd3Mon_t EcuHwCtrlM_MainVdd3Mon;
    Ioc_InputSR1msVdd4Mon_t EcuHwCtrlM_MainVdd4Mon;
    Ioc_InputSR1msVdd5Mon_t EcuHwCtrlM_MainVdd5Mon;
    AdcIf_Conv1msAdcInvalid_t EcuHwCtrlM_MainAdcInvalid;
    Ach_InputAchAsicInvalidInfo_t WssM_MainAchAsicInvalid;
    Ioc_InputSR1msVBatt1Mon_t WssM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t WssM_MainDiagClrSrs;
    Ioc_InputSR1msVBatt1Mon_t PressM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t PressM_MainDiagClrSrs;
    Ach_InputAchAsicInvalidInfo_t PedalM_MainAchAsicInvalid;
    Diag_HndlrDiagClr_t PedalM_MainDiagClrSrs;
    Ioc_InputSR1msVdd3Mon_t PedalM_MainVdd3Mon;
    Diag_HndlrDiagClr_t WssP_MainDiagClrSrs;
    Ioc_InputSR1msVBatt1Mon_t PressP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t PressP_MainDiagClrSrs;
    Diag_HndlrDiagClr_t PedalP_MainDiagClrSrs;
    Ach_InputAchAsicInvalidInfo_t BbsVlvM_MainAchAsicInvalid;
    Ioc_InputSR1msVBatt1Mon_t BbsVlvM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t BbsVlvM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t BbsVlvM_MainDiagClrSrs;
    Ioc_InputSR1msFspCbsHMon_t BbsVlvM_MainFspCbsHMon;
    Ioc_InputSR1msFspCbsMon_t BbsVlvM_MainFspCbsMon;
    Vlvd_A3944_HndlrVlvdInvalid_t BbsVlvM_MainVlvdInvalid;
    Ach_InputAchAsicInvalidInfo_t AbsVlvM_MainAchAsicInvalid;
    Ioc_InputSR1msVBatt1Mon_t AbsVlvM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t AbsVlvM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t AbsVlvM_MainDiagClrSrs;
    Ioc_InputSR1msFspAbsHMon_t AbsVlvM_MainFspAbsHMon;
    Ioc_InputSR1msFspAbsMon_t AbsVlvM_MainFspAbsMon;
    Ioc_InputSR1msVBatt1Mon_t Mps_TLE5012M_MainVBatt1Mon;
    Diag_HndlrDiagClr_t Mps_TLE5012M_MainDiagClrSrs;
    Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012M_MainMpsInvalid;
    Arbitrator_MtrMtrArbDriveState_t Mps_TLE5012M_MainMtrArbDriveState;
    Ioc_InputSR1msVBatt1Mon_t MgdM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t MgdM_MainDiagClrSrs;
    Mgd_TLE9180_HndlrMgdInvalid_t MgdM_MainMgdInvalid;
    Arbitrator_MtrMtrArbDriveState_t MgdM_MainMtrArbDriveState;
    Ioc_InputSR1msVBatt1Mon_t MtrM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t MtrM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t MtrM_MainDiagClrSrs;
    Mgd_TLE9180_HndlrMgdInvalid_t MtrM_MainMgdInvalid;
    Arbitrator_MtrMtrArbDriveState_t MtrM_MainMtrArbDriveState;
    Diag_HndlrDiagClr_t Watchdog_MainDiagClrSrs;
    Diag_HndlrDiagClr_t Eem_MainDiagClrSrs;
    Diag_HndlrDiagSci_t Eem_MainDiagSci;
    Diag_HndlrDiagAhbSci_t Eem_MainDiagAhbSci;
    Diag_HndlrDiagClr_t Eem_SuspcDetnDiagClrSrs;
    Acm_MainAcmAsicInitCompleteFlag_t Arbitrator_VlvAcmAsicInitCompleteFlag;
    Diag_HndlrDiagSci_t Arbitrator_VlvDiagSci;
    Diag_HndlrDiagAhbSci_t Arbitrator_VlvDiagAhbSci;
    Diag_HndlrDiagSci_t Arbitrator_RlyDiagSci;
    Diag_HndlrDiagAhbSci_t Arbitrator_RlyDiagAhbSci;
    Acm_MainAcmAsicInitCompleteFlag_t Fsr_ActrAcmAsicInitCompleteFlag;
    Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrSyncAcmAsicInitCompleteFlag;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS5msAcmAsicInitCompleteFlag;

/* Output Data Element */
    Proxy_RxByComRxYawSerialInfo_t Proxy_RxByComRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t Proxy_RxByComRxYawAccInfo;
    Proxy_RxByComRxSasInfo_t Proxy_RxByComRxSasInfo;
    Proxy_RxByComRxLongAccInfo_t Proxy_RxByComRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo;
    Proxy_RxCanRxRegenInfo_t Proxy_RxCanRxRegenInfo;
    Proxy_RxCanRxAccelPedlInfo_t Proxy_RxCanRxAccelPedlInfo;
    Proxy_RxCanRxIdbInfo_t Proxy_RxCanRxIdbInfo;
    Proxy_RxCanRxEngTempInfo_t Proxy_RxCanRxEngTempInfo;
    Proxy_RxCanRxEscInfo_t Proxy_RxCanRxEscInfo;
    Proxy_RxCanRxEemInfo_t Proxy_RxCanRxEemInfo;
    Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo;
    Proxy_RxIMUCalc_t Proxy_RxIMUCalcInfo;
    Nvm_HndlrLogicEepDataInfo_t Nvm_HndlrLogicEepDataInfo;
    Wss_SenWhlSpdInfo_t Wss_SenWhlSpdInfo;
    Wss_SenWhlEdgeCntInfo_t Wss_SenWhlEdgeCntInfo;
    Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
    Wss_SenWssCalc_t Wss_SenWssCalcInfo;
    Pedal_SenSyncPdt5msRawInfo_t Pedal_SenSyncPdt5msRawInfo;
    Pedal_SenSyncPdf5msRawInfo_t Pedal_SenSyncPdf5msRawInfo;
    Press_SenSyncPistP5msRawInfo_t Press_SenSyncPistP5msRawInfo;
    Swt_SenEscSwtStInfo_t Swt_SenEscSwtStInfo;
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t Bbc_CtrlBaseBrkCtrlModInfo;
    Rbc_CtrlTarRgnBrkTqInfo_t Rbc_CtrlTarRgnBrkTqInfo;
    Abc_CtrlCanTxInfo_t Abc_CtrlCanTxInfo;
    Abc_CtrlAbsCtrlInfo_t Abc_CtrlAbsCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Abc_CtrlEscCtrlInfo;
    Abc_CtrlTcsCtrlInfo_t Abc_CtrlTcsCtrlInfo;
    Pct_5msCtrlStkRecvryActnIfInfo_t Pct_5msCtrlStkRecvryActnIfInfo;
    Vat_CtrlIdbVlvActInfo_t Vat_CtrlIdbVlvActInfo;
    Eem_MainEemFailData_t Eem_MainEemFailData;
    Eem_MainEemCtrlInhibitData_t Eem_MainEemCtrlInhibitData;
    Eem_MainEemSuspectData_t Eem_MainEemSuspectData;
    Eem_MainEemEceData_t Eem_MainEemEceData;
    Eem_MainEemLampData_t Eem_MainEemLampData;
    Eem_SuspcDetnCanTimeOutStInfo_t Eem_SuspcDetnCanTimeOutStInfo;
    Arbitrator_VlvArbWhlVlvReqInfo_t Arbitrator_VlvArbWhlVlvReqInfo;
    Fsr_ActrFsrAbsDrvInfo_t Fsr_ActrFsrAbsDrvInfo;
    Fsr_ActrFsrCbsDrvInfo_t Fsr_ActrFsrCbsDrvInfo;
    Rly_ActrRlyDrvInfo_t Rly_ActrRlyDrvInfo;
    Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrSyncNormVlvReqInfo;
    Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrSyncWhlVlvReqInfo;
    Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrSyncBalVlvReqInfo;
    Proxy_RxCanRxGearSelDispErrInfo_t Proxy_RxCanRxGearSelDispErrInfo;
    Prly_HndlrIgnOnOffSts_t Prly_HndlrIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Prly_HndlrIgnEdgeSts;
    Prly_HndlrPrlyEcuInhibit_t Prly_HndlrPrlyEcuInhibit;
    Mom_HndlrEcuModeSts_t Mom_HndlrEcuModeSts;
    Det_5msCtrlVehSpdFild_t Det_5msCtrlVehSpdFild;
    Abc_CtrlVehSpd_t Abc_CtrlVehSpd;
    Pct_5msCtrlTgtDeltaStkType_t Pct_5msCtrlTgtDeltaStkType;
    Pct_5msCtrlPCtrlBoostMod_t Pct_5msCtrlPCtrlBoostMod;
    Pct_5msCtrlPCtrlFadeoutSt_t Pct_5msCtrlPCtrlFadeoutSt;
    Pct_5msCtrlPCtrlSt_t Pct_5msCtrlPCtrlSt;
    Pct_5msCtrlInVlvAllCloseReq_t Pct_5msCtrlInVlvAllCloseReq;
    Pct_5msCtrlPChamberVolume_t Pct_5msCtrlPChamberVolume;
    Pct_5msCtrlTarDeltaStk_t Pct_5msCtrlTarDeltaStk;
    Pct_5msCtrlFinalTarPFromPCtrl_t Pct_5msCtrlFinalTarPFromPCtrl;
    Eem_SuspcDetnFuncInhibitVlvSts_t Eem_SuspcDetnFuncInhibitVlvSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Eem_SuspcDetnFuncInhibitIocSts;
    Eem_SuspcDetnFuncInhibitProxySts_t Eem_SuspcDetnFuncInhibitProxySts;
    Eem_SuspcDetnFuncInhibitDiagSts_t Eem_SuspcDetnFuncInhibitDiagSts;
    Eem_SuspcDetnFuncInhibitAcmioSts_t Eem_SuspcDetnFuncInhibitAcmioSts;
    Eem_SuspcDetnFuncInhibitAcmctlSts_t Eem_SuspcDetnFuncInhibitAcmctlSts;
    Eem_SuspcDetnFuncInhibitMspSts_t Eem_SuspcDetnFuncInhibitMspSts;
    Eem_SuspcDetnFuncInhibitPedalSts_t Eem_SuspcDetnFuncInhibitPedalSts;
    Eem_SuspcDetnFuncInhibitPressSts_t Eem_SuspcDetnFuncInhibitPressSts;
    Eem_SuspcDetnFuncInhibitSesSts_t Eem_SuspcDetnFuncInhibitSesSts;
    Eem_SuspcDetnFuncInhibitVlvdSts_t Eem_SuspcDetnFuncInhibitVlvdSts;
    Eem_SuspcDetnFuncInhibitAdcifSts_t Eem_SuspcDetnFuncInhibitAdcifSts;
    Eem_SuspcDetnFuncInhibitCanTrcvSts_t Eem_SuspcDetnFuncInhibitCanTrcvSts;
    Eem_SuspcDetnFuncInhibitMccSts_t Eem_SuspcDetnFuncInhibitMccSts;
    Arbitrator_VlvArbVlvDriveState_t Arbitrator_VlvArbVlvDriveState;
    Fsr_ActrFsrDcMtrShutDwn_t Fsr_ActrFsrDcMtrShutDwn;
    Fsr_ActrFsrEnDrDrv_t Fsr_ActrFsrEnDrDrv;
    Vlv_ActrSyncVlvSync_t Vlv_ActrSyncVlvSync;
}Task_5ms_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_5MS_TYPES_H_ */
/** @} */
