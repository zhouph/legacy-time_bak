Prly_HndlrSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
Prly_HndlrSenPwrMonitorData = CreateBus(Prly_HndlrSenPwrMonitorData, DeList);
clear DeList;

Wss_SenAchWssPort0AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH0_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH0_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH0_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH0_WSIRSDR3_NODATA'
    'Ach_Asic_CH0_WSIRSDR3_INVALID'
    'Ach_Asic_CH0_WSIRSDR3_OPEN'
    'Ach_Asic_CH0_WSIRSDR3_STB'
    'Ach_Asic_CH0_WSIRSDR3_STG'
    'Ach_Asic_CH0_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH0_WSIRSDR3_CURRENT_HI'
    };
Wss_SenAchWssPort0AsicInfo = CreateBus(Wss_SenAchWssPort0AsicInfo, DeList);
clear DeList;

Wss_SenAchWssPort1AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH1_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH1_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH1_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH1_WSIRSDR3_NODATA'
    'Ach_Asic_CH1_WSIRSDR3_INVALID'
    'Ach_Asic_CH1_WSIRSDR3_OPEN'
    'Ach_Asic_CH1_WSIRSDR3_STB'
    'Ach_Asic_CH1_WSIRSDR3_STG'
    'Ach_Asic_CH1_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH1_WSIRSDR3_CURRENT_HI'
    };
Wss_SenAchWssPort1AsicInfo = CreateBus(Wss_SenAchWssPort1AsicInfo, DeList);
clear DeList;

Wss_SenAchWssPort2AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH2_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH2_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH2_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH2_WSIRSDR3_NODATA'
    'Ach_Asic_CH2_WSIRSDR3_INVALID'
    'Ach_Asic_CH2_WSIRSDR3_OPEN'
    'Ach_Asic_CH2_WSIRSDR3_STB'
    'Ach_Asic_CH2_WSIRSDR3_STG'
    'Ach_Asic_CH2_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH2_WSIRSDR3_CURRENT_HI'
    };
Wss_SenAchWssPort2AsicInfo = CreateBus(Wss_SenAchWssPort2AsicInfo, DeList);
clear DeList;

Wss_SenAchWssPort3AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH3_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH3_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH3_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH3_WSIRSDR3_NODATA'
    'Ach_Asic_CH3_WSIRSDR3_INVALID'
    'Ach_Asic_CH3_WSIRSDR3_OPEN'
    'Ach_Asic_CH3_WSIRSDR3_STB'
    'Ach_Asic_CH3_WSIRSDR3_STG'
    'Ach_Asic_CH3_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH3_WSIRSDR3_CURRENT_HI'
    };
Wss_SenAchWssPort3AsicInfo = CreateBus(Wss_SenAchWssPort3AsicInfo, DeList);
clear DeList;

Pedal_SenSyncPdtBufInfo = Simulink.Bus;
DeList={
    'PdtRaw_array_0'
    'PdtRaw_array_1'
    'PdtRaw_array_2'
    'PdtRaw_array_3'
    'PdtRaw_array_4'
    };
Pedal_SenSyncPdtBufInfo = CreateBus(Pedal_SenSyncPdtBufInfo, DeList);
clear DeList;

Pedal_SenSyncPdfBufInfo = Simulink.Bus;
DeList={
    'PdfRaw_array_0'
    'PdfRaw_array_1'
    'PdfRaw_array_2'
    'PdfRaw_array_3'
    'PdfRaw_array_4'
    };
Pedal_SenSyncPdfBufInfo = CreateBus(Pedal_SenSyncPdfBufInfo, DeList);
clear DeList;

Press_SenSyncCircPBufInfo = Simulink.Bus;
DeList={
    'PrimCircPRaw_array_0'
    'PrimCircPRaw_array_1'
    'PrimCircPRaw_array_2'
    'PrimCircPRaw_array_3'
    'PrimCircPRaw_array_4'
    'SecdCircPRaw_array_0'
    'SecdCircPRaw_array_1'
    'SecdCircPRaw_array_2'
    'SecdCircPRaw_array_3'
    'SecdCircPRaw_array_4'
    };
Press_SenSyncCircPBufInfo = CreateBus(Press_SenSyncCircPBufInfo, DeList);
clear DeList;

Press_SenSyncPistPBufInfo = Simulink.Bus;
DeList={
    'PistPRaw_array_0'
    'PistPRaw_array_1'
    'PistPRaw_array_2'
    'PistPRaw_array_3'
    'PistPRaw_array_4'
    };
Press_SenSyncPistPBufInfo = CreateBus(Press_SenSyncPistPBufInfo, DeList);
clear DeList;

Press_SenSyncPspBufInfo = Simulink.Bus;
DeList={
    'PedlSimPRaw_array_0'
    'PedlSimPRaw_array_1'
    'PedlSimPRaw_array_2'
    'PedlSimPRaw_array_3'
    'PedlSimPRaw_array_4'
    };
Press_SenSyncPspBufInfo = CreateBus(Press_SenSyncPspBufInfo, DeList);
clear DeList;

Swt_SenSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BflSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'DoorSwtMon'
    'EscSwtMon'
    'FlexBrkASwtMon'
    'FlexBrkBSwtMon'
    'HzrdSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
Swt_SenSwtMonInfo = CreateBus(Swt_SenSwtMonInfo, DeList);
clear DeList;

Swt_SenSwtMonInfoEsc = Simulink.Bus;
DeList={
    'BlsSwtMon_Esc'
    'AvhSwtMon_Esc'
    'EscSwtMon_Esc'
    'HdcSwtMon_Esc'
    'PbSwtMon_Esc'
    'GearRSwtMon_Esc'
    'BlfSwtMon_Esc'
    'ClutchSwtMon_Esc'
    'ItpmsSwtMon_Esc'
    };
Swt_SenSwtMonInfoEsc = CreateBus(Swt_SenSwtMonInfoEsc, DeList);
clear DeList;

Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo = Simulink.Bus;
DeList={
    'PedlTrvlOffsEolCalcReqFlg'
    'PSnsrOffsEolCalcReqFlg'
    };
Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo = CreateBus(Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo, DeList);
clear DeList;

Det_5msCtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Det_5msCtrlMotRotgAgSigInfo = CreateBus(Det_5msCtrlMotRotgAgSigInfo, DeList);
clear DeList;

Bbc_CtrlBrkPedlStatus1msInfo = Simulink.Bus;
DeList={
    'PanicBrkSt1msFlg'
    };
Bbc_CtrlBrkPedlStatus1msInfo = CreateBus(Bbc_CtrlBrkPedlStatus1msInfo, DeList);
clear DeList;

Abc_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Abc_CtrlMotRotgAgSigInfo = CreateBus(Abc_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Abc_CtrlSasCalInfo = Simulink.Bus;
DeList={
    'DiagSasCaltoAppl'
    };
Abc_CtrlSasCalInfo = CreateBus(Abc_CtrlSasCalInfo, DeList);
clear DeList;

Abc_CtrlTxESCSensorInfo = Simulink.Bus;
DeList={
    'EstimatedYaw'
    'EstimatedAY'
    'A_long_1_1000g'
    };
Abc_CtrlTxESCSensorInfo = CreateBus(Abc_CtrlTxESCSensorInfo, DeList);
clear DeList;

Abc_CtrlSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
Abc_CtrlSenPwrMonitorData = CreateBus(Abc_CtrlSenPwrMonitorData, DeList);
clear DeList;

Abc_CtrlYAWMSerialInfo = Simulink.Bus;
DeList={
    'YAWM_SerialNumOK_Flg'
    'YAWM_SerialUnMatch_Flg'
    };
Abc_CtrlYAWMSerialInfo = CreateBus(Abc_CtrlYAWMSerialInfo, DeList);
clear DeList;

Abc_CtrlYAWMOutInfo = Simulink.Bus;
DeList={
    'YAWM_Timeout_Err'
    'YAWM_Invalid_Err'
    'YAWM_CRC_Err'
    'YAWM_Rolling_Err'
    'YAWM_Temperature_Err'
    'YAWM_Range_Err'
    };
Abc_CtrlYAWMOutInfo = CreateBus(Abc_CtrlYAWMOutInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Pct_5msCtrlPedlTrvlFild1msInfo = CreateBus(Pct_5msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Pct_5msCtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Pct_5msCtrlMotRotgAgSigInfo = CreateBus(Pct_5msCtrlMotRotgAgSigInfo, DeList);
clear DeList;

Pct_5msCtrlBrkPedlStatus1msInfo = Simulink.Bus;
DeList={
    'PanicBrkSt1msFlg'
    };
Pct_5msCtrlBrkPedlStatus1msInfo = CreateBus(Pct_5msCtrlBrkPedlStatus1msInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Pct_5msCtrlPedlTrvlRate1msInfo = CreateBus(Pct_5msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;

Pct_5msCtrlIdbMotPosnInfo = Simulink.Bus;
DeList={
    'MotTarPosn'
    };
Pct_5msCtrlIdbMotPosnInfo = CreateBus(Pct_5msCtrlIdbMotPosnInfo, DeList);
clear DeList;

SysPwrM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
SysPwrM_MainAchSysPwrAsicInfo = CreateBus(SysPwrM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

EcuHwCtrlM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
EcuHwCtrlM_MainAchSysPwrAsicInfo = CreateBus(EcuHwCtrlM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

EcuHwCtrlM_MainAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_VPWR_UV'
    'Ach_Asic_VPWR_OV'
    'Ach_Asic_PMP_LD_ACT'
    'Ach_Asic_FS_TURN_ON'
    'Ach_Asic_FS_TURN_OFF'
    'Ach_Asic_FS_VDS_FAULT'
    'Ach_Asic_FS_RVP_FAULT'
    'Ach_Asic_VHD_OV'
    'Ach_Asic_T_SD_INT'
    'Ach_Asic_No0_AVG_CURRENT'
    'Ach_Asic_No0_PWM_FAULT'
    'Ach_Asic_No0_CURR_SENSE'
    'Ach_Asic_No0_ADC_FAULT'
    'Ach_Asic_No0_T_SD'
    'Ach_Asic_No0_OPEN_LOAD'
    'Ach_Asic_No0_GND_LOSS'
    'Ach_Asic_No0_LVT'
    'Ach_Asic_No0_LS_OVC'
    'Ach_Asic_No0_VGS_LS_FAULT'
    'Ach_Asic_No0_VGS_HS_FAULT'
    'Ach_Asic_No0_HS_SHORT'
    'Ach_Asic_No0_LS_CLAMP_ON'
    'Ach_Asic_No0_UNDER_CURR'
    'Ach_Asic_No1_AVG_CURRENT'
    'Ach_Asic_No1_PWM_FAULT'
    'Ach_Asic_No1_CURR_SENSE'
    'Ach_Asic_No1_ADC_FAULT'
    'Ach_Asic_No1_T_SD'
    'Ach_Asic_No1_OPEN_LOAD'
    'Ach_Asic_No1_GND_LOSS'
    'Ach_Asic_No1_LVT'
    'Ach_Asic_No1_LS_OVC'
    'Ach_Asic_No1_VGS_LS_FAULT'
    'Ach_Asic_No1_VGS_HS_FAULT'
    'Ach_Asic_No1_HS_SHORT'
    'Ach_Asic_No1_LS_CLAMP_ON'
    'Ach_Asic_No1_UNDER_CURR'
    'Ach_Asic_No2_AVG_CURRENT'
    'Ach_Asic_No2_PWM_FAULT'
    'Ach_Asic_No2_CURR_SENSE'
    'Ach_Asic_No2_ADC_FAULT'
    'Ach_Asic_No2_T_SD'
    'Ach_Asic_No2_OPEN_LOAD'
    'Ach_Asic_No2_GND_LOSS'
    'Ach_Asic_No2_LVT'
    'Ach_Asic_No2_LS_OVC'
    'Ach_Asic_No2_VGS_LS_FAULT'
    'Ach_Asic_No2_VGS_HS_FAULT'
    'Ach_Asic_No2_HS_SHORT'
    'Ach_Asic_No2_LS_CLAMP_ON'
    'Ach_Asic_No2_UNDER_CURR'
    'Ach_Asic_No3_AVG_CURRENT'
    'Ach_Asic_No3_PWM_FAULT'
    'Ach_Asic_No3_CURR_SENSE'
    'Ach_Asic_No3_ADC_FAULT'
    'Ach_Asic_No3_T_SD'
    'Ach_Asic_No3_OPEN_LOAD'
    'Ach_Asic_No3_GND_LOSS'
    'Ach_Asic_No3_LVT'
    'Ach_Asic_No3_LS_OVC'
    'Ach_Asic_No3_VGS_LS_FAULT'
    'Ach_Asic_No3_VGS_HS_FAULT'
    'Ach_Asic_No3_HS_SHORT'
    'Ach_Asic_No3_LS_CLAMP_ON'
    'Ach_Asic_No3_UNDER_CURR'
    'Ach_Asic_Nc0_AVG_CURRENT'
    'Ach_Asic_Nc0_PWM_FAULT'
    'Ach_Asic_Nc0_CURR_SENSE'
    'Ach_Asic_Nc0_ADC_FAULT'
    'Ach_Asic_Nc0_T_SD'
    'Ach_Asic_Nc0_OPEN_LOAD'
    'Ach_Asic_Nc0_GND_LOSS'
    'Ach_Asic_Nc0_LVT'
    'Ach_Asic_Nc0_LS_OVC'
    'Ach_Asic_Nc0_VGS_LS_FAULT'
    'Ach_Asic_Nc0_VGS_HS_FAULT'
    'Ach_Asic_Nc0_HS_SHORT'
    'Ach_Asic_Nc0_LS_CLAMP_ON'
    'Ach_Asic_Nc0_UNDER_CURR'
    'Ach_Asic_Nc1_AVG_CURRENT'
    'Ach_Asic_Nc1_PWM_FAULT'
    'Ach_Asic_Nc1_CURR_SENSE'
    'Ach_Asic_Nc1_ADC_FAULT'
    'Ach_Asic_Nc1_T_SD'
    'Ach_Asic_Nc1_OPEN_LOAD'
    'Ach_Asic_Nc1_GND_LOSS'
    'Ach_Asic_Nc1_LVT'
    'Ach_Asic_Nc1_LS_OVC'
    'Ach_Asic_Nc1_VGS_LS_FAULT'
    'Ach_Asic_Nc1_VGS_HS_FAULT'
    'Ach_Asic_Nc1_HS_SHORT'
    'Ach_Asic_Nc1_LS_CLAMP_ON'
    'Ach_Asic_Nc1_UNDER_CURR'
    'Ach_Asic_Nc2_AVG_CURRENT'
    'Ach_Asic_Nc2_PWM_FAULT'
    'Ach_Asic_Nc2_CURR_SENSE'
    'Ach_Asic_Nc2_ADC_FAULT'
    'Ach_Asic_Nc2_T_SD'
    'Ach_Asic_Nc2_OPEN_LOAD'
    'Ach_Asic_Nc2_GND_LOSS'
    'Ach_Asic_Nc2_LVT'
    'Ach_Asic_Nc2_LS_OVC'
    'Ach_Asic_Nc2_VGS_LS_FAULT'
    'Ach_Asic_Nc2_VGS_HS_FAULT'
    'Ach_Asic_Nc2_HS_SHORT'
    'Ach_Asic_Nc2_LS_CLAMP_ON'
    'Ach_Asic_Nc2_UNDER_CURR'
    'Ach_Asic_Nc3_AVG_CURRENT'
    'Ach_Asic_Nc3_PWM_FAULT'
    'Ach_Asic_Nc3_CURR_SENSE'
    'Ach_Asic_Nc3_ADC_FAULT'
    'Ach_Asic_Nc3_T_SD'
    'Ach_Asic_Nc3_OPEN_LOAD'
    'Ach_Asic_Nc3_GND_LOSS'
    'Ach_Asic_Nc3_LVT'
    'Ach_Asic_Nc3_LS_OVC'
    'Ach_Asic_Nc3_VGS_LS_FAULT'
    'Ach_Asic_Nc3_VGS_HS_FAULT'
    'Ach_Asic_Nc3_HS_SHORT'
    'Ach_Asic_Nc3_LS_CLAMP_ON'
    'Ach_Asic_Nc3_UNDER_CURR'
    'Ach_Asic_Tc0_AVG_CURRENT'
    'Ach_Asic_Tc0_PWM_FAULT'
    'Ach_Asic_Tc0_CURR_SENSE'
    'Ach_Asic_Tc0_ADC_FAULT'
    'Ach_Asic_Tc0_T_SD'
    'Ach_Asic_Tc0_OPEN_LOAD'
    'Ach_Asic_Tc0_GND_LOSS'
    'Ach_Asic_Tc0_LVT'
    'Ach_Asic_Tc0_LS_OVC'
    'Ach_Asic_Tc0_VGS_LS_FAULT'
    'Ach_Asic_Tc0_VGS_HS_FAULT'
    'Ach_Asic_Tc0_HS_SHORT'
    'Ach_Asic_Tc0_LS_CLAMP_ON'
    'Ach_Asic_Tc0_UNDER_CURR'
    'Ach_Asic_Tc1_AVG_CURRENT'
    'Ach_Asic_Tc1_PWM_FAULT'
    'Ach_Asic_Tc1_CURR_SENSE'
    'Ach_Asic_Tc1_ADC_FAULT'
    'Ach_Asic_Tc1_T_SD'
    'Ach_Asic_Tc1_OPEN_LOAD'
    'Ach_Asic_Tc1_GND_LOSS'
    'Ach_Asic_Tc1_LVT'
    'Ach_Asic_Tc1_LS_OVC'
    'Ach_Asic_Tc1_VGS_LS_FAULT'
    'Ach_Asic_Tc1_VGS_HS_FAULT'
    'Ach_Asic_Tc1_HS_SHORT'
    'Ach_Asic_Tc1_LS_CLAMP_ON'
    'Ach_Asic_Tc1_UNDER_CURR'
    'Ach_Asic_Esv0_AVG_CURRENT'
    'Ach_Asic_Esv0_PWM_FAULT'
    'Ach_Asic_Esv0_CURR_SENSE'
    'Ach_Asic_Esv0_ADC_FAULT'
    'Ach_Asic_Esv0_T_SD'
    'Ach_Asic_Esv0_OPEN_LOAD'
    'Ach_Asic_Esv0_GND_LOSS'
    'Ach_Asic_Esv0_LVT'
    'Ach_Asic_Esv0_LS_OVC'
    'Ach_Asic_Esv0_VGS_LS_FAULT'
    'Ach_Asic_Esv0_VGS_HS_FAULT'
    'Ach_Asic_Esv0_HS_SHORT'
    'Ach_Asic_Esv0_LS_CLAMP_ON'
    'Ach_Asic_Esv0_UNDER_CURR'
    'Ach_Asic_Esv1_AVG_CURRENT'
    'Ach_Asic_Esv1_PWM_FAULT'
    'Ach_Asic_Esv1_CURR_SENSE'
    'Ach_Asic_Esv1_ADC_FAULT'
    'Ach_Asic_Esv1_T_SD'
    'Ach_Asic_Esv1_OPEN_LOAD'
    'Ach_Asic_Esv1_GND_LOSS'
    'Ach_Asic_Esv1_LVT'
    'Ach_Asic_Esv1_LS_OVC'
    'Ach_Asic_Esv1_VGS_LS_FAULT'
    'Ach_Asic_Esv1_VGS_HS_FAULT'
    'Ach_Asic_Esv1_HS_SHORT'
    'Ach_Asic_Esv1_LS_CLAMP_ON'
    'Ach_Asic_Esv1_UNDER_CURR'
    };
EcuHwCtrlM_MainAchValveAsicInfo = CreateBus(EcuHwCtrlM_MainAchValveAsicInfo, DeList);
clear DeList;

WssM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
WssM_MainAchSysPwrAsicInfo = CreateBus(WssM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort0AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH0_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH0_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH0_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH0_WSIRSDR3_NODATA'
    'Ach_Asic_CH0_WSIRSDR3_INVALID'
    'Ach_Asic_CH0_WSIRSDR3_OPEN'
    'Ach_Asic_CH0_WSIRSDR3_STB'
    'Ach_Asic_CH0_WSIRSDR3_STG'
    'Ach_Asic_CH0_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH0_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort0AsicInfo = CreateBus(WssM_MainAchWssPort0AsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort1AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH1_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH1_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH1_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH1_WSIRSDR3_NODATA'
    'Ach_Asic_CH1_WSIRSDR3_INVALID'
    'Ach_Asic_CH1_WSIRSDR3_OPEN'
    'Ach_Asic_CH1_WSIRSDR3_STB'
    'Ach_Asic_CH1_WSIRSDR3_STG'
    'Ach_Asic_CH1_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH1_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort1AsicInfo = CreateBus(WssM_MainAchWssPort1AsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort2AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH2_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH2_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH2_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH2_WSIRSDR3_NODATA'
    'Ach_Asic_CH2_WSIRSDR3_INVALID'
    'Ach_Asic_CH2_WSIRSDR3_OPEN'
    'Ach_Asic_CH2_WSIRSDR3_STB'
    'Ach_Asic_CH2_WSIRSDR3_STG'
    'Ach_Asic_CH2_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH2_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort2AsicInfo = CreateBus(WssM_MainAchWssPort2AsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort3AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH3_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH3_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH3_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH3_WSIRSDR3_NODATA'
    'Ach_Asic_CH3_WSIRSDR3_INVALID'
    'Ach_Asic_CH3_WSIRSDR3_OPEN'
    'Ach_Asic_CH3_WSIRSDR3_STB'
    'Ach_Asic_CH3_WSIRSDR3_STG'
    'Ach_Asic_CH3_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH3_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort3AsicInfo = CreateBus(WssM_MainAchWssPort3AsicInfo, DeList);
clear DeList;

PressM_MainHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
PressM_MainHalPressureInfo = CreateBus(PressM_MainHalPressureInfo, DeList);
clear DeList;

PressM_MainSentHPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
PressM_MainSentHPressureInfo = CreateBus(PressM_MainSentHPressureInfo, DeList);
clear DeList;

PedalM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
PedalM_MainAchSysPwrAsicInfo = CreateBus(PedalM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

PedalM_MainPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
PedalM_MainPedlSigMonInfo = CreateBus(PedalM_MainPedlSigMonInfo, DeList);
clear DeList;

PedalM_MainPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
PedalM_MainPedlPwrMonInfo = CreateBus(PedalM_MainPedlPwrMonInfo, DeList);
clear DeList;

PressP_MainPressCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimP_1_100_bar'
    'PressP_CirP1_1_100_bar'
    'PressP_CirP2_1_100_bar'
    'SimPMoveAvr'
    'CirPMoveAvr'
    'CirP2MoveAvr'
    };
PressP_MainPressCalcInfo = CreateBus(PressP_MainPressCalcInfo, DeList);
clear DeList;

BbsVlvM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
BbsVlvM_MainAchSysPwrAsicInfo = CreateBus(BbsVlvM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

BbsVlvM_MainAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_VPWR_UV'
    'Ach_Asic_VPWR_OV'
    'Ach_Asic_PMP_LD_ACT'
    'Ach_Asic_FS_TURN_ON'
    'Ach_Asic_FS_TURN_OFF'
    'Ach_Asic_FS_VDS_FAULT'
    'Ach_Asic_FS_RVP_FAULT'
    'Ach_Asic_VHD_OV'
    'Ach_Asic_T_SD_INT'
    'Ach_Asic_No0_AVG_CURRENT'
    'Ach_Asic_No0_PWM_FAULT'
    'Ach_Asic_No0_CURR_SENSE'
    'Ach_Asic_No0_ADC_FAULT'
    'Ach_Asic_No0_T_SD'
    'Ach_Asic_No0_OPEN_LOAD'
    'Ach_Asic_No0_GND_LOSS'
    'Ach_Asic_No0_LVT'
    'Ach_Asic_No0_LS_OVC'
    'Ach_Asic_No0_VGS_LS_FAULT'
    'Ach_Asic_No0_VGS_HS_FAULT'
    'Ach_Asic_No0_HS_SHORT'
    'Ach_Asic_No0_LS_CLAMP_ON'
    'Ach_Asic_No0_UNDER_CURR'
    'Ach_Asic_No1_AVG_CURRENT'
    'Ach_Asic_No1_PWM_FAULT'
    'Ach_Asic_No1_CURR_SENSE'
    'Ach_Asic_No1_ADC_FAULT'
    'Ach_Asic_No1_T_SD'
    'Ach_Asic_No1_OPEN_LOAD'
    'Ach_Asic_No1_GND_LOSS'
    'Ach_Asic_No1_LVT'
    'Ach_Asic_No1_LS_OVC'
    'Ach_Asic_No1_VGS_LS_FAULT'
    'Ach_Asic_No1_VGS_HS_FAULT'
    'Ach_Asic_No1_HS_SHORT'
    'Ach_Asic_No1_LS_CLAMP_ON'
    'Ach_Asic_No1_UNDER_CURR'
    'Ach_Asic_No2_AVG_CURRENT'
    'Ach_Asic_No2_PWM_FAULT'
    'Ach_Asic_No2_CURR_SENSE'
    'Ach_Asic_No2_ADC_FAULT'
    'Ach_Asic_No2_T_SD'
    'Ach_Asic_No2_OPEN_LOAD'
    'Ach_Asic_No2_GND_LOSS'
    'Ach_Asic_No2_LVT'
    'Ach_Asic_No2_LS_OVC'
    'Ach_Asic_No2_VGS_LS_FAULT'
    'Ach_Asic_No2_VGS_HS_FAULT'
    'Ach_Asic_No2_HS_SHORT'
    'Ach_Asic_No2_LS_CLAMP_ON'
    'Ach_Asic_No2_UNDER_CURR'
    'Ach_Asic_No3_AVG_CURRENT'
    'Ach_Asic_No3_PWM_FAULT'
    'Ach_Asic_No3_CURR_SENSE'
    'Ach_Asic_No3_ADC_FAULT'
    'Ach_Asic_No3_T_SD'
    'Ach_Asic_No3_OPEN_LOAD'
    'Ach_Asic_No3_GND_LOSS'
    'Ach_Asic_No3_LVT'
    'Ach_Asic_No3_LS_OVC'
    'Ach_Asic_No3_VGS_LS_FAULT'
    'Ach_Asic_No3_VGS_HS_FAULT'
    'Ach_Asic_No3_HS_SHORT'
    'Ach_Asic_No3_LS_CLAMP_ON'
    'Ach_Asic_No3_UNDER_CURR'
    'Ach_Asic_Nc0_AVG_CURRENT'
    'Ach_Asic_Nc0_PWM_FAULT'
    'Ach_Asic_Nc0_CURR_SENSE'
    'Ach_Asic_Nc0_ADC_FAULT'
    'Ach_Asic_Nc0_T_SD'
    'Ach_Asic_Nc0_OPEN_LOAD'
    'Ach_Asic_Nc0_GND_LOSS'
    'Ach_Asic_Nc0_LVT'
    'Ach_Asic_Nc0_LS_OVC'
    'Ach_Asic_Nc0_VGS_LS_FAULT'
    'Ach_Asic_Nc0_VGS_HS_FAULT'
    'Ach_Asic_Nc0_HS_SHORT'
    'Ach_Asic_Nc0_LS_CLAMP_ON'
    'Ach_Asic_Nc0_UNDER_CURR'
    'Ach_Asic_Nc1_AVG_CURRENT'
    'Ach_Asic_Nc1_PWM_FAULT'
    'Ach_Asic_Nc1_CURR_SENSE'
    'Ach_Asic_Nc1_ADC_FAULT'
    'Ach_Asic_Nc1_T_SD'
    'Ach_Asic_Nc1_OPEN_LOAD'
    'Ach_Asic_Nc1_GND_LOSS'
    'Ach_Asic_Nc1_LVT'
    'Ach_Asic_Nc1_LS_OVC'
    'Ach_Asic_Nc1_VGS_LS_FAULT'
    'Ach_Asic_Nc1_VGS_HS_FAULT'
    'Ach_Asic_Nc1_HS_SHORT'
    'Ach_Asic_Nc1_LS_CLAMP_ON'
    'Ach_Asic_Nc1_UNDER_CURR'
    'Ach_Asic_Nc2_AVG_CURRENT'
    'Ach_Asic_Nc2_PWM_FAULT'
    'Ach_Asic_Nc2_CURR_SENSE'
    'Ach_Asic_Nc2_ADC_FAULT'
    'Ach_Asic_Nc2_T_SD'
    'Ach_Asic_Nc2_OPEN_LOAD'
    'Ach_Asic_Nc2_GND_LOSS'
    'Ach_Asic_Nc2_LVT'
    'Ach_Asic_Nc2_LS_OVC'
    'Ach_Asic_Nc2_VGS_LS_FAULT'
    'Ach_Asic_Nc2_VGS_HS_FAULT'
    'Ach_Asic_Nc2_HS_SHORT'
    'Ach_Asic_Nc2_LS_CLAMP_ON'
    'Ach_Asic_Nc2_UNDER_CURR'
    'Ach_Asic_Nc3_AVG_CURRENT'
    'Ach_Asic_Nc3_PWM_FAULT'
    'Ach_Asic_Nc3_CURR_SENSE'
    'Ach_Asic_Nc3_ADC_FAULT'
    'Ach_Asic_Nc3_T_SD'
    'Ach_Asic_Nc3_OPEN_LOAD'
    'Ach_Asic_Nc3_GND_LOSS'
    'Ach_Asic_Nc3_LVT'
    'Ach_Asic_Nc3_LS_OVC'
    'Ach_Asic_Nc3_VGS_LS_FAULT'
    'Ach_Asic_Nc3_VGS_HS_FAULT'
    'Ach_Asic_Nc3_HS_SHORT'
    'Ach_Asic_Nc3_LS_CLAMP_ON'
    'Ach_Asic_Nc3_UNDER_CURR'
    'Ach_Asic_Tc0_AVG_CURRENT'
    'Ach_Asic_Tc0_PWM_FAULT'
    'Ach_Asic_Tc0_CURR_SENSE'
    'Ach_Asic_Tc0_ADC_FAULT'
    'Ach_Asic_Tc0_T_SD'
    'Ach_Asic_Tc0_OPEN_LOAD'
    'Ach_Asic_Tc0_GND_LOSS'
    'Ach_Asic_Tc0_LVT'
    'Ach_Asic_Tc0_LS_OVC'
    'Ach_Asic_Tc0_VGS_LS_FAULT'
    'Ach_Asic_Tc0_VGS_HS_FAULT'
    'Ach_Asic_Tc0_HS_SHORT'
    'Ach_Asic_Tc0_LS_CLAMP_ON'
    'Ach_Asic_Tc0_UNDER_CURR'
    'Ach_Asic_Tc1_AVG_CURRENT'
    'Ach_Asic_Tc1_PWM_FAULT'
    'Ach_Asic_Tc1_CURR_SENSE'
    'Ach_Asic_Tc1_ADC_FAULT'
    'Ach_Asic_Tc1_T_SD'
    'Ach_Asic_Tc1_OPEN_LOAD'
    'Ach_Asic_Tc1_GND_LOSS'
    'Ach_Asic_Tc1_LVT'
    'Ach_Asic_Tc1_LS_OVC'
    'Ach_Asic_Tc1_VGS_LS_FAULT'
    'Ach_Asic_Tc1_VGS_HS_FAULT'
    'Ach_Asic_Tc1_HS_SHORT'
    'Ach_Asic_Tc1_LS_CLAMP_ON'
    'Ach_Asic_Tc1_UNDER_CURR'
    'Ach_Asic_Esv0_AVG_CURRENT'
    'Ach_Asic_Esv0_PWM_FAULT'
    'Ach_Asic_Esv0_CURR_SENSE'
    'Ach_Asic_Esv0_ADC_FAULT'
    'Ach_Asic_Esv0_T_SD'
    'Ach_Asic_Esv0_OPEN_LOAD'
    'Ach_Asic_Esv0_GND_LOSS'
    'Ach_Asic_Esv0_LVT'
    'Ach_Asic_Esv0_LS_OVC'
    'Ach_Asic_Esv0_VGS_LS_FAULT'
    'Ach_Asic_Esv0_VGS_HS_FAULT'
    'Ach_Asic_Esv0_HS_SHORT'
    'Ach_Asic_Esv0_LS_CLAMP_ON'
    'Ach_Asic_Esv0_UNDER_CURR'
    'Ach_Asic_Esv1_AVG_CURRENT'
    'Ach_Asic_Esv1_PWM_FAULT'
    'Ach_Asic_Esv1_CURR_SENSE'
    'Ach_Asic_Esv1_ADC_FAULT'
    'Ach_Asic_Esv1_T_SD'
    'Ach_Asic_Esv1_OPEN_LOAD'
    'Ach_Asic_Esv1_GND_LOSS'
    'Ach_Asic_Esv1_LVT'
    'Ach_Asic_Esv1_LS_OVC'
    'Ach_Asic_Esv1_VGS_LS_FAULT'
    'Ach_Asic_Esv1_VGS_HS_FAULT'
    'Ach_Asic_Esv1_HS_SHORT'
    'Ach_Asic_Esv1_LS_CLAMP_ON'
    'Ach_Asic_Esv1_UNDER_CURR'
    };
BbsVlvM_MainAchValveAsicInfo = CreateBus(BbsVlvM_MainAchValveAsicInfo, DeList);
clear DeList;

BbsVlvM_MainVlvdFF0DcdInfo = Simulink.Bus;
DeList={
    'C0ol'
    'C0sb'
    'C0sg'
    'C1ol'
    'C1sb'
    'C1sg'
    'C2ol'
    'C2sb'
    'C2sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
BbsVlvM_MainVlvdFF0DcdInfo = CreateBus(BbsVlvM_MainVlvdFF0DcdInfo, DeList);
clear DeList;

BbsVlvM_MainVlvdFF1DcdInfo = Simulink.Bus;
DeList={
    'C3ol'
    'C3sb'
    'C3sg'
    'C4ol'
    'C4sb'
    'C4sg'
    'C5ol'
    'C5sb'
    'C5sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
BbsVlvM_MainVlvdFF1DcdInfo = CreateBus(BbsVlvM_MainVlvdFF1DcdInfo, DeList);
clear DeList;

AbsVlvM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
AbsVlvM_MainAchSysPwrAsicInfo = CreateBus(AbsVlvM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

AbsVlvM_MainAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_VPWR_UV'
    'Ach_Asic_VPWR_OV'
    'Ach_Asic_PMP_LD_ACT'
    'Ach_Asic_FS_TURN_ON'
    'Ach_Asic_FS_TURN_OFF'
    'Ach_Asic_FS_VDS_FAULT'
    'Ach_Asic_FS_RVP_FAULT'
    'Ach_Asic_VHD_OV'
    'Ach_Asic_T_SD_INT'
    'Ach_Asic_No0_AVG_CURRENT'
    'Ach_Asic_No0_PWM_FAULT'
    'Ach_Asic_No0_CURR_SENSE'
    'Ach_Asic_No0_ADC_FAULT'
    'Ach_Asic_No0_T_SD'
    'Ach_Asic_No0_OPEN_LOAD'
    'Ach_Asic_No0_GND_LOSS'
    'Ach_Asic_No0_LVT'
    'Ach_Asic_No0_LS_OVC'
    'Ach_Asic_No0_VGS_LS_FAULT'
    'Ach_Asic_No0_VGS_HS_FAULT'
    'Ach_Asic_No0_HS_SHORT'
    'Ach_Asic_No0_LS_CLAMP_ON'
    'Ach_Asic_No0_UNDER_CURR'
    'Ach_Asic_No1_AVG_CURRENT'
    'Ach_Asic_No1_PWM_FAULT'
    'Ach_Asic_No1_CURR_SENSE'
    'Ach_Asic_No1_ADC_FAULT'
    'Ach_Asic_No1_T_SD'
    'Ach_Asic_No1_OPEN_LOAD'
    'Ach_Asic_No1_GND_LOSS'
    'Ach_Asic_No1_LVT'
    'Ach_Asic_No1_LS_OVC'
    'Ach_Asic_No1_VGS_LS_FAULT'
    'Ach_Asic_No1_VGS_HS_FAULT'
    'Ach_Asic_No1_HS_SHORT'
    'Ach_Asic_No1_LS_CLAMP_ON'
    'Ach_Asic_No1_UNDER_CURR'
    'Ach_Asic_No2_AVG_CURRENT'
    'Ach_Asic_No2_PWM_FAULT'
    'Ach_Asic_No2_CURR_SENSE'
    'Ach_Asic_No2_ADC_FAULT'
    'Ach_Asic_No2_T_SD'
    'Ach_Asic_No2_OPEN_LOAD'
    'Ach_Asic_No2_GND_LOSS'
    'Ach_Asic_No2_LVT'
    'Ach_Asic_No2_LS_OVC'
    'Ach_Asic_No2_VGS_LS_FAULT'
    'Ach_Asic_No2_VGS_HS_FAULT'
    'Ach_Asic_No2_HS_SHORT'
    'Ach_Asic_No2_LS_CLAMP_ON'
    'Ach_Asic_No2_UNDER_CURR'
    'Ach_Asic_No3_AVG_CURRENT'
    'Ach_Asic_No3_PWM_FAULT'
    'Ach_Asic_No3_CURR_SENSE'
    'Ach_Asic_No3_ADC_FAULT'
    'Ach_Asic_No3_T_SD'
    'Ach_Asic_No3_OPEN_LOAD'
    'Ach_Asic_No3_GND_LOSS'
    'Ach_Asic_No3_LVT'
    'Ach_Asic_No3_LS_OVC'
    'Ach_Asic_No3_VGS_LS_FAULT'
    'Ach_Asic_No3_VGS_HS_FAULT'
    'Ach_Asic_No3_HS_SHORT'
    'Ach_Asic_No3_LS_CLAMP_ON'
    'Ach_Asic_No3_UNDER_CURR'
    'Ach_Asic_Nc0_AVG_CURRENT'
    'Ach_Asic_Nc0_PWM_FAULT'
    'Ach_Asic_Nc0_CURR_SENSE'
    'Ach_Asic_Nc0_ADC_FAULT'
    'Ach_Asic_Nc0_T_SD'
    'Ach_Asic_Nc0_OPEN_LOAD'
    'Ach_Asic_Nc0_GND_LOSS'
    'Ach_Asic_Nc0_LVT'
    'Ach_Asic_Nc0_LS_OVC'
    'Ach_Asic_Nc0_VGS_LS_FAULT'
    'Ach_Asic_Nc0_VGS_HS_FAULT'
    'Ach_Asic_Nc0_HS_SHORT'
    'Ach_Asic_Nc0_LS_CLAMP_ON'
    'Ach_Asic_Nc0_UNDER_CURR'
    'Ach_Asic_Nc1_AVG_CURRENT'
    'Ach_Asic_Nc1_PWM_FAULT'
    'Ach_Asic_Nc1_CURR_SENSE'
    'Ach_Asic_Nc1_ADC_FAULT'
    'Ach_Asic_Nc1_T_SD'
    'Ach_Asic_Nc1_OPEN_LOAD'
    'Ach_Asic_Nc1_GND_LOSS'
    'Ach_Asic_Nc1_LVT'
    'Ach_Asic_Nc1_LS_OVC'
    'Ach_Asic_Nc1_VGS_LS_FAULT'
    'Ach_Asic_Nc1_VGS_HS_FAULT'
    'Ach_Asic_Nc1_HS_SHORT'
    'Ach_Asic_Nc1_LS_CLAMP_ON'
    'Ach_Asic_Nc1_UNDER_CURR'
    'Ach_Asic_Nc2_AVG_CURRENT'
    'Ach_Asic_Nc2_PWM_FAULT'
    'Ach_Asic_Nc2_CURR_SENSE'
    'Ach_Asic_Nc2_ADC_FAULT'
    'Ach_Asic_Nc2_T_SD'
    'Ach_Asic_Nc2_OPEN_LOAD'
    'Ach_Asic_Nc2_GND_LOSS'
    'Ach_Asic_Nc2_LVT'
    'Ach_Asic_Nc2_LS_OVC'
    'Ach_Asic_Nc2_VGS_LS_FAULT'
    'Ach_Asic_Nc2_VGS_HS_FAULT'
    'Ach_Asic_Nc2_HS_SHORT'
    'Ach_Asic_Nc2_LS_CLAMP_ON'
    'Ach_Asic_Nc2_UNDER_CURR'
    'Ach_Asic_Nc3_AVG_CURRENT'
    'Ach_Asic_Nc3_PWM_FAULT'
    'Ach_Asic_Nc3_CURR_SENSE'
    'Ach_Asic_Nc3_ADC_FAULT'
    'Ach_Asic_Nc3_T_SD'
    'Ach_Asic_Nc3_OPEN_LOAD'
    'Ach_Asic_Nc3_GND_LOSS'
    'Ach_Asic_Nc3_LVT'
    'Ach_Asic_Nc3_LS_OVC'
    'Ach_Asic_Nc3_VGS_LS_FAULT'
    'Ach_Asic_Nc3_VGS_HS_FAULT'
    'Ach_Asic_Nc3_HS_SHORT'
    'Ach_Asic_Nc3_LS_CLAMP_ON'
    'Ach_Asic_Nc3_UNDER_CURR'
    'Ach_Asic_Tc0_AVG_CURRENT'
    'Ach_Asic_Tc0_PWM_FAULT'
    'Ach_Asic_Tc0_CURR_SENSE'
    'Ach_Asic_Tc0_ADC_FAULT'
    'Ach_Asic_Tc0_T_SD'
    'Ach_Asic_Tc0_OPEN_LOAD'
    'Ach_Asic_Tc0_GND_LOSS'
    'Ach_Asic_Tc0_LVT'
    'Ach_Asic_Tc0_LS_OVC'
    'Ach_Asic_Tc0_VGS_LS_FAULT'
    'Ach_Asic_Tc0_VGS_HS_FAULT'
    'Ach_Asic_Tc0_HS_SHORT'
    'Ach_Asic_Tc0_LS_CLAMP_ON'
    'Ach_Asic_Tc0_UNDER_CURR'
    'Ach_Asic_Tc1_AVG_CURRENT'
    'Ach_Asic_Tc1_PWM_FAULT'
    'Ach_Asic_Tc1_CURR_SENSE'
    'Ach_Asic_Tc1_ADC_FAULT'
    'Ach_Asic_Tc1_T_SD'
    'Ach_Asic_Tc1_OPEN_LOAD'
    'Ach_Asic_Tc1_GND_LOSS'
    'Ach_Asic_Tc1_LVT'
    'Ach_Asic_Tc1_LS_OVC'
    'Ach_Asic_Tc1_VGS_LS_FAULT'
    'Ach_Asic_Tc1_VGS_HS_FAULT'
    'Ach_Asic_Tc1_HS_SHORT'
    'Ach_Asic_Tc1_LS_CLAMP_ON'
    'Ach_Asic_Tc1_UNDER_CURR'
    'Ach_Asic_Esv0_AVG_CURRENT'
    'Ach_Asic_Esv0_PWM_FAULT'
    'Ach_Asic_Esv0_CURR_SENSE'
    'Ach_Asic_Esv0_ADC_FAULT'
    'Ach_Asic_Esv0_T_SD'
    'Ach_Asic_Esv0_OPEN_LOAD'
    'Ach_Asic_Esv0_GND_LOSS'
    'Ach_Asic_Esv0_LVT'
    'Ach_Asic_Esv0_LS_OVC'
    'Ach_Asic_Esv0_VGS_LS_FAULT'
    'Ach_Asic_Esv0_VGS_HS_FAULT'
    'Ach_Asic_Esv0_HS_SHORT'
    'Ach_Asic_Esv0_LS_CLAMP_ON'
    'Ach_Asic_Esv0_UNDER_CURR'
    'Ach_Asic_Esv1_AVG_CURRENT'
    'Ach_Asic_Esv1_PWM_FAULT'
    'Ach_Asic_Esv1_CURR_SENSE'
    'Ach_Asic_Esv1_ADC_FAULT'
    'Ach_Asic_Esv1_T_SD'
    'Ach_Asic_Esv1_OPEN_LOAD'
    'Ach_Asic_Esv1_GND_LOSS'
    'Ach_Asic_Esv1_LVT'
    'Ach_Asic_Esv1_LS_OVC'
    'Ach_Asic_Esv1_VGS_LS_FAULT'
    'Ach_Asic_Esv1_VGS_HS_FAULT'
    'Ach_Asic_Esv1_HS_SHORT'
    'Ach_Asic_Esv1_LS_CLAMP_ON'
    'Ach_Asic_Esv1_UNDER_CURR'
    };
AbsVlvM_MainAchValveAsicInfo = CreateBus(AbsVlvM_MainAchValveAsicInfo, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD1SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012M_MainMpsD1SpiDcdInfo = CreateBus(Mps_TLE5012M_MainMpsD1SpiDcdInfo, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD2SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012M_MainMpsD2SpiDcdInfo = CreateBus(Mps_TLE5012M_MainMpsD2SpiDcdInfo, DeList);
clear DeList;

MgdM_MainMgdDcdInfo = Simulink.Bus;
DeList={
    'mgdIdleM'
    'mgdConfM'
    'mgdConfLock'
    'mgdSelfTestM'
    'mgdSoffM'
    'mgdErrM'
    'mgdRectM'
    'mgdNormM'
    'mgdOsf'
    'mgdOp'
    'mgdScd'
    'mgdSd'
    'mgdIndiag'
    'mgdOutp'
    'mgdExt'
    'mgdInt12'
    'mgdRom'
    'mgdLimpOn'
    'mgdStIncomplete'
    'mgdApcAct'
    'mgdGtm'
    'mgdCtrlRegInvalid'
    'mgdLfw'
    'mgdErrOtW'
    'mgdErrOvReg1'
    'mgdErrUvVccRom'
    'mgdErrUvReg4'
    'mgdErrOvReg6'
    'mgdErrUvReg6'
    'mgdErrUvReg5'
    'mgdErrUvCb'
    'mgdErrClkTrim'
    'mgdErrUvBs3'
    'mgdErrUvBs2'
    'mgdErrUvBs1'
    'mgdErrCp2'
    'mgdErrCp1'
    'mgdErrOvBs3'
    'mgdErrOvBs2'
    'mgdErrOvBs1'
    'mgdErrOvVdh'
    'mgdErrUvVdh'
    'mgdErrOvVs'
    'mgdErrUvVs'
    'mgdErrUvVcc'
    'mgdErrOvVcc'
    'mgdErrOvLdVdh'
    'mgdSdDdpStuck'
    'mgdSdCp1'
    'mgdSdOvCp'
    'mgdSdClkfail'
    'mgdSdUvCb'
    'mgdSdOvVdh'
    'mgdSdOvVs'
    'mgdSdOt'
    'mgdErrScdLs3'
    'mgdErrScdLs2'
    'mgdErrScdLs1'
    'mgdErrScdHs3'
    'mgdErrScdHs2'
    'mgdErrScdHs1'
    'mgdErrIndLs3'
    'mgdErrIndLs2'
    'mgdErrIndLs1'
    'mgdErrIndHs3'
    'mgdErrIndHs2'
    'mgdErrIndHs1'
    'mgdErrOsfLs1'
    'mgdErrOsfLs2'
    'mgdErrOsfLs3'
    'mgdErrOsfHs1'
    'mgdErrOsfHs2'
    'mgdErrOsfHs3'
    'mgdErrSpiFrame'
    'mgdErrSpiTo'
    'mgdErrSpiWd'
    'mgdErrSpiCrc'
    'mgdEpiAddInvalid'
    'mgdEonfTo'
    'mgdEonfSigInvalid'
    'mgdErrOcOp1'
    'mgdErrOp1Uv'
    'mgdErrOp1Ov'
    'mgdErrOp1Calib'
    'mgdErrOcOp2'
    'mgdErrOp2Uv'
    'mgdErrOp2Ov'
    'mgdErrOp2Calib'
    'mgdErrOcOp3'
    'mgdErrOp3Uv'
    'mgdErrOp3Ov'
    'mgdErrOp3Calib'
    'mgdErrOutpErrn'
    'mgdErrOutpMiso'
    'mgdErrOutpPFB1'
    'mgdErrOutpPFB2'
    'mgdErrOutpPFB3'
    'mgdTle9180ErrPort'
    };
MgdM_MainMgdDcdInfo = CreateBus(MgdM_MainMgdDcdInfo, DeList);
clear DeList;

MtrM_MainMotMonInfo = Simulink.Bus;
DeList={
    'MotPwrVoltMon'
    'MotStarMon'
    };
MtrM_MainMotMonInfo = CreateBus(MtrM_MainMotMonInfo, DeList);
clear DeList;

MtrM_MainMotVoltsMonInfo = Simulink.Bus;
DeList={
    'MotVoltPhUMon'
    'MotVoltPhVMon'
    'MotVoltPhWMon'
    };
MtrM_MainMotVoltsMonInfo = CreateBus(MtrM_MainMotVoltsMonInfo, DeList);
clear DeList;

MtrM_MainMpsD1SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
MtrM_MainMpsD1SpiDcdInfo = CreateBus(MtrM_MainMpsD1SpiDcdInfo, DeList);
clear DeList;

MtrM_MainMpsD2SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
MtrM_MainMpsD2SpiDcdInfo = CreateBus(MtrM_MainMpsD2SpiDcdInfo, DeList);
clear DeList;

MtrM_MainMgdDcdInfo = Simulink.Bus;
DeList={
    'mgdIdleM'
    'mgdConfM'
    'mgdConfLock'
    'mgdSelfTestM'
    'mgdSoffM'
    'mgdErrM'
    'mgdRectM'
    'mgdNormM'
    'mgdOsf'
    'mgdOp'
    'mgdScd'
    'mgdSd'
    'mgdIndiag'
    'mgdOutp'
    'mgdExt'
    'mgdInt12'
    'mgdRom'
    'mgdLimpOn'
    'mgdStIncomplete'
    'mgdApcAct'
    'mgdGtm'
    'mgdCtrlRegInvalid'
    'mgdLfw'
    'mgdErrOtW'
    'mgdErrOvReg1'
    'mgdErrUvVccRom'
    'mgdErrUvReg4'
    'mgdErrOvReg6'
    'mgdErrUvReg6'
    'mgdErrUvReg5'
    'mgdErrUvCb'
    'mgdErrClkTrim'
    'mgdErrUvBs3'
    'mgdErrUvBs2'
    'mgdErrUvBs1'
    'mgdErrCp2'
    'mgdErrCp1'
    'mgdErrOvBs3'
    'mgdErrOvBs2'
    'mgdErrOvBs1'
    'mgdErrOvVdh'
    'mgdErrUvVdh'
    'mgdErrOvVs'
    'mgdErrUvVs'
    'mgdErrUvVcc'
    'mgdErrOvVcc'
    'mgdErrOvLdVdh'
    'mgdSdDdpStuck'
    'mgdSdCp1'
    'mgdSdOvCp'
    'mgdSdClkfail'
    'mgdSdUvCb'
    'mgdSdOvVdh'
    'mgdSdOvVs'
    'mgdSdOt'
    'mgdErrScdLs3'
    'mgdErrScdLs2'
    'mgdErrScdLs1'
    'mgdErrScdHs3'
    'mgdErrScdHs2'
    'mgdErrScdHs1'
    'mgdErrIndLs3'
    'mgdErrIndLs2'
    'mgdErrIndLs1'
    'mgdErrIndHs3'
    'mgdErrIndHs2'
    'mgdErrIndHs1'
    'mgdErrOsfLs1'
    'mgdErrOsfLs2'
    'mgdErrOsfLs3'
    'mgdErrOsfHs1'
    'mgdErrOsfHs2'
    'mgdErrOsfHs3'
    'mgdErrSpiFrame'
    'mgdErrSpiTo'
    'mgdErrSpiWd'
    'mgdErrSpiCrc'
    'mgdEpiAddInvalid'
    'mgdEonfTo'
    'mgdEonfSigInvalid'
    'mgdErrOcOp1'
    'mgdErrOp1Uv'
    'mgdErrOp1Ov'
    'mgdErrOp1Calib'
    'mgdErrOcOp2'
    'mgdErrOp2Uv'
    'mgdErrOp2Ov'
    'mgdErrOp2Calib'
    'mgdErrOcOp3'
    'mgdErrOp3Uv'
    'mgdErrOp3Ov'
    'mgdErrOp3Calib'
    'mgdErrOutpErrn'
    'mgdErrOutpMiso'
    'mgdErrOutpPFB1'
    'mgdErrOutpPFB2'
    'mgdErrOutpPFB3'
    'mgdTle9180ErrPort'
    };
MtrM_MainMgdDcdInfo = CreateBus(MtrM_MainMgdDcdInfo, DeList);
clear DeList;

MtrM_MainMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
MtrM_MainMotDqIRefMccInfo = CreateBus(MtrM_MainMotDqIRefMccInfo, DeList);
clear DeList;

MtrM_MainHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
MtrM_MainHwTrigMotInfo = CreateBus(MtrM_MainHwTrigMotInfo, DeList);
clear DeList;

MtrM_MainMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
MtrM_MainMotCurrMonInfo = CreateBus(MtrM_MainMotCurrMonInfo, DeList);
clear DeList;

MtrM_MainMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
MtrM_MainMotAngleMonInfo = CreateBus(MtrM_MainMotAngleMonInfo, DeList);
clear DeList;

MtrM_MainMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
MtrM_MainMtrProcessOutInfo = CreateBus(MtrM_MainMtrProcessOutInfo, DeList);
clear DeList;

Eem_MainRelayMonitorData = Simulink.Bus;
DeList={
    'RlyM_HDCRelay_Open_Err'
    'RlyM_HDCRelay_Short_Err'
    'RlyM_ESSRelay_Open_Err'
    'RlyM_ESSRelay_Short_Err'
    };
Eem_MainRelayMonitorData = CreateBus(Eem_MainRelayMonitorData, DeList);
clear DeList;

Eem_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
Eem_MainSenPwrMonitorData = CreateBus(Eem_MainSenPwrMonitorData, DeList);
clear DeList;

Eem_MainSwtMonFaultInfo = Simulink.Bus;
DeList={
    'SWM_ESCOFF_Sw_Err'
    'SWM_HDC_Sw_Err'
    'SWM_AVH_Sw_Err'
    'SWM_BFL_Sw_Err'
    'SWM_PB_Sw_Err'
    };
Eem_MainSwtMonFaultInfo = CreateBus(Eem_MainSwtMonFaultInfo, DeList);
clear DeList;

Eem_MainSwtPFaultInfo = Simulink.Bus;
DeList={
    'SWM_BLS_Sw_HighStick_Err'
    'SWM_BLS_Sw_LowStick_Err'
    'SWM_BS_Sw_HighStick_Err'
    'SWM_BS_Sw_LowStick_Err'
    };
Eem_MainSwtPFaultInfo = CreateBus(Eem_MainSwtPFaultInfo, DeList);
clear DeList;

Eem_MainYAWMOutInfo = Simulink.Bus;
DeList={
    'YAWM_Timeout_Err'
    'YAWM_Invalid_Err'
    'YAWM_CRC_Err'
    'YAWM_Rolling_Err'
    'YAWM_Temperature_Err'
    'YAWM_Range_Err'
    };
Eem_MainYAWMOutInfo = CreateBus(Eem_MainYAWMOutInfo, DeList);
clear DeList;

Eem_MainAYMOuitInfo = Simulink.Bus;
DeList={
    'AYM_Timeout_Err'
    'AYM_Invalid_Err'
    'AYM_CRC_Err'
    'AYM_Rolling_Err'
    'AYM_Temperature_Err'
    'AYM_Range_Err'
    };
Eem_MainAYMOuitInfo = CreateBus(Eem_MainAYMOuitInfo, DeList);
clear DeList;

Eem_MainAXMOutInfo = Simulink.Bus;
DeList={
    'AXM_Timeout_Err'
    'AXM_Invalid_Err'
    'AXM_CRC_Err'
    'AXM_Rolling_Err'
    'AXM_Temperature_Err'
    'AXM_Range_Err'
    };
Eem_MainAXMOutInfo = CreateBus(Eem_MainAXMOutInfo, DeList);
clear DeList;

Eem_MainSASMOutInfo = Simulink.Bus;
DeList={
    'SASM_Timeout_Err'
    'SASM_Invalid_Err'
    'SASM_CRC_Err'
    'SASM_Rolling_Err'
    'SASM_Range_Err'
    };
Eem_MainSASMOutInfo = CreateBus(Eem_MainSASMOutInfo, DeList);
clear DeList;

Eem_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    'CanM_SubBusOff_Err'
    'CanM_MainOverRun_Err'
    'CanM_SubOverRun_Err'
    'CanM_EMS1_Tout_Err'
    'CanM_EMS2_Tout_Err'
    'CanM_TCU1_Tout_Err'
    'CanM_TCU5_Tout_Err'
    'CanM_FWD1_Tout_Err'
    'CanM_HCU1_Tout_Err'
    'CanM_HCU2_Tout_Err'
    'CanM_HCU3_Tout_Err'
    'CanM_VSM2_Tout_Err'
    'CanM_EMS_Invalid_Err'
    'CanM_TCU_Invalid_Err'
    };
Eem_MainCanMonData = CreateBus(Eem_MainCanMonData, DeList);
clear DeList;

Eem_MainYawPlauOutput = Simulink.Bus;
DeList={
    'YawPlauModelErr'
    'YawPlauNoisekErr'
    'YawPlauShockErr'
    'YawPlauRangeErr'
    'YawPlauStandStillErr'
    };
Eem_MainYawPlauOutput = CreateBus(Eem_MainYawPlauOutput, DeList);
clear DeList;

Eem_MainAxPlauOutput = Simulink.Bus;
DeList={
    'AxPlauOffsetErr'
    'AxPlauDrivingOffsetErr'
    'AxPlauStickErr'
    'AxPlauNoiseErr'
    };
Eem_MainAxPlauOutput = CreateBus(Eem_MainAxPlauOutput, DeList);
clear DeList;

Eem_MainAyPlauOutput = Simulink.Bus;
DeList={
    'AyPlauNoiselErr'
    'AyPlauModelErr'
    'AyPlauShockErr'
    'AyPlauRangeErr'
    'AyPlauStandStillErr'
    };
Eem_MainAyPlauOutput = CreateBus(Eem_MainAyPlauOutput, DeList);
clear DeList;

Eem_MainSasPlauOutput = Simulink.Bus;
DeList={
    'SasPlauModelErr'
    'SasPlauOffsetErr'
    'SasPlauStickErr'
    };
Eem_MainSasPlauOutput = CreateBus(Eem_MainSasPlauOutput, DeList);
clear DeList;

Arbitrator_VlvNormVlvReqSesInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    };
Arbitrator_VlvNormVlvReqSesInfo = CreateBus(Arbitrator_VlvNormVlvReqSesInfo, DeList);
clear DeList;

Arbitrator_VlvWhlVlvReqSesInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    };
Arbitrator_VlvWhlVlvReqSesInfo = CreateBus(Arbitrator_VlvWhlVlvReqSesInfo, DeList);
clear DeList;

Arbitrator_VlvDiagVlvActrInfo = Simulink.Bus;
DeList={
    'SolenoidDrvDuty'
    'FlOvReqData'
    'FlIvReqData'
    'FrOvReqData'
    'FrIvReqData'
    'RlOvReqData'
    'RlIvReqData'
    'RrOvReqData'
    'RrIvReqData'
    'PrimCutVlvReqData'
    'SecdCutVlvReqData'
    'SimVlvReqData'
    'ResPVlvReqData'
    'BalVlvReqData'
    'CircVlvReqData'
    'PressDumpReqData'
    'RelsVlvReqData'
    };
Arbitrator_VlvDiagVlvActrInfo = CreateBus(Arbitrator_VlvDiagVlvActrInfo, DeList);
clear DeList;

Ioc_InputSR5msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_InputSR5msAcmAsicInitCompleteFlag'};
Ioc_InputSR5msAcmAsicInitCompleteFlag = CreateBus(Ioc_InputSR5msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Prly_HndlrCEMon = Simulink.Bus;
DeList={'Prly_HndlrCEMon'};
Prly_HndlrCEMon = CreateBus(Prly_HndlrCEMon, DeList);
clear DeList;

Swt_SenRsmDbcMon = Simulink.Bus;
DeList={'Swt_SenRsmDbcMon'};
Swt_SenRsmDbcMon = CreateBus(Swt_SenRsmDbcMon, DeList);
clear DeList;

Spc_5msCtrlMotCtrlState = Simulink.Bus;
DeList={'Spc_5msCtrlMotCtrlState'};
Spc_5msCtrlMotCtrlState = CreateBus(Spc_5msCtrlMotCtrlState, DeList);
clear DeList;

Abc_CtrlVBatt1Mon = Simulink.Bus;
DeList={'Abc_CtrlVBatt1Mon'};
Abc_CtrlVBatt1Mon = CreateBus(Abc_CtrlVBatt1Mon, DeList);
clear DeList;

Abc_CtrlDiagSci = Simulink.Bus;
DeList={'Abc_CtrlDiagSci'};
Abc_CtrlDiagSci = CreateBus(Abc_CtrlDiagSci, DeList);
clear DeList;

Pct_5msCtrlMotCtrlMode = Simulink.Bus;
DeList={'Pct_5msCtrlMotCtrlMode'};
Pct_5msCtrlMotCtrlMode = CreateBus(Pct_5msCtrlMotCtrlMode, DeList);
clear DeList;

Pct_5msCtrlMotICtrlFadeOutState = Simulink.Bus;
DeList={'Pct_5msCtrlMotICtrlFadeOutState'};
Pct_5msCtrlMotICtrlFadeOutState = CreateBus(Pct_5msCtrlMotICtrlFadeOutState, DeList);
clear DeList;

Pct_5msCtrlPreBoostMod = Simulink.Bus;
DeList={'Pct_5msCtrlPreBoostMod'};
Pct_5msCtrlPreBoostMod = CreateBus(Pct_5msCtrlPreBoostMod, DeList);
clear DeList;

Pct_5msCtrlStkRecvryStabnEndOK = Simulink.Bus;
DeList={'Pct_5msCtrlStkRecvryStabnEndOK'};
Pct_5msCtrlStkRecvryStabnEndOK = CreateBus(Pct_5msCtrlStkRecvryStabnEndOK, DeList);
clear DeList;

SysPwrM_MainVBatt1Mon = Simulink.Bus;
DeList={'SysPwrM_MainVBatt1Mon'};
SysPwrM_MainVBatt1Mon = CreateBus(SysPwrM_MainVBatt1Mon, DeList);
clear DeList;

SysPwrM_MainVBatt2Mon = Simulink.Bus;
DeList={'SysPwrM_MainVBatt2Mon'};
SysPwrM_MainVBatt2Mon = CreateBus(SysPwrM_MainVBatt2Mon, DeList);
clear DeList;

SysPwrM_MainDiagClrSrs = Simulink.Bus;
DeList={'SysPwrM_MainDiagClrSrs'};
SysPwrM_MainDiagClrSrs = CreateBus(SysPwrM_MainDiagClrSrs, DeList);
clear DeList;

SysPwrM_MainVdd1Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd1Mon'};
SysPwrM_MainVdd1Mon = CreateBus(SysPwrM_MainVdd1Mon, DeList);
clear DeList;

SysPwrM_MainVdd2Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd2Mon'};
SysPwrM_MainVdd2Mon = CreateBus(SysPwrM_MainVdd2Mon, DeList);
clear DeList;

SysPwrM_MainVdd3Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd3Mon'};
SysPwrM_MainVdd3Mon = CreateBus(SysPwrM_MainVdd3Mon, DeList);
clear DeList;

SysPwrM_MainVdd4Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd4Mon'};
SysPwrM_MainVdd4Mon = CreateBus(SysPwrM_MainVdd4Mon, DeList);
clear DeList;

SysPwrM_MainVdd5Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd5Mon'};
SysPwrM_MainVdd5Mon = CreateBus(SysPwrM_MainVdd5Mon, DeList);
clear DeList;

SysPwrM_MainVddMon = Simulink.Bus;
DeList={'SysPwrM_MainVddMon'};
SysPwrM_MainVddMon = CreateBus(SysPwrM_MainVddMon, DeList);
clear DeList;

SysPwrM_MainMtrArbDriveState = Simulink.Bus;
DeList={'SysPwrM_MainMtrArbDriveState'};
SysPwrM_MainMtrArbDriveState = CreateBus(SysPwrM_MainMtrArbDriveState, DeList);
clear DeList;

EcuHwCtrlM_MainAchAsicInvalid = Simulink.Bus;
DeList={'EcuHwCtrlM_MainAchAsicInvalid'};
EcuHwCtrlM_MainAchAsicInvalid = CreateBus(EcuHwCtrlM_MainAchAsicInvalid, DeList);
clear DeList;

EcuHwCtrlM_MainVBatt1Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVBatt1Mon'};
EcuHwCtrlM_MainVBatt1Mon = CreateBus(EcuHwCtrlM_MainVBatt1Mon, DeList);
clear DeList;

EcuHwCtrlM_MainDiagClrSrs = Simulink.Bus;
DeList={'EcuHwCtrlM_MainDiagClrSrs'};
EcuHwCtrlM_MainDiagClrSrs = CreateBus(EcuHwCtrlM_MainDiagClrSrs, DeList);
clear DeList;

EcuHwCtrlM_MainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'EcuHwCtrlM_MainAcmAsicInitCompleteFlag'};
EcuHwCtrlM_MainAcmAsicInitCompleteFlag = CreateBus(EcuHwCtrlM_MainAcmAsicInitCompleteFlag, DeList);
clear DeList;

EcuHwCtrlM_MainVdd1Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd1Mon'};
EcuHwCtrlM_MainVdd1Mon = CreateBus(EcuHwCtrlM_MainVdd1Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd2Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd2Mon'};
EcuHwCtrlM_MainVdd2Mon = CreateBus(EcuHwCtrlM_MainVdd2Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd3Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd3Mon'};
EcuHwCtrlM_MainVdd3Mon = CreateBus(EcuHwCtrlM_MainVdd3Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd4Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd4Mon'};
EcuHwCtrlM_MainVdd4Mon = CreateBus(EcuHwCtrlM_MainVdd4Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd5Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd5Mon'};
EcuHwCtrlM_MainVdd5Mon = CreateBus(EcuHwCtrlM_MainVdd5Mon, DeList);
clear DeList;

EcuHwCtrlM_MainAdcInvalid = Simulink.Bus;
DeList={'EcuHwCtrlM_MainAdcInvalid'};
EcuHwCtrlM_MainAdcInvalid = CreateBus(EcuHwCtrlM_MainAdcInvalid, DeList);
clear DeList;

WssM_MainAchAsicInvalid = Simulink.Bus;
DeList={'WssM_MainAchAsicInvalid'};
WssM_MainAchAsicInvalid = CreateBus(WssM_MainAchAsicInvalid, DeList);
clear DeList;

WssM_MainVBatt1Mon = Simulink.Bus;
DeList={'WssM_MainVBatt1Mon'};
WssM_MainVBatt1Mon = CreateBus(WssM_MainVBatt1Mon, DeList);
clear DeList;

WssM_MainDiagClrSrs = Simulink.Bus;
DeList={'WssM_MainDiagClrSrs'};
WssM_MainDiagClrSrs = CreateBus(WssM_MainDiagClrSrs, DeList);
clear DeList;

PressM_MainVBatt1Mon = Simulink.Bus;
DeList={'PressM_MainVBatt1Mon'};
PressM_MainVBatt1Mon = CreateBus(PressM_MainVBatt1Mon, DeList);
clear DeList;

PressM_MainDiagClrSrs = Simulink.Bus;
DeList={'PressM_MainDiagClrSrs'};
PressM_MainDiagClrSrs = CreateBus(PressM_MainDiagClrSrs, DeList);
clear DeList;

PedalM_MainAchAsicInvalid = Simulink.Bus;
DeList={'PedalM_MainAchAsicInvalid'};
PedalM_MainAchAsicInvalid = CreateBus(PedalM_MainAchAsicInvalid, DeList);
clear DeList;

PedalM_MainDiagClrSrs = Simulink.Bus;
DeList={'PedalM_MainDiagClrSrs'};
PedalM_MainDiagClrSrs = CreateBus(PedalM_MainDiagClrSrs, DeList);
clear DeList;

PedalM_MainVdd3Mon = Simulink.Bus;
DeList={'PedalM_MainVdd3Mon'};
PedalM_MainVdd3Mon = CreateBus(PedalM_MainVdd3Mon, DeList);
clear DeList;

WssP_MainDiagClrSrs = Simulink.Bus;
DeList={'WssP_MainDiagClrSrs'};
WssP_MainDiagClrSrs = CreateBus(WssP_MainDiagClrSrs, DeList);
clear DeList;

PressP_MainVBatt1Mon = Simulink.Bus;
DeList={'PressP_MainVBatt1Mon'};
PressP_MainVBatt1Mon = CreateBus(PressP_MainVBatt1Mon, DeList);
clear DeList;

PressP_MainDiagClrSrs = Simulink.Bus;
DeList={'PressP_MainDiagClrSrs'};
PressP_MainDiagClrSrs = CreateBus(PressP_MainDiagClrSrs, DeList);
clear DeList;

PedalP_MainDiagClrSrs = Simulink.Bus;
DeList={'PedalP_MainDiagClrSrs'};
PedalP_MainDiagClrSrs = CreateBus(PedalP_MainDiagClrSrs, DeList);
clear DeList;

BbsVlvM_MainAchAsicInvalid = Simulink.Bus;
DeList={'BbsVlvM_MainAchAsicInvalid'};
BbsVlvM_MainAchAsicInvalid = CreateBus(BbsVlvM_MainAchAsicInvalid, DeList);
clear DeList;

BbsVlvM_MainVBatt1Mon = Simulink.Bus;
DeList={'BbsVlvM_MainVBatt1Mon'};
BbsVlvM_MainVBatt1Mon = CreateBus(BbsVlvM_MainVBatt1Mon, DeList);
clear DeList;

BbsVlvM_MainVBatt2Mon = Simulink.Bus;
DeList={'BbsVlvM_MainVBatt2Mon'};
BbsVlvM_MainVBatt2Mon = CreateBus(BbsVlvM_MainVBatt2Mon, DeList);
clear DeList;

BbsVlvM_MainDiagClrSrs = Simulink.Bus;
DeList={'BbsVlvM_MainDiagClrSrs'};
BbsVlvM_MainDiagClrSrs = CreateBus(BbsVlvM_MainDiagClrSrs, DeList);
clear DeList;

BbsVlvM_MainFspCbsHMon = Simulink.Bus;
DeList={'BbsVlvM_MainFspCbsHMon'};
BbsVlvM_MainFspCbsHMon = CreateBus(BbsVlvM_MainFspCbsHMon, DeList);
clear DeList;

BbsVlvM_MainFspCbsMon = Simulink.Bus;
DeList={'BbsVlvM_MainFspCbsMon'};
BbsVlvM_MainFspCbsMon = CreateBus(BbsVlvM_MainFspCbsMon, DeList);
clear DeList;

BbsVlvM_MainVlvdInvalid = Simulink.Bus;
DeList={'BbsVlvM_MainVlvdInvalid'};
BbsVlvM_MainVlvdInvalid = CreateBus(BbsVlvM_MainVlvdInvalid, DeList);
clear DeList;

AbsVlvM_MainAchAsicInvalid = Simulink.Bus;
DeList={'AbsVlvM_MainAchAsicInvalid'};
AbsVlvM_MainAchAsicInvalid = CreateBus(AbsVlvM_MainAchAsicInvalid, DeList);
clear DeList;

AbsVlvM_MainVBatt1Mon = Simulink.Bus;
DeList={'AbsVlvM_MainVBatt1Mon'};
AbsVlvM_MainVBatt1Mon = CreateBus(AbsVlvM_MainVBatt1Mon, DeList);
clear DeList;

AbsVlvM_MainVBatt2Mon = Simulink.Bus;
DeList={'AbsVlvM_MainVBatt2Mon'};
AbsVlvM_MainVBatt2Mon = CreateBus(AbsVlvM_MainVBatt2Mon, DeList);
clear DeList;

AbsVlvM_MainDiagClrSrs = Simulink.Bus;
DeList={'AbsVlvM_MainDiagClrSrs'};
AbsVlvM_MainDiagClrSrs = CreateBus(AbsVlvM_MainDiagClrSrs, DeList);
clear DeList;

AbsVlvM_MainFspAbsHMon = Simulink.Bus;
DeList={'AbsVlvM_MainFspAbsHMon'};
AbsVlvM_MainFspAbsHMon = CreateBus(AbsVlvM_MainFspAbsHMon, DeList);
clear DeList;

AbsVlvM_MainFspAbsMon = Simulink.Bus;
DeList={'AbsVlvM_MainFspAbsMon'};
AbsVlvM_MainFspAbsMon = CreateBus(AbsVlvM_MainFspAbsMon, DeList);
clear DeList;

Mps_TLE5012M_MainVBatt1Mon = Simulink.Bus;
DeList={'Mps_TLE5012M_MainVBatt1Mon'};
Mps_TLE5012M_MainVBatt1Mon = CreateBus(Mps_TLE5012M_MainVBatt1Mon, DeList);
clear DeList;

Mps_TLE5012M_MainDiagClrSrs = Simulink.Bus;
DeList={'Mps_TLE5012M_MainDiagClrSrs'};
Mps_TLE5012M_MainDiagClrSrs = CreateBus(Mps_TLE5012M_MainDiagClrSrs, DeList);
clear DeList;

Mps_TLE5012M_MainMpsInvalid = Simulink.Bus;
DeList={'Mps_TLE5012M_MainMpsInvalid'};
Mps_TLE5012M_MainMpsInvalid = CreateBus(Mps_TLE5012M_MainMpsInvalid, DeList);
clear DeList;

Mps_TLE5012M_MainMtrArbDriveState = Simulink.Bus;
DeList={'Mps_TLE5012M_MainMtrArbDriveState'};
Mps_TLE5012M_MainMtrArbDriveState = CreateBus(Mps_TLE5012M_MainMtrArbDriveState, DeList);
clear DeList;

MgdM_MainVBatt1Mon = Simulink.Bus;
DeList={'MgdM_MainVBatt1Mon'};
MgdM_MainVBatt1Mon = CreateBus(MgdM_MainVBatt1Mon, DeList);
clear DeList;

MgdM_MainDiagClrSrs = Simulink.Bus;
DeList={'MgdM_MainDiagClrSrs'};
MgdM_MainDiagClrSrs = CreateBus(MgdM_MainDiagClrSrs, DeList);
clear DeList;

MgdM_MainMgdInvalid = Simulink.Bus;
DeList={'MgdM_MainMgdInvalid'};
MgdM_MainMgdInvalid = CreateBus(MgdM_MainMgdInvalid, DeList);
clear DeList;

MgdM_MainMtrArbDriveState = Simulink.Bus;
DeList={'MgdM_MainMtrArbDriveState'};
MgdM_MainMtrArbDriveState = CreateBus(MgdM_MainMtrArbDriveState, DeList);
clear DeList;

MtrM_MainVBatt1Mon = Simulink.Bus;
DeList={'MtrM_MainVBatt1Mon'};
MtrM_MainVBatt1Mon = CreateBus(MtrM_MainVBatt1Mon, DeList);
clear DeList;

MtrM_MainVBatt2Mon = Simulink.Bus;
DeList={'MtrM_MainVBatt2Mon'};
MtrM_MainVBatt2Mon = CreateBus(MtrM_MainVBatt2Mon, DeList);
clear DeList;

MtrM_MainDiagClrSrs = Simulink.Bus;
DeList={'MtrM_MainDiagClrSrs'};
MtrM_MainDiagClrSrs = CreateBus(MtrM_MainDiagClrSrs, DeList);
clear DeList;

MtrM_MainMgdInvalid = Simulink.Bus;
DeList={'MtrM_MainMgdInvalid'};
MtrM_MainMgdInvalid = CreateBus(MtrM_MainMgdInvalid, DeList);
clear DeList;

MtrM_MainMtrArbDriveState = Simulink.Bus;
DeList={'MtrM_MainMtrArbDriveState'};
MtrM_MainMtrArbDriveState = CreateBus(MtrM_MainMtrArbDriveState, DeList);
clear DeList;

Watchdog_MainDiagClrSrs = Simulink.Bus;
DeList={'Watchdog_MainDiagClrSrs'};
Watchdog_MainDiagClrSrs = CreateBus(Watchdog_MainDiagClrSrs, DeList);
clear DeList;

Eem_MainDiagClrSrs = Simulink.Bus;
DeList={'Eem_MainDiagClrSrs'};
Eem_MainDiagClrSrs = CreateBus(Eem_MainDiagClrSrs, DeList);
clear DeList;

Eem_MainDiagSci = Simulink.Bus;
DeList={'Eem_MainDiagSci'};
Eem_MainDiagSci = CreateBus(Eem_MainDiagSci, DeList);
clear DeList;

Eem_MainDiagAhbSci = Simulink.Bus;
DeList={'Eem_MainDiagAhbSci'};
Eem_MainDiagAhbSci = CreateBus(Eem_MainDiagAhbSci, DeList);
clear DeList;

Eem_SuspcDetnDiagClrSrs = Simulink.Bus;
DeList={'Eem_SuspcDetnDiagClrSrs'};
Eem_SuspcDetnDiagClrSrs = CreateBus(Eem_SuspcDetnDiagClrSrs, DeList);
clear DeList;

Arbitrator_VlvAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Arbitrator_VlvAcmAsicInitCompleteFlag'};
Arbitrator_VlvAcmAsicInitCompleteFlag = CreateBus(Arbitrator_VlvAcmAsicInitCompleteFlag, DeList);
clear DeList;

Arbitrator_VlvDiagSci = Simulink.Bus;
DeList={'Arbitrator_VlvDiagSci'};
Arbitrator_VlvDiagSci = CreateBus(Arbitrator_VlvDiagSci, DeList);
clear DeList;

Arbitrator_VlvDiagAhbSci = Simulink.Bus;
DeList={'Arbitrator_VlvDiagAhbSci'};
Arbitrator_VlvDiagAhbSci = CreateBus(Arbitrator_VlvDiagAhbSci, DeList);
clear DeList;

Arbitrator_RlyDiagSci = Simulink.Bus;
DeList={'Arbitrator_RlyDiagSci'};
Arbitrator_RlyDiagSci = CreateBus(Arbitrator_RlyDiagSci, DeList);
clear DeList;

Arbitrator_RlyDiagAhbSci = Simulink.Bus;
DeList={'Arbitrator_RlyDiagAhbSci'};
Arbitrator_RlyDiagAhbSci = CreateBus(Arbitrator_RlyDiagAhbSci, DeList);
clear DeList;

Fsr_ActrAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Fsr_ActrAcmAsicInitCompleteFlag'};
Fsr_ActrAcmAsicInitCompleteFlag = CreateBus(Fsr_ActrAcmAsicInitCompleteFlag, DeList);
clear DeList;

Vlv_ActrSyncAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Vlv_ActrSyncAcmAsicInitCompleteFlag'};
Vlv_ActrSyncAcmAsicInitCompleteFlag = CreateBus(Vlv_ActrSyncAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_OutputCS5msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_OutputCS5msAcmAsicInitCompleteFlag'};
Ioc_OutputCS5msAcmAsicInitCompleteFlag = CreateBus(Ioc_OutputCS5msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Proxy_RxByComRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
Proxy_RxByComRxYawSerialInfo = CreateBus(Proxy_RxByComRxYawSerialInfo, DeList);
clear DeList;

Proxy_RxByComRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
Proxy_RxByComRxYawAccInfo = CreateBus(Proxy_RxByComRxYawAccInfo, DeList);
clear DeList;

Proxy_RxByComRxSasInfo = Simulink.Bus;
DeList={
    'Angle'
    'Speed'
    'Ok'
    'Cal'
    'Trim'
    'CheckSum'
    'MsgCount'
    };
Proxy_RxByComRxSasInfo = CreateBus(Proxy_RxByComRxSasInfo, DeList);
clear DeList;

Proxy_RxByComRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
Proxy_RxByComRxLongAccInfo = CreateBus(Proxy_RxByComRxLongAccInfo, DeList);
clear DeList;

Proxy_RxByComRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
Proxy_RxByComRxMsgOkFlgInfo = CreateBus(Proxy_RxByComRxMsgOkFlgInfo, DeList);
clear DeList;

Proxy_RxCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
Proxy_RxCanRxRegenInfo = CreateBus(Proxy_RxCanRxRegenInfo, DeList);
clear DeList;

Proxy_RxCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Proxy_RxCanRxAccelPedlInfo = CreateBus(Proxy_RxCanRxAccelPedlInfo, DeList);
clear DeList;

Proxy_RxCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
Proxy_RxCanRxIdbInfo = CreateBus(Proxy_RxCanRxIdbInfo, DeList);
clear DeList;

Proxy_RxCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Proxy_RxCanRxEngTempInfo = CreateBus(Proxy_RxCanRxEngTempInfo, DeList);
clear DeList;

Proxy_RxCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
Proxy_RxCanRxEscInfo = CreateBus(Proxy_RxCanRxEscInfo, DeList);
clear DeList;

Proxy_RxCanRxEemInfo = Simulink.Bus;
DeList={
    'YawRateInvld'
    'AyInvld'
    'SteeringAngleRxOk'
    'AxInvldData'
    'Ems1RxErr'
    'Ems2RxErr'
    'Tcu1RxErr'
    'Tcu5RxErr'
    'Hcu1RxErr'
    'Hcu2RxErr'
    'Hcu3RxErr'
    };
Proxy_RxCanRxEemInfo = CreateBus(Proxy_RxCanRxEemInfo, DeList);
clear DeList;

Proxy_RxCanRxInfo = Simulink.Bus;
DeList={
    'MainBusOffFlag'
    'SenBusOffFlag'
    };
Proxy_RxCanRxInfo = CreateBus(Proxy_RxCanRxInfo, DeList);
clear DeList;

Proxy_RxIMUCalcInfo = Simulink.Bus;
DeList={
    'Reverse_Gear_flg'
    'Reverse_Judge_Time'
    };
Proxy_RxIMUCalcInfo = CreateBus(Proxy_RxIMUCalcInfo, DeList);
clear DeList;

Nvm_HndlrLogicEepDataInfo = Simulink.Bus;
DeList={
    'KPdtOffsEolReadVal'
    'KPdfOffsEolReadVal'
    'KPdtOffsDrvgReadVal'
    'KPdfOffsDrvgReadVal'
    'KPedlSimPOffsEolReadVal'
    'KPedlSimPOffsDrvgReadVal'
    'KPistPOffsEolReadVal'
    'KPistPOffsDrvgReadVal'
    'KPrimCircPOffsEolReadVal'
    'KPrimCircPOffsDrvgReadVal'
    'KSecdCircPOffsEolReadVal'
    'KSecdCircPOffsDrvgReadVal'
    'KSteerEepOffs'
    'KYawEepOffs'
    'KLatEepOffs'
    'KFsYawEepOffs'
    'KYawEepMax'
    'KYawEepMin'
    'KSteerEepMax'
    'KSteerEepMin'
    'KLatEepMax'
    'KLatEepMin'
    'KYawStillEepMax'
    'KYawStillEepMin'
    'KYawStandEepOffs'
    'KYawTmpEepMap_array_0'
    'KYawTmpEepMap_array_1'
    'KYawTmpEepMap_array_2'
    'KYawTmpEepMap_array_3'
    'KYawTmpEepMap_array_4'
    'KYawTmpEepMap_array_5'
    'KYawTmpEepMap_array_6'
    'KYawTmpEepMap_array_7'
    'KYawTmpEepMap_array_8'
    'KYawTmpEepMap_array_9'
    'KYawTmpEepMap_array_10'
    'KYawTmpEepMap_array_11'
    'KYawTmpEepMap_array_12'
    'KYawTmpEepMap_array_13'
    'KYawTmpEepMap_array_14'
    'KYawTmpEepMap_array_15'
    'KYawTmpEepMap_array_16'
    'KYawTmpEepMap_array_17'
    'KYawTmpEepMap_array_18'
    'KYawTmpEepMap_array_19'
    'KYawTmpEepMap_array_20'
    'KYawTmpEepMap_array_21'
    'KYawTmpEepMap_array_22'
    'KYawTmpEepMap_array_23'
    'KYawTmpEepMap_array_24'
    'KYawTmpEepMap_array_25'
    'KYawTmpEepMap_array_26'
    'KYawTmpEepMap_array_27'
    'KYawTmpEepMap_array_28'
    'KYawTmpEepMap_array_29'
    'KYawTmpEepMap_array_30'
    'KAdpvVehMdlVchEep'
    'KAdpvVehMdlVcrtRatEep'
    'KFlBtcTmpEep'
    'KFrBtcTmpEep'
    'KRlBtcTmpEep'
    'KRrBtcTmpEep'
    'KEeBtcsDataEep'
    'KLgtSnsrEolEepOffs'
    'KLgtSnsrDrvgEepOffs'
    'ReadInvld'
    'WrReqCmpld'
    };
Nvm_HndlrLogicEepDataInfo = CreateBus(Nvm_HndlrLogicEepDataInfo, DeList);
clear DeList;

Wss_SenWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Wss_SenWhlSpdInfo = CreateBus(Wss_SenWhlSpdInfo, DeList);
clear DeList;

Wss_SenWhlEdgeCntInfo = Simulink.Bus;
DeList={
    'FlWhlEdgeCnt'
    'FrWhlEdgeCnt'
    'RlWhlEdgeCnt'
    'RrWhlEdgeCnt'
    };
Wss_SenWhlEdgeCntInfo = CreateBus(Wss_SenWhlEdgeCntInfo, DeList);
clear DeList;

Wss_SenWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
Wss_SenWssSpeedOut = CreateBus(Wss_SenWssSpeedOut, DeList);
clear DeList;

Wss_SenWssCalcInfo = Simulink.Bus;
DeList={
    'Rough_Sus_Flg'
    };
Wss_SenWssCalcInfo = CreateBus(Wss_SenWssCalcInfo, DeList);
clear DeList;

Pedal_SenSyncPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Pedal_SenSyncPdt5msRawInfo = CreateBus(Pedal_SenSyncPdt5msRawInfo, DeList);
clear DeList;

Pedal_SenSyncPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
Pedal_SenSyncPdf5msRawInfo = CreateBus(Pedal_SenSyncPdf5msRawInfo, DeList);
clear DeList;

Press_SenSyncPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Press_SenSyncPistP5msRawInfo = CreateBus(Press_SenSyncPistP5msRawInfo, DeList);
clear DeList;

Swt_SenEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
Swt_SenEscSwtStInfo = CreateBus(Swt_SenEscSwtStInfo, DeList);
clear DeList;

Spc_5msCtrlIdbSnsrEolOfsCalcInfo = Simulink.Bus;
DeList={
    'PedalEolOfsCalcOk'
    'PedalEolOfsCalcEnd'
    'PressEolOfsCalcOk'
    'PressEolOfsCalcEnd'
    };
Spc_5msCtrlIdbSnsrEolOfsCalcInfo = CreateBus(Spc_5msCtrlIdbSnsrEolOfsCalcInfo, DeList);
clear DeList;

Bbc_CtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'LowSpdCtrlInhibitFlg'
    'StopdVehCtrlModFlg'
    'VehStandStillStFlg'
    };
Bbc_CtrlBaseBrkCtrlModInfo = CreateBus(Bbc_CtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Rbc_CtrlTarRgnBrkTqInfo = Simulink.Bus;
DeList={
    'TarRgnBrkTq'
    'VirtStkDep'
    'VirtStkValid'
    'EstTotBrkForce'
    'EstHydBrkForce'
    'EhbStat'
    };
Rbc_CtrlTarRgnBrkTqInfo = CreateBus(Rbc_CtrlTarRgnBrkTqInfo, DeList);
clear DeList;

Abc_CtrlCanTxInfo = Simulink.Bus;
DeList={
    'TqIntvTCS'
    'TqIntvMsr'
    'TqIntvSlowTCS'
    'MinGear'
    'MaxGear'
    'TcsReq'
    'TcsCtrl'
    'AbsAct'
    'TcsGearShiftChr'
    'EspCtrl'
    'MsrReq'
    'TcsProductInfo'
    };
Abc_CtrlCanTxInfo = CreateBus(Abc_CtrlCanTxInfo, DeList);
clear DeList;

Abc_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
Abc_CtrlAbsCtrlInfo = CreateBus(Abc_CtrlAbsCtrlInfo, DeList);
clear DeList;

Abc_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    'EscCtrlModeFrntLe'
    'EscCtrlModeFrntRi'
    'EscCtrlModeReLe'
    'EscCtrlModeReRi'
    'EscTarPRateFrntLe'
    'EscTarPRateFrntRi'
    'EscTarPRateReLe'
    'EscTarPRateReRi'
    'EscPrioFrntLe'
    'EscPrioFrntRi'
    'EscPrioReLe'
    'EscPrioReRi'
    'EscPreCtrlModeFrntLe'
    'EscPreCtrlModeFrntRi'
    'EscPreCtrlModeReLe'
    'EscPreCtrlModeReRi'
    'EscDelTarPFrntLe'
    'EscDelTarPFrntRi'
    'EscDelTarPReLe'
    'EscDelTarPReRi'
    };
Abc_CtrlEscCtrlInfo = CreateBus(Abc_CtrlEscCtrlInfo, DeList);
clear DeList;

Abc_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    'BrkCtrlFctFrntLeByWspc'
    'BrkCtrlFctFrntRiByWspc'
    'BrkCtrlFctReLeByWspc'
    'BrkCtrlFctReRiByWspc'
    'BrkTqGrdtReqFrntLeByWspc'
    'BrkTqGrdtReqFrntRiByWspc'
    'BrkTqGrdtReqReLeByWspc'
    'BrkTqGrdtReqReRiByWspc'
    'TcsDelTarPFrntLe'
    'TcsDelTarPFrntRi'
    'TcsDelTarPReLe'
    'TcsDelTarPReRi'
    };
Abc_CtrlTcsCtrlInfo = CreateBus(Abc_CtrlTcsCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'RecommendStkRcvrLvl'
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Pct_5msCtrlStkRecvryActnIfInfo = CreateBus(Pct_5msCtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Vat_CtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'VlvFadeoutEndOK'
    'FadeoutSt2EndOk'
    'CutVlvOpenFlg'
    };
Vat_CtrlIdbVlvActInfo = CreateBus(Vat_CtrlIdbVlvActInfo, DeList);
clear DeList;

Eem_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Eem_MainEemFailData = CreateBus(Eem_MainEemFailData, DeList);
clear DeList;

Eem_MainEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Eem_MainEemCtrlInhibitData = CreateBus(Eem_MainEemCtrlInhibitData, DeList);
clear DeList;

Eem_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
Eem_MainEemSuspectData = CreateBus(Eem_MainEemSuspectData, DeList);
clear DeList;

Eem_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
Eem_MainEemEceData = CreateBus(Eem_MainEemEceData, DeList);
clear DeList;

Eem_MainEemLampData = Simulink.Bus;
DeList={
    'Eem_Lamp_EBDLampRequest'
    'Eem_Lamp_ABSLampRequest'
    'Eem_Lamp_TCSLampRequest'
    'Eem_Lamp_TCSOffLampRequest'
    'Eem_Lamp_VDCLampRequest'
    'Eem_Lamp_VDCOffLampRequest'
    'Eem_Lamp_HDCLampRequest'
    'Eem_Lamp_BBSBuzzorRequest'
    'Eem_Lamp_RBCSLampRequest'
    'Eem_Lamp_AHBLampRequest'
    'Eem_Lamp_ServiceLampRequest'
    'Eem_Lamp_TCSFuncLampRequest'
    'Eem_Lamp_VDCFuncLampRequest'
    'Eem_Lamp_AVHLampRequest'
    'Eem_Lamp_AVHILampRequest'
    'Eem_Lamp_ACCEnable'
    'Eem_Lamp_CDMEnable'
    'Eem_Buzzor_On'
    };
Eem_MainEemLampData = CreateBus(Eem_MainEemLampData, DeList);
clear DeList;

Eem_SuspcDetnCanTimeOutStInfo = Simulink.Bus;
DeList={
    'MaiCanSusDet'
    'EmsTiOutSusDet'
    'TcuTiOutSusDet'
    };
Eem_SuspcDetnCanTimeOutStInfo = CreateBus(Eem_SuspcDetnCanTimeOutStInfo, DeList);
clear DeList;

Arbitrator_VlvArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Arbitrator_VlvArbWhlVlvReqInfo = CreateBus(Arbitrator_VlvArbWhlVlvReqInfo, DeList);
clear DeList;

Fsr_ActrFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsOff'
    'FsrAbsDrv'
    };
Fsr_ActrFsrAbsDrvInfo = CreateBus(Fsr_ActrFsrAbsDrvInfo, DeList);
clear DeList;

Fsr_ActrFsrCbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsOff'
    'FsrCbsDrv'
    };
Fsr_ActrFsrCbsDrvInfo = CreateBus(Fsr_ActrFsrCbsDrvInfo, DeList);
clear DeList;

Rly_ActrRlyDrvInfo = Simulink.Bus;
DeList={
    'RlyDbcDrv'
    'RlyEssDrv'
    };
Rly_ActrRlyDrvInfo = CreateBus(Rly_ActrRlyDrvInfo, DeList);
clear DeList;

Vlv_ActrSyncNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    };
Vlv_ActrSyncNormVlvReqInfo = CreateBus(Vlv_ActrSyncNormVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Vlv_ActrSyncWhlVlvReqInfo = CreateBus(Vlv_ActrSyncWhlVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    };
Vlv_ActrSyncBalVlvReqInfo = CreateBus(Vlv_ActrSyncBalVlvReqInfo, DeList);
clear DeList;

Proxy_RxCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'Proxy_RxCanRxGearSelDispErrInfo'};
Proxy_RxCanRxGearSelDispErrInfo = CreateBus(Proxy_RxCanRxGearSelDispErrInfo, DeList);
clear DeList;

Prly_HndlrIgnOnOffSts = Simulink.Bus;
DeList={'Prly_HndlrIgnOnOffSts'};
Prly_HndlrIgnOnOffSts = CreateBus(Prly_HndlrIgnOnOffSts, DeList);
clear DeList;

Prly_HndlrIgnEdgeSts = Simulink.Bus;
DeList={'Prly_HndlrIgnEdgeSts'};
Prly_HndlrIgnEdgeSts = CreateBus(Prly_HndlrIgnEdgeSts, DeList);
clear DeList;

Prly_HndlrPrlyEcuInhibit = Simulink.Bus;
DeList={'Prly_HndlrPrlyEcuInhibit'};
Prly_HndlrPrlyEcuInhibit = CreateBus(Prly_HndlrPrlyEcuInhibit, DeList);
clear DeList;

Mom_HndlrEcuModeSts = Simulink.Bus;
DeList={'Mom_HndlrEcuModeSts'};
Mom_HndlrEcuModeSts = CreateBus(Mom_HndlrEcuModeSts, DeList);
clear DeList;

Det_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Det_5msCtrlVehSpdFild'};
Det_5msCtrlVehSpdFild = CreateBus(Det_5msCtrlVehSpdFild, DeList);
clear DeList;

Abc_CtrlVehSpd = Simulink.Bus;
DeList={'Abc_CtrlVehSpd'};
Abc_CtrlVehSpd = CreateBus(Abc_CtrlVehSpd, DeList);
clear DeList;

Pct_5msCtrlTgtDeltaStkType = Simulink.Bus;
DeList={'Pct_5msCtrlTgtDeltaStkType'};
Pct_5msCtrlTgtDeltaStkType = CreateBus(Pct_5msCtrlTgtDeltaStkType, DeList);
clear DeList;

Pct_5msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlBoostMod'};
Pct_5msCtrlPCtrlBoostMod = CreateBus(Pct_5msCtrlPCtrlBoostMod, DeList);
clear DeList;

Pct_5msCtrlPCtrlFadeoutSt = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlFadeoutSt'};
Pct_5msCtrlPCtrlFadeoutSt = CreateBus(Pct_5msCtrlPCtrlFadeoutSt, DeList);
clear DeList;

Pct_5msCtrlPCtrlSt = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlSt'};
Pct_5msCtrlPCtrlSt = CreateBus(Pct_5msCtrlPCtrlSt, DeList);
clear DeList;

Pct_5msCtrlInVlvAllCloseReq = Simulink.Bus;
DeList={'Pct_5msCtrlInVlvAllCloseReq'};
Pct_5msCtrlInVlvAllCloseReq = CreateBus(Pct_5msCtrlInVlvAllCloseReq, DeList);
clear DeList;

Pct_5msCtrlPChamberVolume = Simulink.Bus;
DeList={'Pct_5msCtrlPChamberVolume'};
Pct_5msCtrlPChamberVolume = CreateBus(Pct_5msCtrlPChamberVolume, DeList);
clear DeList;

Pct_5msCtrlTarDeltaStk = Simulink.Bus;
DeList={'Pct_5msCtrlTarDeltaStk'};
Pct_5msCtrlTarDeltaStk = CreateBus(Pct_5msCtrlTarDeltaStk, DeList);
clear DeList;

Pct_5msCtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Pct_5msCtrlFinalTarPFromPCtrl'};
Pct_5msCtrlFinalTarPFromPCtrl = CreateBus(Pct_5msCtrlFinalTarPFromPCtrl, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvSts'};
Eem_SuspcDetnFuncInhibitVlvSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitIocSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitIocSts'};
Eem_SuspcDetnFuncInhibitIocSts = CreateBus(Eem_SuspcDetnFuncInhibitIocSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitProxySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitProxySts'};
Eem_SuspcDetnFuncInhibitProxySts = CreateBus(Eem_SuspcDetnFuncInhibitProxySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitDiagSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitDiagSts'};
Eem_SuspcDetnFuncInhibitDiagSts = CreateBus(Eem_SuspcDetnFuncInhibitDiagSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAcmioSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAcmioSts'};
Eem_SuspcDetnFuncInhibitAcmioSts = CreateBus(Eem_SuspcDetnFuncInhibitAcmioSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAcmctlSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAcmctlSts'};
Eem_SuspcDetnFuncInhibitAcmctlSts = CreateBus(Eem_SuspcDetnFuncInhibitAcmctlSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMspSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMspSts'};
Eem_SuspcDetnFuncInhibitMspSts = CreateBus(Eem_SuspcDetnFuncInhibitMspSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPedalSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPedalSts'};
Eem_SuspcDetnFuncInhibitPedalSts = CreateBus(Eem_SuspcDetnFuncInhibitPedalSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPressSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPressSts'};
Eem_SuspcDetnFuncInhibitPressSts = CreateBus(Eem_SuspcDetnFuncInhibitPressSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitSesSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitSesSts'};
Eem_SuspcDetnFuncInhibitSesSts = CreateBus(Eem_SuspcDetnFuncInhibitSesSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvdSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvdSts'};
Eem_SuspcDetnFuncInhibitVlvdSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvdSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAdcifSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAdcifSts'};
Eem_SuspcDetnFuncInhibitAdcifSts = CreateBus(Eem_SuspcDetnFuncInhibitAdcifSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitCanTrcvSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitCanTrcvSts'};
Eem_SuspcDetnFuncInhibitCanTrcvSts = CreateBus(Eem_SuspcDetnFuncInhibitCanTrcvSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMccSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMccSts'};
Eem_SuspcDetnFuncInhibitMccSts = CreateBus(Eem_SuspcDetnFuncInhibitMccSts, DeList);
clear DeList;

Arbitrator_VlvArbVlvDriveState = Simulink.Bus;
DeList={'Arbitrator_VlvArbVlvDriveState'};
Arbitrator_VlvArbVlvDriveState = CreateBus(Arbitrator_VlvArbVlvDriveState, DeList);
clear DeList;

Fsr_ActrFsrDcMtrShutDwn = Simulink.Bus;
DeList={'Fsr_ActrFsrDcMtrShutDwn'};
Fsr_ActrFsrDcMtrShutDwn = CreateBus(Fsr_ActrFsrDcMtrShutDwn, DeList);
clear DeList;

Fsr_ActrFsrEnDrDrv = Simulink.Bus;
DeList={'Fsr_ActrFsrEnDrDrv'};
Fsr_ActrFsrEnDrDrv = CreateBus(Fsr_ActrFsrEnDrDrv, DeList);
clear DeList;

Vlv_ActrSyncVlvSync = Simulink.Bus;
DeList={'Vlv_ActrSyncVlvSync'};
Vlv_ActrSyncVlvSync = CreateBus(Vlv_ActrSyncVlvSync, DeList);
clear DeList;




Proxy_RxByComRxBms1Info = Simulink.Bus;
DeList={
    'Bms1SocPc'
    };
Proxy_RxByComRxBms1Info = CreateBus(Proxy_RxByComRxBms1Info, DeList);
clear DeList;

Proxy_RxByComRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
Proxy_RxByComRxYawSerialInfo = CreateBus(Proxy_RxByComRxYawSerialInfo, DeList);
clear DeList;

Proxy_RxByComRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
Proxy_RxByComRxYawAccInfo = CreateBus(Proxy_RxByComRxYawAccInfo, DeList);
clear DeList;

Proxy_RxByComRxTcu6Info = Simulink.Bus;
DeList={
    'ShiftClass_Ccan'
    };
Proxy_RxByComRxTcu6Info = CreateBus(Proxy_RxByComRxTcu6Info, DeList);
clear DeList;

Proxy_RxByComRxTcu5Info = Simulink.Bus;
DeList={
    'Typ'
    'GearTyp'
    };
Proxy_RxByComRxTcu5Info = CreateBus(Proxy_RxByComRxTcu5Info, DeList);
clear DeList;

Proxy_RxByComRxTcu1Info = Simulink.Bus;
DeList={
    'Targe'
    'GarChange'
    'Flt'
    'GarSelDisp'
    'TQRedReq_PC'
    'TQRedReqSlw_PC'
    'TQIncReq_PC'
    };
Proxy_RxByComRxTcu1Info = CreateBus(Proxy_RxByComRxTcu1Info, DeList);
clear DeList;

Proxy_RxByComRxSasInfo = Simulink.Bus;
DeList={
    'Angle'
    'Speed'
    'Ok'
    'Cal'
    'Trim'
    'CheckSum'
    'MsgCount'
    };
Proxy_RxByComRxSasInfo = CreateBus(Proxy_RxByComRxSasInfo, DeList);
clear DeList;

Proxy_RxByComRxMcu2Info = Simulink.Bus;
DeList={
    'Flt'
    };
Proxy_RxByComRxMcu2Info = CreateBus(Proxy_RxByComRxMcu2Info, DeList);
clear DeList;

Proxy_RxByComRxMcu1Info = Simulink.Bus;
DeList={
    'MoTestTQ_PC'
    'MotActRotSpd_RPM'
    };
Proxy_RxByComRxMcu1Info = CreateBus(Proxy_RxByComRxMcu1Info, DeList);
clear DeList;

Proxy_RxByComRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
Proxy_RxByComRxLongAccInfo = CreateBus(Proxy_RxByComRxLongAccInfo, DeList);
clear DeList;

Proxy_RxByComRxHcu5Info = Simulink.Bus;
DeList={
    'HevMod'
    };
Proxy_RxByComRxHcu5Info = CreateBus(Proxy_RxByComRxHcu5Info, DeList);
clear DeList;

Proxy_RxByComRxHcu3Info = Simulink.Bus;
DeList={
    'TmIntQcMDBINV_PC'
    'MotTQCMC_PC'
    'MotTQCMDBINV_PC'
    };
Proxy_RxByComRxHcu3Info = CreateBus(Proxy_RxByComRxHcu3Info, DeList);
clear DeList;

Proxy_RxByComRxHcu2Info = Simulink.Bus;
DeList={
    'ServiceMod'
    'RegenENA'
    'RegenBRKTQ_NM'
    'CrpTQ_NM'
    'WhlDEMTQ_NM'
    };
Proxy_RxByComRxHcu2Info = CreateBus(Proxy_RxByComRxHcu2Info, DeList);
clear DeList;

Proxy_RxByComRxHcu1Info = Simulink.Bus;
DeList={
    'EngCltStat'
    'HEVRDY'
    'EngTQCmdBinV_PC'
    'EngTQCmd_PC'
    };
Proxy_RxByComRxHcu1Info = CreateBus(Proxy_RxByComRxHcu1Info, DeList);
clear DeList;

Proxy_RxByComRxFact1Info = Simulink.Bus;
DeList={
    'OutTemp_SNR_C'
    };
Proxy_RxByComRxFact1Info = CreateBus(Proxy_RxByComRxFact1Info, DeList);
clear DeList;

Proxy_RxByComRxEms3Info = Simulink.Bus;
DeList={
    'EngColTemp_C'
    };
Proxy_RxByComRxEms3Info = CreateBus(Proxy_RxByComRxEms3Info, DeList);
clear DeList;

Proxy_RxByComRxEms2Info = Simulink.Bus;
DeList={
    'EngSpdErr'
    'AccPedDep_PC'
    'Tps_PC'
    };
Proxy_RxByComRxEms2Info = CreateBus(Proxy_RxByComRxEms2Info, DeList);
clear DeList;

Proxy_RxByComRxEms1Info = Simulink.Bus;
DeList={
    'TqStd_NM'
    'ActINDTQ_PC'
    'EngSpd_RPM'
    'IndTQ_PC'
    'FrictTQ_PC'
    };
Proxy_RxByComRxEms1Info = CreateBus(Proxy_RxByComRxEms1Info, DeList);
clear DeList;

Proxy_RxByComRxClu2Info = Simulink.Bus;
DeList={
    'IGN_SW'
    };
Proxy_RxByComRxClu2Info = CreateBus(Proxy_RxByComRxClu2Info, DeList);
clear DeList;

Proxy_RxByComRxClu1Info = Simulink.Bus;
DeList={
    'P_Brake_Act'
    'Cf_Clu_BrakeFluIDSW'
    };
Proxy_RxByComRxClu1Info = CreateBus(Proxy_RxByComRxClu1Info, DeList);
clear DeList;

Proxy_RxByComRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
Proxy_RxByComRxMsgOkFlgInfo = CreateBus(Proxy_RxByComRxMsgOkFlgInfo, DeList);
clear DeList;

Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = Simulink.Bus;
DeList={
    'ST_ECU_ST_REPAT_XTRQ_FTAX_BAX'
    'ALIV_ST_REPAT_XTRQ_FTAX_BAX'
    'CRC_ST_REPAT_XTRQ_FTAX_BAX'
    'QU_SER_REPAT_XTRQ_FTAX_BAX_ACT'
    'AVL_REPAT_XTRQ_FTAX_BAX'
    'QU_AVL_REPAT_XTRQ_FTAX_BAX'
    };
Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = CreateBus(Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo, DeList);
clear DeList;

Proxy_RxByComAVL_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'QU_SER_LTRQD_BAX'
    'CRC_AVL_LTRQD_BAX'
    'QU_AVL_LTRQD_BAX'
    'AVL_LTRQD_BAX'
    'ALIV_AVL_LTRQD_BAX'
    };
Proxy_RxByComAVL_LTRQD_BAXInfo = CreateBus(Proxy_RxByComAVL_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo = Simulink.Bus;
DeList={
    'AVL_TORQ_CRSH_DMEE'
    'AVL_TORQ_CRSH'
    'ST_SAIL_DRV_2'
    'CRC_ANG_ACPD'
    'QU_AVL_RPM_ENG_CRSH'
    'AVL_RPM_ENG_CRSH'
    'ALIV_TORQ_CRSH_1'
    'CRC_TORQ_CRSH_1'
    'AVL_ANG_ACPD'
    'QU_AVL_ANG_ACPD'
    'ALIV_ANG_ACPD'
    'ST_ECU_ANG_ACPD'
    'AVL_ANG_ACPD_VIRT'
    'ECO_ANG_ACPD'
    'GRAD_AVL_ANG_ACPD'
    'ST_INTF_DRASY'
    };
Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo = CreateBus(Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo, DeList);
clear DeList;

Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info = Simulink.Bus;
DeList={
    'AVL_RPM_BAX_RED'
    'ST_PENG_PT'
    'ST_AVAI_INTV_PT_DRS'
    'QU_AVL_RPM_BAX_RED'
    'QU_SER_WMOM_PT_SUM_DRS'
    'QU_SER_WMOM_PT_SUM_STAB'
    'ST_ECU_WMOM_DRV_5'
    'TAR_WMOM_PT_SUM_COOTD'
    'QU_SER_COOR_TORQ_BDRV'
    'ST_ECU_WMOM_DRV_4'
    'ST_DRVDIR_DVCH'
    'CRC_WMOM_DRV_4'
    'ALIV_WMOM_DRV_4'
    'ALIV_WMOM_DRV_5'
    'CRC_WMOM_DRV_5'
    };
Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info = CreateBus(Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info, DeList);
clear DeList;

Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info = Simulink.Bus;
DeList={
    'QU_AVL_WMOM_PT_SUM'
    'AVL_WMOM_PT_SUM_ERR_AMP'
    'REIN_PT'
    'AVL_WMOM_PT_SUM'
    'CRC_WMOM_DRV_1'
    'ALIV_WMOM_DRV_1'
    'ST_ECU_WMOM_DRV_1'
    'AVL_WMOM_PT_SUM_MAX'
    'AVL_WMOM_PT_SUM_FAST_TOP'
    'AVL_WMOM_PT_SUM_FAST_BOT'
    'ST_ECU_WMOM_DRV_2'
    'QU_REIN_PT'
    'CRC_WMOM_DRV_2'
    'ALIV_WMOM_DRV_2'
    };
Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info = CreateBus(Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info, DeList);
clear DeList;

Proxy_RxByComHGLV_VEH_FILTInfo = Simulink.Bus;
DeList={
    'HGLV_VEH_FILT_FRH'
    'HGLV_VEH_FILT_FLH'
    'QU_HGLV_VEH_FILT_FRH'
    'QU_HGLV_VEH_FILT_FLH'
    'QU_HGLV_VEH_FILT_RRH'
    'HGLV_VEH_FILT_RLH'
    'ALIV_HGLV_VEH_FILT'
    'HGLV_VEH_FILT_RRH'
    'QU_HGLV_VEH_FILT_RLH'
    'CRC_HGLV_VEH_FILT'
    };
Proxy_RxByComHGLV_VEH_FILTInfo = CreateBus(Proxy_RxByComHGLV_VEH_FILTInfo, DeList);
clear DeList;

Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo = Simulink.Bus;
DeList={
    'ATTAV_ESTI'
    'ATTA_ESTI_ERR_AMP'
    'VY_ESTI_ERR_AMP'
    'VY_ESTI'
    'ATTAV_ESTI_ERR_AMP'
    'CRC_VEH_DYNMC_DT_ESTI_VRFD'
    'ALIV_VEH_DYNMC_DT_ESTI_VRFD'
    'QU_VEH_DYNMC_DT_ESTI'
    'ATTA_ESTI'
    };
Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo = CreateBus(Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo, DeList);
clear DeList;

Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = Simulink.Bus;
DeList={
    'QU_ACLNY_COG'
    'ACLNY_COG_ERR_AMP'
    'ALIV_ACLNX_COG'
    'ACLNX_COG'
    'ALIV_ACLNY_COG'
    'CRC_ACLNX_COG'
    'CRC_ACLNY_COG'
    'ACLNY_COG'
    'ACLNX_COG_ERR_AMP'
    'QU_ACLNX_COG'
    };
Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = CreateBus(Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo, DeList);
clear DeList;

Proxy_RxByComV_VEH_V_VEH_2Info = Simulink.Bus;
DeList={
    'CRC_V_VEH'
    'ALIV_V_VEH'
    'QU_V_VEH_COG'
    'DVCO_VEH'
    'ST_V_VEH_NSS'
    'V_VEH_COG'
    };
Proxy_RxByComV_VEH_V_VEH_2Info = CreateBus(Proxy_RxByComV_VEH_V_VEH_2Info, DeList);
clear DeList;

Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo = Simulink.Bus;
DeList={
    'ALIV_VYAW_VEH'
    'VYAW_VEH_ERR_AMP'
    'CRC_VYAW_VEH'
    'QU_VYAW_VEH'
    'VYAW_VEH'
    };
Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo = CreateBus(Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo, DeList);
clear DeList;

Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo = Simulink.Bus;
DeList={
    'QU_AVL_TRGR_RW'
    'AVL_TRGR_RW'
    'AVL_LOGR_RW_FAST'
    'ALIV_TLT_RW'
    'CRC_TLT_RW'
    'QU_AVL_LOGR_RW'
    'AVL_LOGR_RW'
    };
Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo = CreateBus(Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo, DeList);
clear DeList;

Proxy_RxByComAVL_STEA_FTAXInfo = Simulink.Bus;
DeList={
    'QU_AVL_STEA_FTAX_PNI'
    'AVL_STEA_FTAX_PNI'
    'CRC_AVL_STEA_FTAX'
    'QU_AVL_STEA_FTAX_WHL'
    'ALIV_AVL_STEA_FTAX'
    'AVL_STEA_FTAX_WHL'
    'AVL_STEA_FTAX_WHL_ERR_AMP'
    };
Proxy_RxByComAVL_STEA_FTAXInfo = CreateBus(Proxy_RxByComAVL_STEA_FTAXInfo, DeList);
clear DeList;

Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = Simulink.Bus;
DeList={
    'TAR_PRMSN_DBC_DCRN_MAX'
    'QU_TAR_PRF_BRK'
    'QU_TAR_V_HDC'
    'TAR_PRMSN_DBC_DCRN_GRAD_MAX'
    'ALIV_SPEC_PRMSN_IBRK_HDC'
    'CRC_SPEC_PRMSN_IBRK_HDC'
    'TAR_PRMSN_DBC_TR_THRV'
    'TAR_BRTORQ_SUM'
    'QU_TAR_BRTORQ_SUM'
    'QU_TAR_PRMSN_DBC'
    'RQ_TAO_SSM'
    'CRC_RQ_BRTORQ_SUM'
    'ALIV_RQ_BRTORQ_SUM'
    };
Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = CreateBus(Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo, DeList);
clear DeList;

Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info = Simulink.Bus;
DeList={
    'AVL_WMOM_PT_SUM_DTORQ_BOT'
    'AVL_WMOM_PT_SUM_RECUP_MAX'
    'AVL_WMOM_PT_SUM_ILS'
    'CRC_WMOM_DRV_3'
    'ALIV_WMOM_DRV_3'
    'AVL_WMOM_PT_SUM_DTORQ_TOP'
    'ST_ECU_WMOM_DRV_6'
    'ALIV_WMOM_DRV_6'
    'CRC_WMOM_DRV_6'
    };
Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info = CreateBus(Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info, DeList);
clear DeList;

Proxy_RxByComWMOM_DRV_7Info = Simulink.Bus;
DeList={
    'ST_EL_DRVG'
    'PRD_AVL_WMOM_PT_SUM_RECUP_MAX'
    'ALIV_WMOM_DRV_7'
    'CRC_WMOM_DRV_7'
    'ST_ECU_WMOM_DRV_7'
    'QU_SER_WMOM_PT_SUM_RECUP'
    };
Proxy_RxByComWMOM_DRV_7Info = CreateBus(Proxy_RxByComWMOM_DRV_7Info, DeList);
clear DeList;

Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = Simulink.Bus;
DeList={
    'TAR_DIFF_BRTORQ_BAX_YMR'
    'FACT_TAR_COMPT_DRV_YMR'
    'QU_TAR_DIFF_BRTORQ_YMR'
    'CRC_RQ_DIFF_BRTORQ_YMR'
    'ALIV_RQ_DIFF_BRTORQ_YMR'
    'TAR_DIFF_BRTORQ_FTAX_YMR'
    };
Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = CreateBus(Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo, DeList);
clear DeList;

Proxy_RxByComDT_BRKSYS_ENGMGInfo = Simulink.Bus;
DeList={
    'QU_AVL_LOWP_BRKFA'
    'AVL_LOWP_BRKFA'
    'CRC_DT_BRKSYS_ENGMG'
    'ALIV_DT_BRKSYS_ENGMG'
    };
Proxy_RxByComDT_BRKSYS_ENGMGInfo = CreateBus(Proxy_RxByComDT_BRKSYS_ENGMGInfo, DeList);
clear DeList;

Proxy_RxByComERRM_BN_UInfo = Simulink.Bus;
DeList={
    'CTR_ERRM_BN_U'
    };
Proxy_RxByComERRM_BN_UInfo = CreateBus(Proxy_RxByComERRM_BN_UInfo, DeList);
clear DeList;

Proxy_RxByComCTR_CRInfo = Simulink.Bus;
DeList={
    'ALIV_CTR_CR'
    'ST_EXCE_ACLN_THRV'
    'CTR_PHTR_CR'
    'CTR_ITLI_CR'
    'CTR_CLSY_CR'
    'CTR_AUTOM_ECAL_CR'
    'CTR_SWO_EKP_CR'
    'CRC_CTR_CR'
    'CTR_PCSH_MST'
    'CTR_HAZW_CR'
    };
Proxy_RxByComCTR_CRInfo = CreateBus(Proxy_RxByComCTR_CRInfo, DeList);
clear DeList;

Proxy_RxByComKLEMMENInfo = Simulink.Bus;
DeList={
    'ST_OP_MSA'
    'RQ_DRVG_RDI'
    'ST_KL_DBG'
    'ST_KL_50_MSA'
    'ST_SSP'
    'RWDT_BLS'
    'ST_STCD_PENG'
    'ST_KL'
    'ST_KL_DIV'
    'ST_VEH_CON'
    'CRC_KL'
    'ALIV_COU_KL'
    'ST_KL_30B'
    'CON_CLT_SW'
    'CTR_ENG_STOP'
    'ST_PLK'
    'ST_KL_15N'
    'ST_KL_KEY_VLD'
    };
Proxy_RxByComKLEMMENInfo = CreateBus(Proxy_RxByComKLEMMENInfo, DeList);
clear DeList;

Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = Simulink.Bus;
DeList={
    'RDC_MES_TSTMP'
    'PCKG_ID'
    'SUPP_ID'
    'RDC_DT_5'
    'RDC_DT_8'
    'RDC_DT_7'
    'RDC_DT_6'
    'ALIV_RDC_DT_PCKG_2'
    'RDC_DT_1'
    'TYR_ID'
    'ALIV_RDC_DT_PCKG_1'
    'RDC_DT_4'
    'RDC_DT_3'
    'RDC_DT_2'
    };
Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = CreateBus(Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info, DeList);
clear DeList;

Proxy_RxByComBEDIENUNG_WISCHERInfo = Simulink.Bus;
DeList={
    'ALIV_WISW'
    'OP_WISW'
    'OP_WIPO'
    };
Proxy_RxByComBEDIENUNG_WISCHERInfo = CreateBus(Proxy_RxByComBEDIENUNG_WISCHERInfo, DeList);
clear DeList;

Proxy_RxByComDT_PT_2Info = Simulink.Bus;
DeList={
    'ST_GRSEL_DRV'
    'TEMP_EOI_DRV'
    'RLS_ENGSTA'
    'RPM_ENG_MAX_ALW'
    'RDUC_DOCTR_RPM_DRV_2'
    'TEMP_ENG_DRV'
    'ST_DRV_VEH'
    'ST_ECU_DT_PT_2'
    'ST_IDLG_ENG_DRV'
    'ST_ILK_STRT_DRV'
    'ST_SW_CLT_DRV'
    'CRC_DT_PT_2'
    'ALIV_DT_PT_2'
    'ST_ENG_RUN_DRV'
    };
Proxy_RxByComDT_PT_2Info = CreateBus(Proxy_RxByComDT_PT_2Info, DeList);
clear DeList;

Proxy_RxByComSTAT_ENG_STA_AUTOInfo = Simulink.Bus;
DeList={
    'RQ_MSA_ENG_STA_1'
    'RQ_MSA_ENG_STA_2'
    'ALIV_ST_ENG_STA_AUTO'
    'PSBTY_MSA_ENG_STOP_STA'
    'CRC_ST_ENG_STA_AUTO'
    'ST_SHFT_MSA_ENGSTP'
    'RQ_SLIP_K0'
    'ST_TAR_PENG_CENG'
    'SPEC_TYP_ENGSTA'
    'ST_RPM_CLCTR_MOT'
    'ST_FN_MSA'
    'ST_AVL_PENG_CENG_ENGMG'
    'DISP_REAS_PREV_SWO_CENG'
    'VARI_TYP_ENGSTA'
    'ST_TAR_CENG'
    'ST_AVAI_SAIL_DME'
    };
Proxy_RxByComSTAT_ENG_STA_AUTOInfo = CreateBus(Proxy_RxByComSTAT_ENG_STA_AUTOInfo, DeList);
clear DeList;

Proxy_RxByComST_ENERG_GENInfo = Simulink.Bus;
DeList={
    'ST_LDST_GEN_DRV'
    'DT_PCU_SCP'
    'ST_GEN_DRV'
    'AVL_I_GEN_DRV'
    'LDST_GEN_DRV'
    'ST_LDREL_GEN'
    'ST_CHG_STOR'
    'TEMP_BT_14V'
    'ST_I_IBS'
    'ST_SEP_STOR'
    'ST_BN2_SCP'
    };
Proxy_RxByComST_ENERG_GENInfo = CreateBus(Proxy_RxByComST_ENERG_GENInfo, DeList);
clear DeList;

Proxy_RxByComDT_PT_1Info = Simulink.Bus;
DeList={
    'RQ_SHPA_GRB_REGE_PAFI'
    'ST_RTIR_DRV'
    'CTR_SLCK_DRV'
    'RQ_STASS_ENGMG'
    'ST_CAT_HT'
    'RQ_SHPA_GRB_CHGBLC'
    'TAR_RPM_IDLG_DRV_EXT'
    'SLCTN_BUS_COMM_ENG_GRB'
    'TAR_RPM_IDLG_DRV'
    'ST_INFS_DRV'
    'ST_SW_WAUP_DRV'
    'AIP_ENG_DRV'
    'RDUC_DOCTR_RPM_DRV'
    };
Proxy_RxByComDT_PT_1Info = CreateBus(Proxy_RxByComDT_PT_1Info, DeList);
clear DeList;

Proxy_RxByComDT_GRDT_DRVInfo = Simulink.Bus;
DeList={
    'ST_OPMO_GRDT_DRV'
    'ST_RSTA_GRDT'
    'CRC_GRDT_DRV'
    'ALIV_GRDT_DRV'
    };
Proxy_RxByComDT_GRDT_DRVInfo = CreateBus(Proxy_RxByComDT_GRDT_DRVInfo, DeList);
clear DeList;

Proxy_RxByComDIAG_OBD_ENGMG_ELInfo = Simulink.Bus;
DeList={
    'RQ_MIL_DIAG_OBD_ENGMG_EL'
    'RQ_RST_OBD_DIAG'
    };
Proxy_RxByComDIAG_OBD_ENGMG_ELInfo = CreateBus(Proxy_RxByComDIAG_OBD_ENGMG_ELInfo, DeList);
clear DeList;

Proxy_RxByComSTAT_CT_HABRInfo = Simulink.Bus;
DeList={
    'ST_CT_HABR'
    };
Proxy_RxByComSTAT_CT_HABRInfo = CreateBus(Proxy_RxByComSTAT_CT_HABRInfo, DeList);
clear DeList;

Proxy_RxByComDT_PT_3Info = Simulink.Bus;
DeList={
    'TRNRAO_BAX'
    'QU_TRNRAO_BAX'
    'ALIV_DT_PT_3'
    };
Proxy_RxByComDT_PT_3Info = CreateBus(Proxy_RxByComDT_PT_3Info, DeList);
clear DeList;

Proxy_RxByComEINHEITEN_BN2020Info = Simulink.Bus;
DeList={
    'UN_TORQ_S_MOD'
    'UN_PWR_S_MOD'
    'UN_DATE_EXT'
    'UN_COSP_EL'
    'UN_TEMP'
    'UN_AIP'
    'LANG'
    'UN_DATE'
    'UN_T'
    'UN_SPDM_DGTL'
    'UN_MILE'
    'UN_FU'
    'UN_COSP'
    };
Proxy_RxByComEINHEITEN_BN2020Info = CreateBus(Proxy_RxByComEINHEITEN_BN2020Info, DeList);
clear DeList;

Proxy_RxByComA_TEMPInfo = Simulink.Bus;
DeList={
    'TEMP_EX_UNFILT'
    'TEMP_EX'
    };
Proxy_RxByComA_TEMPInfo = CreateBus(Proxy_RxByComA_TEMPInfo, DeList);
clear DeList;

Proxy_RxByComWISCHERGESCHWINDIGKEITInfo = Simulink.Bus;
DeList={
    'ST_RNSE'
    'INT_RN'
    'V_WI'
    };
Proxy_RxByComWISCHERGESCHWINDIGKEITInfo = CreateBus(Proxy_RxByComWISCHERGESCHWINDIGKEITInfo, DeList);
clear DeList;

Proxy_RxByComSTAT_DS_VRFDInfo = Simulink.Bus;
DeList={
    'ST_DSW_DRD_SFY_CTRL'
    'ST_DSW_DVDR_VRFD'
    'ST_DSW_DVDR_SFY_CTRL'
    'ST_DSW_PSDR_SFY_CTRL'
    'ST_DSW_PSD_SFY_CTRL'
    'ALIV_ST_DSW_VRFD'
    'CRC_ST_DSW_VRFD'
    'ST_DSW_DRD_VRFD'
    'ST_DSW_PSDR_VRFD'
    'ST_DSW_PSD_VRFD'
    };
Proxy_RxByComSTAT_DS_VRFDInfo = CreateBus(Proxy_RxByComSTAT_DS_VRFDInfo, DeList);
clear DeList;

Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info = Simulink.Bus;
DeList={
    'AVL_MOD_SW_DRDY'
    'RQ_SW_DRDY_MMID'
    'RQ_SU_SW_DRDY'
    'CRC_SU_SW_DRDY'
    'ALIV_SU_SW_DRDY'
    'RQ_SW_DRDY_KDIS'
    'AVL_MOD_SW_DRDY_CHAS'
    'AVL_MOD_SW_DRDY_DRV'
    'AVL_MOD_SW_DRDY_STAB'
    'DISP_ST_DSC'
    };
Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info = CreateBus(Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info, DeList);
clear DeList;

Proxy_RxByComSTAT_ANHAENGERInfo = Simulink.Bus;
DeList={
    'ST_SYNCN_HAZWCL_TRAI'
    'ST_RFLI_DF_TRAI'
    'ST_TRAI'
    'ST_DI_DF_TRAI'
    'ST_PO_AHV'
    };
Proxy_RxByComSTAT_ANHAENGERInfo = CreateBus(Proxy_RxByComSTAT_ANHAENGERInfo, DeList);
clear DeList;

Proxy_RxByComFZZSTDInfo = Simulink.Bus;
DeList={
    'ST_ILK_ERRM_FZM'
    'ST_ENERG_FZM'
    'ST_BT_PROTE_WUP'
    };
Proxy_RxByComFZZSTDInfo = CreateBus(Proxy_RxByComFZZSTDInfo, DeList);
clear DeList;

Proxy_RxByComBEDIENUNG_FAHRWERKInfo = Simulink.Bus;
DeList={
    'OP_MOD_TRCT_DSC'
    'OP_TPCT'
    };
Proxy_RxByComBEDIENUNG_FAHRWERKInfo = CreateBus(Proxy_RxByComBEDIENUNG_FAHRWERKInfo, DeList);
clear DeList;

Proxy_RxByComFAHRZEUGTYPInfo = Simulink.Bus;
DeList={
    'QUAN_CYL'
    'QUAN_GR'
    'TYP_VEH'
    'TYP_BODY'
    'TYP_ENG'
    'CLAS_PWR'
    'TYP_CNT'
    'TYP_STE'
    'TYP_GRB'
    'TYP_CAPA'
    };
Proxy_RxByComFAHRZEUGTYPInfo = CreateBus(Proxy_RxByComFAHRZEUGTYPInfo, DeList);
clear DeList;

Proxy_RxByComST_BLT_CT_SOCCUInfo = Simulink.Bus;
DeList={
    'ST_SEAT_OCCU_RRH'
    'ST_BLTB_SW_RM'
    'ST_BLTB_SW_RRH'
    'ST_ERR_SEAT_MT_DR'
    'ST_BLTB_SW_DR'
    'CRC_ST_BLT_CT_SOCCU'
    'ALIV_COU_ST_BLT_CT_SOCCU'
    'ST_SEAT_OCCU_DR'
    'ST_BLTB_SW_RLH'
    'ST_SEAT_OCCU_RLH'
    'ST_BLTB_SW_PS'
    'ST_SEAT_OCCU_PS'
    };
Proxy_RxByComST_BLT_CT_SOCCUInfo = CreateBus(Proxy_RxByComST_BLT_CT_SOCCUInfo, DeList);
clear DeList;

Proxy_RxByComFAHRGESTELLNUMMERInfo = Simulink.Bus;
DeList={
    'NO_VECH_1'
    'NO_VECH_2'
    'NO_VECH_3'
    'NO_VECH_6'
    'NO_VECH_7'
    'NO_VECH_4'
    'NO_VECH_5'
    };
Proxy_RxByComFAHRGESTELLNUMMERInfo = CreateBus(Proxy_RxByComFAHRGESTELLNUMMERInfo, DeList);
clear DeList;

Proxy_RxByComRELATIVZEITInfo = Simulink.Bus;
DeList={
    'T_SEC_COU_REL'
    'T_DAY_COU_ABSL'
    };
Proxy_RxByComRELATIVZEITInfo = CreateBus(Proxy_RxByComRELATIVZEITInfo, DeList);
clear DeList;

Proxy_RxByComKILOMETERSTANDInfo = Simulink.Bus;
DeList={
    'RNG'
    'ST_FLLV_FUTA_SPAR'
    'FLLV_FUTA_RH'
    'FLLV_FUTA_LH'
    'FLLV_FUTA'
    'MILE_KM'
    };
Proxy_RxByComKILOMETERSTANDInfo = CreateBus(Proxy_RxByComKILOMETERSTANDInfo, DeList);
clear DeList;

Proxy_RxByComUHRZEIT_DATUMInfo = Simulink.Bus;
DeList={
    'DISP_DATE_WDAY'
    'DISP_DATE_DAY'
    'DISP_DATE_MON'
    'ST_DISP_CTI_DATE'
    'DISP_DATE_YR'
    'DISP_HR'
    'DISP_SEC'
    'DISP_MN'
    };
Proxy_RxByComUHRZEIT_DATUMInfo = CreateBus(Proxy_RxByComUHRZEIT_DATUMInfo, DeList);
clear DeList;


Proxy_RxRxBms1Info = Simulink.Bus;
DeList={
    'Bms1SocPc'
    };
Proxy_RxRxBms1Info = CreateBus(Proxy_RxRxBms1Info, DeList);
clear DeList;

Proxy_RxRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
Proxy_RxRxYawSerialInfo = CreateBus(Proxy_RxRxYawSerialInfo, DeList);
clear DeList;

Proxy_RxRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
Proxy_RxRxYawAccInfo = CreateBus(Proxy_RxRxYawAccInfo, DeList);
clear DeList;

Proxy_RxRxTcu6Info = Simulink.Bus;
DeList={
    'ShiftClass_Ccan'
    };
Proxy_RxRxTcu6Info = CreateBus(Proxy_RxRxTcu6Info, DeList);
clear DeList;

Proxy_RxRxTcu5Info = Simulink.Bus;
DeList={
    'Typ'
    'GearTyp'
    };
Proxy_RxRxTcu5Info = CreateBus(Proxy_RxRxTcu5Info, DeList);
clear DeList;

Proxy_RxRxTcu1Info = Simulink.Bus;
DeList={
    'Targe'
    'GarChange'
    'Flt'
    'GarSelDisp'
    'TQRedReq_PC'
    'TQRedReqSlw_PC'
    'TQIncReq_PC'
    };
Proxy_RxRxTcu1Info = CreateBus(Proxy_RxRxTcu1Info, DeList);
clear DeList;

Proxy_RxRxSasInfo = Simulink.Bus;
DeList={
    'Angle'
    'Speed'
    'Ok'
    'Cal'
    'Trim'
    'CheckSum'
    'MsgCount'
    };
Proxy_RxRxSasInfo = CreateBus(Proxy_RxRxSasInfo, DeList);
clear DeList;

Proxy_RxRxMcu2Info = Simulink.Bus;
DeList={
    'Flt'
    };
Proxy_RxRxMcu2Info = CreateBus(Proxy_RxRxMcu2Info, DeList);
clear DeList;

Proxy_RxRxMcu1Info = Simulink.Bus;
DeList={
    'MoTestTQ_PC'
    'MotActRotSpd_RPM'
    };
Proxy_RxRxMcu1Info = CreateBus(Proxy_RxRxMcu1Info, DeList);
clear DeList;

Proxy_RxRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
Proxy_RxRxLongAccInfo = CreateBus(Proxy_RxRxLongAccInfo, DeList);
clear DeList;

Proxy_RxRxHcu5Info = Simulink.Bus;
DeList={
    'HevMod'
    };
Proxy_RxRxHcu5Info = CreateBus(Proxy_RxRxHcu5Info, DeList);
clear DeList;

Proxy_RxRxHcu3Info = Simulink.Bus;
DeList={
    'TmIntQcMDBINV_PC'
    'MotTQCMC_PC'
    'MotTQCMDBINV_PC'
    };
Proxy_RxRxHcu3Info = CreateBus(Proxy_RxRxHcu3Info, DeList);
clear DeList;

Proxy_RxRxHcu2Info = Simulink.Bus;
DeList={
    'ServiceMod'
    'RegenENA'
    'RegenBRKTQ_NM'
    'CrpTQ_NM'
    'WhlDEMTQ_NM'
    };
Proxy_RxRxHcu2Info = CreateBus(Proxy_RxRxHcu2Info, DeList);
clear DeList;

Proxy_RxRxHcu1Info = Simulink.Bus;
DeList={
    'EngCltStat'
    'HEVRDY'
    'EngTQCmdBinV_PC'
    'EngTQCmd_PC'
    };
Proxy_RxRxHcu1Info = CreateBus(Proxy_RxRxHcu1Info, DeList);
clear DeList;

Proxy_RxRxFact1Info = Simulink.Bus;
DeList={
    'OutTemp_SNR_C'
    };
Proxy_RxRxFact1Info = CreateBus(Proxy_RxRxFact1Info, DeList);
clear DeList;

Proxy_RxRxEms3Info = Simulink.Bus;
DeList={
    'EngColTemp_C'
    };
Proxy_RxRxEms3Info = CreateBus(Proxy_RxRxEms3Info, DeList);
clear DeList;

Proxy_RxRxEms2Info = Simulink.Bus;
DeList={
    'EngSpdErr'
    'AccPedDep_PC'
    'Tps_PC'
    };
Proxy_RxRxEms2Info = CreateBus(Proxy_RxRxEms2Info, DeList);
clear DeList;

Proxy_RxRxEms1Info = Simulink.Bus;
DeList={
    'TqStd_NM'
    'ActINDTQ_PC'
    'EngSpd_RPM'
    'IndTQ_PC'
    'FrictTQ_PC'
    };
Proxy_RxRxEms1Info = CreateBus(Proxy_RxRxEms1Info, DeList);
clear DeList;

Proxy_RxRxClu2Info = Simulink.Bus;
DeList={
    'IGN_SW'
    };
Proxy_RxRxClu2Info = CreateBus(Proxy_RxRxClu2Info, DeList);
clear DeList;

Proxy_RxRxClu1Info = Simulink.Bus;
DeList={
    'P_Brake_Act'
    'Cf_Clu_BrakeFluIDSW'
    };
Proxy_RxRxClu1Info = CreateBus(Proxy_RxRxClu1Info, DeList);
clear DeList;

Proxy_RxRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
Proxy_RxRxMsgOkFlgInfo = CreateBus(Proxy_RxRxMsgOkFlgInfo, DeList);
clear DeList;

Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = Simulink.Bus;
DeList={
    'ST_ECU_ST_REPAT_XTRQ_FTAX_BAX'
    'QU_SER_REPAT_XTRQ_FTAX_BAX_ACT'
    'AVL_REPAT_XTRQ_FTAX_BAX'
    'QU_AVL_REPAT_XTRQ_FTAX_BAX'
    };
Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = CreateBus(Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo, DeList);
clear DeList;

Proxy_RxAVL_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'QU_SER_LTRQD_BAX'
    'QU_AVL_LTRQD_BAX'
    'AVL_LTRQD_BAX'
    };
Proxy_RxAVL_LTRQD_BAXInfo = CreateBus(Proxy_RxAVL_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo = Simulink.Bus;
DeList={
    'AVL_TORQ_CRSH_DMEE'
    'AVL_TORQ_CRSH'
    'ST_SAIL_DRV_2'
    'QU_AVL_RPM_ENG_CRSH'
    'AVL_RPM_ENG_CRSH'
    'AVL_ANG_ACPD'
    'QU_AVL_ANG_ACPD'
    'ST_ECU_ANG_ACPD'
    'AVL_ANG_ACPD_VIRT'
    'ECO_ANG_ACPD'
    'GRAD_AVL_ANG_ACPD'
    'ST_INTF_DRASY'
    };
Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo = CreateBus(Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo, DeList);
clear DeList;

Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info = Simulink.Bus;
DeList={
    'AVL_RPM_BAX_RED'
    'ST_PENG_PT'
    'ST_AVAI_INTV_PT_DRS'
    'QU_AVL_RPM_BAX_RED'
    'QU_SER_WMOM_PT_SUM_DRS'
    'QU_SER_WMOM_PT_SUM_STAB'
    'ST_ECU_WMOM_DRV_5'
    'TAR_WMOM_PT_SUM_COOTD'
    'QU_SER_COOR_TORQ_BDRV'
    'ST_ECU_WMOM_DRV_4'
    'ST_DRVDIR_DVCH'
    };
Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info = CreateBus(Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info, DeList);
clear DeList;

Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info = Simulink.Bus;
DeList={
    'QU_AVL_WMOM_PT_SUM'
    'AVL_WMOM_PT_SUM_ERR_AMP'
    'REIN_PT'
    'AVL_WMOM_PT_SUM'
    'ST_ECU_WMOM_DRV_1'
    'AVL_WMOM_PT_SUM_MAX'
    'AVL_WMOM_PT_SUM_FAST_TOP'
    'AVL_WMOM_PT_SUM_FAST_BOT'
    'ST_ECU_WMOM_DRV_2'
    'QU_REIN_PT'
    };
Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info = CreateBus(Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info, DeList);
clear DeList;

Proxy_RxHGLV_VEH_FILTInfo = Simulink.Bus;
DeList={
    'HGLV_VEH_FILT_FRH'
    'HGLV_VEH_FILT_FLH'
    'QU_HGLV_VEH_FILT_FRH'
    'QU_HGLV_VEH_FILT_FLH'
    'QU_HGLV_VEH_FILT_RRH'
    'HGLV_VEH_FILT_RLH'
    'HGLV_VEH_FILT_RRH'
    'QU_HGLV_VEH_FILT_RLH'
    };
Proxy_RxHGLV_VEH_FILTInfo = CreateBus(Proxy_RxHGLV_VEH_FILTInfo, DeList);
clear DeList;

Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo = Simulink.Bus;
DeList={
    'ATTAV_ESTI'
    'ATTA_ESTI_ERR_AMP'
    'VY_ESTI_ERR_AMP'
    'VY_ESTI'
    'ATTAV_ESTI_ERR_AMP'
    'QU_VEH_DYNMC_DT_ESTI'
    'ATTA_ESTI'
    };
Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo = CreateBus(Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo, DeList);
clear DeList;

Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = Simulink.Bus;
DeList={
    'QU_ACLNY_COG'
    'ACLNY_COG_ERR_AMP'
    'ACLNX_COG'
    'ACLNY_COG'
    'ACLNX_COG_ERR_AMP'
    'QU_ACLNX_COG'
    };
Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = CreateBus(Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo, DeList);
clear DeList;

Proxy_RxV_VEH_V_VEH_2Info = Simulink.Bus;
DeList={
    'QU_V_VEH_COG'
    'DVCO_VEH'
    'ST_V_VEH_NSS'
    'V_VEH_COG'
    };
Proxy_RxV_VEH_V_VEH_2Info = CreateBus(Proxy_RxV_VEH_V_VEH_2Info, DeList);
clear DeList;

Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo = Simulink.Bus;
DeList={
    'VYAW_VEH_ERR_AMP'
    'QU_VYAW_VEH'
    'VYAW_VEH'
    };
Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo = CreateBus(Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo, DeList);
clear DeList;

Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo = Simulink.Bus;
DeList={
    'QU_AVL_TRGR_RW'
    'AVL_TRGR_RW'
    'AVL_LOGR_RW_FAST'
    'QU_AVL_LOGR_RW'
    'AVL_LOGR_RW'
    };
Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo = CreateBus(Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo, DeList);
clear DeList;

Proxy_RxAVL_STEA_FTAXInfo = Simulink.Bus;
DeList={
    'QU_AVL_STEA_FTAX_PNI'
    'AVL_STEA_FTAX_PNI'
    'QU_AVL_STEA_FTAX_WHL'
    'AVL_STEA_FTAX_WHL'
    'AVL_STEA_FTAX_WHL_ERR_AMP'
    };
Proxy_RxAVL_STEA_FTAXInfo = CreateBus(Proxy_RxAVL_STEA_FTAXInfo, DeList);
clear DeList;

Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = Simulink.Bus;
DeList={
    'TAR_PRMSN_DBC_DCRN_MAX'
    'QU_TAR_PRF_BRK'
    'QU_TAR_V_HDC'
    'TAR_PRMSN_DBC_DCRN_GRAD_MAX'
    'TAR_PRMSN_DBC_TR_THRV'
    'TAR_BRTORQ_SUM'
    'QU_TAR_BRTORQ_SUM'
    'QU_TAR_PRMSN_DBC'
    'RQ_TAO_SSM'
    };
Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = CreateBus(Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo, DeList);
clear DeList;

Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info = Simulink.Bus;
DeList={
    'AVL_WMOM_PT_SUM_DTORQ_BOT'
    'AVL_WMOM_PT_SUM_RECUP_MAX'
    'AVL_WMOM_PT_SUM_ILS'
    'AVL_WMOM_PT_SUM_DTORQ_TOP'
    'ST_ECU_WMOM_DRV_6'
    };
Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info = CreateBus(Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info, DeList);
clear DeList;

Proxy_RxWMOM_DRV_7Info = Simulink.Bus;
DeList={
    'ST_EL_DRVG'
    'PRD_AVL_WMOM_PT_SUM_RECUP_MAX'
    'ST_ECU_WMOM_DRV_7'
    'QU_SER_WMOM_PT_SUM_RECUP'
    };
Proxy_RxWMOM_DRV_7Info = CreateBus(Proxy_RxWMOM_DRV_7Info, DeList);
clear DeList;

Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = Simulink.Bus;
DeList={
    'TAR_DIFF_BRTORQ_BAX_YMR'
    'FACT_TAR_COMPT_DRV_YMR'
    'QU_TAR_DIFF_BRTORQ_YMR'
    'TAR_DIFF_BRTORQ_FTAX_YMR'
    };
Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = CreateBus(Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo, DeList);
clear DeList;

Proxy_RxDT_BRKSYS_ENGMGInfo = Simulink.Bus;
DeList={
    'QU_AVL_LOWP_BRKFA'
    'AVL_LOWP_BRKFA'
    };
Proxy_RxDT_BRKSYS_ENGMGInfo = CreateBus(Proxy_RxDT_BRKSYS_ENGMGInfo, DeList);
clear DeList;

Proxy_RxERRM_BN_UInfo = Simulink.Bus;
DeList={
    'CTR_ERRM_BN_U'
    };
Proxy_RxERRM_BN_UInfo = CreateBus(Proxy_RxERRM_BN_UInfo, DeList);
clear DeList;

Proxy_RxCTR_CRInfo = Simulink.Bus;
DeList={
    'ST_EXCE_ACLN_THRV'
    'CTR_PHTR_CR'
    'CTR_ITLI_CR'
    'CTR_CLSY_CR'
    'CTR_AUTOM_ECAL_CR'
    'CTR_SWO_EKP_CR'
    'CTR_PCSH_MST'
    'CTR_HAZW_CR'
    };
Proxy_RxCTR_CRInfo = CreateBus(Proxy_RxCTR_CRInfo, DeList);
clear DeList;

Proxy_RxKLEMMENInfo = Simulink.Bus;
DeList={
    'ST_OP_MSA'
    'RQ_DRVG_RDI'
    'ST_KL_DBG'
    'ST_KL_50_MSA'
    'ST_SSP'
    'RWDT_BLS'
    'ST_STCD_PENG'
    'ST_KL'
    'ST_KL_DIV'
    'ST_VEH_CON'
    'ST_KL_30B'
    'CON_CLT_SW'
    'CTR_ENG_STOP'
    'ST_PLK'
    'ST_KL_15N'
    'ST_KL_KEY_VLD'
    };
Proxy_RxKLEMMENInfo = CreateBus(Proxy_RxKLEMMENInfo, DeList);
clear DeList;

Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = Simulink.Bus;
DeList={
    'RDC_MES_TSTMP'
    'PCKG_ID'
    'SUPP_ID'
    'RDC_DT_5'
    'RDC_DT_8'
    'RDC_DT_7'
    'RDC_DT_6'
    'RDC_DT_1'
    'TYR_ID'
    'RDC_DT_4'
    'RDC_DT_3'
    'RDC_DT_2'
    };
Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = CreateBus(Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info, DeList);
clear DeList;

Proxy_RxBEDIENUNG_WISCHERInfo = Simulink.Bus;
DeList={
    'OP_WISW'
    'OP_WIPO'
    };
Proxy_RxBEDIENUNG_WISCHERInfo = CreateBus(Proxy_RxBEDIENUNG_WISCHERInfo, DeList);
clear DeList;

Proxy_RxDT_PT_2Info = Simulink.Bus;
DeList={
    'ST_GRSEL_DRV'
    'TEMP_EOI_DRV'
    'RLS_ENGSTA'
    'RPM_ENG_MAX_ALW'
    'RDUC_DOCTR_RPM_DRV_2'
    'TEMP_ENG_DRV'
    'ST_DRV_VEH'
    'ST_ECU_DT_PT_2'
    'ST_IDLG_ENG_DRV'
    'ST_ILK_STRT_DRV'
    'ST_SW_CLT_DRV'
    'ST_ENG_RUN_DRV'
    };
Proxy_RxDT_PT_2Info = CreateBus(Proxy_RxDT_PT_2Info, DeList);
clear DeList;

Proxy_RxSTAT_ENG_STA_AUTOInfo = Simulink.Bus;
DeList={
    'RQ_MSA_ENG_STA_1'
    'RQ_MSA_ENG_STA_2'
    'PSBTY_MSA_ENG_STOP_STA'
    'ST_SHFT_MSA_ENGSTP'
    'RQ_SLIP_K0'
    'ST_TAR_PENG_CENG'
    'SPEC_TYP_ENGSTA'
    'ST_RPM_CLCTR_MOT'
    'ST_FN_MSA'
    'ST_AVL_PENG_CENG_ENGMG'
    'DISP_REAS_PREV_SWO_CENG'
    'VARI_TYP_ENGSTA'
    'ST_TAR_CENG'
    'ST_AVAI_SAIL_DME'
    };
Proxy_RxSTAT_ENG_STA_AUTOInfo = CreateBus(Proxy_RxSTAT_ENG_STA_AUTOInfo, DeList);
clear DeList;

Proxy_RxST_ENERG_GENInfo = Simulink.Bus;
DeList={
    'ST_LDST_GEN_DRV'
    'DT_PCU_SCP'
    'ST_GEN_DRV'
    'AVL_I_GEN_DRV'
    'LDST_GEN_DRV'
    'ST_LDREL_GEN'
    'ST_CHG_STOR'
    'TEMP_BT_14V'
    'ST_I_IBS'
    'ST_SEP_STOR'
    'ST_BN2_SCP'
    };
Proxy_RxST_ENERG_GENInfo = CreateBus(Proxy_RxST_ENERG_GENInfo, DeList);
clear DeList;

Proxy_RxDT_PT_1Info = Simulink.Bus;
DeList={
    'RQ_SHPA_GRB_REGE_PAFI'
    'ST_RTIR_DRV'
    'CTR_SLCK_DRV'
    'RQ_STASS_ENGMG'
    'ST_CAT_HT'
    'RQ_SHPA_GRB_CHGBLC'
    'TAR_RPM_IDLG_DRV_EXT'
    'SLCTN_BUS_COMM_ENG_GRB'
    'TAR_RPM_IDLG_DRV'
    'ST_INFS_DRV'
    'ST_SW_WAUP_DRV'
    'AIP_ENG_DRV'
    'RDUC_DOCTR_RPM_DRV'
    };
Proxy_RxDT_PT_1Info = CreateBus(Proxy_RxDT_PT_1Info, DeList);
clear DeList;

Proxy_RxDT_GRDT_DRVInfo = Simulink.Bus;
DeList={
    'ST_OPMO_GRDT_DRV'
    'ST_RSTA_GRDT'
    };
Proxy_RxDT_GRDT_DRVInfo = CreateBus(Proxy_RxDT_GRDT_DRVInfo, DeList);
clear DeList;

Proxy_RxDIAG_OBD_ENGMG_ELInfo = Simulink.Bus;
DeList={
    'RQ_MIL_DIAG_OBD_ENGMG_EL'
    'RQ_RST_OBD_DIAG'
    };
Proxy_RxDIAG_OBD_ENGMG_ELInfo = CreateBus(Proxy_RxDIAG_OBD_ENGMG_ELInfo, DeList);
clear DeList;

Proxy_RxSTAT_CT_HABRInfo = Simulink.Bus;
DeList={
    'ST_CT_HABR'
    };
Proxy_RxSTAT_CT_HABRInfo = CreateBus(Proxy_RxSTAT_CT_HABRInfo, DeList);
clear DeList;

Proxy_RxDT_PT_3Info = Simulink.Bus;
DeList={
    'TRNRAO_BAX'
    'QU_TRNRAO_BAX'
    };
Proxy_RxDT_PT_3Info = CreateBus(Proxy_RxDT_PT_3Info, DeList);
clear DeList;

Proxy_RxEINHEITEN_BN2020Info = Simulink.Bus;
DeList={
    'UN_TORQ_S_MOD'
    'UN_PWR_S_MOD'
    'UN_DATE_EXT'
    'UN_COSP_EL'
    'UN_TEMP'
    'UN_AIP'
    'LANG'
    'UN_DATE'
    'UN_T'
    'UN_SPDM_DGTL'
    'UN_MILE'
    'UN_FU'
    'UN_COSP'
    };
Proxy_RxEINHEITEN_BN2020Info = CreateBus(Proxy_RxEINHEITEN_BN2020Info, DeList);
clear DeList;

Proxy_RxA_TEMPInfo = Simulink.Bus;
DeList={
    'TEMP_EX_UNFILT'
    'TEMP_EX'
    };
Proxy_RxA_TEMPInfo = CreateBus(Proxy_RxA_TEMPInfo, DeList);
clear DeList;

Proxy_RxWISCHERGESCHWINDIGKEITInfo = Simulink.Bus;
DeList={
    'ST_RNSE'
    'INT_RN'
    'V_WI'
    };
Proxy_RxWISCHERGESCHWINDIGKEITInfo = CreateBus(Proxy_RxWISCHERGESCHWINDIGKEITInfo, DeList);
clear DeList;

Proxy_RxSTAT_DS_VRFDInfo = Simulink.Bus;
DeList={
    'ST_DSW_DRD_SFY_CTRL'
    'ST_DSW_DVDR_VRFD'
    'ST_DSW_DVDR_SFY_CTRL'
    'ST_DSW_PSDR_SFY_CTRL'
    'ST_DSW_PSD_SFY_CTRL'
    'ST_DSW_DRD_VRFD'
    'ST_DSW_PSDR_VRFD'
    'ST_DSW_PSD_VRFD'
    };
Proxy_RxSTAT_DS_VRFDInfo = CreateBus(Proxy_RxSTAT_DS_VRFDInfo, DeList);
clear DeList;

Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info = Simulink.Bus;
DeList={
    'AVL_MOD_SW_DRDY'
    'RQ_SW_DRDY_MMID'
    'RQ_SU_SW_DRDY'
    'RQ_SW_DRDY_KDIS'
    'AVL_MOD_SW_DRDY_CHAS'
    'AVL_MOD_SW_DRDY_DRV'
    'AVL_MOD_SW_DRDY_STAB'
    'DISP_ST_DSC'
    };
Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info = CreateBus(Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info, DeList);
clear DeList;

Proxy_RxSTAT_ANHAENGERInfo = Simulink.Bus;
DeList={
    'ST_SYNCN_HAZWCL_TRAI'
    'ST_RFLI_DF_TRAI'
    'ST_TRAI'
    'ST_DI_DF_TRAI'
    'ST_PO_AHV'
    };
Proxy_RxSTAT_ANHAENGERInfo = CreateBus(Proxy_RxSTAT_ANHAENGERInfo, DeList);
clear DeList;

Proxy_RxFZZSTDInfo = Simulink.Bus;
DeList={
    'ST_ILK_ERRM_FZM'
    'ST_ENERG_FZM'
    'ST_BT_PROTE_WUP'
    };
Proxy_RxFZZSTDInfo = CreateBus(Proxy_RxFZZSTDInfo, DeList);
clear DeList;

Proxy_RxBEDIENUNG_FAHRWERKInfo = Simulink.Bus;
DeList={
    'OP_MOD_TRCT_DSC'
    'OP_TPCT'
    };
Proxy_RxBEDIENUNG_FAHRWERKInfo = CreateBus(Proxy_RxBEDIENUNG_FAHRWERKInfo, DeList);
clear DeList;

Proxy_RxFAHRZEUGTYPInfo = Simulink.Bus;
DeList={
    'QUAN_CYL'
    'QUAN_GR'
    'TYP_VEH'
    'TYP_BODY'
    'TYP_ENG'
    'CLAS_PWR'
    'TYP_CNT'
    'TYP_STE'
    'TYP_GRB'
    'TYP_CAPA'
    };
Proxy_RxFAHRZEUGTYPInfo = CreateBus(Proxy_RxFAHRZEUGTYPInfo, DeList);
clear DeList;

Proxy_RxST_BLT_CT_SOCCUInfo = Simulink.Bus;
DeList={
    'ST_SEAT_OCCU_RRH'
    'ST_BLTB_SW_RM'
    'ST_BLTB_SW_RRH'
    'ST_ERR_SEAT_MT_DR'
    'ST_BLTB_SW_DR'
    'ST_SEAT_OCCU_DR'
    'ST_BLTB_SW_RLH'
    'ST_SEAT_OCCU_RLH'
    'ST_BLTB_SW_PS'
    'ST_SEAT_OCCU_PS'
    };
Proxy_RxST_BLT_CT_SOCCUInfo = CreateBus(Proxy_RxST_BLT_CT_SOCCUInfo, DeList);
clear DeList;

Proxy_RxFAHRGESTELLNUMMERInfo = Simulink.Bus;
DeList={
    'NO_VECH_1'
    'NO_VECH_2'
    'NO_VECH_3'
    'NO_VECH_6'
    'NO_VECH_7'
    'NO_VECH_4'
    'NO_VECH_5'
    };
Proxy_RxFAHRGESTELLNUMMERInfo = CreateBus(Proxy_RxFAHRGESTELLNUMMERInfo, DeList);
clear DeList;

Proxy_RxRELATIVZEITInfo = Simulink.Bus;
DeList={
    'T_SEC_COU_REL'
    'T_DAY_COU_ABSL'
    };
Proxy_RxRELATIVZEITInfo = CreateBus(Proxy_RxRELATIVZEITInfo, DeList);
clear DeList;

Proxy_RxKILOMETERSTANDInfo = Simulink.Bus;
DeList={
    'RNG'
    'ST_FLLV_FUTA_SPAR'
    'FLLV_FUTA_RH'
    'FLLV_FUTA_LH'
    'FLLV_FUTA'
    'MILE_KM'
    };
Proxy_RxKILOMETERSTANDInfo = CreateBus(Proxy_RxKILOMETERSTANDInfo, DeList);
clear DeList;

Proxy_RxUHRZEIT_DATUMInfo = Simulink.Bus;
DeList={
    'DISP_DATE_WDAY'
    'DISP_DATE_DAY'
    'DISP_DATE_MON'
    'ST_DISP_CTI_DATE'
    'DISP_DATE_YR'
    'DISP_HR'
    'DISP_SEC'
    'DISP_MN'
    };
Proxy_RxUHRZEIT_DATUMInfo = CreateBus(Proxy_RxUHRZEIT_DATUMInfo, DeList);
clear DeList;

Proxy_RxEcuModeSts = Simulink.Bus;
DeList={'Proxy_RxEcuModeSts'};
Proxy_RxEcuModeSts = CreateBus(Proxy_RxEcuModeSts, DeList);
clear DeList;

Proxy_RxPCtrlAct = Simulink.Bus;
DeList={'Proxy_RxPCtrlAct'};
Proxy_RxPCtrlAct = CreateBus(Proxy_RxPCtrlAct, DeList);
clear DeList;

Proxy_RxFuncInhibitProxySts = Simulink.Bus;
DeList={'Proxy_RxFuncInhibitProxySts'};
Proxy_RxFuncInhibitProxySts = CreateBus(Proxy_RxFuncInhibitProxySts, DeList);
clear DeList;

Proxy_RxCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
Proxy_RxCanRxRegenInfo = CreateBus(Proxy_RxCanRxRegenInfo, DeList);
clear DeList;

Proxy_RxCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Proxy_RxCanRxAccelPedlInfo = CreateBus(Proxy_RxCanRxAccelPedlInfo, DeList);
clear DeList;

Proxy_RxCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
Proxy_RxCanRxIdbInfo = CreateBus(Proxy_RxCanRxIdbInfo, DeList);
clear DeList;

Proxy_RxCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Proxy_RxCanRxEngTempInfo = CreateBus(Proxy_RxCanRxEngTempInfo, DeList);
clear DeList;

Proxy_RxCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
Proxy_RxCanRxEscInfo = CreateBus(Proxy_RxCanRxEscInfo, DeList);
clear DeList;

Proxy_RxCanRxClu2Info = Simulink.Bus;
DeList={
    'IgnRun'
    };
Proxy_RxCanRxClu2Info = CreateBus(Proxy_RxCanRxClu2Info, DeList);
clear DeList;

Proxy_RxCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'Proxy_RxCanRxGearSelDispErrInfo'};
Proxy_RxCanRxGearSelDispErrInfo = CreateBus(Proxy_RxCanRxGearSelDispErrInfo, DeList);
clear DeList;


Icu_InputCaptureEcuModeSts = Simulink.Bus;
DeList={'Icu_InputCaptureEcuModeSts'};
Icu_InputCaptureEcuModeSts = CreateBus(Icu_InputCaptureEcuModeSts, DeList);
clear DeList;

Icu_InputCaptureFuncInhibitIcuSts = Simulink.Bus;
DeList={'Icu_InputCaptureFuncInhibitIcuSts'};
Icu_InputCaptureFuncInhibitIcuSts = CreateBus(Icu_InputCaptureFuncInhibitIcuSts, DeList);
clear DeList;

Icu_InputCaptureRisngIdxInfo = Simulink.Bus;
DeList={
    'FlRisngIdx'
    'FrRisngIdx'
    'RlRisngIdx'
    'RrRisngIdx'
    };
Icu_InputCaptureRisngIdxInfo = CreateBus(Icu_InputCaptureRisngIdxInfo, DeList);
clear DeList;

Icu_InputCaptureFallIdxInfo = Simulink.Bus;
DeList={
    'FlFallIdx'
    'FrFallIdx'
    'RlFallIdx'
    'RrFallIdx'
    };
Icu_InputCaptureFallIdxInfo = CreateBus(Icu_InputCaptureFallIdxInfo, DeList);
clear DeList;

Icu_InputCaptureRisngTiStampInfo = Simulink.Bus;
DeList={
    'FlRisngTiStamp_array_0'
    'FlRisngTiStamp_array_1'
    'FlRisngTiStamp_array_2'
    'FlRisngTiStamp_array_3'
    'FlRisngTiStamp_array_4'
    'FlRisngTiStamp_array_5'
    'FlRisngTiStamp_array_6'
    'FlRisngTiStamp_array_7'
    'FlRisngTiStamp_array_8'
    'FlRisngTiStamp_array_9'
    'FlRisngTiStamp_array_10'
    'FlRisngTiStamp_array_11'
    'FlRisngTiStamp_array_12'
    'FlRisngTiStamp_array_13'
    'FlRisngTiStamp_array_14'
    'FlRisngTiStamp_array_15'
    'FlRisngTiStamp_array_16'
    'FlRisngTiStamp_array_17'
    'FlRisngTiStamp_array_18'
    'FlRisngTiStamp_array_19'
    'FlRisngTiStamp_array_20'
    'FlRisngTiStamp_array_21'
    'FlRisngTiStamp_array_22'
    'FlRisngTiStamp_array_23'
    'FlRisngTiStamp_array_24'
    'FlRisngTiStamp_array_25'
    'FlRisngTiStamp_array_26'
    'FlRisngTiStamp_array_27'
    'FlRisngTiStamp_array_28'
    'FlRisngTiStamp_array_29'
    'FlRisngTiStamp_array_30'
    'FlRisngTiStamp_array_31'
    'FrRisngTiStamp_array_0'
    'FrRisngTiStamp_array_1'
    'FrRisngTiStamp_array_2'
    'FrRisngTiStamp_array_3'
    'FrRisngTiStamp_array_4'
    'FrRisngTiStamp_array_5'
    'FrRisngTiStamp_array_6'
    'FrRisngTiStamp_array_7'
    'FrRisngTiStamp_array_8'
    'FrRisngTiStamp_array_9'
    'FrRisngTiStamp_array_10'
    'FrRisngTiStamp_array_11'
    'FrRisngTiStamp_array_12'
    'FrRisngTiStamp_array_13'
    'FrRisngTiStamp_array_14'
    'FrRisngTiStamp_array_15'
    'FrRisngTiStamp_array_16'
    'FrRisngTiStamp_array_17'
    'FrRisngTiStamp_array_18'
    'FrRisngTiStamp_array_19'
    'FrRisngTiStamp_array_20'
    'FrRisngTiStamp_array_21'
    'FrRisngTiStamp_array_22'
    'FrRisngTiStamp_array_23'
    'FrRisngTiStamp_array_24'
    'FrRisngTiStamp_array_25'
    'FrRisngTiStamp_array_26'
    'FrRisngTiStamp_array_27'
    'FrRisngTiStamp_array_28'
    'FrRisngTiStamp_array_29'
    'FrRisngTiStamp_array_30'
    'FrRisngTiStamp_array_31'
    'RlRisngTiStamp_array_0'
    'RlRisngTiStamp_array_1'
    'RlRisngTiStamp_array_2'
    'RlRisngTiStamp_array_3'
    'RlRisngTiStamp_array_4'
    'RlRisngTiStamp_array_5'
    'RlRisngTiStamp_array_6'
    'RlRisngTiStamp_array_7'
    'RlRisngTiStamp_array_8'
    'RlRisngTiStamp_array_9'
    'RlRisngTiStamp_array_10'
    'RlRisngTiStamp_array_11'
    'RlRisngTiStamp_array_12'
    'RlRisngTiStamp_array_13'
    'RlRisngTiStamp_array_14'
    'RlRisngTiStamp_array_15'
    'RlRisngTiStamp_array_16'
    'RlRisngTiStamp_array_17'
    'RlRisngTiStamp_array_18'
    'RlRisngTiStamp_array_19'
    'RlRisngTiStamp_array_20'
    'RlRisngTiStamp_array_21'
    'RlRisngTiStamp_array_22'
    'RlRisngTiStamp_array_23'
    'RlRisngTiStamp_array_24'
    'RlRisngTiStamp_array_25'
    'RlRisngTiStamp_array_26'
    'RlRisngTiStamp_array_27'
    'RlRisngTiStamp_array_28'
    'RlRisngTiStamp_array_29'
    'RlRisngTiStamp_array_30'
    'RlRisngTiStamp_array_31'
    'RrRisngTiStamp_array_0'
    'RrRisngTiStamp_array_1'
    'RrRisngTiStamp_array_2'
    'RrRisngTiStamp_array_3'
    'RrRisngTiStamp_array_4'
    'RrRisngTiStamp_array_5'
    'RrRisngTiStamp_array_6'
    'RrRisngTiStamp_array_7'
    'RrRisngTiStamp_array_8'
    'RrRisngTiStamp_array_9'
    'RrRisngTiStamp_array_10'
    'RrRisngTiStamp_array_11'
    'RrRisngTiStamp_array_12'
    'RrRisngTiStamp_array_13'
    'RrRisngTiStamp_array_14'
    'RrRisngTiStamp_array_15'
    'RrRisngTiStamp_array_16'
    'RrRisngTiStamp_array_17'
    'RrRisngTiStamp_array_18'
    'RrRisngTiStamp_array_19'
    'RrRisngTiStamp_array_20'
    'RrRisngTiStamp_array_21'
    'RrRisngTiStamp_array_22'
    'RrRisngTiStamp_array_23'
    'RrRisngTiStamp_array_24'
    'RrRisngTiStamp_array_25'
    'RrRisngTiStamp_array_26'
    'RrRisngTiStamp_array_27'
    'RrRisngTiStamp_array_28'
    'RrRisngTiStamp_array_29'
    'RrRisngTiStamp_array_30'
    'RrRisngTiStamp_array_31'
    };
Icu_InputCaptureRisngTiStampInfo = CreateBus(Icu_InputCaptureRisngTiStampInfo, DeList);
clear DeList;

Icu_InputCaptureFallTiStampInfo = Simulink.Bus;
DeList={
    'FlFallTiStamp_array_0'
    'FlFallTiStamp_array_1'
    'FlFallTiStamp_array_2'
    'FlFallTiStamp_array_3'
    'FlFallTiStamp_array_4'
    'FlFallTiStamp_array_5'
    'FlFallTiStamp_array_6'
    'FlFallTiStamp_array_7'
    'FlFallTiStamp_array_8'
    'FlFallTiStamp_array_9'
    'FlFallTiStamp_array_10'
    'FlFallTiStamp_array_11'
    'FlFallTiStamp_array_12'
    'FlFallTiStamp_array_13'
    'FlFallTiStamp_array_14'
    'FlFallTiStamp_array_15'
    'FlFallTiStamp_array_16'
    'FlFallTiStamp_array_17'
    'FlFallTiStamp_array_18'
    'FlFallTiStamp_array_19'
    'FlFallTiStamp_array_20'
    'FlFallTiStamp_array_21'
    'FlFallTiStamp_array_22'
    'FlFallTiStamp_array_23'
    'FlFallTiStamp_array_24'
    'FlFallTiStamp_array_25'
    'FlFallTiStamp_array_26'
    'FlFallTiStamp_array_27'
    'FlFallTiStamp_array_28'
    'FlFallTiStamp_array_29'
    'FlFallTiStamp_array_30'
    'FlFallTiStamp_array_31'
    'FrFallTiStamp_array_0'
    'FrFallTiStamp_array_1'
    'FrFallTiStamp_array_2'
    'FrFallTiStamp_array_3'
    'FrFallTiStamp_array_4'
    'FrFallTiStamp_array_5'
    'FrFallTiStamp_array_6'
    'FrFallTiStamp_array_7'
    'FrFallTiStamp_array_8'
    'FrFallTiStamp_array_9'
    'FrFallTiStamp_array_10'
    'FrFallTiStamp_array_11'
    'FrFallTiStamp_array_12'
    'FrFallTiStamp_array_13'
    'FrFallTiStamp_array_14'
    'FrFallTiStamp_array_15'
    'FrFallTiStamp_array_16'
    'FrFallTiStamp_array_17'
    'FrFallTiStamp_array_18'
    'FrFallTiStamp_array_19'
    'FrFallTiStamp_array_20'
    'FrFallTiStamp_array_21'
    'FrFallTiStamp_array_22'
    'FrFallTiStamp_array_23'
    'FrFallTiStamp_array_24'
    'FrFallTiStamp_array_25'
    'FrFallTiStamp_array_26'
    'FrFallTiStamp_array_27'
    'FrFallTiStamp_array_28'
    'FrFallTiStamp_array_29'
    'FrFallTiStamp_array_30'
    'FrFallTiStamp_array_31'
    'RlFallTiStamp_array_0'
    'RlFallTiStamp_array_1'
    'RlFallTiStamp_array_2'
    'RlFallTiStamp_array_3'
    'RlFallTiStamp_array_4'
    'RlFallTiStamp_array_5'
    'RlFallTiStamp_array_6'
    'RlFallTiStamp_array_7'
    'RlFallTiStamp_array_8'
    'RlFallTiStamp_array_9'
    'RlFallTiStamp_array_10'
    'RlFallTiStamp_array_11'
    'RlFallTiStamp_array_12'
    'RlFallTiStamp_array_13'
    'RlFallTiStamp_array_14'
    'RlFallTiStamp_array_15'
    'RlFallTiStamp_array_16'
    'RlFallTiStamp_array_17'
    'RlFallTiStamp_array_18'
    'RlFallTiStamp_array_19'
    'RlFallTiStamp_array_20'
    'RlFallTiStamp_array_21'
    'RlFallTiStamp_array_22'
    'RlFallTiStamp_array_23'
    'RlFallTiStamp_array_24'
    'RlFallTiStamp_array_25'
    'RlFallTiStamp_array_26'
    'RlFallTiStamp_array_27'
    'RlFallTiStamp_array_28'
    'RlFallTiStamp_array_29'
    'RlFallTiStamp_array_30'
    'RlFallTiStamp_array_31'
    'RrFallTiStamp_array_0'
    'RrFallTiStamp_array_1'
    'RrFallTiStamp_array_2'
    'RrFallTiStamp_array_3'
    'RrFallTiStamp_array_4'
    'RrFallTiStamp_array_5'
    'RrFallTiStamp_array_6'
    'RrFallTiStamp_array_7'
    'RrFallTiStamp_array_8'
    'RrFallTiStamp_array_9'
    'RrFallTiStamp_array_10'
    'RrFallTiStamp_array_11'
    'RrFallTiStamp_array_12'
    'RrFallTiStamp_array_13'
    'RrFallTiStamp_array_14'
    'RrFallTiStamp_array_15'
    'RrFallTiStamp_array_16'
    'RrFallTiStamp_array_17'
    'RrFallTiStamp_array_18'
    'RrFallTiStamp_array_19'
    'RrFallTiStamp_array_20'
    'RrFallTiStamp_array_21'
    'RrFallTiStamp_array_22'
    'RrFallTiStamp_array_23'
    'RrFallTiStamp_array_24'
    'RrFallTiStamp_array_25'
    'RrFallTiStamp_array_26'
    'RrFallTiStamp_array_27'
    'RrFallTiStamp_array_28'
    'RrFallTiStamp_array_29'
    'RrFallTiStamp_array_30'
    'RrFallTiStamp_array_31'
    };
Icu_InputCaptureFallTiStampInfo = CreateBus(Icu_InputCaptureFallTiStampInfo, DeList);
clear DeList;


Ioc_InputSR5msRisngIdxInfo = Simulink.Bus;
DeList={
    'FlRisngIdx'
    'FrRisngIdx'
    'RlRisngIdx'
    'RrRisngIdx'
    };
Ioc_InputSR5msRisngIdxInfo = CreateBus(Ioc_InputSR5msRisngIdxInfo, DeList);
clear DeList;

Ioc_InputSR5msFallIdxInfo = Simulink.Bus;
DeList={
    'FlFallIdx'
    'FrFallIdx'
    'RlFallIdx'
    'RrFallIdx'
    };
Ioc_InputSR5msFallIdxInfo = CreateBus(Ioc_InputSR5msFallIdxInfo, DeList);
clear DeList;

Ioc_InputSR5msRisngTiStampInfo = Simulink.Bus;
DeList={
    'FlRisngTiStamp_array_0'
    'FlRisngTiStamp_array_1'
    'FlRisngTiStamp_array_2'
    'FlRisngTiStamp_array_3'
    'FlRisngTiStamp_array_4'
    'FlRisngTiStamp_array_5'
    'FlRisngTiStamp_array_6'
    'FlRisngTiStamp_array_7'
    'FlRisngTiStamp_array_8'
    'FlRisngTiStamp_array_9'
    'FlRisngTiStamp_array_10'
    'FlRisngTiStamp_array_11'
    'FlRisngTiStamp_array_12'
    'FlRisngTiStamp_array_13'
    'FlRisngTiStamp_array_14'
    'FlRisngTiStamp_array_15'
    'FlRisngTiStamp_array_16'
    'FlRisngTiStamp_array_17'
    'FlRisngTiStamp_array_18'
    'FlRisngTiStamp_array_19'
    'FlRisngTiStamp_array_20'
    'FlRisngTiStamp_array_21'
    'FlRisngTiStamp_array_22'
    'FlRisngTiStamp_array_23'
    'FlRisngTiStamp_array_24'
    'FlRisngTiStamp_array_25'
    'FlRisngTiStamp_array_26'
    'FlRisngTiStamp_array_27'
    'FlRisngTiStamp_array_28'
    'FlRisngTiStamp_array_29'
    'FlRisngTiStamp_array_30'
    'FlRisngTiStamp_array_31'
    'FrRisngTiStamp_array_0'
    'FrRisngTiStamp_array_1'
    'FrRisngTiStamp_array_2'
    'FrRisngTiStamp_array_3'
    'FrRisngTiStamp_array_4'
    'FrRisngTiStamp_array_5'
    'FrRisngTiStamp_array_6'
    'FrRisngTiStamp_array_7'
    'FrRisngTiStamp_array_8'
    'FrRisngTiStamp_array_9'
    'FrRisngTiStamp_array_10'
    'FrRisngTiStamp_array_11'
    'FrRisngTiStamp_array_12'
    'FrRisngTiStamp_array_13'
    'FrRisngTiStamp_array_14'
    'FrRisngTiStamp_array_15'
    'FrRisngTiStamp_array_16'
    'FrRisngTiStamp_array_17'
    'FrRisngTiStamp_array_18'
    'FrRisngTiStamp_array_19'
    'FrRisngTiStamp_array_20'
    'FrRisngTiStamp_array_21'
    'FrRisngTiStamp_array_22'
    'FrRisngTiStamp_array_23'
    'FrRisngTiStamp_array_24'
    'FrRisngTiStamp_array_25'
    'FrRisngTiStamp_array_26'
    'FrRisngTiStamp_array_27'
    'FrRisngTiStamp_array_28'
    'FrRisngTiStamp_array_29'
    'FrRisngTiStamp_array_30'
    'FrRisngTiStamp_array_31'
    'RlRisngTiStamp_array_0'
    'RlRisngTiStamp_array_1'
    'RlRisngTiStamp_array_2'
    'RlRisngTiStamp_array_3'
    'RlRisngTiStamp_array_4'
    'RlRisngTiStamp_array_5'
    'RlRisngTiStamp_array_6'
    'RlRisngTiStamp_array_7'
    'RlRisngTiStamp_array_8'
    'RlRisngTiStamp_array_9'
    'RlRisngTiStamp_array_10'
    'RlRisngTiStamp_array_11'
    'RlRisngTiStamp_array_12'
    'RlRisngTiStamp_array_13'
    'RlRisngTiStamp_array_14'
    'RlRisngTiStamp_array_15'
    'RlRisngTiStamp_array_16'
    'RlRisngTiStamp_array_17'
    'RlRisngTiStamp_array_18'
    'RlRisngTiStamp_array_19'
    'RlRisngTiStamp_array_20'
    'RlRisngTiStamp_array_21'
    'RlRisngTiStamp_array_22'
    'RlRisngTiStamp_array_23'
    'RlRisngTiStamp_array_24'
    'RlRisngTiStamp_array_25'
    'RlRisngTiStamp_array_26'
    'RlRisngTiStamp_array_27'
    'RlRisngTiStamp_array_28'
    'RlRisngTiStamp_array_29'
    'RlRisngTiStamp_array_30'
    'RlRisngTiStamp_array_31'
    'RrRisngTiStamp_array_0'
    'RrRisngTiStamp_array_1'
    'RrRisngTiStamp_array_2'
    'RrRisngTiStamp_array_3'
    'RrRisngTiStamp_array_4'
    'RrRisngTiStamp_array_5'
    'RrRisngTiStamp_array_6'
    'RrRisngTiStamp_array_7'
    'RrRisngTiStamp_array_8'
    'RrRisngTiStamp_array_9'
    'RrRisngTiStamp_array_10'
    'RrRisngTiStamp_array_11'
    'RrRisngTiStamp_array_12'
    'RrRisngTiStamp_array_13'
    'RrRisngTiStamp_array_14'
    'RrRisngTiStamp_array_15'
    'RrRisngTiStamp_array_16'
    'RrRisngTiStamp_array_17'
    'RrRisngTiStamp_array_18'
    'RrRisngTiStamp_array_19'
    'RrRisngTiStamp_array_20'
    'RrRisngTiStamp_array_21'
    'RrRisngTiStamp_array_22'
    'RrRisngTiStamp_array_23'
    'RrRisngTiStamp_array_24'
    'RrRisngTiStamp_array_25'
    'RrRisngTiStamp_array_26'
    'RrRisngTiStamp_array_27'
    'RrRisngTiStamp_array_28'
    'RrRisngTiStamp_array_29'
    'RrRisngTiStamp_array_30'
    'RrRisngTiStamp_array_31'
    };
Ioc_InputSR5msRisngTiStampInfo = CreateBus(Ioc_InputSR5msRisngTiStampInfo, DeList);
clear DeList;

Ioc_InputSR5msFallTiStampInfo = Simulink.Bus;
DeList={
    'FlFallTiStamp_array_0'
    'FlFallTiStamp_array_1'
    'FlFallTiStamp_array_2'
    'FlFallTiStamp_array_3'
    'FlFallTiStamp_array_4'
    'FlFallTiStamp_array_5'
    'FlFallTiStamp_array_6'
    'FlFallTiStamp_array_7'
    'FlFallTiStamp_array_8'
    'FlFallTiStamp_array_9'
    'FlFallTiStamp_array_10'
    'FlFallTiStamp_array_11'
    'FlFallTiStamp_array_12'
    'FlFallTiStamp_array_13'
    'FlFallTiStamp_array_14'
    'FlFallTiStamp_array_15'
    'FlFallTiStamp_array_16'
    'FlFallTiStamp_array_17'
    'FlFallTiStamp_array_18'
    'FlFallTiStamp_array_19'
    'FlFallTiStamp_array_20'
    'FlFallTiStamp_array_21'
    'FlFallTiStamp_array_22'
    'FlFallTiStamp_array_23'
    'FlFallTiStamp_array_24'
    'FlFallTiStamp_array_25'
    'FlFallTiStamp_array_26'
    'FlFallTiStamp_array_27'
    'FlFallTiStamp_array_28'
    'FlFallTiStamp_array_29'
    'FlFallTiStamp_array_30'
    'FlFallTiStamp_array_31'
    'FrFallTiStamp_array_0'
    'FrFallTiStamp_array_1'
    'FrFallTiStamp_array_2'
    'FrFallTiStamp_array_3'
    'FrFallTiStamp_array_4'
    'FrFallTiStamp_array_5'
    'FrFallTiStamp_array_6'
    'FrFallTiStamp_array_7'
    'FrFallTiStamp_array_8'
    'FrFallTiStamp_array_9'
    'FrFallTiStamp_array_10'
    'FrFallTiStamp_array_11'
    'FrFallTiStamp_array_12'
    'FrFallTiStamp_array_13'
    'FrFallTiStamp_array_14'
    'FrFallTiStamp_array_15'
    'FrFallTiStamp_array_16'
    'FrFallTiStamp_array_17'
    'FrFallTiStamp_array_18'
    'FrFallTiStamp_array_19'
    'FrFallTiStamp_array_20'
    'FrFallTiStamp_array_21'
    'FrFallTiStamp_array_22'
    'FrFallTiStamp_array_23'
    'FrFallTiStamp_array_24'
    'FrFallTiStamp_array_25'
    'FrFallTiStamp_array_26'
    'FrFallTiStamp_array_27'
    'FrFallTiStamp_array_28'
    'FrFallTiStamp_array_29'
    'FrFallTiStamp_array_30'
    'FrFallTiStamp_array_31'
    'RlFallTiStamp_array_0'
    'RlFallTiStamp_array_1'
    'RlFallTiStamp_array_2'
    'RlFallTiStamp_array_3'
    'RlFallTiStamp_array_4'
    'RlFallTiStamp_array_5'
    'RlFallTiStamp_array_6'
    'RlFallTiStamp_array_7'
    'RlFallTiStamp_array_8'
    'RlFallTiStamp_array_9'
    'RlFallTiStamp_array_10'
    'RlFallTiStamp_array_11'
    'RlFallTiStamp_array_12'
    'RlFallTiStamp_array_13'
    'RlFallTiStamp_array_14'
    'RlFallTiStamp_array_15'
    'RlFallTiStamp_array_16'
    'RlFallTiStamp_array_17'
    'RlFallTiStamp_array_18'
    'RlFallTiStamp_array_19'
    'RlFallTiStamp_array_20'
    'RlFallTiStamp_array_21'
    'RlFallTiStamp_array_22'
    'RlFallTiStamp_array_23'
    'RlFallTiStamp_array_24'
    'RlFallTiStamp_array_25'
    'RlFallTiStamp_array_26'
    'RlFallTiStamp_array_27'
    'RlFallTiStamp_array_28'
    'RlFallTiStamp_array_29'
    'RlFallTiStamp_array_30'
    'RlFallTiStamp_array_31'
    'RrFallTiStamp_array_0'
    'RrFallTiStamp_array_1'
    'RrFallTiStamp_array_2'
    'RrFallTiStamp_array_3'
    'RrFallTiStamp_array_4'
    'RrFallTiStamp_array_5'
    'RrFallTiStamp_array_6'
    'RrFallTiStamp_array_7'
    'RrFallTiStamp_array_8'
    'RrFallTiStamp_array_9'
    'RrFallTiStamp_array_10'
    'RrFallTiStamp_array_11'
    'RrFallTiStamp_array_12'
    'RrFallTiStamp_array_13'
    'RrFallTiStamp_array_14'
    'RrFallTiStamp_array_15'
    'RrFallTiStamp_array_16'
    'RrFallTiStamp_array_17'
    'RrFallTiStamp_array_18'
    'RrFallTiStamp_array_19'
    'RrFallTiStamp_array_20'
    'RrFallTiStamp_array_21'
    'RrFallTiStamp_array_22'
    'RrFallTiStamp_array_23'
    'RrFallTiStamp_array_24'
    'RrFallTiStamp_array_25'
    'RrFallTiStamp_array_26'
    'RrFallTiStamp_array_27'
    'RrFallTiStamp_array_28'
    'RrFallTiStamp_array_29'
    'RrFallTiStamp_array_30'
    'RrFallTiStamp_array_31'
    };
Ioc_InputSR5msFallTiStampInfo = CreateBus(Ioc_InputSR5msFallTiStampInfo, DeList);
clear DeList;

Ioc_InputSR5msEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputSR5msEcuModeSts'};
Ioc_InputSR5msEcuModeSts = CreateBus(Ioc_InputSR5msEcuModeSts, DeList);
clear DeList;

Ioc_InputSR5msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputSR5msFuncInhibitIocSts'};
Ioc_InputSR5msFuncInhibitIocSts = CreateBus(Ioc_InputSR5msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_InputSR5msWssMonInfo = Simulink.Bus;
DeList={
    'FlRisngIdx'
    'FlFallIdx'
    'FlRisngTiStamp_array_0'
    'FlRisngTiStamp_array_1'
    'FlRisngTiStamp_array_2'
    'FlRisngTiStamp_array_3'
    'FlRisngTiStamp_array_4'
    'FlRisngTiStamp_array_5'
    'FlRisngTiStamp_array_6'
    'FlRisngTiStamp_array_7'
    'FlRisngTiStamp_array_8'
    'FlRisngTiStamp_array_9'
    'FlRisngTiStamp_array_10'
    'FlRisngTiStamp_array_11'
    'FlRisngTiStamp_array_12'
    'FlRisngTiStamp_array_13'
    'FlRisngTiStamp_array_14'
    'FlRisngTiStamp_array_15'
    'FlRisngTiStamp_array_16'
    'FlRisngTiStamp_array_17'
    'FlRisngTiStamp_array_18'
    'FlRisngTiStamp_array_19'
    'FlRisngTiStamp_array_20'
    'FlRisngTiStamp_array_21'
    'FlRisngTiStamp_array_22'
    'FlRisngTiStamp_array_23'
    'FlRisngTiStamp_array_24'
    'FlRisngTiStamp_array_25'
    'FlRisngTiStamp_array_26'
    'FlRisngTiStamp_array_27'
    'FlRisngTiStamp_array_28'
    'FlRisngTiStamp_array_29'
    'FlRisngTiStamp_array_30'
    'FlRisngTiStamp_array_31'
    'FlFallTiStamp_array_0'
    'FlFallTiStamp_array_1'
    'FlFallTiStamp_array_2'
    'FlFallTiStamp_array_3'
    'FlFallTiStamp_array_4'
    'FlFallTiStamp_array_5'
    'FlFallTiStamp_array_6'
    'FlFallTiStamp_array_7'
    'FlFallTiStamp_array_8'
    'FlFallTiStamp_array_9'
    'FlFallTiStamp_array_10'
    'FlFallTiStamp_array_11'
    'FlFallTiStamp_array_12'
    'FlFallTiStamp_array_13'
    'FlFallTiStamp_array_14'
    'FlFallTiStamp_array_15'
    'FlFallTiStamp_array_16'
    'FlFallTiStamp_array_17'
    'FlFallTiStamp_array_18'
    'FlFallTiStamp_array_19'
    'FlFallTiStamp_array_20'
    'FlFallTiStamp_array_21'
    'FlFallTiStamp_array_22'
    'FlFallTiStamp_array_23'
    'FlFallTiStamp_array_24'
    'FlFallTiStamp_array_25'
    'FlFallTiStamp_array_26'
    'FlFallTiStamp_array_27'
    'FlFallTiStamp_array_28'
    'FlFallTiStamp_array_29'
    'FlFallTiStamp_array_30'
    'FlFallTiStamp_array_31'
    'FrRisngIdx'
    'FrFallIdx'
    'FrRisngTiStamp_array_0'
    'FrRisngTiStamp_array_1'
    'FrRisngTiStamp_array_2'
    'FrRisngTiStamp_array_3'
    'FrRisngTiStamp_array_4'
    'FrRisngTiStamp_array_5'
    'FrRisngTiStamp_array_6'
    'FrRisngTiStamp_array_7'
    'FrRisngTiStamp_array_8'
    'FrRisngTiStamp_array_9'
    'FrRisngTiStamp_array_10'
    'FrRisngTiStamp_array_11'
    'FrRisngTiStamp_array_12'
    'FrRisngTiStamp_array_13'
    'FrRisngTiStamp_array_14'
    'FrRisngTiStamp_array_15'
    'FrRisngTiStamp_array_16'
    'FrRisngTiStamp_array_17'
    'FrRisngTiStamp_array_18'
    'FrRisngTiStamp_array_19'
    'FrRisngTiStamp_array_20'
    'FrRisngTiStamp_array_21'
    'FrRisngTiStamp_array_22'
    'FrRisngTiStamp_array_23'
    'FrRisngTiStamp_array_24'
    'FrRisngTiStamp_array_25'
    'FrRisngTiStamp_array_26'
    'FrRisngTiStamp_array_27'
    'FrRisngTiStamp_array_28'
    'FrRisngTiStamp_array_29'
    'FrRisngTiStamp_array_30'
    'FrRisngTiStamp_array_31'
    'FrFallTiStamp_array_0'
    'FrFallTiStamp_array_1'
    'FrFallTiStamp_array_2'
    'FrFallTiStamp_array_3'
    'FrFallTiStamp_array_4'
    'FrFallTiStamp_array_5'
    'FrFallTiStamp_array_6'
    'FrFallTiStamp_array_7'
    'FrFallTiStamp_array_8'
    'FrFallTiStamp_array_9'
    'FrFallTiStamp_array_10'
    'FrFallTiStamp_array_11'
    'FrFallTiStamp_array_12'
    'FrFallTiStamp_array_13'
    'FrFallTiStamp_array_14'
    'FrFallTiStamp_array_15'
    'FrFallTiStamp_array_16'
    'FrFallTiStamp_array_17'
    'FrFallTiStamp_array_18'
    'FrFallTiStamp_array_19'
    'FrFallTiStamp_array_20'
    'FrFallTiStamp_array_21'
    'FrFallTiStamp_array_22'
    'FrFallTiStamp_array_23'
    'FrFallTiStamp_array_24'
    'FrFallTiStamp_array_25'
    'FrFallTiStamp_array_26'
    'FrFallTiStamp_array_27'
    'FrFallTiStamp_array_28'
    'FrFallTiStamp_array_29'
    'FrFallTiStamp_array_30'
    'FrFallTiStamp_array_31'
    'RlRisngIdx'
    'RlFallIdx'
    'RlRisngTiStamp_array_0'
    'RlRisngTiStamp_array_1'
    'RlRisngTiStamp_array_2'
    'RlRisngTiStamp_array_3'
    'RlRisngTiStamp_array_4'
    'RlRisngTiStamp_array_5'
    'RlRisngTiStamp_array_6'
    'RlRisngTiStamp_array_7'
    'RlRisngTiStamp_array_8'
    'RlRisngTiStamp_array_9'
    'RlRisngTiStamp_array_10'
    'RlRisngTiStamp_array_11'
    'RlRisngTiStamp_array_12'
    'RlRisngTiStamp_array_13'
    'RlRisngTiStamp_array_14'
    'RlRisngTiStamp_array_15'
    'RlRisngTiStamp_array_16'
    'RlRisngTiStamp_array_17'
    'RlRisngTiStamp_array_18'
    'RlRisngTiStamp_array_19'
    'RlRisngTiStamp_array_20'
    'RlRisngTiStamp_array_21'
    'RlRisngTiStamp_array_22'
    'RlRisngTiStamp_array_23'
    'RlRisngTiStamp_array_24'
    'RlRisngTiStamp_array_25'
    'RlRisngTiStamp_array_26'
    'RlRisngTiStamp_array_27'
    'RlRisngTiStamp_array_28'
    'RlRisngTiStamp_array_29'
    'RlRisngTiStamp_array_30'
    'RlRisngTiStamp_array_31'
    'RlFallTiStamp_array_0'
    'RlFallTiStamp_array_1'
    'RlFallTiStamp_array_2'
    'RlFallTiStamp_array_3'
    'RlFallTiStamp_array_4'
    'RlFallTiStamp_array_5'
    'RlFallTiStamp_array_6'
    'RlFallTiStamp_array_7'
    'RlFallTiStamp_array_8'
    'RlFallTiStamp_array_9'
    'RlFallTiStamp_array_10'
    'RlFallTiStamp_array_11'
    'RlFallTiStamp_array_12'
    'RlFallTiStamp_array_13'
    'RlFallTiStamp_array_14'
    'RlFallTiStamp_array_15'
    'RlFallTiStamp_array_16'
    'RlFallTiStamp_array_17'
    'RlFallTiStamp_array_18'
    'RlFallTiStamp_array_19'
    'RlFallTiStamp_array_20'
    'RlFallTiStamp_array_21'
    'RlFallTiStamp_array_22'
    'RlFallTiStamp_array_23'
    'RlFallTiStamp_array_24'
    'RlFallTiStamp_array_25'
    'RlFallTiStamp_array_26'
    'RlFallTiStamp_array_27'
    'RlFallTiStamp_array_28'
    'RlFallTiStamp_array_29'
    'RlFallTiStamp_array_30'
    'RlFallTiStamp_array_31'
    'RrRisngIdx'
    'RrFallIdx'
    'RrRisngTiStamp_array_0'
    'RrRisngTiStamp_array_1'
    'RrRisngTiStamp_array_2'
    'RrRisngTiStamp_array_3'
    'RrRisngTiStamp_array_4'
    'RrRisngTiStamp_array_5'
    'RrRisngTiStamp_array_6'
    'RrRisngTiStamp_array_7'
    'RrRisngTiStamp_array_8'
    'RrRisngTiStamp_array_9'
    'RrRisngTiStamp_array_10'
    'RrRisngTiStamp_array_11'
    'RrRisngTiStamp_array_12'
    'RrRisngTiStamp_array_13'
    'RrRisngTiStamp_array_14'
    'RrRisngTiStamp_array_15'
    'RrRisngTiStamp_array_16'
    'RrRisngTiStamp_array_17'
    'RrRisngTiStamp_array_18'
    'RrRisngTiStamp_array_19'
    'RrRisngTiStamp_array_20'
    'RrRisngTiStamp_array_21'
    'RrRisngTiStamp_array_22'
    'RrRisngTiStamp_array_23'
    'RrRisngTiStamp_array_24'
    'RrRisngTiStamp_array_25'
    'RrRisngTiStamp_array_26'
    'RrRisngTiStamp_array_27'
    'RrRisngTiStamp_array_28'
    'RrRisngTiStamp_array_29'
    'RrRisngTiStamp_array_30'
    'RrRisngTiStamp_array_31'
    'RrFallTiStamp_array_0'
    'RrFallTiStamp_array_1'
    'RrFallTiStamp_array_2'
    'RrFallTiStamp_array_3'
    'RrFallTiStamp_array_4'
    'RrFallTiStamp_array_5'
    'RrFallTiStamp_array_6'
    'RrFallTiStamp_array_7'
    'RrFallTiStamp_array_8'
    'RrFallTiStamp_array_9'
    'RrFallTiStamp_array_10'
    'RrFallTiStamp_array_11'
    'RrFallTiStamp_array_12'
    'RrFallTiStamp_array_13'
    'RrFallTiStamp_array_14'
    'RrFallTiStamp_array_15'
    'RrFallTiStamp_array_16'
    'RrFallTiStamp_array_17'
    'RrFallTiStamp_array_18'
    'RrFallTiStamp_array_19'
    'RrFallTiStamp_array_20'
    'RrFallTiStamp_array_21'
    'RrFallTiStamp_array_22'
    'RrFallTiStamp_array_23'
    'RrFallTiStamp_array_24'
    'RrFallTiStamp_array_25'
    'RrFallTiStamp_array_26'
    'RrFallTiStamp_array_27'
    'RrFallTiStamp_array_28'
    'RrFallTiStamp_array_29'
    'RrFallTiStamp_array_30'
    'RrFallTiStamp_array_31'
    };
Ioc_InputSR5msWssMonInfo = CreateBus(Ioc_InputSR5msWssMonInfo, DeList);
clear DeList;


Nvm_HndlrEcuModeSts = Simulink.Bus;
DeList={'Nvm_HndlrEcuModeSts'};
Nvm_HndlrEcuModeSts = CreateBus(Nvm_HndlrEcuModeSts, DeList);
clear DeList;

Nvm_HndlrFuncInhibitNvmSts = Simulink.Bus;
DeList={'Nvm_HndlrFuncInhibitNvmSts'};
Nvm_HndlrFuncInhibitNvmSts = CreateBus(Nvm_HndlrFuncInhibitNvmSts, DeList);
clear DeList;


Prly_HndlrCanRxClu2Info = Simulink.Bus;
DeList={
    'IgnRun'
    };
Prly_HndlrCanRxClu2Info = CreateBus(Prly_HndlrCanRxClu2Info, DeList);
clear DeList;

Prly_HndlrEcuModeSts = Simulink.Bus;
DeList={'Prly_HndlrEcuModeSts'};
Prly_HndlrEcuModeSts = CreateBus(Prly_HndlrEcuModeSts, DeList);
clear DeList;

Prly_HndlrMomEcuInhibit = Simulink.Bus;
DeList={'Prly_HndlrMomEcuInhibit'};
Prly_HndlrMomEcuInhibit = CreateBus(Prly_HndlrMomEcuInhibit, DeList);
clear DeList;

Prly_HndlrFuncInhibitPrlySts = Simulink.Bus;
DeList={'Prly_HndlrFuncInhibitPrlySts'};
Prly_HndlrFuncInhibitPrlySts = CreateBus(Prly_HndlrFuncInhibitPrlySts, DeList);
clear DeList;

Prly_HndlrIgnOnOffSts = Simulink.Bus;
DeList={'Prly_HndlrIgnOnOffSts'};
Prly_HndlrIgnOnOffSts = CreateBus(Prly_HndlrIgnOnOffSts, DeList);
clear DeList;

Prly_HndlrIgnEdgeSts = Simulink.Bus;
DeList={'Prly_HndlrIgnEdgeSts'};
Prly_HndlrIgnEdgeSts = CreateBus(Prly_HndlrIgnEdgeSts, DeList);
clear DeList;


Mom_HndlrFuncInhibitMomSts = Simulink.Bus;
DeList={'Mom_HndlrFuncInhibitMomSts'};
Mom_HndlrFuncInhibitMomSts = CreateBus(Mom_HndlrFuncInhibitMomSts, DeList);
clear DeList;

Mom_HndlrEcuModeSts = Simulink.Bus;
DeList={'Mom_HndlrEcuModeSts'};
Mom_HndlrEcuModeSts = CreateBus(Mom_HndlrEcuModeSts, DeList);
clear DeList;

Mom_HndlrMomEcuInhibit = Simulink.Bus;
DeList={'Mom_HndlrMomEcuInhibit'};
Mom_HndlrMomEcuInhibit = CreateBus(Mom_HndlrMomEcuInhibit, DeList);
clear DeList;


Wss_SenWssMonInfo = Simulink.Bus;
DeList={
    'FlRisngIdx'
    'FlFallIdx'
    'FlRisngTiStamp_array_0'
    'FlRisngTiStamp_array_1'
    'FlRisngTiStamp_array_2'
    'FlRisngTiStamp_array_3'
    'FlRisngTiStamp_array_4'
    'FlRisngTiStamp_array_5'
    'FlRisngTiStamp_array_6'
    'FlRisngTiStamp_array_7'
    'FlRisngTiStamp_array_8'
    'FlRisngTiStamp_array_9'
    'FlRisngTiStamp_array_10'
    'FlRisngTiStamp_array_11'
    'FlRisngTiStamp_array_12'
    'FlRisngTiStamp_array_13'
    'FlRisngTiStamp_array_14'
    'FlRisngTiStamp_array_15'
    'FlRisngTiStamp_array_16'
    'FlRisngTiStamp_array_17'
    'FlRisngTiStamp_array_18'
    'FlRisngTiStamp_array_19'
    'FlRisngTiStamp_array_20'
    'FlRisngTiStamp_array_21'
    'FlRisngTiStamp_array_22'
    'FlRisngTiStamp_array_23'
    'FlRisngTiStamp_array_24'
    'FlRisngTiStamp_array_25'
    'FlRisngTiStamp_array_26'
    'FlRisngTiStamp_array_27'
    'FlRisngTiStamp_array_28'
    'FlRisngTiStamp_array_29'
    'FlRisngTiStamp_array_30'
    'FlRisngTiStamp_array_31'
    'FlFallTiStamp_array_0'
    'FlFallTiStamp_array_1'
    'FlFallTiStamp_array_2'
    'FlFallTiStamp_array_3'
    'FlFallTiStamp_array_4'
    'FlFallTiStamp_array_5'
    'FlFallTiStamp_array_6'
    'FlFallTiStamp_array_7'
    'FlFallTiStamp_array_8'
    'FlFallTiStamp_array_9'
    'FlFallTiStamp_array_10'
    'FlFallTiStamp_array_11'
    'FlFallTiStamp_array_12'
    'FlFallTiStamp_array_13'
    'FlFallTiStamp_array_14'
    'FlFallTiStamp_array_15'
    'FlFallTiStamp_array_16'
    'FlFallTiStamp_array_17'
    'FlFallTiStamp_array_18'
    'FlFallTiStamp_array_19'
    'FlFallTiStamp_array_20'
    'FlFallTiStamp_array_21'
    'FlFallTiStamp_array_22'
    'FlFallTiStamp_array_23'
    'FlFallTiStamp_array_24'
    'FlFallTiStamp_array_25'
    'FlFallTiStamp_array_26'
    'FlFallTiStamp_array_27'
    'FlFallTiStamp_array_28'
    'FlFallTiStamp_array_29'
    'FlFallTiStamp_array_30'
    'FlFallTiStamp_array_31'
    'FrRisngIdx'
    'FrFallIdx'
    'FrRisngTiStamp_array_0'
    'FrRisngTiStamp_array_1'
    'FrRisngTiStamp_array_2'
    'FrRisngTiStamp_array_3'
    'FrRisngTiStamp_array_4'
    'FrRisngTiStamp_array_5'
    'FrRisngTiStamp_array_6'
    'FrRisngTiStamp_array_7'
    'FrRisngTiStamp_array_8'
    'FrRisngTiStamp_array_9'
    'FrRisngTiStamp_array_10'
    'FrRisngTiStamp_array_11'
    'FrRisngTiStamp_array_12'
    'FrRisngTiStamp_array_13'
    'FrRisngTiStamp_array_14'
    'FrRisngTiStamp_array_15'
    'FrRisngTiStamp_array_16'
    'FrRisngTiStamp_array_17'
    'FrRisngTiStamp_array_18'
    'FrRisngTiStamp_array_19'
    'FrRisngTiStamp_array_20'
    'FrRisngTiStamp_array_21'
    'FrRisngTiStamp_array_22'
    'FrRisngTiStamp_array_23'
    'FrRisngTiStamp_array_24'
    'FrRisngTiStamp_array_25'
    'FrRisngTiStamp_array_26'
    'FrRisngTiStamp_array_27'
    'FrRisngTiStamp_array_28'
    'FrRisngTiStamp_array_29'
    'FrRisngTiStamp_array_30'
    'FrRisngTiStamp_array_31'
    'FrFallTiStamp_array_0'
    'FrFallTiStamp_array_1'
    'FrFallTiStamp_array_2'
    'FrFallTiStamp_array_3'
    'FrFallTiStamp_array_4'
    'FrFallTiStamp_array_5'
    'FrFallTiStamp_array_6'
    'FrFallTiStamp_array_7'
    'FrFallTiStamp_array_8'
    'FrFallTiStamp_array_9'
    'FrFallTiStamp_array_10'
    'FrFallTiStamp_array_11'
    'FrFallTiStamp_array_12'
    'FrFallTiStamp_array_13'
    'FrFallTiStamp_array_14'
    'FrFallTiStamp_array_15'
    'FrFallTiStamp_array_16'
    'FrFallTiStamp_array_17'
    'FrFallTiStamp_array_18'
    'FrFallTiStamp_array_19'
    'FrFallTiStamp_array_20'
    'FrFallTiStamp_array_21'
    'FrFallTiStamp_array_22'
    'FrFallTiStamp_array_23'
    'FrFallTiStamp_array_24'
    'FrFallTiStamp_array_25'
    'FrFallTiStamp_array_26'
    'FrFallTiStamp_array_27'
    'FrFallTiStamp_array_28'
    'FrFallTiStamp_array_29'
    'FrFallTiStamp_array_30'
    'FrFallTiStamp_array_31'
    'RlRisngIdx'
    'RlFallIdx'
    'RlRisngTiStamp_array_0'
    'RlRisngTiStamp_array_1'
    'RlRisngTiStamp_array_2'
    'RlRisngTiStamp_array_3'
    'RlRisngTiStamp_array_4'
    'RlRisngTiStamp_array_5'
    'RlRisngTiStamp_array_6'
    'RlRisngTiStamp_array_7'
    'RlRisngTiStamp_array_8'
    'RlRisngTiStamp_array_9'
    'RlRisngTiStamp_array_10'
    'RlRisngTiStamp_array_11'
    'RlRisngTiStamp_array_12'
    'RlRisngTiStamp_array_13'
    'RlRisngTiStamp_array_14'
    'RlRisngTiStamp_array_15'
    'RlRisngTiStamp_array_16'
    'RlRisngTiStamp_array_17'
    'RlRisngTiStamp_array_18'
    'RlRisngTiStamp_array_19'
    'RlRisngTiStamp_array_20'
    'RlRisngTiStamp_array_21'
    'RlRisngTiStamp_array_22'
    'RlRisngTiStamp_array_23'
    'RlRisngTiStamp_array_24'
    'RlRisngTiStamp_array_25'
    'RlRisngTiStamp_array_26'
    'RlRisngTiStamp_array_27'
    'RlRisngTiStamp_array_28'
    'RlRisngTiStamp_array_29'
    'RlRisngTiStamp_array_30'
    'RlRisngTiStamp_array_31'
    'RlFallTiStamp_array_0'
    'RlFallTiStamp_array_1'
    'RlFallTiStamp_array_2'
    'RlFallTiStamp_array_3'
    'RlFallTiStamp_array_4'
    'RlFallTiStamp_array_5'
    'RlFallTiStamp_array_6'
    'RlFallTiStamp_array_7'
    'RlFallTiStamp_array_8'
    'RlFallTiStamp_array_9'
    'RlFallTiStamp_array_10'
    'RlFallTiStamp_array_11'
    'RlFallTiStamp_array_12'
    'RlFallTiStamp_array_13'
    'RlFallTiStamp_array_14'
    'RlFallTiStamp_array_15'
    'RlFallTiStamp_array_16'
    'RlFallTiStamp_array_17'
    'RlFallTiStamp_array_18'
    'RlFallTiStamp_array_19'
    'RlFallTiStamp_array_20'
    'RlFallTiStamp_array_21'
    'RlFallTiStamp_array_22'
    'RlFallTiStamp_array_23'
    'RlFallTiStamp_array_24'
    'RlFallTiStamp_array_25'
    'RlFallTiStamp_array_26'
    'RlFallTiStamp_array_27'
    'RlFallTiStamp_array_28'
    'RlFallTiStamp_array_29'
    'RlFallTiStamp_array_30'
    'RlFallTiStamp_array_31'
    'RrRisngIdx'
    'RrFallIdx'
    'RrRisngTiStamp_array_0'
    'RrRisngTiStamp_array_1'
    'RrRisngTiStamp_array_2'
    'RrRisngTiStamp_array_3'
    'RrRisngTiStamp_array_4'
    'RrRisngTiStamp_array_5'
    'RrRisngTiStamp_array_6'
    'RrRisngTiStamp_array_7'
    'RrRisngTiStamp_array_8'
    'RrRisngTiStamp_array_9'
    'RrRisngTiStamp_array_10'
    'RrRisngTiStamp_array_11'
    'RrRisngTiStamp_array_12'
    'RrRisngTiStamp_array_13'
    'RrRisngTiStamp_array_14'
    'RrRisngTiStamp_array_15'
    'RrRisngTiStamp_array_16'
    'RrRisngTiStamp_array_17'
    'RrRisngTiStamp_array_18'
    'RrRisngTiStamp_array_19'
    'RrRisngTiStamp_array_20'
    'RrRisngTiStamp_array_21'
    'RrRisngTiStamp_array_22'
    'RrRisngTiStamp_array_23'
    'RrRisngTiStamp_array_24'
    'RrRisngTiStamp_array_25'
    'RrRisngTiStamp_array_26'
    'RrRisngTiStamp_array_27'
    'RrRisngTiStamp_array_28'
    'RrRisngTiStamp_array_29'
    'RrRisngTiStamp_array_30'
    'RrRisngTiStamp_array_31'
    'RrFallTiStamp_array_0'
    'RrFallTiStamp_array_1'
    'RrFallTiStamp_array_2'
    'RrFallTiStamp_array_3'
    'RrFallTiStamp_array_4'
    'RrFallTiStamp_array_5'
    'RrFallTiStamp_array_6'
    'RrFallTiStamp_array_7'
    'RrFallTiStamp_array_8'
    'RrFallTiStamp_array_9'
    'RrFallTiStamp_array_10'
    'RrFallTiStamp_array_11'
    'RrFallTiStamp_array_12'
    'RrFallTiStamp_array_13'
    'RrFallTiStamp_array_14'
    'RrFallTiStamp_array_15'
    'RrFallTiStamp_array_16'
    'RrFallTiStamp_array_17'
    'RrFallTiStamp_array_18'
    'RrFallTiStamp_array_19'
    'RrFallTiStamp_array_20'
    'RrFallTiStamp_array_21'
    'RrFallTiStamp_array_22'
    'RrFallTiStamp_array_23'
    'RrFallTiStamp_array_24'
    'RrFallTiStamp_array_25'
    'RrFallTiStamp_array_26'
    'RrFallTiStamp_array_27'
    'RrFallTiStamp_array_28'
    'RrFallTiStamp_array_29'
    'RrFallTiStamp_array_30'
    'RrFallTiStamp_array_31'
    };
Wss_SenWssMonInfo = CreateBus(Wss_SenWssMonInfo, DeList);
clear DeList;

Wss_SenEcuModeSts = Simulink.Bus;
DeList={'Wss_SenEcuModeSts'};
Wss_SenEcuModeSts = CreateBus(Wss_SenEcuModeSts, DeList);
clear DeList;

Wss_SenFuncInhibitWssSts = Simulink.Bus;
DeList={'Wss_SenFuncInhibitWssSts'};
Wss_SenFuncInhibitWssSts = CreateBus(Wss_SenFuncInhibitWssSts, DeList);
clear DeList;

Wss_SenWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Wss_SenWhlSpdInfo = CreateBus(Wss_SenWhlSpdInfo, DeList);
clear DeList;

Wss_SenWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
Wss_SenWssSpeedOut = CreateBus(Wss_SenWssSpeedOut, DeList);
clear DeList;


Pedal_SenSyncEcuModeSts = Simulink.Bus;
DeList={'Pedal_SenSyncEcuModeSts'};
Pedal_SenSyncEcuModeSts = CreateBus(Pedal_SenSyncEcuModeSts, DeList);
clear DeList;

Pedal_SenSyncFuncInhibitPedalSts = Simulink.Bus;
DeList={'Pedal_SenSyncFuncInhibitPedalSts'};
Pedal_SenSyncFuncInhibitPedalSts = CreateBus(Pedal_SenSyncFuncInhibitPedalSts, DeList);
clear DeList;

Pedal_SenSyncPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Pedal_SenSyncPdt5msRawInfo = CreateBus(Pedal_SenSyncPdt5msRawInfo, DeList);
clear DeList;

Pedal_SenSyncPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
Pedal_SenSyncPdf5msRawInfo = CreateBus(Pedal_SenSyncPdf5msRawInfo, DeList);
clear DeList;


Press_SenSyncEcuModeSts = Simulink.Bus;
DeList={'Press_SenSyncEcuModeSts'};
Press_SenSyncEcuModeSts = CreateBus(Press_SenSyncEcuModeSts, DeList);
clear DeList;

Press_SenSyncFuncInhibitPressSts = Simulink.Bus;
DeList={'Press_SenSyncFuncInhibitPressSts'};
Press_SenSyncFuncInhibitPressSts = CreateBus(Press_SenSyncFuncInhibitPressSts, DeList);
clear DeList;

Press_SenSyncCircP5msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Press_SenSyncCircP5msRawInfo = CreateBus(Press_SenSyncCircP5msRawInfo, DeList);
clear DeList;

Press_SenSyncPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Press_SenSyncPistP5msRawInfo = CreateBus(Press_SenSyncPistP5msRawInfo, DeList);
clear DeList;

Press_SenSyncPressSenCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimPMoveAve'
    'PressP_CirP1MoveAve'
    'PressP_CirP2MoveAve'
    'PressP_SimPMoveAveEolOfs'
    'PressP_CirP1MoveAveEolOfs'
    'PressP_CirP2MoveAveEolOfs'
    };
Press_SenSyncPressSenCalcInfo = CreateBus(Press_SenSyncPressSenCalcInfo, DeList);
clear DeList;

Press_SenSyncPedlSimPRaw = Simulink.Bus;
DeList={'Press_SenSyncPedlSimPRaw'};
Press_SenSyncPedlSimPRaw = CreateBus(Press_SenSyncPedlSimPRaw, DeList);
clear DeList;


Swt_SenEcuModeSts = Simulink.Bus;
DeList={'Swt_SenEcuModeSts'};
Swt_SenEcuModeSts = CreateBus(Swt_SenEcuModeSts, DeList);
clear DeList;

Swt_SenFuncInhibitSwtSts = Simulink.Bus;
DeList={'Swt_SenFuncInhibitSwtSts'};
Swt_SenFuncInhibitSwtSts = CreateBus(Swt_SenFuncInhibitSwtSts, DeList);
clear DeList;

Swt_SenSwtStsFlexBrkInfo = Simulink.Bus;
DeList={
    'FlexBrkASwt'
    'FlexBrkBSwt'
    };
Swt_SenSwtStsFlexBrkInfo = CreateBus(Swt_SenSwtStsFlexBrkInfo, DeList);
clear DeList;

Swt_SenEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
Swt_SenEscSwtStInfo = CreateBus(Swt_SenEscSwtStInfo, DeList);
clear DeList;

Swt_SenBlsSwt = Simulink.Bus;
DeList={'Swt_SenBlsSwt'};
Swt_SenBlsSwt = CreateBus(Swt_SenBlsSwt, DeList);
clear DeList;

Swt_SenEscSwt = Simulink.Bus;
DeList={'Swt_SenEscSwt'};
Swt_SenEscSwt = CreateBus(Swt_SenEscSwt, DeList);
clear DeList;


Spc_5msCtrlNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    };
Spc_5msCtrlNormVlvReqVlvActInfo = CreateBus(Spc_5msCtrlNormVlvReqVlvActInfo, DeList);
clear DeList;

Spc_5msCtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    };
Spc_5msCtrlEemFailData = CreateBus(Spc_5msCtrlEemFailData, DeList);
clear DeList;

Spc_5msCtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Spc_5msCtrlCanRxAccelPedlInfo = CreateBus(Spc_5msCtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Spc_5msCtrlPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Spc_5msCtrlPdt5msRawInfo = CreateBus(Spc_5msCtrlPdt5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    };
Spc_5msCtrlPdf5msRawInfo = CreateBus(Spc_5msCtrlPdf5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlSwtStsFlexBrkInfo = Simulink.Bus;
DeList={
    'FlexBrkASwt'
    'FlexBrkBSwt'
    };
Spc_5msCtrlSwtStsFlexBrkInfo = CreateBus(Spc_5msCtrlSwtStsFlexBrkInfo, DeList);
clear DeList;

Spc_5msCtrlWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Spc_5msCtrlWhlSpdInfo = CreateBus(Spc_5msCtrlWhlSpdInfo, DeList);
clear DeList;

Spc_5msCtrlCircP5msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Spc_5msCtrlCircP5msRawInfo = CreateBus(Spc_5msCtrlCircP5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Spc_5msCtrlPistP5msRawInfo = CreateBus(Spc_5msCtrlPistP5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Spc_5msCtrlAbsCtrlInfo = CreateBus(Spc_5msCtrlAbsCtrlInfo, DeList);
clear DeList;

Spc_5msCtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    };
Spc_5msCtrlEscCtrlInfo = CreateBus(Spc_5msCtrlEscCtrlInfo, DeList);
clear DeList;

Spc_5msCtrlSwtStsFSInfo = Simulink.Bus;
DeList={
    'FlexBrkSwtFaultDet'
    };
Spc_5msCtrlSwtStsFSInfo = CreateBus(Spc_5msCtrlSwtStsFSInfo, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlTagInfo = Simulink.Bus;
DeList={
    'PdfSigNoiseSuspcFlgH'
    'PdfSigTag'
    'PdtSigNoiseSuspcFlgH'
    'PdtSigTag'
    };
Spc_5msCtrlPedlTrvlTagInfo = CreateBus(Spc_5msCtrlPedlTrvlTagInfo, DeList);
clear DeList;

Spc_5msCtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BLS'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    };
Spc_5msCtrlEemSuspectData = CreateBus(Spc_5msCtrlEemSuspectData, DeList);
clear DeList;

Spc_5msCtrlEcuModeSts = Simulink.Bus;
DeList={'Spc_5msCtrlEcuModeSts'};
Spc_5msCtrlEcuModeSts = CreateBus(Spc_5msCtrlEcuModeSts, DeList);
clear DeList;

Spc_5msCtrlPCtrlAct = Simulink.Bus;
DeList={'Spc_5msCtrlPCtrlAct'};
Spc_5msCtrlPCtrlAct = CreateBus(Spc_5msCtrlPCtrlAct, DeList);
clear DeList;

Spc_5msCtrlBlsSwt = Simulink.Bus;
DeList={'Spc_5msCtrlBlsSwt'};
Spc_5msCtrlBlsSwt = CreateBus(Spc_5msCtrlBlsSwt, DeList);
clear DeList;

Spc_5msCtrlPedlSimPRaw = Simulink.Bus;
DeList={'Spc_5msCtrlPedlSimPRaw'};
Spc_5msCtrlPedlSimPRaw = CreateBus(Spc_5msCtrlPedlSimPRaw, DeList);
clear DeList;

Spc_5msCtrlActvBrkCtrlrActFlg = Simulink.Bus;
DeList={'Spc_5msCtrlActvBrkCtrlrActFlg'};
Spc_5msCtrlActvBrkCtrlrActFlg = CreateBus(Spc_5msCtrlActvBrkCtrlrActFlg, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Spc_5msCtrlPedlTrvlFinal'};
Spc_5msCtrlPedlTrvlFinal = CreateBus(Spc_5msCtrlPedlTrvlFinal, DeList);
clear DeList;

Spc_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Spc_5msCtrlVehSpdFild'};
Spc_5msCtrlVehSpdFild = CreateBus(Spc_5msCtrlVehSpdFild, DeList);
clear DeList;

Spc_5msCtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild'
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild'
    'SecdCircPFild_1_100Bar'
    };
Spc_5msCtrlCircPFildInfo = CreateBus(Spc_5msCtrlCircPFildInfo, DeList);
clear DeList;

Spc_5msCtrlCircPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PrimCircPOffsCorrd'
    'PrimCircPOffsCorrdStOkFlg'
    'SecdCircPOffsCorrd'
    'SecdCircPOffsCorrdStOkFlg'
    };
Spc_5msCtrlCircPOffsCorrdInfo = CreateBus(Spc_5msCtrlCircPOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild'
    'PedlSimrPFild_1_100Bar'
    };
Spc_5msCtrlPedlSimrPFildInfo = CreateBus(Spc_5msCtrlPedlSimrPFildInfo, DeList);
clear DeList;

Spc_5msCtrlPedlSimrPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PedlSimrPOffsCorrd'
    'PedlSimrPOffsCorrdStOkFlg'
    };
Spc_5msCtrlPedlSimrPOffsCorrdInfo = CreateBus(Spc_5msCtrlPedlSimrPOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlFildInfo = Simulink.Bus;
DeList={
    'PdfFild'
    'PdtFild'
    };
Spc_5msCtrlPedlTrvlFildInfo = CreateBus(Spc_5msCtrlPedlTrvlFildInfo, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlOffsCorrdInfo = Simulink.Bus;
DeList={
    'PdfOffsCorrdStOkFlg'
    'PdfRawOffsCorrd'
    'PdtOffsCorrdStOkFlg'
    'PdtRawOffsCorrd'
    };
Spc_5msCtrlPedlTrvlOffsCorrdInfo = CreateBus(Spc_5msCtrlPedlTrvlOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild'
    'PistPFild_1_100Bar'
    };
Spc_5msCtrlPistPFildInfo = CreateBus(Spc_5msCtrlPistPFildInfo, DeList);
clear DeList;

Spc_5msCtrlPistPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PistPOffsCorrd'
    'PistPOffsCorrdStOkFlg'
    };
Spc_5msCtrlPistPOffsCorrdInfo = CreateBus(Spc_5msCtrlPistPOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlWhlSpdFildInfo = Simulink.Bus;
DeList={
    'WhlSpdFildFrntLe'
    'WhlSpdFildFrntRi'
    'WhlSpdFildReLe'
    'WhlSpdFildReRi'
    };
Spc_5msCtrlWhlSpdFildInfo = CreateBus(Spc_5msCtrlWhlSpdFildInfo, DeList);
clear DeList;

Spc_5msCtrlBlsFild = Simulink.Bus;
DeList={'Spc_5msCtrlBlsFild'};
Spc_5msCtrlBlsFild = CreateBus(Spc_5msCtrlBlsFild, DeList);
clear DeList;

Spc_5msCtrlFlexBrkSwtFild = Simulink.Bus;
DeList={'Spc_5msCtrlFlexBrkSwtFild'};
Spc_5msCtrlFlexBrkSwtFild = CreateBus(Spc_5msCtrlFlexBrkSwtFild, DeList);
clear DeList;


Det_5msCtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    };
Det_5msCtrlWhlVlvReqAbcInfo = CreateBus(Det_5msCtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Det_5msCtrlNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'RelsVlvReq'
    'CircVlvReq'
    'PressDumpVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    'CircVlvDataLen'
    'RelsVlvDataLen'
    'PressDumpVlvDataLen'
    };
Det_5msCtrlNormVlvReqVlvActInfo = CreateBus(Det_5msCtrlNormVlvReqVlvActInfo, DeList);
clear DeList;

Det_5msCtrlWhlVlvReqIdbInfo = Simulink.Bus;
DeList={
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    };
Det_5msCtrlWhlVlvReqIdbInfo = CreateBus(Det_5msCtrlWhlVlvReqIdbInfo, DeList);
clear DeList;

Det_5msCtrlIdbBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'SecdBalVlvReqData_array_0'
    'SecdBalVlvReqData_array_1'
    'SecdBalVlvReqData_array_2'
    'SecdBalVlvReqData_array_3'
    'SecdBalVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    'PrimBalVlvReq'
    'SecdBalVlvReq'
    'ChmbBalVlvReq'
    'PrimBalVlvDataLen'
    'SecdBalVlvDataLen'
    'ChmbBalVlvDataLen'
    };
Det_5msCtrlIdbBalVlvReqInfo = CreateBus(Det_5msCtrlIdbBalVlvReqInfo, DeList);
clear DeList;

Det_5msCtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SimP'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
Det_5msCtrlEemFailData = CreateBus(Det_5msCtrlEemFailData, DeList);
clear DeList;

Det_5msCtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'HcuServiceMod'
    };
Det_5msCtrlCanRxIdbInfo = CreateBus(Det_5msCtrlCanRxIdbInfo, DeList);
clear DeList;

Det_5msCtrlPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Det_5msCtrlPdt5msRawInfo = CreateBus(Det_5msCtrlPdt5msRawInfo, DeList);
clear DeList;

Det_5msCtrlPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    };
Det_5msCtrlPdf5msRawInfo = CreateBus(Det_5msCtrlPdf5msRawInfo, DeList);
clear DeList;

Det_5msCtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Det_5msCtrlAbsCtrlInfo = CreateBus(Det_5msCtrlAbsCtrlInfo, DeList);
clear DeList;

Det_5msCtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild'
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild'
    'SecdCircPFild_1_100Bar'
    };
Det_5msCtrlCircPFildInfo = CreateBus(Det_5msCtrlCircPFildInfo, DeList);
clear DeList;

Det_5msCtrlCircPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PrimCircPOffsCorrd'
    'SecdCircPOffsCorrd'
    };
Det_5msCtrlCircPOffsCorrdInfo = CreateBus(Det_5msCtrlCircPOffsCorrdInfo, DeList);
clear DeList;

Det_5msCtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild_1_100Bar'
    };
Det_5msCtrlPedlSimrPFildInfo = CreateBus(Det_5msCtrlPedlSimrPFildInfo, DeList);
clear DeList;

Det_5msCtrlPedlSimrPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PedlSimrPOffsCorrd'
    };
Det_5msCtrlPedlSimrPOffsCorrdInfo = CreateBus(Det_5msCtrlPedlSimrPOffsCorrdInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlFildInfo = Simulink.Bus;
DeList={
    'PdfFild'
    'PdtFild'
    };
Det_5msCtrlPedlTrvlFildInfo = CreateBus(Det_5msCtrlPedlTrvlFildInfo, DeList);
clear DeList;

Det_5msCtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild'
    'PistPFild_1_100Bar'
    };
Det_5msCtrlPistPFildInfo = CreateBus(Det_5msCtrlPistPFildInfo, DeList);
clear DeList;

Det_5msCtrlPistPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PistPOffsCorrd'
    };
Det_5msCtrlPistPOffsCorrdInfo = CreateBus(Det_5msCtrlPistPOffsCorrdInfo, DeList);
clear DeList;

Det_5msCtrlWhlSpdFildInfo = Simulink.Bus;
DeList={
    'WhlSpdFildFrntLe'
    'WhlSpdFildFrntRi'
    'WhlSpdFildReLe'
    'WhlSpdFildReRi'
    };
Det_5msCtrlWhlSpdFildInfo = CreateBus(Det_5msCtrlWhlSpdFildInfo, DeList);
clear DeList;

Det_5msCtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SimP'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    };
Det_5msCtrlEemSuspectData = CreateBus(Det_5msCtrlEemSuspectData, DeList);
clear DeList;

Det_5msCtrlEcuModeSts = Simulink.Bus;
DeList={'Det_5msCtrlEcuModeSts'};
Det_5msCtrlEcuModeSts = CreateBus(Det_5msCtrlEcuModeSts, DeList);
clear DeList;

Det_5msCtrlPCtrlAct = Simulink.Bus;
DeList={'Det_5msCtrlPCtrlAct'};
Det_5msCtrlPCtrlAct = CreateBus(Det_5msCtrlPCtrlAct, DeList);
clear DeList;

Det_5msCtrlActvBrkCtrlrActFlg = Simulink.Bus;
DeList={'Det_5msCtrlActvBrkCtrlrActFlg'};
Det_5msCtrlActvBrkCtrlrActFlg = CreateBus(Det_5msCtrlActvBrkCtrlrActFlg, DeList);
clear DeList;

Det_5msCtrlVehSpd = Simulink.Bus;
DeList={'Det_5msCtrlVehSpd'};
Det_5msCtrlVehSpd = CreateBus(Det_5msCtrlVehSpd, DeList);
clear DeList;

Det_5msCtrlBlsFild = Simulink.Bus;
DeList={'Det_5msCtrlBlsFild'};
Det_5msCtrlBlsFild = CreateBus(Det_5msCtrlBlsFild, DeList);
clear DeList;

Det_5msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Det_5msCtrlPCtrlBoostMod'};
Det_5msCtrlPCtrlBoostMod = CreateBus(Det_5msCtrlPCtrlBoostMod, DeList);
clear DeList;

Det_5msCtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'BrkPedlStatus'
    'DrvrIntendToRelsBrk'
    'PanicBrkStFlg'
    'PedlReldStFlg1'
    'PedlReldStFlg2'
    'PedlReldStUsingPedlSimrPFlg'
    };
Det_5msCtrlBrkPedlStatusInfo = CreateBus(Det_5msCtrlBrkPedlStatusInfo, DeList);
clear DeList;

Det_5msCtrlCircPRateInfo = Simulink.Bus;
DeList={
    'PrimCircPChgDurg10ms'
    'PrimCircPChgDurg5ms'
    'PrimCircPRate'
    'PrimCircPRateBarPerSec'
    'SecdCircPChgDurg10ms'
    'SecdCircPChgDurg5ms'
    'SecdCircPRate'
    'SecdCircPRateBarPerSec'
    };
Det_5msCtrlCircPRateInfo = CreateBus(Det_5msCtrlCircPRateInfo, DeList);
clear DeList;

Det_5msCtrlEstimdWhlPInfo = Simulink.Bus;
DeList={
    'FrntLeEstimdWhlP'
    'FrntRiEstimdWhlP'
    'ReLeEstimdWhlP'
    'ReRiEstimdWhlP'
    };
Det_5msCtrlEstimdWhlPInfo = CreateBus(Det_5msCtrlEstimdWhlPInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate'
    'PedlTrvlRateMilliMtrPerSec'
    };
Det_5msCtrlPedlTrvlRateInfo = CreateBus(Det_5msCtrlPedlTrvlRateInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlTagInfo = Simulink.Bus;
DeList={
    'PdfSigNoiseSuspcFlgH'
    'PdfSigNoiseSuspcFlgL'
    'PdfSigTag'
    'PdtSigNoiseSuspcFlgH'
    'PdtSigNoiseSuspcFlgL'
    'PdtSigTag'
    'PedlSigTag'
    'PedlSimrPSigNoiseSuspcFlgH'
    'PedlSimrPSigNoiseSuspcFlgL'
    };
Det_5msCtrlPedlTrvlTagInfo = CreateBus(Det_5msCtrlPedlTrvlTagInfo, DeList);
clear DeList;

Det_5msCtrlPistPRateInfo = Simulink.Bus;
DeList={
    'PistPChgDurg10ms'
    'PistPChgDurg5ms'
    'PistPRate'
    'PistPRateBarPerSec'
    };
Det_5msCtrlPistPRateInfo = CreateBus(Det_5msCtrlPistPRateInfo, DeList);
clear DeList;

Det_5msCtrlVehStopStInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    'VehStopStFlg'
    };
Det_5msCtrlVehStopStInfo = CreateBus(Det_5msCtrlVehStopStInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Det_5msCtrlPedlTrvlFinal'};
Det_5msCtrlPedlTrvlFinal = CreateBus(Det_5msCtrlPedlTrvlFinal, DeList);
clear DeList;

Det_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Det_5msCtrlVehSpdFild'};
Det_5msCtrlVehSpdFild = CreateBus(Det_5msCtrlVehSpdFild, DeList);
clear DeList;


Bbc_CtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BLS'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
Bbc_CtrlEemFailData = CreateBus(Bbc_CtrlEemFailData, DeList);
clear DeList;

Bbc_CtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'GearSelDisp'
    'TcuSwiGs'
    };
Bbc_CtrlCanRxIdbInfo = CreateBus(Bbc_CtrlCanRxIdbInfo, DeList);
clear DeList;

Bbc_CtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'PanicBrkStFlg'
    'PedlReldStFlg1'
    'PedlReldStFlg2'
    };
Bbc_CtrlBrkPedlStatusInfo = CreateBus(Bbc_CtrlBrkPedlStatusInfo, DeList);
clear DeList;

Bbc_CtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild_1_100Bar'
    };
Bbc_CtrlPedlSimrPFildInfo = CreateBus(Bbc_CtrlPedlSimrPFildInfo, DeList);
clear DeList;

Bbc_CtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate'
    'PedlTrvlRateMilliMtrPerSec'
    };
Bbc_CtrlPedlTrvlRateInfo = CreateBus(Bbc_CtrlPedlTrvlRateInfo, DeList);
clear DeList;

Bbc_CtrlVehStopStInfo = Simulink.Bus;
DeList={
    'VehStopStFlg'
    };
Bbc_CtrlVehStopStInfo = CreateBus(Bbc_CtrlVehStopStInfo, DeList);
clear DeList;

Bbc_CtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    };
Bbc_CtrlEemSuspectData = CreateBus(Bbc_CtrlEemSuspectData, DeList);
clear DeList;

Bbc_CtrlEcuModeSts = Simulink.Bus;
DeList={'Bbc_CtrlEcuModeSts'};
Bbc_CtrlEcuModeSts = CreateBus(Bbc_CtrlEcuModeSts, DeList);
clear DeList;

Bbc_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Bbc_CtrlIgnOnOffSts'};
Bbc_CtrlIgnOnOffSts = CreateBus(Bbc_CtrlIgnOnOffSts, DeList);
clear DeList;

Bbc_CtrlFuncInhibitBbcSts = Simulink.Bus;
DeList={'Bbc_CtrlFuncInhibitBbcSts'};
Bbc_CtrlFuncInhibitBbcSts = CreateBus(Bbc_CtrlFuncInhibitBbcSts, DeList);
clear DeList;

Bbc_CtrlBlsFild = Simulink.Bus;
DeList={'Bbc_CtrlBlsFild'};
Bbc_CtrlBlsFild = CreateBus(Bbc_CtrlBlsFild, DeList);
clear DeList;

Bbc_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Bbc_CtrlBrkPRednForBaseBrkCtrlr'};
Bbc_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Bbc_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Bbc_CtrlFlexBrkSwtFild = Simulink.Bus;
DeList={'Bbc_CtrlFlexBrkSwtFild'};
Bbc_CtrlFlexBrkSwtFild = CreateBus(Bbc_CtrlFlexBrkSwtFild, DeList);
clear DeList;

Bbc_CtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Bbc_CtrlPedlTrvlFinal'};
Bbc_CtrlPedlTrvlFinal = CreateBus(Bbc_CtrlPedlTrvlFinal, DeList);
clear DeList;

Bbc_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Bbc_CtrlRgnBrkCtrlrActStFlg'};
Bbc_CtrlRgnBrkCtrlrActStFlg = CreateBus(Bbc_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

Bbc_CtrlVehSpdFild = Simulink.Bus;
DeList={'Bbc_CtrlVehSpdFild'};
Bbc_CtrlVehSpdFild = CreateBus(Bbc_CtrlVehSpdFild, DeList);
clear DeList;

Bbc_CtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'LowSpdCtrlInhibitFlg'
    'StopdVehCtrlModFlg'
    'VehStandStillStFlg'
    };
Bbc_CtrlBaseBrkCtrlModInfo = CreateBus(Bbc_CtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Bbc_CtrlBBCCtrlInfo = Simulink.Bus;
DeList={
    'BBCCtrlSt'
    };
Bbc_CtrlBBCCtrlInfo = CreateBus(Bbc_CtrlBBCCtrlInfo, DeList);
clear DeList;

Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo = Simulink.Bus;
DeList={
    'PedlSimrPGendOnFlg'
    };
Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo = CreateBus(Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo, DeList);
clear DeList;

Bbc_CtrlBaseBrkCtrlrActFlg = Simulink.Bus;
DeList={'Bbc_CtrlBaseBrkCtrlrActFlg'};
Bbc_CtrlBaseBrkCtrlrActFlg = CreateBus(Bbc_CtrlBaseBrkCtrlrActFlg, DeList);
clear DeList;

Bbc_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Bbc_CtrlTarPFromBaseBrkCtrlr'};
Bbc_CtrlTarPFromBaseBrkCtrlr = CreateBus(Bbc_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Bbc_CtrlTarPRateFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Bbc_CtrlTarPRateFromBaseBrkCtrlr'};
Bbc_CtrlTarPRateFromBaseBrkCtrlr = CreateBus(Bbc_CtrlTarPRateFromBaseBrkCtrlr, DeList);
clear DeList;

Bbc_CtrlTarPFromBrkPedl = Simulink.Bus;
DeList={'Bbc_CtrlTarPFromBrkPedl'};
Bbc_CtrlTarPFromBrkPedl = CreateBus(Bbc_CtrlTarPFromBrkPedl, DeList);
clear DeList;


Rbc_CtrlCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
Rbc_CtrlCanRxRegenInfo = CreateBus(Rbc_CtrlCanRxRegenInfo, DeList);
clear DeList;

Rbc_CtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Rbc_CtrlCanRxAccelPedlInfo = CreateBus(Rbc_CtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Rbc_CtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
Rbc_CtrlCanRxIdbInfo = CreateBus(Rbc_CtrlCanRxIdbInfo, DeList);
clear DeList;

Rbc_CtrlCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Rbc_CtrlCanRxEngTempInfo = CreateBus(Rbc_CtrlCanRxEngTempInfo, DeList);
clear DeList;

Rbc_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Rbc_CtrlAbsCtrlInfo = CreateBus(Rbc_CtrlAbsCtrlInfo, DeList);
clear DeList;

Rbc_CtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Rbc_CtrlAvhCtrlInfo = CreateBus(Rbc_CtrlAvhCtrlInfo, DeList);
clear DeList;

Rbc_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    };
Rbc_CtrlEscCtrlInfo = CreateBus(Rbc_CtrlEscCtrlInfo, DeList);
clear DeList;

Rbc_CtrlSccCtrlInfo = Simulink.Bus;
DeList={
    'SccActFlg'
    'SccDefectFlg'
    'SccTarP'
    };
Rbc_CtrlSccCtrlInfo = CreateBus(Rbc_CtrlSccCtrlInfo, DeList);
clear DeList;

Rbc_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    };
Rbc_CtrlTcsCtrlInfo = CreateBus(Rbc_CtrlTcsCtrlInfo, DeList);
clear DeList;

Rbc_CtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild_1_100Bar'
    };
Rbc_CtrlCircPFildInfo = CreateBus(Rbc_CtrlCircPFildInfo, DeList);
clear DeList;

Rbc_CtrlPedlTrvlFildInfo = Simulink.Bus;
DeList={
    'PdtFild'
    };
Rbc_CtrlPedlTrvlFildInfo = CreateBus(Rbc_CtrlPedlTrvlFildInfo, DeList);
clear DeList;

Rbc_CtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRateMilliMtrPerSec'
    };
Rbc_CtrlPedlTrvlRateInfo = CreateBus(Rbc_CtrlPedlTrvlRateInfo, DeList);
clear DeList;

Rbc_CtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild_1_100Bar'
    };
Rbc_CtrlPistPFildInfo = CreateBus(Rbc_CtrlPistPFildInfo, DeList);
clear DeList;

Rbc_CtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
Rbc_CtrlEemFailData = CreateBus(Rbc_CtrlEemFailData, DeList);
clear DeList;

Rbc_CtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    };
Rbc_CtrlEemSuspectData = CreateBus(Rbc_CtrlEemSuspectData, DeList);
clear DeList;

Rbc_CtrlEcuModeSts = Simulink.Bus;
DeList={'Rbc_CtrlEcuModeSts'};
Rbc_CtrlEcuModeSts = CreateBus(Rbc_CtrlEcuModeSts, DeList);
clear DeList;

Rbc_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Rbc_CtrlIgnOnOffSts'};
Rbc_CtrlIgnOnOffSts = CreateBus(Rbc_CtrlIgnOnOffSts, DeList);
clear DeList;

Rbc_CtrlPCtrlAct = Simulink.Bus;
DeList={'Rbc_CtrlPCtrlAct'};
Rbc_CtrlPCtrlAct = CreateBus(Rbc_CtrlPCtrlAct, DeList);
clear DeList;

Rbc_CtrlFuncInhibitRbcSts = Simulink.Bus;
DeList={'Rbc_CtrlFuncInhibitRbcSts'};
Rbc_CtrlFuncInhibitRbcSts = CreateBus(Rbc_CtrlFuncInhibitRbcSts, DeList);
clear DeList;

Rbc_CtrlAy = Simulink.Bus;
DeList={'Rbc_CtrlAy'};
Rbc_CtrlAy = CreateBus(Rbc_CtrlAy, DeList);
clear DeList;

Rbc_CtrlBlsFild = Simulink.Bus;
DeList={'Rbc_CtrlBlsFild'};
Rbc_CtrlBlsFild = CreateBus(Rbc_CtrlBlsFild, DeList);
clear DeList;

Rbc_CtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Rbc_CtrlPedlTrvlFinal'};
Rbc_CtrlPedlTrvlFinal = CreateBus(Rbc_CtrlPedlTrvlFinal, DeList);
clear DeList;

Rbc_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Rbc_CtrlTarPFromBaseBrkCtrlr'};
Rbc_CtrlTarPFromBaseBrkCtrlr = CreateBus(Rbc_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Rbc_CtrlTarPRateFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Rbc_CtrlTarPRateFromBaseBrkCtrlr'};
Rbc_CtrlTarPRateFromBaseBrkCtrlr = CreateBus(Rbc_CtrlTarPRateFromBaseBrkCtrlr, DeList);
clear DeList;

Rbc_CtrlTarPFromBrkPedl = Simulink.Bus;
DeList={'Rbc_CtrlTarPFromBrkPedl'};
Rbc_CtrlTarPFromBrkPedl = CreateBus(Rbc_CtrlTarPFromBrkPedl, DeList);
clear DeList;

Rbc_CtrlVehSpdFild = Simulink.Bus;
DeList={'Rbc_CtrlVehSpdFild'};
Rbc_CtrlVehSpdFild = CreateBus(Rbc_CtrlVehSpdFild, DeList);
clear DeList;

Rbc_CtrlRgnBrkCoopWithAbsInfo = Simulink.Bus;
DeList={
    'FrntSlipDetThdSlip'
    'FrntSlipDetThdUDiff'
    };
Rbc_CtrlRgnBrkCoopWithAbsInfo = CreateBus(Rbc_CtrlRgnBrkCoopWithAbsInfo, DeList);
clear DeList;

Rbc_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Rbc_CtrlBrkPRednForBaseBrkCtrlr'};
Rbc_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Rbc_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Rbc_CtrlRgnBrkCtlrBlendgFlg = Simulink.Bus;
DeList={'Rbc_CtrlRgnBrkCtlrBlendgFlg'};
Rbc_CtrlRgnBrkCtlrBlendgFlg = CreateBus(Rbc_CtrlRgnBrkCtlrBlendgFlg, DeList);
clear DeList;

Rbc_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Rbc_CtrlRgnBrkCtrlrActStFlg'};
Rbc_CtrlRgnBrkCtrlrActStFlg = CreateBus(Rbc_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;


Abc_CtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Abc_CtrlEemFailData = CreateBus(Abc_CtrlEemFailData, DeList);
clear DeList;

Abc_CtrlEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Abc_CtrlEemCtrlInhibitData = CreateBus(Abc_CtrlEemCtrlInhibitData, DeList);
clear DeList;

Abc_CtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Abc_CtrlCanRxAccelPedlInfo = CreateBus(Abc_CtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Abc_CtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    };
Abc_CtrlCanRxIdbInfo = CreateBus(Abc_CtrlCanRxIdbInfo, DeList);
clear DeList;

Abc_CtrlCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Abc_CtrlCanRxEngTempInfo = CreateBus(Abc_CtrlCanRxEngTempInfo, DeList);
clear DeList;

Abc_CtrlCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
Abc_CtrlCanRxEscInfo = CreateBus(Abc_CtrlCanRxEscInfo, DeList);
clear DeList;

Abc_CtrlEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    };
Abc_CtrlEscSwtStInfo = CreateBus(Abc_CtrlEscSwtStInfo, DeList);
clear DeList;

Abc_CtrlWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Abc_CtrlWhlSpdInfo = CreateBus(Abc_CtrlWhlSpdInfo, DeList);
clear DeList;

Abc_CtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'DrvrIntendToRelsBrk'
    };
Abc_CtrlBrkPedlStatusInfo = CreateBus(Abc_CtrlBrkPedlStatusInfo, DeList);
clear DeList;

Abc_CtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild_1_100Bar'
    };
Abc_CtrlCircPFildInfo = CreateBus(Abc_CtrlCircPFildInfo, DeList);
clear DeList;

Abc_CtrlCircPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PrimCircPOffsCorrd'
    'SecdCircPOffsCorrd'
    };
Abc_CtrlCircPOffsCorrdInfo = CreateBus(Abc_CtrlCircPOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlEstimdWhlPInfo = Simulink.Bus;
DeList={
    'FrntLeEstimdWhlP'
    'FrntRiEstimdWhlP'
    'ReLeEstimdWhlP'
    'ReRiEstimdWhlP'
    };
Abc_CtrlEstimdWhlPInfo = CreateBus(Abc_CtrlEstimdWhlPInfo, DeList);
clear DeList;

Abc_CtrlPedlSimrPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PedlSimrPOffsCorrd'
    };
Abc_CtrlPedlSimrPOffsCorrdInfo = CreateBus(Abc_CtrlPedlSimrPOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlPedlTrvlOffsCorrdInfo = Simulink.Bus;
DeList={
    'PdfRawOffsCorrd'
    'PdtRawOffsCorrd'
    };
Abc_CtrlPedlTrvlOffsCorrdInfo = CreateBus(Abc_CtrlPedlTrvlOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild_1_100Bar'
    };
Abc_CtrlPistPFildInfo = CreateBus(Abc_CtrlPistPFildInfo, DeList);
clear DeList;

Abc_CtrlPistPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PistPOffsCorrd'
    };
Abc_CtrlPistPOffsCorrdInfo = CreateBus(Abc_CtrlPistPOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlRgnBrkCoopWithAbsInfo = Simulink.Bus;
DeList={
    'FrntSlipDetThdSlip'
    'FrntSlipDetThdUDiff'
    };
Abc_CtrlRgnBrkCoopWithAbsInfo = CreateBus(Abc_CtrlRgnBrkCoopWithAbsInfo, DeList);
clear DeList;

Abc_CtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'RecommendStkRcvrLvl'
    'StkRecvryCtrlState'
    };
Abc_CtrlStkRecvryActnIfInfo = CreateBus(Abc_CtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Abc_CtrlMuxCmdExecStInfo = Simulink.Bus;
DeList={
    'MuxCmdExecStOfWhlFrntLe'
    'MuxCmdExecStOfWhlFrntRi'
    'MuxCmdExecStOfWhlReLe'
    'MuxCmdExecStOfWhlReRi'
    };
Abc_CtrlMuxCmdExecStInfo = CreateBus(Abc_CtrlMuxCmdExecStInfo, DeList);
clear DeList;

Abc_CtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
Abc_CtrlEemSuspectData = CreateBus(Abc_CtrlEemSuspectData, DeList);
clear DeList;

Abc_CtrlEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
Abc_CtrlEemEceData = CreateBus(Abc_CtrlEemEceData, DeList);
clear DeList;

Abc_CtrlFinalTarPInfo = Simulink.Bus;
DeList={
    'FinalMaxCircuitTp'
    };
Abc_CtrlFinalTarPInfo = CreateBus(Abc_CtrlFinalTarPInfo, DeList);
clear DeList;

Abc_CtrlFSBbsVlvInitEndFlg = Simulink.Bus;
DeList={'Abc_CtrlFSBbsVlvInitEndFlg'};
Abc_CtrlFSBbsVlvInitEndFlg = CreateBus(Abc_CtrlFSBbsVlvInitEndFlg, DeList);
clear DeList;

Abc_CtrlEcuModeSts = Simulink.Bus;
DeList={'Abc_CtrlEcuModeSts'};
Abc_CtrlEcuModeSts = CreateBus(Abc_CtrlEcuModeSts, DeList);
clear DeList;

Abc_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Abc_CtrlIgnOnOffSts'};
Abc_CtrlIgnOnOffSts = CreateBus(Abc_CtrlIgnOnOffSts, DeList);
clear DeList;

Abc_CtrlFSEscVlvInitEndFlg = Simulink.Bus;
DeList={'Abc_CtrlFSEscVlvInitEndFlg'};
Abc_CtrlFSEscVlvInitEndFlg = CreateBus(Abc_CtrlFSEscVlvInitEndFlg, DeList);
clear DeList;

Abc_CtrlCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'Abc_CtrlCanRxGearSelDispErrInfo'};
Abc_CtrlCanRxGearSelDispErrInfo = CreateBus(Abc_CtrlCanRxGearSelDispErrInfo, DeList);
clear DeList;

Abc_CtrlPCtrlAct = Simulink.Bus;
DeList={'Abc_CtrlPCtrlAct'};
Abc_CtrlPCtrlAct = CreateBus(Abc_CtrlPCtrlAct, DeList);
clear DeList;

Abc_CtrlBlsSwt = Simulink.Bus;
DeList={'Abc_CtrlBlsSwt'};
Abc_CtrlBlsSwt = CreateBus(Abc_CtrlBlsSwt, DeList);
clear DeList;

Abc_CtrlEscSwt = Simulink.Bus;
DeList={'Abc_CtrlEscSwt'};
Abc_CtrlEscSwt = CreateBus(Abc_CtrlEscSwt, DeList);
clear DeList;

Abc_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Abc_CtrlBrkPRednForBaseBrkCtrlr'};
Abc_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Abc_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Abc_CtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Abc_CtrlPedlTrvlFinal'};
Abc_CtrlPedlTrvlFinal = CreateBus(Abc_CtrlPedlTrvlFinal, DeList);
clear DeList;

Abc_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Abc_CtrlRgnBrkCtrlrActStFlg'};
Abc_CtrlRgnBrkCtrlrActStFlg = CreateBus(Abc_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

Abc_CtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Abc_CtrlFinalTarPFromPCtrl'};
Abc_CtrlFinalTarPFromPCtrl = CreateBus(Abc_CtrlFinalTarPFromPCtrl, DeList);
clear DeList;

Abc_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Abc_CtrlTarPFromBaseBrkCtrlr'};
Abc_CtrlTarPFromBaseBrkCtrlr = CreateBus(Abc_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Abc_CtrlTarPFromBrkPedl = Simulink.Bus;
DeList={'Abc_CtrlTarPFromBrkPedl'};
Abc_CtrlTarPFromBrkPedl = CreateBus(Abc_CtrlTarPFromBrkPedl, DeList);
clear DeList;

Abc_CtrlMTRInitEndFlg = Simulink.Bus;
DeList={'Abc_CtrlMTRInitEndFlg'};
Abc_CtrlMTRInitEndFlg = CreateBus(Abc_CtrlMTRInitEndFlg, DeList);
clear DeList;

Abc_CtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    };
Abc_CtrlWhlVlvReqAbcInfo = CreateBus(Abc_CtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Abc_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
Abc_CtrlAbsCtrlInfo = CreateBus(Abc_CtrlAbsCtrlInfo, DeList);
clear DeList;

Abc_CtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Abc_CtrlAvhCtrlInfo = CreateBus(Abc_CtrlAvhCtrlInfo, DeList);
clear DeList;

Abc_CtrlBaCtrlInfo = Simulink.Bus;
DeList={
    'BaActFlg'
    'BaCDefectFlg'
    'BaTarP'
    };
Abc_CtrlBaCtrlInfo = CreateBus(Abc_CtrlBaCtrlInfo, DeList);
clear DeList;

Abc_CtrlEbdCtrlInfo = Simulink.Bus;
DeList={
    'EbdActFlg'
    'EbdDefectFlg'
    'EbdTarPFrntLe'
    'EbdTarPFrntRi'
    'EbdTarPReLe'
    'EbdTarPReRi'
    'EbdTarPRateReLe'
    'EbdTarPRateReRi'
    'EbdCtrlModeReLe'
    'EbdCtrlModeReRi'
    'EbdDelTarPReLe'
    'EbdDelTarPReRi'
    };
Abc_CtrlEbdCtrlInfo = CreateBus(Abc_CtrlEbdCtrlInfo, DeList);
clear DeList;

Abc_CtrlEbpCtrlInfo = Simulink.Bus;
DeList={
    'EbpActFlg'
    'EbpDefectFlg'
    'EbpTarP'
    };
Abc_CtrlEbpCtrlInfo = CreateBus(Abc_CtrlEbpCtrlInfo, DeList);
clear DeList;

Abc_CtrlEpbiCtrlInfo = Simulink.Bus;
DeList={
    'EpbiActFlg'
    'EpbiDefectFlg'
    'EpbiTarP'
    };
Abc_CtrlEpbiCtrlInfo = CreateBus(Abc_CtrlEpbiCtrlInfo, DeList);
clear DeList;

Abc_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    'EscCtrlModeFrntLe'
    'EscCtrlModeFrntRi'
    'EscCtrlModeReLe'
    'EscCtrlModeReRi'
    'EscTarPRateFrntLe'
    'EscTarPRateFrntRi'
    'EscTarPRateReLe'
    'EscTarPRateReRi'
    'EscPrioFrntLe'
    'EscPrioFrntRi'
    'EscPrioReLe'
    'EscPrioReRi'
    'EscPreCtrlModeFrntLe'
    'EscPreCtrlModeFrntRi'
    'EscPreCtrlModeReLe'
    'EscPreCtrlModeReRi'
    'EscDelTarPFrntLe'
    'EscDelTarPFrntRi'
    'EscDelTarPReLe'
    'EscDelTarPReRi'
    };
Abc_CtrlEscCtrlInfo = CreateBus(Abc_CtrlEscCtrlInfo, DeList);
clear DeList;

Abc_CtrlHdcCtrlInfo = Simulink.Bus;
DeList={
    'HdcActFlg'
    'HdcDefectFlg'
    'HdcTarP'
    };
Abc_CtrlHdcCtrlInfo = CreateBus(Abc_CtrlHdcCtrlInfo, DeList);
clear DeList;

Abc_CtrlHsaCtrlInfo = Simulink.Bus;
DeList={
    'HsaActFlg'
    'HsaDefectFlg'
    'HsaTarP'
    };
Abc_CtrlHsaCtrlInfo = CreateBus(Abc_CtrlHsaCtrlInfo, DeList);
clear DeList;

Abc_CtrlSccCtrlInfo = Simulink.Bus;
DeList={
    'SccActFlg'
    'SccDefectFlg'
    'SccTarP'
    };
Abc_CtrlSccCtrlInfo = CreateBus(Abc_CtrlSccCtrlInfo, DeList);
clear DeList;

Abc_CtrlStkRecvryCtrlIfInfo = Simulink.Bus;
DeList={
    'StkRcvrEscAllowedTime'
    'StkRcvrAbsAllowedTime'
    'StkRcvrTcsAllowedTime'
    };
Abc_CtrlStkRecvryCtrlIfInfo = CreateBus(Abc_CtrlStkRecvryCtrlIfInfo, DeList);
clear DeList;

Abc_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    'BrkCtrlFctFrntLeByWspc'
    'BrkCtrlFctFrntRiByWspc'
    'BrkCtrlFctReLeByWspc'
    'BrkCtrlFctReRiByWspc'
    'BrkTqGrdtReqFrntLeByWspc'
    'BrkTqGrdtReqFrntRiByWspc'
    'BrkTqGrdtReqReLeByWspc'
    'BrkTqGrdtReqReRiByWspc'
    'TcsDelTarPFrntLe'
    'TcsDelTarPFrntRi'
    'TcsDelTarPReLe'
    'TcsDelTarPReRi'
    };
Abc_CtrlTcsCtrlInfo = CreateBus(Abc_CtrlTcsCtrlInfo, DeList);
clear DeList;

Abc_CtrlTvbbCtrlInfo = Simulink.Bus;
DeList={
    'TvbbActFlg'
    'TvbbDefectFlg'
    'TvbbTarP'
    };
Abc_CtrlTvbbCtrlInfo = CreateBus(Abc_CtrlTvbbCtrlInfo, DeList);
clear DeList;

Abc_CtrlActvBrkCtrlrActFlg = Simulink.Bus;
DeList={'Abc_CtrlActvBrkCtrlrActFlg'};
Abc_CtrlActvBrkCtrlrActFlg = CreateBus(Abc_CtrlActvBrkCtrlrActFlg, DeList);
clear DeList;

Abc_CtrlAy = Simulink.Bus;
DeList={'Abc_CtrlAy'};
Abc_CtrlAy = CreateBus(Abc_CtrlAy, DeList);
clear DeList;

Abc_CtrlVehSpd = Simulink.Bus;
DeList={'Abc_CtrlVehSpd'};
Abc_CtrlVehSpd = CreateBus(Abc_CtrlVehSpd, DeList);
clear DeList;


Arb_CtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    };
Arb_CtrlWhlVlvReqAbcInfo = CreateBus(Arb_CtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Arb_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
Arb_CtrlAbsCtrlInfo = CreateBus(Arb_CtrlAbsCtrlInfo, DeList);
clear DeList;

Arb_CtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Arb_CtrlAvhCtrlInfo = CreateBus(Arb_CtrlAvhCtrlInfo, DeList);
clear DeList;

Arb_CtrlBaCtrlInfo = Simulink.Bus;
DeList={
    'BaActFlg'
    'BaCDefectFlg'
    'BaTarP'
    };
Arb_CtrlBaCtrlInfo = CreateBus(Arb_CtrlBaCtrlInfo, DeList);
clear DeList;

Arb_CtrlEbdCtrlInfo = Simulink.Bus;
DeList={
    'EbdActFlg'
    'EbdDefectFlg'
    'EbdTarPFrntLe'
    'EbdTarPFrntRi'
    'EbdTarPReLe'
    'EbdTarPReRi'
    'EbdTarPRateReLe'
    'EbdTarPRateReRi'
    'EbdCtrlModeReLe'
    'EbdCtrlModeReRi'
    'EbdDelTarPReLe'
    'EbdDelTarPReRi'
    };
Arb_CtrlEbdCtrlInfo = CreateBus(Arb_CtrlEbdCtrlInfo, DeList);
clear DeList;

Arb_CtrlEbpCtrlInfo = Simulink.Bus;
DeList={
    'EbpActFlg'
    'EbpDefectFlg'
    'EbpTarP'
    };
Arb_CtrlEbpCtrlInfo = CreateBus(Arb_CtrlEbpCtrlInfo, DeList);
clear DeList;

Arb_CtrlEpbiCtrlInfo = Simulink.Bus;
DeList={
    'EpbiActFlg'
    'EpbiDefectFlg'
    'EpbiTarP'
    };
Arb_CtrlEpbiCtrlInfo = CreateBus(Arb_CtrlEpbiCtrlInfo, DeList);
clear DeList;

Arb_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    'EscCtrlModeFrntLe'
    'EscCtrlModeFrntRi'
    'EscCtrlModeReLe'
    'EscCtrlModeReRi'
    'EscTarPRateFrntLe'
    'EscTarPRateFrntRi'
    'EscTarPRateReLe'
    'EscTarPRateReRi'
    'EscPrioFrntLe'
    'EscPrioFrntRi'
    'EscPrioReLe'
    'EscPrioReRi'
    'EscPreCtrlModeFrntLe'
    'EscPreCtrlModeFrntRi'
    'EscPreCtrlModeReLe'
    'EscPreCtrlModeReRi'
    'EscDelTarPFrntLe'
    'EscDelTarPFrntRi'
    'EscDelTarPReLe'
    'EscDelTarPReRi'
    };
Arb_CtrlEscCtrlInfo = CreateBus(Arb_CtrlEscCtrlInfo, DeList);
clear DeList;

Arb_CtrlHsaCtrlInfo = Simulink.Bus;
DeList={
    'HsaActFlg'
    'HsaDefectFlg'
    'HsaTarP'
    };
Arb_CtrlHsaCtrlInfo = CreateBus(Arb_CtrlHsaCtrlInfo, DeList);
clear DeList;

Arb_CtrlSccCtrlInfo = Simulink.Bus;
DeList={
    'SccActFlg'
    'SccDefectFlg'
    'SccTarP'
    };
Arb_CtrlSccCtrlInfo = CreateBus(Arb_CtrlSccCtrlInfo, DeList);
clear DeList;

Arb_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    'BrkCtrlFctFrntLeByWspc'
    'BrkCtrlFctFrntRiByWspc'
    'BrkCtrlFctReLeByWspc'
    'BrkCtrlFctReRiByWspc'
    'BrkTqGrdtReqFrntLeByWspc'
    'BrkTqGrdtReqFrntRiByWspc'
    'BrkTqGrdtReqReLeByWspc'
    'BrkTqGrdtReqReRiByWspc'
    'TcsDelTarPFrntLe'
    'TcsDelTarPFrntRi'
    'TcsDelTarPReLe'
    'TcsDelTarPReRi'
    };
Arb_CtrlTcsCtrlInfo = CreateBus(Arb_CtrlTcsCtrlInfo, DeList);
clear DeList;

Arb_CtrlTvbbCtrlInfo = Simulink.Bus;
DeList={
    'TvbbActFlg'
    'TvbbDefectFlg'
    'TvbbTarP'
    };
Arb_CtrlTvbbCtrlInfo = CreateBus(Arb_CtrlTvbbCtrlInfo, DeList);
clear DeList;

Arb_CtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
Arb_CtrlBaseBrkCtrlModInfo = CreateBus(Arb_CtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Arb_CtrlBBCCtrlInfo = Simulink.Bus;
DeList={
    'BBCCtrlSt'
    };
Arb_CtrlBBCCtrlInfo = CreateBus(Arb_CtrlBBCCtrlInfo, DeList);
clear DeList;

Arb_CtrlEcuModeSts = Simulink.Bus;
DeList={'Arb_CtrlEcuModeSts'};
Arb_CtrlEcuModeSts = CreateBus(Arb_CtrlEcuModeSts, DeList);
clear DeList;

Arb_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Arb_CtrlIgnOnOffSts'};
Arb_CtrlIgnOnOffSts = CreateBus(Arb_CtrlIgnOnOffSts, DeList);
clear DeList;

Arb_CtrlFuncInhibitArbiSts = Simulink.Bus;
DeList={'Arb_CtrlFuncInhibitArbiSts'};
Arb_CtrlFuncInhibitArbiSts = CreateBus(Arb_CtrlFuncInhibitArbiSts, DeList);
clear DeList;

Arb_CtrlBaseBrkCtrlrActFlg = Simulink.Bus;
DeList={'Arb_CtrlBaseBrkCtrlrActFlg'};
Arb_CtrlBaseBrkCtrlrActFlg = CreateBus(Arb_CtrlBaseBrkCtrlrActFlg, DeList);
clear DeList;

Arb_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Arb_CtrlBrkPRednForBaseBrkCtrlr'};
Arb_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Arb_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Arb_CtrlPCtrlSt = Simulink.Bus;
DeList={'Arb_CtrlPCtrlSt'};
Arb_CtrlPCtrlSt = CreateBus(Arb_CtrlPCtrlSt, DeList);
clear DeList;

Arb_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Arb_CtrlRgnBrkCtrlrActStFlg'};
Arb_CtrlRgnBrkCtrlrActStFlg = CreateBus(Arb_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

Arb_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Arb_CtrlTarPFromBaseBrkCtrlr'};
Arb_CtrlTarPFromBaseBrkCtrlr = CreateBus(Arb_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Arb_CtrlTarPRateFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Arb_CtrlTarPRateFromBaseBrkCtrlr'};
Arb_CtrlTarPRateFromBaseBrkCtrlr = CreateBus(Arb_CtrlTarPRateFromBaseBrkCtrlr, DeList);
clear DeList;

Arb_CtrlVehSpdFild = Simulink.Bus;
DeList={'Arb_CtrlVehSpdFild'};
Arb_CtrlVehSpdFild = CreateBus(Arb_CtrlVehSpdFild, DeList);
clear DeList;

Arb_CtrlFinalTarPInfo = Simulink.Bus;
DeList={
    'FinalTarPOfWhlFrntLe'
    'FinalTarPOfWhlFrntRi'
    'FinalTarPOfWhlReLe'
    'FinalTarPOfWhlReRi'
    'FinalTarPRateOfWhlFrntLe'
    'FinalTarPRateOfWhlFrntRi'
    'FinalTarPRateOfWhlReLe'
    'FinalTarPRateOfWhlReRi'
    'PCtrlPrioOfWhlFrntLe'
    'PCtrlPrioOfWhlFrntRi'
    'PCtrlPrioOfWhlReLe'
    'PCtrlPrioOfWhlReRi'
    'CtrlModOfWhlFrntLe'
    'CtrlModOfOfWhlFrntRi'
    'CtrlModOfWhlReLe'
    'CtrlModOfWhlReRi'
    'ReqdPCtrlBoostMod'
    'FinalDelTarPOfWhlFrntLe'
    'FinalDelTarPOfWhlFrntRi'
    'FinalDelTarPOfWhlReLe'
    'FinalDelTarPOfWhlReRi'
    'FinalMaxCircuitTp'
    };
Arb_CtrlFinalTarPInfo = CreateBus(Arb_CtrlFinalTarPInfo, DeList);
clear DeList;

Arb_CtrlWhlOutVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlOutVlvCtrlTar_array_0'
    'FlOutVlvCtrlTar_array_1'
    'FlOutVlvCtrlTar_array_2'
    'FlOutVlvCtrlTar_array_3'
    'FlOutVlvCtrlTar_array_4'
    'FrOutVlvCtrlTar_array_0'
    'FrOutVlvCtrlTar_array_1'
    'FrOutVlvCtrlTar_array_2'
    'FrOutVlvCtrlTar_array_3'
    'FrOutVlvCtrlTar_array_4'
    'RlOutVlvCtrlTar_array_0'
    'RlOutVlvCtrlTar_array_1'
    'RlOutVlvCtrlTar_array_2'
    'RlOutVlvCtrlTar_array_3'
    'RlOutVlvCtrlTar_array_4'
    'RrOutVlvCtrlTar_array_0'
    'RrOutVlvCtrlTar_array_1'
    'RrOutVlvCtrlTar_array_2'
    'RrOutVlvCtrlTar_array_3'
    'RrOutVlvCtrlTar_array_4'
    };
Arb_CtrlWhlOutVlvCtrlTarInfo = CreateBus(Arb_CtrlWhlOutVlvCtrlTarInfo, DeList);
clear DeList;

Arb_CtrlWhlPreFillInfo = Simulink.Bus;
DeList={
    'FlPreFillActFlg'
    'FrPreFillActFlg'
    'RlPreFillActFlg'
    'RrPreFillActFlg'
    };
Arb_CtrlWhlPreFillInfo = CreateBus(Arb_CtrlWhlPreFillInfo, DeList);
clear DeList;


Pct_5msCtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    };
Pct_5msCtrlWhlVlvReqAbcInfo = CreateBus(Pct_5msCtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Pct_5msCtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    };
Pct_5msCtrlEemFailData = CreateBus(Pct_5msCtrlEemFailData, DeList);
clear DeList;

Pct_5msCtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Pct_5msCtrlCanRxAccelPedlInfo = CreateBus(Pct_5msCtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Pct_5msCtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Pct_5msCtrlAbsCtrlInfo = CreateBus(Pct_5msCtrlAbsCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Pct_5msCtrlAvhCtrlInfo = CreateBus(Pct_5msCtrlAvhCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    };
Pct_5msCtrlEscCtrlInfo = CreateBus(Pct_5msCtrlEscCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlHsaCtrlInfo = Simulink.Bus;
DeList={
    'HsaActFlg'
    'HsaDefectFlg'
    'HsaTarP'
    };
Pct_5msCtrlHsaCtrlInfo = CreateBus(Pct_5msCtrlHsaCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlStkRecvryCtrlIfInfo = Simulink.Bus;
DeList={
    'StkRcvrEscAllowedTime'
    'StkRcvrAbsAllowedTime'
    'StkRcvrTcsAllowedTime'
    };
Pct_5msCtrlStkRecvryCtrlIfInfo = CreateBus(Pct_5msCtrlStkRecvryCtrlIfInfo, DeList);
clear DeList;

Pct_5msCtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    };
Pct_5msCtrlTcsCtrlInfo = CreateBus(Pct_5msCtrlTcsCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
Pct_5msCtrlBaseBrkCtrlModInfo = CreateBus(Pct_5msCtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Pct_5msCtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'DrvrIntendToRelsBrk'
    'PanicBrkStFlg'
    'PedlReldStFlg2'
    };
Pct_5msCtrlBrkPedlStatusInfo = CreateBus(Pct_5msCtrlBrkPedlStatusInfo, DeList);
clear DeList;

Pct_5msCtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild_1_100Bar'
    };
Pct_5msCtrlCircPFildInfo = CreateBus(Pct_5msCtrlCircPFildInfo, DeList);
clear DeList;

Pct_5msCtrlCircPRateInfo = Simulink.Bus;
DeList={
    'PrimCircPChgDurg10ms'
    'SecdCircPChgDurg10ms'
    };
Pct_5msCtrlCircPRateInfo = CreateBus(Pct_5msCtrlCircPRateInfo, DeList);
clear DeList;

Pct_5msCtrlEstimdWhlPInfo = Simulink.Bus;
DeList={
    'FrntLeEstimdWhlP'
    'FrntRiEstimdWhlP'
    'ReLeEstimdWhlP'
    'ReRiEstimdWhlP'
    };
Pct_5msCtrlEstimdWhlPInfo = CreateBus(Pct_5msCtrlEstimdWhlPInfo, DeList);
clear DeList;

Pct_5msCtrlFinalTarPInfo = Simulink.Bus;
DeList={
    'FinalTarPOfWhlFrntLe'
    'FinalTarPOfWhlFrntRi'
    'FinalTarPOfWhlReLe'
    'FinalTarPOfWhlReRi'
    'FinalTarPRateOfWhlFrntLe'
    'FinalTarPRateOfWhlFrntRi'
    'FinalTarPRateOfWhlReLe'
    'FinalTarPRateOfWhlReRi'
    'PCtrlPrioOfWhlFrntLe'
    'PCtrlPrioOfWhlFrntRi'
    'PCtrlPrioOfWhlReLe'
    'PCtrlPrioOfWhlReRi'
    'CtrlModOfWhlFrntLe'
    'CtrlModOfOfWhlFrntRi'
    'CtrlModOfWhlReLe'
    'CtrlModOfWhlReRi'
    'ReqdPCtrlBoostMod'
    'FinalDelTarPOfWhlFrntLe'
    'FinalDelTarPOfWhlFrntRi'
    'FinalDelTarPOfWhlReLe'
    'FinalDelTarPOfWhlReRi'
    };
Pct_5msCtrlFinalTarPInfo = CreateBus(Pct_5msCtrlFinalTarPInfo, DeList);
clear DeList;

Pct_5msCtrlWhlOutVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlOutVlvCtrlTar_array_0'
    'FlOutVlvCtrlTar_array_1'
    'FlOutVlvCtrlTar_array_2'
    'FlOutVlvCtrlTar_array_3'
    'FlOutVlvCtrlTar_array_4'
    'FrOutVlvCtrlTar_array_0'
    'FrOutVlvCtrlTar_array_1'
    'FrOutVlvCtrlTar_array_2'
    'FrOutVlvCtrlTar_array_3'
    'FrOutVlvCtrlTar_array_4'
    'RlOutVlvCtrlTar_array_0'
    'RlOutVlvCtrlTar_array_1'
    'RlOutVlvCtrlTar_array_2'
    'RlOutVlvCtrlTar_array_3'
    'RlOutVlvCtrlTar_array_4'
    'RrOutVlvCtrlTar_array_0'
    'RrOutVlvCtrlTar_array_1'
    'RrOutVlvCtrlTar_array_2'
    'RrOutVlvCtrlTar_array_3'
    'RrOutVlvCtrlTar_array_4'
    };
Pct_5msCtrlWhlOutVlvCtrlTarInfo = CreateBus(Pct_5msCtrlWhlOutVlvCtrlTarInfo, DeList);
clear DeList;

Pct_5msCtrlWhlPreFillInfo = Simulink.Bus;
DeList={
    'FlPreFillActFlg'
    'FrPreFillActFlg'
    'RlPreFillActFlg'
    'RrPreFillActFlg'
    };
Pct_5msCtrlWhlPreFillInfo = CreateBus(Pct_5msCtrlWhlPreFillInfo, DeList);
clear DeList;

Pct_5msCtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'VlvFadeoutEndOK'
    'CutVlvOpenFlg'
    };
Pct_5msCtrlIdbVlvActInfo = CreateBus(Pct_5msCtrlIdbVlvActInfo, DeList);
clear DeList;

Pct_5msCtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild_1_100Bar'
    };
Pct_5msCtrlPedlSimrPFildInfo = CreateBus(Pct_5msCtrlPedlSimrPFildInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRateMilliMtrPerSec'
    };
Pct_5msCtrlPedlTrvlRateInfo = CreateBus(Pct_5msCtrlPedlTrvlRateInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlTagInfo = Simulink.Bus;
DeList={
    'PedlSigTag'
    };
Pct_5msCtrlPedlTrvlTagInfo = CreateBus(Pct_5msCtrlPedlTrvlTagInfo, DeList);
clear DeList;

Pct_5msCtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild_1_100Bar'
    };
Pct_5msCtrlPistPFildInfo = CreateBus(Pct_5msCtrlPistPFildInfo, DeList);
clear DeList;

Pct_5msCtrlPistPRateInfo = Simulink.Bus;
DeList={
    'PistPChgDurg10ms'
    };
Pct_5msCtrlPistPRateInfo = CreateBus(Pct_5msCtrlPistPRateInfo, DeList);
clear DeList;

Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo = Simulink.Bus;
DeList={
    'PedlSimrPGendOnFlg'
    };
Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo = CreateBus(Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo, DeList);
clear DeList;

Pct_5msCtrlWhlSpdFildInfo = Simulink.Bus;
DeList={
    'WhlSpdFildReLe'
    'WhlSpdFildReRi'
    };
Pct_5msCtrlWhlSpdFildInfo = CreateBus(Pct_5msCtrlWhlSpdFildInfo, DeList);
clear DeList;

Pct_5msCtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_SimP'
    };
Pct_5msCtrlEemSuspectData = CreateBus(Pct_5msCtrlEemSuspectData, DeList);
clear DeList;

Pct_5msCtrlEcuModeSts = Simulink.Bus;
DeList={'Pct_5msCtrlEcuModeSts'};
Pct_5msCtrlEcuModeSts = CreateBus(Pct_5msCtrlEcuModeSts, DeList);
clear DeList;

Pct_5msCtrlFuncInhibitPctrlSts = Simulink.Bus;
DeList={'Pct_5msCtrlFuncInhibitPctrlSts'};
Pct_5msCtrlFuncInhibitPctrlSts = CreateBus(Pct_5msCtrlFuncInhibitPctrlSts, DeList);
clear DeList;

Pct_5msCtrlBaseBrkCtrlrActFlg = Simulink.Bus;
DeList={'Pct_5msCtrlBaseBrkCtrlrActFlg'};
Pct_5msCtrlBaseBrkCtrlrActFlg = CreateBus(Pct_5msCtrlBaseBrkCtrlrActFlg, DeList);
clear DeList;

Pct_5msCtrlBlsFild = Simulink.Bus;
DeList={'Pct_5msCtrlBlsFild'};
Pct_5msCtrlBlsFild = CreateBus(Pct_5msCtrlBlsFild, DeList);
clear DeList;

Pct_5msCtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Pct_5msCtrlBrkPRednForBaseBrkCtrlr'};
Pct_5msCtrlBrkPRednForBaseBrkCtrlr = CreateBus(Pct_5msCtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Pct_5msCtrlRgnBrkCtlrBlendgFlg = Simulink.Bus;
DeList={'Pct_5msCtrlRgnBrkCtlrBlendgFlg'};
Pct_5msCtrlRgnBrkCtlrBlendgFlg = CreateBus(Pct_5msCtrlRgnBrkCtlrBlendgFlg, DeList);
clear DeList;

Pct_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Pct_5msCtrlVehSpdFild'};
Pct_5msCtrlVehSpdFild = CreateBus(Pct_5msCtrlVehSpdFild, DeList);
clear DeList;

Pct_5msCtrlIdbPCtrllVlvCtrlStInfo = Simulink.Bus;
DeList={
    'CvVlvSt'
    'RlVlvSt'
    'PdVlvSt'
    'BalVlvSt'
    'CvVlvNoiseCtrlSt'
    'RlVlvNoiseCtrlSt'
    'PdVlvNoiseCtrlSt'
    'BalVlvNoiseCtrlSt'
    'CvVlvHoldTime'
    'RlVlvHoldTime'
    'PdVlvHoldTime'
    'BalVlvHoldTime'
    };
Pct_5msCtrlIdbPCtrllVlvCtrlStInfo = CreateBus(Pct_5msCtrlIdbPCtrllVlvCtrlStInfo, DeList);
clear DeList;

Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo = Simulink.Bus;
DeList={
    'PrimCutVlvSt'
    'SecdCutVlvSt'
    'SimVlvSt'
    'PrimCutVlvNoiseCtrlSt'
    'SecdCutVlvNoiseCtrlSt'
    'SimVlvNoiseCtrlSt'
    'PrimCutVlvHoldTime'
    'SecdCutVlvHoldTime'
    'SimVlvHoldTime'
    'CutVlvLongOpen'
    };
Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo = CreateBus(Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo, DeList);
clear DeList;

Pct_5msCtrlWhlnVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlInVlvCtrlTar_array_0'
    'FlInVlvCtrlTar_array_1'
    'FlInVlvCtrlTar_array_2'
    'FlInVlvCtrlTar_array_3'
    'FlInVlvCtrlTar_array_4'
    'FrInVlvCtrlTar_array_0'
    'FrInVlvCtrlTar_array_1'
    'FrInVlvCtrlTar_array_2'
    'FrInVlvCtrlTar_array_3'
    'FrInVlvCtrlTar_array_4'
    'RlInVlvCtrlTar_array_0'
    'RlInVlvCtrlTar_array_1'
    'RlInVlvCtrlTar_array_2'
    'RlInVlvCtrlTar_array_3'
    'RlInVlvCtrlTar_array_4'
    'RrInVlvCtrlTar_array_0'
    'RrInVlvCtrlTar_array_1'
    'RrInVlvCtrlTar_array_2'
    'RrInVlvCtrlTar_array_3'
    'RrInVlvCtrlTar_array_4'
    };
Pct_5msCtrlWhlnVlvCtrlTarInfo = CreateBus(Pct_5msCtrlWhlnVlvCtrlTarInfo, DeList);
clear DeList;

Pct_5msCtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'RecommendStkRcvrLvl'
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Pct_5msCtrlStkRecvryActnIfInfo = CreateBus(Pct_5msCtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Pct_5msCtrlMuxCmdExecStInfo = Simulink.Bus;
DeList={
    'MuxCmdExecStOfWhlFrntLe'
    'MuxCmdExecStOfWhlFrntRi'
    'MuxCmdExecStOfWhlReLe'
    'MuxCmdExecStOfWhlReRi'
    };
Pct_5msCtrlMuxCmdExecStInfo = CreateBus(Pct_5msCtrlMuxCmdExecStInfo, DeList);
clear DeList;

Pct_5msCtrlPCtrlAct = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlAct'};
Pct_5msCtrlPCtrlAct = CreateBus(Pct_5msCtrlPCtrlAct, DeList);
clear DeList;

Pct_5msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlBoostMod'};
Pct_5msCtrlPCtrlBoostMod = CreateBus(Pct_5msCtrlPCtrlBoostMod, DeList);
clear DeList;

Pct_5msCtrlPCtrlSt = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlSt'};
Pct_5msCtrlPCtrlSt = CreateBus(Pct_5msCtrlPCtrlSt, DeList);
clear DeList;

Pct_5msCtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Pct_5msCtrlFinalTarPFromPCtrl'};
Pct_5msCtrlFinalTarPFromPCtrl = CreateBus(Pct_5msCtrlFinalTarPFromPCtrl, DeList);
clear DeList;


Vat_CtrlWhlOutVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlOutVlvCtrlTar_array_0'
    'FlOutVlvCtrlTar_array_1'
    'FlOutVlvCtrlTar_array_2'
    'FlOutVlvCtrlTar_array_3'
    'FlOutVlvCtrlTar_array_4'
    'FrOutVlvCtrlTar_array_0'
    'FrOutVlvCtrlTar_array_1'
    'FrOutVlvCtrlTar_array_2'
    'FrOutVlvCtrlTar_array_3'
    'FrOutVlvCtrlTar_array_4'
    'RlOutVlvCtrlTar_array_0'
    'RlOutVlvCtrlTar_array_1'
    'RlOutVlvCtrlTar_array_2'
    'RlOutVlvCtrlTar_array_3'
    'RlOutVlvCtrlTar_array_4'
    'RrOutVlvCtrlTar_array_0'
    'RrOutVlvCtrlTar_array_1'
    'RrOutVlvCtrlTar_array_2'
    'RrOutVlvCtrlTar_array_3'
    'RrOutVlvCtrlTar_array_4'
    };
Vat_CtrlWhlOutVlvCtrlTarInfo = CreateBus(Vat_CtrlWhlOutVlvCtrlTarInfo, DeList);
clear DeList;

Vat_CtrlIdbPCtrllVlvCtrlStInfo = Simulink.Bus;
DeList={
    'CvVlvSt'
    'RlVlvSt'
    'PdVlvSt'
    'BalVlvSt'
    'CvVlvNoiseCtrlSt'
    'RlVlvNoiseCtrlSt'
    'PdVlvNoiseCtrlSt'
    'BalVlvNoiseCtrlSt'
    'CvVlvHoldTime'
    'RlVlvHoldTime'
    'PdVlvHoldTime'
    'BalVlvHoldTime'
    };
Vat_CtrlIdbPCtrllVlvCtrlStInfo = CreateBus(Vat_CtrlIdbPCtrllVlvCtrlStInfo, DeList);
clear DeList;

Vat_CtrlIdbPedlFeelVlvCtrlStInfo = Simulink.Bus;
DeList={
    'PrimCutVlvSt'
    'SecdCutVlvSt'
    'SimVlvSt'
    'PrimCutVlvNoiseCtrlSt'
    'SecdCutVlvNoiseCtrlSt'
    'SimVlvNoiseCtrlSt'
    'PrimCutVlvHoldTime'
    'SecdCutVlvHoldTime'
    'SimVlvHoldTime'
    'CutVlvLongOpen'
    };
Vat_CtrlIdbPedlFeelVlvCtrlStInfo = CreateBus(Vat_CtrlIdbPedlFeelVlvCtrlStInfo, DeList);
clear DeList;

Vat_CtrlWhlnVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlInVlvCtrlTar_array_0'
    'FlInVlvCtrlTar_array_1'
    'FlInVlvCtrlTar_array_2'
    'FlInVlvCtrlTar_array_3'
    'FlInVlvCtrlTar_array_4'
    'FrInVlvCtrlTar_array_0'
    'FrInVlvCtrlTar_array_1'
    'FrInVlvCtrlTar_array_2'
    'FrInVlvCtrlTar_array_3'
    'FrInVlvCtrlTar_array_4'
    'RlInVlvCtrlTar_array_0'
    'RlInVlvCtrlTar_array_1'
    'RlInVlvCtrlTar_array_2'
    'RlInVlvCtrlTar_array_3'
    'RlInVlvCtrlTar_array_4'
    'RrInVlvCtrlTar_array_0'
    'RrInVlvCtrlTar_array_1'
    'RrInVlvCtrlTar_array_2'
    'RrInVlvCtrlTar_array_3'
    'RrInVlvCtrlTar_array_4'
    };
Vat_CtrlWhlnVlvCtrlTarInfo = CreateBus(Vat_CtrlWhlnVlvCtrlTarInfo, DeList);
clear DeList;

Vat_CtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Vat_CtrlStkRecvryActnIfInfo = CreateBus(Vat_CtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo = Simulink.Bus;
DeList={
    'PedlSimrPGendOnFlg'
    };
Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo = CreateBus(Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo, DeList);
clear DeList;

Vat_CtrlEcuModeSts = Simulink.Bus;
DeList={'Vat_CtrlEcuModeSts'};
Vat_CtrlEcuModeSts = CreateBus(Vat_CtrlEcuModeSts, DeList);
clear DeList;

Vat_CtrlPCtrlAct = Simulink.Bus;
DeList={'Vat_CtrlPCtrlAct'};
Vat_CtrlPCtrlAct = CreateBus(Vat_CtrlPCtrlAct, DeList);
clear DeList;

Vat_CtrlFuncInhibitVlvActSts = Simulink.Bus;
DeList={'Vat_CtrlFuncInhibitVlvActSts'};
Vat_CtrlFuncInhibitVlvActSts = CreateBus(Vat_CtrlFuncInhibitVlvActSts, DeList);
clear DeList;

Vat_CtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Vat_CtrlPCtrlBoostMod'};
Vat_CtrlPCtrlBoostMod = CreateBus(Vat_CtrlPCtrlBoostMod, DeList);
clear DeList;

Vat_CtrlNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'RelsVlvReq'
    'CircVlvReq'
    'PressDumpVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    'CircVlvDataLen'
    'RelsVlvDataLen'
    'PressDumpVlvDataLen'
    };
Vat_CtrlNormVlvReqVlvActInfo = CreateBus(Vat_CtrlNormVlvReqVlvActInfo, DeList);
clear DeList;

Vat_CtrlWhlVlvReqIdbInfo = Simulink.Bus;
DeList={
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    };
Vat_CtrlWhlVlvReqIdbInfo = CreateBus(Vat_CtrlWhlVlvReqIdbInfo, DeList);
clear DeList;

Vat_CtrlIdbBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'SecdBalVlvReqData_array_0'
    'SecdBalVlvReqData_array_1'
    'SecdBalVlvReqData_array_2'
    'SecdBalVlvReqData_array_3'
    'SecdBalVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    'PrimBalVlvReq'
    'SecdBalVlvReq'
    'ChmbBalVlvReq'
    'PrimBalVlvDataLen'
    'SecdBalVlvDataLen'
    'ChmbBalVlvDataLen'
    };
Vat_CtrlIdbBalVlvReqInfo = CreateBus(Vat_CtrlIdbBalVlvReqInfo, DeList);
clear DeList;

Vat_CtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'VlvFadeoutEndOK'
    'FadeoutSt2EndOk'
    'CutVlvOpenFlg'
    };
Vat_CtrlIdbVlvActInfo = CreateBus(Vat_CtrlIdbVlvActInfo, DeList);
clear DeList;


SysPwrM_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
SysPwrM_MainWhlSpdInfo = CreateBus(SysPwrM_MainWhlSpdInfo, DeList);
clear DeList;

SysPwrM_MainRxClu2Info = Simulink.Bus;
DeList={
    'IGN_SW'
    };
SysPwrM_MainRxClu2Info = CreateBus(SysPwrM_MainRxClu2Info, DeList);
clear DeList;

SysPwrM_MainEcuModeSts = Simulink.Bus;
DeList={'SysPwrM_MainEcuModeSts'};
SysPwrM_MainEcuModeSts = CreateBus(SysPwrM_MainEcuModeSts, DeList);
clear DeList;

SysPwrM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SysPwrM_MainIgnOnOffSts'};
SysPwrM_MainIgnOnOffSts = CreateBus(SysPwrM_MainIgnOnOffSts, DeList);
clear DeList;

SysPwrM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SysPwrM_MainIgnEdgeSts'};
SysPwrM_MainIgnEdgeSts = CreateBus(SysPwrM_MainIgnEdgeSts, DeList);
clear DeList;

SysPwrM_MainArbVlvDriveState = Simulink.Bus;
DeList={'SysPwrM_MainArbVlvDriveState'};
SysPwrM_MainArbVlvDriveState = CreateBus(SysPwrM_MainArbVlvDriveState, DeList);
clear DeList;

SysPwrM_MainSysPwrMonData = Simulink.Bus;
DeList={
    'SysPwrM_OverVolt_1st_Err'
    'SysPwrM_OverVolt_2st_Err'
    'SysPwrM_UnderVolt_1st_Err'
    'SysPwrM_UnderVolt_2st_Err'
    'SysPwrM_UnderVolt_3st_Err'
    'SysPwrM_Asic_Vdd_Err'
    'SysPwrM_Asic_OverVolt_Err'
    'SysPwrM_Asic_UnderVolt_Err'
    'SysPwrM_Asic_OverTemp_Err'
    'SysPwrM_Asic_Gndloss_Err'
    'SysPwrM_Asic_Comm_Err'
    'SysPwrM_IgnLine_Open_Err'
    };
SysPwrM_MainSysPwrMonData = CreateBus(SysPwrM_MainSysPwrMonData, DeList);
clear DeList;


EcuHwCtrlM_MainEcuModeSts = Simulink.Bus;
DeList={'EcuHwCtrlM_MainEcuModeSts'};
EcuHwCtrlM_MainEcuModeSts = CreateBus(EcuHwCtrlM_MainEcuModeSts, DeList);
clear DeList;

EcuHwCtrlM_MainIgnOnOffSts = Simulink.Bus;
DeList={'EcuHwCtrlM_MainIgnOnOffSts'};
EcuHwCtrlM_MainIgnOnOffSts = CreateBus(EcuHwCtrlM_MainIgnOnOffSts, DeList);
clear DeList;

EcuHwCtrlM_MainIgnEdgeSts = Simulink.Bus;
DeList={'EcuHwCtrlM_MainIgnEdgeSts'};
EcuHwCtrlM_MainIgnEdgeSts = CreateBus(EcuHwCtrlM_MainIgnEdgeSts, DeList);
clear DeList;

EcuHwCtrlM_MainAsicMonFaultInfo = Simulink.Bus;
DeList={
    'ASICM_AsicOverVolt_Err'
    'ASICM_AsicOverVolt_Err_Info'
    'ASICM_AsicUnderVolt_Err'
    'ASICM_AsicUnderVolt_Err_Info'
    'ASICM_AsicOverTemp_Err'
    'ASICM_AsicOverTemp_Err_Info'
    'ASICM_AsicComm_Err'
    'ASICM_AsicOsc_Err'
    'ASICM_AsicNvm_Err'
    'ASICM_AsicGroundLoss_Err'
    };
EcuHwCtrlM_MainAsicMonFaultInfo = CreateBus(EcuHwCtrlM_MainAsicMonFaultInfo, DeList);
clear DeList;


WssM_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMin'
    };
WssM_MainWssSpeedOut = CreateBus(WssM_MainWssSpeedOut, DeList);
clear DeList;

WssM_MainEcuModeSts = Simulink.Bus;
DeList={'WssM_MainEcuModeSts'};
WssM_MainEcuModeSts = CreateBus(WssM_MainEcuModeSts, DeList);
clear DeList;

WssM_MainIgnOnOffSts = Simulink.Bus;
DeList={'WssM_MainIgnOnOffSts'};
WssM_MainIgnOnOffSts = CreateBus(WssM_MainIgnOnOffSts, DeList);
clear DeList;

WssM_MainIgnEdgeSts = Simulink.Bus;
DeList={'WssM_MainIgnEdgeSts'};
WssM_MainIgnEdgeSts = CreateBus(WssM_MainIgnEdgeSts, DeList);
clear DeList;

WssM_MainWssMonData = Simulink.Bus;
DeList={
    'WssM_FlOpen_Err'
    'WssM_FlShort_Err'
    'WssM_FlOverTemp_Err'
    'WssM_FlLeakage_Err'
    'WssM_FrOpen_Err'
    'WssM_FrShort_Err'
    'WssM_FrOverTemp_Err'
    'WssM_FrLeakage_Err'
    'WssM_RlOpen_Err'
    'WssM_RlShort_Err'
    'WssM_RlOverTemp_Err'
    'WssM_RlLeakage_Err'
    'WssM_RrOpen_Err'
    'WssM_RrShort_Err'
    'WssM_RrOverTemp_Err'
    'WssM_RrLeakage_Err'
    'WssM_Asic_Comm_Err'
    'WssM_Inhibit_FL'
    'WssM_Inhibit_FR'
    'WssM_Inhibit_RL'
    'WssM_Inhibit_RR'
    };
WssM_MainWssMonData = CreateBus(WssM_MainWssMonData, DeList);
clear DeList;


PressM_MainEcuModeSts = Simulink.Bus;
DeList={'PressM_MainEcuModeSts'};
PressM_MainEcuModeSts = CreateBus(PressM_MainEcuModeSts, DeList);
clear DeList;

PressM_MainIgnOnOffSts = Simulink.Bus;
DeList={'PressM_MainIgnOnOffSts'};
PressM_MainIgnOnOffSts = CreateBus(PressM_MainIgnOnOffSts, DeList);
clear DeList;

PressM_MainIgnEdgeSts = Simulink.Bus;
DeList={'PressM_MainIgnEdgeSts'};
PressM_MainIgnEdgeSts = CreateBus(PressM_MainIgnEdgeSts, DeList);
clear DeList;

PressM_MainPresMonFaultInfo = Simulink.Bus;
DeList={
    'CIRP1_FC_Fault_Err'
    'CIRP1_FC_Mismatch_Err'
    'CIRP1_FC_CRC_Err'
    'CIRP1_SC_Temp_Err'
    'CIRP1_SC_Mismatch_Err'
    'CIRP1_SC_CRC_Err'
    'CIRP1_SC_Diag_Info'
    'CIRP2_FC_Fault_Err'
    'CIRP2_FC_Mismatch_Err'
    'CIRP2_FC_CRC_Err'
    'CIRP2_SC_Temp_Err'
    'CIRP2_SC_Mismatch_Err'
    'CIRP2_SC_CRC_Err'
    'CIRP2_SC_Diag_Info'
    'SIMP_FC_Fault_Err'
    'SIMP_FC_Mismatch_Err'
    'SIMP_FC_CRC_Err'
    'SIMP_SC_Temp_Err'
    'SIMP_SC_Mismatch_Err'
    'SIMP_SC_CRC_Err'
    'SIMP_SC_Diag_Info'
    };
PressM_MainPresMonFaultInfo = CreateBus(PressM_MainPresMonFaultInfo, DeList);
clear DeList;


PedalM_MainEcuModeSts = Simulink.Bus;
DeList={'PedalM_MainEcuModeSts'};
PedalM_MainEcuModeSts = CreateBus(PedalM_MainEcuModeSts, DeList);
clear DeList;

PedalM_MainIgnOnOffSts = Simulink.Bus;
DeList={'PedalM_MainIgnOnOffSts'};
PedalM_MainIgnOnOffSts = CreateBus(PedalM_MainIgnOnOffSts, DeList);
clear DeList;

PedalM_MainIgnEdgeSts = Simulink.Bus;
DeList={'PedalM_MainIgnEdgeSts'};
PedalM_MainIgnEdgeSts = CreateBus(PedalM_MainIgnEdgeSts, DeList);
clear DeList;

PedalM_MainPedalMData = Simulink.Bus;
DeList={
    'PedalM_PTS1_Open_Err'
    'PedalM_PTS1_Short_Err'
    'PedalM_PTS2_Open_Err'
    'PedalM_PTS2_Short_Err'
    'PedalM_PTS1_SupplyPower_Err'
    'PedalM_PTS2_SupplyPower_Err'
    'PedalM_VDD3_OverVolt_Err'
    'PedalM_VDD3_UnderVolt_Err'
    'PedalM_VDD3_OverCurrent_Err'
    'PedalM_PTS1_Check_Ihb'
    'PedalM_PTS2_Check_Ihb'
    };
PedalM_MainPedalMData = CreateBus(PedalM_MainPedalMData, DeList);
clear DeList;


WssP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
WssP_MainWhlSpdInfo = CreateBus(WssP_MainWhlSpdInfo, DeList);
clear DeList;

WssP_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    };
WssP_MainAbsCtrlInfo = CreateBus(WssP_MainAbsCtrlInfo, DeList);
clear DeList;

WssP_MainWssMonData = Simulink.Bus;
DeList={
    'WssM_Inhibit_FL'
    'WssM_Inhibit_FR'
    'WssM_Inhibit_RL'
    'WssM_Inhibit_RR'
    };
WssP_MainWssMonData = CreateBus(WssP_MainWssMonData, DeList);
clear DeList;

WssP_MainEcuModeSts = Simulink.Bus;
DeList={'WssP_MainEcuModeSts'};
WssP_MainEcuModeSts = CreateBus(WssP_MainEcuModeSts, DeList);
clear DeList;

WssP_MainIgnOnOffSts = Simulink.Bus;
DeList={'WssP_MainIgnOnOffSts'};
WssP_MainIgnOnOffSts = CreateBus(WssP_MainIgnOnOffSts, DeList);
clear DeList;

WssP_MainIgnEdgeSts = Simulink.Bus;
DeList={'WssP_MainIgnEdgeSts'};
WssP_MainIgnEdgeSts = CreateBus(WssP_MainIgnEdgeSts, DeList);
clear DeList;

WssP_MainWssPlauData = Simulink.Bus;
DeList={
    'WssP_AbsLongTermErr'
    'WssP_AbsLongTerm_Ihb'
    'WssP_FL_PhaseErr'
    'WssP_FL_PhaseErr_Ihb'
    'WssP_FR_PhaseErr'
    'WssP_FR_PhaseErr_Ihb'
    'WssP_RL_PhaseErr'
    'WssP_RL_PhaseErr_Ihb'
    'WssP_RR_PhaseErr'
    'WssP_RR_PhaseErr_Ihb'
    'WssP_FL_ExciterErr'
    'WssP_FL_ExciterErr_Ihb'
    'WssP_FR_ExciterErr'
    'WssP_FR_ExciterErr_Ihb'
    'WssP_RL_ExciterErr'
    'WssP_RL_ExciterErr_Ihb'
    'WssP_RR_ExciterErr'
    'WssP_RR_ExciterErr_Ihb'
    'WssP_FL_AirGapErr'
    'WssP_FL_AirGapErr_Ihb'
    'WssP_FR_AirGapErr'
    'WssP_FR_AirGapErr_Ihb'
    'WssP_RL_AirGapErr'
    'WssP_RL_AirGapErr_Ihb'
    'WssP_RR_AirGapErr'
    'WssP_RR_AirGapErr_Ihb'
    'WssP_FL_PJumpErr'
    'WssP_FL_PJumpErr_Ihb'
    'WssP_FR_PJumpErr'
    'WssP_FR_PJumpErr_Ihb'
    'WssP_RL_PJumpErr'
    'WssP_RL_PJumpErr_Ihb'
    'WssP_RR_PJumpErr'
    'WssP_RR_PJumpErr_Ihb'
    'WssP_FL_MJumpErr'
    'WssP_FL_MJumpErr_Ihb'
    'WssP_FR_MJumpErr'
    'WssP_FR_MJumpErr_Ihb'
    'WssP_RL_MJumpErr'
    'WssP_RL_MJumpErr_Ihb'
    'WssP_RR_MJumpErr'
    'WssP_RR_MJumpErr_Ihb'
    'WssP_FL_WheelTypeSwapErr'
    'WssP_FL_WheelTypeSwapErr_Ihb'
    'WssP_FR_WheelTypeSwapErr'
    'WssP_FR_WheelTypeSwapErr_Ihb'
    'WssP_RL_WheelTypeSwapErr'
    'WssP_RL_WheelTypeSwapErr_Ihb'
    'WssP_RR_WheelTypeSwapErr'
    'WssP_RR_WheelTypeSwapErr_Ihb'
    'WssP_max_speed'
    'WssP_2nd_speed'
    'WssP_3rd_speed'
    'WssP_min_speed'
    };
WssP_MainWssPlauData = CreateBus(WssP_MainWssPlauData, DeList);
clear DeList;


PressP_MainPdf5msRawInfo = Simulink.Bus;
DeList={
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
PressP_MainPdf5msRawInfo = CreateBus(PressP_MainPdf5msRawInfo, DeList);
clear DeList;

PressP_MainPressSenCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimPMoveAve'
    'PressP_CirP1MoveAve'
    'PressP_CirP2MoveAve'
    'PressP_SimPMoveAveEolOfs'
    'PressP_CirP1MoveAveEolOfs'
    'PressP_CirP2MoveAveEolOfs'
    };
PressP_MainPressSenCalcInfo = CreateBus(PressP_MainPressSenCalcInfo, DeList);
clear DeList;

PressP_MainEcuModeSts = Simulink.Bus;
DeList={'PressP_MainEcuModeSts'};
PressP_MainEcuModeSts = CreateBus(PressP_MainEcuModeSts, DeList);
clear DeList;

PressP_MainIgnOnOffSts = Simulink.Bus;
DeList={'PressP_MainIgnOnOffSts'};
PressP_MainIgnOnOffSts = CreateBus(PressP_MainIgnOnOffSts, DeList);
clear DeList;

PressP_MainIgnEdgeSts = Simulink.Bus;
DeList={'PressP_MainIgnEdgeSts'};
PressP_MainIgnEdgeSts = CreateBus(PressP_MainIgnEdgeSts, DeList);
clear DeList;

PressP_MainPressPInfo = Simulink.Bus;
DeList={
    'SimP_Noise_Err'
    'CirP1_Noise_Err'
    'CirP2_Noise_Err'
    };
PressP_MainPressPInfo = CreateBus(PressP_MainPressPInfo, DeList);
clear DeList;


PedalP_MainCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    };
PedalP_MainCanRxAccelPedlInfo = CreateBus(PedalP_MainCanRxAccelPedlInfo, DeList);
clear DeList;

PedalP_MainPdf5msRawInfo = Simulink.Bus;
DeList={
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
PedalP_MainPdf5msRawInfo = CreateBus(PedalP_MainPdf5msRawInfo, DeList);
clear DeList;

PedalP_MainPressSenCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimPMoveAve'
    'PressP_CirP1MoveAve'
    'PressP_CirP2MoveAve'
    'PressP_SimPMoveAveEolOfs'
    'PressP_CirP1MoveAveEolOfs'
    'PressP_CirP2MoveAveEolOfs'
    };
PedalP_MainPressSenCalcInfo = CreateBus(PedalP_MainPressSenCalcInfo, DeList);
clear DeList;

PedalP_MainPedalMData = Simulink.Bus;
DeList={
    'PedalM_PTS1_Check_Ihb'
    'PedalM_PTS2_Check_Ihb'
    };
PedalP_MainPedalMData = CreateBus(PedalP_MainPedalMData, DeList);
clear DeList;

PedalP_MainWssPlauData = Simulink.Bus;
DeList={
    'WssP_min_speed'
    };
PedalP_MainWssPlauData = CreateBus(PedalP_MainWssPlauData, DeList);
clear DeList;

PedalP_MainEcuModeSts = Simulink.Bus;
DeList={'PedalP_MainEcuModeSts'};
PedalP_MainEcuModeSts = CreateBus(PedalP_MainEcuModeSts, DeList);
clear DeList;

PedalP_MainIgnOnOffSts = Simulink.Bus;
DeList={'PedalP_MainIgnOnOffSts'};
PedalP_MainIgnOnOffSts = CreateBus(PedalP_MainIgnOnOffSts, DeList);
clear DeList;

PedalP_MainIgnEdgeSts = Simulink.Bus;
DeList={'PedalP_MainIgnEdgeSts'};
PedalP_MainIgnEdgeSts = CreateBus(PedalP_MainIgnEdgeSts, DeList);
clear DeList;

PedalP_MainBlsSwt = Simulink.Bus;
DeList={'PedalP_MainBlsSwt'};
PedalP_MainBlsSwt = CreateBus(PedalP_MainBlsSwt, DeList);
clear DeList;


BbsVlvM_MainArbFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
BbsVlvM_MainArbFsrBbsDrvInfo = CreateBus(BbsVlvM_MainArbFsrBbsDrvInfo, DeList);
clear DeList;

BbsVlvM_MainArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    };
BbsVlvM_MainArbNormVlvReqInfo = CreateBus(BbsVlvM_MainArbNormVlvReqInfo, DeList);
clear DeList;

BbsVlvM_MainEcuModeSts = Simulink.Bus;
DeList={'BbsVlvM_MainEcuModeSts'};
BbsVlvM_MainEcuModeSts = CreateBus(BbsVlvM_MainEcuModeSts, DeList);
clear DeList;

BbsVlvM_MainIgnOnOffSts = Simulink.Bus;
DeList={'BbsVlvM_MainIgnOnOffSts'};
BbsVlvM_MainIgnOnOffSts = CreateBus(BbsVlvM_MainIgnOnOffSts, DeList);
clear DeList;

BbsVlvM_MainIgnEdgeSts = Simulink.Bus;
DeList={'BbsVlvM_MainIgnEdgeSts'};
BbsVlvM_MainIgnEdgeSts = CreateBus(BbsVlvM_MainIgnEdgeSts, DeList);
clear DeList;

BbsVlvM_MainAsicEnDrDrv = Simulink.Bus;
DeList={'BbsVlvM_MainAsicEnDrDrv'};
BbsVlvM_MainAsicEnDrDrv = CreateBus(BbsVlvM_MainAsicEnDrDrv, DeList);
clear DeList;

BbsVlvM_MainArbVlvDriveState = Simulink.Bus;
DeList={'BbsVlvM_MainArbVlvDriveState'};
BbsVlvM_MainArbVlvDriveState = CreateBus(BbsVlvM_MainArbVlvDriveState, DeList);
clear DeList;

BbsVlvM_MainFSBbsVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_sim_time'
    'fs_on_cutp_time'
    'fs_on_cuts_time'
    'fs_on_rlv_time'
    'fs_on_cv_time'
    };
BbsVlvM_MainFSBbsVlvActrInfo = CreateBus(BbsVlvM_MainFSBbsVlvActrInfo, DeList);
clear DeList;

BbsVlvM_MainFSFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
BbsVlvM_MainFSFsrBbsDrvInfo = CreateBus(BbsVlvM_MainFSFsrBbsDrvInfo, DeList);
clear DeList;

BbsVlvM_MainBBSVlvErrInfo = Simulink.Bus;
DeList={
    'Batt1Fuse_Open_Err'
    'BbsVlvRelay_Open_Err'
    'BbsVlvRelay_S2G_Err'
    'BbsVlvRelay_S2B_Err'
    'BbsVlvRelay_Short_Err'
    'BbsVlvRelay_OverTemp_Err'
    'BbsVlvRelay_ShutdownLine_Err'
    'BbsVlvRelay_CP_Err'
    'BbsVlv_Sim_Open_Err'
    'BbsVlv_Sim_PsvOpen_Err'
    'BbsVlv_Sim_Short_Err'
    'BbsVlv_Sim_CurReg_Err'
    'BbsVlv_CutP_Open_Err'
    'BbsVlv_CutP_PsvOpen_Err'
    'BbsVlv_CutP_Short_Err'
    'BbsVlv_CutP_CurReg_Err'
    'BbsVlv_CutS_Open_Err'
    'BbsVlv_CutS_PsvOpen_Err'
    'BbsVlv_CutS_Short_Err'
    'BbsVlv_CutS_CurReg_Err'
    'BbsVlv_Rlv_Open_Err'
    'BbsVlv_Rlv_PsvOpen_Err'
    'BbsVlv_Rlv_Short_Err'
    'BbsVlv_Rlv_CurReg_Err'
    'BbsVlv_CircVlv_Open_Err'
    'BbsVlv_CircVlv_PsvOpen_Err'
    'BbsVlv_CircVlv_Short_Err'
    'BbsVlv_CircVlv_CurReg_Err'
    };
BbsVlvM_MainBBSVlvErrInfo = CreateBus(BbsVlvM_MainBBSVlvErrInfo, DeList);
clear DeList;

BbsVlvM_MainFSBbsVlvInitEndFlg = Simulink.Bus;
DeList={'BbsVlvM_MainFSBbsVlvInitEndFlg'};
BbsVlvM_MainFSBbsVlvInitEndFlg = CreateBus(BbsVlvM_MainFSBbsVlvInitEndFlg, DeList);
clear DeList;


AbsVlvM_MainArbFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
AbsVlvM_MainArbFsrAbsDrvInfo = CreateBus(AbsVlvM_MainArbFsrAbsDrvInfo, DeList);
clear DeList;

AbsVlvM_MainArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
AbsVlvM_MainArbWhlVlvReqInfo = CreateBus(AbsVlvM_MainArbWhlVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    };
AbsVlvM_MainArbNormVlvReqInfo = CreateBus(AbsVlvM_MainArbNormVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainArbBalVlvReqInfo = Simulink.Bus;
DeList={
    'BalVlvReqData_array_0'
    'BalVlvReqData_array_1'
    'BalVlvReqData_array_2'
    'BalVlvReqData_array_3'
    'BalVlvReqData_array_4'
    };
AbsVlvM_MainArbBalVlvReqInfo = CreateBus(AbsVlvM_MainArbBalVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainArbResPVlvReqInfo = Simulink.Bus;
DeList={
    'ResPVlvReqData_array_0'
    'ResPVlvReqData_array_1'
    'ResPVlvReqData_array_2'
    'ResPVlvReqData_array_3'
    'ResPVlvReqData_array_4'
    };
AbsVlvM_MainArbResPVlvReqInfo = CreateBus(AbsVlvM_MainArbResPVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainEcuModeSts = Simulink.Bus;
DeList={'AbsVlvM_MainEcuModeSts'};
AbsVlvM_MainEcuModeSts = CreateBus(AbsVlvM_MainEcuModeSts, DeList);
clear DeList;

AbsVlvM_MainIgnOnOffSts = Simulink.Bus;
DeList={'AbsVlvM_MainIgnOnOffSts'};
AbsVlvM_MainIgnOnOffSts = CreateBus(AbsVlvM_MainIgnOnOffSts, DeList);
clear DeList;

AbsVlvM_MainIgnEdgeSts = Simulink.Bus;
DeList={'AbsVlvM_MainIgnEdgeSts'};
AbsVlvM_MainIgnEdgeSts = CreateBus(AbsVlvM_MainIgnEdgeSts, DeList);
clear DeList;

AbsVlvM_MainAsicEnDrDrv = Simulink.Bus;
DeList={'AbsVlvM_MainAsicEnDrDrv'};
AbsVlvM_MainAsicEnDrDrv = CreateBus(AbsVlvM_MainAsicEnDrDrv, DeList);
clear DeList;

AbsVlvM_MainArbVlvDriveState = Simulink.Bus;
DeList={'AbsVlvM_MainArbVlvDriveState'};
AbsVlvM_MainArbVlvDriveState = CreateBus(AbsVlvM_MainArbVlvDriveState, DeList);
clear DeList;

AbsVlvM_MainFSEscVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_flno_time'
    'fs_on_frno_time'
    'fs_on_rlno_time'
    'fs_on_rrno_time'
    'fs_on_bal_time'
    'fs_on_pressdump_time'
    'fs_on_resp_time'
    'fs_on_flnc_time'
    'fs_on_frnc_time'
    'fs_on_rlnc_time'
    'fs_on_rrnc_time'
    };
AbsVlvM_MainFSEscVlvActrInfo = CreateBus(AbsVlvM_MainFSEscVlvActrInfo, DeList);
clear DeList;

AbsVlvM_MainFSFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
AbsVlvM_MainFSFsrAbsDrvInfo = CreateBus(AbsVlvM_MainFSFsrAbsDrvInfo, DeList);
clear DeList;

AbsVlvM_MainAbsVlvMData = Simulink.Bus;
DeList={
    'AbsVlvM_ValveRelay_Open_Err'
    'AbsVlvM_ValveRelay_S2G_Err'
    'AbsVlvM_ValveRelay_S2B_Err'
    'AbsVlvM_ValveRelay_CP_Err'
    'AbsVlvM_ValveRelay_Short_Err'
    'AbsVlvM_ValveRelay_Batt1FuseOpen_Err'
    'AbsVlvM_ValveRelay_OverTemp_Err'
    'AbsVlvM_ValveRelay_ShutdownLine_Err'
    'AbsVlvM_ValveFLNO_Open_Err'
    'AbsVlvM_ValveFLNO_PsvOpen_Err'
    'AbsVlvM_ValveFLNO_Short_Err'
    'AbsVlvM_ValveFLNO_OverTemp_Err'
    'AbsVlvM_ValveFLNO_CurReg_Err'
    'AbsVlvM_ValveFRNO_Open_Err'
    'AbsVlvM_ValveFRNO_PsvOpen_Err'
    'AbsVlvM_ValveFRNO_Short_Err'
    'AbsVlvM_ValveFRNO_OverTemp_Err'
    'AbsVlvM_ValveFRNO_CurReg_Err'
    'AbsVlvM_ValveRLNO_Open_Err'
    'AbsVlvM_ValveRLNO_PsvOpen_Err'
    'AbsVlvM_ValveRLNO_Short_Err'
    'AbsVlvM_ValveRLNO_OverTemp_Err'
    'AbsVlvM_ValveRLNO_CurReg_Err'
    'AbsVlvM_ValveRRNO_Open_Err'
    'AbsVlvM_ValveRRNO_PsvOpen_Err'
    'AbsVlvM_ValveRRNO_Short_Err'
    'AbsVlvM_ValveRRNO_OverTemp_Err'
    'AbsVlvM_ValveRRNO_CurReg_Err'
    'AbsVlvM_ValveBAL_Open_Err'
    'AbsVlvM_ValveBAL_PsvOpen_Err'
    'AbsVlvM_ValveBAL_Short_Err'
    'AbsVlvM_ValveBAL_OverTemp_Err'
    'AbsVlvM_ValveBAL_CurReg_Err'
    'AbsVlvM_ValvePD_Open_Err'
    'AbsVlvM_ValvePD_PsvOpen_Err'
    'AbsVlvM_ValvePD_Short_Err'
    'AbsVlvM_ValvePD_OverTemp_Err'
    'AbsVlvM_ValvePD_CurReg_Err'
    'AbsVlvM_ValveRESP_Open_Err'
    'AbsVlvM_ValveRESP_PsvOpen_Err'
    'AbsVlvM_ValveRESP_Short_Err'
    'AbsVlvM_ValveRESP_OverTemp_Err'
    'AbsVlvM_ValveRESP_CurReg_Err'
    'AbsVlvM_ValveFLNC_Open_Err'
    'AbsVlvM_ValveFLNC_PsvOpen_Err'
    'AbsVlvM_ValveFLNC_Short_Err'
    'AbsVlvM_ValveFLNC_OverTemp_Err'
    'AbsVlvM_ValveFLNC_CurReg_Err'
    'AbsVlvM_ValveFRNC_Open_Err'
    'AbsVlvM_ValveFRNC_PsvOpen_Err'
    'AbsVlvM_ValveFRNC_Short_Err'
    'AbsVlvM_ValveFRNC_OverTemp_Err'
    'AbsVlvM_ValveFRNC_CurReg_Err'
    'AbsVlvM_ValveRLNC_Open_Err'
    'AbsVlvM_ValveRLNC_PsvOpen_Err'
    'AbsVlvM_ValveRLNC_Short_Err'
    'AbsVlvM_ValveRLNC_OverTemp_Err'
    'AbsVlvM_ValveRLNC_CurReg_Err'
    'AbsVlvM_ValveRRNC_Open_Err'
    'AbsVlvM_ValveRRNC_PsvOpen_Err'
    'AbsVlvM_ValveRRNC_Short_Err'
    'AbsVlvM_ValveRRNC_OverTemp_Err'
    'AbsVlvM_ValveRRNC_CurReg_Err'
    };
AbsVlvM_MainAbsVlvMData = CreateBus(AbsVlvM_MainAbsVlvMData, DeList);
clear DeList;

AbsVlvM_MainFSEscVlvInitEndFlg = Simulink.Bus;
DeList={'AbsVlvM_MainFSEscVlvInitEndFlg'};
AbsVlvM_MainFSEscVlvInitEndFlg = CreateBus(AbsVlvM_MainFSEscVlvInitEndFlg, DeList);
clear DeList;


Mps_TLE5012M_MainEcuModeSts = Simulink.Bus;
DeList={'Mps_TLE5012M_MainEcuModeSts'};
Mps_TLE5012M_MainEcuModeSts = CreateBus(Mps_TLE5012M_MainEcuModeSts, DeList);
clear DeList;

Mps_TLE5012M_MainIgnOnOffSts = Simulink.Bus;
DeList={'Mps_TLE5012M_MainIgnOnOffSts'};
Mps_TLE5012M_MainIgnOnOffSts = CreateBus(Mps_TLE5012M_MainIgnOnOffSts, DeList);
clear DeList;

Mps_TLE5012M_MainIgnEdgeSts = Simulink.Bus;
DeList={'Mps_TLE5012M_MainIgnEdgeSts'};
Mps_TLE5012M_MainIgnEdgeSts = CreateBus(Mps_TLE5012M_MainIgnEdgeSts, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD1ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Mps_TLE5012M_MainMpsD1ErrInfo = CreateBus(Mps_TLE5012M_MainMpsD1ErrInfo, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD2ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Mps_TLE5012M_MainMpsD2ErrInfo = CreateBus(Mps_TLE5012M_MainMpsD2ErrInfo, DeList);
clear DeList;


MgdM_MainEcuModeSts = Simulink.Bus;
DeList={'MgdM_MainEcuModeSts'};
MgdM_MainEcuModeSts = CreateBus(MgdM_MainEcuModeSts, DeList);
clear DeList;

MgdM_MainIgnOnOffSts = Simulink.Bus;
DeList={'MgdM_MainIgnOnOffSts'};
MgdM_MainIgnOnOffSts = CreateBus(MgdM_MainIgnOnOffSts, DeList);
clear DeList;

MgdM_MainIgnEdgeSts = Simulink.Bus;
DeList={'MgdM_MainIgnEdgeSts'};
MgdM_MainIgnEdgeSts = CreateBus(MgdM_MainIgnEdgeSts, DeList);
clear DeList;


MtrM_MainEcuModeSts = Simulink.Bus;
DeList={'MtrM_MainEcuModeSts'};
MtrM_MainEcuModeSts = CreateBus(MtrM_MainEcuModeSts, DeList);
clear DeList;

MtrM_MainIgnOnOffSts = Simulink.Bus;
DeList={'MtrM_MainIgnOnOffSts'};
MtrM_MainIgnOnOffSts = CreateBus(MtrM_MainIgnOnOffSts, DeList);
clear DeList;

MtrM_MainIgnEdgeSts = Simulink.Bus;
DeList={'MtrM_MainIgnEdgeSts'};
MtrM_MainIgnEdgeSts = CreateBus(MtrM_MainIgnEdgeSts, DeList);
clear DeList;

MtrM_MainMTRMOutInfo = Simulink.Bus;
DeList={
    'MTRM_Power_Open_Err'
    'MTRM_Open_Err'
    'MTRM_Phase_U_OverCurret_Err'
    'MTRM_Phase_V_OverCurret_Err'
    'MTRM_Phase_W_OverCurret_Err'
    'MTRM_Calibration_Err'
    'MTRM_Short_Err'
    'MTRM_Driver_Err'
    };
MtrM_MainMTRMOutInfo = CreateBus(MtrM_MainMTRMOutInfo, DeList);
clear DeList;

MtrM_MainMgdErrInfo = Simulink.Bus;
DeList={
    'MgdInterPwrSuppMonErr'
    'MgdClkMonErr'
    'MgdBISTErr'
    'MgdFlashMemoryErr'
    'MgdRAMErr'
    'MgdConfigRegiErr'
    'MgdInputPattMonErr'
    'MgdOverTempErr'
    'MgdCPmpVMonErr'
    'MgdHBuffCapVErr'
    'MgdInOutPlauErr'
    'MgdCtrlSigMonErr'
    };
MtrM_MainMgdErrInfo = CreateBus(MtrM_MainMgdErrInfo, DeList);
clear DeList;

MtrM_MainMTRInitEndFlg = Simulink.Bus;
DeList={'MtrM_MainMTRInitEndFlg'};
MtrM_MainMTRInitEndFlg = CreateBus(MtrM_MainMTRInitEndFlg, DeList);
clear DeList;


Watchdog_MainEcuModeSts = Simulink.Bus;
DeList={'Watchdog_MainEcuModeSts'};
Watchdog_MainEcuModeSts = CreateBus(Watchdog_MainEcuModeSts, DeList);
clear DeList;

Watchdog_MainIgnOnOffSts = Simulink.Bus;
DeList={'Watchdog_MainIgnOnOffSts'};
Watchdog_MainIgnOnOffSts = CreateBus(Watchdog_MainIgnOnOffSts, DeList);
clear DeList;

Watchdog_MainIgnEdgeSts = Simulink.Bus;
DeList={'Watchdog_MainIgnEdgeSts'};
Watchdog_MainIgnEdgeSts = CreateBus(Watchdog_MainIgnEdgeSts, DeList);
clear DeList;


Eem_MainBBSVlvErrInfo = Simulink.Bus;
DeList={
    'Batt1Fuse_Open_Err'
    'BbsVlvRelay_Open_Err'
    'BbsVlvRelay_S2G_Err'
    'BbsVlvRelay_S2B_Err'
    'BbsVlvRelay_Short_Err'
    'BbsVlvRelay_OverTemp_Err'
    'BbsVlvRelay_ShutdownLine_Err'
    'BbsVlvRelay_CP_Err'
    'BbsVlv_Sim_Open_Err'
    'BbsVlv_Sim_PsvOpen_Err'
    'BbsVlv_Sim_Short_Err'
    'BbsVlv_Sim_CurReg_Err'
    'BbsVlv_CutP_Open_Err'
    'BbsVlv_CutP_PsvOpen_Err'
    'BbsVlv_CutP_Short_Err'
    'BbsVlv_CutP_CurReg_Err'
    'BbsVlv_CutS_Open_Err'
    'BbsVlv_CutS_PsvOpen_Err'
    'BbsVlv_CutS_Short_Err'
    'BbsVlv_CutS_CurReg_Err'
    'BbsVlv_Rlv_Open_Err'
    'BbsVlv_Rlv_PsvOpen_Err'
    'BbsVlv_Rlv_Short_Err'
    'BbsVlv_Rlv_CurReg_Err'
    'BbsVlv_CircVlv_Open_Err'
    'BbsVlv_CircVlv_PsvOpen_Err'
    'BbsVlv_CircVlv_Short_Err'
    'BbsVlv_CircVlv_CurReg_Err'
    };
Eem_MainBBSVlvErrInfo = CreateBus(Eem_MainBBSVlvErrInfo, DeList);
clear DeList;

Eem_MainAbsVlvMData = Simulink.Bus;
DeList={
    'AbsVlvM_ValveRelay_Open_Err'
    'AbsVlvM_ValveRelay_S2G_Err'
    'AbsVlvM_ValveRelay_S2B_Err'
    'AbsVlvM_ValveRelay_CP_Err'
    'AbsVlvM_ValveRelay_Short_Err'
    'AbsVlvM_ValveRelay_Batt1FuseOpen_Err'
    'AbsVlvM_ValveRelay_OverTemp_Err'
    'AbsVlvM_ValveRelay_ShutdownLine_Err'
    'AbsVlvM_ValveFLNO_Open_Err'
    'AbsVlvM_ValveFLNO_PsvOpen_Err'
    'AbsVlvM_ValveFLNO_Short_Err'
    'AbsVlvM_ValveFLNO_OverTemp_Err'
    'AbsVlvM_ValveFLNO_CurReg_Err'
    'AbsVlvM_ValveFRNO_Open_Err'
    'AbsVlvM_ValveFRNO_PsvOpen_Err'
    'AbsVlvM_ValveFRNO_Short_Err'
    'AbsVlvM_ValveFRNO_OverTemp_Err'
    'AbsVlvM_ValveFRNO_CurReg_Err'
    'AbsVlvM_ValveRLNO_Open_Err'
    'AbsVlvM_ValveRLNO_PsvOpen_Err'
    'AbsVlvM_ValveRLNO_Short_Err'
    'AbsVlvM_ValveRLNO_OverTemp_Err'
    'AbsVlvM_ValveRLNO_CurReg_Err'
    'AbsVlvM_ValveRRNO_Open_Err'
    'AbsVlvM_ValveRRNO_PsvOpen_Err'
    'AbsVlvM_ValveRRNO_Short_Err'
    'AbsVlvM_ValveRRNO_OverTemp_Err'
    'AbsVlvM_ValveRRNO_CurReg_Err'
    'AbsVlvM_ValveBAL_Open_Err'
    'AbsVlvM_ValveBAL_PsvOpen_Err'
    'AbsVlvM_ValveBAL_Short_Err'
    'AbsVlvM_ValveBAL_OverTemp_Err'
    'AbsVlvM_ValveBAL_CurReg_Err'
    'AbsVlvM_ValvePD_Open_Err'
    'AbsVlvM_ValvePD_PsvOpen_Err'
    'AbsVlvM_ValvePD_Short_Err'
    'AbsVlvM_ValvePD_OverTemp_Err'
    'AbsVlvM_ValvePD_CurReg_Err'
    'AbsVlvM_ValveRESP_Open_Err'
    'AbsVlvM_ValveRESP_PsvOpen_Err'
    'AbsVlvM_ValveRESP_Short_Err'
    'AbsVlvM_ValveRESP_OverTemp_Err'
    'AbsVlvM_ValveRESP_CurReg_Err'
    'AbsVlvM_ValveFLNC_Open_Err'
    'AbsVlvM_ValveFLNC_PsvOpen_Err'
    'AbsVlvM_ValveFLNC_Short_Err'
    'AbsVlvM_ValveFLNC_OverTemp_Err'
    'AbsVlvM_ValveFLNC_CurReg_Err'
    'AbsVlvM_ValveFRNC_Open_Err'
    'AbsVlvM_ValveFRNC_PsvOpen_Err'
    'AbsVlvM_ValveFRNC_Short_Err'
    'AbsVlvM_ValveFRNC_OverTemp_Err'
    'AbsVlvM_ValveFRNC_CurReg_Err'
    'AbsVlvM_ValveRLNC_Open_Err'
    'AbsVlvM_ValveRLNC_PsvOpen_Err'
    'AbsVlvM_ValveRLNC_Short_Err'
    'AbsVlvM_ValveRLNC_OverTemp_Err'
    'AbsVlvM_ValveRLNC_CurReg_Err'
    'AbsVlvM_ValveRRNC_Open_Err'
    'AbsVlvM_ValveRRNC_PsvOpen_Err'
    'AbsVlvM_ValveRRNC_Short_Err'
    'AbsVlvM_ValveRRNC_OverTemp_Err'
    'AbsVlvM_ValveRRNC_CurReg_Err'
    };
Eem_MainAbsVlvMData = CreateBus(Eem_MainAbsVlvMData, DeList);
clear DeList;

Eem_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
Eem_MainEscSwtStInfo = CreateBus(Eem_MainEscSwtStInfo, DeList);
clear DeList;

Eem_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Eem_MainWhlSpdInfo = CreateBus(Eem_MainWhlSpdInfo, DeList);
clear DeList;

Eem_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    };
Eem_MainAbsCtrlInfo = CreateBus(Eem_MainAbsCtrlInfo, DeList);
clear DeList;

Eem_MainEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    };
Eem_MainEscCtrlInfo = CreateBus(Eem_MainEscCtrlInfo, DeList);
clear DeList;

Eem_MainHdcCtrlInfo = Simulink.Bus;
DeList={
    'HdcActFlg'
    };
Eem_MainHdcCtrlInfo = CreateBus(Eem_MainHdcCtrlInfo, DeList);
clear DeList;

Eem_MainTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    };
Eem_MainTcsCtrlInfo = CreateBus(Eem_MainTcsCtrlInfo, DeList);
clear DeList;

Eem_MainPedalMData = Simulink.Bus;
DeList={
    'PedalM_PTS1_Open_Err'
    'PedalM_PTS1_Short_Err'
    'PedalM_PTS2_Open_Err'
    'PedalM_PTS2_Short_Err'
    'PedalM_PTS1_SupplyPower_Err'
    'PedalM_PTS2_SupplyPower_Err'
    'PedalM_VDD3_OverVolt_Err'
    'PedalM_VDD3_UnderVolt_Err'
    'PedalM_VDD3_OverCurrent_Err'
    };
Eem_MainPedalMData = CreateBus(Eem_MainPedalMData, DeList);
clear DeList;

Eem_MainPresMonFaultInfo = Simulink.Bus;
DeList={
    'CIRP1_FC_Fault_Err'
    'CIRP1_FC_Mismatch_Err'
    'CIRP1_FC_CRC_Err'
    'CIRP1_SC_Temp_Err'
    'CIRP1_SC_Mismatch_Err'
    'CIRP1_SC_CRC_Err'
    'CIRP1_SC_Diag_Info'
    'CIRP2_FC_Fault_Err'
    'CIRP2_FC_Mismatch_Err'
    'CIRP2_FC_CRC_Err'
    'CIRP2_SC_Temp_Err'
    'CIRP2_SC_Mismatch_Err'
    'CIRP2_SC_CRC_Err'
    'CIRP2_SC_Diag_Info'
    'SIMP_FC_Fault_Err'
    'SIMP_FC_Mismatch_Err'
    'SIMP_FC_CRC_Err'
    'SIMP_SC_Temp_Err'
    'SIMP_SC_Mismatch_Err'
    'SIMP_SC_CRC_Err'
    'SIMP_SC_Diag_Info'
    };
Eem_MainPresMonFaultInfo = CreateBus(Eem_MainPresMonFaultInfo, DeList);
clear DeList;

Eem_MainAsicMonFaultInfo = Simulink.Bus;
DeList={
    'ASICM_AsicOverVolt_Err'
    'ASICM_AsicOverVolt_Err_Info'
    'ASICM_AsicUnderVolt_Err'
    'ASICM_AsicUnderVolt_Err_Info'
    'ASICM_AsicOverTemp_Err'
    'ASICM_AsicOverTemp_Err_Info'
    'ASICM_AsicComm_Err'
    'ASICM_AsicOsc_Err'
    'ASICM_AsicNvm_Err'
    'ASICM_AsicGroundLoss_Err'
    };
Eem_MainAsicMonFaultInfo = CreateBus(Eem_MainAsicMonFaultInfo, DeList);
clear DeList;

Eem_MainMTRMOutInfo = Simulink.Bus;
DeList={
    'MTRM_Power_Open_Err'
    'MTRM_Open_Err'
    'MTRM_Phase_U_OverCurret_Err'
    'MTRM_Phase_V_OverCurret_Err'
    'MTRM_Phase_W_OverCurret_Err'
    'MTRM_Calibration_Err'
    'MTRM_Short_Err'
    'MTRM_Driver_Err'
    };
Eem_MainMTRMOutInfo = CreateBus(Eem_MainMTRMOutInfo, DeList);
clear DeList;

Eem_MainSysPwrMonData = Simulink.Bus;
DeList={
    'SysPwrM_OverVolt_1st_Err'
    'SysPwrM_OverVolt_2st_Err'
    'SysPwrM_UnderVolt_1st_Err'
    'SysPwrM_UnderVolt_2st_Err'
    'SysPwrM_UnderVolt_3st_Err'
    'SysPwrM_Asic_Vdd_Err'
    'SysPwrM_Asic_OverVolt_Err'
    'SysPwrM_Asic_UnderVolt_Err'
    'SysPwrM_Asic_OverTemp_Err'
    'SysPwrM_Asic_Gndloss_Err'
    'SysPwrM_Asic_Comm_Err'
    'SysPwrM_IgnLine_Open_Err'
    };
Eem_MainSysPwrMonData = CreateBus(Eem_MainSysPwrMonData, DeList);
clear DeList;

Eem_MainWssMonData = Simulink.Bus;
DeList={
    'WssM_FlOpen_Err'
    'WssM_FlShort_Err'
    'WssM_FlOverTemp_Err'
    'WssM_FlLeakage_Err'
    'WssM_FrOpen_Err'
    'WssM_FrShort_Err'
    'WssM_FrOverTemp_Err'
    'WssM_FrLeakage_Err'
    'WssM_RlOpen_Err'
    'WssM_RlShort_Err'
    'WssM_RlOverTemp_Err'
    'WssM_RlLeakage_Err'
    'WssM_RrOpen_Err'
    'WssM_RrShort_Err'
    'WssM_RrOverTemp_Err'
    'WssM_RrLeakage_Err'
    'WssM_Asic_Comm_Err'
    };
Eem_MainWssMonData = CreateBus(Eem_MainWssMonData, DeList);
clear DeList;

Eem_MainMgdErrInfo = Simulink.Bus;
DeList={
    'MgdInterPwrSuppMonErr'
    'MgdClkMonErr'
    'MgdBISTErr'
    'MgdFlashMemoryErr'
    'MgdRAMErr'
    'MgdConfigRegiErr'
    'MgdInputPattMonErr'
    'MgdOverTempErr'
    'MgdCPmpVMonErr'
    'MgdHBuffCapVErr'
    'MgdInOutPlauErr'
    'MgdCtrlSigMonErr'
    };
Eem_MainMgdErrInfo = CreateBus(Eem_MainMgdErrInfo, DeList);
clear DeList;

Eem_MainMpsD1ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Eem_MainMpsD1ErrInfo = CreateBus(Eem_MainMpsD1ErrInfo, DeList);
clear DeList;

Eem_MainMpsD2ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Eem_MainMpsD2ErrInfo = CreateBus(Eem_MainMpsD2ErrInfo, DeList);
clear DeList;

Eem_MainPressPInfo = Simulink.Bus;
DeList={
    'SimP_Noise_Err'
    'CirP1_Noise_Err'
    'CirP2_Noise_Err'
    };
Eem_MainPressPInfo = CreateBus(Eem_MainPressPInfo, DeList);
clear DeList;

Eem_MainEcuModeSts = Simulink.Bus;
DeList={'Eem_MainEcuModeSts'};
Eem_MainEcuModeSts = CreateBus(Eem_MainEcuModeSts, DeList);
clear DeList;

Eem_MainIgnOnOffSts = Simulink.Bus;
DeList={'Eem_MainIgnOnOffSts'};
Eem_MainIgnOnOffSts = CreateBus(Eem_MainIgnOnOffSts, DeList);
clear DeList;

Eem_MainIgnEdgeSts = Simulink.Bus;
DeList={'Eem_MainIgnEdgeSts'};
Eem_MainIgnEdgeSts = CreateBus(Eem_MainIgnEdgeSts, DeList);
clear DeList;

Eem_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Eem_MainEemFailData = CreateBus(Eem_MainEemFailData, DeList);
clear DeList;

Eem_MainEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Eem_MainEemCtrlInhibitData = CreateBus(Eem_MainEemCtrlInhibitData, DeList);
clear DeList;

Eem_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
Eem_MainEemSuspectData = CreateBus(Eem_MainEemSuspectData, DeList);
clear DeList;

Eem_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
Eem_MainEemEceData = CreateBus(Eem_MainEemEceData, DeList);
clear DeList;


Eem_SuspcDetnEcuModeSts = Simulink.Bus;
DeList={'Eem_SuspcDetnEcuModeSts'};
Eem_SuspcDetnEcuModeSts = CreateBus(Eem_SuspcDetnEcuModeSts, DeList);
clear DeList;

Eem_SuspcDetnSwtStsFSInfo = Simulink.Bus;
DeList={
    'FlexBrkSwtFaultDet'
    };
Eem_SuspcDetnSwtStsFSInfo = CreateBus(Eem_SuspcDetnSwtStsFSInfo, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvSts'};
Eem_SuspcDetnFuncInhibitVlvSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitIocSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitIocSts'};
Eem_SuspcDetnFuncInhibitIocSts = CreateBus(Eem_SuspcDetnFuncInhibitIocSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitProxySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitProxySts'};
Eem_SuspcDetnFuncInhibitProxySts = CreateBus(Eem_SuspcDetnFuncInhibitProxySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitFsrSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitFsrSts'};
Eem_SuspcDetnFuncInhibitFsrSts = CreateBus(Eem_SuspcDetnFuncInhibitFsrSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitNvmSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitNvmSts'};
Eem_SuspcDetnFuncInhibitNvmSts = CreateBus(Eem_SuspcDetnFuncInhibitNvmSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitWssSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitWssSts'};
Eem_SuspcDetnFuncInhibitWssSts = CreateBus(Eem_SuspcDetnFuncInhibitWssSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPedalSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPedalSts'};
Eem_SuspcDetnFuncInhibitPedalSts = CreateBus(Eem_SuspcDetnFuncInhibitPedalSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPressSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPressSts'};
Eem_SuspcDetnFuncInhibitPressSts = CreateBus(Eem_SuspcDetnFuncInhibitPressSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitRlySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitRlySts'};
Eem_SuspcDetnFuncInhibitRlySts = CreateBus(Eem_SuspcDetnFuncInhibitRlySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitSwtSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitSwtSts'};
Eem_SuspcDetnFuncInhibitSwtSts = CreateBus(Eem_SuspcDetnFuncInhibitSwtSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPrlySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPrlySts'};
Eem_SuspcDetnFuncInhibitPrlySts = CreateBus(Eem_SuspcDetnFuncInhibitPrlySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMomSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMomSts'};
Eem_SuspcDetnFuncInhibitMomSts = CreateBus(Eem_SuspcDetnFuncInhibitMomSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitIcuSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitIcuSts'};
Eem_SuspcDetnFuncInhibitIcuSts = CreateBus(Eem_SuspcDetnFuncInhibitIcuSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitBbcSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitBbcSts'};
Eem_SuspcDetnFuncInhibitBbcSts = CreateBus(Eem_SuspcDetnFuncInhibitBbcSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitRbcSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitRbcSts'};
Eem_SuspcDetnFuncInhibitRbcSts = CreateBus(Eem_SuspcDetnFuncInhibitRbcSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitArbiSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitArbiSts'};
Eem_SuspcDetnFuncInhibitArbiSts = CreateBus(Eem_SuspcDetnFuncInhibitArbiSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPctrlSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPctrlSts'};
Eem_SuspcDetnFuncInhibitPctrlSts = CreateBus(Eem_SuspcDetnFuncInhibitPctrlSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvActSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvActSts'};
Eem_SuspcDetnFuncInhibitVlvActSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvActSts, DeList);
clear DeList;


Arbitrator_VlvFSBbsVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_sim_time'
    'fs_on_cutp_time'
    'fs_on_cuts_time'
    'fs_on_rlv_time'
    'fs_on_cv_time'
    };
Arbitrator_VlvFSBbsVlvActrInfo = CreateBus(Arbitrator_VlvFSBbsVlvActrInfo, DeList);
clear DeList;

Arbitrator_VlvFSFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
Arbitrator_VlvFSFsrBbsDrvInfo = CreateBus(Arbitrator_VlvFSFsrBbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvFSEscVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_flno_time'
    'fs_on_frno_time'
    'fs_on_rlno_time'
    'fs_on_rrno_time'
    'fs_on_bal_time'
    'fs_on_pressdump_time'
    'fs_on_resp_time'
    'fs_on_flnc_time'
    'fs_on_frnc_time'
    'fs_on_rlnc_time'
    'fs_on_rrnc_time'
    };
Arbitrator_VlvFSEscVlvActrInfo = CreateBus(Arbitrator_VlvFSEscVlvActrInfo, DeList);
clear DeList;

Arbitrator_VlvFSFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
Arbitrator_VlvFSFsrAbsDrvInfo = CreateBus(Arbitrator_VlvFSFsrAbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    };
Arbitrator_VlvWhlVlvReqAbcInfo = CreateBus(Arbitrator_VlvWhlVlvReqAbcInfo, DeList);
clear DeList;

Arbitrator_VlvNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'RelsVlvReq'
    'CircVlvReq'
    'PressDumpVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    'CircVlvDataLen'
    'RelsVlvDataLen'
    'PressDumpVlvDataLen'
    };
Arbitrator_VlvNormVlvReqVlvActInfo = CreateBus(Arbitrator_VlvNormVlvReqVlvActInfo, DeList);
clear DeList;

Arbitrator_VlvWhlVlvReqIdbInfo = Simulink.Bus;
DeList={
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    };
Arbitrator_VlvWhlVlvReqIdbInfo = CreateBus(Arbitrator_VlvWhlVlvReqIdbInfo, DeList);
clear DeList;

Arbitrator_VlvIdbBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'SecdBalVlvReqData_array_0'
    'SecdBalVlvReqData_array_1'
    'SecdBalVlvReqData_array_2'
    'SecdBalVlvReqData_array_3'
    'SecdBalVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    'PrimBalVlvReq'
    'SecdBalVlvReq'
    'ChmbBalVlvReq'
    'PrimBalVlvDataLen'
    'SecdBalVlvDataLen'
    'ChmbBalVlvDataLen'
    };
Arbitrator_VlvIdbBalVlvReqInfo = CreateBus(Arbitrator_VlvIdbBalVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    };
Arbitrator_VlvEemFailData = CreateBus(Arbitrator_VlvEemFailData, DeList);
clear DeList;

Arbitrator_VlvEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Arbitrator_VlvEemCtrlInhibitData = CreateBus(Arbitrator_VlvEemCtrlInhibitData, DeList);
clear DeList;

Arbitrator_VlvWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Arbitrator_VlvWhlSpdInfo = CreateBus(Arbitrator_VlvWhlSpdInfo, DeList);
clear DeList;

Arbitrator_VlvEcuModeSts = Simulink.Bus;
DeList={'Arbitrator_VlvEcuModeSts'};
Arbitrator_VlvEcuModeSts = CreateBus(Arbitrator_VlvEcuModeSts, DeList);
clear DeList;

Arbitrator_VlvIgnOnOffSts = Simulink.Bus;
DeList={'Arbitrator_VlvIgnOnOffSts'};
Arbitrator_VlvIgnOnOffSts = CreateBus(Arbitrator_VlvIgnOnOffSts, DeList);
clear DeList;

Arbitrator_VlvIgnEdgeSts = Simulink.Bus;
DeList={'Arbitrator_VlvIgnEdgeSts'};
Arbitrator_VlvIgnEdgeSts = CreateBus(Arbitrator_VlvIgnEdgeSts, DeList);
clear DeList;

Arbitrator_VlvFuncInhibitVlvSts = Simulink.Bus;
DeList={'Arbitrator_VlvFuncInhibitVlvSts'};
Arbitrator_VlvFuncInhibitVlvSts = CreateBus(Arbitrator_VlvFuncInhibitVlvSts, DeList);
clear DeList;

Arbitrator_VlvArbFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
Arbitrator_VlvArbFsrBbsDrvInfo = CreateBus(Arbitrator_VlvArbFsrBbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvArbFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
Arbitrator_VlvArbFsrAbsDrvInfo = CreateBus(Arbitrator_VlvArbFsrAbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Arbitrator_VlvArbWhlVlvReqInfo = CreateBus(Arbitrator_VlvArbWhlVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    };
Arbitrator_VlvArbNormVlvReqInfo = CreateBus(Arbitrator_VlvArbNormVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvArbBalVlvReqInfo = Simulink.Bus;
DeList={
    'BalVlvReqData_array_0'
    'BalVlvReqData_array_1'
    'BalVlvReqData_array_2'
    'BalVlvReqData_array_3'
    'BalVlvReqData_array_4'
    };
Arbitrator_VlvArbBalVlvReqInfo = CreateBus(Arbitrator_VlvArbBalVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvArbResPVlvReqInfo = Simulink.Bus;
DeList={
    'ResPVlvReqData_array_0'
    'ResPVlvReqData_array_1'
    'ResPVlvReqData_array_2'
    'ResPVlvReqData_array_3'
    'ResPVlvReqData_array_4'
    };
Arbitrator_VlvArbResPVlvReqInfo = CreateBus(Arbitrator_VlvArbResPVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvAsicEnDrDrv = Simulink.Bus;
DeList={'Arbitrator_VlvAsicEnDrDrv'};
Arbitrator_VlvAsicEnDrDrv = CreateBus(Arbitrator_VlvAsicEnDrDrv, DeList);
clear DeList;

Arbitrator_VlvArbVlvSync = Simulink.Bus;
DeList={'Arbitrator_VlvArbVlvSync'};
Arbitrator_VlvArbVlvSync = CreateBus(Arbitrator_VlvArbVlvSync, DeList);
clear DeList;

Arbitrator_VlvArbVlvDriveState = Simulink.Bus;
DeList={'Arbitrator_VlvArbVlvDriveState'};
Arbitrator_VlvArbVlvDriveState = CreateBus(Arbitrator_VlvArbVlvDriveState, DeList);
clear DeList;


Arbitrator_RlyEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    };
Arbitrator_RlyEemFailData = CreateBus(Arbitrator_RlyEemFailData, DeList);
clear DeList;

Arbitrator_RlyWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Arbitrator_RlyWhlSpdInfo = CreateBus(Arbitrator_RlyWhlSpdInfo, DeList);
clear DeList;

Arbitrator_RlyEcuModeSts = Simulink.Bus;
DeList={'Arbitrator_RlyEcuModeSts'};
Arbitrator_RlyEcuModeSts = CreateBus(Arbitrator_RlyEcuModeSts, DeList);
clear DeList;

Arbitrator_RlyIgnOnOffSts = Simulink.Bus;
DeList={'Arbitrator_RlyIgnOnOffSts'};
Arbitrator_RlyIgnOnOffSts = CreateBus(Arbitrator_RlyIgnOnOffSts, DeList);
clear DeList;

Arbitrator_RlyIgnEdgeSts = Simulink.Bus;
DeList={'Arbitrator_RlyIgnEdgeSts'};
Arbitrator_RlyIgnEdgeSts = CreateBus(Arbitrator_RlyIgnEdgeSts, DeList);
clear DeList;


Fsr_ActrArbFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
Fsr_ActrArbFsrBbsDrvInfo = CreateBus(Fsr_ActrArbFsrBbsDrvInfo, DeList);
clear DeList;

Fsr_ActrArbFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
Fsr_ActrArbFsrAbsDrvInfo = CreateBus(Fsr_ActrArbFsrAbsDrvInfo, DeList);
clear DeList;

Fsr_ActrEcuModeSts = Simulink.Bus;
DeList={'Fsr_ActrEcuModeSts'};
Fsr_ActrEcuModeSts = CreateBus(Fsr_ActrEcuModeSts, DeList);
clear DeList;

Fsr_ActrAsicEnDrDrv = Simulink.Bus;
DeList={'Fsr_ActrAsicEnDrDrv'};
Fsr_ActrAsicEnDrDrv = CreateBus(Fsr_ActrAsicEnDrDrv, DeList);
clear DeList;

Fsr_ActrFuncInhibitFsrSts = Simulink.Bus;
DeList={'Fsr_ActrFuncInhibitFsrSts'};
Fsr_ActrFuncInhibitFsrSts = CreateBus(Fsr_ActrFuncInhibitFsrSts, DeList);
clear DeList;


Rly_ActrEcuModeSts = Simulink.Bus;
DeList={'Rly_ActrEcuModeSts'};
Rly_ActrEcuModeSts = CreateBus(Rly_ActrEcuModeSts, DeList);
clear DeList;

Rly_ActrFuncInhibitRlySts = Simulink.Bus;
DeList={'Rly_ActrFuncInhibitRlySts'};
Rly_ActrFuncInhibitRlySts = CreateBus(Rly_ActrFuncInhibitRlySts, DeList);
clear DeList;


Vlv_ActrSyncArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Vlv_ActrSyncArbWhlVlvReqInfo = CreateBus(Vlv_ActrSyncArbWhlVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    };
Vlv_ActrSyncArbNormVlvReqInfo = CreateBus(Vlv_ActrSyncArbNormVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncArbBalVlvReqInfo = Simulink.Bus;
DeList={
    'BalVlvReqData_array_0'
    'BalVlvReqData_array_1'
    'BalVlvReqData_array_2'
    'BalVlvReqData_array_3'
    'BalVlvReqData_array_4'
    };
Vlv_ActrSyncArbBalVlvReqInfo = CreateBus(Vlv_ActrSyncArbBalVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncArbResPVlvReqInfo = Simulink.Bus;
DeList={
    'ResPVlvReqData_array_0'
    'ResPVlvReqData_array_1'
    'ResPVlvReqData_array_2'
    'ResPVlvReqData_array_3'
    'ResPVlvReqData_array_4'
    };
Vlv_ActrSyncArbResPVlvReqInfo = CreateBus(Vlv_ActrSyncArbResPVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncEcuModeSts = Simulink.Bus;
DeList={'Vlv_ActrSyncEcuModeSts'};
Vlv_ActrSyncEcuModeSts = CreateBus(Vlv_ActrSyncEcuModeSts, DeList);
clear DeList;

Vlv_ActrSyncArbVlvSync = Simulink.Bus;
DeList={'Vlv_ActrSyncArbVlvSync'};
Vlv_ActrSyncArbVlvSync = CreateBus(Vlv_ActrSyncArbVlvSync, DeList);
clear DeList;

Vlv_ActrSyncFuncInhibitVlvSts = Simulink.Bus;
DeList={'Vlv_ActrSyncFuncInhibitVlvSts'};
Vlv_ActrSyncFuncInhibitVlvSts = CreateBus(Vlv_ActrSyncFuncInhibitVlvSts, DeList);
clear DeList;


Ioc_OutputCS5msEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputCS5msEcuModeSts'};
Ioc_OutputCS5msEcuModeSts = CreateBus(Ioc_OutputCS5msEcuModeSts, DeList);
clear DeList;

Ioc_OutputCS5msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputCS5msFuncInhibitIocSts'};
Ioc_OutputCS5msFuncInhibitIocSts = CreateBus(Ioc_OutputCS5msFuncInhibitIocSts, DeList);
clear DeList;


