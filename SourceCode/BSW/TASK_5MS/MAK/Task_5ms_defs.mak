# \file
#
# \brief Task_5ms
#
# This file contains the implementation of the SWC
# module Task_5ms.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Task_5ms_CORE_PATH     := $(MANDO_BSW_ROOT)\Task_5ms
Task_5ms_CAL_PATH      := $(Task_5ms_CORE_PATH)\CAL
Task_5ms_SRC_PATH      := $(Task_5ms_CORE_PATH)\SRC
Task_5ms_CFG_PATH      := $(Task_5ms_CORE_PATH)\CFG\$(Task_5ms_VARIANT)
Task_5ms_HDR_PATH      := $(Task_5ms_CORE_PATH)\HDR
Task_5ms_IFA_PATH      := $(Task_5ms_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Task_5ms_CMN_PATH      := $(Task_5ms_CORE_PATH)\ICE\CMN
endif

CC_INCLUDE_PATH    += $(Task_5ms_CAL_PATH)
CC_INCLUDE_PATH    += $(Task_5ms_SRC_PATH)
CC_INCLUDE_PATH    += $(Task_5ms_CFG_PATH)
CC_INCLUDE_PATH    += $(Task_5ms_HDR_PATH)
CC_INCLUDE_PATH    += $(Task_5ms_IFA_PATH)
CC_INCLUDE_PATH    += $(Task_5ms_CMN_PATH)

