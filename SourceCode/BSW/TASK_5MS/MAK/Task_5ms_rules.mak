# \file
#
# \brief Task_5ms
#
# This file contains the implementation of the SWC
# module Task_5ms.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Task_5ms_src

Task_5ms_src_FILES        += $(Task_5ms_SRC_PATH)\Task_5ms.c
Task_5ms_src_FILES        += $(Task_5ms_IFA_PATH)\Task_5ms_Ifa.c
Task_5ms_src_FILES        += $(Task_5ms_CFG_PATH)\Task_5ms_Cfg.c
Task_5ms_src_FILES        += $(Task_5ms_CAL_PATH)\Task_5ms_Cal.c
