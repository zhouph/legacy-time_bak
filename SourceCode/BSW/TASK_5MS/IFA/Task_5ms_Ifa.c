/**
 * @defgroup Task_5ms_Ifa Task_5ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_5ms_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_5ms_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TASK_5MS_START_SEC_CONST_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define TASK_5MS_STOP_SEC_CONST_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_5ms_MemMap.h"
#define TASK_5MS_START_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_5MS_STOP_SEC_VAR_32BIT
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TASK_5MS_START_SEC_CODE
#include "Task_5ms_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TASK_5MS_STOP_SEC_CODE
#include "Task_5ms_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
