/**
 * @defgroup AyM_Main AyM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYM_MAIN_H_
#define AYM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyM_Types.h"
#include "AyM_Cfg.h"
#include "AyM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define AYM_MAIN_MODULE_ID      (0)
 #define AYM_MAIN_MAJOR_VERSION  (2)
 #define AYM_MAIN_MINOR_VERSION  (0)
 #define AYM_MAIN_PATCH_VERSION  (0)
 #define AYM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AyM_Main_HdrBusType AyM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t AyM_MainVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxEscInfo_t AyM_MainCanRxEscInfo;
extern Proxy_RxByComRxYawSerialInfo_t AyM_MainRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t AyM_MainRxYawAccInfo;
extern Proxy_RxByComRxLongAccInfo_t AyM_MainRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t AyM_MainRxMsgOkFlgInfo;
extern SenPwrM_MainSenPwrMonitor_t AyM_MainSenPwrMonitorData;
extern Eem_MainEemFailData_t AyM_MainEemFailData;
extern Mom_HndlrEcuModeSts_t AyM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t AyM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t AyM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t AyM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t AyM_MainDiagClrSrs;

/* Output Data Element */
extern AyM_MainAYMOutInfo_t AyM_MainAYMOuitInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AyM_Main_Init(void);
extern void AyM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AYM_MAIN_H_ */
/** @} */
