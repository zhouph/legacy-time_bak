/**
 * @defgroup AyM_Types AyM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYM_TYPES_H_
#define AYM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxEscInfo_t AyM_MainCanRxEscInfo;
    Proxy_RxByComRxYawSerialInfo_t AyM_MainRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t AyM_MainRxYawAccInfo;
    Proxy_RxByComRxLongAccInfo_t AyM_MainRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t AyM_MainRxMsgOkFlgInfo;
    SenPwrM_MainSenPwrMonitor_t AyM_MainSenPwrMonitorData;
    Eem_MainEemFailData_t AyM_MainEemFailData;
    Mom_HndlrEcuModeSts_t AyM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AyM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AyM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AyM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t AyM_MainDiagClrSrs;

/* Output Data Element */
    AyM_MainAYMOutInfo_t AyM_MainAYMOuitInfo;
}AyM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AYM_TYPES_H_ */
/** @} */
