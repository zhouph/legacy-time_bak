AyM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ay'
    };
AyM_MainCanRxEscInfo = CreateBus(AyM_MainCanRxEscInfo, DeList);
clear DeList;

AyM_MainRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
AyM_MainRxYawSerialInfo = CreateBus(AyM_MainRxYawSerialInfo, DeList);
clear DeList;

AyM_MainRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
AyM_MainRxYawAccInfo = CreateBus(AyM_MainRxYawAccInfo, DeList);
clear DeList;

AyM_MainRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    };
AyM_MainRxLongAccInfo = CreateBus(AyM_MainRxLongAccInfo, DeList);
clear DeList;

AyM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'LongAccMsgOkFlg'
    };
AyM_MainRxMsgOkFlgInfo = CreateBus(AyM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

AyM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AyM_MainSenPwrMonitorData = CreateBus(AyM_MainSenPwrMonitorData, DeList);
clear DeList;

AyM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
AyM_MainEemFailData = CreateBus(AyM_MainEemFailData, DeList);
clear DeList;

AyM_MainEcuModeSts = Simulink.Bus;
DeList={'AyM_MainEcuModeSts'};
AyM_MainEcuModeSts = CreateBus(AyM_MainEcuModeSts, DeList);
clear DeList;

AyM_MainIgnOnOffSts = Simulink.Bus;
DeList={'AyM_MainIgnOnOffSts'};
AyM_MainIgnOnOffSts = CreateBus(AyM_MainIgnOnOffSts, DeList);
clear DeList;

AyM_MainIgnEdgeSts = Simulink.Bus;
DeList={'AyM_MainIgnEdgeSts'};
AyM_MainIgnEdgeSts = CreateBus(AyM_MainIgnEdgeSts, DeList);
clear DeList;

AyM_MainVBatt1Mon = Simulink.Bus;
DeList={'AyM_MainVBatt1Mon'};
AyM_MainVBatt1Mon = CreateBus(AyM_MainVBatt1Mon, DeList);
clear DeList;

AyM_MainDiagClrSrs = Simulink.Bus;
DeList={'AyM_MainDiagClrSrs'};
AyM_MainDiagClrSrs = CreateBus(AyM_MainDiagClrSrs, DeList);
clear DeList;

AyM_MainAYMOuitInfo = Simulink.Bus;
DeList={
    'AYM_Timeout_Err'
    'AYM_Invalid_Err'
    'AYM_CRC_Err'
    'AYM_Rolling_Err'
    'AYM_Temperature_Err'
    'AYM_Range_Err'
    };
AyM_MainAYMOuitInfo = CreateBus(AyM_MainAYMOuitInfo, DeList);
clear DeList;

