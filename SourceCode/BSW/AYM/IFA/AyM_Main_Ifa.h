/**
 * @defgroup AyM_Main_Ifa AyM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYM_MAIN_IFA_H_
#define AYM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AyM_Main_Read_AyM_MainCanRxEscInfo(data) do \
{ \
    *data = AyM_MainCanRxEscInfo; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawSerialInfo(data) do \
{ \
    *data = AyM_MainRxYawSerialInfo; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo(data) do \
{ \
    *data = AyM_MainRxYawAccInfo; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo(data) do \
{ \
    *data = AyM_MainRxLongAccInfo; \
}while(0);

#define AyM_Main_Read_AyM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = AyM_MainRxMsgOkFlgInfo; \
}while(0);

#define AyM_Main_Read_AyM_MainSenPwrMonitorData(data) do \
{ \
    *data = AyM_MainSenPwrMonitorData; \
}while(0);

#define AyM_Main_Read_AyM_MainEemFailData(data) do \
{ \
    *data = AyM_MainEemFailData; \
}while(0);

#define AyM_Main_Read_AyM_MainCanRxEscInfo_Ay(data) do \
{ \
    *data = AyM_MainCanRxEscInfo.Ay; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = AyM_MainRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = AyM_MainRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = AyM_MainRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.YawRateValidData; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Gyro_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Raster_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Eep_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Asic_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Accel_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Ram_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Rom_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Ad_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Osc_Fail; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.RollingCounter; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.Can_Func_Err; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_LatAccValidData(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.LatAccValidData; \
}while(0);

#define AyM_Main_Read_AyM_MainRxYawAccInfo_LatAccSelfTestStatus(data) do \
{ \
    *data = AyM_MainRxYawAccInfo.LatAccSelfTestStatus; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = AyM_MainRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = AyM_MainRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = AyM_MainRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = AyM_MainRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = AyM_MainRxLongAccInfo.LongRollingCounter; \
}while(0);

#define AyM_Main_Read_AyM_MainRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = AyM_MainRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define AyM_Main_Read_AyM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define AyM_Main_Read_AyM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define AyM_Main_Read_AyM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define AyM_Main_Read_AyM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = AyM_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define AyM_Main_Read_AyM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = AyM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = AyM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = AyM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = AyM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define AyM_Main_Read_AyM_MainEcuModeSts(data) do \
{ \
    *data = AyM_MainEcuModeSts; \
}while(0);

#define AyM_Main_Read_AyM_MainIgnOnOffSts(data) do \
{ \
    *data = AyM_MainIgnOnOffSts; \
}while(0);

#define AyM_Main_Read_AyM_MainIgnEdgeSts(data) do \
{ \
    *data = AyM_MainIgnEdgeSts; \
}while(0);

#define AyM_Main_Read_AyM_MainVBatt1Mon(data) do \
{ \
    *data = AyM_MainVBatt1Mon; \
}while(0);

#define AyM_Main_Read_AyM_MainDiagClrSrs(data) do \
{ \
    *data = AyM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define AyM_Main_Write_AyM_MainAYMOuitInfo(data) do \
{ \
    AyM_MainAYMOuitInfo = *data; \
}while(0);

#define AyM_Main_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define AyM_Main_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define AyM_Main_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define AyM_Main_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define AyM_Main_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define AyM_Main_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AYM_MAIN_IFA_H_ */
/** @} */
