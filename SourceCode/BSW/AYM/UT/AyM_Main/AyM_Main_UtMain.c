#include "unity.h"
#include "unity_fixture.h"
#include "AyM_Main.h"
#include "AyM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_AyM_MainCanRxEscInfo_Ay[MAX_STEP] = AYM_MAINCANRXESCINFO_AY;
const Saluint8 UtInput_AyM_MainRxYawSerialInfo_YawSerialNum_0[MAX_STEP] = AYM_MAINRXYAWSERIALINFO_YAWSERIALNUM_0;
const Saluint8 UtInput_AyM_MainRxYawSerialInfo_YawSerialNum_1[MAX_STEP] = AYM_MAINRXYAWSERIALINFO_YAWSERIALNUM_1;
const Saluint8 UtInput_AyM_MainRxYawSerialInfo_YawSerialNum_2[MAX_STEP] = AYM_MAINRXYAWSERIALINFO_YAWSERIALNUM_2;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_YawRateValidData[MAX_STEP] = AYM_MAINRXYAWACCINFO_YAWRATEVALIDDATA;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_YawRateSelfTestStatus[MAX_STEP] = AYM_MAINRXYAWACCINFO_YAWRATESELFTESTSTATUS;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_SensorOscFreqDev[MAX_STEP] = AYM_MAINRXYAWACCINFO_SENSOROSCFREQDEV;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Gyro_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_GYRO_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Raster_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_RASTER_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Eep_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_EEP_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Batt_Range_Err[MAX_STEP] = AYM_MAINRXYAWACCINFO_BATT_RANGE_ERR;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Asic_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_ASIC_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Accel_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_ACCEL_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Ram_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_RAM_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Rom_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_ROM_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Ad_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_AD_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Osc_Fail[MAX_STEP] = AYM_MAINRXYAWACCINFO_OSC_FAIL;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Watchdog_Rst[MAX_STEP] = AYM_MAINRXYAWACCINFO_WATCHDOG_RST;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Plaus_Err_Pst[MAX_STEP] = AYM_MAINRXYAWACCINFO_PLAUS_ERR_PST;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_RollingCounter[MAX_STEP] = AYM_MAINRXYAWACCINFO_ROLLINGCOUNTER;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_Can_Func_Err[MAX_STEP] = AYM_MAINRXYAWACCINFO_CAN_FUNC_ERR;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_LatAccValidData[MAX_STEP] = AYM_MAINRXYAWACCINFO_LATACCVALIDDATA;
const Saluint8 UtInput_AyM_MainRxYawAccInfo_LatAccSelfTestStatus[MAX_STEP] = AYM_MAINRXYAWACCINFO_LATACCSELFTESTSTATUS;
const Saluint8 UtInput_AyM_MainRxLongAccInfo_IntSenFltSymtmActive[MAX_STEP] = AYM_MAINRXLONGACCINFO_INTSENFLTSYMTMACTIVE;
const Saluint8 UtInput_AyM_MainRxLongAccInfo_IntSenFaultPresent[MAX_STEP] = AYM_MAINRXLONGACCINFO_INTSENFAULTPRESENT;
const Saluint8 UtInput_AyM_MainRxLongAccInfo_LongAccSenCirErrPre[MAX_STEP] = AYM_MAINRXLONGACCINFO_LONGACCSENCIRERRPRE;
const Saluint8 UtInput_AyM_MainRxLongAccInfo_LonACSenRanChkErrPre[MAX_STEP] = AYM_MAINRXLONGACCINFO_LONACSENRANCHKERRPRE;
const Saluint8 UtInput_AyM_MainRxLongAccInfo_LongRollingCounter[MAX_STEP] = AYM_MAINRXLONGACCINFO_LONGROLLINGCOUNTER;
const Saluint8 UtInput_AyM_MainRxLongAccInfo_IntTempSensorFault[MAX_STEP] = AYM_MAINRXLONGACCINFO_INTTEMPSENSORFAULT;
const Saluint8 UtInput_AyM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[MAX_STEP] = AYM_MAINRXMSGOKFLGINFO_YAWSERIALMSGOKFLG;
const Saluint8 UtInput_AyM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[MAX_STEP] = AYM_MAINRXMSGOKFLGINFO_YAWACCMSGOKFLG;
const Saluint8 UtInput_AyM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[MAX_STEP] = AYM_MAINRXMSGOKFLGINFO_LONGACCMSGOKFLG;
const Saluint8 UtInput_AyM_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = AYM_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_AyM_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = AYM_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_AyM_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = AYM_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_AyM_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = AYM_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_AyM_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = AYM_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_AyM_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = AYM_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Mom_HndlrEcuModeSts_t UtInput_AyM_MainEcuModeSts[MAX_STEP] = AYM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_AyM_MainIgnOnOffSts[MAX_STEP] = AYM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_AyM_MainIgnEdgeSts[MAX_STEP] = AYM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_AyM_MainVBatt1Mon[MAX_STEP] = AYM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_AyM_MainDiagClrSrs[MAX_STEP] = AYM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_AyM_MainAYMOuitInfo_AYM_Timeout_Err[MAX_STEP] = AYM_MAINAYMOUITINFO_AYM_TIMEOUT_ERR;
const Saluint8 UtExpected_AyM_MainAYMOuitInfo_AYM_Invalid_Err[MAX_STEP] = AYM_MAINAYMOUITINFO_AYM_INVALID_ERR;
const Saluint8 UtExpected_AyM_MainAYMOuitInfo_AYM_CRC_Err[MAX_STEP] = AYM_MAINAYMOUITINFO_AYM_CRC_ERR;
const Saluint8 UtExpected_AyM_MainAYMOuitInfo_AYM_Rolling_Err[MAX_STEP] = AYM_MAINAYMOUITINFO_AYM_ROLLING_ERR;
const Saluint8 UtExpected_AyM_MainAYMOuitInfo_AYM_Temperature_Err[MAX_STEP] = AYM_MAINAYMOUITINFO_AYM_TEMPERATURE_ERR;
const Saluint8 UtExpected_AyM_MainAYMOuitInfo_AYM_Range_Err[MAX_STEP] = AYM_MAINAYMOUITINFO_AYM_RANGE_ERR;



TEST_GROUP(AyM_Main);
TEST_SETUP(AyM_Main)
{
    AyM_Main_Init();
}

TEST_TEAR_DOWN(AyM_Main)
{   /* Postcondition */

}

TEST(AyM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        AyM_MainCanRxEscInfo.Ay = UtInput_AyM_MainCanRxEscInfo_Ay[i];
        AyM_MainRxYawSerialInfo.YawSerialNum_0 = UtInput_AyM_MainRxYawSerialInfo_YawSerialNum_0[i];
        AyM_MainRxYawSerialInfo.YawSerialNum_1 = UtInput_AyM_MainRxYawSerialInfo_YawSerialNum_1[i];
        AyM_MainRxYawSerialInfo.YawSerialNum_2 = UtInput_AyM_MainRxYawSerialInfo_YawSerialNum_2[i];
        AyM_MainRxYawAccInfo.YawRateValidData = UtInput_AyM_MainRxYawAccInfo_YawRateValidData[i];
        AyM_MainRxYawAccInfo.YawRateSelfTestStatus = UtInput_AyM_MainRxYawAccInfo_YawRateSelfTestStatus[i];
        AyM_MainRxYawAccInfo.SensorOscFreqDev = UtInput_AyM_MainRxYawAccInfo_SensorOscFreqDev[i];
        AyM_MainRxYawAccInfo.Gyro_Fail = UtInput_AyM_MainRxYawAccInfo_Gyro_Fail[i];
        AyM_MainRxYawAccInfo.Raster_Fail = UtInput_AyM_MainRxYawAccInfo_Raster_Fail[i];
        AyM_MainRxYawAccInfo.Eep_Fail = UtInput_AyM_MainRxYawAccInfo_Eep_Fail[i];
        AyM_MainRxYawAccInfo.Batt_Range_Err = UtInput_AyM_MainRxYawAccInfo_Batt_Range_Err[i];
        AyM_MainRxYawAccInfo.Asic_Fail = UtInput_AyM_MainRxYawAccInfo_Asic_Fail[i];
        AyM_MainRxYawAccInfo.Accel_Fail = UtInput_AyM_MainRxYawAccInfo_Accel_Fail[i];
        AyM_MainRxYawAccInfo.Ram_Fail = UtInput_AyM_MainRxYawAccInfo_Ram_Fail[i];
        AyM_MainRxYawAccInfo.Rom_Fail = UtInput_AyM_MainRxYawAccInfo_Rom_Fail[i];
        AyM_MainRxYawAccInfo.Ad_Fail = UtInput_AyM_MainRxYawAccInfo_Ad_Fail[i];
        AyM_MainRxYawAccInfo.Osc_Fail = UtInput_AyM_MainRxYawAccInfo_Osc_Fail[i];
        AyM_MainRxYawAccInfo.Watchdog_Rst = UtInput_AyM_MainRxYawAccInfo_Watchdog_Rst[i];
        AyM_MainRxYawAccInfo.Plaus_Err_Pst = UtInput_AyM_MainRxYawAccInfo_Plaus_Err_Pst[i];
        AyM_MainRxYawAccInfo.RollingCounter = UtInput_AyM_MainRxYawAccInfo_RollingCounter[i];
        AyM_MainRxYawAccInfo.Can_Func_Err = UtInput_AyM_MainRxYawAccInfo_Can_Func_Err[i];
        AyM_MainRxYawAccInfo.LatAccValidData = UtInput_AyM_MainRxYawAccInfo_LatAccValidData[i];
        AyM_MainRxYawAccInfo.LatAccSelfTestStatus = UtInput_AyM_MainRxYawAccInfo_LatAccSelfTestStatus[i];
        AyM_MainRxLongAccInfo.IntSenFltSymtmActive = UtInput_AyM_MainRxLongAccInfo_IntSenFltSymtmActive[i];
        AyM_MainRxLongAccInfo.IntSenFaultPresent = UtInput_AyM_MainRxLongAccInfo_IntSenFaultPresent[i];
        AyM_MainRxLongAccInfo.LongAccSenCirErrPre = UtInput_AyM_MainRxLongAccInfo_LongAccSenCirErrPre[i];
        AyM_MainRxLongAccInfo.LonACSenRanChkErrPre = UtInput_AyM_MainRxLongAccInfo_LonACSenRanChkErrPre[i];
        AyM_MainRxLongAccInfo.LongRollingCounter = UtInput_AyM_MainRxLongAccInfo_LongRollingCounter[i];
        AyM_MainRxLongAccInfo.IntTempSensorFault = UtInput_AyM_MainRxLongAccInfo_IntTempSensorFault[i];
        AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = UtInput_AyM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[i];
        AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = UtInput_AyM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[i];
        AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = UtInput_AyM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[i];
        AyM_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_AyM_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_AyM_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        AyM_MainEemFailData.Eem_Fail_WssFL = UtInput_AyM_MainEemFailData_Eem_Fail_WssFL[i];
        AyM_MainEemFailData.Eem_Fail_WssFR = UtInput_AyM_MainEemFailData_Eem_Fail_WssFR[i];
        AyM_MainEemFailData.Eem_Fail_WssRL = UtInput_AyM_MainEemFailData_Eem_Fail_WssRL[i];
        AyM_MainEemFailData.Eem_Fail_WssRR = UtInput_AyM_MainEemFailData_Eem_Fail_WssRR[i];
        AyM_MainEcuModeSts = UtInput_AyM_MainEcuModeSts[i];
        AyM_MainIgnOnOffSts = UtInput_AyM_MainIgnOnOffSts[i];
        AyM_MainIgnEdgeSts = UtInput_AyM_MainIgnEdgeSts[i];
        AyM_MainVBatt1Mon = UtInput_AyM_MainVBatt1Mon[i];
        AyM_MainDiagClrSrs = UtInput_AyM_MainDiagClrSrs[i];

        AyM_Main();

        TEST_ASSERT_EQUAL(AyM_MainAYMOuitInfo.AYM_Timeout_Err, UtExpected_AyM_MainAYMOuitInfo_AYM_Timeout_Err[i]);
        TEST_ASSERT_EQUAL(AyM_MainAYMOuitInfo.AYM_Invalid_Err, UtExpected_AyM_MainAYMOuitInfo_AYM_Invalid_Err[i]);
        TEST_ASSERT_EQUAL(AyM_MainAYMOuitInfo.AYM_CRC_Err, UtExpected_AyM_MainAYMOuitInfo_AYM_CRC_Err[i]);
        TEST_ASSERT_EQUAL(AyM_MainAYMOuitInfo.AYM_Rolling_Err, UtExpected_AyM_MainAYMOuitInfo_AYM_Rolling_Err[i]);
        TEST_ASSERT_EQUAL(AyM_MainAYMOuitInfo.AYM_Temperature_Err, UtExpected_AyM_MainAYMOuitInfo_AYM_Temperature_Err[i]);
        TEST_ASSERT_EQUAL(AyM_MainAYMOuitInfo.AYM_Range_Err, UtExpected_AyM_MainAYMOuitInfo_AYM_Range_Err[i]);
    }
}

TEST_GROUP_RUNNER(AyM_Main)
{
    RUN_TEST_CASE(AyM_Main, All);
}
