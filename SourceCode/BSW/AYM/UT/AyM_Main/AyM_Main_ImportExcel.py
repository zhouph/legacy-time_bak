import xlrd

def get_stripped_cell_value(sheet, row, col):
    return str(sheet.cell_value(row, col)).strip(" \t\n")

if __name__ == '__main__':
    book = xlrd.open_workbook('AyM_MainTst.xlsx')
    sheet = book.sheet_by_index(0)
    f = open('AyM_Main_UtMain.h','w')

    UT_MAX_STEP = sheet.nrows - 1
    f.write("#define UT_MAX_STEP "+str(UT_MAX_STEP)+"\n")

    for col in range(sheet.ncols)[1:]:
        f.write("#define " + get_stripped_cell_value(sheet,0,col).upper() + " {")
        for row in range(sheet.nrows)[1:]:
            f.write(str(int(round(sheet.cell_value(row,col))))+",")
        f.write("}\n")
    f.close()