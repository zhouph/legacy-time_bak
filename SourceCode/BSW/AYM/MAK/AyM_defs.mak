# \file
#
# \brief AyM
#
# This file contains the implementation of the SWC
# module AyM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

AyM_CORE_PATH     := $(MANDO_BSW_ROOT)\AyM
AyM_CAL_PATH      := $(AyM_CORE_PATH)\CAL\$(AyM_VARIANT)
AyM_SRC_PATH      := $(AyM_CORE_PATH)\SRC
AyM_CFG_PATH      := $(AyM_CORE_PATH)\CFG\$(AyM_VARIANT)
AyM_HDR_PATH      := $(AyM_CORE_PATH)\HDR
AyM_IFA_PATH      := $(AyM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    AyM_CMN_PATH      := $(AyM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	AyM_UT_PATH		:= $(AyM_CORE_PATH)\UT
	AyM_UNITY_PATH	:= $(AyM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(AyM_UT_PATH)
	CC_INCLUDE_PATH		+= $(AyM_UNITY_PATH)
	AyM_Main_PATH 	:= AyM_UT_PATH\AyM_Main
endif
CC_INCLUDE_PATH    += $(AyM_CAL_PATH)
CC_INCLUDE_PATH    += $(AyM_SRC_PATH)
CC_INCLUDE_PATH    += $(AyM_CFG_PATH)
CC_INCLUDE_PATH    += $(AyM_HDR_PATH)
CC_INCLUDE_PATH    += $(AyM_IFA_PATH)
CC_INCLUDE_PATH    += $(AyM_CMN_PATH)

