# \file
#
# \brief AyM
#
# This file contains the implementation of the SWC
# module AyM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += AyM_src

AyM_src_FILES        += $(AyM_SRC_PATH)\AyM_Main.c
AyM_src_FILES        += $(AyM_SRC_PATH)\AyM_Processing.c
AyM_src_FILES        += $(AyM_IFA_PATH)\AyM_Main_Ifa.c
AyM_src_FILES        += $(AyM_CFG_PATH)\AyM_Cfg.c
AyM_src_FILES        += $(AyM_CAL_PATH)\AyM_Cal.c

ifeq ($(ICE_COMPILE),true)
AyM_src_FILES        += $(AyM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	AyM_src_FILES        += $(AyM_UNITY_PATH)\unity.c
	AyM_src_FILES        += $(AyM_UNITY_PATH)\unity_fixture.c	
	AyM_src_FILES        += $(AyM_UT_PATH)\main.c
	AyM_src_FILES        += $(AyM_UT_PATH)\AyM_Main\AyM_Main_UtMain.c
endif