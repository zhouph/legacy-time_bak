/**
 * @defgroup AyM_Cfg AyM_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyM_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyM_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AYM_START_SEC_CONST_UNSPECIFIED
#include "AyM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AYM_STOP_SEC_CONST_UNSPECIFIED
#include "AyM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_START_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYM_STOP_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
#define AYM_START_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYM_STOP_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_START_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/** Variable Section (32BIT)**/


#define AYM_STOP_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_START_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYM_STOP_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
#define AYM_START_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYM_STOP_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_START_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/** Variable Section (32BIT)**/


#define AYM_STOP_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AYM_START_SEC_CODE
#include "AyM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AYM_STOP_SEC_CODE
#include "AyM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
