#define S_FUNCTION_NAME      AyM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          43
#define WidthOutputPort         6

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "AyM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    AyM_MainCanRxEscInfo.Ay = input[0];
    AyM_MainRxYawSerialInfo.YawSerialNum_0 = input[1];
    AyM_MainRxYawSerialInfo.YawSerialNum_1 = input[2];
    AyM_MainRxYawSerialInfo.YawSerialNum_2 = input[3];
    AyM_MainRxYawAccInfo.YawRateValidData = input[4];
    AyM_MainRxYawAccInfo.YawRateSelfTestStatus = input[5];
    AyM_MainRxYawAccInfo.SensorOscFreqDev = input[6];
    AyM_MainRxYawAccInfo.Gyro_Fail = input[7];
    AyM_MainRxYawAccInfo.Raster_Fail = input[8];
    AyM_MainRxYawAccInfo.Eep_Fail = input[9];
    AyM_MainRxYawAccInfo.Batt_Range_Err = input[10];
    AyM_MainRxYawAccInfo.Asic_Fail = input[11];
    AyM_MainRxYawAccInfo.Accel_Fail = input[12];
    AyM_MainRxYawAccInfo.Ram_Fail = input[13];
    AyM_MainRxYawAccInfo.Rom_Fail = input[14];
    AyM_MainRxYawAccInfo.Ad_Fail = input[15];
    AyM_MainRxYawAccInfo.Osc_Fail = input[16];
    AyM_MainRxYawAccInfo.Watchdog_Rst = input[17];
    AyM_MainRxYawAccInfo.Plaus_Err_Pst = input[18];
    AyM_MainRxYawAccInfo.RollingCounter = input[19];
    AyM_MainRxYawAccInfo.Can_Func_Err = input[20];
    AyM_MainRxYawAccInfo.LatAccValidData = input[21];
    AyM_MainRxYawAccInfo.LatAccSelfTestStatus = input[22];
    AyM_MainRxLongAccInfo.IntSenFltSymtmActive = input[23];
    AyM_MainRxLongAccInfo.IntSenFaultPresent = input[24];
    AyM_MainRxLongAccInfo.LongAccSenCirErrPre = input[25];
    AyM_MainRxLongAccInfo.LonACSenRanChkErrPre = input[26];
    AyM_MainRxLongAccInfo.LongRollingCounter = input[27];
    AyM_MainRxLongAccInfo.IntTempSensorFault = input[28];
    AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = input[29];
    AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = input[30];
    AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = input[31];
    AyM_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[32];
    AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[33];
    AyM_MainEemFailData.Eem_Fail_WssFL = input[34];
    AyM_MainEemFailData.Eem_Fail_WssFR = input[35];
    AyM_MainEemFailData.Eem_Fail_WssRL = input[36];
    AyM_MainEemFailData.Eem_Fail_WssRR = input[37];
    AyM_MainEcuModeSts = input[38];
    AyM_MainIgnOnOffSts = input[39];
    AyM_MainIgnEdgeSts = input[40];
    AyM_MainVBatt1Mon = input[41];
    AyM_MainDiagClrSrs = input[42];

    AyM_Main();


    output[0] = AyM_MainAYMOuitInfo.AYM_Timeout_Err;
    output[1] = AyM_MainAYMOuitInfo.AYM_Invalid_Err;
    output[2] = AyM_MainAYMOuitInfo.AYM_CRC_Err;
    output[3] = AyM_MainAYMOuitInfo.AYM_Rolling_Err;
    output[4] = AyM_MainAYMOuitInfo.AYM_Temperature_Err;
    output[5] = AyM_MainAYMOuitInfo.AYM_Range_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    AyM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
