/**
 * @defgroup AyM_Main AyM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyM_Processing.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyM_Main.h"
#include "AyM_Processing.h"

#include "common.h"

#include <stdlib.h>

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AYM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AyM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AYM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AyM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/** Variable Section (32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_32BIT
#include "AyM_MemMap.h"

extern signed int Proxy_fys16EstimatedAY;
extern Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo; /* TODO */

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/** Variable Section (32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_32BIT
#include "AyM_MemMap.h"

static boolean gAyM_bInit;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AYM_MAIN_START_SEC_CODE
#include "AyM_MemMap.h"

static boolean AyM_CheckInhibit(AyM_Main_HdrBusType *pAyMInfo);
static void AyM_CheckTimeout(AyM_Main_HdrBusType *pAyMInfo);
static void AyM_CheckSum(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit);
static void AyM_RollingCnt(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit);
static void AyM_Invalid(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit);
static void AyM_Temperature(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit);
static void AyM_Range(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AyM_Processing(AyM_Main_HdrBusType *pAyMInfo)
{
    boolean bInhibit = FALSE;
        
    if(gAyM_bInit == TRUE)
    {
        AyM_CheckTimeout(pAyMInfo);
        
        bInhibit = AyM_CheckInhibit(pAyMInfo);

        AyM_CheckSum(pAyMInfo, bInhibit);
        AyM_RollingCnt(pAyMInfo, bInhibit);
        AyM_Invalid(pAyMInfo, bInhibit);
        AyM_Temperature(pAyMInfo, bInhibit);
        AyM_Range(pAyMInfo, bInhibit);
    }
}

void AyM_Initialize(void)
{
    gAyM_bInit = TRUE;
}


/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static boolean AyM_CheckInhibit(AyM_Main_HdrBusType *pAyMInfo)
{
    boolean bInhibit = FALSE;
    
    if((pAyMInfo->AyM_MainAYMOuitInfo.AYM_Timeout_Err == ERR_PREFAILED) ||
        (pAyMInfo->AyM_MainAYMOuitInfo.AYM_Timeout_Err == ERR_INHIBIT)
      )
    {
        bInhibit = TRUE;
    }

    return bInhibit;
}

static void AyM_CheckTimeout(AyM_Main_HdrBusType *pAyMInfo)
{
    boolean inhibit = FALSE;

    if((pAyMInfo->AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable == FALSE) ||
       (Proxy_RxCanRxInfo.SenBusOffFlag == TRUE)
      )
    {
        inhibit = TRUE;
    }
    
    if(pAyMInfo->AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg == 1)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Timeout_Err = ERR_PREPASSED;
    }
    else
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Timeout_Err = ERR_PREFAILED;
    }

    if(inhibit==TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Timeout_Err = ERR_INHIBIT;
    }
}

static void AyM_CheckSum(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit)
{
    pAyMInfo->AyM_MainAYMOuitInfo.AYM_CRC_Err = NONE; /* for GM Variant */
    
    if(inhibit==TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_CRC_Err = ERR_INHIBIT;
    }
}

static void AyM_RollingCnt(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit)
{
    pAyMInfo->AyM_MainAYMOuitInfo.AYM_Rolling_Err = NONE; /* TODO New Concept */

    if(inhibit==TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Rolling_Err = ERR_INHIBIT;
    }
}

static void AyM_Invalid(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit)
{
    boolean bAyInvalid = FALSE;

    if((pAyMInfo->AyM_MainRxYawAccInfo.YawRateValidData == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Can_Func_Err == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Gyro_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Eep_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Batt_Range_Err == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Asic_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Accel_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Ram_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Rom_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Ad_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Osc_Fail == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Plaus_Err_Pst == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Can_Func_Err == 1) ||
       (pAyMInfo->AyM_MainRxYawAccInfo.Raster_Fail == 1)
      )
    {
        bAyInvalid = TRUE;
    }

    if(bAyInvalid == TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Invalid_Err = ERR_PREFAILED;
    }
    else
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Invalid_Err = ERR_PREPASSED;
    }

    if(inhibit==TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Invalid_Err = ERR_INHIBIT;
    }
}

static void AyM_Temperature(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit)
{
    sint8 s8RawValue = 0;

    s8RawValue = (sint8)(pAyMInfo->AyM_MainRxYawAccInfo.SensorOscFreqDev);
    
    if((s8RawValue > AY_RAST_Hz_TEMP_M55_r0_5) || (s8RawValue < AY_RAST_Hz_TEMP_P100_r0_5))
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Temperature_Err = ERR_PREFAILED;
    }
    else
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Temperature_Err = ERR_PREPASSED;
    }

    if(inhibit==TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Temperature_Err = ERR_INHIBIT;
    }
}

static void AyM_Range(AyM_Main_HdrBusType *pAyMInfo, boolean inhibit)
{
    unsigned int abs_u16EstimatedAY = 0;

    abs_u16EstimatedAY = abs(Proxy_fys16EstimatedAY);

    /* TODO : New Concept
                    if((rev_for_judge_time<=0)&&(fu1CLSTEEROK==1)&&(Temp_Abs_STR_Mdl_0_<U16_STEER_300DEG))
    */
    
    if(abs_u16EstimatedAY >= AY_RANGE_ERR_DET_THRES)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Range_Err = ERR_PREFAILED;
    }
    else
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Range_Err = ERR_PREPASSED;
    }
    
    if(inhibit==TRUE)
    {
        pAyMInfo->AyM_MainAYMOuitInfo.AYM_Range_Err = ERR_INHIBIT;
    }
}

#define AYM_MAIN_STOP_SEC_CODE
#include "AyM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
