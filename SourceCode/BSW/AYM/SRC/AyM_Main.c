/**
 * @defgroup AyM_Main AyM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyM_Main.h"
#include "AyM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AYM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AyM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AYM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AyM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AyM_Main_HdrBusType AyM_MainBus;

/* Version Info */
const SwcVersionInfo_t AyM_MainVersionInfo = 
{   
    AYM_MAIN_MODULE_ID,           /* AyM_MainVersionInfo.ModuleId */
    AYM_MAIN_MAJOR_VERSION,       /* AyM_MainVersionInfo.MajorVer */
    AYM_MAIN_MINOR_VERSION,       /* AyM_MainVersionInfo.MinorVer */
    AYM_MAIN_PATCH_VERSION,       /* AyM_MainVersionInfo.PatchVer */
    AYM_MAIN_BRANCH_VERSION       /* AyM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxEscInfo_t AyM_MainCanRxEscInfo;
Proxy_RxByComRxYawSerialInfo_t AyM_MainRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t AyM_MainRxYawAccInfo;
Proxy_RxByComRxLongAccInfo_t AyM_MainRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t AyM_MainRxMsgOkFlgInfo;
SenPwrM_MainSenPwrMonitor_t AyM_MainSenPwrMonitorData;
Eem_MainEemFailData_t AyM_MainEemFailData;
Mom_HndlrEcuModeSts_t AyM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t AyM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t AyM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t AyM_MainVBatt1Mon;
Diag_HndlrDiagClr_t AyM_MainDiagClrSrs;

/* Output Data Element */
AyM_MainAYMOutInfo_t AyM_MainAYMOuitInfo;

uint32 AyM_Main_Timer_Start;
uint32 AyM_Main_Timer_Elapsed;

#define AYM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/** Variable Section (32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyM_MemMap.h"
#define AYM_MAIN_START_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/** Variable Section (32BIT)**/


#define AYM_MAIN_STOP_SEC_VAR_32BIT
#include "AyM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AYM_MAIN_START_SEC_CODE
#include "AyM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AyM_Main_Init(void)
{
    /* Initialize internal bus */
    AyM_MainBus.AyM_MainCanRxEscInfo.Ay = 0;
    AyM_MainBus.AyM_MainRxYawSerialInfo.YawSerialNum_0 = 0;
    AyM_MainBus.AyM_MainRxYawSerialInfo.YawSerialNum_1 = 0;
    AyM_MainBus.AyM_MainRxYawSerialInfo.YawSerialNum_2 = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.YawRateValidData = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.YawRateSelfTestStatus = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.SensorOscFreqDev = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Gyro_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Raster_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Eep_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Batt_Range_Err = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Asic_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Accel_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Ram_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Rom_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Ad_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Osc_Fail = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Watchdog_Rst = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Plaus_Err_Pst = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.RollingCounter = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.Can_Func_Err = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.LatAccValidData = 0;
    AyM_MainBus.AyM_MainRxYawAccInfo.LatAccSelfTestStatus = 0;
    AyM_MainBus.AyM_MainRxLongAccInfo.IntSenFltSymtmActive = 0;
    AyM_MainBus.AyM_MainRxLongAccInfo.IntSenFaultPresent = 0;
    AyM_MainBus.AyM_MainRxLongAccInfo.LongAccSenCirErrPre = 0;
    AyM_MainBus.AyM_MainRxLongAccInfo.LonACSenRanChkErrPre = 0;
    AyM_MainBus.AyM_MainRxLongAccInfo.LongRollingCounter = 0;
    AyM_MainBus.AyM_MainRxLongAccInfo.IntTempSensorFault = 0;
    AyM_MainBus.AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = 0;
    AyM_MainBus.AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = 0;
    AyM_MainBus.AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = 0;
    AyM_MainBus.AyM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    AyM_MainBus.AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssFL = 0;
    AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssFR = 0;
    AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssRL = 0;
    AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssRR = 0;
    AyM_MainBus.AyM_MainEcuModeSts = 0;
    AyM_MainBus.AyM_MainIgnOnOffSts = 0;
    AyM_MainBus.AyM_MainIgnEdgeSts = 0;
    AyM_MainBus.AyM_MainVBatt1Mon = 0;
    AyM_MainBus.AyM_MainDiagClrSrs = 0;
    AyM_MainBus.AyM_MainAYMOuitInfo.AYM_Timeout_Err = 0;
    AyM_MainBus.AyM_MainAYMOuitInfo.AYM_Invalid_Err = 0;
    AyM_MainBus.AyM_MainAYMOuitInfo.AYM_CRC_Err = 0;
    AyM_MainBus.AyM_MainAYMOuitInfo.AYM_Rolling_Err = 0;
    AyM_MainBus.AyM_MainAYMOuitInfo.AYM_Temperature_Err = 0;
    AyM_MainBus.AyM_MainAYMOuitInfo.AYM_Range_Err = 0;
}

void AyM_Main(void)
{
    AyM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    AyM_Main_Read_AyM_MainCanRxEscInfo_Ay(&AyM_MainBus.AyM_MainCanRxEscInfo.Ay);

    AyM_Main_Read_AyM_MainRxYawSerialInfo(&AyM_MainBus.AyM_MainRxYawSerialInfo);
    /*==============================================================================
    * Members of structure AyM_MainRxYawSerialInfo 
     : AyM_MainRxYawSerialInfo.YawSerialNum_0;
     : AyM_MainRxYawSerialInfo.YawSerialNum_1;
     : AyM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/
    
    /* Decomposed structure interface */
    AyM_Main_Read_AyM_MainRxYawAccInfo_YawRateValidData(&AyM_MainBus.AyM_MainRxYawAccInfo.YawRateValidData);
    AyM_Main_Read_AyM_MainRxYawAccInfo_YawRateSelfTestStatus(&AyM_MainBus.AyM_MainRxYawAccInfo.YawRateSelfTestStatus);
    AyM_Main_Read_AyM_MainRxYawAccInfo_SensorOscFreqDev(&AyM_MainBus.AyM_MainRxYawAccInfo.SensorOscFreqDev);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Gyro_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Gyro_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Raster_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Raster_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Eep_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Eep_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Batt_Range_Err(&AyM_MainBus.AyM_MainRxYawAccInfo.Batt_Range_Err);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Asic_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Asic_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Accel_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Accel_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Ram_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Ram_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Rom_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Rom_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Ad_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Ad_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Osc_Fail(&AyM_MainBus.AyM_MainRxYawAccInfo.Osc_Fail);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Watchdog_Rst(&AyM_MainBus.AyM_MainRxYawAccInfo.Watchdog_Rst);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Plaus_Err_Pst(&AyM_MainBus.AyM_MainRxYawAccInfo.Plaus_Err_Pst);
    AyM_Main_Read_AyM_MainRxYawAccInfo_RollingCounter(&AyM_MainBus.AyM_MainRxYawAccInfo.RollingCounter);
    AyM_Main_Read_AyM_MainRxYawAccInfo_Can_Func_Err(&AyM_MainBus.AyM_MainRxYawAccInfo.Can_Func_Err);
    AyM_Main_Read_AyM_MainRxYawAccInfo_LatAccValidData(&AyM_MainBus.AyM_MainRxYawAccInfo.LatAccValidData);
    AyM_Main_Read_AyM_MainRxYawAccInfo_LatAccSelfTestStatus(&AyM_MainBus.AyM_MainRxYawAccInfo.LatAccSelfTestStatus);

    /* Decomposed structure interface */
    AyM_Main_Read_AyM_MainRxLongAccInfo_IntSenFltSymtmActive(&AyM_MainBus.AyM_MainRxLongAccInfo.IntSenFltSymtmActive);
    AyM_Main_Read_AyM_MainRxLongAccInfo_IntSenFaultPresent(&AyM_MainBus.AyM_MainRxLongAccInfo.IntSenFaultPresent);
    AyM_Main_Read_AyM_MainRxLongAccInfo_LongAccSenCirErrPre(&AyM_MainBus.AyM_MainRxLongAccInfo.LongAccSenCirErrPre);
    AyM_Main_Read_AyM_MainRxLongAccInfo_LonACSenRanChkErrPre(&AyM_MainBus.AyM_MainRxLongAccInfo.LonACSenRanChkErrPre);
    AyM_Main_Read_AyM_MainRxLongAccInfo_LongRollingCounter(&AyM_MainBus.AyM_MainRxLongAccInfo.LongRollingCounter);
    AyM_Main_Read_AyM_MainRxLongAccInfo_IntTempSensorFault(&AyM_MainBus.AyM_MainRxLongAccInfo.IntTempSensorFault);

    /* Decomposed structure interface */
    AyM_Main_Read_AyM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(&AyM_MainBus.AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg);
    AyM_Main_Read_AyM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(&AyM_MainBus.AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg);
    AyM_Main_Read_AyM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(&AyM_MainBus.AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg);

    /* Decomposed structure interface */
    AyM_Main_Read_AyM_MainSenPwrMonitorData_SenPwrM_5V_Stable(&AyM_MainBus.AyM_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    AyM_Main_Read_AyM_MainSenPwrMonitorData_SenPwrM_12V_Stable(&AyM_MainBus.AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssFL(&AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssFL);
    AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssFR(&AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssFR);
    AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssRL(&AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssRL);
    AyM_Main_Read_AyM_MainEemFailData_Eem_Fail_WssRR(&AyM_MainBus.AyM_MainEemFailData.Eem_Fail_WssRR);

    AyM_Main_Read_AyM_MainEcuModeSts(&AyM_MainBus.AyM_MainEcuModeSts);
    AyM_Main_Read_AyM_MainIgnOnOffSts(&AyM_MainBus.AyM_MainIgnOnOffSts);
    AyM_Main_Read_AyM_MainIgnEdgeSts(&AyM_MainBus.AyM_MainIgnEdgeSts);
    AyM_Main_Read_AyM_MainVBatt1Mon(&AyM_MainBus.AyM_MainVBatt1Mon);
    AyM_Main_Read_AyM_MainDiagClrSrs(&AyM_MainBus.AyM_MainDiagClrSrs);

    /* Process */

    /* Output */
    AyM_Main_Write_AyM_MainAYMOuitInfo(&AyM_MainBus.AyM_MainAYMOuitInfo);
    /*==============================================================================
    * Members of structure AyM_MainAYMOuitInfo 
     : AyM_MainAYMOuitInfo.AYM_Timeout_Err;
     : AyM_MainAYMOuitInfo.AYM_Invalid_Err;
     : AyM_MainAYMOuitInfo.AYM_CRC_Err;
     : AyM_MainAYMOuitInfo.AYM_Rolling_Err;
     : AyM_MainAYMOuitInfo.AYM_Temperature_Err;
     : AyM_MainAYMOuitInfo.AYM_Range_Err;
     =============================================================================*/
    

    AyM_Main_Timer_Elapsed = STM0_TIM0.U - AyM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AYM_MAIN_STOP_SEC_CODE
#include "AyM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
