/**
 * @defgroup PedalP_Main_Ifa PedalP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALP_MAIN_IFA_H_
#define PEDALP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define PedalP_Main_Read_PedalP_MainCanRxAccelPedlInfo(data) do \
{ \
    *data = PedalP_MainCanRxAccelPedlInfo; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPdf5msRawInfo(data) do \
{ \
    *data = PedalP_MainPdf5msRawInfo; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPedalMData(data) do \
{ \
    *data = PedalP_MainPedalMData; \
}while(0);

#define PedalP_Main_Read_PedalP_MainWssPlauData(data) do \
{ \
    *data = PedalP_MainWssPlauData; \
}while(0);

#define PedalP_Main_Read_PedalP_MainCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = PedalP_MainCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdfSig(data) do \
{ \
    *data = PedalP_MainPdf5msRawInfo.MoveAvrPdfSig; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(data) do \
{ \
    *data = PedalP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdtSig(data) do \
{ \
    *data = PedalP_MainPdf5msRawInfo.MoveAvrPdtSig; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(data) do \
{ \
    *data = PedalP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo_PressP_SimPMoveAve(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo_PressP_CirP1MoveAve(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo_PressP_CirP2MoveAve(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo_PressP_SimPMoveAveEolOfs(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo_PressP_CirP1MoveAveEolOfs(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPressSenCalcInfo_PressP_CirP2MoveAveEolOfs(data) do \
{ \
    *data = PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPedalMData_PedalM_PTS1_Check_Ihb(data) do \
{ \
    *data = PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb; \
}while(0);

#define PedalP_Main_Read_PedalP_MainPedalMData_PedalM_PTS2_Check_Ihb(data) do \
{ \
    *data = PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb; \
}while(0);

#define PedalP_Main_Read_PedalP_MainWssPlauData_WssP_min_speed(data) do \
{ \
    *data = PedalP_MainWssPlauData.WssP_min_speed; \
}while(0);

#define PedalP_Main_Read_PedalP_MainEcuModeSts(data) do \
{ \
    *data = PedalP_MainEcuModeSts; \
}while(0);

#define PedalP_Main_Read_PedalP_MainIgnOnOffSts(data) do \
{ \
    *data = PedalP_MainIgnOnOffSts; \
}while(0);

#define PedalP_Main_Read_PedalP_MainIgnEdgeSts(data) do \
{ \
    *data = PedalP_MainIgnEdgeSts; \
}while(0);

#define PedalP_Main_Read_PedalP_MainDiagClrSrs(data) do \
{ \
    *data = PedalP_MainDiagClrSrs; \
}while(0);

#define PedalP_Main_Read_PedalP_MainBlsSwt(data) do \
{ \
    *data = PedalP_MainBlsSwt; \
}while(0);


/* Set Output DE MAcro Function */
#define PedalP_Main_Write_PedalP_MainPedalPData(data) do \
{ \
    PedalP_MainPedalPData = *data; \
}while(0);

#define PedalP_Main_Write_PedalP_MainPedalPData_PedalP_PTS1_Stick_Err(data) do \
{ \
    PedalP_MainPedalPData.PedalP_PTS1_Stick_Err = *data; \
}while(0);

#define PedalP_Main_Write_PedalP_MainPedalPData_PedalP_PTS1_Offset_Err(data) do \
{ \
    PedalP_MainPedalPData.PedalP_PTS1_Offset_Err = *data; \
}while(0);

#define PedalP_Main_Write_PedalP_MainPedalPData_PedalP_PTS1_Noise_Err(data) do \
{ \
    PedalP_MainPedalPData.PedalP_PTS1_Noise_Err = *data; \
}while(0);

#define PedalP_Main_Write_PedalP_MainPedalPData_PedalP_PTS2_Stick_Err(data) do \
{ \
    PedalP_MainPedalPData.PedalP_PTS2_Stick_Err = *data; \
}while(0);

#define PedalP_Main_Write_PedalP_MainPedalPData_PedalP_PTS2_Offset_Err(data) do \
{ \
    PedalP_MainPedalPData.PedalP_PTS2_Offset_Err = *data; \
}while(0);

#define PedalP_Main_Write_PedalP_MainPedalPData_PedalP_PTS2_Noise_Err(data) do \
{ \
    PedalP_MainPedalPData.PedalP_PTS2_Noise_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALP_MAIN_IFA_H_ */
/** @} */
