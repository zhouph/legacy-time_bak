/**
 * @defgroup PedalP_Types PedalP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALP_TYPES_H_
#define PEDALP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxAccelPedlInfo_t PedalP_MainCanRxAccelPedlInfo;
    Pedal_SenSyncPdf5msRawInfo_t PedalP_MainPdf5msRawInfo;
    Press_SenSyncPressSenCalcInfo_t PedalP_MainPressSenCalcInfo;
    PedalM_MainPedalMData_t PedalP_MainPedalMData;
    WssP_MainWssPlauData_t PedalP_MainWssPlauData;
    Mom_HndlrEcuModeSts_t PedalP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t PedalP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t PedalP_MainIgnEdgeSts;
    Diag_HndlrDiagClr_t PedalP_MainDiagClrSrs;
    Swt_SenBlsSwt_t PedalP_MainBlsSwt;

/* Output Data Element */
    PedalP_MainPedalPData_t PedalP_MainPedalPData;
}PedalP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALP_TYPES_H_ */
/** @} */
