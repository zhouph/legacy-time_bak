/**
 * @defgroup PedalP_Main PedalP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALP_MAIN_H_
#define PEDALP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalP_Types.h"
#include "PedalP_Cfg.h"
#include "PedalP_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PEDALP_MAIN_MODULE_ID      (0)
 #define PEDALP_MAIN_MAJOR_VERSION  (2)
 #define PEDALP_MAIN_MINOR_VERSION  (0)
 #define PEDALP_MAIN_PATCH_VERSION  (0)
 #define PEDALP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern PedalP_Main_HdrBusType PedalP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t PedalP_MainVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxAccelPedlInfo_t PedalP_MainCanRxAccelPedlInfo;
extern Pedal_SenSyncPdf5msRawInfo_t PedalP_MainPdf5msRawInfo;
extern Press_SenSyncPressSenCalcInfo_t PedalP_MainPressSenCalcInfo;
extern PedalM_MainPedalMData_t PedalP_MainPedalMData;
extern WssP_MainWssPlauData_t PedalP_MainWssPlauData;
extern Mom_HndlrEcuModeSts_t PedalP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t PedalP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t PedalP_MainIgnEdgeSts;
extern Diag_HndlrDiagClr_t PedalP_MainDiagClrSrs;
extern Swt_SenBlsSwt_t PedalP_MainBlsSwt;

/* Output Data Element */
extern PedalP_MainPedalPData_t PedalP_MainPedalPData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void PedalP_Main_Init(void);
extern void PedalP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALP_MAIN_H_ */
/** @} */
