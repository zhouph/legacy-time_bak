#define S_FUNCTION_NAME      PedalP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          19
#define WidthOutputPort         6

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "PedalP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    PedalP_MainCanRxAccelPedlInfo.AccelPedlVal = input[0];
    PedalP_MainPdf5msRawInfo.MoveAvrPdfSig = input[1];
    PedalP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = input[2];
    PedalP_MainPdf5msRawInfo.MoveAvrPdtSig = input[3];
    PedalP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = input[4];
    PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve = input[5];
    PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve = input[6];
    PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve = input[7];
    PedalP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs = input[8];
    PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = input[9];
    PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = input[10];
    PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb = input[11];
    PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb = input[12];
    PedalP_MainWssPlauData.WssP_min_speed = input[13];
    PedalP_MainEcuModeSts = input[14];
    PedalP_MainIgnOnOffSts = input[15];
    PedalP_MainIgnEdgeSts = input[16];
    PedalP_MainDiagClrSrs = input[17];
    PedalP_MainBlsSwt = input[18];

    PedalP_Main();


    output[0] = PedalP_MainPedalPData.PedalP_PTS1_Stick_Err;
    output[1] = PedalP_MainPedalPData.PedalP_PTS1_Offset_Err;
    output[2] = PedalP_MainPedalPData.PedalP_PTS1_Noise_Err;
    output[3] = PedalP_MainPedalPData.PedalP_PTS2_Stick_Err;
    output[4] = PedalP_MainPedalPData.PedalP_PTS2_Offset_Err;
    output[5] = PedalP_MainPedalPData.PedalP_PTS2_Noise_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    PedalP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
