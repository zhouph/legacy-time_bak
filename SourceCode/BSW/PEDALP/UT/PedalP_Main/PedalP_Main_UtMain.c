#include "unity.h"
#include "unity_fixture.h"
#include "PedalP_Main.h"
#include "PedalP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_PedalP_MainCanRxAccelPedlInfo_AccelPedlVal[MAX_STEP] = PEDALP_MAINCANRXACCELPEDLINFO_ACCELPEDLVAL;
const Salsint16 UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdfSig[MAX_STEP] = PEDALP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIG;
const Salsint16 UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset[MAX_STEP] = PEDALP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIGEOLOFFSET;
const Salsint16 UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdtSig[MAX_STEP] = PEDALP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIG;
const Salsint16 UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset[MAX_STEP] = PEDALP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIGEOLOFFSET;
const Saluint16 UtInput_PedalP_MainPressSenCalcInfo_PressP_SimPMoveAve[MAX_STEP] = PEDALP_MAINPRESSSENCALCINFO_PRESSP_SIMPMOVEAVE;
const Saluint16 UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP1MoveAve[MAX_STEP] = PEDALP_MAINPRESSSENCALCINFO_PRESSP_CIRP1MOVEAVE;
const Saluint16 UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP2MoveAve[MAX_STEP] = PEDALP_MAINPRESSSENCALCINFO_PRESSP_CIRP2MOVEAVE;
const Saluint16 UtInput_PedalP_MainPressSenCalcInfo_PressP_SimPMoveAveEolOfs[MAX_STEP] = PEDALP_MAINPRESSSENCALCINFO_PRESSP_SIMPMOVEAVEEOLOFS;
const Saluint16 UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP1MoveAveEolOfs[MAX_STEP] = PEDALP_MAINPRESSSENCALCINFO_PRESSP_CIRP1MOVEAVEEOLOFS;
const Saluint16 UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP2MoveAveEolOfs[MAX_STEP] = PEDALP_MAINPRESSSENCALCINFO_PRESSP_CIRP2MOVEAVEEOLOFS;
const Saluint8 UtInput_PedalP_MainPedalMData_PedalM_PTS1_Check_Ihb[MAX_STEP] = PEDALP_MAINPEDALMDATA_PEDALM_PTS1_CHECK_IHB;
const Saluint8 UtInput_PedalP_MainPedalMData_PedalM_PTS2_Check_Ihb[MAX_STEP] = PEDALP_MAINPEDALMDATA_PEDALM_PTS2_CHECK_IHB;
const Saluint16 UtInput_PedalP_MainWssPlauData_WssP_min_speed[MAX_STEP] = PEDALP_MAINWSSPLAUDATA_WSSP_MIN_SPEED;
const Mom_HndlrEcuModeSts_t UtInput_PedalP_MainEcuModeSts[MAX_STEP] = PEDALP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_PedalP_MainIgnOnOffSts[MAX_STEP] = PEDALP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_PedalP_MainIgnEdgeSts[MAX_STEP] = PEDALP_MAINIGNEDGESTS;
const Diag_HndlrDiagClr_t UtInput_PedalP_MainDiagClrSrs[MAX_STEP] = PEDALP_MAINDIAGCLRSRS;
const Swt_SenBlsSwt_t UtInput_PedalP_MainBlsSwt[MAX_STEP] = PEDALP_MAINBLSSWT;

const Saluint8 UtExpected_PedalP_MainPedalPData_PedalP_PTS1_Stick_Err[MAX_STEP] = PEDALP_MAINPEDALPDATA_PEDALP_PTS1_STICK_ERR;
const Saluint8 UtExpected_PedalP_MainPedalPData_PedalP_PTS1_Offset_Err[MAX_STEP] = PEDALP_MAINPEDALPDATA_PEDALP_PTS1_OFFSET_ERR;
const Saluint8 UtExpected_PedalP_MainPedalPData_PedalP_PTS1_Noise_Err[MAX_STEP] = PEDALP_MAINPEDALPDATA_PEDALP_PTS1_NOISE_ERR;
const Saluint8 UtExpected_PedalP_MainPedalPData_PedalP_PTS2_Stick_Err[MAX_STEP] = PEDALP_MAINPEDALPDATA_PEDALP_PTS2_STICK_ERR;
const Saluint8 UtExpected_PedalP_MainPedalPData_PedalP_PTS2_Offset_Err[MAX_STEP] = PEDALP_MAINPEDALPDATA_PEDALP_PTS2_OFFSET_ERR;
const Saluint8 UtExpected_PedalP_MainPedalPData_PedalP_PTS2_Noise_Err[MAX_STEP] = PEDALP_MAINPEDALPDATA_PEDALP_PTS2_NOISE_ERR;



TEST_GROUP(PedalP_Main);
TEST_SETUP(PedalP_Main)
{
    PedalP_Main_Init();
}

TEST_TEAR_DOWN(PedalP_Main)
{   /* Postcondition */

}

TEST(PedalP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        PedalP_MainCanRxAccelPedlInfo.AccelPedlVal = UtInput_PedalP_MainCanRxAccelPedlInfo_AccelPedlVal[i];
        PedalP_MainPdf5msRawInfo.MoveAvrPdfSig = UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdfSig[i];
        PedalP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset[i];
        PedalP_MainPdf5msRawInfo.MoveAvrPdtSig = UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdtSig[i];
        PedalP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = UtInput_PedalP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset[i];
        PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve = UtInput_PedalP_MainPressSenCalcInfo_PressP_SimPMoveAve[i];
        PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve = UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP1MoveAve[i];
        PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve = UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP2MoveAve[i];
        PedalP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs = UtInput_PedalP_MainPressSenCalcInfo_PressP_SimPMoveAveEolOfs[i];
        PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP1MoveAveEolOfs[i];
        PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = UtInput_PedalP_MainPressSenCalcInfo_PressP_CirP2MoveAveEolOfs[i];
        PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb = UtInput_PedalP_MainPedalMData_PedalM_PTS1_Check_Ihb[i];
        PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb = UtInput_PedalP_MainPedalMData_PedalM_PTS2_Check_Ihb[i];
        PedalP_MainWssPlauData.WssP_min_speed = UtInput_PedalP_MainWssPlauData_WssP_min_speed[i];
        PedalP_MainEcuModeSts = UtInput_PedalP_MainEcuModeSts[i];
        PedalP_MainIgnOnOffSts = UtInput_PedalP_MainIgnOnOffSts[i];
        PedalP_MainIgnEdgeSts = UtInput_PedalP_MainIgnEdgeSts[i];
        PedalP_MainDiagClrSrs = UtInput_PedalP_MainDiagClrSrs[i];
        PedalP_MainBlsSwt = UtInput_PedalP_MainBlsSwt[i];

        PedalP_Main();

        TEST_ASSERT_EQUAL(PedalP_MainPedalPData.PedalP_PTS1_Stick_Err, UtExpected_PedalP_MainPedalPData_PedalP_PTS1_Stick_Err[i]);
        TEST_ASSERT_EQUAL(PedalP_MainPedalPData.PedalP_PTS1_Offset_Err, UtExpected_PedalP_MainPedalPData_PedalP_PTS1_Offset_Err[i]);
        TEST_ASSERT_EQUAL(PedalP_MainPedalPData.PedalP_PTS1_Noise_Err, UtExpected_PedalP_MainPedalPData_PedalP_PTS1_Noise_Err[i]);
        TEST_ASSERT_EQUAL(PedalP_MainPedalPData.PedalP_PTS2_Stick_Err, UtExpected_PedalP_MainPedalPData_PedalP_PTS2_Stick_Err[i]);
        TEST_ASSERT_EQUAL(PedalP_MainPedalPData.PedalP_PTS2_Offset_Err, UtExpected_PedalP_MainPedalPData_PedalP_PTS2_Offset_Err[i]);
        TEST_ASSERT_EQUAL(PedalP_MainPedalPData.PedalP_PTS2_Noise_Err, UtExpected_PedalP_MainPedalPData_PedalP_PTS2_Noise_Err[i]);
    }
}

TEST_GROUP_RUNNER(PedalP_Main)
{
    RUN_TEST_CASE(PedalP_Main, All);
}
