/**
 * @defgroup PedalP_Main PedalP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalP_Main.h"
#include "PedalP_Types.h"
#include "PedalP_Main_Ifa.h"
#include "PedalP_Main_Proc.h"
#include "Common.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void PedalP_Main_Proc(PedalP_Main_HdrBusType *pPedalPInfo);
static void FP_vPTS1NoiseCheck(void);
static void FP_vPTS2NoiseCheck(void);
static void FC_vPedalStickChk(void);
static Saluint8 PedalDiffSusSet(DetPdlDiff_t *PdlDiffSus);
static void FC_vPedalOffsetChk_Fun(pdl_offset_t *ChkOffsetSig, Salsint16 ChkSig);
void FS_vErrCheckNormalClear(ChkErrNormalClear_t *ChkNormalClear,Saluint8 ErrSt,Saluint16 ErrTime,Saluint8 NormalSt,Saluint16 NormalTime,Saluint8 Inhibit);
void F_vDetectErrChk(ErrChk_t *ErrChk, Saluint8 ErrState, Saluint8 Inhibit, Saluint16 DetectTime);
    
static Saluint8 fcu8PdtChkInhibit;
static Saluint8 fcu8PdfChkInhibit;
static Saluint8 fcu8PdtNoiseErr;
static Saluint8 fcu8PdfNoiseErr;
static Saluint8 fcu8PdtStickErr;
static Saluint8 fcu8PdfStickErr;
static Salsint16 fs16MoveAvrPdtSig;
static Salsint16 fs16MoveAvrPdfSig;
static Salsint16 fs16MoveAvrPspPrs;
static Salsint16 fs16MoveAvrBcpPPrs;
static Salsint16 fs16MoveAvrBcpSPrs;
static Salsint16 fu16VehicleSpeed;
static Salsint16 fu16MinSpeed;
static Saluint8 fu8AccelOn;
static Saluint8 fu8BlsOn;
extern BbsVlvM_MainFSFsrBbsDrvInfo_t Arbitrator_VlvFSFsrBbsDrvInfo;
extern Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
ChkDiff_t fcChkPspDiff,fcChkPdlPdtStickDiff,fcChkPdlPdfStickDiff,fcChkBcpPDiff,fcChkBcpSDiff;

DetPdlDiff_t DetPdtDiffSus,DetPdfDiffSus;
pdl_offset_t chkoffsetPdt,chkoffsetpdf;

void PedalP_Main_Proc(PedalP_Main_HdrBusType *pPedalPInfo)
{
    /*input*/
    fs16MoveAvrPdtSig=pPedalPInfo->PedalP_MainPdf5msRawInfo.MoveAvrPdtSig;
    fs16MoveAvrPdfSig=pPedalPInfo->PedalP_MainPdf5msRawInfo.MoveAvrPdfSig;
    fs16MoveAvrPspPrs=pPedalPInfo->PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve;
    fs16MoveAvrBcpPPrs=pPedalPInfo->PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve;
    fs16MoveAvrBcpSPrs=pPedalPInfo->PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve;
    fu16VehicleSpeed=pPedalPInfo->PedalP_MainWssPlauData.WssP_min_speed ; /*temp vehicle speed -> min speed*/
    fu16MinSpeed=pPedalPInfo->PedalP_MainWssPlauData.WssP_min_speed ;
    fu8AccelOn=pPedalPInfo->PedalP_MainCanRxAccelPedlInfo.AccelPedlVal;
    fu8BlsOn=pPedalPInfo->PedalP_MainBlsSwt;
    
    /* Process */
    FP_vPTS1NoiseCheck();
    FP_vPTS2NoiseCheck();

    FC_vPedalStickChk();
    FC_vPedalOffsetChk_Fun(&chkoffsetPdt,fs16MoveAvrPdtSig);
    FC_vPedalOffsetChk_Fun(&chkoffsetpdf,fs16MoveAvrPdfSig);
    
    /* Output */
    pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Stick_Err = fcu8PdtStickErr;
    pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Offset_Err = chkoffsetPdt.fcu8PedalOffsetFail;
    pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Noise_Err = fcu8PdtNoiseErr;
    pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Stick_Err = fcu8PdfStickErr;
    pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Offset_Err = chkoffsetpdf.fcu8PedalOffsetFail;
    pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Noise_Err = fcu8PdfNoiseErr;

    /*inhibit*/
    if(pPedalPInfo->PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb==1)
    {                         
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Stick_Err = ERR_INHIBIT;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Offset_Err = ERR_INHIBIT;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Noise_Err =ERR_INHIBIT;
    }
    if(pPedalPInfo->PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb==1)
    {
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Stick_Err = ERR_INHIBIT;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Offset_Err = ERR_INHIBIT;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Noise_Err =ERR_INHIBIT;
    }

    /*Clear via Diag*/
    if(pPedalPInfo->PedalP_MainDiagClrSrs==1)
    {
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Stick_Err = ERR_NONE;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Offset_Err = ERR_NONE;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS1_Noise_Err = ERR_NONE;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Stick_Err = ERR_NONE;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Offset_Err = ERR_NONE;
        pPedalPInfo->PedalP_MainPedalPData.PedalP_PTS2_Noise_Err = ERR_NONE;
        fcu8PdtStickErr = ERR_NONE;
        chkoffsetPdt.fcu8PedalOffsetFail = ERR_NONE;
        fcu8PdtNoiseErr = ERR_NONE;
        fcu8PdfStickErr = ERR_NONE;
        chkoffsetpdf.fcu8PedalOffsetFail = ERR_NONE;
        fcu8PdfNoiseErr = ERR_NONE;
    }
}

static void FP_vPTS1NoiseCheck(void)
{
    Saluint16 DiffSig;
    Saluint16 DiffPTS1;
    Saluint16 PTS1NoiseErrCnt;
    Saluint16 PTS1NoiseChkCnt;
    Salsint16 OldPTS1;
    
    DiffSig = (Saluint16)abs(fs16MoveAvrPdtSig - fs16MoveAvrPdfSig);
    /*PDT Noise Check*/
    if(fcu8PdtChkInhibit==0)
    {
        DiffPTS1=abs(fs16MoveAvrPdtSig-OldPTS1);

        if((DiffPTS1>U8_PEDAL_PLUS_15MM)&&(DiffSig>U8_PEDAL_10MM))
        {
            if(PTS1NoiseErrCnt<0xFF)
            {
                PTS1NoiseErrCnt++;
            }
        }

        if(PTS1NoiseChkCnt<U16_3SEC_TIME)
        {
            PTS1NoiseChkCnt++;
        }
        else
        {
            PTS1NoiseChkCnt=0;
            PTS1NoiseErrCnt=0;
        }

        if(fcu8PdtChkInhibit==1)
        {
            PTS1NoiseErrCnt=0;
        }

        if(PTS1NoiseErrCnt>=100)
        {
            PTS1NoiseErrCnt=0;
            PTS1NoiseChkCnt=0;
            fcu8PdtNoiseErr=ERR_FAILED;
        }
    }   
    OldPTS1=fs16MoveAvrPdtSig;
}

static void FP_vPTS2NoiseCheck(void)
{
    Saluint16 DiffSig;
    Saluint16 DiffPTS2;
    Saluint16 PTS2NoiseErrCnt;
    Saluint16 PTS2NoiseChkCnt;
    Salsint16 OldPTS2;
    
    DiffSig = (Saluint16)abs(fs16MoveAvrPdtSig - fs16MoveAvrPdfSig);
    /*PDF Noise Check*/
    if(fcu8PdfChkInhibit==0)
    {
        DiffPTS2=abs(fs16MoveAvrPdfSig-OldPTS2);

        if( (DiffPTS2>U8_PEDAL_PLUS_15MM)&&(DiffSig>U8_PEDAL_10MM) )
        {
            if(PTS2NoiseErrCnt<0xFF)
            {
                PTS2NoiseErrCnt++;
            }
        }

        if(PTS2NoiseChkCnt<U16_3SEC_TIME)
        {
            PTS2NoiseChkCnt++;
        }
        else
        {
            PTS2NoiseChkCnt=0;
            PTS2NoiseErrCnt=0;
        }

        if(fcu8PdfChkInhibit==1)
        {
            PTS2NoiseErrCnt=0;
        }

        if(PTS2NoiseErrCnt>=100)
        {
            PTS2NoiseErrCnt=0;
            PTS2NoiseChkCnt=0;
            fcu8PdfNoiseErr=ERR_FAILED;
        }
    }   
    OldPTS2=fs16MoveAvrPdfSig;
}

static void FC_vPedalStickChk(void)
{
    Saluint8 InitialSet=0;
    Saluint8 PrsOnState=0;
    Saluint8 u8StickErrSet; 
    Saluint8 fcu8PdlStickStartCnt;

    // 1. No Actuator 상태에서 PSP 와 Pedal 비교.
    // AHB 비 제어 중  운전자가 Braking On 시 CVV 를 통하여 PSP,BCP-Pri,BCP-Sec 의 압력 증가 [ Residual Brake ]
    if(Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff==1)
    {
        fcu8PdlStickStartCnt++;
    }
    else
    {
        fcu8PdlStickStartCnt=0;
    }

    if(fcu8PdlStickStartCnt<U8_200MS_TIME)
    {
        InitialSet=1;
    }

    // Min/Max Calc
    FS_vMinMaxSetting(&fcChkPspDiff,fs16MoveAvrPspPrs,InitialSet);
    FS_vMinMaxSetting(&fcChkBcpPDiff,fs16MoveAvrBcpPPrs,InitialSet);
    FS_vMinMaxSetting(&fcChkBcpSDiff,fs16MoveAvrBcpSPrs,InitialSet);
    #if __USE_PDT_SIGNAL==ENABLE
    FS_vMinMaxSetting(&fcChkPdlPdtStickDiff,fs16MoveAvrPdtSig,InitialSet);
    #endif

    #if __USE_PDF_SIGNAL==ENABLE
    FS_vMinMaxSetting(&fcChkPdlPdfStickDiff,fs16MoveAvrPdfSig,InitialSet);
    #endif

    // 비 제어 중 압력이 높은 경우.
    if( (fs16MoveAvrPspPrs>U16_PRESSURE_15BAR) ||
    (fs16MoveAvrBcpPPrs>U16_PRESSURE_15BAR) ||
    (fs16MoveAvrBcpSPrs>U16_PRESSURE_15BAR) )
    {
        #if __USE_PDT_SIGNAL==ENABLE
        DetPdtDiffSus.PdlDiffValue = fcChkPdlPdtStickDiff.absValueDifference;
        DetPdtDiffSus.PdlValue = fs16MoveAvrPdtSig;
        u8StickErrSet = PedalDiffSusSet(&DetPdtDiffSus);
        if(fcu8PdtChkInhibit==1)
        {
            u8StickErrSet=0;
        }
        if(u8StickErrSet==1)
        {
            fcu8PdtStickErr=ERR_FAILED; // Error Set  
        }
        #endif

        #if __USE_PDF_SIGNAL==ENABLE
        DetPdfDiffSus.PdlDiffValue = fcChkPdlPdfStickDiff.absValueDifference;
        DetPdfDiffSus.PdlValue = fs16MoveAvrPdfSig;
        u8StickErrSet = PedalDiffSusSet(&DetPdfDiffSus);
        if(fcu8PdfChkInhibit==1)
        {
            u8StickErrSet=0;
        }
        if(u8StickErrSet==1)
        {
            fcu8PdfStickErr=ERR_FAILED; // Error Set  
        }
        #endif
    }
}

void FS_vMinMaxSetting(ChkDiff_t *ChkDiff,Salsint16 Value,Saluint8 Initial)
{
    if(Initial==1)
    {
        ChkDiff->MaxStoredValue=Value;
        ChkDiff->MinStoredValue=Value;
        ChkDiff->ValueDifference=0; 
        ChkDiff->absValueDifference=0;
    }
    else
    {
        if(ChkDiff->MaxStoredValue < Value )
        {
            ChkDiff->MaxStoredValue=Value;	
        }

        if(ChkDiff->MinStoredValue > Value )
        {
            ChkDiff->MinStoredValue=Value;	
        }
        ChkDiff->ValueDifference=ChkDiff->MaxStoredValue - ChkDiff->MinStoredValue; 
        ChkDiff->absValueDifference=(Saluint16)(abs(ChkDiff->ValueDifference));
    }
}

static Saluint8 PedalDiffSusSet(DetPdlDiff_t *PdlDiffSus)
{
    Saluint8 SusStateNo=0,RetVal=0;

    if( (PdlDiffSus->PdlDiffValue < U8_PEDAL_5MM)&&(PdlDiffSus->PdlValue < U8_PEDAL_5MM) )
    {
        if( (fcChkBcpPDiff.absValueDifference > U16_PRESSURE_10BAR) && 
        (fs16MoveAvrBcpPPrs > U16_PRESSURE_15BAR) )
        {
            SusStateNo++;
        }

        if( (fcChkBcpSDiff.absValueDifference > U16_PRESSURE_10BAR) && 
        (fs16MoveAvrBcpSPrs > U16_PRESSURE_15BAR) )
        {
            SusStateNo++;
        }

        if( (fcChkPspDiff.absValueDifference > U16_PRESSURE_10BAR) && 
        (fs16MoveAvrPspPrs > U16_PRESSURE_15BAR) )
        {
            SusStateNo++;
        }

        if(fu8BlsOn == 1 )
        {
            SusStateNo++;
        }
    }

    // PSP/BCP/BCS 증가 및 BLS 비교 시 모두 Unmtch 됨. // Residual Brake
    if(SusStateNo>=4)
    {
        PdlDiffSus->PdlDiffSusCnt[0]++;
    }
    else
    {
        PdlDiffSus->PdlDiffSusCnt[0] = 0;
    }

    // 4개가 아닌 경우에는 주행 중에만 비교.
    if(fu16VehicleSpeed > U16_F64SPEED_5KPH )
    {        
        if(SusStateNo>=3)
        {
            PdlDiffSus->PdlDiffSusCnt[1]++;
        }
        else
        {
            PdlDiffSus->PdlDiffSusCnt[1] = 0;
        }

        if(SusStateNo>=2)
        {
            PdlDiffSus->PdlDiffSusCnt[2]++;
        }
        else
        {
            PdlDiffSus->PdlDiffSusCnt[2] = 0;
        }
    }
    else
    {
        PdlDiffSus->PdlDiffSusCnt[1] = 0;
        PdlDiffSus->PdlDiffSusCnt[2] = 0;
    }

    if( (PdlDiffSus->PdlDiffSusCnt[0] > 100) ||
    (PdlDiffSus->PdlDiffSusCnt[1] > 300) ||
    (PdlDiffSus->PdlDiffSusCnt[2] > 500) )
    {
        RetVal = 1;
    }

    return RetVal;
}

static void FC_vPedalOffsetChk_Fun(pdl_offset_t *ChkOffsetSig, Salsint16 ChkSig)
{
    Saluint8 TempErrState,TempNmlState,TempInhibitState,InitialSet,TempSusState;
    Saluint16 TimeSet,NormalTime;
    TempErrState=TempNmlState=TempInhibitState=InitialSet=0;
    TimeSet=NormalTime=0;
    //ChkSig=fs16MoveAvrPdtSig;	// 향후 EOL Offset 보정값 입력 !

    // 정상 판단 조건은 10mm 이하 100ms 유지시 Set.
    if(ChkSig<_PEDAL_OFFSET_STROKE)
    {
        if(ChkOffsetSig->PedalUnderOffsetCnt<U8_100MS_TIME)
        {
            ChkOffsetSig->PedalUnderOffsetCnt++;
        }
        else
        {
            TempNmlState=1;
        }
    }
    else
    {
        ChkOffsetSig->PedalUnderOffsetCnt=0;
    }

    /* 1. 주행중 20mm 이상 Long Term Offset Check */
    TimeSet=U8_300MS_TIME;
    NormalTime=U8_100MS_TIME;
    if((ChkSig>_PEDAL_ERROR_STROKE)&&(fu16MinSpeed>U16_F64SPEED_15KPH))
    {
        TempErrState=1;
    }
    FS_vErrCheckNormalClear(&ChkOffsetSig->ChkPdlOffset,TempErrState,TimeSet,TempNmlState,NormalTime,TempInhibitState);

    /* 2. Accel Pedal 과 Pedal Signal 비교 */
    TempErrState=0;
    TempSusState=0;
    TimeSet=U16_3SEC_TIME;
    if(fu8AccelOn>0)
    {
        if(ChkSig>_PEDAL_ERROR_STROKE)
        {
            TempErrState=1;
        }
        if( (ChkSig>_PEDAL_OFFSET_STROKE)&&(Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv==1) )
        {
            TempSusState=1;
        }
    }
    FS_vErrCheckNormalClear(&ChkOffsetSig->ChkPdlVsAclPdl,TempErrState,TimeSet,TempNmlState,NormalTime,TempInhibitState);
    FS_vErrCheckNormalClear(&ChkOffsetSig->ChkPdlVsAclPdlSus,TempSusState,TimeSet,TempNmlState,NormalTime,TempInhibitState);

    /* 3. Speed 증가 시 Pedal Signal 비교. */        
    if(ChkSig<_PEDAL_OFFSET_STROKE)
    {
        ChkOffsetSig->fcu8PedalOffsetErrCont=0;
        ChkOffsetSig->fcu8PedalOffsetSusCont=0;
        InitialSet=1;
        ChkOffsetSig->fpu1SpdChkStartStep=0;
    }
    else
    {
        if(ChkOffsetSig->fpu1SpdChkStartStep==0)
        {
            InitialSet=1;
            ChkOffsetSig->RefStartSpd=fu16VehicleSpeed;
            ChkOffsetSig->fpu1SpdChkStartStep=1;
        }
        else
        {
            if( (fu16VehicleSpeed+U8_F64SPEED_3KPH) < ChkOffsetSig->RefStartSpd )
            {
                ChkOffsetSig->fpu1SpdChkStartStep=0;
            }
            else if(fu16VehicleSpeed>(ChkOffsetSig->RefStartSpd+U16_F64SPEED_10KPH))
            {
                if(ChkOffsetSig->ChkPdlDiff.MinStoredValue>_PEDAL_ERROR_STROKE)
                {
                    ChkOffsetSig->fcu8PedalOffsetErrCont++;
                }
                if(ChkOffsetSig->ChkPdlDiff.MinStoredValue>_PEDAL_OFFSET_STROKE)
                {
                    ChkOffsetSig->fcu8PedalOffsetSusCont++;
                }
                ChkOffsetSig->fpu1SpdChkStartStep=0;
            }
            else
            {
                ;
            }
        }
    }    
    FS_vMinMaxSetting(&ChkOffsetSig->ChkPdlDiff,ChkSig,InitialSet);

    // 4. Brake Signal 과 Pedal Signal 비교.
    TempErrState=0;
    TempSusState=0;
    TimeSet=U16_3SEC_TIME;
    if(fu8BlsOn==0)
    {
        if(ChkSig>_PEDAL_ERROR_STROKE)
        {
            TempErrState=1;
        }
        if( (ChkSig>_PEDAL_OFFSET_STROKE)&&(Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv==1) )
        {
            TempSusState=1;
        }
    }        
    FS_vErrCheckNormalClear(&ChkOffsetSig->ChkPdlVsBLS,TempErrState,TimeSet,TempNmlState,NormalTime,TempInhibitState);
    FS_vErrCheckNormalClear(&ChkOffsetSig->ChkPdlVsBLSSus,TempSusState,TimeSet,TempNmlState,NormalTime,TempInhibitState);

    // 5. PSP Signal 과 Pedal Signal 비교.
    TempErrState=0;
    TempSusState=0;
    /*if((fpu1PspStableState==1)&&(fs16MoveAvrPspPrs<=50))*/
    if(fs16MoveAvrPspPrs<=50)
    {
        if(ChkSig>_PEDAL_ERROR_STROKE)
        {
            TempErrState=1;
        }
        if( (ChkSig>_PEDAL_OFFSET_STROKE)&&(Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv==1) )
        {
            TempSusState=1;
        }    
    }
    TimeSet=U8_1SEC_TIME;
    F_vDetectErrChk(&ChkOffsetSig->PspVsPdl,TempErrState,0,TimeSet);
    F_vDetectErrChk(&ChkOffsetSig->PspVsPdlSus,TempSusState,0,TimeSet);

    // Error / Suspect set.
    ChkOffsetSig->fcu8PedalOffsetFail=0;
    if( (ChkOffsetSig->ChkPdlOffset.ErrorSet==1) || // Long-Term Fail : Backup
    ( (ChkOffsetSig->ChkPdlVsAclPdl.ErrorSet==1) && 
    (ChkOffsetSig->fcu8PedalOffsetErrCont>0) && 
    (ChkOffsetSig->ChkPdlVsBLS.ErrorSet==1) &&
    (ChkOffsetSig->PspVsPdl.u1ErrDet==1) ) || // all 만족 시 즉시 고장 검출
    ( (ChkOffsetSig->ChkPdlVsAclPdl.ErrorSet==1) && 
    (ChkOffsetSig->fcu8PedalOffsetErrCont>=2) && 
    (ChkOffsetSig->PspVsPdl.u1ErrDet==1) ) || // BLS 제외.
    ( (ChkOffsetSig->ChkPdlVsAclPdl.ErrorSet==1) && 
    (ChkOffsetSig->fcu8PedalOffsetErrCont>=5) )   
    )
    {
        ChkOffsetSig->fcu8PedalOffsetFail=ERR_FAILED;
    } 
}

void FS_vErrCheckNormalClear(ChkErrNormalClear_t *ChkNormalClear,Saluint8 ErrSt,Saluint16 ErrTime,Saluint8 NormalSt,Saluint16 NormalTime,Saluint8 Inhibit)
{
    Saluint8 TmpInhibit;

    TmpInhibit=Inhibit;
    if(ErrSt>0)
    {
        ChkNormalClear->NonStateCnt=0;
        if(ChkNormalClear->ErrorStateDetCnt<ErrTime)
        {
            ChkNormalClear->ErrorStateDetCnt++;
        }
        else
        {
            ChkNormalClear->ErrorStateDetCnt=0;
            ChkNormalClear->ErrorSet=1;
        }
        ChkNormalClear->NormalSet=0;
    }
    else if(NormalSt>0)
    {
        ChkNormalClear->NonStateCnt=0;
        ChkNormalClear->ErrorStateDetCnt=0;
        ChkNormalClear->ErrorSet=0;

        if(ChkNormalClear->NormalStateDetCnt>=NormalTime) /* 정상 판단은 >= 적용 : 즉시 정상판단하는 경우 적용 위함 */
        {
            ChkNormalClear->NormalStateDetCnt=0;
            ChkNormalClear->NormalSet=1;
        }
        else
        {
            ChkNormalClear->NormalStateDetCnt++;
        }
    }
    else
    {
        if(ChkNormalClear->NonStateCnt<1000)
        {
            ChkNormalClear->NonStateCnt++;
        }
        else
        {
            TmpInhibit=1;	
        }
    }

    if(TmpInhibit==1)
    {
        ChkNormalClear->ErrorStateDetCnt=0;
        ChkNormalClear->ErrorSet=0;
    }
}

void F_vDetectErrChk(ErrChk_t *ErrChk, Saluint8 ErrState, Saluint8 Inhibit, Saluint16 DetectTime)
{
    if(ErrState == 1)
    {
        if (ErrChk->u16ErrChkTime < DetectTime)
        {
            ErrChk->u16ErrChkTime++;    
        }
        else
        {
            ErrChk->u1ErrDet = 1;  
        }
        ErrChk->u16NrmChkTime = 0;
        ErrChk->u1NrmStateDet =0;
    }
    else
    {
        ErrChk->u16ErrChkTime = 0;
        ErrChk->u1ErrDet = 0;
        if (ErrChk->u16NrmChkTime < DetectTime)
        {
            ErrChk->u16NrmChkTime++;    
        }
        else
        {
            ErrChk->u16NrmChkTime = 0;
            ErrChk->u1NrmStateDet   = 1;
        }
    }
    
    if (Inhibit == 1)
    {
        ErrChk->u16ErrChkTime = 0;  
        ErrChk->u1ErrDet = 0;     
    }
}

