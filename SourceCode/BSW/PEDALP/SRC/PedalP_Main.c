/**
 * @defgroup PedalP_Main PedalP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalP_Main.h"
#include "PedalP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDALP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDALP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
PedalP_Main_HdrBusType PedalP_MainBus;

/* Version Info */
const SwcVersionInfo_t PedalP_MainVersionInfo = 
{   
    PEDALP_MAIN_MODULE_ID,           /* PedalP_MainVersionInfo.ModuleId */
    PEDALP_MAIN_MAJOR_VERSION,       /* PedalP_MainVersionInfo.MajorVer */
    PEDALP_MAIN_MINOR_VERSION,       /* PedalP_MainVersionInfo.MinorVer */
    PEDALP_MAIN_PATCH_VERSION,       /* PedalP_MainVersionInfo.PatchVer */
    PEDALP_MAIN_BRANCH_VERSION       /* PedalP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxAccelPedlInfo_t PedalP_MainCanRxAccelPedlInfo;
Pedal_SenSyncPdf5msRawInfo_t PedalP_MainPdf5msRawInfo;
Press_SenSyncPressSenCalcInfo_t PedalP_MainPressSenCalcInfo;
PedalM_MainPedalMData_t PedalP_MainPedalMData;
WssP_MainWssPlauData_t PedalP_MainWssPlauData;
Mom_HndlrEcuModeSts_t PedalP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t PedalP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t PedalP_MainIgnEdgeSts;
Diag_HndlrDiagClr_t PedalP_MainDiagClrSrs;
Swt_SenBlsSwt_t PedalP_MainBlsSwt;

/* Output Data Element */
PedalP_MainPedalPData_t PedalP_MainPedalPData;

uint32 PedalP_Main_Timer_Start;
uint32 PedalP_Main_Timer_Elapsed;

#define PEDALP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
#define PEDALP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_MAIN_START_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALP_MAIN_STOP_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDALP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
#define PEDALP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_MAIN_START_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALP_MAIN_STOP_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDALP_MAIN_START_SEC_CODE
#include "PedalP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PedalP_Main_Init(void)
{
    /* Initialize internal bus */
    PedalP_MainBus.PedalP_MainCanRxAccelPedlInfo.AccelPedlVal = 0;
    PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdfSig = 0;
    PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = 0;
    PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdtSig = 0;
    PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = 0;
    PedalP_MainBus.PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve = 0;
    PedalP_MainBus.PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve = 0;
    PedalP_MainBus.PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve = 0;
    PedalP_MainBus.PedalP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs = 0;
    PedalP_MainBus.PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = 0;
    PedalP_MainBus.PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = 0;
    PedalP_MainBus.PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb = 0;
    PedalP_MainBus.PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb = 0;
    PedalP_MainBus.PedalP_MainWssPlauData.WssP_min_speed = 0;
    PedalP_MainBus.PedalP_MainEcuModeSts = 0;
    PedalP_MainBus.PedalP_MainIgnOnOffSts = 0;
    PedalP_MainBus.PedalP_MainIgnEdgeSts = 0;
    PedalP_MainBus.PedalP_MainDiagClrSrs = 0;
    PedalP_MainBus.PedalP_MainBlsSwt = 0;
    PedalP_MainBus.PedalP_MainPedalPData.PedalP_PTS1_Stick_Err = 0;
    PedalP_MainBus.PedalP_MainPedalPData.PedalP_PTS1_Offset_Err = 0;
    PedalP_MainBus.PedalP_MainPedalPData.PedalP_PTS1_Noise_Err = 0;
    PedalP_MainBus.PedalP_MainPedalPData.PedalP_PTS2_Stick_Err = 0;
    PedalP_MainBus.PedalP_MainPedalPData.PedalP_PTS2_Offset_Err = 0;
    PedalP_MainBus.PedalP_MainPedalPData.PedalP_PTS2_Noise_Err = 0;
}

void PedalP_Main(void)
{
    PedalP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    PedalP_Main_Read_PedalP_MainCanRxAccelPedlInfo_AccelPedlVal(&PedalP_MainBus.PedalP_MainCanRxAccelPedlInfo.AccelPedlVal);

    /* Decomposed structure interface */
    PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdfSig(&PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdfSig);
    PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(&PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset);
    PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdtSig(&PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdtSig);
    PedalP_Main_Read_PedalP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(&PedalP_MainBus.PedalP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset);

    PedalP_Main_Read_PedalP_MainPressSenCalcInfo(&PedalP_MainBus.PedalP_MainPressSenCalcInfo);
    /*==============================================================================
    * Members of structure PedalP_MainPressSenCalcInfo 
     : PedalP_MainPressSenCalcInfo.PressP_SimPMoveAve;
     : PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAve;
     : PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAve;
     : PedalP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs;
     : PedalP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs;
     : PedalP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs;
     =============================================================================*/
    
    /* Decomposed structure interface */
    PedalP_Main_Read_PedalP_MainPedalMData_PedalM_PTS1_Check_Ihb(&PedalP_MainBus.PedalP_MainPedalMData.PedalM_PTS1_Check_Ihb);
    PedalP_Main_Read_PedalP_MainPedalMData_PedalM_PTS2_Check_Ihb(&PedalP_MainBus.PedalP_MainPedalMData.PedalM_PTS2_Check_Ihb);

    /* Decomposed structure interface */
    PedalP_Main_Read_PedalP_MainWssPlauData_WssP_min_speed(&PedalP_MainBus.PedalP_MainWssPlauData.WssP_min_speed);

    PedalP_Main_Read_PedalP_MainEcuModeSts(&PedalP_MainBus.PedalP_MainEcuModeSts);
    PedalP_Main_Read_PedalP_MainIgnOnOffSts(&PedalP_MainBus.PedalP_MainIgnOnOffSts);
    PedalP_Main_Read_PedalP_MainIgnEdgeSts(&PedalP_MainBus.PedalP_MainIgnEdgeSts);
    PedalP_Main_Read_PedalP_MainDiagClrSrs(&PedalP_MainBus.PedalP_MainDiagClrSrs);
    PedalP_Main_Read_PedalP_MainBlsSwt(&PedalP_MainBus.PedalP_MainBlsSwt);

    /* Process */
    PedalP_Main_Proc(&PedalP_MainBus);
    /* Output */
    PedalP_Main_Write_PedalP_MainPedalPData(&PedalP_MainBus.PedalP_MainPedalPData);
    /*==============================================================================
    * Members of structure PedalP_MainPedalPData 
     : PedalP_MainPedalPData.PedalP_PTS1_Stick_Err;
     : PedalP_MainPedalPData.PedalP_PTS1_Offset_Err;
     : PedalP_MainPedalPData.PedalP_PTS1_Noise_Err;
     : PedalP_MainPedalPData.PedalP_PTS2_Stick_Err;
     : PedalP_MainPedalPData.PedalP_PTS2_Offset_Err;
     : PedalP_MainPedalPData.PedalP_PTS2_Noise_Err;
     =============================================================================*/
    

    PedalP_Main_Timer_Elapsed = STM0_TIM0.U - PedalP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDALP_MAIN_STOP_SEC_CODE
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
