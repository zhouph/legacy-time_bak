/**
 * @defgroup PedalP_Main PedalP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALP_MAIN_PROC_H_
#define PEDALP_MAIN_PROC_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalP_Types.h"
#include "PedalP_Cfg.h"
#include "Sal_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define U8_PEDAL_5MM                    (50)
#define U8_PEDAL_10MM                  (100)
#define U8_PEDAL_PLUS_15MM        (150)
#define U8_PEDAL_MINUS_15MM     (-150)

#define _PEDAL_OFFSET_STROKE    (100) // 10mm
#define _PEDAL_ERROR_STROKE      (200) // 20mm

#define U16_PRESSURE_10BAR         (1000)
#define U16_PRESSURE_15BAR         (1500)

#define U8_100MS_TIME      20
#define U8_200MS_TIME      40
#define U8_300MS_TIME      60
#define U8_1SEC_TIME      200
#define U16_3SEC_TIME      600

#define     U16_F64SPEED_5KPH           (5*64) 

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    Salsint16 MaxStoredValue;
    Salsint16 MinStoredValue;  
    Salsint16 ValueDifference;
    Saluint16 absValueDifference;
}ChkDiff_t;

typedef struct
{
    Salsint16 PdlValue;
    Saluint16 PdlDiffValue;
    Saluint16  PdlDiffSusCnt[3];
}DetPdlDiff_t;

typedef struct
{
    Saluint16 ErrorStateDetCnt;
    Saluint8  NormalStateDetCnt;
    Saluint16 NonStateCnt;
    Saluint8 NormalSet;
    Saluint8 ErrorSet;
    Saluint8 ChkErrReserve;
}ChkErrNormalClear_t;

typedef struct
{
    Saluint16 u16NrmChkTime;
    Saluint16 u16ErrChkTime;    
    Saluint8  u1NrmStateDet;
    Saluint8  u1ErrDet;
    Saluint8  u6FIReserved;
}ErrChk_t;

typedef struct
{
    Saluint16 RefStartSpd;
    Saluint16 PedalUnderOffsetCnt;
    Saluint8 fpu1SpdChkStartStep;
    Saluint8 fcu8PedalOffsetErrCont;
    Saluint8 fcu8PedalOffsetSusCont;
    Saluint8 fcu8PedalOffsetFail;
    ChkErrNormalClear_t ChkPdlOffset;
    ChkErrNormalClear_t ChkPdlVsAclPdl;
    ChkErrNormalClear_t ChkPdlVsAclPdlSus;
    ChkErrNormalClear_t ChkPdlVsBLS;
    ChkErrNormalClear_t ChkPdlVsBLSSus;
    ErrChk_t PspVsPdl;
    ErrChk_t PspVsPdlSus;
    ChkDiff_t ChkPdlDiff;
}pdl_offset_t;
extern pdl_offset_t chkoffsetPdt,chkoffsetpdf;

extern void FS_vMinMaxSetting(ChkDiff_t *ChkDiff,Salsint16 Value,Saluint8 Initial);
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

extern void PedalP_Main_Proc(PedalP_Main_HdrBusType *pPedalPInfo);


/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALP_MAIN_PROC_H_ */
/** @} */