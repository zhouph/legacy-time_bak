PedalP_MainCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    };
PedalP_MainCanRxAccelPedlInfo = CreateBus(PedalP_MainCanRxAccelPedlInfo, DeList);
clear DeList;

PedalP_MainPdf5msRawInfo = Simulink.Bus;
DeList={
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
PedalP_MainPdf5msRawInfo = CreateBus(PedalP_MainPdf5msRawInfo, DeList);
clear DeList;

PedalP_MainPressSenCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimPMoveAve'
    'PressP_CirP1MoveAve'
    'PressP_CirP2MoveAve'
    'PressP_SimPMoveAveEolOfs'
    'PressP_CirP1MoveAveEolOfs'
    'PressP_CirP2MoveAveEolOfs'
    };
PedalP_MainPressSenCalcInfo = CreateBus(PedalP_MainPressSenCalcInfo, DeList);
clear DeList;

PedalP_MainPedalMData = Simulink.Bus;
DeList={
    'PedalM_PTS1_Check_Ihb'
    'PedalM_PTS2_Check_Ihb'
    };
PedalP_MainPedalMData = CreateBus(PedalP_MainPedalMData, DeList);
clear DeList;

PedalP_MainWssPlauData = Simulink.Bus;
DeList={
    'WssP_min_speed'
    };
PedalP_MainWssPlauData = CreateBus(PedalP_MainWssPlauData, DeList);
clear DeList;

PedalP_MainEcuModeSts = Simulink.Bus;
DeList={'PedalP_MainEcuModeSts'};
PedalP_MainEcuModeSts = CreateBus(PedalP_MainEcuModeSts, DeList);
clear DeList;

PedalP_MainIgnOnOffSts = Simulink.Bus;
DeList={'PedalP_MainIgnOnOffSts'};
PedalP_MainIgnOnOffSts = CreateBus(PedalP_MainIgnOnOffSts, DeList);
clear DeList;

PedalP_MainIgnEdgeSts = Simulink.Bus;
DeList={'PedalP_MainIgnEdgeSts'};
PedalP_MainIgnEdgeSts = CreateBus(PedalP_MainIgnEdgeSts, DeList);
clear DeList;

PedalP_MainDiagClrSrs = Simulink.Bus;
DeList={'PedalP_MainDiagClrSrs'};
PedalP_MainDiagClrSrs = CreateBus(PedalP_MainDiagClrSrs, DeList);
clear DeList;

PedalP_MainBlsSwt = Simulink.Bus;
DeList={'PedalP_MainBlsSwt'};
PedalP_MainBlsSwt = CreateBus(PedalP_MainBlsSwt, DeList);
clear DeList;

PedalP_MainPedalPData = Simulink.Bus;
DeList={
    'PedalP_PTS1_Stick_Err'
    'PedalP_PTS1_Offset_Err'
    'PedalP_PTS1_Noise_Err'
    'PedalP_PTS2_Stick_Err'
    'PedalP_PTS2_Offset_Err'
    'PedalP_PTS2_Noise_Err'
    };
PedalP_MainPedalPData = CreateBus(PedalP_MainPedalPData, DeList);
clear DeList;

