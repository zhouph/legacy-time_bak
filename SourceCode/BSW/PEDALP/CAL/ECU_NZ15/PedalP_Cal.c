/**
 * @defgroup PedalP_Cal PedalP_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalP_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalP_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDALP_START_SEC_CALIB_UNSPECIFIED
#include "PedalP_MemMap.h"
/* Global Calibration Section */


#define PEDALP_STOP_SEC_CALIB_UNSPECIFIED
#include "PedalP_MemMap.h"

#define PEDALP_START_SEC_CONST_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDALP_STOP_SEC_CONST_UNSPECIFIED
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDALP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_START_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALP_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
#define PEDALP_START_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALP_STOP_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_START_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALP_STOP_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDALP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_START_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALP_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalP_MemMap.h"
#define PEDALP_START_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALP_STOP_SEC_VAR_UNSPECIFIED
#include "PedalP_MemMap.h"
#define PEDALP_START_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALP_STOP_SEC_VAR_32BIT
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDALP_START_SEC_CODE
#include "PedalP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDALP_STOP_SEC_CODE
#include "PedalP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
