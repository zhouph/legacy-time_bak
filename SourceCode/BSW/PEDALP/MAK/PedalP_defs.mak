# \file
#
# \brief PedalP
#
# This file contains the implementation of the SWC
# module PedalP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

PedalP_CORE_PATH     := $(MANDO_BSW_ROOT)\PedalP
PedalP_CAL_PATH      := $(PedalP_CORE_PATH)\CAL\$(PedalP_VARIANT)
PedalP_SRC_PATH      := $(PedalP_CORE_PATH)\SRC
PedalP_CFG_PATH      := $(PedalP_CORE_PATH)\CFG\$(PedalP_VARIANT)
PedalP_HDR_PATH      := $(PedalP_CORE_PATH)\HDR
PedalP_IFA_PATH      := $(PedalP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    PedalP_CMN_PATH      := $(PedalP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	PedalP_UT_PATH		:= $(PedalP_CORE_PATH)\UT
	PedalP_UNITY_PATH	:= $(PedalP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(PedalP_UT_PATH)
	CC_INCLUDE_PATH		+= $(PedalP_UNITY_PATH)
	PedalP_Main_PATH 	:= PedalP_UT_PATH\PedalP_Main
endif
CC_INCLUDE_PATH    += $(PedalP_CAL_PATH)
CC_INCLUDE_PATH    += $(PedalP_SRC_PATH)
CC_INCLUDE_PATH    += $(PedalP_CFG_PATH)
CC_INCLUDE_PATH    += $(PedalP_HDR_PATH)
CC_INCLUDE_PATH    += $(PedalP_IFA_PATH)
CC_INCLUDE_PATH    += $(PedalP_CMN_PATH)

