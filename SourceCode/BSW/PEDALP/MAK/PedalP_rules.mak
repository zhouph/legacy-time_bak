# \file
#
# \brief PedalP
#
# This file contains the implementation of the SWC
# module PedalP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += PedalP_src

PedalP_src_FILES        += $(PedalP_SRC_PATH)\PedalP_Main.c
PedalP_src_FILES        += $(PedalP_SRC_PATH)\PedalP_Main_Proc.c
PedalP_src_FILES        += $(PedalP_IFA_PATH)\PedalP_Main_Ifa.c
PedalP_src_FILES        += $(PedalP_CFG_PATH)\PedalP_Cfg.c
PedalP_src_FILES        += $(PedalP_CAL_PATH)\PedalP_Cal.c

ifeq ($(ICE_COMPILE),true)
PedalP_src_FILES        += $(PedalP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	PedalP_src_FILES        += $(PedalP_UNITY_PATH)\unity.c
	PedalP_src_FILES        += $(PedalP_UNITY_PATH)\unity_fixture.c	
	PedalP_src_FILES        += $(PedalP_UT_PATH)\main.c
	PedalP_src_FILES        += $(PedalP_UT_PATH)\PedalP_Main\PedalP_Main_UtMain.c
endif