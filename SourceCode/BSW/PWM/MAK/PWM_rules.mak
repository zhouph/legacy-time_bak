# \file
#
# \brief PWM
#
# This file contains the implementation of the BSW
# module PWM.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += PWM_src

PWM_src_FILES       += $(PWM_CORE_PATH)\Pwm.c

#################################################################
