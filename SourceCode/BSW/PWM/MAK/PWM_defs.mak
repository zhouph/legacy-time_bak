# \file
#
# \brief PWM
#
# This file contains the implementation of the BSW
# module PWM.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

PWM_CORE_PATH     := $(MANDO_BSW_ROOT)\PWM

