/**
 * @defgroup YawP_Main_Ifa YawP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWP_MAIN_IFA_H_
#define YAWP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define YawP_Main_Read_YawP_MainEemFailData(data) do \
{ \
    *data = YawP_MainEemFailData; \
}while(0);

#define YawP_Main_Read_YawP_MainCanRxEscInfo(data) do \
{ \
    *data = YawP_MainCanRxEscInfo; \
}while(0);

#define YawP_Main_Read_YawP_MainEscSwtStInfo(data) do \
{ \
    *data = YawP_MainEscSwtStInfo; \
}while(0);

#define YawP_Main_Read_YawP_MainWhlSpdInfo(data) do \
{ \
    *data = YawP_MainWhlSpdInfo; \
}while(0);

#define YawP_Main_Read_YawP_MainBaseBrkCtrlModInfo(data) do \
{ \
    *data = YawP_MainBaseBrkCtrlModInfo; \
}while(0);

#define YawP_Main_Read_YawP_MainSenPwrMonitorData(data) do \
{ \
    *data = YawP_MainSenPwrMonitorData; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData(data) do \
{ \
    *data = YawP_MainEemSuspectData; \
}while(0);

#define YawP_Main_Read_YawP_MainEemEceData(data) do \
{ \
    *data = YawP_MainEemEceData; \
}while(0);

#define YawP_Main_Read_YawP_MainCanMonData(data) do \
{ \
    *data = YawP_MainCanMonData; \
}while(0);

#define YawP_Main_Read_YawP_MainIMUCalcInfo(data) do \
{ \
    *data = YawP_MainIMUCalcInfo; \
}while(0);

#define YawP_Main_Read_YawP_MainWssSpeedOut(data) do \
{ \
    *data = YawP_MainWssSpeedOut; \
}while(0);

#define YawP_Main_Read_YawP_MainWssCalcInfo(data) do \
{ \
    *data = YawP_MainWssCalcInfo; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_Yaw; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_Ay; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_Str; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = YawP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define YawP_Main_Read_YawP_MainCanRxEscInfo_YawRate(data) do \
{ \
    *data = YawP_MainCanRxEscInfo.YawRate; \
}while(0);

#define YawP_Main_Read_YawP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = YawP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define YawP_Main_Read_YawP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = YawP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define YawP_Main_Read_YawP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = YawP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define YawP_Main_Read_YawP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = YawP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define YawP_Main_Read_YawP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = YawP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define YawP_Main_Read_YawP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define YawP_Main_Read_YawP_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = YawP_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define YawP_Main_Read_YawP_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = YawP_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define YawP_Main_Read_YawP_MainEemEceData_Eem_Ece_Yaw(data) do \
{ \
    *data = YawP_MainEemEceData.Eem_Ece_Yaw; \
}while(0);

#define YawP_Main_Read_YawP_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    *data = YawP_MainCanMonData.CanM_SubBusOff_Err; \
}while(0);

#define YawP_Main_Read_YawP_MainIMUCalcInfo_Reverse_Gear_flg(data) do \
{ \
    *data = YawP_MainIMUCalcInfo.Reverse_Gear_flg; \
}while(0);

#define YawP_Main_Read_YawP_MainIMUCalcInfo_Reverse_Judge_Time(data) do \
{ \
    *data = YawP_MainIMUCalcInfo.Reverse_Judge_Time; \
}while(0);

#define YawP_Main_Read_YawP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = YawP_MainWssSpeedOut.WssMax; \
}while(0);

#define YawP_Main_Read_YawP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = YawP_MainWssSpeedOut.WssMin; \
}while(0);

#define YawP_Main_Read_YawP_MainWssCalcInfo_Rough_Sus_Flg(data) do \
{ \
    *data = YawP_MainWssCalcInfo.Rough_Sus_Flg; \
}while(0);

#define YawP_Main_Read_YawP_MainEcuModeSts(data) do \
{ \
    *data = YawP_MainEcuModeSts; \
}while(0);

#define YawP_Main_Read_YawP_MainIgnOnOffSts(data) do \
{ \
    *data = YawP_MainIgnOnOffSts; \
}while(0);

#define YawP_Main_Read_YawP_MainIgnEdgeSts(data) do \
{ \
    *data = YawP_MainIgnEdgeSts; \
}while(0);

#define YawP_Main_Read_YawP_MainVBatt1Mon(data) do \
{ \
    *data = YawP_MainVBatt1Mon; \
}while(0);

#define YawP_Main_Read_YawP_MainDiagClrSrs(data) do \
{ \
    *data = YawP_MainDiagClrSrs; \
}while(0);

#define YawP_Main_Read_YawP_MainVehSpd(data) do \
{ \
    *data = YawP_MainVehSpd; \
}while(0);


/* Set Output DE MAcro Function */
#define YawP_Main_Write_YawP_MainYawPlauOutput(data) do \
{ \
    YawP_MainYawPlauOutput = *data; \
}while(0);

#define YawP_Main_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define YawP_Main_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define YawP_Main_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define YawP_Main_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define YawP_Main_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* YAWP_MAIN_IFA_H_ */
/** @} */
