/**
 * @defgroup YawP_Types YawP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWP_TYPES_H_
#define YAWP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t YawP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t YawP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t YawP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t YawP_MainWhlSpdInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t YawP_MainBaseBrkCtrlModInfo;
    SenPwrM_MainSenPwrMonitor_t YawP_MainSenPwrMonitorData;
    Eem_MainEemSuspectData_t YawP_MainEemSuspectData;
    Eem_MainEemEceData_t YawP_MainEemEceData;
    CanM_MainCanMonData_t YawP_MainCanMonData;
    Proxy_RxIMUCalc_t YawP_MainIMUCalcInfo;
    Wss_SenWssSpeedOut_t YawP_MainWssSpeedOut;
    Wss_SenWssCalc_t YawP_MainWssCalcInfo;
    Mom_HndlrEcuModeSts_t YawP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t YawP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t YawP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t YawP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t YawP_MainDiagClrSrs;
    Abc_CtrlVehSpd_t YawP_MainVehSpd;

/* Output Data Element */
    YawP_MainYawPlauOutput_t YawP_MainYawPlauOutput;
}YawP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* YAWP_TYPES_H_ */
/** @} */
