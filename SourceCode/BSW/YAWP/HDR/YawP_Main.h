/**
 * @defgroup YawP_Main YawP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWP_MAIN_H_
#define YAWP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawP_Types.h"
#include "YawP_Cfg.h"
#include "YawP_Cal.h"
#include "YawP_Process.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define YAWP_MAIN_MODULE_ID      (0)
 #define YAWP_MAIN_MAJOR_VERSION  (2)
 #define YAWP_MAIN_MINOR_VERSION  (0)
 #define YAWP_MAIN_PATCH_VERSION  (0)
 #define YAWP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern YawP_Main_HdrBusType YawP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t YawP_MainVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t YawP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t YawP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t YawP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t YawP_MainWhlSpdInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t YawP_MainBaseBrkCtrlModInfo;
extern SenPwrM_MainSenPwrMonitor_t YawP_MainSenPwrMonitorData;
extern Eem_MainEemSuspectData_t YawP_MainEemSuspectData;
extern Eem_MainEemEceData_t YawP_MainEemEceData;
extern CanM_MainCanMonData_t YawP_MainCanMonData;
extern Proxy_RxIMUCalc_t YawP_MainIMUCalcInfo;
extern Wss_SenWssSpeedOut_t YawP_MainWssSpeedOut;
extern Wss_SenWssCalc_t YawP_MainWssCalcInfo;
extern Mom_HndlrEcuModeSts_t YawP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t YawP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t YawP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t YawP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t YawP_MainDiagClrSrs;
extern Abc_CtrlVehSpd_t YawP_MainVehSpd;

/* Output Data Element */
extern YawP_MainYawPlauOutput_t YawP_MainYawPlauOutput;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void YawP_Main_Init(void);
extern void YawP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* YAWP_MAIN_H_ */
/** @} */
