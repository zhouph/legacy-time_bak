/**
 * @defgroup YawP_Process YawP_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Process.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWP_PROCESS_H_
#define YAWP_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawP_Types.h"
#include "YawP_Cfg.h"
#include "Common.h"

#define DEF_STANDSTILL     1
#define DEF_NON_STANDSTILL 0

#define YAW_INHIBIT     1
#define YAW_ALLOW      0
#define DEF_2SEC_TIME  200
#define DEF_5SEC_TIME  500


#define DEF_YAW_5DPS  500
#define DEF_YAW_6DPS  600
#define DEF_YAW_75DPS 7500

#define DEF_YAW_STAND_OFFSET_INIT           0x00u
#define DEF_YAW_STAND_OFFSET_SUSPECT    0x01u
#define DEF_YAW_STAND_OFFSET_INVALID     0x02u
#define DEF_YAW_STAND_OFFSET_ERR            0x03u

extern void YawP_Process(YawP_Main_HdrBusType *pYawP_Process);

#endif 