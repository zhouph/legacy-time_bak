# \file
#
# \brief YawP
#
# This file contains the implementation of the SWC
# module YawP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

YawP_CORE_PATH     := $(MANDO_BSW_ROOT)\YawP
YawP_CAL_PATH      := $(YawP_CORE_PATH)\CAL\$(YawP_VARIANT)
YawP_SRC_PATH      := $(YawP_CORE_PATH)\SRC
YawP_CFG_PATH      := $(YawP_CORE_PATH)\CFG\$(YawP_VARIANT)
YawP_HDR_PATH      := $(YawP_CORE_PATH)\HDR
YawP_IFA_PATH      := $(YawP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    YawP_CMN_PATH      := $(YawP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	YawP_UT_PATH		:= $(YawP_CORE_PATH)\UT
	YawP_UNITY_PATH	:= $(YawP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(YawP_UT_PATH)
	CC_INCLUDE_PATH		+= $(YawP_UNITY_PATH)
	YawP_Main_PATH 	:= YawP_UT_PATH\YawP_Main
endif
CC_INCLUDE_PATH    += $(YawP_CAL_PATH)
CC_INCLUDE_PATH    += $(YawP_SRC_PATH)
CC_INCLUDE_PATH    += $(YawP_CFG_PATH)
CC_INCLUDE_PATH    += $(YawP_HDR_PATH)
CC_INCLUDE_PATH    += $(YawP_IFA_PATH)
CC_INCLUDE_PATH    += $(YawP_CMN_PATH)

