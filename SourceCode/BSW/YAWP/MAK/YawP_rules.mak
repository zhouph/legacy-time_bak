# \file
#
# \brief YawP
#
# This file contains the implementation of the SWC
# module YawP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += YawP_src

YawP_src_FILES        += $(YawP_SRC_PATH)\YawP_Main.c
YawP_src_FILES        += $(YawP_SRC_PATH)\YawP_Process.c
YawP_src_FILES        += $(YawP_IFA_PATH)\YawP_Main_Ifa.c
YawP_src_FILES        += $(YawP_CFG_PATH)\YawP_Cfg.c
YawP_src_FILES        += $(YawP_CAL_PATH)\YawP_Cal.c

ifeq ($(ICE_COMPILE),true)
YawP_src_FILES        += $(YawP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	YawP_src_FILES        += $(YawP_UNITY_PATH)\unity.c
	YawP_src_FILES        += $(YawP_UNITY_PATH)\unity_fixture.c	
	YawP_src_FILES        += $(YawP_UT_PATH)\main.c
	YawP_src_FILES        += $(YawP_UT_PATH)\YawP_Main\YawP_Main_UtMain.c
endif