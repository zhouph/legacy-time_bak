YawP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Str'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
YawP_MainEemFailData = CreateBus(YawP_MainEemFailData, DeList);
clear DeList;

YawP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'YawRate'
    };
YawP_MainCanRxEscInfo = CreateBus(YawP_MainCanRxEscInfo, DeList);
clear DeList;

YawP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    };
YawP_MainEscSwtStInfo = CreateBus(YawP_MainEscSwtStInfo, DeList);
clear DeList;

YawP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
YawP_MainWhlSpdInfo = CreateBus(YawP_MainWhlSpdInfo, DeList);
clear DeList;

YawP_MainBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
YawP_MainBaseBrkCtrlModInfo = CreateBus(YawP_MainBaseBrkCtrlModInfo, DeList);
clear DeList;

YawP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
YawP_MainSenPwrMonitorData = CreateBus(YawP_MainSenPwrMonitorData, DeList);
clear DeList;

YawP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_SenPwr_12V'
    };
YawP_MainEemSuspectData = CreateBus(YawP_MainEemSuspectData, DeList);
clear DeList;

YawP_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Yaw'
    };
YawP_MainEemEceData = CreateBus(YawP_MainEemEceData, DeList);
clear DeList;

YawP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_SubBusOff_Err'
    };
YawP_MainCanMonData = CreateBus(YawP_MainCanMonData, DeList);
clear DeList;

YawP_MainIMUCalcInfo = Simulink.Bus;
DeList={
    'Reverse_Gear_flg'
    'Reverse_Judge_Time'
    };
YawP_MainIMUCalcInfo = CreateBus(YawP_MainIMUCalcInfo, DeList);
clear DeList;

YawP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
YawP_MainWssSpeedOut = CreateBus(YawP_MainWssSpeedOut, DeList);
clear DeList;

YawP_MainWssCalcInfo = Simulink.Bus;
DeList={
    'Rough_Sus_Flg'
    };
YawP_MainWssCalcInfo = CreateBus(YawP_MainWssCalcInfo, DeList);
clear DeList;

YawP_MainEcuModeSts = Simulink.Bus;
DeList={'YawP_MainEcuModeSts'};
YawP_MainEcuModeSts = CreateBus(YawP_MainEcuModeSts, DeList);
clear DeList;

YawP_MainIgnOnOffSts = Simulink.Bus;
DeList={'YawP_MainIgnOnOffSts'};
YawP_MainIgnOnOffSts = CreateBus(YawP_MainIgnOnOffSts, DeList);
clear DeList;

YawP_MainIgnEdgeSts = Simulink.Bus;
DeList={'YawP_MainIgnEdgeSts'};
YawP_MainIgnEdgeSts = CreateBus(YawP_MainIgnEdgeSts, DeList);
clear DeList;

YawP_MainVBatt1Mon = Simulink.Bus;
DeList={'YawP_MainVBatt1Mon'};
YawP_MainVBatt1Mon = CreateBus(YawP_MainVBatt1Mon, DeList);
clear DeList;

YawP_MainDiagClrSrs = Simulink.Bus;
DeList={'YawP_MainDiagClrSrs'};
YawP_MainDiagClrSrs = CreateBus(YawP_MainDiagClrSrs, DeList);
clear DeList;

YawP_MainVehSpd = Simulink.Bus;
DeList={'YawP_MainVehSpd'};
YawP_MainVehSpd = CreateBus(YawP_MainVehSpd, DeList);
clear DeList;

YawP_MainYawPlauOutput = Simulink.Bus;
DeList={
    'YawPlauModelErr'
    'YawPlauNoisekErr'
    'YawPlauShockErr'
    'YawPlauRangeErr'
    'YawPlauStandStillErr'
    };
YawP_MainYawPlauOutput = CreateBus(YawP_MainYawPlauOutput, DeList);
clear DeList;

