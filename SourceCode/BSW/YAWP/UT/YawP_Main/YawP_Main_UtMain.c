#include "unity.h"
#include "unity_fixture.h"
#include "YawP_Main.h"
#include "YawP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_Yaw[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_YAW;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_Ay[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_AY;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_Str[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_STR;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_YawP_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = YAWP_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Salsint16 UtInput_YawP_MainCanRxEscInfo_YawRate[MAX_STEP] = YAWP_MAINCANRXESCINFO_YAWRATE;
const Saluint8 UtInput_YawP_MainEscSwtStInfo_EscDisabledBySwt[MAX_STEP] = YAWP_MAINESCSWTSTINFO_ESCDISABLEDBYSWT;
const Saluint16 UtInput_YawP_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = YAWP_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_YawP_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = YAWP_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_YawP_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = YAWP_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_YawP_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = YAWP_MAINWHLSPDINFO_RRLWHLSPD;
const Rtesint32 UtInput_YawP_MainBaseBrkCtrlModInfo_VehStandStillStFlg[MAX_STEP] = YAWP_MAINBASEBRKCTRLMODINFO_VEHSTANDSTILLSTFLG;
const Saluint8 UtInput_YawP_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = YAWP_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_YawP_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = YAWP_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssFL[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFL;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssFR[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFR;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssRL[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRL;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssRR[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRR;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_FrontWss[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_RearWss[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_REARWSS;
const Saluint8 UtInput_YawP_MainEemSuspectData_Eem_Suspect_SenPwr_12V[MAX_STEP] = YAWP_MAINEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_12V;
const Saluint8 UtInput_YawP_MainEemEceData_Eem_Ece_Yaw[MAX_STEP] = YAWP_MAINEEMECEDATA_EEM_ECE_YAW;
const Saluint8 UtInput_YawP_MainCanMonData_CanM_SubBusOff_Err[MAX_STEP] = YAWP_MAINCANMONDATA_CANM_SUBBUSOFF_ERR;
const Saluint8 UtInput_YawP_MainIMUCalcInfo_Reverse_Gear_flg[MAX_STEP] = YAWP_MAINIMUCALCINFO_REVERSE_GEAR_FLG;
const Saluint8 UtInput_YawP_MainIMUCalcInfo_Reverse_Judge_Time[MAX_STEP] = YAWP_MAINIMUCALCINFO_REVERSE_JUDGE_TIME;
const Saluint32 UtInput_YawP_MainWssSpeedOut_WssMax[MAX_STEP] = YAWP_MAINWSSSPEEDOUT_WSSMAX;
const Saluint32 UtInput_YawP_MainWssSpeedOut_WssMin[MAX_STEP] = YAWP_MAINWSSSPEEDOUT_WSSMIN;
const Saluint8 UtInput_YawP_MainWssCalcInfo_Rough_Sus_Flg[MAX_STEP] = YAWP_MAINWSSCALCINFO_ROUGH_SUS_FLG;
const Mom_HndlrEcuModeSts_t UtInput_YawP_MainEcuModeSts[MAX_STEP] = YAWP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_YawP_MainIgnOnOffSts[MAX_STEP] = YAWP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_YawP_MainIgnEdgeSts[MAX_STEP] = YAWP_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_YawP_MainVBatt1Mon[MAX_STEP] = YAWP_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_YawP_MainDiagClrSrs[MAX_STEP] = YAWP_MAINDIAGCLRSRS;
const Abc_CtrlVehSpd_t UtInput_YawP_MainVehSpd[MAX_STEP] = YAWP_MAINVEHSPD;

const Saluint8 UtExpected_YawP_MainYawPlauOutput_YawPlauModelErr[MAX_STEP] = YAWP_MAINYAWPLAUOUTPUT_YAWPLAUMODELERR;
const Saluint8 UtExpected_YawP_MainYawPlauOutput_YawPlauNoisekErr[MAX_STEP] = YAWP_MAINYAWPLAUOUTPUT_YAWPLAUNOISEKERR;
const Saluint8 UtExpected_YawP_MainYawPlauOutput_YawPlauShockErr[MAX_STEP] = YAWP_MAINYAWPLAUOUTPUT_YAWPLAUSHOCKERR;
const Saluint8 UtExpected_YawP_MainYawPlauOutput_YawPlauRangeErr[MAX_STEP] = YAWP_MAINYAWPLAUOUTPUT_YAWPLAURANGEERR;
const Saluint8 UtExpected_YawP_MainYawPlauOutput_YawPlauStandStillErr[MAX_STEP] = YAWP_MAINYAWPLAUOUTPUT_YAWPLAUSTANDSTILLERR;



TEST_GROUP(YawP_Main);
TEST_SETUP(YawP_Main)
{
    YawP_Main_Init();
}

TEST_TEAR_DOWN(YawP_Main)
{   /* Postcondition */

}

TEST(YawP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        YawP_MainEemFailData.Eem_Fail_SenPwr_12V = UtInput_YawP_MainEemFailData_Eem_Fail_SenPwr_12V[i];
        YawP_MainEemFailData.Eem_Fail_Yaw = UtInput_YawP_MainEemFailData_Eem_Fail_Yaw[i];
        YawP_MainEemFailData.Eem_Fail_Ay = UtInput_YawP_MainEemFailData_Eem_Fail_Ay[i];
        YawP_MainEemFailData.Eem_Fail_Str = UtInput_YawP_MainEemFailData_Eem_Fail_Str[i];
        YawP_MainEemFailData.Eem_Fail_WssFL = UtInput_YawP_MainEemFailData_Eem_Fail_WssFL[i];
        YawP_MainEemFailData.Eem_Fail_WssFR = UtInput_YawP_MainEemFailData_Eem_Fail_WssFR[i];
        YawP_MainEemFailData.Eem_Fail_WssRL = UtInput_YawP_MainEemFailData_Eem_Fail_WssRL[i];
        YawP_MainEemFailData.Eem_Fail_WssRR = UtInput_YawP_MainEemFailData_Eem_Fail_WssRR[i];
        YawP_MainCanRxEscInfo.YawRate = UtInput_YawP_MainCanRxEscInfo_YawRate[i];
        YawP_MainEscSwtStInfo.EscDisabledBySwt = UtInput_YawP_MainEscSwtStInfo_EscDisabledBySwt[i];
        YawP_MainWhlSpdInfo.FlWhlSpd = UtInput_YawP_MainWhlSpdInfo_FlWhlSpd[i];
        YawP_MainWhlSpdInfo.FrWhlSpd = UtInput_YawP_MainWhlSpdInfo_FrWhlSpd[i];
        YawP_MainWhlSpdInfo.RlWhlSpd = UtInput_YawP_MainWhlSpdInfo_RlWhlSpd[i];
        YawP_MainWhlSpdInfo.RrlWhlSpd = UtInput_YawP_MainWhlSpdInfo_RrlWhlSpd[i];
        YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = UtInput_YawP_MainBaseBrkCtrlModInfo_VehStandStillStFlg[i];
        YawP_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_YawP_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        YawP_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_YawP_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        YawP_MainEemSuspectData.Eem_Suspect_WssFL = UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssFL[i];
        YawP_MainEemSuspectData.Eem_Suspect_WssFR = UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssFR[i];
        YawP_MainEemSuspectData.Eem_Suspect_WssRL = UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssRL[i];
        YawP_MainEemSuspectData.Eem_Suspect_WssRR = UtInput_YawP_MainEemSuspectData_Eem_Suspect_WssRR[i];
        YawP_MainEemSuspectData.Eem_Suspect_FrontWss = UtInput_YawP_MainEemSuspectData_Eem_Suspect_FrontWss[i];
        YawP_MainEemSuspectData.Eem_Suspect_RearWss = UtInput_YawP_MainEemSuspectData_Eem_Suspect_RearWss[i];
        YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = UtInput_YawP_MainEemSuspectData_Eem_Suspect_SenPwr_12V[i];
        YawP_MainEemEceData.Eem_Ece_Yaw = UtInput_YawP_MainEemEceData_Eem_Ece_Yaw[i];
        YawP_MainCanMonData.CanM_SubBusOff_Err = UtInput_YawP_MainCanMonData_CanM_SubBusOff_Err[i];
        YawP_MainIMUCalcInfo.Reverse_Gear_flg = UtInput_YawP_MainIMUCalcInfo_Reverse_Gear_flg[i];
        YawP_MainIMUCalcInfo.Reverse_Judge_Time = UtInput_YawP_MainIMUCalcInfo_Reverse_Judge_Time[i];
        YawP_MainWssSpeedOut.WssMax = UtInput_YawP_MainWssSpeedOut_WssMax[i];
        YawP_MainWssSpeedOut.WssMin = UtInput_YawP_MainWssSpeedOut_WssMin[i];
        YawP_MainWssCalcInfo.Rough_Sus_Flg = UtInput_YawP_MainWssCalcInfo_Rough_Sus_Flg[i];
        YawP_MainEcuModeSts = UtInput_YawP_MainEcuModeSts[i];
        YawP_MainIgnOnOffSts = UtInput_YawP_MainIgnOnOffSts[i];
        YawP_MainIgnEdgeSts = UtInput_YawP_MainIgnEdgeSts[i];
        YawP_MainVBatt1Mon = UtInput_YawP_MainVBatt1Mon[i];
        YawP_MainDiagClrSrs = UtInput_YawP_MainDiagClrSrs[i];
        YawP_MainVehSpd = UtInput_YawP_MainVehSpd[i];

        YawP_Main();

        TEST_ASSERT_EQUAL(YawP_MainYawPlauOutput.YawPlauModelErr, UtExpected_YawP_MainYawPlauOutput_YawPlauModelErr[i]);
        TEST_ASSERT_EQUAL(YawP_MainYawPlauOutput.YawPlauNoisekErr, UtExpected_YawP_MainYawPlauOutput_YawPlauNoisekErr[i]);
        TEST_ASSERT_EQUAL(YawP_MainYawPlauOutput.YawPlauShockErr, UtExpected_YawP_MainYawPlauOutput_YawPlauShockErr[i]);
        TEST_ASSERT_EQUAL(YawP_MainYawPlauOutput.YawPlauRangeErr, UtExpected_YawP_MainYawPlauOutput_YawPlauRangeErr[i]);
        TEST_ASSERT_EQUAL(YawP_MainYawPlauOutput.YawPlauStandStillErr, UtExpected_YawP_MainYawPlauOutput_YawPlauStandStillErr[i]);
    }
}

TEST_GROUP_RUNNER(YawP_Main)
{
    RUN_TEST_CASE(YawP_Main, All);
}
