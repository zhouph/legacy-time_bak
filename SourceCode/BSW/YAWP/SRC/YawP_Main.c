/**
 * @defgroup YawP_Main YawP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawP_Main.h"
#include "YawP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define YAWP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "YawP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define YAWP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "YawP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
YawP_Main_HdrBusType YawP_MainBus;

/* Version Info */
const SwcVersionInfo_t YawP_MainVersionInfo = 
{   
    YAWP_MAIN_MODULE_ID,           /* YawP_MainVersionInfo.ModuleId */
    YAWP_MAIN_MAJOR_VERSION,       /* YawP_MainVersionInfo.MajorVer */
    YAWP_MAIN_MINOR_VERSION,       /* YawP_MainVersionInfo.MinorVer */
    YAWP_MAIN_PATCH_VERSION,       /* YawP_MainVersionInfo.PatchVer */
    YAWP_MAIN_BRANCH_VERSION       /* YawP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t YawP_MainEemFailData;
Proxy_RxCanRxEscInfo_t YawP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t YawP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t YawP_MainWhlSpdInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t YawP_MainBaseBrkCtrlModInfo;
SenPwrM_MainSenPwrMonitor_t YawP_MainSenPwrMonitorData;
Eem_MainEemSuspectData_t YawP_MainEemSuspectData;
Eem_MainEemEceData_t YawP_MainEemEceData;
CanM_MainCanMonData_t YawP_MainCanMonData;
Proxy_RxIMUCalc_t YawP_MainIMUCalcInfo;
Wss_SenWssSpeedOut_t YawP_MainWssSpeedOut;
Wss_SenWssCalc_t YawP_MainWssCalcInfo;
Mom_HndlrEcuModeSts_t YawP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t YawP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t YawP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t YawP_MainVBatt1Mon;
Diag_HndlrDiagClr_t YawP_MainDiagClrSrs;
Abc_CtrlVehSpd_t YawP_MainVehSpd;

/* Output Data Element */
YawP_MainYawPlauOutput_t YawP_MainYawPlauOutput;

uint32 YawP_Main_Timer_Start;
uint32 YawP_Main_Timer_Elapsed;

#define YAWP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
#define YAWP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_MAIN_START_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWP_MAIN_STOP_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
#define YAWP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_MAIN_START_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWP_MAIN_STOP_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define YAWP_MAIN_START_SEC_CODE
#include "YawP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void YawP_Main_Init(void)
{
    /* Initialize internal bus */
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_SenPwr_12V = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_Yaw = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_Ay = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_Str = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssFL = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssFR = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssRL = 0;
    YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssRR = 0;
    YawP_MainBus.YawP_MainCanRxEscInfo.YawRate = 0;
    YawP_MainBus.YawP_MainEscSwtStInfo.EscDisabledBySwt = 0;
    YawP_MainBus.YawP_MainWhlSpdInfo.FlWhlSpd = 0;
    YawP_MainBus.YawP_MainWhlSpdInfo.FrWhlSpd = 0;
    YawP_MainBus.YawP_MainWhlSpdInfo.RlWhlSpd = 0;
    YawP_MainBus.YawP_MainWhlSpdInfo.RrlWhlSpd = 0;
    YawP_MainBus.YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = 0;
    YawP_MainBus.YawP_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    YawP_MainBus.YawP_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssFL = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssFR = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssRL = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssRR = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_FrontWss = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_RearWss = 0;
    YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = 0;
    YawP_MainBus.YawP_MainEemEceData.Eem_Ece_Yaw = 0;
    YawP_MainBus.YawP_MainCanMonData.CanM_SubBusOff_Err = 0;
    YawP_MainBus.YawP_MainIMUCalcInfo.Reverse_Gear_flg = 0;
    YawP_MainBus.YawP_MainIMUCalcInfo.Reverse_Judge_Time = 0;
    YawP_MainBus.YawP_MainWssSpeedOut.WssMax = 0;
    YawP_MainBus.YawP_MainWssSpeedOut.WssMin = 0;
    YawP_MainBus.YawP_MainWssCalcInfo.Rough_Sus_Flg = 0;
    YawP_MainBus.YawP_MainEcuModeSts = 0;
    YawP_MainBus.YawP_MainIgnOnOffSts = 0;
    YawP_MainBus.YawP_MainIgnEdgeSts = 0;
    YawP_MainBus.YawP_MainVBatt1Mon = 0;
    YawP_MainBus.YawP_MainDiagClrSrs = 0;
    YawP_MainBus.YawP_MainVehSpd = 0;
    YawP_MainBus.YawP_MainYawPlauOutput.YawPlauModelErr = 0;
    YawP_MainBus.YawP_MainYawPlauOutput.YawPlauNoisekErr = 0;
    YawP_MainBus.YawP_MainYawPlauOutput.YawPlauShockErr = 0;
    YawP_MainBus.YawP_MainYawPlauOutput.YawPlauRangeErr = 0;
    YawP_MainBus.YawP_MainYawPlauOutput.YawPlauStandStillErr = 0;
}

void YawP_Main(void)
{
    YawP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_SenPwr_12V(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_SenPwr_12V);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_Yaw(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_Yaw);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_Ay(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_Ay);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_Str(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_Str);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssFL(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssFL);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssFR(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssFR);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssRL(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssRL);
    YawP_Main_Read_YawP_MainEemFailData_Eem_Fail_WssRR(&YawP_MainBus.YawP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainCanRxEscInfo_YawRate(&YawP_MainBus.YawP_MainCanRxEscInfo.YawRate);

    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainEscSwtStInfo_EscDisabledBySwt(&YawP_MainBus.YawP_MainEscSwtStInfo.EscDisabledBySwt);

    YawP_Main_Read_YawP_MainWhlSpdInfo(&YawP_MainBus.YawP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure YawP_MainWhlSpdInfo 
     : YawP_MainWhlSpdInfo.FlWhlSpd;
     : YawP_MainWhlSpdInfo.FrWhlSpd;
     : YawP_MainWhlSpdInfo.RlWhlSpd;
     : YawP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(&YawP_MainBus.YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainSenPwrMonitorData_SenPwrM_5V_Stable(&YawP_MainBus.YawP_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    YawP_Main_Read_YawP_MainSenPwrMonitorData_SenPwrM_12V_Stable(&YawP_MainBus.YawP_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFL(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssFL);
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFR(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssFR);
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRL(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssRL);
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRR(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_WssRR);
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_FrontWss(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_FrontWss);
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_RearWss(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_RearWss);
    YawP_Main_Read_YawP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(&YawP_MainBus.YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V);

    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainEemEceData_Eem_Ece_Yaw(&YawP_MainBus.YawP_MainEemEceData.Eem_Ece_Yaw);

    /* Decomposed structure interface */
    YawP_Main_Read_YawP_MainCanMonData_CanM_SubBusOff_Err(&YawP_MainBus.YawP_MainCanMonData.CanM_SubBusOff_Err);

    YawP_Main_Read_YawP_MainIMUCalcInfo(&YawP_MainBus.YawP_MainIMUCalcInfo);
    /*==============================================================================
    * Members of structure YawP_MainIMUCalcInfo 
     : YawP_MainIMUCalcInfo.Reverse_Gear_flg;
     : YawP_MainIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/
    
    YawP_Main_Read_YawP_MainWssSpeedOut(&YawP_MainBus.YawP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure YawP_MainWssSpeedOut 
     : YawP_MainWssSpeedOut.WssMax;
     : YawP_MainWssSpeedOut.WssMin;
     =============================================================================*/
    
    YawP_Main_Read_YawP_MainWssCalcInfo(&YawP_MainBus.YawP_MainWssCalcInfo);
    /*==============================================================================
    * Members of structure YawP_MainWssCalcInfo 
     : YawP_MainWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/
    
    YawP_Main_Read_YawP_MainEcuModeSts(&YawP_MainBus.YawP_MainEcuModeSts);
    YawP_Main_Read_YawP_MainIgnOnOffSts(&YawP_MainBus.YawP_MainIgnOnOffSts);
    YawP_Main_Read_YawP_MainIgnEdgeSts(&YawP_MainBus.YawP_MainIgnEdgeSts);
    YawP_Main_Read_YawP_MainVBatt1Mon(&YawP_MainBus.YawP_MainVBatt1Mon);
    YawP_Main_Read_YawP_MainDiagClrSrs(&YawP_MainBus.YawP_MainDiagClrSrs);
    YawP_Main_Read_YawP_MainVehSpd(&YawP_MainBus.YawP_MainVehSpd);

    /* Process */
    YawP_Process(&YawP_MainBus);
    /* Output */
    YawP_Main_Write_YawP_MainYawPlauOutput(&YawP_MainBus.YawP_MainYawPlauOutput);
    /*==============================================================================
    * Members of structure YawP_MainYawPlauOutput 
     : YawP_MainYawPlauOutput.YawPlauModelErr;
     : YawP_MainYawPlauOutput.YawPlauNoisekErr;
     : YawP_MainYawPlauOutput.YawPlauShockErr;
     : YawP_MainYawPlauOutput.YawPlauRangeErr;
     : YawP_MainYawPlauOutput.YawPlauStandStillErr;
     =============================================================================*/
    

    YawP_Main_Timer_Elapsed = STM0_TIM0.U - YawP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define YAWP_MAIN_STOP_SEC_CODE
#include "YawP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
