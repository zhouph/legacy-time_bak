/**
 * @defgroup YawP_Process YawP_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawP_Main.h"
#include "YawP_Main_Ifa.h"
#include "YawP_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define YAWP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "YawP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define YAWP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "YawP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"

extern signed int Proxy_fys16EstimatedYaw; /* TODO */

static Saluint8 YawP_Inhibit_Define(YawP_Main_HdrBusType *pYawInhibitDefine);
static Saluint8 YawP_StandStillState(YawP_Main_HdrBusType *pStandStillState);
static void YawP_Range(YawP_Main_HdrBusType *pYawPRange, boolean inhibit);
static void YawP_StandstillOffsetChk(YawP_Main_HdrBusType *pYawPStandstillChk, boolean inhibit);
static void YawP_NoiseChk(YawP_Main_HdrBusType *pYawPNoiseChk, boolean inhibit);

void YawP_Process(YawP_Main_HdrBusType *pYawP_Process)
{
    boolean Inhibit  = YawP_Inhibit_Define(pYawP_Process);
    YawP_Range(pYawP_Process,Inhibit);
    YawP_StandstillOffsetChk(pYawP_Process,Inhibit);
    YawP_NoiseChk(pYawP_Process,Inhibit);
}

static Saluint8 YawP_Inhibit_Define(YawP_Main_HdrBusType *pYawInhibitDefine)
{
    boolean inhibit =YAW_ALLOW;
    if((pYawInhibitDefine->YawP_MainSenPwrMonitorData.SenPwrM_12V_Open_Err==1)
        ||(pYawInhibitDefine->YawP_MainSenPwrMonitorData.SenPwrM_12V_Short_Err==1)
        ||(pYawInhibitDefine->YawP_MainSenPwrMonitorData.SenPwrM_12V_Stable==1)
        ||(pYawInhibitDefine->YawP_MainCanMonData.CanM_SubBusOff_Err==1)
        ||(pYawInhibitDefine->YawP_MainEemEceData.Eem_Ece_Yaw==1)
        ||(pYawInhibitDefine->YawP_MainEemFailData.Eem_Fail_Yaw==1)
     )
    {
        inhibit =YAW_INHIBIT;
    }

    return inhibit;
}

/***************
     Yaw Range Error 
     **************/
static void YawP_Range(YawP_Main_HdrBusType *pYawPRange, boolean inhibit)
{
    Saluint16 abs_u16EstimatedYaw = 0;

    abs_u16EstimatedYaw = abs(Proxy_fys16EstimatedYaw);

    /* TODO : New Concept
                    if((rev_for_judge_time<=0)&&(fu1CLSTEEROK==1)&&(Temp_Abs_STR_Mdl_0_<U16_STEER_300DEG))
    */
    
    if(abs_u16EstimatedYaw >= DEF_YAW_75DPS)
    {
        pYawPRange->YawP_MainYawPlauOutput.YawPlauRangeErr = ERR_PREFAILED;
    }
    else
    {
        pYawPRange->YawP_MainYawPlauOutput.YawPlauRangeErr = ERR_PREPASSED;
    }

    if(inhibit==TRUE)
    {
        pYawPRange->YawP_MainYawPlauOutput.YawPlauRangeErr = ERR_INHIBIT;
    }
}

/***************
     Yaw Standstill Error 
     **************/
static void YawP_StandstillOffsetChk(YawP_Main_HdrBusType *pYawPStandstillChk, boolean inhibit)
{
    Salsint16 Yaw_1_100deg = Proxy_fys16EstimatedYaw;
    Saluint8 Yaw_Standstill =YawP_StandStillState(pYawPStandstillChk);
    static Saluint8 Yaw_State =DEF_YAW_STAND_OFFSET_INIT;

    switch(Yaw_State)
    {
        case DEF_YAW_STAND_OFFSET_INIT:
            if(Yaw_1_100deg >DEF_YAW_6DPS)
            {
                Yaw_State = DEF_YAW_STAND_OFFSET_SUSPECT;
            }
            break;
        case DEF_YAW_STAND_OFFSET_SUSPECT:
            if(Yaw_Standstill ==DEF_STANDSTILL)
            {
                Yaw_State =DEF_YAW_STAND_OFFSET_INVALID;
            }
            else if(Yaw_1_100deg<=DEF_YAW_6DPS)
            {
                Yaw_State =DEF_YAW_STAND_OFFSET_INIT;
            }
            else
            {
                ;
            }
            break;
         case DEF_YAW_STAND_OFFSET_INVALID:
             if (pYawPStandstillChk->YawP_MainVehSpd>U16_F64SPEED_15KPH)
            {
                Yaw_State =DEF_YAW_STAND_OFFSET_ERR;
            }
            else if(Yaw_1_100deg<=DEF_YAW_6DPS)
            {
                Yaw_State =DEF_YAW_STAND_OFFSET_INIT;
            }
            else
            {
                ;
            }
            break;
         case DEF_YAW_STAND_OFFSET_ERR:
            Yaw_State =DEF_YAW_STAND_OFFSET_INIT;
            break;
       default:
        break;
    }

    if (Yaw_State ==DEF_YAW_STAND_OFFSET_ERR)
    {
          pYawPStandstillChk->YawP_MainYawPlauOutput.YawPlauStandStillErr = ERR_FAILED;
 
    }
    else
    {
         pYawPStandstillChk->YawP_MainYawPlauOutput.YawPlauStandStillErr = ERR_PREPASSED;
    }

    if(inhibit==TRUE)
    {
        pYawPStandstillChk->YawP_MainYawPlauOutput.YawPlauStandStillErr = ERR_INHIBIT;
    }
}

static Saluint8 YawP_StandStillState(YawP_Main_HdrBusType *pStandStillState)
{
    Salsint32 Vehicle_Speed = U8_F64SPEED_0KPH25;
    Saluint32 Vehicle_Max_Wheel_Speed =U8_F64SPEED_0KPH25;
    static Saluint16 Standstill_Time =0;
    static Saluint8 StandStill_State =DEF_NON_STANDSTILL;   

    Vehicle_Speed =pStandStillState->YawP_MainVehSpd;
    Vehicle_Max_Wheel_Speed   =pStandStillState->YawP_MainWssSpeedOut.WssMax;
       

    if((Vehicle_Speed <=U8_F64SPEED_2KPH25)&&(Vehicle_Max_Wheel_Speed<U8_F64SPEED_0KPH25))
    {
        if(Standstill_Time<DEF_2SEC_TIME)
        {
            Standstill_Time ++;
        }
        else
        {
            StandStill_State =DEF_STANDSTILL;
        }
    }
    else
    {
        StandStill_State =DEF_NON_STANDSTILL;
        Standstill_Time =0;
    }

    return StandStill_State;
}


/***************
     Yaw Noise Error 
     **************/
static void YawP_NoiseChk(YawP_Main_HdrBusType *pYawPNoiseChk, boolean inhibit)
{
    static Salsint16 Yaw_1_100deg =0;
    Salsint16 Yaw_1_100deg_old = 0;
    Saluint16 Diff_Yaw_Value  =0;
    static Saluint16 Yaw_Noise_Clear_Cnt =0;
    boolean YawP_Noise_Inhibit=0;
    
    Yaw_1_100deg_old =Yaw_1_100deg;
    Yaw_1_100deg =Proxy_fys16EstimatedYaw;
    Diff_Yaw_Value =(Saluint16)(abs(Yaw_1_100deg-Yaw_1_100deg_old));
    if(Yaw_Noise_Clear_Cnt>DEF_5SEC_TIME)
    {
        pYawPNoiseChk->YawP_MainYawPlauOutput.YawPlauNoisekErr =ERR_PASSED;
        Yaw_Noise_Clear_Cnt =0;
    }
    else
    {
        Yaw_Noise_Clear_Cnt++;
    }
    
    if(Diff_Yaw_Value>DEF_YAW_5DPS)
    {
        pYawPNoiseChk->YawP_MainYawPlauOutput.YawPlauNoisekErr =ERR_PREFAILED;
    }
    else
    {
        pYawPNoiseChk->YawP_MainYawPlauOutput.YawPlauNoisekErr =ERR_PREPASSED;
    }

    if( (pYawPNoiseChk->YawP_MainWssCalcInfo.Rough_Sus_Flg==1)
    )
    {
        YawP_Noise_Inhibit =1;
    }

    if((inhibit==TRUE)||(YawP_Noise_Inhibit==TRUE))
    {
        pYawPNoiseChk->YawP_MainYawPlauOutput.YawPlauNoisekErr= ERR_INHIBIT;
    }
}