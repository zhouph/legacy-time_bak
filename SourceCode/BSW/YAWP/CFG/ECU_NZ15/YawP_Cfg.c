/**
 * @defgroup YawP_Cfg YawP_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawP_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawP_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define YAWP_START_SEC_CONST_UNSPECIFIED
#include "YawP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define YAWP_STOP_SEC_CONST_UNSPECIFIED
#include "YawP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_START_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWP_STOP_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
#define YAWP_START_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWP_STOP_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_START_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWP_STOP_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_START_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWP_STOP_SEC_VAR_NOINIT_32BIT
#include "YawP_MemMap.h"
#define YAWP_START_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWP_STOP_SEC_VAR_UNSPECIFIED
#include "YawP_MemMap.h"
#define YAWP_START_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWP_STOP_SEC_VAR_32BIT
#include "YawP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define YAWP_START_SEC_CODE
#include "YawP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define YAWP_STOP_SEC_CODE
#include "YawP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
