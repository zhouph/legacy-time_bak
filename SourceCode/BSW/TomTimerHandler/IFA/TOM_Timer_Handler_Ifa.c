/**
 * @defgroup TOMTimerHandler_Ifa TOMTimerHandler_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TOMTimerHandler_Ifa.c
 * @brief       Template file
 * @date        2014. 7. 7.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TOMTimerHandler.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_CONST_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define TOMTIMERHANDLER_STOP_SEC_CONST_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_CODE
#include "TOMTimerHandler_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TOMTIMERHANDLER_STOP_SEC_CODE
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
