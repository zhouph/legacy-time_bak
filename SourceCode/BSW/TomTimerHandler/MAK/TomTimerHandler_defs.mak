# \file
#
# \brief TomTimerHandler
#
# This file contains the implementation of the BSW
# module TomTimerHandler.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

TomTimerHandler_CORE_PATH     := $(MANDO_BSW_ROOT)\TomTimerHandler
TomTimerHandler_SRC_PATH      := $(TomTimerHandler_CORE_PATH)\SRC
TomTimerHandler_CFG_PATH      := $(TomTimerHandler_CORE_PATH)\CFG
TomTimerHandler_HDR_PATH      := $(TomTimerHandler_CORE_PATH)\HDR
TomTimerHandler_CMN_PATH      := $(TomTimerHandler_CORE_PATH)\CMN
TomTimerHandler_IFA_PATH      := $(TomTimerHandler_CORE_PATH)\IFA

CC_INCLUDE_PATH    += $(TomTimerHandler_CFG_PATH)
CC_INCLUDE_PATH    += $(TomTimerHandler_HDR_PATH)
CC_INCLUDE_PATH    += $(TomTimerHandler_CMN_PATH)
CC_INCLUDE_PATH    += $(TomTimerHandler_IFA_PATH)
CC_INCLUDE_PATH    += $(TomTimerHandler_SRC_PATH)

