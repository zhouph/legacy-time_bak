# \file
#
# \brief TomTimerHandler
#
# This file contains the implementation of the BSW
# module TomTimerHandler.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += TomTimerHandler_src

TomTimerHandler_src_FILES       += $(TomTimerHandler_CORE_PATH)\SRC\TOM_Timer_Driver.c
TomTimerHandler_src_FILES       += $(TomTimerHandler_CORE_PATH)\SRC\TOM_Timer_Handler.c

#################################################################
