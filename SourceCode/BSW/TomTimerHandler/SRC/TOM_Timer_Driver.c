/**
 * @defgroup TOM_Timer_Driver TOM_Timer_Driver
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TOM_Timer_Driver.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TOM_Timer_Driver.h"
#include "Gpt.h"
#include "../../LIB/Common.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_CONST_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Constant Section (UNSPECIFIED)**/


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define TOMTIMERHANDLER_STOP_SEC_CONST_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"

#define TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
#include "TOMTimerHandler_MemMap.h" 
#define TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (UNSPECIFIED)**/

#define TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
#include "TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/** Variable Section (32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_CODE
#include "TOMTimerHandler_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void TOM_Timer_Driver_Init(void)
{
	Gpt_StartTimer(GptConf_GptChannel_System_FreeRunCnt,0xFFFF);		// Free Running Counter Enable
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TOMTIMERHANDLER_STOP_SEC_CODE
#include "TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
