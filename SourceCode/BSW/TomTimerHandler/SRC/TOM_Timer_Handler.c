/**
 * @defgroup TOMTimerHandler TOMTimerHandler
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TOMTimerHandler.c
 * @brief       SWC Source File(with Scheduled functions)
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TOM_Timer_Driver.h"
#include "Gpt.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_CONST_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define TOMTIMERHANDLER_STOP_SEC_CONST_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
//msw TOMTimerHandler_HDRbusType    TOM_Timerbus;

#define TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h" 
#define TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
#include "../HDR/TOMTimerHandler_MemMap.h"
#define TOMTIMERHANDLER_START_SEC_VAR_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
/** Variable Section (32BIT)**/


#define TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
#include "../HDR/TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TOMTIMERHANDLER_START_SEC_CODE
#include "../HDR/TOMTimerHandler_MemMap.h"


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void TOM_Timer_Initialization(void)
{
    TOM_Timer_Driver_Init();
}


//****************************************************************************
// Configurated function in Tresos for 1ms timer Interrupt
//****************************************************************************


/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TOMTIMERHANDLER_STOP_SEC_CODE
#include "../HDR/TOMTimerHandler_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
