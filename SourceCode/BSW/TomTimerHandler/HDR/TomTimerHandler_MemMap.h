/**
 * @defgroup TOMTimerHandler_MemMap TOMTimerHandler_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TOMTimerHandler_MemMap.h
 * @brief       Template file
 * @date        2014. 7. 7.
 ******************************************************************************/

#ifndef TOMTIMERHANDLER_MEMMAP_H_
#define TOMTIMERHANDLER_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (TOMTIMERHANDLER_START_SEC_CODE)
  #undef TOMTIMERHANDLER_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_CODE)
  #undef TOMTIMERHANDLER_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_CODE_STARTED
    #error "TOMTIMERHANDLER_SEC_CODE not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (TOMTIMERHANDLER_START_SEC_CONST_UNSPECIFIED)
  #undef TOMTIMERHANDLER_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_CONST_UNSPECIFIED)
  #undef TOMTIMERHANDLER_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_CONST_UNSPECIFIED_STARTED
    #error "TOMTIMERHANDLER_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED)
  #undef TOMTIMERHANDLER_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED)
  #undef TOMTIMERHANDLER_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_VAR_UNSPECIFIED_STARTED /* EB_BACFIX BAC-1046 */
    #error "TOMTIMERHANDLER_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_START_SEC_VAR_32BIT)
  #undef TOMTIMERHANDLER_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_VAR_32BIT)
  #undef TOMTIMERHANDLER_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_VAR_32BIT_STARTED /* EB_BACFIX BAC-1046 */
    #error "TOMTIMERHANDLER_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef TOMTIMERHANDLER_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_VAR_NOINIT_UNSPECIFIED_STARTED /* EB_BACFIX BAC-1046 */
    #error "TOMTIMERHANDLER_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT)
  #undef TOMTIMERHANDLER_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT)
  #undef TOMTIMERHANDLER_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_VAR_NOINIT_32BIT_STARTED /* EB_BACFIX BAC-1046 */
    #error "TOMTIMERHANDLER_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR
/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (TOMTIMERHANDLER_START_SEC_CALIB_UNSPECIFIED)
  #undef TOMTIMERHANDLER_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_TOMTIMERHANDLER_SEC_STARTED
    #error "TOM_TIMER_HANDLER section not closed"
  #endif
  #define CHK_TOMTIMERHANDLER_SEC_STARTED
  #define CHK_TOMTIMERHANDLER_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (TOMTIMERHANDLER_STOP_SEC_CALIB_UNSPECIFIED)
  #undef TOMTIMERHANDLER_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_TOMTIMERHANDLER_SEC_CALIB_UNSPECIFIED_STARTED /* EB_BACFIX BAC-1046 */
    #error "TOMTIMERHANDLER_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_TOMTIMERHANDLER_SEC_STARTED
  #undef CHK_TOMTIMERHANDLER_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR
#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TOMTIMERHANDLER_MEMMAP_H_ */
/** @} */
