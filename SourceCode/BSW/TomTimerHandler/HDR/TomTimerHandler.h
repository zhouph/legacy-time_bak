/**
 * @defgroup TomTimerHandler TomTimerHandler
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TomTimerHandler.h
 * @brief       SWC Header File(with Scheduled functions)
 * @date        20YY. MM. DD.
 ******************************************************************************/
/***************************************************************************//**
 * @mainpage    TomTimerHandler MainPage
 * @section INTRO   Introduction
 *  - 소개 : 프로그램의 기본을 배울수있는 프로그램.
 * @section AUTHOR  Developer
 *  - Developer Name(e-mail)
 * @section PROGRAM Program
 *  - Name : Hello World 프로그램.
 *  - Context : 화면에 Hello World!을 출력한다.
 * @section INFO Information
 *  - Doygen 문서 테스트용 소스
 * @section ADVENCED Advenced
 *  - 글머리는 '-' 태그를 사용하면 되며
 *  - 탭으로 들여쓸경우 하위 항목이 된다.
 *  -# 번호매기기는 '-#' 방식으로 할수 있다.
 *  -# 위와 같이 탭으로 들여쓸경우 하위 항목이 된다.
 *  -# 두번째 하위 항목
 *
 * @section  INOUTPUT Input/Output Information
 *  - INPUT : 없음.
 *  - OUTPUT : 화면출력.
 * @section  CREATEINFO Create Information
 *  - Author : infiniterun
 *  - Date : 2005. 04. 18
 * @section  MODIFYINFO Revision History
 *  - Modifier / Modified Date : Gwangho Choi / 20YY. MM. DD.
 * <pre>
 *------------------------------------------------------------------------------
 *      Date       Version      Author       Description
 * -------------   -------   ------------  -------------------------------------
 * 20YY. MM. DD.    1.0.0     Gwangho Choi     Initial version
 * </pre>
 *------------------------------------------------------------------------------
 ******************************************************************************/
 
#ifndef TOMTIMERHANDLER_H_
#define TOMTIMERHANDLER_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TomTimerHandler_Types.h"
#include "TomTimerHandler_Cfg.h"
#include "TomTimerHandler_Ifa.h"
#include "Gpt.h"

/* example */
//#include CDD_PATH_LIB(SwcCommonFunction.h)

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define CURRENT_TIME		Gpt_GetTimeElapsed(GptConf_GptChannel_System_FreeRunCnt)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
//extern TomTimerHandler_HDRbusType    TOM_Timerbus;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
/***************************************************************************//**
 * @fn      void WI_vISR_NotificationCtrl1msTimer(void)
 * @brief   No description
 * @par Runnable Entity Information
 *  -# Sync/Async : Synchronous
 *  -# Reentrancy : Non reentrant
 *  -# Timing : Fixed Cyclic
 *  -# Trigger Event : Period Event
 *
 * @par Parameters (In)
 *  - none
 *
 * @par Parameters (Out)
 *  - none
 *
 * @par Inter-runnable Variables
 *  - none
 ******************************************************************************/
extern void WI_vISR_NotificationCtrl1msTimer(void);

/***************************************************************************//**
 * @fn      void TOM_Timer_Initialization(void)
 * @brief   No description
 * @par Runnable Entity Information
 *  -# Sync/Async : Synchronous
 *  -# Reentrancy : Non reentrant
 *  -# Timing : Fixed Cyclic
 *  -# Trigger Event : Period Event
 *
 * @par Parameters (In)
 *  - none
 *
 * @par Parameters (Out)
 *  - none
 *
 * @par Inter-runnable Variables
 *  - none
 ******************************************************************************/
extern void TOM_Timer_Initialization(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TOMTIMERHANDLER_H_ */
/** @} */
