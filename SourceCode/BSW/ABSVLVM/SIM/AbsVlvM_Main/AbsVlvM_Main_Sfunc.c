#define S_FUNCTION_NAME      AbsVlvM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          273
#define WidthOutputPort         77

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "AbsVlvM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = input[0];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = input[1];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = input[2];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = input[3];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = input[4];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = input[5];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = input[6];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = input[7];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = input[8];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = input[9];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = input[10];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = input[11];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = input[12];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = input[13];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = input[14];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = input[15];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = input[16];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = input[17];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = input[18];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = input[19];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = input[20];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = input[21];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = input[22];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = input[23];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = input[24];
    AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = input[25];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV = input[26];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV = input[27];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV = input[28];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV = input[29];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = input[30];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON = input[31];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = input[32];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = input[33];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = input[34];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV = input[35];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = input[36];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT = input[37];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT = input[38];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE = input[39];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT = input[40];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD = input[41];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD = input[42];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS = input[43];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT = input[44];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC = input[45];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT = input[46];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT = input[47];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT = input[48];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON = input[49];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR = input[50];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT = input[51];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT = input[52];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE = input[53];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT = input[54];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD = input[55];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD = input[56];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS = input[57];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT = input[58];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC = input[59];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT = input[60];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT = input[61];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT = input[62];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON = input[63];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR = input[64];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT = input[65];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT = input[66];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE = input[67];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT = input[68];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD = input[69];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD = input[70];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS = input[71];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT = input[72];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC = input[73];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT = input[74];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT = input[75];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT = input[76];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON = input[77];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR = input[78];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT = input[79];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT = input[80];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE = input[81];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT = input[82];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD = input[83];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD = input[84];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS = input[85];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT = input[86];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC = input[87];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT = input[88];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT = input[89];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT = input[90];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON = input[91];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR = input[92];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT = input[93];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT = input[94];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE = input[95];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT = input[96];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD = input[97];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD = input[98];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS = input[99];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT = input[100];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC = input[101];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT = input[102];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT = input[103];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT = input[104];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON = input[105];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR = input[106];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT = input[107];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT = input[108];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE = input[109];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT = input[110];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD = input[111];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD = input[112];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS = input[113];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT = input[114];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC = input[115];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT = input[116];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT = input[117];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT = input[118];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON = input[119];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR = input[120];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT = input[121];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT = input[122];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE = input[123];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT = input[124];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD = input[125];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD = input[126];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS = input[127];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT = input[128];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC = input[129];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT = input[130];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT = input[131];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT = input[132];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON = input[133];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR = input[134];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT = input[135];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT = input[136];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE = input[137];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT = input[138];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD = input[139];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD = input[140];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS = input[141];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT = input[142];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC = input[143];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT = input[144];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT = input[145];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT = input[146];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON = input[147];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR = input[148];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT = input[149];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT = input[150];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE = input[151];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT = input[152];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD = input[153];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD = input[154];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS = input[155];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT = input[156];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC = input[157];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT = input[158];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT = input[159];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT = input[160];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON = input[161];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR = input[162];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT = input[163];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT = input[164];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE = input[165];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT = input[166];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD = input[167];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD = input[168];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS = input[169];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT = input[170];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC = input[171];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT = input[172];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT = input[173];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT = input[174];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON = input[175];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR = input[176];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT = input[177];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT = input[178];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE = input[179];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT = input[180];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_T_SD = input[181];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD = input[182];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS = input[183];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LVT = input[184];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC = input[185];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT = input[186];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT = input[187];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT = input[188];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON = input[189];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR = input[190];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT = input[191];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT = input[192];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE = input[193];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT = input[194];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_T_SD = input[195];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD = input[196];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS = input[197];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LVT = input[198];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC = input[199];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT = input[200];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT = input[201];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT = input[202];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON = input[203];
    AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR = input[204];
    AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv = input[205];
    AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsOff = input[206];
    AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[0] = input[207];
    AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[1] = input[208];
    AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[2] = input[209];
    AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[3] = input[210];
    AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[4] = input[211];
    AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[0] = input[212];
    AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[1] = input[213];
    AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[2] = input[214];
    AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[3] = input[215];
    AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[4] = input[216];
    AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[0] = input[217];
    AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[1] = input[218];
    AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[2] = input[219];
    AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[3] = input[220];
    AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[4] = input[221];
    AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[0] = input[222];
    AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[1] = input[223];
    AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[2] = input[224];
    AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[3] = input[225];
    AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[4] = input[226];
    AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[0] = input[227];
    AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[1] = input[228];
    AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[2] = input[229];
    AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[3] = input[230];
    AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[4] = input[231];
    AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[0] = input[232];
    AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[1] = input[233];
    AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[2] = input[234];
    AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[3] = input[235];
    AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[4] = input[236];
    AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[0] = input[237];
    AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[1] = input[238];
    AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[2] = input[239];
    AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[3] = input[240];
    AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[4] = input[241];
    AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[0] = input[242];
    AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[1] = input[243];
    AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[2] = input[244];
    AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[3] = input[245];
    AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[4] = input[246];
    AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[0] = input[247];
    AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[1] = input[248];
    AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[2] = input[249];
    AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[3] = input[250];
    AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[4] = input[251];
    AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[0] = input[252];
    AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[1] = input[253];
    AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[2] = input[254];
    AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[3] = input[255];
    AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[4] = input[256];
    AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[0] = input[257];
    AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[1] = input[258];
    AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[2] = input[259];
    AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[3] = input[260];
    AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[4] = input[261];
    AbsVlvM_MainAchAsicInvalid = input[262];
    AbsVlvM_MainEcuModeSts = input[263];
    AbsVlvM_MainIgnOnOffSts = input[264];
    AbsVlvM_MainIgnEdgeSts = input[265];
    AbsVlvM_MainVBatt1Mon = input[266];
    AbsVlvM_MainVBatt2Mon = input[267];
    AbsVlvM_MainDiagClrSrs = input[268];
    AbsVlvM_MainAsicEnDrDrv = input[269];
    AbsVlvM_MainArbVlvDriveState = input[270];
    AbsVlvM_MainFspAbsHMon = input[271];
    AbsVlvM_MainFspAbsMon = input[272];

    AbsVlvM_Main();


    output[0] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_flno_time;
    output[1] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_frno_time;
    output[2] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlno_time;
    output[3] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrno_time;
    output[4] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_bal_time;
    output[5] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_pressdump_time;
    output[6] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_resp_time;
    output[7] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_flnc_time;
    output[8] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_frnc_time;
    output[9] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlnc_time;
    output[10] = AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrnc_time;
    output[11] = AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsDrv;
    output[12] = AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsOff;
    output[13] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err;
    output[14] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err;
    output[15] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err;
    output[16] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err;
    output[17] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err;
    output[18] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err;
    output[19] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err;
    output[20] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err;
    output[21] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err;
    output[22] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err;
    output[23] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err;
    output[24] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err;
    output[25] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err;
    output[26] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err;
    output[27] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err;
    output[28] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err;
    output[29] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err;
    output[30] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err;
    output[31] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err;
    output[32] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err;
    output[33] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err;
    output[34] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err;
    output[35] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err;
    output[36] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err;
    output[37] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err;
    output[38] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err;
    output[39] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err;
    output[40] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err;
    output[41] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err;
    output[42] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err;
    output[43] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err;
    output[44] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err;
    output[45] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err;
    output[46] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err;
    output[47] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err;
    output[48] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err;
    output[49] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err;
    output[50] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err;
    output[51] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err;
    output[52] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err;
    output[53] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err;
    output[54] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err;
    output[55] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err;
    output[56] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err;
    output[57] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err;
    output[58] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err;
    output[59] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err;
    output[60] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err;
    output[61] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err;
    output[62] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err;
    output[63] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err;
    output[64] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err;
    output[65] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err;
    output[66] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err;
    output[67] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err;
    output[68] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err;
    output[69] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err;
    output[70] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err;
    output[71] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err;
    output[72] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err;
    output[73] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err;
    output[74] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err;
    output[75] = AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err;
    output[76] = AbsVlvM_MainFSEscVlvInitEndFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    AbsVlvM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
