# \file
#
# \brief AbsVlvM
#
# This file contains the implementation of the SWC
# module AbsVlvM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += AbsVlvM_src

AbsVlvM_src_FILES        += $(AbsVlvM_SRC_PATH)\AbsVlvM_Main.c
AbsVlvM_src_FILES        += $(AbsVlvM_IFA_PATH)\AbsVlvM_Main_Ifa.c
AbsVlvM_src_FILES        += $(AbsVlvM_CFG_PATH)\AbsVlvM_Cfg.c
AbsVlvM_src_FILES        += $(AbsVlvM_CAL_PATH)\AbsVlvM_Cal.c
AbsVlvM_src_FILES        += $(AbsVlvM_SRC_PATH)\AbsVlvM_Process.c

ifeq ($(ICE_COMPILE),true)
AbsVlvM_src_FILES        += $(AbsVlvM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	AbsVlvM_src_FILES        += $(AbsVlvM_UNITY_PATH)\unity.c
	AbsVlvM_src_FILES        += $(AbsVlvM_UNITY_PATH)\unity_fixture.c	
	AbsVlvM_src_FILES        += $(AbsVlvM_UT_PATH)\main.c
	AbsVlvM_src_FILES        += $(AbsVlvM_UT_PATH)\AbsVlvM_Main\AbsVlvM_Main_UtMain.c
endif