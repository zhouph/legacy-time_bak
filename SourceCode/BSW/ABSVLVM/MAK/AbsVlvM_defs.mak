# \file
#
# \brief AbsVlvM
#
# This file contains the implementation of the SWC
# module AbsVlvM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

AbsVlvM_CORE_PATH     := $(MANDO_BSW_ROOT)\AbsVlvM
AbsVlvM_CAL_PATH      := $(AbsVlvM_CORE_PATH)\CAL\$(AbsVlvM_VARIANT)
AbsVlvM_SRC_PATH      := $(AbsVlvM_CORE_PATH)\SRC
AbsVlvM_CFG_PATH      := $(AbsVlvM_CORE_PATH)\CFG\$(AbsVlvM_VARIANT)
AbsVlvM_HDR_PATH      := $(AbsVlvM_CORE_PATH)\HDR
AbsVlvM_IFA_PATH      := $(AbsVlvM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    AbsVlvM_CMN_PATH      := $(AbsVlvM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	AbsVlvM_UT_PATH		:= $(AbsVlvM_CORE_PATH)\UT
	AbsVlvM_UNITY_PATH	:= $(AbsVlvM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(AbsVlvM_UT_PATH)
	CC_INCLUDE_PATH		+= $(AbsVlvM_UNITY_PATH)
	AbsVlvM_Main_PATH 	:= AbsVlvM_UT_PATH\AbsVlvM_Main
endif
CC_INCLUDE_PATH    += $(AbsVlvM_CAL_PATH)
CC_INCLUDE_PATH    += $(AbsVlvM_SRC_PATH)
CC_INCLUDE_PATH    += $(AbsVlvM_CFG_PATH)
CC_INCLUDE_PATH    += $(AbsVlvM_HDR_PATH)
CC_INCLUDE_PATH    += $(AbsVlvM_IFA_PATH)
CC_INCLUDE_PATH    += $(AbsVlvM_CMN_PATH)

