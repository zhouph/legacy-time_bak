/**
 * @defgroup AbsVlvM_Main_Ifa AbsVlvM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AbsVlvM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ABSVLVM_MAIN_IFA_H_
#define ABSVLVM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbFsrAbsDrvInfo(data) do \
{ \
    *data = AbsVlvM_MainArbFsrAbsDrvInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo(data) do \
{ \
    *data = AbsVlvM_MainArbWhlVlvReqInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbNormVlvReqInfo(data) do \
{ \
    *data = AbsVlvM_MainArbNormVlvReqInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbBalVlvReqInfo(data) do \
{ \
    *data = AbsVlvM_MainArbBalVlvReqInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbResPVlvReqInfo(data) do \
{ \
    *data = AbsVlvM_MainArbResPVlvReqInfo; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(data) do \
{ \
    *data = AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_OV(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_UV(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_UV(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_OV(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_PMP_LD_ACT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_OFF(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_VHD_OV(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No0_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No1_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No2_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_No3_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc0_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc1_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc2_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Nc3_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc0_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Tc1_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv0_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_AVG_CURRENT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_PWM_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_CURR_SENSE(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_ADC_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_T_SD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_T_SD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_OPEN_LOAD(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_GND_LOSS(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_LVT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LVT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_LS_OVC(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_VGS_LS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_VGS_HS_FAULT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_HS_SHORT(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_LS_CLAMP_ON(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo_Ach_Asic_Esv1_UNDER_CURR(data) do \
{ \
    *data = AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    *data = AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    *data = AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsOff; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbNormVlvReqInfo_PressDumpVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbBalVlvReqInfo_BalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbResPVlvReqInfo_ResPVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[i]; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAchAsicInvalid(data) do \
{ \
    *data = AbsVlvM_MainAchAsicInvalid; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainEcuModeSts(data) do \
{ \
    *data = AbsVlvM_MainEcuModeSts; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainIgnOnOffSts(data) do \
{ \
    *data = AbsVlvM_MainIgnOnOffSts; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainIgnEdgeSts(data) do \
{ \
    *data = AbsVlvM_MainIgnEdgeSts; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainVBatt1Mon(data) do \
{ \
    *data = AbsVlvM_MainVBatt1Mon; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainVBatt2Mon(data) do \
{ \
    *data = AbsVlvM_MainVBatt2Mon; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainDiagClrSrs(data) do \
{ \
    *data = AbsVlvM_MainDiagClrSrs; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainAsicEnDrDrv(data) do \
{ \
    *data = AbsVlvM_MainAsicEnDrDrv; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainArbVlvDriveState(data) do \
{ \
    *data = AbsVlvM_MainArbVlvDriveState; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainFspAbsHMon(data) do \
{ \
    *data = AbsVlvM_MainFspAbsHMon; \
}while(0);

#define AbsVlvM_Main_Read_AbsVlvM_MainFspAbsMon(data) do \
{ \
    *data = AbsVlvM_MainFspAbsMon; \
}while(0);


/* Set Output DE MAcro Function */
#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSFsrAbsDrvInfo(data) do \
{ \
    AbsVlvM_MainFSFsrAbsDrvInfo = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData(data) do \
{ \
    AbsVlvM_MainAbsVlvMData = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_flno_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_flno_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_frno_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_frno_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_rlno_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlno_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_rrno_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrno_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_bal_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_bal_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_pressdump_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_pressdump_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_resp_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_resp_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_flnc_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_flnc_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_frnc_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_frnc_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_rlnc_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlnc_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo_fs_on_rrnc_time(data) do \
{ \
    AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrnc_time = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsDrv = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsOff = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_S2G_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_S2B_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_CP_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_Batt1FuseOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRelay_ShutdownLine_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNO_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNO_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNO_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNO_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNO_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNO_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNO_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNO_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNO_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNO_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNO_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNO_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNO_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNO_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNO_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNO_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNO_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNO_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNO_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNO_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveBAL_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveBAL_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveBAL_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveBAL_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveBAL_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValvePD_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValvePD_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValvePD_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValvePD_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValvePD_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRESP_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRESP_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRESP_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRESP_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRESP_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNC_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNC_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNC_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNC_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFLNC_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNC_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNC_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNC_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNC_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveFRNC_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNC_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNC_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNC_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNC_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRLNC_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNC_Open_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNC_PsvOpen_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNC_Short_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNC_OverTemp_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData_AbsVlvM_ValveRRNC_CurReg_Err(data) do \
{ \
    AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err = *data; \
}while(0);

#define AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvInitEndFlg(data) do \
{ \
    AbsVlvM_MainFSEscVlvInitEndFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ABSVLVM_MAIN_IFA_H_ */
/** @} */
