/**
 * @defgroup AbsVlvM_Main AbsVlvM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AbsVlvM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AbsVlvM_Main.h"
#include "AbsVlvM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ABSVLVM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ABSVLVM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABSVLVM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AbsVlvM_Main_HdrBusType AbsVlvM_MainBus;

/* Version Info */
const SwcVersionInfo_t AbsVlvM_MainVersionInfo = 
{   
    ABSVLVM_MAIN_MODULE_ID,           /* AbsVlvM_MainVersionInfo.ModuleId */
    ABSVLVM_MAIN_MAJOR_VERSION,       /* AbsVlvM_MainVersionInfo.MajorVer */
    ABSVLVM_MAIN_MINOR_VERSION,       /* AbsVlvM_MainVersionInfo.MinorVer */
    ABSVLVM_MAIN_PATCH_VERSION,       /* AbsVlvM_MainVersionInfo.PatchVer */
    ABSVLVM_MAIN_BRANCH_VERSION       /* AbsVlvM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchSysPwrAsicInfo_t AbsVlvM_MainAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t AbsVlvM_MainAchValveAsicInfo;
Arbitrator_VlvArbFsrAbsDrvInfo_t AbsVlvM_MainArbFsrAbsDrvInfo;
Arbitrator_VlvArbWhlVlvReqInfo_t AbsVlvM_MainArbWhlVlvReqInfo;
Arbitrator_VlvArbNormVlvReqInfo_t AbsVlvM_MainArbNormVlvReqInfo;
Arbitrator_VlvArbBalVlvReqInfo_t AbsVlvM_MainArbBalVlvReqInfo;
Arbitrator_VlvArbResPVlvReqInfo_t AbsVlvM_MainArbResPVlvReqInfo;
Ach_InputAchAsicInvalidInfo_t AbsVlvM_MainAchAsicInvalid;
Mom_HndlrEcuModeSts_t AbsVlvM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t AbsVlvM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t AbsVlvM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t AbsVlvM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t AbsVlvM_MainVBatt2Mon;
Diag_HndlrDiagClr_t AbsVlvM_MainDiagClrSrs;
Arbitrator_VlvArbAsicEnDrDrvInfo_t AbsVlvM_MainAsicEnDrDrv;
Arbitrator_VlvArbVlvDriveState_t AbsVlvM_MainArbVlvDriveState;
Ioc_InputSR1msFspAbsHMon_t AbsVlvM_MainFspAbsHMon;
Ioc_InputSR1msFspAbsMon_t AbsVlvM_MainFspAbsMon;

/* Output Data Element */
AbsVlvM_MainFSEscVlvActr_t AbsVlvM_MainFSEscVlvActrInfo;
AbsVlvM_MainFSFsrAbsDrvInfo_t AbsVlvM_MainFSFsrAbsDrvInfo;
AbsVlvM_MainAbsVlvMData_t AbsVlvM_MainAbsVlvMData;
AbsVlvM_MainFSEscVlvInitTest_t AbsVlvM_MainFSEscVlvInitEndFlg;

uint32 AbsVlvM_Main_Timer_Start;
uint32 AbsVlvM_Main_Timer_Elapsed;

#define ABSVLVM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_MAIN_START_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABSVLVM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_MAIN_START_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define ABSVLVM_MAIN_STOP_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ABSVLVM_MAIN_START_SEC_CODE
#include "AbsVlvM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AbsVlvM_Main_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_T_SD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LVT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR = 0;
    AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsOff = 0;
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) AbsVlvM_MainBus.AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData[i] = 0;   
    AbsVlvM_MainBus.AbsVlvM_MainAchAsicInvalid = 0;
    AbsVlvM_MainBus.AbsVlvM_MainEcuModeSts = 0;
    AbsVlvM_MainBus.AbsVlvM_MainIgnOnOffSts = 0;
    AbsVlvM_MainBus.AbsVlvM_MainIgnEdgeSts = 0;
    AbsVlvM_MainBus.AbsVlvM_MainVBatt1Mon = 0;
    AbsVlvM_MainBus.AbsVlvM_MainVBatt2Mon = 0;
    AbsVlvM_MainBus.AbsVlvM_MainDiagClrSrs = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAsicEnDrDrv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainArbVlvDriveState = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFspAbsHMon = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFspAbsMon = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_flno_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_frno_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlno_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrno_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_bal_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_pressdump_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_resp_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_flnc_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_frnc_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlnc_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrnc_time = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsDrv = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsOff = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err = 0;
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvInitEndFlg = 0;
}

void AbsVlvM_Main(void)
{
    uint16 i;
    
    AbsVlvM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    AbsVlvM_Main_Read_AbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);

    AbsVlvM_Main_Read_AbsVlvM_MainAchValveAsicInfo(&AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainAchValveAsicInfo 
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LVT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
     : AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
     =============================================================================*/
    
    AbsVlvM_Main_Read_AbsVlvM_MainArbFsrAbsDrvInfo(&AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbFsrAbsDrvInfo 
     : AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv;
     : AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/
    
    AbsVlvM_Main_Read_AbsVlvM_MainArbWhlVlvReqInfo(&AbsVlvM_MainBus.AbsVlvM_MainArbWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbWhlVlvReqInfo 
     : AbsVlvM_MainArbWhlVlvReqInfo.FlOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.FlIvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.FrOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.FrIvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RlOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RlIvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RrOvReqData;
     : AbsVlvM_MainArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/
    
    /* Decomposed structure interface */
    AbsVlvM_Main_Read_AbsVlvM_MainArbNormVlvReqInfo_PressDumpVlvReqData(&AbsVlvM_MainBus.AbsVlvM_MainArbNormVlvReqInfo.PressDumpVlvReqData);

    AbsVlvM_Main_Read_AbsVlvM_MainArbBalVlvReqInfo(&AbsVlvM_MainBus.AbsVlvM_MainArbBalVlvReqInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbBalVlvReqInfo 
     : AbsVlvM_MainArbBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/
    
    AbsVlvM_Main_Read_AbsVlvM_MainArbResPVlvReqInfo(&AbsVlvM_MainBus.AbsVlvM_MainArbResPVlvReqInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainArbResPVlvReqInfo 
     : AbsVlvM_MainArbResPVlvReqInfo.ResPVlvReqData;
     =============================================================================*/
    
    AbsVlvM_Main_Read_AbsVlvM_MainAchAsicInvalid(&AbsVlvM_MainBus.AbsVlvM_MainAchAsicInvalid);
    AbsVlvM_Main_Read_AbsVlvM_MainEcuModeSts(&AbsVlvM_MainBus.AbsVlvM_MainEcuModeSts);
    AbsVlvM_Main_Read_AbsVlvM_MainIgnOnOffSts(&AbsVlvM_MainBus.AbsVlvM_MainIgnOnOffSts);
    AbsVlvM_Main_Read_AbsVlvM_MainIgnEdgeSts(&AbsVlvM_MainBus.AbsVlvM_MainIgnEdgeSts);
    AbsVlvM_Main_Read_AbsVlvM_MainVBatt1Mon(&AbsVlvM_MainBus.AbsVlvM_MainVBatt1Mon);
    AbsVlvM_Main_Read_AbsVlvM_MainVBatt2Mon(&AbsVlvM_MainBus.AbsVlvM_MainVBatt2Mon);
    AbsVlvM_Main_Read_AbsVlvM_MainDiagClrSrs(&AbsVlvM_MainBus.AbsVlvM_MainDiagClrSrs);
    AbsVlvM_Main_Read_AbsVlvM_MainAsicEnDrDrv(&AbsVlvM_MainBus.AbsVlvM_MainAsicEnDrDrv);
    AbsVlvM_Main_Read_AbsVlvM_MainArbVlvDriveState(&AbsVlvM_MainBus.AbsVlvM_MainArbVlvDriveState);
    AbsVlvM_Main_Read_AbsVlvM_MainFspAbsHMon(&AbsVlvM_MainBus.AbsVlvM_MainFspAbsHMon);
    AbsVlvM_Main_Read_AbsVlvM_MainFspAbsMon(&AbsVlvM_MainBus.AbsVlvM_MainFspAbsMon);

    /* Process */
	AbsVlvM_ProcessMainFunction();

    /* Output */
    AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvActrInfo(&AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvActrInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainFSEscVlvActrInfo 
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_flno_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_frno_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlno_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrno_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_bal_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_pressdump_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_resp_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_flnc_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_frnc_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_rlnc_time;
     : AbsVlvM_MainFSEscVlvActrInfo.fs_on_rrnc_time;
     =============================================================================*/
    
    AbsVlvM_Main_Write_AbsVlvM_MainFSFsrAbsDrvInfo(&AbsVlvM_MainBus.AbsVlvM_MainFSFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure AbsVlvM_MainFSFsrAbsDrvInfo 
     : AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsDrv;
     : AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/
    
    AbsVlvM_Main_Write_AbsVlvM_MainAbsVlvMData(&AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData);
    /*==============================================================================
    * Members of structure AbsVlvM_MainAbsVlvMData 
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err;
     : AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err;
     =============================================================================*/
    
    AbsVlvM_Main_Write_AbsVlvM_MainFSEscVlvInitEndFlg(&AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvInitEndFlg);

    AbsVlvM_Main_Timer_Elapsed = STM0_TIM0.U - AbsVlvM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ABSVLVM_MAIN_STOP_SEC_CODE
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
