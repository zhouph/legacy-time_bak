/**
 * @defgroup AbsVlvM_Process AbsVlvM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AbsVlvM_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AbsVlvM_Process.h"
#include "AbsVlvM_Main.h"
#include "common.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
uint8 AbsVlvM_BATT1Mon;
uint8 AbsVlvM_FSRMon;
uint8 AbsVlvM_Cycletime = 5;
uint8 AbsVlvM_ActuatorCheckInhibitTime;
uint8 AbsVlvM_PreVRDrvReq;

uint8 AbsVlvM_FSR_Err_BATT1FuseOpen_Err;
uint8 AbsVlvM_FSR_Err_VlvRelayCP_Err;
uint8 AbsVlvM_FSR_Err_VlvRelayOpen_Err;
uint8 AbsVlvM_FSR_Err_VlvRelayS2B_Err;
uint8 AbsVlvM_FSR_Err_VlvRelayS2G_Err;
uint8 AbsVlvM_FSR_Err_VlvRelayOverTemp_Err;
uint8 AbsVlvM_FSR_Err_VlvRelayShutdown_Err;

uint8 AbsVlvM_FSR_Err_All_Err;

uint8 AbsVlvM_Solenid_Err_TC0_Bal_Open_Err;
uint8 AbsVlvM_Solenid_Err_TC1_PD_Open_Err;
uint8 AbsVlvM_Solenid_Err_NO0_RLNO_Open_Err;
uint8 AbsVlvM_Solenid_Err_NO1_RRNO_Open_Err;
uint8 AbsVlvM_Solenid_Err_NO2_FRNO_Open_Err;
uint8 AbsVlvM_Solenid_Err_NO3_FLNO_Open_Err;
uint8 AbsVlvM_Solenid_Err_NC0_FRNC_Open_Err;
uint8 AbsVlvM_Solenid_Err_NC1_RRNC_Open_Err;
uint8 AbsVlvM_Solenid_Err_NC2_FLNC_Open_Err;
uint8 AbsVlvM_Solenid_Err_NC3_RLNC_Open_Err;

uint8 AbsVlvM_Solenid_Err_TC0_Bal_Short_Err;
uint8 AbsVlvM_Solenid_Err_TC1_PD_Short_Err;
uint8 AbsVlvM_Solenid_Err_NO0_RLNO_Short_Err;
uint8 AbsVlvM_Solenid_Err_NO1_RRNO_Short_Err;
uint8 AbsVlvM_Solenid_Err_NO2_FRNO_Short_Err;
uint8 AbsVlvM_Solenid_Err_NO3_FLNO_Short_Err;
uint8 AbsVlvM_Solenid_Err_NC0_FRNC_Short_Err;
uint8 AbsVlvM_Solenid_Err_NC1_RRNC_Short_Err;
uint8 AbsVlvM_Solenid_Err_NC2_FLNC_Short_Err;
uint8 AbsVlvM_Solenid_Err_NC3_RLNC_Short_Err;

uint8 AbsVlvM_Solenid_Err_TC0_Bal_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_TC1_PD_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NO0_RLNO_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NO1_RRNO_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NO2_FRNO_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NO3_FLNO_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NC0_FRNC_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NC1_RRNC_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NC2_FLNC_OverTemp_Err;
uint8 AbsVlvM_Solenid_Err_NC3_RLNC_OverTemp_Err;

uint8 AbsVlvM_Solenid_Err_TC0_Bal_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_TC1_PD_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NO0_RLNO_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NO1_RRNO_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NO2_FRNO_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NO3_FLNO_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NC0_FRNC_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NC1_RRNC_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NC2_FLNC_CurReg_Err;
uint8 AbsVlvM_Solenid_Err_NC3_RLNC_CurReg_Err;

uint8 AbsVlvM_Solenid_Err_TC0_Bal_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_TC1_PD_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NO0_RLNO_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NO1_RRNO_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NO2_FRNO_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NO3_FLNO_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NC0_FRNC_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NC1_RRNC_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NC2_FLNC_PsvOpen_Err;
uint8 AbsVlvM_Solenid_Err_NC3_RLNC_PsvOpen_Err;

/*==============================================================================
 *                  GLOBAL FUNCTIONS PROTOTYPES
 =============================================================================*/
void AbsVlvM_ProcessMainFunction(void);

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
void AbsVlvM_PreCondition(void);

void AbsVlvM_VlvRelayHWCheck(void);
void AbsVlvM_VlvRelayBATT1FuseOpenCheck(uint8 inhibit);
void AbsVlvM_VlvRelayCPCheck(uint8 inhibit);
void AbsVlvM_VlvRelayOpenCheck(uint8 inhibit);
void AbsVlvM_VlvRelayShortBATTCheck(uint8 inhibit);
void AbsVlvM_VlvRelayOverTempCheck(uint8 inhibit);
void AbsVlvM_VlvRelayShutdownLineCheck(uint8 inhibit);
void AbsVlvM_VlvRelayAllErrCheck(void);

void AbsVlvM_VlvSolenoidHWCheck(void);
void AbsVlvM_VlvSolenoidOpenCheck(uint8 inhibit);
void AbsVlvM_VlvSolenoidShortCheck(uint8 inhibit);
void AbsVlvM_VlvSolenoidOverTempCheck(uint8 inhibit);
void AbsVlvM_VlvSolenoidCurRegCheck(uint8 inhibit);

void AbsVlvM_VlvErrIhbCopy(void);
/*==============================================================================
 *                  GLOBAL/LOCAL FUNCTIONS 
 =============================================================================*/
void AbsVlvM_ProcessMainFunction(void)
{
	AbsVlvM_PreCondition();
	AbsVlvM_VlvRelayHWCheck();
	AbsVlvM_VlvSolenoidHWCheck();
	AbsVlvM_VlvErrIhbCopy();

    /* Temp Valve, FSR On */
    AbsVlvM_MainBus.AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsDrv = STD_ON;
    AbsVlvM_MainBus.AbsVlvM_MainFSFsrAbsDrvInfo.FsrAbsOff = STD_OFF;

    /* Temp Init End Flg On */
    AbsVlvM_MainBus.AbsVlvM_MainFSEscVlvInitEndFlg = TRUE;
}

void AbsVlvM_PreCondition(void)
{	
	if(AbsVlvM_MainBus.AbsVlvM_MainVBatt1Mon >= ABSVLVM_BATT1_ON_DET_VOLT)
	{
		AbsVlvM_BATT1Mon = 1;
	}	
	else if(AbsVlvM_MainBus.AbsVlvM_MainVBatt1Mon < ABSVLVM_BATT1_OFF_DET_VOLT)
	{
		AbsVlvM_BATT1Mon = 0;
	}	
	else
	{
		;
	}

	if(AbsVlvM_MainBus.AbsVlvM_MainFspAbsMon >= ABSVLVM_FSR_ON_DET_VOLT)
	{
		AbsVlvM_FSRMon = 1;
	}
	else if(AbsVlvM_MainBus.AbsVlvM_MainFspAbsMon <= ABSVLVM_FSR_OFF_DET_VOLT)
	{
		AbsVlvM_FSRMon = 0;
	}	
	else
	{
		;
	}

	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT  == 1)
	{
        if(AbsVlvM_Cycletime > 0)
        {
           AbsVlvM_ActuatorCheckInhibitTime = (ABSVLVM_ACT_CHECK_IHB_TIME) / (AbsVlvM_Cycletime);
        }
    }
    else if(AbsVlvM_ActuatorCheckInhibitTime > 0)
    {
        AbsVlvM_ActuatorCheckInhibitTime--;
    }
    else
    {
        ;
    }		
}

void AbsVlvM_VlvRelayHWCheck(void)
{
	uint8 inhibit = 0;
	
	if(	(AbsVlvM_MainBus.AbsVlvM_MainVBatt1Mon > U16_CALCVOLT_17V) || 
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchAsicInvalid == 1) ||
	   	(AbsVlvM_BATT1Mon == 0) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv != AbsVlvM_PreVRDrvReq) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsOff == 1) ||
	   	(AbsVlvM_ActuatorCheckInhibitTime > 0) )
	{
		inhibit = 1;
	}
	
	AbsVlvM_VlvRelayBATT1FuseOpenCheck(inhibit);
	AbsVlvM_VlvRelayCPCheck(inhibit);
	AbsVlvM_VlvRelayOpenCheck(inhibit);
	AbsVlvM_VlvRelayShortBATTCheck(inhibit);
	AbsVlvM_VlvRelayOverTempCheck(inhibit);

	AbsVlvM_VlvRelayAllErrCheck();

	AbsVlvM_PreVRDrvReq = AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv;
}

void AbsVlvM_VlvRelayBATT1FuseOpenCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;
	
	AbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_NONE;

	if(AbsVlvM_MainBus.AbsVlvM_MainVBatt2Mon > U16_CALCVOLT_7V)
	{
		if(AbsVlvM_BATT1Mon == 0)
		{
			AbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_PREFAILED;
		}	
		else
		{
			AbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_PREPASSED;
		}	
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_INHIBIT;
	}	
}

void AbsVlvM_VlvRelayCPCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV == 1) )
	{
		AbsVlvM_FSR_Err_VlvRelayCP_Err = ERR_PREFAILED;
	}	  
	else
	{
		AbsVlvM_FSR_Err_VlvRelayCP_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		AbsVlvM_FSR_Err_VlvRelayCP_Err = ERR_INHIBIT;
	}	
}

void AbsVlvM_VlvRelayOpenCheck(uint8 inhibit)
{
	uint8 HsdShort = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

    if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT == 1) ||
        (AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV == 1) )
    {
		HsdShort = 1;
	}    

	if( (AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv == 1) &&
		(AbsVlvM_MainBus.AbsVlvM_MainArbFsrAbsDrvInfo.FsrAbsDrv == AbsVlvM_PreVRDrvReq) &&
		(AbsVlvM_FSRMon == 0) &&
		(HsdShort == 0) )
	{
		AbsVlvM_FSR_Err_VlvRelayOpen_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_FSR_Err_VlvRelayOpen_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		AbsVlvM_FSR_Err_VlvRelayOpen_Err = ERR_INHIBIT;
	}	
}

void AbsVlvM_VlvRelayShortBATTCheck(uint8 inhibit)
{
	uint8 HsdShort = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

    if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT == 1) ||
        (AbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV == 1) )
    {
		HsdShort = 1;
	}    

	if(HsdShort == 1)
    {
        AbsVlvM_FSR_Err_VlvRelayS2B_Err = ERR_PREFAILED;
    }
    else
    {
        AbsVlvM_FSR_Err_VlvRelayS2B_Err = ERR_PREPASSED;
    }

	if(temp_inbibit == 1)
	{
		AbsVlvM_FSR_Err_VlvRelayS2B_Err = ERR_INHIBIT;
	}	
}

void AbsVlvM_VlvRelayOverTempCheck(uint8 inhibit)
{
	uint8 HsdOverTemp = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT == 1)
	{
		HsdOverTemp = 1;
	}	

	if(HsdOverTemp == 1)
	{
		AbsVlvM_FSR_Err_VlvRelayOverTemp_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_FSR_Err_VlvRelayOverTemp_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		AbsVlvM_FSR_Err_VlvRelayOverTemp_Err = ERR_INHIBIT;
	}	
}

void AbsVlvM_VlvRelayShutdownLineCheck(uint8 inhibit)
{
	uint8 HsdShort = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;
}

void AbsVlvM_VlvRelayAllErrCheck(void)
{
	if( (AbsVlvM_FSR_Err_BATT1FuseOpen_Err == ERR_PREFAILED) ||
		(AbsVlvM_FSR_Err_VlvRelayCP_Err == ERR_PREFAILED) ||
		(AbsVlvM_FSR_Err_VlvRelayOpen_Err == ERR_PREFAILED) ||
		(AbsVlvM_FSR_Err_VlvRelayS2B_Err == ERR_PREFAILED) ||
		(AbsVlvM_FSR_Err_VlvRelayOverTemp_Err == ERR_PREFAILED) ||
		(AbsVlvM_FSR_Err_VlvRelayShutdown_Err == ERR_PREFAILED) )
	{
		AbsVlvM_FSR_Err_All_Err = 1;
	}
	else
	{
		AbsVlvM_FSR_Err_All_Err = 0;		
	}	
}

void AbsVlvM_VlvSolenoidHWCheck(void)
{
    uint8 inhibit = 0;

    if( (AbsVlvM_FSRMon == 0) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV == 1) ||
	   	(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV == 1) ||
        (AbsVlvM_BATT1Mon == 0) ||
        (AbsVlvM_FSR_Err_All_Err == 1) ||
        (AbsVlvM_ActuatorCheckInhibitTime > 0) )
    {
        inhibit = 1;
    }	

	AbsVlvM_VlvSolenoidOpenCheck(inhibit);
	AbsVlvM_VlvSolenoidShortCheck(inhibit);
	AbsVlvM_VlvSolenoidOverTempCheck(inhibit);
	AbsVlvM_VlvSolenoidCurRegCheck(inhibit);
}

void AbsVlvM_VlvSolenoidOpenCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	/* 100 Asic HW Pin Tc1 - SW Name Tc0 - Bal */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_TC0_Bal_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_TC0_Bal_Open_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_TC0_Bal_Open_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Tc2 - SW Name Tc1 - PD */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_TC1_PD_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_TC1_PD_Open_Err = ERR_PREPASSED;
	}

	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_TC1_PD_Open_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No1 - SW Name No0 - RLNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_Open_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_Open_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No2 - SW Name No1 - RRNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_Open_Err = ERR_INHIBIT;
	}		

	/* 100 Asic HW Pin No3 - SW Name No2 - FRNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_Open_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No4 - SW Name No3 - FLNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD == 1) ||
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_Open_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc1 - SW Name Nc0 - FRNC */
	if(
		#ifndef ABSVLV_ASIC_ERRATA
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD == 1) ||
		#endif
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_Open_Err = ERR_INHIBIT;
	}			

	/* 100 Asic HW Pin Nc2 - SW Name Nc1 - RRNC */
	if( 
		#ifndef ABSVLV_ASIC_ERRATA
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD == 1) ||
		#endif
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_Open_Err = ERR_INHIBIT;
	}			

	/* 100 Asic HW Pin Nc3 - SW Name Nc2 - FLNC */
	if( 
		#ifndef ABSVLV_ASIC_ERRATA
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD == 1) ||
		#endif
        (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_Open_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc4 - SW Name Nc3 - RLNC */
	if( 
		#ifndef ABSVLV_ASIC_ERRATA
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD == 1) ||
		#endif
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LVT == 1) )
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_Open_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_Open_Err = ERR_INHIBIT;
	}		
}	

void AbsVlvM_VlvSolenoidShortCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;
	
	/* 100 Asic HW Pin Tc1 - SW Name Tc0 - Bal */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_TC0_Bal_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_TC0_Bal_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_TC0_Bal_Short_Err = ERR_INHIBIT;
	}	
	
	/* 100 Asic HW Pin Tc2 - SW Name Tc1 - PD */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_TC1_PD_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_TC1_PD_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_TC1_PD_Short_Err = ERR_INHIBIT;
	}		

	/* 100 Asic HW Pin No1 - SW Name No0 - RLNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_Short_Err = ERR_INHIBIT;
	}		

	/* 100 Asic HW Pin No2 - SW Name No1 - RRNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_Short_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_Short_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No3 - SW Name No2 - FRNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_Short_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No4 - SW Name No3 - FLNO */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_Short_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc1 - SW Name Nc0 - FRNC */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_Short_Err = ERR_PREPASSED;
	}
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_Short_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc2 - SW Name Nc1 - RRNC */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_Short_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc3 - SW Name Nc2 - FLNC */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_Short_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc4 - SW Name Nc3 - RLNC */
	if( (AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT == 1) ||
		(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON == 1) )
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_Short_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_Short_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_Short_Err = ERR_INHIBIT;
	}		
}


void AbsVlvM_VlvSolenoidOverTempCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	/* 100 Asic HW Pin Tc1 - SW Name Tc0 - Bal */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc0_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_TC0_Bal_OverTemp_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_TC0_Bal_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_TC0_Bal_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Tc2 - SW Name Tc1 - PD */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Tc1_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_TC1_PD_OverTemp_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_TC1_PD_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_TC1_PD_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No1 - SW Name No0 - RLNO */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No0_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO0_RLNO_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No2 - SW Name No1 - RRNO */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No1_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO1_RRNO_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No3 - SW Name No2 - FRNO */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No2_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_OverTemp_Err = ERR_PREFAILED;
	}	
	else
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO2_FRNO_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin No4 - SW Name No3 - FLNO */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_No3_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NO3_FLNO_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc1 - SW Name Nc0 - FRNC */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc0_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC0_FRNC_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc2 - SW Name Nc1 - RRNC */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc1_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC1_RRNC_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc3 - SW Name Nc2 - FLNC */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc2_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC2_FLNC_OverTemp_Err = ERR_INHIBIT;
	}	

	/* 100 Asic HW Pin Nc4 - SW Name Nc3 - RLNC */
	if(AbsVlvM_MainBus.AbsVlvM_MainAchValveAsicInfo.Ach_Asic_Nc3_T_SD == 1)
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_OverTemp_Err = ERR_PREFAILED;
	}
	else
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_OverTemp_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		AbsVlvM_Solenid_Err_NC3_RLNC_OverTemp_Err = ERR_INHIBIT;
	}	
}

void AbsVlvM_VlvSolenoidCurRegCheck(uint8 inhibit)
{	
	/* 100 Asic HW Pin Tc1 - SW Name Tc0 - Bal */


	/* 100 Asic HW Pin Tc2 - SW Name Tc1 - PD */


	/* 100 Asic HW Pin No1 - SW Name No0 - RLNO */


	/* 100 Asic HW Pin No2 - SW Name No1 - RRNO */


	/* 100 Asic HW Pin No3 - SW Name No2 - FRNO */


	/* 100 Asic HW Pin No4 - SW Name No3 - FLNO */


	/* 100 Asic HW Pin Nc1 - SW Name Nc0 - FRNC */


	/* 100 Asic HW Pin Nc2 - SW Name Nc1 - RRNC */


	/* 100 Asic HW Pin Nc3 - SW Name Nc2 - FLNC */


	/* 100 Asic HW Pin Nc4 - SW Name Nc3 - RLNC */
}	

void AbsVlvM_VlvErrIhbCopy(void)
{
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err= AbsVlvM_FSR_Err_BATT1FuseOpen_Err; 
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err 	= AbsVlvM_FSR_Err_VlvRelayOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err 		= AbsVlvM_FSR_Err_VlvRelayS2G_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err 		= AbsVlvM_FSR_Err_VlvRelayS2B_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err = AbsVlvM_FSR_Err_VlvRelayOverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err = AbsVlvM_FSR_Err_VlvRelayShutdown_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err 			= AbsVlvM_FSR_Err_VlvRelayCP_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err 		= AbsVlvM_Solenid_Err_NO0_RLNO_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NO0_RLNO_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err 	= AbsVlvM_Solenid_Err_NO0_RLNO_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err 	= AbsVlvM_Solenid_Err_NO0_RLNO_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err 	= AbsVlvM_Solenid_Err_NO0_RLNO_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err 		= AbsVlvM_Solenid_Err_NO1_RRNO_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NO1_RRNO_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err 	= AbsVlvM_Solenid_Err_NO1_RRNO_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err 	= AbsVlvM_Solenid_Err_NO1_RRNO_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err 	= AbsVlvM_Solenid_Err_NO1_RRNO_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err 		= AbsVlvM_Solenid_Err_NO2_FRNO_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NO2_FRNO_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err 	= AbsVlvM_Solenid_Err_NO2_FRNO_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err 	= AbsVlvM_Solenid_Err_NO2_FRNO_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err 	= AbsVlvM_Solenid_Err_NO2_FRNO_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err 		= AbsVlvM_Solenid_Err_NO3_FLNO_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NO3_FLNO_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err 	= AbsVlvM_Solenid_Err_NO3_FLNO_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err 	= AbsVlvM_Solenid_Err_NO3_FLNO_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err 	= AbsVlvM_Solenid_Err_NO3_FLNO_CurReg_Err;

	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err 		= AbsVlvM_Solenid_Err_TC0_Bal_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err 	= AbsVlvM_Solenid_Err_TC0_Bal_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err 		= AbsVlvM_Solenid_Err_TC0_Bal_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err	= AbsVlvM_Solenid_Err_TC0_Bal_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err 	= AbsVlvM_Solenid_Err_TC0_Bal_CurReg_Err;

	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err 		= AbsVlvM_Solenid_Err_TC1_PD_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err 	= AbsVlvM_Solenid_Err_TC1_PD_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err 		= AbsVlvM_Solenid_Err_TC1_PD_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err	= AbsVlvM_Solenid_Err_TC1_PD_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err 		= AbsVlvM_Solenid_Err_TC1_PD_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err 		= AbsVlvM_Solenid_Err_NC3_RLNC_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NC3_RLNC_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err 	= AbsVlvM_Solenid_Err_NC3_RLNC_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err 	= AbsVlvM_Solenid_Err_NC3_RLNC_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err 	= AbsVlvM_Solenid_Err_NC3_RLNC_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err 		= AbsVlvM_Solenid_Err_NC0_FRNC_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NC0_FRNC_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err 	= AbsVlvM_Solenid_Err_NC0_FRNC_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err 	= AbsVlvM_Solenid_Err_NC0_FRNC_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err 	= AbsVlvM_Solenid_Err_NC0_FRNC_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err 		= AbsVlvM_Solenid_Err_NC1_RRNC_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NC1_RRNC_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err 	= AbsVlvM_Solenid_Err_NC1_RRNC_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err 	= AbsVlvM_Solenid_Err_NC1_RRNC_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err 	= AbsVlvM_Solenid_Err_NC1_RRNC_CurReg_Err;
	
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err 		= AbsVlvM_Solenid_Err_NC2_FLNC_Open_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err 	= AbsVlvM_Solenid_Err_NC2_FLNC_PsvOpen_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err 	= AbsVlvM_Solenid_Err_NC2_FLNC_Short_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err 	= AbsVlvM_Solenid_Err_NC2_FLNC_OverTemp_Err;
	AbsVlvM_MainBus.AbsVlvM_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err 	= AbsVlvM_Solenid_Err_NC2_FLNC_CurReg_Err;
}

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

