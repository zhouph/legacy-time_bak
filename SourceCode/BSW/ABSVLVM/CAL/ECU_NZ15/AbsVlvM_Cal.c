/**
 * @defgroup AbsVlvM_Cal AbsVlvM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AbsVlvM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AbsVlvM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ABSVLVM_START_SEC_CALIB_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/* Global Calibration Section */


#define ABSVLVM_STOP_SEC_CALIB_UNSPECIFIED
#include "AbsVlvM_MemMap.h"

#define ABSVLVM_START_SEC_CONST_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ABSVLVM_STOP_SEC_CONST_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABSVLVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABSVLVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_START_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABSVLVM_STOP_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_START_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABSVLVM_STOP_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_START_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define ABSVLVM_STOP_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABSVLVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABSVLVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_START_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABSVLVM_STOP_SEC_VAR_NOINIT_32BIT
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_START_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABSVLVM_STOP_SEC_VAR_UNSPECIFIED
#include "AbsVlvM_MemMap.h"
#define ABSVLVM_START_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define ABSVLVM_STOP_SEC_VAR_32BIT
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ABSVLVM_START_SEC_CODE
#include "AbsVlvM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ABSVLVM_STOP_SEC_CODE
#include "AbsVlvM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
