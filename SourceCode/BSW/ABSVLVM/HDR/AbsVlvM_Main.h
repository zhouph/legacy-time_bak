/**
 * @defgroup AbsVlvM_Main AbsVlvM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AbsVlvM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ABSVLVM_MAIN_H_
#define ABSVLVM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AbsVlvM_Types.h"
#include "AbsVlvM_Cfg.h"
#include "AbsVlvM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ABSVLVM_MAIN_MODULE_ID      (0)
 #define ABSVLVM_MAIN_MAJOR_VERSION  (2)
 #define ABSVLVM_MAIN_MINOR_VERSION  (0)
 #define ABSVLVM_MAIN_PATCH_VERSION  (0)
 #define ABSVLVM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AbsVlvM_Main_HdrBusType AbsVlvM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t AbsVlvM_MainVersionInfo;

/* Input Data Element */
extern Ach_InputAchSysPwrAsicInfo_t AbsVlvM_MainAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t AbsVlvM_MainAchValveAsicInfo;
extern Arbitrator_VlvArbFsrAbsDrvInfo_t AbsVlvM_MainArbFsrAbsDrvInfo;
extern Arbitrator_VlvArbWhlVlvReqInfo_t AbsVlvM_MainArbWhlVlvReqInfo;
extern Arbitrator_VlvArbNormVlvReqInfo_t AbsVlvM_MainArbNormVlvReqInfo;
extern Arbitrator_VlvArbBalVlvReqInfo_t AbsVlvM_MainArbBalVlvReqInfo;
extern Arbitrator_VlvArbResPVlvReqInfo_t AbsVlvM_MainArbResPVlvReqInfo;
extern Ach_InputAchAsicInvalidInfo_t AbsVlvM_MainAchAsicInvalid;
extern Mom_HndlrEcuModeSts_t AbsVlvM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t AbsVlvM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t AbsVlvM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t AbsVlvM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t AbsVlvM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t AbsVlvM_MainDiagClrSrs;
extern Arbitrator_VlvArbAsicEnDrDrvInfo_t AbsVlvM_MainAsicEnDrDrv;
extern Arbitrator_VlvArbVlvDriveState_t AbsVlvM_MainArbVlvDriveState;
extern Ioc_InputSR1msFspAbsHMon_t AbsVlvM_MainFspAbsHMon;
extern Ioc_InputSR1msFspAbsMon_t AbsVlvM_MainFspAbsMon;

/* Output Data Element */
extern AbsVlvM_MainFSEscVlvActr_t AbsVlvM_MainFSEscVlvActrInfo;
extern AbsVlvM_MainFSFsrAbsDrvInfo_t AbsVlvM_MainFSFsrAbsDrvInfo;
extern AbsVlvM_MainAbsVlvMData_t AbsVlvM_MainAbsVlvMData;
extern AbsVlvM_MainFSEscVlvInitTest_t AbsVlvM_MainFSEscVlvInitEndFlg;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AbsVlvM_Main_Init(void);
extern void AbsVlvM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ABSVLVM_MAIN_H_ */
/** @} */
