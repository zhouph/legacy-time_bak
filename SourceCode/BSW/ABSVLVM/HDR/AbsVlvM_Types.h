/**
 * @defgroup AbsVlvM_Types AbsVlvM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AbsVlvM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ABSVLVM_TYPES_H_
#define ABSVLVM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ach_InputAchSysPwrAsicInfo_t AbsVlvM_MainAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t AbsVlvM_MainAchValveAsicInfo;
    Arbitrator_VlvArbFsrAbsDrvInfo_t AbsVlvM_MainArbFsrAbsDrvInfo;
    Arbitrator_VlvArbWhlVlvReqInfo_t AbsVlvM_MainArbWhlVlvReqInfo;
    Arbitrator_VlvArbNormVlvReqInfo_t AbsVlvM_MainArbNormVlvReqInfo;
    Arbitrator_VlvArbBalVlvReqInfo_t AbsVlvM_MainArbBalVlvReqInfo;
    Arbitrator_VlvArbResPVlvReqInfo_t AbsVlvM_MainArbResPVlvReqInfo;
    Ach_InputAchAsicInvalidInfo_t AbsVlvM_MainAchAsicInvalid;
    Mom_HndlrEcuModeSts_t AbsVlvM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AbsVlvM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AbsVlvM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AbsVlvM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t AbsVlvM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t AbsVlvM_MainDiagClrSrs;
    Arbitrator_VlvArbAsicEnDrDrvInfo_t AbsVlvM_MainAsicEnDrDrv;
    Arbitrator_VlvArbVlvDriveState_t AbsVlvM_MainArbVlvDriveState;
    Ioc_InputSR1msFspAbsHMon_t AbsVlvM_MainFspAbsHMon;
    Ioc_InputSR1msFspAbsMon_t AbsVlvM_MainFspAbsMon;

/* Output Data Element */
    AbsVlvM_MainFSEscVlvActr_t AbsVlvM_MainFSEscVlvActrInfo;
    AbsVlvM_MainFSFsrAbsDrvInfo_t AbsVlvM_MainFSFsrAbsDrvInfo;
    AbsVlvM_MainAbsVlvMData_t AbsVlvM_MainAbsVlvMData;
    AbsVlvM_MainFSEscVlvInitTest_t AbsVlvM_MainFSEscVlvInitEndFlg;
}AbsVlvM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ABSVLVM_TYPES_H_ */
/** @} */
