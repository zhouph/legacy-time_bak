AbsVlvM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    };
AbsVlvM_MainAchSysPwrAsicInfo = CreateBus(AbsVlvM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

AbsVlvM_MainAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_VPWR_UV'
    'Ach_Asic_VPWR_OV'
    'Ach_Asic_PMP_LD_ACT'
    'Ach_Asic_FS_TURN_ON'
    'Ach_Asic_FS_TURN_OFF'
    'Ach_Asic_FS_VDS_FAULT'
    'Ach_Asic_FS_RVP_FAULT'
    'Ach_Asic_VHD_OV'
    'Ach_Asic_T_SD_INT'
    'Ach_Asic_No0_AVG_CURRENT'
    'Ach_Asic_No0_PWM_FAULT'
    'Ach_Asic_No0_CURR_SENSE'
    'Ach_Asic_No0_ADC_FAULT'
    'Ach_Asic_No0_T_SD'
    'Ach_Asic_No0_OPEN_LOAD'
    'Ach_Asic_No0_GND_LOSS'
    'Ach_Asic_No0_LVT'
    'Ach_Asic_No0_LS_OVC'
    'Ach_Asic_No0_VGS_LS_FAULT'
    'Ach_Asic_No0_VGS_HS_FAULT'
    'Ach_Asic_No0_HS_SHORT'
    'Ach_Asic_No0_LS_CLAMP_ON'
    'Ach_Asic_No0_UNDER_CURR'
    'Ach_Asic_No1_AVG_CURRENT'
    'Ach_Asic_No1_PWM_FAULT'
    'Ach_Asic_No1_CURR_SENSE'
    'Ach_Asic_No1_ADC_FAULT'
    'Ach_Asic_No1_T_SD'
    'Ach_Asic_No1_OPEN_LOAD'
    'Ach_Asic_No1_GND_LOSS'
    'Ach_Asic_No1_LVT'
    'Ach_Asic_No1_LS_OVC'
    'Ach_Asic_No1_VGS_LS_FAULT'
    'Ach_Asic_No1_VGS_HS_FAULT'
    'Ach_Asic_No1_HS_SHORT'
    'Ach_Asic_No1_LS_CLAMP_ON'
    'Ach_Asic_No1_UNDER_CURR'
    'Ach_Asic_No2_AVG_CURRENT'
    'Ach_Asic_No2_PWM_FAULT'
    'Ach_Asic_No2_CURR_SENSE'
    'Ach_Asic_No2_ADC_FAULT'
    'Ach_Asic_No2_T_SD'
    'Ach_Asic_No2_OPEN_LOAD'
    'Ach_Asic_No2_GND_LOSS'
    'Ach_Asic_No2_LVT'
    'Ach_Asic_No2_LS_OVC'
    'Ach_Asic_No2_VGS_LS_FAULT'
    'Ach_Asic_No2_VGS_HS_FAULT'
    'Ach_Asic_No2_HS_SHORT'
    'Ach_Asic_No2_LS_CLAMP_ON'
    'Ach_Asic_No2_UNDER_CURR'
    'Ach_Asic_No3_AVG_CURRENT'
    'Ach_Asic_No3_PWM_FAULT'
    'Ach_Asic_No3_CURR_SENSE'
    'Ach_Asic_No3_ADC_FAULT'
    'Ach_Asic_No3_T_SD'
    'Ach_Asic_No3_OPEN_LOAD'
    'Ach_Asic_No3_GND_LOSS'
    'Ach_Asic_No3_LVT'
    'Ach_Asic_No3_LS_OVC'
    'Ach_Asic_No3_VGS_LS_FAULT'
    'Ach_Asic_No3_VGS_HS_FAULT'
    'Ach_Asic_No3_HS_SHORT'
    'Ach_Asic_No3_LS_CLAMP_ON'
    'Ach_Asic_No3_UNDER_CURR'
    'Ach_Asic_Nc0_AVG_CURRENT'
    'Ach_Asic_Nc0_PWM_FAULT'
    'Ach_Asic_Nc0_CURR_SENSE'
    'Ach_Asic_Nc0_ADC_FAULT'
    'Ach_Asic_Nc0_T_SD'
    'Ach_Asic_Nc0_OPEN_LOAD'
    'Ach_Asic_Nc0_GND_LOSS'
    'Ach_Asic_Nc0_LVT'
    'Ach_Asic_Nc0_LS_OVC'
    'Ach_Asic_Nc0_VGS_LS_FAULT'
    'Ach_Asic_Nc0_VGS_HS_FAULT'
    'Ach_Asic_Nc0_HS_SHORT'
    'Ach_Asic_Nc0_LS_CLAMP_ON'
    'Ach_Asic_Nc0_UNDER_CURR'
    'Ach_Asic_Nc1_AVG_CURRENT'
    'Ach_Asic_Nc1_PWM_FAULT'
    'Ach_Asic_Nc1_CURR_SENSE'
    'Ach_Asic_Nc1_ADC_FAULT'
    'Ach_Asic_Nc1_T_SD'
    'Ach_Asic_Nc1_OPEN_LOAD'
    'Ach_Asic_Nc1_GND_LOSS'
    'Ach_Asic_Nc1_LVT'
    'Ach_Asic_Nc1_LS_OVC'
    'Ach_Asic_Nc1_VGS_LS_FAULT'
    'Ach_Asic_Nc1_VGS_HS_FAULT'
    'Ach_Asic_Nc1_HS_SHORT'
    'Ach_Asic_Nc1_LS_CLAMP_ON'
    'Ach_Asic_Nc1_UNDER_CURR'
    'Ach_Asic_Nc2_AVG_CURRENT'
    'Ach_Asic_Nc2_PWM_FAULT'
    'Ach_Asic_Nc2_CURR_SENSE'
    'Ach_Asic_Nc2_ADC_FAULT'
    'Ach_Asic_Nc2_T_SD'
    'Ach_Asic_Nc2_OPEN_LOAD'
    'Ach_Asic_Nc2_GND_LOSS'
    'Ach_Asic_Nc2_LVT'
    'Ach_Asic_Nc2_LS_OVC'
    'Ach_Asic_Nc2_VGS_LS_FAULT'
    'Ach_Asic_Nc2_VGS_HS_FAULT'
    'Ach_Asic_Nc2_HS_SHORT'
    'Ach_Asic_Nc2_LS_CLAMP_ON'
    'Ach_Asic_Nc2_UNDER_CURR'
    'Ach_Asic_Nc3_AVG_CURRENT'
    'Ach_Asic_Nc3_PWM_FAULT'
    'Ach_Asic_Nc3_CURR_SENSE'
    'Ach_Asic_Nc3_ADC_FAULT'
    'Ach_Asic_Nc3_T_SD'
    'Ach_Asic_Nc3_OPEN_LOAD'
    'Ach_Asic_Nc3_GND_LOSS'
    'Ach_Asic_Nc3_LVT'
    'Ach_Asic_Nc3_LS_OVC'
    'Ach_Asic_Nc3_VGS_LS_FAULT'
    'Ach_Asic_Nc3_VGS_HS_FAULT'
    'Ach_Asic_Nc3_HS_SHORT'
    'Ach_Asic_Nc3_LS_CLAMP_ON'
    'Ach_Asic_Nc3_UNDER_CURR'
    'Ach_Asic_Tc0_AVG_CURRENT'
    'Ach_Asic_Tc0_PWM_FAULT'
    'Ach_Asic_Tc0_CURR_SENSE'
    'Ach_Asic_Tc0_ADC_FAULT'
    'Ach_Asic_Tc0_T_SD'
    'Ach_Asic_Tc0_OPEN_LOAD'
    'Ach_Asic_Tc0_GND_LOSS'
    'Ach_Asic_Tc0_LVT'
    'Ach_Asic_Tc0_LS_OVC'
    'Ach_Asic_Tc0_VGS_LS_FAULT'
    'Ach_Asic_Tc0_VGS_HS_FAULT'
    'Ach_Asic_Tc0_HS_SHORT'
    'Ach_Asic_Tc0_LS_CLAMP_ON'
    'Ach_Asic_Tc0_UNDER_CURR'
    'Ach_Asic_Tc1_AVG_CURRENT'
    'Ach_Asic_Tc1_PWM_FAULT'
    'Ach_Asic_Tc1_CURR_SENSE'
    'Ach_Asic_Tc1_ADC_FAULT'
    'Ach_Asic_Tc1_T_SD'
    'Ach_Asic_Tc1_OPEN_LOAD'
    'Ach_Asic_Tc1_GND_LOSS'
    'Ach_Asic_Tc1_LVT'
    'Ach_Asic_Tc1_LS_OVC'
    'Ach_Asic_Tc1_VGS_LS_FAULT'
    'Ach_Asic_Tc1_VGS_HS_FAULT'
    'Ach_Asic_Tc1_HS_SHORT'
    'Ach_Asic_Tc1_LS_CLAMP_ON'
    'Ach_Asic_Tc1_UNDER_CURR'
    'Ach_Asic_Esv0_AVG_CURRENT'
    'Ach_Asic_Esv0_PWM_FAULT'
    'Ach_Asic_Esv0_CURR_SENSE'
    'Ach_Asic_Esv0_ADC_FAULT'
    'Ach_Asic_Esv0_T_SD'
    'Ach_Asic_Esv0_OPEN_LOAD'
    'Ach_Asic_Esv0_GND_LOSS'
    'Ach_Asic_Esv0_LVT'
    'Ach_Asic_Esv0_LS_OVC'
    'Ach_Asic_Esv0_VGS_LS_FAULT'
    'Ach_Asic_Esv0_VGS_HS_FAULT'
    'Ach_Asic_Esv0_HS_SHORT'
    'Ach_Asic_Esv0_LS_CLAMP_ON'
    'Ach_Asic_Esv0_UNDER_CURR'
    'Ach_Asic_Esv1_AVG_CURRENT'
    'Ach_Asic_Esv1_PWM_FAULT'
    'Ach_Asic_Esv1_CURR_SENSE'
    'Ach_Asic_Esv1_ADC_FAULT'
    'Ach_Asic_Esv1_T_SD'
    'Ach_Asic_Esv1_OPEN_LOAD'
    'Ach_Asic_Esv1_GND_LOSS'
    'Ach_Asic_Esv1_LVT'
    'Ach_Asic_Esv1_LS_OVC'
    'Ach_Asic_Esv1_VGS_LS_FAULT'
    'Ach_Asic_Esv1_VGS_HS_FAULT'
    'Ach_Asic_Esv1_HS_SHORT'
    'Ach_Asic_Esv1_LS_CLAMP_ON'
    'Ach_Asic_Esv1_UNDER_CURR'
    };
AbsVlvM_MainAchValveAsicInfo = CreateBus(AbsVlvM_MainAchValveAsicInfo, DeList);
clear DeList;

AbsVlvM_MainArbFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
AbsVlvM_MainArbFsrAbsDrvInfo = CreateBus(AbsVlvM_MainArbFsrAbsDrvInfo, DeList);
clear DeList;

AbsVlvM_MainArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
AbsVlvM_MainArbWhlVlvReqInfo = CreateBus(AbsVlvM_MainArbWhlVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    };
AbsVlvM_MainArbNormVlvReqInfo = CreateBus(AbsVlvM_MainArbNormVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainArbBalVlvReqInfo = Simulink.Bus;
DeList={
    'BalVlvReqData_array_0'
    'BalVlvReqData_array_1'
    'BalVlvReqData_array_2'
    'BalVlvReqData_array_3'
    'BalVlvReqData_array_4'
    };
AbsVlvM_MainArbBalVlvReqInfo = CreateBus(AbsVlvM_MainArbBalVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainArbResPVlvReqInfo = Simulink.Bus;
DeList={
    'ResPVlvReqData_array_0'
    'ResPVlvReqData_array_1'
    'ResPVlvReqData_array_2'
    'ResPVlvReqData_array_3'
    'ResPVlvReqData_array_4'
    };
AbsVlvM_MainArbResPVlvReqInfo = CreateBus(AbsVlvM_MainArbResPVlvReqInfo, DeList);
clear DeList;

AbsVlvM_MainAchAsicInvalid = Simulink.Bus;
DeList={'AbsVlvM_MainAchAsicInvalid'};
AbsVlvM_MainAchAsicInvalid = CreateBus(AbsVlvM_MainAchAsicInvalid, DeList);
clear DeList;

AbsVlvM_MainEcuModeSts = Simulink.Bus;
DeList={'AbsVlvM_MainEcuModeSts'};
AbsVlvM_MainEcuModeSts = CreateBus(AbsVlvM_MainEcuModeSts, DeList);
clear DeList;

AbsVlvM_MainIgnOnOffSts = Simulink.Bus;
DeList={'AbsVlvM_MainIgnOnOffSts'};
AbsVlvM_MainIgnOnOffSts = CreateBus(AbsVlvM_MainIgnOnOffSts, DeList);
clear DeList;

AbsVlvM_MainIgnEdgeSts = Simulink.Bus;
DeList={'AbsVlvM_MainIgnEdgeSts'};
AbsVlvM_MainIgnEdgeSts = CreateBus(AbsVlvM_MainIgnEdgeSts, DeList);
clear DeList;

AbsVlvM_MainVBatt1Mon = Simulink.Bus;
DeList={'AbsVlvM_MainVBatt1Mon'};
AbsVlvM_MainVBatt1Mon = CreateBus(AbsVlvM_MainVBatt1Mon, DeList);
clear DeList;

AbsVlvM_MainVBatt2Mon = Simulink.Bus;
DeList={'AbsVlvM_MainVBatt2Mon'};
AbsVlvM_MainVBatt2Mon = CreateBus(AbsVlvM_MainVBatt2Mon, DeList);
clear DeList;

AbsVlvM_MainDiagClrSrs = Simulink.Bus;
DeList={'AbsVlvM_MainDiagClrSrs'};
AbsVlvM_MainDiagClrSrs = CreateBus(AbsVlvM_MainDiagClrSrs, DeList);
clear DeList;

AbsVlvM_MainAsicEnDrDrv = Simulink.Bus;
DeList={'AbsVlvM_MainAsicEnDrDrv'};
AbsVlvM_MainAsicEnDrDrv = CreateBus(AbsVlvM_MainAsicEnDrDrv, DeList);
clear DeList;

AbsVlvM_MainArbVlvDriveState = Simulink.Bus;
DeList={'AbsVlvM_MainArbVlvDriveState'};
AbsVlvM_MainArbVlvDriveState = CreateBus(AbsVlvM_MainArbVlvDriveState, DeList);
clear DeList;

AbsVlvM_MainFspAbsHMon = Simulink.Bus;
DeList={'AbsVlvM_MainFspAbsHMon'};
AbsVlvM_MainFspAbsHMon = CreateBus(AbsVlvM_MainFspAbsHMon, DeList);
clear DeList;

AbsVlvM_MainFspAbsMon = Simulink.Bus;
DeList={'AbsVlvM_MainFspAbsMon'};
AbsVlvM_MainFspAbsMon = CreateBus(AbsVlvM_MainFspAbsMon, DeList);
clear DeList;

AbsVlvM_MainFSEscVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_flno_time'
    'fs_on_frno_time'
    'fs_on_rlno_time'
    'fs_on_rrno_time'
    'fs_on_bal_time'
    'fs_on_pressdump_time'
    'fs_on_resp_time'
    'fs_on_flnc_time'
    'fs_on_frnc_time'
    'fs_on_rlnc_time'
    'fs_on_rrnc_time'
    };
AbsVlvM_MainFSEscVlvActrInfo = CreateBus(AbsVlvM_MainFSEscVlvActrInfo, DeList);
clear DeList;

AbsVlvM_MainFSFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
AbsVlvM_MainFSFsrAbsDrvInfo = CreateBus(AbsVlvM_MainFSFsrAbsDrvInfo, DeList);
clear DeList;

AbsVlvM_MainAbsVlvMData = Simulink.Bus;
DeList={
    'AbsVlvM_ValveRelay_Open_Err'
    'AbsVlvM_ValveRelay_S2G_Err'
    'AbsVlvM_ValveRelay_S2B_Err'
    'AbsVlvM_ValveRelay_CP_Err'
    'AbsVlvM_ValveRelay_Short_Err'
    'AbsVlvM_ValveRelay_Batt1FuseOpen_Err'
    'AbsVlvM_ValveRelay_OverTemp_Err'
    'AbsVlvM_ValveRelay_ShutdownLine_Err'
    'AbsVlvM_ValveFLNO_Open_Err'
    'AbsVlvM_ValveFLNO_PsvOpen_Err'
    'AbsVlvM_ValveFLNO_Short_Err'
    'AbsVlvM_ValveFLNO_OverTemp_Err'
    'AbsVlvM_ValveFLNO_CurReg_Err'
    'AbsVlvM_ValveFRNO_Open_Err'
    'AbsVlvM_ValveFRNO_PsvOpen_Err'
    'AbsVlvM_ValveFRNO_Short_Err'
    'AbsVlvM_ValveFRNO_OverTemp_Err'
    'AbsVlvM_ValveFRNO_CurReg_Err'
    'AbsVlvM_ValveRLNO_Open_Err'
    'AbsVlvM_ValveRLNO_PsvOpen_Err'
    'AbsVlvM_ValveRLNO_Short_Err'
    'AbsVlvM_ValveRLNO_OverTemp_Err'
    'AbsVlvM_ValveRLNO_CurReg_Err'
    'AbsVlvM_ValveRRNO_Open_Err'
    'AbsVlvM_ValveRRNO_PsvOpen_Err'
    'AbsVlvM_ValveRRNO_Short_Err'
    'AbsVlvM_ValveRRNO_OverTemp_Err'
    'AbsVlvM_ValveRRNO_CurReg_Err'
    'AbsVlvM_ValveBAL_Open_Err'
    'AbsVlvM_ValveBAL_PsvOpen_Err'
    'AbsVlvM_ValveBAL_Short_Err'
    'AbsVlvM_ValveBAL_OverTemp_Err'
    'AbsVlvM_ValveBAL_CurReg_Err'
    'AbsVlvM_ValvePD_Open_Err'
    'AbsVlvM_ValvePD_PsvOpen_Err'
    'AbsVlvM_ValvePD_Short_Err'
    'AbsVlvM_ValvePD_OverTemp_Err'
    'AbsVlvM_ValvePD_CurReg_Err'
    'AbsVlvM_ValveRESP_Open_Err'
    'AbsVlvM_ValveRESP_PsvOpen_Err'
    'AbsVlvM_ValveRESP_Short_Err'
    'AbsVlvM_ValveRESP_OverTemp_Err'
    'AbsVlvM_ValveRESP_CurReg_Err'
    'AbsVlvM_ValveFLNC_Open_Err'
    'AbsVlvM_ValveFLNC_PsvOpen_Err'
    'AbsVlvM_ValveFLNC_Short_Err'
    'AbsVlvM_ValveFLNC_OverTemp_Err'
    'AbsVlvM_ValveFLNC_CurReg_Err'
    'AbsVlvM_ValveFRNC_Open_Err'
    'AbsVlvM_ValveFRNC_PsvOpen_Err'
    'AbsVlvM_ValveFRNC_Short_Err'
    'AbsVlvM_ValveFRNC_OverTemp_Err'
    'AbsVlvM_ValveFRNC_CurReg_Err'
    'AbsVlvM_ValveRLNC_Open_Err'
    'AbsVlvM_ValveRLNC_PsvOpen_Err'
    'AbsVlvM_ValveRLNC_Short_Err'
    'AbsVlvM_ValveRLNC_OverTemp_Err'
    'AbsVlvM_ValveRLNC_CurReg_Err'
    'AbsVlvM_ValveRRNC_Open_Err'
    'AbsVlvM_ValveRRNC_PsvOpen_Err'
    'AbsVlvM_ValveRRNC_Short_Err'
    'AbsVlvM_ValveRRNC_OverTemp_Err'
    'AbsVlvM_ValveRRNC_CurReg_Err'
    };
AbsVlvM_MainAbsVlvMData = CreateBus(AbsVlvM_MainAbsVlvMData, DeList);
clear DeList;

AbsVlvM_MainFSEscVlvInitEndFlg = Simulink.Bus;
DeList={'AbsVlvM_MainFSEscVlvInitEndFlg'};
AbsVlvM_MainFSEscVlvInitEndFlg = CreateBus(AbsVlvM_MainFSEscVlvInitEndFlg, DeList);
clear DeList;

