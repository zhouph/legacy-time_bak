/**
 * @defgroup Mom_Cfg Mom_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mom_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mom_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MOM_START_SEC_CONST_UNSPECIFIED
#include "Mom_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MOM_STOP_SEC_CONST_UNSPECIFIED
#include "Mom_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_START_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MOM_STOP_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
#define MOM_START_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MOM_STOP_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_START_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/** Variable Section (32BIT)**/


#define MOM_STOP_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_START_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MOM_STOP_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
#define MOM_START_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MOM_STOP_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_START_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/** Variable Section (32BIT)**/


#define MOM_STOP_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MOM_START_SEC_CODE
#include "Mom_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MOM_STOP_SEC_CODE
#include "Mom_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
