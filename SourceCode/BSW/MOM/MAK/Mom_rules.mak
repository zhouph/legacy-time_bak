# \file
#
# \brief Mom
#
# This file contains the implementation of the SWC
# module Mom.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Mom_src

Mom_src_FILES        += $(Mom_SRC_PATH)\Mom_Hndlr.c
Mom_src_FILES        += $(Mom_IFA_PATH)\Mom_Hndlr_Ifa.c
Mom_src_FILES        += $(Mom_CFG_PATH)\Mom_Cfg.c
Mom_src_FILES        += $(Mom_CAL_PATH)\Mom_Cal.c

ifeq ($(ICE_COMPILE),true)
Mom_src_FILES        += $(Mom_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Mom_src_FILES        += $(Mom_UNITY_PATH)\unity.c
	Mom_src_FILES        += $(Mom_UNITY_PATH)\unity_fixture.c	
	Mom_src_FILES        += $(Mom_UT_PATH)\main.c
	Mom_src_FILES        += $(Mom_UT_PATH)\Mom_Hndlr\Mom_Hndlr_UtMain.c
endif