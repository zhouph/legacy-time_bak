# \file
#
# \brief Mom
#
# This file contains the implementation of the SWC
# module Mom.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Mom_CORE_PATH     := $(MANDO_BSW_ROOT)\Mom
Mom_CAL_PATH      := $(Mom_CORE_PATH)\CAL\$(Mom_VARIANT)
Mom_SRC_PATH      := $(Mom_CORE_PATH)\SRC
Mom_CFG_PATH      := $(Mom_CORE_PATH)\CFG\$(Mom_VARIANT)
Mom_HDR_PATH      := $(Mom_CORE_PATH)\HDR
Mom_IFA_PATH      := $(Mom_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Mom_CMN_PATH      := $(Mom_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Mom_UT_PATH		:= $(Mom_CORE_PATH)\UT
	Mom_UNITY_PATH	:= $(Mom_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Mom_UT_PATH)
	CC_INCLUDE_PATH		+= $(Mom_UNITY_PATH)
	Mom_Hndlr_PATH 	:= Mom_UT_PATH\Mom_Hndlr
endif
CC_INCLUDE_PATH    += $(Mom_CAL_PATH)
CC_INCLUDE_PATH    += $(Mom_SRC_PATH)
CC_INCLUDE_PATH    += $(Mom_CFG_PATH)
CC_INCLUDE_PATH    += $(Mom_HDR_PATH)
CC_INCLUDE_PATH    += $(Mom_IFA_PATH)
CC_INCLUDE_PATH    += $(Mom_CMN_PATH)

