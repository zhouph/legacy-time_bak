#include "unity.h"
#include "unity_fixture.h"
#include "Mom_Hndlr.h"
#include "Mom_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Eem_SuspcDetnFuncInhibitMomSts_t UtInput_Mom_HndlrFuncInhibitMomSts[MAX_STEP] = MOM_HNDLRFUNCINHIBITMOMSTS;

const Mom_HndlrEcuModeSts_t UtExpected_Mom_HndlrEcuModeSts[MAX_STEP] = MOM_HNDLRECUMODESTS;
const Mom_HndlrMomEcuInhibit_t UtExpected_Mom_HndlrMomEcuInhibit[MAX_STEP] = MOM_HNDLRMOMECUINHIBIT;



TEST_GROUP(Mom_Hndlr);
TEST_SETUP(Mom_Hndlr)
{
    Mom_Hndlr_Init();
}

TEST_TEAR_DOWN(Mom_Hndlr)
{   /* Postcondition */

}

TEST(Mom_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mom_HndlrFuncInhibitMomSts = UtInput_Mom_HndlrFuncInhibitMomSts[i];

        Mom_Hndlr();

        TEST_ASSERT_EQUAL(Mom_HndlrEcuModeSts, UtExpected_Mom_HndlrEcuModeSts[i]);
        TEST_ASSERT_EQUAL(Mom_HndlrMomEcuInhibit, UtExpected_Mom_HndlrMomEcuInhibit[i]);
    }
}

TEST_GROUP_RUNNER(Mom_Hndlr)
{
    RUN_TEST_CASE(Mom_Hndlr, All);
}
