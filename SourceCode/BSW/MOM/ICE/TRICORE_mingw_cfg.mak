# \file
#
# \brief AUTOSAR ApplTemplates
#
# This file contains the implementation of the AUTOSAR
# module ApplTemplates.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# This file contains common compiler options for all TRICORE devices using
# the TASKING toolchain.


# TASKING_MODE:
#
# To compile a C source file the C compiler and the assembler must
# be called. This can be done by one call of the control program cctc
# or by a call of the compiler ctc and the assembler astc. The variable
# TASKING_MODE allows to select the kind of the tool chain call.
# Valid values are CONTROL_PROGRAM and COMPILER_ASSEMBLER.
TASKING_MODE = CONTROL_PROGRAM


# CC_OPT defines common options for the compiler.

# Set TriCore EABI compliancy mode:
# -D disallow an alternative DWARF version
# -f allow treat 'double' as 'float'; see also --no-double option
# -H disallow half-word alignment
# -N disallow the use of option --no-clear
# -S disallow structure-return optimization
CC_OPT += 

# Define the default options for the assembler

# Use the TASKING preprocessor
ASM_OPT += 

# Define the options for the linker

# Tell linker about LINK_PLACE options
LINK_OPT += $(LINK_PLACE)

# Linker archive options: insert-replace/create/update
AR_OPT += 
# EXT_LOCATOR_FILE:
# specify the name for an external locator file
# if no name is given, a default locator file $(BOARD).ldscript is taken
# which is composed in file <board>.mak
EXT_LOCATOR_FILE +=

# General path setup

