

typedef struct _Ifx_STM_TIM0_Bits
{
    unsigned int STM31_0:32;                /**< \brief [31:0] System Timer Bits [31:0] (r) */
} Ifx_STM_TIM0_Bits;


typedef union
{
    /** \brief Unsigned access */
    unsigned int U;
    /** \brief Signed access */
    signed int I;
    /** \brief Bitfield access */
    Ifx_STM_TIM0_Bits B;
} Ifx_STM_TIM0;

#define STM0_TIM0 /*lint --e(923)*/ (*(volatile Ifx_STM_TIM0*)0xF0000010u)