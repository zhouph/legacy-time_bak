/**
 * @defgroup Mom_Hndlr Mom_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mom_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MOM_HNDLR_H_
#define MOM_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mom_Types.h"
#include "Mom_Cfg.h"
#include "Mom_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MOM_HNDLR_MODULE_ID      (0)
 #define MOM_HNDLR_MAJOR_VERSION  (2)
 #define MOM_HNDLR_MINOR_VERSION  (0)
 #define MOM_HNDLR_PATCH_VERSION  (0)
 #define MOM_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mom_Hndlr_HdrBusType Mom_HndlrBus;

/* Version Info */
extern const SwcVersionInfo_t Mom_HndlrVersionInfo;

/* Input Data Element */
extern Eem_SuspcDetnFuncInhibitMomSts_t Mom_HndlrFuncInhibitMomSts;

/* Output Data Element */
extern Mom_HndlrEcuModeSts_t Mom_HndlrEcuModeSts;
extern Mom_HndlrMomEcuInhibit_t Mom_HndlrMomEcuInhibit;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mom_Hndlr_Init(void);
extern void Mom_Hndlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MOM_HNDLR_H_ */
/** @} */
