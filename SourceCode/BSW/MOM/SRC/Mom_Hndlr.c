/**
 * @defgroup Mom_Hndlr Mom_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mom_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mom_Hndlr.h"
#include "Mom_Hndlr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MOM_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Mom_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MOM_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Mom_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MOM_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mom_Hndlr_HdrBusType Mom_HndlrBus;

/* Version Info */
const SwcVersionInfo_t Mom_HndlrVersionInfo = 
{   
    MOM_HNDLR_MODULE_ID,           /* Mom_HndlrVersionInfo.ModuleId */
    MOM_HNDLR_MAJOR_VERSION,       /* Mom_HndlrVersionInfo.MajorVer */
    MOM_HNDLR_MINOR_VERSION,       /* Mom_HndlrVersionInfo.MinorVer */
    MOM_HNDLR_PATCH_VERSION,       /* Mom_HndlrVersionInfo.PatchVer */
    MOM_HNDLR_BRANCH_VERSION       /* Mom_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_SuspcDetnFuncInhibitMomSts_t Mom_HndlrFuncInhibitMomSts;

/* Output Data Element */
Mom_HndlrEcuModeSts_t Mom_HndlrEcuModeSts;
Mom_HndlrMomEcuInhibit_t Mom_HndlrMomEcuInhibit;

uint32 Mom_Hndlr_Timer_Start;
uint32 Mom_Hndlr_Timer_Elapsed;

#define MOM_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MOM_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
#define MOM_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MOM_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_HNDLR_START_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/** Variable Section (32BIT)**/


#define MOM_HNDLR_STOP_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MOM_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MOM_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MOM_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Mom_MemMap.h"
#define MOM_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MOM_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Mom_MemMap.h"
#define MOM_HNDLR_START_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/** Variable Section (32BIT)**/


#define MOM_HNDLR_STOP_SEC_VAR_32BIT
#include "Mom_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MOM_HNDLR_START_SEC_CODE
#include "Mom_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mom_Hndlr_Init(void)
{
    /* Initialize internal bus */
    Mom_HndlrBus.Mom_HndlrFuncInhibitMomSts = 0;
    Mom_HndlrBus.Mom_HndlrEcuModeSts = 0;
    Mom_HndlrBus.Mom_HndlrMomEcuInhibit = 0;
}

void Mom_Hndlr(void)
{
    Mom_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mom_Hndlr_Read_Mom_HndlrFuncInhibitMomSts(&Mom_HndlrBus.Mom_HndlrFuncInhibitMomSts);

    /* Process */

    /* Output */
    Mom_Hndlr_Write_Mom_HndlrEcuModeSts(&Mom_HndlrBus.Mom_HndlrEcuModeSts);
    Mom_Hndlr_Write_Mom_HndlrMomEcuInhibit(&Mom_HndlrBus.Mom_HndlrMomEcuInhibit);

    Mom_Hndlr_Timer_Elapsed = STM0_TIM0.U - Mom_Hndlr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MOM_HNDLR_STOP_SEC_CODE
#include "Mom_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
