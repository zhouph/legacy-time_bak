Mom_HndlrFuncInhibitMomSts = Simulink.Bus;
DeList={'Mom_HndlrFuncInhibitMomSts'};
Mom_HndlrFuncInhibitMomSts = CreateBus(Mom_HndlrFuncInhibitMomSts, DeList);
clear DeList;

Mom_HndlrEcuModeSts = Simulink.Bus;
DeList={'Mom_HndlrEcuModeSts'};
Mom_HndlrEcuModeSts = CreateBus(Mom_HndlrEcuModeSts, DeList);
clear DeList;

Mom_HndlrMomEcuInhibit = Simulink.Bus;
DeList={'Mom_HndlrMomEcuInhibit'};
Mom_HndlrMomEcuInhibit = CreateBus(Mom_HndlrMomEcuInhibit, DeList);
clear DeList;

