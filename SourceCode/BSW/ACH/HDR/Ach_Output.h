/**
 * @defgroup Ach_Output Ach_Output
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Output.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACH_OUTPUT_H_
#define ACH_OUTPUT_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Types.h"
#include "Ach_Cfg.h"
#include "Ach_Cal.h"
#include "Spim_Cdd.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ACH_OUTPUT_MODULE_ID      (0)
 #define ACH_OUTPUT_MAJOR_VERSION  (2)
 #define ACH_OUTPUT_MINOR_VERSION  (0)
 #define ACH_OUTPUT_PATCH_VERSION  (0)
 #define ACH_OUTPUT_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ach_Output_HdrBusType Ach_OutputBus;

/* Version Info */
extern const SwcVersionInfo_t Ach_OutputVersionInfo;

/* Input Data Element */
extern Aka_MainAkaKeepAliveWriteData_t Ach_OutputAkaKeepAliveWriteData;
extern Ioc_OutputSR1msIocDcMtrDutyData_t Ach_OutputIocDcMtrDutyData;
extern Ioc_OutputSR1msIocDcMtrFreqData_t Ach_OutputIocDcMtrFreqData;
extern Ioc_OutputSR1msIocVlvNo0Data_t Ach_OutputIocVlvNo0Data;
extern Ioc_OutputSR1msIocVlvNo1Data_t Ach_OutputIocVlvNo1Data;
extern Ioc_OutputSR1msIocVlvNo2Data_t Ach_OutputIocVlvNo2Data;
extern Ioc_OutputSR1msIocVlvNo3Data_t Ach_OutputIocVlvNo3Data;
extern Ioc_OutputSR1msIocVlvTc0Data_t Ach_OutputIocVlvTc0Data;
extern Ioc_OutputSR1msIocVlvTc1Data_t Ach_OutputIocVlvTc1Data;
extern Ioc_OutputSR1msIocVlvEsv0Data_t Ach_OutputIocVlvEsv0Data;
extern Ioc_OutputSR1msIocVlvEsv1Data_t Ach_OutputIocVlvEsv1Data;
extern Ioc_OutputSR1msIocVlvNc0Data_t Ach_OutputIocVlvNc0Data;
extern Ioc_OutputSR1msIocVlvNc1Data_t Ach_OutputIocVlvNc1Data;
extern Ioc_OutputSR1msIocVlvNc2Data_t Ach_OutputIocVlvNc2Data;
extern Ioc_OutputSR1msIocVlvNc3Data_t Ach_OutputIocVlvNc3Data;
extern Ioc_OutputSR1msIocVlvRelayData_t Ach_OutputIocVlvRelayData;
extern Ioc_OutputSR1msIocAdcSelWriteData_t Ach_OutputIocAdcSelWriteData;
extern Acm_MainAcmAsicInitCompleteFlag_t Ach_OutputAcmAsicInitCompleteFlag;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ach_Output_Init(void);
extern void Ach_Output(void);
extern Std_ReturnType Ach_SetReadSpiData_CS(ACH_MsgIdType ReadMsgId);
extern Std_ReturnType Ach_SetReadSpiData_CS(ACH_MsgIdType ReadMsgId);
extern Std_ReturnType Ach_SetWriteSpiData_CS(ACH_WriteMsgSetType WriteMsgSet, void* Data);


/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACH_OUTPUT_H_ */
/** @} */
