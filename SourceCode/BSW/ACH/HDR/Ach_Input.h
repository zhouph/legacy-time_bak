/**
 * @defgroup Ach_Input Ach_Input
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Input.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACH_INPUT_H_
#define ACH_INPUT_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Types.h"
#include "Ach_Cfg.h"
#include "Ach_Cal.h"
#include "Spim_Cdd.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ACH_INPUT_MODULE_ID      (0)
 #define ACH_INPUT_MAJOR_VERSION  (2)
 #define ACH_INPUT_MINOR_VERSION  (0)
 #define ACH_INPUT_PATCH_VERSION  (0)
 #define ACH_INPUT_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ach_Input_HdrBusType Ach_InputBus;

/* Version Info */
extern const SwcVersionInfo_t Ach_InputVersionInfo;

/* Input Data Element */

/* Output Data Element */
extern Ach_InputAchSysPwrAsicInfo_t Ach_InputAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t Ach_InputAchValveAsicInfo;
extern Ach_InputAchAdcData_t Ach_InputAchAdcData;
extern Ach_InputAchWssPort0AsicInfo_t Ach_InputAchWssPort0AsicInfo;
extern Ach_InputAchWssPort1AsicInfo_t Ach_InputAchWssPort1AsicInfo;
extern Ach_InputAchWssPort2AsicInfo_t Ach_InputAchWssPort2AsicInfo;
extern Ach_InputAchWssPort3AsicInfo_t Ach_InputAchWssPort3AsicInfo;
extern Ach_InputAchVsoSelAsicInfo_t Ach_InputAchVsoSelAsicInfo;
extern Ach_InputAchWldShlsAsicInfo_t Ach_InputAchWldShlsAsicInfo;
extern Ach_InputAchWssRedncyAsicInfo_t Ach_InputAchWssRedncyAsicInfo;
extern Ach_InputAchMotorAsicInfo_t Ach_InputAchMotorAsicInfo;
extern Ach_InputAchAsicInvalidInfo_t Ach_InputAchAsicInvalid;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ach_Input_Init(void);
extern void Ach_Input(void);
extern Std_ReturnType ACH_GetSigData_CS(ACH_SigIdType ACH_SigId, uint16* Data);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACH_INPUT_H_ */
/** @} */
