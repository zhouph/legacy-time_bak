/**
 * @defgroup Ach_Types Ach_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACH_TYPES_H_
#define ACH_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define CRC5_POLY_U32       (0x94000000u)   /* CRC polynomial x^5 + X^2 + 1 */
 

#define MAX_BUFFER_SIZE     60u

#define SPI_DMA_ENABLE

#define MOTOR_10PER_DUTY        10
#define MOTOR_90PER_DUTY        90
#define MOTOR_MAX_DUTY          100     /*100 = 100% duty*/

 /* SPI ADDRESS */
#define ASIC_MSG0_ID_CHIPID             0
#define ASIC_MSG1_ID_GENCFG             1
#define ASIC_MSG2_ID_GENCFG2            2
#define ASIC_MSG3_ID_GENSTATUS          3
#define ASIC_MSG4_ID_TEMPSENS           4
#define ASIC_MSG5_ID_COMM_FAULTS        5
#define ASIC_MSG6_ID_SERVTESTEN         6
#define ASIC_MSG7_ID_SERVTESTSTATUS     7
#define ASIC_MSG8_ID_PUMPCFG            8
#define ASIC_MSG9_ID_PUMPCFG2           9
#define ASIC_MSG10_ID_PUMPDRD           10
#define ASIC_MSG11_ID_VDDFLT            11
#define ASIC_MSG12_ID_VDDSTATUS         12
#define ASIC_MSG13_ID_ADC_DR            13
#define ASIC_MSG15_ID_VSOSER            15
#define ASIC_MSG16_ID_VSODR             16
#define ASIC_MSG17_ID_WLD1SHLS_CONFIG   17
#define ASIC_MSG18_ID_WLD1SHLSDR        18
#define ASIC_MSG19_ID_SERVFLTMSK        19
#define ASIC_MSG20_ID_RESOUTPUT         20
#define ASIC_MSG32_ID_WSITEST           32
#define ASIC_MSG33_ID_WSIAUXCONF        33
#define ASIC_MSG34_ID_WSIAUXCONF2       34
#define ASIC_MSG35_ID_WSIRSCR1          35
#define ASIC_MSG36_ID_WSIRSCR2          36
#define ASIC_MSG37_ID_WSIRSCR3          37
#define ASIC_MSG38_ID_WSIRSCR4          38
#define ASIC_MSG39_ID_WSICTRL           39
#define ASIC_MSG40_ID_WSCOUNT1          40
#define ASIC_MSG41_ID_WSCOUNT2          41
#define ASIC_MSG42_ID_WSCOUNT3          42
#define ASIC_MSG43_ID_WSCOUNT4          43
#define ASIC_MSG44_ID_WSBIST            44
#define ASIC_MSG48_ID_WSIRSDR1          48
#define ASIC_MSG49_ID_WSIRSDR1          49
#define ASIC_MSG50_ID_WSIRSDR1          50
#define ASIC_MSG51_ID_WSIRSDR1          51
#define ASIC_MSG52_ID_WSIRSDR2          52
#define ASIC_MSG53_ID_WSIRSDR2          53
#define ASIC_MSG54_ID_WSIRSDR2          54
#define ASIC_MSG55_ID_WSIRSDR2          55
#define ASIC_MSG56_ID_WSIRSDR3          56
#define ASIC_MSG57_ID_WSIRSDR3          57
#define ASIC_MSG58_ID_WSIRSDR3          58
#define ASIC_MSG59_ID_WSIRSDR3          59
#define ASIC_MSG60_ID_WSIRSDR4          60
#define ASIC_MSG61_ID_WSIRSDR4          61
#define ASIC_MSG62_ID_WSIRSDR4          62
#define ASIC_MSG63_ID_WSIRSDR4          63
#define ASIC_MSG64_ID_SOLSERVENA        64
#define ASIC_MSG65_ID_SOLENDR           65
#define ASIC_MSG67_ID_ONOFF_CH_CONFIG   67
#define ASIC_MSG68_ID_ONOFF_CH_1_DR     68
#define ASIC_MSG69_ID_ONOFF_CH_2_DR     69
#define ASIC_MSG70_ID_ONOFF_CH_3_DR     70
#define ASIC_MSG71_ID_ONOFF_CH_4_DR     71
#define ASIC_MSG72_ID_WDG2SEED          72
#define ASIC_MSG73_ID_WDG2STATUS        73
#define ASIC_MSG74_ID_WDG2STARTTMG      74
#define ASIC_MSG75_ID_WDG2DELTATMG      75
#define ASIC_MSG76_ID_WDG2TOUTTMG       76
#define ASIC_MSG77_ID_WDG2PGM           77
#define ASIC_MSG78_ID_WDG2ANS           78
#define ASIC_MSG90_ID_TEST_MODE_ENTER   90

/* NO 0 */
#define ASIC_MSG128_NO0_CHX_FAULT          128
#define ASIC_MSG129_NO0_EXCEPTIONS_CHX     129
#define ASIC_MSG130_NO0_CONFIGURATION_CHX  130
#define ASIC_MSG133_NO0_SETPOINT_CHX       133
#define ASIC_MSG134_NO0_CTRLCFG_CHX        134
#define ASIC_MSG135_NO0_FMODULATION_CHX    135
#define ASIC_MSG136_NO0_KGAINS_CHX         136
#define ASIC_MSG137_NO0_OFFCMP_CHX         137
#define ASIC_MSG138_NO0_AVGCURR_CHX        138
#define ASIC_MSG140_NO0_PWMSENSE_CHX       140
#define ASIC_MSG141_NO0_BASE_DELTA_CURR    141
#define ASIC_MSG142_NO0_DELTA_CURR         142

/* NO 1 */
#define ASIC_MSG144_NO1_CHX_FAULT          144
#define ASIC_MSG145_NO1_EXCEPTIONS_CHX     145
#define ASIC_MSG146_NO1_CONFIGURATION_CHX  146
#define ASIC_MSG149_NO1_SETPOINT_CHX       149
#define ASIC_MSG150_NO1_CTRLCFG_CHX        150
#define ASIC_MSG151_NO1_FMODULATION_CHX    151
#define ASIC_MSG152_NO1_KGAINS_CHX         152
#define ASIC_MSG153_NO1_OFFCMP_CHX         153
#define ASIC_MSG154_NO1_AVGCURR_CHX        154
#define ASIC_MSG156_NO1_PWMSENSE_CHX       156
#define ASIC_MSG157_NO1_BASE_DELTA_CURR    157
#define ASIC_MSG158_NO1_DELTA_CURR         158

/* NO 2 */
#define ASIC_MSG160_NO2_CHX_FAULT          160
#define ASIC_MSG161_NO2_EXCEPTIONS_CHX     161
#define ASIC_MSG162_NO2_CONFIGURATION_CHX  162
#define ASIC_MSG165_NO2_SETPOINT_CHX       165
#define ASIC_MSG166_NO2_CTRLCFG_CHX        166
#define ASIC_MSG167_NO2_FMODULATION_CHX    167
#define ASIC_MSG168_NO2_KGAINS_CHX         168
#define ASIC_MSG169_NO2_OFFCMP_CHX         169
#define ASIC_MSG170_NO2_AVGCURR_CHX        170
#define ASIC_MSG172_NO2_PWMSENSE_CHX       172
#define ASIC_MSG173_NO2_BASE_DELTA_CURR    173
#define ASIC_MSG174_NO2_DELTA_CURR         174

/* NO 3 */
#define ASIC_MSG176_NO3_CHX_FAULT          176
#define ASIC_MSG177_NO3_EXCEPTIONS_CHX     177
#define ASIC_MSG178_NO3_CONFIGURATION_CHX  178
#define ASIC_MSG181_NO3_SETPOINT_CHX       181
#define ASIC_MSG182_NO3_CTRLCFG_CHX        182
#define ASIC_MSG183_NO3_FMODULATION_CHX    183
#define ASIC_MSG184_NO3_KGAINS_CHX         184
#define ASIC_MSG185_NO3_OFFCMP_CHX         185
#define ASIC_MSG186_NO3_AVGCURR_CHX        186
#define ASIC_MSG188_NO3_PWMSENSE_CHX       188
#define ASIC_MSG189_NO3_BASE_DELTA_CURR    189
#define ASIC_MSG190_NO3_DELTA_CURR         190

/* TC 0 */
#define ASIC_MSG192_TC0_CHX_FAULT          192
#define ASIC_MSG193_TC0_EXCEPTIONS_CHX     193
#define ASIC_MSG194_TC0_CONFIGURATION_CHX  194
#define ASIC_MSG197_TC0_SETPOINT_CHX       197
#define ASIC_MSG198_TC0_CTRLCFG_CHX        198
#define ASIC_MSG199_TC0_FMODULATION_CHX    199
#define ASIC_MSG200_TC0_KGAINS_CHX         200
#define ASIC_MSG201_TC0_OFFCMP_CHX         201
#define ASIC_MSG202_TC0_AVGCURR_CHX        202
#define ASIC_MSG204_TC0_PWMSENSE_CHX       204
#define ASIC_MSG205_TC0_BASE_DELTA_CURR    205
#define ASIC_MSG206_TC0_DELTA_CURR         206

/* TC 1 */
#define ASIC_MSG208_TC1_CHX_FAULT          208
#define ASIC_MSG209_TC1_EXCEPTIONS_CHX     209
#define ASIC_MSG210_TC1_CONFIGURATION_CHX  210
#define ASIC_MSG213_TC1_SETPOINT_CHX       213
#define ASIC_MSG214_TC1_CTRLCFG_CHX        214
#define ASIC_MSG215_TC1_FMODULATION_CHX    215
#define ASIC_MSG216_TC1_KGAINS_CHX         216
#define ASIC_MSG217_TC1_OFFCMP_CHX         217
#define ASIC_MSG218_TC1_AVGCURR_CHX        218
#define ASIC_MSG220_TC1_PWMSENSE_CHX       220
#define ASIC_MSG221_TC1_BASE_DELTA_CURR    221
#define ASIC_MSG222_TC1_DELTA_CURR         222

/* ESV 0 */
#define ASIC_MSG225_ESV0_EXCEPTIONS_CHX    225
#define ASIC_MSG226_ESV0_CONFIGURATION_CHX 226
#define ASIC_MSG229_ESV0_SETPOINT_CHX      229
#define ASIC_MSG230_ESV0_CTRLCFG_CHX       230
#define ASIC_MSG232_ESV0_KGAINS_CHX        232
#define ASIC_MSG233_ESV0_OFFCMP_CHX        233
#define ASIC_MSG234_ESV0_AVGCURR_CHX       234
#define ASIC_MSG236_ESV0_PWMSENSE_CHX      236

/* ESV 1 */
#define ASIC_MSG241_ESV1_EXCEPTIONS_CHX    241
#define ASIC_MSG242_ESV1_CONFIGURATION_CHX 242
#define ASIC_MSG245_ESV1_SETPOINT_CHX      245
#define ASIC_MSG246_ESV1_CTRLCFG_CHX       246
#define ASIC_MSG248_ESV1_KGAINS_CHX        248
#define ASIC_MSG249_ESV1_OFFCMP_CHX        249
#define ASIC_MSG250_ESV1_AVGCURR_CHX       250
#define ASIC_MSG252_ESV1_PWMSENSE_CHX      252


/* INVALID */
#define ACH_INV_NO_FAULT                    (0U)
#define ACH_INV_IS_ERROR                    (1UL << 0)
#define ACH_INV_TIMEOUT                     (1UL << 1)
#define ACH_INV_CHECKSUM                    (1UL << 2)
#define ACH_INV_INVALID_MSG                 (1UL << 3)
#define ACH_INV_INCORRECT_FREQ              (1UL << 4)
#define ACH_INV_ERRATIC                     (1UL << 5)
#define ACH_INV_ROM_FAIL                    (1UL << 6)
#define ACH_INV_RAM_FAIL                    (1UL << 7)
#define ACH_INV_EEPROM_FAIL                 (1UL << 8)
#define ACH_INV_INTERNAL_ELEC               (1UL << 9)
#define ACH_INV_INCORRECT_COMPNT            (1UL << 10)
#define ACH_INV_ALIVE_CNT                   (1UL << 11)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Aka_MainAkaKeepAliveWriteData_t Ach_OutputAkaKeepAliveWriteData;
    Ioc_OutputSR1msIocDcMtrDutyData_t Ach_OutputIocDcMtrDutyData;
    Ioc_OutputSR1msIocDcMtrFreqData_t Ach_OutputIocDcMtrFreqData;
    Ioc_OutputSR1msIocVlvNo0Data_t Ach_OutputIocVlvNo0Data;
    Ioc_OutputSR1msIocVlvNo1Data_t Ach_OutputIocVlvNo1Data;
    Ioc_OutputSR1msIocVlvNo2Data_t Ach_OutputIocVlvNo2Data;
    Ioc_OutputSR1msIocVlvNo3Data_t Ach_OutputIocVlvNo3Data;
    Ioc_OutputSR1msIocVlvTc0Data_t Ach_OutputIocVlvTc0Data;
    Ioc_OutputSR1msIocVlvTc1Data_t Ach_OutputIocVlvTc1Data;
    Ioc_OutputSR1msIocVlvEsv0Data_t Ach_OutputIocVlvEsv0Data;
    Ioc_OutputSR1msIocVlvEsv1Data_t Ach_OutputIocVlvEsv1Data;
    Ioc_OutputSR1msIocVlvNc0Data_t Ach_OutputIocVlvNc0Data;
    Ioc_OutputSR1msIocVlvNc1Data_t Ach_OutputIocVlvNc1Data;
    Ioc_OutputSR1msIocVlvNc2Data_t Ach_OutputIocVlvNc2Data;
    Ioc_OutputSR1msIocVlvNc3Data_t Ach_OutputIocVlvNc3Data;
    Ioc_OutputSR1msIocVlvRelayData_t Ach_OutputIocVlvRelayData;
    Ioc_OutputSR1msIocAdcSelWriteData_t Ach_OutputIocAdcSelWriteData;
    Acm_MainAcmAsicInitCompleteFlag_t Ach_OutputAcmAsicInitCompleteFlag;

/* Output Data Element */
}Ach_Output_HdrBusType;

typedef struct
{
/* Input Data Element */

/* Output Data Element */
    Ach_InputAchSysPwrAsicInfo_t Ach_InputAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t Ach_InputAchValveAsicInfo;
    Ach_InputAchAdcData_t Ach_InputAchAdcData;
    Ach_InputAchWssPort0AsicInfo_t Ach_InputAchWssPort0AsicInfo;
    Ach_InputAchWssPort1AsicInfo_t Ach_InputAchWssPort1AsicInfo;
    Ach_InputAchWssPort2AsicInfo_t Ach_InputAchWssPort2AsicInfo;
    Ach_InputAchWssPort3AsicInfo_t Ach_InputAchWssPort3AsicInfo;
    Ach_InputAchVsoSelAsicInfo_t Ach_InputAchVsoSelAsicInfo;
    Ach_InputAchWldShlsAsicInfo_t Ach_InputAchWldShlsAsicInfo;
    Ach_InputAchWssRedncyAsicInfo_t Ach_InputAchWssRedncyAsicInfo;
    Ach_InputAchMotorAsicInfo_t Ach_InputAchMotorAsicInfo;
    Ach_InputAchAsicInvalidInfo_t Ach_InputAchAsicInvalid;
}Ach_Input_HdrBusType;

enum
{
    ACH_DISABLE      = 0u,
    ACH_ENABLE       = 1u
};

enum
{
    ACH_ASIC_RW_READ  = 0u,
    ACH_ASIC_RW_WRITE = 1u
};

enum
{
    ACH_REQ_NOT_SET  = 0u,
    ACH_REQ_SET      = 1u
};

enum
{
    ACH_STATUS_IDLE  = 0u,
    ACH_STATUS_SENT  = 1u,
    ACH_STATUS_ERROR = 2u
};

enum
{
    ACH_VALVE_TRISTATE_MODE = 0u,
    ACH_VALVE_ACTIVE_MODE   = 1u
};

enum
{
    ACH_VALVE_PWM_MODE      = 0u,
    ACH_VALVE_FULLON_MODE   = 1u
};

enum
{
    NC_VALVE_ON    = 0u,
    NC_VALVE_OFF   = 1u
};

typedef struct
{
    uint32 RESERVED     :4;
    uint32 STATUS       :2;
    uint32 RD_REQ       :1;
    uint32 WRT_REQ      :1;
    uint32 DATA         :16;
    uint32 ADD          :8;
}ACH_MsgConfigType;

enum
{
    Ach_TimeIndex_0ms = 0u,
    Ach_TimeIndex_1ms,
    Ach_TimeIndex_2ms,
    Ach_TimeIndex_3ms,
    Ach_TimeIndex_4ms,
    Ach_TimeIndex_5ms,
    Ach_TimeIndex_6ms,
    Ach_TimeIndex_7ms,
    Ach_TimeIndex_8ms,
    Ach_TimeIndex_9ms
};


typedef union
{
    uint32  frame;
    struct
    {
        uint32 CRC      :5;    
        uint32 DATA     :16;   
        uint32 FCNT     :1;    
        uint32 RSV      :1;    
        uint32 ADD      :8;    
        uint32 RW       :1;    
    
    }Tx_Field;

}ACH_TxFrameType;



typedef union
{
    uint32  frame;
    struct
    {
        uint32 CRC       :5;     
        uint32 DATA      :16;    
        uint32 ADD_FBK   :8;     
        uint32 IERR      :2;     
        uint32 SPI_ERR   :1;     
    
    }Rx_Field;

}ACH_RxFrameType;


typedef enum    /* ASIC WRITE MESSAGE SET */
{
    ACH_AsicInitMsgSet = 0u,
    ACH_ValveInitMsgSet,
    ACH_MotorInitMsgSet,
    ACH_VsoInitMsgSet,
    ACH_WssInitMsgSet,
    ACH_LampShlsInitMsgSet,
    ACH_WatchdogInitMsgSet,
    ACH_ValveNo0MsgSet,
    ACH_ValveNo1MsgSet,
    ACH_ValveNo2MsgSet,
    ACH_ValveNo3MsgSet,
    ACH_ValveTc0MsgSet,
    ACH_ValveTc1MsgSet,
    ACH_ValveEsv0MsgSet,
    ACH_ValveEsv1MsgSet,
    ACH_ValveNc0MsgSet,
    ACH_ValveNc1MsgSet,
    ACH_ValveNc2MsgSet,
    ACH_ValveNc3MsgSet,
    ACH_MotorDutyMsgSet,
    ACH_MotorFreqMsgSet,
    ACH_ValveRelayMsgSet,
    ACH_WatchdogAnswerSet,
    ACH_WatchdogSeedRequest,
    ACH_ServiceTestEnableSet,
    ACH_AdcSelectSet,
    ACH_WssCountSet,
    ACH_KeepAliveSet,
    ACH_VsoDriveSet,
    ACH_WldDriveSet,
    ACH_ShlsDriveSet,
    
    ACH_WriteMsgSetMax
}ACH_WriteMsgSetType;



typedef enum    /* ASIC MESSAGE ID */
{
    ACH_MSG8_ID_PUMPCFG = 0u,              /* NOTICE : MSG8,1,64,65 are sent first because they are actuator related */
    ACH_MSG1_ID_GENCFG,            
    ACH_MSG64_ID_SOLSERVENA,        
    ACH_MSG65_ID_SOLENDR,           
    ACH_MSG64_ID_SOLSERVENA_2,             /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */ 
    ACH_MSG0_ID_CHIPID,             
    ACH_MSG2_ID_GENCFG2,            
    ACH_MSG3_ID_GENSTATUS,          
    ACH_MSG4_ID_TEMPSENS,           
    ACH_MSG5_ID_COMM_FAULTS,        
    ACH_MSG6_ID_SERVTESTEN,         
    ACH_MSG7_ID_SERVTESTSTATUS,     
    ACH_MSG9_ID_PUMPCFG2,           
    ACH_MSG10_ID_PUMPDRD,           
    ACH_MSG11_ID_VDDFLT,            
    ACH_MSG12_ID_VDDSTATUS,         
    ACH_MSG13_ID_ADC_DR,            
    ACH_MSG15_ID_VSOSER,            
    ACH_MSG16_ID_VSODR,             
    ACH_MSG17_ID_WLD1SHLS_CONFIG,   
    ACH_MSG18_ID_WLD1SHLSDR,        
    ACH_MSG19_ID_SERVFLTMSK,        
    ACH_MSG20_ID_RESOUTPUT,         
    ACH_MSG32_ID_WSITEST,           
    ACH_MSG33_ID_WSIAUXCONF,        
    ACH_MSG34_ID_WSIAUXCONF2,       
    ACH_MSG35_ID_WSIRSCR1,          
    ACH_MSG36_ID_WSIRSCR2,          
    ACH_MSG37_ID_WSIRSCR3,          
    ACH_MSG38_ID_WSIRSCR4,          
    ACH_MSG39_ID_WSICTRL,           
    ACH_MSG40_ID_WSCOUNT1,          
    ACH_MSG41_ID_WSCOUNT2,          
    ACH_MSG42_ID_WSCOUNT3,          
    ACH_MSG43_ID_WSCOUNT4,          
    ACH_MSG44_ID_WSBIST,            
    ACH_MSG48_ID_WSIRSDR1,          
    ACH_MSG49_ID_WSIRSDR1,          
    ACH_MSG50_ID_WSIRSDR1,          
    ACH_MSG51_ID_WSIRSDR1,          
    ACH_MSG52_ID_WSIRSDR2,          
    ACH_MSG53_ID_WSIRSDR2,          
    ACH_MSG54_ID_WSIRSDR2,          
    ACH_MSG55_ID_WSIRSDR2,          
    ACH_MSG56_ID_WSIRSDR3,          
    ACH_MSG57_ID_WSIRSDR3,          
    ACH_MSG58_ID_WSIRSDR3,          
    ACH_MSG59_ID_WSIRSDR3,          
    ACH_MSG60_ID_WSIRSDR4,          
    ACH_MSG61_ID_WSIRSDR4,         
    ACH_MSG62_ID_WSIRSDR4,          
    ACH_MSG63_ID_WSIRSDR4,          
    ACH_MSG67_ID_ONOFF_CH_CONFIG,   
    ACH_MSG68_ID_ONOFF_CH_1_DR,     
    ACH_MSG69_ID_ONOFF_CH_2_DR,     
    ACH_MSG70_ID_ONOFF_CH_3_DR,     
    ACH_MSG71_ID_ONOFF_CH_4_DR,     
    ACH_MSG72_ID_WDG2SEED,          
    ACH_MSG73_ID_WDG2STATUS,        
    ACH_MSG74_ID_WDG2STARTTMG,      
    ACH_MSG75_ID_WDG2DELTATMG,      
    ACH_MSG76_ID_WDG2TOUTTMG,       
    ACH_MSG77_ID_WDG2PGM,           
    ACH_MSG78_ID_WDG2ANS,           
    ACH_MSG90_ID_TEST_MODE_ENTER,   
    ACH_MSG128_NO0_0_CHX_FAULT,          
    ACH_MSG129_NO0_1_EXCEPTIONS_CHX,     
    ACH_MSG130_NO0_2_CONFIGURATION_CHX,  
    ACH_MSG133_NO0_5_SETPOINT_CHX,       
    ACH_MSG134_NO0_6_CTRLCFG_CHX,        
    ACH_MSG135_NO0_7_FMODULATION_CHX,    
    ACH_MSG136_NO0_8_KGAINS_CHX,         
    ACH_MSG137_NO0_9_OFFCMP_CHX,         
    ACH_MSG138_NO0_10_AVGCURR_CHX,        
    ACH_MSG140_NO0_12_PWMSENSE_CHX,       
    ACH_MSG141_NO0_13_BASE_DELTA_CURR,    
    ACH_MSG142_NO0_14_DELTA_CURR,         
    ACH_MSG144_NO1_0_CHX_FAULT,          
    ACH_MSG145_NO1_1_EXCEPTIONS_CHX,     
    ACH_MSG146_NO1_2_CONFIGURATION_CHX,  
    ACH_MSG149_NO1_5_SETPOINT_CHX,       
    ACH_MSG150_NO1_6_CTRLCFG_CHX,        
    ACH_MSG151_NO1_7_FMODULATION_CHX,    
    ACH_MSG152_NO1_8_KGAINS_CHX,         
    ACH_MSG153_NO1_9_OFFCMP_CHX,         
    ACH_MSG154_NO1_10_AVGCURR_CHX,        
    ACH_MSG156_NO1_12_PWMSENSE_CHX,       
    ACH_MSG157_NO1_13_BASE_DELTA_CURR,    
    ACH_MSG158_NO1_14_DELTA_CURR,         
    ACH_MSG160_NO2_0_CHX_FAULT,          
    ACH_MSG161_NO2_1_EXCEPTIONS_CHX,     
    ACH_MSG162_NO2_2_CONFIGURATION_CHX,  
    ACH_MSG165_NO2_5_SETPOINT_CHX,       
    ACH_MSG166_NO2_6_CTRLCFG_CHX,        
    ACH_MSG167_NO2_7_FMODULATION_CHX,    
    ACH_MSG168_NO2_8_KGAINS_CHX,         
    ACH_MSG169_NO2_9_OFFCMP_CHX,         
    ACH_MSG170_NO2_10_AVGCURR_CHX,        
    ACH_MSG172_NO2_12_PWMSENSE_CHX,       
    ACH_MSG173_NO2_13_BASE_DELTA_CURR,    
    ACH_MSG174_NO2_14_DELTA_CURR,         
    ACH_MSG176_NO3_0_CHX_FAULT,          
    ACH_MSG177_NO3_1_EXCEPTIONS_CHX,     
    ACH_MSG178_NO3_2_CONFIGURATION_CHX,  
    ACH_MSG181_NO3_5_SETPOINT_CHX,       
    ACH_MSG182_NO3_6_CTRLCFG_CHX,        
    ACH_MSG183_NO3_7_FMODULATION_CHX,    
    ACH_MSG184_NO3_8_KGAINS_CHX,         
    ACH_MSG185_NO3_9_OFFCMP_CHX,         
    ACH_MSG186_NO3_10_AVGCURR_CHX,        
    ACH_MSG188_NO3_12_PWMSENSE_CHX,       
    ACH_MSG189_NO3_13_BASE_DELTA_CURR,    
    ACH_MSG190_NO3_14_DELTA_CURR,         
    ACH_MSG192_TC0_0_CHX_FAULT,          
    ACH_MSG193_TC0_1_EXCEPTIONS_CHX,     
    ACH_MSG194_TC0_2_CONFIGURATION_CHX,  
    ACH_MSG197_TC0_5_SETPOINT_CHX,       
    ACH_MSG198_TC0_6_CTRLCFG_CHX,        
    ACH_MSG199_TC0_7_FMODULATION_CHX,    
    ACH_MSG200_TC0_8_KGAINS_CHX,         
    ACH_MSG201_TC0_9_OFFCMP_CHX,         
    ACH_MSG202_TC0_10_AVGCURR_CHX,        
    ACH_MSG204_TC0_12_PWMSENSE_CHX,       
    ACH_MSG205_TC0_13_BASE_DELTA_CURR,    
    ACH_MSG206_TC0_14_DELTA_CURR,         
    ACH_MSG208_TC1_0_CHX_FAULT,          
    ACH_MSG209_TC1_1_EXCEPTIONS_CHX,     
    ACH_MSG210_TC1_2_CONFIGURATION_CHX,  
    ACH_MSG213_TC1_5_SETPOINT_CHX,       
    ACH_MSG214_TC1_6_CTRLCFG_CHX,        
    ACH_MSG215_TC1_7_FMODULATION_CHX,    
    ACH_MSG216_TC1_8_KGAINS_CHX,         
    ACH_MSG217_TC1_9_OFFCMP_CHX,         
    ACH_MSG218_TC1_10_AVGCURR_CHX,        
    ACH_MSG220_TC1_12_PWMSENSE_CHX,      
    ACH_MSG221_TC1_13_BASE_DELTA_CURR,    
    ACH_MSG222_TC1_14_DELTA_CURR,         
    ACH_MSG225_ESV0_1_EXCEPTIONS_CHX,    
    ACH_MSG226_ESV0_2_CONFIGURATION_CHX, 
    ACH_MSG229_ESV0_5_SETPOINT_CHX,      
    ACH_MSG230_ESV0_6_CTRLCFG_CHX,       
    ACH_MSG232_ESV0_8_KGAINS_CHX,        
    ACH_MSG233_ESV0_9_OFFCMP_CHX,        
    ACH_MSG234_ESV0_10_AVGCURR_CHX,       
    ACH_MSG236_ESV0_12_PWMSENSE_CHX,      
    ACH_MSG241_ESV1_1_EXCEPTIONS_CHX,    
    ACH_MSG242_ESV1_2_CONFIGURATION_CHX, 
    ACH_MSG245_ESV1_5_SETPOINT_CHX,      
    ACH_MSG246_ESV1_6_CTRLCFG_CHX,       
    ACH_MSG248_ESV1_8_KGAINS_CHX,        
    ACH_MSG249_ESV1_9_OFFCMP_CHX,        
    ACH_MSG250_ESV1_10_AVGCURR_CHX,       
    ACH_MSG252_ESV1_12_PWMSENSE_CHX,      

    ACH_MSGID_MAX           /* Indicates the Number of Messages */

} ACH_MsgIdType;

typedef struct
{
	uint32 ACH_Tx1_FS_EN                      :1;
	uint32 ACH_Tx1_FS_CMD                     :1;
	uint32 ACH_Tx1_FS_VDS_TH                  :2;
	uint32 ACH_Tx1_PMP_EN                     :1;
	uint32 ACH_Tx1_OSC_SprSpect_DIS           :1;
	uint32 ACH_Tx1_VDD_4_OVC_SD_RESTART_TIME  :2;
	uint32 ACH_Tx1_VDD4_OVC_FLT_TIME          :2;
	uint32 ACH_Tx1_VDD_3_OVC_SD_RESTART_TIME  :2;
	uint32 ACH_Tx1_VDD3_OVC_FLT_TIME          :2;
    uint32 ACH_Tx_2_WD_SW_DIS                 :2;
	uint32 ACH_Tx2_VDD_2d5_AUTO_SWITCH_OFF    :1;
	uint32 ACH_Tx2_VDD1_DIODELOSS_FILT        :1;
    uint32 ACH_Tx_2_WD_DIS                    :2;
	uint32 ACH_Tx2_VDD5_DIS                   :1;
	uint32 ACH_Tx2_VDD2_DIS                   :1;
	uint32 ACH_Tx19_E_PVDS_GPO                :1;
	uint32 ACH_Tx19_E_VDD2d5dWSx_OT_GPO       :1;
	uint32 ACH_Tx19_E_FBO_GPO                 :1;
	uint32 ACH_Tx19_E_FSPI_GPO                :1;
	uint32 ACH_Tx19_E_EN_GPO                  :1;
	uint32 ACH_Tx19_E_FS_VDS_GPO              :1;
    uint32 ACH_Asic_InitData_Reserved         :4;
}ACH_AsicInitDataType;


typedef struct
{
	uint32 ACH_Tx64_SOLENOID_ENABLE_0         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_1         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_2         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_3         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_4         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_5         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_6         :1;
	uint32 ACH_Tx64_SOLENOID_ENABLE_7         :1;
    uint32 ACH_Tx65_CH0_STATUS_L              :1;
    uint32 ACH_Tx65_CH0_STATUS_H              :1;
    uint32 ACH_Tx65_CH1_STATUS_L              :1;
    uint32 ACH_Tx65_CH1_STATUS_H              :1;
    uint32 ACH_Tx65_CH2_STATUS_L              :1;
    uint32 ACH_Tx65_CH2_STATUS_H              :1;
    uint32 ACH_Tx65_CH3_STATUS_L              :1;
    uint32 ACH_Tx65_CH3_STATUS_H              :1;
    uint32 ACH_Tx65_CH4_STATUS_L              :1;
    uint32 ACH_Tx65_CH4_STATUS_H              :1;
    uint32 ACH_Tx65_CH5_STATUS_L              :1;
    uint32 ACH_Tx65_CH5_STATUS_H              :1;
    uint32 ACH_Tx65_CH6_STATUS_L              :1;
    uint32 ACH_Tx65_CH6_STATUS_H              :1;
    uint32 ACH_Tx65_CH7_STATUS_L              :1;
    uint32 ACH_Tx65_CH7_STATUS_H              :1;
	uint32 ACH_Tx67_CH1_EN                    :1;
	uint32 ACH_Tx67_CH2_EN                    :1;
	uint32 ACH_Tx67_CH3_EN                    :1;
	uint32 ACH_Tx67_CH4_EN                    :1;
	uint32 ACH_Tx67_CH1_DIAG_EN               :1;
	uint32 ACH_Tx67_CH2_DIAG_EN               :1;
	uint32 ACH_Tx67_CH3_DIAG_EN               :1;
	uint32 ACH_Tx67_CH4_DIAG_EN               :1;
	uint32 ACH_Tx67_CH1_TD_BLANK              :1;
	uint32 ACH_Tx67_CH2_TD_BLANK              :1;
	uint32 ACH_Tx67_CH3_TD_BLANK              :1;
	uint32 ACH_Tx67_CH4_TD_BLANK              :1;
	uint32 ACH_Tx67_CH1_CMD                   :1;
	uint32 ACH_Tx67_CH2_CMD                   :1;
	uint32 ACH_Tx67_CH3_CMD                   :1;
	uint32 ACH_Tx67_CH4_CMD                   :1;
	uint32 ACH_TxNO0_2_SR_SEL                 :2;
	uint32 ACH_TxNO0_2_CALIBRATION_DIS        :1;
	uint32 ACH_TxNO0_2_OFS_CHOP_DIS           :1;
	uint32 ACH_TxNO0_2_TD_BLANK_SEL           :1;
	uint32 ACH_TxNO0_2_LS_CLAMP_DIS           :1;
	uint32 ACH_TxNO0_2_LOGIC_BIST_EN          :1;
	uint32 ACH_TxNO0_2_DIAG_BIST_EN           :1;
	uint32 ACH_TxNO0_2_ADC_BIST_EN            :1;
	uint32 ACH_TxNO0_2_OFF_DIAG_EN            :1;
	uint32 ACH_TxNO0_2_E_SOL_GPO              :1;
	uint32 ACH_TxNO0_2_E_LSCLAMP_GPO          :1;
	uint32 ACH_TxNO0_6_PWM_CODE               :10;
	uint32 ACH_TxNO0_7_FREQ_MOD_STEP          :7;
	uint32 ACH_TxNO0_7_MAX_FREQ_DELTA         :8;
	uint32 ACH_TxNO0_8_KI                     :3;
	uint32 ACH_TxNO0_8_KP                     :3;
	uint32 ACH_TxNO0_9_INTTIME                :2;
	uint32 ACH_TxNO0_9_FILT_TIME              :2;
	uint32 ACH_TxNO0_9_CHOP_TIME              :1;
	uint32 ACH_TxNO0_13_BASE_DELTA_CURENT     :8;
	uint32 ACH_TxNO1_2_SR_SEL                 :2;
	uint32 ACH_TxNO1_2_CALIBRATION_DIS        :1;
	uint32 ACH_TxNO1_2_OFS_CHOP_DIS           :1;
	uint32 ACH_TxNO1_2_TD_BLANK_SEL           :1;
	uint32 ACH_TxNO1_2_LS_CLAMP_DIS           :1;
	uint32 ACH_TxNO1_2_LOGIC_BIST_EN          :1;
	uint32 ACH_TxNO1_2_DIAG_BIST_EN           :1;
	uint32 ACH_TxNO1_2_ADC_BIST_EN            :1;
	uint32 ACH_TxNO1_2_OFF_DIAG_EN            :1;
	uint32 ACH_TxNO1_2_E_SOL_GPO              :1;
	uint32 ACH_TxNO1_2_E_LSCLAMP_GPO          :1;
	uint32 ACH_TxNO1_6_PWM_CODE               :10;
	uint32 ACH_TxNO1_7_FREQ_MOD_STEP          :7;
	uint32 ACH_TxNO1_7_MAX_FREQ_DELTA         :8;
	uint32 ACH_TxNO1_8_KI                     :3;
	uint32 ACH_TxNO1_8_KP                     :3;
	uint32 ACH_TxNO1_9_INTTIME                :2;
	uint32 ACH_TxNO1_9_FILT_TIME              :2;
	uint32 ACH_TxNO1_9_CHOP_TIME              :1;
	uint32 ACH_TxNO1_13_BASE_DELTA_CURENT     :8;
	uint32 ACH_TxNO2_2_SR_SEL                 :2;
	uint32 ACH_TxNO2_2_CALIBRATION_DIS        :1;
	uint32 ACH_TxNO2_2_OFS_CHOP_DIS           :1;
	uint32 ACH_TxNO2_2_TD_BLANK_SEL           :1;
	uint32 ACH_TxNO2_2_LS_CLAMP_DIS           :1;
	uint32 ACH_TxNO2_2_LOGIC_BIST_EN          :1;
	uint32 ACH_TxNO2_2_DIAG_BIST_EN           :1;
	uint32 ACH_TxNO2_2_ADC_BIST_EN            :1;
	uint32 ACH_TxNO2_2_OFF_DIAG_EN            :1;
	uint32 ACH_TxNO2_2_E_SOL_GPO              :1;
	uint32 ACH_TxNO2_2_E_LSCLAMP_GPO          :1;
	uint32 ACH_TxNO2_6_PWM_CODE               :10;
	uint32 ACH_TxNO2_7_FREQ_MOD_STEP          :7;
	uint32 ACH_TxNO2_7_MAX_FREQ_DELTA         :8;
	uint32 ACH_TxNO2_8_KI                     :3;
	uint32 ACH_TxNO2_8_KP                     :3;
	uint32 ACH_TxNO2_9_INTTIME                :2;
	uint32 ACH_TxNO2_9_FILT_TIME              :2;
	uint32 ACH_TxNO2_9_CHOP_TIME              :1;
	uint32 ACH_TxNO2_13_BASE_DELTA_CURENT     :8;
	uint32 ACH_TxNO3_2_SR_SEL                 :2;
	uint32 ACH_TxNO3_2_CALIBRATION_DIS        :1;
	uint32 ACH_TxNO3_2_OFS_CHOP_DIS           :1;
	uint32 ACH_TxNO3_2_TD_BLANK_SEL           :1;
	uint32 ACH_TxNO3_2_LS_CLAMP_DIS           :1;
	uint32 ACH_TxNO3_2_LOGIC_BIST_EN          :1;
	uint32 ACH_TxNO3_2_DIAG_BIST_EN           :1;
	uint32 ACH_TxNO3_2_ADC_BIST_EN            :1;
	uint32 ACH_TxNO3_2_OFF_DIAG_EN            :1;
	uint32 ACH_TxNO3_2_E_SOL_GPO              :1;
	uint32 ACH_TxNO3_2_E_LSCLAMP_GPO          :1;
	uint32 ACH_TxNO3_6_PWM_CODE               :10;
	uint32 ACH_TxNO3_7_FREQ_MOD_STEP          :7;
	uint32 ACH_TxNO3_7_MAX_FREQ_DELTA         :8;
	uint32 ACH_TxNO3_8_KI                     :3;
	uint32 ACH_TxNO3_8_KP                     :3;
	uint32 ACH_TxNO3_9_INTTIME                :2;
	uint32 ACH_TxNO3_9_FILT_TIME              :2;
	uint32 ACH_TxNO3_9_CHOP_TIME              :1;
	uint32 ACH_TxNO3_13_BASE_DELTA_CURENT     :8;
	uint32 ACH_TxTC0_2_SR_SEL                 :2;
	uint32 ACH_TxTC0_2_CALIBRATION_DIS        :1;
	uint32 ACH_TxTC0_2_OFS_CHOP_DIS           :1;
	uint32 ACH_TxTC0_2_TD_BLANK_SEL           :1;
	uint32 ACH_TxTC0_2_LS_CLAMP_DIS           :1;
	uint32 ACH_TxTC0_2_LOGIC_BIST_EN          :1;
	uint32 ACH_TxTC0_2_DIAG_BIST_EN           :1;
	uint32 ACH_TxTC0_2_ADC_BIST_EN            :1;
	uint32 ACH_TxTC0_2_OFF_DIAG_EN            :1;
	uint32 ACH_TxTC0_2_E_SOL_GPO              :1;
	uint32 ACH_TxTC0_2_E_LSCLAMP_GPO          :1;
	uint32 ACH_TxTC0_6_PWM_CODE               :10;
	uint32 ACH_TxTC0_7_FREQ_MOD_STEP          :7;
	uint32 ACH_TxTC0_7_MAX_FREQ_DELTA         :8;
	uint32 ACH_TxTC0_8_KI                     :3;
	uint32 ACH_TxTC0_8_KP                     :3;
	uint32 ACH_TxTC0_9_INTTIME                :2;
	uint32 ACH_TxTC0_9_FILT_TIME              :2;
	uint32 ACH_TxTC0_9_CHOP_TIME              :1;
	uint32 ACH_TxTC0_13_BASE_DELTA_CURENT     :8;
	uint32 ACH_TxTC1_2_SR_SEL                 :2;
	uint32 ACH_TxTC1_2_CALIBRATION_DIS        :1;
	uint32 ACH_TxTC1_2_OFS_CHOP_DIS           :1;
	uint32 ACH_TxTC1_2_TD_BLANK_SEL           :1;
	uint32 ACH_TxTC1_2_LS_CLAMP_DIS           :1;
	uint32 ACH_TxTC1_2_LOGIC_BIST_EN          :1;
	uint32 ACH_TxTC1_2_DIAG_BIST_EN           :1;
	uint32 ACH_TxTC1_2_ADC_BIST_EN            :1;
	uint32 ACH_TxTC1_2_OFF_DIAG_EN            :1;
	uint32 ACH_TxTC1_2_E_SOL_GPO              :1;
	uint32 ACH_TxTC1_2_E_LSCLAMP_GPO          :1;
	uint32 ACH_TxTC1_6_PWM_CODE               :10;
	uint32 ACH_TxTC1_7_FREQ_MOD_STEP          :7;
	uint32 ACH_TxTC1_7_MAX_FREQ_DELTA         :8;
	uint32 ACH_TxTC1_8_KI                     :3;
	uint32 ACH_TxTC1_8_KP                     :3;
	uint32 ACH_TxTC1_9_INTTIME                :2;
	uint32 ACH_TxTC1_9_FILT_TIME              :2;
	uint32 ACH_TxTC1_9_CHOP_TIME              :1;
	uint32 ACH_TxTC1_13_BASE_DELTA_CURENT     :8;
	uint32 ACH_TxESV0_2_SR_SEL                :5;
	uint32 ACH_TxESV0_2_CALIBRATION_DIS       :1;
	uint32 ACH_TxESV0_2_OFS_CHOP_DIS          :1;
	uint32 ACH_TxESV0_2_TD_BLANK_SEL          :1;
	uint32 ACH_TxESV0_2_LS_CLAMP_DIS          :1;
	uint32 ACH_TxESV0_2_LOGIC_BIST_EN         :1;
	uint32 ACH_TxESV0_2_DIAG_BIST_EN          :1;
	uint32 ACH_TxESV0_2_ADC_BIST_EN           :1;
	uint32 ACH_TxESV0_2_OFF_DIAG_EN           :1;
	uint32 ACH_TxESV0_2_E_SOL_GPO             :1;
	uint32 ACH_TxESV0_2_E_LSCLAMP_GPO         :1;
	uint32 ACH_TxESV0_6_PWM_CODE              :6;
	uint32 ACH_TxESV0_6_FREQ_MOD_STEP         :4;
	uint32 ACH_TxESV0_6_MAX_FREQ_DELTA        :5;
	uint32 ACH_TxESV0_8_KI                    :3;
	uint32 ACH_TxESV0_8_KP                    :3;
	uint32 ACH_TxESV0_9_INTTIME               :2;
	uint32 ACH_TxESV0_9_FILT_TIME             :2;
	uint32 ACH_TxESV0_9_CHOP_TIME             :1;
	uint32 ACH_TxESV1_2_SR_SEL                :5;
	uint32 ACH_TxESV1_2_CALIBRATION_DIS       :1;
	uint32 ACH_TxESV1_2_OFS_CHOP_DIS          :1;
	uint32 ACH_TxESV1_2_TD_BLANK_SEL          :1;
	uint32 ACH_TxESV1_2_LS_CLAMP_DIS          :1;
	uint32 ACH_TxESV1_2_LOGIC_BIST_EN         :1;
	uint32 ACH_TxESV1_2_DIAG_BIST_EN          :1;
	uint32 ACH_TxESV1_2_ADC_BIST_EN           :1;
	uint32 ACH_TxESV1_2_OFF_DIAG_EN           :1;
	uint32 ACH_TxESV1_2_E_SOL_GPO             :1;
	uint32 ACH_TxESV1_2_E_LSCLAMP_GPO         :1;
	uint32 ACH_TxESV1_6_PWM_CODE              :6;
	uint32 ACH_TxESV1_6_FREQ_MOD_STEP         :4;
	uint32 ACH_TxESV1_6_MAX_FREQ_DELTA        :5;
	uint32 ACH_TxESV1_8_KI                    :3;
	uint32 ACH_TxESV1_8_KP                    :3;
	uint32 ACH_TxESV1_9_INTTIME               :2;
	uint32 ACH_TxESV1_9_FILT_TIME             :2;
	uint32 ACH_TxESV1_9_CHOP_TIME             :1;
    uint32 ACH_ValveInit_Reserved             :22;
}ACH_ValveInitDataType;

typedef struct
{
    uint32 ACH_Tx8_PMP_TCK_PWM                :8;
    uint32 ACH_Tx9_PMP_VDS_TH                 :2;
    uint32 ACH_Tx9_PMP2_DIS                   :1;
    uint32 ACH_Tx9_PMP_VDS_FIL                :3;
    uint32 ACH_Tx9_LDACT_DIS                  :1;
    uint32 ACH_Tx9_PMP1_ISINC                 :1;
    uint32 ACH_Tx9_PMP_DT                     :4;
    uint32 ACH_Tx9_PMP_TEST                   :1;
    uint32 ACH_Tx9_PMP_BIST_EN                :1;
    uint32 ACH_MotorInit_Reserved             :10;
}ACH_MotorInitDataType;

typedef struct
{
	uint32 ACH_Tx15_VSO_CONF                  :1;
	uint32 ACH_Tx15_WSO_VSO_S                 :2;
	uint32 ACH_Tx15_SSIG_VSO                  :1;
	uint32 ACH_Tx15_VSO_PTEN                  :1;
	uint32 ACH_Tx15_VSO_OFF_DIAG_EN           :1;
	uint32 ACH_Tx15_VSO_LS_CLAMP_DIS          :1;
	uint32 ACH_Tx15_SEL_CONF                  :1;
	uint32 ACH_Tx15_WSO_SEL_S                 :2;
	uint32 ACH_Tx15_SSIG_SEL                  :1;
	uint32 ACH_Tx15_SEL_OUT_CMD               :1;
	uint32 ACH_Tx15_SEL_OFF_DIAG_EN           :1;
	uint32 ACH_Tx15_SEL_LS_CLAMP_DIS          :1;
	uint32 ACH_Tx15_SEL_PTEN                  :1;
    uint32 ACH_VsoInit_Reserved               :17;
}ACH_VsoInitDataType;

typedef struct
{
	uint32 ACH_Tx32_CONFIG_RANGE              :7;
	uint32 ACH_Tx32_WSO_TEST                  :1;
	uint32 ACH_Tx33_WSI_FIRST_THRESHOLD       :8;
	uint32 ACH_Tx34_WSI_OFFSET_THRESHOLD      :8;
	uint32 ACH_Tx35_FILTER_SELECTION          :2;
	uint32 ACH_Tx35_PTEN                      :1;
	uint32 ACH_Tx35_SS_DIS                    :1;
	uint32 ACH_Tx35_FIX_TH                    :1;
	uint32 ACH_Tx35_SENSOR_TYPE_SELECTION     :2;
	uint32 ACH_Tx36_FILTER_SELECTION          :2;
	uint32 ACH_Tx36_PTEN                      :1;
	uint32 ACH_Tx36_SS_DIS                    :1;
	uint32 ACH_Tx36_FIX_TH                    :1;
	uint32 ACH_Tx36_SENSOR_TYPE_SELECTION     :2;
	uint32 ACH_Tx37_FILTER_SELECTION          :2;
	uint32 ACH_Tx37_PTEN                      :1;
	uint32 ACH_Tx37_SS_DIS                    :1;
	uint32 ACH_Tx37_FIX_TH                    :1;
	uint32 ACH_Tx37_SENSOR_TYPE_SELECTION     :2;
	uint32 ACH_Tx38_FILTER_SELECTION          :2;     
	uint32 ACH_Tx38_PTEN                      :1;
	uint32 ACH_Tx38_SS_DIS                    :1;
	uint32 ACH_Tx38_FIX_TH                    :1;
	uint32 ACH_Tx38_SENSOR_TYPE_SELECTION     :2;
	uint32 ACH_Tx39_FVPWR_ACT                 :1;
	uint32 ACH_Tx39_WSI_VDD4_UV               :1;
	uint32 ACH_Tx39_INIT                      :1;
	uint32 ACH_Tx39_DIAG                      :1;
	uint32 ACH_Tx39_CH4_EN                    :1;
	uint32 ACH_Tx39_CH3_EN                    :1;
	uint32 ACH_Tx39_CH2_EN                    :1;
	uint32 ACH_Tx39_CH1_EN                    :1;
	uint32 ACH_Tx44_WSI1_LBIST_EN             :1;
	uint32 ACH_Tx44_WSI2_LBIST_EN             :1;
	uint32 ACH_Tx44_WSI3_LBIST_EN             :1;
	uint32 ACH_Tx44_WSI4_LBIST_EN             :1;
}ACH_WssInitDataType;

typedef struct
{
	uint32 ACH_Tx17_WLD_CONF                  :1;
	uint32 ACH_Tx17_WLD_CMD                   :1;
	uint32 ACH_Tx17_WLD_DIAGOFF_EN            :1;
	uint32 ACH_Tx17_WLD_LS_CLAMP_DIS          :1;
	uint32 ACH_Tx17_SHLS_CMD                  :1;
	uint32 ACH_Tx17_SHLS_DIAGOFF_EN           :1;
	uint32 ACH_Tx17_SHLS_CONFIG               :1;
	uint32 ACH_Tx17_SHLS_LS_CLAMP_DIS         :1;
    uint32 ACH_LampShlsInit_Reserved          :24;
}ACH_LampShlsInitDataType;

typedef struct
{
	uint32 ACH_Tx2_WD_SW_DIS                  :2;
	uint32 ACH_Tx2_WD_DIS                     :1;
	uint32 ACH_Tx74_VALID_REQUEST_START       :8;
	uint32 ACH_Tx74_VALID_ANSWER_START        :8;
	uint32 ACH_Tx75_VALID_ANSWER_DELTA        :6;
	uint32 ACH_Tx75_VALID_REQUEST_DELTA       :6;
	uint32 ACH_Tx75_REQ_CHECK_ENABLE          :1;
	uint32 ACH_Tx76_ANSWER_TIME_OUT_DELTA     :6;
	uint32 ACH_Tx76_REQUEST_TIME_OUT_DELTA    :6;
	uint32 ACH_Tx76_TO_RESET_ENABLE           :1;
	uint32 ACH_Tx77_NUM_OF_GOOD_STEPS         :3;
	uint32 ACH_Tx77_NUM_OF_BAD_STEPS          :3;
	uint32 ACH_Tx77_HIGH_TH                   :4;
	uint32 ACH_Tx77_LOW_TH                    :4;
	uint32 ACH_Tx77_CLOCK_DIVISION            :1;
	uint32 ACH_Tx77_RESET_ENABLE              :1;
    uint32 ACH_WatchdogInit_Reserved          :3;
}ACH_WatchdogInitDataType;

typedef struct
{
	uint32 ACH_Tx78_ANSWER_LOW                :8;
	uint32 ACH_Tx78_ANSWER_HIGH               :8;
    uint32 ACH_WdAnswer_Reserved              :16;
}ACH_WdAnswerDataType;

typedef struct
{
	uint32 ACH_TxValve_SET_POINT              :11;
	uint32 ACH_TxValve_Reserved               :21;
}ACH_ValveDataType;

typedef struct
{
	uint32 ACH_Tx8_PUMP_DTY_PWM               :8;
	uint32 ACH_Tx_MotorDuty_Reserved          :24;
}ACH_MotorDutyDataType;

typedef struct
{
	uint32 ACH_Tx13_ADC_SEL                   :4;
	uint32 ACH_Tx_ADC_Selelct_Reserved        :28;
}ACH_AdcSelectDataType;

typedef struct
{
	uint32 ACH_Tx1_PHOLD                      :1;
	uint32 ACH_Tx1_KA                         :1;
}ACH_KeepAliveDataType;

typedef struct
{
	uint32 ACH_Tx8_PUMP_TCK_PWM               :8;
	uint32 ACH_Tx_MotorFreq_Reserved          :24;
}ACH_MotorFreqDataType;

typedef struct
{
	uint32 ACH_Tx6_NVM_DWN                    :1;
	uint32 ACH_Tx6_NVM_CRC_MASK               :1;
	uint32 ACH_Tx6_CORE_BIST_EN               :1;
	uint32 ACH_Tx6_ANALOG_BIST_EN             :1;
    uint32 ACH_ServiceTestEnable_Reserved     :28;
}ACH_ServiceTestEnableDataType;

typedef struct
{
	uint32 ACH_Tx1_FS_CMD                     :1;
	uint32 ACH_Tx_ValveRelay_Reserved         :31;
}ACH_ValveRelayDataType;

typedef struct
{
	uint32 ACH_Tx40_WS_CNT_EN                 :1;
	uint32 ACH_Tx40_WS_CNT_RST                :1;
	uint32 ACH_Tx41_WS_CNT_EN                 :1;
	uint32 ACH_Tx41_WS_CNT_RST                :1;
	uint32 ACH_Tx42_WS_CNT_EN                 :1;
	uint32 ACH_Tx42_WS_CNT_RST                :1;
	uint32 ACH_Tx43_WS_CNT_EN                 :1;
	uint32 ACH_Tx43_WS_CNT_RST                :1;
    uint32 ACH_WssCount_Reserved              :24;
}ACH_WssCountDataType;

typedef struct
{
	uint32 ACH_Tx_15_SEL_OUT_CMD              :1;
	uint32 ACH_Tx_VsoDrive_Reserved           :31;
}ACH_VsoDriveDataType;




/************************************* Rx ******************************************/
typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_0_CRC          :5;
        uint32    mu16_Rx_0_CHIP_ID     :16;
        uint32    mu8_Rx_0_ADD_FBK      :8;
        uint32    mu2_Rx_0_IERR         :2;
        uint32    mu1_Rx_0_SPI_ERR      :1;
    }B;
}ASIC_RX_MSG_0_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_1_CRC                          :5;   
        uint32    mu2_Rx_1_VDD3_OVC_FLT_TIME            :2;
        uint32    mu2_Rx_1_VDD_3_OVC_SD_RESTART_TIME    :2;
        uint32    mu2_Rx_1_VDD4_OVC_FLT_TIME            :2;
        uint32    mu2_Rx_1_VDD_4_OVC_SD_RESTART_TIME    :2;
        uint32    mu1_Rx_1_OSC_SpreadSpectrum_DIS       :1;
        uint32    mu1_Rx_1_PMP_EN                       :1;
        uint32    mu1_Rx_1_FS_EN                        :1;
        uint32    mu1_Rx_1_FS_CMD                       :1;
        uint32    mu2_Rx_1_FS_VDS_TH                    :2;
        uint32    mu1_Rx_1_PHOLD                        :1;
        uint32    mu1_Rx_1_KA                           :1;
        uint32    mu8_Rx_1_ADD_FBK                      :8;
        uint32    mu2_Rx_1_IERR                         :2;
        uint32    mu1_Rx_1_SPI_ERR                      :1;
    }B;
}ASIC_RX_MSG_1_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_2_CRC                          :5;   
        uint32    mu1_Rx_2_VDD2_DIS                     :1;
        uint32    mu1_Rx_2_VDD5_DIS                     :1;
        uint32    mu1_Rx_2_WD_DIS                       :1;
        uint32    mu1_Rx_2_VDD1_DIODELOSS_FILT          :1;
        uint32    mu1_Rx_2_VDD_2d5_AUTO_SWITCH_OFF      :1;
        uint32    mu2_Rx_2_WD_SW_DIS                    :2;
        uint32    mu9_Rx_2_RESERVED                     :9;
        uint32    mu8_Rx_2_ADD_FBK                      :8;
        uint32    mu2_Rx_2_IERR                         :2;
        uint32    mu1_Rx_2_SPI_ERR                      :1;
    }B;
}ASIC_RX_MSG_2_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_3_CRC                       :5;   
        uint32    mu1_Rx_3_OSC_STUCK_MON             :1;
        uint32    mu1_Rx_3_OSC_FRQ_MON               :1;
        uint32    mu1_Rx_3_OSC_SELF_TEST_RESULT      :1;
        uint32    mu1_Rx_3_T_SD_INT                  :1;
        uint32    mu1_Rx_3_WDOG_TO                   :1;
        uint32    mu1_Rx_3_CP_OV                     :1;
        uint32    mu1_Rx_3_CP_UV                     :1;
        uint32    mu1_Rx_3_FS_TURN_OFF               :1;
        uint32    mu1_Rx_3_FS_TURN_ON                :1;
        uint32    mu1_Rx_3_FS_VDS_FAULT              :1;
        uint32    mu1_Rx_3_FS_RVP_FAULT              :1;
        uint32    mu1_Rx_3_VHD_OV                    :1;
        uint32    mu1_Rx_3_VPWR_OV                   :1;
        uint32    mu1_Rx_3_VPWR_UV                   :1;
        uint32    mu2_Rx_3_RESERVED                  :2;
        uint32    mu8_Rx_3_ADD_FBK                   :8;
        uint32    mu2_Rx_3_IERR                      :2;
        uint32    mu1_Rx_3_SPI_ERR                   :1;
    }B;
}ASIC_RX_MSG_3_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_4_CRC                  :5;
        uint32    mu8_Rx_4_DIE_TEMP_MONITOR     :8;
        uint32    mu8_Rx_4_RESERVED             :8;
        uint32    mu8_Rx_4_ADD_FBK              :8;
        uint32    mu2_Rx_4_IERR                 :2;
        uint32    mu1_Rx_4_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_4_t;




typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_5_CRC          :5;
        uint32    mu1_Rx_5_TOO_LONG     :1;
        uint32    mu1_Rx_5_TOO_SHORT    :1;
        uint32    mu1_Rx_5_WRONG_CRC    :1;
        uint32    mu1_Rx_5_WRONG_FCNT   :1;
        uint32    mu1_Rx_5_WRONG_ADD    :1;
        uint32    mu11_Rx_5_RESERVED    :11;
        uint32    mu8_Rx_5_ADD_FBK      :8;
        uint32    mu2_Rx_5_IERR         :2;
        uint32    mu1_Rx_5_SPI_ERR      :1;
    }B;
}ASIC_RX_MSG_5_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_6_CRC               :5;
        uint32    mu1_Rx_6_ANALOG_BIST_EN    :1;
        uint32    mu1_Rx_6_CORE_BIST_EN      :1;
        uint32    mu2_Rx_6_RESERVED_0        :2;
        uint32    mu1_Rx_6_NVM_CRC_MASK      :1;
        uint32    mu1_Rx_6_RESERVED_1        :1;
        uint32    mu1_Rx_6_NVM_DWN           :1;
        uint32    mu9_Rx_6_RESERVED_2        :9;
        uint32    mu8_Rx_6_ADD_FBK           :8;
        uint32    mu2_Rx_6_IERR              :2;
        uint32    mu1_Rx_6_SPI_ERR           :1;
    }B;
}ASIC_RX_MSG_6_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_7_CRC                 :5;
        uint32    mu4_Rx_7_RESERVED0           :4;
        uint32    mu1_Rx_7_AN_TRIM_CRC_RESULT  :1;
        uint32    mu1_Rx_7_NVM_CRC_RESULT      :1;
        uint32    mu1_Rx_7_NVM_BUSY            :1;
        uint32    mu9_Rx_7_RESERVED1           :9;
        uint32    mu8_Rx_7_ADD_FBK             :8;
        uint32    mu2_Rx_7_IERR                :2;
        uint32    mu1_Rx_7_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_7_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_8_CRC                 :5;
        uint32    mu8_Rx_8_PUMP_DTY_PWM        :8;
        uint32    mu8_Rx_8_PUMP_TCK_PWM        :8;
        uint32    mu8_Rx_8_ADD_FBK             :8;
        uint32    mu2_Rx_8_IERR                :2;
        uint32    mu1_Rx_8_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_8_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_9_CRC                 :5;
        uint32    mu2_Rx_9_PMP_VDS_TH          :2;
        uint32    mu1_Rx_9_PMP2_DIS            :1;
        uint32    mu3_Rx_9_PMP_VDS_FIL         :3;
        uint32    mu1_Rx_9_LDACT_DIS           :1;
        uint32    mu1_Rx_9_PMP1_ISINC          :1;
        uint32    mu4_Rx_9_PMP_DT              :4;
        uint32    mu1_Rx_9_PMP_TEST            :1;
        uint32    mu2_Rx_9_RESERVED            :2;
        uint32    mu1_Rx_9_PMP_BIST_EN         :1;
        uint32    mu8_Rx_9_ADD_FBK             :8;
        uint32    mu2_Rx_9_IERR                :2;
        uint32    mu1_Rx_9_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_9_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_10_CRC                 :5;
        uint32    mu1_Rx_10_PMP1_TURN_ON        :1;
        uint32    mu1_Rx_10_PMP1_TURN_OFF       :1;
        uint32    mu1_Rx_10_PMP2_TURN_ON        :1;
        uint32    mu1_Rx_10_PMP2_TURN_OFF       :1;
        uint32    mu1_Rx_10_PMP3_TURN_ON        :1;
        uint32    mu1_Rx_10_PMP3_TURN_OFF       :1;
        uint32    mu1_Rx_10_PMP_VDS_TURNOFF     :1;
        uint32    mu1_Rx_10_PMP1_VDS_FAULT      :1;
        uint32    mu1_Rx_10_PMP_FLYBACK         :1;
        uint32    mu1_Rx_10_PMP_LD_ACT          :1;
        uint32    mu3_Rx_10_PMP_TEST_STATUS     :3;
        uint32    mu2_Rx_10_PMP_BIST_STATUS     :2;
        uint32    mu1_Rx_10_RESERVED            :1;
        uint32    mu8_Rx_10_ADD_FBK             :8;
        uint32    mu2_Rx_10_IERR                :2;
        uint32    mu1_Rx_10_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_10_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_11_CRC                 :5;
        uint32    mu1_Rx_11_VDD1_OVC            :1;
        uint32    mu1_Rx_11_VDD2_OUT_OF_REG     :1;
        uint32    mu1_Rx_11_VDD4_OVC            :1;
        uint32    mu1_Rx_11_VDD3_OVC            :1;
        uint32    mu1_Rx_11_VDD1DIODE_LOSS_ECHO :1;
        uint32    mu1_Rx_11_VDD2_REV_CURR       :1;
        uint32    mu1_Rx_11_VDD1_T_SD           :1;
        uint32    mu1_Rx_11_VDD2_T_SD           :1;
        uint32    mu1_Rx_11_VDD1_UV             :1;
        uint32    mu1_Rx_11_VDD2_UV             :1;
        uint32    mu1_Rx_11_VDD3_UV             :1;
        uint32    mu1_Rx_11_VDD4_UV             :1;
        uint32    mu1_Rx_11_VDD1_OV             :1;
        uint32    mu1_Rx_11_VDD2_OV             :1;
        uint32    mu1_Rx_11_VDD3_OV             :1;
        uint32    mu1_Rx_11_VDD4_OV             :1;
        uint32    mu8_Rx_11_ADD_FBK             :8;
        uint32    mu2_Rx_11_IERR                :2;
        uint32    mu1_Rx_11_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_11_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_12_CRC                 :5;
        uint32    mu1_Rx_12_VDD1_SEL_ECHO       :1;
        uint32    mu1_Rx_12_VDD2_SEL_ECHO       :1;
        uint32    mu2_Rx_12_RESERVED0           :2;
        uint32    mu1_Rx_12_VDD5_SEL_ECHO       :1;
        uint32    mu7_Rx_12_RESERVED1           :7;
        uint32    mu1_Rx_12_VDD5_OUT_OF_REG     :1;
        uint32    mu1_Rx_12_VDD5_T_SD           :1;
        uint32    mu1_Rx_12_VDD5_UV             :1;
        uint32    mu1_Rx_12_VDD5_OV             :1;
        uint32    mu8_Rx_12_ADD_FBK             :8;
        uint32    mu2_Rx_12_IERR                :2;
        uint32    mu1_Rx_12_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_12_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_13_CRC                 :5;
        uint32    mu10_Rx_13_ADC_DATA_OUT       :10;
        uint32    mu4_Rx_13_ADC_SEL             :4;
        uint32    mu1_Rx_13_RESERVED            :1;
        uint32    mu1_Rx_13_ADC_BUSY            :1;
        uint32    mu8_Rx_13_ADD_FBK             :8;
        uint32    mu2_Rx_13_IERR                :2;
        uint32    mu1_Rx_13_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_13_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_15_CRC                 :5;
        uint32    mu1_Rx_15_VSO_CONF            :1;
        uint32    mu2_Rx_15_WSO_VSO_S           :2;
        uint32    mu1_Rx_15_SSIG_VSO            :1;
        uint32    mu1_Rx_15_VSO_PTEN            :1;
        uint32    mu1_Rx_15_VSO_OFF_DIAG_EN     :1;
        uint32    mu1_Rx_15_VSO_LS_CLAMP_DIS    :1;
        uint32    mu1_Rx_15_RESERVED            :1;
        uint32    mu1_Rx_15_SEL_CONF            :1;
        uint32    mu2_Rx_15_WSO_SEL_S           :2;
        uint32    mu1_Rx_15_SSIG_SEL            :1;
        uint32    mu1_Rx_15_SEL_OUT_CMD         :1;
        uint32    mu1_Rx_15_SEL_OFF_DIAG_EN     :1;
        uint32    mu1_Rx_15_SEL_LS_CLAMP_DIS    :1;
        uint32    mu1_Rx_15_SEL_PTEN            :1;
        uint32    mu8_Rx_15_ADD_FBK             :8;
        uint32    mu2_Rx_15_IERR                :2;
        uint32    mu1_Rx_15_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_15_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_16_CRC                 :5;
        uint32    mu1_Rx_16_RESERVED0           :1;
        uint32    mu1_Rx_16_VSO_T_SD            :1;
        uint32    mu1_Rx_16_VSO_OPENLOAD        :1;
        uint32    mu1_Rx_16_VSO_LVT             :1;
        uint32    mu1_Rx_16_VSO_VGS_LS_FAULT    :1;
        uint32    mu1_Rx_16_VSO_LS_CLAMP_ACT    :1;
        uint32    mu1_Rx_16_VSO_LS_OVC          :1;
        uint32    mu1_Rx_16_GND_LOSS            :1;
        uint32    mu1_Rx_16_RESERVED1           :1;
        uint32    mu1_Rx_16_SEL_T_SD            :1;
        uint32    mu1_Rx_16_SEL_OPENLOAD        :1;
        uint32    mu1_Rx_16_SEL_LVT             :1;
        uint32    mu1_Rx_16_SEL_VGS_LS_FAULT    :1;
        uint32    mu1_Rx_16_SEL_LS_CLAMP_ACT    :1;
        uint32    mu1_Rx_16_SEL_LS_OVC          :1;
        uint32    mu1_Rx_16_RESERVED2           :1;
        uint32    mu8_Rx_16_ADD_FBK             :8;
        uint32    mu2_Rx_16_IERR                :2;
        uint32    mu1_Rx_16_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_16_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_17_CRC                 :5;
        uint32    mu1_Rx_17_WLD_CONF            :1;
        uint32    mu1_Rx_17_WLD_CMD             :1;
        uint32    mu1_Rx_17_WLD_DIAGOFF_EN      :1;
        uint32    mu1_Rx_17_WLD_LS_CLAMP_DIS    :1;
        uint32    mu4_Rx_17_RESERVED0           :4;
        uint32    mu1_Rx_17_SHLS_CMD            :1;
        uint32    mu1_Rx_17_SHLS_DIAGOFF_EN     :1;
        uint32    mu1_Rx_17_SHLS_CONFIG         :1;
        uint32    mu1_Rx_17_SHLS_LS_CLAMP_DIS   :1;
        uint32    mu4_Rx_17_RESERVED1           :4;
        uint32    mu8_Rx_17_ADD_FBK             :8;
        uint32    mu2_Rx_17_IERR                :2;
        uint32    mu1_Rx_17_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_17_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_18_CRC                 :5;
        uint32    mu1_Rx_18_RESERVED0           :1;
        uint32    mu1_Rx_18_WLD_T_SD            :1;
        uint32    mu1_Rx_18_WLD_OPL             :1;
        uint32    mu1_Rx_18_WLD_LVT             :1;
        uint32    mu1_Rx_18_WLD_LS_OVC          :1;
        uint32    mu1_Rx_18_WLD_VGS_LS_FAULT    :1;
        uint32    mu1_Rx_18_WLD_LS_CLAMP        :1;
        uint32    mu2_Rx_18_RESERVED1           :2;
        uint32    mu1_Rx_18_SHLS_T_SD           :1;
        uint32    mu1_Rx_18_SHLS_OPL            :1;
        uint32    mu1_Rx_18_SHLS_LVT            :1;
        uint32    mu1_Rx_18_SHLS_OVC            :1;
        uint32    mu1_Rx_18_SHLS_VGS_FAULT      :1;
        uint32    mu1_Rx_18_SHLS_LS_CLAMP       :1;
        uint32    mu1_Rx_18_RESERVED2           :1;
        uint32    mu8_Rx_18_ADD_FBK             :8;
        uint32    mu2_Rx_18_IERR                :2;
        uint32    mu1_Rx_18_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_18_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_19_CRC                 :5;
        uint32    mu1_Rx_19_E_PVDS_GPO          :1;
        uint32    mu1_Rx_19_E_VDD2d5dWSx_OT_GPO :1;
        uint32    mu1_Rx_19_E_FBO_GPO           :1;
        uint32    mu1_Rx_19_E_FSPI_GPO          :1;
        uint32    mu1_Rx_19_E_EN_GPO            :1;
        uint32    mu1_Rx_19_E_FS_VDS_GPO        :1;
        uint32    mu10_Rx_19_RESERVED           :10;
        uint32    mu8_Rx_19_ADD_FBK             :8;
        uint32    mu2_Rx_19_IERR                :2;
        uint32    mu1_Rx_19_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_19_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_20_CRC                 :5;
        uint32    mu1_Rx_20_RESERVED0           :1;
        uint32    mu1_Rx_20_VINTA_OV            :1;
        uint32    mu1_Rx_20_DGNDLOSS            :1;
        uint32    mu1_Rx_20_VINTA_UV            :1;
        uint32    mu1_Rx_20_PORN                :1;
        uint32    mu1_Rx_20_RES1_ECHO           :1;
        uint32    mu1_Rx_20_IGN_ECHO            :1;
        uint32    mu9_Rx_20_RESERVED1           :9;
        uint32    mu8_Rx_20_ADD_FBK             :8;
        uint32    mu2_Rx_20_IERR                :2;
        uint32    mu1_Rx_20_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_20_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_32_CRC                 :5;
        uint32    mu1_Rx_32_WSO_TEST            :1;
        uint32    mu7_Rx_32_CONFIG_RANGE        :7;
        uint32    mu8_Rx_32_RESERVED            :8;
        uint32    mu8_Rx_32_ADD_FBK             :8;
        uint32    mu2_Rx_32_IERR                :2;
        uint32    mu1_Rx_32_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_32_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_33_CRC                 :5;
        uint32    mu8_Rx_33_WSI_FIRST_THRESHOLD :8;
        uint32    mu8_Rx_33_RESERVED            :8;
        uint32    mu8_Rx_33_ADD_FBK             :8;
        uint32    mu2_Rx_33_IERR                :2;
        uint32    mu1_Rx_33_SPI_ERR             :1;
    }B;
}ASIC_RX_MSG_33_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_34_CRC                  :5;
        uint32    mu8_Rx_34_WSI_OFFSET_THRESHOLD :8;
        uint32    mu8_Rx_34_RESERVED             :8;
        uint32    mu8_Rx_34_ADD_FBK              :8;
        uint32    mu2_Rx_34_IERR                 :2;
        uint32    mu1_Rx_34_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_34_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_35_CRC                      :5;
        uint32    mu2_Rx_35_SENSOR_TYPE_SELECTION    :2;
        uint32    mu1_Rx_35_RESERVED0                :1;
        uint32    mu1_Rx_35_FIX_TH                   :1;
        uint32    mu1_Rx_35_SS_DIS                   :1;
        uint32    mu1_Rx_35_PTEN                     :1;
        uint32    mu2_Rx_35_FILTER_SELECTION         :2;
        uint32    mu8_Rx_35_RESEVED1                 :8;
        uint32    mu8_Rx_35_ADD_FBK                  :8;
        uint32    mu2_Rx_35_IERR                     :2;
        uint32    mu1_Rx_35_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_35_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_36_CRC                      :5;
        uint32    mu2_Rx_36_SENSOR_TYPE_SELECTION    :2;
        uint32    mu1_Rx_36_RESERVED0                :1;
        uint32    mu1_Rx_36_FIX_TH                   :1;
        uint32    mu1_Rx_36_SS_DIS                   :1;
        uint32    mu1_Rx_36_PTEN                     :1;
        uint32    mu2_Rx_36_FILTER_SELECTION         :2;
        uint32    mu8_Rx_36_RESEVED1                 :8;
        uint32    mu8_Rx_36_ADD_FBK                  :8;
        uint32    mu2_Rx_36_IERR                     :2;
        uint32    mu1_Rx_36_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_36_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_37_CRC                      :5;
        uint32    mu2_Rx_37_SENSOR_TYPE_SELECTION    :2;
        uint32    mu1_Rx_37_RESERVED0                :1;
        uint32    mu1_Rx_37_FIX_TH                   :1;
        uint32    mu1_Rx_37_SS_DIS                   :1;
        uint32    mu1_Rx_37_PTEN                     :1;
        uint32    mu2_Rx_37_FILTER_SELECTION         :2;
        uint32    mu8_Rx_37_RESEVED1                 :8;
        uint32    mu8_Rx_37_ADD_FBK                  :8;
        uint32    mu2_Rx_37_IERR                     :2;
        uint32    mu1_Rx_37_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_37_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_38_CRC                      :5;
        uint32    mu2_Rx_38_SENSOR_TYPE_SELECTION    :2;
        uint32    mu1_Rx_38_RESERVED0                :1;
        uint32    mu1_Rx_38_FIX_TH                   :1;
        uint32    mu1_Rx_38_SS_DIS                   :1;
        uint32    mu1_Rx_38_PTEN                     :1;
        uint32    mu2_Rx_38_FILTER_SELECTION         :2;
        uint32    mu8_Rx_38_RESEVED1                 :8;
        uint32    mu8_Rx_38_ADD_FBK                  :8;
        uint32    mu2_Rx_38_IERR                     :2;
        uint32    mu1_Rx_38_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_38_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_39_CRC                      :5;
        uint32    mu1_Rx_39_CH1_EN                   :1;
        uint32    mu1_Rx_39_CH2_EN                   :1;
        uint32    mu1_Rx_39_CH3_EN                   :1;
        uint32    mu1_Rx_39_CH4_EN                   :1;
        uint32    mu1_Rx_39_DIAG                     :1;
        uint32    mu1_Rx_39_INIT                     :1;
        uint32    mu1_Rx_39_WSI_VDD4_UV              :1;
        uint32    mu1_Rx_39_FVPWR_ACT                :1;
        uint32    mu8_Rx_39_RESERVED                 :8;
        uint32    mu8_Rx_39_ADD_FBK                  :8;
        uint32    mu2_Rx_39_IERR                     :2;
        uint32    mu1_Rx_39_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_39_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_40_CRC                      :5;
        uint32    mu8_Rx_40_WS1_CNT                  :8;
        uint32    mu1_Rx_40_WS_CNT_OV                :1;
        uint32    mu1_Rx_40_WS_CNT_EN                :1;
        uint32    mu1_Rx_40_WS_CNT_RST               :1;
        uint32    mu5_Rx_40_RESERVED                 :5;
        uint32    mu8_Rx_40_ADD_FBK                  :8;
        uint32    mu2_Rx_40_IERR                     :2;
        uint32    mu1_Rx_40_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_40_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_41_CRC                      :5;
        uint32    mu8_Rx_41_WS1_CNT                  :8;
        uint32    mu1_Rx_41_WS_CNT_OV                :1;
        uint32    mu1_Rx_41_WS_CNT_EN                :1;
        uint32    mu1_Rx_41_WS_CNT_RST               :1;
        uint32    mu5_Rx_41_RESERVED                 :5;
        uint32    mu8_Rx_41_ADD_FBK                  :8;
        uint32    mu2_Rx_41_IERR                     :2;
        uint32    mu1_Rx_41_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_41_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_42_CRC                      :5;
        uint32    mu8_Rx_42_WS1_CNT                  :8;
        uint32    mu1_Rx_42_WS_CNT_OV                :1;
        uint32    mu1_Rx_42_WS_CNT_EN                :1;
        uint32    mu1_Rx_42_WS_CNT_RST               :1;
        uint32    mu5_Rx_42_RESERVED                 :5;
        uint32    mu8_Rx_42_ADD_FBK                  :8;
        uint32    mu2_Rx_42_IERR                     :2;
        uint32    mu1_Rx_42_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_42_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_43_CRC                      :5;
        uint32    mu8_Rx_43_WS1_CNT                  :8;
        uint32    mu1_Rx_43_WS_CNT_OV                :1;
        uint32    mu1_Rx_43_WS_CNT_EN                :1;
        uint32    mu1_Rx_43_WS_CNT_RST               :1;
        uint32    mu5_Rx_43_RESERVED                 :5;
        uint32    mu8_Rx_43_ADD_FBK                  :8;
        uint32    mu2_Rx_43_IERR                     :2;
        uint32    mu1_Rx_43_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_43_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_44_CRC                      :5;
        uint32    mu2_Rx_44_WSI1_LBIST_STATUS        :2;
        uint32    mu2_Rx_44_WSI2_LBIST_STATUS        :2;
        uint32    mu2_Rx_44_WSI3_LBIST_STATUS        :2;
        uint32    mu2_Rx_44_WSI4_LBIST_STATUS        :2;
        uint32    mu1_Rx_44_WSI1_LBIST_EN            :1;
        uint32    mu1_Rx_44_WSI2_LBIST_EN            :1;
        uint32    mu1_Rx_44_WSI3_LBIST_EN            :1;
        uint32    mu1_Rx_44_WSI4_LBIST_EN            :1;
        uint32    mu1_Rx_44_WSI1_SELFTEST_RESULT     :1;
        uint32    mu1_Rx_44_WSI2_SELFTEST_RESULT     :1;
        uint32    mu1_Rx_44_WSI3_SELFTEST_RESULT     :1;
        uint32    mu1_Rx_44_WSI4_SELFTEST_RESULT     :1;
        uint32    mu8_Rx_44_ADD_FBK                  :8;
        uint32    mu2_Rx_44_IERR                     :2;
        uint32    mu1_Rx_44_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_44_t;

/* MSG 48~63 */
typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_WSIRSDR0_CRC                      :5;
        uint32    mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT   :10;
        uint32    mu6_Rx_WSIRSDR0_RESERVED                 :6;
        uint32    mu8_Rx_WSIRSDR0_ADD_FBK                  :8;
        uint32    mu2_Rx_WSIRSDR0_IERR                     :2;
        uint32    mu1_Rx_WSIRSDR0_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_WSIRSDR0_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_WSIRSDR1_CRC                      :5;
        uint32    mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT  :10;
        uint32    mu6_Rx_WSIRSDR1_RESERVED                 :6;
        uint32    mu8_Rx_WSIRSDR1_ADD_FBK                  :8;
        uint32    mu2_Rx_WSIRSDR1_IERR                     :2;
        uint32    mu1_Rx_WSIRSDR1_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_WSIRSDR1_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_WSIRSDR2_CRC                      :5;
        uint32    mu10_Rx_WSIRSDR2_BASE_CURRENT            :10;
        uint32    mu1_Rx_WSIRSDR2_RESERVED0                :1;
        uint32    mu2_Rx_WSIRSDR2_LOGIC_CH_ID              :2;
        uint32    mu3_Rx_WSIRSDR2_RESERVED1                :3;
        uint32    mu8_Rx_WSIRSDR2_ADD_FBK                  :8;
        uint32    mu2_Rx_WSIRSDR2_IERR                     :2;
        uint32    mu1_Rx_WSIRSDR2_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_WSIRSDR2_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_WSIRSDR3_CRC                        :5;
        uint32    mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA  :12;
        uint32    mu1_Rx_WSIRSDR3_LATCH_D0                   :1;
        uint32    mu1_Rx_WSIRSDR3_NO_FAULT                   :1;
        uint32    mu1_Rx_WSIRSDR3_STANDSTILL                 :1;
        uint32    mu1_Rx_WSIRSDR3_PTY_BIT                    :1;
        uint32    mu8_Rx_WSIRSDR3_ADD_FBK                    :8;
        uint32    mu2_Rx_WSIRSDR3_IERR                       :2;
        uint32    mu1_Rx_WSIRSDR3_SPI_ERR                    :1;
    }B;
    struct
    {
        uint32    mu5_Rx_WSIRSDR3_CRC              :5;
        uint32    mu1_Rx_WSIRSDR3_ZERO_0           :1;
        uint32    mu1_Rx_WSIRSDR3_ZERO_1           :1;
        uint32    mu1_Rx_WSIRSDR3_PULSE_OVERFLOW   :1;
        uint32    mu1_Rx_WSIRSDR3_NODATA           :1;
        uint32    mu1_Rx_WSIRSDR3_INVALID          :1;
        uint32    mu1_Rx_WSIRSDR3_WSI_OT           :1;
        uint32    mu1_Rx_WSIRSDR3_OPEN             :1;
        uint32    mu1_Rx_WSIRSDR3_CURRENT_HI       :1;
        uint32    mu1_Rx_WSIRSDR3_STB              :1;
        uint32    mu1_Rx_WSIRSDR3_STG              :1;
        uint32    mu1_Rx_WSIRSDR3_ZERO_2           :1;
        uint32    mu1_Rx_WSIRSDR3_ZERO_3           :1;
        uint32    mu1_Rx_WSIRSDR3_ONOFF            :1;
        uint32    mu1_Rx_WSIRSDR3_ONE              :1;
        uint32    mu1_Rx_WSIRSDR3_ZERO_4           :1;
        uint32    mu1_Rx_WSIRSDR3_RESERVED         :1;
        uint32    mu8_Rx_WSIRSDR3_ADD_FBK          :8;
        uint32    mu2_Rx_WSIRSDR3_IERR             :2;
        uint32    mu1_Rx_WSIRSDR3_SPI_ERR          :1;
    }B1;

}ASIC_RX_MSG_WSIRSDR3_t;


typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_64_CRC                      :5;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_0        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_1        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_2        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_3        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_4        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_5        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_6        :1;
        uint32    mu1_Rx_64_SOLENOID_ENABLE_7        :1;        
        uint32    mu8_Rx_64_RESERVED                 :8;
        uint32    mu8_Rx_64_ADD_FBK                  :8;
        uint32    mu2_Rx_64_IERR                     :2;
        uint32    mu1_Rx_64_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_64_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_65_CRC                      :5;
        uint32    mu1_Rx_65_CH0_STATUS_L             :1;
        uint32    mu1_Rx_65_CH0_STATUS_H             :1;
        uint32    mu1_Rx_65_CH1_STATUS_L             :1;
        uint32    mu1_Rx_65_CH1_STATUS_H             :1;
        uint32    mu1_Rx_65_CH2_STATUS_L             :1;
        uint32    mu1_Rx_65_CH2_STATUS_H             :1;
        uint32    mu1_Rx_65_CH3_STATUS_L             :1;
        uint32    mu1_Rx_65_CH3_STATUS_H             :1;
        uint32    mu1_Rx_65_CH4_STATUS_L             :1;
        uint32    mu1_Rx_65_CH4_STATUS_H             :1;
        uint32    mu1_Rx_65_CH5_STATUS_L             :1;
        uint32    mu1_Rx_65_CH5_STATUS_H             :1;
        uint32    mu1_Rx_65_CH6_STATUS_L             :1;
        uint32    mu1_Rx_65_CH6_STATUS_H             :1;
        uint32    mu1_Rx_65_CH7_STATUS_L             :1;
        uint32    mu1_Rx_65_CH7_STATUS_H             :1;
        uint32    mu8_Rx_65_ADD_FBK                  :8;
        uint32    mu2_Rx_65_IERR                     :2;
        uint32    mu1_Rx_65_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_65_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_67_CRC                      :5;
        uint32    mu1_Rx_67_CH1_DIAG_EN              :1;
        uint32    mu1_Rx_67_CH1_TD_BLANK             :1;
        uint32    mu1_Rx_67_CH2_DIAG_EN              :1;
        uint32    mu1_Rx_67_CH_2TD_BLANK             :1;
        uint32    mu1_Rx_67_CH3_DIAG_EN              :1;
        uint32    mu1_Rx_67_CH3_TD_BLANK             :1;
        uint32    mu1_Rx_67_CH4_DIAG_EN              :1;
        uint32    mu1_Rx_67_CH4_TD_BLANK             :1;
        uint32    mu1_Rx_67_CH1_EN                   :1;
        uint32    mu1_Rx_67_CH2_EN                   :1;
        uint32    mu1_Rx_67_CH3_EN                   :1;
        uint32    mu1_Rx_67_CH4_EN                   :1;
        uint32    mu1_Rx_67_CH1_CMD                  :1;
        uint32    mu1_Rx_67_CH2_CMD                  :1;
        uint32    mu1_Rx_67_CH3_CMD                  :1;
        uint32    mu1_Rx_67_CH4_CMD                  :1;
        uint32    mu8_Rx_67_ADD_FBK                  :8;
        uint32    mu2_Rx_67_IERR                     :2;
        uint32    mu1_Rx_67_SPI_ERR                  :1;
    }B;
}ASIC_RX_MSG_67_t;

/* MSG 68~71 */
typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_CH_DIAG_CRC                  :5;
        uint32    mu1_Rx_CH_DIAG_TH_WARN              :1;
        uint32    mu1_Rx_CH_DIAG_T_SD                 :1;
        uint32    mu1_Rx_CH_DIAG_UNDER_CURR           :1;
        uint32    mu1_Rx_CH_DIAG_OPL                  :1;
        uint32    mu1_Rx_CH_DIAG_LVT                  :1;
        uint32    mu1_Rx_CH_DIAG_GND_LOSS             :1;
        uint32    mu1_Rx_CH_DIAG_VGS_LS_FAULT         :1;
        uint32    mu1_Rx_CH_DIAG_LS_OVC               :1;
        uint32    mu1_Rx_CH_DIAG_LS_CLAMP             :1;
        uint32    mu1_Rx_CH_DIAG_RESERVED             :7;
        uint32    mu8_Rx_CH_DIAG_ADD_FBK              :8;
        uint32    mu2_Rx_CH_DIAG_IERR                 :2;
        uint32    mu1_Rx_CH_DIAG_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ONOFF_CH_DIAG_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_72_CRC                  :5;
        uint32    mu8_Rx_72_SEED                 :8;
        uint32    mu8_Rx_72_RESERVED             :8;
        uint32    mu8_Rx_72_ADD_FBK              :8;
        uint32    mu2_Rx_72_IERR                 :2;
        uint32    mu1_Rx_72_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_72_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_73_CRC                  :5;
        uint32    mu4_Rx_73_WD_CNT_VALUE         :4;
        uint32    mu1_Rx_73_WD_EARLY_ANSW        :1;
        uint32    mu1_Rx_73_WD_LATE_ANSW         :1;
        uint32    mu1_Rx_73_WD_BAD_ANSW          :1;
        uint32    mu1_Rx_73_WD_EARLY_REQ         :1;
        uint32    mu1_Rx_73_WD_LATE_REQ          :1;
        uint32    mu1_Rx_73_WD_RST_TO_ANSW       :1;
        uint32    mu1_Rx_73_WD_RST_TO_REQ        :1;
        uint32    mu1_Rx_73_WD_RST_CNT           :1;
        uint32    mu4_Rx_73_WD_RST_EVENT_VALUE   :4;
        uint32    mu8_Rx_73_ADD_FBK              :8;
        uint32    mu2_Rx_73_IERR                 :2;
        uint32    mu1_Rx_73_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_73_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_74_CRC                  :5;
        uint32    mu8_Rx_74_VALID_ANSWER_START   :8;
        uint32    mu8_Rx_74_VALID_REQUEST_START  :8;
        uint32    mu8_Rx_74_ADD_FBK              :8;
        uint32    mu2_Rx_74_IERR                 :2;
        uint32    mu1_Rx_74_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_74_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_75_CRC                  :5;
        uint32    mu6_Rx_75_VALID_ANSWER_DELTA     :6;
        uint32    mu2_Rx_75_RESERVED0              :2;
        uint32    mu6_Rx_75_VALID_REQUEST_DELTA    :6;
        uint32    mu1_Rx_75_REQ_CHECK_ENABLE       :1;
        uint32    mu1_Rx_75_RESERVED1              :1;
        uint32    mu8_Rx_75_ADD_FBK              :8;
        uint32    mu2_Rx_75_IERR                 :2;
        uint32    mu1_Rx_75_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_75_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_76_CRC                  :5;
        uint32    mu6_Rx_76_ANSWER_TIME_OUT_DELTA  :6;
        uint32    mu2_Rx_76_RESERVED0              :2;
        uint32    mu6_Rx_76_REQUEST_TIME_OUT_DELTA :6;
        uint32    mu1_Rx_76_TO_RESET_ENABLE        :1;
        uint32    mu1_Rx_76_RESERVED1              :1;
        uint32    mu8_Rx_76_ADD_FBK              :8;
        uint32    mu2_Rx_76_IERR                 :2;
        uint32    mu1_Rx_76_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_76_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_77_CRC                  :5;
        uint32    mu3_Rx_77_NUM_OF_GOOD_STEPS      :3;
        uint32    mu3_Rx_77_NUM_OF_BAD_STEPS       :3;
        uint32    mu4_Rx_77_HIGH_TH                :4;
        uint32    mu4_Rx_77_LOW_TH                 :4;
        uint32    mu1_Rx_77_CLOCK_DIVISION         :1;
        uint32    mu1_Rx_77_RESET_ENABLE           :1;
        uint32    mu8_Rx_77_ADD_FBK              :8;
        uint32    mu2_Rx_77_IERR                 :2;
        uint32    mu1_Rx_77_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_77_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_78_CRC                  :5;
        uint32    mu8_Rx_78_ANSWER_LOW           :8;
        uint32    mu8_Rx_78_ANSWER_HIGH          :8;
        uint32    mu8_Rx_78_ADD_FBK              :8;
        uint32    mu2_Rx_78_IERR                 :2;
        uint32    mu1_Rx_78_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_78_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_90_CRC                  :5;
        uint32    mu16_Rx_90_RESERVED            :16;
        uint32    mu8_Rx_90_ADD_FBK              :8;
        uint32    mu2_Rx_90_IERR                 :2;
        uint32    mu1_Rx_90_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_90_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT0_CRC                  :5;
        uint32    mu1_Rx_NT0_PWM_FAULT            :1;
        uint32    mu1_Rx_NT0_CURR_SENSE           :1;
        uint32    mu2_Rx_NT0_RESERVED0            :2;
        uint32    mu1_Rx_NT0_ADC_FAULT            :1;
        uint32    mu2_Rx_NT0_LOGIC_BIST_STATUS    :2;
        uint32    mu1_Rx_NT0_RESERVED1            :9;
        uint32    mu8_Rx_NT0_ADD_FBK              :8;
        uint32    mu2_Rx_NT0_IERR                 :2;
        uint32    mu1_Rx_NT0_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_0_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT1_CRC                  :5;
        uint32    mu1_Rx_NT1_LS_CLAMP_ON          :1;
        uint32    mu1_Rx_NT1_LS_OVC               :1;
        uint32    mu1_Rx_NT1_VGS_LS_FAULT         :1;
        uint32    mu1_Rx_NT1_VGS_HS_FAULT         :1;
        uint32    mu1_Rx_NT1_HS_SHORT             :1;
        uint32    mu1_Rx_NT1_LVT                  :1;
        uint32    mu1_Rx_NT1_OPEN_LOAD            :1;
        uint32    mu1_Rx_NT1_GND_LOSS             :1;
        uint32    mu1_Rx_NT1_RESERVED0            :1;
        uint32    mu1_Rx_NT1_TH_WARN              :1;
        uint32    mu1_Rx_NT1_T_SD                 :1;
        uint32    mu5_Rx_NT1_RESERVED1            :5;
        uint32    mu8_Rx_NT1_ADD_FBK              :8;
        uint32    mu2_Rx_NT1_IERR                 :2;
        uint32    mu1_Rx_NT1_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_1_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT2_CRC                  :5;
        uint32    mu2_Rx_NT2_SR_SEL               :2;
        uint32    mu2_Rx_NT2_RESERVED0            :2;
        uint32    mu1_Rx_NT2_CALIBRATION_DIS      :1;
        uint32    mu1_Rx_NT2_OFS_CHOP_DIS         :1;
        uint32    mu1_Rx_NT2_TD_BLANK_SEL         :1;
        uint32    mu1_Rx_NT2_LS_CLAMP_DIS         :1;
        uint32    mu1_Rx_NT2_LOGIC_BIST_EN        :1;
        uint32    mu1_Rx_NT2_DIAG_BIST_EN         :1;
        uint32    mu1_Rx_NT2_ADC_BIST_EN          :1;
        uint32    mu1_Rx_NT2_OFF_DIAG_EN          :1;
        uint32    mu1_Rx_NT2_E_SOL_GPO            :1;
        uint32    mu1_Rx_NT2_E_LSCLAMP_GPO        :1;
        uint32    mu2_Rx_NT2_RESERVED1            :2;
        uint32    mu8_Rx_NT2_ADD_FBK              :8;
        uint32    mu2_Rx_NT2_IERR                 :2;
        uint32    mu1_Rx_NT2_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_2_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT5_CRC                  :5;
        uint32    mu11_Rx_NT5_SET_POINT           :11;
        uint32    mu5_Rx_NT5_RESERVED             :5;
        uint32    mu8_Rx_NT5_ADD_FBK              :8;
        uint32    mu2_Rx_NT5_IERR                 :2;
        uint32    mu1_Rx_NT5_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_5_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT6_CRC                  :5;
        uint32    mu10_Rx_NT6_PWM_CODE            :10;
        uint32    mu6_Rx_NT6_RESERVED             :6;
        uint32    mu8_Rx_NT6_ADD_FBK              :8;
        uint32    mu2_Rx_NT6_IERR                 :2;
        uint32    mu1_Rx_NT6_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_6_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT7_CRC                  :5;
        uint32    mu8_Rx_NT7_MAX_FREQ_DELTA       :8;
        uint32    mu7_Rx_NT7_FREQ_MOD_STEP        :7;
        uint32    mu1_Rx_NT7_RESERVED             :1;
        uint32    mu8_Rx_NT7_ADD_FBK              :8;
        uint32    mu2_Rx_NT7_IERR                 :2;
        uint32    mu1_Rx_NT7_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_7_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT8_CRC                  :5;
        uint32    mu3_Rx_NT8_KP                   :3;
        uint32    mu3_Rx_NT8_KI                   :3;
        uint32    mu10_Rx_NT8_RESERVED            :10;
        uint32    mu8_Rx_NT8_ADD_FBK              :8;
        uint32    mu2_Rx_NT8_IERR                 :2;
        uint32    mu1_Rx_NT8_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_8_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT9_CRC                  :5;
        uint32    mu2_Rx_NT9_INTTIME              :2;
        uint32    mu2_Rx_NT9_FILT_TIME            :2;
        uint32    mu1_Rx_NT9_CHOP_TIME            :1;
        uint32    mu11_Rx_NT9_RESERVED            :11;
        uint32    mu8_Rx_NT9_ADD_FBK              :8;
        uint32    mu2_Rx_NT9_IERR                 :2;
        uint32    mu1_Rx_NT9_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_9_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT10_CRC                  :5;
        uint32    mu12_Rx_NT10_AVG_CURRENT         :11;
        uint32    mu4_Rx_NT10_RESERVED             :5;
        uint32    mu8_Rx_NT10_ADD_FBK              :8;
        uint32    mu2_Rx_NT10_IERR                 :2;
        uint32    mu1_Rx_NT10_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_10_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT12_CRC                  :5;
        uint32    mu13_Rx_NT12_PWM_CODE            :13;
        uint32    mu1_Rx_NT12_TMOUT                :1;
        uint32    mu2_Rx_NT12_RESERVED             :2;
        uint32    mu8_Rx_NT12_ADD_FBK              :8;
        uint32    mu2_Rx_NT12_IERR                 :2;
        uint32    mu1_Rx_NT12_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_12_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT13_CRC                  :5;
        uint32    mu8_Rx_NT13_BASE_DELTA_CURENT    :8;
        uint32    mu8_Rx_NT13_RESERVED             :8;
        uint32    mu8_Rx_NT13_ADD_FBK              :8;
        uint32    mu2_Rx_NT13_IERR                 :2;
        uint32    mu1_Rx_NT13_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_13_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_NT14_CRC                  :5;
        uint32    mu8_Rx_NT14_DELTA_CURRENT        :8;
        uint32    mu8_Rx_NT14_RESERVED             :8;
        uint32    mu8_Rx_NT14_ADD_FBK              :8;
        uint32    mu2_Rx_NT14_IERR                 :2;
        uint32    mu1_Rx_NT14_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_NO_TC_14_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV1_CRC                  :5;
        uint32    mu1_Rx_ESV1_LS_CLAMP_ON          :1;
        uint32    mu1_Rx_ESV1_LS_OVC               :1;
        uint32    mu1_Rx_ESV1_VGS_LS_FAULT         :1;
        uint32    mu2_Rx_ESV1_RESERVED0            :2;
        uint32    mu1_Rx_ESV1_LVT                  :1;
        uint32    mu1_Rx_ESV1_OPEN_LOAD            :1;
        uint32    mu1_Rx_ESV1_GND_LOSS             :1;
        uint32    mu1_Rx_ESV1_PWM_FAULT            :1;
        uint32    mu1_Rx_ESV1_TH_WARN              :1;
        uint32    mu1_Rx_ESV1_T_SD                 :1;
        uint32    mu1_Rx_ESV1_RESERVED1            :2;
        uint32    mu1_Rx_ESV1_ADC_FAULT            :1;
        uint32    mu2_Rx_ESV1_LOGIC_BIST_STATUS    :2;
        uint32    mu8_Rx_ESV1_ADD_FBK              :8;
        uint32    mu2_Rx_ESV1_IERR                 :2;
        uint32    mu1_Rx_ESV1_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_1_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV2_CRC                  :5;
        uint32    mu2_Rx_ESV2_SR_SEL               :2;
        uint32    mu2_Rx_ESV2_RESERVED0            :2;
        uint32    mu1_Rx_ESV2_CALIBRATION_DIS      :1;
        uint32    mu1_Rx_ESV2_OFS_CHOP_DIS         :1;
        uint32    mu1_Rx_ESV2_TD_BLANK_SEL         :1;
        uint32    mu1_Rx_ESV2_LS_CLAMP_DIS         :1;
        uint32    mu1_Rx_ESV2_LOGIC_BIST_EN        :1;
        uint32    mu1_Rx_ESV2_DIAG_BIST_EN         :1;
        uint32    mu1_Rx_ESV2_ADC_BIST_EN          :1;
        uint32    mu1_Rx_ESV2_OFF_DIAG_EN          :1;
        uint32    mu1_Rx_ESV2_E_SOL_GPO            :1;
        uint32    mu1_Rx_ESV2_E_LSCLAMP_GPO        :1;
        uint32    mu2_Rx_ESV2_RESERVED1            :2;
        uint32    mu8_Rx_ESV2_ADD_FBK              :8;
        uint32    mu2_Rx_ESV2_IERR                 :2;
        uint32    mu1_Rx_ESV2_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_2_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV5_CRC                  :5;
        uint32    mu11_Rx_ESV5_SET_POINT           :11;
        uint32    mu5_Rx_ESV5_RESERVED             :5;
        uint32    mu8_Rx_ESV5_ADD_FBK              :8;
        uint32    mu2_Rx_ESV5_IERR                 :2;
        uint32    mu1_Rx_ESV5_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_5_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV6_CRC                  :5;
        uint32    mu6_Rx_ESV6_PWM_CODE             :6;
        uint32    mu4_Rx_ESV6_FREQ_MOD_STEP        :4;
        uint32    mu5_Rx_ESV6_MAX_FREQ_DELTA       :5;
        uint32    mu1_Rx_ESV6_RESERVED             :1;
        uint32    mu8_Rx_ESV6_ADD_FBK              :8;
        uint32    mu2_Rx_ESV6_IERR                 :2;
        uint32    mu1_Rx_ESV6_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_6_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV8_CRC                  :5;
        uint32    mu3_Rx_ESV8_KP                   :3;
        uint32    mu3_Rx_ESV8_KI                   :3;
        uint32    mu10_Rx_ESV8_RESERVED            :10;
        uint32    mu8_Rx_ESV8_ADD_FBK              :8;
        uint32    mu2_Rx_ESV8_IERR                 :2;
        uint32    mu1_Rx_ESV8_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_8_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV9_CRC                  :5;
        uint32    mu2_Rx_ESV9_INTTIME              :2;
        uint32    mu2_Rx_ESV9_FILT_TIME            :2;
        uint32    mu1_Rx_ESV9_CHOP_TIME            :1;
        uint32    mu11_Rx_ESV9_RESERVED            :11;
        uint32    mu8_Rx_ESV9_ADD_FBK              :8;
        uint32    mu2_Rx_ESV9_IERR                 :2;
        uint32    mu1_Rx_ESV9_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_9_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV10_CRC                  :5;
        uint32    mu11_Rx_ESV10_AVG_CURRENT         :11;
        uint32    mu5_Rx_ESV10_RESERVED             :5;
        uint32    mu8_Rx_ESV10_ADD_FBK              :8;
        uint32    mu2_Rx_ESV10_IERR                 :2;
        uint32    mu1_Rx_ESV10_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_10_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_ESV12_CRC                  :5;
        uint32    mu12_Rx_ESV12_PWM_CODE            :12;
        uint32    mu1_Rx_ESV12_TMOUT                :1;
        uint32    mu3_Rx_ESV12_RESERVED             :3;
        uint32    mu8_Rx_ESV12_ADD_FBK              :8;
        uint32    mu2_Rx_ESV12_IERR                 :2;
        uint32    mu1_Rx_ESV12_SPI_ERR              :1;
    }B;
}ASIC_RX_MSG_ESV_12_t;

typedef union
{
    uint32    R;
    struct
    {
        uint32    mu5_Rx_Msg_CRC                    :5;
        uint32    mu8_Rx_Msg_RESERVED               :16;
        uint32    mu8_Rx_Msg_ADD                    :8;
        uint32    mu1_Rx_Msg_Zero                   :2;
        uint32    mu1_Rx_Msg_SPIE                   :1;
    } B;
}ASIC_RX_MSG_EMPTY_DATA_t;

/******************************** Tx *********************************************/

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_1_VDD3_OVC_FLT_TIME           :2;
            uint32    mu2_Tx_1_VDD_3_OVC_SD_RESTART_TIME   :2;
            uint32    mu2_Tx_1_VDD4_OVC_FLT_TIME           :2;
            uint32    mu2_Tx_1_VDD_4_OVC_SD_RESTART_TIME   :2;
            uint32    mu1_Tx_1_OSC_SprSpect_DIS            :1;
            uint32    mu1_Tx_1_PMP_EN                      :1;
            uint32    mu1_Tx_1_FS_EN                       :1;
            uint32    mu1_Tx_1_FS_CMD                      :1;
            uint32    mu2_Tx_1_FS_VDS_TH                   :2;
            uint32    mu1_Tx_1_PHOLD                       :1;
            uint32    mu1_Tx_1_KA                          :1;    
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_1_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_2_VDD2_DIS                     :1;
            uint32    mu1_Tx_2_VDD5_DIS                     :1;
            uint32    mu1_Tx_2_WD_DIS                       :1;
            uint32    mu1_Tx_2_VDD1_DIODELOSS_FILT          :1;
            uint32    mu1_Tx_2_VDD_2d5_AUTO_SWITCH_OFF      :1;
            uint32    mu2_Tx_2_WD_SW_DIS                    :2;
            uint32    mu9_Tx_2_RESERVED                     :9;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_2_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_6_ANALOG_BIST_EN    :1;
            uint32    mu1_Tx_6_CORE_BIST_EN      :1;
            uint32    mu2_Tx_6_RESERVED_0        :2;
            uint32    mu1_Tx_6_NVM_CRC_MASK      :1;
            uint32    mu1_Tx_6_RESERVED_1        :1;
            uint32    mu1_Tx_6_NVM_DWN           :1;
            uint32    mu9_Tx_6_RESERVED_2        :9;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_6_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_8_PUMP_DTY_PWM :8;
            uint32    mu8_Tx_8_PUMP_TCK_PWM :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_8_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_9_PMP_VDS_TH    :2;
            uint32    mu1_Tx_9_PMP2_DIS      :1;
            uint32    mu3_Tx_9_PMP_VDS_FIL   :3;
            uint32    mu1_Tx_9_LDACT_DIS     :1;
            uint32    mu1_Tx_9_PMP1_ISINC    :1;
            uint32    mu4_Tx_9_PMP_DT        :4;
            uint32    mu1_Tx_9_PMP_TEST      :1;
            uint32    mu2_Tx_9_RESERVED      :2;
            uint32    mu1_Tx_9_PMP_BIST_EN   :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_9_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu10_Tx_13_RESERVED0   :10;
            uint32    mu4_Tx_13_ADC_SEL      :4;
            uint32    mu2_Tx_13_RESERVED1    :2;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_13_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_15_VSO_CONF            :1;
            uint32    mu2_Tx_15_WSO_VSO_S           :2;
            uint32    mu1_Tx_15_SSIG_VSO            :1;
            uint32    mu1_Tx_15_VSO_PTEN            :1;
            uint32    mu1_Tx_15_VSO_OFF_DIAG_EN     :1;
            uint32    mu1_Tx_15_VSO_LS_CLAMP_DIS    :1;
            uint32    mu1_Tx_15_RESERVED            :1;
            uint32    mu1_Tx_15_SEL_CONF            :1;
            uint32    mu2_Tx_15_WSO_SEL_S           :2;
            uint32    mu1_Tx_15_SSIG_SEL            :1;
            uint32    mu1_Tx_15_SEL_OUT_CMD         :1;
            uint32    mu1_Tx_15_SEL_OFF_DIAG_EN     :1;
            uint32    mu1_Tx_15_SEL_LS_CLAMP_DIS    :1;
            uint32    mu1_Tx_15_SEL_PTEN            :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_15_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_17_WLD_CONF          :1;
            uint32    mu1_Tx_17_WLD_CMD           :1;
            uint32    mu1_Tx_17_WLD_DIAGOFF_EN    :1;
            uint32    mu1_Tx_17_WLD_LS_CLAMP_DIS  :1;
            uint32    mu4_Tx_17_RESERVED0         :4;
            uint32    mu1_Tx_17_SHLS_CMD          :1;
            uint32    mu1_Tx_17_SHLS_DIAGOFF_EN   :1;
            uint32    mu1_Tx_17_SHLS_CONFIG       :1;
            uint32    mu1_Tx_17_SHLS_LS_CLAMP_DIS :1;
            uint32    mu4_Tx_17_RESERVED1         :4;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_17_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_19_E_PVDS_GPO             :1;
            uint32    mu1_Tx_19_E_VDD2d5dWSx_OT_GPO    :1;
            uint32    mu1_Tx_19_E_FBO_GPO              :1;
            uint32    mu1_Tx_19_E_FSPI_GPO             :1;
            uint32    mu1_Tx_19_E_EN_GPO               :1;
            uint32    mu1_Tx_19_E_FS_VDS_GPO           :1;
            uint32    mu10_Tx_19_RESERVED              :10;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_19_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_32_WSO_TEST         :1;
            uint32    mu7_Tx_32_CONFIG_RANGE     :7;
            uint32    mu8_Tx_32_RESERVED         :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_32_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_33_WSI_FIRST_THRESHOLD    :8;
            uint32    mu8_Tx_33_RESERVED               :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_33_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_34_WSI_OFFSET_THRESHOLD    :8;
            uint32    mu8_Tx_34_RESERVED               :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_34_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_35_SENSOR_TYPE_SELECTION    :2;
            uint32    mu1_Tx_35_RESERVED0                :1;
            uint32    mu1_Tx_35_FIX_TH                   :1;
            uint32    mu1_Tx_35_SS_DIS                   :1;
            uint32    mu1_Tx_35_PTEN                     :1;
            uint32    mu2_Tx_35_FILTER_SELECTION         :2;
            uint32    mu8_Tx_35_RESEVED1                 :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_35_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_36_SENSOR_TYPE_SELECTION    :2;
            uint32    mu1_Tx_36_RESERVED0                :1;
            uint32    mu1_Tx_36_FIX_TH                   :1;
            uint32    mu1_Tx_36_SS_DIS                   :1;
            uint32    mu1_Tx_36_PTEN                     :1;
            uint32    mu2_Tx_36_FILTER_SELECTION         :2;
            uint32    mu8_Tx_36_RESEVED1                 :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_36_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_37_SENSOR_TYPE_SELECTION    :2;
            uint32    mu1_Tx_37_RESERVED0                :1;
            uint32    mu1_Tx_37_FIX_TH                   :1;
            uint32    mu1_Tx_37_SS_DIS                   :1;
            uint32    mu1_Tx_37_PTEN                     :1;
            uint32    mu2_Tx_37_FILTER_SELECTION         :2;
            uint32    mu8_Tx_37_RESEVED1                 :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_37_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_38_SENSOR_TYPE_SELECTION    :2;
            uint32    mu1_Tx_38_RESERVED0                :1;
            uint32    mu1_Tx_38_FIX_TH                   :1;
            uint32    mu1_Tx_38_SS_DIS                   :1;
            uint32    mu1_Tx_38_PTEN                     :1;
            uint32    mu2_Tx_38_FILTER_SELECTION         :2;
            uint32    mu8_Tx_38_RESEVED1                 :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_38_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_39_CH1_EN         :1;
            uint32    mu1_Tx_39_CH2_EN         :1;
            uint32    mu1_Tx_39_CH3_EN         :1;
            uint32    mu1_Tx_39_CH4_EN         :1;
            uint32    mu1_Tx_39_DIAG           :1;
            uint32    mu1_Tx_39_INIT           :1;
            uint32    mu1_Tx_39_WSI_VDD4_UV    :1;
            uint32    mu1_Tx_39_FVPWR_ACT      :1;
            uint32    mu8_Tx_39_RESERVED       :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_39_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu9_Tx_40_RESERVED0    :9;
            uint32    mu1_Tx_40_WS_CNT_EN    :1;
            uint32    mu1_Tx_40_WS_CNT_RST   :1;
            uint32    mu5_Tx_40_RESERVED1    :5;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_40_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu9_Tx_41_RESERVED0    :9;
            uint32    mu1_Tx_41_WS_CNT_EN    :1;
            uint32    mu1_Tx_41_WS_CNT_RST   :1;
            uint32    mu5_Tx_41_RESERVED1    :5;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_41_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu9_Tx_42_RESERVED0    :9;
            uint32    mu1_Tx_42_WS_CNT_EN    :1;
            uint32    mu1_Tx_42_WS_CNT_RST   :1;
            uint32    mu5_Tx_42_RESERVED1    :5;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_42_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu9_Tx_43_RESERVED0    :9;
            uint32    mu1_Tx_43_WS_CNT_EN    :1;
            uint32    mu1_Tx_43_WS_CNT_RST   :1;
            uint32    mu5_Tx_43_RESERVED1    :5;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_43_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_44_RESERVED0    :8;
            uint32    mu1_Tx_44_WSI1_LBIST_EN  :1;
            uint32    mu1_Tx_44_WSI2_LBIST_EN  :1;
            uint32    mu1_Tx_44_WSI3_LBIST_EN  :1;
            uint32    mu1_Tx_44_WSI4_LBIST_EN  :1;
            uint32    mu4_Tx_44_RESERVED1    :4;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_44_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_64_SOLENOID_ENABLE_0  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_1  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_2  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_3  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_4  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_5  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_6  :1;
            uint32    mu1_Tx_64_SOLENOID_ENABLE_7  :1;
            uint32    mu8_Tx_64_RESERVED           :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_64_t;



typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_65_CH0_STATUS_L       :1;
            uint32    mu1_Tx_65_CH0_STATUS_H       :1;
            uint32    mu1_Tx_65_CH1_STATUS_L       :1;
            uint32    mu1_Tx_65_CH1_STATUS_H       :1;
            uint32    mu1_Tx_65_CH2_STATUS_L       :1;
            uint32    mu1_Tx_65_CH2_STATUS_H       :1;
            uint32    mu1_Tx_65_CH3_STATUS_L       :1;
            uint32    mu1_Tx_65_CH3_STATUS_H       :1;
            uint32    mu1_Tx_65_CH4_STATUS_L       :1;
            uint32    mu1_Tx_65_CH4_STATUS_H       :1;
            uint32    mu1_Tx_65_CH5_STATUS_L       :1;
            uint32    mu1_Tx_65_CH5_STATUS_H       :1;
            uint32    mu1_Tx_65_CH6_STATUS_L       :1;
            uint32    mu1_Tx_65_CH6_STATUS_H       :1;
            uint32    mu1_Tx_65_CH7_STATUS_L       :1;
            uint32    mu1_Tx_65_CH7_STATUS_H       :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_65_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu1_Tx_67_CH1_DIAG_EN     :1;
            uint32    mu1_Tx_67_CH1_TD_BLANK    :1;
            uint32    mu1_Tx_67_CH2_DIAG_EN     :1;
            uint32    mu1_Tx_67_CH2_TD_BLANK    :1;
            uint32    mu1_Tx_67_CH3_DIAG_EN     :1;
            uint32    mu1_Tx_67_CH3_TD_BLANK    :1;
            uint32    mu1_Tx_67_CH4_DIAG_EN     :1;
            uint32    mu1_Tx_67_CH4_TD_BLANK    :1;
            uint32    mu1_Tx_67_CH1_EN          :1;
            uint32    mu1_Tx_67_CH2_EN          :1;
            uint32    mu1_Tx_67_CH3_EN          :1;
            uint32    mu1_Tx_67_CH4_EN          :1;
            uint32    mu1_Tx_67_CH1_CMD         :1;
            uint32    mu1_Tx_67_CH2_CMD         :1;
            uint32    mu1_Tx_67_CH3_CMD         :1;
            uint32    mu1_Tx_67_CH4_CMD         :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_67_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu16_Tx_72_RESERVED              :16;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_72_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_74_VALID_ANSWER_START     :8;
            uint32    mu8_Tx_74_VALID_REQUEST_START    :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_74_t;


typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu6_Tx_75_VALID_ANSWER_DELTA     :6;
            uint32    mu2_Tx_75_RESERVED0              :2;
            uint32    mu6_Tx_75_VALID_REQUEST_DELTA    :6;
            uint32    mu1_Tx_75_REQ_CHECK_ENABLE       :1;
            uint32    mu1_Tx_75_RESERVED1              :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_75_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu6_Tx_76_ANSWER_TIME_OUT_DELTA  :6;
            uint32    mu2_Tx_76_RESERVED0              :2;
            uint32    mu6_Tx_76_REQUEST_TIME_OUT_DELTA :6;
            uint32    mu1_Tx_76_TO_RESET_ENABLE        :1;
            uint32    mu1_Tx_76_RESERVED1              :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_76_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu3_Tx_77_NUM_OF_GOOD_STEPS      :3;
            uint32    mu3_Tx_77_NUM_OF_BAD_STEPS       :3;
            uint32    mu4_Tx_77_HIGH_TH                :4;
            uint32    mu4_Tx_77_LOW_TH                 :4;
            uint32    mu1_Tx_77_CLOCK_DIVISION         :1;
            uint32    mu1_Tx_77_RESET_ENABLE           :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_77_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_78_ANSWER_LOW             :8;
            uint32    mu8_Tx_78_ANSWER_HIGH            :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_78_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_NT2_SR_SEL           :2;
            uint32    mu2_Tx_NT2_RESERVED0        :2;
            uint32    mu1_Tx_NT2_CALIBRATION_DIS  :1;
            uint32    mu1_Tx_NT2_OFS_CHOP_DIS     :1;
            uint32    mu1_Tx_NT2_TD_BLANK_SEL     :1;
            uint32    mu1_Tx_NT2_LS_CLAMP_DIS     :1;
            uint32    mu1_Tx_NT2_LOGIC_BIST_EN    :1;
            uint32    mu1_Tx_NT2_DIAG_BIST_EN     :1;
            uint32    mu1_Tx_NT2_ADC_BIST_EN      :1;
            uint32    mu1_Tx_NT2_OFF_DIAG_EN      :1;
            uint32    mu1_Tx_NT2_E_SOL_GPO        :1;
            uint32    mu1_Tx_NT2_E_LSCLAMP_GPO    :1;
            uint32    mu2_Tx_NT2_RESERVED1        :2;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_2_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu11_Tx_NT5_SET_POINT   :11;
            uint32    mu5_Tx_NT5_RESERVED     :5;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_5_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu10_Tx_NT6_PWM_CODE    :10;
            uint32    mu6_Tx_NT6_RESERVED     :6;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_6_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_NT7_MAX_FREQ_DELTA    :8;
            uint32    mu7_Tx_NT7_FREQ_MOD_STEP     :7;
            uint32    mu1_Tx_NT7_RESERVED          :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_7_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu3_Tx_NT8_KP                :3;
            uint32    mu3_Tx_NT8_KI                :3;
            uint32    mu10_Tx_NT8_RESERVED         :10;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_8_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_NT9_INTTIME           :2;
            uint32    mu2_Tx_NT9_FILT_TIME         :2;
            uint32    mu1_Tx_NT9_CHOP_TIME         :1;
            uint32    mu11_Tx_NT9_RESERVED         :11;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_9_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu8_Tx_NT13_BASE_DELTA_CURENT    :8;
            uint32    mu8_Tx_NT13_RESERVED             :8;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_NO_TC_13_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_ESV2_SR_SEL               :2;
            uint32    mu2_Tx_ESV2_RESERVED0            :2;
            uint32    mu1_Tx_ESV2_CALIBRATION_DIS      :1;
            uint32    mu1_Tx_ESV2_OFS_CHOP_DIS         :1;
            uint32    mu1_Tx_ESV2_TD_BLANK_SEL         :1;
            uint32    mu1_Tx_ESV2_LS_CLAMP_DIS         :1;
            uint32    mu1_Tx_ESV2_LOGIC_BIST_EN        :1;
            uint32    mu1_Tx_ESV2_DIAG_BIST_EN         :1;
            uint32    mu1_Tx_ESV2_ADC_BIST_EN          :1;
            uint32    mu1_Tx_ESV2_OFF_DIAG_EN          :1;
            uint32    mu1_Tx_ESV2_E_SOL_GPO            :1;
            uint32    mu1_Tx_ESV2_E_LSCLAMP_GPO        :1;
            uint32    mu2_Tx_ESV2_RESERVED1            :2;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_ESV_2_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu11_Tx_ESV5_SET_POINT           :11;
            uint32    mu5_Tx_ESV5_RESERVED             :5;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_ESV_5_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu6_Tx_ESV6_PWM_CODE             :6;
            uint32    mu4_Tx_ESV6_FREQ_MOD_STEP        :4;
            uint32    mu5_Tx_ESV6_MAX_FREQ_DELTA       :5;
            uint32    mu1_Tx_ESV6_RESERVED             :1;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_ESV_6_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu3_Tx_ESV8_KP                   :3;
            uint32    mu3_Tx_ESV8_KI                   :3;
            uint32    mu10_Tx_ESV8_RESERVED            :10;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_ESV_8_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu2_Tx_ESV9_INTTIME              :2;
            uint32    mu2_Tx_ESV9_FILT_TIME            :2;
            uint32    mu1_Tx_ESV9_CHOP_TIME            :1;
            uint32    mu11_Tx_ESV9_RESERVED            :11;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_ESV_9_t;

typedef struct
{
    union
    {
        uint16  DATA;
        struct
        {
            uint32    mu16_Tx_Msg_RESERVED    :16;
        }B;
    }R;
    uint32    mu16_RESERVED                                :16;
}ASIC_TX_MSG_EMPTY_DATA_t;




typedef struct
{
    union
    {
        uint32 ASIC_RxArray[ACH_MSGID_MAX];
        struct
        {
            /* ASIC RX Message */
            ASIC_RX_MSG_8_t             ASIC_RX_MSG_8;      /* Notice : The order should be the same as ACH_MsgIdType */
            ASIC_RX_MSG_1_t             ASIC_RX_MSG_1;
            ASIC_RX_MSG_64_t            ASIC_RX_MSG_64;
            ASIC_RX_MSG_65_t            ASIC_RX_MSG_65;
            ASIC_RX_MSG_64_t            ASIC_RX_MSG_64_2;   /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
            ASIC_RX_MSG_0_t             ASIC_RX_MSG_0;
            ASIC_RX_MSG_2_t             ASIC_RX_MSG_2;
            ASIC_RX_MSG_3_t             ASIC_RX_MSG_3;
            ASIC_RX_MSG_4_t             ASIC_RX_MSG_4;
            ASIC_RX_MSG_5_t             ASIC_RX_MSG_5;
            ASIC_RX_MSG_6_t             ASIC_RX_MSG_6;
            ASIC_RX_MSG_7_t             ASIC_RX_MSG_7;
            ASIC_RX_MSG_9_t             ASIC_RX_MSG_9;
            ASIC_RX_MSG_10_t            ASIC_RX_MSG_10;
            ASIC_RX_MSG_11_t            ASIC_RX_MSG_11;
            ASIC_RX_MSG_12_t            ASIC_RX_MSG_12;
            ASIC_RX_MSG_13_t            ASIC_RX_MSG_13;
            ASIC_RX_MSG_15_t            ASIC_RX_MSG_15;
            ASIC_RX_MSG_16_t            ASIC_RX_MSG_16;
            ASIC_RX_MSG_17_t            ASIC_RX_MSG_17;
            ASIC_RX_MSG_18_t            ASIC_RX_MSG_18;
            ASIC_RX_MSG_19_t            ASIC_RX_MSG_19;
            ASIC_RX_MSG_20_t            ASIC_RX_MSG_20;
            ASIC_RX_MSG_32_t            ASIC_RX_MSG_32;
            ASIC_RX_MSG_33_t            ASIC_RX_MSG_33;
            ASIC_RX_MSG_34_t            ASIC_RX_MSG_34;
            ASIC_RX_MSG_35_t            ASIC_RX_MSG_35;
            ASIC_RX_MSG_36_t            ASIC_RX_MSG_36;
            ASIC_RX_MSG_37_t            ASIC_RX_MSG_37;
            ASIC_RX_MSG_38_t            ASIC_RX_MSG_38;
            ASIC_RX_MSG_39_t            ASIC_RX_MSG_39;
            ASIC_RX_MSG_40_t            ASIC_RX_MSG_40;
            ASIC_RX_MSG_41_t            ASIC_RX_MSG_41;
            ASIC_RX_MSG_42_t            ASIC_RX_MSG_42;
            ASIC_RX_MSG_43_t            ASIC_RX_MSG_43;
            ASIC_RX_MSG_44_t            ASIC_RX_MSG_44;

            /* MSG 48~63 WSS Data & Fault register */
            ASIC_RX_MSG_WSIRSDR0_t      ASIC_RX_MSG_48;
            ASIC_RX_MSG_WSIRSDR1_t      ASIC_RX_MSG_49;
            ASIC_RX_MSG_WSIRSDR2_t      ASIC_RX_MSG_50;
            ASIC_RX_MSG_WSIRSDR3_t      ASIC_RX_MSG_51;
            ASIC_RX_MSG_WSIRSDR0_t      ASIC_RX_MSG_52;
            ASIC_RX_MSG_WSIRSDR1_t      ASIC_RX_MSG_53;
            ASIC_RX_MSG_WSIRSDR2_t      ASIC_RX_MSG_54;
            ASIC_RX_MSG_WSIRSDR3_t      ASIC_RX_MSG_55;
            ASIC_RX_MSG_WSIRSDR0_t      ASIC_RX_MSG_56;
            ASIC_RX_MSG_WSIRSDR1_t      ASIC_RX_MSG_57;
            ASIC_RX_MSG_WSIRSDR2_t      ASIC_RX_MSG_58;
            ASIC_RX_MSG_WSIRSDR3_t      ASIC_RX_MSG_59;
            ASIC_RX_MSG_WSIRSDR0_t      ASIC_RX_MSG_60;
            ASIC_RX_MSG_WSIRSDR1_t      ASIC_RX_MSG_61;
            ASIC_RX_MSG_WSIRSDR2_t      ASIC_RX_MSG_62;
            ASIC_RX_MSG_WSIRSDR3_t      ASIC_RX_MSG_63;


            ASIC_RX_MSG_67_t            ASIC_RX_MSG_67;

            /* MSG 68~71 NC Valve ON/OFF Channel Diag */
            ASIC_RX_MSG_ONOFF_CH_DIAG_t ASIC_RX_MSG_68;
            ASIC_RX_MSG_ONOFF_CH_DIAG_t ASIC_RX_MSG_69;
            ASIC_RX_MSG_ONOFF_CH_DIAG_t ASIC_RX_MSG_70;
            ASIC_RX_MSG_ONOFF_CH_DIAG_t ASIC_RX_MSG_71;

            ASIC_RX_MSG_72_t            ASIC_RX_MSG_72;
            ASIC_RX_MSG_73_t            ASIC_RX_MSG_73;
            ASIC_RX_MSG_74_t            ASIC_RX_MSG_74;
            ASIC_RX_MSG_75_t            ASIC_RX_MSG_75;
            ASIC_RX_MSG_76_t            ASIC_RX_MSG_76;
            ASIC_RX_MSG_77_t            ASIC_RX_MSG_77;
            ASIC_RX_MSG_78_t            ASIC_RX_MSG_78;
            ASIC_RX_MSG_90_t            ASIC_RX_MSG_90;

            /* NO/TC Valve */
            /* NO CH0*/
            ASIC_RX_MSG_NO_TC_0_t       ASIC_RX_MSG_NO0_0;
            ASIC_RX_MSG_NO_TC_1_t       ASIC_RX_MSG_NO0_1;
            ASIC_RX_MSG_NO_TC_2_t       ASIC_RX_MSG_NO0_2;
            ASIC_RX_MSG_NO_TC_5_t       ASIC_RX_MSG_NO0_5;
            ASIC_RX_MSG_NO_TC_6_t       ASIC_RX_MSG_NO0_6;
            ASIC_RX_MSG_NO_TC_7_t       ASIC_RX_MSG_NO0_7;
            ASIC_RX_MSG_NO_TC_8_t       ASIC_RX_MSG_NO0_8;
            ASIC_RX_MSG_NO_TC_9_t       ASIC_RX_MSG_NO0_9;
            ASIC_RX_MSG_NO_TC_10_t      ASIC_RX_MSG_NO0_10;
            ASIC_RX_MSG_NO_TC_12_t      ASIC_RX_MSG_NO0_12;
            ASIC_RX_MSG_NO_TC_13_t      ASIC_RX_MSG_NO0_13;
            ASIC_RX_MSG_NO_TC_14_t      ASIC_RX_MSG_NO0_14;
            /* NO CH1*/
            ASIC_RX_MSG_NO_TC_0_t       ASIC_RX_MSG_NO1_0;
            ASIC_RX_MSG_NO_TC_1_t       ASIC_RX_MSG_NO1_1;
            ASIC_RX_MSG_NO_TC_2_t       ASIC_RX_MSG_NO1_2;
            ASIC_RX_MSG_NO_TC_5_t       ASIC_RX_MSG_NO1_5;
            ASIC_RX_MSG_NO_TC_6_t       ASIC_RX_MSG_NO1_6;
            ASIC_RX_MSG_NO_TC_7_t       ASIC_RX_MSG_NO1_7;
            ASIC_RX_MSG_NO_TC_8_t       ASIC_RX_MSG_NO1_8;
            ASIC_RX_MSG_NO_TC_9_t       ASIC_RX_MSG_NO1_9;
            ASIC_RX_MSG_NO_TC_10_t      ASIC_RX_MSG_NO1_10;
            ASIC_RX_MSG_NO_TC_12_t      ASIC_RX_MSG_NO1_12;
            ASIC_RX_MSG_NO_TC_13_t      ASIC_RX_MSG_NO1_13;
            ASIC_RX_MSG_NO_TC_14_t      ASIC_RX_MSG_NO1_14;
            /* NO CH2*/
            ASIC_RX_MSG_NO_TC_0_t       ASIC_RX_MSG_NO2_0;
            ASIC_RX_MSG_NO_TC_1_t       ASIC_RX_MSG_NO2_1;
            ASIC_RX_MSG_NO_TC_2_t       ASIC_RX_MSG_NO2_2;
            ASIC_RX_MSG_NO_TC_5_t       ASIC_RX_MSG_NO2_5;
            ASIC_RX_MSG_NO_TC_6_t       ASIC_RX_MSG_NO2_6;
            ASIC_RX_MSG_NO_TC_7_t       ASIC_RX_MSG_NO2_7;
            ASIC_RX_MSG_NO_TC_8_t       ASIC_RX_MSG_NO2_8;
            ASIC_RX_MSG_NO_TC_9_t       ASIC_RX_MSG_NO2_9;
            ASIC_RX_MSG_NO_TC_10_t      ASIC_RX_MSG_NO2_10;
            ASIC_RX_MSG_NO_TC_12_t      ASIC_RX_MSG_NO2_12;
            ASIC_RX_MSG_NO_TC_13_t      ASIC_RX_MSG_NO2_13;
            ASIC_RX_MSG_NO_TC_14_t      ASIC_RX_MSG_NO2_14;
            /* NO CH3*/
            ASIC_RX_MSG_NO_TC_0_t       ASIC_RX_MSG_NO3_0;
            ASIC_RX_MSG_NO_TC_1_t       ASIC_RX_MSG_NO3_1;
            ASIC_RX_MSG_NO_TC_2_t       ASIC_RX_MSG_NO3_2;
            ASIC_RX_MSG_NO_TC_5_t       ASIC_RX_MSG_NO3_5;
            ASIC_RX_MSG_NO_TC_6_t       ASIC_RX_MSG_NO3_6;
            ASIC_RX_MSG_NO_TC_7_t       ASIC_RX_MSG_NO3_7;
            ASIC_RX_MSG_NO_TC_8_t       ASIC_RX_MSG_NO3_8;
            ASIC_RX_MSG_NO_TC_9_t       ASIC_RX_MSG_NO3_9;
            ASIC_RX_MSG_NO_TC_10_t      ASIC_RX_MSG_NO3_10;
            ASIC_RX_MSG_NO_TC_12_t      ASIC_RX_MSG_NO3_12;
            ASIC_RX_MSG_NO_TC_13_t      ASIC_RX_MSG_NO3_13;
            ASIC_RX_MSG_NO_TC_14_t      ASIC_RX_MSG_NO3_14;
            /* TC CH0*/
            ASIC_RX_MSG_NO_TC_0_t       ASIC_RX_MSG_TC0_0;
            ASIC_RX_MSG_NO_TC_1_t       ASIC_RX_MSG_TC0_1;
            ASIC_RX_MSG_NO_TC_2_t       ASIC_RX_MSG_TC0_2;
            ASIC_RX_MSG_NO_TC_5_t       ASIC_RX_MSG_TC0_5;
            ASIC_RX_MSG_NO_TC_6_t       ASIC_RX_MSG_TC0_6;
            ASIC_RX_MSG_NO_TC_7_t       ASIC_RX_MSG_TC0_7;
            ASIC_RX_MSG_NO_TC_8_t       ASIC_RX_MSG_TC0_8;
            ASIC_RX_MSG_NO_TC_9_t       ASIC_RX_MSG_TC0_9;
            ASIC_RX_MSG_NO_TC_10_t      ASIC_RX_MSG_TC0_10;
            ASIC_RX_MSG_NO_TC_12_t      ASIC_RX_MSG_TC0_12;
            ASIC_RX_MSG_NO_TC_13_t      ASIC_RX_MSG_TC0_13;
            ASIC_RX_MSG_NO_TC_14_t      ASIC_RX_MSG_TC0_14;
            /* TC CH1*/
            ASIC_RX_MSG_NO_TC_0_t       ASIC_RX_MSG_TC1_0;
            ASIC_RX_MSG_NO_TC_1_t       ASIC_RX_MSG_TC1_1;
            ASIC_RX_MSG_NO_TC_2_t       ASIC_RX_MSG_TC1_2;
            ASIC_RX_MSG_NO_TC_5_t       ASIC_RX_MSG_TC1_5;
            ASIC_RX_MSG_NO_TC_6_t       ASIC_RX_MSG_TC1_6;
            ASIC_RX_MSG_NO_TC_7_t       ASIC_RX_MSG_TC1_7;
            ASIC_RX_MSG_NO_TC_8_t       ASIC_RX_MSG_TC1_8;
            ASIC_RX_MSG_NO_TC_9_t       ASIC_RX_MSG_TC1_9;
            ASIC_RX_MSG_NO_TC_10_t      ASIC_RX_MSG_TC1_10;
            ASIC_RX_MSG_NO_TC_12_t      ASIC_RX_MSG_TC1_12;
            ASIC_RX_MSG_NO_TC_13_t      ASIC_RX_MSG_TC1_13;
            ASIC_RX_MSG_NO_TC_14_t      ASIC_RX_MSG_TC1_14;

            /* ESV Valve */
            /* ESV CH0 */
            ASIC_RX_MSG_ESV_1_t         ASIC_RX_MSG_ESV0_1;
            ASIC_RX_MSG_ESV_2_t         ASIC_RX_MSG_ESV0_2;
            ASIC_RX_MSG_ESV_5_t         ASIC_RX_MSG_ESV0_5;
            ASIC_RX_MSG_ESV_6_t         ASIC_RX_MSG_ESV0_6;
            ASIC_RX_MSG_ESV_8_t         ASIC_RX_MSG_ESV0_8;
            ASIC_RX_MSG_ESV_9_t         ASIC_RX_MSG_ESV0_9;
            ASIC_RX_MSG_ESV_10_t        ASIC_RX_MSG_ESV0_10;
            ASIC_RX_MSG_ESV_12_t        ASIC_RX_MSG_ESV0_12;
            /* ESV CH0 */
            ASIC_RX_MSG_ESV_1_t         ASIC_RX_MSG_ESV1_1;
            ASIC_RX_MSG_ESV_2_t         ASIC_RX_MSG_ESV1_2;
            ASIC_RX_MSG_ESV_5_t         ASIC_RX_MSG_ESV1_5;
            ASIC_RX_MSG_ESV_6_t         ASIC_RX_MSG_ESV1_6;
            ASIC_RX_MSG_ESV_8_t         ASIC_RX_MSG_ESV1_8;
            ASIC_RX_MSG_ESV_9_t         ASIC_RX_MSG_ESV1_9;
            ASIC_RX_MSG_ESV_10_t        ASIC_RX_MSG_ESV1_10;
            ASIC_RX_MSG_ESV_12_t        ASIC_RX_MSG_ESV1_12;        
            }M;
    }A;
}ASIC_RX_MSG_t;


/************************* SIGNAL ENUM ****************************/
typedef enum
{
    ASIC_RX_MSG_8mu8_Rx_8_PUMP_DTY_PWM = 0u,      /* Notice : The order should be the same as ACH_MsgIdType */
    ASIC_RX_MSG_8mu8_Rx_8_PUMP_TCK_PWM,
    ASIC_RX_MSG_1mu2_Rx_1_VDD3_OVC_FLT_TIME,
    ASIC_RX_MSG_1mu2_Rx_1_VDD_3_OVC_SD_RESTART_TIME,
    ASIC_RX_MSG_1mu2_Rx_1_VDD4_OVC_FLT_TIME,
    ASIC_RX_MSG_1mu2_Rx_1_VDD_4_OVC_SD_RESTART_TIME,
    ASIC_RX_MSG_1mu1_Rx_1_OSC_SpreadSpectrum_DIS,
    ASIC_RX_MSG_1mu1_Rx_1_PMP_EN,
    ASIC_RX_MSG_1mu1_Rx_1_FS_EN,
    ASIC_RX_MSG_1mu1_Rx_1_FS_CMD,
    ASIC_RX_MSG_1mu2_Rx_1_FS_VDS_TH,
    ASIC_RX_MSG_1mu1_Rx_1_PHOLD,
    ASIC_RX_MSG_1mu1_Rx_1_KA,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_0,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_1,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_2,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_3,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_4,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_5,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_6,
    ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_7,

    ASIC_RX_MSG_65mu1_Rx_65_CH0_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH0_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH1_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH1_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH2_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH2_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH3_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH3_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH4_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH4_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH5_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH5_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH6_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH6_STATUS_H,
    ASIC_RX_MSG_65mu1_Rx_65_CH7_STATUS_L,
    ASIC_RX_MSG_65mu1_Rx_65_CH7_STATUS_H,

    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_0,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_1,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_2,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_3,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_4,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_5,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_6,
    ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_7,
    ASIC_RX_MSG_0mu16_Rx_0_CHIP_ID,
    ASIC_RX_MSG_2mu1_Rx_2_VDD2_DIS,
    ASIC_RX_MSG_2mu1_Rx_2_VDD5_DIS,
    ASIC_RX_MSG_2mu1_Rx_2_WD_DIS,
    ASIC_RX_MSG_2mu1_Rx_2_VDD1_DIODELOSS_FILT,
    ASIC_RX_MSG_2mu1_Rx_2_VDD_2d5_AUTO_SWITCH_OFF,
    ASIC_RX_MSG_2mu2_Rx_2_WD_SW_DIS,
    ASIC_RX_MSG_3mu1_Rx_3_OSC_STUCK_MON,
    ASIC_RX_MSG_3mu1_Rx_3_OSC_FRQ_MON,
    ASIC_RX_MSG_3mu1_Rx_3_OSC_SELF_TEST_RESULT,
    ASIC_RX_MSG_3mu1_Rx_3_T_SD_INT,
    ASIC_RX_MSG_3mu1_Rx_3_WDOG_TO,
    ASIC_RX_MSG_3mu1_Rx_3_CP_OV,
    ASIC_RX_MSG_3mu1_Rx_3_CP_UV,
    ASIC_RX_MSG_3mu1_Rx_3_FS_TURN_OFF,
    ASIC_RX_MSG_3mu1_Rx_3_FS_TURN_ON,
    ASIC_RX_MSG_3mu1_Rx_3_FS_VDS_FAULT,
    ASIC_RX_MSG_3mu1_Rx_3_FS_RVP_FAULT,
    ASIC_RX_MSG_3mu1_Rx_3_VHD_OV,
    ASIC_RX_MSG_3mu1_Rx_3_VPWR_OV,
    ASIC_RX_MSG_3mu1_Rx_3_VPWR_UV,
    ASIC_RX_MSG_4mu8_Rx_4_DIE_TEMP_MONITOR,
    ASIC_RX_MSG_5mu1_Rx_5_TOO_LONG,
    ASIC_RX_MSG_5mu1_Rx_5_TOO_SHORT,
    ASIC_RX_MSG_5mu1_Rx_5_WRONG_CRC,
    ASIC_RX_MSG_5mu1_Rx_5_WRONG_FCNT,
    ASIC_RX_MSG_5mu1_Rx_5_WRONG_ADD,
    ASIC_RX_MSG_6mu1_Rx_6_ANALOG_BIST_EN,
    ASIC_RX_MSG_6mu1_Rx_6_CORE_BIST_EN,
    ASIC_RX_MSG_6mu1_Rx_6_NVM_CRC_MASK,
    ASIC_RX_MSG_6mu1_Rx_6_NVM_DWN,
    ASIC_RX_MSG_7mu1_Rx_7_AN_TRIM_CRC_RESULT,
    ASIC_RX_MSG_7mu1_Rx_7_NVM_CRC_RESULT,
    ASIC_RX_MSG_7mu1_Rx_7_NVM_BUSY,
    ASIC_RX_MSG_9mu2_Rx_9_PMP_VDS_TH,
    ASIC_RX_MSG_9mu1_Rx_9_PMP2_DIS,
    ASIC_RX_MSG_9mu3_Rx_9_PMP_VDS_FIL,
    ASIC_RX_MSG_9mu1_Rx_9_LDACT_DIS,
    ASIC_RX_MSG_9mu1_Rx_9_PMP1_ISINC,
    ASIC_RX_MSG_9mu4_Rx_9_PMP_DT,
    ASIC_RX_MSG_9mu1_Rx_9_PMP_TEST,
    ASIC_RX_MSG_9mu1_Rx_9_PMP_BIST_EN,
    ASIC_RX_MSG_10mu1_Rx_10_PMP1_TURN_ON,
    ASIC_RX_MSG_10mu1_Rx_10_PMP1_TURN_OFF,
    ASIC_RX_MSG_10mu1_Rx_10_PMP2_TURN_ON,
    ASIC_RX_MSG_10mu1_Rx_10_PMP2_TURN_OFF,
    ASIC_RX_MSG_10mu1_Rx_10_PMP3_TURN_ON,
    ASIC_RX_MSG_10mu1_Rx_10_PMP3_TURN_OFF,
    ASIC_RX_MSG_10mu1_Rx_10_PMP_VDS_TURNOFF,
    ASIC_RX_MSG_10mu1_Rx_10_PMP1_VDS_FAULT,
    ASIC_RX_MSG_10mu1_Rx_10_PMP_FLYBACK,
    ASIC_RX_MSG_10mu1_Rx_10_PMP_LD_ACT,
    ASIC_RX_MSG_10mu3_Rx_10_PMP_TEST_STATUS,
    ASIC_RX_MSG_10mu2_Rx_10_PMP_BIST_STATUS,
    ASIC_RX_MSG_11mu1_Rx_11_VDD1_OVC,
    ASIC_RX_MSG_11mu1_Rx_11_VDD2_OUT_OF_REG,
    ASIC_RX_MSG_11mu1_Rx_11_VDD4_OVC,
    ASIC_RX_MSG_11mu1_Rx_11_VDD3_OVC,
    ASIC_RX_MSG_11mu1_Rx_11_VDD1DIODE_LOSS_ECHO,
    ASIC_RX_MSG_11mu1_Rx_11_VDD2_REV_CURR,
    ASIC_RX_MSG_11mu1_Rx_11_VDD1_T_SD,
    ASIC_RX_MSG_11mu1_Rx_11_VDD2_T_SD,
    ASIC_RX_MSG_11mu1_Rx_11_VDD1_UV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD2_UV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD3_UV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD4_UV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD1_OV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD2_OV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD3_OV,
    ASIC_RX_MSG_11mu1_Rx_11_VDD4_OV,
    ASIC_RX_MSG_12mu1_Rx_12_VDD1_SEL_ECHO,
    ASIC_RX_MSG_12mu1_Rx_12_VDD2_SEL_ECHO,
    ASIC_RX_MSG_12mu2_Rx_12_RESERVED0,
    ASIC_RX_MSG_12mu1_Rx_12_VDD5_SEL_ECHO,
    ASIC_RX_MSG_12mu7_Rx_12_RESERVED1,
    ASIC_RX_MSG_12mu1_Rx_12_VDD5_OUT_OF_REG,
    ASIC_RX_MSG_12mu1_Rx_12_VDD5_T_SD,
    ASIC_RX_MSG_12mu1_Rx_12_VDD5_UV,
    ASIC_RX_MSG_12mu1_Rx_12_VDD5_OV,
    ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT,
    ASIC_RX_MSG_13mu4_Rx_13_ADC_SEL,
    ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY,
    ASIC_RX_MSG_15mu1_Rx_15_VSO_CONF,
    ASIC_RX_MSG_15mu2_Rx_15_WSO_VSO_S,
    ASIC_RX_MSG_15mu1_Rx_15_SSIG_VSO,
    ASIC_RX_MSG_15mu1_Rx_15_VSO_PTEN,
    ASIC_RX_MSG_15mu1_Rx_15_VSO_OFF_DIAG_EN,
    ASIC_RX_MSG_15mu1_Rx_15_VSO_LS_CLAMP_DIS,
    ASIC_RX_MSG_15mu1_Rx_15_SEL_CONF,
    ASIC_RX_MSG_15mu2_Rx_15_WSO_SEL_S,
    ASIC_RX_MSG_15mu1_Rx_15_SSIG_SEL,
    ASIC_RX_MSG_15mu1_Rx_15_SEL_OUT_CMD,
    ASIC_RX_MSG_15mu1_Rx_15_SEL_OFF_DIAG_EN,
    ASIC_RX_MSG_15mu1_Rx_15_SEL_LS_CLAMP_DIS,
    ASIC_RX_MSG_15mu1_Rx_15_SEL_PTEN,
    ASIC_RX_MSG_16mu1_Rx_16_VSO_T_SD,
    ASIC_RX_MSG_16mu1_Rx_16_VSO_OPENLOAD,
    ASIC_RX_MSG_16mu1_Rx_16_VSO_LVT,
    ASIC_RX_MSG_16mu1_Rx_16_VSO_VGS_LS_FAULT,
    ASIC_RX_MSG_16mu1_Rx_16_VSO_LS_CLAMP_ACT,
    ASIC_RX_MSG_16mu1_Rx_16_VSO_LS_OVC,
    ASIC_RX_MSG_16mu1_Rx_16_GND_LOSS,
    ASIC_RX_MSG_16mu1_Rx_16_SEL_T_SD,
    ASIC_RX_MSG_16mu1_Rx_16_SEL_OPENLOAD,
    ASIC_RX_MSG_16mu1_Rx_16_SEL_LVT,
    ASIC_RX_MSG_16mu1_Rx_16_SEL_VGS_LS_FAULT,
    ASIC_RX_MSG_16mu1_Rx_16_SEL_LS_CLAMP_ACT,
    ASIC_RX_MSG_16mu1_Rx_16_SEL_LS_OVC,
    ASIC_RX_MSG_17mu1_Rx_17_WLD_CONF,
    ASIC_RX_MSG_17mu1_Rx_17_WLD_CMD,
    ASIC_RX_MSG_17mu1_Rx_17_WLD_DIAGOFF_EN,
    ASIC_RX_MSG_17mu1_Rx_17_WLD_LS_CLAMP_DIS,
    ASIC_RX_MSG_17mu1_Rx_17_SHLS_CMD,
    ASIC_RX_MSG_17mu1_Rx_17_SHLS_DIAGOFF_EN,
    ASIC_RX_MSG_17mu1_Rx_17_SHLS_CONFIG,
    ASIC_RX_MSG_17mu1_Rx_17_SHLS_LS_CLAMP_DIS,
    ASIC_RX_MSG_18mu1_Rx_18_WLD_T_SD,
    ASIC_RX_MSG_18mu1_Rx_18_WLD_OPL,
    ASIC_RX_MSG_18mu1_Rx_18_WLD_LVT,
    ASIC_RX_MSG_18mu1_Rx_18_WLD_LS_OVC,
    ASIC_RX_MSG_18mu1_Rx_18_WLD_VGS_LS_FAULT,
    ASIC_RX_MSG_18mu1_Rx_18_WLD_LS_CLAMP,
    ASIC_RX_MSG_18mu1_Rx_18_SHLS_T_SD,
    ASIC_RX_MSG_18mu1_Rx_18_SHLS_OPL,
    ASIC_RX_MSG_18mu1_Rx_18_SHLS_LVT,
    ASIC_RX_MSG_18mu1_Rx_18_SHLS_OVC,
    ASIC_RX_MSG_18mu1_Rx_18_SHLS_VGS_FAULT,
    ASIC_RX_MSG_18mu1_Rx_18_SHLS_LS_CLAMP,
    ASIC_RX_MSG_19mu1_Rx_19_E_PVDS_GPO,
    ASIC_RX_MSG_19mu1_Rx_19_E_VDD2d5dWSx_OT_GPO,
    ASIC_RX_MSG_19mu1_Rx_19_E_FBO_GPO,
    ASIC_RX_MSG_19mu1_Rx_19_E_FSPI_GPO,
    ASIC_RX_MSG_19mu1_Rx_19_E_EN_GPO,
    ASIC_RX_MSG_19mu1_Rx_19_E_FS_VDS_GPO,
    ASIC_RX_MSG_20mu1_Rx_20_VINTA_OV,
    ASIC_RX_MSG_20mu1_Rx_20_DGNDLOSS,
    ASIC_RX_MSG_20mu1_Rx_20_VINTA_UV,
    ASIC_RX_MSG_20mu1_Rx_20_PORN,
    ASIC_RX_MSG_20mu1_Rx_20_RES1_ECHO,
    ASIC_RX_MSG_20mu1_Rx_20_IGN_ECHO,
    ASIC_RX_MSG_32mu1_Rx_32_WSO_TEST,
    ASIC_RX_MSG_32mu7_Rx_32_CONFIG_RANGE,
    ASIC_RX_MSG_33mu8_Rx_33_WSI_FIRST_THRESHOLD,
    ASIC_RX_MSG_34mu8_Rx_34_WSI_OFFSET_THRESHOLD,
    ASIC_RX_MSG_35mu2_Rx_35_SENSOR_TYPE_SELECTION,
    ASIC_RX_MSG_35mu1_Rx_35_FIX_TH,
    ASIC_RX_MSG_35mu1_Rx_35_SS_DIS,
    ASIC_RX_MSG_35mu1_Rx_35_PTEN,
    ASIC_RX_MSG_35mu2_Rx_35_FILTER_SELECTION,
    ASIC_RX_MSG_36mu2_Rx_36_SENSOR_TYPE_SELECTION,
    ASIC_RX_MSG_36mu1_Rx_36_FIX_TH,
    ASIC_RX_MSG_36mu1_Rx_36_SS_DIS,
    ASIC_RX_MSG_36mu1_Rx_36_PTEN,
    ASIC_RX_MSG_36mu2_Rx_36_FILTER_SELECTION,
    ASIC_RX_MSG_37mu2_Rx_37_SENSOR_TYPE_SELECTION,
    ASIC_RX_MSG_37mu1_Rx_37_FIX_TH,
    ASIC_RX_MSG_37mu1_Rx_37_SS_DIS,
    ASIC_RX_MSG_37mu1_Rx_37_PTEN,
    ASIC_RX_MSG_37mu2_Rx_37_FILTER_SELECTION,
    ASIC_RX_MSG_38mu2_Rx_38_SENSOR_TYPE_SELECTION,
    ASIC_RX_MSG_38mu1_Rx_38_FIX_TH,
    ASIC_RX_MSG_38mu1_Rx_38_SS_DIS,
    ASIC_RX_MSG_38mu1_Rx_38_PTEN,
    ASIC_RX_MSG_38mu2_Rx_38_FILTER_SELECTION,
    ASIC_RX_MSG_39mu1_Rx_39_CH1_EN,
    ASIC_RX_MSG_39mu1_Rx_39_CH2_EN,
    ASIC_RX_MSG_39mu1_Rx_39_CH3_EN,
    ASIC_RX_MSG_39mu1_Rx_39_CH4_EN,
    ASIC_RX_MSG_39mu1_Rx_39_DIAG,
    ASIC_RX_MSG_39mu1_Rx_39_INIT,
    ASIC_RX_MSG_39mu1_Rx_39_WSI_VDD4_UV,
    ASIC_RX_MSG_39mu1_Rx_39_FVPWR_ACT,
    ASIC_RX_MSG_40mu8_Rx_40_WS1_CNT,
    ASIC_RX_MSG_40mu1_Rx_40_WS_CNT_OV,
    ASIC_RX_MSG_40mu1_Rx_40_WS_CNT_EN,
    ASIC_RX_MSG_40mu1_Rx_40_WS_CNT_RST,
    ASIC_RX_MSG_41mu8_Rx_41_WS1_CNT,
    ASIC_RX_MSG_41mu1_Rx_41_WS_CNT_OV,
    ASIC_RX_MSG_41mu1_Rx_41_WS_CNT_EN,
    ASIC_RX_MSG_41mu1_Rx_41_WS_CNT_RST,
    ASIC_RX_MSG_42mu8_Rx_42_WS1_CNT,
    ASIC_RX_MSG_42mu1_Rx_42_WS_CNT_OV,
    ASIC_RX_MSG_42mu1_Rx_42_WS_CNT_EN,
    ASIC_RX_MSG_42mu1_Rx_42_WS_CNT_RST,
    ASIC_RX_MSG_43mu8_Rx_43_WS1_CNT,
    ASIC_RX_MSG_43mu1_Rx_43_WS_CNT_OV,
    ASIC_RX_MSG_43mu1_Rx_43_WS_CNT_EN,
    ASIC_RX_MSG_43mu1_Rx_43_WS_CNT_RST,
    ASIC_RX_MSG_44mu2_Rx_44_WSI1_LBIST_STATUS,
    ASIC_RX_MSG_44mu2_Rx_44_WSI2_LBIST_STATUS,
    ASIC_RX_MSG_44mu2_Rx_44_WSI3_LBIST_STATUS,
    ASIC_RX_MSG_44mu2_Rx_44_WSI4_LBIST_STATUS,
    ASIC_RX_MSG_44mu1_Rx_44_WSI1_LBIST_EN,
    ASIC_RX_MSG_44mu1_Rx_44_WSI2_LBIST_EN,
    ASIC_RX_MSG_44mu1_Rx_44_WSI3_LBIST_EN,
    ASIC_RX_MSG_44mu1_Rx_44_WSI4_LBIST_EN,
    ASIC_RX_MSG_44mu1_Rx_44_WSI1_SELFTEST_RESULT,
    ASIC_RX_MSG_44mu1_Rx_44_WSI2_SELFTEST_RESULT,
    ASIC_RX_MSG_44mu1_Rx_44_WSI3_SELFTEST_RESULT,
    ASIC_RX_MSG_44mu1_Rx_44_WSI4_SELFTEST_RESULT,
    /* MSG 48~63 WSS Data & Fault register */
    ASIC_RX_MSG_48mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT,
    ASIC_RX_MSG_49mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT,
    ASIC_RX_MSG_50mu10_Rx_WSIRSDR2_BASE_CURRENT,
    ASIC_RX_MSG_50mu1_Rx_WSIRSDR2_RESERVED0,
    ASIC_RX_MSG_50mu2_Rx_WSIRSDR2_LOGIC_CH_ID,
    ASIC_RX_MSG_51mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA,
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_LATCH_D0,
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_NO_FAULT,
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_STANDSTILL,
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_PTY_BIT,

    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_0,       
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_1,        
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_PULSE_OVERFLOW,
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_NODATA,
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_INVALID,        
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_WSI_OT,         
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_OPEN,           
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_CURRENT_HI,     
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_STB,   
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_STG,           
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_2,         
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_3,         
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ONOFF,          
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ONE,            
    ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_4,         
    

    ASIC_RX_MSG_52mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT,
    ASIC_RX_MSG_53mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT,
    ASIC_RX_MSG_54mu10_Rx_WSIRSDR2_BASE_CURRENT,
    ASIC_RX_MSG_54mu1_Rx_WSIRSDR2_RESERVED0,
    ASIC_RX_MSG_54mu2_Rx_WSIRSDR2_LOGIC_CH_ID,
    ASIC_RX_MSG_55mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA,
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_LATCH_D0,
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_NO_FAULT,
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_STANDSTILL,
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_PTY_BIT,

    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_0,       
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_1,        
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_PULSE_OVERFLOW,
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_NODATA,
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_INVALID,        
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_WSI_OT,         
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_OPEN,           
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_CURRENT_HI,     
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_STB,   
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_STG,           
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_2,         
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_3,         
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ONOFF,          
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ONE,            
    ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_4,         

    ASIC_RX_MSG_56mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT,
    ASIC_RX_MSG_57mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT,
    ASIC_RX_MSG_58mu10_Rx_WSIRSDR2_BASE_CURRENT,
    ASIC_RX_MSG_58mu2_Rx_WSIRSDR2_LOGIC_CH_ID,
    ASIC_RX_MSG_59mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA,
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_LATCH_D0,
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_NO_FAULT,
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_STANDSTILL,
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_PTY_BIT,

    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_0,       
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_1,        
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_PULSE_OVERFLOW,
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_NODATA,
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_INVALID,        
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_WSI_OT,         
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_OPEN,           
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_CURRENT_HI,     
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_STB,   
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_STG,           
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_2,         
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_3,         
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ONOFF,          
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ONE,            
    ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_4,         
            
    ASIC_RX_MSG_60mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT,
    ASIC_RX_MSG_61mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT,
    ASIC_RX_MSG_62mu10_Rx_WSIRSDR2_BASE_CURRENT,
    ASIC_RX_MSG_62mu2_Rx_WSIRSDR2_LOGIC_CH_ID,
    ASIC_RX_MSG_63mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA,
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_LATCH_D0,
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_NO_FAULT,
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_STANDSTILL,
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_PTY_BIT,

    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_0,       
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_1,        
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_PULSE_OVERFLOW,
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_NODATA,
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_INVALID,        
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_WSI_OT,         
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_OPEN,           
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_CURRENT_HI,     
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_STB,   
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_STG,           
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_2,         
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_3,         
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ONOFF,          
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ONE,            
    ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_4,         

    ASIC_RX_MSG_67mu1_Rx_67_CH1_DIAG_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH1_TD_BLANK,
    ASIC_RX_MSG_67mu1_Rx_67_CH2_DIAG_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH_2TD_BLANK,
    ASIC_RX_MSG_67mu1_Rx_67_CH3_DIAG_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH3_TD_BLANK,
    ASIC_RX_MSG_67mu1_Rx_67_CH4_DIAG_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH4_TD_BLANK,
    ASIC_RX_MSG_67mu1_Rx_67_CH1_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH2_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH3_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH4_EN,
    ASIC_RX_MSG_67mu1_Rx_67_CH1_CMD,
    ASIC_RX_MSG_67mu1_Rx_67_CH2_CMD,
    ASIC_RX_MSG_67mu1_Rx_67_CH3_CMD,
    ASIC_RX_MSG_67mu1_Rx_67_CH4_CMD,

    /*MSG68~71NCValveON/OFFChannelDiag*/
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_TH_WARN,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_T_SD,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_UNDER_CURR,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_OPL,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_LVT,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_GND_LOSS,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_VGS_LS_FAULT,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_LS_OVC,
    ASIC_RX_MSG_68mu1_Rx_CH_DIAG_LS_CLAMP,

    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_TH_WARN,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_T_SD,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_UNDER_CURR,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_OPL,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_LVT,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_GND_LOSS,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_VGS_LS_FAULT,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_LS_OVC,
    ASIC_RX_MSG_69mu1_Rx_CH_DIAG_LS_CLAMP,

    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_TH_WARN,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_T_SD,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_UNDER_CURR,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_OPL,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_LVT,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_GND_LOSS,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_VGS_LS_FAULT,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_LS_OVC,
    ASIC_RX_MSG_70mu1_Rx_CH_DIAG_LS_CLAMP,

    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_TH_WARN,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_T_SD,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_UNDER_CURR,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_OPL,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_LVT,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_GND_LOSS,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_VGS_LS_FAULT,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_LS_OVC,
    ASIC_RX_MSG_71mu1_Rx_CH_DIAG_LS_CLAMP,
    ASIC_RX_MSG_72mu8_Rx_72_SEED,
    ASIC_RX_MSG_73mu4_Rx_73_WD_CNT_VALUE,
    ASIC_RX_MSG_73mu1_Rx_73_WD_EARLY_ANSW,
    ASIC_RX_MSG_73mu1_Rx_73_WD_LATE_ANSW,
    ASIC_RX_MSG_73mu1_Rx_73_WD_BAD_ANSW,
    ASIC_RX_MSG_73mu1_Rx_73_WD_EARLY_REQ,
    ASIC_RX_MSG_73mu1_Rx_73_WD_LATE_REQ,
    ASIC_RX_MSG_73mu1_Rx_73_WD_RST_TO_ANSW,
    ASIC_RX_MSG_73mu1_Rx_73_WD_RST_TO_REQ,
    ASIC_RX_MSG_73mu1_Rx_73_WD_RST_CNT,
    ASIC_RX_MSG_73mu4_Rx_73_WD_RST_EVENT_VALUE,
    ASIC_RX_MSG_74mu8_Rx_74_VALID_ANSWER_START,
    ASIC_RX_MSG_74mu8_Rx_74_VALID_REQUEST_START,
    ASIC_RX_MSG_75mu6_Rx_75_VALID_ANSWER_DELTA,
    ASIC_RX_MSG_75mu6_Rx_75_VALID_REQUEST_DELTA,
    ASIC_RX_MSG_75mu1_Rx_75_REQ_CHECK_ENABLE,
    ASIC_RX_MSG_76mu6_Rx_76_ANSWER_TIME_OUT_DELTA,
    ASIC_RX_MSG_76mu6_Rx_76_REQUEST_TIME_OUT_DELTA,
    ASIC_RX_MSG_76mu1_Rx_76_TO_RESET_ENABLE,
    ASIC_RX_MSG_77mu3_Rx_77_NUM_OF_GOOD_STEPS,
    ASIC_RX_MSG_77mu3_Rx_77_NUM_OF_BAD_STEPS,
    ASIC_RX_MSG_77mu4_Rx_77_HIGH_TH,
    ASIC_RX_MSG_77mu4_Rx_77_LOW_TH,
    ASIC_RX_MSG_77mu1_Rx_77_CLOCK_DIVISION,
    ASIC_RX_MSG_77mu1_Rx_77_RESET_ENABLE,
    ASIC_RX_MSG_78mu8_Rx_78_ANSWER_LOW,
    ASIC_RX_MSG_78mu8_Rx_78_ANSWER_HIGH,
    /*NO/TCValve*/
    /*NOCH0*/
    ASIC_RX_MSG_NO0_0mu1_Rx_NT0_PWM_FAULT,
    ASIC_RX_MSG_NO0_0mu1_Rx_NT0_CURR_SENSE,
    ASIC_RX_MSG_NO0_0mu1_Rx_NT0_ADC_FAULT,
    ASIC_RX_MSG_NO0_0mu2_Rx_NT0_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_LS_CLAMP_ON,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_LS_OVC,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_VGS_LS_FAULT,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_VGS_HS_FAULT,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_HS_SHORT,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_LVT,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_OPEN_LOAD,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_GND_LOSS,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_TH_WARN,
    ASIC_RX_MSG_NO0_1mu1_Rx_NT1_T_SD,
    ASIC_RX_MSG_NO0_2mu2_Rx_NT2_SR_SEL,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_CALIBRATION_DIS,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_OFS_CHOP_DIS,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_TD_BLANK_SEL,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_LS_CLAMP_DIS,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_LOGIC_BIST_EN,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_DIAG_BIST_EN,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_ADC_BIST_EN,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_OFF_DIAG_EN,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_E_SOL_GPO,
    ASIC_RX_MSG_NO0_2mu1_Rx_NT2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_NO0_5mu11_Rx_NT5_SET_POINT,
    ASIC_RX_MSG_NO0_6mu10_Rx_NT6_PWM_CODE,
    ASIC_RX_MSG_NO0_7mu8_Rx_NT7_MAX_FREQ_DELTA,
    ASIC_RX_MSG_NO0_7mu7_Rx_NT7_FREQ_MOD_STEP,
    ASIC_RX_MSG_NO0_8mu3_Rx_NT8_KP,
    ASIC_RX_MSG_NO0_8mu3_Rx_NT8_KI,
    ASIC_RX_MSG_NO0_9mu2_Rx_NT9_INTTIME,
    ASIC_RX_MSG_NO0_9mu2_Rx_NT9_FILT_TIME,
    ASIC_RX_MSG_NO0_9mu1_Rx_NT9_CHOP_TIME,
    ASIC_RX_MSG_NO0_10mu12_Rx_NT10_AVG_CURRENT,
    ASIC_RX_MSG_NO0_12mu13_Rx_NT12_PWM_CODE,
    ASIC_RX_MSG_NO0_12mu1_Rx_NT12_TMOUT,
    ASIC_RX_MSG_NO0_13mu8_Rx_NT13_BASE_DELTA_CURENT,
    ASIC_RX_MSG_NO0_14mu8_Rx_NT14_DELTA_CURRENT,

    /*NOCH1*/
    ASIC_RX_MSG_NO1_0mu1_Rx_NT0_PWM_FAULT,
    ASIC_RX_MSG_NO1_0mu1_Rx_NT0_CURR_SENSE,
    ASIC_RX_MSG_NO1_0mu1_Rx_NT0_ADC_FAULT,
    ASIC_RX_MSG_NO1_0mu2_Rx_NT0_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_LS_CLAMP_ON,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_LS_OVC,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_VGS_LS_FAULT,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_VGS_HS_FAULT,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_HS_SHORT,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_LVT,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_OPEN_LOAD,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_GND_LOSS,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_TH_WARN,
    ASIC_RX_MSG_NO1_1mu1_Rx_NT1_T_SD,
    ASIC_RX_MSG_NO1_2mu2_Rx_NT2_SR_SEL,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_CALIBRATION_DIS,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_OFS_CHOP_DIS,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_TD_BLANK_SEL,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_LS_CLAMP_DIS,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_LOGIC_BIST_EN,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_DIAG_BIST_EN,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_ADC_BIST_EN,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_OFF_DIAG_EN,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_E_SOL_GPO,
    ASIC_RX_MSG_NO1_2mu1_Rx_NT2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_NO1_5mu11_Rx_NT5_SET_POINT,
    ASIC_RX_MSG_NO1_6mu10_Rx_NT6_PWM_CODE,
    ASIC_RX_MSG_NO1_7mu8_Rx_NT7_MAX_FREQ_DELTA,
    ASIC_RX_MSG_NO1_7mu7_Rx_NT7_FREQ_MOD_STEP,
    ASIC_RX_MSG_NO1_8mu3_Rx_NT8_KP,
    ASIC_RX_MSG_NO1_8mu3_Rx_NT8_KI,
    ASIC_RX_MSG_NO1_9mu2_Rx_NT9_INTTIME,
    ASIC_RX_MSG_NO1_9mu2_Rx_NT9_FILT_TIME,
    ASIC_RX_MSG_NO1_9mu1_Rx_NT9_CHOP_TIME,
    ASIC_RX_MSG_NO1_10mu12_Rx_NT10_AVG_CURRENT,
    ASIC_RX_MSG_NO1_12mu13_Rx_NT12_PWM_CODE,
    ASIC_RX_MSG_NO1_12mu1_Rx_NT12_TMOUT,
    ASIC_RX_MSG_NO1_13mu8_Rx_NT13_BASE_DELTA_CURENT,
    ASIC_RX_MSG_NO1_14mu8_Rx_NT14_DELTA_CURRENT,

    /*NOCH2*/
    ASIC_RX_MSG_NO2_0mu1_Rx_NT0_PWM_FAULT,
    ASIC_RX_MSG_NO2_0mu1_Rx_NT0_CURR_SENSE,
    ASIC_RX_MSG_NO2_0mu1_Rx_NT0_ADC_FAULT,
    ASIC_RX_MSG_NO2_0mu2_Rx_NT0_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_LS_CLAMP_ON,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_LS_OVC,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_VGS_LS_FAULT,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_VGS_HS_FAULT,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_HS_SHORT,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_LVT,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_OPEN_LOAD,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_GND_LOSS,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_TH_WARN,
    ASIC_RX_MSG_NO2_1mu1_Rx_NT1_T_SD,
    ASIC_RX_MSG_NO2_2mu2_Rx_NT2_SR_SEL,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_CALIBRATION_DIS,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_OFS_CHOP_DIS,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_TD_BLANK_SEL,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_LS_CLAMP_DIS,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_LOGIC_BIST_EN,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_DIAG_BIST_EN,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_ADC_BIST_EN,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_OFF_DIAG_EN,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_E_SOL_GPO,
    ASIC_RX_MSG_NO2_2mu1_Rx_NT2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_NO2_5mu11_Rx_NT5_SET_POINT,
    ASIC_RX_MSG_NO2_6mu10_Rx_NT6_PWM_CODE,
    ASIC_RX_MSG_NO2_7mu8_Rx_NT7_MAX_FREQ_DELTA,
    ASIC_RX_MSG_NO2_7mu7_Rx_NT7_FREQ_MOD_STEP,
    ASIC_RX_MSG_NO2_8mu3_Rx_NT8_KP,
    ASIC_RX_MSG_NO2_8mu3_Rx_NT8_KI,
    ASIC_RX_MSG_NO2_9mu2_Rx_NT9_INTTIME,
    ASIC_RX_MSG_NO2_9mu2_Rx_NT9_FILT_TIME,
    ASIC_RX_MSG_NO2_9mu1_Rx_NT9_CHOP_TIME,
    ASIC_RX_MSG_NO2_10mu12_Rx_NT10_AVG_CURRENT,
    ASIC_RX_MSG_NO2_12mu13_Rx_NT12_PWM_CODE,
    ASIC_RX_MSG_NO2_12mu1_Rx_NT12_TMOUT,
    ASIC_RX_MSG_NO2_13mu8_Rx_NT13_BASE_DELTA_CURENT,
    ASIC_RX_MSG_NO2_14mu8_Rx_NT14_DELTA_CURRENT,

    /*NOCH3*/
    ASIC_RX_MSG_NO3_0mu1_Rx_NT0_PWM_FAULT,
    ASIC_RX_MSG_NO3_0mu1_Rx_NT0_CURR_SENSE,
    ASIC_RX_MSG_NO3_0mu1_Rx_NT0_ADC_FAULT,
    ASIC_RX_MSG_NO3_0mu2_Rx_NT0_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_LS_CLAMP_ON,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_LS_OVC,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_VGS_LS_FAULT,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_VGS_HS_FAULT,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_HS_SHORT,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_LVT,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_OPEN_LOAD,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_GND_LOSS,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_TH_WARN,
    ASIC_RX_MSG_NO3_1mu1_Rx_NT1_T_SD,
    ASIC_RX_MSG_NO3_2mu2_Rx_NT2_SR_SEL,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_CALIBRATION_DIS,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_OFS_CHOP_DIS,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_TD_BLANK_SEL,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_LS_CLAMP_DIS,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_LOGIC_BIST_EN,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_DIAG_BIST_EN,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_ADC_BIST_EN,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_OFF_DIAG_EN,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_E_SOL_GPO,
    ASIC_RX_MSG_NO3_2mu1_Rx_NT2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_NO3_5mu11_Rx_NT5_SET_POINT,
    ASIC_RX_MSG_NO3_6mu10_Rx_NT6_PWM_CODE,
    ASIC_RX_MSG_NO3_7mu8_Rx_NT7_MAX_FREQ_DELTA,
    ASIC_RX_MSG_NO3_7mu7_Rx_NT7_FREQ_MOD_STEP,
    ASIC_RX_MSG_NO3_8mu3_Rx_NT8_KP,
    ASIC_RX_MSG_NO3_8mu3_Rx_NT8_KI,
    ASIC_RX_MSG_NO3_9mu2_Rx_NT9_INTTIME,
    ASIC_RX_MSG_NO3_9mu2_Rx_NT9_FILT_TIME,
    ASIC_RX_MSG_NO3_9mu1_Rx_NT9_CHOP_TIME,
    ASIC_RX_MSG_NO3_10mu12_Rx_NT10_AVG_CURRENT,
    ASIC_RX_MSG_NO3_12mu13_Rx_NT12_PWM_CODE,
    ASIC_RX_MSG_NO3_12mu1_Rx_NT12_TMOUT,
    ASIC_RX_MSG_NO3_13mu8_Rx_NT13_BASE_DELTA_CURENT,
    ASIC_RX_MSG_NO3_14mu8_Rx_NT14_DELTA_CURRENT,

    /*TCCH0*/
    ASIC_RX_MSG_TC0_0mu1_Rx_NT0_PWM_FAULT,
    ASIC_RX_MSG_TC0_0mu1_Rx_NT0_CURR_SENSE,
    ASIC_RX_MSG_TC0_0mu1_Rx_NT0_ADC_FAULT,
    ASIC_RX_MSG_TC0_0mu2_Rx_NT0_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_LS_CLAMP_ON,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_LS_OVC,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_VGS_LS_FAULT,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_VGS_HS_FAULT,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_HS_SHORT,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_LVT,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_OPEN_LOAD,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_GND_LOSS,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_TH_WARN,
    ASIC_RX_MSG_TC0_1mu1_Rx_NT1_T_SD,
    ASIC_RX_MSG_TC0_2mu2_Rx_NT2_SR_SEL,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_CALIBRATION_DIS,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_OFS_CHOP_DIS,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_TD_BLANK_SEL,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_LS_CLAMP_DIS,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_LOGIC_BIST_EN,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_DIAG_BIST_EN,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_ADC_BIST_EN,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_OFF_DIAG_EN,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_E_SOL_GPO,
    ASIC_RX_MSG_TC0_2mu1_Rx_NT2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_TC0_5mu11_Rx_NT5_SET_POINT,
    ASIC_RX_MSG_TC0_6mu10_Rx_NT6_PWM_CODE,
    ASIC_RX_MSG_TC0_7mu8_Rx_NT7_MAX_FREQ_DELTA,
    ASIC_RX_MSG_TC0_7mu7_Rx_NT7_FREQ_MOD_STEP,
    ASIC_RX_MSG_TC0_8mu3_Rx_NT8_KP,
    ASIC_RX_MSG_TC0_8mu3_Rx_NT8_KI,
    ASIC_RX_MSG_TC0_9mu2_Rx_NT9_INTTIME,
    ASIC_RX_MSG_TC0_9mu2_Rx_NT9_FILT_TIME,
    ASIC_RX_MSG_TC0_9mu1_Rx_NT9_CHOP_TIME,
    ASIC_RX_MSG_TC0_10mu12_Rx_NT10_AVG_CURRENT,
    ASIC_RX_MSG_TC0_12mu13_Rx_NT12_PWM_CODE,
    ASIC_RX_MSG_TC0_12mu1_Rx_NT12_TMOUT,
    ASIC_RX_MSG_TC0_13mu8_Rx_NT13_BASE_DELTA_CURENT,
    ASIC_RX_MSG_TC0_14mu8_Rx_NT14_DELTA_CURRENT,

    /*TCCH1*/
    ASIC_RX_MSG_TC1_0mu1_Rx_NT0_PWM_FAULT,
    ASIC_RX_MSG_TC1_0mu1_Rx_NT0_CURR_SENSE,
    ASIC_RX_MSG_TC1_0mu1_Rx_NT0_ADC_FAULT,
    ASIC_RX_MSG_TC1_0mu2_Rx_NT0_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_LS_CLAMP_ON,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_LS_OVC,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_VGS_LS_FAULT,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_VGS_HS_FAULT,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_HS_SHORT,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_LVT,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_OPEN_LOAD,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_GND_LOSS,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_TH_WARN,
    ASIC_RX_MSG_TC1_1mu1_Rx_NT1_T_SD,
    ASIC_RX_MSG_TC1_2mu2_Rx_NT2_SR_SEL,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_CALIBRATION_DIS,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_OFS_CHOP_DIS,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_TD_BLANK_SEL,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_LS_CLAMP_DIS,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_LOGIC_BIST_EN,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_DIAG_BIST_EN,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_ADC_BIST_EN,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_OFF_DIAG_EN,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_E_SOL_GPO,
    ASIC_RX_MSG_TC1_2mu1_Rx_NT2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_TC1_5mu11_Rx_NT5_SET_POINT,
    ASIC_RX_MSG_TC1_6mu10_Rx_NT6_PWM_CODE,
    ASIC_RX_MSG_TC1_7mu8_Rx_NT7_MAX_FREQ_DELTA,
    ASIC_RX_MSG_TC1_7mu7_Rx_NT7_FREQ_MOD_STEP,
    ASIC_RX_MSG_TC1_8mu3_Rx_NT8_KP,
    ASIC_RX_MSG_TC1_8mu3_Rx_NT8_KI,
    ASIC_RX_MSG_TC1_9mu2_Rx_NT9_INTTIME,
    ASIC_RX_MSG_TC1_9mu2_Rx_NT9_FILT_TIME,
    ASIC_RX_MSG_TC1_9mu1_Rx_NT9_CHOP_TIME,
    ASIC_RX_MSG_TC1_10mu12_Rx_NT10_AVG_CURRENT,
    ASIC_RX_MSG_TC1_12mu13_Rx_NT12_PWM_CODE,
    ASIC_RX_MSG_TC1_12mu1_Rx_NT12_TMOUT,
    ASIC_RX_MSG_TC1_13mu8_Rx_NT13_BASE_DELTA_CURENT,
    ASIC_RX_MSG_TC1_14mu8_Rx_NT14_DELTA_CURRENT,


    /*ESVValve*/
    /*ESVCH0*/
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_LS_CLAMP_ON,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_LS_OVC,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_VGS_LS_FAULT,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_LVT,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_OPEN_LOAD,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_GND_LOSS,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_PWM_FAULT,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_TH_WARN,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_T_SD,
    ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_ADC_FAULT,
    ASIC_RX_MSG_ESV0_1mu2_Rx_ESV1_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_ESV0_2mu2_Rx_ESV2_SR_SEL,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_CALIBRATION_DIS,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_OFS_CHOP_DIS,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_TD_BLANK_SEL,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_LS_CLAMP_DIS,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_LOGIC_BIST_EN,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_DIAG_BIST_EN,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_ADC_BIST_EN,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_OFF_DIAG_EN,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_E_SOL_GPO,
    ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_ESV0_5mu11_Rx_ESV5_SET_POINT,
    ASIC_RX_MSG_ESV0_6mu6_Rx_ESV6_PWM_CODE,
    ASIC_RX_MSG_ESV0_6mu4_Rx_ESV6_FREQ_MOD_STEP,
    ASIC_RX_MSG_ESV0_6mu5_Rx_ESV6_MAX_FREQ_DELTA,
    ASIC_RX_MSG_ESV0_8mu3_Rx_ESV8_KP,
    ASIC_RX_MSG_ESV0_8mu3_Rx_ESV8_KI,
    ASIC_RX_MSG_ESV0_9mu2_Rx_ESV9_INTTIME,
    ASIC_RX_MSG_ESV0_9mu2_Rx_ESV9_FILT_TIME,
    ASIC_RX_MSG_ESV0_9mu1_Rx_ESV9_CHOP_TIME,
    ASIC_RX_MSG_ESV0_10mu11_Rx_ESV10_AVG_CURRENT,
    ASIC_RX_MSG_ESV0_12mu12_Rx_ESV12_PWM_CODE,
    ASIC_RX_MSG_ESV0_12mu1_Rx_ESV12_TMOUT,

    /*ESVCH1*/
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_LS_CLAMP_ON,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_LS_OVC,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_VGS_LS_FAULT,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_LVT,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_OPEN_LOAD,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_GND_LOSS,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_PWM_FAULT,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_TH_WARN,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_T_SD,
    ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_ADC_FAULT,
    ASIC_RX_MSG_ESV1_1mu2_Rx_ESV1_LOGIC_BIST_STATUS,
    ASIC_RX_MSG_ESV1_2mu2_Rx_ESV2_SR_SEL,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_CALIBRATION_DIS,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_OFS_CHOP_DIS,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_TD_BLANK_SEL,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_LS_CLAMP_DIS,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_LOGIC_BIST_EN,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_DIAG_BIST_EN,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_ADC_BIST_EN,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_OFF_DIAG_EN,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_E_SOL_GPO,
    ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_E_LSCLAMP_GPO,
    ASIC_RX_MSG_ESV1_5mu11_Rx_ESV5_SET_POINT,
    ASIC_RX_MSG_ESV1_6mu6_Rx_ESV6_PWM_CODE,
    ASIC_RX_MSG_ESV1_6mu4_Rx_ESV6_FREQ_MOD_STEP,
    ASIC_RX_MSG_ESV1_6mu5_Rx_ESV6_MAX_FREQ_DELTA,
    ASIC_RX_MSG_ESV1_8mu3_Rx_ESV8_KP,
    ASIC_RX_MSG_ESV1_8mu3_Rx_ESV8_KI,
    ASIC_RX_MSG_ESV1_9mu2_Rx_ESV9_INTTIME,
    ASIC_RX_MSG_ESV1_9mu2_Rx_ESV9_FILT_TIME,
    ASIC_RX_MSG_ESV1_9mu1_Rx_ESV9_CHOP_TIME,
    ASIC_RX_MSG_ESV1_10mu11_Rx_ESV10_AVG_CURRENT,
    ASIC_RX_MSG_ESV1_12mu12_Rx_ESV12_PWM_CODE,
    ASIC_RX_MSG_ESV1_12mu1_Rx_ESV12_TMOUT,

    ASIC_RX_MSG_ZERO_RETURN
    
}ACH_SigIdType;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

static const uint16 ACH_MsgAddArray[ACH_MSGID_MAX] =
{
    
    {ASIC_MSG8_ID_PUMPCFG},            /* NOTICE : MSG8,1,64,65 are sent first because they are actuator related AND this should be matched with ACH_MsgIdType enum */
    {ASIC_MSG1_ID_GENCFG},             
    {ASIC_MSG64_ID_SOLSERVENA},        
    {ASIC_MSG65_ID_SOLENDR},           
    {ASIC_MSG64_ID_SOLSERVENA},        /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    {ASIC_MSG0_ID_CHIPID},             
    {ASIC_MSG2_ID_GENCFG2},            
    {ASIC_MSG3_ID_GENSTATUS},          
    {ASIC_MSG4_ID_TEMPSENS},           
    {ASIC_MSG5_ID_COMM_FAULTS},        
    {ASIC_MSG6_ID_SERVTESTEN},         
    {ASIC_MSG7_ID_SERVTESTSTATUS},     
    {ASIC_MSG9_ID_PUMPCFG2},           
    {ASIC_MSG10_ID_PUMPDRD},           
    {ASIC_MSG11_ID_VDDFLT},            
    {ASIC_MSG12_ID_VDDSTATUS},         
    {ASIC_MSG13_ID_ADC_DR},            
    {ASIC_MSG15_ID_VSOSER},            
    {ASIC_MSG16_ID_VSODR},             
    {ASIC_MSG17_ID_WLD1SHLS_CONFIG},   
    {ASIC_MSG18_ID_WLD1SHLSDR},        
    {ASIC_MSG19_ID_SERVFLTMSK},        
    {ASIC_MSG20_ID_RESOUTPUT},         
    {ASIC_MSG32_ID_WSITEST},           
    {ASIC_MSG33_ID_WSIAUXCONF},        
    {ASIC_MSG34_ID_WSIAUXCONF2},       
    {ASIC_MSG35_ID_WSIRSCR1},          
    {ASIC_MSG36_ID_WSIRSCR2},          
    {ASIC_MSG37_ID_WSIRSCR3},          
    {ASIC_MSG38_ID_WSIRSCR4},          
    {ASIC_MSG39_ID_WSICTRL},           
    {ASIC_MSG40_ID_WSCOUNT1},          
    {ASIC_MSG41_ID_WSCOUNT2},          
    {ASIC_MSG42_ID_WSCOUNT3},          
    {ASIC_MSG43_ID_WSCOUNT4},          
    {ASIC_MSG44_ID_WSBIST},            
    {ASIC_MSG48_ID_WSIRSDR1},          
    {ASIC_MSG49_ID_WSIRSDR1},          
    {ASIC_MSG50_ID_WSIRSDR1},          
    {ASIC_MSG51_ID_WSIRSDR1},          
    {ASIC_MSG52_ID_WSIRSDR2},          
    {ASIC_MSG53_ID_WSIRSDR2},          
    {ASIC_MSG54_ID_WSIRSDR2},          
    {ASIC_MSG55_ID_WSIRSDR2},          
    {ASIC_MSG56_ID_WSIRSDR3},          
    {ASIC_MSG57_ID_WSIRSDR3},          
    {ASIC_MSG58_ID_WSIRSDR3},          
    {ASIC_MSG59_ID_WSIRSDR3},          
    {ASIC_MSG60_ID_WSIRSDR4},          
    {ASIC_MSG61_ID_WSIRSDR4},          
    {ASIC_MSG62_ID_WSIRSDR4},          
    {ASIC_MSG63_ID_WSIRSDR4},          
    {ASIC_MSG67_ID_ONOFF_CH_CONFIG},   
    {ASIC_MSG68_ID_ONOFF_CH_1_DR},     
    {ASIC_MSG69_ID_ONOFF_CH_2_DR},     
    {ASIC_MSG70_ID_ONOFF_CH_3_DR},     
    {ASIC_MSG71_ID_ONOFF_CH_4_DR},     
    {ASIC_MSG72_ID_WDG2SEED},          
    {ASIC_MSG73_ID_WDG2STATUS},        
    {ASIC_MSG74_ID_WDG2STARTTMG},      
    {ASIC_MSG75_ID_WDG2DELTATMG},      
    {ASIC_MSG76_ID_WDG2TOUTTMG},       
    {ASIC_MSG77_ID_WDG2PGM},           
    {ASIC_MSG78_ID_WDG2ANS},           
    {ASIC_MSG90_ID_TEST_MODE_ENTER},   
    {ASIC_MSG128_NO0_CHX_FAULT},          
    {ASIC_MSG129_NO0_EXCEPTIONS_CHX},     
    {ASIC_MSG130_NO0_CONFIGURATION_CHX},  
    {ASIC_MSG133_NO0_SETPOINT_CHX},       
    {ASIC_MSG134_NO0_CTRLCFG_CHX},        
    {ASIC_MSG135_NO0_FMODULATION_CHX},    
    {ASIC_MSG136_NO0_KGAINS_CHX},         
    {ASIC_MSG137_NO0_OFFCMP_CHX},         
    {ASIC_MSG138_NO0_AVGCURR_CHX},        
    {ASIC_MSG140_NO0_PWMSENSE_CHX},       
    {ASIC_MSG141_NO0_BASE_DELTA_CURR},    
    {ASIC_MSG142_NO0_DELTA_CURR},         
    {ASIC_MSG144_NO1_CHX_FAULT},          
    {ASIC_MSG145_NO1_EXCEPTIONS_CHX},     
    {ASIC_MSG146_NO1_CONFIGURATION_CHX},  
    {ASIC_MSG149_NO1_SETPOINT_CHX},       
    {ASIC_MSG150_NO1_CTRLCFG_CHX},        
    {ASIC_MSG151_NO1_FMODULATION_CHX},    
    {ASIC_MSG152_NO1_KGAINS_CHX},         
    {ASIC_MSG153_NO1_OFFCMP_CHX},         
    {ASIC_MSG154_NO1_AVGCURR_CHX},        
    {ASIC_MSG156_NO1_PWMSENSE_CHX},       
    {ASIC_MSG157_NO1_BASE_DELTA_CURR},    
    {ASIC_MSG158_NO1_DELTA_CURR},         
    {ASIC_MSG160_NO2_CHX_FAULT},          
    {ASIC_MSG161_NO2_EXCEPTIONS_CHX},     
    {ASIC_MSG162_NO2_CONFIGURATION_CHX},  
    {ASIC_MSG165_NO2_SETPOINT_CHX},       
    {ASIC_MSG166_NO2_CTRLCFG_CHX},        
    {ASIC_MSG167_NO2_FMODULATION_CHX},    
    {ASIC_MSG168_NO2_KGAINS_CHX},         
    {ASIC_MSG169_NO2_OFFCMP_CHX},         
    {ASIC_MSG170_NO2_AVGCURR_CHX},        
    {ASIC_MSG172_NO2_PWMSENSE_CHX},       
    {ASIC_MSG173_NO2_BASE_DELTA_CURR},    
    {ASIC_MSG174_NO2_DELTA_CURR},         
    {ASIC_MSG176_NO3_CHX_FAULT},          
    {ASIC_MSG177_NO3_EXCEPTIONS_CHX},     
    {ASIC_MSG178_NO3_CONFIGURATION_CHX},  
    {ASIC_MSG181_NO3_SETPOINT_CHX},       
    {ASIC_MSG182_NO3_CTRLCFG_CHX},        
    {ASIC_MSG183_NO3_FMODULATION_CHX},    
    {ASIC_MSG184_NO3_KGAINS_CHX},         
    {ASIC_MSG185_NO3_OFFCMP_CHX},         
    {ASIC_MSG186_NO3_AVGCURR_CHX},        
    {ASIC_MSG188_NO3_PWMSENSE_CHX},       
    {ASIC_MSG189_NO3_BASE_DELTA_CURR},    
    {ASIC_MSG190_NO3_DELTA_CURR},         
    {ASIC_MSG192_TC0_CHX_FAULT},          
    {ASIC_MSG193_TC0_EXCEPTIONS_CHX},     
    {ASIC_MSG194_TC0_CONFIGURATION_CHX},  
    {ASIC_MSG197_TC0_SETPOINT_CHX},       
    {ASIC_MSG198_TC0_CTRLCFG_CHX},        
    {ASIC_MSG199_TC0_FMODULATION_CHX},    
    {ASIC_MSG200_TC0_KGAINS_CHX},         
    {ASIC_MSG201_TC0_OFFCMP_CHX},         
    {ASIC_MSG202_TC0_AVGCURR_CHX},        
    {ASIC_MSG204_TC0_PWMSENSE_CHX},       
    {ASIC_MSG205_TC0_BASE_DELTA_CURR},    
    {ASIC_MSG206_TC0_DELTA_CURR},         
    {ASIC_MSG208_TC1_CHX_FAULT},          
    {ASIC_MSG209_TC1_EXCEPTIONS_CHX},     
    {ASIC_MSG210_TC1_CONFIGURATION_CHX},  
    {ASIC_MSG213_TC1_SETPOINT_CHX},       
    {ASIC_MSG214_TC1_CTRLCFG_CHX},        
    {ASIC_MSG215_TC1_FMODULATION_CHX},    
    {ASIC_MSG216_TC1_KGAINS_CHX},         
    {ASIC_MSG217_TC1_OFFCMP_CHX},         
    {ASIC_MSG218_TC1_AVGCURR_CHX},        
    {ASIC_MSG220_TC1_PWMSENSE_CHX},       
    {ASIC_MSG221_TC1_BASE_DELTA_CURR},    
    {ASIC_MSG222_TC1_DELTA_CURR},         
    {ASIC_MSG225_ESV0_EXCEPTIONS_CHX},    
    {ASIC_MSG226_ESV0_CONFIGURATION_CHX}, 
    {ASIC_MSG229_ESV0_SETPOINT_CHX},      
    {ASIC_MSG230_ESV0_CTRLCFG_CHX},       
    {ASIC_MSG232_ESV0_KGAINS_CHX},        
    {ASIC_MSG233_ESV0_OFFCMP_CHX},        
    {ASIC_MSG234_ESV0_AVGCURR_CHX},       
    {ASIC_MSG236_ESV0_PWMSENSE_CHX},      
    {ASIC_MSG241_ESV1_EXCEPTIONS_CHX},    
    {ASIC_MSG242_ESV1_CONFIGURATION_CHX}, 
    {ASIC_MSG245_ESV1_SETPOINT_CHX},      
    {ASIC_MSG246_ESV1_CTRLCFG_CHX},       
    {ASIC_MSG248_ESV1_KGAINS_CHX},        
    {ASIC_MSG249_ESV1_OFFCMP_CHX},        
    {ASIC_MSG250_ESV1_AVGCURR_CHX},       
    {ASIC_MSG252_ESV1_PWMSENSE_CHX}      

};

typedef union
{
    uint32    CrcUnion;
    struct
    {
        uint8 CrcByte[4];
    }B;
}Crc5Type;


/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern ACH_TxFrameType  ACH_TxFrame[MAX_BUFFER_SIZE];
extern ACH_MsgConfigType ACH_MsgConfig[ACH_MSGID_MAX];
extern ACH_RxFrameType  ACH_RxFrame[MAX_BUFFER_SIZE];
extern uint8 ACH_FrameCount;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern uint8 Ach_CRC5Gen(uint32 DATA);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACH_TYPES_H_ */
/** @} */
