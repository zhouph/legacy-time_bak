#include "unity.h"
#include "unity_fixture.h"
#include "Ach_Output.h"
#include "Ach_Output_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Ach_OutputAkaKeepAliveWriteData_KeepAliveDataFlg[MAX_STEP] = ACH_OUTPUTAKAKEEPALIVEWRITEDATA_KEEPALIVEDATAFLG;
const Saluint8 UtInput_Ach_OutputAkaKeepAliveWriteData_ACH_Tx1_PHOLD[MAX_STEP] = ACH_OUTPUTAKAKEEPALIVEWRITEDATA_ACH_TX1_PHOLD;
const Saluint8 UtInput_Ach_OutputAkaKeepAliveWriteData_ACH_Tx1_KA[MAX_STEP] = ACH_OUTPUTAKAKEEPALIVEWRITEDATA_ACH_TX1_KA;
const Haluint8 UtInput_Ach_OutputIocDcMtrDutyData_MtrDutyDataFlg[MAX_STEP] = ACH_OUTPUTIOCDCMTRDUTYDATA_MTRDUTYDATAFLG;
const Haluint8 UtInput_Ach_OutputIocDcMtrDutyData_ACH_Tx8_PUMP_DTY_PWM[MAX_STEP] = ACH_OUTPUTIOCDCMTRDUTYDATA_ACH_TX8_PUMP_DTY_PWM;
const Haluint8 UtInput_Ach_OutputIocDcMtrFreqData_MtrFreqDataFlg[MAX_STEP] = ACH_OUTPUTIOCDCMTRFREQDATA_MTRFREQDATAFLG;
const Haluint8 UtInput_Ach_OutputIocDcMtrFreqData_ACH_Tx8_PUMP_TCK_PWM[MAX_STEP] = ACH_OUTPUTIOCDCMTRFREQDATA_ACH_TX8_PUMP_TCK_PWM;
const Haluint8 UtInput_Ach_OutputIocVlvNo0Data_VlvNo0DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNO0DATA_VLVNO0DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNo0Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNO0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNo1Data_VlvNo1DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNO1DATA_VLVNO1DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNo1Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNO1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNo2Data_VlvNo2DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNO2DATA_VLVNO2DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNo2Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNO2DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNo3Data_VlvNo3DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNO3DATA_VLVNO3DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNo3Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNO3DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvTc0Data_VlvTc0DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVTC0DATA_VLVTC0DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvTc0Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVTC0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvTc1Data_VlvTc1DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVTC1DATA_VLVTC1DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvTc1Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVTC1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvEsv0Data_VlvEsv0DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVESV0DATA_VLVESV0DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvEsv0Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVESV0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvEsv1Data_VlvEsv1DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVESV1DATA_VLVESV1DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvEsv1Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVESV1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNc0Data_VlvNc0DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNC0DATA_VLVNC0DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNc0Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNC0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNc1Data_VlvNc1DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNC1DATA_VLVNC1DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNc1Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNC1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNc2Data_VlvNc2DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNC2DATA_VLVNC2DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNc2Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNC2DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvNc3Data_VlvNc3DataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVNC3DATA_VLVNC3DATAFLG;
const Haluint16 UtInput_Ach_OutputIocVlvNc3Data_ACH_TxValve_SET_POINT[MAX_STEP] = ACH_OUTPUTIOCVLVNC3DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtInput_Ach_OutputIocVlvRelayData_VlvRelayDataFlg[MAX_STEP] = ACH_OUTPUTIOCVLVRELAYDATA_VLVRELAYDATAFLG;
const Haluint8 UtInput_Ach_OutputIocVlvRelayData_ACH_Tx1_FS_CMD[MAX_STEP] = ACH_OUTPUTIOCVLVRELAYDATA_ACH_TX1_FS_CMD;
const Haluint8 UtInput_Ach_OutputIocAdcSelWriteData_AdcSelDataFlg[MAX_STEP] = ACH_OUTPUTIOCADCSELWRITEDATA_ADCSELDATAFLG;
const Haluint8 UtInput_Ach_OutputIocAdcSelWriteData_ACH_Tx13_ADC_SEL[MAX_STEP] = ACH_OUTPUTIOCADCSELWRITEDATA_ACH_TX13_ADC_SEL;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Ach_OutputAcmAsicInitCompleteFlag[MAX_STEP] = ACH_OUTPUTACMASICINITCOMPLETEFLAG;




TEST_GROUP(Ach_Output);
TEST_SETUP(Ach_Output)
{
    Ach_Output_Init();
}

TEST_TEAR_DOWN(Ach_Output)
{   /* Postcondition */

}

TEST(Ach_Output, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg = UtInput_Ach_OutputAkaKeepAliveWriteData_KeepAliveDataFlg[i];
        Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD = UtInput_Ach_OutputAkaKeepAliveWriteData_ACH_Tx1_PHOLD[i];
        Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA = UtInput_Ach_OutputAkaKeepAliveWriteData_ACH_Tx1_KA[i];
        Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg = UtInput_Ach_OutputIocDcMtrDutyData_MtrDutyDataFlg[i];
        Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM = UtInput_Ach_OutputIocDcMtrDutyData_ACH_Tx8_PUMP_DTY_PWM[i];
        Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg = UtInput_Ach_OutputIocDcMtrFreqData_MtrFreqDataFlg[i];
        Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM = UtInput_Ach_OutputIocDcMtrFreqData_ACH_Tx8_PUMP_TCK_PWM[i];
        Ach_OutputIocVlvNo0Data.VlvNo0DataFlg = UtInput_Ach_OutputIocVlvNo0Data_VlvNo0DataFlg[i];
        Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNo0Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNo1Data.VlvNo1DataFlg = UtInput_Ach_OutputIocVlvNo1Data_VlvNo1DataFlg[i];
        Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNo1Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNo2Data.VlvNo2DataFlg = UtInput_Ach_OutputIocVlvNo2Data_VlvNo2DataFlg[i];
        Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNo2Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNo3Data.VlvNo3DataFlg = UtInput_Ach_OutputIocVlvNo3Data_VlvNo3DataFlg[i];
        Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNo3Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvTc0Data.VlvTc0DataFlg = UtInput_Ach_OutputIocVlvTc0Data_VlvTc0DataFlg[i];
        Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvTc0Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvTc1Data.VlvTc1DataFlg = UtInput_Ach_OutputIocVlvTc1Data_VlvTc1DataFlg[i];
        Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvTc1Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg = UtInput_Ach_OutputIocVlvEsv0Data_VlvEsv0DataFlg[i];
        Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvEsv0Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg = UtInput_Ach_OutputIocVlvEsv1Data_VlvEsv1DataFlg[i];
        Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvEsv1Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNc0Data.VlvNc0DataFlg = UtInput_Ach_OutputIocVlvNc0Data_VlvNc0DataFlg[i];
        Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNc0Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNc1Data.VlvNc1DataFlg = UtInput_Ach_OutputIocVlvNc1Data_VlvNc1DataFlg[i];
        Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNc1Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNc2Data.VlvNc2DataFlg = UtInput_Ach_OutputIocVlvNc2Data_VlvNc2DataFlg[i];
        Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNc2Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvNc3Data.VlvNc3DataFlg = UtInput_Ach_OutputIocVlvNc3Data_VlvNc3DataFlg[i];
        Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT = UtInput_Ach_OutputIocVlvNc3Data_ACH_TxValve_SET_POINT[i];
        Ach_OutputIocVlvRelayData.VlvRelayDataFlg = UtInput_Ach_OutputIocVlvRelayData_VlvRelayDataFlg[i];
        Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD = UtInput_Ach_OutputIocVlvRelayData_ACH_Tx1_FS_CMD[i];
        Ach_OutputIocAdcSelWriteData.AdcSelDataFlg = UtInput_Ach_OutputIocAdcSelWriteData_AdcSelDataFlg[i];
        Ach_OutputIocAdcSelWriteData.ACH_Tx13_ADC_SEL = UtInput_Ach_OutputIocAdcSelWriteData_ACH_Tx13_ADC_SEL[i];
        Ach_OutputAcmAsicInitCompleteFlag = UtInput_Ach_OutputAcmAsicInitCompleteFlag[i];

        Ach_Output();

    }
}

TEST_GROUP_RUNNER(Ach_Output)
{
    RUN_TEST_CASE(Ach_Output, All);
}
