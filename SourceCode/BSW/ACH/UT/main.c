/* all_test.c */
#include <stdio.h>
#include "unity_fixture.h"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(Ach_Output);
    RUN_TEST_GROUP(Ach_Input);
}

int main(int argc, char * argv[])
{
    int retUnityMain = 0;

    retUnityMain = UnityMain(argc, argv, RunAllTests);

    return retUnityMain;
}
