/**
 * @defgroup Ach_Cal Ach_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACH_START_SEC_CALIB_UNSPECIFIED
#include "Ach_MemMap.h"
/* Global Calibration Section */


#define ACH_STOP_SEC_CALIB_UNSPECIFIED
#include "Ach_MemMap.h"

#define ACH_START_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACH_STOP_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

uint8 Crc5Table0[256] = {
    0x0, 0x28, 0x50, 0x78, 0xA0, 0x88, 0xF0, 0xD8, 0x68, 0x40, 
    0x38, 0x10, 0xC8, 0xE0, 0x98, 0xB0, 0xD0, 0xF8, 0x80, 0xA8, 
    0x70, 0x58, 0x20, 0x8, 0xB8, 0x90, 0xE8, 0xC0, 0x18, 0x30, 
    0x48, 0x60, 0x88, 0xA0, 0xD8, 0xF0, 0x28, 0x0, 0x78, 0x50, 
    0xE0, 0xC8, 0xB0, 0x98, 0x40, 0x68, 0x10, 0x38, 0x58, 0x70, 
    0x8, 0x20, 0xF8, 0xD0, 0xA8, 0x80, 0x30, 0x18, 0x60, 0x48, 
    0x90, 0xB8, 0xC0, 0xE8, 0x38, 0x10, 0x68, 0x40, 0x98, 0xB0, 
    0xC8, 0xE0, 0x50, 0x78, 0x0, 0x28, 0xF0, 0xD8, 0xA0, 0x88, 
    0xE8, 0xC0, 0xB8, 0x90, 0x48, 0x60, 0x18, 0x30, 0x80, 0xA8, 
    0xD0, 0xF8, 0x20, 0x8, 0x70, 0x58, 0xB0, 0x98, 0xE0, 0xC8, 
    0x10, 0x38, 0x40, 0x68, 0xD8, 0xF0, 0x88, 0xA0, 0x78, 0x50, 
    0x28, 0x0, 0x60, 0x48, 0x30, 0x18, 0xC0, 0xE8, 0x90, 0xB8, 
    0x8, 0x20, 0x58, 0x70, 0xA8, 0x80, 0xF8, 0xD0, 0x70, 0x58, 
    0x20, 0x8, 0xD0, 0xF8, 0x80, 0xA8, 0x18, 0x30, 0x48, 0x60, 
    0xB8, 0x90, 0xE8, 0xC0, 0xA0, 0x88, 0xF0, 0xD8, 0x0, 0x28, 
    0x50, 0x78, 0xC8, 0xE0, 0x98, 0xB0, 0x68, 0x40, 0x38, 0x10, 
    0xF8, 0xD0, 0xA8, 0x80, 0x58, 0x70, 0x8, 0x20, 0x90, 0xB8, 
    0xC0, 0xE8, 0x30, 0x18, 0x60, 0x48, 0x28, 0x0, 0x78, 0x50, 
    0x88, 0xA0, 0xD8, 0xF0, 0x40, 0x68, 0x10, 0x38, 0xE0, 0xC8, 
    0xB0, 0x98, 0x48, 0x60, 0x18, 0x30, 0xE8, 0xC0, 0xB8, 0x90, 
    0x20, 0x8, 0x70, 0x58, 0x80, 0xA8, 0xD0, 0xF8, 0x98, 0xB0, 
    0xC8, 0xE0, 0x38, 0x10, 0x68, 0x40, 0xF0, 0xD8, 0xA0, 0x88, 
    0x50, 0x78, 0x0, 0x28, 0xC0, 0xE8, 0x90, 0xB8, 0x60, 0x48, 
    0x30, 0x18, 0xA8, 0x80, 0xF8, 0xD0, 0x8, 0x20, 0x58, 0x70, 
    0x10, 0x38, 0x40, 0x68, 0xB0, 0x98, 0xE0, 0xC8, 0x78, 0x50, 
    0x28, 0x0, 0xD8, 0xF0, 0x88, 0xA0
};

uint8 Crc5Table1[256] = {
    0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 
    0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 
    0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 
    0x1E, 0x1F, 0x5, 0x4, 0x7, 0x6, 0x1, 0x0, 0x3, 0x2, 
    0x0D, 0x0C, 0x0F, 0x0E, 0x9, 0x8, 0x0B, 0x0A, 0x15, 0x14, 
    0x17, 0x16, 0x11, 0x10, 0x13, 0x12, 0x1D, 0x1C, 0x1F, 0x1E, 
    0x19, 0x18, 0x1B, 0x1A, 0x0A, 0x0B, 0x8, 0x9, 0x0E, 0x0F, 
    0x0C, 0x0D, 0x2, 0x3, 0x0, 0x1, 0x6, 0x7, 0x4, 0x5, 
    0x1A, 0x1B, 0x18, 0x19, 0x1E, 0x1F, 0x1C, 0x1D, 0x12, 0x13, 
    0x10, 0x11, 0x16, 0x17, 0x14, 0x15, 0x0F, 0x0E, 0x0D, 0x0C, 
    0x0B, 0x0A, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x3, 0x2, 
    0x1, 0x0, 0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18, 
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10, 0x14, 0x15, 
    0x16, 0x17, 0x10, 0x11, 0x12, 0x13, 0x1C, 0x1D, 0x1E, 0x1F, 
    0x18, 0x19, 0x1A, 0x1B, 0x4, 0x5, 0x6, 0x7, 0x0, 0x1, 
    0x2, 0x3, 0x0C, 0x0D, 0x0E, 0x0F, 0x8, 0x9, 0x0A, 0x0B, 
    0x11, 0x10, 0x13, 0x12, 0x15, 0x14, 0x17, 0x16, 0x19, 0x18, 
    0x1B, 0x1A, 0x1D, 0x1C, 0x1F, 0x1E, 0x1, 0x0, 0x3, 0x2, 
    0x5, 0x4, 0x7, 0x6, 0x9, 0x8, 0x0B, 0x0A, 0x0D, 0x0C, 
    0x0F, 0x0E, 0x1E, 0x1F, 0x1C, 0x1D, 0x1A, 0x1B, 0x18, 0x19, 
    0x16, 0x17, 0x14, 0x15, 0x12, 0x13, 0x10, 0x11, 0x0E, 0x0F, 
    0x0C, 0x0D, 0x0A, 0x0B, 0x8, 0x9, 0x6, 0x7, 0x4, 0x5, 
    0x2, 0x3, 0x0, 0x1, 0x1B, 0x1A, 0x19, 0x18, 0x1F, 0x1E, 
    0x1D, 0x1C, 0x13, 0x12, 0x11, 0x10, 0x17, 0x16, 0x15, 0x14, 
    0x0B, 0x0A, 0x9, 0x8, 0x0F, 0x0E, 0x0D, 0x0C, 0x3, 0x2, 
    0x1, 0x0, 0x7, 0x6, 0x5, 0x4
};

#define ACH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACH_START_SEC_CODE
#include "Ach_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACH_STOP_SEC_CODE
#include "Ach_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
