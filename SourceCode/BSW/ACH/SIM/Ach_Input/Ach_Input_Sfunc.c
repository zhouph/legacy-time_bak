#define S_FUNCTION_NAME      Ach_Input_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          0
#define WidthOutputPort         303

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ach_Input.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];


    Ach_Input();


    output[0] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
    output[1] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
    output[2] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
    output[3] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
    output[4] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
    output[5] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
    output[6] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
    output[7] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
    output[8] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
    output[9] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
    output[10] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
    output[11] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
    output[12] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
    output[13] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
    output[14] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
    output[15] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
    output[16] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov;
    output[17] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv;
    output[18] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss;
    output[19] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
    output[20] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
    output[21] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
    output[22] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
    output[23] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
    output[24] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
    output[25] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
    output[26] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
    output[27] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
    output[28] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
    output[29] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
    output[30] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
    output[31] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
    output[32] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
    output[33] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
    output[34] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
    output[35] = Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
    output[36] = Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV;
    output[37] = Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV;
    output[38] = Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV;
    output[39] = Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV;
    output[40] = Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
    output[41] = Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
    output[42] = Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
    output[43] = Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
    output[44] = Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
    output[45] = Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV;
    output[46] = Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT;
    output[47] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
    output[48] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
    output[49] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
    output[50] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
    output[51] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD;
    output[52] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
    output[53] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
    output[54] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT;
    output[55] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
    output[56] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
    output[57] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
    output[58] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
    output[59] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
    output[60] = Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
    output[61] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
    output[62] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
    output[63] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
    output[64] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
    output[65] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD;
    output[66] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
    output[67] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
    output[68] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT;
    output[69] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
    output[70] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
    output[71] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
    output[72] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
    output[73] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
    output[74] = Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
    output[75] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
    output[76] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
    output[77] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
    output[78] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
    output[79] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD;
    output[80] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
    output[81] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
    output[82] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT;
    output[83] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
    output[84] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
    output[85] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
    output[86] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
    output[87] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
    output[88] = Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
    output[89] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
    output[90] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
    output[91] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
    output[92] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
    output[93] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD;
    output[94] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
    output[95] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
    output[96] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT;
    output[97] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
    output[98] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
    output[99] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
    output[100] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
    output[101] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
    output[102] = Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
    output[103] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
    output[104] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
    output[105] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
    output[106] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
    output[107] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
    output[108] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
    output[109] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
    output[110] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT;
    output[111] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
    output[112] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
    output[113] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
    output[114] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
    output[115] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
    output[116] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
    output[117] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
    output[118] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
    output[119] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
    output[120] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
    output[121] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
    output[122] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
    output[123] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
    output[124] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT;
    output[125] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
    output[126] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
    output[127] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
    output[128] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
    output[129] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
    output[130] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
    output[131] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
    output[132] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
    output[133] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
    output[134] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
    output[135] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
    output[136] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
    output[137] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
    output[138] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT;
    output[139] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
    output[140] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
    output[141] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
    output[142] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
    output[143] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
    output[144] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
    output[145] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
    output[146] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
    output[147] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
    output[148] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
    output[149] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
    output[150] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
    output[151] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
    output[152] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT;
    output[153] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
    output[154] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
    output[155] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
    output[156] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
    output[157] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
    output[158] = Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
    output[159] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
    output[160] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
    output[161] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
    output[162] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
    output[163] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
    output[164] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
    output[165] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
    output[166] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT;
    output[167] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
    output[168] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
    output[169] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
    output[170] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
    output[171] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
    output[172] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
    output[173] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
    output[174] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
    output[175] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
    output[176] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
    output[177] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
    output[178] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
    output[179] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
    output[180] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT;
    output[181] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
    output[182] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
    output[183] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
    output[184] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
    output[185] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
    output[186] = Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
    output[187] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
    output[188] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
    output[189] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
    output[190] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
    output[191] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
    output[192] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
    output[193] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
    output[194] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT;
    output[195] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
    output[196] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
    output[197] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
    output[198] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
    output[199] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
    output[200] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
    output[201] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
    output[202] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
    output[203] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
    output[204] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
    output[205] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
    output[206] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
    output[207] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
    output[208] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT;
    output[209] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
    output[210] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
    output[211] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
    output[212] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
    output[213] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
    output[214] = Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
    output[215] = Ach_InputAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT;
    output[216] = Ach_InputAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY;
    output[217] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
    output[218] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    output[219] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
    output[220] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
    output[221] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
    output[222] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
    output[223] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
    output[224] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
    output[225] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
    output[226] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
    output[227] = Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
    output[228] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
    output[229] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    output[230] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
    output[231] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
    output[232] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
    output[233] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
    output[234] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
    output[235] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
    output[236] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
    output[237] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
    output[238] = Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
    output[239] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
    output[240] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    output[241] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
    output[242] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
    output[243] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
    output[244] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
    output[245] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
    output[246] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
    output[247] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
    output[248] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
    output[249] = Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
    output[250] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
    output[251] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    output[252] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
    output[253] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
    output[254] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
    output[255] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
    output[256] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
    output[257] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
    output[258] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
    output[259] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
    output[260] = Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
    output[261] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_T_SD;
    output[262] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_OPENLOAD;
    output[263] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LVT;
    output[264] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_VGS_LS_FAULT;
    output[265] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_CLAMP_ACT;
    output[266] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_OVC;
    output[267] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_GND_LOSS;
    output[268] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_T_SD;
    output[269] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_OPENLOAD;
    output[270] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LVT;
    output[271] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_VGS_LS_FAULT;
    output[272] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_CLAMP_ACT;
    output[273] = Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_OVC;
    output[274] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_T_SD;
    output[275] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_OPL;
    output[276] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LVT;
    output[277] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_OVC;
    output[278] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_VGS_LS_FAULT;
    output[279] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_CLAMP;
    output[280] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_T_SD;
    output[281] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OPL;
    output[282] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LVT;
    output[283] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OVC;
    output[284] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_VGS_FAULT;
    output[285] = Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LS_CLAMP;
    output[286] = Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH0_WS1_CNT;
    output[287] = Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH1_WS1_CNT;
    output[288] = Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH2_WS1_CNT;
    output[289] = Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH3_WS1_CNT;
    output[290] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_LD_ACT;
    output[291] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_ON;
    output[292] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_OFF;
    output[293] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_ON;
    output[294] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_OFF;
    output[295] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_ON;
    output[296] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_OFF;
    output[297] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_VDS_TURNOFF;
    output[298] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_VDS_FAULT;
    output[299] = Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_FLYBACK;
    output[300] = Ach_InputAchMotorAsicInfo.Ach_Asic_CP_OV;
    output[301] = Ach_InputAchMotorAsicInfo.Ach_Asic_CP_UV;
    output[302] = Ach_InputAchAsicInvalid;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ach_Input_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
