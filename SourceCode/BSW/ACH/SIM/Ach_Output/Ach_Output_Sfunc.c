#define S_FUNCTION_NAME      Ach_Output_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          36
#define WidthOutputPort         0

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ach_Output.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg = input[0];
    Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD = input[1];
    Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA = input[2];
    Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg = input[3];
    Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM = input[4];
    Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg = input[5];
    Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM = input[6];
    Ach_OutputIocVlvNo0Data.VlvNo0DataFlg = input[7];
    Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT = input[8];
    Ach_OutputIocVlvNo1Data.VlvNo1DataFlg = input[9];
    Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT = input[10];
    Ach_OutputIocVlvNo2Data.VlvNo2DataFlg = input[11];
    Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT = input[12];
    Ach_OutputIocVlvNo3Data.VlvNo3DataFlg = input[13];
    Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT = input[14];
    Ach_OutputIocVlvTc0Data.VlvTc0DataFlg = input[15];
    Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT = input[16];
    Ach_OutputIocVlvTc1Data.VlvTc1DataFlg = input[17];
    Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT = input[18];
    Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg = input[19];
    Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT = input[20];
    Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg = input[21];
    Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT = input[22];
    Ach_OutputIocVlvNc0Data.VlvNc0DataFlg = input[23];
    Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT = input[24];
    Ach_OutputIocVlvNc1Data.VlvNc1DataFlg = input[25];
    Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT = input[26];
    Ach_OutputIocVlvNc2Data.VlvNc2DataFlg = input[27];
    Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT = input[28];
    Ach_OutputIocVlvNc3Data.VlvNc3DataFlg = input[29];
    Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT = input[30];
    Ach_OutputIocVlvRelayData.VlvRelayDataFlg = input[31];
    Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD = input[32];
    Ach_OutputIocAdcSelWriteData.AdcSelDataFlg = input[33];
    Ach_OutputIocAdcSelWriteData.ACH_Tx13_ADC_SEL = input[34];
    Ach_OutputAcmAsicInitCompleteFlag = input[35];

    Ach_Output();


    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ach_Output_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
