/**
 * @defgroup Ach_Output_Ifa Ach_Output_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Output_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACH_OUTPUT_IFA_H_
#define ACH_OUTPUT_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ach_Output_Read_Ach_OutputAkaKeepAliveWriteData(data) do \
{ \
    *data = Ach_OutputAkaKeepAliveWriteData; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocDcMtrDutyData(data) do \
{ \
    *data = Ach_OutputIocDcMtrDutyData; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocDcMtrFreqData(data) do \
{ \
    *data = Ach_OutputIocDcMtrFreqData; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo0Data(data) do \
{ \
    *data = Ach_OutputIocVlvNo0Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo1Data(data) do \
{ \
    *data = Ach_OutputIocVlvNo1Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo2Data(data) do \
{ \
    *data = Ach_OutputIocVlvNo2Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo3Data(data) do \
{ \
    *data = Ach_OutputIocVlvNo3Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvTc0Data(data) do \
{ \
    *data = Ach_OutputIocVlvTc0Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvTc1Data(data) do \
{ \
    *data = Ach_OutputIocVlvTc1Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvEsv0Data(data) do \
{ \
    *data = Ach_OutputIocVlvEsv0Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvEsv1Data(data) do \
{ \
    *data = Ach_OutputIocVlvEsv1Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc0Data(data) do \
{ \
    *data = Ach_OutputIocVlvNc0Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc1Data(data) do \
{ \
    *data = Ach_OutputIocVlvNc1Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc2Data(data) do \
{ \
    *data = Ach_OutputIocVlvNc2Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc3Data(data) do \
{ \
    *data = Ach_OutputIocVlvNc3Data; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvRelayData(data) do \
{ \
    *data = Ach_OutputIocVlvRelayData; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocAdcSelWriteData(data) do \
{ \
    *data = Ach_OutputIocAdcSelWriteData; \
}while(0);

#define Ach_Output_Read_Ach_OutputAkaKeepAliveWriteData_KeepAliveDataFlg(data) do \
{ \
    *data = Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputAkaKeepAliveWriteData_ACH_Tx1_PHOLD(data) do \
{ \
    *data = Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD; \
}while(0);

#define Ach_Output_Read_Ach_OutputAkaKeepAliveWriteData_ACH_Tx1_KA(data) do \
{ \
    *data = Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocDcMtrDutyData_MtrDutyDataFlg(data) do \
{ \
    *data = Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocDcMtrDutyData_ACH_Tx8_PUMP_DTY_PWM(data) do \
{ \
    *data = Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocDcMtrFreqData_MtrFreqDataFlg(data) do \
{ \
    *data = Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocDcMtrFreqData_ACH_Tx8_PUMP_TCK_PWM(data) do \
{ \
    *data = Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo0Data_VlvNo0DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNo0Data.VlvNo0DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo1Data_VlvNo1DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNo1Data.VlvNo1DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo2Data_VlvNo2DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNo2Data.VlvNo2DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo2Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo3Data_VlvNo3DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNo3Data.VlvNo3DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNo3Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvTc0Data_VlvTc0DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvTc0Data.VlvTc0DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvTc0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvTc1Data_VlvTc1DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvTc1Data.VlvTc1DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvTc1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvEsv0Data_VlvEsv0DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvEsv0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvEsv1Data_VlvEsv1DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvEsv1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc0Data_VlvNc0DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNc0Data.VlvNc0DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc0Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc1Data_VlvNc1DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNc1Data.VlvNc1DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc1Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc2Data_VlvNc2DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNc2Data.VlvNc2DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc2Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc3Data_VlvNc3DataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvNc3Data.VlvNc3DataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvNc3Data_ACH_TxValve_SET_POINT(data) do \
{ \
    *data = Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvRelayData_VlvRelayDataFlg(data) do \
{ \
    *data = Ach_OutputIocVlvRelayData.VlvRelayDataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocVlvRelayData_ACH_Tx1_FS_CMD(data) do \
{ \
    *data = Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocAdcSelWriteData_AdcSelDataFlg(data) do \
{ \
    *data = Ach_OutputIocAdcSelWriteData.AdcSelDataFlg; \
}while(0);

#define Ach_Output_Read_Ach_OutputIocAdcSelWriteData_ACH_Tx13_ADC_SEL(data) do \
{ \
    *data = Ach_OutputIocAdcSelWriteData.ACH_Tx13_ADC_SEL; \
}while(0);

#define Ach_Output_Read_Ach_OutputAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Ach_OutputAcmAsicInitCompleteFlag; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACH_OUTPUT_IFA_H_ */
/** @} */
