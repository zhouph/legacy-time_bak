/**
 * @defgroup Ach_Input_Ifa Ach_Input_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Input_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACH_INPUT_IFA_H_
#define ACH_INPUT_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */

/* Set Output DE MAcro Function */
#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo(data) do \
{ \
    Ach_InputAchValveAsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchAdcData(data) do \
{ \
    Ach_InputAchAdcData = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssRedncyAsicInfo(data) do \
{ \
    Ach_InputAchWssRedncyAsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo(data) do \
{ \
    Ach_InputAchMotorAsicInfo = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vint_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vint_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_dgndloss(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_comm_too_long(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_comm_too_short(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd5_ov(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd5_uv(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_3_OSC_STUCK_MON(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_3_OSC_FRQ_MON(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_3_WDOG_TO(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_7_AN_TRIM_CRC_RESULT(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_7_NVM_CRC_RESULT(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchSysPwrAsicInfo_Ach_Asic_7_NVM_BUSY(data) do \
{ \
    Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_CP_OV(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_CP_UV(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_VPWR_UV(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_VPWR_OV(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_PMP_LD_ACT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_FS_TURN_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_FS_TURN_OFF(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_VHD_OV(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_T_SD_INT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No0_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No1_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No2_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_No3_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc0_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc1_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc2_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Nc3_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc0_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Tc1_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv0_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_AVG_CURRENT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_PWM_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_CURR_SENSE(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_ADC_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_T_SD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_OPEN_LOAD(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_GND_LOSS(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_LVT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_LS_OVC(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_VGS_HS_FAULT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_HS_SHORT(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_LS_CLAMP_ON(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchValveAsicInfo_Ach_Asic_Esv1_UNDER_CURR(data) do \
{ \
    Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchAdcData_ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT(data) do \
{ \
    Ach_InputAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchAdcData_ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY(data) do \
{ \
    Ach_InputAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NO_FAULT(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STANDSTILL(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_LATCH_D0(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NODATA(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_INVALID(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_OPEN(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STB(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STG(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WSI_OT(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_CURRENT_HI(data) do \
{ \
    Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NO_FAULT(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STANDSTILL(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_LATCH_D0(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NODATA(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_INVALID(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_OPEN(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STB(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STG(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WSI_OT(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_CURRENT_HI(data) do \
{ \
    Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NO_FAULT(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STANDSTILL(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_LATCH_D0(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NODATA(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_INVALID(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_OPEN(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STB(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STG(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WSI_OT(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_CURRENT_HI(data) do \
{ \
    Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NO_FAULT(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STANDSTILL(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_LATCH_D0(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NODATA(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_INVALID(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_OPEN(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STB(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STG(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WSI_OT(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_CURRENT_HI(data) do \
{ \
    Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_VSO_T_SD(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_VSO_OPENLOAD(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_OPENLOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_VSO_LVT(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_VSO_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_VSO_LS_CLAMP_ACT(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_CLAMP_ACT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_VSO_LS_OVC(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_GND_LOSS(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_GND_LOSS = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_SEL_T_SD(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_SEL_OPENLOAD(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_OPENLOAD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_SEL_LVT(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_SEL_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_SEL_LS_CLAMP_ACT(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_CLAMP_ACT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchVsoSelAsicInfo_Ach_Asic_SEL_LS_OVC(data) do \
{ \
    Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_WLD_T_SD(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_WLD_OPL(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_OPL = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_WLD_LVT(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_WLD_LS_OVC(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_WLD_VGS_LS_FAULT(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_VGS_LS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_WLD_LS_CLAMP(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_CLAMP = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_SHLS_T_SD(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_T_SD = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_SHLS_OPL(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OPL = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_SHLS_LVT(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LVT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_SHLS_OVC(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OVC = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_SHLS_VGS_FAULT(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_VGS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWldShlsAsicInfo_Ach_Asic_SHLS_LS_CLAMP(data) do \
{ \
    Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LS_CLAMP = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssRedncyAsicInfo_Ach_Asic_CH0_WS1_CNT(data) do \
{ \
    Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH0_WS1_CNT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssRedncyAsicInfo_Ach_Asic_CH1_WS1_CNT(data) do \
{ \
    Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH1_WS1_CNT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssRedncyAsicInfo_Ach_Asic_CH2_WS1_CNT(data) do \
{ \
    Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH2_WS1_CNT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchWssRedncyAsicInfo_Ach_Asic_CH3_WS1_CNT(data) do \
{ \
    Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH3_WS1_CNT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP_LD_ACT(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_LD_ACT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP1_TURN_ON(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP1_TURN_OFF(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_OFF = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP2_TURN_ON(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP2_TURN_OFF(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_OFF = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP3_TURN_ON(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_ON = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP3_TURN_OFF(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_OFF = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP_VDS_TURNOFF(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_VDS_TURNOFF = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP1_VDS_FAULT(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_VDS_FAULT = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_PMP_FLYBACK(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_FLYBACK = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_CP_OV(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_CP_OV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchMotorAsicInfo_Ach_Asic_CP_UV(data) do \
{ \
    Ach_InputAchMotorAsicInfo.Ach_Asic_CP_UV = *data; \
}while(0);

#define Ach_Input_Write_Ach_InputAchAsicInvalid(data) do \
{ \
    Ach_InputAchAsicInvalid = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACH_INPUT_IFA_H_ */
/** @} */
