Ach_OutputAkaKeepAliveWriteData = Simulink.Bus;
DeList={
    'KeepAliveDataFlg'
    'ACH_Tx1_PHOLD'
    'ACH_Tx1_KA'
    };
Ach_OutputAkaKeepAliveWriteData = CreateBus(Ach_OutputAkaKeepAliveWriteData, DeList);
clear DeList;

Ach_OutputIocDcMtrDutyData = Simulink.Bus;
DeList={
    'MtrDutyDataFlg'
    'ACH_Tx8_PUMP_DTY_PWM'
    };
Ach_OutputIocDcMtrDutyData = CreateBus(Ach_OutputIocDcMtrDutyData, DeList);
clear DeList;

Ach_OutputIocDcMtrFreqData = Simulink.Bus;
DeList={
    'MtrFreqDataFlg'
    'ACH_Tx8_PUMP_TCK_PWM'
    };
Ach_OutputIocDcMtrFreqData = CreateBus(Ach_OutputIocDcMtrFreqData, DeList);
clear DeList;

Ach_OutputIocVlvNo0Data = Simulink.Bus;
DeList={
    'VlvNo0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo0Data = CreateBus(Ach_OutputIocVlvNo0Data, DeList);
clear DeList;

Ach_OutputIocVlvNo1Data = Simulink.Bus;
DeList={
    'VlvNo1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo1Data = CreateBus(Ach_OutputIocVlvNo1Data, DeList);
clear DeList;

Ach_OutputIocVlvNo2Data = Simulink.Bus;
DeList={
    'VlvNo2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo2Data = CreateBus(Ach_OutputIocVlvNo2Data, DeList);
clear DeList;

Ach_OutputIocVlvNo3Data = Simulink.Bus;
DeList={
    'VlvNo3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo3Data = CreateBus(Ach_OutputIocVlvNo3Data, DeList);
clear DeList;

Ach_OutputIocVlvTc0Data = Simulink.Bus;
DeList={
    'VlvTc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvTc0Data = CreateBus(Ach_OutputIocVlvTc0Data, DeList);
clear DeList;

Ach_OutputIocVlvTc1Data = Simulink.Bus;
DeList={
    'VlvTc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvTc1Data = CreateBus(Ach_OutputIocVlvTc1Data, DeList);
clear DeList;

Ach_OutputIocVlvEsv0Data = Simulink.Bus;
DeList={
    'VlvEsv0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvEsv0Data = CreateBus(Ach_OutputIocVlvEsv0Data, DeList);
clear DeList;

Ach_OutputIocVlvEsv1Data = Simulink.Bus;
DeList={
    'VlvEsv1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvEsv1Data = CreateBus(Ach_OutputIocVlvEsv1Data, DeList);
clear DeList;

Ach_OutputIocVlvNc0Data = Simulink.Bus;
DeList={
    'VlvNc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc0Data = CreateBus(Ach_OutputIocVlvNc0Data, DeList);
clear DeList;

Ach_OutputIocVlvNc1Data = Simulink.Bus;
DeList={
    'VlvNc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc1Data = CreateBus(Ach_OutputIocVlvNc1Data, DeList);
clear DeList;

Ach_OutputIocVlvNc2Data = Simulink.Bus;
DeList={
    'VlvNc2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc2Data = CreateBus(Ach_OutputIocVlvNc2Data, DeList);
clear DeList;

Ach_OutputIocVlvNc3Data = Simulink.Bus;
DeList={
    'VlvNc3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc3Data = CreateBus(Ach_OutputIocVlvNc3Data, DeList);
clear DeList;

Ach_OutputIocVlvRelayData = Simulink.Bus;
DeList={
    'VlvRelayDataFlg'
    'ACH_Tx1_FS_CMD'
    };
Ach_OutputIocVlvRelayData = CreateBus(Ach_OutputIocVlvRelayData, DeList);
clear DeList;

Ach_OutputIocAdcSelWriteData = Simulink.Bus;
DeList={
    'AdcSelDataFlg'
    'ACH_Tx13_ADC_SEL'
    };
Ach_OutputIocAdcSelWriteData = CreateBus(Ach_OutputIocAdcSelWriteData, DeList);
clear DeList;

Ach_OutputAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ach_OutputAcmAsicInitCompleteFlag'};
Ach_OutputAcmAsicInitCompleteFlag = CreateBus(Ach_OutputAcmAsicInitCompleteFlag, DeList);
clear DeList;

