/**
 * @defgroup Ach_Input Ach_Input
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Input.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Input.h"
#include "Ach_Input_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACH_INPUT_START_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACH_INPUT_STOP_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_INPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ach_Input_HdrBusType Ach_InputBus;

/* Version Info */
const SwcVersionInfo_t Ach_InputVersionInfo = 
{   
    ACH_INPUT_MODULE_ID,           /* Ach_InputVersionInfo.ModuleId */
    ACH_INPUT_MAJOR_VERSION,       /* Ach_InputVersionInfo.MajorVer */
    ACH_INPUT_MINOR_VERSION,       /* Ach_InputVersionInfo.MinorVer */
    ACH_INPUT_PATCH_VERSION,       /* Ach_InputVersionInfo.PatchVer */
    ACH_INPUT_BRANCH_VERSION       /* Ach_InputVersionInfo.BranchVer */
};
    
/* Input Data Element */

/* Output Data Element */
Ach_InputAchSysPwrAsicInfo_t Ach_InputAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t Ach_InputAchValveAsicInfo;
Ach_InputAchAdcData_t Ach_InputAchAdcData;
Ach_InputAchWssPort0AsicInfo_t Ach_InputAchWssPort0AsicInfo;
Ach_InputAchWssPort1AsicInfo_t Ach_InputAchWssPort1AsicInfo;
Ach_InputAchWssPort2AsicInfo_t Ach_InputAchWssPort2AsicInfo;
Ach_InputAchWssPort3AsicInfo_t Ach_InputAchWssPort3AsicInfo;
Ach_InputAchVsoSelAsicInfo_t Ach_InputAchVsoSelAsicInfo;
Ach_InputAchWldShlsAsicInfo_t Ach_InputAchWldShlsAsicInfo;
Ach_InputAchWssRedncyAsicInfo_t Ach_InputAchWssRedncyAsicInfo;
Ach_InputAchMotorAsicInfo_t Ach_InputAchMotorAsicInfo;
Ach_InputAchAsicInvalidInfo_t Ach_InputAchAsicInvalid;

uint32 Ach_Input_Timer_Start;
uint32 Ach_Input_Timer_Elapsed;

#define ACH_INPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_INPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_INPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACH_INPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_INPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACH_INPUT_START_SEC_CODE
#include "Ach_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ach_Input_Init(void)
{
    /* Initialize internal bus */
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT = 0;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON = 0;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR = 0;
    Ach_InputBus.Ach_InputAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT = 0;
    Ach_InputBus.Ach_InputAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = 0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = 0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = 0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = 0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_T_SD = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_OPENLOAD = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LVT = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_CLAMP_ACT = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_GND_LOSS = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_T_SD = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_OPENLOAD = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LVT = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_CLAMP_ACT = 0;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_T_SD = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_OPL = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LVT = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_OVC = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_VGS_LS_FAULT = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_CLAMP = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_T_SD = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OPL = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LVT = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OVC = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_VGS_FAULT = 0;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LS_CLAMP = 0;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH0_WS1_CNT = 0;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH1_WS1_CNT = 0;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH2_WS1_CNT = 0;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH3_WS1_CNT = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_LD_ACT = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_ON = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_OFF = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_ON = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_OFF = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_ON = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_OFF = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_VDS_TURNOFF = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_VDS_FAULT = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_FLYBACK = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_CP_OV = 0;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_CP_UV = 0;
    Ach_InputBus.Ach_InputAchAsicInvalid = 0;
}

void Ach_Input(void)
{
    Ach_Input_Timer_Start = STM0_TIM0.U;

    /* Input */

    /* Process */
    ACH_SpiCallback();

    /* Output */
    Ach_Input_Write_Ach_InputAchSysPwrAsicInfo(&Ach_InputBus.Ach_InputAchSysPwrAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchSysPwrAsicInfo 
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchValveAsicInfo(&Ach_InputBus.Ach_InputAchValveAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchValveAsicInfo 
     : Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchAdcData(&Ach_InputBus.Ach_InputAchAdcData);
    /*==============================================================================
    * Members of structure Ach_InputAchAdcData 
     : Ach_InputAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT;
     : Ach_InputAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchWssPort0AsicInfo(&Ach_InputBus.Ach_InputAchWssPort0AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort0AsicInfo 
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchWssPort1AsicInfo(&Ach_InputBus.Ach_InputAchWssPort1AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort1AsicInfo 
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchWssPort2AsicInfo(&Ach_InputBus.Ach_InputAchWssPort2AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort2AsicInfo 
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchWssPort3AsicInfo(&Ach_InputBus.Ach_InputAchWssPort3AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort3AsicInfo 
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchVsoSelAsicInfo(&Ach_InputBus.Ach_InputAchVsoSelAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchVsoSelAsicInfo 
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_T_SD;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_OPENLOAD;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LVT;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_VGS_LS_FAULT;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_CLAMP_ACT;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_OVC;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_GND_LOSS;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_T_SD;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_OPENLOAD;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LVT;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_VGS_LS_FAULT;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_CLAMP_ACT;
     : Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_OVC;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchWldShlsAsicInfo(&Ach_InputBus.Ach_InputAchWldShlsAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWldShlsAsicInfo 
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_T_SD;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_OPL;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LVT;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_OVC;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_VGS_LS_FAULT;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_CLAMP;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_T_SD;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OPL;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LVT;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OVC;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_VGS_FAULT;
     : Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LS_CLAMP;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchWssRedncyAsicInfo(&Ach_InputBus.Ach_InputAchWssRedncyAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssRedncyAsicInfo 
     : Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH0_WS1_CNT;
     : Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH1_WS1_CNT;
     : Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH2_WS1_CNT;
     : Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH3_WS1_CNT;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchMotorAsicInfo(&Ach_InputBus.Ach_InputAchMotorAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchMotorAsicInfo 
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_LD_ACT;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_ON;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_OFF;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_ON;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_OFF;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_ON;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_OFF;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_VDS_TURNOFF;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_VDS_FAULT;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_FLYBACK;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_CP_OV;
     : Ach_InputAchMotorAsicInfo.Ach_Asic_CP_UV;
     =============================================================================*/
    
    Ach_Input_Write_Ach_InputAchAsicInvalid(&Ach_InputBus.Ach_InputAchAsicInvalid);

    Ach_Input_Timer_Elapsed = STM0_TIM0.U - Ach_Input_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACH_INPUT_STOP_SEC_CODE
#include "Ach_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
