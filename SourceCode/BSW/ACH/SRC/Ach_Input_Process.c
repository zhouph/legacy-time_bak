/**
 * @defgroup Ach_Input Ach_Input
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Input.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Input.h"
#include "Ach_Input_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACH_INPUT_START_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACH_INPUT_STOP_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_INPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ACH_INPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_INPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_INPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


uint16 BECK_TEST_ERROR_COUNT;

ACH_RxFrameType  ACH_RxFrame[MAX_BUFFER_SIZE];

/*************************** ASIC RX Message *************************/
ASIC_RX_MSG_t ASIC_RX_MSG;

 

#define ACH_INPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_INPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_INPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_INPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACH_INPUT_START_SEC_CODE
#include "Ach_MemMap.h"

static Std_ReturnType Ach_CopySigData(void);
#ifndef SPI_DMA_ENABLE
void ACH_DecodeAsicMsg(void);
#endif
Std_ReturnType ACH_GetSigData_CS(ACH_SigIdType ACH_SigId, uint16* Data);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
#ifdef SPI_DMA_ENABLE
void ACH_SpiCallback(void)
{
    /* Decoding Asic Message */
    uint32  MsgIdIndex  = 0;
    uint16  msg_num     = 1;
    uint16  crc_temp    = 0;
    uint8   i           = 0;
    uint8   FcntErrFlag = 0;

    for(MsgIdIndex = 0u ; MsgIdIndex < ACH_MSGID_MAX ; MsgIdIndex++)
    {
        if(ACH_MsgConfig[MsgIdIndex].STATUS == ACH_STATUS_SENT)
        {
            crc_temp = Ach_CRC5Gen(ACH_RxFrame[msg_num].frame);
            if(ACH_RxFrame[msg_num].Rx_Field.SPI_ERR == 1)  /* SPI Error Flag */
            {
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_ERROR;
                BECK_TEST_ERROR_COUNT++;    /* TEST */
                if((ACH_RxFrame[msg_num].Rx_Field.DATA & 0x1800) == 0x1000) /*  Only in case of FCNT ERROR FLAG : 1, CRC ERROR FLaG : 0*/
                {
                    FcntErrFlag = 1;
                }
            }
            else if(ACH_RxFrame[msg_num].Rx_Field.CRC != crc_temp)  /* CRC Error */
            {
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_ERROR;
                BECK_TEST_ERROR_COUNT++;    /* TEST */
            }
            else if(ACH_RxFrame[msg_num].Rx_Field.ADD_FBK != ACH_MsgConfig[MsgIdIndex].ADD) /* Address Feedback Error */
            {
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_ERROR; 
                BECK_TEST_ERROR_COUNT++;    /* TEST */
            }
            else
            {
                ASIC_RX_MSG.A.ASIC_RxArray[MsgIdIndex] = ACH_RxFrame[msg_num].frame;
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_IDLE;
            }
            msg_num++;
        }
    }
    for(i = 0 ; i < MAX_BUFFER_SIZE ; i++)
    {

        ACH_TxFrame[i].frame = 0u;
        ACH_RxFrame[i].frame = 0u;
    }

    if(FcntErrFlag == 1)    /* FCNT SWAP */
    {
        ACH_FrameCount ^= 1;
    }
    
    Ach_CopySigData();

}
#else
void ACH_DecodeAsicMsg(void)
{
    /* Decoding Asic Message */
    uint32  MsgIdIndex  = 0;
    uint16  msg_num     = 1;
    uint16  crc_temp    = 0;
    uint8   i           = 0;
    uint8   FcntErrFlag = 0;
    for(MsgIdIndex = 0u ; MsgIdIndex < ACH_MSGID_MAX ; MsgIdIndex++)
    {
        if(ACH_MsgConfig[MsgIdIndex].STATUS == ACH_STATUS_SENT)
        {
            crc_temp = Ach_CRC5Gen(ACH_RxFrame[msg_num].frame);
            if(ACH_RxFrame[msg_num].Rx_Field.SPI_ERR == 1)  /* SPI Error Flag */
            {
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_ERROR;
                BECK_TEST_ERROR_COUNT++;    /* TEST */
                Ach_InputBus.Ach_InputAchAsicInvalid |= ACH_INV_INVALID_MSG;
                Ach_InputBus.Ach_InputAchAsicInvalid |= ACH_INV_IS_ERROR;                
                if((ACH_RxFrame[msg_num].Rx_Field.DATA & 0x1800) == 0x1000) /*  Only in case of FCNT ERROR FLAG : 1, CRC ERROR FLaG : 0*/
                {
                    FcntErrFlag = 1;
                }
            }
            else if(ACH_RxFrame[msg_num].Rx_Field.CRC != crc_temp)  /* CRC Error */
            {
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_ERROR;
                BECK_TEST_ERROR_COUNT++;    /* TEST */
                Ach_InputBus.Ach_InputAchAsicInvalid |= ACH_INV_CHECKSUM;
                Ach_InputBus.Ach_InputAchAsicInvalid |= ACH_INV_IS_ERROR;
            }
            else if(ACH_RxFrame[msg_num].Rx_Field.ADD_FBK != ACH_MsgConfig[MsgIdIndex].ADD) /* Address Feedback Error */
            {
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_ERROR; 
                BECK_TEST_ERROR_COUNT++;    /* TEST */
                Ach_InputBus.Ach_InputAchAsicInvalid |= ACH_INV_INVALID_MSG;
                Ach_InputBus.Ach_InputAchAsicInvalid |= ACH_INV_IS_ERROR;                
            }
            else
            {
                ASIC_RX_MSG.A.ASIC_RxArray[MsgIdIndex] = ACH_RxFrame[msg_num].frame;
                ACH_MsgConfig[MsgIdIndex].STATUS = ACH_STATUS_IDLE;
                Ach_InputBus.Ach_InputAchAsicInvalid = ACH_INV_NO_FAULT;
            }
            msg_num++;
        }
    }
    for(i = 0 ; i < MAX_BUFFER_SIZE ; i++)
    {
        ACH_TxFrame[i].frame = 0u;
        ACH_RxFrame[i].frame = 0u;
    }   

    if(FcntErrFlag == 1)    /* FCNT SWAP */
    {
        ACH_FrameCount ^= 1;
    }
    
    Ach_CopySigData();

}

#endif



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
 
 
Std_ReturnType Ach_CopySigData(void)
{
    Std_ReturnType rtn = E_OK;
    
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_NO_FAULT;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA   = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_STANDSTILL;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_LATCH_D0;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_NODATA;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_INVALID;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN                       = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_OPEN;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_STB;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_STG;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_WSI_OT;
    Ach_InputBus.Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_NO_FAULT;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA   = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_STANDSTILL;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_LATCH_D0;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_NODATA;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_INVALID;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN                       = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_OPEN;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_STB;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_STG;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_WSI_OT;
    Ach_InputBus.Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_NO_FAULT;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA   = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_STANDSTILL;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_LATCH_D0;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_NODATA;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_INVALID;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN                       = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_OPEN;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_STB;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_STG;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_WSI_OT;
    Ach_InputBus.Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_NO_FAULT;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA   = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_STANDSTILL;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_LATCH_D0;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_NODATA;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_INVALID;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN                       = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_OPEN;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_STB;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG                        = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_STG;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_WSI_OT;
    Ach_InputBus.Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
    Ach_InputBus.Ach_InputAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT                     = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_13.B.mu10_Rx_13_ADC_DATA_OUT;
    Ach_InputBus.Ach_InputAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_13.B.mu4_Rx_13_ADC_SEL;

	/* ECU_NZ15_InterfaceAdded_150619 */
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_T_SD                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_T_SD;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_OPENLOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_OPENLOAD;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LVT                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_LVT;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_VGS_LS_FAULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_CLAMP_ACT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_LS_CLAMP_ACT;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_VSO_LS_OVC                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_LS_OVC;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_GND_LOSS                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_GND_LOSS;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_T_SD                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_T_SD;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_OPENLOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_OPENLOAD;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LVT                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_LVT;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_VGS_LS_FAULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_CLAMP_ACT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_LS_CLAMP_ACT;
    Ach_InputBus.Ach_InputAchVsoSelAsicInfo.Ach_Asic_SEL_LS_OVC                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_LS_OVC;

    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_T_SD                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_T_SD;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_OPL                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_OPL;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LVT                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_LVT;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_OVC                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_LS_OVC;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_VGS_LS_FAULT                         = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_WLD_LS_CLAMP                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_LS_CLAMP;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_T_SD                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_T_SD;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OPL                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_OPL;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LVT                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_LVT;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_OVC;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_VGS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_VGS_FAULT;
    Ach_InputBus.Ach_InputAchWldShlsAsicInfo.Ach_Asic_SHLS_LS_CLAMP                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_LS_CLAMP;

    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH0_WS1_CNT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_40.B.mu8_Rx_40_WS1_CNT;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH1_WS1_CNT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_41.B.mu8_Rx_41_WS1_CNT;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH2_WS1_CNT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_42.B.mu8_Rx_42_WS1_CNT;
    Ach_InputBus.Ach_InputAchWssRedncyAsicInfo.Ach_Asic_CH3_WS1_CNT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_43.B.mu8_Rx_43_WS1_CNT;

    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_OVC;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_OUT_OF_REG;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD3_OVC;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD4_OVC;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1DIODE_LOSS_ECHO;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_REV_CURR;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_T_SD;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_T_SD;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD3_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD4_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD3_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD4_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_VINTA_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_VINTA_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_DGNDLOSS;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VPWR_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VPWR_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_TOO_LONG;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_TOO_SHORT;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_WRONG_ADD;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_WRONG_FCNT;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_WRONG_CRC;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_OV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_UV;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_T_SD;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_OUT_OF_REG;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_OSC_STUCK_MON;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_OSC_FRQ_MON;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_WDOG_TO;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT                      = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_7.B.mu1_Rx_7_AN_TRIM_CRC_RESULT;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_7.B.mu1_Rx_7_NVM_CRC_RESULT;
    Ach_InputBus.Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_7.B.mu1_Rx_7_NVM_BUSY;

    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_TURN_ON;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_TURN_OFF;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_VDS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_RVP_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV                                     = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VHD_OV;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_T_SD_INT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV                                      = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_CP_OV;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV                                      = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_CP_UV;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VPWR_UV;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VPWR_OV;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_LD_ACT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT                            = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_10.B.mu12_Rx_NT10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu1_Rx_NT0_PWM_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu1_Rx_NT0_CURR_SENSE;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu1_Rx_NT0_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_HS_SHORT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT                            = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_10.B.mu12_Rx_NT10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu1_Rx_NT0_PWM_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu1_Rx_NT0_CURR_SENSE;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu1_Rx_NT0_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_HS_SHORT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT                            = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_10.B.mu12_Rx_NT10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu1_Rx_NT0_PWM_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu1_Rx_NT0_CURR_SENSE;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu1_Rx_NT0_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_HS_SHORT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT                            = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_10.B.mu12_Rx_NT10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu1_Rx_NT0_PWM_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu1_Rx_NT0_CURR_SENSE;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu1_Rx_NT0_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_HS_SHORT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_OPL;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_LS_CLAMP;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_UNDER_CURR;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_OPL;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_LS_CLAMP;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_UNDER_CURR;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_OPL;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_LS_CLAMP;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_UNDER_CURR;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_OPL;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_LS_CLAMP;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_UNDER_CURR;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT                            = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_10.B.mu12_Rx_NT10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu1_Rx_NT0_PWM_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu1_Rx_NT0_CURR_SENSE;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu1_Rx_NT0_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_HS_SHORT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT                            = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_10.B.mu12_Rx_NT10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu1_Rx_NT0_PWM_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu1_Rx_NT0_CURR_SENSE;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu1_Rx_NT0_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT                                    = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_HS_SHORT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT                           = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_10.B.mu11_Rx_ESV10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_PWM_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_VGS_LS_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT                           = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_10.B.mu11_Rx_ESV10_AVG_CURRENT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_PWM_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_ADC_FAULT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD                                  = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_T_SD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_OPEN_LOAD;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_GND_LOSS;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT                                   = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_LVT;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_LS_OVC;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_VGS_LS_FAULT;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT                          = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;
    Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON                           = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_LS_CLAMP_ON;
    //Ach_InputBus.Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_;

    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_LD_ACT                                 = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_LD_ACT;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_ON                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP1_TURN_ON;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_TURN_OFF                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP1_TURN_OFF;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_ON                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP2_TURN_ON;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP2_TURN_OFF                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP2_TURN_OFF;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_ON                               = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP3_TURN_ON;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP3_TURN_OFF                              = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP3_TURN_OFF;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_VDS_TURNOFF                            = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_VDS_TURNOFF;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP1_VDS_FAULT                             = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP1_VDS_FAULT;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_PMP_FLYBACK                                = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_FLYBACK;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_CP_OV                                      = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_CP_OV;
    Ach_InputBus.Ach_InputAchMotorAsicInfo.Ach_Asic_CP_UV                                      = (uint8)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_CP_UV;

    return rtn;
}

Std_ReturnType ACH_GetSigData_CS(ACH_SigIdType ACH_SigId, uint16* Data)
{
    Std_ReturnType rtn = E_OK;

    switch(ACH_SigId)
    {
        case ASIC_RX_MSG_8mu8_Rx_8_PUMP_DTY_PWM:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_8.B.mu8_Rx_8_PUMP_DTY_PWM;
            break;
        }
        case ASIC_RX_MSG_8mu8_Rx_8_PUMP_TCK_PWM:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_8.B.mu8_Rx_8_PUMP_TCK_PWM;
            break;
        }
        case ASIC_RX_MSG_1mu2_Rx_1_VDD3_OVC_FLT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu2_Rx_1_VDD3_OVC_FLT_TIME;
            break;
        }
        case ASIC_RX_MSG_1mu2_Rx_1_VDD_3_OVC_SD_RESTART_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu2_Rx_1_VDD_3_OVC_SD_RESTART_TIME;
            break;
        }
        case ASIC_RX_MSG_1mu2_Rx_1_VDD4_OVC_FLT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu2_Rx_1_VDD4_OVC_FLT_TIME;
            break;
        }
        case ASIC_RX_MSG_1mu2_Rx_1_VDD_4_OVC_SD_RESTART_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu2_Rx_1_VDD_4_OVC_SD_RESTART_TIME;
            break;
        }
        case ASIC_RX_MSG_1mu1_Rx_1_OSC_SpreadSpectrum_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu1_Rx_1_OSC_SpreadSpectrum_DIS;
            break;
        }
        case ASIC_RX_MSG_1mu1_Rx_1_PMP_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu1_Rx_1_PMP_EN;
            break;
        }
        case ASIC_RX_MSG_1mu1_Rx_1_FS_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu1_Rx_1_FS_EN;
            break;
        }
        case ASIC_RX_MSG_1mu1_Rx_1_FS_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu1_Rx_1_FS_CMD;
            break;
        }
        case ASIC_RX_MSG_1mu2_Rx_1_FS_VDS_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu2_Rx_1_FS_VDS_TH;
            break;
        }
        case ASIC_RX_MSG_1mu1_Rx_1_PHOLD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu1_Rx_1_PHOLD;
            break;
        }
        case ASIC_RX_MSG_1mu1_Rx_1_KA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_1.B.mu1_Rx_1_KA;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_0;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_1;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_2:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_2;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_3:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_3;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_4:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_4;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_5:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_5;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_6:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_6;
            break;
        }
        case ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_7:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64.B.mu1_Rx_64_SOLENOID_ENABLE_7;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH0_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH0_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH0_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH0_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH1_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH1_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH1_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH1_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH2_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH2_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH2_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH2_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH3_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH3_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH3_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH3_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH4_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH4_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH4_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH4_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH5_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH5_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH5_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH5_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH6_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH6_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH6_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH6_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH7_STATUS_L:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH7_STATUS_L;
            break;
        }
        case ASIC_RX_MSG_65mu1_Rx_65_CH7_STATUS_H:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_65.B.mu1_Rx_65_CH7_STATUS_H;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_0;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_1;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_2:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_2;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_3:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_3;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_4:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_4;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_5:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_5;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_6:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_6;
            break;
        }
        case ASIC_RX_MSG_64_2mu1_Rx_64_SOLENOID_ENABLE_7:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_64_2.B.mu1_Rx_64_SOLENOID_ENABLE_7;
            break;
        }
        case ASIC_RX_MSG_0mu16_Rx_0_CHIP_ID:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_0.B.mu16_Rx_0_CHIP_ID;
            break;
        }
        case ASIC_RX_MSG_2mu1_Rx_2_VDD2_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_2.B.mu1_Rx_2_VDD2_DIS;
            break;
        }
        case ASIC_RX_MSG_2mu1_Rx_2_VDD5_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_2.B.mu1_Rx_2_VDD5_DIS;
            break;
        }
        case ASIC_RX_MSG_2mu1_Rx_2_WD_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_2.B.mu1_Rx_2_WD_DIS;
            break;
        }
        case ASIC_RX_MSG_2mu1_Rx_2_VDD1_DIODELOSS_FILT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_2.B.mu1_Rx_2_VDD1_DIODELOSS_FILT;
            break;
        }
        case ASIC_RX_MSG_2mu1_Rx_2_VDD_2d5_AUTO_SWITCH_OFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_2.B.mu1_Rx_2_VDD_2d5_AUTO_SWITCH_OFF;
            break;
        }
        case ASIC_RX_MSG_2mu2_Rx_2_WD_SW_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_2.B.mu2_Rx_2_WD_SW_DIS;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_OSC_STUCK_MON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_OSC_STUCK_MON;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_OSC_FRQ_MON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_OSC_FRQ_MON;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_OSC_SELF_TEST_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_OSC_SELF_TEST_RESULT;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_T_SD_INT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_T_SD_INT;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_WDOG_TO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_WDOG_TO;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_CP_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_CP_OV;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_CP_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_CP_UV;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_FS_TURN_OFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_TURN_OFF;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_FS_TURN_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_TURN_ON;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_FS_VDS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_VDS_FAULT;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_FS_RVP_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_FS_RVP_FAULT;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_VHD_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VHD_OV;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_VPWR_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VPWR_OV;
            break;
        }
        case ASIC_RX_MSG_3mu1_Rx_3_VPWR_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_3.B.mu1_Rx_3_VPWR_UV;
            break;
        }
        case ASIC_RX_MSG_4mu8_Rx_4_DIE_TEMP_MONITOR:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_4.B.mu8_Rx_4_DIE_TEMP_MONITOR;
            break;
        }
        case ASIC_RX_MSG_5mu1_Rx_5_TOO_LONG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_TOO_LONG;
            break;
        }
        case ASIC_RX_MSG_5mu1_Rx_5_TOO_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_TOO_SHORT;
            break;
        }
        case ASIC_RX_MSG_5mu1_Rx_5_WRONG_CRC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_WRONG_CRC;
            break;
        }
        case ASIC_RX_MSG_5mu1_Rx_5_WRONG_FCNT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_WRONG_FCNT;
            break;
        }
        case ASIC_RX_MSG_5mu1_Rx_5_WRONG_ADD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_5.B.mu1_Rx_5_WRONG_ADD;
            break;
        }
        case ASIC_RX_MSG_6mu1_Rx_6_ANALOG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_6.B.mu1_Rx_6_ANALOG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_6mu1_Rx_6_CORE_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_6.B.mu1_Rx_6_CORE_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_6mu1_Rx_6_NVM_CRC_MASK:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_6.B.mu1_Rx_6_NVM_CRC_MASK;
            break;
        }
        case ASIC_RX_MSG_6mu1_Rx_6_NVM_DWN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_6.B.mu1_Rx_6_NVM_DWN;
            break;
        }
        case ASIC_RX_MSG_7mu1_Rx_7_AN_TRIM_CRC_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_7.B.mu1_Rx_7_AN_TRIM_CRC_RESULT;
            break;
        }
        case ASIC_RX_MSG_7mu1_Rx_7_NVM_CRC_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_7.B.mu1_Rx_7_NVM_CRC_RESULT;
            break;
        }
        case ASIC_RX_MSG_7mu1_Rx_7_NVM_BUSY:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_7.B.mu1_Rx_7_NVM_BUSY;
            break;
        }
        case ASIC_RX_MSG_9mu2_Rx_9_PMP_VDS_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu2_Rx_9_PMP_VDS_TH;
            break;
        }
        case ASIC_RX_MSG_9mu1_Rx_9_PMP2_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu1_Rx_9_PMP2_DIS;
            break;
        }
        case ASIC_RX_MSG_9mu3_Rx_9_PMP_VDS_FIL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu3_Rx_9_PMP_VDS_FIL;
            break;
        }
        case ASIC_RX_MSG_9mu1_Rx_9_LDACT_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu1_Rx_9_LDACT_DIS;
            break;
        }
        case ASIC_RX_MSG_9mu1_Rx_9_PMP1_ISINC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu1_Rx_9_PMP1_ISINC;
            break;
        }
        case ASIC_RX_MSG_9mu4_Rx_9_PMP_DT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu4_Rx_9_PMP_DT;
            break;
        }
        case ASIC_RX_MSG_9mu1_Rx_9_PMP_TEST:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu1_Rx_9_PMP_TEST;
            break;
        }
        case ASIC_RX_MSG_9mu1_Rx_9_PMP_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_9.B.mu1_Rx_9_PMP_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP1_TURN_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP1_TURN_ON;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP1_TURN_OFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP1_TURN_OFF;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP2_TURN_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP2_TURN_ON;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP2_TURN_OFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP2_TURN_OFF;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP3_TURN_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP3_TURN_ON;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP3_TURN_OFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP3_TURN_OFF;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP_VDS_TURNOFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_VDS_TURNOFF;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP1_VDS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP1_VDS_FAULT;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP_FLYBACK:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_FLYBACK;
            break;
        }
        case ASIC_RX_MSG_10mu1_Rx_10_PMP_LD_ACT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu1_Rx_10_PMP_LD_ACT;
            break;
        }
        case ASIC_RX_MSG_10mu3_Rx_10_PMP_TEST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu3_Rx_10_PMP_TEST_STATUS;
            break;
        }
        case ASIC_RX_MSG_10mu2_Rx_10_PMP_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_10.B.mu2_Rx_10_PMP_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD1_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_OVC;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD2_OUT_OF_REG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_OUT_OF_REG;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD4_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD4_OVC;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD3_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD3_OVC;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD1DIODE_LOSS_ECHO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1DIODE_LOSS_ECHO;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD2_REV_CURR:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_REV_CURR;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_T_SD;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD2_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_T_SD;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD1_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_UV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD2_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_UV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD3_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD3_UV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD4_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD4_UV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD1_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD1_OV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD2_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD2_OV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD3_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD3_OV;
            break;
        }
        case ASIC_RX_MSG_11mu1_Rx_11_VDD4_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_11.B.mu1_Rx_11_VDD4_OV;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD1_SEL_ECHO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD1_SEL_ECHO;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD2_SEL_ECHO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD2_SEL_ECHO;
            break;
        }
        case ASIC_RX_MSG_12mu2_Rx_12_RESERVED0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu2_Rx_12_RESERVED0;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD5_SEL_ECHO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_SEL_ECHO;
            break;
        }
        case ASIC_RX_MSG_12mu7_Rx_12_RESERVED1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu7_Rx_12_RESERVED1;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD5_OUT_OF_REG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_OUT_OF_REG;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD5_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_T_SD;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD5_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_UV;
            break;
        }
        case ASIC_RX_MSG_12mu1_Rx_12_VDD5_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_12.B.mu1_Rx_12_VDD5_OV;
            break;
        }
        case ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_13.B.mu10_Rx_13_ADC_DATA_OUT;
            break;
        }
        case ASIC_RX_MSG_13mu4_Rx_13_ADC_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_13.B.mu4_Rx_13_ADC_SEL;
            break;
        }
        case ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_13.B.mu1_Rx_13_ADC_BUSY;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_VSO_CONF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_VSO_CONF;
            break;
        }
        case ASIC_RX_MSG_15mu2_Rx_15_WSO_VSO_S:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu2_Rx_15_WSO_VSO_S;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SSIG_VSO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SSIG_VSO;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_VSO_PTEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_VSO_PTEN;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_VSO_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_VSO_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_VSO_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_VSO_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SEL_CONF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SEL_CONF;
            break;
        }
        case ASIC_RX_MSG_15mu2_Rx_15_WSO_SEL_S:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu2_Rx_15_WSO_SEL_S;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SSIG_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SSIG_SEL;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SEL_OUT_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SEL_OUT_CMD;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SEL_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SEL_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SEL_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SEL_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_15mu1_Rx_15_SEL_PTEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_15.B.mu1_Rx_15_SEL_PTEN;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_VSO_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_T_SD;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_VSO_OPENLOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_OPENLOAD;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_VSO_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_LVT;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_VSO_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_VSO_LS_CLAMP_ACT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_LS_CLAMP_ACT;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_VSO_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_VSO_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_SEL_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_T_SD;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_SEL_OPENLOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_OPENLOAD;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_SEL_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_LVT;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_SEL_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_SEL_LS_CLAMP_ACT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_LS_CLAMP_ACT;
            break;
        }
        case ASIC_RX_MSG_16mu1_Rx_16_SEL_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_16.B.mu1_Rx_16_SEL_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_WLD_CONF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_WLD_CONF;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_WLD_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_WLD_CMD;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_WLD_DIAGOFF_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_WLD_DIAGOFF_EN;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_WLD_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_WLD_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_SHLS_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_SHLS_CMD;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_SHLS_DIAGOFF_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_SHLS_DIAGOFF_EN;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_SHLS_CONFIG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_SHLS_CONFIG;
            break;
        }
        case ASIC_RX_MSG_17mu1_Rx_17_SHLS_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_17.B.mu1_Rx_17_SHLS_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_WLD_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_T_SD;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_WLD_OPL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_OPL;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_WLD_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_LVT;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_WLD_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_WLD_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_WLD_LS_CLAMP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_WLD_LS_CLAMP;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_SHLS_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_T_SD;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_SHLS_OPL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_OPL;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_SHLS_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_LVT;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_SHLS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_OVC;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_SHLS_VGS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_VGS_FAULT;
            break;
        }
        case ASIC_RX_MSG_18mu1_Rx_18_SHLS_LS_CLAMP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_18.B.mu1_Rx_18_SHLS_LS_CLAMP;
            break;
        }
        case ASIC_RX_MSG_19mu1_Rx_19_E_PVDS_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_19.B.mu1_Rx_19_E_PVDS_GPO;
            break;
        }
        case ASIC_RX_MSG_19mu1_Rx_19_E_VDD2d5dWSx_OT_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_19.B.mu1_Rx_19_E_VDD2d5dWSx_OT_GPO;
            break;
        }
        case ASIC_RX_MSG_19mu1_Rx_19_E_FBO_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_19.B.mu1_Rx_19_E_FBO_GPO;
            break;
        }
        case ASIC_RX_MSG_19mu1_Rx_19_E_FSPI_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_19.B.mu1_Rx_19_E_FSPI_GPO;
            break;
        }
        case ASIC_RX_MSG_19mu1_Rx_19_E_EN_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_19.B.mu1_Rx_19_E_EN_GPO;
            break;
        }
        case ASIC_RX_MSG_19mu1_Rx_19_E_FS_VDS_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_19.B.mu1_Rx_19_E_FS_VDS_GPO;
            break;
        }
        case ASIC_RX_MSG_20mu1_Rx_20_VINTA_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_VINTA_OV;
            break;
        }
        case ASIC_RX_MSG_20mu1_Rx_20_DGNDLOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_DGNDLOSS;
            break;
        }
        case ASIC_RX_MSG_20mu1_Rx_20_VINTA_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_VINTA_UV;
            break;
        }
        case ASIC_RX_MSG_20mu1_Rx_20_PORN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_PORN;
            break;
        }
        case ASIC_RX_MSG_20mu1_Rx_20_RES1_ECHO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_RES1_ECHO;
            break;
        }
        case ASIC_RX_MSG_20mu1_Rx_20_IGN_ECHO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_20.B.mu1_Rx_20_IGN_ECHO;
            break;
        }
        case ASIC_RX_MSG_32mu1_Rx_32_WSO_TEST:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_32.B.mu1_Rx_32_WSO_TEST;
            break;
        }
        case ASIC_RX_MSG_32mu7_Rx_32_CONFIG_RANGE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_32.B.mu7_Rx_32_CONFIG_RANGE;
            break;
        }
        case ASIC_RX_MSG_33mu8_Rx_33_WSI_FIRST_THRESHOLD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_33.B.mu8_Rx_33_WSI_FIRST_THRESHOLD;
            break;
        }
        case ASIC_RX_MSG_34mu8_Rx_34_WSI_OFFSET_THRESHOLD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_34.B.mu8_Rx_34_WSI_OFFSET_THRESHOLD;
            break;
        }
        case ASIC_RX_MSG_35mu2_Rx_35_SENSOR_TYPE_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_35.B.mu2_Rx_35_SENSOR_TYPE_SELECTION;
            break;
        }
        case ASIC_RX_MSG_35mu1_Rx_35_FIX_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_35.B.mu1_Rx_35_FIX_TH;
            break;
        }
        case ASIC_RX_MSG_35mu1_Rx_35_SS_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_35.B.mu1_Rx_35_SS_DIS;
            break;
        }
        case ASIC_RX_MSG_35mu1_Rx_35_PTEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_35.B.mu1_Rx_35_PTEN;
            break;
        }
        case ASIC_RX_MSG_35mu2_Rx_35_FILTER_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_35.B.mu2_Rx_35_FILTER_SELECTION;
            break;
        }
        case ASIC_RX_MSG_36mu2_Rx_36_SENSOR_TYPE_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_36.B.mu2_Rx_36_SENSOR_TYPE_SELECTION;
            break;
        }
        case ASIC_RX_MSG_36mu1_Rx_36_FIX_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_36.B.mu1_Rx_36_FIX_TH;
            break;
        }
        case ASIC_RX_MSG_36mu1_Rx_36_SS_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_36.B.mu1_Rx_36_SS_DIS;
            break;
        }
        case ASIC_RX_MSG_36mu1_Rx_36_PTEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_36.B.mu1_Rx_36_PTEN;
            break;
        }
        case ASIC_RX_MSG_36mu2_Rx_36_FILTER_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_36.B.mu2_Rx_36_FILTER_SELECTION;
            break;
        }
        case ASIC_RX_MSG_37mu2_Rx_37_SENSOR_TYPE_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_37.B.mu2_Rx_37_SENSOR_TYPE_SELECTION;
            break;
        }
        case ASIC_RX_MSG_37mu1_Rx_37_FIX_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_37.B.mu1_Rx_37_FIX_TH;
            break;
        }
        case ASIC_RX_MSG_37mu1_Rx_37_SS_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_37.B.mu1_Rx_37_SS_DIS;
            break;
        }
        case ASIC_RX_MSG_37mu1_Rx_37_PTEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_37.B.mu1_Rx_37_PTEN;
            break;
        }
        case ASIC_RX_MSG_37mu2_Rx_37_FILTER_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_37.B.mu2_Rx_37_FILTER_SELECTION;
            break;
        }
        case ASIC_RX_MSG_38mu2_Rx_38_SENSOR_TYPE_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_38.B.mu2_Rx_38_SENSOR_TYPE_SELECTION;
            break;
        }
        case ASIC_RX_MSG_38mu1_Rx_38_FIX_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_38.B.mu1_Rx_38_FIX_TH;
            break;
        }
        case ASIC_RX_MSG_38mu1_Rx_38_SS_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_38.B.mu1_Rx_38_SS_DIS;
            break;
        }
        case ASIC_RX_MSG_38mu1_Rx_38_PTEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_38.B.mu1_Rx_38_PTEN;
            break;
        }
        case ASIC_RX_MSG_38mu2_Rx_38_FILTER_SELECTION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_38.B.mu2_Rx_38_FILTER_SELECTION;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_CH1_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_CH1_EN;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_CH2_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_CH2_EN;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_CH3_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_CH3_EN;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_CH4_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_CH4_EN;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_DIAG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_DIAG;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_INIT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_INIT;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_WSI_VDD4_UV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_WSI_VDD4_UV;
            break;
        }
        case ASIC_RX_MSG_39mu1_Rx_39_FVPWR_ACT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_39.B.mu1_Rx_39_FVPWR_ACT;
            break;
        }
        case ASIC_RX_MSG_40mu8_Rx_40_WS1_CNT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_40.B.mu8_Rx_40_WS1_CNT;
            break;
        }
        case ASIC_RX_MSG_40mu1_Rx_40_WS_CNT_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_40.B.mu1_Rx_40_WS_CNT_OV;
            break;
        }
        case ASIC_RX_MSG_40mu1_Rx_40_WS_CNT_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_40.B.mu1_Rx_40_WS_CNT_EN;
            break;
        }
        case ASIC_RX_MSG_40mu1_Rx_40_WS_CNT_RST:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_40.B.mu1_Rx_40_WS_CNT_RST;
            break;
        }
        case ASIC_RX_MSG_41mu8_Rx_41_WS1_CNT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_41.B.mu8_Rx_41_WS1_CNT;
            break;
        }
        case ASIC_RX_MSG_41mu1_Rx_41_WS_CNT_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_41.B.mu1_Rx_41_WS_CNT_OV;
            break;
        }
        case ASIC_RX_MSG_41mu1_Rx_41_WS_CNT_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_41.B.mu1_Rx_41_WS_CNT_EN;
            break;
        }
        case ASIC_RX_MSG_41mu1_Rx_41_WS_CNT_RST:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_41.B.mu1_Rx_41_WS_CNT_RST;
            break;
        }
        case ASIC_RX_MSG_42mu8_Rx_42_WS1_CNT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_42.B.mu8_Rx_42_WS1_CNT;
            break;
        }
        case ASIC_RX_MSG_42mu1_Rx_42_WS_CNT_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_42.B.mu1_Rx_42_WS_CNT_OV;
            break;
        }
        case ASIC_RX_MSG_42mu1_Rx_42_WS_CNT_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_42.B.mu1_Rx_42_WS_CNT_EN;
            break;
        }
        case ASIC_RX_MSG_42mu1_Rx_42_WS_CNT_RST:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_42.B.mu1_Rx_42_WS_CNT_RST;
            break;
        }
        case ASIC_RX_MSG_43mu8_Rx_43_WS1_CNT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_43.B.mu8_Rx_43_WS1_CNT;
            break;
        }
        case ASIC_RX_MSG_43mu1_Rx_43_WS_CNT_OV:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_43.B.mu1_Rx_43_WS_CNT_OV;
            break;
        }
        case ASIC_RX_MSG_43mu1_Rx_43_WS_CNT_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_43.B.mu1_Rx_43_WS_CNT_EN;
            break;
        }
        case ASIC_RX_MSG_43mu1_Rx_43_WS_CNT_RST:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_43.B.mu1_Rx_43_WS_CNT_RST;
            break;
        }
        case ASIC_RX_MSG_44mu2_Rx_44_WSI1_LBIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu2_Rx_44_WSI1_LBIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_44mu2_Rx_44_WSI2_LBIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu2_Rx_44_WSI2_LBIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_44mu2_Rx_44_WSI3_LBIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu2_Rx_44_WSI3_LBIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_44mu2_Rx_44_WSI4_LBIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu2_Rx_44_WSI4_LBIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI1_LBIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI1_LBIST_EN;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI2_LBIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI2_LBIST_EN;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI3_LBIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI3_LBIST_EN;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI4_LBIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI4_LBIST_EN;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI1_SELFTEST_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI1_SELFTEST_RESULT;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI2_SELFTEST_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI2_SELFTEST_RESULT;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI3_SELFTEST_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI3_SELFTEST_RESULT;
            break;
        }
        case ASIC_RX_MSG_44mu1_Rx_44_WSI4_SELFTEST_RESULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_44.B.mu1_Rx_44_WSI4_SELFTEST_RESULT;
            break;
        }
        case ASIC_RX_MSG_48mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_48.B.mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_49mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_49.B.mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_50mu10_Rx_WSIRSDR2_BASE_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_50.B.mu10_Rx_WSIRSDR2_BASE_CURRENT;
            break;
        }
        case ASIC_RX_MSG_50mu1_Rx_WSIRSDR2_RESERVED0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_50.B.mu1_Rx_WSIRSDR2_RESERVED0;
            break;
        }
        case ASIC_RX_MSG_50mu2_Rx_WSIRSDR2_LOGIC_CH_ID:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_50.B.mu2_Rx_WSIRSDR2_LOGIC_CH_ID;
            break;
        }
        case ASIC_RX_MSG_51mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_LATCH_D0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_LATCH_D0;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_NO_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_NO_FAULT;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_STANDSTILL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_STANDSTILL;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_PTY_BIT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B.mu1_Rx_WSIRSDR3_PTY_BIT;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ZERO_0;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ZERO_1;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_PULSE_OVERFLOW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_PULSE_OVERFLOW;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_NODATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_NODATA;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_INVALID:        
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_INVALID;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_WSI_OT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_WSI_OT;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_OPEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_OPEN;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_CURRENT_HI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_STB:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_STB;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_STG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_STG;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_2:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ZERO_2;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_3:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ZERO_3;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ONOFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ONOFF;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ONE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ONE;
            break;
        }
        case ASIC_RX_MSG_51mu1_Rx_WSIRSDR3_ZERO_4:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_51.B1.mu1_Rx_WSIRSDR3_ZERO_4;
            break;
        }
        case ASIC_RX_MSG_52mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_52.B.mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_53mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_53.B.mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_54mu10_Rx_WSIRSDR2_BASE_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_54.B.mu10_Rx_WSIRSDR2_BASE_CURRENT;
            break;
        }
        case ASIC_RX_MSG_54mu1_Rx_WSIRSDR2_RESERVED0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_54.B.mu1_Rx_WSIRSDR2_RESERVED0;
            break;
        }
        case ASIC_RX_MSG_54mu2_Rx_WSIRSDR2_LOGIC_CH_ID:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_54.B.mu2_Rx_WSIRSDR2_LOGIC_CH_ID;
            break;
        }
        case ASIC_RX_MSG_55mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_LATCH_D0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_LATCH_D0;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_NO_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_NO_FAULT;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_STANDSTILL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_STANDSTILL;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_PTY_BIT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B.mu1_Rx_WSIRSDR3_PTY_BIT;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ZERO_0;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ZERO_1;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_PULSE_OVERFLOW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_PULSE_OVERFLOW;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_NODATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_NODATA;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_INVALID:        
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_INVALID;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_WSI_OT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_WSI_OT;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_OPEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_OPEN;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_CURRENT_HI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_STB:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_STB;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_STG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_STG;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_2:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ZERO_2;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_3:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ZERO_3;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ONOFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ONOFF;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ONE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ONE;
            break;
        }
        case ASIC_RX_MSG_55mu1_Rx_WSIRSDR3_ZERO_4:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_55.B1.mu1_Rx_WSIRSDR3_ZERO_4;
            break;
        }
        case ASIC_RX_MSG_56mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_56.B.mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_57mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_57.B.mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_58mu10_Rx_WSIRSDR2_BASE_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_58.B.mu10_Rx_WSIRSDR2_BASE_CURRENT;
            break;
        }
        case ASIC_RX_MSG_58mu2_Rx_WSIRSDR2_LOGIC_CH_ID:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_58.B.mu2_Rx_WSIRSDR2_LOGIC_CH_ID;
            break;
        }
        case ASIC_RX_MSG_59mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_LATCH_D0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_LATCH_D0;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_NO_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_NO_FAULT;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_STANDSTILL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_STANDSTILL;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_PTY_BIT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B.mu1_Rx_WSIRSDR3_PTY_BIT;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ZERO_0;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ZERO_1;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_PULSE_OVERFLOW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_PULSE_OVERFLOW;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_NODATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_NODATA;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_INVALID:        
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_INVALID;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_WSI_OT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_WSI_OT;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_OPEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_OPEN;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_CURRENT_HI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_STB:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_STB;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_STG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_STG;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_2:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ZERO_2;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_3:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ZERO_3;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ONOFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ONOFF;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ONE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ONE;
            break;
        }
        case ASIC_RX_MSG_59mu1_Rx_WSIRSDR3_ZERO_4:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_59.B1.mu1_Rx_WSIRSDR3_ZERO_4;
            break;
        }
        case ASIC_RX_MSG_60mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_60.B.mu10_Rx_WSIRSDR0_LOW_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_61mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_61.B.mu10_Rx_WSIRSDR1_HIGH_THRESHOLD_CURRENT;
            break;
        }
        case ASIC_RX_MSG_62mu10_Rx_WSIRSDR2_BASE_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_62.B.mu10_Rx_WSIRSDR2_BASE_CURRENT;
            break;
        }
        case ASIC_RX_MSG_62mu2_Rx_WSIRSDR2_LOGIC_CH_ID:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_62.B.mu2_Rx_WSIRSDR2_LOGIC_CH_ID;
            break;
        }
        case ASIC_RX_MSG_63mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu12_Rx_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_LATCH_D0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_LATCH_D0;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_NO_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_NO_FAULT;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_STANDSTILL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_STANDSTILL;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_PTY_BIT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B.mu1_Rx_WSIRSDR3_PTY_BIT;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_0:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ZERO_0;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_1:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ZERO_1;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_PULSE_OVERFLOW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_PULSE_OVERFLOW;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_NODATA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_NODATA;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_INVALID:        
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_INVALID;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_WSI_OT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_WSI_OT;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_OPEN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_OPEN;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_CURRENT_HI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_CURRENT_HI;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_STB:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_STB;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_STG:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_STG;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_2:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ZERO_2;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_3:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ZERO_3;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ONOFF:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ONOFF;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ONE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ONE;
            break;
        }
        case ASIC_RX_MSG_63mu1_Rx_WSIRSDR3_ZERO_4:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_63.B1.mu1_Rx_WSIRSDR3_ZERO_4;
            break;
        }

        case ASIC_RX_MSG_67mu1_Rx_67_CH1_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH1_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH1_TD_BLANK:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH1_TD_BLANK;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH2_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH2_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH_2TD_BLANK:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH_2TD_BLANK;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH3_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH3_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH3_TD_BLANK:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH3_TD_BLANK;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH4_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH4_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH4_TD_BLANK:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH4_TD_BLANK;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH1_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH1_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH2_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH2_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH3_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH3_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH4_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH4_EN;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH1_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH1_CMD;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH2_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH2_CMD;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH3_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH3_CMD;
            break;
        }
        case ASIC_RX_MSG_67mu1_Rx_67_CH4_CMD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_67.B.mu1_Rx_67_CH4_CMD;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_T_SD;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_UNDER_CURR:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_UNDER_CURR;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_OPL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_OPL;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_LVT;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_68mu1_Rx_CH_DIAG_LS_CLAMP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_68.B.mu1_Rx_CH_DIAG_LS_CLAMP;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_T_SD;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_UNDER_CURR:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_UNDER_CURR;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_OPL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_OPL;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_LVT;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_69mu1_Rx_CH_DIAG_LS_CLAMP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_69.B.mu1_Rx_CH_DIAG_LS_CLAMP;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_T_SD;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_UNDER_CURR:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_UNDER_CURR;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_OPL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_OPL;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_LVT;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_70mu1_Rx_CH_DIAG_LS_CLAMP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_70.B.mu1_Rx_CH_DIAG_LS_CLAMP;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_T_SD;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_UNDER_CURR:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_UNDER_CURR;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_OPL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_OPL;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_LVT;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_71mu1_Rx_CH_DIAG_LS_CLAMP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_71.B.mu1_Rx_CH_DIAG_LS_CLAMP;
            break;
        }
        case ASIC_RX_MSG_72mu8_Rx_72_SEED:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_72.B.mu8_Rx_72_SEED;
            break;
        }
        case ASIC_RX_MSG_73mu4_Rx_73_WD_CNT_VALUE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu4_Rx_73_WD_CNT_VALUE;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_EARLY_ANSW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_EARLY_ANSW;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_LATE_ANSW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_LATE_ANSW;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_BAD_ANSW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_BAD_ANSW;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_EARLY_REQ:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_EARLY_REQ;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_LATE_REQ:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_LATE_REQ;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_RST_TO_ANSW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_RST_TO_ANSW;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_RST_TO_REQ:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_RST_TO_REQ;
            break;
        }
        case ASIC_RX_MSG_73mu1_Rx_73_WD_RST_CNT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu1_Rx_73_WD_RST_CNT;
            break;
        }
        case ASIC_RX_MSG_73mu4_Rx_73_WD_RST_EVENT_VALUE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_73.B.mu4_Rx_73_WD_RST_EVENT_VALUE;
            break;
        }
        case ASIC_RX_MSG_74mu8_Rx_74_VALID_ANSWER_START:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_74.B.mu8_Rx_74_VALID_ANSWER_START;
            break;
        }
        case ASIC_RX_MSG_74mu8_Rx_74_VALID_REQUEST_START:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_74.B.mu8_Rx_74_VALID_REQUEST_START;
            break;
        }
        case ASIC_RX_MSG_75mu6_Rx_75_VALID_ANSWER_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_75.B.mu6_Rx_75_VALID_ANSWER_DELTA;
            break;
        }
        case ASIC_RX_MSG_75mu6_Rx_75_VALID_REQUEST_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_75.B.mu6_Rx_75_VALID_REQUEST_DELTA;
            break;
        }
        case ASIC_RX_MSG_75mu1_Rx_75_REQ_CHECK_ENABLE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_75.B.mu1_Rx_75_REQ_CHECK_ENABLE;
            break;
        }
        case ASIC_RX_MSG_76mu6_Rx_76_ANSWER_TIME_OUT_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_76.B.mu6_Rx_76_ANSWER_TIME_OUT_DELTA;
            break;
        }
        case ASIC_RX_MSG_76mu6_Rx_76_REQUEST_TIME_OUT_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_76.B.mu6_Rx_76_REQUEST_TIME_OUT_DELTA;
            break;
        }
        case ASIC_RX_MSG_76mu1_Rx_76_TO_RESET_ENABLE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_76.B.mu1_Rx_76_TO_RESET_ENABLE;
            break;
        }
        case ASIC_RX_MSG_77mu3_Rx_77_NUM_OF_GOOD_STEPS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_77.B.mu3_Rx_77_NUM_OF_GOOD_STEPS;
            break;
        }
        case ASIC_RX_MSG_77mu3_Rx_77_NUM_OF_BAD_STEPS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_77.B.mu3_Rx_77_NUM_OF_BAD_STEPS;
            break;
        }
        case ASIC_RX_MSG_77mu4_Rx_77_HIGH_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_77.B.mu4_Rx_77_HIGH_TH;
            break;
        }
        case ASIC_RX_MSG_77mu4_Rx_77_LOW_TH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_77.B.mu4_Rx_77_LOW_TH;
            break;
        }
        case ASIC_RX_MSG_77mu1_Rx_77_CLOCK_DIVISION:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_77.B.mu1_Rx_77_CLOCK_DIVISION;
            break;
        }
        case ASIC_RX_MSG_77mu1_Rx_77_RESET_ENABLE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_77.B.mu1_Rx_77_RESET_ENABLE;
            break;
        }
        case ASIC_RX_MSG_78mu8_Rx_78_ANSWER_LOW:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_78.B.mu8_Rx_78_ANSWER_LOW;
            break;
        }
        case ASIC_RX_MSG_78mu8_Rx_78_ANSWER_HIGH:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_78.B.mu8_Rx_78_ANSWER_HIGH;
            break;
        }
        case ASIC_RX_MSG_NO0_0mu1_Rx_NT0_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu1_Rx_NT0_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO0_0mu1_Rx_NT0_CURR_SENSE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu1_Rx_NT0_CURR_SENSE;
            break;
        }
        case ASIC_RX_MSG_NO0_0mu1_Rx_NT0_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu1_Rx_NT0_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO0_0mu2_Rx_NT0_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_0.B.mu2_Rx_NT0_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_VGS_HS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_HS_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_HS_SHORT;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_LVT;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_NO0_1mu1_Rx_NT1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_1.B.mu1_Rx_NT1_T_SD;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu2_Rx_NT2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu2_Rx_NT2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_NO0_2mu1_Rx_NT2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_2.B.mu1_Rx_NT2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_NO0_5mu11_Rx_NT5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_5.B.mu11_Rx_NT5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_NO0_6mu10_Rx_NT6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_6.B.mu10_Rx_NT6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO0_7mu8_Rx_NT7_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_7.B.mu8_Rx_NT7_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_NO0_7mu7_Rx_NT7_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_7.B.mu7_Rx_NT7_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_NO0_8mu3_Rx_NT8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_8.B.mu3_Rx_NT8_KP;
            break;
        }
        case ASIC_RX_MSG_NO0_8mu3_Rx_NT8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_8.B.mu3_Rx_NT8_KI;
            break;
        }
        case ASIC_RX_MSG_NO0_9mu2_Rx_NT9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_9.B.mu2_Rx_NT9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_NO0_9mu2_Rx_NT9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_9.B.mu2_Rx_NT9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_NO0_9mu1_Rx_NT9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_9.B.mu1_Rx_NT9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_NO0_10mu12_Rx_NT10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_10.B.mu12_Rx_NT10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO0_12mu13_Rx_NT12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_12.B.mu13_Rx_NT12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO0_12mu1_Rx_NT12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_12.B.mu1_Rx_NT12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_NO0_13mu8_Rx_NT13_BASE_DELTA_CURENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_13.B.mu8_Rx_NT13_BASE_DELTA_CURENT;
            break;
        }
        case ASIC_RX_MSG_NO0_14mu8_Rx_NT14_DELTA_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO0_14.B.mu8_Rx_NT14_DELTA_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO1_0mu1_Rx_NT0_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu1_Rx_NT0_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO1_0mu1_Rx_NT0_CURR_SENSE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu1_Rx_NT0_CURR_SENSE;
            break;
        }
        case ASIC_RX_MSG_NO1_0mu1_Rx_NT0_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu1_Rx_NT0_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO1_0mu2_Rx_NT0_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_0.B.mu2_Rx_NT0_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_VGS_HS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_HS_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_HS_SHORT;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_LVT;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_NO1_1mu1_Rx_NT1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_1.B.mu1_Rx_NT1_T_SD;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu2_Rx_NT2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu2_Rx_NT2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_NO1_2mu1_Rx_NT2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_2.B.mu1_Rx_NT2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_NO1_5mu11_Rx_NT5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_5.B.mu11_Rx_NT5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_NO1_6mu10_Rx_NT6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_6.B.mu10_Rx_NT6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO1_7mu8_Rx_NT7_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_7.B.mu8_Rx_NT7_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_NO1_7mu7_Rx_NT7_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_7.B.mu7_Rx_NT7_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_NO1_8mu3_Rx_NT8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_8.B.mu3_Rx_NT8_KP;
            break;
        }
        case ASIC_RX_MSG_NO1_8mu3_Rx_NT8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_8.B.mu3_Rx_NT8_KI;
            break;
        }
        case ASIC_RX_MSG_NO1_9mu2_Rx_NT9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_9.B.mu2_Rx_NT9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_NO1_9mu2_Rx_NT9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_9.B.mu2_Rx_NT9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_NO1_9mu1_Rx_NT9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_9.B.mu1_Rx_NT9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_NO1_10mu12_Rx_NT10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_10.B.mu12_Rx_NT10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO1_12mu13_Rx_NT12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_12.B.mu13_Rx_NT12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO1_12mu1_Rx_NT12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_12.B.mu1_Rx_NT12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_NO1_13mu8_Rx_NT13_BASE_DELTA_CURENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_13.B.mu8_Rx_NT13_BASE_DELTA_CURENT;
            break;
        }
        case ASIC_RX_MSG_NO1_14mu8_Rx_NT14_DELTA_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO1_14.B.mu8_Rx_NT14_DELTA_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO2_0mu1_Rx_NT0_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu1_Rx_NT0_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO2_0mu1_Rx_NT0_CURR_SENSE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu1_Rx_NT0_CURR_SENSE;
            break;
        }
        case ASIC_RX_MSG_NO2_0mu1_Rx_NT0_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu1_Rx_NT0_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO2_0mu2_Rx_NT0_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_0.B.mu2_Rx_NT0_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_VGS_HS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_HS_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_HS_SHORT;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_LVT;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_NO2_1mu1_Rx_NT1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_1.B.mu1_Rx_NT1_T_SD;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu2_Rx_NT2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu2_Rx_NT2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_NO2_2mu1_Rx_NT2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_2.B.mu1_Rx_NT2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_NO2_5mu11_Rx_NT5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_5.B.mu11_Rx_NT5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_NO2_6mu10_Rx_NT6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_6.B.mu10_Rx_NT6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO2_7mu8_Rx_NT7_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_7.B.mu8_Rx_NT7_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_NO2_7mu7_Rx_NT7_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_7.B.mu7_Rx_NT7_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_NO2_8mu3_Rx_NT8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_8.B.mu3_Rx_NT8_KP;
            break;
        }
        case ASIC_RX_MSG_NO2_8mu3_Rx_NT8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_8.B.mu3_Rx_NT8_KI;
            break;
        }
        case ASIC_RX_MSG_NO2_9mu2_Rx_NT9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_9.B.mu2_Rx_NT9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_NO2_9mu2_Rx_NT9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_9.B.mu2_Rx_NT9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_NO2_9mu1_Rx_NT9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_9.B.mu1_Rx_NT9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_NO2_10mu12_Rx_NT10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_10.B.mu12_Rx_NT10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO2_12mu13_Rx_NT12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_12.B.mu13_Rx_NT12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO2_12mu1_Rx_NT12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_12.B.mu1_Rx_NT12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_NO2_13mu8_Rx_NT13_BASE_DELTA_CURENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_13.B.mu8_Rx_NT13_BASE_DELTA_CURENT;
            break;
        }
        case ASIC_RX_MSG_NO2_14mu8_Rx_NT14_DELTA_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO2_14.B.mu8_Rx_NT14_DELTA_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO3_0mu1_Rx_NT0_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu1_Rx_NT0_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO3_0mu1_Rx_NT0_CURR_SENSE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu1_Rx_NT0_CURR_SENSE;
            break;
        }
        case ASIC_RX_MSG_NO3_0mu1_Rx_NT0_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu1_Rx_NT0_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO3_0mu2_Rx_NT0_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_0.B.mu2_Rx_NT0_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_VGS_HS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_HS_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_HS_SHORT;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_LVT;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_NO3_1mu1_Rx_NT1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_1.B.mu1_Rx_NT1_T_SD;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu2_Rx_NT2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu2_Rx_NT2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_NO3_2mu1_Rx_NT2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_2.B.mu1_Rx_NT2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_NO3_5mu11_Rx_NT5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_5.B.mu11_Rx_NT5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_NO3_6mu10_Rx_NT6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_6.B.mu10_Rx_NT6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO3_7mu8_Rx_NT7_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_7.B.mu8_Rx_NT7_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_NO3_7mu7_Rx_NT7_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_7.B.mu7_Rx_NT7_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_NO3_8mu3_Rx_NT8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_8.B.mu3_Rx_NT8_KP;
            break;
        }
        case ASIC_RX_MSG_NO3_8mu3_Rx_NT8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_8.B.mu3_Rx_NT8_KI;
            break;
        }
        case ASIC_RX_MSG_NO3_9mu2_Rx_NT9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_9.B.mu2_Rx_NT9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_NO3_9mu2_Rx_NT9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_9.B.mu2_Rx_NT9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_NO3_9mu1_Rx_NT9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_9.B.mu1_Rx_NT9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_NO3_10mu12_Rx_NT10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_10.B.mu12_Rx_NT10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_NO3_12mu13_Rx_NT12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_12.B.mu13_Rx_NT12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_NO3_12mu1_Rx_NT12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_12.B.mu1_Rx_NT12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_NO3_13mu8_Rx_NT13_BASE_DELTA_CURENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_13.B.mu8_Rx_NT13_BASE_DELTA_CURENT;
            break;
        }
        case ASIC_RX_MSG_NO3_14mu8_Rx_NT14_DELTA_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_NO3_14.B.mu8_Rx_NT14_DELTA_CURRENT;
            break;
        }
        case ASIC_RX_MSG_TC0_0mu1_Rx_NT0_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu1_Rx_NT0_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC0_0mu1_Rx_NT0_CURR_SENSE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu1_Rx_NT0_CURR_SENSE;
            break;
        }
        case ASIC_RX_MSG_TC0_0mu1_Rx_NT0_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu1_Rx_NT0_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC0_0mu2_Rx_NT0_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_0.B.mu2_Rx_NT0_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_VGS_HS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_HS_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_HS_SHORT;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_LVT;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_TC0_1mu1_Rx_NT1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_1.B.mu1_Rx_NT1_T_SD;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu2_Rx_NT2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu2_Rx_NT2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_TC0_2mu1_Rx_NT2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_2.B.mu1_Rx_NT2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_TC0_5mu11_Rx_NT5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_5.B.mu11_Rx_NT5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_TC0_6mu10_Rx_NT6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_6.B.mu10_Rx_NT6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_TC0_7mu8_Rx_NT7_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_7.B.mu8_Rx_NT7_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_TC0_7mu7_Rx_NT7_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_7.B.mu7_Rx_NT7_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_TC0_8mu3_Rx_NT8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_8.B.mu3_Rx_NT8_KP;
            break;
        }
        case ASIC_RX_MSG_TC0_8mu3_Rx_NT8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_8.B.mu3_Rx_NT8_KI;
            break;
        }
        case ASIC_RX_MSG_TC0_9mu2_Rx_NT9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_9.B.mu2_Rx_NT9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_TC0_9mu2_Rx_NT9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_9.B.mu2_Rx_NT9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_TC0_9mu1_Rx_NT9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_9.B.mu1_Rx_NT9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_TC0_10mu12_Rx_NT10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_10.B.mu12_Rx_NT10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_TC0_12mu13_Rx_NT12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_12.B.mu13_Rx_NT12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_TC0_12mu1_Rx_NT12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_12.B.mu1_Rx_NT12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_TC0_13mu8_Rx_NT13_BASE_DELTA_CURENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_13.B.mu8_Rx_NT13_BASE_DELTA_CURENT;
            break;
        }
        case ASIC_RX_MSG_TC0_14mu8_Rx_NT14_DELTA_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC0_14.B.mu8_Rx_NT14_DELTA_CURRENT;
            break;
        }
        case ASIC_RX_MSG_TC1_0mu1_Rx_NT0_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu1_Rx_NT0_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC1_0mu1_Rx_NT0_CURR_SENSE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu1_Rx_NT0_CURR_SENSE;
            break;
        }
        case ASIC_RX_MSG_TC1_0mu1_Rx_NT0_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu1_Rx_NT0_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC1_0mu2_Rx_NT0_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_0.B.mu2_Rx_NT0_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_VGS_HS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_VGS_HS_FAULT;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_HS_SHORT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_HS_SHORT;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_LVT;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_TC1_1mu1_Rx_NT1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_1.B.mu1_Rx_NT1_T_SD;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu2_Rx_NT2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu2_Rx_NT2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_TC1_2mu1_Rx_NT2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_2.B.mu1_Rx_NT2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_TC1_5mu11_Rx_NT5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_5.B.mu11_Rx_NT5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_TC1_6mu10_Rx_NT6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_6.B.mu10_Rx_NT6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_TC1_7mu8_Rx_NT7_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_7.B.mu8_Rx_NT7_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_TC1_7mu7_Rx_NT7_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_7.B.mu7_Rx_NT7_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_TC1_8mu3_Rx_NT8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_8.B.mu3_Rx_NT8_KP;
            break;
        }
        case ASIC_RX_MSG_TC1_8mu3_Rx_NT8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_8.B.mu3_Rx_NT8_KI;
            break;
        }
        case ASIC_RX_MSG_TC1_9mu2_Rx_NT9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_9.B.mu2_Rx_NT9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_TC1_9mu2_Rx_NT9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_9.B.mu2_Rx_NT9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_TC1_9mu1_Rx_NT9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_9.B.mu1_Rx_NT9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_TC1_10mu12_Rx_NT10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_10.B.mu12_Rx_NT10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_TC1_12mu13_Rx_NT12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_12.B.mu13_Rx_NT12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_TC1_12mu1_Rx_NT12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_12.B.mu1_Rx_NT12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_TC1_13mu8_Rx_NT13_BASE_DELTA_CURENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_13.B.mu8_Rx_NT13_BASE_DELTA_CURENT;
            break;
        }
        case ASIC_RX_MSG_TC1_14mu8_Rx_NT14_DELTA_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_TC1_14.B.mu8_Rx_NT14_DELTA_CURRENT;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_LVT;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_T_SD;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu1_Rx_ESV1_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu1_Rx_ESV1_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_ESV0_1mu2_Rx_ESV1_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_1.B.mu2_Rx_ESV1_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu2_Rx_ESV2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu2_Rx_ESV2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_2.B.mu1_Rx_ESV2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_ESV0_5mu11_Rx_ESV5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_5.B.mu11_Rx_ESV5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_ESV0_6mu6_Rx_ESV6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_6.B.mu6_Rx_ESV6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_ESV0_6mu4_Rx_ESV6_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_6.B.mu4_Rx_ESV6_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_ESV0_6mu5_Rx_ESV6_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_6.B.mu5_Rx_ESV6_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_ESV0_8mu3_Rx_ESV8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_8.B.mu3_Rx_ESV8_KP;
            break;
        }
        case ASIC_RX_MSG_ESV0_8mu3_Rx_ESV8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_8.B.mu3_Rx_ESV8_KI;
            break;
        }
        case ASIC_RX_MSG_ESV0_9mu2_Rx_ESV9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_9.B.mu2_Rx_ESV9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_ESV0_9mu2_Rx_ESV9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_9.B.mu2_Rx_ESV9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_ESV0_9mu1_Rx_ESV9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_9.B.mu1_Rx_ESV9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_ESV0_10mu11_Rx_ESV10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_10.B.mu11_Rx_ESV10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_ESV0_12mu12_Rx_ESV12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_12.B.mu12_Rx_ESV12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_ESV0_12mu1_Rx_ESV12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV0_12.B.mu1_Rx_ESV12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_LS_CLAMP_ON:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_LS_CLAMP_ON;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_LS_OVC:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_LS_OVC;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_VGS_LS_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_VGS_LS_FAULT;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_LVT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_LVT;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_OPEN_LOAD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_OPEN_LOAD;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_GND_LOSS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_GND_LOSS;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_PWM_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_PWM_FAULT;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_TH_WARN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_TH_WARN;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_T_SD:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_T_SD;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu1_Rx_ESV1_ADC_FAULT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu1_Rx_ESV1_ADC_FAULT;
            break;
        }
        case ASIC_RX_MSG_ESV1_1mu2_Rx_ESV1_LOGIC_BIST_STATUS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_1.B.mu2_Rx_ESV1_LOGIC_BIST_STATUS;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu2_Rx_ESV2_SR_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu2_Rx_ESV2_SR_SEL;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_CALIBRATION_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_CALIBRATION_DIS;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_OFS_CHOP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_OFS_CHOP_DIS;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_TD_BLANK_SEL:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_TD_BLANK_SEL;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_LS_CLAMP_DIS:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_LS_CLAMP_DIS;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_LOGIC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_LOGIC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_DIAG_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_DIAG_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_ADC_BIST_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_ADC_BIST_EN;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_OFF_DIAG_EN:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_OFF_DIAG_EN;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_E_SOL_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_E_SOL_GPO;
            break;
        }
        case ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_E_LSCLAMP_GPO:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_2.B.mu1_Rx_ESV2_E_LSCLAMP_GPO;
            break;
        }
        case ASIC_RX_MSG_ESV1_5mu11_Rx_ESV5_SET_POINT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_5.B.mu11_Rx_ESV5_SET_POINT;
            break;
        }
        case ASIC_RX_MSG_ESV1_6mu6_Rx_ESV6_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_6.B.mu6_Rx_ESV6_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_ESV1_6mu4_Rx_ESV6_FREQ_MOD_STEP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_6.B.mu4_Rx_ESV6_FREQ_MOD_STEP;
            break;
        }
        case ASIC_RX_MSG_ESV1_6mu5_Rx_ESV6_MAX_FREQ_DELTA:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_6.B.mu5_Rx_ESV6_MAX_FREQ_DELTA;
            break;
        }
        case ASIC_RX_MSG_ESV1_8mu3_Rx_ESV8_KP:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_8.B.mu3_Rx_ESV8_KP;
            break;
        }
        case ASIC_RX_MSG_ESV1_8mu3_Rx_ESV8_KI:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_8.B.mu3_Rx_ESV8_KI;
            break;
        }
        case ASIC_RX_MSG_ESV1_9mu2_Rx_ESV9_INTTIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_9.B.mu2_Rx_ESV9_INTTIME;
            break;
        }
        case ASIC_RX_MSG_ESV1_9mu2_Rx_ESV9_FILT_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_9.B.mu2_Rx_ESV9_FILT_TIME;
            break;
        }
        case ASIC_RX_MSG_ESV1_9mu1_Rx_ESV9_CHOP_TIME:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_9.B.mu1_Rx_ESV9_CHOP_TIME;
            break;
        }
        case ASIC_RX_MSG_ESV1_10mu11_Rx_ESV10_AVG_CURRENT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_10.B.mu11_Rx_ESV10_AVG_CURRENT;
            break;
        }
        case ASIC_RX_MSG_ESV1_12mu12_Rx_ESV12_PWM_CODE:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_12.B.mu12_Rx_ESV12_PWM_CODE;
            break;
        }
        case ASIC_RX_MSG_ESV1_12mu1_Rx_ESV12_TMOUT:
        {
            *Data = (uint16)ASIC_RX_MSG.A.M.ASIC_RX_MSG_ESV1_12.B.mu1_Rx_ESV12_TMOUT;
            break;
        }
        case ASIC_RX_MSG_ZERO_RETURN:
        {
            *Data = 0u;
            break;
        }
        default:
        {
            break;
        }
        
    }

    return rtn;
}


#define ACH_INPUT_STOP_SEC_CODE
#include "Ach_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

