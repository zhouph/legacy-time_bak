/**
 * @defgroup Ach_Output Ach_Output
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Output.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Output.h"
#include "Ach_Output_Ifa.h"
#include "Ach_Output_Process.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACH_OUTPUT_STOP_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ach_Output_HdrBusType Ach_OutputBus;

/* Version Info */
const SwcVersionInfo_t Ach_OutputVersionInfo = 
{   
    ACH_OUTPUT_MODULE_ID,           /* Ach_OutputVersionInfo.ModuleId */
    ACH_OUTPUT_MAJOR_VERSION,       /* Ach_OutputVersionInfo.MajorVer */
    ACH_OUTPUT_MINOR_VERSION,       /* Ach_OutputVersionInfo.MinorVer */
    ACH_OUTPUT_PATCH_VERSION,       /* Ach_OutputVersionInfo.PatchVer */
    ACH_OUTPUT_BRANCH_VERSION       /* Ach_OutputVersionInfo.BranchVer */
};
    
/* Input Data Element */
Aka_MainAkaKeepAliveWriteData_t Ach_OutputAkaKeepAliveWriteData;
Ioc_OutputSR1msIocDcMtrDutyData_t Ach_OutputIocDcMtrDutyData;
Ioc_OutputSR1msIocDcMtrFreqData_t Ach_OutputIocDcMtrFreqData;
Ioc_OutputSR1msIocVlvNo0Data_t Ach_OutputIocVlvNo0Data;
Ioc_OutputSR1msIocVlvNo1Data_t Ach_OutputIocVlvNo1Data;
Ioc_OutputSR1msIocVlvNo2Data_t Ach_OutputIocVlvNo2Data;
Ioc_OutputSR1msIocVlvNo3Data_t Ach_OutputIocVlvNo3Data;
Ioc_OutputSR1msIocVlvTc0Data_t Ach_OutputIocVlvTc0Data;
Ioc_OutputSR1msIocVlvTc1Data_t Ach_OutputIocVlvTc1Data;
Ioc_OutputSR1msIocVlvEsv0Data_t Ach_OutputIocVlvEsv0Data;
Ioc_OutputSR1msIocVlvEsv1Data_t Ach_OutputIocVlvEsv1Data;
Ioc_OutputSR1msIocVlvNc0Data_t Ach_OutputIocVlvNc0Data;
Ioc_OutputSR1msIocVlvNc1Data_t Ach_OutputIocVlvNc1Data;
Ioc_OutputSR1msIocVlvNc2Data_t Ach_OutputIocVlvNc2Data;
Ioc_OutputSR1msIocVlvNc3Data_t Ach_OutputIocVlvNc3Data;
Ioc_OutputSR1msIocVlvRelayData_t Ach_OutputIocVlvRelayData;
Ioc_OutputSR1msIocAdcSelWriteData_t Ach_OutputIocAdcSelWriteData;
Acm_MainAcmAsicInitCompleteFlag_t Ach_OutputAcmAsicInitCompleteFlag;

/* Output Data Element */

uint32 Ach_Output_Timer_Start;
uint32 Ach_Output_Timer_Elapsed;

#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_OUTPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_OUTPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_CODE
#include "Ach_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ach_Output_Init(void)
{
    /* Initialize internal bus */
    Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg = 0;
    Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD = 0;
    Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA = 0;
    Ach_OutputBus.Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg = 0;
    Ach_OutputBus.Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM = 0;
    Ach_OutputBus.Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg = 0;
    Ach_OutputBus.Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo0Data.VlvNo0DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo1Data.VlvNo1DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo2Data.VlvNo2DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo3Data.VlvNo3DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvTc0Data.VlvTc0DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvTc1Data.VlvTc1DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc0Data.VlvNc0DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc1Data.VlvNc1DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc2Data.VlvNc2DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc3Data.VlvNc3DataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT = 0;
    Ach_OutputBus.Ach_OutputIocVlvRelayData.VlvRelayDataFlg = 0;
    Ach_OutputBus.Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD = 0;
    Ach_OutputBus.Ach_OutputIocAdcSelWriteData.AdcSelDataFlg = 0;
    Ach_OutputBus.Ach_OutputIocAdcSelWriteData.ACH_Tx13_ADC_SEL = 0;
    Ach_OutputBus.Ach_OutputAcmAsicInitCompleteFlag = 0;
    Ach_Init();
}

void Ach_Output(void)
{
    Ach_Output_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ach_Output_Read_Ach_OutputAkaKeepAliveWriteData(&Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData);
    /*==============================================================================
    * Members of structure Ach_OutputAkaKeepAliveWriteData 
     : Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg;
     : Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD;
     : Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocDcMtrDutyData(&Ach_OutputBus.Ach_OutputIocDcMtrDutyData);
    /*==============================================================================
    * Members of structure Ach_OutputIocDcMtrDutyData 
     : Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg;
     : Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocDcMtrFreqData(&Ach_OutputBus.Ach_OutputIocDcMtrFreqData);
    /*==============================================================================
    * Members of structure Ach_OutputIocDcMtrFreqData 
     : Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg;
     : Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNo0Data(&Ach_OutputBus.Ach_OutputIocVlvNo0Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo0Data 
     : Ach_OutputIocVlvNo0Data.VlvNo0DataFlg;
     : Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNo1Data(&Ach_OutputBus.Ach_OutputIocVlvNo1Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo1Data 
     : Ach_OutputIocVlvNo1Data.VlvNo1DataFlg;
     : Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNo2Data(&Ach_OutputBus.Ach_OutputIocVlvNo2Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo2Data 
     : Ach_OutputIocVlvNo2Data.VlvNo2DataFlg;
     : Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNo3Data(&Ach_OutputBus.Ach_OutputIocVlvNo3Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo3Data 
     : Ach_OutputIocVlvNo3Data.VlvNo3DataFlg;
     : Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvTc0Data(&Ach_OutputBus.Ach_OutputIocVlvTc0Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvTc0Data 
     : Ach_OutputIocVlvTc0Data.VlvTc0DataFlg;
     : Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvTc1Data(&Ach_OutputBus.Ach_OutputIocVlvTc1Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvTc1Data 
     : Ach_OutputIocVlvTc1Data.VlvTc1DataFlg;
     : Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvEsv0Data(&Ach_OutputBus.Ach_OutputIocVlvEsv0Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvEsv0Data 
     : Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg;
     : Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvEsv1Data(&Ach_OutputBus.Ach_OutputIocVlvEsv1Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvEsv1Data 
     : Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg;
     : Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNc0Data(&Ach_OutputBus.Ach_OutputIocVlvNc0Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc0Data 
     : Ach_OutputIocVlvNc0Data.VlvNc0DataFlg;
     : Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNc1Data(&Ach_OutputBus.Ach_OutputIocVlvNc1Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc1Data 
     : Ach_OutputIocVlvNc1Data.VlvNc1DataFlg;
     : Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNc2Data(&Ach_OutputBus.Ach_OutputIocVlvNc2Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc2Data 
     : Ach_OutputIocVlvNc2Data.VlvNc2DataFlg;
     : Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvNc3Data(&Ach_OutputBus.Ach_OutputIocVlvNc3Data);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc3Data 
     : Ach_OutputIocVlvNc3Data.VlvNc3DataFlg;
     : Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocVlvRelayData(&Ach_OutputBus.Ach_OutputIocVlvRelayData);
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvRelayData 
     : Ach_OutputIocVlvRelayData.VlvRelayDataFlg;
     : Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputIocAdcSelWriteData(&Ach_OutputBus.Ach_OutputIocAdcSelWriteData);
    /*==============================================================================
    * Members of structure Ach_OutputIocAdcSelWriteData 
     : Ach_OutputIocAdcSelWriteData.AdcSelDataFlg;
     : Ach_OutputIocAdcSelWriteData.ACH_Tx13_ADC_SEL;
     =============================================================================*/
    
    Ach_Output_Read_Ach_OutputAcmAsicInitCompleteFlag(&Ach_OutputBus.Ach_OutputAcmAsicInitCompleteFlag);

    /* Process */
    Ach_Process();

    /* Output */

    Ach_Output_Timer_Elapsed = STM0_TIM0.U - Ach_Output_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACH_OUTPUT_STOP_SEC_CODE
#include "Ach_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
