/**
 * @defgroup Ach_Output Ach_Output
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Output.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Output.h"
#include "Ach_Output_Ifa.h"
#include "Ach_Output_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACH_OUTPUT_STOP_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_OUTPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

ACH_TxFrameType  ACH_TxFrame[MAX_BUFFER_SIZE];
ACH_MsgConfigType ACH_MsgConfig[ACH_MSGID_MAX];

uint8 ACH_FrameCount;


/*************************** ASIC TX Message *************************/
ASIC_TX_MSG_EMPTY_DATA_t    ASIC_TX_MSG_EMPTY_DATA; /* for empty data Tx */

ASIC_TX_MSG_1_t     ASIC_TX_MSG_1;
ASIC_TX_MSG_2_t     ASIC_TX_MSG_2;
ASIC_TX_MSG_6_t     ASIC_TX_MSG_6;
ASIC_TX_MSG_8_t     ASIC_TX_MSG_8;
ASIC_TX_MSG_9_t     ASIC_TX_MSG_9;
ASIC_TX_MSG_13_t    ASIC_TX_MSG_13;
ASIC_TX_MSG_15_t    ASIC_TX_MSG_15;
ASIC_TX_MSG_17_t    ASIC_TX_MSG_17;
ASIC_TX_MSG_19_t    ASIC_TX_MSG_19;
ASIC_TX_MSG_32_t    ASIC_TX_MSG_32;
ASIC_TX_MSG_33_t    ASIC_TX_MSG_33;
ASIC_TX_MSG_34_t    ASIC_TX_MSG_34;
ASIC_TX_MSG_35_t    ASIC_TX_MSG_35;
ASIC_TX_MSG_36_t    ASIC_TX_MSG_36;
ASIC_TX_MSG_37_t    ASIC_TX_MSG_37;
ASIC_TX_MSG_38_t    ASIC_TX_MSG_38;
ASIC_TX_MSG_39_t    ASIC_TX_MSG_39;
ASIC_TX_MSG_40_t    ASIC_TX_MSG_40;
ASIC_TX_MSG_41_t    ASIC_TX_MSG_41;
ASIC_TX_MSG_42_t    ASIC_TX_MSG_42;
ASIC_TX_MSG_43_t    ASIC_TX_MSG_43;
ASIC_TX_MSG_44_t    ASIC_TX_MSG_44;
ASIC_TX_MSG_64_t    ASIC_TX_MSG_64;
ASIC_TX_MSG_65_t    ASIC_TX_MSG_65;
ASIC_TX_MSG_64_t    ASIC_TX_MSG_64_2;   /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
ASIC_TX_MSG_67_t    ASIC_TX_MSG_67;
ASIC_TX_MSG_72_t    ASIC_TX_MSG_72;
ASIC_TX_MSG_74_t    ASIC_TX_MSG_74;
ASIC_TX_MSG_75_t    ASIC_TX_MSG_75;
ASIC_TX_MSG_76_t    ASIC_TX_MSG_76;
ASIC_TX_MSG_77_t    ASIC_TX_MSG_77;
ASIC_TX_MSG_78_t    ASIC_TX_MSG_78;

/* NO TC ESV Valve */
/* NO CH0 */
ASIC_TX_MSG_NO_TC_2_t   ASIC_TX_MSG_NO0_2;
ASIC_TX_MSG_NO_TC_5_t   ASIC_TX_MSG_NO0_5;
ASIC_TX_MSG_NO_TC_6_t   ASIC_TX_MSG_NO0_6;
ASIC_TX_MSG_NO_TC_7_t   ASIC_TX_MSG_NO0_7;
ASIC_TX_MSG_NO_TC_8_t   ASIC_TX_MSG_NO0_8;
ASIC_TX_MSG_NO_TC_9_t   ASIC_TX_MSG_NO0_9;
ASIC_TX_MSG_NO_TC_13_t  ASIC_TX_MSG_NO0_13;
/* NO CH1 */
ASIC_TX_MSG_NO_TC_2_t   ASIC_TX_MSG_NO1_2;
ASIC_TX_MSG_NO_TC_5_t   ASIC_TX_MSG_NO1_5;
ASIC_TX_MSG_NO_TC_6_t   ASIC_TX_MSG_NO1_6;
ASIC_TX_MSG_NO_TC_7_t   ASIC_TX_MSG_NO1_7;
ASIC_TX_MSG_NO_TC_8_t   ASIC_TX_MSG_NO1_8;
ASIC_TX_MSG_NO_TC_9_t   ASIC_TX_MSG_NO1_9;
ASIC_TX_MSG_NO_TC_13_t  ASIC_TX_MSG_NO1_13;
/* NO CH2 */
ASIC_TX_MSG_NO_TC_2_t   ASIC_TX_MSG_NO2_2;
ASIC_TX_MSG_NO_TC_5_t   ASIC_TX_MSG_NO2_5;
ASIC_TX_MSG_NO_TC_6_t   ASIC_TX_MSG_NO2_6;
ASIC_TX_MSG_NO_TC_7_t   ASIC_TX_MSG_NO2_7;
ASIC_TX_MSG_NO_TC_8_t   ASIC_TX_MSG_NO2_8;
ASIC_TX_MSG_NO_TC_9_t   ASIC_TX_MSG_NO2_9;
ASIC_TX_MSG_NO_TC_13_t  ASIC_TX_MSG_NO2_13;
/* NO CH3 */
ASIC_TX_MSG_NO_TC_2_t   ASIC_TX_MSG_NO3_2;
ASIC_TX_MSG_NO_TC_5_t   ASIC_TX_MSG_NO3_5;
ASIC_TX_MSG_NO_TC_6_t   ASIC_TX_MSG_NO3_6;
ASIC_TX_MSG_NO_TC_7_t   ASIC_TX_MSG_NO3_7;
ASIC_TX_MSG_NO_TC_8_t   ASIC_TX_MSG_NO3_8;
ASIC_TX_MSG_NO_TC_9_t   ASIC_TX_MSG_NO3_9;
ASIC_TX_MSG_NO_TC_13_t  ASIC_TX_MSG_NO3_13;
/* TC CH0 */
ASIC_TX_MSG_NO_TC_2_t   ASIC_TX_MSG_TC0_2;
ASIC_TX_MSG_NO_TC_5_t   ASIC_TX_MSG_TC0_5;
ASIC_TX_MSG_NO_TC_6_t   ASIC_TX_MSG_TC0_6;
ASIC_TX_MSG_NO_TC_7_t   ASIC_TX_MSG_TC0_7;
ASIC_TX_MSG_NO_TC_8_t   ASIC_TX_MSG_TC0_8;
ASIC_TX_MSG_NO_TC_9_t   ASIC_TX_MSG_TC0_9;
ASIC_TX_MSG_NO_TC_13_t  ASIC_TX_MSG_TC0_13;
/* TC CH1 */
ASIC_TX_MSG_NO_TC_2_t   ASIC_TX_MSG_TC1_2;
ASIC_TX_MSG_NO_TC_5_t   ASIC_TX_MSG_TC1_5;
ASIC_TX_MSG_NO_TC_6_t   ASIC_TX_MSG_TC1_6;
ASIC_TX_MSG_NO_TC_7_t   ASIC_TX_MSG_TC1_7;
ASIC_TX_MSG_NO_TC_8_t   ASIC_TX_MSG_TC1_8;
ASIC_TX_MSG_NO_TC_9_t   ASIC_TX_MSG_TC1_9;
ASIC_TX_MSG_NO_TC_13_t  ASIC_TX_MSG_TC1_13;
/* ESV CH0 */
ASIC_TX_MSG_ESV_2_t     ASIC_TX_MSG_ESV0_2;
ASIC_TX_MSG_ESV_5_t     ASIC_TX_MSG_ESV0_5;
ASIC_TX_MSG_ESV_6_t     ASIC_TX_MSG_ESV0_6;
ASIC_TX_MSG_ESV_8_t     ASIC_TX_MSG_ESV0_8;
ASIC_TX_MSG_ESV_9_t     ASIC_TX_MSG_ESV0_9;
/* ESV CH1 */
ASIC_TX_MSG_ESV_2_t     ASIC_TX_MSG_ESV1_2;
ASIC_TX_MSG_ESV_5_t     ASIC_TX_MSG_ESV1_5;
ASIC_TX_MSG_ESV_6_t     ASIC_TX_MSG_ESV1_6;
ASIC_TX_MSG_ESV_8_t     ASIC_TX_MSG_ESV1_8;
ASIC_TX_MSG_ESV_9_t     ASIC_TX_MSG_ESV1_9;



#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_OUTPUT_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_OUTPUT_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_OUTPUT_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACH_OUTPUT_START_SEC_CODE
#include "Ach_MemMap.h"

Std_ReturnType Ach_SetReadSpiData_CS(ACH_MsgIdType ReadMsgId);
static Std_ReturnType Ach_SetWriteSpiData_SR(void);
Std_ReturnType Ach_SetWriteSpiData_CS(ACH_WriteMsgSetType WriteMsgSet, void* Data);
static Std_ReturnType Ach_AsicInitMsgSetFunc_CS(ACH_AsicInitDataType* Data);
static Std_ReturnType Ach_ValveInitMsgSetFunc_CS(ACH_ValveInitDataType* Data);
static Std_ReturnType Ach_MotorInitMsgSetFunc_CS(ACH_MotorInitDataType* Data);
static Std_ReturnType Ach_VsoInitMsgSetFunc_CS(ACH_VsoInitDataType* Data);
static Std_ReturnType Ach_WssInitMsgSetFunc_CS(ACH_WssInitDataType* Data);
static Std_ReturnType Ach_LampShlsInitMsgSetFunc_CS(ACH_LampShlsInitDataType* Data);
static Std_ReturnType Ach_WatchdogInitMsgSetFunc_CS(ACH_WatchdogInitDataType* Data);
static Std_ReturnType Ach_ValveNo0MsgSetFunc(void);
static Std_ReturnType Ach_ValveNo1MsgSetFunc(void);
static Std_ReturnType Ach_ValveNo2MsgSetFunc(void);
static Std_ReturnType Ach_ValveNo3MsgSetFunc(void);
static Std_ReturnType Ach_ValveTc0MsgSetFunc(void);
static Std_ReturnType Ach_ValveTc1MsgSetFunc(void);
static Std_ReturnType Ach_ValveEsv0MsgSetFunc(void);
static Std_ReturnType Ach_ValveEsv1MsgSetFunc(void);
static Std_ReturnType Ach_ValveNc0MsgSetFunc(void);
static Std_ReturnType Ach_ValveNc1MsgSetFunc(void);
static Std_ReturnType Ach_ValveNc2MsgSetFunc(void);
static Std_ReturnType Ach_ValveNc3MsgSetFunc(void);
static Std_ReturnType Ach_MotorDutyMsgSetFunc(void);
static Std_ReturnType Ach_MotorFreqMsgSetFunc(void);
static Std_ReturnType Ach_ValveRelayMsgSetFunc(void);
static Std_ReturnType Ach_WachdogAnswerSetFunc_CS(ACH_WdAnswerDataType* Data);
static Std_ReturnType Ach_WachdogSeedRequestFunc_CS(void* Data);
static Std_ReturnType Ach_ServiceTestEnableSetFunc(void);
static Std_ReturnType Ach_AdcSelectSetFunc(void);
static Std_ReturnType Ach_WssCountSetFunc(void);
static Std_ReturnType Ach_KeepAliveSetFunc(void);
static Std_ReturnType Ach_VsoDriveSetFunc(void);
static Std_ReturnType Ach_WldDriveSetFunc(void);
static Std_ReturnType Ach_ShlsDriveSetFunc(void);
uint8 Ach_CRC5Gen(uint32 DATA);
#if 0
uint8 Crc_Test(uint32 DATA);
#endif
#ifndef SPI_DMA_ENABLE
static uint32 W_u32TrnsSpi0Single(uint32 data);
#endif

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Ach_Init(void)
{
    uint32 MsgIdIndex = 0;

    for ( MsgIdIndex = 0u; MsgIdIndex < ACH_MSGID_MAX; MsgIdIndex++ )
    {
        ACH_MsgConfig[MsgIdIndex].ADD       = ACH_MsgAddArray[MsgIdIndex];
        ACH_MsgConfig[MsgIdIndex].WRT_REQ   = ACH_REQ_NOT_SET;
        ACH_MsgConfig[MsgIdIndex].RD_REQ    = ACH_REQ_NOT_SET;
        ACH_MsgConfig[MsgIdIndex].STATUS    = ACH_STATUS_IDLE;
        ACH_MsgConfig[MsgIdIndex].DATA      = 0;
    }
}

void Ach_Process(void)
{
    uint32       MsgIdIndex           = 0;
    uint16       msg_num              = 0;
    #ifndef SPI_DMA_ENABLE
    uint8 i = 0;
    #endif

    Ach_SetWriteSpiData_SR();

    for(MsgIdIndex = 0u ; MsgIdIndex < ACH_MSGID_MAX ; MsgIdIndex++)
    {
        if((ACH_MsgConfig[MsgIdIndex].RD_REQ == ACH_REQ_SET)||(ACH_MsgConfig[MsgIdIndex].WRT_REQ == ACH_REQ_SET))
        {
            if(ACH_MsgConfig[MsgIdIndex].WRT_REQ == ACH_REQ_SET)    /* Set Write Spi Data */
            {
                ACH_TxFrame[msg_num].Tx_Field.RW    = ACH_ASIC_RW_WRITE;
                ACH_TxFrame[msg_num].Tx_Field.DATA  = ACH_MsgConfig[MsgIdIndex].DATA;
                ACH_MsgConfig[MsgIdIndex].WRT_REQ   = ACH_REQ_NOT_SET;
            }
            ACH_TxFrame[msg_num].Tx_Field.ADD       = ACH_MsgConfig[MsgIdIndex].ADD;
            ACH_TxFrame[msg_num].Tx_Field.FCNT      = ACH_FrameCount;
            ACH_TxFrame[msg_num].Tx_Field.CRC       = Ach_CRC5Gen(ACH_TxFrame[msg_num].frame);
            ACH_MsgConfig[MsgIdIndex].RD_REQ        = ACH_REQ_NOT_SET;
            ACH_MsgConfig[MsgIdIndex].STATUS        = ACH_STATUS_SENT;
            ACH_FrameCount^=1;
            msg_num++;
        }
    }
    if(msg_num > 0)
    {
        /* Dummy Message */
        {
            ACH_TxFrame[msg_num].frame              = 0u;
            ACH_TxFrame[msg_num].Tx_Field.FCNT      = ACH_FrameCount;
            ACH_TxFrame[msg_num].Tx_Field.CRC       = Ach_CRC5Gen(ACH_TxFrame[msg_num].frame);
            ACH_FrameCount^=1;
            msg_num++;
        }

        #ifdef SPI_DMA_ENABLE /* SPI DMA ENABLE */
        if(msg_num >= 1)
        {
            Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_100Asic, (Spim_Cdd_DataType*)ACH_TxFrame, (Spim_Cdd_DataType*)ACH_RxFrame, msg_num);
            Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_100Asic);
        }
        #else

        for(i = 0; i < msg_num; i++)
        {
            ACH_RxFrame[i].frame = W_u32TrnsSpi0Single(ACH_TxFrame[i].frame);
        }
        ACH_DecodeAsicMsg();
        #endif
    }
}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
 

#ifndef SPI_DMA_ENABLE

static uint32 W_u32TrnsSpi0Single(uint32 data)
{
  /* write enable */
	uint32 ret = SPI_SEQ_OK;

	Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_100Asic,(Spim_Cdd_DataType*)&data,(Spim_Cdd_DataType*)&ret,1);
	Spim_Cdd_SyncTransmit(Spim_Cdd_SpiSequence_100Asic);
	while(Spi_GetSequenceResult(Spim_Cdd_SpiChannel_100Asic) != SPI_SEQ_OK)
	{
		if(Spi_GetSequenceResult(Spim_Cdd_SpiChannel_100Asic) == SPI_SEQ_FAILED)
		{
			ret = -1;
		}
	}

    return ret;
}
#endif

Std_ReturnType Ach_SetReadSpiData_CS(ACH_MsgIdType ReadMsgId)
{
    Std_ReturnType rtn = E_OK;
    
    ACH_MsgConfig[ReadMsgId].RD_REQ = ACH_REQ_SET;

    return rtn;

}

static Std_ReturnType Ach_SetWriteSpiData_SR(void)
{
    Std_ReturnType rtn = E_OK;

    if(Ach_OutputBus.Ach_OutputIocVlvNo0Data.VlvNo0DataFlg == 1)
    {
        Ach_ValveNo0MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNo1Data.VlvNo1DataFlg == 1)
    {
        Ach_ValveNo1MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNo2Data.VlvNo2DataFlg == 1)
    {
        Ach_ValveNo2MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNo3Data.VlvNo3DataFlg == 1)
    {
        Ach_ValveNo3MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvTc0Data.VlvTc0DataFlg == 1)
    {
        Ach_ValveTc0MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvTc1Data.VlvTc1DataFlg == 1)
    {
        Ach_ValveTc1MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg == 1)
    {
        Ach_ValveEsv0MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg == 1)
    {
        Ach_ValveEsv1MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNc0Data.VlvNc0DataFlg == 1)
    {
        Ach_ValveNc0MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNc1Data.VlvNc1DataFlg == 1)
    {
        Ach_ValveNc1MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNc2Data.VlvNc2DataFlg == 1)
    {
        Ach_ValveNc2MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvNc3Data.VlvNc3DataFlg == 1)
    {
        Ach_ValveNc3MsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg == 1)
    {
        Ach_MotorDutyMsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg == 1)
    {
        Ach_MotorFreqMsgSetFunc();
    }
    if(Ach_OutputBus.Ach_OutputIocVlvRelayData.VlvRelayDataFlg == 1)
    {
        Ach_ValveRelayMsgSetFunc();
    }
    #if 0   /* TODO */
    if(Ach_OutputBus.Ach_OutputHalServiceTestEnaWriteData.ServiceTestEnaDataFlg == 1)
    {
        Ach_ServiceTestEnableSetFunc();
    }
    #endif
    if(Ach_OutputBus.Ach_OutputIocAdcSelWriteData.AdcSelDataFlg == 1)
    {
        Ach_AdcSelectSetFunc();
    }
    #if 0   /* TODO */
    if(Ach_OutputBus.Ach_OutputHalWssCntWriteData.WssCntDataFlg == 1)
    {
        Ach_WssCountSetFunc();
    }
    #endif
    if(Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg == 1)
    {
        Ach_KeepAliveSetFunc();
    }
    #if 0   /* TODO */
    if(Ach_OutputBus.Ach_OutputHalVsoDrvWriteData.VsoDrvDataFlg == 1)
    {
        Ach_VsoDriveSetFunc();
    }
    #endif
    #if 0   /* TODO */
    if(Ach_OutputBus.Ach_OutputHalWldDrvWriteData.WldDrvDataFlg == 1)
    {
        Ach_WldDriveSetFunc();
    }
    #endif
    #if 0   /* TODO */
    if(Ach_OutputBus.Ach_OutputHalShlsDrvWriteData.ShlsDrvDataFlg == 1)
    {
        Ach_ShlsDriveSetFunc();
    }
    #endif

    return rtn;

}

Std_ReturnType Ach_SetWriteSpiData_CS(ACH_WriteMsgSetType WriteMsgSet, void* Data)
{
    Std_ReturnType rtn = E_OK;

    switch(WriteMsgSet)
    {
        case ACH_AsicInitMsgSet:
        {
            Ach_AsicInitMsgSetFunc_CS((ACH_AsicInitDataType*)Data);
            break;
        }
        case ACH_ValveInitMsgSet:
        {
            Ach_ValveInitMsgSetFunc_CS((ACH_ValveInitDataType*)Data);

            break;
        }
        case ACH_MotorInitMsgSet:
        {
            Ach_MotorInitMsgSetFunc_CS((ACH_MotorInitDataType*)Data);
            break;
        }
        case ACH_VsoInitMsgSet:
        {
            Ach_VsoInitMsgSetFunc_CS((ACH_VsoInitDataType*)Data);
            break;
        }
        case ACH_WssInitMsgSet:
        {
            Ach_WssInitMsgSetFunc_CS((ACH_WssInitDataType*)Data);
            break;
        }
        case ACH_LampShlsInitMsgSet:
        {
            Ach_LampShlsInitMsgSetFunc_CS((ACH_LampShlsInitDataType*)Data);
            break;
        }
        case ACH_WatchdogInitMsgSet:
        {
            Ach_WatchdogInitMsgSetFunc_CS((ACH_WatchdogInitDataType*)Data);
            break;
        }
        case ACH_WatchdogAnswerSet:
        {
            Ach_WachdogAnswerSetFunc_CS((ACH_WdAnswerDataType*)Data);
            break;
        }
        case ACH_WatchdogSeedRequest:
        {
            Ach_WachdogSeedRequestFunc_CS(Data);
            break;
        }        
        default:
        {
            break;
        }
        
    }
    return rtn;

}

static Std_ReturnType Ach_AsicInitMsgSetFunc_CS(ACH_AsicInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;    
    ACH_AsicInitDataType* AsicInitDataTemp;
    AsicInitDataTemp = Data;

    ASIC_TX_MSG_1.R.B.mu2_Tx_1_FS_VDS_TH                      = AsicInitDataTemp->ACH_Tx1_FS_VDS_TH;
    ASIC_TX_MSG_1.R.B.mu1_Tx_1_FS_CMD                         = AsicInitDataTemp->ACH_Tx1_FS_CMD;
    ASIC_TX_MSG_1.R.B.mu1_Tx_1_FS_EN                          = AsicInitDataTemp->ACH_Tx1_FS_EN;
    ASIC_TX_MSG_1.R.B.mu1_Tx_1_PMP_EN                         = AsicInitDataTemp->ACH_Tx1_PMP_EN;
    ASIC_TX_MSG_1.R.B.mu1_Tx_1_OSC_SprSpect_DIS               = AsicInitDataTemp->ACH_Tx1_OSC_SprSpect_DIS;
    ASIC_TX_MSG_1.R.B.mu2_Tx_1_VDD_4_OVC_SD_RESTART_TIME      = AsicInitDataTemp->ACH_Tx1_VDD_4_OVC_SD_RESTART_TIME;
    ASIC_TX_MSG_1.R.B.mu2_Tx_1_VDD4_OVC_FLT_TIME              = AsicInitDataTemp->ACH_Tx1_VDD4_OVC_FLT_TIME;
    ASIC_TX_MSG_1.R.B.mu2_Tx_1_VDD_3_OVC_SD_RESTART_TIME      = AsicInitDataTemp->ACH_Tx1_VDD_3_OVC_SD_RESTART_TIME;
    ASIC_TX_MSG_1.R.B.mu2_Tx_1_VDD3_OVC_FLT_TIME              = AsicInitDataTemp->ACH_Tx1_VDD3_OVC_FLT_TIME;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].DATA                    = ASIC_TX_MSG_1.R.DATA;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].WRT_REQ                 = ACH_REQ_SET;

    ASIC_TX_MSG_2.R.B.mu2_Tx_2_WD_SW_DIS                      = AsicInitDataTemp->ACH_Tx_2_WD_SW_DIS;
    ASIC_TX_MSG_2.R.B.mu1_Tx_2_VDD_2d5_AUTO_SWITCH_OFF        = AsicInitDataTemp->ACH_Tx2_VDD_2d5_AUTO_SWITCH_OFF;
    ASIC_TX_MSG_2.R.B.mu1_Tx_2_VDD1_DIODELOSS_FILT            = AsicInitDataTemp->ACH_Tx2_VDD1_DIODELOSS_FILT;
    ASIC_TX_MSG_2.R.B.mu1_Tx_2_WD_DIS                         = AsicInitDataTemp->ACH_Tx_2_WD_DIS;
    ASIC_TX_MSG_2.R.B.mu1_Tx_2_VDD5_DIS                       = AsicInitDataTemp->ACH_Tx2_VDD5_DIS;
    ASIC_TX_MSG_2.R.B.mu1_Tx_2_VDD2_DIS                       = AsicInitDataTemp->ACH_Tx2_VDD2_DIS;
    ACH_MsgConfig[ACH_MSG2_ID_GENCFG2].DATA                   = ASIC_TX_MSG_2.R.DATA;
    ACH_MsgConfig[ACH_MSG2_ID_GENCFG2].WRT_REQ                = ACH_REQ_SET;

    ASIC_TX_MSG_19.R.B.mu1_Tx_19_E_PVDS_GPO                   = AsicInitDataTemp->ACH_Tx19_E_PVDS_GPO;
    ASIC_TX_MSG_19.R.B.mu1_Tx_19_E_VDD2d5dWSx_OT_GPO          = AsicInitDataTemp->ACH_Tx19_E_VDD2d5dWSx_OT_GPO;
    ASIC_TX_MSG_19.R.B.mu1_Tx_19_E_FBO_GPO                    = AsicInitDataTemp->ACH_Tx19_E_FBO_GPO;
    ASIC_TX_MSG_19.R.B.mu1_Tx_19_E_FSPI_GPO                   = AsicInitDataTemp->ACH_Tx19_E_FSPI_GPO;
    ASIC_TX_MSG_19.R.B.mu1_Tx_19_E_EN_GPO                     = AsicInitDataTemp->ACH_Tx19_E_EN_GPO;
    ASIC_TX_MSG_19.R.B.mu1_Tx_19_E_FS_VDS_GPO                 = AsicInitDataTemp->ACH_Tx19_E_FS_VDS_GPO;
    ACH_MsgConfig[ACH_MSG19_ID_SERVFLTMSK].DATA               = ASIC_TX_MSG_19.R.DATA;
    ACH_MsgConfig[ACH_MSG19_ID_SERVFLTMSK].WRT_REQ            = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveInitMsgSetFunc_CS(ACH_ValveInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_ValveInitDataType* ValveInitDataTemp;
    ValveInitDataTemp = Data;

    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_0            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_0;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_1            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_1;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_2            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_2;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_3            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_3;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_4            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_4;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_5            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_5;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_6            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_6;
    ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_7            = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_7;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA               = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ            = ACH_REQ_SET;

    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH0_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH0_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH0_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH0_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH1_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH1_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH1_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH1_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH2_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH2_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH2_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH2_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH3_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH3_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH3_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH3_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH4_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH4_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH4_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH4_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH5_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH5_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH5_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH5_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH6_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH6_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH6_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH6_STATUS_H;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH7_STATUS_L                 = ValveInitDataTemp->ACH_Tx65_CH7_STATUS_L;
    ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH7_STATUS_H                 = ValveInitDataTemp->ACH_Tx65_CH7_STATUS_H;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                  = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ               = ACH_REQ_SET;
    
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_0          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_0;  /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_1          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_1;
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_2          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_2;
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_3          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_3;
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_4          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_4;
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_5          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_5;
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_6          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_6;
    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_7          = ValveInitDataTemp->ACH_Tx64_SOLENOID_ENABLE_7;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA             = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ          = ACH_REQ_SET;

    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH1_EN                       = ValveInitDataTemp->ACH_Tx67_CH1_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH2_EN                       = ValveInitDataTemp->ACH_Tx67_CH2_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH3_EN                       = ValveInitDataTemp->ACH_Tx67_CH3_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH4_EN                       = ValveInitDataTemp->ACH_Tx67_CH4_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH1_DIAG_EN                  = ValveInitDataTemp->ACH_Tx67_CH1_DIAG_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH2_DIAG_EN                  = ValveInitDataTemp->ACH_Tx67_CH2_DIAG_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH3_DIAG_EN                  = ValveInitDataTemp->ACH_Tx67_CH3_DIAG_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH4_DIAG_EN                  = ValveInitDataTemp->ACH_Tx67_CH4_DIAG_EN;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH1_TD_BLANK                 = ValveInitDataTemp->ACH_Tx67_CH1_TD_BLANK;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH2_TD_BLANK                 = ValveInitDataTemp->ACH_Tx67_CH2_TD_BLANK;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH3_TD_BLANK                 = ValveInitDataTemp->ACH_Tx67_CH3_TD_BLANK;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH4_TD_BLANK                 = ValveInitDataTemp->ACH_Tx67_CH4_TD_BLANK;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH1_CMD                      = ValveInitDataTemp->ACH_Tx67_CH1_CMD;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH2_CMD                      = ValveInitDataTemp->ACH_Tx67_CH2_CMD;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH3_CMD                      = ValveInitDataTemp->ACH_Tx67_CH3_CMD;
    ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH4_CMD                      = ValveInitDataTemp->ACH_Tx67_CH4_CMD;
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].DATA          = ASIC_TX_MSG_67.R.DATA;
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_NO0_2.R.B.mu2_Tx_NT2_SR_SEL                   = ValveInitDataTemp->ACH_TxNO0_2_SR_SEL;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_CALIBRATION_DIS          = ValveInitDataTemp->ACH_TxNO0_2_CALIBRATION_DIS;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_OFS_CHOP_DIS             = ValveInitDataTemp->ACH_TxNO0_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_TD_BLANK_SEL             = ValveInitDataTemp->ACH_TxNO0_2_TD_BLANK_SEL;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_LS_CLAMP_DIS             = ValveInitDataTemp->ACH_TxNO0_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_LOGIC_BIST_EN            = ValveInitDataTemp->ACH_TxNO0_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_DIAG_BIST_EN             = ValveInitDataTemp->ACH_TxNO0_2_DIAG_BIST_EN;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_ADC_BIST_EN              = ValveInitDataTemp->ACH_TxNO0_2_ADC_BIST_EN;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_OFF_DIAG_EN              = ValveInitDataTemp->ACH_TxNO0_2_OFF_DIAG_EN;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_E_SOL_GPO                = ValveInitDataTemp->ACH_TxNO0_2_E_SOL_GPO;
    ASIC_TX_MSG_NO0_2.R.B.mu1_Tx_NT2_E_LSCLAMP_GPO            = ValveInitDataTemp->ACH_TxNO0_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG130_NO0_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_NO0_2.R.DATA;
    ACH_MsgConfig[ACH_MSG130_NO0_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_NO0_6.R.B.mu10_Tx_NT6_PWM_CODE                = ValveInitDataTemp->ACH_TxNO0_6_PWM_CODE;
    ACH_MsgConfig[ACH_MSG134_NO0_6_CTRLCFG_CHX].DATA          = ASIC_TX_MSG_NO0_6.R.DATA;
    ACH_MsgConfig[ACH_MSG134_NO0_6_CTRLCFG_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_NO0_7.R.B.mu7_Tx_NT7_FREQ_MOD_STEP            = ValveInitDataTemp->ACH_TxNO0_7_FREQ_MOD_STEP;
    ASIC_TX_MSG_NO0_7.R.B.mu8_Tx_NT7_MAX_FREQ_DELTA           = ValveInitDataTemp->ACH_TxNO0_7_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG135_NO0_7_FMODULATION_CHX].DATA      = ASIC_TX_MSG_NO0_7.R.DATA;
    ACH_MsgConfig[ACH_MSG135_NO0_7_FMODULATION_CHX].WRT_REQ   = ACH_REQ_SET;

    ASIC_TX_MSG_NO0_8.R.B.mu3_Tx_NT8_KI                       = ValveInitDataTemp->ACH_TxNO0_8_KI;
    ASIC_TX_MSG_NO0_8.R.B.mu3_Tx_NT8_KP                       = ValveInitDataTemp->ACH_TxNO0_8_KP;
    ACH_MsgConfig[ACH_MSG136_NO0_8_KGAINS_CHX].DATA           = ASIC_TX_MSG_NO0_8.R.DATA;
    ACH_MsgConfig[ACH_MSG136_NO0_8_KGAINS_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO0_9.R.B.mu2_Tx_NT9_INTTIME                  = ValveInitDataTemp->ACH_TxNO0_9_INTTIME;
    ASIC_TX_MSG_NO0_9.R.B.mu2_Tx_NT9_FILT_TIME                = ValveInitDataTemp->ACH_TxNO0_9_FILT_TIME;
    ASIC_TX_MSG_NO0_9.R.B.mu1_Tx_NT9_CHOP_TIME                = ValveInitDataTemp->ACH_TxNO0_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG137_NO0_9_OFFCMP_CHX].DATA           = ASIC_TX_MSG_NO0_9.R.DATA;
    ACH_MsgConfig[ACH_MSG137_NO0_9_OFFCMP_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO0_13.R.B.mu8_Tx_NT13_BASE_DELTA_CURENT      = ValveInitDataTemp->ACH_TxNO0_13_BASE_DELTA_CURENT;
    ACH_MsgConfig[ACH_MSG141_NO0_13_BASE_DELTA_CURR].DATA     = ASIC_TX_MSG_NO0_13.R.DATA;
    ACH_MsgConfig[ACH_MSG141_NO0_13_BASE_DELTA_CURR].WRT_REQ  = ACH_REQ_SET;


    ASIC_TX_MSG_NO1_2.R.B.mu2_Tx_NT2_SR_SEL                   = ValveInitDataTemp->ACH_TxNO1_2_SR_SEL;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_CALIBRATION_DIS          = ValveInitDataTemp->ACH_TxNO1_2_CALIBRATION_DIS;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_OFS_CHOP_DIS             = ValveInitDataTemp->ACH_TxNO1_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_TD_BLANK_SEL             = ValveInitDataTemp->ACH_TxNO1_2_TD_BLANK_SEL;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_LS_CLAMP_DIS             = ValveInitDataTemp->ACH_TxNO1_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_LOGIC_BIST_EN            = ValveInitDataTemp->ACH_TxNO1_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_DIAG_BIST_EN             = ValveInitDataTemp->ACH_TxNO1_2_DIAG_BIST_EN;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_ADC_BIST_EN              = ValveInitDataTemp->ACH_TxNO1_2_ADC_BIST_EN;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_OFF_DIAG_EN              = ValveInitDataTemp->ACH_TxNO1_2_OFF_DIAG_EN;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_E_SOL_GPO                = ValveInitDataTemp->ACH_TxNO1_2_E_SOL_GPO;
    ASIC_TX_MSG_NO1_2.R.B.mu1_Tx_NT2_E_LSCLAMP_GPO            = ValveInitDataTemp->ACH_TxNO1_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG146_NO1_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_NO1_2.R.DATA;
    ACH_MsgConfig[ACH_MSG146_NO1_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_NO1_6.R.B.mu10_Tx_NT6_PWM_CODE                = ValveInitDataTemp->ACH_TxNO1_6_PWM_CODE;
    ACH_MsgConfig[ACH_MSG150_NO1_6_CTRLCFG_CHX].DATA          = ASIC_TX_MSG_NO1_6.R.DATA;
    ACH_MsgConfig[ACH_MSG150_NO1_6_CTRLCFG_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_NO1_7.R.B.mu7_Tx_NT7_FREQ_MOD_STEP            = ValveInitDataTemp->ACH_TxNO1_7_FREQ_MOD_STEP;
    ASIC_TX_MSG_NO1_7.R.B.mu8_Tx_NT7_MAX_FREQ_DELTA           = ValveInitDataTemp->ACH_TxNO1_7_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG151_NO1_7_FMODULATION_CHX].DATA      = ASIC_TX_MSG_NO1_7.R.DATA;
    ACH_MsgConfig[ACH_MSG151_NO1_7_FMODULATION_CHX].WRT_REQ   = ACH_REQ_SET;

    ASIC_TX_MSG_NO1_8.R.B.mu3_Tx_NT8_KI                       = ValveInitDataTemp->ACH_TxNO1_8_KI;
    ASIC_TX_MSG_NO1_8.R.B.mu3_Tx_NT8_KP                       = ValveInitDataTemp->ACH_TxNO1_8_KP;
    ACH_MsgConfig[ACH_MSG152_NO1_8_KGAINS_CHX].DATA           = ASIC_TX_MSG_NO1_8.R.DATA;
    ACH_MsgConfig[ACH_MSG152_NO1_8_KGAINS_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO1_9.R.B.mu2_Tx_NT9_INTTIME                  = ValveInitDataTemp->ACH_TxNO1_9_INTTIME;
    ASIC_TX_MSG_NO1_9.R.B.mu2_Tx_NT9_FILT_TIME                = ValveInitDataTemp->ACH_TxNO1_9_FILT_TIME;
    ASIC_TX_MSG_NO1_9.R.B.mu1_Tx_NT9_CHOP_TIME                = ValveInitDataTemp->ACH_TxNO1_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG153_NO1_9_OFFCMP_CHX].DATA           = ASIC_TX_MSG_NO1_9.R.DATA;
    ACH_MsgConfig[ACH_MSG153_NO1_9_OFFCMP_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO1_13.R.B.mu8_Tx_NT13_BASE_DELTA_CURENT      = ValveInitDataTemp->ACH_TxNO1_13_BASE_DELTA_CURENT;
    ACH_MsgConfig[ACH_MSG157_NO1_13_BASE_DELTA_CURR].DATA     = ASIC_TX_MSG_NO1_13.R.DATA;
    ACH_MsgConfig[ACH_MSG157_NO1_13_BASE_DELTA_CURR].WRT_REQ  = ACH_REQ_SET;

    ASIC_TX_MSG_NO2_2.R.B.mu2_Tx_NT2_SR_SEL                   = ValveInitDataTemp->ACH_TxNO2_2_SR_SEL;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_CALIBRATION_DIS          = ValveInitDataTemp->ACH_TxNO2_2_CALIBRATION_DIS;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_OFS_CHOP_DIS             = ValveInitDataTemp->ACH_TxNO2_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_TD_BLANK_SEL             = ValveInitDataTemp->ACH_TxNO2_2_TD_BLANK_SEL;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_LS_CLAMP_DIS             = ValveInitDataTemp->ACH_TxNO2_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_LOGIC_BIST_EN            = ValveInitDataTemp->ACH_TxNO2_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_DIAG_BIST_EN             = ValveInitDataTemp->ACH_TxNO2_2_DIAG_BIST_EN;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_ADC_BIST_EN              = ValveInitDataTemp->ACH_TxNO2_2_ADC_BIST_EN;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_OFF_DIAG_EN              = ValveInitDataTemp->ACH_TxNO2_2_OFF_DIAG_EN;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_E_SOL_GPO                = ValveInitDataTemp->ACH_TxNO2_2_E_SOL_GPO;
    ASIC_TX_MSG_NO2_2.R.B.mu1_Tx_NT2_E_LSCLAMP_GPO            = ValveInitDataTemp->ACH_TxNO2_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG162_NO2_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_NO2_2.R.DATA;
    ACH_MsgConfig[ACH_MSG162_NO2_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_NO2_6.R.B.mu10_Tx_NT6_PWM_CODE                = ValveInitDataTemp->ACH_TxNO2_6_PWM_CODE;
    ACH_MsgConfig[ACH_MSG166_NO2_6_CTRLCFG_CHX].DATA          = ASIC_TX_MSG_NO2_6.R.DATA;
    ACH_MsgConfig[ACH_MSG166_NO2_6_CTRLCFG_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_NO2_7.R.B.mu7_Tx_NT7_FREQ_MOD_STEP            = ValveInitDataTemp->ACH_TxNO2_7_FREQ_MOD_STEP;
    ASIC_TX_MSG_NO2_7.R.B.mu8_Tx_NT7_MAX_FREQ_DELTA           = ValveInitDataTemp->ACH_TxNO2_7_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG167_NO2_7_FMODULATION_CHX].DATA      = ASIC_TX_MSG_NO2_7.R.DATA;
    ACH_MsgConfig[ACH_MSG167_NO2_7_FMODULATION_CHX].WRT_REQ   = ACH_REQ_SET;

    ASIC_TX_MSG_NO2_8.R.B.mu3_Tx_NT8_KI                       = ValveInitDataTemp->ACH_TxNO2_8_KI;
    ASIC_TX_MSG_NO2_8.R.B.mu3_Tx_NT8_KP                       = ValveInitDataTemp->ACH_TxNO2_8_KP;
    ACH_MsgConfig[ACH_MSG168_NO2_8_KGAINS_CHX].DATA           = ASIC_TX_MSG_NO2_8.R.DATA;
    ACH_MsgConfig[ACH_MSG168_NO2_8_KGAINS_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO2_9.R.B.mu2_Tx_NT9_INTTIME                  = ValveInitDataTemp->ACH_TxNO2_9_INTTIME;
    ASIC_TX_MSG_NO2_9.R.B.mu2_Tx_NT9_FILT_TIME                = ValveInitDataTemp->ACH_TxNO2_9_FILT_TIME;
    ASIC_TX_MSG_NO2_9.R.B.mu1_Tx_NT9_CHOP_TIME                = ValveInitDataTemp->ACH_TxNO2_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG169_NO2_9_OFFCMP_CHX].DATA           = ASIC_TX_MSG_NO2_9.R.DATA;
    ACH_MsgConfig[ACH_MSG169_NO2_9_OFFCMP_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO2_13.R.B.mu8_Tx_NT13_BASE_DELTA_CURENT      = ValveInitDataTemp->ACH_TxNO2_13_BASE_DELTA_CURENT;
    ACH_MsgConfig[ACH_MSG173_NO2_13_BASE_DELTA_CURR].DATA     = ASIC_TX_MSG_NO2_13.R.DATA;
    ACH_MsgConfig[ACH_MSG173_NO2_13_BASE_DELTA_CURR].WRT_REQ  = ACH_REQ_SET;

    ASIC_TX_MSG_NO3_2.R.B.mu2_Tx_NT2_SR_SEL                   = ValveInitDataTemp->ACH_TxNO3_2_SR_SEL;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_CALIBRATION_DIS          = ValveInitDataTemp->ACH_TxNO3_2_CALIBRATION_DIS;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_OFS_CHOP_DIS             = ValveInitDataTemp->ACH_TxNO3_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_TD_BLANK_SEL             = ValveInitDataTemp->ACH_TxNO3_2_TD_BLANK_SEL;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_LS_CLAMP_DIS             = ValveInitDataTemp->ACH_TxNO3_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_LOGIC_BIST_EN            = ValveInitDataTemp->ACH_TxNO3_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_DIAG_BIST_EN             = ValveInitDataTemp->ACH_TxNO3_2_DIAG_BIST_EN;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_ADC_BIST_EN              = ValveInitDataTemp->ACH_TxNO3_2_ADC_BIST_EN;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_OFF_DIAG_EN              = ValveInitDataTemp->ACH_TxNO3_2_OFF_DIAG_EN;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_E_SOL_GPO                = ValveInitDataTemp->ACH_TxNO3_2_E_SOL_GPO;
    ASIC_TX_MSG_NO3_2.R.B.mu1_Tx_NT2_E_LSCLAMP_GPO            = ValveInitDataTemp->ACH_TxNO3_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG178_NO3_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_NO3_2.R.DATA;
    ACH_MsgConfig[ACH_MSG178_NO3_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_NO3_6.R.B.mu10_Tx_NT6_PWM_CODE                = ValveInitDataTemp->ACH_TxNO3_6_PWM_CODE;
    ACH_MsgConfig[ACH_MSG182_NO3_6_CTRLCFG_CHX].DATA          = ASIC_TX_MSG_NO3_6.R.DATA;
    ACH_MsgConfig[ACH_MSG182_NO3_6_CTRLCFG_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_NO3_7.R.B.mu7_Tx_NT7_FREQ_MOD_STEP            = ValveInitDataTemp->ACH_TxNO3_7_FREQ_MOD_STEP;
    ASIC_TX_MSG_NO3_7.R.B.mu8_Tx_NT7_MAX_FREQ_DELTA           = ValveInitDataTemp->ACH_TxNO3_7_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG183_NO3_7_FMODULATION_CHX].DATA      = ASIC_TX_MSG_NO3_7.R.DATA;
    ACH_MsgConfig[ACH_MSG183_NO3_7_FMODULATION_CHX].WRT_REQ   = ACH_REQ_SET;

    ASIC_TX_MSG_NO3_8.R.B.mu3_Tx_NT8_KI                       = ValveInitDataTemp->ACH_TxNO3_8_KI;
    ASIC_TX_MSG_NO3_8.R.B.mu3_Tx_NT8_KP                       = ValveInitDataTemp->ACH_TxNO3_8_KP;
    ACH_MsgConfig[ACH_MSG184_NO3_8_KGAINS_CHX].DATA           = ASIC_TX_MSG_NO3_8.R.DATA;
    ACH_MsgConfig[ACH_MSG184_NO3_8_KGAINS_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO3_9.R.B.mu2_Tx_NT9_INTTIME                  = ValveInitDataTemp->ACH_TxNO3_9_INTTIME;
    ASIC_TX_MSG_NO3_9.R.B.mu2_Tx_NT9_FILT_TIME                = ValveInitDataTemp->ACH_TxNO3_9_FILT_TIME;
    ASIC_TX_MSG_NO3_9.R.B.mu1_Tx_NT9_CHOP_TIME                = ValveInitDataTemp->ACH_TxNO3_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG185_NO3_9_OFFCMP_CHX].DATA           = ASIC_TX_MSG_NO3_9.R.DATA;
    ACH_MsgConfig[ACH_MSG185_NO3_9_OFFCMP_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_NO3_13.R.B.mu8_Tx_NT13_BASE_DELTA_CURENT      = ValveInitDataTemp->ACH_TxNO3_13_BASE_DELTA_CURENT;
    ACH_MsgConfig[ACH_MSG189_NO3_13_BASE_DELTA_CURR].DATA     = ASIC_TX_MSG_NO3_13.R.DATA;
    ACH_MsgConfig[ACH_MSG189_NO3_13_BASE_DELTA_CURR].WRT_REQ  = ACH_REQ_SET;

    ASIC_TX_MSG_TC0_2.R.B.mu2_Tx_NT2_SR_SEL                   = ValveInitDataTemp->ACH_TxTC0_2_SR_SEL;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_CALIBRATION_DIS          = ValveInitDataTemp->ACH_TxTC0_2_CALIBRATION_DIS;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_OFS_CHOP_DIS             = ValveInitDataTemp->ACH_TxTC0_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_TD_BLANK_SEL             = ValveInitDataTemp->ACH_TxTC0_2_TD_BLANK_SEL;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_LS_CLAMP_DIS             = ValveInitDataTemp->ACH_TxTC0_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_LOGIC_BIST_EN            = ValveInitDataTemp->ACH_TxTC0_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_DIAG_BIST_EN             = ValveInitDataTemp->ACH_TxTC0_2_DIAG_BIST_EN;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_ADC_BIST_EN              = ValveInitDataTemp->ACH_TxTC0_2_ADC_BIST_EN;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_OFF_DIAG_EN              = ValveInitDataTemp->ACH_TxTC0_2_OFF_DIAG_EN;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_E_SOL_GPO                = ValveInitDataTemp->ACH_TxTC0_2_E_SOL_GPO;
    ASIC_TX_MSG_TC0_2.R.B.mu1_Tx_NT2_E_LSCLAMP_GPO            = ValveInitDataTemp->ACH_TxTC0_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG194_TC0_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_TC0_2.R.DATA;
    ACH_MsgConfig[ACH_MSG194_TC0_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_TC0_6.R.B.mu10_Tx_NT6_PWM_CODE                = ValveInitDataTemp->ACH_TxTC0_6_PWM_CODE;
    ACH_MsgConfig[ACH_MSG198_TC0_6_CTRLCFG_CHX].DATA          = ASIC_TX_MSG_TC0_6.R.DATA;
    ACH_MsgConfig[ACH_MSG198_TC0_6_CTRLCFG_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_TC0_7.R.B.mu7_Tx_NT7_FREQ_MOD_STEP            = ValveInitDataTemp->ACH_TxTC0_7_FREQ_MOD_STEP;
    ASIC_TX_MSG_TC0_7.R.B.mu8_Tx_NT7_MAX_FREQ_DELTA           = ValveInitDataTemp->ACH_TxTC0_7_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG199_TC0_7_FMODULATION_CHX].DATA      = ASIC_TX_MSG_TC0_7.R.DATA;
    ACH_MsgConfig[ACH_MSG199_TC0_7_FMODULATION_CHX].WRT_REQ   = ACH_REQ_SET;

    ASIC_TX_MSG_TC0_8.R.B.mu3_Tx_NT8_KI                       = ValveInitDataTemp->ACH_TxTC0_8_KI;
    ASIC_TX_MSG_TC0_8.R.B.mu3_Tx_NT8_KP                       = ValveInitDataTemp->ACH_TxTC0_8_KP;
    ACH_MsgConfig[ACH_MSG200_TC0_8_KGAINS_CHX].DATA           = ASIC_TX_MSG_TC0_8.R.DATA;
    ACH_MsgConfig[ACH_MSG200_TC0_8_KGAINS_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_TC0_9.R.B.mu2_Tx_NT9_INTTIME                  = ValveInitDataTemp->ACH_TxTC0_9_INTTIME;
    ASIC_TX_MSG_TC0_9.R.B.mu2_Tx_NT9_FILT_TIME                = ValveInitDataTemp->ACH_TxTC0_9_FILT_TIME;
    ASIC_TX_MSG_TC0_9.R.B.mu1_Tx_NT9_CHOP_TIME                = ValveInitDataTemp->ACH_TxTC0_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG201_TC0_9_OFFCMP_CHX].DATA           = ASIC_TX_MSG_TC0_9.R.DATA;
    ACH_MsgConfig[ACH_MSG201_TC0_9_OFFCMP_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_TC0_13.R.B.mu8_Tx_NT13_BASE_DELTA_CURENT      = ValveInitDataTemp->ACH_TxTC0_13_BASE_DELTA_CURENT;
    ACH_MsgConfig[ACH_MSG205_TC0_13_BASE_DELTA_CURR].DATA     = ASIC_TX_MSG_TC0_13.R.DATA;
    ACH_MsgConfig[ACH_MSG205_TC0_13_BASE_DELTA_CURR].WRT_REQ  = ACH_REQ_SET;

    ASIC_TX_MSG_TC1_2.R.B.mu2_Tx_NT2_SR_SEL                   = ValveInitDataTemp->ACH_TxTC1_2_SR_SEL;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_CALIBRATION_DIS          = ValveInitDataTemp->ACH_TxTC1_2_CALIBRATION_DIS;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_OFS_CHOP_DIS             = ValveInitDataTemp->ACH_TxTC1_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_TD_BLANK_SEL             = ValveInitDataTemp->ACH_TxTC1_2_TD_BLANK_SEL;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_LS_CLAMP_DIS             = ValveInitDataTemp->ACH_TxTC1_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_LOGIC_BIST_EN            = ValveInitDataTemp->ACH_TxTC1_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_DIAG_BIST_EN             = ValveInitDataTemp->ACH_TxTC1_2_DIAG_BIST_EN;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_ADC_BIST_EN              = ValveInitDataTemp->ACH_TxTC1_2_ADC_BIST_EN;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_OFF_DIAG_EN              = ValveInitDataTemp->ACH_TxTC1_2_OFF_DIAG_EN;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_E_SOL_GPO                = ValveInitDataTemp->ACH_TxTC1_2_E_SOL_GPO;
    ASIC_TX_MSG_TC1_2.R.B.mu1_Tx_NT2_E_LSCLAMP_GPO            = ValveInitDataTemp->ACH_TxTC1_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG210_TC1_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_TC1_2.R.DATA;
    ACH_MsgConfig[ACH_MSG210_TC1_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_TC1_6.R.B.mu10_Tx_NT6_PWM_CODE                = ValveInitDataTemp->ACH_TxTC1_6_PWM_CODE;
    ACH_MsgConfig[ACH_MSG214_TC1_6_CTRLCFG_CHX].DATA          = ASIC_TX_MSG_TC1_6.R.DATA;
    ACH_MsgConfig[ACH_MSG214_TC1_6_CTRLCFG_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_TC1_7.R.B.mu7_Tx_NT7_FREQ_MOD_STEP            = ValveInitDataTemp->ACH_TxTC1_7_FREQ_MOD_STEP;
    ASIC_TX_MSG_TC1_7.R.B.mu8_Tx_NT7_MAX_FREQ_DELTA           = ValveInitDataTemp->ACH_TxTC1_7_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG215_TC1_7_FMODULATION_CHX].DATA      = ASIC_TX_MSG_TC1_7.R.DATA;
    ACH_MsgConfig[ACH_MSG215_TC1_7_FMODULATION_CHX].WRT_REQ   = ACH_REQ_SET;

    ASIC_TX_MSG_TC1_8.R.B.mu3_Tx_NT8_KI                       = ValveInitDataTemp->ACH_TxTC1_8_KI;
    ASIC_TX_MSG_TC1_8.R.B.mu3_Tx_NT8_KP                       = ValveInitDataTemp->ACH_TxTC1_8_KP;
    ACH_MsgConfig[ACH_MSG216_TC1_8_KGAINS_CHX].DATA           = ASIC_TX_MSG_TC1_8.R.DATA;
    ACH_MsgConfig[ACH_MSG216_TC1_8_KGAINS_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_TC1_9.R.B.mu2_Tx_NT9_INTTIME                  = ValveInitDataTemp->ACH_TxTC1_9_INTTIME;
    ASIC_TX_MSG_TC1_9.R.B.mu2_Tx_NT9_FILT_TIME                = ValveInitDataTemp->ACH_TxTC1_9_FILT_TIME;
    ASIC_TX_MSG_TC1_9.R.B.mu1_Tx_NT9_CHOP_TIME                = ValveInitDataTemp->ACH_TxTC1_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG217_TC1_9_OFFCMP_CHX].DATA           = ASIC_TX_MSG_TC1_9.R.DATA;
    ACH_MsgConfig[ACH_MSG217_TC1_9_OFFCMP_CHX].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_TC1_13.R.B.mu8_Tx_NT13_BASE_DELTA_CURENT      = ValveInitDataTemp->ACH_TxTC1_13_BASE_DELTA_CURENT;
    ACH_MsgConfig[ACH_MSG221_TC1_13_BASE_DELTA_CURR].DATA     = ASIC_TX_MSG_TC1_13.R.DATA;
    ACH_MsgConfig[ACH_MSG221_TC1_13_BASE_DELTA_CURR].WRT_REQ  = ACH_REQ_SET;

    ASIC_TX_MSG_ESV0_2.R.B.mu2_Tx_ESV2_SR_SEL                 = ValveInitDataTemp->ACH_TxESV0_2_SR_SEL;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_CALIBRATION_DIS        = ValveInitDataTemp->ACH_TxESV0_2_CALIBRATION_DIS;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_OFS_CHOP_DIS           = ValveInitDataTemp->ACH_TxESV0_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_TD_BLANK_SEL           = ValveInitDataTemp->ACH_TxESV0_2_TD_BLANK_SEL;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_LS_CLAMP_DIS           = ValveInitDataTemp->ACH_TxESV0_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_LOGIC_BIST_EN          = ValveInitDataTemp->ACH_TxESV0_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_DIAG_BIST_EN           = ValveInitDataTemp->ACH_TxESV0_2_DIAG_BIST_EN;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_ADC_BIST_EN            = ValveInitDataTemp->ACH_TxESV0_2_ADC_BIST_EN;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_OFF_DIAG_EN            = ValveInitDataTemp->ACH_TxESV0_2_OFF_DIAG_EN;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_E_SOL_GPO              = ValveInitDataTemp->ACH_TxESV0_2_E_SOL_GPO;
    ASIC_TX_MSG_ESV0_2.R.B.mu1_Tx_ESV2_E_LSCLAMP_GPO          = ValveInitDataTemp->ACH_TxESV0_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG226_ESV0_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_ESV0_2.R.DATA;
    ACH_MsgConfig[ACH_MSG226_ESV0_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_ESV0_6.R.B.mu6_Tx_ESV6_PWM_CODE               = ValveInitDataTemp->ACH_TxESV0_6_PWM_CODE;
    ASIC_TX_MSG_ESV0_6.R.B.mu4_Tx_ESV6_FREQ_MOD_STEP          = ValveInitDataTemp->ACH_TxESV0_6_FREQ_MOD_STEP;
    ASIC_TX_MSG_ESV0_6.R.B.mu5_Tx_ESV6_MAX_FREQ_DELTA         = ValveInitDataTemp->ACH_TxESV0_6_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG230_ESV0_6_CTRLCFG_CHX].DATA         = ASIC_TX_MSG_ESV0_6.R.DATA;
    ACH_MsgConfig[ACH_MSG230_ESV0_6_CTRLCFG_CHX].WRT_REQ      = ACH_REQ_SET;

    ASIC_TX_MSG_ESV0_8.R.B.mu3_Tx_ESV8_KI                     = ValveInitDataTemp->ACH_TxESV0_8_KI;
    ASIC_TX_MSG_ESV0_8.R.B.mu3_Tx_ESV8_KP                     = ValveInitDataTemp->ACH_TxESV0_8_KP;
    ACH_MsgConfig[ACH_MSG232_ESV0_8_KGAINS_CHX].DATA          = ASIC_TX_MSG_ESV0_8.R.DATA;
    ACH_MsgConfig[ACH_MSG232_ESV0_8_KGAINS_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_ESV0_9.R.B.mu2_Tx_ESV9_INTTIME                = ValveInitDataTemp->ACH_TxESV0_9_INTTIME;
    ASIC_TX_MSG_ESV0_9.R.B.mu2_Tx_ESV9_FILT_TIME              = ValveInitDataTemp->ACH_TxESV0_9_FILT_TIME;
    ASIC_TX_MSG_ESV0_9.R.B.mu1_Tx_ESV9_CHOP_TIME              = ValveInitDataTemp->ACH_TxESV0_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG233_ESV0_9_OFFCMP_CHX].DATA          = ASIC_TX_MSG_ESV0_9.R.DATA;
    ACH_MsgConfig[ACH_MSG233_ESV0_9_OFFCMP_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_ESV1_2.R.B.mu2_Tx_ESV2_SR_SEL                 = ValveInitDataTemp->ACH_TxESV1_2_SR_SEL;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_CALIBRATION_DIS        = ValveInitDataTemp->ACH_TxESV1_2_CALIBRATION_DIS;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_OFS_CHOP_DIS           = ValveInitDataTemp->ACH_TxESV1_2_OFS_CHOP_DIS;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_TD_BLANK_SEL           = ValveInitDataTemp->ACH_TxESV1_2_TD_BLANK_SEL;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_LS_CLAMP_DIS           = ValveInitDataTemp->ACH_TxESV1_2_LS_CLAMP_DIS;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_LOGIC_BIST_EN          = ValveInitDataTemp->ACH_TxESV1_2_LOGIC_BIST_EN;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_DIAG_BIST_EN           = ValveInitDataTemp->ACH_TxESV1_2_DIAG_BIST_EN;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_ADC_BIST_EN            = ValveInitDataTemp->ACH_TxESV1_2_ADC_BIST_EN;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_OFF_DIAG_EN            = ValveInitDataTemp->ACH_TxESV1_2_OFF_DIAG_EN;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_E_SOL_GPO              = ValveInitDataTemp->ACH_TxESV1_2_E_SOL_GPO;
    ASIC_TX_MSG_ESV1_2.R.B.mu1_Tx_ESV2_E_LSCLAMP_GPO          = ValveInitDataTemp->ACH_TxESV1_2_E_LSCLAMP_GPO;
    ACH_MsgConfig[ACH_MSG242_ESV1_2_CONFIGURATION_CHX].DATA    = ASIC_TX_MSG_ESV1_2.R.DATA;
    ACH_MsgConfig[ACH_MSG242_ESV1_2_CONFIGURATION_CHX].WRT_REQ = ACH_REQ_SET;

    ASIC_TX_MSG_ESV1_6.R.B.mu6_Tx_ESV6_PWM_CODE               = ValveInitDataTemp->ACH_TxESV1_6_PWM_CODE;
    ASIC_TX_MSG_ESV1_6.R.B.mu4_Tx_ESV6_FREQ_MOD_STEP          = ValveInitDataTemp->ACH_TxESV1_6_FREQ_MOD_STEP;
    ASIC_TX_MSG_ESV1_6.R.B.mu5_Tx_ESV6_MAX_FREQ_DELTA         = ValveInitDataTemp->ACH_TxESV1_6_MAX_FREQ_DELTA;
    ACH_MsgConfig[ACH_MSG246_ESV1_6_CTRLCFG_CHX].DATA         = ASIC_TX_MSG_ESV1_6.R.DATA;
    ACH_MsgConfig[ACH_MSG246_ESV1_6_CTRLCFG_CHX].WRT_REQ      = ACH_REQ_SET;

    ASIC_TX_MSG_ESV1_8.R.B.mu3_Tx_ESV8_KI                     = ValveInitDataTemp->ACH_TxESV1_8_KI;
    ASIC_TX_MSG_ESV1_8.R.B.mu3_Tx_ESV8_KP                     = ValveInitDataTemp->ACH_TxESV1_8_KP;
    ACH_MsgConfig[ACH_MSG248_ESV1_8_KGAINS_CHX].DATA          = ASIC_TX_MSG_ESV1_8.R.DATA;
    ACH_MsgConfig[ACH_MSG248_ESV1_8_KGAINS_CHX].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_ESV1_9.R.B.mu2_Tx_ESV9_INTTIME                = ValveInitDataTemp->ACH_TxESV1_9_INTTIME;
    ASIC_TX_MSG_ESV1_9.R.B.mu2_Tx_ESV9_FILT_TIME              = ValveInitDataTemp->ACH_TxESV1_9_FILT_TIME;
    ASIC_TX_MSG_ESV1_9.R.B.mu1_Tx_ESV9_CHOP_TIME              = ValveInitDataTemp->ACH_TxESV1_9_CHOP_TIME;
    ACH_MsgConfig[ACH_MSG249_ESV1_9_OFFCMP_CHX].DATA          = ASIC_TX_MSG_ESV1_9.R.DATA;
    ACH_MsgConfig[ACH_MSG249_ESV1_9_OFFCMP_CHX].WRT_REQ       = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_MotorInitMsgSetFunc_CS(ACH_MotorInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_MotorInitDataType* MotorInitDataTemp;
    MotorInitDataTemp = Data;

    ASIC_TX_MSG_8.R.B.mu8_Tx_8_PUMP_TCK_PWM                = MotorInitDataTemp->ACH_Tx8_PMP_TCK_PWM;
    ACH_MsgConfig[ACH_MSG8_ID_PUMPCFG].DATA                = ASIC_TX_MSG_8.R.DATA;
    ACH_MsgConfig[ACH_MSG8_ID_PUMPCFG].WRT_REQ             = ACH_REQ_SET;

    ASIC_TX_MSG_9.R.B.mu2_Tx_9_PMP_VDS_TH                  = MotorInitDataTemp->ACH_Tx9_PMP_VDS_TH;
    ASIC_TX_MSG_9.R.B.mu1_Tx_9_PMP2_DIS                    = MotorInitDataTemp->ACH_Tx9_PMP2_DIS;
    ASIC_TX_MSG_9.R.B.mu3_Tx_9_PMP_VDS_FIL                 = MotorInitDataTemp->ACH_Tx9_PMP_VDS_FIL;
    ASIC_TX_MSG_9.R.B.mu1_Tx_9_LDACT_DIS                   = MotorInitDataTemp->ACH_Tx9_LDACT_DIS;
    ASIC_TX_MSG_9.R.B.mu1_Tx_9_PMP1_ISINC                  = MotorInitDataTemp->ACH_Tx9_PMP1_ISINC;
    ASIC_TX_MSG_9.R.B.mu4_Tx_9_PMP_DT                      = MotorInitDataTemp->ACH_Tx9_PMP_DT;
    ASIC_TX_MSG_9.R.B.mu1_Tx_9_PMP_TEST                    = MotorInitDataTemp->ACH_Tx9_PMP_TEST;
    ASIC_TX_MSG_9.R.B.mu1_Tx_9_PMP_BIST_EN                 = MotorInitDataTemp->ACH_Tx9_PMP_BIST_EN;
    ACH_MsgConfig[ACH_MSG9_ID_PUMPCFG2].DATA               = ASIC_TX_MSG_9.R.DATA;
    ACH_MsgConfig[ACH_MSG9_ID_PUMPCFG2].WRT_REQ            = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_VsoInitMsgSetFunc_CS(ACH_VsoInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_VsoInitDataType* VsoInitDataTemp;
    VsoInitDataTemp = Data;

    ASIC_TX_MSG_15.R.B.mu1_Tx_15_VSO_CONF                  = VsoInitDataTemp->ACH_Tx15_VSO_CONF;
    ASIC_TX_MSG_15.R.B.mu2_Tx_15_WSO_VSO_S                 = VsoInitDataTemp->ACH_Tx15_WSO_VSO_S;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SSIG_VSO                  = VsoInitDataTemp->ACH_Tx15_SSIG_VSO;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_VSO_PTEN                  = VsoInitDataTemp->ACH_Tx15_VSO_PTEN;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_VSO_OFF_DIAG_EN           = VsoInitDataTemp->ACH_Tx15_VSO_OFF_DIAG_EN;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_VSO_LS_CLAMP_DIS          = VsoInitDataTemp->ACH_Tx15_VSO_LS_CLAMP_DIS;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SEL_CONF                  = VsoInitDataTemp->ACH_Tx15_SEL_CONF;
    ASIC_TX_MSG_15.R.B.mu2_Tx_15_WSO_SEL_S                 = VsoInitDataTemp->ACH_Tx15_WSO_SEL_S;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SSIG_SEL                  = VsoInitDataTemp->ACH_Tx15_SSIG_SEL;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SEL_OUT_CMD               = VsoInitDataTemp->ACH_Tx15_SEL_OUT_CMD;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SEL_OFF_DIAG_EN           = VsoInitDataTemp->ACH_Tx15_SEL_OFF_DIAG_EN;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SEL_LS_CLAMP_DIS          = VsoInitDataTemp->ACH_Tx15_SEL_LS_CLAMP_DIS;
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SEL_PTEN                  = VsoInitDataTemp->ACH_Tx15_SEL_PTEN;
    ACH_MsgConfig[ACH_MSG15_ID_VSOSER].DATA                = ASIC_TX_MSG_15.R.DATA;
    ACH_MsgConfig[ACH_MSG15_ID_VSOSER].WRT_REQ             = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_WssInitMsgSetFunc_CS(ACH_WssInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_WssInitDataType* WssInitDataTemp;
    WssInitDataTemp = Data;

    ASIC_TX_MSG_32.R.B.mu7_Tx_32_CONFIG_RANGE              = WssInitDataTemp->ACH_Tx32_CONFIG_RANGE;
    ASIC_TX_MSG_32.R.B.mu1_Tx_32_WSO_TEST                  = WssInitDataTemp->ACH_Tx32_WSO_TEST;
    ACH_MsgConfig[ACH_MSG32_ID_WSITEST].DATA               = ASIC_TX_MSG_32.R.DATA;
    ACH_MsgConfig[ACH_MSG32_ID_WSITEST].WRT_REQ            = ACH_REQ_SET;

    ASIC_TX_MSG_33.R.B.mu8_Tx_33_WSI_FIRST_THRESHOLD       = WssInitDataTemp->ACH_Tx33_WSI_FIRST_THRESHOLD;
    ACH_MsgConfig[ACH_MSG33_ID_WSIAUXCONF].DATA            = ASIC_TX_MSG_33.R.DATA;
    ACH_MsgConfig[ACH_MSG33_ID_WSIAUXCONF].WRT_REQ         = ACH_REQ_SET;

    ASIC_TX_MSG_34.R.B.mu8_Tx_34_WSI_OFFSET_THRESHOLD      = WssInitDataTemp->ACH_Tx34_WSI_OFFSET_THRESHOLD;
    ACH_MsgConfig[ACH_MSG34_ID_WSIAUXCONF2].DATA           = ASIC_TX_MSG_34.R.DATA;
    ACH_MsgConfig[ACH_MSG34_ID_WSIAUXCONF2].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_35.R.B.mu2_Tx_35_FILTER_SELECTION          = WssInitDataTemp->ACH_Tx35_FILTER_SELECTION;
    ASIC_TX_MSG_35.R.B.mu1_Tx_35_PTEN                      = WssInitDataTemp->ACH_Tx35_PTEN;
    ASIC_TX_MSG_35.R.B.mu1_Tx_35_SS_DIS                    = WssInitDataTemp->ACH_Tx35_SS_DIS;
    ASIC_TX_MSG_35.R.B.mu1_Tx_35_FIX_TH                    = WssInitDataTemp->ACH_Tx35_FIX_TH;
    ASIC_TX_MSG_35.R.B.mu2_Tx_35_SENSOR_TYPE_SELECTION     = WssInitDataTemp->ACH_Tx35_SENSOR_TYPE_SELECTION;
    ACH_MsgConfig[ACH_MSG35_ID_WSIRSCR1].DATA              = ASIC_TX_MSG_35.R.DATA;
    ACH_MsgConfig[ACH_MSG35_ID_WSIRSCR1].WRT_REQ           = ACH_REQ_SET;

    ASIC_TX_MSG_36.R.B.mu2_Tx_36_FILTER_SELECTION          = WssInitDataTemp->ACH_Tx36_FILTER_SELECTION;
    ASIC_TX_MSG_36.R.B.mu1_Tx_36_PTEN                      = WssInitDataTemp->ACH_Tx36_PTEN;
    ASIC_TX_MSG_36.R.B.mu1_Tx_36_SS_DIS                    = WssInitDataTemp->ACH_Tx36_SS_DIS;
    ASIC_TX_MSG_36.R.B.mu1_Tx_36_FIX_TH                    = WssInitDataTemp->ACH_Tx36_FIX_TH;
    ASIC_TX_MSG_36.R.B.mu2_Tx_36_SENSOR_TYPE_SELECTION     = WssInitDataTemp->ACH_Tx36_SENSOR_TYPE_SELECTION;
    ACH_MsgConfig[ACH_MSG36_ID_WSIRSCR2].DATA              = ASIC_TX_MSG_36.R.DATA;
    ACH_MsgConfig[ACH_MSG36_ID_WSIRSCR2].WRT_REQ           = ACH_REQ_SET;

    ASIC_TX_MSG_37.R.B.mu2_Tx_37_FILTER_SELECTION          = WssInitDataTemp->ACH_Tx37_FILTER_SELECTION;
    ASIC_TX_MSG_37.R.B.mu1_Tx_37_PTEN                      = WssInitDataTemp->ACH_Tx37_PTEN;
    ASIC_TX_MSG_37.R.B.mu1_Tx_37_SS_DIS                    = WssInitDataTemp->ACH_Tx37_SS_DIS;
    ASIC_TX_MSG_37.R.B.mu1_Tx_37_FIX_TH                    = WssInitDataTemp->ACH_Tx37_FIX_TH;
    ASIC_TX_MSG_37.R.B.mu2_Tx_37_SENSOR_TYPE_SELECTION     = WssInitDataTemp->ACH_Tx37_SENSOR_TYPE_SELECTION;
    ACH_MsgConfig[ACH_MSG37_ID_WSIRSCR3].DATA              = ASIC_TX_MSG_37.R.DATA;
    ACH_MsgConfig[ACH_MSG37_ID_WSIRSCR3].WRT_REQ           = ACH_REQ_SET;

    ASIC_TX_MSG_38.R.B.mu2_Tx_38_FILTER_SELECTION          = WssInitDataTemp->ACH_Tx38_FILTER_SELECTION;
    ASIC_TX_MSG_38.R.B.mu1_Tx_38_PTEN                      = WssInitDataTemp->ACH_Tx38_PTEN;
    ASIC_TX_MSG_38.R.B.mu1_Tx_38_SS_DIS                    = WssInitDataTemp->ACH_Tx38_SS_DIS;
    ASIC_TX_MSG_38.R.B.mu1_Tx_38_FIX_TH                    = WssInitDataTemp->ACH_Tx38_FIX_TH;
    ASIC_TX_MSG_38.R.B.mu2_Tx_38_SENSOR_TYPE_SELECTION     = WssInitDataTemp->ACH_Tx38_SENSOR_TYPE_SELECTION;
    ACH_MsgConfig[ACH_MSG38_ID_WSIRSCR4].DATA              = ASIC_TX_MSG_38.R.DATA;
    ACH_MsgConfig[ACH_MSG38_ID_WSIRSCR4].WRT_REQ           = ACH_REQ_SET;

    ASIC_TX_MSG_39.R.B.mu1_Tx_39_FVPWR_ACT                 = WssInitDataTemp->ACH_Tx39_FVPWR_ACT;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_WSI_VDD4_UV               = WssInitDataTemp->ACH_Tx39_WSI_VDD4_UV;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_INIT                      = WssInitDataTemp->ACH_Tx39_INIT;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_DIAG                      = WssInitDataTemp->ACH_Tx39_DIAG;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_CH4_EN                    = WssInitDataTemp->ACH_Tx39_CH4_EN;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_CH3_EN                    = WssInitDataTemp->ACH_Tx39_CH3_EN;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_CH2_EN                    = WssInitDataTemp->ACH_Tx39_CH2_EN;
    ASIC_TX_MSG_39.R.B.mu1_Tx_39_CH1_EN                    = WssInitDataTemp->ACH_Tx39_CH1_EN;
    ACH_MsgConfig[ACH_MSG39_ID_WSICTRL].DATA               = ASIC_TX_MSG_39.R.DATA;
    ACH_MsgConfig[ACH_MSG39_ID_WSICTRL].WRT_REQ            = ACH_REQ_SET;

    ASIC_TX_MSG_44.R.B.mu1_Tx_44_WSI1_LBIST_EN             = WssInitDataTemp->ACH_Tx44_WSI1_LBIST_EN;
    ASIC_TX_MSG_44.R.B.mu1_Tx_44_WSI2_LBIST_EN             = WssInitDataTemp->ACH_Tx44_WSI2_LBIST_EN;
    ASIC_TX_MSG_44.R.B.mu1_Tx_44_WSI3_LBIST_EN             = WssInitDataTemp->ACH_Tx44_WSI3_LBIST_EN;
    ASIC_TX_MSG_44.R.B.mu1_Tx_44_WSI4_LBIST_EN             = WssInitDataTemp->ACH_Tx44_WSI4_LBIST_EN;
    ACH_MsgConfig[ACH_MSG44_ID_WSBIST].DATA                = ASIC_TX_MSG_44.R.DATA;
    ACH_MsgConfig[ACH_MSG44_ID_WSBIST].WRT_REQ             = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_LampShlsInitMsgSetFunc_CS(ACH_LampShlsInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_LampShlsInitDataType* LampShlsInitDataTemp;
    LampShlsInitDataTemp = Data;

    ASIC_TX_MSG_17.R.B.mu1_Tx_17_WLD_CONF                  = LampShlsInitDataTemp->ACH_Tx17_WLD_CONF;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_WLD_CMD                   = LampShlsInitDataTemp->ACH_Tx17_WLD_CMD;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_WLD_DIAGOFF_EN            = LampShlsInitDataTemp->ACH_Tx17_WLD_DIAGOFF_EN;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_WLD_LS_CLAMP_DIS          = LampShlsInitDataTemp->ACH_Tx17_WLD_LS_CLAMP_DIS;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_SHLS_CMD                  = LampShlsInitDataTemp->ACH_Tx17_SHLS_CMD;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_SHLS_DIAGOFF_EN           = LampShlsInitDataTemp->ACH_Tx17_SHLS_DIAGOFF_EN;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_SHLS_CONFIG               = LampShlsInitDataTemp->ACH_Tx17_SHLS_CONFIG;
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_SHLS_LS_CLAMP_DIS         = LampShlsInitDataTemp->ACH_Tx17_SHLS_LS_CLAMP_DIS;
    ACH_MsgConfig[ACH_MSG17_ID_WLD1SHLS_CONFIG].DATA       = ASIC_TX_MSG_17.R.DATA;
    ACH_MsgConfig[ACH_MSG17_ID_WLD1SHLS_CONFIG].WRT_REQ    = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_WatchdogInitMsgSetFunc_CS(ACH_WatchdogInitDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_WatchdogInitDataType* WatchdogInitDataTemp;
    WatchdogInitDataTemp = Data;

    ASIC_TX_MSG_2.R.B.mu2_Tx_2_WD_SW_DIS                   = WatchdogInitDataTemp->ACH_Tx2_WD_SW_DIS;
    ASIC_TX_MSG_2.R.B.mu1_Tx_2_WD_DIS                      = WatchdogInitDataTemp->ACH_Tx2_WD_DIS;
    ACH_MsgConfig[ACH_MSG2_ID_GENCFG2].DATA                = ASIC_TX_MSG_2.R.DATA;
    ACH_MsgConfig[ACH_MSG2_ID_GENCFG2].WRT_REQ             = ACH_REQ_SET;

    ASIC_TX_MSG_74.R.B.mu8_Tx_74_VALID_REQUEST_START       = WatchdogInitDataTemp->ACH_Tx74_VALID_REQUEST_START;
    ASIC_TX_MSG_74.R.B.mu8_Tx_74_VALID_ANSWER_START        = WatchdogInitDataTemp->ACH_Tx74_VALID_ANSWER_START;
    ACH_MsgConfig[ACH_MSG74_ID_WDG2STARTTMG].DATA          = ASIC_TX_MSG_74.R.DATA;
    ACH_MsgConfig[ACH_MSG74_ID_WDG2STARTTMG].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_75.R.B.mu6_Tx_75_VALID_ANSWER_DELTA        = WatchdogInitDataTemp->ACH_Tx75_VALID_ANSWER_DELTA;
    ASIC_TX_MSG_75.R.B.mu6_Tx_75_VALID_REQUEST_DELTA       = WatchdogInitDataTemp->ACH_Tx75_VALID_REQUEST_DELTA;
    ASIC_TX_MSG_75.R.B.mu1_Tx_75_REQ_CHECK_ENABLE          = WatchdogInitDataTemp->ACH_Tx75_REQ_CHECK_ENABLE;
    ACH_MsgConfig[ACH_MSG75_ID_WDG2DELTATMG].DATA          = ASIC_TX_MSG_75.R.DATA;
    ACH_MsgConfig[ACH_MSG75_ID_WDG2DELTATMG].WRT_REQ       = ACH_REQ_SET;

    ASIC_TX_MSG_76.R.B.mu6_Tx_76_ANSWER_TIME_OUT_DELTA     = WatchdogInitDataTemp->ACH_Tx76_ANSWER_TIME_OUT_DELTA;
    ASIC_TX_MSG_76.R.B.mu6_Tx_76_REQUEST_TIME_OUT_DELTA    = WatchdogInitDataTemp->ACH_Tx76_REQUEST_TIME_OUT_DELTA;
    ASIC_TX_MSG_76.R.B.mu1_Tx_76_TO_RESET_ENABLE           = WatchdogInitDataTemp->ACH_Tx76_TO_RESET_ENABLE;
    ACH_MsgConfig[ACH_MSG76_ID_WDG2TOUTTMG].DATA           = ASIC_TX_MSG_76.R.DATA;
    ACH_MsgConfig[ACH_MSG76_ID_WDG2TOUTTMG].WRT_REQ        = ACH_REQ_SET;

    ASIC_TX_MSG_77.R.B.mu3_Tx_77_NUM_OF_GOOD_STEPS         = WatchdogInitDataTemp->ACH_Tx77_NUM_OF_GOOD_STEPS;
    ASIC_TX_MSG_77.R.B.mu3_Tx_77_NUM_OF_BAD_STEPS          = WatchdogInitDataTemp->ACH_Tx77_NUM_OF_BAD_STEPS;
    ASIC_TX_MSG_77.R.B.mu4_Tx_77_HIGH_TH                   = WatchdogInitDataTemp->ACH_Tx77_HIGH_TH;
    ASIC_TX_MSG_77.R.B.mu4_Tx_77_LOW_TH                    = WatchdogInitDataTemp->ACH_Tx77_LOW_TH;
    ASIC_TX_MSG_77.R.B.mu1_Tx_77_CLOCK_DIVISION            = WatchdogInitDataTemp->ACH_Tx77_CLOCK_DIVISION;
    ASIC_TX_MSG_77.R.B.mu1_Tx_77_RESET_ENABLE              = WatchdogInitDataTemp->ACH_Tx77_RESET_ENABLE;
    ACH_MsgConfig[ACH_MSG77_ID_WDG2PGM].DATA               = ASIC_TX_MSG_77.R.DATA;
    ACH_MsgConfig[ACH_MSG77_ID_WDG2PGM].WRT_REQ            = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNo0MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_NO0_5.R.B.mu11_Tx_NT5_SET_POINT              = Ach_OutputBus.Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG133_NO0_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_NO0_5.R.DATA;
    ACH_MsgConfig[ACH_MSG133_NO0_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_0       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH0_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_0       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH0_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_0         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNo1MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_NO1_5.R.B.mu11_Tx_NT5_SET_POINT              = Ach_OutputBus.Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG149_NO1_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_NO1_5.R.DATA;
    ACH_MsgConfig[ACH_MSG149_NO1_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_1       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH1_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_1       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH1_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_1         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNo2MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_NO2_5.R.B.mu11_Tx_NT5_SET_POINT              = Ach_OutputBus.Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG165_NO2_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_NO2_5.R.DATA;
    ACH_MsgConfig[ACH_MSG165_NO2_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_2       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH2_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_2       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH2_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_2         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNo3MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_NO3_5.R.B.mu11_Tx_NT5_SET_POINT              = Ach_OutputBus.Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG181_NO3_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_NO3_5.R.DATA;
    ACH_MsgConfig[ACH_MSG181_NO3_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_3       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH3_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_3       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH3_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_3         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveTc0MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_TC0_5.R.B.mu11_Tx_NT5_SET_POINT              = Ach_OutputBus.Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG197_TC0_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_TC0_5.R.DATA;
    ACH_MsgConfig[ACH_MSG197_TC0_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_4       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH4_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_4       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH4_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_4         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveTc1MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_TC1_5.R.B.mu11_Tx_NT5_SET_POINT              = Ach_OutputBus.Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG213_TC1_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_TC1_5.R.DATA;
    ACH_MsgConfig[ACH_MSG213_TC1_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_5       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH5_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_5       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH5_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_5         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveEsv0MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_ESV0_5.R.B.mu11_Tx_ESV5_SET_POINT             = Ach_OutputBus.Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG229_ESV0_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_ESV0_5.R.DATA;
    ACH_MsgConfig[ACH_MSG229_ESV0_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_6       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH6_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_6       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH6_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_6         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveEsv1MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_ESV1_5.R.B.mu11_Tx_ESV5_SET_POINT             = Ach_OutputBus.Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT;
    ACH_MsgConfig[ACH_MSG245_ESV1_5_SETPOINT_CHX].DATA        = ASIC_TX_MSG_ESV1_5.R.DATA;
    ACH_MsgConfig[ACH_MSG245_ESV1_5_SETPOINT_CHX].WRT_REQ     = ACH_REQ_SET;
    
    if(Ach_OutputBus.Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT == 0)
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_7       = ACH_DISABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH7_STATUS_H            = ACH_VALVE_TRISTATE_MODE;
    }
    else
    {
        ASIC_TX_MSG_64.R.B.mu1_Tx_64_SOLENOID_ENABLE_7       = ACH_ENABLE;
        ASIC_TX_MSG_65.R.B.mu1_Tx_65_CH7_STATUS_H            = ACH_VALVE_ACTIVE_MODE;
    }
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].DATA              = ASIC_TX_MSG_64.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA].WRT_REQ           = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].DATA                 = ASIC_TX_MSG_65.R.DATA;
    ACH_MsgConfig[ACH_MSG65_ID_SOLENDR].WRT_REQ              = ACH_REQ_SET;

    ASIC_TX_MSG_64_2.R.B.mu1_Tx_64_SOLENOID_ENABLE_7         = ACH_ENABLE;              /* NOTICE : Additional MSG64, Need to send MSG64 twice in 1ms */
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].DATA            = ASIC_TX_MSG_64_2.R.DATA;
    ACH_MsgConfig[ACH_MSG64_ID_SOLSERVENA_2].WRT_REQ         = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNc0MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT > 0)
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH1_CMD = ACH_ENABLE;
    }
    else
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH1_CMD = ACH_DISABLE;
    }
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].DATA         = ASIC_TX_MSG_67.R.DATA;
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].WRT_REQ      = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNc1MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT > 0)
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH2_CMD = ACH_ENABLE;
    }
    else
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH2_CMD = ACH_DISABLE;
    }
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].DATA         = ASIC_TX_MSG_67.R.DATA;
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].WRT_REQ      = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNc2MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT > 0)
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH3_CMD = ACH_ENABLE;
    }
    else
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH3_CMD = ACH_DISABLE;
    }
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].DATA         = ASIC_TX_MSG_67.R.DATA;
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].WRT_REQ      = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveNc3MsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;
    
    if(Ach_OutputBus.Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT > 0)
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH4_CMD = ACH_ENABLE;
    }
    else
    {
        ASIC_TX_MSG_67.R.B.mu1_Tx_67_CH4_CMD = ACH_DISABLE;
    }
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].DATA         = ASIC_TX_MSG_67.R.DATA;
    ACH_MsgConfig[ACH_MSG67_ID_ONOFF_CH_CONFIG].WRT_REQ      = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_MotorDutyMsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    if(Ach_OutputBus.Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM <= MOTOR_10PER_DUTY)
    {
        ASIC_TX_MSG_8.R.B.mu8_Tx_8_PUMP_DTY_PWM             = MOTOR_10PER_DUTY;
        ASIC_TX_MSG_1.R.B.mu1_Tx_1_PMP_EN                   = ACH_DISABLE;
    }
    else if(Ach_OutputBus.Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM > MOTOR_90PER_DUTY)
    {
        ASIC_TX_MSG_8.R.B.mu8_Tx_8_PUMP_DTY_PWM             = MOTOR_MAX_DUTY;   /* Motor Max Duty Enabled */
        /*  ASIC_TX_MSG_8.R.B.mu8_Tx_8_PUMP_DTY_PWM             = MOTOR_90PER_DUTY; */
        ASIC_TX_MSG_1.R.B.mu1_Tx_1_PMP_EN                   = ACH_ENABLE;
    }
    else
    {
        ASIC_TX_MSG_8.R.B.mu8_Tx_8_PUMP_DTY_PWM             = Ach_OutputBus.Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM;
        ASIC_TX_MSG_1.R.B.mu1_Tx_1_PMP_EN                   = ACH_ENABLE;
    }
        
    ACH_MsgConfig[ACH_MSG8_ID_PUMPCFG].DATA                 = ASIC_TX_MSG_8.R.DATA;
    ACH_MsgConfig[ACH_MSG8_ID_PUMPCFG].WRT_REQ              = ACH_REQ_SET;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].DATA                  = ASIC_TX_MSG_1.R.DATA;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].WRT_REQ               = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_MotorFreqMsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_8.R.B.mu8_Tx_8_PUMP_TCK_PWM                 = Ach_OutputBus.Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM;
    ACH_MsgConfig[ACH_MSG8_ID_PUMPCFG].DATA                 = ASIC_TX_MSG_8.R.DATA;
    ACH_MsgConfig[ACH_MSG8_ID_PUMPCFG].WRT_REQ              = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ValveRelayMsgSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_1.R.B.mu1_Tx_1_FS_CMD                      = Ach_OutputBus.Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].DATA                 = ASIC_TX_MSG_1.R.DATA;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].WRT_REQ              = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_WachdogAnswerSetFunc_CS(ACH_WdAnswerDataType* Data)
{
    Std_ReturnType rtn = E_OK;
    ACH_WdAnswerDataType* ACH_WdAnswerDataTemp;
    ACH_WdAnswerDataTemp = Data;

    ASIC_TX_MSG_78.R.B.mu8_Tx_78_ANSWER_LOW                = ACH_WdAnswerDataTemp->ACH_Tx78_ANSWER_LOW;
    ASIC_TX_MSG_78.R.B.mu8_Tx_78_ANSWER_HIGH               = ACH_WdAnswerDataTemp->ACH_Tx78_ANSWER_HIGH;
    ACH_MsgConfig[ACH_MSG78_ID_WDG2ANS].DATA               = ASIC_TX_MSG_78.R.DATA;
    ACH_MsgConfig[ACH_MSG78_ID_WDG2ANS].WRT_REQ            = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_WachdogSeedRequestFunc_CS(void* Data)
{
    Std_ReturnType rtn = E_OK;
    
    /* Request Seed = Write on Seed register, Data no need */
    ACH_MsgConfig[ACH_MSG72_ID_WDG2SEED].WRT_REQ           = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ServiceTestEnableSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    #if 0 /* TODO */
    ASIC_TX_MSG_6.R.B.mu1_Tx_6_NVM_DWN                     = Ach_OutputBus.Ach_OutputHalServiceTestEnaWriteData.ACH_Tx6_NVM_DWN;
    ASIC_TX_MSG_6.R.B.mu1_Tx_6_NVM_CRC_MASK                = Ach_OutputBus.Ach_OutputHalServiceTestEnaWriteData.ACH_Tx6_NVM_CRC_MASK;
    ASIC_TX_MSG_6.R.B.mu1_Tx_6_CORE_BIST_EN                = Ach_OutputBus.Ach_OutputHalServiceTestEnaWriteData.ACH_Tx6_CORE_BIST_EN;
    ASIC_TX_MSG_6.R.B.mu1_Tx_6_ANALOG_BIST_EN              = Ach_OutputBus.Ach_OutputHalServiceTestEnaWriteData.ACH_Tx6_ANALOG_BIST_EN;
    ACH_MsgConfig[ACH_MSG6_ID_SERVTESTEN].DATA             = ASIC_TX_MSG_6.R.DATA;
    ACH_MsgConfig[ACH_MSG6_ID_SERVTESTEN].WRT_REQ          = ACH_REQ_SET;
    #endif
    
    return rtn;
}

static Std_ReturnType Ach_AdcSelectSetFunc(void)
{
    Std_ReturnType rtn = E_OK;
    
    #if 0 /* TODO */
    ASIC_TX_MSG_13.R.B.mu4_Tx_13_ADC_SEL                   = Ach_OutputBus.Ach_OutputHalAdcSelWriteData.ACH_Tx13_ADC_SEL;
    ACH_MsgConfig[ACH_MSG13_ID_ADC_DR].DATA                = ASIC_TX_MSG_13.R.DATA;
    ACH_MsgConfig[ACH_MSG13_ID_ADC_DR].WRT_REQ             = ACH_REQ_SET;
    #endif

    return rtn;
}

static Std_ReturnType Ach_WssCountSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    #if 0 /* TODO */
    ASIC_TX_MSG_40.R.B.mu1_Tx_40_WS_CNT_EN                 = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx40_WS_CNT_EN;
    ASIC_TX_MSG_40.R.B.mu1_Tx_40_WS_CNT_RST                = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx40_WS_CNT_RST;
    ACH_MsgConfig[ACH_MSG40_ID_WSCOUNT1].DATA              = ASIC_TX_MSG_40.R.DATA;
    ACH_MsgConfig[ACH_MSG40_ID_WSCOUNT1].WRT_REQ           = ACH_REQ_SET;
    
    ASIC_TX_MSG_41.R.B.mu1_Tx_41_WS_CNT_EN                 = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx41_WS_CNT_EN;
    ASIC_TX_MSG_41.R.B.mu1_Tx_41_WS_CNT_RST                = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx41_WS_CNT_RST;
    ACH_MsgConfig[ACH_MSG41_ID_WSCOUNT2].DATA              = ASIC_TX_MSG_41.R.DATA;
    ACH_MsgConfig[ACH_MSG41_ID_WSCOUNT2].WRT_REQ           = ACH_REQ_SET;
    
    ASIC_TX_MSG_42.R.B.mu1_Tx_42_WS_CNT_EN                 = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx42_WS_CNT_EN;
    ASIC_TX_MSG_42.R.B.mu1_Tx_42_WS_CNT_RST                = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx42_WS_CNT_RST;
    ACH_MsgConfig[ACH_MSG42_ID_WSCOUNT3].DATA              = ASIC_TX_MSG_42.R.DATA;
    ACH_MsgConfig[ACH_MSG42_ID_WSCOUNT3].WRT_REQ           = ACH_REQ_SET;
    
    ASIC_TX_MSG_43.R.B.mu1_Tx_43_WS_CNT_EN                 = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx43_WS_CNT_EN;
    ASIC_TX_MSG_43.R.B.mu1_Tx_43_WS_CNT_RST                = Ach_OutputBus.Ach_OutputHalWssCntWriteData.ACH_Tx43_WS_CNT_RST;
    ACH_MsgConfig[ACH_MSG43_ID_WSCOUNT4].DATA              = ASIC_TX_MSG_43.R.DATA;
    ACH_MsgConfig[ACH_MSG43_ID_WSCOUNT4].WRT_REQ           = ACH_REQ_SET;
    #endif

    return rtn;
}

static Std_ReturnType Ach_KeepAliveSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    ASIC_TX_MSG_1.R.B.mu1_Tx_1_PHOLD                       = Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD;
    ASIC_TX_MSG_1.R.B.mu1_Tx_1_KA                          = Ach_OutputBus.Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].DATA                 = ASIC_TX_MSG_1.R.DATA;
    ACH_MsgConfig[ACH_MSG1_ID_GENCFG].WRT_REQ              = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_VsoDriveSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    #if 0 /* TODO */
    ASIC_TX_MSG_15.R.B.mu1_Tx_15_SEL_OUT_CMD                = Ach_OutputBus.Ach_OutputHalVsoDrvWriteData.ACH_Tx_15_SEL_OUT_CMD;
    #endif
    ACH_MsgConfig[ACH_MSG15_ID_VSOSER].DATA                 = ASIC_TX_MSG_15.R.DATA;
    ACH_MsgConfig[ACH_MSG15_ID_VSOSER].WRT_REQ              = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_WldDriveSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    #if 0 /* TODO */
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_WLD_CMD                    = ACH_WldDriveDataTemp->ACH_Tx17_WLD_CMD;
    #endif
    ACH_MsgConfig[ACH_MSG17_ID_WLD1SHLS_CONFIG].DATA        = ASIC_TX_MSG_17.R.DATA;
    ACH_MsgConfig[ACH_MSG17_ID_WLD1SHLS_CONFIG].WRT_REQ     = ACH_REQ_SET;

    return rtn;
}

static Std_ReturnType Ach_ShlsDriveSetFunc(void)
{
    Std_ReturnType rtn = E_OK;

    #if 0 /* TODO */
    ASIC_TX_MSG_17.R.B.mu1_Tx_17_SHLS_CMD                   = ACH_ShlsDriveDataTemp->ACH_Tx17_SHLS_CMD;
    #endif
    ACH_MsgConfig[ACH_MSG17_ID_WLD1SHLS_CONFIG].DATA        = ASIC_TX_MSG_17.R.DATA;
    ACH_MsgConfig[ACH_MSG17_ID_WLD1SHLS_CONFIG].WRT_REQ     = ACH_REQ_SET;

    return rtn;
}

uint8 Ach_CRC5Gen(uint32 DATA)
{
#if 0   /* Original CRC Generator */
    uint32 l_u32CrcData = 0;
    uint32 l_u32CrcPoly = 0x4A000000;     /* Poly 0x25 25bit left shifted (not 26bit shifted because 1bit is ignored) */
    uint32 l_u32Data_MSB_check = 0;
    uint32 l_u32Data_MSB = 0x40000000;  /* MSB of 31 bit because 1bit(21th) is ignored */
    
    l_u32CrcData = ((DATA&0x1FFFE0)|((DATA>>1)&0x7FE00000))^0x7C000000;  /* ignore bit remove and complement the upper 5bits */
    l_u32Data_MSB_check = l_u32CrcData&l_u32Data_MSB;
    while(l_u32CrcData > 0x1F)
    {
        if(l_u32Data_MSB_check > 0)
        {
            l_u32CrcData = l_u32CrcData^l_u32CrcPoly;
        }
        l_u32Data_MSB = l_u32Data_MSB >> 1;
        l_u32CrcPoly = l_u32CrcPoly >> 1;
        l_u32Data_MSB_check = l_u32CrcData&l_u32Data_MSB;
    }
    return (uint8)l_u32CrcData;

#elif 0 /* Optimized CRC Generator */
    uint8 index = 0;
    uint32 crc = 0;
    
    crc = (((DATA & 0xFFC00000u) >> 1u) | (DATA & 0x001FFFE0u))^0x7C000000u;
    
    for ( index = 31u; index > 4u; index-- )
    {
        if  (0x80000000u == (crc & 0x80000000u))
        {
            crc = crc ^ CRC5_POLY_U32;
        }
        crc <<= 1u;
    }
    
    crc >>= 27;        

    return (uint8)crc;

#else /* CRC Generator with look-up table */

    Crc5Type CrcData;
    uint8 CrcTemp = 0;

    CrcData.CrcUnion = (uint32)(((DATA&0x1FFFE0)|((DATA>>1)&0x7FE00000))^0x7C000000);  /* ignore bit remove and complement the upper 5bits */
    CrcTemp = (Crc5Table0[(uint8)CrcData.B.CrcByte[3]])^(uint8)CrcData.B.CrcByte[2];
    CrcTemp = (Crc5Table0[CrcTemp])^(uint8)CrcData.B.CrcByte[1];
    CrcTemp = (Crc5Table0[CrcTemp])^(uint8)CrcData.B.CrcByte[0];
    CrcTemp = Crc5Table1[CrcTemp];

    return CrcTemp;

#endif
}

#if 0
uint8 Crc_Test(uint32 DATA)
{
#if 0 /* Crc Table 0 Generator */
    uint16 l_u16CrcData = 0;
    uint16 l_u16CrcPoly = 0x9400;     /* Poly 0x25 25bit left shifted (not 26bit shifted because 1bit is ignored) */
    uint16 l_u16Data_MSB_check = 0;
    uint16 l_u16Data_MSB = 0x8000;  /* MSB of 31 bit because 1bit(21th) is ignored */
    static uint8 crc_table[256];
    static uint8 i;
    
    l_u16CrcData = (uint16)i<<8;
    l_u16Data_MSB_check = l_u16CrcData&l_u16Data_MSB;
    while(l_u16CrcData > 0xFF)
    {
        if(l_u16Data_MSB_check > 0)
        {
            l_u16CrcData = l_u16CrcData^l_u16CrcPoly;
        }
        l_u16Data_MSB = l_u16Data_MSB >> 1;
        l_u16CrcPoly = l_u16CrcPoly >> 1;
        l_u16Data_MSB_check = l_u16CrcData&l_u16Data_MSB;
    }
    crc_table[i] = (uint8)l_u16CrcData;
    i++;
    return (uint8)l_u16CrcData;
#elif 0 /* Crc Table 1 Generator */
    uint8 l_u8CrcData = 0;
    uint8 l_u8CrcPoly = 0x94;     
    uint8 l_u8Data_MSB_check = 0;
    uint8 l_u8Data_MSB = 0x80;  
    static uint8 crc_table[256];
    static uint8 i;
    
    l_u8CrcData = i;
    l_u8Data_MSB_check = l_u8CrcData&l_u8Data_MSB;
    while(l_u8CrcData > 0x1F)
    {
        if(l_u8Data_MSB_check > 0)
        {
            l_u8CrcData = l_u8CrcData^l_u8CrcPoly;
        }
        l_u8Data_MSB = l_u8Data_MSB >> 1;
        l_u8CrcPoly = l_u8CrcPoly >> 1;
        l_u8Data_MSB_check = l_u8CrcData&l_u8Data_MSB;
    }
    crc_table[i] = (uint8)l_u8CrcData;
    i++;
    return (uint8)l_u8CrcData;
#else /* CRC generator with look-up table */
    Crc5Type CrcData;
    uint8 CrcTemp = 0;

    CrcData.CrcUnion = 0;
    CrcData.CrcUnion = ((DATA&0x1FFFE0)|((DATA>>1)&0x7FE00000))^0x7C000000;  /* ignore bit remove and complement the upper 5bits */
    CrcTemp = (Crc5Table0[(uint8)CrcData.B.CrcByte[3]])^(uint8)CrcData.B.CrcByte[2];
    CrcTemp = (Crc5Table0[CrcTemp])^(uint8)CrcData.B.CrcByte[1];
    CrcTemp = (Crc5Table0[CrcTemp])^(uint8)CrcData.B.CrcByte[0];
    CrcTemp = Crc5Table1[CrcTemp];

    return CrcTemp;

#endif
}
#endif

#define ACH_OUTPUT_STOP_SEC_CODE
#include "Ach_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

