/**
 * @defgroup Ach_Output Ach_Output
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Output.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACH_OUTPUT_PROCESS_H_
#define ACH_OUTPUT_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Types.h"
#include "Ach_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ach_Init(void);
extern void Ach_Process(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACH_OUTPUT_H_ */
/** @} */



