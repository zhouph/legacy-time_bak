/**
 * @defgroup Ach_Cfg Ach_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ach_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ach_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACH_START_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACH_STOP_SEC_CONST_UNSPECIFIED
#include "Ach_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACH_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACH_STOP_SEC_VAR_NOINIT_32BIT
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACH_STOP_SEC_VAR_UNSPECIFIED
#include "Ach_MemMap.h"
#define ACH_START_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/** Variable Section (32BIT)**/


#define ACH_STOP_SEC_VAR_32BIT
#include "Ach_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACH_START_SEC_CODE
#include "Ach_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACH_STOP_SEC_CODE
#include "Ach_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
