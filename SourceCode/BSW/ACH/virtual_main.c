/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Ach_Output.h"
#include "Ach_Input.h"

int main(void)
{
    Ach_Output_Init();
    Ach_Input_Init();

    while(1)
    {
        Ach_Output();
        Ach_Input();
    }
}