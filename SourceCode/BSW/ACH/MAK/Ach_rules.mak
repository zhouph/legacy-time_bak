# \file
#
# \brief Ach
#
# This file contains the implementation of the SWC
# module Ach.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Ach_src

Ach_src_FILES        += $(Ach_SRC_PATH)\Ach_Output.c
Ach_src_FILES        += $(Ach_SRC_PATH)\Ach_Output_Process.c
Ach_src_FILES        += $(Ach_IFA_PATH)\Ach_Output_Ifa.c
Ach_src_FILES        += $(Ach_SRC_PATH)\Ach_Input.c
Ach_src_FILES        += $(Ach_SRC_PATH)\Ach_Input_Process.c
Ach_src_FILES        += $(Ach_IFA_PATH)\Ach_Input_Ifa.c
Ach_src_FILES        += $(Ach_CFG_PATH)\Ach_Cfg.c
Ach_src_FILES        += $(Ach_CAL_PATH)\Ach_Cal.c

ifeq ($(ICE_COMPILE),true)
Ach_src_FILES        += $(Ach_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Ach_src_FILES        += $(Ach_UNITY_PATH)\unity.c
	Ach_src_FILES        += $(Ach_UNITY_PATH)\unity_fixture.c	
	Ach_src_FILES        += $(Ach_UT_PATH)\main.c
	Ach_src_FILES        += $(Ach_UT_PATH)\Ach_Output\Ach_Output_UtMain.c
	Ach_src_FILES        += $(Ach_UT_PATH)\Ach_Input\Ach_Input_UtMain.c
endif