# \file
#
# \brief Ach
#
# This file contains the implementation of the SWC
# module Ach.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Ach_CORE_PATH     := $(MANDO_BSW_ROOT)\Ach
Ach_CAL_PATH      := $(Ach_CORE_PATH)\CAL\$(Ach_VARIANT)
Ach_SRC_PATH      := $(Ach_CORE_PATH)\SRC
Ach_CFG_PATH      := $(Ach_CORE_PATH)\CFG\$(Ach_VARIANT)
Ach_HDR_PATH      := $(Ach_CORE_PATH)\HDR
Ach_IFA_PATH      := $(Ach_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Ach_CMN_PATH      := $(Ach_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Ach_UT_PATH		:= $(Ach_CORE_PATH)\UT
	Ach_UNITY_PATH	:= $(Ach_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Ach_UT_PATH)
	CC_INCLUDE_PATH		+= $(Ach_UNITY_PATH)
	Ach_Output_PATH 	:= Ach_UT_PATH\Ach_Output
	Ach_Input_PATH 	:= Ach_UT_PATH\Ach_Input
endif
CC_INCLUDE_PATH    += $(Ach_CAL_PATH)
CC_INCLUDE_PATH    += $(Ach_SRC_PATH)
CC_INCLUDE_PATH    += $(Ach_CFG_PATH)
CC_INCLUDE_PATH    += $(Ach_HDR_PATH)
CC_INCLUDE_PATH    += $(Ach_IFA_PATH)
CC_INCLUDE_PATH    += $(Ach_CMN_PATH)

