/**
 * @defgroup BbsVlvM_Main_Ifa BbsVlvM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        BbsVlvM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBSVLVM_MAIN_IFA_H_
#define BBSVLVM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbFsrBbsDrvInfo(data) do \
{ \
    *data = BbsVlvM_MainArbFsrBbsDrvInfo; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo(data) do \
{ \
    *data = BbsVlvM_MainArbNormVlvReqInfo; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(data) do \
{ \
    *data = BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_OV(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_UV(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_UV(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_OV(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_PMP_LD_ACT(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_ON(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_OFF(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VHD_OV(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(data) do \
{ \
    *data = BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbFsrBbsDrvInfo_FsrCbsDrv(data) do \
{ \
    *data = BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbFsrBbsDrvInfo_FsrCbsOff(data) do \
{ \
    *data = BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C0ol(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C0ol; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C0sb(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C0sb; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C0sg(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C0sg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C1ol(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C1ol; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C1sb(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C1sb; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C1sg(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C1sg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C2ol(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C2ol; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C2sb(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C2sb; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_C2sg(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.C2sg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_Fr(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.Fr; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_Ot(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.Ot; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_Lr(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.Lr; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_Uv(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.Uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo_Ff(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF0DcdInfo.Ff; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C3ol(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C3ol; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C3sb(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C3sb; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C3sg(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C3sg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C4ol(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C4ol; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C4sb(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C4sb; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C4sg(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C4sg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C5ol(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C5ol; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C5sb(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C5sb; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_C5sg(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.C5sg; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_Fr(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.Fr; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_Ot(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.Ot; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_Lr(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.Lr; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_Uv(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.Uv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo_Ff(data) do \
{ \
    *data = BbsVlvM_MainVlvdFF1DcdInfo.Ff; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[i]; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[i]; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[i]; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[i]; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[i]; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAchAsicInvalid(data) do \
{ \
    *data = BbsVlvM_MainAchAsicInvalid; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainEcuModeSts(data) do \
{ \
    *data = BbsVlvM_MainEcuModeSts; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainIgnOnOffSts(data) do \
{ \
    *data = BbsVlvM_MainIgnOnOffSts; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainIgnEdgeSts(data) do \
{ \
    *data = BbsVlvM_MainIgnEdgeSts; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVBatt1Mon(data) do \
{ \
    *data = BbsVlvM_MainVBatt1Mon; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVBatt2Mon(data) do \
{ \
    *data = BbsVlvM_MainVBatt2Mon; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainDiagClrSrs(data) do \
{ \
    *data = BbsVlvM_MainDiagClrSrs; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainFspCbsHMon(data) do \
{ \
    *data = BbsVlvM_MainFspCbsHMon; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainFspCbsMon(data) do \
{ \
    *data = BbsVlvM_MainFspCbsMon; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainVlvdInvalid(data) do \
{ \
    *data = BbsVlvM_MainVlvdInvalid; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainAsicEnDrDrv(data) do \
{ \
    *data = BbsVlvM_MainAsicEnDrDrv; \
}while(0);

#define BbsVlvM_Main_Read_BbsVlvM_MainArbVlvDriveState(data) do \
{ \
    *data = BbsVlvM_MainArbVlvDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo(data) do \
{ \
    BbsVlvM_MainFSBbsVlvActrInfo = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSFsrBbsDrvInfo(data) do \
{ \
    BbsVlvM_MainFSFsrBbsDrvInfo = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_sim_time(data) do \
{ \
    BbsVlvM_MainFSBbsVlvActrInfo.fs_on_sim_time = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cutp_time(data) do \
{ \
    BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cutp_time = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cuts_time(data) do \
{ \
    BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cuts_time = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_rlv_time(data) do \
{ \
    BbsVlvM_MainFSBbsVlvActrInfo.fs_on_rlv_time = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cv_time(data) do \
{ \
    BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cv_time = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSFsrBbsDrvInfo_FsrCbsDrv(data) do \
{ \
    BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsDrv = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSFsrBbsDrvInfo_FsrCbsOff(data) do \
{ \
    BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsOff = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_Batt1Fuse_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.Batt1Fuse_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_S2G_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_S2B_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_Short_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_OverTemp_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_ShutdownLine_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_CP_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_PsvOpen_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_Short_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_CurReg_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_PsvOpen_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_Short_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_CurReg_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_PsvOpen_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_Short_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_CurReg_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_PsvOpen_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_Short_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_CurReg_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_Open_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_PsvOpen_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_Short_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_CurReg_Err(data) do \
{ \
    BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err = *data; \
}while(0);

#define BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvInitEndFlg(data) do \
{ \
    BbsVlvM_MainFSBbsVlvInitEndFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBSVLVM_MAIN_IFA_H_ */
/** @} */
