/**
 * @defgroup BbsVlvM_Main_Ifa BbsVlvM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        BbsVlvM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "BbsVlvM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define BBSVLVM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_CODE
#include "BbsVlvM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define BBSVLVM_MAIN_STOP_SEC_CODE
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
