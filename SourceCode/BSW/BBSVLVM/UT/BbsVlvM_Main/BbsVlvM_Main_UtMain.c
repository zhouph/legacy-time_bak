#include "unity.h"
#include "unity_fixture.h"
#include "BbsVlvM_Main.h"
#include "BbsVlvM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_OVC;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_OUT_OF_REG;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OVC;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_OVC;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_DIODE_LOSS;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_REV_CURR;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_T_SD;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_T_SD;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_UV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_UV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_UV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_UV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_OV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_OV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_OV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VINT_OV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VINT_UV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_DGNDLOSS;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VPWR_UV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_VPWR_OV;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_LONG;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_SHORT;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_ADD;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_FCNT;
const Haluint8 UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[MAX_STEP] = BBSVLVM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_CRC;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_OV[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_CP_OV;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_UV[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_CP_UV;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_UV[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_VPWR_UV;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_OV[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_VPWR_OV;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_PMP_LD_ACT[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_PMP_LD_ACT;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_ON[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_FS_TURN_ON;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_OFF[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_FS_TURN_OFF;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_FS_VDS_FAULT;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_FS_RVP_FAULT;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VHD_OV[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_VHD_OV;
const Haluint8 UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT[MAX_STEP] = BBSVLVM_MAINACHVALVEASICINFO_ACH_ASIC_T_SD_INT;
const Saluint8 UtInput_BbsVlvM_MainArbFsrBbsDrvInfo_FsrCbsDrv[MAX_STEP] = BBSVLVM_MAINARBFSRBBSDRVINFO_FSRCBSDRV;
const Saluint8 UtInput_BbsVlvM_MainArbFsrBbsDrvInfo_FsrCbsOff[MAX_STEP] = BBSVLVM_MAINARBFSRBBSDRVINFO_FSRCBSOFF;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C0ol[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C0OL;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C0sb[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C0SB;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C0sg[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C0SG;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C1ol[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C1OL;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C1sb[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C1SB;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C1sg[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C1SG;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C2ol[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C2OL;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C2sb[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C2SB;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C2sg[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_C2SG;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Fr[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_FR;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Ot[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_OT;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Lr[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_LR;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Uv[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_UV;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Ff[MAX_STEP] = BBSVLVM_MAINVLVDFF0DCDINFO_FF;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C3ol[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C3OL;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C3sb[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C3SB;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C3sg[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C3SG;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C4ol[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C4OL;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C4sb[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C4SB;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C4sg[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C4SG;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C5ol[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C5OL;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C5sb[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C5SB;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C5sg[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_C5SG;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Fr[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_FR;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Ot[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_OT;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Lr[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_LR;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Uv[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_UV;
const Haluint8 UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Ff[MAX_STEP] = BBSVLVM_MAINVLVDFF1DCDINFO_FF;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_0[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_PRIMCUTVLVREQDATA_0;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_1[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_PRIMCUTVLVREQDATA_1;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_2[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_PRIMCUTVLVREQDATA_2;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_3[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_PRIMCUTVLVREQDATA_3;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_4[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_PRIMCUTVLVREQDATA_4;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_0[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SECDCUTVLVREQDATA_0;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_1[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SECDCUTVLVREQDATA_1;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_2[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SECDCUTVLVREQDATA_2;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_3[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SECDCUTVLVREQDATA_3;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_4[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SECDCUTVLVREQDATA_4;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_0[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SIMVLVREQDATA_0;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_1[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SIMVLVREQDATA_1;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_2[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SIMVLVREQDATA_2;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_3[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SIMVLVREQDATA_3;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_4[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_SIMVLVREQDATA_4;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_0[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_RELSVLVREQDATA_0;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_1[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_RELSVLVREQDATA_1;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_2[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_RELSVLVREQDATA_2;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_3[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_RELSVLVREQDATA_3;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_4[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_RELSVLVREQDATA_4;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_0[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_CIRCVLVREQDATA_0;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_1[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_CIRCVLVREQDATA_1;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_2[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_CIRCVLVREQDATA_2;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_3[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_CIRCVLVREQDATA_3;
const Saluint16 UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_4[MAX_STEP] = BBSVLVM_MAINARBNORMVLVREQINFO_CIRCVLVREQDATA_4;
const Ach_InputAchAsicInvalidInfo_t UtInput_BbsVlvM_MainAchAsicInvalid[MAX_STEP] = BBSVLVM_MAINACHASICINVALID;
const Mom_HndlrEcuModeSts_t UtInput_BbsVlvM_MainEcuModeSts[MAX_STEP] = BBSVLVM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_BbsVlvM_MainIgnOnOffSts[MAX_STEP] = BBSVLVM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_BbsVlvM_MainIgnEdgeSts[MAX_STEP] = BBSVLVM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_BbsVlvM_MainVBatt1Mon[MAX_STEP] = BBSVLVM_MAINVBATT1MON;
const Ioc_InputSR1msVBatt2Mon_t UtInput_BbsVlvM_MainVBatt2Mon[MAX_STEP] = BBSVLVM_MAINVBATT2MON;
const Diag_HndlrDiagClr_t UtInput_BbsVlvM_MainDiagClrSrs[MAX_STEP] = BBSVLVM_MAINDIAGCLRSRS;
const Ioc_InputSR1msFspCbsHMon_t UtInput_BbsVlvM_MainFspCbsHMon[MAX_STEP] = BBSVLVM_MAINFSPCBSHMON;
const Ioc_InputSR1msFspCbsMon_t UtInput_BbsVlvM_MainFspCbsMon[MAX_STEP] = BBSVLVM_MAINFSPCBSMON;
const Vlvd_A3944_HndlrVlvdInvalid_t UtInput_BbsVlvM_MainVlvdInvalid[MAX_STEP] = BBSVLVM_MAINVLVDINVALID;
const Arbitrator_VlvArbAsicEnDrDrvInfo_t UtInput_BbsVlvM_MainAsicEnDrDrv[MAX_STEP] = BBSVLVM_MAINASICENDRDRV;
const Arbitrator_VlvArbVlvDriveState_t UtInput_BbsVlvM_MainArbVlvDriveState[MAX_STEP] = BBSVLVM_MAINARBVLVDRIVESTATE;

const Saluint8 UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_sim_time[MAX_STEP] = BBSVLVM_MAINFSBBSVLVACTRINFO_FS_ON_SIM_TIME;
const Saluint8 UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cutp_time[MAX_STEP] = BBSVLVM_MAINFSBBSVLVACTRINFO_FS_ON_CUTP_TIME;
const Saluint8 UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cuts_time[MAX_STEP] = BBSVLVM_MAINFSBBSVLVACTRINFO_FS_ON_CUTS_TIME;
const Saluint8 UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_rlv_time[MAX_STEP] = BBSVLVM_MAINFSBBSVLVACTRINFO_FS_ON_RLV_TIME;
const Saluint8 UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cv_time[MAX_STEP] = BBSVLVM_MAINFSBBSVLVACTRINFO_FS_ON_CV_TIME;
const Saluint8 UtExpected_BbsVlvM_MainFSFsrBbsDrvInfo_FsrCbsDrv[MAX_STEP] = BBSVLVM_MAINFSFSRBBSDRVINFO_FSRCBSDRV;
const Saluint8 UtExpected_BbsVlvM_MainFSFsrBbsDrvInfo_FsrCbsOff[MAX_STEP] = BBSVLVM_MAINFSFSRBBSDRVINFO_FSRCBSOFF;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_Batt1Fuse_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BATT1FUSE_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_S2G_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_S2G_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_S2B_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_S2B_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_Short_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_SHORT_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_OverTemp_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_OVERTEMP_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_ShutdownLine_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_SHUTDOWNLINE_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_CP_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLVRELAY_CP_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_SIM_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_PsvOpen_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_SIM_PSVOPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_Short_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_SIM_SHORT_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_CurReg_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_SIM_CURREG_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTP_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_PsvOpen_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTP_PSVOPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_Short_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTP_SHORT_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_CurReg_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTP_CURREG_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTS_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_PsvOpen_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTS_PSVOPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_Short_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTS_SHORT_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_CurReg_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CUTS_CURREG_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_RLV_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_PsvOpen_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_RLV_PSVOPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_Short_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_RLV_SHORT_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_CurReg_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_RLV_CURREG_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_Open_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CIRCVLV_OPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_PsvOpen_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CIRCVLV_PSVOPEN_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_Short_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CIRCVLV_SHORT_ERR;
const Saluint8 UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_CurReg_Err[MAX_STEP] = BBSVLVM_MAINBBSVLVERRINFO_BBSVLV_CIRCVLV_CURREG_ERR;
const BbsVlvM_MainFSBbsVlvInitTest_t UtExpected_BbsVlvM_MainFSBbsVlvInitEndFlg[MAX_STEP] = BBSVLVM_MAINFSBBSVLVINITENDFLG;



TEST_GROUP(BbsVlvM_Main);
TEST_SETUP(BbsVlvM_Main)
{
    BbsVlvM_Main_Init();
}

TEST_TEAR_DOWN(BbsVlvM_Main)
{   /* Postcondition */

}

TEST(BbsVlvM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[i];
        BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = UtInput_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_OV[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_UV[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_UV[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_OV[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_PMP_LD_ACT[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_ON[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_OFF[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VHD_OV[i];
        BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = UtInput_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT[i];
        BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv = UtInput_BbsVlvM_MainArbFsrBbsDrvInfo_FsrCbsDrv[i];
        BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff = UtInput_BbsVlvM_MainArbFsrBbsDrvInfo_FsrCbsOff[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C0ol = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C0ol[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C0sb = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C0sb[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C0sg = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C0sg[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C1ol = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C1ol[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C1sb = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C1sb[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C1sg = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C1sg[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C2ol = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C2ol[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C2sb = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C2sb[i];
        BbsVlvM_MainVlvdFF0DcdInfo.C2sg = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_C2sg[i];
        BbsVlvM_MainVlvdFF0DcdInfo.Fr = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Fr[i];
        BbsVlvM_MainVlvdFF0DcdInfo.Ot = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Ot[i];
        BbsVlvM_MainVlvdFF0DcdInfo.Lr = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Lr[i];
        BbsVlvM_MainVlvdFF0DcdInfo.Uv = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Uv[i];
        BbsVlvM_MainVlvdFF0DcdInfo.Ff = UtInput_BbsVlvM_MainVlvdFF0DcdInfo_Ff[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C3ol = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C3ol[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C3sb = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C3sb[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C3sg = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C3sg[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C4ol = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C4ol[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C4sb = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C4sb[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C4sg = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C4sg[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C5ol = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C5ol[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C5sb = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C5sb[i];
        BbsVlvM_MainVlvdFF1DcdInfo.C5sg = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_C5sg[i];
        BbsVlvM_MainVlvdFF1DcdInfo.Fr = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Fr[i];
        BbsVlvM_MainVlvdFF1DcdInfo.Ot = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Ot[i];
        BbsVlvM_MainVlvdFF1DcdInfo.Lr = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Lr[i];
        BbsVlvM_MainVlvdFF1DcdInfo.Uv = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Uv[i];
        BbsVlvM_MainVlvdFF1DcdInfo.Ff = UtInput_BbsVlvM_MainVlvdFF1DcdInfo_Ff[i];
        BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[0] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_0[i];
        BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[1] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_1[i];
        BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[2] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_2[i];
        BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[3] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_3[i];
        BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[4] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData_4[i];
        BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[0] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_0[i];
        BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[1] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_1[i];
        BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[2] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_2[i];
        BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[3] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_3[i];
        BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[4] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData_4[i];
        BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[0] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_0[i];
        BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[1] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_1[i];
        BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[2] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_2[i];
        BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[3] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_3[i];
        BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[4] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData_4[i];
        BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[0] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_0[i];
        BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[1] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_1[i];
        BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[2] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_2[i];
        BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[3] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_3[i];
        BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[4] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData_4[i];
        BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[0] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_0[i];
        BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[1] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_1[i];
        BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[2] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_2[i];
        BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[3] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_3[i];
        BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[4] = UtInput_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData_4[i];
        BbsVlvM_MainAchAsicInvalid = UtInput_BbsVlvM_MainAchAsicInvalid[i];
        BbsVlvM_MainEcuModeSts = UtInput_BbsVlvM_MainEcuModeSts[i];
        BbsVlvM_MainIgnOnOffSts = UtInput_BbsVlvM_MainIgnOnOffSts[i];
        BbsVlvM_MainIgnEdgeSts = UtInput_BbsVlvM_MainIgnEdgeSts[i];
        BbsVlvM_MainVBatt1Mon = UtInput_BbsVlvM_MainVBatt1Mon[i];
        BbsVlvM_MainVBatt2Mon = UtInput_BbsVlvM_MainVBatt2Mon[i];
        BbsVlvM_MainDiagClrSrs = UtInput_BbsVlvM_MainDiagClrSrs[i];
        BbsVlvM_MainFspCbsHMon = UtInput_BbsVlvM_MainFspCbsHMon[i];
        BbsVlvM_MainFspCbsMon = UtInput_BbsVlvM_MainFspCbsMon[i];
        BbsVlvM_MainVlvdInvalid = UtInput_BbsVlvM_MainVlvdInvalid[i];
        BbsVlvM_MainAsicEnDrDrv = UtInput_BbsVlvM_MainAsicEnDrDrv[i];
        BbsVlvM_MainArbVlvDriveState = UtInput_BbsVlvM_MainArbVlvDriveState[i];

        BbsVlvM_Main();

        TEST_ASSERT_EQUAL(BbsVlvM_MainFSBbsVlvActrInfo.fs_on_sim_time, UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_sim_time[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cutp_time, UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cutp_time[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cuts_time, UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cuts_time[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSBbsVlvActrInfo.fs_on_rlv_time, UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_rlv_time[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cv_time, UtExpected_BbsVlvM_MainFSBbsVlvActrInfo_fs_on_cv_time[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsDrv, UtExpected_BbsVlvM_MainFSFsrBbsDrvInfo_FsrCbsDrv[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsOff, UtExpected_BbsVlvM_MainFSFsrBbsDrvInfo_FsrCbsOff[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.Batt1Fuse_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_Batt1Fuse_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_S2G_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_S2B_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_Short_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_OverTemp_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_ShutdownLine_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlvRelay_CP_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_PsvOpen_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_Short_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Sim_CurReg_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_PsvOpen_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_Short_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutP_CurReg_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_PsvOpen_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_Short_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CutS_CurReg_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_PsvOpen_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_Short_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_Rlv_CurReg_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_Open_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_PsvOpen_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_Short_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err, UtExpected_BbsVlvM_MainBBSVlvErrInfo_BbsVlv_CircVlv_CurReg_Err[i]);
        TEST_ASSERT_EQUAL(BbsVlvM_MainFSBbsVlvInitEndFlg, UtExpected_BbsVlvM_MainFSBbsVlvInitEndFlg[i]);
    }
}

TEST_GROUP_RUNNER(BbsVlvM_Main)
{
    RUN_TEST_CASE(BbsVlvM_Main, All);
}
