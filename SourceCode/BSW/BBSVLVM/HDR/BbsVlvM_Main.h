/**
 * @defgroup BbsVlvM_Main BbsVlvM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        BbsVlvM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBSVLVM_MAIN_H_
#define BBSVLVM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "BbsVlvM_Types.h"
#include "BbsVlvM_Cfg.h"
#include "BbsVlvM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define BBSVLVM_MAIN_MODULE_ID      (0)
 #define BBSVLVM_MAIN_MAJOR_VERSION  (2)
 #define BBSVLVM_MAIN_MINOR_VERSION  (0)
 #define BBSVLVM_MAIN_PATCH_VERSION  (0)
 #define BBSVLVM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern BbsVlvM_Main_HdrBusType BbsVlvM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t BbsVlvM_MainVersionInfo;

/* Input Data Element */
extern Ach_InputAchSysPwrAsicInfo_t BbsVlvM_MainAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t BbsVlvM_MainAchValveAsicInfo;
extern Arbitrator_VlvArbFsrBbsDrvInfo_t BbsVlvM_MainArbFsrBbsDrvInfo;
extern Vlvd_A3944_HndlrVlvdFF0DcdInfo_t BbsVlvM_MainVlvdFF0DcdInfo;
extern Vlvd_A3944_HndlrVlvdFF1DcdInfo_t BbsVlvM_MainVlvdFF1DcdInfo;
extern Arbitrator_VlvArbNormVlvReqInfo_t BbsVlvM_MainArbNormVlvReqInfo;
extern Ach_InputAchAsicInvalidInfo_t BbsVlvM_MainAchAsicInvalid;
extern Mom_HndlrEcuModeSts_t BbsVlvM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t BbsVlvM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t BbsVlvM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t BbsVlvM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t BbsVlvM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t BbsVlvM_MainDiagClrSrs;
extern Ioc_InputSR1msFspCbsHMon_t BbsVlvM_MainFspCbsHMon;
extern Ioc_InputSR1msFspCbsMon_t BbsVlvM_MainFspCbsMon;
extern Vlvd_A3944_HndlrVlvdInvalid_t BbsVlvM_MainVlvdInvalid;
extern Arbitrator_VlvArbAsicEnDrDrvInfo_t BbsVlvM_MainAsicEnDrDrv;
extern Arbitrator_VlvArbVlvDriveState_t BbsVlvM_MainArbVlvDriveState;

/* Output Data Element */
extern BbsVlvM_MainFSBbsVlvActr_t BbsVlvM_MainFSBbsVlvActrInfo;
extern BbsVlvM_MainFSFsrBbsDrvInfo_t BbsVlvM_MainFSFsrBbsDrvInfo;
extern BbsVlvM_MainBBSVlvErrInfo_t BbsVlvM_MainBBSVlvErrInfo;
extern BbsVlvM_MainFSBbsVlvInitTest_t BbsVlvM_MainFSBbsVlvInitEndFlg;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void BbsVlvM_Main_Init(void);
extern void BbsVlvM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBSVLVM_MAIN_H_ */
/** @} */
