/**
 * @defgroup BbsVlvM_Types BbsVlvM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        BbsVlvM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBSVLVM_TYPES_H_
#define BBSVLVM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ach_InputAchSysPwrAsicInfo_t BbsVlvM_MainAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t BbsVlvM_MainAchValveAsicInfo;
    Arbitrator_VlvArbFsrBbsDrvInfo_t BbsVlvM_MainArbFsrBbsDrvInfo;
    Vlvd_A3944_HndlrVlvdFF0DcdInfo_t BbsVlvM_MainVlvdFF0DcdInfo;
    Vlvd_A3944_HndlrVlvdFF1DcdInfo_t BbsVlvM_MainVlvdFF1DcdInfo;
    Arbitrator_VlvArbNormVlvReqInfo_t BbsVlvM_MainArbNormVlvReqInfo;
    Ach_InputAchAsicInvalidInfo_t BbsVlvM_MainAchAsicInvalid;
    Mom_HndlrEcuModeSts_t BbsVlvM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t BbsVlvM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t BbsVlvM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t BbsVlvM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t BbsVlvM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t BbsVlvM_MainDiagClrSrs;
    Ioc_InputSR1msFspCbsHMon_t BbsVlvM_MainFspCbsHMon;
    Ioc_InputSR1msFspCbsMon_t BbsVlvM_MainFspCbsMon;
    Vlvd_A3944_HndlrVlvdInvalid_t BbsVlvM_MainVlvdInvalid;
    Arbitrator_VlvArbAsicEnDrDrvInfo_t BbsVlvM_MainAsicEnDrDrv;
    Arbitrator_VlvArbVlvDriveState_t BbsVlvM_MainArbVlvDriveState;

/* Output Data Element */
    BbsVlvM_MainFSBbsVlvActr_t BbsVlvM_MainFSBbsVlvActrInfo;
    BbsVlvM_MainFSFsrBbsDrvInfo_t BbsVlvM_MainFSFsrBbsDrvInfo;
    BbsVlvM_MainBBSVlvErrInfo_t BbsVlvM_MainBBSVlvErrInfo;
    BbsVlvM_MainFSBbsVlvInitTest_t BbsVlvM_MainFSBbsVlvInitEndFlg;
}BbsVlvM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBSVLVM_TYPES_H_ */
/** @} */
