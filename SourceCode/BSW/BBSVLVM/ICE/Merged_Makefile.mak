# don't generate code for Os_MemoryProtection.xdm (enables MemoryProtection)
#TRESOS2_NOGEN_PLUGINS += Os_MemoryProtection

# don't generate code for MicroKernel Os
#TRESOS2_NOGEN_PLUGINS += MicroOs

# enable MicroKernel Os plugin
#ENABLED_PLUGINS += MicroOs

# Settings for Multicore: Use other board directory
#BOARD = TriboardTC277_MK
