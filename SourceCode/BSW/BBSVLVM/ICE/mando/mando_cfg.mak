# \file
#
# \brief Mando make configuration module
#
# This file contains the implementation of the Mando make 
# configuration module.
#
# \author Mando, Advanced R&D, Korea


Module             ?= BbsVlvM

MANDO_BSW_ROOT     ?= $(PROJECT_ROOT)/..
$(Module)_VARIANT  ?= VariantName
EXT_HEADER         ?= $(UTIL_DIR)/ext_header

ICE_COMPILE        ?= true
UNIT_TEST          ?= false