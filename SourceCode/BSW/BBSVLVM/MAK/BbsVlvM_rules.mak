# \file
#
# \brief BbsVlvM
#
# This file contains the implementation of the SWC
# module BbsVlvM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += BbsVlvM_src

BbsVlvM_src_FILES        += $(BbsVlvM_SRC_PATH)\BbsVlvM_Main.c
BbsVlvM_src_FILES        += $(BbsVlvM_IFA_PATH)\BbsVlvM_Main_Ifa.c
BbsVlvM_src_FILES        += $(BbsVlvM_CFG_PATH)\BbsVlvM_Cfg.c
BbsVlvM_src_FILES        += $(BbsVlvM_CAL_PATH)\BbsVlvM_Cal.c
BbsVlvM_src_FILES        += $(BbsVlvM_SRC_PATH)\BbsVlvM_Process.c

ifeq ($(ICE_COMPILE),true)
BbsVlvM_src_FILES        += $(BbsVlvM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	BbsVlvM_src_FILES        += $(BbsVlvM_UNITY_PATH)\unity.c
	BbsVlvM_src_FILES        += $(BbsVlvM_UNITY_PATH)\unity_fixture.c	
	BbsVlvM_src_FILES        += $(BbsVlvM_UT_PATH)\main.c
	BbsVlvM_src_FILES        += $(BbsVlvM_UT_PATH)\BbsVlvM_Main\BbsVlvM_Main_UtMain.c
endif