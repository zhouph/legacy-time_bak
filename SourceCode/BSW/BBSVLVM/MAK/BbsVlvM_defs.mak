# \file
#
# \brief BbsVlvM
#
# This file contains the implementation of the SWC
# module BbsVlvM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

BbsVlvM_CORE_PATH     := $(MANDO_BSW_ROOT)\BbsVlvM
BbsVlvM_CAL_PATH      := $(BbsVlvM_CORE_PATH)\CAL\$(BbsVlvM_VARIANT)
BbsVlvM_SRC_PATH      := $(BbsVlvM_CORE_PATH)\SRC
BbsVlvM_CFG_PATH      := $(BbsVlvM_CORE_PATH)\CFG\$(BbsVlvM_VARIANT)
BbsVlvM_HDR_PATH      := $(BbsVlvM_CORE_PATH)\HDR
BbsVlvM_IFA_PATH      := $(BbsVlvM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    BbsVlvM_CMN_PATH      := $(BbsVlvM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	BbsVlvM_UT_PATH		:= $(BbsVlvM_CORE_PATH)\UT
	BbsVlvM_UNITY_PATH	:= $(BbsVlvM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(BbsVlvM_UT_PATH)
	CC_INCLUDE_PATH		+= $(BbsVlvM_UNITY_PATH)
	BbsVlvM_Main_PATH 	:= BbsVlvM_UT_PATH\BbsVlvM_Main
endif
CC_INCLUDE_PATH    += $(BbsVlvM_CAL_PATH)
CC_INCLUDE_PATH    += $(BbsVlvM_SRC_PATH)
CC_INCLUDE_PATH    += $(BbsVlvM_CFG_PATH)
CC_INCLUDE_PATH    += $(BbsVlvM_HDR_PATH)
CC_INCLUDE_PATH    += $(BbsVlvM_IFA_PATH)
CC_INCLUDE_PATH    += $(BbsVlvM_CMN_PATH)

