BbsVlvM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    };
BbsVlvM_MainAchSysPwrAsicInfo = CreateBus(BbsVlvM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

BbsVlvM_MainAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_VPWR_UV'
    'Ach_Asic_VPWR_OV'
    'Ach_Asic_PMP_LD_ACT'
    'Ach_Asic_FS_TURN_ON'
    'Ach_Asic_FS_TURN_OFF'
    'Ach_Asic_FS_VDS_FAULT'
    'Ach_Asic_FS_RVP_FAULT'
    'Ach_Asic_VHD_OV'
    'Ach_Asic_T_SD_INT'
    };
BbsVlvM_MainAchValveAsicInfo = CreateBus(BbsVlvM_MainAchValveAsicInfo, DeList);
clear DeList;

BbsVlvM_MainArbFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
BbsVlvM_MainArbFsrBbsDrvInfo = CreateBus(BbsVlvM_MainArbFsrBbsDrvInfo, DeList);
clear DeList;

BbsVlvM_MainVlvdFF0DcdInfo = Simulink.Bus;
DeList={
    'C0ol'
    'C0sb'
    'C0sg'
    'C1ol'
    'C1sb'
    'C1sg'
    'C2ol'
    'C2sb'
    'C2sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
BbsVlvM_MainVlvdFF0DcdInfo = CreateBus(BbsVlvM_MainVlvdFF0DcdInfo, DeList);
clear DeList;

BbsVlvM_MainVlvdFF1DcdInfo = Simulink.Bus;
DeList={
    'C3ol'
    'C3sb'
    'C3sg'
    'C4ol'
    'C4sb'
    'C4sg'
    'C5ol'
    'C5sb'
    'C5sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
BbsVlvM_MainVlvdFF1DcdInfo = CreateBus(BbsVlvM_MainVlvdFF1DcdInfo, DeList);
clear DeList;

BbsVlvM_MainArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    };
BbsVlvM_MainArbNormVlvReqInfo = CreateBus(BbsVlvM_MainArbNormVlvReqInfo, DeList);
clear DeList;

BbsVlvM_MainAchAsicInvalid = Simulink.Bus;
DeList={'BbsVlvM_MainAchAsicInvalid'};
BbsVlvM_MainAchAsicInvalid = CreateBus(BbsVlvM_MainAchAsicInvalid, DeList);
clear DeList;

BbsVlvM_MainEcuModeSts = Simulink.Bus;
DeList={'BbsVlvM_MainEcuModeSts'};
BbsVlvM_MainEcuModeSts = CreateBus(BbsVlvM_MainEcuModeSts, DeList);
clear DeList;

BbsVlvM_MainIgnOnOffSts = Simulink.Bus;
DeList={'BbsVlvM_MainIgnOnOffSts'};
BbsVlvM_MainIgnOnOffSts = CreateBus(BbsVlvM_MainIgnOnOffSts, DeList);
clear DeList;

BbsVlvM_MainIgnEdgeSts = Simulink.Bus;
DeList={'BbsVlvM_MainIgnEdgeSts'};
BbsVlvM_MainIgnEdgeSts = CreateBus(BbsVlvM_MainIgnEdgeSts, DeList);
clear DeList;

BbsVlvM_MainVBatt1Mon = Simulink.Bus;
DeList={'BbsVlvM_MainVBatt1Mon'};
BbsVlvM_MainVBatt1Mon = CreateBus(BbsVlvM_MainVBatt1Mon, DeList);
clear DeList;

BbsVlvM_MainVBatt2Mon = Simulink.Bus;
DeList={'BbsVlvM_MainVBatt2Mon'};
BbsVlvM_MainVBatt2Mon = CreateBus(BbsVlvM_MainVBatt2Mon, DeList);
clear DeList;

BbsVlvM_MainDiagClrSrs = Simulink.Bus;
DeList={'BbsVlvM_MainDiagClrSrs'};
BbsVlvM_MainDiagClrSrs = CreateBus(BbsVlvM_MainDiagClrSrs, DeList);
clear DeList;

BbsVlvM_MainFspCbsHMon = Simulink.Bus;
DeList={'BbsVlvM_MainFspCbsHMon'};
BbsVlvM_MainFspCbsHMon = CreateBus(BbsVlvM_MainFspCbsHMon, DeList);
clear DeList;

BbsVlvM_MainFspCbsMon = Simulink.Bus;
DeList={'BbsVlvM_MainFspCbsMon'};
BbsVlvM_MainFspCbsMon = CreateBus(BbsVlvM_MainFspCbsMon, DeList);
clear DeList;

BbsVlvM_MainVlvdInvalid = Simulink.Bus;
DeList={'BbsVlvM_MainVlvdInvalid'};
BbsVlvM_MainVlvdInvalid = CreateBus(BbsVlvM_MainVlvdInvalid, DeList);
clear DeList;

BbsVlvM_MainAsicEnDrDrv = Simulink.Bus;
DeList={'BbsVlvM_MainAsicEnDrDrv'};
BbsVlvM_MainAsicEnDrDrv = CreateBus(BbsVlvM_MainAsicEnDrDrv, DeList);
clear DeList;

BbsVlvM_MainArbVlvDriveState = Simulink.Bus;
DeList={'BbsVlvM_MainArbVlvDriveState'};
BbsVlvM_MainArbVlvDriveState = CreateBus(BbsVlvM_MainArbVlvDriveState, DeList);
clear DeList;

BbsVlvM_MainFSBbsVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_sim_time'
    'fs_on_cutp_time'
    'fs_on_cuts_time'
    'fs_on_rlv_time'
    'fs_on_cv_time'
    };
BbsVlvM_MainFSBbsVlvActrInfo = CreateBus(BbsVlvM_MainFSBbsVlvActrInfo, DeList);
clear DeList;

BbsVlvM_MainFSFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
BbsVlvM_MainFSFsrBbsDrvInfo = CreateBus(BbsVlvM_MainFSFsrBbsDrvInfo, DeList);
clear DeList;

BbsVlvM_MainBBSVlvErrInfo = Simulink.Bus;
DeList={
    'Batt1Fuse_Open_Err'
    'BbsVlvRelay_Open_Err'
    'BbsVlvRelay_S2G_Err'
    'BbsVlvRelay_S2B_Err'
    'BbsVlvRelay_Short_Err'
    'BbsVlvRelay_OverTemp_Err'
    'BbsVlvRelay_ShutdownLine_Err'
    'BbsVlvRelay_CP_Err'
    'BbsVlv_Sim_Open_Err'
    'BbsVlv_Sim_PsvOpen_Err'
    'BbsVlv_Sim_Short_Err'
    'BbsVlv_Sim_CurReg_Err'
    'BbsVlv_CutP_Open_Err'
    'BbsVlv_CutP_PsvOpen_Err'
    'BbsVlv_CutP_Short_Err'
    'BbsVlv_CutP_CurReg_Err'
    'BbsVlv_CutS_Open_Err'
    'BbsVlv_CutS_PsvOpen_Err'
    'BbsVlv_CutS_Short_Err'
    'BbsVlv_CutS_CurReg_Err'
    'BbsVlv_Rlv_Open_Err'
    'BbsVlv_Rlv_PsvOpen_Err'
    'BbsVlv_Rlv_Short_Err'
    'BbsVlv_Rlv_CurReg_Err'
    'BbsVlv_CircVlv_Open_Err'
    'BbsVlv_CircVlv_PsvOpen_Err'
    'BbsVlv_CircVlv_Short_Err'
    'BbsVlv_CircVlv_CurReg_Err'
    };
BbsVlvM_MainBBSVlvErrInfo = CreateBus(BbsVlvM_MainBBSVlvErrInfo, DeList);
clear DeList;

BbsVlvM_MainFSBbsVlvInitEndFlg = Simulink.Bus;
DeList={'BbsVlvM_MainFSBbsVlvInitEndFlg'};
BbsVlvM_MainFSBbsVlvInitEndFlg = CreateBus(BbsVlvM_MainFSBbsVlvInitEndFlg, DeList);
clear DeList;

