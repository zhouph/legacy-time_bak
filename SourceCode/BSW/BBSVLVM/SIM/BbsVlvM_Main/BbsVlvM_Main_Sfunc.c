#define S_FUNCTION_NAME      BbsVlvM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          104
#define WidthOutputPort         36

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "BbsVlvM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = input[0];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = input[1];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = input[2];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = input[3];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = input[4];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = input[5];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = input[6];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = input[7];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = input[8];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = input[9];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = input[10];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = input[11];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = input[12];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = input[13];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = input[14];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = input[15];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = input[16];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = input[17];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = input[18];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = input[19];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = input[20];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = input[21];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = input[22];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = input[23];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = input[24];
    BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = input[25];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV = input[26];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV = input[27];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV = input[28];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV = input[29];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = input[30];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON = input[31];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = input[32];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = input[33];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = input[34];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV = input[35];
    BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = input[36];
    BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv = input[37];
    BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff = input[38];
    BbsVlvM_MainVlvdFF0DcdInfo.C0ol = input[39];
    BbsVlvM_MainVlvdFF0DcdInfo.C0sb = input[40];
    BbsVlvM_MainVlvdFF0DcdInfo.C0sg = input[41];
    BbsVlvM_MainVlvdFF0DcdInfo.C1ol = input[42];
    BbsVlvM_MainVlvdFF0DcdInfo.C1sb = input[43];
    BbsVlvM_MainVlvdFF0DcdInfo.C1sg = input[44];
    BbsVlvM_MainVlvdFF0DcdInfo.C2ol = input[45];
    BbsVlvM_MainVlvdFF0DcdInfo.C2sb = input[46];
    BbsVlvM_MainVlvdFF0DcdInfo.C2sg = input[47];
    BbsVlvM_MainVlvdFF0DcdInfo.Fr = input[48];
    BbsVlvM_MainVlvdFF0DcdInfo.Ot = input[49];
    BbsVlvM_MainVlvdFF0DcdInfo.Lr = input[50];
    BbsVlvM_MainVlvdFF0DcdInfo.Uv = input[51];
    BbsVlvM_MainVlvdFF0DcdInfo.Ff = input[52];
    BbsVlvM_MainVlvdFF1DcdInfo.C3ol = input[53];
    BbsVlvM_MainVlvdFF1DcdInfo.C3sb = input[54];
    BbsVlvM_MainVlvdFF1DcdInfo.C3sg = input[55];
    BbsVlvM_MainVlvdFF1DcdInfo.C4ol = input[56];
    BbsVlvM_MainVlvdFF1DcdInfo.C4sb = input[57];
    BbsVlvM_MainVlvdFF1DcdInfo.C4sg = input[58];
    BbsVlvM_MainVlvdFF1DcdInfo.C5ol = input[59];
    BbsVlvM_MainVlvdFF1DcdInfo.C5sb = input[60];
    BbsVlvM_MainVlvdFF1DcdInfo.C5sg = input[61];
    BbsVlvM_MainVlvdFF1DcdInfo.Fr = input[62];
    BbsVlvM_MainVlvdFF1DcdInfo.Ot = input[63];
    BbsVlvM_MainVlvdFF1DcdInfo.Lr = input[64];
    BbsVlvM_MainVlvdFF1DcdInfo.Uv = input[65];
    BbsVlvM_MainVlvdFF1DcdInfo.Ff = input[66];
    BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[0] = input[67];
    BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[1] = input[68];
    BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[2] = input[69];
    BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[3] = input[70];
    BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[4] = input[71];
    BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[0] = input[72];
    BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[1] = input[73];
    BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[2] = input[74];
    BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[3] = input[75];
    BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[4] = input[76];
    BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[0] = input[77];
    BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[1] = input[78];
    BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[2] = input[79];
    BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[3] = input[80];
    BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[4] = input[81];
    BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[0] = input[82];
    BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[1] = input[83];
    BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[2] = input[84];
    BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[3] = input[85];
    BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[4] = input[86];
    BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[0] = input[87];
    BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[1] = input[88];
    BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[2] = input[89];
    BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[3] = input[90];
    BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[4] = input[91];
    BbsVlvM_MainAchAsicInvalid = input[92];
    BbsVlvM_MainEcuModeSts = input[93];
    BbsVlvM_MainIgnOnOffSts = input[94];
    BbsVlvM_MainIgnEdgeSts = input[95];
    BbsVlvM_MainVBatt1Mon = input[96];
    BbsVlvM_MainVBatt2Mon = input[97];
    BbsVlvM_MainDiagClrSrs = input[98];
    BbsVlvM_MainFspCbsHMon = input[99];
    BbsVlvM_MainFspCbsMon = input[100];
    BbsVlvM_MainVlvdInvalid = input[101];
    BbsVlvM_MainAsicEnDrDrv = input[102];
    BbsVlvM_MainArbVlvDriveState = input[103];

    BbsVlvM_Main();


    output[0] = BbsVlvM_MainFSBbsVlvActrInfo.fs_on_sim_time;
    output[1] = BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cutp_time;
    output[2] = BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cuts_time;
    output[3] = BbsVlvM_MainFSBbsVlvActrInfo.fs_on_rlv_time;
    output[4] = BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cv_time;
    output[5] = BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsDrv;
    output[6] = BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsOff;
    output[7] = BbsVlvM_MainBBSVlvErrInfo.Batt1Fuse_Open_Err;
    output[8] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err;
    output[9] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err;
    output[10] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err;
    output[11] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err;
    output[12] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err;
    output[13] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err;
    output[14] = BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err;
    output[15] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err;
    output[16] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err;
    output[17] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err;
    output[18] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err;
    output[19] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err;
    output[20] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err;
    output[21] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err;
    output[22] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err;
    output[23] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err;
    output[24] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err;
    output[25] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err;
    output[26] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err;
    output[27] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err;
    output[28] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err;
    output[29] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err;
    output[30] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err;
    output[31] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err;
    output[32] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err;
    output[33] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err;
    output[34] = BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err;
    output[35] = BbsVlvM_MainFSBbsVlvInitEndFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    BbsVlvM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
