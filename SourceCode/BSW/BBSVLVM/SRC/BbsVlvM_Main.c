/**
 * @defgroup BbsVlvM_Main BbsVlvM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        BbsVlvM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "BbsVlvM_Main.h"
#include "BbsVlvM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define BBSVLVM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
BbsVlvM_Main_HdrBusType BbsVlvM_MainBus;

/* Version Info */
const SwcVersionInfo_t BbsVlvM_MainVersionInfo = 
{   
    BBSVLVM_MAIN_MODULE_ID,           /* BbsVlvM_MainVersionInfo.ModuleId */
    BBSVLVM_MAIN_MAJOR_VERSION,       /* BbsVlvM_MainVersionInfo.MajorVer */
    BBSVLVM_MAIN_MINOR_VERSION,       /* BbsVlvM_MainVersionInfo.MinorVer */
    BBSVLVM_MAIN_PATCH_VERSION,       /* BbsVlvM_MainVersionInfo.PatchVer */
    BBSVLVM_MAIN_BRANCH_VERSION       /* BbsVlvM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchSysPwrAsicInfo_t BbsVlvM_MainAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t BbsVlvM_MainAchValveAsicInfo;
Arbitrator_VlvArbFsrBbsDrvInfo_t BbsVlvM_MainArbFsrBbsDrvInfo;
Vlvd_A3944_HndlrVlvdFF0DcdInfo_t BbsVlvM_MainVlvdFF0DcdInfo;
Vlvd_A3944_HndlrVlvdFF1DcdInfo_t BbsVlvM_MainVlvdFF1DcdInfo;
Arbitrator_VlvArbNormVlvReqInfo_t BbsVlvM_MainArbNormVlvReqInfo;
Ach_InputAchAsicInvalidInfo_t BbsVlvM_MainAchAsicInvalid;
Mom_HndlrEcuModeSts_t BbsVlvM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t BbsVlvM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t BbsVlvM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t BbsVlvM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t BbsVlvM_MainVBatt2Mon;
Diag_HndlrDiagClr_t BbsVlvM_MainDiagClrSrs;
Ioc_InputSR1msFspCbsHMon_t BbsVlvM_MainFspCbsHMon;
Ioc_InputSR1msFspCbsMon_t BbsVlvM_MainFspCbsMon;
Vlvd_A3944_HndlrVlvdInvalid_t BbsVlvM_MainVlvdInvalid;
Arbitrator_VlvArbAsicEnDrDrvInfo_t BbsVlvM_MainAsicEnDrDrv;
Arbitrator_VlvArbVlvDriveState_t BbsVlvM_MainArbVlvDriveState;

/* Output Data Element */
BbsVlvM_MainFSBbsVlvActr_t BbsVlvM_MainFSBbsVlvActrInfo;
BbsVlvM_MainFSFsrBbsDrvInfo_t BbsVlvM_MainFSFsrBbsDrvInfo;
BbsVlvM_MainBBSVlvErrInfo_t BbsVlvM_MainBBSVlvErrInfo;
BbsVlvM_MainFSBbsVlvInitTest_t BbsVlvM_MainFSBbsVlvInitEndFlg;

uint32 BbsVlvM_Main_Timer_Start;
uint32 BbsVlvM_Main_Timer_Elapsed;

#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "BbsVlvM_MemMap.h"
#define BBSVLVM_MAIN_START_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/** Variable Section (32BIT)**/


#define BBSVLVM_MAIN_STOP_SEC_VAR_32BIT
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBSVLVM_MAIN_START_SEC_CODE
#include "BbsVlvM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void BbsVlvM_Main_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = 0;
    BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C0ol = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C0sb = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C0sg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C1ol = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C1sb = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C1sg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C2ol = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C2sb = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C2sg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.Fr = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.Ot = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.Lr = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.Uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.Ff = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C3ol = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C3sb = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C3sg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C4ol = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C4sb = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C4sg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C5ol = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C5sb = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C5sg = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.Fr = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.Ot = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.Lr = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.Uv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.Ff = 0;
    for(i=0;i<5;i++) BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData[i] = 0;   
    for(i=0;i<5;i++) BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData[i] = 0;   
    BbsVlvM_MainBus.BbsVlvM_MainAchAsicInvalid = 0;
    BbsVlvM_MainBus.BbsVlvM_MainEcuModeSts = 0;
    BbsVlvM_MainBus.BbsVlvM_MainIgnOnOffSts = 0;
    BbsVlvM_MainBus.BbsVlvM_MainIgnEdgeSts = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVBatt1Mon = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVBatt2Mon = 0;
    BbsVlvM_MainBus.BbsVlvM_MainDiagClrSrs = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFspCbsHMon = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFspCbsMon = 0;
    BbsVlvM_MainBus.BbsVlvM_MainVlvdInvalid = 0;
    BbsVlvM_MainBus.BbsVlvM_MainAsicEnDrDrv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainArbVlvDriveState = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvActrInfo.fs_on_sim_time = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cutp_time = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cuts_time = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvActrInfo.fs_on_rlv_time = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cv_time = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsDrv = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsOff = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.Batt1Fuse_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err = 0;
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvInitEndFlg = 0;
}

void BbsVlvM_Main(void)
{
    uint16 i;
    
    BbsVlvM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    BbsVlvM_Main_Read_BbsVlvM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);

    /* Decomposed structure interface */
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_OV(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_CP_UV(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_UV(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_UV);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VPWR_OV(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VPWR_OV);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_PMP_LD_ACT(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_ON(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_ON);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_TURN_OFF(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_VDS_FAULT(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_FS_RVP_FAULT(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_VHD_OV(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV);
    BbsVlvM_Main_Read_BbsVlvM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(&BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT);

    BbsVlvM_Main_Read_BbsVlvM_MainArbFsrBbsDrvInfo(&BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainArbFsrBbsDrvInfo 
     : BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv;
     : BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/
    
    BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF0DcdInfo(&BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainVlvdFF0DcdInfo 
     : BbsVlvM_MainVlvdFF0DcdInfo.C0ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C0sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C0sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C1sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2ol;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2sb;
     : BbsVlvM_MainVlvdFF0DcdInfo.C2sg;
     : BbsVlvM_MainVlvdFF0DcdInfo.Fr;
     : BbsVlvM_MainVlvdFF0DcdInfo.Ot;
     : BbsVlvM_MainVlvdFF0DcdInfo.Lr;
     : BbsVlvM_MainVlvdFF0DcdInfo.Uv;
     : BbsVlvM_MainVlvdFF0DcdInfo.Ff;
     =============================================================================*/
    
    BbsVlvM_Main_Read_BbsVlvM_MainVlvdFF1DcdInfo(&BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainVlvdFF1DcdInfo 
     : BbsVlvM_MainVlvdFF1DcdInfo.C3ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C3sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C3sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C4sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5ol;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5sb;
     : BbsVlvM_MainVlvdFF1DcdInfo.C5sg;
     : BbsVlvM_MainVlvdFF1DcdInfo.Fr;
     : BbsVlvM_MainVlvdFF1DcdInfo.Ot;
     : BbsVlvM_MainVlvdFF1DcdInfo.Lr;
     : BbsVlvM_MainVlvdFF1DcdInfo.Uv;
     : BbsVlvM_MainVlvdFF1DcdInfo.Ff;
     =============================================================================*/
    
    /* Decomposed structure interface */
    BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_PrimCutVlvReqData(&BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.PrimCutVlvReqData);
    BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_SecdCutVlvReqData(&BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.SecdCutVlvReqData);
    BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_SimVlvReqData(&BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.SimVlvReqData);
    BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_RelsVlvReqData(&BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.RelsVlvReqData);
    BbsVlvM_Main_Read_BbsVlvM_MainArbNormVlvReqInfo_CircVlvReqData(&BbsVlvM_MainBus.BbsVlvM_MainArbNormVlvReqInfo.CircVlvReqData);

    BbsVlvM_Main_Read_BbsVlvM_MainAchAsicInvalid(&BbsVlvM_MainBus.BbsVlvM_MainAchAsicInvalid);
    BbsVlvM_Main_Read_BbsVlvM_MainEcuModeSts(&BbsVlvM_MainBus.BbsVlvM_MainEcuModeSts);
    BbsVlvM_Main_Read_BbsVlvM_MainIgnOnOffSts(&BbsVlvM_MainBus.BbsVlvM_MainIgnOnOffSts);
    BbsVlvM_Main_Read_BbsVlvM_MainIgnEdgeSts(&BbsVlvM_MainBus.BbsVlvM_MainIgnEdgeSts);
    BbsVlvM_Main_Read_BbsVlvM_MainVBatt1Mon(&BbsVlvM_MainBus.BbsVlvM_MainVBatt1Mon);
    BbsVlvM_Main_Read_BbsVlvM_MainVBatt2Mon(&BbsVlvM_MainBus.BbsVlvM_MainVBatt2Mon);
    BbsVlvM_Main_Read_BbsVlvM_MainDiagClrSrs(&BbsVlvM_MainBus.BbsVlvM_MainDiagClrSrs);
    BbsVlvM_Main_Read_BbsVlvM_MainFspCbsHMon(&BbsVlvM_MainBus.BbsVlvM_MainFspCbsHMon);
    BbsVlvM_Main_Read_BbsVlvM_MainFspCbsMon(&BbsVlvM_MainBus.BbsVlvM_MainFspCbsMon);
    BbsVlvM_Main_Read_BbsVlvM_MainVlvdInvalid(&BbsVlvM_MainBus.BbsVlvM_MainVlvdInvalid);
    BbsVlvM_Main_Read_BbsVlvM_MainAsicEnDrDrv(&BbsVlvM_MainBus.BbsVlvM_MainAsicEnDrDrv);
    BbsVlvM_Main_Read_BbsVlvM_MainArbVlvDriveState(&BbsVlvM_MainBus.BbsVlvM_MainArbVlvDriveState);

    /* Process */
	BbsVlvM_ProcessMainFunction();

    /* Output */
    BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvActrInfo(&BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvActrInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainFSBbsVlvActrInfo 
     : BbsVlvM_MainFSBbsVlvActrInfo.fs_on_sim_time;
     : BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cutp_time;
     : BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cuts_time;
     : BbsVlvM_MainFSBbsVlvActrInfo.fs_on_rlv_time;
     : BbsVlvM_MainFSBbsVlvActrInfo.fs_on_cv_time;
     =============================================================================*/
    
    BbsVlvM_Main_Write_BbsVlvM_MainFSFsrBbsDrvInfo(&BbsVlvM_MainBus.BbsVlvM_MainFSFsrBbsDrvInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainFSFsrBbsDrvInfo 
     : BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsDrv;
     : BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/
    
    BbsVlvM_Main_Write_BbsVlvM_MainBBSVlvErrInfo(&BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo);
    /*==============================================================================
    * Members of structure BbsVlvM_MainBBSVlvErrInfo 
     : BbsVlvM_MainBBSVlvErrInfo.Batt1Fuse_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err;
     : BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err;
     =============================================================================*/
    
    BbsVlvM_Main_Write_BbsVlvM_MainFSBbsVlvInitEndFlg(&BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvInitEndFlg);

    BbsVlvM_Main_Timer_Elapsed = STM0_TIM0.U - BbsVlvM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define BBSVLVM_MAIN_STOP_SEC_CODE
#include "BbsVlvM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
