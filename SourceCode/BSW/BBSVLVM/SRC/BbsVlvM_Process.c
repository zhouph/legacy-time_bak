/**
 * @defgroup BbsVlvM_Process BbsVlvM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        BbsVlvM_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "BbsVlvM_Process.h"
#include "BbsVlvM_Main.h"
#include "common.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
uint8 BbsVlvM_BATT1Mon;
uint8 BbsVlvM_FSRMon;
uint8 BbsVlvM_Cycletime = 5;
uint8 BbsVlvM_ActuatorCheckInhibitTime;
uint8 BbsVlvM_PreVRDrvReq;

uint8 BbsVlvM_FSR_Err_BATT1FuseOpen_Err;
uint8 BbsVlvM_FSR_Err_VlvRelayCP_Err;
uint8 BbsVlvM_FSR_Err_VlvRelayOpen_Err;
uint8 BbsVlvM_FSR_Err_VlvRelayS2B_Err;
uint8 BbsVlvM_FSR_Err_VlvRelayS2G_Err;
uint8 BbsVlvM_FSR_Err_VlvRelayOverTemp_Err;
uint8 BbsVlvM_FSR_Err_VlvRelayShutdown_Err;

uint8 BbsVlvM_FSR_Err_All_Err;

uint8 BbsVlvM_Solenid_Err_CV_Open_Err;
uint8 BbsVlvM_Solenid_Err_CV_S2B_Err;
uint8 BbsVlvM_Solenid_Err_CV_S2G_Err;
uint8 BbsVlvM_Solenid_Err_CV_OverTemp_Err;
uint8 BbsVlvM_Solenid_Err_CV_CurrReg_Err;
uint8 BbsVlvM_Solenid_Err_CV_PsvOpen_Err;

uint8 BbsVlvM_Solenid_Err_RLV_Open_Err;
uint8 BbsVlvM_Solenid_Err_RLV_S2B_Err;
uint8 BbsVlvM_Solenid_Err_RLV_S2G_Err;
uint8 BbsVlvM_Solenid_Err_RLV_OverTemp_Err;
uint8 BbsVlvM_Solenid_Err_RLV_CurrReg_Err;
uint8 BbsVlvM_Solenid_Err_RLV_PsvOpen_Err;

uint8 BbsVlvM_Solenid_Err_CutP_Open_Err;
uint8 BbsVlvM_Solenid_Err_CutP_S2B_Err;
uint8 BbsVlvM_Solenid_Err_CutP_S2G_Err;
uint8 BbsVlvM_Solenid_Err_CutP_OverTemp_Err;
uint8 BbsVlvM_Solenid_Err_CutP_CurrReg_Err;
uint8 BbsVlvM_Solenid_Err_CutP_PsvOpen_Err;

uint8 BbsVlvM_Solenid_Err_CutS_Open_Err;
uint8 BbsVlvM_Solenid_Err_CutS_S2B_Err;
uint8 BbsVlvM_Solenid_Err_CutS_S2G_Err;
uint8 BbsVlvM_Solenid_Err_CutS_OverTemp_Err;
uint8 BbsVlvM_Solenid_Err_CutS_CurrReg_Err;
uint8 BbsVlvM_Solenid_Err_CutS_PsvOpen_Err;

uint8 BbsVlvM_Solenid_Err_Sim_Open_Err;
uint8 BbsVlvM_Solenid_Err_Sim_S2B_Err;
uint8 BbsVlvM_Solenid_Err_Sim_S2G_Err;
uint8 BbsVlvM_Solenid_Err_Sim_OverTemp_Err;
uint8 BbsVlvM_Solenid_Err_Sim_CurrReg_Err;
uint8 BbsVlvM_Solenid_Err_Sim_PsvOpen_Err;


/*==============================================================================
 *                  GLOBAL FUNCTIONS PROTOTYPES
 =============================================================================*/
void BbsVlvM_ProcessMainFunction(void);

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
void BbsVlvM_PreCondition(void);

void BbsVlvM_VlvRelayHWCheck(void);
void BbsVlvM_VlvRelayBATT1FuseOpenCheck(uint8 inhibit);
void BbsVlvM_VlvRelayCPCheck(uint8 inhibit);
void BbsVlvM_VlvRelayOpenCheck(uint8 inhibit);
void BbsVlvM_VlvRelayShortBATTCheck(uint8 inhibit);
void BbsVlvM_VlvRelayOverTempCheck(uint8 inhibit);
void BbsVlvM_VlvRelayShutdownLineCheck(uint8 inhibit);
void BbsVlvM_VlvRelayAllErrCheck(void);

void BbsVlvM_VlvSolenoidHWCheck(void);
void BbsVlvM_VlvSolenoidOpenCheck(uint8 inhibit);
void BbsVlvM_VlvSolenoidS2BCheck(uint8 inhibit);
void BbsVlvM_VlvSolenoidS2GCheck(uint8 inhibit);
void BbsVlvM_VlvSolenoidOverTempCheck(uint8 inhibit);
void BbsVlvM_VlvSolenoidCurRegCheck(uint8 inhibit);

void BbsVlvM_VlvErrIhbCopy(void);

/*==============================================================================
 *                  GLOBAL/LOCAL FUNCTIONS 
 =============================================================================*/
void BbsVlvM_ProcessMainFunction(void)
{
	BbsVlvM_PreCondition();
	BbsVlvM_VlvRelayHWCheck();
	BbsVlvM_VlvSolenoidHWCheck();
	BbsVlvM_VlvErrIhbCopy();

    /* Temp Valve, FSR On */
    BbsVlvM_MainBus.BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsDrv = STD_ON;
    BbsVlvM_MainBus.BbsVlvM_MainFSFsrBbsDrvInfo.FsrCbsOff = STD_OFF;

    /* Temp Init End Flg On */
    BbsVlvM_MainBus.BbsVlvM_MainFSBbsVlvInitEndFlg = TRUE;
}

void BbsVlvM_PreCondition(void)
{
	if(BbsVlvM_MainBus.BbsVlvM_MainVBatt1Mon >= BBSVLVM_BATT1_ON_DET_VOLT)
	{
		BbsVlvM_BATT1Mon = 1;
	}
	else if(BbsVlvM_MainBus.BbsVlvM_MainVBatt1Mon <= BBSVLVM_BATT1_OFF_DET_VOLT)
	{
		BbsVlvM_BATT1Mon = 0;
	}
	else
	{
		;
	}	

	if(BbsVlvM_MainBus.BbsVlvM_MainFspCbsMon >= BBSVLVM_FSR_ON_DET_VOLT)
	{
		BbsVlvM_FSRMon = 1;
	}
	else if(BbsVlvM_MainBus.BbsVlvM_MainFspCbsMon <= BBSVLVM_FSR_OFF_DET_VOLT)
	{
		BbsVlvM_FSRMon = 0;
	}	
	else
	{
		;
	}

	if(BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_PMP_LD_ACT == 1)
    {
        if(BbsVlvM_Cycletime > 0)
        {
           BbsVlvM_ActuatorCheckInhibitTime = (BBSVLVM_ACT_CHECK_IHB_TIME) / (BbsVlvM_Cycletime);
        }
    }
    else if(BbsVlvM_ActuatorCheckInhibitTime > 0)
    {
        BbsVlvM_ActuatorCheckInhibitTime--;
    }
    else
    {
        ;
    }
  
}

void BbsVlvM_VlvRelayHWCheck(void)
{
	uint8 inhibit = 0;
	
	if(	(BbsVlvM_MainBus.BbsVlvM_MainVBatt1Mon > U16_CALCVOLT_17V) || 
	   	(BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv == 1) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov == 1) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV == 1) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV == 1) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainAchAsicInvalid == 1) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainVlvdInvalid == 1) || 
	   	(BbsVlvM_BATT1Mon == 0) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv != BbsVlvM_PreVRDrvReq) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsOff == 1) ||
	   	(BbsVlvM_ActuatorCheckInhibitTime > 0) )
	{
		inhibit = 1;
	}
	
	BbsVlvM_VlvRelayBATT1FuseOpenCheck(inhibit);
	BbsVlvM_VlvRelayCPCheck(inhibit);
	BbsVlvM_VlvRelayOpenCheck(inhibit);
	BbsVlvM_VlvRelayShortBATTCheck(inhibit);
	BbsVlvM_VlvRelayOverTempCheck(inhibit);

	BbsVlvM_VlvRelayAllErrCheck();

	BbsVlvM_PreVRDrvReq = BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv;
}	

void BbsVlvM_VlvRelayBATT1FuseOpenCheck(uint8 inhibit)
{
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;
	
	BbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_NONE;

	if(BbsVlvM_MainBus.BbsVlvM_MainVBatt2Mon > U16_CALCVOLT_7V)
	{
		if(BbsVlvM_BATT1Mon == 0)
		{
			BbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_PREFAILED;
		}	
		else
		{
			BbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_PREPASSED;
		}	
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_FSR_Err_BATT1FuseOpen_Err = ERR_INHIBIT;
	}	
}

void BbsVlvM_VlvRelayCPCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	if( (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV == 1) ||
	   	(BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV == 1) )
	{
		BbsVlvM_FSR_Err_VlvRelayCP_Err = ERR_PREFAILED;
	}	  
	else
	{
		BbsVlvM_FSR_Err_VlvRelayCP_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		BbsVlvM_FSR_Err_VlvRelayCP_Err = ERR_INHIBIT;
	}	
}

void BbsVlvM_VlvRelayOpenCheck(uint8 inhibit)
{
	uint8 HsdShort = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

    if( (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV == 1) )
         
    {
		HsdShort = 1;
	}    

	if( (BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv == 1) &&
		(BbsVlvM_MainBus.BbsVlvM_MainArbFsrBbsDrvInfo.FsrCbsDrv == BbsVlvM_PreVRDrvReq) &&
		(BbsVlvM_FSRMon == 0) &&
		(HsdShort == 0) )
	{
		BbsVlvM_FSR_Err_VlvRelayOpen_Err = ERR_PREFAILED;
	}	
	else
	{
		BbsVlvM_FSR_Err_VlvRelayOpen_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		BbsVlvM_FSR_Err_VlvRelayOpen_Err = ERR_INHIBIT;
	}	
}	

void BbsVlvM_VlvRelayShortBATTCheck(uint8 inhibit)
{
	uint8 HsdShort = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

    if( (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_TURN_OFF == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_VHD_OV == 1) )
    {
		HsdShort = 1;
	}    

	if(HsdShort == 1)
    {
        BbsVlvM_FSR_Err_VlvRelayS2B_Err = ERR_PREFAILED;
    }
    else
    {
        BbsVlvM_FSR_Err_VlvRelayS2B_Err = ERR_PREPASSED;
    }

	if(temp_inbibit == 1)
	{
		BbsVlvM_FSR_Err_VlvRelayS2B_Err = ERR_INHIBIT;
	}	
}

void BbsVlvM_VlvRelayOverTempCheck(uint8 inhibit)
{
	uint8 HsdOverTemp = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	if(BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT == 1)
	{
		HsdOverTemp = 1;
	}	

	if(HsdOverTemp == 1)
	{
		BbsVlvM_FSR_Err_VlvRelayOverTemp_Err = ERR_PREFAILED;
	}	
	else
	{
		BbsVlvM_FSR_Err_VlvRelayOverTemp_Err = ERR_PREPASSED;
	}	

	if(temp_inbibit == 1)
	{
		BbsVlvM_FSR_Err_VlvRelayOverTemp_Err = ERR_INHIBIT;
	}	
}	

void BbsVlvM_VlvRelayShutdownLineCheck(uint8 inhibit)
{
	uint8 HsdShort = 0;
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;
}

void BbsVlvM_VlvRelayAllErrCheck(void)
{
	if( (BbsVlvM_FSR_Err_BATT1FuseOpen_Err == ERR_PREFAILED) ||
		(BbsVlvM_FSR_Err_VlvRelayCP_Err == ERR_PREFAILED) ||
		(BbsVlvM_FSR_Err_VlvRelayOpen_Err == ERR_PREFAILED) ||
		(BbsVlvM_FSR_Err_VlvRelayS2B_Err == ERR_PREFAILED) ||
		(BbsVlvM_FSR_Err_VlvRelayOverTemp_Err == ERR_PREFAILED) ||
		(BbsVlvM_FSR_Err_VlvRelayShutdown_Err == ERR_PREFAILED) )
	{
		BbsVlvM_FSR_Err_All_Err = 1;
	}	
	else
	{
		BbsVlvM_FSR_Err_All_Err = 0;
	}	
}

void BbsVlvM_VlvSolenoidHWCheck(void)
{
    uint8 inhibit = 0;

    if( (BbsVlvM_FSRMon == 0) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv == 1) ||  
        (BbsVlvM_MainBus.BbsVlvM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_OV == 1) ||
        (BbsVlvM_MainBus.BbsVlvM_MainAchValveAsicInfo.Ach_Asic_CP_UV == 1) ||
        (BbsVlvM_BATT1Mon == 0) ||
        (BbsVlvM_FSR_Err_All_Err == 1) ||
        (BbsVlvM_ActuatorCheckInhibitTime > 0) )
    {
        inhibit = 1;
    }	

	BbsVlvM_VlvSolenoidOpenCheck(inhibit);
	BbsVlvM_VlvSolenoidS2BCheck(inhibit);
	BbsVlvM_VlvSolenoidS2GCheck(inhibit);
	BbsVlvM_VlvSolenoidOverTempCheck(inhibit);
	BbsVlvM_VlvSolenoidCurRegCheck(inhibit);
}	

void BbsVlvM_VlvSolenoidOpenCheck(uint8 inhibit)
{	
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	/* A3944 Ch0 - CV */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C0ol == 1)
	{
		BbsVlvM_Solenid_Err_CV_Open_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CV_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CV_Open_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch1 - RLV */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C1ol == 1)
	{
		BbsVlvM_Solenid_Err_RLV_Open_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_RLV_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_RLV_Open_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch2 - CutS */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C2ol == 1)
	{
		BbsVlvM_Solenid_Err_CutS_Open_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutS_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CutS_Open_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch3 - CutP */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C3ol == 1)
	{
		BbsVlvM_Solenid_Err_CutP_Open_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutP_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CutP_Open_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch4 - Sim */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C4ol == 1)
	{
		BbsVlvM_Solenid_Err_Sim_Open_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_Sim_Open_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_Sim_Open_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch5 -  */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C5ol == 1)
	{
		;
	}
	else
	{
		;
	}	
}

void BbsVlvM_VlvSolenoidS2BCheck(uint8 inhibit)
{
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	/* A3944 Ch0 - CV */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C0sb == 1)
	{
		BbsVlvM_Solenid_Err_CV_S2B_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CV_S2B_Err = ERR_PREPASSED;	
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CV_S2B_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch1 - RLV */ 
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C1sb == 1)
	{
		BbsVlvM_Solenid_Err_RLV_S2B_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_RLV_S2B_Err = ERR_PREPASSED;	
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_RLV_S2B_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch2 - CutS */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C2sb == 1)
	{
		BbsVlvM_Solenid_Err_CutS_S2B_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutS_S2B_Err = ERR_PREPASSED;	
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CutS_S2B_Err = ERR_INHIBIT;
	}	
	
	/* A3944 Ch3 - CutP */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C3sb == 1)
	{
		BbsVlvM_Solenid_Err_CutP_S2B_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutP_S2B_Err = ERR_PREPASSED;	
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CutP_S2B_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch4 - Sim */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C4sb == 1)
	{
		BbsVlvM_Solenid_Err_Sim_S2B_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_Sim_S2B_Err = ERR_PREPASSED;	
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_Sim_S2B_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch5 -  */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C5sb == 1)
	{
		;
	}
	else
	{
		;
	}	
}	

void BbsVlvM_VlvSolenoidS2GCheck(uint8 inhibit)
{
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	/* A3944 Ch0 - CV */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C0sg == 1)
	{
		BbsVlvM_Solenid_Err_CV_S2G_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CV_S2G_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CV_S2G_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch1 - RLV */ 
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C1sg == 1)
	{
		BbsVlvM_Solenid_Err_RLV_S2G_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_RLV_S2G_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_RLV_S2G_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch2 - CutS */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.C2sg == 1)
	{
		BbsVlvM_Solenid_Err_CutS_S2G_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutS_S2G_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CutS_S2G_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch3 - CutP */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C3sg == 1)
	{
		BbsVlvM_Solenid_Err_CutP_S2G_Err = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutP_S2G_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_CutP_S2G_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch4 - Sim */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C4sg == 1)
	{
		 BbsVlvM_Solenid_Err_Sim_S2G_Err = ERR_PREFAILED;
	}
	else
	{
		 BbsVlvM_Solenid_Err_Sim_S2G_Err = ERR_PREPASSED;
	}	
	
	if(temp_inbibit == 1)
	{
		BbsVlvM_Solenid_Err_Sim_S2G_Err = ERR_INHIBIT;
	}	

	/* A3944 Ch5 -  */
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.C5sg == 1)
	{
		;
	}
	else
	{
		;
	}	
}


void BbsVlvM_VlvSolenoidOverTempCheck(uint8 inhibit)
{
	uint8 temp_inbibit = 0;
	temp_inbibit = inhibit;

	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF0DcdInfo.Ot == 1)
	{
		BbsVlvM_Solenid_Err_CV_OverTemp_Err = ERR_PREFAILED;
		BbsVlvM_Solenid_Err_RLV_OverTemp_Err = ERR_PREFAILED; 
		BbsVlvM_Solenid_Err_CutS_OverTemp_Err = ERR_PREFAILED; 
	}
	else
	{
		BbsVlvM_Solenid_Err_CV_OverTemp_Err = ERR_PREPASSED;
		BbsVlvM_Solenid_Err_RLV_OverTemp_Err = ERR_PREPASSED; 
		BbsVlvM_Solenid_Err_CutS_OverTemp_Err = ERR_PREPASSED; 
	}	

	if(temp_inbibit == 1)
	{	
		BbsVlvM_Solenid_Err_CV_OverTemp_Err 	= ERR_INHIBIT;
		BbsVlvM_Solenid_Err_RLV_OverTemp_Err 	= ERR_INHIBIT;
		BbsVlvM_Solenid_Err_CutS_OverTemp_Err 	= ERR_INHIBIT;
	}
	
	if(BbsVlvM_MainBus.BbsVlvM_MainVlvdFF1DcdInfo.Ot == 1)
	{
		BbsVlvM_Solenid_Err_CutP_OverTemp_Err = ERR_PREFAILED;
		BbsVlvM_Solenid_Err_Sim_OverTemp_Err  = ERR_PREFAILED;
	}
	else
	{
		BbsVlvM_Solenid_Err_CutP_OverTemp_Err = ERR_PREPASSED; 
		BbsVlvM_Solenid_Err_Sim_OverTemp_Err  = ERR_PREPASSED; 	
	}	
	
	if(temp_inbibit == 1)
	{	
		BbsVlvM_Solenid_Err_CutP_OverTemp_Err 	= ERR_INHIBIT;
		BbsVlvM_Solenid_Err_Sim_OverTemp_Err 	= ERR_INHIBIT;
	}
}

void BbsVlvM_VlvSolenoidCurRegCheck(uint8 inhibit)
{
	;
}

void BbsVlvM_VlvErrIhbCopy(void)
{
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.Batt1Fuse_Open_Err		= BbsVlvM_FSR_Err_BATT1FuseOpen_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err 		= BbsVlvM_FSR_Err_VlvRelayOpen_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err  		= BbsVlvM_FSR_Err_VlvRelayS2G_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err  		= BbsVlvM_FSR_Err_VlvRelayS2B_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err 	= BbsVlvM_FSR_Err_VlvRelayOverTemp_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err = BbsVlvM_FSR_Err_VlvRelayShutdown_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err 		= BbsVlvM_FSR_Err_VlvRelayCP_Err;
	
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err 		= BbsVlvM_Solenid_Err_Sim_Open_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err 	= BbsVlvM_Solenid_Err_Sim_PsvOpen_Err; 
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err  		= BbsVlvM_Solenid_Err_Sim_S2G_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err  	= BbsVlvM_Solenid_Err_Sim_S2B_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err 	= BbsVlvM_Solenid_Err_Sim_CurrReg_Err;
	
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err 		= BbsVlvM_Solenid_Err_CutP_Open_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err 	= BbsVlvM_Solenid_Err_CutP_PsvOpen_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err 		= BbsVlvM_Solenid_Err_CutP_S2G_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err 	= BbsVlvM_Solenid_Err_CutP_S2B_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err 	= BbsVlvM_Solenid_Err_CutP_CurrReg_Err;
	
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err 		= BbsVlvM_Solenid_Err_CutS_Open_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err 	= BbsVlvM_Solenid_Err_CutS_PsvOpen_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err 		= BbsVlvM_Solenid_Err_CutS_S2G_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err 	= BbsVlvM_Solenid_Err_CutS_S2B_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err 	= BbsVlvM_Solenid_Err_CutS_CurrReg_Err;
	
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err 	= BbsVlvM_Solenid_Err_CV_Open_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err= BbsVlvM_Solenid_Err_CV_PsvOpen_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err 	= BbsVlvM_Solenid_Err_CV_S2G_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err 	= BbsVlvM_Solenid_Err_CV_S2B_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err = BbsVlvM_Solenid_Err_CV_CurrReg_Err;
	
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err 		= BbsVlvM_Solenid_Err_RLV_Open_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err 	= BbsVlvM_Solenid_Err_RLV_PsvOpen_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err  		= BbsVlvM_Solenid_Err_RLV_S2G_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err 		= BbsVlvM_Solenid_Err_RLV_S2B_Err;
	BbsVlvM_MainBus.BbsVlvM_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err = BbsVlvM_Solenid_Err_RLV_CurrReg_Err;
}	
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
