Watchdog_MainEcuModeSts = Simulink.Bus;
DeList={'Watchdog_MainEcuModeSts'};
Watchdog_MainEcuModeSts = CreateBus(Watchdog_MainEcuModeSts, DeList);
clear DeList;

Watchdog_MainIgnOnOffSts = Simulink.Bus;
DeList={'Watchdog_MainIgnOnOffSts'};
Watchdog_MainIgnOnOffSts = CreateBus(Watchdog_MainIgnOnOffSts, DeList);
clear DeList;

Watchdog_MainIgnEdgeSts = Simulink.Bus;
DeList={'Watchdog_MainIgnEdgeSts'};
Watchdog_MainIgnEdgeSts = CreateBus(Watchdog_MainIgnEdgeSts, DeList);
clear DeList;

Watchdog_MainDiagClrSrs = Simulink.Bus;
DeList={'Watchdog_MainDiagClrSrs'};
Watchdog_MainDiagClrSrs = CreateBus(Watchdog_MainDiagClrSrs, DeList);
clear DeList;

