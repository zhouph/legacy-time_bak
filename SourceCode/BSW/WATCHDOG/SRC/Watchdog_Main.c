/**
 * @defgroup Watchdog_Main Watchdog_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Watchdog_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Watchdog_Main.h"
#include "Watchdog_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WATCHDOG_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Watchdog_Main_HdrBusType Watchdog_MainBus;

/* Version Info */
const SwcVersionInfo_t Watchdog_MainVersionInfo = 
{   
    WATCHDOG_MAIN_MODULE_ID,           /* Watchdog_MainVersionInfo.ModuleId */
    WATCHDOG_MAIN_MAJOR_VERSION,       /* Watchdog_MainVersionInfo.MajorVer */
    WATCHDOG_MAIN_MINOR_VERSION,       /* Watchdog_MainVersionInfo.MinorVer */
    WATCHDOG_MAIN_PATCH_VERSION,       /* Watchdog_MainVersionInfo.PatchVer */
    WATCHDOG_MAIN_BRANCH_VERSION       /* Watchdog_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Watchdog_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Watchdog_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Watchdog_MainIgnEdgeSts;
Diag_HndlrDiagClr_t Watchdog_MainDiagClrSrs;

/* Output Data Element */

uint32 Watchdog_Main_Timer_Start;
uint32 Watchdog_Main_Timer_Elapsed;

#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_CODE
#include "Watchdog_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Watchdog_Main_Init(void)
{
    /* Initialize internal bus */
    Watchdog_MainBus.Watchdog_MainEcuModeSts = 0;
    Watchdog_MainBus.Watchdog_MainIgnOnOffSts = 0;
    Watchdog_MainBus.Watchdog_MainIgnEdgeSts = 0;
    Watchdog_MainBus.Watchdog_MainDiagClrSrs = 0;
}

void Watchdog_Main(void)
{
    Watchdog_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    Watchdog_Main_Read_Watchdog_MainEcuModeSts(&Watchdog_MainBus.Watchdog_MainEcuModeSts);
    Watchdog_Main_Read_Watchdog_MainIgnOnOffSts(&Watchdog_MainBus.Watchdog_MainIgnOnOffSts);
    Watchdog_Main_Read_Watchdog_MainIgnEdgeSts(&Watchdog_MainBus.Watchdog_MainIgnEdgeSts);
    Watchdog_Main_Read_Watchdog_MainDiagClrSrs(&Watchdog_MainBus.Watchdog_MainDiagClrSrs);

    /* Process */
	Wdg_lTrigger();
    /* Output */

    Watchdog_Main_Timer_Elapsed = STM0_TIM0.U - Watchdog_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WATCHDOG_MAIN_STOP_SEC_CODE
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
