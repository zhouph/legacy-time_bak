# \file
#
# \brief Watchdog
#
# This file contains the implementation of the SWC
# module Watchdog.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Watchdog_CORE_PATH     := $(MANDO_BSW_ROOT)\Watchdog
Watchdog_CAL_PATH      := $(Watchdog_CORE_PATH)\CAL\$(Watchdog_VARIANT)
Watchdog_SRC_PATH      := $(Watchdog_CORE_PATH)\SRC
Watchdog_CFG_PATH      := $(Watchdog_CORE_PATH)\CFG\$(Watchdog_VARIANT)
Watchdog_HDR_PATH      := $(Watchdog_CORE_PATH)\HDR
Watchdog_IFA_PATH      := $(Watchdog_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Watchdog_CMN_PATH      := $(Watchdog_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Watchdog_UT_PATH		:= $(Watchdog_CORE_PATH)\UT
	Watchdog_UNITY_PATH	:= $(Watchdog_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Watchdog_UT_PATH)
	CC_INCLUDE_PATH		+= $(Watchdog_UNITY_PATH)
	Watchdog_Main_PATH 	:= Watchdog_UT_PATH\Watchdog_Main
endif
CC_INCLUDE_PATH    += $(Watchdog_CAL_PATH)
CC_INCLUDE_PATH    += $(Watchdog_SRC_PATH)
CC_INCLUDE_PATH    += $(Watchdog_CFG_PATH)
CC_INCLUDE_PATH    += $(Watchdog_HDR_PATH)
CC_INCLUDE_PATH    += $(Watchdog_IFA_PATH)
CC_INCLUDE_PATH    += $(Watchdog_CMN_PATH)

