# \file
#
# \brief Watchdog
#
# This file contains the implementation of the SWC
# module Watchdog.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Watchdog_src

Watchdog_src_FILES        += $(Watchdog_SRC_PATH)\Watchdog_Main.c
Watchdog_src_FILES        += $(Watchdog_IFA_PATH)\Watchdog_Main_Ifa.c
Watchdog_src_FILES        += $(Watchdog_CFG_PATH)\Watchdog_Cfg.c
Watchdog_src_FILES        += $(Watchdog_CAL_PATH)\Watchdog_Cal.c

ifeq ($(ICE_COMPILE),true)
Watchdog_src_FILES        += $(Watchdog_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Watchdog_src_FILES        += $(Watchdog_UNITY_PATH)\unity.c
	Watchdog_src_FILES        += $(Watchdog_UNITY_PATH)\unity_fixture.c	
	Watchdog_src_FILES        += $(Watchdog_UT_PATH)\main.c
	Watchdog_src_FILES        += $(Watchdog_UT_PATH)\Watchdog_Main\Watchdog_Main_UtMain.c
endif