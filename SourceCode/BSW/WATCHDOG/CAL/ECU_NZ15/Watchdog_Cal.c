/**
 * @defgroup Watchdog_Cal Watchdog_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Watchdog_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Watchdog_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WATCHDOG_START_SEC_CALIB_UNSPECIFIED
#include "Watchdog_MemMap.h"
/* Global Calibration Section */


#define WATCHDOG_STOP_SEC_CALIB_UNSPECIFIED
#include "Watchdog_MemMap.h"

#define WATCHDOG_START_SEC_CONST_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WATCHDOG_STOP_SEC_CONST_UNSPECIFIED
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WATCHDOG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WATCHDOG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_START_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WATCHDOG_STOP_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
#define WATCHDOG_START_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WATCHDOG_STOP_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_START_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (32BIT)**/


#define WATCHDOG_STOP_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WATCHDOG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WATCHDOG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_START_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WATCHDOG_STOP_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
#define WATCHDOG_START_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WATCHDOG_STOP_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_START_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (32BIT)**/


#define WATCHDOG_STOP_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WATCHDOG_START_SEC_CODE
#include "Watchdog_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WATCHDOG_STOP_SEC_CODE
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
