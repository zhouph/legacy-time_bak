/**
 * @defgroup Watchdog_Main_Ifa Watchdog_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Watchdog_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WATCHDOG_MAIN_IFA_H_
#define WATCHDOG_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Watchdog_Main_Read_Watchdog_MainEcuModeSts(data) do \
{ \
    *data = Watchdog_MainEcuModeSts; \
}while(0);

#define Watchdog_Main_Read_Watchdog_MainIgnOnOffSts(data) do \
{ \
    *data = Watchdog_MainIgnOnOffSts; \
}while(0);

#define Watchdog_Main_Read_Watchdog_MainIgnEdgeSts(data) do \
{ \
    *data = Watchdog_MainIgnEdgeSts; \
}while(0);

#define Watchdog_Main_Read_Watchdog_MainDiagClrSrs(data) do \
{ \
    *data = Watchdog_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WATCHDOG_MAIN_IFA_H_ */
/** @} */
