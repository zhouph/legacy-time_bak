/**
 * @defgroup Watchdog_Main_Ifa Watchdog_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Watchdog_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Watchdog_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WATCHDOG_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Watchdog_MemMap.h"
#define WATCHDOG_MAIN_START_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/** Variable Section (32BIT)**/


#define WATCHDOG_MAIN_STOP_SEC_VAR_32BIT
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WATCHDOG_MAIN_START_SEC_CODE
#include "Watchdog_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WATCHDOG_MAIN_STOP_SEC_CODE
#include "Watchdog_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
