/**
 * @defgroup Watchdog_Main Watchdog_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Watchdog_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WATCHDOG_MAIN_H_
#define WATCHDOG_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Watchdog_Types.h"
#include "Watchdog_Cfg.h"
#include "Watchdog_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define WATCHDOG_MAIN_MODULE_ID      (0)
 #define WATCHDOG_MAIN_MAJOR_VERSION  (2)
 #define WATCHDOG_MAIN_MINOR_VERSION  (0)
 #define WATCHDOG_MAIN_PATCH_VERSION  (0)
 #define WATCHDOG_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Watchdog_Main_HdrBusType Watchdog_MainBus;

/* Version Info */
extern const SwcVersionInfo_t Watchdog_MainVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Watchdog_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Watchdog_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Watchdog_MainIgnEdgeSts;
extern Diag_HndlrDiagClr_t Watchdog_MainDiagClrSrs;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Watchdog_Main_Init(void);
extern void Watchdog_Main(void);
extern void Wdg_lTrigger(void);
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WATCHDOG_MAIN_H_ */
/** @} */
