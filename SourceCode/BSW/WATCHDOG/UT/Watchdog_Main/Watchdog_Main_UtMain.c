#include "unity.h"
#include "unity_fixture.h"
#include "Watchdog_Main.h"
#include "Watchdog_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Watchdog_MainEcuModeSts[MAX_STEP] = WATCHDOG_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Watchdog_MainIgnOnOffSts[MAX_STEP] = WATCHDOG_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_Watchdog_MainIgnEdgeSts[MAX_STEP] = WATCHDOG_MAINIGNEDGESTS;
const Diag_HndlrDiagClr_t UtInput_Watchdog_MainDiagClrSrs[MAX_STEP] = WATCHDOG_MAINDIAGCLRSRS;




TEST_GROUP(Watchdog_Main);
TEST_SETUP(Watchdog_Main)
{
    Watchdog_Main_Init();
}

TEST_TEAR_DOWN(Watchdog_Main)
{   /* Postcondition */

}

TEST(Watchdog_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Watchdog_MainEcuModeSts = UtInput_Watchdog_MainEcuModeSts[i];
        Watchdog_MainIgnOnOffSts = UtInput_Watchdog_MainIgnOnOffSts[i];
        Watchdog_MainIgnEdgeSts = UtInput_Watchdog_MainIgnEdgeSts[i];
        Watchdog_MainDiagClrSrs = UtInput_Watchdog_MainDiagClrSrs[i];

        Watchdog_Main();

    }
}

TEST_GROUP_RUNNER(Watchdog_Main)
{
    RUN_TEST_CASE(Watchdog_Main, All);
}
