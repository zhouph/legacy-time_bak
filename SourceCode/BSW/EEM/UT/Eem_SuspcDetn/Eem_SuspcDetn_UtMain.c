#include "unity.h"
#include "unity_fixture.h"
#include "Eem_SuspcDetn.h"
#include "Eem_SuspcDetn_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Eem_SuspcDetnEcuModeSts[MAX_STEP] = EEM_SUSPCDETNECUMODESTS;
const Diag_HndlrDiagClr_t UtInput_Eem_SuspcDetnDiagClrSrs[MAX_STEP] = EEM_SUSPCDETNDIAGCLRSRS;

const Salsint32 UtExpected_Eem_SuspcDetnSwtStsFSInfo_FlexBrkSwtFaultDet[MAX_STEP] = EEM_SUSPCDETNSWTSTSFSINFO_FLEXBRKSWTFAULTDET;
const Salsint32 UtExpected_Eem_SuspcDetnCanTimeOutStInfo_MaiCanSusDet[MAX_STEP] = EEM_SUSPCDETNCANTIMEOUTSTINFO_MAICANSUSDET;
const Salsint32 UtExpected_Eem_SuspcDetnCanTimeOutStInfo_EmsTiOutSusDet[MAX_STEP] = EEM_SUSPCDETNCANTIMEOUTSTINFO_EMSTIOUTSUSDET;
const Salsint32 UtExpected_Eem_SuspcDetnCanTimeOutStInfo_TcuTiOutSusDet[MAX_STEP] = EEM_SUSPCDETNCANTIMEOUTSTINFO_TCUTIOUTSUSDET;
const Eem_SuspcDetnFuncInhibitVlvSts_t UtExpected_Eem_SuspcDetnFuncInhibitVlvSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITVLVSTS;
const Eem_SuspcDetnFuncInhibitIocSts_t UtExpected_Eem_SuspcDetnFuncInhibitIocSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITIOCSTS;
const Eem_SuspcDetnFuncInhibitGdSts_t UtExpected_Eem_SuspcDetnFuncInhibitGdSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITGDSTS;
const Eem_SuspcDetnFuncInhibitProxySts_t UtExpected_Eem_SuspcDetnFuncInhibitProxySts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITPROXYSTS;
const Eem_SuspcDetnFuncInhibitDiagSts_t UtExpected_Eem_SuspcDetnFuncInhibitDiagSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITDIAGSTS;
const Eem_SuspcDetnFuncInhibitFsrSts_t UtExpected_Eem_SuspcDetnFuncInhibitFsrSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITFSRSTS;
const Eem_SuspcDetnFuncInhibitAcmioSts_t UtExpected_Eem_SuspcDetnFuncInhibitAcmioSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITACMIOSTS;
const Eem_SuspcDetnFuncInhibitAcmctlSts_t UtExpected_Eem_SuspcDetnFuncInhibitAcmctlSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITACMCTLSTS;
const Eem_SuspcDetnFuncInhibitMspSts_t UtExpected_Eem_SuspcDetnFuncInhibitMspSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITMSPSTS;
const Eem_SuspcDetnFuncInhibitNvmSts_t UtExpected_Eem_SuspcDetnFuncInhibitNvmSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITNVMSTS;
const Eem_SuspcDetnFuncInhibitWssSts_t UtExpected_Eem_SuspcDetnFuncInhibitWssSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITWSSSTS;
const Eem_SuspcDetnFuncInhibitPedalSts_t UtExpected_Eem_SuspcDetnFuncInhibitPedalSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITPEDALSTS;
const Eem_SuspcDetnFuncInhibitPressSts_t UtExpected_Eem_SuspcDetnFuncInhibitPressSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITPRESSSTS;
const Eem_SuspcDetnFuncInhibitRlySts_t UtExpected_Eem_SuspcDetnFuncInhibitRlySts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITRLYSTS;
const Eem_SuspcDetnFuncInhibitSesSts_t UtExpected_Eem_SuspcDetnFuncInhibitSesSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITSESSTS;
const Eem_SuspcDetnFuncInhibitSwtSts_t UtExpected_Eem_SuspcDetnFuncInhibitSwtSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITSWTSTS;
const Eem_SuspcDetnFuncInhibitPrlySts_t UtExpected_Eem_SuspcDetnFuncInhibitPrlySts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITPRLYSTS;
const Eem_SuspcDetnFuncInhibitMomSts_t UtExpected_Eem_SuspcDetnFuncInhibitMomSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITMOMSTS;
const Eem_SuspcDetnFuncInhibitVlvdSts_t UtExpected_Eem_SuspcDetnFuncInhibitVlvdSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITVLVDSTS;
const Eem_SuspcDetnFuncInhibitSpimSts_t UtExpected_Eem_SuspcDetnFuncInhibitSpimSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITSPIMSTS;
const Eem_SuspcDetnFuncInhibitAdcifSts_t UtExpected_Eem_SuspcDetnFuncInhibitAdcifSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITADCIFSTS;
const Eem_SuspcDetnFuncInhibitIcuSts_t UtExpected_Eem_SuspcDetnFuncInhibitIcuSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITICUSTS;
const Eem_SuspcDetnFuncInhibitMpsSts_t UtExpected_Eem_SuspcDetnFuncInhibitMpsSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITMPSSTS;
const Eem_SuspcDetnFuncInhibitRegSts_t UtExpected_Eem_SuspcDetnFuncInhibitRegSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITREGSTS;
const Eem_SuspcDetnFuncInhibitAsicSts_t UtExpected_Eem_SuspcDetnFuncInhibitAsicSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITASICSTS;
const Eem_SuspcDetnFuncInhibitCanTrcvSts_t UtExpected_Eem_SuspcDetnFuncInhibitCanTrcvSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITCANTRCVSTS;
const Eem_SuspcDetnFuncInhibitBbcSts_t UtExpected_Eem_SuspcDetnFuncInhibitBbcSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITBBCSTS;
const Eem_SuspcDetnFuncInhibitRbcSts_t UtExpected_Eem_SuspcDetnFuncInhibitRbcSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITRBCSTS;
const Eem_SuspcDetnFuncInhibitArbiSts_t UtExpected_Eem_SuspcDetnFuncInhibitArbiSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITARBISTS;
const Eem_SuspcDetnFuncInhibitPctrlSts_t UtExpected_Eem_SuspcDetnFuncInhibitPctrlSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITPCTRLSTS;
const Eem_SuspcDetnFuncInhibitMccSts_t UtExpected_Eem_SuspcDetnFuncInhibitMccSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITMCCSTS;
const Eem_SuspcDetnFuncInhibitVlvActSts_t UtExpected_Eem_SuspcDetnFuncInhibitVlvActSts[MAX_STEP] = EEM_SUSPCDETNFUNCINHIBITVLVACTSTS;



TEST_GROUP(Eem_SuspcDetn);
TEST_SETUP(Eem_SuspcDetn)
{
    Eem_SuspcDetn_Init();
}

TEST_TEAR_DOWN(Eem_SuspcDetn)
{   /* Postcondition */

}

TEST(Eem_SuspcDetn, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Eem_SuspcDetnEcuModeSts = UtInput_Eem_SuspcDetnEcuModeSts[i];
        Eem_SuspcDetnDiagClrSrs = UtInput_Eem_SuspcDetnDiagClrSrs[i];

        Eem_SuspcDetn();

        TEST_ASSERT_EQUAL(Eem_SuspcDetnSwtStsFSInfo.FlexBrkSwtFaultDet, UtExpected_Eem_SuspcDetnSwtStsFSInfo_FlexBrkSwtFaultDet[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet, UtExpected_Eem_SuspcDetnCanTimeOutStInfo_MaiCanSusDet[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet, UtExpected_Eem_SuspcDetnCanTimeOutStInfo_EmsTiOutSusDet[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet, UtExpected_Eem_SuspcDetnCanTimeOutStInfo_TcuTiOutSusDet[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitVlvSts, UtExpected_Eem_SuspcDetnFuncInhibitVlvSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitIocSts, UtExpected_Eem_SuspcDetnFuncInhibitIocSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitGdSts, UtExpected_Eem_SuspcDetnFuncInhibitGdSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitProxySts, UtExpected_Eem_SuspcDetnFuncInhibitProxySts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitDiagSts, UtExpected_Eem_SuspcDetnFuncInhibitDiagSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitFsrSts, UtExpected_Eem_SuspcDetnFuncInhibitFsrSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitAcmioSts, UtExpected_Eem_SuspcDetnFuncInhibitAcmioSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitAcmctlSts, UtExpected_Eem_SuspcDetnFuncInhibitAcmctlSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitMspSts, UtExpected_Eem_SuspcDetnFuncInhibitMspSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitNvmSts, UtExpected_Eem_SuspcDetnFuncInhibitNvmSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitWssSts, UtExpected_Eem_SuspcDetnFuncInhibitWssSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitPedalSts, UtExpected_Eem_SuspcDetnFuncInhibitPedalSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitPressSts, UtExpected_Eem_SuspcDetnFuncInhibitPressSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitRlySts, UtExpected_Eem_SuspcDetnFuncInhibitRlySts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitSesSts, UtExpected_Eem_SuspcDetnFuncInhibitSesSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitSwtSts, UtExpected_Eem_SuspcDetnFuncInhibitSwtSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitPrlySts, UtExpected_Eem_SuspcDetnFuncInhibitPrlySts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitMomSts, UtExpected_Eem_SuspcDetnFuncInhibitMomSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitVlvdSts, UtExpected_Eem_SuspcDetnFuncInhibitVlvdSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitSpimSts, UtExpected_Eem_SuspcDetnFuncInhibitSpimSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitAdcifSts, UtExpected_Eem_SuspcDetnFuncInhibitAdcifSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitIcuSts, UtExpected_Eem_SuspcDetnFuncInhibitIcuSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitMpsSts, UtExpected_Eem_SuspcDetnFuncInhibitMpsSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitRegSts, UtExpected_Eem_SuspcDetnFuncInhibitRegSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitAsicSts, UtExpected_Eem_SuspcDetnFuncInhibitAsicSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitCanTrcvSts, UtExpected_Eem_SuspcDetnFuncInhibitCanTrcvSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitBbcSts, UtExpected_Eem_SuspcDetnFuncInhibitBbcSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitRbcSts, UtExpected_Eem_SuspcDetnFuncInhibitRbcSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitArbiSts, UtExpected_Eem_SuspcDetnFuncInhibitArbiSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitPctrlSts, UtExpected_Eem_SuspcDetnFuncInhibitPctrlSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitMccSts, UtExpected_Eem_SuspcDetnFuncInhibitMccSts[i]);
        TEST_ASSERT_EQUAL(Eem_SuspcDetnFuncInhibitVlvActSts, UtExpected_Eem_SuspcDetnFuncInhibitVlvActSts[i]);
    }
}

TEST_GROUP_RUNNER(Eem_SuspcDetn)
{
    RUN_TEST_CASE(Eem_SuspcDetn, All);
}
