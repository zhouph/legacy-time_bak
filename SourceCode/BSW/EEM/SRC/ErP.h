/*
 * ErP.h
 *
 *  Created on: 2015. 1. 26.
 *      Author: jinsu.park
 */

#ifndef ERP_H_
#define ERP_H_

#include "ErP_Types.h"


#define TIME_OUT_100MS 10
extern uint8    fu8mec;
extern void ErP_Init(void);
//extern void Diag_5ms_Mainfunction(void);
extern void FE_vWriteErrorEEPROM(void);
extern void FE_vArrangeErrorCode(UINT u16MandoDtc);
extern void Diag_5ms_FaultDetect(void);
extern void Diag_5ms_ErrorHandle(void);
extern uint8            number_of_error;
#endif /* ERP_H_ */
