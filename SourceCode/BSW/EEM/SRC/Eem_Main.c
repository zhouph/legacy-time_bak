/**
 * @defgroup Eem_Main Eem_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Main.h"
#include "Eem_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define EEM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Eem_Main_HdrBusType Eem_MainBus;

/* Version Info */
const SwcVersionInfo_t Eem_MainVersionInfo = 
{   
    EEM_MAIN_MODULE_ID,           /* Eem_MainVersionInfo.ModuleId */
    EEM_MAIN_MAJOR_VERSION,       /* Eem_MainVersionInfo.MajorVer */
    EEM_MAIN_MINOR_VERSION,       /* Eem_MainVersionInfo.MinorVer */
    EEM_MAIN_PATCH_VERSION,       /* Eem_MainVersionInfo.PatchVer */
    EEM_MAIN_BRANCH_VERSION       /* Eem_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
BbsVlvM_MainBBSVlvErrInfo_t Eem_MainBBSVlvErrInfo;
AbsVlvM_MainAbsVlvMData_t Eem_MainAbsVlvMData;
Swt_SenEscSwtStInfo_t Eem_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t Eem_MainWhlSpdInfo;
Abc_CtrlAbsCtrlInfo_t Eem_MainAbsCtrlInfo;
Abc_CtrlEscCtrlInfo_t Eem_MainEscCtrlInfo;
Abc_CtrlHdcCtrlInfo_t Eem_MainHdcCtrlInfo;
Abc_CtrlTcsCtrlInfo_t Eem_MainTcsCtrlInfo;
RlyM_MainRelayMonitor_t Eem_MainRelayMonitorData;
SenPwrM_MainSenPwrMonitor_t Eem_MainSenPwrMonitorData;
PedalM_MainPedalMData_t Eem_MainPedalMData;
PressM_MainPresMonFaultInfo_t Eem_MainPresMonFaultInfo;
EcuHwCtrlM_MainAsicMonFaultInfo_t Eem_MainAsicMonFaultInfo;
SwtM_MainSwtMonFaultInfo_t Eem_MainSwtMonFaultInfo;
SwtP_MainSwtPFaultInfo_t Eem_MainSwtPFaultInfo;
YawM_MainYAWMOutInfo_t Eem_MainYAWMOutInfo;
AyM_MainAYMOutInfo_t Eem_MainAYMOuitInfo;
AxM_MainAXMOutInfo_t Eem_MainAXMOutInfo;
SasM_MainSASMOutInfo_t Eem_MainSASMOutInfo;
MtrM_MainMTRMOutInfo_t Eem_MainMTRMOutInfo;
SysPwrM_MainSysPwrMonData_t Eem_MainSysPwrMonData;
WssM_MainWssMonData_t Eem_MainWssMonData;
CanM_MainCanMonData_t Eem_MainCanMonData;
MtrM_MainMgdErrInfo_t Eem_MainMgdErrInfo;
Mps_TLE5012M_MainMpsD1ErrInfo_t Eem_MainMpsD1ErrInfo;
Mps_TLE5012M_MainMpsD2ErrInfo_t Eem_MainMpsD2ErrInfo;
YawP_MainYawPlauOutput_t Eem_MainYawPlauOutput;
AxP_MainAxPlauOutput_t Eem_MainAxPlauOutput;
AyP_MainAyPlauOutput_t Eem_MainAyPlauOutput;
SasP_MainSasPlauOutput_t Eem_MainSasPlauOutput;
PressP_MainPressPInfo_t Eem_MainPressPInfo;
Mom_HndlrEcuModeSts_t Eem_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Eem_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Eem_MainIgnEdgeSts;
Diag_HndlrDiagClr_t Eem_MainDiagClrSrs;
Diag_HndlrDiagSci_t Eem_MainDiagSci;
Diag_HndlrDiagAhbSci_t Eem_MainDiagAhbSci;

/* Output Data Element */
Eem_MainEemFailData_t Eem_MainEemFailData;
Eem_MainEemCtrlInhibitData_t Eem_MainEemCtrlInhibitData;
Eem_MainEemSuspectData_t Eem_MainEemSuspectData;
Eem_MainEemEceData_t Eem_MainEemEceData;
Eem_MainEemLampData_t Eem_MainEemLampData;

uint32 Eem_Main_Timer_Start;
uint32 Eem_Main_Timer_Elapsed;

#define EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define EEM_MAIN_START_SEC_CODE
#include "Eem_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Eem_Main_Init(void)
{
    /* Initialize internal bus */
    Eem_MainBus.Eem_MainBBSVlvErrInfo.Batt1Fuse_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err = 0;
    Eem_MainBus.Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err = 0;
    Eem_MainBus.Eem_MainEscSwtStInfo.EscDisabledBySwt = 0;
    Eem_MainBus.Eem_MainEscSwtStInfo.TcsDisabledBySwt = 0;
    Eem_MainBus.Eem_MainEscSwtStInfo.HdcEnabledBySwt = 0;
    Eem_MainBus.Eem_MainWhlSpdInfo.FlWhlSpd = 0;
    Eem_MainBus.Eem_MainWhlSpdInfo.FrWhlSpd = 0;
    Eem_MainBus.Eem_MainWhlSpdInfo.RlWhlSpd = 0;
    Eem_MainBus.Eem_MainWhlSpdInfo.RrlWhlSpd = 0;
    Eem_MainBus.Eem_MainAbsCtrlInfo.AbsActFlg = 0;
    Eem_MainBus.Eem_MainEscCtrlInfo.EscActFlg = 0;
    Eem_MainBus.Eem_MainHdcCtrlInfo.HdcActFlg = 0;
    Eem_MainBus.Eem_MainTcsCtrlInfo.TcsActFlg = 0;
    Eem_MainBus.Eem_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = 0;
    Eem_MainBus.Eem_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = 0;
    Eem_MainBus.Eem_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = 0;
    Eem_MainBus.Eem_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = 0;
    Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Err = 0;
    Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = 0;
    Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_PTS1_Open_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_PTS1_Short_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_PTS2_Open_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_PTS2_Short_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_PTS1_SupplyPower_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_PTS2_SupplyPower_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_VDD3_OverVolt_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_VDD3_UnderVolt_Err = 0;
    Eem_MainBus.Eem_MainPedalMData.PedalM_VDD3_OverCurrent_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_FC_Fault_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_FC_CRC_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_SC_Temp_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_SC_CRC_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP1_SC_Diag_Info = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_FC_Fault_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_FC_CRC_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_SC_Temp_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_SC_CRC_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.CIRP2_SC_Diag_Info = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_FC_Fault_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_FC_CRC_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_SC_Temp_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_SC_CRC_Err = 0;
    Eem_MainBus.Eem_MainPresMonFaultInfo.SIMP_SC_Diag_Info = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicComm_Err = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = 0;
    Eem_MainBus.Eem_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = 0;
    Eem_MainBus.Eem_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = 0;
    Eem_MainBus.Eem_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = 0;
    Eem_MainBus.Eem_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = 0;
    Eem_MainBus.Eem_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = 0;
    Eem_MainBus.Eem_MainSwtMonFaultInfo.SWM_PB_Sw_Err = 0;
    Eem_MainBus.Eem_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = 0;
    Eem_MainBus.Eem_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = 0;
    Eem_MainBus.Eem_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = 0;
    Eem_MainBus.Eem_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = 0;
    Eem_MainBus.Eem_MainYAWMOutInfo.YAWM_Timeout_Err = 0;
    Eem_MainBus.Eem_MainYAWMOutInfo.YAWM_Invalid_Err = 0;
    Eem_MainBus.Eem_MainYAWMOutInfo.YAWM_CRC_Err = 0;
    Eem_MainBus.Eem_MainYAWMOutInfo.YAWM_Rolling_Err = 0;
    Eem_MainBus.Eem_MainYAWMOutInfo.YAWM_Temperature_Err = 0;
    Eem_MainBus.Eem_MainYAWMOutInfo.YAWM_Range_Err = 0;
    Eem_MainBus.Eem_MainAYMOuitInfo.AYM_Timeout_Err = 0;
    Eem_MainBus.Eem_MainAYMOuitInfo.AYM_Invalid_Err = 0;
    Eem_MainBus.Eem_MainAYMOuitInfo.AYM_CRC_Err = 0;
    Eem_MainBus.Eem_MainAYMOuitInfo.AYM_Rolling_Err = 0;
    Eem_MainBus.Eem_MainAYMOuitInfo.AYM_Temperature_Err = 0;
    Eem_MainBus.Eem_MainAYMOuitInfo.AYM_Range_Err = 0;
    Eem_MainBus.Eem_MainAXMOutInfo.AXM_Timeout_Err = 0;
    Eem_MainBus.Eem_MainAXMOutInfo.AXM_Invalid_Err = 0;
    Eem_MainBus.Eem_MainAXMOutInfo.AXM_CRC_Err = 0;
    Eem_MainBus.Eem_MainAXMOutInfo.AXM_Rolling_Err = 0;
    Eem_MainBus.Eem_MainAXMOutInfo.AXM_Temperature_Err = 0;
    Eem_MainBus.Eem_MainAXMOutInfo.AXM_Range_Err = 0;
    Eem_MainBus.Eem_MainSASMOutInfo.SASM_Timeout_Err = 0;
    Eem_MainBus.Eem_MainSASMOutInfo.SASM_Invalid_Err = 0;
    Eem_MainBus.Eem_MainSASMOutInfo.SASM_CRC_Err = 0;
    Eem_MainBus.Eem_MainSASMOutInfo.SASM_Rolling_Err = 0;
    Eem_MainBus.Eem_MainSASMOutInfo.SASM_Range_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Power_Open_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Open_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Calibration_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Short_Err = 0;
    Eem_MainBus.Eem_MainMTRMOutInfo.MTRM_Driver_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_Asic_Comm_Err = 0;
    Eem_MainBus.Eem_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FlOpen_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FlShort_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FlOverTemp_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FlLeakage_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FrOpen_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FrShort_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FrOverTemp_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_FrLeakage_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RlOpen_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RlShort_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RlOverTemp_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RlLeakage_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RrOpen_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RrShort_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RrOverTemp_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_RrLeakage_Err = 0;
    Eem_MainBus.Eem_MainWssMonData.WssM_Asic_Comm_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_MainBusOff_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_SubBusOff_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_MainOverRun_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_SubOverRun_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_EMS1_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_EMS2_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_TCU1_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_TCU5_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_FWD1_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_HCU1_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_HCU2_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_HCU3_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_VSM2_Tout_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_EMS_Invalid_Err = 0;
    Eem_MainBus.Eem_MainCanMonData.CanM_TCU_Invalid_Err = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdInterPwrSuppMonErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdClkMonErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdBISTErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdFlashMemoryErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdRAMErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdConfigRegiErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdInputPattMonErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdOverTempErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdCPmpVMonErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdHBuffCapVErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdInOutPlauErr = 0;
    Eem_MainBus.Eem_MainMgdErrInfo.MgdCtrlSigMonErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.ExtPWRSuppMonErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.IntPWRSuppMonErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.WatchDogErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.FlashMemoryErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.StartUpTestErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.ConfigRegiTestErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.HWIntegConsisErr = 0;
    Eem_MainBus.Eem_MainMpsD1ErrInfo.MagnetLossErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.ExtPWRSuppMonErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.IntPWRSuppMonErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.WatchDogErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.FlashMemoryErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.StartUpTestErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.ConfigRegiTestErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.HWIntegConsisErr = 0;
    Eem_MainBus.Eem_MainMpsD2ErrInfo.MagnetLossErr = 0;
    Eem_MainBus.Eem_MainYawPlauOutput.YawPlauModelErr = 0;
    Eem_MainBus.Eem_MainYawPlauOutput.YawPlauNoisekErr = 0;
    Eem_MainBus.Eem_MainYawPlauOutput.YawPlauShockErr = 0;
    Eem_MainBus.Eem_MainYawPlauOutput.YawPlauRangeErr = 0;
    Eem_MainBus.Eem_MainYawPlauOutput.YawPlauStandStillErr = 0;
    Eem_MainBus.Eem_MainAxPlauOutput.AxPlauOffsetErr = 0;
    Eem_MainBus.Eem_MainAxPlauOutput.AxPlauDrivingOffsetErr = 0;
    Eem_MainBus.Eem_MainAxPlauOutput.AxPlauStickErr = 0;
    Eem_MainBus.Eem_MainAxPlauOutput.AxPlauNoiseErr = 0;
    Eem_MainBus.Eem_MainAyPlauOutput.AyPlauNoiselErr = 0;
    Eem_MainBus.Eem_MainAyPlauOutput.AyPlauModelErr = 0;
    Eem_MainBus.Eem_MainAyPlauOutput.AyPlauShockErr = 0;
    Eem_MainBus.Eem_MainAyPlauOutput.AyPlauRangeErr = 0;
    Eem_MainBus.Eem_MainAyPlauOutput.AyPlauStandStillErr = 0;
    Eem_MainBus.Eem_MainSasPlauOutput.SasPlauModelErr = 0;
    Eem_MainBus.Eem_MainSasPlauOutput.SasPlauOffsetErr = 0;
    Eem_MainBus.Eem_MainSasPlauOutput.SasPlauStickErr = 0;
    Eem_MainBus.Eem_MainPressPInfo.SimP_Noise_Err = 0;
    Eem_MainBus.Eem_MainPressPInfo.CirP1_Noise_Err = 0;
    Eem_MainBus.Eem_MainPressPInfo.CirP2_Noise_Err = 0;
    Eem_MainBus.Eem_MainEcuModeSts = 0;
    Eem_MainBus.Eem_MainIgnOnOffSts = 0;
    Eem_MainBus.Eem_MainIgnEdgeSts = 0;
    Eem_MainBus.Eem_MainDiagClrSrs = 0;
    Eem_MainBus.Eem_MainDiagSci = 0;
    Eem_MainBus.Eem_MainDiagAhbSci = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_BBSSol = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_ESCSol = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_FrontSol = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_RearSol = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_Motor = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_MPS = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_MGD = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_BBSValveRelay = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_ESCValveRelay = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_ECUHw = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_ASIC = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_OverVolt = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_UnderVolt = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_LowVolt = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_LowerVolt = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_SenPwr_12V = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_SenPwr_5V = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_Yaw = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_Ay = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_Ax = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_Str = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_CirP1 = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_CirP2 = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_SimP = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_BLS = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_ESCSw = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_HDCSw = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_AVHSw = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_BrakeLampRelay = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_EssRelay = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_GearR = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_Clutch = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_ParkBrake = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_PedalPDT = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_PedalPDF = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_BrakeFluid = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_TCSTemp = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_HDCTemp = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_SCCTemp = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_TVBBTemp = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_MainCanLine = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_SubCanLine = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_EMSTimeOut = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_FWDTimeOut = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_TCUTimeOut = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_HCUTimeOut = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_MCUTimeOut = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_VariantCoding = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_WssFL = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_WssFR = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_WssRL = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_WssRR = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_SameSideWss = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_DiagonalWss = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_FrontWss = 0;
    Eem_MainBus.Eem_MainEemFailData.Eem_Fail_RearWss = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail = 0;
    Eem_MainBus.Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_WssFL = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_WssFR = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_WssRL = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_WssRR = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_SameSideWss = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_FrontWss = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_RearWss = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_BBSSol = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_ESCSol = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_FrontSol = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_RearSol = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_Motor = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_MPS = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_MGD = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_ECUHw = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_ASIC = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_OverVolt = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_UnderVolt = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_LowVolt = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_Yaw = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_Ay = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_Ax = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_Str = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_CirP1 = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_CirP2 = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_SimP = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_BS = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_BLS = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_ESCSw = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_HDCSw = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_AVHSw = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_EssRelay = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_GearR = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_Clutch = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_ParkBrake = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_PedalPDT = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_PedalPDF = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_TCSTemp = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_HDCTemp = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_SCCTemp = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_TVBBTemp = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_MainCanLine = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_SubCanLine = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut = 0;
    Eem_MainBus.Eem_MainEemSuspectData.Eem_Suspect_VariantCoding = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Wss = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Yaw = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Ay = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Ax = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Cir1P = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Cir2P = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_SimP = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Bls = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Pedal = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Motor = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Vdc_Sw = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Hdc_Sw = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_GearR_Sw = 0;
    Eem_MainBus.Eem_MainEemEceData.Eem_Ece_Clutch_Sw = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_EBDLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_ABSLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_TCSLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_VDCLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_HDCLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_BBSBuzzorRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_AHBLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_TCSFuncLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_AVHLampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_AVHILampRequest = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_ACCEnable = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Lamp_CDMEnable = 0;
    Eem_MainBus.Eem_MainEemLampData.Eem_Buzzor_On = 0;

    EEM_Initialize();
}

void Eem_Main(void)
{
    Eem_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    Eem_Main_Read_Eem_MainBBSVlvErrInfo(&Eem_MainBus.Eem_MainBBSVlvErrInfo);
    /*==============================================================================
    * Members of structure Eem_MainBBSVlvErrInfo 
     : Eem_MainBBSVlvErrInfo.Batt1Fuse_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err;
     : Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainAbsVlvMData(&Eem_MainBus.Eem_MainAbsVlvMData);
    /*==============================================================================
    * Members of structure Eem_MainAbsVlvMData 
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err;
     : Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainEscSwtStInfo(&Eem_MainBus.Eem_MainEscSwtStInfo);
    /*==============================================================================
    * Members of structure Eem_MainEscSwtStInfo 
     : Eem_MainEscSwtStInfo.EscDisabledBySwt;
     : Eem_MainEscSwtStInfo.TcsDisabledBySwt;
     : Eem_MainEscSwtStInfo.HdcEnabledBySwt;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainWhlSpdInfo(&Eem_MainBus.Eem_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure Eem_MainWhlSpdInfo 
     : Eem_MainWhlSpdInfo.FlWhlSpd;
     : Eem_MainWhlSpdInfo.FrWhlSpd;
     : Eem_MainWhlSpdInfo.RlWhlSpd;
     : Eem_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainAbsCtrlInfo_AbsActFlg(&Eem_MainBus.Eem_MainAbsCtrlInfo.AbsActFlg);

    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainEscCtrlInfo_EscActFlg(&Eem_MainBus.Eem_MainEscCtrlInfo.EscActFlg);

    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainHdcCtrlInfo_HdcActFlg(&Eem_MainBus.Eem_MainHdcCtrlInfo.HdcActFlg);

    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainTcsCtrlInfo_TcsActFlg(&Eem_MainBus.Eem_MainTcsCtrlInfo.TcsActFlg);

    Eem_Main_Read_Eem_MainRelayMonitorData(&Eem_MainBus.Eem_MainRelayMonitorData);
    /*==============================================================================
    * Members of structure Eem_MainRelayMonitorData 
     : Eem_MainRelayMonitorData.RlyM_HDCRelay_Open_Err;
     : Eem_MainRelayMonitorData.RlyM_HDCRelay_Short_Err;
     : Eem_MainRelayMonitorData.RlyM_ESSRelay_Open_Err;
     : Eem_MainRelayMonitorData.RlyM_ESSRelay_Short_Err;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_5V_Stable(&Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_5V_Err(&Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_5V_Err);
    Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(&Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err);
    Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(&Eem_MainBus.Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err);

    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS1_Open_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_PTS1_Open_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS1_Short_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_PTS1_Short_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS2_Open_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_PTS2_Open_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS2_Short_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_PTS2_Short_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS1_SupplyPower_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_PTS1_SupplyPower_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS2_SupplyPower_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_PTS2_SupplyPower_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_VDD3_OverVolt_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_VDD3_OverVolt_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_VDD3_UnderVolt_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_VDD3_UnderVolt_Err);
    Eem_Main_Read_Eem_MainPedalMData_PedalM_VDD3_OverCurrent_Err(&Eem_MainBus.Eem_MainPedalMData.PedalM_VDD3_OverCurrent_Err);

    Eem_Main_Read_Eem_MainPresMonFaultInfo(&Eem_MainBus.Eem_MainPresMonFaultInfo);
    /*==============================================================================
    * Members of structure Eem_MainPresMonFaultInfo 
     : Eem_MainPresMonFaultInfo.CIRP1_FC_Fault_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_FC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_Temp_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP1_SC_Diag_Info;
     : Eem_MainPresMonFaultInfo.CIRP2_FC_Fault_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_FC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_Temp_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_CRC_Err;
     : Eem_MainPresMonFaultInfo.CIRP2_SC_Diag_Info;
     : Eem_MainPresMonFaultInfo.SIMP_FC_Fault_Err;
     : Eem_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.SIMP_FC_CRC_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_Temp_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_CRC_Err;
     : Eem_MainPresMonFaultInfo.SIMP_SC_Diag_Info;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainAsicMonFaultInfo(&Eem_MainBus.Eem_MainAsicMonFaultInfo);
    /*==============================================================================
    * Members of structure Eem_MainAsicMonFaultInfo 
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicComm_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicOsc_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicNvm_Err;
     : Eem_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainSwtMonFaultInfo(&Eem_MainBus.Eem_MainSwtMonFaultInfo);
    /*==============================================================================
    * Members of structure Eem_MainSwtMonFaultInfo 
     : Eem_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_HDC_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_AVH_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_BFL_Sw_Err;
     : Eem_MainSwtMonFaultInfo.SWM_PB_Sw_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainSwtPFaultInfo(&Eem_MainBus.Eem_MainSwtPFaultInfo);
    /*==============================================================================
    * Members of structure Eem_MainSwtPFaultInfo 
     : Eem_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
     : Eem_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainYAWMOutInfo(&Eem_MainBus.Eem_MainYAWMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainYAWMOutInfo 
     : Eem_MainYAWMOutInfo.YAWM_Timeout_Err;
     : Eem_MainYAWMOutInfo.YAWM_Invalid_Err;
     : Eem_MainYAWMOutInfo.YAWM_CRC_Err;
     : Eem_MainYAWMOutInfo.YAWM_Rolling_Err;
     : Eem_MainYAWMOutInfo.YAWM_Temperature_Err;
     : Eem_MainYAWMOutInfo.YAWM_Range_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainAYMOuitInfo(&Eem_MainBus.Eem_MainAYMOuitInfo);
    /*==============================================================================
    * Members of structure Eem_MainAYMOuitInfo 
     : Eem_MainAYMOuitInfo.AYM_Timeout_Err;
     : Eem_MainAYMOuitInfo.AYM_Invalid_Err;
     : Eem_MainAYMOuitInfo.AYM_CRC_Err;
     : Eem_MainAYMOuitInfo.AYM_Rolling_Err;
     : Eem_MainAYMOuitInfo.AYM_Temperature_Err;
     : Eem_MainAYMOuitInfo.AYM_Range_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainAXMOutInfo(&Eem_MainBus.Eem_MainAXMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainAXMOutInfo 
     : Eem_MainAXMOutInfo.AXM_Timeout_Err;
     : Eem_MainAXMOutInfo.AXM_Invalid_Err;
     : Eem_MainAXMOutInfo.AXM_CRC_Err;
     : Eem_MainAXMOutInfo.AXM_Rolling_Err;
     : Eem_MainAXMOutInfo.AXM_Temperature_Err;
     : Eem_MainAXMOutInfo.AXM_Range_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainSASMOutInfo(&Eem_MainBus.Eem_MainSASMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainSASMOutInfo 
     : Eem_MainSASMOutInfo.SASM_Timeout_Err;
     : Eem_MainSASMOutInfo.SASM_Invalid_Err;
     : Eem_MainSASMOutInfo.SASM_CRC_Err;
     : Eem_MainSASMOutInfo.SASM_Rolling_Err;
     : Eem_MainSASMOutInfo.SASM_Range_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainMTRMOutInfo(&Eem_MainBus.Eem_MainMTRMOutInfo);
    /*==============================================================================
    * Members of structure Eem_MainMTRMOutInfo 
     : Eem_MainMTRMOutInfo.MTRM_Power_Open_Err;
     : Eem_MainMTRMOutInfo.MTRM_Open_Err;
     : Eem_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err;
     : Eem_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err;
     : Eem_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err;
     : Eem_MainMTRMOutInfo.MTRM_Calibration_Err;
     : Eem_MainMTRMOutInfo.MTRM_Short_Err;
     : Eem_MainMTRMOutInfo.MTRM_Driver_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainSysPwrMonData(&Eem_MainBus.Eem_MainSysPwrMonData);
    /*==============================================================================
    * Members of structure Eem_MainSysPwrMonData 
     : Eem_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err;
     : Eem_MainSysPwrMonData.SysPwrM_Asic_Comm_Err;
     : Eem_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Eem_Main_Read_Eem_MainWssMonData_WssM_FlOpen_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FlOpen_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FlShort_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FlShort_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FlOverTemp_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FlOverTemp_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FlLeakage_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FlLeakage_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FrOpen_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FrOpen_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FrShort_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FrShort_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FrOverTemp_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FrOverTemp_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_FrLeakage_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_FrLeakage_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RlOpen_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RlOpen_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RlShort_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RlShort_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RlOverTemp_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RlOverTemp_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RlLeakage_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RlLeakage_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RrOpen_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RrOpen_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RrShort_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RrShort_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RrOverTemp_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RrOverTemp_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_RrLeakage_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_RrLeakage_Err);
    Eem_Main_Read_Eem_MainWssMonData_WssM_Asic_Comm_Err(&Eem_MainBus.Eem_MainWssMonData.WssM_Asic_Comm_Err);

    Eem_Main_Read_Eem_MainCanMonData(&Eem_MainBus.Eem_MainCanMonData);
    /*==============================================================================
    * Members of structure Eem_MainCanMonData 
     : Eem_MainCanMonData.CanM_MainBusOff_Err;
     : Eem_MainCanMonData.CanM_SubBusOff_Err;
     : Eem_MainCanMonData.CanM_MainOverRun_Err;
     : Eem_MainCanMonData.CanM_SubOverRun_Err;
     : Eem_MainCanMonData.CanM_EMS1_Tout_Err;
     : Eem_MainCanMonData.CanM_EMS2_Tout_Err;
     : Eem_MainCanMonData.CanM_TCU1_Tout_Err;
     : Eem_MainCanMonData.CanM_TCU5_Tout_Err;
     : Eem_MainCanMonData.CanM_FWD1_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU1_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU2_Tout_Err;
     : Eem_MainCanMonData.CanM_HCU3_Tout_Err;
     : Eem_MainCanMonData.CanM_VSM2_Tout_Err;
     : Eem_MainCanMonData.CanM_EMS_Invalid_Err;
     : Eem_MainCanMonData.CanM_TCU_Invalid_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainMgdErrInfo(&Eem_MainBus.Eem_MainMgdErrInfo);
    /*==============================================================================
    * Members of structure Eem_MainMgdErrInfo 
     : Eem_MainMgdErrInfo.MgdInterPwrSuppMonErr;
     : Eem_MainMgdErrInfo.MgdClkMonErr;
     : Eem_MainMgdErrInfo.MgdBISTErr;
     : Eem_MainMgdErrInfo.MgdFlashMemoryErr;
     : Eem_MainMgdErrInfo.MgdRAMErr;
     : Eem_MainMgdErrInfo.MgdConfigRegiErr;
     : Eem_MainMgdErrInfo.MgdInputPattMonErr;
     : Eem_MainMgdErrInfo.MgdOverTempErr;
     : Eem_MainMgdErrInfo.MgdCPmpVMonErr;
     : Eem_MainMgdErrInfo.MgdHBuffCapVErr;
     : Eem_MainMgdErrInfo.MgdInOutPlauErr;
     : Eem_MainMgdErrInfo.MgdCtrlSigMonErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainMpsD1ErrInfo(&Eem_MainBus.Eem_MainMpsD1ErrInfo);
    /*==============================================================================
    * Members of structure Eem_MainMpsD1ErrInfo 
     : Eem_MainMpsD1ErrInfo.ExtPWRSuppMonErr;
     : Eem_MainMpsD1ErrInfo.IntPWRSuppMonErr;
     : Eem_MainMpsD1ErrInfo.WatchDogErr;
     : Eem_MainMpsD1ErrInfo.FlashMemoryErr;
     : Eem_MainMpsD1ErrInfo.StartUpTestErr;
     : Eem_MainMpsD1ErrInfo.ConfigRegiTestErr;
     : Eem_MainMpsD1ErrInfo.HWIntegConsisErr;
     : Eem_MainMpsD1ErrInfo.MagnetLossErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainMpsD2ErrInfo(&Eem_MainBus.Eem_MainMpsD2ErrInfo);
    /*==============================================================================
    * Members of structure Eem_MainMpsD2ErrInfo 
     : Eem_MainMpsD2ErrInfo.ExtPWRSuppMonErr;
     : Eem_MainMpsD2ErrInfo.IntPWRSuppMonErr;
     : Eem_MainMpsD2ErrInfo.WatchDogErr;
     : Eem_MainMpsD2ErrInfo.FlashMemoryErr;
     : Eem_MainMpsD2ErrInfo.StartUpTestErr;
     : Eem_MainMpsD2ErrInfo.ConfigRegiTestErr;
     : Eem_MainMpsD2ErrInfo.HWIntegConsisErr;
     : Eem_MainMpsD2ErrInfo.MagnetLossErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainYawPlauOutput(&Eem_MainBus.Eem_MainYawPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainYawPlauOutput 
     : Eem_MainYawPlauOutput.YawPlauModelErr;
     : Eem_MainYawPlauOutput.YawPlauNoisekErr;
     : Eem_MainYawPlauOutput.YawPlauShockErr;
     : Eem_MainYawPlauOutput.YawPlauRangeErr;
     : Eem_MainYawPlauOutput.YawPlauStandStillErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainAxPlauOutput(&Eem_MainBus.Eem_MainAxPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainAxPlauOutput 
     : Eem_MainAxPlauOutput.AxPlauOffsetErr;
     : Eem_MainAxPlauOutput.AxPlauDrivingOffsetErr;
     : Eem_MainAxPlauOutput.AxPlauStickErr;
     : Eem_MainAxPlauOutput.AxPlauNoiseErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainAyPlauOutput(&Eem_MainBus.Eem_MainAyPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainAyPlauOutput 
     : Eem_MainAyPlauOutput.AyPlauNoiselErr;
     : Eem_MainAyPlauOutput.AyPlauModelErr;
     : Eem_MainAyPlauOutput.AyPlauShockErr;
     : Eem_MainAyPlauOutput.AyPlauRangeErr;
     : Eem_MainAyPlauOutput.AyPlauStandStillErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainSasPlauOutput(&Eem_MainBus.Eem_MainSasPlauOutput);
    /*==============================================================================
    * Members of structure Eem_MainSasPlauOutput 
     : Eem_MainSasPlauOutput.SasPlauModelErr;
     : Eem_MainSasPlauOutput.SasPlauOffsetErr;
     : Eem_MainSasPlauOutput.SasPlauStickErr;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainPressPInfo(&Eem_MainBus.Eem_MainPressPInfo);
    /*==============================================================================
    * Members of structure Eem_MainPressPInfo 
     : Eem_MainPressPInfo.SimP_Noise_Err;
     : Eem_MainPressPInfo.CirP1_Noise_Err;
     : Eem_MainPressPInfo.CirP2_Noise_Err;
     =============================================================================*/
    
    Eem_Main_Read_Eem_MainEcuModeSts(&Eem_MainBus.Eem_MainEcuModeSts);
    Eem_Main_Read_Eem_MainIgnOnOffSts(&Eem_MainBus.Eem_MainIgnOnOffSts);
    Eem_Main_Read_Eem_MainIgnEdgeSts(&Eem_MainBus.Eem_MainIgnEdgeSts);
    Eem_Main_Read_Eem_MainDiagClrSrs(&Eem_MainBus.Eem_MainDiagClrSrs);
    Eem_Main_Read_Eem_MainDiagSci(&Eem_MainBus.Eem_MainDiagSci);
    Eem_Main_Read_Eem_MainDiagAhbSci(&Eem_MainBus.Eem_MainDiagAhbSci);

    /* Process */
    EEM_ErrorHandler(&Eem_MainBus);

    /* Output */
    Eem_Main_Write_Eem_MainEemFailData(&Eem_MainBus.Eem_MainEemFailData);
    /*==============================================================================
    * Members of structure Eem_MainEemFailData 
     : Eem_MainEemFailData.Eem_Fail_BBSSol;
     : Eem_MainEemFailData.Eem_Fail_ESCSol;
     : Eem_MainEemFailData.Eem_Fail_FrontSol;
     : Eem_MainEemFailData.Eem_Fail_RearSol;
     : Eem_MainEemFailData.Eem_Fail_Motor;
     : Eem_MainEemFailData.Eem_Fail_MPS;
     : Eem_MainEemFailData.Eem_Fail_MGD;
     : Eem_MainEemFailData.Eem_Fail_BBSValveRelay;
     : Eem_MainEemFailData.Eem_Fail_ESCValveRelay;
     : Eem_MainEemFailData.Eem_Fail_ECUHw;
     : Eem_MainEemFailData.Eem_Fail_ASIC;
     : Eem_MainEemFailData.Eem_Fail_OverVolt;
     : Eem_MainEemFailData.Eem_Fail_UnderVolt;
     : Eem_MainEemFailData.Eem_Fail_LowVolt;
     : Eem_MainEemFailData.Eem_Fail_LowerVolt;
     : Eem_MainEemFailData.Eem_Fail_SenPwr_12V;
     : Eem_MainEemFailData.Eem_Fail_SenPwr_5V;
     : Eem_MainEemFailData.Eem_Fail_Yaw;
     : Eem_MainEemFailData.Eem_Fail_Ay;
     : Eem_MainEemFailData.Eem_Fail_Ax;
     : Eem_MainEemFailData.Eem_Fail_Str;
     : Eem_MainEemFailData.Eem_Fail_CirP1;
     : Eem_MainEemFailData.Eem_Fail_CirP2;
     : Eem_MainEemFailData.Eem_Fail_SimP;
     : Eem_MainEemFailData.Eem_Fail_BLS;
     : Eem_MainEemFailData.Eem_Fail_ESCSw;
     : Eem_MainEemFailData.Eem_Fail_HDCSw;
     : Eem_MainEemFailData.Eem_Fail_AVHSw;
     : Eem_MainEemFailData.Eem_Fail_BrakeLampRelay;
     : Eem_MainEemFailData.Eem_Fail_EssRelay;
     : Eem_MainEemFailData.Eem_Fail_GearR;
     : Eem_MainEemFailData.Eem_Fail_Clutch;
     : Eem_MainEemFailData.Eem_Fail_ParkBrake;
     : Eem_MainEemFailData.Eem_Fail_PedalPDT;
     : Eem_MainEemFailData.Eem_Fail_PedalPDF;
     : Eem_MainEemFailData.Eem_Fail_BrakeFluid;
     : Eem_MainEemFailData.Eem_Fail_TCSTemp;
     : Eem_MainEemFailData.Eem_Fail_HDCTemp;
     : Eem_MainEemFailData.Eem_Fail_SCCTemp;
     : Eem_MainEemFailData.Eem_Fail_TVBBTemp;
     : Eem_MainEemFailData.Eem_Fail_MainCanLine;
     : Eem_MainEemFailData.Eem_Fail_SubCanLine;
     : Eem_MainEemFailData.Eem_Fail_EMSTimeOut;
     : Eem_MainEemFailData.Eem_Fail_FWDTimeOut;
     : Eem_MainEemFailData.Eem_Fail_TCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_HCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_MCUTimeOut;
     : Eem_MainEemFailData.Eem_Fail_VariantCoding;
     : Eem_MainEemFailData.Eem_Fail_WssFL;
     : Eem_MainEemFailData.Eem_Fail_WssFR;
     : Eem_MainEemFailData.Eem_Fail_WssRL;
     : Eem_MainEemFailData.Eem_Fail_WssRR;
     : Eem_MainEemFailData.Eem_Fail_SameSideWss;
     : Eem_MainEemFailData.Eem_Fail_DiagonalWss;
     : Eem_MainEemFailData.Eem_Fail_FrontWss;
     : Eem_MainEemFailData.Eem_Fail_RearWss;
     =============================================================================*/
    
    Eem_Main_Write_Eem_MainEemCtrlInhibitData(&Eem_MainBus.Eem_MainEemCtrlInhibitData);
    /*==============================================================================
    * Members of structure Eem_MainEemCtrlInhibitData 
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/
    
    Eem_Main_Write_Eem_MainEemSuspectData(&Eem_MainBus.Eem_MainEemSuspectData);
    /*==============================================================================
    * Members of structure Eem_MainEemSuspectData 
     : Eem_MainEemSuspectData.Eem_Suspect_WssFL;
     : Eem_MainEemSuspectData.Eem_Suspect_WssFR;
     : Eem_MainEemSuspectData.Eem_Suspect_WssRL;
     : Eem_MainEemSuspectData.Eem_Suspect_WssRR;
     : Eem_MainEemSuspectData.Eem_Suspect_SameSideWss;
     : Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss;
     : Eem_MainEemSuspectData.Eem_Suspect_FrontWss;
     : Eem_MainEemSuspectData.Eem_Suspect_RearWss;
     : Eem_MainEemSuspectData.Eem_Suspect_BBSSol;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCSol;
     : Eem_MainEemSuspectData.Eem_Suspect_FrontSol;
     : Eem_MainEemSuspectData.Eem_Suspect_RearSol;
     : Eem_MainEemSuspectData.Eem_Suspect_Motor;
     : Eem_MainEemSuspectData.Eem_Suspect_MPS;
     : Eem_MainEemSuspectData.Eem_Suspect_MGD;
     : Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_ECUHw;
     : Eem_MainEemSuspectData.Eem_Suspect_ASIC;
     : Eem_MainEemSuspectData.Eem_Suspect_OverVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_UnderVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_LowVolt;
     : Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V;
     : Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V;
     : Eem_MainEemSuspectData.Eem_Suspect_Yaw;
     : Eem_MainEemSuspectData.Eem_Suspect_Ay;
     : Eem_MainEemSuspectData.Eem_Suspect_Ax;
     : Eem_MainEemSuspectData.Eem_Suspect_Str;
     : Eem_MainEemSuspectData.Eem_Suspect_CirP1;
     : Eem_MainEemSuspectData.Eem_Suspect_CirP2;
     : Eem_MainEemSuspectData.Eem_Suspect_SimP;
     : Eem_MainEemSuspectData.Eem_Suspect_BS;
     : Eem_MainEemSuspectData.Eem_Suspect_BLS;
     : Eem_MainEemSuspectData.Eem_Suspect_ESCSw;
     : Eem_MainEemSuspectData.Eem_Suspect_HDCSw;
     : Eem_MainEemSuspectData.Eem_Suspect_AVHSw;
     : Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_EssRelay;
     : Eem_MainEemSuspectData.Eem_Suspect_GearR;
     : Eem_MainEemSuspectData.Eem_Suspect_Clutch;
     : Eem_MainEemSuspectData.Eem_Suspect_ParkBrake;
     : Eem_MainEemSuspectData.Eem_Suspect_PedalPDT;
     : Eem_MainEemSuspectData.Eem_Suspect_PedalPDF;
     : Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid;
     : Eem_MainEemSuspectData.Eem_Suspect_TCSTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_HDCTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_SCCTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_TVBBTemp;
     : Eem_MainEemSuspectData.Eem_Suspect_MainCanLine;
     : Eem_MainEemSuspectData.Eem_Suspect_SubCanLine;
     : Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut;
     : Eem_MainEemSuspectData.Eem_Suspect_VariantCoding;
     =============================================================================*/
    
    Eem_Main_Write_Eem_MainEemEceData(&Eem_MainBus.Eem_MainEemEceData);
    /*==============================================================================
    * Members of structure Eem_MainEemEceData 
     : Eem_MainEemEceData.Eem_Ece_Wss;
     : Eem_MainEemEceData.Eem_Ece_Yaw;
     : Eem_MainEemEceData.Eem_Ece_Ay;
     : Eem_MainEemEceData.Eem_Ece_Ax;
     : Eem_MainEemEceData.Eem_Ece_Cir1P;
     : Eem_MainEemEceData.Eem_Ece_Cir2P;
     : Eem_MainEemEceData.Eem_Ece_SimP;
     : Eem_MainEemEceData.Eem_Ece_Bls;
     : Eem_MainEemEceData.Eem_Ece_Pedal;
     : Eem_MainEemEceData.Eem_Ece_Motor;
     : Eem_MainEemEceData.Eem_Ece_Vdc_Sw;
     : Eem_MainEemEceData.Eem_Ece_Hdc_Sw;
     : Eem_MainEemEceData.Eem_Ece_GearR_Sw;
     : Eem_MainEemEceData.Eem_Ece_Clutch_Sw;
     =============================================================================*/
    
    Eem_Main_Write_Eem_MainEemLampData(&Eem_MainBus.Eem_MainEemLampData);
    /*==============================================================================
    * Members of structure Eem_MainEemLampData 
     : Eem_MainEemLampData.Eem_Lamp_EBDLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ABSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_HDCLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_BBSBuzzorRequest;
     : Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AHBLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_TCSFuncLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AVHLampRequest;
     : Eem_MainEemLampData.Eem_Lamp_AVHILampRequest;
     : Eem_MainEemLampData.Eem_Lamp_ACCEnable;
     : Eem_MainEemLampData.Eem_Lamp_CDMEnable;
     : Eem_MainEemLampData.Eem_Buzzor_On;
     =============================================================================*/
    

    Eem_Main_Timer_Elapsed = STM0_TIM0.U - Eem_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define EEM_MAIN_STOP_SEC_CODE
#include "Eem_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
