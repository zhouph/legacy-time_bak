/**
 * @defgroup Eem_Main Eem_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Handling.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_HANDLING_H_
#define EEM_HANDLING_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Types.h"
#include "Eem_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define EEM_FDC_LOW_LIMIT       (-32768)
#define EEM_FDC_HIGH_LIMIT      (32767)

#define EEM_NOT_FOUND           (0xFFFF)

#define STANDSTILL_LAMP_ON_TIME (3000)  /* 3 sec */
#define HDC_ACT_LAMP_ON_TIME    (250)   /* 250 ms */
#define HDC_ACT_LAMP_OFF_TIME   (500)   /* 500 ms */
#define FUNC_LAMP_ON_TIME       (250)   /* 250 ms */
#define FUNC_LAMP_OFF_TIME      (1000)  /* 1000 ms */

#define EEM_CYCLETIME           (5)

#define DRIVING_LAMP_ON_TIME    (1000)  /* 1 sec */
#define DRIVING_LAMP_ON_SPEED   (U16_F64SPEED_10KPH)

#define ERROR_BYTE_NO           (96)

#define SERVICE_LAMP_OK         (0)
#define SERVICE_LAMP_DEGRADED   (1)
#define SERVICE_LAMP_DEFECTIVE  (2)

#define FUNC_LAMP_OFF           (0)
#define FUNC_LAMP_ON            (1)
#define FUNC_LAMP_BLINKING      (2)

#define HDC_LAMP_OFF            (0)
#define HDC_LAMP_READY          (1)
#define HDC_LAMP_ACTIVE         (2)
#define HDC_LAMP_ERROR          (3)

#define AVH_LAMP_IS_OFF         (0) /* Lamp is off */
#define AVH_LAMP_IS_FAILED      (1) /* Lamp is on as yellow */
#define AVH_LAMP_IS_ON          (2) /* Lamp is on as green */
#define AVH_LAMP_IS_READY       (3) /* Lamp is on as white */

#define AVH_I_LAMP_STATUS_OFF         (1)
#define AVH_I_LAMP_STATUS_ON_WHITE    (0)

#define FLEX_SWT_APPLY          (ENABLE)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    uint8  EEM_Err;
    uint8  reserve_0;
    uint16 reserve_1;
} EEM_ErrInfoType;

typedef struct
{
    sint16 EEM_FDC;
    uint8  EEM_Status;
} EEM_FDCInfoType;

typedef struct
{
    boolean bWssFL;
    boolean bWssFR;
    boolean bWssRL;
    boolean bWssRR;
    boolean bSameSideWss;
    boolean bDiagonalWss;
    boolean bFrontWss;
    boolean bRearWss;

    boolean bBBSSol;
    boolean bESCSol;
    boolean bFrontSol;
    boolean bRearSol;
    boolean bBBSValveRelay;
    boolean bESCValveRelay;

    boolean bMotor;
    boolean bMPS;
    boolean bMGD;

    boolean bHigherVolt;
    boolean bOverVolt;
    boolean bUnderVolt;
    boolean bLowVolt;
    boolean bLowerVolt;
    boolean bSenPwr_5V;
    boolean bSenPwr_12V;

    boolean bEcuHW;
    boolean bAsic;

    boolean bYaw;
    boolean bAy;
    boolean bAx;
    boolean bSAS;

    boolean bCIRP1;
    boolean bCIRP2;
    boolean bSIMP;

    boolean bBLS;
    boolean bBS;
    boolean bESCSw;
    boolean bHDCSw;
    boolean bAVHSw;
    boolean bBrakeFluid;

    boolean bPedal_PTS1;
    boolean bPedal_PTS2;

    boolean bBrakeLampRelay;
    boolean bESSRelay;

    boolean bTCSTemp;
    boolean bHDCTemp;
    boolean bTVBBTemp;
    boolean bSCCTemp;

    boolean bMainCanLine;
    boolean bSubCanLine;
    boolean bEMS_Timeout;
    boolean bTCU_Timeout;
    boolean bFWD_Timeout;
    boolean bHCU_Timeout;
    boolean bMCU_Timeout;

    boolean bVariantCoding;
} EEM_FailSusInfoType;

typedef struct
{
    boolean bCtrlIhb_Bbs;
    boolean bCtrlIhb_Ebd;
    boolean bCtrlIhb_Abs;
    boolean bCtrlIhb_Edc;
    boolean bCtrlIhb_Btcs;
    boolean bCtrlIhb_ETcs;
    boolean bCtrlIhb_Tcs;
    boolean bCtrlIhb_Vdc;
    boolean bCtrlIhb_Hsa;
    boolean bCtrlIhb_Hdc;
    boolean bCtrlIhb_Pba;
    boolean bCtrlIhb_Avh;
    boolean bCtrlIhb_Moc;

    boolean bCtrlIhb_BbsAllCtrlInhibit;
    boolean bCtrlIhb_BbsDiagControlInhibit;
    boolean bCtrlIhb_BbsDegradeModeFail;
    boolean bCtrlIhb_BbsDefectiveModeFail;
} EEM_CtrlIhbType;

typedef struct
{
    uint16 u16HDCActOnTime;
    uint16 u16HDCActOffTime;
} EEM_LampInfoType;

typedef boolean EEM_Request;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void EEM_ErrorHandler(Eem_Main_HdrBusType *pEemData);
extern void EEM_Initialize(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_HANDLING_H_ */
/** @} */

