/**
 * @defgroup Eem_Main Eem_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Handling.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_ERRINFOTABLE_H_
#define EEM_ERRINFOTABLE_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Types.h"
#include "Eem_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define EEM_CYCLETIME_5     (0)
#define EEM_CYCLETIME_10_0  (1)
#define EEM_CYCLETIME_10_1  (2)
#define EEM_CYCLETIME_20_0  (3)
#define EEM_CYCLETIME_20_1  (4)
#define EEM_CYCLETIME_20_2  (5)
#define EEM_CYCLETIME_20_3  (6)

#define EEM_ATTR_PERMANENT_FAIL (0)
#define EEM_ATTR_AUTO_RECOVERY  (1)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef enum
{
    /* System Power Monitor */
    EEM_Err_id_SysOverVolt_1st = 0,
    EEM_Err_id_SysOverVolt_2nd,
    EEM_Err_id_SysUnderVolt_1st,
    EEM_Err_id_SysUnderVolt_2nd,
    EEM_Err_id_SysUnderVolt_3rd,
    EEM_Err_id_IgnOpen,

    /* Asic Monitor */
    EEM_Err_id_AsicOverVolt,
    EEM_Err_id_AsicUnderVolt,
    EEM_Err_id_AsicOverTemp,
    EEM_Err_id_AsicGndLoss,
    EEM_Err_id_AsicComm,
    EEM_Err_id_AsicNvm,
    EEM_Err_id_AsicOsc,
    EEM_Err_id_AsicCP,

    /* Sensor Power Monitor */
    EEM_Err_id_5V,
    EEM_Err_id_12V_Open,
    EEM_Err_id_12V_Short,

    /* BBS Valve */
    EEM_Err_id_SIM_Open,
    EEM_Err_id_SIM_PsvOpen,
    EEM_Err_id_SIM_Short,
    EEM_Err_id_SIM_CurReg,

    EEM_Err_id_CutP_Open,
    EEM_Err_id_CutP_PsvOpen,
    EEM_Err_id_CutP_Short,
    EEM_Err_id_CutP_CurReg,

    EEM_Err_id_CutS_Open,
    EEM_Err_id_CutS_PsvOpen,
    EEM_Err_id_CutS_Short,
    EEM_Err_id_CutS_CurReg,

    EEM_Err_id_Rlv_Open,
    EEM_Err_id_Rlv_PsvOpen,
    EEM_Err_id_Rlv_Short,
    EEM_Err_id_Rlv_CurReg,

    EEM_Err_id_CircV_Open,
    EEM_Err_id_CircV_PsvOpen,
    EEM_Err_id_CircV_Short,
    EEM_Err_id_CircV_CurReg,

    /* BBS Valve Relay */
    EEM_Err_id_Batt1Fuse_Open,    
    EEM_Err_id_BBSValveRelay_Open,
    EEM_Err_id_BBSValveRelay_S2B,
    EEM_Err_id_BBSValveRelay_S2G,
    EEM_Err_id_BBSValveRelay_OverTemp,
    EEM_Err_id_BBSValveRelay_ShutdownLine,    

    /* ESC Valve Monitor */
    EEM_Err_id_FLNO_Open,
    EEM_Err_id_FLNO_PsvOpen,
    EEM_Err_id_FLNO_Short,
    EEM_Err_id_FLNO_OverTemp,
    EEM_Err_id_FLNO_CurReg,

    EEM_Err_id_FRNO_Open,
    EEM_Err_id_FRNO_PsvOpen,
    EEM_Err_id_FRNO_Short,
    EEM_Err_id_FRNO_OverTemp,
    EEM_Err_id_FRNO_CurReg,

    EEM_Err_id_RLNO_Open,
    EEM_Err_id_RLNO_PsvOpen,
    EEM_Err_id_RLNO_Short,
    EEM_Err_id_RLNO_OverTemp,
    EEM_Err_id_RLNO_CurReg,

    EEM_Err_id_RRNO_Open,
    EEM_Err_id_RRNO_PsvOpen,
    EEM_Err_id_RRNO_Short,
    EEM_Err_id_RRNO_OverTemp,
    EEM_Err_id_RRNO_CurReg,

    EEM_Err_id_BAL_Open,
    EEM_Err_id_BAL_PsvOpen,
    EEM_Err_id_BAL_Short,
    EEM_Err_id_BAL_OverTemp,
    EEM_Err_id_BAL_CurReg,

    EEM_Err_id_PD_Open,
    EEM_Err_id_PD_PsvOpen,
    EEM_Err_id_PD_Short,
    EEM_Err_id_PD_OverTemp,
    EEM_Err_id_PD_CurReg,

    EEM_Err_id_RESP_Open,
    EEM_Err_id_RESP_PsvOpen,
    EEM_Err_id_RESP_Short,
    EEM_Err_id_RESP_OverTemp,
    EEM_Err_id_RESP_CurReg,

    EEM_Err_id_FLNC_Open,
    EEM_Err_id_FLNC_PsvOpen,
    EEM_Err_id_FLNC_Short,
    EEM_Err_id_FLNC_OverTemp,

    EEM_Err_id_FRNC_Open,
    EEM_Err_id_FRNC_PsvOpen,
    EEM_Err_id_FRNC_Short,
    EEM_Err_id_FRNC_OverTemp,

    EEM_Err_id_RLNC_Open,
    EEM_Err_id_RLNC_PsvOpen,
    EEM_Err_id_RLNC_Short,
    EEM_Err_id_RLNC_OverTemp,

    EEM_Err_id_RRNC_Open,
    EEM_Err_id_RRNC_PsvOpen,
    EEM_Err_id_RRNC_Short,
    EEM_Err_id_RRNC_OverTemp,

    /* ESC Valve Relay */
    EEM_Err_id_ESCValveRelay_Open,
    EEM_Err_id_ESCValveRelay_S2B,
    EEM_Err_id_ESCValveRelay_S2G,
    EEM_Err_id_ESCValveRelay_ShutdownLine,

    /* Motor */
    EEM_Err_id_Motor_Open,
    EEM_Err_id_Motor_FuseOpen,
    EEM_Err_id_Motor_Phase_U_OverCurrent,
    EEM_Err_id_Motor_Phase_V_OverCurrent,
    EEM_Err_id_Motor_Phase_W_OverCurrent,
    EEM_Err_id_Motor_Short,
    EEM_Err_id_Motor_Driver,
    EEM_Err_id_Motor_Calibration,

    /* MGD */
    EEM_Err_id_MGD_SupplyPower_Open,
    EEM_Err_id_MGD_Clock,
    EEM_Err_id_MGD_BIST,
    EEM_Err_id_MGD_NVM,
    EEM_Err_id_MGD_RAM,
    EEM_Err_id_MGD_ConfigReg,
    EEM_Err_id_MGD_InputPattern,
    EEM_Err_id_MGD_Temperature,
    EEM_Err_id_MGD_CP,
    EEM_Err_id_MGD_HBuffCap,
    EEM_Err_id_MGD_InOutPlau,
    EEM_Err_id_MGD_CtrlSig,

    /* MPS */
    EEM_Err_id_MPS_D1_ExtPower,
    EEM_Err_id_MPS_D1_IntPower,
    EEM_Err_id_MPS_D1_Watchdog,
    EEM_Err_id_MPS_D1_NVM,
    EEM_Err_id_MPS_D1_BIST,
    EEM_Err_id_MPS_D1_ConfigReg,
    EEM_Err_id_MPS_D1_HWIntegConsis,
    EEM_Err_id_MPS_D1_Magnet_Loss,

    EEM_Err_id_MPS_D2_ExtPower,
    EEM_Err_id_MPS_D2_IntPower,
    EEM_Err_id_MPS_D2_Watchdog,
    EEM_Err_id_MPS_D2_NVM,
    EEM_Err_id_MPS_D2_BIST,
    EEM_Err_id_MPS_D2_ConfigReg,
    EEM_Err_id_MPS_D2_HWIntegConsis,
    EEM_Err_id_MPS_D2_Magnet_Loss,    

    /* Wheel Speed Sensor */
    EEM_Err_id_WssFL_Open,
    EEM_Err_id_WssFL_Short,
    EEM_Err_id_WssFL_OverTemp,
    EEM_Err_id_WssFL_Leakage,
    EEM_Err_id_WssFR_Open,
    EEM_Err_id_WssFR_Short,
    EEM_Err_id_WssFR_OverTemp,
    EEM_Err_id_WssFR_Leakage,
    EEM_Err_id_WssRL_Open,
    EEM_Err_id_WssRL_Short,
    EEM_Err_id_WssRL_OverTemp,
    EEM_Err_id_WssRL_Leakage,
    EEM_Err_id_WssRR_Open,
    EEM_Err_id_WssRR_Short,
    EEM_Err_id_WssRR_OverTemp,
    EEM_Err_id_WssRR_Leakage,

    EEM_Err_id_WssFL_Airgap,
    EEM_Err_id_WssFL_SpeedJump_Plus,
    EEM_Err_id_WssFL_SpeedJump_Minus,
    EEM_Err_id_WssFL_WrongExciter,
    EEM_Err_id_WssFL_WrongInstalled,
    EEM_Err_id_WssFL_Phase,
    EEM_Err_id_WssFR_Airgap,
    EEM_Err_id_WssFR_SpeedJump_Plus,
    EEM_Err_id_WssFR_SpeedJump_Minus,
    EEM_Err_id_WssFR_WrongExciter,
    EEM_Err_id_WssFR_WrongInstalled,
    EEM_Err_id_WssFR_Phase,
    EEM_Err_id_WssRL_Airgap,
    EEM_Err_id_WssRL_SpeedJump_Plus,
    EEM_Err_id_WssRL_SpeedJump_Minus,
    EEM_Err_id_WssRL_WrongExciter,
    EEM_Err_id_WssRL_WrongInstalled,
    EEM_Err_id_WssRL_Phase,
    EEM_Err_id_WssRR_Airgap,
    EEM_Err_id_WssRR_SpeedJump_Plus,
    EEM_Err_id_WssRR_SpeedJump_Minus,
    EEM_Err_id_WssRR_WrongExciter,
    EEM_Err_id_WssRR_WrongInstalled,
    EEM_Err_id_WssRR_Phase,

    EEM_Err_id_LongTerm_ABS,

    /* Relay */
    EEM_Err_id_Relay1_Open,
    EEM_Err_id_Relay1_Short,
    EEM_Err_id_Relay2_Open,
    EEM_Err_id_Relay2_Short,

    /* Pedal */
    EEM_Err_id_Pedal_PTS1_Open,
    EEM_Err_id_Pedal_PTS1_Short,
    EEM_Err_id_Pedal_PTS2_Open,
    EEM_Err_id_Pedal_PTS2_Short,
    EEM_Err_id_Pedal_PTS1_SupplyPower_Open,
    EEM_Err_id_Pedal_PTS2_SupplyPower_Open,
    EEM_Err_id_Pedal_VDD3_OverVolt,
    EEM_Err_id_Pedal_VDD3_UnderVolt,
    EEM_Err_id_Pedal_VDD3_OverCurrent,
    EEM_Err_id_Pedal_Calibration,

    EEM_Err_id_Pedal_Offset,
    EEM_Err_id_Pedal_Stick,
    EEM_Err_id_Pedal_Noise,

    /* Switch */
    EEM_Err_id_EscOffSwitch_Invalid,
    EEM_Err_id_HDCSwitch_Invalid,
    EEM_Err_id_AvhSwitch_Invalid,
    EEM_Err_id_BLSSwitch_HighStick,
    EEM_Err_id_BLSSwitch_LowStick,
    EEM_Err_id_BSSwitch_HighStick,
    EEM_Err_id_BSSwitch_LowStick,
    EEM_Err_id_BFLSwitch_Invalid,
    EEM_Err_id_ClutchSwitch_Invalid,
    EEM_Err_id_GearR_Invalid,
    EEM_Err_id_PBSwitch_Invalid,

    /* SAS */
    EEM_Err_id_SAS_Timeout,
    EEM_Err_id_SAS_CheckSum,
    EEM_Err_id_SAS_RollingCnt,
    EEM_Err_id_SAS_Invalid,
    EEM_Err_id_SAS_Range,
    EEM_Err_id_SAS_Calibration,

    EEM_Err_id_SAS_ShortTerm_Offset,
    EEM_Err_id_SAS_LongTerm_Offset,
    EEM_Err_id_SAS_ModelMay,
    EEM_Err_id_SAS_Stick,

    /* YawG */
    EEM_Err_id_Yaw_Timeout,
    EEM_Err_id_Yaw_CheckSum,
    EEM_Err_id_Yaw_RollingCnt,
    EEM_Err_id_Yaw_Invalid,
    EEM_Err_id_Yaw_Temperature,
    EEM_Err_id_Yaw_Range,

    EEM_Err_id_Ay_Timeout,
    EEM_Err_id_Ay_CheckSum,
    EEM_Err_id_Ay_RollingCnt,
    EEM_Err_id_Ay_Invalid,
    EEM_Err_id_Ay_Temperature,
    EEM_Err_id_Ay_Range,

    EEM_Err_id_Ax_Timeout,
    EEM_Err_id_Ax_CheckSum,
    EEM_Err_id_Ax_RollingCnt,
    EEM_Err_id_Ax_Invalid,
    EEM_Err_id_Ax_Temperature,
    EEM_Err_id_Ax_Range,

    /* Pressure */
    EEM_Err_id_CIRP1_Timeout,
    EEM_Err_id_CIRP1_FC_Fault,
    EEM_Err_id_CIRP1_FC_Mismatch,
    EEM_Err_id_CIRP1_FC_CRC,
    EEM_Err_id_CIRP1_SC_Temperature,
    EEM_Err_id_CIRP1_SC_Mismatch,
    EEM_Err_id_CIRP1_SC_CRC,

    EEM_Err_id_CIRP1_Noise,

    EEM_Err_id_CIRP2_Timeout,
    EEM_Err_id_CIRP2_FC_Fault,
    EEM_Err_id_CIRP2_FC_Mismatch,
    EEM_Err_id_CIRP2_FC_CRC,
    EEM_Err_id_CIRP2_SC_Temperature,
    EEM_Err_id_CIRP2_SC_Mismatch,
    EEM_Err_id_CIRP2_SC_CRC,

    EEM_Err_id_CIRP2_Noise,

    EEM_Err_id_SIMP_Timeout,
    EEM_Err_id_SIMP_FC_Fault,
    EEM_Err_id_SIMP_FC_Mismatch,
    EEM_Err_id_SIMP_FC_CRC,
    EEM_Err_id_SIMP_SC_Temperature,
    EEM_Err_id_SIMP_SC_Mismatch,
    EEM_Err_id_SIMP_SC_CRC,

    EEM_Err_id_SIMP_Noise,

    /* CAN */
    EEM_Err_id_MainCan_BusOff,
    EEM_Err_id_SubCan_BusOff,
    EEM_Err_id_MainCan_OverRun,
    EEM_Err_id_SubCan_OverRun,
    EEM_Err_id_EMS1_Timeout,
    EEM_Err_id_EMS2_Timeout,
    EEM_Err_id_TCU1_Timeout,
    EEM_Err_id_TCU5_Timeout,
    EEM_Err_id_FWD1_Timeout,
    EEM_Err_id_HCU1_Timeout,
    EEM_Err_id_HCU2_Timeout,
    EEM_Err_id_HCU3_Timeout,
    EEM_Err_id_VSM2_Timeout,
    EEM_Err_id_EMS_Invalid,
    EEM_Err_id_TCU_Invalid,
    
    /* Logic Temperature */
    EEM_Err_id_TCSTemp,
    EEM_Err_id_HDCTemp,
    EEM_Err_id_SCCTemp,
    EEM_Err_id_TVBBTemp,
    
    EEM_Err_id_Max
} EEM_ErrIdType;

typedef struct
{
    EEM_ErrIdType EEM_IntErrId;
    uint16 EEM_DemErrId;
    uint16 EEM_Ftb;

    sint16 EEM_FDCFailThres;
    sint16 EEM_FDCPassThres;

    sint16 EEM_FDCIncrement;
    sint16 EEM_FDCDecrement;

    uint8   EEM_Cycletime;

    boolean EEM_DtcWrite;
    uint8   EEM_Attr;
    
    /* TODO : Jump Down, Jump Up */
} EEM_EventListInfoType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern const EEM_EventListInfoType gEEM_IntErrTable[EEM_Err_id_Max];

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern uint16 EEM_FindIndexByErrId(EEM_ErrIdType id);
extern void   EEM_ClearIntErrTable(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_ERRINFOTABLE_H_ */
/** @} */

