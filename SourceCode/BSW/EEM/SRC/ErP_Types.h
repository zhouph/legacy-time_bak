/*
 * ErP_Types.h
 *
 *  Created on: 2015. 1. 26.
 *      Author: jinsu.park
 */

#ifndef ERP_TYPES_H_
#define ERP_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define ERP_PATH_BSWINCLUDE( _FILENAME_ ) STRINGIFY(../../include/_FILENAME_)
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Common.h"

struct ERROR_FLAG
{
    uBitType InternalDTCByte0_Bit7              :1;
    uBitType InternalDTCByte0_Bit6              :1;
    uBitType InternalDTCByte0_Bit5              :1;
    uBitType InternalDTCByte0_Bit4              :1;
    uBitType InternalDTCByte0_Bit3              :1;
    uBitType InternalDTCByte0_Bit2              :1;
    uBitType InternalDTCByte0_Bit1              :1;
    uBitType InternalDTCByte0_Bit0              :1;

    uBitType InternalDTCByte1_Bit7              :1;
    uBitType InternalDTCByte1_Bit6              :1;
    uBitType InternalDTCByte1_Bit5              :1;
    uBitType InternalDTCByte1_Bit4              :1;
    uBitType InternalDTCByte1_Bit3              :1;
    uBitType InternalDTCByte1_Bit2              :1;
    uBitType InternalDTCByte1_Bit1              :1;
    uBitType InternalDTCByte1_Bit0              :1;

    uBitType InternalDTCByte2_Bit7              :1;
    uBitType InternalDTCByte2_Bit6              :1;
    uBitType InternalDTCByte2_Bit5              :1;
    uBitType InternalDTCByte2_Bit4              :1;
    uBitType InternalDTCByte2_Bit3              :1;
    uBitType InternalDTCByte2_Bit2              :1;
    uBitType InternalDTCByte2_Bit1              :1;
    uBitType InternalDTCByte2_Bit0              :1;

    uBitType InternalDTCByte3_Bit7              :1;
    uBitType InternalDTCByte3_Bit6              :1;
    uBitType InternalDTCByte3_Bit5              :1;
    uBitType InternalDTCByte3_Bit4              :1;
    uBitType InternalDTCByte3_Bit3              :1;
    uBitType InternalDTCByte3_Bit2              :1;
    uBitType InternalDTCByte3_Bit1              :1;
    uBitType InternalDTCByte3_Bit0              :1;

    uBitType InternalDTCByte4_Bit7              :1;
    uBitType InternalDTCByte4_Bit6              :1;
    uBitType InternalDTCByte4_Bit5              :1;
    uBitType InternalDTCByte4_Bit4              :1;
    uBitType InternalDTCByte4_Bit3              :1;
    uBitType InternalDTCByte4_Bit2              :1;
    uBitType InternalDTCByte4_Bit1              :1;
    uBitType InternalDTCByte4_Bit0              :1;

    uBitType InternalDTCByte5_Bit7              :1;
    uBitType InternalDTCByte5_Bit6              :1;
    uBitType InternalDTCByte5_Bit5              :1;
    uBitType InternalDTCByte5_Bit4              :1;
    uBitType InternalDTCByte5_Bit3              :1;
    uBitType InternalDTCByte5_Bit2              :1;
    uBitType InternalDTCByte5_Bit1              :1;
    uBitType InternalDTCByte5_Bit0              :1;

    uBitType InternalDTCByte6_Bit7              :1;
    uBitType InternalDTCByte6_Bit6              :1;
    uBitType InternalDTCByte6_Bit5              :1;
    uBitType InternalDTCByte6_Bit4              :1;
    uBitType InternalDTCByte6_Bit3              :1;
    uBitType InternalDTCByte6_Bit2              :1;
    uBitType InternalDTCByte6_Bit1              :1;
    uBitType InternalDTCByte6_Bit0              :1;

    uBitType InternalDTCByte7_Bit7              :1;
    uBitType InternalDTCByte7_Bit6              :1;
    uBitType InternalDTCByte7_Bit5              :1;
    uBitType InternalDTCByte7_Bit4              :1;
    uBitType InternalDTCByte7_Bit3              :1;
    uBitType InternalDTCByte7_Bit2              :1;
    uBitType InternalDTCByte7_Bit1              :1;
    uBitType InternalDTCByte7_Bit0              :1;

    uBitType InternalDTCByte8_Bit7              :1;
    uBitType InternalDTCByte8_Bit6              :1;
    uBitType InternalDTCByte8_Bit5              :1;
    uBitType InternalDTCByte8_Bit4              :1;
    uBitType InternalDTCByte8_Bit3              :1;
    uBitType InternalDTCByte8_Bit2              :1;
    uBitType InternalDTCByte8_Bit1              :1;
    uBitType InternalDTCByte8_Bit0              :1;

    uBitType InternalDTCByte9_Bit7              :1;
    uBitType InternalDTCByte9_Bit6              :1;
    uBitType InternalDTCByte9_Bit5              :1;
    uBitType InternalDTCByte9_Bit4              :1;
    uBitType InternalDTCByte9_Bit3              :1;
    uBitType InternalDTCByte9_Bit2              :1;
    uBitType InternalDTCByte9_Bit1              :1;
    uBitType InternalDTCByte9_Bit0              :1;

    uBitType InternalDTCByte10_Bit7             :1;
    uBitType InternalDTCByte10_Bit6             :1;
    uBitType InternalDTCByte10_Bit5             :1;
    uBitType InternalDTCByte10_Bit4             :1;
    uBitType InternalDTCByte10_Bit3             :1;
    uBitType InternalDTCByte10_Bit2             :1;
    uBitType InternalDTCByte10_Bit1             :1;
    uBitType InternalDTCByte10_Bit0             :1;

    uBitType InternalDTCByte11_Bit7             :1;
    uBitType InternalDTCByte11_Bit6             :1;
    uBitType InternalDTCByte11_Bit5             :1;
    uBitType InternalDTCByte11_Bit4             :1;
    uBitType InternalDTCByte11_Bit3             :1;
    uBitType InternalDTCByte11_Bit2             :1;
    uBitType InternalDTCByte11_Bit1             :1;
    uBitType InternalDTCByte11_Bit0             :1;

    uBitType InternalDTCByte12_Bit7             :1;
    uBitType InternalDTCByte12_Bit6             :1;
    uBitType InternalDTCByte12_Bit5             :1;
    uBitType InternalDTCByte12_Bit4             :1;
    uBitType InternalDTCByte12_Bit3             :1;
    uBitType InternalDTCByte12_Bit2             :1;
    uBitType InternalDTCByte12_Bit1             :1;
    uBitType InternalDTCByte12_Bit0             :1;

    uBitType InternalDTCByte13_Bit7             :1;
    uBitType InternalDTCByte13_Bit6             :1;
    uBitType InternalDTCByte13_Bit5             :1;
    uBitType InternalDTCByte13_Bit4             :1;
    uBitType InternalDTCByte13_Bit3             :1;
    uBitType InternalDTCByte13_Bit2             :1;
    uBitType InternalDTCByte13_Bit1             :1;
    uBitType InternalDTCByte13_Bit0             :1;

    uBitType InternalDTCByte14_Bit7             :1;
    uBitType InternalDTCByte14_Bit6             :1;
    uBitType InternalDTCByte14_Bit5             :1;
    uBitType InternalDTCByte14_Bit4             :1;
    uBitType InternalDTCByte14_Bit3             :1;
    uBitType InternalDTCByte14_Bit2             :1;
    uBitType InternalDTCByte14_Bit1             :1;
    uBitType InternalDTCByte14_Bit0             :1;

    uBitType InternalDTCByte15_Bit7             :1;
    uBitType InternalDTCByte15_Bit6             :1;
    uBitType InternalDTCByte15_Bit5             :1;
    uBitType InternalDTCByte15_Bit4             :1;
    uBitType InternalDTCByte15_Bit3             :1;
    uBitType InternalDTCByte15_Bit2             :1;
    uBitType InternalDTCByte15_Bit1             :1;
    uBitType InternalDTCByte15_Bit0             :1;

    uBitType InternalDTCByte16_Bit7             :1;
    uBitType InternalDTCByte16_Bit6             :1;
    uBitType InternalDTCByte16_Bit5             :1;
    uBitType InternalDTCByte16_Bit4             :1;
    uBitType InternalDTCByte16_Bit3             :1;
    uBitType InternalDTCByte16_Bit2             :1;
    uBitType InternalDTCByte16_Bit1             :1;
    uBitType InternalDTCByte16_Bit0             :1;

    uBitType InternalDTCByte17_Bit7             :1;
    uBitType InternalDTCByte17_Bit6             :1;
    uBitType InternalDTCByte17_Bit5             :1;
    uBitType InternalDTCByte17_Bit4             :1;
    uBitType InternalDTCByte17_Bit3             :1;
    uBitType InternalDTCByte17_Bit2             :1;
    uBitType InternalDTCByte17_Bit1             :1;
    uBitType InternalDTCByte17_Bit0             :1;

    uBitType InternalDTCByte18_Bit7             :1;
    uBitType InternalDTCByte18_Bit6             :1;
    uBitType InternalDTCByte18_Bit5             :1;
    uBitType InternalDTCByte18_Bit4             :1;
    uBitType InternalDTCByte18_Bit3             :1;
    uBitType InternalDTCByte18_Bit2             :1;
    uBitType InternalDTCByte18_Bit1             :1;
    uBitType InternalDTCByte18_Bit0             :1;

    uBitType InternalDTCByte19_Bit7             :1;
    uBitType InternalDTCByte19_Bit6             :1;
    uBitType InternalDTCByte19_Bit5             :1;
    uBitType InternalDTCByte19_Bit4             :1;
    uBitType InternalDTCByte19_Bit3             :1;
    uBitType InternalDTCByte19_Bit2             :1;
    uBitType InternalDTCByte19_Bit1             :1;
    uBitType InternalDTCByte19_Bit0             :1;

    uBitType InternalDTCByte20_Bit7             :1;
    uBitType InternalDTCByte20_Bit6             :1;
    uBitType InternalDTCByte20_Bit5             :1;
    uBitType InternalDTCByte20_Bit4             :1;
    uBitType InternalDTCByte20_Bit3             :1;
    uBitType InternalDTCByte20_Bit2             :1;
    uBitType InternalDTCByte20_Bit1             :1;
    uBitType InternalDTCByte20_Bit0             :1;

    uBitType InternalDTCByte21_Bit7             :1;
    uBitType InternalDTCByte21_Bit6             :1;
    uBitType InternalDTCByte21_Bit5             :1;
    uBitType InternalDTCByte21_Bit4             :1;
    uBitType InternalDTCByte21_Bit3             :1;
    uBitType InternalDTCByte21_Bit2             :1;
    uBitType InternalDTCByte21_Bit1             :1;
    uBitType InternalDTCByte21_Bit0             :1;

    uBitType InternalDTCByte22_Bit7             :1;
    uBitType InternalDTCByte22_Bit6             :1;
    uBitType InternalDTCByte22_Bit5             :1;
    uBitType InternalDTCByte22_Bit4             :1;
    uBitType InternalDTCByte22_Bit3             :1;
    uBitType InternalDTCByte22_Bit2             :1;
    uBitType InternalDTCByte22_Bit1             :1;
    uBitType InternalDTCByte22_Bit0             :1;

    uBitType InternalDTCByte23_Bit7             :1;
    uBitType InternalDTCByte23_Bit6             :1;
    uBitType InternalDTCByte23_Bit5             :1;
    uBitType InternalDTCByte23_Bit4             :1;
    uBitType InternalDTCByte23_Bit3             :1;
    uBitType InternalDTCByte23_Bit2             :1;
    uBitType InternalDTCByte23_Bit1             :1;
    uBitType InternalDTCByte23_Bit0             :1;

    uBitType InternalDTCByte24_Bit7             :1;
    uBitType InternalDTCByte24_Bit6             :1;
    uBitType InternalDTCByte24_Bit5             :1;
    uBitType InternalDTCByte24_Bit4             :1;
    uBitType InternalDTCByte24_Bit3             :1;
    uBitType InternalDTCByte24_Bit2             :1;
    uBitType InternalDTCByte24_Bit1             :1;
    uBitType InternalDTCByte24_Bit0             :1;

    uBitType InternalDTCByte25_Bit7             :1;
    uBitType InternalDTCByte25_Bit6             :1;
    uBitType InternalDTCByte25_Bit5             :1;
    uBitType InternalDTCByte25_Bit4             :1;
    uBitType InternalDTCByte25_Bit3             :1;
    uBitType InternalDTCByte25_Bit2             :1;
    uBitType InternalDTCByte25_Bit1             :1;
    uBitType InternalDTCByte25_Bit0             :1;

    uBitType InternalDTCByte26_Bit7             :1;
    uBitType InternalDTCByte26_Bit6             :1;
    uBitType InternalDTCByte26_Bit5             :1;
    uBitType InternalDTCByte26_Bit4             :1;
    uBitType InternalDTCByte26_Bit3             :1;
    uBitType InternalDTCByte26_Bit2             :1;
    uBitType InternalDTCByte26_Bit1             :1;
    uBitType InternalDTCByte26_Bit0             :1;

    uBitType InternalDTCByte27_Bit7             :1;
    uBitType InternalDTCByte27_Bit6             :1;
    uBitType InternalDTCByte27_Bit5             :1;
    uBitType InternalDTCByte27_Bit4             :1;
    uBitType InternalDTCByte27_Bit3             :1;
    uBitType InternalDTCByte27_Bit2             :1;
    uBitType InternalDTCByte27_Bit1             :1;
    uBitType InternalDTCByte27_Bit0             :1;

    uBitType InternalDTCByte28_Bit7             :1;
    uBitType InternalDTCByte28_Bit6             :1;
    uBitType InternalDTCByte28_Bit5             :1;
    uBitType InternalDTCByte28_Bit4             :1;
    uBitType InternalDTCByte28_Bit3             :1;
    uBitType InternalDTCByte28_Bit2             :1;
    uBitType InternalDTCByte28_Bit1             :1;
    uBitType InternalDTCByte28_Bit0             :1;

    uBitType InternalDTCByte29_Bit7             :1;
    uBitType InternalDTCByte29_Bit6             :1;
    uBitType InternalDTCByte29_Bit5             :1;
    uBitType InternalDTCByte29_Bit4             :1;
    uBitType InternalDTCByte29_Bit3             :1;
    uBitType InternalDTCByte29_Bit2             :1;
    uBitType InternalDTCByte29_Bit1             :1;
    uBitType InternalDTCByte29_Bit0             :1;

    uBitType InternalDTCByte30_Bit7             :1;
    uBitType InternalDTCByte30_Bit6             :1;
    uBitType InternalDTCByte30_Bit5             :1;
    uBitType InternalDTCByte30_Bit4             :1;
    uBitType InternalDTCByte30_Bit3             :1;
    uBitType InternalDTCByte30_Bit2             :1;
    uBitType InternalDTCByte30_Bit1             :1;
    uBitType InternalDTCByte30_Bit0             :1;

    uBitType InternalDTCByte31_Bit7             :1;
    uBitType InternalDTCByte31_Bit6             :1;
    uBitType InternalDTCByte31_Bit5             :1;
    uBitType InternalDTCByte31_Bit4             :1;
    uBitType InternalDTCByte31_Bit3             :1;
    uBitType InternalDTCByte31_Bit2             :1;
    uBitType InternalDTCByte31_Bit1             :1;
    uBitType InternalDTCByte31_Bit0             :1;

    uBitType InternalDTCByte32_Bit7             :1;
    uBitType InternalDTCByte32_Bit6             :1;
    uBitType InternalDTCByte32_Bit5             :1;
    uBitType InternalDTCByte32_Bit4             :1;
    uBitType InternalDTCByte32_Bit3             :1;
    uBitType InternalDTCByte32_Bit2             :1;
    uBitType InternalDTCByte32_Bit1             :1;
    uBitType InternalDTCByte32_Bit0             :1;

    uBitType InternalDTCByte33_Bit7             :1;
    uBitType InternalDTCByte33_Bit6             :1;
    uBitType InternalDTCByte33_Bit5             :1;
    uBitType InternalDTCByte33_Bit4             :1;
    uBitType InternalDTCByte33_Bit3             :1;
    uBitType InternalDTCByte33_Bit2             :1;
    uBitType InternalDTCByte33_Bit1             :1;
    uBitType InternalDTCByte33_Bit0             :1;

    uBitType InternalDTCByte34_Bit7             :1;
    uBitType InternalDTCByte34_Bit6             :1;
    uBitType InternalDTCByte34_Bit5             :1;
    uBitType InternalDTCByte34_Bit4             :1;
    uBitType InternalDTCByte34_Bit3             :1;
    uBitType InternalDTCByte34_Bit2             :1;
    uBitType InternalDTCByte34_Bit1             :1;
    uBitType InternalDTCByte34_Bit0             :1;

    uBitType InternalDTCByte35_Bit7             :1;
    uBitType InternalDTCByte35_Bit6             :1;
    uBitType InternalDTCByte35_Bit5             :1;
    uBitType   InternalDTCByte35_Bit4             :1;
    uBitType   InternalDTCByte35_Bit3             :1;
    uBitType   InternalDTCByte35_Bit2             :1;
    uBitType   InternalDTCByte35_Bit1             :1;
    uBitType   InternalDTCByte35_Bit0             :1;

    uBitType   InternalDTCByte36_Bit7             :1;
    uBitType   InternalDTCByte36_Bit6             :1;
    uBitType   InternalDTCByte36_Bit5             :1;
    uBitType   InternalDTCByte36_Bit4             :1;
    uBitType   InternalDTCByte36_Bit3             :1;
    uBitType   InternalDTCByte36_Bit2             :1;
    uBitType   InternalDTCByte36_Bit1             :1;
    uBitType   InternalDTCByte36_Bit0             :1;

    uBitType   InternalDTCByte37_Bit7             :1;
    uBitType   InternalDTCByte37_Bit6             :1;
    uBitType   InternalDTCByte37_Bit5             :1;
    uBitType   InternalDTCByte37_Bit4             :1;
    uBitType   InternalDTCByte37_Bit3             :1;
    uBitType   InternalDTCByte37_Bit2             :1;
    uBitType   InternalDTCByte37_Bit1             :1;
    uBitType   InternalDTCByte37_Bit0             :1;

    uBitType   InternalDTCByte38_Bit7             :1;
    uBitType   InternalDTCByte38_Bit6             :1;
    uBitType   InternalDTCByte38_Bit5             :1;
    uBitType   InternalDTCByte38_Bit4             :1;
    uBitType   InternalDTCByte38_Bit3             :1;
    uBitType   InternalDTCByte38_Bit2             :1;
    uBitType   InternalDTCByte38_Bit1             :1;
    uBitType   InternalDTCByte38_Bit0             :1;

    uBitType   InternalDTCByte39_Bit7             :1;
    uBitType   InternalDTCByte39_Bit6             :1;
    uBitType   InternalDTCByte39_Bit5             :1;
    uBitType   InternalDTCByte39_Bit4             :1;
    uBitType   InternalDTCByte39_Bit3             :1;
    uBitType   InternalDTCByte39_Bit2             :1;
    uBitType   InternalDTCByte39_Bit1             :1;
    uBitType   InternalDTCByte39_Bit0             :1;

    uBitType   InternalDTCByte40_Bit7             :1;
    uBitType   InternalDTCByte40_Bit6             :1;
    uBitType   InternalDTCByte40_Bit5             :1;
    uBitType   InternalDTCByte40_Bit4             :1;
    uBitType   InternalDTCByte40_Bit3             :1;
    uBitType   InternalDTCByte40_Bit2             :1;
    uBitType   InternalDTCByte40_Bit1             :1;
    uBitType   InternalDTCByte40_Bit0             :1;

    uBitType   InternalDTCByte41_Bit7             :1;
    uBitType   InternalDTCByte41_Bit6             :1;
    uBitType   InternalDTCByte41_Bit5             :1;
    uBitType   InternalDTCByte41_Bit4             :1;
    uBitType   InternalDTCByte41_Bit3             :1;
    uBitType   InternalDTCByte41_Bit2             :1;
    uBitType   InternalDTCByte41_Bit1             :1;
    uBitType   InternalDTCByte41_Bit0             :1;

    uBitType   InternalDTCByte42_Bit7             :1;
    uBitType   InternalDTCByte42_Bit6             :1;
    uBitType   InternalDTCByte42_Bit5             :1;
    uBitType   InternalDTCByte42_Bit4             :1;
    uBitType   InternalDTCByte42_Bit3             :1;
    uBitType   InternalDTCByte42_Bit2             :1;
    uBitType   InternalDTCByte42_Bit1             :1;
    uBitType   InternalDTCByte42_Bit0             :1;

    uBitType   InternalDTCByte43_Bit7             :1;
    uBitType   InternalDTCByte43_Bit6             :1;
    uBitType   InternalDTCByte43_Bit5             :1;
    uBitType   InternalDTCByte43_Bit4             :1;
    uBitType   InternalDTCByte43_Bit3             :1;
    uBitType   InternalDTCByte43_Bit2             :1;
    uBitType   InternalDTCByte43_Bit1             :1;
    uBitType   InternalDTCByte43_Bit0             :1;

    uBitType   InternalDTCByte44_Bit7             :1;
    uBitType   InternalDTCByte44_Bit6             :1;
    uBitType   InternalDTCByte44_Bit5             :1;
    uBitType   InternalDTCByte44_Bit4             :1;
    uBitType   InternalDTCByte44_Bit3             :1;
    uBitType   InternalDTCByte44_Bit2             :1;
    uBitType   InternalDTCByte44_Bit1             :1;
    uBitType   InternalDTCByte44_Bit0             :1;

    uBitType   InternalDTCByte45_Bit7             :1;
    uBitType   InternalDTCByte45_Bit6             :1;
    uBitType   InternalDTCByte45_Bit5             :1;
    uBitType   InternalDTCByte45_Bit4             :1;
    uBitType   InternalDTCByte45_Bit3             :1;
    uBitType   InternalDTCByte45_Bit2             :1;
    uBitType   InternalDTCByte45_Bit1             :1;
    uBitType   InternalDTCByte45_Bit0             :1;

    uBitType   InternalDTCByte46_Bit7             :1;
    uBitType   InternalDTCByte46_Bit6             :1;
    uBitType   InternalDTCByte46_Bit5             :1;
    uBitType   InternalDTCByte46_Bit4             :1;
    uBitType   InternalDTCByte46_Bit3             :1;
    uBitType   InternalDTCByte46_Bit2             :1;
    uBitType   InternalDTCByte46_Bit1             :1;
    uBitType   InternalDTCByte46_Bit0             :1;

    uBitType   InternalDTCByte47_Bit7             :1;
    uBitType   InternalDTCByte47_Bit6             :1;
    uBitType   InternalDTCByte47_Bit5             :1;
    uBitType   InternalDTCByte47_Bit4             :1;
    uBitType   InternalDTCByte47_Bit3             :1;
    uBitType   InternalDTCByte47_Bit2             :1;
    uBitType   InternalDTCByte47_Bit1             :1;
    uBitType   InternalDTCByte47_Bit0             :1;

    uBitType   InternalDTCByte48_Bit7             :1;
    uBitType   InternalDTCByte48_Bit6             :1;
    uBitType   InternalDTCByte48_Bit5             :1;
    uBitType   InternalDTCByte48_Bit4             :1;
    uBitType   InternalDTCByte48_Bit3             :1;
    uBitType   InternalDTCByte48_Bit2             :1;
    uBitType   InternalDTCByte48_Bit1             :1;
    uBitType   InternalDTCByte48_Bit0             :1;

    uBitType   InternalDTCByte49_Bit7             :1;
    uBitType   InternalDTCByte49_Bit6             :1;
    uBitType   InternalDTCByte49_Bit5             :1;
    uBitType   InternalDTCByte49_Bit4             :1;
    uBitType   InternalDTCByte49_Bit3             :1;
    uBitType   InternalDTCByte49_Bit2             :1;
    uBitType   InternalDTCByte49_Bit1             :1;
    uBitType   InternalDTCByte49_Bit0             :1;

    uBitType   InternalDTCByte50_Bit7             :1;
    uBitType   InternalDTCByte50_Bit6             :1;
    uBitType   InternalDTCByte50_Bit5             :1;
    uBitType   InternalDTCByte50_Bit4             :1;
    uBitType   InternalDTCByte50_Bit3             :1;
    uBitType   InternalDTCByte50_Bit2             :1;
    uBitType   InternalDTCByte50_Bit1             :1;
    uBitType   InternalDTCByte50_Bit0             :1;

    uBitType   InternalDTCByte51_Bit7             :1;
    uBitType   InternalDTCByte51_Bit6             :1;
    uBitType   InternalDTCByte51_Bit5             :1;
    uBitType   InternalDTCByte51_Bit4             :1;
    uBitType   InternalDTCByte51_Bit3             :1;
    uBitType   InternalDTCByte51_Bit2             :1;
    uBitType   InternalDTCByte51_Bit1             :1;
    uBitType   InternalDTCByte51_Bit0             :1;

    uBitType   InternalDTCByte52_Bit7             :1;
    uBitType   InternalDTCByte52_Bit6             :1;
    uBitType   InternalDTCByte52_Bit5             :1;
    uBitType   InternalDTCByte52_Bit4             :1;
    uBitType   InternalDTCByte52_Bit3             :1;
    uBitType   InternalDTCByte52_Bit2             :1;
    uBitType   InternalDTCByte52_Bit1             :1;
    uBitType   InternalDTCByte52_Bit0             :1;

    uBitType   InternalDTCByte53_Bit7             :1;
    uBitType   InternalDTCByte53_Bit6             :1;
    uBitType   InternalDTCByte53_Bit5             :1;
    uBitType   InternalDTCByte53_Bit4             :1;
    uBitType   InternalDTCByte53_Bit3             :1;
    uBitType   InternalDTCByte53_Bit2             :1;
    uBitType   InternalDTCByte53_Bit1             :1;
    uBitType   InternalDTCByte53_Bit0             :1;

    uBitType   InternalDTCByte54_Bit7             :1;
    uBitType   InternalDTCByte54_Bit6             :1;
    uBitType   InternalDTCByte54_Bit5             :1;
    uBitType   InternalDTCByte54_Bit4             :1;
    uBitType   InternalDTCByte54_Bit3             :1;
    uBitType   InternalDTCByte54_Bit2             :1;
    uBitType   InternalDTCByte54_Bit1             :1;
    uBitType   InternalDTCByte54_Bit0             :1;

    uBitType   InternalDTCByte55_Bit7             :1;
    uBitType   InternalDTCByte55_Bit6             :1;
    uBitType   InternalDTCByte55_Bit5             :1;
    uBitType   InternalDTCByte55_Bit4             :1;
    uBitType   InternalDTCByte55_Bit3             :1;
    uBitType   InternalDTCByte55_Bit2             :1;
    uBitType   InternalDTCByte55_Bit1             :1;
    uBitType   InternalDTCByte55_Bit0             :1;

    uBitType   InternalDTCByte56_Bit7             :1;
    uBitType   InternalDTCByte56_Bit6             :1;
    uBitType   InternalDTCByte56_Bit5             :1;
    uBitType   InternalDTCByte56_Bit4             :1;
    uBitType   InternalDTCByte56_Bit3             :1;
    uBitType   InternalDTCByte56_Bit2             :1;
    uBitType   InternalDTCByte56_Bit1             :1;
    uBitType   InternalDTCByte56_Bit0             :1;

    uBitType   InternalDTCByte57_Bit7             :1;
    uBitType   InternalDTCByte57_Bit6             :1;
    uBitType   InternalDTCByte57_Bit5             :1;
    uBitType   InternalDTCByte57_Bit4             :1;
    uBitType   InternalDTCByte57_Bit3             :1;
    uBitType   InternalDTCByte57_Bit2             :1;
    uBitType   InternalDTCByte57_Bit1             :1;
    uBitType   InternalDTCByte57_Bit0             :1;

    uBitType   InternalDTCByte58_Bit7             :1;
    uBitType   InternalDTCByte58_Bit6             :1;
    uBitType   InternalDTCByte58_Bit5             :1;
    uBitType   InternalDTCByte58_Bit4             :1;
    uBitType   InternalDTCByte58_Bit3             :1;
    uBitType   InternalDTCByte58_Bit2             :1;
    uBitType   InternalDTCByte58_Bit1             :1;
    uBitType   InternalDTCByte58_Bit0             :1;

    uBitType   InternalDTCByte59_Bit7             :1;
    uBitType   InternalDTCByte59_Bit6             :1;
    uBitType   InternalDTCByte59_Bit5             :1;
    uBitType   InternalDTCByte59_Bit4             :1;
    uBitType   InternalDTCByte59_Bit3             :1;
    uBitType   InternalDTCByte59_Bit2             :1;
    uBitType   InternalDTCByte59_Bit1             :1;
    uBitType   InternalDTCByte59_Bit0             :1;

    uBitType   InternalDTCByte60_Bit7             :1;
    uBitType   InternalDTCByte60_Bit6             :1;
    uBitType   InternalDTCByte60_Bit5             :1;
    uBitType   InternalDTCByte60_Bit4             :1;
    uBitType   InternalDTCByte60_Bit3             :1;
    uBitType   InternalDTCByte60_Bit2             :1;
    uBitType   InternalDTCByte60_Bit1             :1;
    uBitType   InternalDTCByte60_Bit0             :1;

    uBitType   InternalDTCByte61_Bit7             :1;
    uBitType   InternalDTCByte61_Bit6             :1;
    uBitType   InternalDTCByte61_Bit5             :1;
    uBitType   InternalDTCByte61_Bit4             :1;
    uBitType   InternalDTCByte61_Bit3             :1;
    uBitType   InternalDTCByte61_Bit2             :1;
    uBitType   InternalDTCByte61_Bit1             :1;
    uBitType   InternalDTCByte61_Bit0             :1;

    uBitType   InternalDTCByte62_Bit7             :1;
    uBitType   InternalDTCByte62_Bit6             :1;
    uBitType   InternalDTCByte62_Bit5             :1;
    uBitType   InternalDTCByte62_Bit4             :1;
    uBitType   InternalDTCByte62_Bit3             :1;
    uBitType   InternalDTCByte62_Bit2             :1;
    uBitType   InternalDTCByte62_Bit1             :1;
    uBitType   InternalDTCByte62_Bit0             :1;

    uBitType   InternalDTCByte63_Bit7             :1;
    uBitType   InternalDTCByte63_Bit6             :1;
    uBitType   InternalDTCByte63_Bit5             :1;
    uBitType   InternalDTCByte63_Bit4             :1;
    uBitType   InternalDTCByte63_Bit3             :1;
    uBitType   InternalDTCByte63_Bit2             :1;
    uBitType   InternalDTCByte63_Bit1             :1;
    uBitType   InternalDTCByte63_Bit0             :1;

    uBitType   InternalDTCByte64_Bit7             :1;
    uBitType   InternalDTCByte64_Bit6             :1;
    uBitType   InternalDTCByte64_Bit5             :1;
    uBitType   InternalDTCByte64_Bit4             :1;
    uBitType   InternalDTCByte64_Bit3             :1;
    uBitType   InternalDTCByte64_Bit2             :1;
    uBitType   InternalDTCByte64_Bit1             :1;
    uBitType   InternalDTCByte64_Bit0             :1;

    uBitType   InternalDTCByte65_Bit7             :1;
    uBitType   InternalDTCByte65_Bit6             :1;
    uBitType   InternalDTCByte65_Bit5             :1;
    uBitType   InternalDTCByte65_Bit4             :1;
    uBitType   InternalDTCByte65_Bit3             :1;
    uBitType   InternalDTCByte65_Bit2             :1;
    uBitType   InternalDTCByte65_Bit1             :1;
    uBitType   InternalDTCByte65_Bit0             :1;

    uBitType   InternalDTCByte66_Bit7             :1;
    uBitType   InternalDTCByte66_Bit6             :1;
    uBitType   InternalDTCByte66_Bit5             :1;
    uBitType   InternalDTCByte66_Bit4             :1;
    uBitType   InternalDTCByte66_Bit3             :1;
    uBitType   InternalDTCByte66_Bit2             :1;
    uBitType   InternalDTCByte66_Bit1             :1;
    uBitType   InternalDTCByte66_Bit0             :1;

    uBitType   InternalDTCByte67_Bit7             :1;
    uBitType   InternalDTCByte67_Bit6             :1;
    uBitType   InternalDTCByte67_Bit5             :1;
    uBitType   InternalDTCByte67_Bit4             :1;
    uBitType   InternalDTCByte67_Bit3             :1;
    uBitType   InternalDTCByte67_Bit2             :1;
    uBitType   InternalDTCByte67_Bit1             :1;
    uBitType   InternalDTCByte67_Bit0             :1;

    uBitType   InternalDTCByte68_Bit7             :1;
    uBitType   InternalDTCByte68_Bit6             :1;
    uBitType   InternalDTCByte68_Bit5             :1;
    uBitType   InternalDTCByte68_Bit4             :1;
    uBitType   InternalDTCByte68_Bit3             :1;
    uBitType   InternalDTCByte68_Bit2             :1;
    uBitType   InternalDTCByte68_Bit1             :1;
    uBitType   InternalDTCByte68_Bit0             :1;

    uBitType   InternalDTCByte69_Bit7             :1;
    uBitType   InternalDTCByte69_Bit6             :1;
    uBitType   InternalDTCByte69_Bit5             :1;
    uBitType   InternalDTCByte69_Bit4             :1;
    uBitType   InternalDTCByte69_Bit3             :1;
    uBitType   InternalDTCByte69_Bit2             :1;
    uBitType   InternalDTCByte69_Bit1             :1;
    uBitType   InternalDTCByte69_Bit0             :1;

    uBitType   InternalDTCByte70_Bit7             :1;
    uBitType   InternalDTCByte70_Bit6             :1;
    uBitType   InternalDTCByte70_Bit5             :1;
    uBitType   InternalDTCByte70_Bit4             :1;
    uBitType   InternalDTCByte70_Bit3             :1;
    uBitType   InternalDTCByte70_Bit2             :1;
    uBitType   InternalDTCByte70_Bit1             :1;
    uBitType   InternalDTCByte70_Bit0             :1;

    uBitType   InternalDTCByte71_Bit7             :1;
    uBitType   InternalDTCByte71_Bit6             :1;
    uBitType   InternalDTCByte71_Bit5             :1;
    uBitType   InternalDTCByte71_Bit4             :1;
    uBitType   InternalDTCByte71_Bit3             :1;
    uBitType   InternalDTCByte71_Bit2             :1;
    uBitType   InternalDTCByte71_Bit1             :1;
    uBitType   InternalDTCByte71_Bit0             :1;

    uBitType   InternalDTCByte72_Bit7             :1;
    uBitType   InternalDTCByte72_Bit6             :1;
    uBitType   InternalDTCByte72_Bit5             :1;
    uBitType   InternalDTCByte72_Bit4             :1;
    uBitType   InternalDTCByte72_Bit3             :1;
    uBitType   InternalDTCByte72_Bit2             :1;
    uBitType   InternalDTCByte72_Bit1             :1;
    uBitType   InternalDTCByte72_Bit0             :1;

    uBitType   InternalDTCByte73_Bit7             :1;
    uBitType   InternalDTCByte73_Bit6             :1;
    uBitType   InternalDTCByte73_Bit5             :1;
    uBitType   InternalDTCByte73_Bit4             :1;
    uBitType   InternalDTCByte73_Bit3             :1;
    uBitType   InternalDTCByte73_Bit2             :1;
    uBitType   InternalDTCByte73_Bit1             :1;
    uBitType   InternalDTCByte73_Bit0             :1;

    uBitType   InternalDTCByte74_Bit7             :1;
    uBitType   InternalDTCByte74_Bit6             :1;
    uBitType   InternalDTCByte74_Bit5             :1;
    uBitType   InternalDTCByte74_Bit4             :1;
    uBitType   InternalDTCByte74_Bit3             :1;
    uBitType   InternalDTCByte74_Bit2             :1;
    uBitType   InternalDTCByte74_Bit1             :1;
    uBitType   InternalDTCByte74_Bit0             :1;

};

#define WHEEL_FL_ERROR_BYTE                             0
#define fl_airgap_flg                                   EF->InternalDTCByte0_Bit0
#define fl_phase_flg                                    EF->InternalDTCByte0_Bit1
#define fl_mjump_flg                                    EF->InternalDTCByte0_Bit2
#define fl_pjump_flg                                    EF->InternalDTCByte0_Bit3
#define exciter_l_fl_flg                                EF->InternalDTCByte0_Bit4
#define exciter_h_fl_flg                                EF->InternalDTCByte0_Bit5
#define fl_broken_teeth_err_flg                         EF->InternalDTCByte0_Bit6
#define fl_split_teeth_err_flg                          EF->InternalDTCByte0_Bit7

#define WHEEL_FR_ERROR_BYTE                             1
#define fr_airgap_flg                                   EF->InternalDTCByte1_Bit0
#define fr_phase_flg                                    EF->InternalDTCByte1_Bit1
#define fr_mjump_flg                                    EF->InternalDTCByte1_Bit2
#define fr_pjump_flg                                    EF->InternalDTCByte1_Bit3
#define exciter_l_fr_flg                                EF->InternalDTCByte1_Bit4
#define exciter_h_fr_flg                                EF->InternalDTCByte1_Bit5
#define fr_broken_teeth_err_flg                         EF->InternalDTCByte1_Bit6
#define fr_split_teeth_err_flg                          EF->InternalDTCByte1_Bit7

#define WHEEL_RL_ERROR_BYTE                             2
#define rl_airgap_flg                                   EF->InternalDTCByte2_Bit0
#define rl_phase_flg                                    EF->InternalDTCByte2_Bit1
#define rl_mjump_flg                                    EF->InternalDTCByte2_Bit2
#define rl_pjump_flg                                    EF->InternalDTCByte2_Bit3
#define exciter_l_rl_flg                                EF->InternalDTCByte2_Bit4
#define exciter_h_rl_flg                                EF->InternalDTCByte2_Bit5
#define rl_broken_teeth_err_flg                         EF->InternalDTCByte2_Bit6
#define rl_split_teeth_err_flg                          EF->InternalDTCByte2_Bit7

#define WHEEL_RR_ERROR_BYTE                             3
#define rr_airgap_flg                                   EF->InternalDTCByte3_Bit0
#define rr_phase_flg                                    EF->InternalDTCByte3_Bit1
#define rr_mjump_flg                                    EF->InternalDTCByte3_Bit2
#define rr_pjump_flg                                    EF->InternalDTCByte3_Bit3
#define exciter_l_rr_flg                                EF->InternalDTCByte3_Bit4
#define exciter_h_rr_flg                                EF->InternalDTCByte3_Bit5
#define rr_broken_teeth_err_flg                         EF->InternalDTCByte3_Bit6
#define rr_split_teeth_err_flg                          EF->InternalDTCByte3_Bit7

#define WHEEL_HW_ERROR_BYTE                             4
#define fl_hw_open_flg                                  EF->InternalDTCByte4_Bit0
#define fl_hw_short_flg                                 EF->InternalDTCByte4_Bit1
#define fr_hw_open_flg                                  EF->InternalDTCByte4_Bit2
#define fr_hw_short_flg                                 EF->InternalDTCByte4_Bit3
#define rl_hw_open_flg                                  EF->InternalDTCByte4_Bit4
#define rl_hw_short_flg                                 EF->InternalDTCByte4_Bit5
#define rr_hw_open_flg                                  EF->InternalDTCByte4_Bit6
#define rr_hw_short_flg                                 EF->InternalDTCByte4_Bit7

#define WHEEL_ADDITIONAL_ERROR_BYTE                     5
#define fl_lkg_flg                                      EF->InternalDTCByte5_Bit0
#define fl_hw_overtemp_flg                              EF->InternalDTCByte5_Bit1
#define fr_lkg_flg                                      EF->InternalDTCByte5_Bit2
#define fr_hw_overtemp_flg                              EF->InternalDTCByte5_Bit3
#define rl_lkg_flg                                      EF->InternalDTCByte5_Bit4
#define rl_hw_overtemp_flg                              EF->InternalDTCByte5_Bit5
#define rr_lkg_flg                                      EF->InternalDTCByte5_Bit6
#define rr_hw_overtemp_flg                              EF->InternalDTCByte5_Bit7

#define WHEEL_RESERVED_ERROR_BYTE                       6
#define fl_wire_err_flg                                 EF->InternalDTCByte6_Bit0
#define fr_wire_err_flg                                 EF->InternalDTCByte6_Bit1
#define rl_wire_err_flg                                 EF->InternalDTCByte6_Bit2
#define rr_wire_err_flg                                 EF->InternalDTCByte6_Bit3
#define fl_WheelTypeSwap_Err_flg                        EF->InternalDTCByte6_Bit4
#define fr_WheelTypeSwap_Err_flg                        EF->InternalDTCByte6_Bit5
#define rl_WheelTypeSwap_Err_flg                        EF->InternalDTCByte6_Bit6
#define rr_WheelTypeSwap_Err_flg                        EF->InternalDTCByte6_Bit7

#define SOLENOID_FRONT_ERROR_BYTE                       7
#define fro_open_flg                                    EF->InternalDTCByte7_Bit0
#define fro_short_flg                                   EF->InternalDTCByte7_Bit1
#define fri_open_flg                                    EF->InternalDTCByte7_Bit2
#define fri_short_flg                                   EF->InternalDTCByte7_Bit3
#define flo_open_flg                                    EF->InternalDTCByte7_Bit4
#define flo_short_flg                                   EF->InternalDTCByte7_Bit5
#define fli_open_flg                                    EF->InternalDTCByte7_Bit6
#define fli_short_flg                                   EF->InternalDTCByte7_Bit7

#define SOLENOID_REAR_ERROR_BYTE                        8
#define rro_open_flg                                    EF->InternalDTCByte8_Bit0
#define rro_short_flg                                   EF->InternalDTCByte8_Bit1
#define rri_open_flg                                    EF->InternalDTCByte8_Bit2
#define rri_short_flg                                   EF->InternalDTCByte8_Bit3
#define rlo_open_flg                                    EF->InternalDTCByte8_Bit4
#define rlo_short_flg                                   EF->InternalDTCByte8_Bit5
#define rli_open_flg                                    EF->InternalDTCByte8_Bit6
#define rli_short_flg                                   EF->InternalDTCByte8_Bit7

#define SOLENOID_APV_RLV_ERROR_BYTE_0                   9
#define apv1_open_by_curr                               EF->InternalDTCByte9_Bit0
#define apv1_short_by_curr                              EF->InternalDTCByte9_Bit1
#define apv2_open_by_curr                               EF->InternalDTCByte9_Bit2
#define apv2_short_by_curr                              EF->InternalDTCByte9_Bit3
#define rlv1_open_by_curr                               EF->InternalDTCByte9_Bit4
#define rlv1_short_by_curr                              EF->InternalDTCByte9_Bit5
#define rlv2_open_by_curr                               EF->InternalDTCByte9_Bit6
#define rlv2_short_by_curr                              EF->InternalDTCByte9_Bit7

#define SOLENOID_RESERVED_ERROR_BYTE_1                  10
#define fli_overtemp_flg                                EF->InternalDTCByte10_Bit0
#define fri_overtemp_flg                                EF->InternalDTCByte10_Bit1
#define flo_overtemp_flg                                EF->InternalDTCByte10_Bit2
#define fro_overtemp_flg                                EF->InternalDTCByte10_Bit3
#define rli_overtemp_flg                                EF->InternalDTCByte10_Bit4
#define rri_overtemp_flg                                EF->InternalDTCByte10_Bit5
#define rlo_overtemp_flg                                EF->InternalDTCByte10_Bit6
#define rro_overtemp_flg                                EF->InternalDTCByte10_Bit7

#define MOTOR_ERROR_BYTE                                11
#define motor_fuse_open_flg                             EF->InternalDTCByte11_Bit0
#define motor_short_flg                                 EF->InternalDTCByte11_Bit1
#define motor_open_flg                                  EF->InternalDTCByte11_Bit2
#define mr_open_flg                                     EF->InternalDTCByte11_Bit3
#define motor_lock_flg                                  EF->InternalDTCByte11_Bit4
#define motor_overtemp_flg                              EF->InternalDTCByte11_Bit5
#define VBB_Power_error_flg                				EF->InternalDTCByte11_Bit6
#define feu1MotInternalDTCByte11_Bit7                   EF->InternalDTCByte11_Bit7

#define VALVERELAY_ERROR_BYTE_0                         12
#define vr_bypass_flg                                   EF->InternalDTCByte12_Bit0
#define vr_open_flg                                     EF->InternalDTCByte12_Bit1
#define vr_short_g_flg                                  EF->InternalDTCByte12_Bit2
#define vr_lkg_flg                                      EF->InternalDTCByte12_Bit3
#define abs_vr_shutdown_flg                             EF->InternalDTCByte12_Bit4
#define motor_lsd_open_flg                              EF->InternalDTCByte12_Bit5
#define motor_lsd_short_flg                             EF->InternalDTCByte12_Bit6
#define feu1VRInternalDTCByte12_Bit7                    EF->InternalDTCByte12_Bit7

#define INPUT_VOLTAGE_ERROR_BYTE                        13
#define under_v_flg                                     EF->InternalDTCByte13_Bit0
#define over_v_flg                                      EF->InternalDTCByte13_Bit1
#define low_v_flg                                       EF->InternalDTCByte13_Bit2
#define higher_v_flg						            EF->InternalDTCByte13_Bit3
#define feu1InputVoltInternalDTCByte13_Bit4				EF->InternalDTCByte13_Bit4
#define feu1InputVoltInternalDTCByte13_Bit5             EF->InternalDTCByte13_Bit5
#define feu1InputVoltInternalDTCByte13_Bit6             EF->InternalDTCByte13_Bit6
#define feu1InputVoltInternalDTCByte13_Bit7             EF->InternalDTCByte13_Bit7

#define OUTPUT_VOLTAGE_ERROR_BYTE                       14
#define sen_5V_error_flg                                EF->InternalDTCByte14_Bit0
#define sen_12V_open_err_flg                            EF->InternalDTCByte14_Bit1
#define sen_12V_short_err_flg                           EF->InternalDTCByte14_Bit2
#define sen_5VPDF_error_flg								EF->InternalDTCByte14_Bit3
#define sen_5VPDT_error_flg								EF->InternalDTCByte14_Bit4
#define sen_5VP_PSNSR_error_flg 	  					EF->InternalDTCByte14_Bit5
#define ign_line_open_flg                               EF->InternalDTCByte14_Bit6
#define feu1AbsLongTermErr                              EF->InternalDTCByte14_Bit7

#define ECU_ERROR_BYTE_0                                15
#define adc_error_flg                                   EF->InternalDTCByte15_Bit0
#define rom_error_flg                                   EF->InternalDTCByte15_Bit1
#define ram_error_flg                                   EF->InternalDTCByte15_Bit2
#define eeprom_error_flg                                EF->InternalDTCByte15_Bit3
#define cycle_time_error_flg                            EF->InternalDTCByte15_Bit4
#define ecc_pflash_error_flg                            EF->InternalDTCByte15_Bit5
#define valve_init_error_flg                            EF->InternalDTCByte15_Bit6
#define Invalid_error_flg                               EF->InternalDTCByte15_Bit7

#define ECU_ERROR_BYTE_1                                16
#define vdd_ic_err_flg                                  EF->InternalDTCByte16_Bit0
#define CommASIC_init_error_flg                         EF->InternalDTCByte16_Bit1
#define CommASIC_Parity_error_flg                       EF->InternalDTCByte16_Bit2
#define sub_Sync_error_flg                              EF->InternalDTCByte16_Bit3
#define sub_RamRom_error                                EF->InternalDTCByte16_Bit4
#define rollcnt_error_flg                               EF->InternalDTCByte16_Bit5
#define sub_mcu_error_flg                               EF->InternalDTCByte16_Bit6
#define check_sum_error_flg                             EF->InternalDTCByte16_Bit7

#define CAN_ERROR_BYTE_0                                17
#define EMS1_timeout_err_flg                            EF->InternalDTCByte17_Bit0
#define EMS2_timeout_err_flg                            EF->InternalDTCByte17_Bit1
#define TCU1_timeout_err_flg                            EF->InternalDTCByte17_Bit2
#define TCU_unmatch_err_flg                             EF->InternalDTCByte17_Bit3
#define FWD1_timeout_err_flg                            EF->InternalDTCByte17_Bit4
#define CAN_variant_err_flg                             EF->InternalDTCByte17_Bit5
#define BUS_off_err_flg                                 EF->InternalDTCByte17_Bit6
#define CAN_hw_err_flg                                  EF->InternalDTCByte17_Bit7

#define CAN_ERROR_BYTE_1                                18
#define BodyInf_timeout_err_flg                         EF->InternalDTCByte18_Bit0
#define Bls_timeout_err_flg                             EF->InternalDTCByte18_Bit1
#define HCU_timeout_err_flg                             EF->InternalDTCByte18_Bit2
#define SADS_timeout_err_flg                            EF->InternalDTCByte18_Bit3
#define EPB_timeout_err_flg                             EF->InternalDTCByte18_Bit4
#define Swtch_timeout_err_flg                           EF->InternalDTCByte18_Bit5
#define SysPwr_timeout_err_flg                          EF->InternalDTCByte18_Bit6
#define EPS_timeout_err_flg                             EF->InternalDTCByte18_Bit7

#define CAN_ERROR_BYTE_2                                19
#define VACCUM_timeout_err_flg                          EF->InternalDTCByte19_Bit0
#define VACCUM_signal_err_flg                           EF->InternalDTCByte19_Bit1
#define AFS1_timeout_err_flg                            EF->InternalDTCByte19_Bit2
#define ACC240_timeout_err_flg                          EF->InternalDTCByte19_Bit3
#define epb_invalid_serial_err_flg                      EF->InternalDTCByte19_Bit4
#define ecm_invalid_serial_err_flg                      EF->InternalDTCByte19_Bit5
#define tcm_invalid_serial_err_flg                      EF->InternalDTCByte19_Bit6
#define bcm_invalid_serial_err_flg                      EF->InternalDTCByte19_Bit7

#define CAN_ERROR_BYTE_3                                20
#define engine_torq_err_flg                             EF->InternalDTCByte20_Bit0
#define engine_accel_err_flg                            EF->InternalDTCByte20_Bit1
#define transmission_err_flg                            EF->InternalDTCByte20_Bit2
#define awd_cntrl_err_flg                               EF->InternalDTCByte20_Bit3
#define eng_torq_redc_err_flg                           EF->InternalDTCByte20_Bit4
#define bls_invalid_data_err_flg                        EF->InternalDTCByte20_Bit5
#define dissw_invalid_data_err_flg                      EF->InternalDTCByte20_Bit6
#define feu1CanInternalDTCByte20_Bit7                   EF->InternalDTCByte20_Bit7

#define CAN_ERROR_BYTE_4                                21
#define TCU5_timeout_err_flg                            EF->InternalDTCByte21_Bit0
#define HCU1_timeout_err_flg                            EF->InternalDTCByte21_Bit1
#define HCU2_timeout_err_flg                            EF->InternalDTCByte21_Bit2
#define HCU3_timeout_err_flg                            EF->InternalDTCByte21_Bit3
#define bms_timeout_err_flg                             EF->InternalDTCByte21_Bit4
#define bms_invalid_sig_err_flg                         EF->InternalDTCByte21_Bit5
#define MCU_timeout_err_flg                             EF->InternalDTCByte21_Bit6
#define MCU_invalid_sig_err_flg                         EF->InternalDTCByte21_Bit7

#define ASIC_ERROR_BYTE_0                               22
#define comm_asic_und_v_err_flg                         EF->InternalDTCByte22_Bit0
#define comm_asic_over_v_err_flg                        EF->InternalDTCByte22_Bit1
#define comm_asic_gnd_loss_flg                          EF->InternalDTCByte22_Bit2
#define CommASIC_Fmsg_error_flg                         EF->InternalDTCByte22_Bit3
#define feu1ASICInternalDTCByte22_Bit4                  EF->InternalDTCByte22_Bit4
#define comm_asic_freq_err_flg                          EF->InternalDTCByte22_Bit5
#define comm_asic_over_temperature_err_flg              EF->InternalDTCByte22_Bit6
#define comm_asic_version_err_flg                       EF->InternalDTCByte22_Bit7

#define ASIC_ERROR_BYTE_1                               23
#define feu1ASICInternalDTCByte23_Bit0                  EF->InternalDTCByte23_Bit0
#define feu1ASICInternalDTCByte23_Bit1                  EF->InternalDTCByte23_Bit1
#define feu1ASICInternalDTCByte23_Bit2         	 		EF->InternalDTCByte23_Bit2
#define feu1ASICInternalDTCByte23_Bit3			        EF->InternalDTCByte23_Bit3
#define feu1ASICInternalDTCByte23_Bit4                  EF->InternalDTCByte23_Bit4
#define feu1ASICInternalDTCByte23_Bit5                  EF->InternalDTCByte23_Bit5
#define feu1ASICInternalDTCByte23_Bit6                  EF->InternalDTCByte23_Bit6
#define feu1ASICInternalDTCByte23_Bit7                  EF->InternalDTCByte23_Bit7

#define AX_ERROR_BYTE_0                                 24
#define feu1AX_HW_Line_err_flg                          EF->InternalDTCByte24_Bit0
#define feu1CANAX_SW_Roll_err_flg                       EF->InternalDTCByte24_Bit1
#define feu1CANAX_SW_Checksum_err_flg                   EF->InternalDTCByte24_Bit2
#define feu1CANAX_SW_Internal_err_flg                   EF->InternalDTCByte24_Bit3
#define feu1CANAX_Func_err_flg                          EF->InternalDTCByte24_Bit4
#define feu1CANAX_CBIT_err_flg                          EF->InternalDTCByte24_Bit5
#define feu1AX_Calibration_err_flg                      EF->InternalDTCByte24_Bit6
#define feu1AX_SW_Noise_err_flg			                EF->InternalDTCByte24_Bit7

#define AX_ERROR_BYTE_1                                 25
#define feu1AX_SW_StandOffset_err_flg                   EF->InternalDTCByte25_Bit0
#define feu1AX_SW_ShortTermDrvingOffset_err_flg         EF->InternalDTCByte25_Bit1
#define feu1AX_SW_LongTermDrvingOffset_err_flg          EF->InternalDTCByte25_Bit2
#define feu1AX_SW_Stick_err_flg                         EF->InternalDTCByte25_Bit3
#define feu1AX_SW_Reverse_err_flg                       EF->InternalDTCByte25_Bit4
#define feu1AX_SW_Range_err_flg                         EF->InternalDTCByte25_Bit5
#define feu1Ax_Init_Run_err_flg                         EF->InternalDTCByte25_Bit6
#define feu1Sen_Type_err_flg                            EF->InternalDTCByte25_Bit7

#define BLS_ERROR_BYTE_0                                26
#define bls_bs_err_flg                                  EF->InternalDTCByte26_Bit0
#define bls_high_err_flg                                EF->InternalDTCByte26_Bit1
#define bls_low_err_flg                                 EF->InternalDTCByte26_Bit2
#define bls_vref_err_flg                                EF->InternalDTCByte26_Bit3
#define bls_noise_err_flg                               EF->InternalDTCByte26_Bit4
#define bs_noise_err_flg                                EF->InternalDTCByte26_Bit5
#define feuBasNotLearn_err_flg                          EF->InternalDTCByte26_Bit6
#define feu1BLSInternalDTCByte26_Bit7                   EF->InternalDTCByte26_Bit7

#define LAMP_ERROR_BYTE                                 27
#define tcoff_lamp_short_flg                            EF->InternalDTCByte27_Bit0
#define multi_func_short_flg                            EF->InternalDTCByte27_Bit1
#define tcs_vdc_lamp_short_flg                          EF->InternalDTCByte27_Bit2
#define hdc_lamp_short_flg                              EF->InternalDTCByte27_Bit3
#define abs_lamp_short_flg                              EF->InternalDTCByte27_Bit4
#define vdc_off_lamp_short_flg                          EF->InternalDTCByte27_Bit5
#define feu1LampInternalDTCByte27_Bit6                  EF->InternalDTCByte27_Bit6
#define feu1LampInternalDTCByte27_Bit7                  EF->InternalDTCByte27_Bit7

#define SWITCH_ERROR_BYTE                               28
#define tcs_vdc_sw_error_flg                            EF->InternalDTCByte28_Bit0
#define hdc_sw_error_flg                                EF->InternalDTCByte28_Bit1
#define cal_not_prog_err_flg                            EF->InternalDTCByte28_Bit2
#define feu1HDCRelayOffErr                              EF->InternalDTCByte28_Bit3
#define feu1ESSDriveOnErr                               EF->InternalDTCByte28_Bit4
#define feu1ESSDriveOffErr                              EF->InternalDTCByte28_Bit5
#define feu1ESSRelayOnErr                               EF->InternalDTCByte28_Bit6
#define feu1ESSRelayOffErr                              EF->InternalDTCByte28_Bit7

#define ECE_ERROR_BYTE_0                                29
#define hpa_ece_flg                                     EF->InternalDTCByte29_Bit0
#define bcp1_ece_flg                                    EF->InternalDTCByte29_Bit1
#define bcp2_ece_flg                                    EF->InternalDTCByte29_Bit2
#define psp_ece_flg                                     EF->InternalDTCByte29_Bit3
#define g_ece_flg                                       EF->InternalDTCByte29_Bit4
#define pedal_ece_flg                                   EF->InternalDTCByte29_Bit5
#define bls_ece_flg                                     EF->InternalDTCByte29_Bit6
#define sas_ece_flg                                     EF->InternalDTCByte29_Bit7

#define ECE_ERROR_BYTE_1                                30
#define yaw_ece_flg                                     EF->InternalDTCByte30_Bit0
#define ay_ece_flg                                      EF->InternalDTCByte30_Bit1
#define ece_flg                                         EF->InternalDTCByte30_Bit2
#define vdc_switch_ece_flg                              EF->InternalDTCByte30_Bit3
#define motor_ece_flg                                   EF->InternalDTCByte30_Bit4
#define clutch_ece_flg                                  EF->InternalDTCByte30_Bit5
#define gearR_ece_flg                                   EF->InternalDTCByte30_Bit6
#define hdc_switch_ece_flg                              EF->InternalDTCByte30_Bit7

#define ABS_ETC_ERROR_BYTE_0                            31
#define tc_temp_error_flg                               EF->InternalDTCByte31_Bit0
#define feu1BATT1FuseOpen                               EF->InternalDTCByte31_Bit1
#define wrong_ebd_cntrl_err_flg                         EF->InternalDTCByte31_Bit2
#define low_brk_fluid_err_flg                           EF->InternalDTCByte31_Bit3
#define wrong_immo_id_err_flg                           EF->InternalDTCByte31_Bit4
#define env_id_not_prog_err_flg                         EF->InternalDTCByte31_Bit5
#define vin_not_prog_err_flg                            EF->InternalDTCByte31_Bit6
#define hs_subnet_config_err_flg                        EF->InternalDTCByte31_Bit7

#define VACUUM_ERROR_BYTE                               32
#define vacuum_open_flg                                 EF->InternalDTCByte32_Bit0
#define vacuum_short_flg                                EF->InternalDTCByte32_Bit1
#define vacuum_stick_err_flg                            EF->InternalDTCByte32_Bit2
#define vacuum_leak_err_flg                             EF->InternalDTCByte32_Bit3
#define vacuum_noise_err_flg                            EF->InternalDTCByte32_Bit4
#define feu1VacuumPumpRelayShortErrFlg                  EF->InternalDTCByte32_Bit5
#define feu1VacuumPumpRelayOpenErrFlg                   EF->InternalDTCByte32_Bit6
#define feu1VacuumPumpSystemErrFlg                      EF->InternalDTCByte32_Bit7

#define SOLENOID_RESERVED_ERROR_BYTE_2                  33
#define fli_psv_open_flg                                EF->InternalDTCByte33_Bit0
#define flo_psv_open_flg                                EF->InternalDTCByte33_Bit1
#define fri_psv_open_flg                                EF->InternalDTCByte33_Bit2
#define fro_psv_open_flg                                EF->InternalDTCByte33_Bit3
#define rli_psv_open_flg                                EF->InternalDTCByte33_Bit4
#define rlo_psv_open_flg                                EF->InternalDTCByte33_Bit5
#define rri_psv_open_flg                                EF->InternalDTCByte33_Bit6
#define rro_psv_open_flg                                EF->InternalDTCByte33_Bit7

#define ABS_ETC_ERROR_BYTE_1                            34
#define feu1AbsEtc2InternalDTCByte34_0                  EF->InternalDTCByte34_Bit0
#define feu1AbsEtc2InternalDTCByte34_1                  EF->InternalDTCByte34_Bit1
#define feu1AbsEtc2InternalDTCByte34_2                  EF->InternalDTCByte34_Bit2
#define feu1AbsEtc2InternalDTCByte34_3                  EF->InternalDTCByte34_Bit3
#define feu1AbsEtc2InternalDTCByte34_4                  EF->InternalDTCByte34_Bit4
#define feu1AbsEtc2InternalDTCByte34_5                  EF->InternalDTCByte34_Bit5
#define feu1AbsEtc2InternalDTCByte34_6                  EF->InternalDTCByte34_Bit6
#define feu1AbsEtc2InternalDTCByte34_7                  EF->InternalDTCByte34_Bit7

#define ABS_ETC_ERROR_BYTE_2                            35
#define secure_code_prog_err_flg                        EF->InternalDTCByte35_Bit0
#define wrong_cal_err_flg                               EF->InternalDTCByte35_Bit1
#define feu1AbsEtc3InternalDTCByte35_2                  EF->InternalDTCByte35_Bit2
#define feu1AbsEtc3InternalDTCByte35_3                  EF->InternalDTCByte35_Bit3
#define feu1AbsEtc3InternalDTCByte35_4                  EF->InternalDTCByte35_Bit4
#define feu1AbsEtc3InternalDTCByte35_5                  EF->InternalDTCByte35_Bit5
#define feu1AbsEtc3InternalDTCByte35_6                  EF->InternalDTCByte35_Bit6
#define feu1AbsEtc3InternalDTCByte35_7                  EF->InternalDTCByte35_Bit7

    #define STEER_ERROR_BYTE_0                              36
    #define feu1SAS_HW_Line_err_flg                         EF->InternalDTCByte36_Bit0
    #define feu1CANSAS_SW_Calibration_err_flg               EF->InternalDTCByte36_Bit1
    #define feu1CANSAS_SW_Checksum_err_flg                  EF->InternalDTCByte36_Bit2
    #define feu1CANSAS_SW_Internal_err_flg                  EF->InternalDTCByte36_Bit3
    #define feu1CANSAS_SW_Roll_err_flg                      EF->InternalDTCByte36_Bit4
    #define feu1CANSAS_SW_Protection_err_flg                EF->InternalDTCByte36_Bit5
    #define feu1CANSAS_SW_Range_err_flg                     EF->InternalDTCByte36_Bit6
    #define feu1SASInternalDTCByte36_Bit7                   EF->InternalDTCByte36_Bit7

    #define STEER_ERROR_BYTE_1                              37
    #define feu1CANSAS_SW_ShortTermOffset_err_flg           EF->InternalDTCByte37_Bit0
    #define feu1CANSAS_SW_LongTermOffset_err_flg            EF->InternalDTCByte37_Bit1
    #define feu1CANSAS_SW_ModelMay_err_flg                  EF->InternalDTCByte37_Bit2
    #define feu1CANSAS_SW_Shock_err_flg                     EF->InternalDTCByte37_Bit3
    #define feu1CANSAS_SW_Stick_err_flg                     EF->InternalDTCByte37_Bit4
    #define feu1CANSAS_SW_Reverse_err_flg                   EF->InternalDTCByte37_Bit5
    #define feu1CANSAS_SW_Rate_err_flg                      EF->InternalDTCByte37_Bit6
    #define feu1SASInternalDTCByte37_Bit7                   EF->InternalDTCByte37_Bit7


#define IMU_ERROR_BYTE                                  38
#define feu1IMU_HW_Line_err_flg                         EF->InternalDTCByte38_Bit0
#define feu1IMU_SW_Checksum_err_flg                     EF->InternalDTCByte38_Bit1
#define feu1Yaw_SW_Internal_err_flg                     EF->InternalDTCByte38_Bit2
#define feu1IMU_SW_Roll_err_flg                         EF->InternalDTCByte38_Bit3
#define feu1IMU_SW_CANFunc_err_flg                      EF->InternalDTCByte38_Bit4
#define feu1IMU_SW_Invalid_err_flg                      EF->InternalDTCByte38_Bit5
#define feu1IMU_SW_Raster_err_flg                       EF->InternalDTCByte38_Bit6
#define feu1Ay_SW_Internal_err_flg                      EF->InternalDTCByte38_Bit7

#define YAW_ERROR_BYTE_0                                39
#define feu1Yaw_SW_ShortTermDrivingOffset_err_flg       EF->InternalDTCByte39_Bit0
#define feu1Yaw_SW_LongTermDrivingOffset_err_flg        EF->InternalDTCByte39_Bit1
#define feu1Yaw_SW_ModelMay_err_flg                     EF->InternalDTCByte39_Bit2
#define feu1Yaw_SW_ModelMust_err_flg                    EF->InternalDTCByte39_Bit3
#define feu1Yaw_SW_Shock_err_flg                        EF->InternalDTCByte39_Bit4
#define feu1Yaw_SW_Stick_err_flg                        EF->InternalDTCByte39_Bit5
#define feu1Yaw_SW_Reverse_err_flg                      EF->InternalDTCByte39_Bit6
#define feu1YAWInternalDTCByte39_Bit7                   EF->InternalDTCByte39_Bit7

#define YAW_ERROR_BYTE_1                                40
#define feu1Yaw_SW_StandOffset_err_flg                  EF->InternalDTCByte40_Bit0
#define feu1Yaw_SW_Range_err_flg                        EF->InternalDTCByte40_Bit1
#define feu1Yaw_SW_Noise_err_flg                        EF->InternalDTCByte40_Bit2
#define feu1Yaw_SW_CBIT_err_flg                         EF->InternalDTCByte40_Bit3
#define feu1SEN_Volt_err_flg                            EF->InternalDTCByte40_Bit4
#define feu1YAW_Init_Run_err_flg                        EF->InternalDTCByte40_Bit5
#define feu1Sen_MCU_Volt_err_flg                        EF->InternalDTCByte40_Bit6
#define feu1Sen_MCU_Sen_err_flg                         EF->InternalDTCByte40_Bit7

#define AY_ERROR_BYTE_0                                 41
#define feu1Ay_SW_StandOffset_err_flg                   EF->InternalDTCByte41_Bit0
#define feu1Ay_SW_Range_err_flg                         EF->InternalDTCByte41_Bit1
#define feu1Ay_SW_Noise_err_flg                         EF->InternalDTCByte41_Bit2
#define feu1Ay_SW_CBIT_err_flg                          EF->InternalDTCByte41_Bit3
#define feu1AY_SW_WRONG_INSTALLED_err_flg               EF->InternalDTCByte41_Bit4
#define feu1AY_Init_Run_err_flg                         EF->InternalDTCByte41_Bit5
#define feu1AYInternalDTCByte41_Bit6                    EF->InternalDTCByte41_Bit6
#define feu1AYInternalDTCByte41_Bit7                    EF->InternalDTCByte41_Bit7

#define AY_ERROR_BYTE_1                                 42
#define feu1Ay_SW_ShortTermDrivingOffset_err_flg        EF->InternalDTCByte42_Bit0
#define feu1Ay_SW_LongTermDrivingOffset_err_flg         EF->InternalDTCByte42_Bit1
#define feu1Ay_SW_ModelMay_err_flg                      EF->InternalDTCByte42_Bit2
#define feu1Ay_SW_ModelMust_err_flg                     EF->InternalDTCByte42_Bit3
#define feu1Ay_SW_Shock_err_flg                         EF->InternalDTCByte42_Bit4
#define feu1Ay_SW_Stick_err_flg                         EF->InternalDTCByte42_Bit5
#define feu1Ay_SW_Reverse_err_flg                       EF->InternalDTCByte42_Bit6
#define feu1AYInternalDTCByte42_Bit7                    EF->InternalDTCByte42_Bit7

#define ETC_ERROR_BYTE_0                                43
#define feu1ETCInternalDTCByte43_Bit0                   EF->InternalDTCByte43_Bit0
#define feu1ETCInternalDTCByte43_Bit1                   EF->InternalDTCByte43_Bit1
#define feu1ETCInternalDTCByte43_Bit2                   EF->InternalDTCByte43_Bit2
#define feu1ETCInternalDTCByte43_Bit3                   EF->InternalDTCByte43_Bit3
#define feu1ETCInternalDTCByte43_Bit4                   EF->InternalDTCByte43_Bit4
#define feu1ETCInternalDTCByte43_Bit5                   EF->InternalDTCByte43_Bit5
#define feu1ETCInternalDTCByte43_Bit6                   EF->InternalDTCByte43_Bit6
#define feu1MCPInternalDTCByte43_Bit7                   EF->InternalDTCByte43_Bit7

#define ETC_ERROR_BYTE_1                                44
#define feu1ETCInternalDTCByte44_Bit0                   EF->InternalDTCByte44_Bit0
#define feu1ETCInternalDTCByte44_Bit1                   EF->InternalDTCByte44_Bit1
#define feu1ETCInternalDTCByte44_Bit2                   EF->InternalDTCByte44_Bit2
#define feu1ETCInternalDTCByte44_Bit3                   EF->InternalDTCByte44_Bit3
#define feu1ETCInternalDTCByte44_Bit4                   EF->InternalDTCByte44_Bit4
#define feu1ETCInternalDTCByte44_Bit5                   EF->InternalDTCByte44_Bit5
#define feu1ETCInternalDTCByte44_Bit6                   EF->InternalDTCByte44_Bit6
#define feu1ETCInternalDTCByte44_Bit7                   EF->InternalDTCByte44_Bit7

#define SUBCAN_ERROR_BYTE                               45
#define SEN_BUS_off_err_flg                             EF->InternalDTCByte45_Bit0
#define SEN_hw_err_flg                                  EF->InternalDTCByte45_Bit1
#define feu1VSM2_HW_Line_Err_flg                        EF->InternalDTCByte45_Bit2
#define feu1VSM2_SW_CS_Err_flg                          EF->InternalDTCByte45_Bit3
#define feu1VSM2_SW_Internal_Err_flg                    EF->InternalDTCByte45_Bit4
#define feu1VSM2_SW_Roll_Err_flg                        EF->InternalDTCByte45_Bit5
#define CLU2_timeout_err_flg                            EF->InternalDTCByte45_Bit6
#define feu1CLU2_Signal_Err_flg                         EF->InternalDTCByte45_Bit7

#define SOLENOID_RESERVED_ERROR_BYTE_3                  46
#define cut1_open_by_curr                               EF->InternalDTCByte46_Bit0
#define cut1_short_by_curr                              EF->InternalDTCByte46_Bit1
#define cut2_open_by_curr                               EF->InternalDTCByte46_Bit2
#define cut2_short_by_curr                              EF->InternalDTCByte46_Bit3
#define sim_open_by_curr                                EF->InternalDTCByte46_Bit4
#define sim_short_by_curr                               EF->InternalDTCByte46_Bit5
#define cut1_leak_err_flg                               EF->InternalDTCByte46_Bit6
#define feu1InternalDTCByte46_Bit7                      EF->InternalDTCByte46_Bit7

#define SOLENOID_RESERVED_ERROR_BYTE_4                  47
#define cut1_open_by_neg                                EF->InternalDTCByte47_Bit0
#define cut1_short_by_neg                               EF->InternalDTCByte47_Bit1
#define cut2_open_by_neg                                EF->InternalDTCByte47_Bit2
#define cut2_short_by_neg                               EF->InternalDTCByte47_Bit3
#define sim_open_by_neg                                 EF->InternalDTCByte47_Bit4
#define sim_short_by_neg                                EF->InternalDTCByte47_Bit5
#define simV_stuck_err_flg                              EF->InternalDTCByte47_Bit6
#define feu1InternalDTCByte47_Bit7                      EF->InternalDTCByte47_Bit7

#define ESC_ADDITIONAL_ERROR_BYTE                       48
#define flp_avr_err_flg                                 EF->InternalDTCByte48_Bit0
#define frp_avr_err_flg                                 EF->InternalDTCByte48_Bit1
#define rlp_avr_err_flg                                 EF->InternalDTCByte48_Bit2
#define rrp_avr_err_flg                                 EF->InternalDTCByte48_Bit3
#define flp_state_err_flg                               EF->InternalDTCByte48_Bit4
#define frp_state_err_flg                               EF->InternalDTCByte48_Bit5
#define rlp_state_err_flg                               EF->InternalDTCByte48_Bit6
#define rrp_state_err_flg                               EF->InternalDTCByte48_Bit7

#define ESC_ETC_ERROR_BYTE                              49
#define flp_noise_err_flg                               EF->InternalDTCByte49_Bit0
#define frp_noise_err_flg                               EF->InternalDTCByte49_Bit1
#define rlp_noise_err_flg                               EF->InternalDTCByte49_Bit2
#define rrp_noise_err_flg                               EF->InternalDTCByte49_Bit3
#define fu1BlrFetOpenErr                                EF->InternalDTCByte49_Bit4
#define fu1BlrContactOpenErr                            EF->InternalDTCByte49_Bit5
#define fu1BlrCoilOpenErr                               EF->InternalDTCByte49_Bit6
#define pedal_signal_err_flg                            EF->InternalDTCByte49_Bit7

#define ESC_RESERVED_ERROR_BYTE_0                       50
#define wrong_esc_cntrl_err_flg                         EF->InternalDTCByte50_Bit0
#define ce_subnet_config_err_flg                        EF->InternalDTCByte50_Bit1
#define feu1HdcTemperatureErr                           EF->InternalDTCByte50_Bit2
#define pedal_pdt_pot1_open_flg                         EF->InternalDTCByte50_Bit3
#define pedal_pdf_pot2_open_flg                         EF->InternalDTCByte50_Bit4
#define pedal_pdt_pot1_short_flg                        EF->InternalDTCByte50_Bit5
#define pedal_pdf_pot2_short_flg                        EF->InternalDTCByte50_Bit6
#define pedal_mismatch_flg                              EF->InternalDTCByte50_Bit7

#define ESC_RESERVED_ERROR_BYTE_1                       51
#define feu1GearRSwitchShortErr                         EF->InternalDTCByte51_Bit0
#define feu1GearRSwitchOpenErr                          EF->InternalDTCByte51_Bit1
#define feu1GearRSwitchNoiseErr                         EF->InternalDTCByte51_Bit2
#define feu1ClutchSwitchOpenShortErr                    EF->InternalDTCByte51_Bit3
#define feu1ClutchSwitchNoiseErr                        EF->InternalDTCByte51_Bit4
#define avh_sw_error_flg                   			    EF->InternalDTCByte51_Bit5
#define feu1EPB1_Signal_Err_flg                         EF->InternalDTCByte51_Bit6
#define EPB1_timeout_err_flg                            EF->InternalDTCByte51_Bit7

#define ESC_RESERVED_ERROR_BYTE_2                       52
#define feu1SCCTIMEOUT_Err_flg                          EF->InternalDTCByte52_Bit0
#define feu1SCCMSGINVALID_Err_flg                       EF->InternalDTCByte52_Bit1
#define feu1SCCACCEL_Err_flg                            EF->InternalDTCByte52_Bit2
#define feu1EMS1MSGINVALID_Err_flg                      EF->InternalDTCByte52_Bit3
#define feu1EMS5MSGINVALID_Err_flg                      EF->InternalDTCByte52_Bit4
#define feu1TCU1MSGINVALID_Err_flg                      EF->InternalDTCByte52_Bit5
#define feu1EMS5TIMEOUT_Err_flg                         EF->InternalDTCByte52_Bit6
#define avsm_sw_error_flg                               EF->InternalDTCByte52_Bit7

#define SOLENOID_RESERVED_ERROR_BYTE_5                  53
#define apv1_open_by_neg                               EF->InternalDTCByte53_Bit0
#define apv1_short_by_neg                              EF->InternalDTCByte53_Bit1
#define apv2_open_by_neg                               EF->InternalDTCByte53_Bit2
#define apv2_short_by_neg                              EF->InternalDTCByte53_Bit3
#define rlv1_open_by_neg                               EF->InternalDTCByte53_Bit4
#define rlv1_short_by_neg                              EF->InternalDTCByte53_Bit5
#define rlv2_open_by_neg                               EF->InternalDTCByte53_Bit6
#define rlv2_short_by_neg                              EF->InternalDTCByte53_Bit7

#define ESC_PEDAL_SIGNAL_ERROR                          54
#define pedal_Pdt_Sig_Offset                            EF->InternalDTCByte54_Bit0
#define pedal_Pdf_Sig_Offset                            EF->InternalDTCByte54_Bit1
#define pedal_Pdt_Sig_Stick                             EF->InternalDTCByte54_Bit2
#define pedal_Pdf_Sig_Stick                             EF->InternalDTCByte54_Bit3
#define pedal_Pdt_Sig_Noise                             EF->InternalDTCByte54_Bit4
#define pedal_Pdf_Sig_Noise                             EF->InternalDTCByte54_Bit5
#define pedal_pdt_sig_nochange                          EF->InternalDTCByte54_Bit6
#define pedal_calibration_err_flg                       EF->InternalDTCByte54_Bit7

#define VALVERELAY_ERROR_BYTE_1                         55
#define ahb_vr_bypass_flg                               EF->InternalDTCByte55_Bit0
#define ahb_vr_open_flg                                 EF->InternalDTCByte55_Bit1
#define ahb_vr_short_g_flg                              EF->InternalDTCByte55_Bit2
#define ahb_vr_lkg_flg                                  EF->InternalDTCByte55_Bit3
#define ahb_vr_shutdown_flg                             EF->InternalDTCByte55_Bit4
#define ahb_vr_oc_flg                                       EF->InternalDTCByte55_Bit5
#define abs_vr_oc_flg                                       EF->InternalDTCByte55_Bit6
#define feu1VRInternalDTCByte55_Bit7                    EF->InternalDTCByte55_Bit7

#define SOLENOID_RESERVED_ERROR_BYTE_6                  56
#define apv1_open_ic                                    EF->InternalDTCByte56_Bit0
#define apv1_short_ic                                   EF->InternalDTCByte56_Bit1
#define apv2_open_ic                                    EF->InternalDTCByte56_Bit2
#define apv2_short_ic                                   EF->InternalDTCByte56_Bit3
#define rlv1_open_ic                                    EF->InternalDTCByte56_Bit4
#define rlv1_short_ic                                   EF->InternalDTCByte56_Bit5
#define rlv2_open_ic                                    EF->InternalDTCByte56_Bit6
#define rlv2_short_ic                                   EF->InternalDTCByte56_Bit7



#define PRESSURE_HW_ERROR                               70
#define feu1Hpa_V_ErrFlg                                EF->InternalDTCByte70_Bit0
#define feu1HpaAvrErrFlg                                EF->InternalDTCByte70_Bit1
#define feu1BcpPri_V_ErrFlg                             EF->InternalDTCByte70_Bit2
#define feu1BcpPriAvrErrFlg                             EF->InternalDTCByte70_Bit3
#define feu1BcpSec_V_ErrFlg                             EF->InternalDTCByte70_Bit4
#define feu1BcpSecAvrErrFlg                             EF->InternalDTCByte70_Bit5
#define feu1Psp_V_ErrFlg                                EF->InternalDTCByte70_Bit6
#define feu1PspAvrErrFlg                                EF->InternalDTCByte70_Bit7

#define HPA_PRESSURE_SIG_ERROR                          71
#define feu1HpaLowStickErrFlg                           EF->InternalDTCByte71_Bit0
#define feu1HpaLeakErrFlg                               EF->InternalDTCByte71_Bit1
#define feu1HpaLowPreErrFlg                             EF->InternalDTCByte71_Bit2
#define feu1HpaHighStickErrFlg                          EF->InternalDTCByte71_Bit3
#define feu1HpaOffsetErrFlg                             EF->InternalDTCByte71_Bit4
#define feu1HpaNoiseErrFlg                              EF->InternalDTCByte71_Bit5
#define feu1InternalDTCByte71_Bit6                      EF->InternalDTCByte71_Bit6
#define feu1InternalDTCByte71_Bit7                      EF->InternalDTCByte71_Bit7

#define BCP_PRESSURE_SIG_ERROR                          72
#define feu1BcpPriLowStickErrFlg                        EF->InternalDTCByte72_Bit0
#define feu1BcpPriOffsetErrFlg                          EF->InternalDTCByte72_Bit1
#define feu1BcpPriNoiseErrFlg                           EF->InternalDTCByte72_Bit2
#define feu1BcpSecLowStickErrFlg                        EF->InternalDTCByte72_Bit3
#define feu1BcpSecOffsetErrFlg                          EF->InternalDTCByte72_Bit4
#define feu1BcpSecNoiseErrFlg                           EF->InternalDTCByte72_Bit5
#define feu1InternalDTCByte72_Bit6                      EF->InternalDTCByte72_Bit6
#define feu1InternalDTCByte72_Bit7                      EF->InternalDTCByte72_Bit7

#define PSP_PRESSURE_SIG_ERROR                          73
#define feu1PspStickErrFlg                              EF->InternalDTCByte73_Bit0
#define feu1PspOffsetErrFlg                             EF->InternalDTCByte73_Bit1
#define feu1PspNoiseErrFlg	                      		EF->InternalDTCByte73_Bit2
#define pressure_calibration_err_flg                    EF->InternalDTCByte73_Bit3
#define feu1BcPriLeakErrFlg                             EF->InternalDTCByte73_Bit4
#define feu1BcSecLeakErrFlg                             EF->InternalDTCByte73_Bit5
#define feu1InternalDTCByte73_Bit6                      EF->InternalDTCByte73_Bit6
#define feu1InternalDTCByte73_Bit7                      EF->InternalDTCByte73_Bit7

#define CAN_ERROR_BYTE_5                                74
#define feu1EMSSIGINVALID_Err_flg                       EF->InternalDTCByte74_Bit0
#define feu1TCUGearInvalid_Err_flg                      EF->InternalDTCByte74_Bit1
#define feu1InternalDTCByte74_Bit2	                    EF->InternalDTCByte74_Bit2
#define feu1InternalDTCByte74_Bit3                      EF->InternalDTCByte74_Bit3
#define feu1InternalDTCByte74_Bit4                      EF->InternalDTCByte74_Bit4
#define feu1InternalDTCByte74_Bit5                      EF->InternalDTCByte74_Bit5
#define feu1InternalDTCByte74_Bit6                      EF->InternalDTCByte74_Bit6
#define feu1InternalDTCByte74_Bit7                      EF->InternalDTCByte74_Bit7


extern struct ERROR_FLAG *EF;

typedef struct
{
    uBitType clear_all      :1;
    uBitType snsr_2_error   :1;
    uBitType sector_erase   :1;
    uBitType sector_write   :1;
    uBitType ee_ece         :1;
    uBitType ee_g_ece       :1;
    uBitType EquipGsnsr     :1;
    uBitType FourWdCar      :1;
}ErrHandle_t;
#define clear_all_flg       ErrHandle.clear_all
#define snsr_2_error_flg    ErrHandle.snsr_2_error
#define sector_erase_flg    ErrHandle.sector_erase
#define sector_write_flg    ErrHandle.sector_write
#define ee_ece_flg          ErrHandle.ee_ece
#define ee_g_ece_flg        ErrHandle.ee_g_ece
#define feu1EquipGsnsrFlg   ErrHandle.EquipGsnsr
#define feu1FourWdCarFlg    ErrHandle.FourWdCar
extern ErrHandle_t      ErrHandle;


typedef struct
{
    uBitType WrDtcToEepComplete     :1;
    uBitType ClearDtc               :1;
    uBitType EceChk                 :1;
    uBitType FullErrBuf             :1;
    uBitType WrErrPtrToEepComplete  :1;
    uBitType WriteDtctoEep          :1;
    uBitType WriteErrPtrtoEep       :1;
    uBitType HistoryClrDtc          :1;
    uBitType SaveNewDtc             :1;
    uBitType WriteSysCycleToEepReq  :1;
    uBitType WriteSuppDtcToEepReq   :1;
    uBitType WriteDupErrToEepReq    :1;
}ErrStatus_t;
#define feu1WrDtcToEepComplete      ErrStatus.WrDtcToEepComplete
#define feu1ClearDtcFlg             ErrStatus.ClearDtc
#define feu1EceChkFlg               ErrStatus.EceChk
#define feu1FullErrBuffFlg          ErrStatus.FullErrBuf
#define feu1WrErrPtrToEepComplte    ErrStatus.WrErrPtrToEepComplete
#define feu1WriteDtctoEep           ErrStatus.WriteDtctoEep
#define feu1WriteErrPtrtoEep        ErrStatus.WriteErrPtrtoEep
#define feu1HistoryClrDtc           ErrStatus.HistoryClrDtc
#define feu1SaveNewDtcFlg           ErrStatus.SaveNewDtc
#define feu1WriteSysCycleToEepReq   ErrStatus.WriteSysCycleToEepReq
#define feu1WriteSuppDtcToEepReq    ErrStatus.WriteSuppDtcToEepReq
#define feu1WriteDupErrToEepReq     ErrStatus.WriteDupErrToEepReq

extern ErrStatus_t       ErrStatus;


typedef struct
{
    unsigned int BLS_error          :1;
    unsigned int str_sw_err         :1;
    unsigned int yaw_sw_err         :1;
    unsigned int lg_sw_err          :1;
    unsigned int mp_sw_err          :1;
    unsigned int str_hw_err         :1;
    unsigned int yaw_hw_err         :1;
    unsigned int lg_hw_err          :1;
    unsigned int mp_hw_err          :1;
    unsigned int asic_error         :1;
    unsigned int ecu_error          :1;
}REPRESENT_ERROR_FLAG;

#define BLS_error_flg                                   REF_ERROR_FLG.BLS_error
#define str_sw_err_flg                                  REF_ERROR_FLG.str_sw_err
#define yaw_sw_err_flg                                  REF_ERROR_FLG.yaw_sw_err
#define lg_sw_err_flg                                   REF_ERROR_FLG.lg_sw_err
#define mp_sw_err_flg                                   REF_ERROR_FLG.mp_sw_err
#define str_hw_err_flg                                  REF_ERROR_FLG.str_hw_err
#define yaw_hw_err_flg                                  REF_ERROR_FLG.yaw_hw_err
#define lg_hw_err_flg                                   REF_ERROR_FLG.lg_hw_err
#define mp_hw_err_flg                                   REF_ERROR_FLG.mp_hw_err
#define asic_error_flg                                  REF_ERROR_FLG.asic_error
#define ecu_error_flg                                   REF_ERROR_FLG.ecu_error

extern REPRESENT_ERROR_FLAG  REF_ERROR_FLG;

typedef struct
{
    uBitType FlWssSigErr  :1;
    uBitType FrWssSigErr  :1;
    uBitType RlWssSigErr  :1;
    uBitType RrWssSigErr  :1;
    uBitType FlWssErr     :1;
    uBitType FrWssErr     :1;
    uBitType RlWssErr     :1;
    uBitType RrWssErr     :1;
}ReferWssErr_t;
#define feu1FlWssSigErrFlg  WssErrRefer.FlWssSigErr
#define feu1FrWssSigErrFlg  WssErrRefer.FrWssSigErr
#define feu1RlWssSigErrFlg  WssErrRefer.RlWssSigErr
#define feu1RrWssSigErrFlg  WssErrRefer.RrWssSigErr
#define feu1FlWssErrFlg     WssErrRefer.FlWssErr
#define feu1FrWssErrFlg     WssErrRefer.FrWssErr
#define feu1RlWssErrFlg     WssErrRefer.RlWssErr
#define feu1RrWssErrFlg     WssErrRefer.RrWssErr
extern ReferWssErr_t       WssErrRefer;


typedef struct
{
    uBitType VrOnEna         :1;
    uBitType AbsSolenoidErr     :1;
    uBitType FrontSolErr     :1;
    uBitType RearSolErr      :1;

    uBitType APVSolErr     :1;
    uBitType RLVSolErr     :1;
    uBitType CUTSolErr     :1;
    uBitType SIMSolenoidErr     :1;
    uBitType AhbSolenoidErr       :1;
    uBitType AhbVrErr       :1;
    uBitType AbsVrErr           :1;
    uBitType DiagVrOnEna     :1;
    uBitType SolenoidErr     :1;
}ReferActErr_t;

#define feu1VrOnEnaFlg       ActErrRefer.VrOnEna
#define feu1AbsSolenoidErrFlg   ActErrRefer.AbsSolenoidErr
#define feu1FrontSolErrFlg   ActErrRefer.FrontSolErr
#define feu1RearSolErrFlg    ActErrRefer.RearSolErr
#define feu1APVSolenoidErrFlg     ActErrRefer.APVSolErr
#define feu1RLVSolenoidErrFlg     ActErrRefer.RLVSolErr
#define feu1CUTSolenoidErrFlg     ActErrRefer.CUTSolErr
#define feu1SIMSolenoidErrFlg     ActErrRefer.SIMSolenoidErr
#define feu1AhbSolenoidErrFlg     ActErrRefer.AhbSolenoidErr
#define feu1AhbVrErrFlg     ActErrRefer.AhbVrErr
#define feu1VrErrFlg         ActErrRefer.AbsVrErr
#define feu1DiagVrOnEnaFlg   ActErrRefer.DiagVrOnEna
#define feu1SolenoidErrFlg	ActErrRefer.SolenoidErr

extern ReferActErr_t       ActErrRefer;


typedef struct
{
    uBitType MotErr         :1;
    uBitType UnderPwr       :1;
    uBitType LampErr        :1;
    uBitType CanErr         :1;

}ReferErr1_t;
#define feu1MotErrFlg     ErrRefer1.MotErr
#define feu1UnderPwrFlg   ErrRefer1.UnderPwr
#define feu1LampErrFlg    ErrRefer1.LampErr
#define feu1CanErrFlg     ErrRefer1.CanErr
extern ReferErr1_t      ErrRefer1;

/**************************************************************************************/
/************** Interface Variable From C/Logic --> Failsafe            ***************/
/**************************************************************************************/
typedef struct
{
	uBitType u1ABSOn                        :1;
    uBitType u1EBDOn                        :1;
    uBitType u1EDCOn                        :1;
    uBitType u1CLValveOn                    :1;
    uBitType u1InsideABSOn                  :1;
    uBitType u1UnderControl                 :1;
    uBitType u1AllControl                   :1;
	uBitType u1ABSOnFL                      :1;
    uBitType u1ABSOnFR                      :1;
    uBitType u1ABSOnRL                      :1;
    uBitType u1ABSOnRR                      :1;
    uBitType u1FL_MiniTireDet               :1;
    uBitType u1FR_MiniTireDet               :1;
    uBitType u1RL_MiniTireDet               :1;
    uBitType u1RR_MiniTireDet               :1;
    uBitType u1ESSOn                        :1;

	#if __ECU==ESP_ECU_1
	uBitType u1BTCSOn                       :1;
    uBitType u1FTCSOn                       :1;
    uBitType u1ESPOn                        :1;
    uBitType u1TCSESPOn                     :1;
    uBitType u1CLESPContrlFL                :1;
    uBitType u1CLESPContrlFR                :1;
    uBitType u1CLESPContrlRL                :1;
    uBitType u1CLESPContrlRR                :1;
    uBitType u1TemperatureErrDet            :1;
    uBitType u1PartialControlOn             :1;
    uBitType u1FunctionLampReq              :1;
    uBitType u1VDCFunctionLampReq           :1;
    uBitType u1TCSFunctionLampReq           :1;
    uBitType u1HDCOn                        :1;
    uBitType u1ACCOn                        :1;

    uBitType u1SCCOn                        :1;

    uBitType u1HSAOn                        :1;
    uBitType u1AVHOn                        :1;
    uBitType u1BTCSOnFL                     :1;
    uBitType u1BTCSOnFR                     :1;
    uBitType u1BTCSOnRL                     :1;
    uBitType u1BTCSOnRR                     :1;
    uBitType u1VDCMtrOn                     :1;
    uBitType u1TCSOn                        :1;
    uBitType u1ETCSOn                       :1;
    uBitType u1CLBackWardDet                :1;
    uBitType u1CLASteerOK                   :1;
    uBitType u1ESPACT                       :1;
    uBitType u1CLSTEEROK                    :1;
    uBitType u1CLStraightSet 				:1;
    uBitType u1ESCDisabled                  :1;
    uBitType u1HDCTemperatureErrDet         :1;
    #endif /*#if __ECU==ESP_ECU_1*/
}fu1CLogicBit_t;
#define fu1ABSOn                  fu1CLogicBit.u1ABSOn
#define fu1EBDOn                  fu1CLogicBit.u1EBDOn
#define fu1EDCOn                  fu1CLogicBit.u1EDCOn
#define fu1CLValveOn              fu1CLogicBit.u1CLValveOn
#define fu1InsideABSOn            fu1CLogicBit.u1InsideABSOn
#define fu1UnderControl           fu1CLogicBit.u1UnderControl
#define fu1AllControl             fu1CLogicBit.u1AllControl
#define fu1ABSOnFL                fu1CLogicBit.u1ABSOnFL
#define fu1ABSOnFR                fu1CLogicBit.u1ABSOnFR
#define fu1ABSOnRL                fu1CLogicBit.u1ABSOnRL
#define fu1ABSOnRR                fu1CLogicBit.u1ABSOnRR
#define fu1FL_MiniTireDet         fu1CLogicBit.u1FL_MiniTireDet
#define fu1FR_MiniTireDet         fu1CLogicBit.u1FR_MiniTireDet
#define fu1RL_MiniTireDet         fu1CLogicBit.u1RL_MiniTireDet
#define fu1RR_MiniTireDet         fu1CLogicBit.u1RR_MiniTireDet
#define fu1ESSOn                  fu1CLogicBit.u1ESSOn

#if __ECU==ESP_ECU_1
    #define fu1BTCSOn                 fu1CLogicBit.u1BTCSOn
    #define fu1FTCSOn                 fu1CLogicBit.u1FTCSOn
    #define fu1ESPOn                  fu1CLogicBit.u1ESPOn
    #define fu1TCSESPOn               fu1CLogicBit.u1TCSESPOn
    #define fu1CLESPContrlFL          fu1CLogicBit.u1CLESPContrlFL
    #define fu1CLESPContrlFR          fu1CLogicBit.u1CLESPContrlFR
    #define fu1CLESPContrlRL          fu1CLogicBit.u1CLESPContrlRL
    #define fu1CLESPContrlRR          fu1CLogicBit.u1CLESPContrlRR
    #define fu1TemperatureErrDet      fu1CLogicBit.u1TemperatureErrDet
    #define fu1PartialControlOn       fu1CLogicBit.u1PartialControlOn
    #define fu1FunctionLampReq        fu1CLogicBit.u1FunctionLampReq
    #define fu1VDCFunctionLampReq     fu1CLogicBit.u1VDCFunctionLampReq
    #define fu1TCSFunctionLampReq     fu1CLogicBit.u1TCSFunctionLampReq
    #define fu1HDCOn                  fu1CLogicBit.u1HDCOn
    #define fu1ACCOn                  fu1CLogicBit.u1ACCOn
    #define fu1SCCOn                  fu1CLogicBit.u1SCCOn
    #define fu1HSAOn                  fu1CLogicBit.u1HSAOn
    #define fu1AVHOn                  fu1CLogicBit.u1AVHOn
    #define fu1BTCSOnFL               fu1CLogicBit.u1BTCSOnFL
    #define fu1BTCSOnFR               fu1CLogicBit.u1BTCSOnFR
    #define fu1BTCSOnRL               fu1CLogicBit.u1BTCSOnRL
    #define fu1BTCSOnRR               fu1CLogicBit.u1BTCSOnRR
    #define fu1VDCMtrOn               fu1CLogicBit.u1VDCMtrOn
    #define fu1TCSOn                  fu1CLogicBit.u1TCSOn
    #define fu1ETCSOn                 fu1CLogicBit.u1ETCSOn
    #define fu1CLBackWardDet          fu1CLogicBit.u1CLBackWardDet
    #define fu1CLASteerOK             fu1CLogicBit.u1CLASteerOK
    #define fu1ESPACT                 fu1CLogicBit.u1ESPACT
    #define fu1CLSTEEROK              fu1CLogicBit.u1CLSTEEROK
    #define fu1CLStraightSet 	      fu1CLogicBit.u1CLStraightSet
    #define fu1ESCDisabled            fu1CLogicBit.u1ESCDisabled
    #define fu1HDCTemperatureErrDet   fu1CLogicBit.u1HDCTemperatureErrDet
#endif /*#if __ECU==ESP_ECU_1*/
extern  fu1CLogicBit_t			  fu1CLogicBit;

typedef struct
{
    uBitType abs_error      :1;
    uBitType ebd_error      :1;
    uBitType tcs_error      :1;
    uBitType vdc_error      :1;
    uBitType can_error      :1;
    uBitType hdc_error      :1;
    uBitType cbs_error      :1;
    uBitType hsa_error      :1;
}ErrFlg_t;
#define can_error_flg   ErrFlg.can_error
#define hdc_error_flg   ErrFlg.hdc_error
#define cbs_error_flg   ErrFlg.cbs_error
#define hsa_error_flg   ErrFlg.hsa_error
extern ErrFlg_t     ErrFlg;

//F_VariableLinkSet.h 복사
typedef struct
{
     uBitType u1ebd_error_flg            :1;
     uBitType u1abs_error_flg            :1;
     uBitType u1btcs_error_flg           :1;
     uBitType u1etcs_error_flg           :1;
     uBitType u1edc_error_flg            :1;
     uBitType u1vdc_error_flg            :1;
     uBitType u1pba_error_flg            :1;
     uBitType u1tcs_error_flg            :1;
     #if __AVH==ENABLE
     uBitType u1avh_error_flg            :1;
     #endif
     #if __MGH80_MOCi==ENABLE
     uBitType u1moc_error_flg            :1;
     #endif
}fsTempFlg2_t;
#define ebd_error_flg                fsTempFlg2.u1ebd_error_flg
#define abs_error_flg                fsTempFlg2.u1abs_error_flg
#define btcs_error_flg               fsTempFlg2.u1btcs_error_flg
#define etcs_error_flg               fsTempFlg2.u1etcs_error_flg
#define edc_error_flg                fsTempFlg2.u1edc_error_flg
#define vdc_error_flg                fsTempFlg2.u1vdc_error_flg
#define pba_error_flg                fsTempFlg2.u1pba_error_flg
#define tcs_error_flg                fsTempFlg2.u1tcs_error_flg
#if __AVH==ENABLE
#define avh_error_flg                fsTempFlg2.u1avh_error_flg
#endif
#if __MGH80_MOCi==ENABLE
#define moc_error_flg                fsTempFlg2.u1moc_error_flg
#endif
extern fsTempFlg2_t                  fsTempFlg2;

//F_VariableLinkSet.h 복사
typedef struct
{
    int16_t cs16LWSAngle                    ;
    uBitType cu8LWSSpeed                    :8;
    uBitType cu1LWSOk                       :1;
    uBitType cu1LWSCal                      :1;
    uBitType cu1LWSTrim                     :1;
    uBitType cu4LWSMsgCount                 :4;
    uBitType cu4LWSCheckSum                 :4;
#if __CAR==CHINA_B12
    uint16_t cu1AngleFailure                :1;
    uint16_t cu1BatteryWarning              :1;
    uint16_t cu1InternalFailure             :1;
    uint16_t cu1CANWarining                 :1;
    uint16_t cu1SASInitial                  :1;
    uint16_t cu1SASCalibrated               :1;
    uint16_t cu1SASAngleValid               :1;
    uint16_t cu1AngleSpeedValid             :1;
    uint16_t cu8InternalFaultCode           :8;
    uint16_t cu8LWSCheckSum                 :8;
#endif
}FSCanSASMsg_t;
#define fcs16LWSAngle                   FSCanSASMsg.cs16LWSAngle
#define fcu8LWSSpeed                    FSCanSASMsg.cu8LWSSpeed
#define fcu1LWSOk                       FSCanSASMsg.cu1LWSOk
#define fcu1LWSCal                      FSCanSASMsg.cu1LWSCal
#define fcu1LWSTrim                     FSCanSASMsg.cu1LWSTrim
#define fcu4LWSMsgCount                 FSCanSASMsg.cu4LWSMsgCount
#define fcu4LWSCheckSum                 FSCanSASMsg.cu4LWSCheckSum
#if __CAR==CHINA_B12
    #define fcu1AngleFailure                     FSCanSASMsg.cu1AngleFailure
    #define fcu1BatteryWarning                   FSCanSASMsg.cu1BatteryWarning
    #define fcu1InternalFailure                  FSCanSASMsg.cu1InternalFailure
    #define fcu1CANWarining                      FSCanSASMsg.cu1CANWarining
    #define fcu1SASInitial                       FSCanSASMsg.cu1SASInitial
    #define fcu1SASCalibrated                    FSCanSASMsg.cu1SASCalibrated
    #define fcu1SASAngleValid                    FSCanSASMsg.cu1SASAngleValid
    #define fcu1AngleSpeedValid                  FSCanSASMsg.cu1AngleSpeedValid
    #define fcu8InternalFaultCode                FSCanSASMsg.cu8InternalFaultCode
    #define fcu8LWSCheckSum                      FSCanSASMsg.cu8LWSCheckSum
#endif

extern FSCanSASMsg_t        FSCanSASMsg;

//F_SensorSwCheck.h 복사
typedef struct
{
 		unsigned int mp_suspcs		                    :1;

     	unsigned int u1eepWriteReqSas		            :1;
     	unsigned int u1eepWriteReqYaw		            :1;
     	unsigned int u1eepWriteReqAy		            :1;
        unsigned int u1eepWriteReqPedal                 :1;
        unsigned int u1eepWriteReqHpa                   :1;
        unsigned int u1eepWriteReqBcp1                  :1;
        unsigned int u1eepWriteReqBcp2                  :1;
        unsigned int u1eepWriteReqPsp                   :1;
     	unsigned int u1eepWriteReqMotor		            :1;
     	unsigned int u1eepWriteReqBls		            :1;

     	unsigned int u1eepEraseReqSas	                :1;
     	unsigned int u1eepEraseReqYaw	                :1;
     	unsigned int u1eepEraseReqAy		            :1;
        unsigned int u1eepEraseReqPedal                 :1;
        unsigned int u1eepEraseReqHpa                   :1;
        unsigned int u1eepEraseReqBcp1                  :1;
        unsigned int u1eepEraseReqBcp2                  :1;
        unsigned int u1eepEraseReqPsp                   :1;
     	unsigned int u1eepEraseReqMotor	                :1;

     	unsigned int u1eepEraseReqBls	                :1;

     	unsigned int u1SasCurFailDet	                :1;
     	unsigned int u1YawCurFailDet	                :1;
     	unsigned int u1AyCurFailDet 	                :1;
        unsigned int u1PedalCurFailDet                  :1;
        unsigned int u1HpaCurFailDet                    :1;
        unsigned int u1Bcp1CurFailDet                   :1;
        unsigned int u1Bcp2CurFailDet                   :1;
        unsigned int u1PspCurFailDet                    :1;
     	unsigned int u1BlsCurFailDet	                :1;
     	unsigned int u1MtrCurFailDet	                :1;

     	unsigned int u1SasFmProcEnd	                    :1;
     	unsigned int u1YawFmProcEnd	                    :1;
     	unsigned int u1AyFmProcEnd	                    :1;
        unsigned int u1PedalFmProcEnd                   :1;
        unsigned int u1HpaFmProcEnd                     :1;
        unsigned int u1Bcp1FmProcEnd                    :1;
        unsigned int u1Bcp2FmProcEnd                    :1;
        unsigned int u1PspFmProcEnd                     :1;
     	unsigned int u1BlsFmProcEnd	                    :1;
     	unsigned int u1MtrFmProcEnd	                    :1;

     	unsigned int u1SasFmBackUpProcEnd	            :1;
     	unsigned int u1YawFmBackUpProcEnd	            :1;
     	unsigned int u1AyFmBackUpProcEnd	            :1;
        unsigned int u1PedalFmBackUpProcEnd             :1;
        unsigned int u1HpaFmBackUpProcEnd               :1;
        unsigned int u1Bcp1FmBackUpProcEnd              :1;
        unsigned int u1Bcp2FmBackUpProcEnd              :1;
        unsigned int u1PspFmBackUpProcEnd               :1;
     	unsigned int u1BlsFmBackUpProcEnd	            :1;
     	unsigned int u1MtrFmBackUpProcEnd	            :1;

     	unsigned int u1FmvssStrOk                	    :1;
     	unsigned int u1FmvssCurveOk              	    :1;
     	unsigned int u1FmvssServiceBrkByBLS        	    :1;
        unsigned int u1FmvssServiceBrkByPedal           :1;
        unsigned int u1FmvssHpaStable                   :1;
        unsigned int u1FmvssBcpStable                   :1;
     	unsigned int u1FmvssVrefOk               	    :1;
     	unsigned int u1FmvssYawGainOk               	:1;
     	unsigned int u1FmvssAyGainOk               	    :1;
     	unsigned int u1FmvssSasGainOk               	:1;

     	unsigned int u1RightCurveDet                    :1;
        unsigned int u1RightYawGainDet                  :1;
        unsigned int u1RightAyGainDet                   :1;
        unsigned int u1RightSasGainDet                  :1;
        unsigned int u1LeftCurveDet                     :1;
        unsigned int u1LeftYawGainDet                   :1;
        unsigned int u1LeftAyGainDet                    :1;
        unsigned int u1LeftSasGainDet                   :1;

        unsigned int u1FmvssRightCurveDet               :1;
        unsigned int u1FmvssLeftCurveDet                :1;

        unsigned int u1FmvssRightYawGainDet             :1;
        unsigned int u1FmvssLeftYawGainDet              :1;

        unsigned int u1FmvssRightAyGainDet              :1;
        unsigned int u1FmvssLeftAyGainDet               :1;

        unsigned int u1FmvssRightSasGainDet             :1;
        unsigned int u1FmvssLeftSasGainDet              :1;

        unsigned int u1ServiceBrkPressedByBLS      	    :1;
     	unsigned int u1ServiceBrkDepressedByBLS    	    :1;
        unsigned int u1ServiceBrkPressedByPedal    	    :1;
     	unsigned int u1ServiceBrkDepressedByPedal  	    :1;

     	unsigned int u1FmvssVrefChkStartFlg       	    :1;

}fu1SenSwSuspcs_t;
    #define  mp_suspcs_flg      fu1SenSwSuspcs.mp_suspcs

    #define  fu1eepWriteReqSas              fu1SenSwSuspcs.u1eepWriteReqSas
    #define  fu1eepWriteReqYaw              fu1SenSwSuspcs.u1eepWriteReqYaw
    #define  fu1eepWriteReqAy               fu1SenSwSuspcs.u1eepWriteReqAy
    #define  fu1eepWriteReqPedal            fu1SenSwSuspcs.u1eepWriteReqPedal
    #define  fu1eepWriteReqHpa              fu1SenSwSuspcs.u1eepWriteReqHpa
    #define  fu1eepWriteReqBcp1             fu1SenSwSuspcs.u1eepWriteReqBcp1
    #define  fu1eepWriteReqBcp2             fu1SenSwSuspcs.u1eepWriteReqBcp2
    #define  fu1eepWriteReqPsp              fu1SenSwSuspcs.u1eepWriteReqPsp
    #define  fu1eepWriteReqMotor            fu1SenSwSuspcs.u1eepWriteReqMotor
    #define  fu1eepWriteReqBls              fu1SenSwSuspcs.u1eepWriteReqBls

    #define  fu1eepEraseReqSas              fu1SenSwSuspcs.u1eepEraseReqSas
    #define  fu1eepEraseReqYaw              fu1SenSwSuspcs.u1eepEraseReqYaw
    #define  fu1eepEraseReqAy               fu1SenSwSuspcs.u1eepEraseReqAy
    #define  fu1eepEraseReqPedal            fu1SenSwSuspcs.u1eepEraseReqPedal
    #define  fu1eepEraseReqHpa              fu1SenSwSuspcs.u1eepEraseReqHpa
    #define  fu1eepEraseReqBcp1             fu1SenSwSuspcs.u1eepEraseReqBcp1
    #define  fu1eepEraseReqBcp2             fu1SenSwSuspcs.u1eepEraseReqBcp2
    #define  fu1eepEraseReqPsp              fu1SenSwSuspcs.u1eepEraseReqPsp
    #define  fu1eepEraseReqMotor            fu1SenSwSuspcs.u1eepEraseReqMotor

    #define  fu1eepEraseReqBls              fu1SenSwSuspcs.u1eepEraseReqBls

    #define  fmu1SasCurFailDet	            fu1SenSwSuspcs.u1SasCurFailDet
    #define  fmu1YawCurFailDet	            fu1SenSwSuspcs.u1YawCurFailDet
    #define  fmu1AyCurFailDet 	            fu1SenSwSuspcs.u1AyCurFailDet
    #define  fmu1PedalCurFailDet	        fu1SenSwSuspcs.u1PedalCurFailDet
    #define  fmu1HpaCurFailDet	            fu1SenSwSuspcs.u1HpaCurFailDet
    #define  fmu1Bcp1CurFailDet	            fu1SenSwSuspcs.u1Bcp1CurFailDet
    #define  fmu1Bcp2CurFailDet	            fu1SenSwSuspcs.u1Bcp2CurFailDet
    #define  fmu1PspCurFailDet	            fu1SenSwSuspcs.u1PspCurFailDet
    #define  fmu1BlsCurFailDet	            fu1SenSwSuspcs.u1BlsCurFailDet
    #define  fmu1MtrCurFailDet	            fu1SenSwSuspcs.u1MtrCurFailDet

    #define  fmu1SasFmProcEnd	            fu1SenSwSuspcs.u1SasFmProcEnd
    #define  fmu1YawFmProcEnd	            fu1SenSwSuspcs.u1YawFmProcEnd
    #define  fmu1AyFmProcEnd	            fu1SenSwSuspcs.u1AyFmProcEnd
    #define  fmu1PedalFmProcEnd	            fu1SenSwSuspcs.u1PedalFmProcEnd
    #define  fmu1HpaFmProcEnd	            fu1SenSwSuspcs.u1HpaFmProcEnd
    #define  fmu1Bcp1FmProcEnd	            fu1SenSwSuspcs.u1Bcp1FmProcEnd
    #define  fmu1Bcp2FmProcEnd	            fu1SenSwSuspcs.u1Bcp2FmProcEnd
    #define  fmu1PspFmProcEnd	            fu1SenSwSuspcs.u1PspFmProcEnd
    #define  fmu1BlsFmProcEnd	            fu1SenSwSuspcs.u1BlsFmProcEnd
    #define  fmu1MtrFmProcEnd	            fu1SenSwSuspcs.u1MtrFmProcEnd


    #define  fmu1SasFmBackUpProcEnd	        fu1SenSwSuspcs.u1SasFmBackUpProcEnd
    #define  fmu1YawFmBackUpProcEnd	        fu1SenSwSuspcs.u1YawFmBackUpProcEnd
    #define  fmu1AyFmBackUpProcEnd	        fu1SenSwSuspcs.u1AyFmBackUpProcEnd
    #define  fmu1PedalFmBackUpProcEnd	    fu1SenSwSuspcs.u1McpFmBackUpProcEnd
    #define  fmu1HpaFmBackUpProcEnd	        fu1SenSwSuspcs.u1McpFmBackUpProcEnd
    #define  fmu1Bcp1FmBackUpProcEnd	    fu1SenSwSuspcs.u1McpFmBackUpProcEnd
    #define  fmu1Bcp2FmBackUpProcEnd	    fu1SenSwSuspcs.u1McpFmBackUpProcEnd
    #define  fmu1PspFmBackUpProcEnd	        fu1SenSwSuspcs.u1McpFmBackUpProcEnd
    #define  fmu1BlsFmBackUpProcEnd	        fu1SenSwSuspcs.u1BlsFmBackUpProcEnd
    #define  fmu1MtrFmBackUpProcEnd	        fu1SenSwSuspcs.u1MtrFmBackUpProcEnd


    #define fu1FmvssStrOk                   fu1SenSwSuspcs.u1FmvssStrOk
    #define fu1FmvssCurveOk                 fu1SenSwSuspcs.u1FmvssCurveOk
    #define fu1FmvssServiceBrkByBLS         fu1SenSwSuspcs.u1FmvssServiceBrkByBLS
    #define fu1FmvssServiceBrkByPedal       fu1SenSwSuspcs.u1FmvssServiceBrkByPedal
    #define fu1FmvssHpaStable               fu1SenSwSuspcs.u1FmvssHpaStable
    #define fu1FmvssBcpStable               fu1SenSwSuspcs.u1FmvssBcpStable
    #define fu1FmvssVrefOk                  fu1SenSwSuspcs.u1FmvssVrefOk
    #define fu1FmvssYawGainOk               fu1SenSwSuspcs.u1FmvssYawGainOk
    #define fu1FmvssAyGainOk                fu1SenSwSuspcs.u1FmvssAyGainOk
    #define fu1FmvssSasGainOk               fu1SenSwSuspcs.u1FmvssSasGainOk

    #define fmu1RightCurveDet               fu1SenSwSuspcs.u1RightCurveDet
    #define fmu1RightYawGainDet             fu1SenSwSuspcs.u1RightYawGainDet
    #define fmu1RightAyGainDet              fu1SenSwSuspcs.u1RightAyGainDet
    #define fmu1RightSasGainDet             fu1SenSwSuspcs.u1RightSasGainDet
    #define fmu1LeftCurveDet                fu1SenSwSuspcs.u1LeftCurveDet
    #define fmu1LeftYawGainDet              fu1SenSwSuspcs.u1LeftYawGainDet
    #define fmu1LeftAyGainDet               fu1SenSwSuspcs.u1LeftAyGainDet
    #define fmu1LeftSasGainDet              fu1SenSwSuspcs.u1LeftSasGainDet

    #define fmu1FmvssRightCurveDet          fu1SenSwSuspcs.u1FmvssRightCurveDet
    #define fmu1FmvssLeftCurveDet           fu1SenSwSuspcs.u1FmvssLeftCurveDet
    #define fmu1FmvssRightYawGainDet        fu1SenSwSuspcs.u1FmvssRightYawGainDet
    #define fmu1FmvssLeftYawGainDet         fu1SenSwSuspcs.u1FmvssLeftYawGainDet
    #define fmu1FmvssRightAyGainDet         fu1SenSwSuspcs.u1FmvssRightAyGainDet
    #define fmu1FmvssLeftAyGainDet          fu1SenSwSuspcs.u1FmvssLeftAyGainDet
    #define fmu1FmvssRightSasGainDet        fu1SenSwSuspcs.u1FmvssRightSasGainDet
    #define fmu1FmvssLeftSasGainDet         fu1SenSwSuspcs.u1FmvssLeftSasGainDet

    #define fmu1ServiceBrkByBLSPressed      fu1SenSwSuspcs.u1ServiceBrkPressedByBLS
    #define fmu1ServiceBrkByBLSDepressed    fu1SenSwSuspcs.u1ServiceBrkDepressedByBLS
    #define fmu1ServiceBrkByPedalPressed    fu1SenSwSuspcs.u1ServiceBrkPressedByPedal
    #define fmu1ServiceBrkByPedalDepressed  fu1SenSwSuspcs.u1ServiceBrkDepressedByPedal

    #define fu1FmvssVrefChkStartFlg         fu1SenSwSuspcs.u1FmvssVrefChkStartFlg


extern fu1SenSwSuspcs_t		fu1SenSwSuspcs;

//FBIgnCheck.h 복사
typedef struct
{
	uBitType u1ErrUpdate            :1;
	uBitType u1COPReset             :1;
	uBitType fbu1IGNUnd6V           :1;
	uBitType fbu1LowSupressedErr    :1;
	uBitType u1VBBDRV               :1;
	uBitType u1Vbat2StbFlg 	        :1;
	uBitType u1PreDrvShutDownReq_t  :1;
	uBitType u1PreDrvShutDownReq   	:1;
	uBitType u1DelayedMotorInterupt :1;
	uBitType u1HwIgnState :1;
}IGNCheck_t;
#define fbu1UnderVoltErrUpdate 		fbIGNCheck.u1ErrUpdate
#define fbu1COPResetRequest			fbIGNCheck.u1COPReset
#define fbu1IGNUnd6VDet				fbIGNCheck.fbu1IGNUnd6V
#define fbu1LowSupressedErrDet		fbIGNCheck.fbu1LowSupressedErr
#define fbu1VBBDRV		            fbIGNCheck.u1VBBDRV
#define fbu1Vbat2StbFlg		        fbIGNCheck.u1Vbat2StbFlg
#define fbu1PreDrvShutDownReq_t		fbIGNCheck.u1PreDrvShutDownReq_t
#define fu1PreDrvShutDownReq		fbIGNCheck.u1PreDrvShutDownReq
#define fu1DelayedMotorInterupt	    fbIGNCheck.u1DelayedMotorInterupt
#define fu1HwIgnState	        	    fbIGNCheck.u1HwIgnState
extern IGNCheck_t 	fbIGNCheck;

//WContlPwrSgnl.h 복사
/*****************************************************************************/
/* Global macros for export                                                  */
/*****************************************************************************/
#define CE_ON_STATE     0x00
#define CE_OFF_STATE    0x01

typedef struct
{
	uint8	EMS1_counter;
	uint8	EMS2_counter;
}ERROR_FLAG_COUNTER;





#endif /* ERP_TYPES_H_ */
