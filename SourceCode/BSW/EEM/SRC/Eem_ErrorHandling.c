/**
 * @defgroup Eem_Main Eem_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_ErrorHandling.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Main.h"
#include "Eem_ErrorHandling.h"
#include "Eem_ErrorInfoTable.h"

#include "common.h"

#include "Erp_Types.h"
#include <string.h>

/*#include "LCAVHCallControl.h"*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define EEM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"

static uint16  gEEM_u16LoopCount;
static uint16  gEEM_LampOnCount;
static uint16  gEEM_FuncLampOnCount, gEEM_FuncLampOffCount;
static uint16  gEEM_IgnONCount;
static boolean gEEM_bInit;
static EEM_FDCInfoType  gEEM_FDCInfoTable[EEM_Err_id_Max];
static EEM_ErrInfoType  gEEM_ErrInfoTable[EEM_Err_id_Max];
static EEM_FailSusInfoType gEEM_FailInfo;
static EEM_FailSusInfoType gEEM_SuspectInfo;
static EEM_CtrlIhbType  gEEM_CtrlIhbInfo;
static EEM_LampInfoType gEEM_LampInfo;

static uint8 gEEM_IntDTC[ERROR_BYTE_NO];
static uint8 gEEM_TestComplete[ERROR_BYTE_NO]; 

#if (FLEX_SWT_APPLY == ENABLE)
static uint8 gFlexSwCnt;
uint8 gFlexSwState;
#endif

/* TODO : Excel In/Out */
extern WssP_MainWssPlauData_t WssP_MainWssPlauData;
extern Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
extern Abc_CtrlFunctionLamp_t Abc_CtrlFunctionLamp;
extern Ioc_InputCS1msSwtMonInfo_t Ioc_InputCS1msSwtMonInfo;
extern uint8_t u8IdbCtrlInhibitFlg;
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define EEM_MAIN_START_SEC_CODE
#include "Eem_MemMap.h"

static void EEM_LoopCount(void);
static void EEM_CheckError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CalcFDCDebounce(uint16 i);
static void EEM_ProcessFDCDebounce(EEM_ErrIdType id);

static void EEM_ClearFailRelatedInfo(void);
static void EEM_CheckWssError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckValveError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckMotorError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckMPSError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckMGDError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckECUHwError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckSysPwrError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckSenPwrError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckAsicError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckYawError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckAyError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckAxError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckSASError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckPressureError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckSwitchError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckPedalError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckRelayError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckTempError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckCANError(Eem_Main_HdrBusType *pEem_Data);
static void EEM_CheckCodingError(Eem_Main_HdrBusType *pEem_Data);
static Std_ReturnType EEM_SetErrorInfo(EEM_ErrIdType id, uint8 u8EER_Err, boolean *pIntErrFlg, boolean *pIntSusFlg);
static boolean EEM_GetErrorFlag(EEM_ErrIdType id);
static boolean EEM_GetSuspetFlag(EEM_ErrIdType id);
static Std_ReturnType EEM_ResetErrorInfo(EEM_ErrIdType id);
static void EEM_ProcessDemProxy(void);
static void EEM_ClearAllError(void);
static void EEM_LogicDisableFlag(Eem_Main_HdrBusType *pEem_Data);
static void EEM_MakeInternalDTC(void);
static void EEM_MakeTestComplete(void);
static void EEM_FMVSS126Proc(void);
static void EEM_LampRequest(Eem_Main_HdrBusType *pEem_Data);
static void EEM_BBSLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_ABSEBDLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_VDCLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_VDCOffLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_VDCFunctionLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_HDCLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_AVHLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);
static void EEM_BuzzerRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data);

/* TODO */
extern void FE_vWriteErrorCode(UINT e_code, UCHAR u8DtcFtb);
extern void FE_vClearAllError(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void EEM_ErrorHandler(Eem_Main_HdrBusType *pEem_Data)
{
    if(gEEM_bInit == TRUE)
    {
        /* FDC & Error Flag */
        EEM_CheckError(pEem_Data);

        EEM_ProcessDemProxy();


        /* Error Flags */
        EEM_LogicDisableFlag(pEem_Data);

        EEM_MakeInternalDTC();

        EEM_MakeTestComplete();


        /* FMVSS126 */
        EEM_FMVSS126Proc();


        /* Warning Lamp */
        EEM_LampRequest(pEem_Data);

        /* Loop Count */
        EEM_LoopCount();
    }
}

void EEM_Initialize(void)
{
    memset((void *)&gEEM_FDCInfoTable, 0x00, sizeof(gEEM_FDCInfoTable));
    memset((void *)&gEEM_ErrInfoTable, 0x00, sizeof(gEEM_ErrInfoTable));
    memset((void *)&gEEM_FailInfo, 0x00, sizeof(gEEM_FailInfo));
    memset((void *)&gEEM_SuspectInfo, 0x00, sizeof(gEEM_SuspectInfo));
    memset((void *)&gEEM_CtrlIhbInfo, 0x00, sizeof(gEEM_CtrlIhbInfo));
    memset((void *)&gEEM_LampInfo, 0x00, sizeof(gEEM_LampInfo));

    gEEM_FuncLampOnCount = 0;
    gEEM_FuncLampOffCount = 0;

    gEEM_LampOnCount = 0;
    gEEM_IgnONCount = 0;
    gEEM_u16LoopCount = 0;
    
    gEEM_bInit = TRUE;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void EEM_LoopCount(void)
{
    gEEM_u16LoopCount++;
    if(gEEM_u16LoopCount >= 4)
    {
        gEEM_u16LoopCount = 0;
    }  
}

static void EEM_CheckError(Eem_Main_HdrBusType *pEem_Data)
{
    EEM_CheckWssError(pEem_Data);

    EEM_CheckValveError(pEem_Data);

    EEM_CheckMotorError(pEem_Data);

    EEM_CheckMPSError(pEem_Data);

    EEM_CheckMGDError(pEem_Data);

    EEM_CheckECUHwError(pEem_Data);

    EEM_CheckSysPwrError(pEem_Data);

    EEM_CheckSenPwrError(pEem_Data);

    EEM_CheckAsicError(pEem_Data);

    EEM_CheckYawError(pEem_Data);

    EEM_CheckAyError(pEem_Data);

    EEM_CheckAxError(pEem_Data);

    EEM_CheckSASError(pEem_Data);

    EEM_CheckPressureError(pEem_Data);

    EEM_CheckSwitchError(pEem_Data);

    EEM_CheckPedalError(pEem_Data);

    EEM_CheckRelayError(pEem_Data);

    EEM_CheckTempError(pEem_Data);

    EEM_CheckCANError(pEem_Data);

    EEM_CheckCodingError(pEem_Data);
}

static void EEM_ClearFailRelatedInfo(void)
{
    (void)memset(&gEEM_FailInfo, 0x00, sizeof(gEEM_FailInfo)); 
    (void)memset(&gEEM_SuspectInfo, 0x00, sizeof(gEEM_SuspectInfo));
    (void)memset(&gEEM_CtrlIhbInfo, 0x00, sizeof(gEEM_CtrlIhbInfo));
}

static void EEM_CheckWssError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    WssMonData_t *pWssMonData = NULL;
    WssPlauData_t *pWssPlauData = NULL;

    boolean bErr_WssFL_Open = FALSE, bErr_WssFL_Short = FALSE, bErr_WssFL_OverTemp = FALSE, bErr_WssFL_Leakage = FALSE;
    boolean bErr_WssFL_Airgap = FALSE, bErr_WssFL_SpeedJumpP = FALSE, bErr_WssFL_SpeedJumpM = FALSE;
    boolean bErr_WssFL_WrongExciter_Low = FALSE, bErr_WssFL_WrongExciter_High = FALSE, bErr_WssFL_WrongInstalled = FALSE;
    boolean bErr_WssFL_Phase = FALSE;

    boolean bErr_WssFR_Open = FALSE, bErr_WssFR_Short = FALSE, bErr_WssFR_OverTemp = FALSE, bErr_WssFR_Leakage = FALSE;
    boolean bErr_WssFR_Airgap = FALSE, bErr_WssFR_SpeedJumpP = FALSE, bErr_WssFR_SpeedJumpM = FALSE;
    boolean bErr_WssFR_WrongExciter_Low = FALSE, bErr_WssFR_WrongExciter_High = FALSE, bErr_WssFR_WrongInstalled = FALSE;
    boolean bErr_WssFR_Phase = FALSE;

    boolean bErr_WssRL_Open = FALSE, bErr_WssRL_Short = FALSE, bErr_WssRL_OverTemp = FALSE, bErr_WssRL_Leakage = FALSE;
    boolean bErr_WssRL_Airgap = FALSE, bErr_WssRL_SpeedJumpP = FALSE, bErr_WssRL_SpeedJumpM = FALSE;
    boolean bErr_WssRL_WrongExciter_Low = FALSE, bErr_WssRL_WrongExciter_High = FALSE, bErr_WssRL_WrongInstalled = FALSE;
    boolean bErr_WssRL_Phase = FALSE;    

    boolean bErr_WssRR_Open = FALSE, bErr_WssRR_Short = FALSE, bErr_WssRR_OverTemp = FALSE, bErr_WssRR_Leakage = FALSE;
    boolean bErr_WssRR_Airgap = FALSE, bErr_WssRR_SpeedJumpP = FALSE, bErr_WssRR_SpeedJumpM = FALSE;
    boolean bErr_WssRR_WrongExciter_Low = FALSE, bErr_WssRR_WrongExciter_High = FALSE, bErr_WssRR_WrongInstalled = FALSE;
    boolean bErr_WssRR_Phase = FALSE;

    boolean bSus_WssFL_Open = FALSE, bSus_WssFL_Short = FALSE, bSus_WssFL_OverTemp = FALSE, bSus_WssFL_Leakage = FALSE;
    boolean bSus_WssFL_Airgap = FALSE, bSus_WssFL_SpeedJumpP = FALSE, bSus_WssFL_SpeedJumpM = FALSE;
    boolean bSus_WssFL_WrongExciter_Low = FALSE, bSus_WssFL_WrongExciter_High = FALSE, bSus_WssFL_WrongInstalled = FALSE;
    boolean bSus_WssFL_Phase = FALSE;

    boolean bSus_WssFR_Open = FALSE, bSus_WssFR_Short = FALSE, bSus_WssFR_OverTemp = FALSE, bSus_WssFR_Leakage = FALSE;
    boolean bSus_WssFR_Airgap = FALSE, bSus_WssFR_SpeedJumpP = FALSE, bSus_WssFR_SpeedJumpM = FALSE;
    boolean bSus_WssFR_WrongExciter_Low = FALSE, bSus_WssFR_WrongExciter_High = FALSE, bSus_WssFR_WrongInstalled = FALSE;
    boolean bSus_WssFR_Phase = FALSE;

    boolean bSus_WssRL_Open = FALSE, bSus_WssRL_Short = FALSE, bSus_WssRL_OverTemp = FALSE, bSus_WssRL_Leakage = FALSE;
    boolean bSus_WssRL_Airgap = FALSE, bSus_WssRL_SpeedJumpP = FALSE, bSus_WssRL_SpeedJumpM = FALSE;
    boolean bSus_WssRL_WrongExciter_Low = FALSE, bSus_WssRL_WrongExciter_High = FALSE, bSus_WssRL_WrongInstalled = FALSE;
    boolean bSus_WssRL_Phase = FALSE;    

    boolean bSus_WssRR_Open = FALSE, bSus_WssRR_Short = FALSE, bSus_WssRR_OverTemp = FALSE, bSus_WssRR_Leakage = FALSE;
    boolean bSus_WssRR_Airgap = FALSE, bSus_WssRR_SpeedJumpP = FALSE, bSus_WssRR_SpeedJumpM = FALSE;
    boolean bSus_WssRR_WrongExciter_Low = FALSE, bSus_WssRR_WrongExciter_High = FALSE, bSus_WssRR_WrongInstalled = FALSE;
    boolean bSus_WssRR_Phase = FALSE;    
    
    pWssMonData = &(pEem_Data->Eem_MainWssMonData);
    pWssPlauData = &(WssP_MainWssPlauData);

    /* WSS FL */
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_Open, pWssMonData->WssM_FlOpen_Err, &bErr_WssFL_Open, &bSus_WssFL_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_Short, pWssMonData->WssM_FlShort_Err, &bErr_WssFL_Short, &bSus_WssFL_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_OverTemp, pWssMonData->WssM_FlOverTemp_Err, &bErr_WssFL_OverTemp, &bSus_WssFL_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_Leakage, pWssMonData->WssM_FlLeakage_Err, &bErr_WssFL_Leakage, &bSus_WssFL_Leakage);    

    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_Airgap, pWssPlauData->WssP_FL_AirGapErr, &bErr_WssFL_Airgap, &bSus_WssFL_Airgap);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_SpeedJump_Plus, pWssPlauData->WssP_FL_PJumpErr, &bErr_WssFL_SpeedJumpP, &bSus_WssFL_SpeedJumpP);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_SpeedJump_Minus, pWssPlauData->WssP_FL_MJumpErr, &bErr_WssFL_SpeedJumpM, &bSus_WssFL_SpeedJumpM);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_WrongExciter, pWssPlauData->WssP_FL_ExciterErr, &bErr_WssFL_WrongExciter_Low, &bSus_WssFL_WrongExciter_Low);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_WrongInstalled, pWssPlauData->WssP_FL_WheelTypeSwapErr, &bErr_WssFL_WrongInstalled, &bSus_WssFL_WrongInstalled);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFL_Phase, pWssPlauData->WssP_FL_PhaseErr, &bErr_WssFL_Phase, &bSus_WssFL_Phase);

    /* WSS FR */
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_Open, pWssMonData->WssM_FrOpen_Err, &bErr_WssFR_Open, &bSus_WssFR_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_Short, pWssMonData->WssM_FrShort_Err, &bErr_WssFR_Short, &bSus_WssFR_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_OverTemp, pWssMonData->WssM_FrOverTemp_Err, &bErr_WssFR_OverTemp, &bSus_WssFR_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_Leakage, pWssMonData->WssM_FrLeakage_Err, &bErr_WssFR_Leakage, &bSus_WssFR_Leakage);    

    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_Airgap, pWssPlauData->WssP_FR_AirGapErr, &bErr_WssFR_Airgap, &bSus_WssFR_Airgap);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_SpeedJump_Plus, pWssPlauData->WssP_FR_PJumpErr, &bErr_WssFR_SpeedJumpP, &bSus_WssFR_SpeedJumpP);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_SpeedJump_Minus, pWssPlauData->WssP_FR_MJumpErr, &bErr_WssFR_SpeedJumpM, &bSus_WssFR_SpeedJumpM);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_WrongExciter, pWssPlauData->WssP_FR_ExciterErr, &bErr_WssFR_WrongExciter_Low, &bSus_WssFR_WrongExciter_Low);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_WrongInstalled, pWssPlauData->WssP_FR_WheelTypeSwapErr, &bErr_WssFR_WrongInstalled, &bSus_WssFR_WrongInstalled);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssFR_Phase, pWssPlauData->WssP_FR_PhaseErr, &bErr_WssFR_Phase, &bSus_WssFR_Phase);

    /* WSS RL */
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_Open, pWssMonData->WssM_RlOpen_Err, &bErr_WssRL_Open, &bSus_WssRL_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_Short, pWssMonData->WssM_RlShort_Err, &bErr_WssRL_Short, &bSus_WssRL_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_OverTemp, pWssMonData->WssM_RlOverTemp_Err, &bErr_WssRL_OverTemp, &bSus_WssRL_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_Leakage, pWssMonData->WssM_RlLeakage_Err, &bErr_WssRL_Leakage, &bSus_WssRL_Leakage);    

    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_Airgap, pWssPlauData->WssP_RL_AirGapErr, &bErr_WssRL_Airgap, &bSus_WssRL_Airgap);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_SpeedJump_Plus, pWssPlauData->WssP_RL_PJumpErr, &bErr_WssRL_SpeedJumpP, &bSus_WssRL_SpeedJumpP);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_SpeedJump_Minus, pWssPlauData->WssP_RL_MJumpErr, &bErr_WssRL_SpeedJumpM, &bSus_WssRL_SpeedJumpM);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_WrongExciter, pWssPlauData->WssP_RL_ExciterErr, &bErr_WssRL_WrongExciter_Low, &bSus_WssRL_WrongExciter_Low);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_WrongInstalled, pWssPlauData->WssP_RL_WheelTypeSwapErr, &bErr_WssRL_WrongInstalled, &bSus_WssRL_WrongInstalled);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRL_Phase, pWssPlauData->WssP_RL_PhaseErr, &bErr_WssRL_Phase, &bSus_WssRL_Phase);

    /* WSS RR */
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_Open, pWssMonData->WssM_RrOpen_Err, &bErr_WssRR_Open, &bSus_WssRR_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_Short, pWssMonData->WssM_RrShort_Err, &bErr_WssRR_Short, &bSus_WssRR_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_OverTemp, pWssMonData->WssM_RrOverTemp_Err, &bErr_WssRR_OverTemp, &bSus_WssRR_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_Leakage, pWssMonData->WssM_RrLeakage_Err, &bErr_WssRR_Leakage, &bSus_WssRR_Leakage);    

    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_Airgap, pWssPlauData->WssP_RR_AirGapErr, &bErr_WssRR_Airgap, &bSus_WssRR_Airgap);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_SpeedJump_Plus, pWssPlauData->WssP_RR_PJumpErr, &bErr_WssRR_SpeedJumpP, &bSus_WssRR_SpeedJumpP);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_SpeedJump_Minus, pWssPlauData->WssP_RR_MJumpErr, &bErr_WssRR_SpeedJumpM, &bSus_WssRR_SpeedJumpM);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_WrongExciter, pWssPlauData->WssP_RR_ExciterErr, &bErr_WssRR_WrongExciter_Low, &bSus_WssRR_WrongExciter_Low);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_WrongInstalled, pWssPlauData->WssP_RR_WheelTypeSwapErr, &bErr_WssRR_WrongInstalled, &bSus_WssRR_WrongInstalled);
    ret = EEM_SetErrorInfo(EEM_Err_id_WssRR_Phase, pWssPlauData->WssP_RR_PhaseErr, &bErr_WssRR_Phase, &bSus_WssRR_Phase);


    /* Fail Flag */
    gEEM_FailInfo.bWssFL = FALSE;
    if((bErr_WssFL_Open == TRUE) || (bErr_WssFL_Short == TRUE) || (bErr_WssFL_OverTemp == TRUE) ||
       (bErr_WssFL_Leakage == TRUE) || (bErr_WssFL_Airgap == TRUE) || (bErr_WssFL_SpeedJumpP == TRUE) ||
       (bErr_WssFL_SpeedJumpM == TRUE) || (bErr_WssFL_WrongExciter_Low == TRUE) || (bErr_WssFL_WrongExciter_High == TRUE) ||
       (bErr_WssFL_WrongInstalled == TRUE) || (bErr_WssFL_Phase == TRUE)
       )
    {
        gEEM_FailInfo.bWssFL = TRUE;
    }

    gEEM_FailInfo.bWssFR = FALSE;
    if((bErr_WssFR_Open == TRUE) || (bErr_WssFR_Short == TRUE) || (bErr_WssFR_OverTemp == TRUE) ||
       (bErr_WssFR_Leakage == TRUE) || (bErr_WssFR_Airgap == TRUE) || (bErr_WssFR_SpeedJumpP == TRUE) ||
       (bErr_WssFR_SpeedJumpM == TRUE) || (bErr_WssFR_WrongExciter_Low == TRUE) || (bErr_WssFR_WrongExciter_High == TRUE) ||
       (bErr_WssFR_WrongInstalled == TRUE) || (bErr_WssFR_Phase == TRUE)
       )
    {
        gEEM_FailInfo.bWssFR = TRUE;
    }    

    gEEM_FailInfo.bWssRL = FALSE;
    if((bErr_WssRL_Open == TRUE) || (bErr_WssRL_Short == TRUE) || (bErr_WssRL_OverTemp == TRUE) ||
       (bErr_WssRL_Leakage == TRUE) || (bErr_WssRL_Airgap == TRUE) || (bErr_WssRL_SpeedJumpP == TRUE) ||
       (bErr_WssRL_SpeedJumpM == TRUE) || (bErr_WssRL_WrongExciter_Low == TRUE) || (bErr_WssRL_WrongExciter_High == TRUE) ||
       (bErr_WssRL_WrongInstalled == TRUE) || (bErr_WssRL_Phase == TRUE)
       )
    {
        gEEM_FailInfo.bWssRL = TRUE;
    }      

    gEEM_FailInfo.bWssRR = FALSE;
    if((bErr_WssRR_Open == TRUE) || (bErr_WssRR_Short == TRUE) || (bErr_WssRR_OverTemp == TRUE) ||
       (bErr_WssRR_Leakage == TRUE) || (bErr_WssRR_Airgap == TRUE) || (bErr_WssRR_SpeedJumpP == TRUE) ||
       (bErr_WssRR_SpeedJumpM == TRUE) || (bErr_WssRR_WrongExciter_Low == TRUE) || (bErr_WssRR_WrongExciter_High == TRUE) ||
       (bErr_WssRR_WrongInstalled == TRUE) || (bErr_WssRR_Phase == TRUE)
       )
    {
        gEEM_FailInfo.bWssRR = TRUE;
    }     

    gEEM_FailInfo.bSameSideWss = FALSE;
    if(((gEEM_FailInfo.bWssFL == TRUE) && (gEEM_FailInfo.bWssFR == TRUE)) ||
        ((gEEM_FailInfo.bWssRL == TRUE) && (gEEM_FailInfo.bWssRR == TRUE)) ||
        ((gEEM_FailInfo.bWssFL == TRUE) && (gEEM_FailInfo.bWssRL == TRUE)) ||
        ((gEEM_FailInfo.bWssFR == TRUE) && (gEEM_FailInfo.bWssRR == TRUE))
       )
    {   
        gEEM_FailInfo.bSameSideWss = TRUE;
    }

    gEEM_FailInfo.bDiagonalWss = FALSE;
    if(((gEEM_FailInfo.bWssFL == TRUE) && (gEEM_FailInfo.bWssRR == TRUE)) ||
        ((gEEM_FailInfo.bWssFR == TRUE) && (gEEM_FailInfo.bWssRL == TRUE))
       )
    {
        gEEM_FailInfo.bDiagonalWss = TRUE;
    }

    gEEM_FailInfo.bFrontWss = FALSE;
    if((gEEM_FailInfo.bWssFL == TRUE) || (gEEM_FailInfo.bWssFR == TRUE))
    {
        gEEM_FailInfo.bFrontWss = TRUE;
    }

    gEEM_FailInfo.bRearWss = FALSE;
    if((gEEM_FailInfo.bWssRL == TRUE) || (gEEM_FailInfo.bWssRR == TRUE))
    {
        gEEM_FailInfo.bRearWss = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_WssFL = gEEM_FailInfo.bWssFL;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_WssFR = gEEM_FailInfo.bWssFR;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_WssRL = gEEM_FailInfo.bWssRL;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_WssRR = gEEM_FailInfo.bWssRR;

    pEem_Data->Eem_MainEemFailData.Eem_Fail_SameSideWss = gEEM_FailInfo.bSameSideWss;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_DiagonalWss = gEEM_FailInfo.bDiagonalWss;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_FrontWss = gEEM_FailInfo.bFrontWss;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_RearWss = gEEM_FailInfo.bRearWss;


    /* Suspect Flag */
    gEEM_SuspectInfo.bWssFL = FALSE;
    if((bSus_WssFL_Open == TRUE) || (bSus_WssFL_Short == TRUE) || (bSus_WssFL_OverTemp == TRUE) ||
       (bSus_WssFL_Leakage == TRUE) || (bSus_WssFL_Airgap == TRUE) || (bSus_WssFL_SpeedJumpP == TRUE) ||
       (bSus_WssFL_SpeedJumpM == TRUE) || (bSus_WssFL_WrongExciter_Low == TRUE) || (bSus_WssFL_WrongExciter_High == TRUE) ||
       (bSus_WssFL_WrongInstalled == TRUE) || (bSus_WssFL_Phase == TRUE)
       )
    {
        gEEM_SuspectInfo.bWssFL = TRUE;
    }

    gEEM_SuspectInfo.bWssFR = FALSE;
    if((bSus_WssFR_Open == TRUE) || (bSus_WssFR_Short == TRUE) || (bSus_WssFR_OverTemp == TRUE) ||
       (bSus_WssFR_Leakage == TRUE) || (bSus_WssFR_Airgap == TRUE) || (bSus_WssFR_SpeedJumpP == TRUE) ||
       (bSus_WssFR_SpeedJumpM == TRUE) || (bSus_WssFR_WrongExciter_Low == TRUE) || (bSus_WssFR_WrongExciter_High == TRUE) ||
       (bSus_WssFR_WrongInstalled == TRUE) || (bSus_WssFR_Phase == TRUE)
       )
    {
        gEEM_SuspectInfo.bWssFR = TRUE;
    }    

    gEEM_SuspectInfo.bWssRL = FALSE;
    if((bSus_WssRL_Open == TRUE) || (bSus_WssRL_Short == TRUE) || (bSus_WssRL_OverTemp == TRUE) ||
       (bSus_WssRL_Leakage == TRUE) || (bSus_WssRL_Airgap == TRUE) || (bSus_WssRL_SpeedJumpP == TRUE) ||
       (bSus_WssRL_SpeedJumpM == TRUE) || (bSus_WssRL_WrongExciter_Low == TRUE) || (bSus_WssRL_WrongExciter_High == TRUE) ||
       (bSus_WssRL_WrongInstalled == TRUE) || (bSus_WssRL_Phase == TRUE)
       )
    {
        gEEM_SuspectInfo.bWssRL = TRUE;
    }      

    gEEM_SuspectInfo.bWssRR = FALSE;
    if((bSus_WssRR_Open == TRUE) || (bSus_WssRR_Short == TRUE) || (bSus_WssRR_OverTemp == TRUE) ||
       (bSus_WssRR_Leakage == TRUE) || (bSus_WssRR_Airgap == TRUE) || (bSus_WssRR_SpeedJumpP == TRUE) ||
       (bSus_WssRR_SpeedJumpM == TRUE) || (bSus_WssRR_WrongExciter_Low == TRUE) || (bSus_WssRR_WrongExciter_High == TRUE) ||
       (bSus_WssRR_WrongInstalled == TRUE) || (bSus_WssRR_Phase == TRUE)
       )
    {
        gEEM_SuspectInfo.bWssRR = TRUE;
    }     

    gEEM_SuspectInfo.bSameSideWss = FALSE;
    if(((gEEM_SuspectInfo.bWssFL == TRUE) && (gEEM_SuspectInfo.bWssFR == TRUE)) ||
        ((gEEM_SuspectInfo.bWssRL == TRUE) && (gEEM_SuspectInfo.bWssRR == TRUE)) ||
        ((gEEM_SuspectInfo.bWssFL == TRUE) && (gEEM_SuspectInfo.bWssRL == TRUE)) ||
        ((gEEM_SuspectInfo.bWssFR == TRUE) && (gEEM_SuspectInfo.bWssRR == TRUE))
       )
    {   
        gEEM_SuspectInfo.bSameSideWss = TRUE;
    }

    gEEM_SuspectInfo.bDiagonalWss = FALSE;
    if(((gEEM_SuspectInfo.bWssFL == TRUE) && (gEEM_SuspectInfo.bWssRR == TRUE)) ||
        ((gEEM_SuspectInfo.bWssFR == TRUE) && (gEEM_SuspectInfo.bWssRL == TRUE))
       )
    {
        gEEM_SuspectInfo.bDiagonalWss = TRUE;
    }

    gEEM_SuspectInfo.bFrontWss = FALSE;
    if((gEEM_SuspectInfo.bWssFL == TRUE) || (gEEM_SuspectInfo.bWssFR == TRUE))
    {
        gEEM_SuspectInfo.bFrontWss = TRUE;
    }

    gEEM_SuspectInfo.bRearWss = FALSE;
    if((gEEM_SuspectInfo.bWssRL == TRUE) || (gEEM_SuspectInfo.bWssRR == TRUE))
    {
        gEEM_SuspectInfo.bRearWss = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_WssFL = gEEM_SuspectInfo.bWssFL;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_WssFR = gEEM_SuspectInfo.bWssFR;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_WssRL = gEEM_SuspectInfo.bWssRL;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_WssRR = gEEM_SuspectInfo.bWssRR;

    /* pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_SameSideWss = gEEM_SuspectInfo.bSameSideWss; TODO */
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss = gEEM_SuspectInfo.bDiagonalWss;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_FrontWss = gEEM_SuspectInfo.bFrontWss;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_RearWss = gEEM_SuspectInfo.bRearWss;
}

static void EEM_CheckValveError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    AbsVlvMData_t *pAbsVlvMData = NULL;
    BBSVlvErrInfo_t *pBbsVlvErrInfo = NULL;

    boolean bErr_Batt1Fuse_Open = FALSE, bErr_BBSValveRelay_Open = FALSE, bErr_BBSValveRelay_S2G = FALSE;
    boolean bErr_BBSValveRelay_S2B = FALSE, bErr_BBSValveRelay_OverTemp = FALSE, bErr_BBSValveRelay_ShutdownLine = FALSE;

    boolean bErr_SIM_Open = FALSE, bErr_SIM_PsvOpen = FALSE, bErr_SIM_Short = FALSE, bErr_SIM_CurReg = FALSE;
    boolean bErr_CutP_Open = FALSE, bErr_CutP_PsvOpen = FALSE, bErr_CutP_Short = FALSE, bErr_CutP_CurReg = FALSE;
    boolean bErr_CutS_Open = FALSE, bErr_CutS_PsvOpen = FALSE, bErr_CutS_Short = FALSE, bErr_CutS_CurReg = FALSE;
    boolean bErr_Rlv_Open = FALSE, bErr_Rlv_PsvOpen = FALSE, bErr_Rlv_Short = FALSE, bErr_Rlv_CurReg = FALSE;
    boolean bErr_CircV_Open = FALSE, bErr_CircV_PsvOpen = FALSE, bErr_CircV_Short = FALSE, bErr_CircV_CurReg = FALSE;

    boolean bErr_ESCValveRelay_Open = FALSE, bErr_ESCValveRelay_S2G = FALSE, bErr_ESCValveRelay_S2B = FALSE, bErr_ESCValveRelay_ShutdownLine = FALSE;

    boolean bErr_FLNO_Open = FALSE, bErr_FLNO_PsvOpen = FALSE, bErr_FLNO_Short = FALSE, bErr_FLNO_OverTemp = FALSE, bErr_FLNO_CurReg = FALSE;
    boolean bErr_FRNO_Open = FALSE, bErr_FRNO_PsvOpen = FALSE, bErr_FRNO_Short = FALSE, bErr_FRNO_OverTemp = FALSE, bErr_FRNO_CurReg = FALSE;
    boolean bErr_RLNO_Open = FALSE, bErr_RLNO_PsvOpen = FALSE, bErr_RLNO_Short = FALSE, bErr_RLNO_OverTemp = FALSE, bErr_RLNO_CurReg = FALSE;
    boolean bErr_RRNO_Open = FALSE, bErr_RRNO_PsvOpen = FALSE, bErr_RRNO_Short = FALSE, bErr_RRNO_OverTemp = FALSE, bErr_RRNO_CurReg = FALSE;
    boolean bErr_BAL_Open = FALSE, bErr_BAL_PsvOpen = FALSE, bErr_BAL_Short = FALSE, bErr_BAL_OverTemp = FALSE, bErr_BAL_CurReg = FALSE;
    boolean bErr_PD_Open = FALSE, bErr_PD_PsvOpen = FALSE, bErr_PD_Short = FALSE, bErr_PD_OverTemp = FALSE, bErr_PD_CurReg = FALSE;
    boolean bErr_RESP_Open = FALSE, bErr_RESP_PsvOpen = FALSE, bErr_RESP_Short = FALSE, bErr_RESP_OverTemp = FALSE, bErr_RESP_CurReg = FALSE;
    boolean bErr_FLNC_Open = FALSE, bErr_FLNC_PsvOpen = FALSE, bErr_FLNC_Short = FALSE, bErr_FLNC_OverTemp = FALSE;
    boolean bErr_FRNC_Open = FALSE, bErr_FRNC_PsvOpen = FALSE, bErr_FRNC_Short = FALSE, bErr_FRNC_OverTemp = FALSE;
    boolean bErr_RLNC_Open = FALSE, bErr_RLNC_PsvOpen = FALSE, bErr_RLNC_Short = FALSE, bErr_RLNC_OverTemp = FALSE;
    boolean bErr_RRNC_Open = FALSE, bErr_RRNC_PsvOpen = FALSE, bErr_RRNC_Short = FALSE, bErr_RRNC_OverTemp = FALSE;

    boolean bSus_Batt1Fuse_Open = FALSE, bSus_BBSValveRelay_Open = FALSE, bSus_BBSValveRelay_S2G = FALSE;
    boolean bSus_BBSValveRelay_S2B = FALSE, bSus_BBSValveRelay_OverTemp = FALSE, bSus_BBSValveRelay_ShutdownLine = FALSE;

    boolean bSus_SIM_Open = FALSE, bSus_SIM_PsvOpen = FALSE, bSus_SIM_Short = FALSE, bSus_SIM_CurReg = FALSE;
    boolean bSus_CutP_Open = FALSE, bSus_CutP_PsvOpen = FALSE, bSus_CutP_Short = FALSE, bSus_CutP_CurReg = FALSE;
    boolean bSus_CutS_Open = FALSE, bSus_CutS_PsvOpen = FALSE, bSus_CutS_Short = FALSE, bSus_CutS_CurReg = FALSE;
    boolean bSus_Rlv_Open = FALSE, bSus_Rlv_PsvOpen = FALSE, bSus_Rlv_Short = FALSE, bSus_Rlv_CurReg = FALSE;
    boolean bSus_CircV_Open = FALSE, bSus_CircV_PsvOpen = FALSE, bSus_CircV_Short = FALSE, bSus_CircV_CurReg = FALSE;

    boolean bSus_ESCValveRelay_Open = FALSE, bSus_ESCValveRelay_S2G = FALSE, bSus_ESCValveRelay_S2B = FALSE, bSus_ESCValveRelay_ShutdownLine = FALSE;

    boolean bSus_FLNO_Open = FALSE, bSus_FLNO_PsvOpen = FALSE, bSus_FLNO_Short = FALSE, bSus_FLNO_OverTemp = FALSE, bSus_FLNO_CurReg = FALSE;
    boolean bSus_FRNO_Open = FALSE, bSus_FRNO_PsvOpen = FALSE, bSus_FRNO_Short = FALSE, bSus_FRNO_OverTemp = FALSE, bSus_FRNO_CurReg = FALSE;
    boolean bSus_RLNO_Open = FALSE, bSus_RLNO_PsvOpen = FALSE, bSus_RLNO_Short = FALSE, bSus_RLNO_OverTemp = FALSE, bSus_RLNO_CurReg = FALSE;
    boolean bSus_RRNO_Open = FALSE, bSus_RRNO_PsvOpen = FALSE, bSus_RRNO_Short = FALSE, bSus_RRNO_OverTemp = FALSE, bSus_RRNO_CurReg = FALSE;
    boolean bSus_BAL_Open = FALSE, bSus_BAL_PsvOpen = FALSE, bSus_BAL_Short = FALSE, bSus_BAL_OverTemp = FALSE, bSus_BAL_CurReg = FALSE;
    boolean bSus_PD_Open = FALSE, bSus_PD_PsvOpen = FALSE, bSus_PD_Short = FALSE, bSus_PD_OverTemp = FALSE, bSus_PD_CurReg = FALSE;
    boolean bSus_RESP_Open = FALSE, bSus_RESP_PsvOpen = FALSE, bSus_RESP_Short = FALSE, bSus_RESP_OverTemp = FALSE, bSus_RESP_CurReg = FALSE;
    boolean bSus_FLNC_Open = FALSE, bSus_FLNC_PsvOpen = FALSE, bSus_FLNC_Short = FALSE, bSus_FLNC_OverTemp = FALSE;
    boolean bSus_FRNC_Open = FALSE, bSus_FRNC_PsvOpen = FALSE, bSus_FRNC_Short = FALSE, bSus_FRNC_OverTemp = FALSE;
    boolean bSus_RLNC_Open = FALSE, bSus_RLNC_PsvOpen = FALSE, bSus_RLNC_Short = FALSE, bSus_RLNC_OverTemp = FALSE;
    boolean bSus_RRNC_Open = FALSE, bSus_RRNC_PsvOpen = FALSE, bSus_RRNC_Short = FALSE, bSus_RRNC_OverTemp = FALSE;    

    pAbsVlvMData = &(pEem_Data->Eem_MainAbsVlvMData);
    pBbsVlvErrInfo = &(pEem_Data->Eem_MainBBSVlvErrInfo);

    /* BBS Valve Relay */
    ret = EEM_SetErrorInfo(EEM_Err_id_Batt1Fuse_Open, pBbsVlvErrInfo->Batt1Fuse_Open_Err, &bErr_Batt1Fuse_Open, &bSus_Batt1Fuse_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_BBSValveRelay_Open, pBbsVlvErrInfo->BbsVlvRelay_Open_Err, &bErr_BBSValveRelay_Open, &bSus_BBSValveRelay_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_BBSValveRelay_S2G, pBbsVlvErrInfo->BbsVlvRelay_S2G_Err, &bErr_BBSValveRelay_S2G, &bSus_BBSValveRelay_S2G);
    ret = EEM_SetErrorInfo(EEM_Err_id_BBSValveRelay_S2B, pBbsVlvErrInfo->BbsVlvRelay_S2B_Err, &bErr_BBSValveRelay_S2B, &bSus_BBSValveRelay_S2B);
    ret = EEM_SetErrorInfo(EEM_Err_id_BBSValveRelay_OverTemp, pBbsVlvErrInfo->BbsVlvRelay_OverTemp_Err, &bErr_BBSValveRelay_OverTemp, &bSus_BBSValveRelay_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_BBSValveRelay_ShutdownLine, pBbsVlvErrInfo->BbsVlvRelay_ShutdownLine_Err, &bErr_BBSValveRelay_ShutdownLine, &bSus_BBSValveRelay_ShutdownLine);
    
    /* BBS Valve */
    ret = EEM_SetErrorInfo(EEM_Err_id_SIM_Open, pBbsVlvErrInfo->BbsVlv_Sim_Open_Err, &bErr_SIM_Open, &bSus_SIM_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIM_PsvOpen, pBbsVlvErrInfo->BbsVlv_Sim_PsvOpen_Err, &bErr_SIM_PsvOpen, &bSus_SIM_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIM_Short, pBbsVlvErrInfo->BbsVlv_Sim_Short_Err, &bErr_SIM_Short, &bSus_SIM_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIM_CurReg, pBbsVlvErrInfo->BbsVlv_Sim_CurReg_Err, &bErr_SIM_CurReg, &bSus_SIM_CurReg);

    ret = EEM_SetErrorInfo(EEM_Err_id_CutP_Open, pBbsVlvErrInfo->BbsVlv_CutP_Open_Err, &bErr_CutP_Open, &bSus_CutP_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_CutP_PsvOpen, pBbsVlvErrInfo->BbsVlv_CutP_PsvOpen_Err, &bErr_CutP_PsvOpen, &bSus_CutP_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_CutP_Short, pBbsVlvErrInfo->BbsVlv_CutP_Short_Err, &bErr_CutP_Short, &bSus_CutP_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_CutP_CurReg, pBbsVlvErrInfo->BbsVlv_CutP_CurReg_Err, &bErr_CutP_CurReg, &bSus_CutP_CurReg);

    ret = EEM_SetErrorInfo(EEM_Err_id_CutS_Open, pBbsVlvErrInfo->BbsVlv_CutS_Open_Err, &bErr_CutS_Open, &bSus_CutS_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_CutS_PsvOpen, pBbsVlvErrInfo->BbsVlv_CutS_PsvOpen_Err, &bErr_CutS_PsvOpen, &bSus_CutS_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_CutS_Short, pBbsVlvErrInfo->BbsVlv_CutS_Short_Err, &bErr_CutS_Short, &bSus_CutS_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_CutS_CurReg, pBbsVlvErrInfo->BbsVlv_CutS_CurReg_Err, &bErr_CutS_CurReg, &bSus_CutS_CurReg);

    ret = EEM_SetErrorInfo(EEM_Err_id_Rlv_Open, pBbsVlvErrInfo->BbsVlv_Rlv_Open_Err, &bErr_Rlv_Open, &bSus_Rlv_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_Rlv_PsvOpen, pBbsVlvErrInfo->BbsVlv_Rlv_PsvOpen_Err, &bErr_Rlv_PsvOpen, &bSus_Rlv_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_Rlv_Short, pBbsVlvErrInfo->BbsVlv_Rlv_Short_Err, &bErr_Rlv_Short, &bSus_Rlv_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_Rlv_CurReg, pBbsVlvErrInfo->BbsVlv_Rlv_CurReg_Err, &bErr_Rlv_CurReg, &bSus_Rlv_CurReg);

    ret = EEM_SetErrorInfo(EEM_Err_id_CircV_Open, pBbsVlvErrInfo->BbsVlv_CircVlv_Open_Err, &bErr_CircV_Open, &bSus_CircV_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_CircV_PsvOpen, pBbsVlvErrInfo->BbsVlv_CircVlv_PsvOpen_Err, &bErr_CircV_PsvOpen, &bSus_CircV_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_CircV_Short, pBbsVlvErrInfo->BbsVlv_CircVlv_Short_Err, &bErr_CircV_Short, &bSus_CircV_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_CircV_CurReg, pBbsVlvErrInfo->BbsVlv_CircVlv_CurReg_Err, &bErr_CircV_CurReg, &bSus_CircV_CurReg);



    /* ESC Valve Relay */
    ret = EEM_SetErrorInfo(EEM_Err_id_ESCValveRelay_Open, pAbsVlvMData->AbsVlvM_ValveRelay_Open_Err, &bErr_ESCValveRelay_Open, &bSus_ESCValveRelay_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_ESCValveRelay_S2G, pAbsVlvMData->AbsVlvM_ValveRelay_S2G_Err, &bErr_ESCValveRelay_S2G, &bSus_ESCValveRelay_S2G);
    ret = EEM_SetErrorInfo(EEM_Err_id_ESCValveRelay_S2B, pAbsVlvMData->AbsVlvM_ValveRelay_S2B_Err, &bErr_ESCValveRelay_S2B, &bSus_ESCValveRelay_S2B);
    ret = EEM_SetErrorInfo(EEM_Err_id_ESCValveRelay_ShutdownLine, pAbsVlvMData->AbsVlvM_ValveRelay_ShutdownLine_Err, &bErr_ESCValveRelay_ShutdownLine, &bSus_ESCValveRelay_ShutdownLine);
    
    /* ESC Valve */
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNO_Open, pAbsVlvMData->AbsVlvM_ValveFLNO_Open_Err, &bErr_FLNO_Open, &bSus_FLNO_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNO_PsvOpen, pAbsVlvMData->AbsVlvM_ValveFLNO_PsvOpen_Err, &bErr_FLNO_PsvOpen, &bSus_FLNO_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNO_Short, pAbsVlvMData->AbsVlvM_ValveFLNO_Short_Err, &bErr_FLNO_Short, &bSus_FLNO_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNO_OverTemp, pAbsVlvMData->AbsVlvM_ValveFLNO_OverTemp_Err, &bErr_FLNO_OverTemp, &bSus_FLNO_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNO_CurReg, pAbsVlvMData->AbsVlvM_ValveFLNO_CurReg_Err, &bErr_FLNO_CurReg, &bSus_FLNO_CurReg);

    ret = EEM_SetErrorInfo(EEM_Err_id_FRNO_Open, pAbsVlvMData->AbsVlvM_ValveFRNO_Open_Err, &bErr_FRNO_Open, &bSus_FRNO_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNO_PsvOpen, pAbsVlvMData->AbsVlvM_ValveFRNO_PsvOpen_Err, &bErr_FRNO_PsvOpen, &bSus_FRNO_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNO_Short, pAbsVlvMData->AbsVlvM_ValveFRNO_Short_Err, &bErr_FRNO_Short, &bSus_FRNO_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNO_OverTemp, pAbsVlvMData->AbsVlvM_ValveFRNO_OverTemp_Err, &bErr_FRNO_OverTemp, &bSus_FRNO_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNO_CurReg, pAbsVlvMData->AbsVlvM_ValveFRNO_CurReg_Err, &bErr_FRNO_CurReg, &bSus_FRNO_CurReg);    

    ret = EEM_SetErrorInfo(EEM_Err_id_BAL_Open, pAbsVlvMData->AbsVlvM_ValveBAL_Open_Err, &bErr_BAL_Open, &bSus_BAL_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_BAL_PsvOpen, pAbsVlvMData->AbsVlvM_ValveBAL_PsvOpen_Err, &bErr_BAL_PsvOpen, &bSus_BAL_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_BAL_Short, pAbsVlvMData->AbsVlvM_ValveBAL_Short_Err, &bErr_BAL_Short, &bSus_BAL_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_BAL_OverTemp, pAbsVlvMData->AbsVlvM_ValveBAL_OverTemp_Err, &bErr_BAL_OverTemp, &bSus_BAL_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_BAL_CurReg, pAbsVlvMData->AbsVlvM_ValveBAL_CurReg_Err, &bErr_BAL_CurReg, &bSus_BAL_CurReg);    

    ret = EEM_SetErrorInfo(EEM_Err_id_PD_Open, pAbsVlvMData->AbsVlvM_ValvePD_Open_Err, &bErr_PD_Open, &bSus_PD_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_PD_PsvOpen, pAbsVlvMData->AbsVlvM_ValvePD_PsvOpen_Err, &bErr_PD_PsvOpen, &bSus_PD_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_PD_Short, pAbsVlvMData->AbsVlvM_ValvePD_Short_Err, &bErr_PD_Short, &bSus_PD_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_PD_OverTemp, pAbsVlvMData->AbsVlvM_ValvePD_OverTemp_Err, &bErr_PD_OverTemp, &bSus_PD_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_PD_CurReg, pAbsVlvMData->AbsVlvM_ValvePD_CurReg_Err, &bErr_PD_CurReg, &bSus_PD_CurReg);    

    ret = EEM_SetErrorInfo(EEM_Err_id_RESP_Open, pAbsVlvMData->AbsVlvM_ValveRESP_Open_Err, &bErr_RESP_Open, &bSus_RESP_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_RESP_PsvOpen, pAbsVlvMData->AbsVlvM_ValveRESP_PsvOpen_Err, &bErr_RESP_PsvOpen, &bSus_RESP_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_RESP_Short, pAbsVlvMData->AbsVlvM_ValveRESP_Short_Err, &bErr_RESP_Short, &bSus_RESP_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_RESP_OverTemp, pAbsVlvMData->AbsVlvM_ValveRESP_OverTemp_Err, &bErr_RESP_OverTemp, &bSus_RESP_OverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_RESP_CurReg, pAbsVlvMData->AbsVlvM_ValveRESP_CurReg_Err, &bErr_RESP_CurReg, &bSus_RESP_CurReg); 

    ret = EEM_SetErrorInfo(EEM_Err_id_FLNC_Open, pAbsVlvMData->AbsVlvM_ValveFLNC_Open_Err, &bErr_FLNC_Open, &bSus_FLNC_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNC_PsvOpen, pAbsVlvMData->AbsVlvM_ValveFLNC_PsvOpen_Err, &bErr_FLNC_PsvOpen, &bSus_FLNC_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNC_Short, pAbsVlvMData->AbsVlvM_ValveFLNC_Short_Err, &bErr_FLNC_Short, &bSus_FLNC_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_FLNC_OverTemp, pAbsVlvMData->AbsVlvM_ValveFLNC_OverTemp_Err, &bErr_FLNC_OverTemp, &bSus_FLNC_OverTemp);

    ret = EEM_SetErrorInfo(EEM_Err_id_FRNC_Open, pAbsVlvMData->AbsVlvM_ValveFRNC_Open_Err, &bErr_FRNC_Open, &bSus_FRNC_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNC_PsvOpen, pAbsVlvMData->AbsVlvM_ValveFRNC_PsvOpen_Err, &bErr_FRNC_PsvOpen, &bSus_FRNC_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNC_Short, pAbsVlvMData->AbsVlvM_ValveFRNC_Short_Err, &bErr_FRNC_Short, &bSus_FRNC_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_FRNC_OverTemp, pAbsVlvMData->AbsVlvM_ValveFRNC_OverTemp_Err, &bErr_FRNC_OverTemp, &bSus_FRNC_OverTemp);

    ret = EEM_SetErrorInfo(EEM_Err_id_RLNC_Open, pAbsVlvMData->AbsVlvM_ValveRLNC_Open_Err, &bErr_RLNC_Open, &bSus_RLNC_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_RLNC_PsvOpen, pAbsVlvMData->AbsVlvM_ValveRLNC_PsvOpen_Err, &bErr_RLNC_PsvOpen, &bSus_RLNC_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_RLNC_Short, pAbsVlvMData->AbsVlvM_ValveRLNC_Short_Err, &bErr_RLNC_Short, &bSus_RLNC_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_RLNC_OverTemp, pAbsVlvMData->AbsVlvM_ValveRLNC_OverTemp_Err, &bErr_RLNC_OverTemp, &bSus_RLNC_OverTemp);

    ret = EEM_SetErrorInfo(EEM_Err_id_RRNC_Open, pAbsVlvMData->AbsVlvM_ValveRRNC_Open_Err, &bErr_RRNC_Open, &bSus_RRNC_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_RRNC_PsvOpen, pAbsVlvMData->AbsVlvM_ValveRRNC_PsvOpen_Err, &bErr_RRNC_PsvOpen, &bSus_RRNC_PsvOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_RRNC_Short, pAbsVlvMData->AbsVlvM_ValveRRNC_Short_Err, &bErr_RRNC_Short, &bSus_RRNC_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_RRNC_OverTemp, pAbsVlvMData->AbsVlvM_ValveRRNC_OverTemp_Err, &bErr_RRNC_OverTemp, &bSus_RRNC_OverTemp);


    /* Fail Flag */
    gEEM_FailInfo.bBBSValveRelay = FALSE;
    if((bErr_Batt1Fuse_Open == TRUE) || (bErr_BBSValveRelay_Open == TRUE) || (bErr_BBSValveRelay_S2G == TRUE) ||
       (bErr_BBSValveRelay_S2G == TRUE) || (bErr_BBSValveRelay_OverTemp == TRUE) || (bErr_BBSValveRelay_ShutdownLine == TRUE)
      )
    {
        gEEM_FailInfo.bBBSValveRelay = TRUE;
    }

    gEEM_FailInfo.bESCValveRelay = FALSE;
    if((bErr_Batt1Fuse_Open == TRUE) || (bErr_ESCValveRelay_Open == TRUE) || (bErr_ESCValveRelay_S2G == TRUE) || (bErr_ESCValveRelay_S2B == TRUE) ||
       (bErr_ESCValveRelay_ShutdownLine == TRUE)
      )
    {
        gEEM_FailInfo.bESCValveRelay = TRUE;
    }

    gEEM_FailInfo.bBBSSol = FALSE;
    if((bErr_SIM_Open == TRUE) || (bErr_SIM_PsvOpen == TRUE) || (bErr_SIM_Short == TRUE) || (bErr_SIM_CurReg == TRUE) ||
       (bErr_CutP_Open == TRUE) || (bErr_CutP_PsvOpen == TRUE) || (bErr_CutP_Short == TRUE) || (bErr_CutP_CurReg == TRUE) ||
       (bErr_CutS_Open == TRUE) || (bErr_CutS_PsvOpen == TRUE) || (bErr_CutS_Short == TRUE) || (bErr_CutS_CurReg == TRUE) ||
       (bErr_Rlv_Open == TRUE) || (bErr_Rlv_PsvOpen == TRUE) || (bErr_Rlv_Short == TRUE) || (bErr_Rlv_CurReg == TRUE) ||
       (bErr_CircV_Open == TRUE) || (bErr_CircV_PsvOpen == TRUE) || (bErr_CircV_Short == TRUE) || (bErr_CircV_CurReg == TRUE)
      )
    {
        gEEM_FailInfo.bBBSSol = TRUE;
    }

    gEEM_FailInfo.bFrontSol = FALSE;
    if((bErr_FLNC_Open == TRUE) || (bErr_FLNC_PsvOpen == TRUE) || (bErr_FLNC_Short == TRUE) || (bErr_FLNC_OverTemp == TRUE) ||
        (bErr_FRNC_Open == TRUE) || (bErr_FRNC_PsvOpen == TRUE) || (bErr_FRNC_Short == TRUE) || (bErr_FRNC_OverTemp == TRUE) 
      )
    {
        gEEM_FailInfo.bFrontSol = TRUE;
    }

    gEEM_FailInfo.bRearSol = FALSE;
    if((bErr_RLNC_Open == TRUE) || (bErr_RLNC_PsvOpen == TRUE) || (bErr_RLNC_Short == TRUE) || (bErr_RLNC_OverTemp == TRUE) ||
        (bErr_RRNC_Open == TRUE) || (bErr_RRNC_PsvOpen == TRUE) || (bErr_RRNC_Short == TRUE) || (bErr_RRNC_OverTemp == TRUE)
      )
    {
        gEEM_FailInfo.bRearSol = TRUE;
    }

    gEEM_FailInfo.bESCSol = FALSE;
    if((gEEM_FailInfo.bFrontSol == TRUE) || (gEEM_FailInfo.bRearSol == TRUE) ||
       (bErr_FLNO_Open == TRUE) || (bErr_FLNO_PsvOpen == TRUE) || (bErr_FLNO_Short == TRUE) || (bErr_FLNO_OverTemp == TRUE) || (bErr_FLNO_CurReg == TRUE) ||
       (bErr_FRNO_Open == TRUE) || (bErr_FRNO_PsvOpen == TRUE) || (bErr_FRNO_Short == TRUE) || (bErr_FRNO_OverTemp == TRUE) || (bErr_FRNO_CurReg == TRUE) ||
       (bErr_RLNO_Open == TRUE) || (bErr_RLNO_PsvOpen == TRUE) || (bErr_RLNO_Short == TRUE) || (bErr_RLNO_OverTemp == TRUE) || (bErr_RLNO_CurReg == TRUE) ||
       (bErr_RRNO_Open == TRUE) || (bErr_RRNO_PsvOpen == TRUE) || (bErr_RRNO_Short == TRUE) || (bErr_RRNO_OverTemp == TRUE) || (bErr_RRNO_CurReg == TRUE) ||
       (bErr_BAL_Open == TRUE) || (bErr_BAL_PsvOpen == TRUE) || (bErr_BAL_Short == TRUE) || (bErr_BAL_OverTemp == TRUE) || (bErr_BAL_CurReg == TRUE) ||
       (bErr_PD_Open == TRUE) || (bErr_PD_PsvOpen == TRUE) || (bErr_PD_Short == TRUE) || (bErr_PD_OverTemp == TRUE) || (bErr_PD_CurReg == TRUE) ||
       (bErr_RESP_Open == TRUE) || (bErr_RESP_PsvOpen == TRUE) || (bErr_RESP_Short == TRUE) || (bErr_RESP_OverTemp == TRUE) || (bErr_RESP_CurReg == TRUE)
      )
    {
        gEEM_FailInfo.bESCSol = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_BBSValveRelay = gEEM_FailInfo.bBBSValveRelay;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_ESCValveRelay = gEEM_FailInfo.bESCValveRelay;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_BBSSol = gEEM_FailInfo.bBBSSol;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_ESCSol = gEEM_FailInfo.bESCSol;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_FrontSol = gEEM_FailInfo.bFrontSol;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_RearSol = gEEM_FailInfo.bRearSol;


    /* Suspect Flag */
    gEEM_SuspectInfo.bBBSValveRelay = FALSE;
    if((bSus_Batt1Fuse_Open == TRUE) || (bSus_BBSValveRelay_Open == TRUE) || (bSus_BBSValveRelay_S2G == TRUE) ||
       (bSus_BBSValveRelay_S2G == TRUE) || (bSus_BBSValveRelay_OverTemp == TRUE) || (bSus_BBSValveRelay_ShutdownLine == TRUE)
      )
    {
        gEEM_SuspectInfo.bBBSValveRelay = TRUE;
    }

    gEEM_SuspectInfo.bESCValveRelay = FALSE;
    if((bSus_Batt1Fuse_Open == TRUE) || (bSus_ESCValveRelay_Open == TRUE) || (bSus_ESCValveRelay_S2G == TRUE) || (bSus_ESCValveRelay_S2B == TRUE) ||
       (bSus_ESCValveRelay_ShutdownLine == TRUE)
      )
    {
        gEEM_SuspectInfo.bESCValveRelay = TRUE;
    }

    gEEM_SuspectInfo.bBBSSol = FALSE;
    if((bSus_SIM_Open == TRUE) || (bSus_SIM_PsvOpen == TRUE) || (bSus_SIM_Short == TRUE) || (bSus_SIM_CurReg == TRUE) ||
       (bSus_CutP_Open == TRUE) || (bSus_CutP_PsvOpen == TRUE) || (bSus_CutP_Short == TRUE) || (bSus_CutP_CurReg == TRUE) ||
       (bSus_CutS_Open == TRUE) || (bSus_CutS_PsvOpen == TRUE) || (bSus_CutS_Short == TRUE) || (bSus_CutS_CurReg == TRUE) ||
       (bSus_Rlv_Open == TRUE) || (bSus_Rlv_PsvOpen == TRUE) || (bSus_Rlv_Short == TRUE) || (bSus_Rlv_CurReg == TRUE) ||
       (bSus_CircV_Open == TRUE) || (bSus_CircV_PsvOpen == TRUE) || (bSus_CircV_Short == TRUE) || (bSus_CircV_CurReg == TRUE)
      )
    {
        gEEM_SuspectInfo.bBBSSol = TRUE;
    }

    gEEM_SuspectInfo.bFrontSol = FALSE;
    if((bSus_FLNC_Open == TRUE) || (bSus_FLNC_PsvOpen == TRUE) || (bSus_FLNC_Short == TRUE) || (bSus_FLNC_OverTemp == TRUE) ||
        (bSus_FRNC_Open == TRUE) || (bSus_FRNC_PsvOpen == TRUE) || (bSus_FRNC_Short == TRUE) || (bSus_FRNC_OverTemp == TRUE) 
      )
    {
        gEEM_SuspectInfo.bFrontSol = TRUE;
    }

    gEEM_SuspectInfo.bRearSol = FALSE;
    if((bSus_RLNC_Open == TRUE) || (bSus_RLNC_PsvOpen == TRUE) || (bSus_RLNC_Short == TRUE) || (bSus_RLNC_OverTemp == TRUE) ||
        (bSus_RRNC_Open == TRUE) || (bSus_RRNC_PsvOpen == TRUE) || (bSus_RRNC_Short == TRUE) || (bSus_RRNC_OverTemp == TRUE)
      )
    {
        gEEM_SuspectInfo.bRearSol = TRUE;
    }

    gEEM_SuspectInfo.bESCSol = FALSE;
    if((gEEM_SuspectInfo.bFrontSol == TRUE) || (gEEM_SuspectInfo.bRearSol == TRUE) ||
       (bSus_FLNO_Open == TRUE) || (bSus_FLNO_PsvOpen == TRUE) || (bSus_FLNO_Short == TRUE) || (bSus_FLNO_OverTemp == TRUE) || (bSus_FLNO_CurReg == TRUE) ||
       (bSus_FRNO_Open == TRUE) || (bSus_FRNO_PsvOpen == TRUE) || (bSus_FRNO_Short == TRUE) || (bSus_FRNO_OverTemp == TRUE) || (bSus_FRNO_CurReg == TRUE) ||
       (bSus_RLNO_Open == TRUE) || (bSus_RLNO_PsvOpen == TRUE) || (bSus_RLNO_Short == TRUE) || (bSus_RLNO_OverTemp == TRUE) || (bSus_RLNO_CurReg == TRUE) ||
       (bSus_RRNO_Open == TRUE) || (bSus_RRNO_PsvOpen == TRUE) || (bSus_RRNO_Short == TRUE) || (bSus_RRNO_OverTemp == TRUE) || (bSus_RRNO_CurReg == TRUE) ||
       (bSus_BAL_Open == TRUE) || (bSus_BAL_PsvOpen == TRUE) || (bSus_BAL_Short == TRUE) || (bSus_BAL_OverTemp == TRUE) || (bSus_BAL_CurReg == TRUE) ||
       (bSus_PD_Open == TRUE) || (bSus_PD_PsvOpen == TRUE) || (bSus_PD_Short == TRUE) || (bSus_PD_OverTemp == TRUE) || (bSus_PD_CurReg == TRUE) ||
       (bSus_RESP_Open == TRUE) || (bSus_RESP_PsvOpen == TRUE) || (bSus_RESP_Short == TRUE) || (bSus_RESP_OverTemp == TRUE) || (bSus_RESP_CurReg == TRUE)
      )
    {
        gEEM_SuspectInfo.bESCSol = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay = gEEM_SuspectInfo.bBBSValveRelay;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay = gEEM_SuspectInfo.bESCValveRelay;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_BBSSol = gEEM_SuspectInfo.bBBSSol;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_ESCSol = gEEM_SuspectInfo.bESCSol;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_FrontSol = gEEM_SuspectInfo.bFrontSol;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_RearSol = gEEM_SuspectInfo.bRearSol;    
}

static void EEM_CheckMotorError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    MTRMOutInfo_t *pMTRMOutInfo = NULL;

    boolean bErr_Motor_Open = FALSE, bErr_Motor_FuseOpen = FALSE, bErr_Motor_Cal = FALSE;
    boolean bErr_Motor_U_OC = FALSE, bErr_Motor_V_OC = FALSE, bErr_Motor_W_OC = FALSE, bErr_Motor_Short = FALSE, bErr_Motor_Driver = FALSE;
    boolean bSus_Motor_Open = FALSE, bSus_Motor_FuseOpen = FALSE, bSus_Motor_Cal = FALSE; 
    boolean bSus_Motor_U_OC = FALSE, bSus_Motor_V_OC = FALSE, bSus_Motor_W_OC = FALSE, bSus_Motor_Short = FALSE, bSus_Motor_Driver = FALSE;

    pMTRMOutInfo = &(pEem_Data->Eem_MainMTRMOutInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Open, pMTRMOutInfo->MTRM_Open_Err, &bErr_Motor_Open, &bSus_Motor_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_FuseOpen, pMTRMOutInfo->MTRM_Power_Open_Err, &bErr_Motor_FuseOpen, &bSus_Motor_FuseOpen);
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Phase_U_OverCurrent, pMTRMOutInfo->MTRM_Phase_U_OverCurret_Err, &bErr_Motor_U_OC, &bSus_Motor_U_OC);    
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Phase_V_OverCurrent, pMTRMOutInfo->MTRM_Phase_V_OverCurret_Err, &bErr_Motor_V_OC, &bSus_Motor_V_OC);   
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Phase_W_OverCurrent, pMTRMOutInfo->MTRM_Phase_W_OverCurret_Err, &bErr_Motor_W_OC, &bSus_Motor_W_OC);       
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Short, pMTRMOutInfo->MTRM_Short_Err, &bErr_Motor_Short, &bSus_Motor_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Driver, pMTRMOutInfo->MTRM_Driver_Err, &bErr_Motor_Driver, &bSus_Motor_Driver);
    ret = EEM_SetErrorInfo(EEM_Err_id_Motor_Calibration, pMTRMOutInfo->MTRM_Calibration_Err, &bErr_Motor_Cal, &bSus_Motor_Cal);

    /* Valve BBS, ESC */
    /* EEM Table Editing */


    /* Fail Flag */
    gEEM_FailInfo.bMotor = FALSE;
    if((bErr_Motor_Open == TRUE) || (bErr_Motor_FuseOpen == TRUE) || (bErr_Motor_U_OC == TRUE) || (bErr_Motor_V_OC == TRUE) || 
        (bErr_Motor_W_OC == TRUE) || (bErr_Motor_Short == TRUE) || (bErr_Motor_Driver == TRUE) || (bErr_Motor_Cal == TRUE))
    {
        gEEM_FailInfo.bMotor = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_Motor = gEEM_FailInfo.bMotor;


    /* Suspect Flag */
    gEEM_SuspectInfo.bMotor = FALSE;
    if((bSus_Motor_Open == TRUE) || (bSus_Motor_FuseOpen == TRUE) || (bSus_Motor_U_OC == TRUE) || (bSus_Motor_V_OC == TRUE) ||
        (bSus_Motor_W_OC == TRUE) || (bSus_Motor_Short == TRUE) || (bSus_Motor_Driver == TRUE) || (bSus_Motor_Cal == TRUE))
    {
        gEEM_SuspectInfo.bMotor = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_Motor = gEEM_SuspectInfo.bMotor;    
}

static void EEM_CheckMPSError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    MpsD1ErrInfo_t *pMpsD1ErrInfo = NULL;
    MpsD2ErrInfo_t *pMpsD2ErrInfo = NULL;
    boolean bErr_MPS_D1_ExtPower = FALSE, bErr_MPS_D1_IntPower = FALSE, bErr_MPS_D1_Watchdog = FALSE;
    boolean bErr_MPS_D1_NVM = FALSE, bErr_MPS_D1_BIST = FALSE, bErr_MPS_D1_ConfigReg = FALSE;
    boolean bErr_MPS_D1_HWIntegConsis = FALSE, bErr_MPS_D1_Magnet_Loss = FALSE;
    boolean bErr_MPS_D2_ExtPower = FALSE, bErr_MPS_D2_IntPower = FALSE, bErr_MPS_D2_Watchdog = FALSE;
    boolean bErr_MPS_D2_NVM = FALSE, bErr_MPS_D2_BIST = FALSE, bErr_MPS_D2_ConfigReg = FALSE;
    boolean bErr_MPS_D2_HWIntegConsis = FALSE, bErr_MPS_D2_Magnet_Loss = FALSE;

    boolean bSus_MPS_D1_ExtPower = FALSE, bSus_MPS_D1_IntPower = FALSE, bSus_MPS_D1_Watchdog = FALSE;
    boolean bSus_MPS_D1_NVM = FALSE, bSus_MPS_D1_BIST = FALSE, bSus_MPS_D1_ConfigReg = FALSE;
    boolean bSus_MPS_D1_HWIntegConsis = FALSE, bSus_MPS_D1_Magnet_Loss = FALSE;
    boolean bSus_MPS_D2_ExtPower = FALSE, bSus_MPS_D2_IntPower = FALSE, bSus_MPS_D2_Watchdog = FALSE;
    boolean bSus_MPS_D2_NVM = FALSE, bSus_MPS_D2_BIST = FALSE, bSus_MPS_D2_ConfigReg = FALSE;
    boolean bSus_MPS_D2_HWIntegConsis = FALSE, bSus_MPS_D2_Magnet_Loss = FALSE;

    pMpsD1ErrInfo = &(pEem_Data->Eem_MainMpsD1ErrInfo);
    pMpsD2ErrInfo = &(pEem_Data->Eem_MainMpsD2ErrInfo);

    /* D1 */
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_ExtPower, pMpsD1ErrInfo->ExtPWRSuppMonErr, &bErr_MPS_D1_ExtPower, &bSus_MPS_D1_ExtPower);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_IntPower, pMpsD1ErrInfo->IntPWRSuppMonErr, &bErr_MPS_D1_IntPower, &bSus_MPS_D1_IntPower);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_Watchdog, pMpsD1ErrInfo->WatchDogErr, &bErr_MPS_D1_Watchdog, &bSus_MPS_D1_Watchdog);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_NVM, pMpsD1ErrInfo->FlashMemoryErr, &bErr_MPS_D1_NVM, &bSus_MPS_D1_NVM);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_BIST, pMpsD1ErrInfo->StartUpTestErr, &bErr_MPS_D1_BIST, &bSus_MPS_D1_BIST);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_ConfigReg, pMpsD1ErrInfo->ConfigRegiTestErr, &bErr_MPS_D1_ConfigReg, &bSus_MPS_D1_ConfigReg);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_HWIntegConsis, pMpsD1ErrInfo->HWIntegConsisErr, &bErr_MPS_D1_HWIntegConsis, &bSus_MPS_D1_HWIntegConsis);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D1_Magnet_Loss, pMpsD1ErrInfo->MagnetLossErr, &bErr_MPS_D1_Magnet_Loss, &bSus_MPS_D1_Magnet_Loss);

    /* D2 */
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_ExtPower, pMpsD2ErrInfo->ExtPWRSuppMonErr, &bErr_MPS_D2_ExtPower, &bSus_MPS_D2_ExtPower);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_IntPower, pMpsD2ErrInfo->IntPWRSuppMonErr, &bErr_MPS_D2_IntPower, &bSus_MPS_D2_IntPower);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_Watchdog, pMpsD2ErrInfo->WatchDogErr, &bErr_MPS_D2_Watchdog, &bSus_MPS_D2_Watchdog);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_NVM, pMpsD2ErrInfo->FlashMemoryErr, &bErr_MPS_D2_NVM, &bSus_MPS_D2_NVM);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_BIST, pMpsD2ErrInfo->StartUpTestErr, &bErr_MPS_D2_BIST, &bSus_MPS_D2_BIST);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_ConfigReg, pMpsD2ErrInfo->ConfigRegiTestErr, &bErr_MPS_D2_ConfigReg, &bSus_MPS_D2_ConfigReg);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_HWIntegConsis, pMpsD2ErrInfo->HWIntegConsisErr, &bErr_MPS_D2_HWIntegConsis, &bSus_MPS_D2_HWIntegConsis);
    ret = EEM_SetErrorInfo(EEM_Err_id_MPS_D2_Magnet_Loss, pMpsD2ErrInfo->MagnetLossErr, &bErr_MPS_D2_Magnet_Loss, &bSus_MPS_D2_Magnet_Loss);


    /* Fail Flag */
    gEEM_FailInfo.bMPS = FALSE;
    if((bErr_MPS_D1_ExtPower == TRUE) || (bErr_MPS_D1_IntPower == TRUE) || (bErr_MPS_D1_Watchdog == TRUE) || (bErr_MPS_D1_NVM == TRUE) ||
       (bErr_MPS_D1_BIST == TRUE) || (bErr_MPS_D1_ConfigReg == TRUE) || (bErr_MPS_D1_HWIntegConsis == TRUE) || (bErr_MPS_D1_Magnet_Loss == TRUE) ||
       (bErr_MPS_D2_ExtPower == TRUE) || (bErr_MPS_D2_IntPower == TRUE) || (bErr_MPS_D2_Watchdog == TRUE) || (bErr_MPS_D2_NVM == TRUE) ||
       (bErr_MPS_D2_BIST == TRUE) || (bErr_MPS_D2_ConfigReg == TRUE) || (bErr_MPS_D2_HWIntegConsis == TRUE) || (bErr_MPS_D2_Magnet_Loss == TRUE)
      )
    {
        gEEM_FailInfo.bMPS = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_MPS = gEEM_FailInfo.bMPS;


    /* Suspect Flag */
    gEEM_SuspectInfo.bMPS = FALSE;
    if((bSus_MPS_D1_ExtPower == TRUE) || (bSus_MPS_D1_IntPower == TRUE) || (bSus_MPS_D1_Watchdog == TRUE) || (bSus_MPS_D1_NVM == TRUE) ||
       (bSus_MPS_D1_BIST == TRUE) || (bSus_MPS_D1_ConfigReg == TRUE) || (bSus_MPS_D1_HWIntegConsis == TRUE) || (bSus_MPS_D1_Magnet_Loss == TRUE) ||
       (bSus_MPS_D2_ExtPower == TRUE) || (bSus_MPS_D2_IntPower == TRUE) || (bSus_MPS_D2_Watchdog == TRUE) || (bSus_MPS_D2_NVM == TRUE) ||
       (bSus_MPS_D2_BIST == TRUE) || (bSus_MPS_D2_ConfigReg == TRUE) || (bSus_MPS_D2_HWIntegConsis == TRUE) || (bSus_MPS_D2_Magnet_Loss == TRUE)
      )
    {
        gEEM_SuspectInfo.bMPS = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_MPS = gEEM_SuspectInfo.bMPS;    
}

static void EEM_CheckMGDError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    MgdErrInfo_t *pMgdErrInfo = NULL;

    boolean bErr_MGD_SupplyPower_Open = FALSE, bErr_MGD_Clock = FALSE, bErr_MGD_BIST = FALSE, bErr_MGD_NVM = FALSE;
    boolean bErr_MGD_RAM = FALSE, bErr_MGD_ConfigReg = FALSE, bErr_MGD_InputPattern = FALSE, bErr_MGD_Temperature = FALSE;
    boolean bErr_MGD_CP = FALSE, bErr_MGD_HBuffCap = FALSE, bErr_MGD_InOutPlau = FALSE, bErr_MGD_CtrlSig = FALSE;

    boolean bSus_MGD_SupplyPower_Open = FALSE, bSus_MGD_Clock = FALSE, bSus_MGD_BIST = FALSE, bSus_MGD_NVM = FALSE;
    boolean bSus_MGD_RAM = FALSE, bSus_MGD_ConfigReg = FALSE, bSus_MGD_InputPattern = FALSE, bSus_MGD_Temperature = FALSE;
    boolean bSus_MGD_CP = FALSE, bSus_MGD_HBuffCap = FALSE, bSus_MGD_InOutPlau = FALSE, bSus_MGD_CtrlSig = FALSE;

    pMgdErrInfo = &(pEem_Data->Eem_MainMgdErrInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_SupplyPower_Open, pMgdErrInfo->MgdInterPwrSuppMonErr, &bErr_MGD_SupplyPower_Open, &bSus_MGD_SupplyPower_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_Clock, pMgdErrInfo->MgdClkMonErr, &bErr_MGD_Clock, &bSus_MGD_Clock);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_BIST, pMgdErrInfo->MgdBISTErr, &bErr_MGD_BIST, &bSus_MGD_BIST);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_NVM, pMgdErrInfo->MgdFlashMemoryErr, &bErr_MGD_NVM, &bSus_MGD_NVM);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_RAM, pMgdErrInfo->MgdRAMErr, &bErr_MGD_RAM, &bSus_MGD_RAM);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_ConfigReg, pMgdErrInfo->MgdConfigRegiErr, &bErr_MGD_ConfigReg, &bSus_MGD_ConfigReg);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_InputPattern, pMgdErrInfo->MgdInputPattMonErr, &bErr_MGD_InputPattern, &bSus_MGD_InputPattern);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_Temperature, pMgdErrInfo->MgdOverTempErr, &bErr_MGD_Temperature, &bSus_MGD_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_CP, pMgdErrInfo->MgdCPmpVMonErr, &bErr_MGD_CP, &bSus_MGD_CP);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_HBuffCap, pMgdErrInfo->MgdHBuffCapVErr, &bErr_MGD_HBuffCap, &bSus_MGD_HBuffCap);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_InOutPlau, pMgdErrInfo->MgdInOutPlauErr, &bErr_MGD_InOutPlau, &bSus_MGD_InOutPlau);
    ret = EEM_SetErrorInfo(EEM_Err_id_MGD_CtrlSig, pMgdErrInfo->MgdCtrlSigMonErr, &bErr_MGD_CtrlSig, &bSus_MGD_CtrlSig);


    /* Fail Flag */
    gEEM_FailInfo.bMGD = FALSE;
    if((bErr_MGD_SupplyPower_Open == TRUE) || (bErr_MGD_Clock == TRUE) || (bErr_MGD_BIST == TRUE) || (bErr_MGD_NVM == TRUE) ||
       (bErr_MGD_RAM == TRUE) || (bErr_MGD_ConfigReg == TRUE) || (bErr_MGD_InputPattern == TRUE) || (bErr_MGD_Temperature == TRUE) ||
       (bErr_MGD_CP == TRUE) || (bErr_MGD_HBuffCap == TRUE) || (bErr_MGD_InOutPlau == TRUE) || (bErr_MGD_CtrlSig == TRUE)
      )
    {
        gEEM_FailInfo.bMGD = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_MGD = gEEM_FailInfo.bMGD;


    /* Suspect Flag */
    gEEM_SuspectInfo.bMGD = FALSE;
    if((bSus_MGD_SupplyPower_Open == TRUE) || (bSus_MGD_Clock == TRUE) || (bSus_MGD_BIST == TRUE) || (bSus_MGD_NVM == TRUE) ||
       (bSus_MGD_RAM == TRUE) || (bSus_MGD_ConfigReg == TRUE) || (bSus_MGD_InputPattern == TRUE) || (bSus_MGD_Temperature == TRUE) ||
       (bSus_MGD_CP == TRUE) || (bSus_MGD_HBuffCap == TRUE) || (bSus_MGD_InOutPlau == TRUE) || (bSus_MGD_CtrlSig == TRUE)
      )
    {
        gEEM_SuspectInfo.bMGD = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_MGD = gEEM_SuspectInfo.bMGD;    
}

static void EEM_CheckECUHwError(Eem_Main_HdrBusType *pEem_Data)
{
    /* TODO */
}

static void EEM_CheckSysPwrError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    SysPwrM_MainSysPwrMonData_t *pSysPwrMonData = NULL;

    boolean bErr_SysOverVolt_1st = FALSE, bErr_SysOverVolt_2st = FALSE;
    boolean bErr_SysUnderVolt_1st = FALSE, bErr_SysUnderVolt_2st = FALSE, bErr_SysUnderVolt_3st = FALSE;
    boolean bErr_IgnLine_Open = FALSE;

    boolean bSus_SysOverVolt_1st = FALSE, bSus_SysOverVolt_2st = FALSE;
    boolean bSus_SysUnderVolt_1st = FALSE, bSus_SysUnderVolt_2st = FALSE, bSus_SysUnderVolt_3st = FALSE;
    boolean bSus_IgnLine_Open = FALSE;    

    pSysPwrMonData = &(pEem_Data->Eem_MainSysPwrMonData);

    ret = EEM_SetErrorInfo(EEM_Err_id_SysOverVolt_1st, pSysPwrMonData->SysPwrM_OverVolt_1st_Err, &bErr_SysOverVolt_1st, &bSus_SysOverVolt_1st);
    ret = EEM_SetErrorInfo(EEM_Err_id_SysOverVolt_2nd, pSysPwrMonData->SysPwrM_OverVolt_2st_Err, &bErr_SysOverVolt_2st, &bSus_SysOverVolt_2st);
    ret = EEM_SetErrorInfo(EEM_Err_id_SysUnderVolt_1st, pSysPwrMonData->SysPwrM_UnderVolt_1st_Err, &bErr_SysUnderVolt_1st, &bSus_SysUnderVolt_1st);
    ret = EEM_SetErrorInfo(EEM_Err_id_SysUnderVolt_2nd, pSysPwrMonData->SysPwrM_UnderVolt_2st_Err, &bErr_SysUnderVolt_2st, &bSus_SysUnderVolt_2st);
    ret = EEM_SetErrorInfo(EEM_Err_id_SysUnderVolt_3rd, pSysPwrMonData->SysPwrM_UnderVolt_3st_Err, &bErr_SysUnderVolt_3st, &bSus_SysUnderVolt_3st);
    ret = EEM_SetErrorInfo(EEM_Err_id_IgnOpen, pSysPwrMonData->SysPwrM_IgnLine_Open_Err, &bErr_IgnLine_Open, &bSus_IgnLine_Open);


    /* Fail Flag */
    gEEM_FailInfo.bOverVolt = FALSE;
    if((bErr_SysOverVolt_1st == TRUE) || (bErr_SysOverVolt_2st == TRUE))
    {
        gEEM_FailInfo.bOverVolt = TRUE;
    }

    gEEM_FailInfo.bHigherVolt = FALSE;
    if(bErr_SysOverVolt_2st == TRUE)
    {
        gEEM_FailInfo.bHigherVolt = TRUE;
    }

    gEEM_FailInfo.bUnderVolt = FALSE;
    if(bErr_SysUnderVolt_1st == TRUE)
    {
        gEEM_FailInfo.bUnderVolt = TRUE;
    }

    gEEM_FailInfo.bLowVolt = FALSE;
    if(bErr_SysUnderVolt_2st == TRUE)
    {
        gEEM_FailInfo.bLowVolt = TRUE;
    }

    gEEM_FailInfo.bLowerVolt = FALSE;
    if(bErr_SysUnderVolt_3st == TRUE)
    {
        gEEM_FailInfo.bLowerVolt = TRUE;
    }   

    pEem_Data->Eem_MainEemFailData.Eem_Fail_OverVolt = gEEM_FailInfo.bOverVolt;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_UnderVolt = gEEM_FailInfo.bUnderVolt;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_LowVolt = gEEM_FailInfo.bLowVolt;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_LowerVolt = gEEM_FailInfo.bLowerVolt;


    /* Suspect Flag */
    gEEM_SuspectInfo.bOverVolt = FALSE;
    if((bSus_SysOverVolt_1st == TRUE) || (bSus_SysOverVolt_2st == TRUE))
    {
        gEEM_SuspectInfo.bOverVolt = TRUE;
    }

    gEEM_SuspectInfo.bHigherVolt = FALSE;
    if(bSus_SysOverVolt_2st == TRUE)
    {
        gEEM_SuspectInfo.bHigherVolt = TRUE;
    }

    gEEM_SuspectInfo.bUnderVolt = FALSE;
    if(bSus_SysUnderVolt_1st == TRUE)
    {
        gEEM_SuspectInfo.bUnderVolt = TRUE;
    }

    gEEM_SuspectInfo.bLowVolt = FALSE;
    if(bSus_SysUnderVolt_2st == TRUE)
    {
        gEEM_SuspectInfo.bLowVolt = TRUE;
    }

    gEEM_SuspectInfo.bLowerVolt = FALSE;
    if(bSus_SysUnderVolt_3st == TRUE)
    {
        gEEM_SuspectInfo.bLowerVolt = TRUE;
    }   

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_OverVolt = gEEM_SuspectInfo.bOverVolt;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_UnderVolt = gEEM_SuspectInfo.bUnderVolt;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_LowVolt = gEEM_SuspectInfo.bLowVolt;
    /* pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_LowerVolt = gEEM_SuspectInfo.bLowerVolt; */
}

static void EEM_CheckSenPwrError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    SenPwrM_MainSenPwrMonitor_t *pSenPwrMonData = NULL;

    boolean bErr_5V = FALSE, bErr_12V_Open = FALSE, bErr_12V_Short = FALSE;

    boolean bSus_5V = FALSE, bSus_12V_Open = FALSE, bSus_12V_Short = FALSE;    

    pSenPwrMonData = &(pEem_Data->Eem_MainSenPwrMonitorData);

    ret = EEM_SetErrorInfo(EEM_Err_id_5V, pSenPwrMonData->SenPwrM_5V_Err, &bErr_5V, &bSus_5V);
    ret = EEM_SetErrorInfo(EEM_Err_id_12V_Open, pSenPwrMonData->SenPwrM_12V_Open_Err, &bErr_12V_Open, &bSus_12V_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_12V_Short, pSenPwrMonData->SenPwrM_12V_Short_Err, &bErr_12V_Short, &bSus_12V_Short);


    /* Fail Flag */
    gEEM_FailInfo.bSenPwr_5V = FALSE;
    if(bErr_5V == TRUE)
    {
        gEEM_FailInfo.bSenPwr_5V = TRUE;
    }

    gEEM_FailInfo.bSenPwr_12V = FALSE;
    if((bErr_12V_Open == TRUE) || (bErr_12V_Short == TRUE))
    {
        gEEM_FailInfo.bSenPwr_12V = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_SenPwr_5V = gEEM_FailInfo.bSenPwr_5V;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_SenPwr_12V = gEEM_FailInfo.bSenPwr_12V;


    /* Suspect Flag */
    gEEM_SuspectInfo.bSenPwr_5V = FALSE;
    if(bSus_5V == TRUE)
    {
        gEEM_SuspectInfo.bSenPwr_5V = TRUE;
    }

    gEEM_SuspectInfo.bSenPwr_12V = FALSE;
    if((bSus_12V_Open == TRUE) || (bSus_12V_Short == TRUE))
    {
        gEEM_SuspectInfo.bSenPwr_12V = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V = gEEM_SuspectInfo.bSenPwr_5V;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V = gEEM_SuspectInfo.bSenPwr_12V;    
}

static void EEM_CheckAsicError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    AsicMonFaultInfo_t *pAsicMonFaultInfo = NULL;

    boolean bErr_AsicOverVolt = FALSE, bErr_AsicUnderVolt = FALSE, bErr_AsicOverTemp = FALSE, bErr_AsicGndLoss = FALSE;
    boolean bErr_AsicComm = FALSE, bErr_AsicNvm = FALSE, bErr_AsicOsc = FALSE, bErr_AsicCP = FALSE;

    boolean bSus_AsicOverVolt = FALSE, bSus_AsicUnderVolt = FALSE, bSus_AsicOverTemp = FALSE, bSus_AsicGndLoss = FALSE;
    boolean bSus_AsicComm = FALSE, bSus_AsicNvm = FALSE, bSus_AsicOsc = FALSE, bSus_AsicCP = FALSE;

    pAsicMonFaultInfo = &(pEem_Data->Eem_MainAsicMonFaultInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_AsicOverVolt, pAsicMonFaultInfo->ASICM_AsicOverVolt_Err, &bErr_AsicOverVolt, &bSus_AsicOverVolt);
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicUnderVolt, pAsicMonFaultInfo->ASICM_AsicUnderVolt_Err, &bErr_AsicUnderVolt, &bSus_AsicUnderVolt);
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicOverTemp, pAsicMonFaultInfo->ASICM_AsicOverTemp_Err, &bErr_AsicOverTemp, &bSus_AsicOverTemp);
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicGndLoss, pAsicMonFaultInfo->ASICM_AsicGroundLoss_Err, &bErr_AsicGndLoss, &bSus_AsicGndLoss);
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicComm, pAsicMonFaultInfo->ASICM_AsicComm_Err, &bErr_AsicComm, &bSus_AsicComm);
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicNvm, pAsicMonFaultInfo->ASICM_AsicNvm_Err, &bErr_AsicNvm, &bSus_AsicNvm);
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicOsc, pAsicMonFaultInfo->ASICM_AsicOsc_Err, &bErr_AsicOsc, &bSus_AsicOsc);
    /* TODO
    ret = EEM_SetErrorInfo(EEM_Err_id_AsicCP, pAsicMonFaultInfo->ASICM_AsicCP_Err, pAsicMonFaultInfo->ASICM_AsicCP_Ihb, &bErr_AsicCP); 
    */


    /* Fail Flag */
    gEEM_FailInfo.bAsic = FALSE;
    if((bErr_AsicOverVolt == TRUE) || (bErr_AsicUnderVolt == TRUE) || (bErr_AsicOverTemp == TRUE) || (bErr_AsicGndLoss == TRUE) ||
       (bErr_AsicComm == TRUE) || (bErr_AsicNvm == TRUE) || (bErr_AsicOsc == TRUE) || (bErr_AsicCP == TRUE)
      )
    {
        gEEM_FailInfo.bAsic = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_ASIC = gEEM_FailInfo.bAsic;


    /* Suspect Flag */
    gEEM_SuspectInfo.bAsic = FALSE;
    if((bSus_AsicOverVolt == TRUE) || (bSus_AsicUnderVolt == TRUE) || (bSus_AsicOverTemp == TRUE) || (bSus_AsicGndLoss == TRUE) ||
       (bSus_AsicComm == TRUE) || (bSus_AsicNvm == TRUE) || (bSus_AsicOsc == TRUE) || (bSus_AsicCP == TRUE)
      )
    {
        gEEM_SuspectInfo.bAsic = TRUE;
    }

    /* pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_ASIC = gEEM_SuspectInfo.bAsic; TODO */
}

static void EEM_CheckYawError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    YAWMOutInfo_t *pYawMOutInfo = NULL;

    boolean bErr_Yaw_Timeout = FALSE, bErr_Yaw_Invalid = FALSE, bErr_Yaw_CheckSum = FALSE;
    boolean bErr_Yaw_RollingCnt = FALSE, bErr_Yaw_Temperature = FALSE, bErr_Yaw_Range = FALSE;

    boolean bSus_Yaw_Timeout = FALSE, bSus_Yaw_Invalid = FALSE, bSus_Yaw_CheckSum = FALSE;
    boolean bSus_Yaw_RollingCnt = FALSE, bSus_Yaw_Temperature = FALSE, bSus_Yaw_Range = FALSE;    

    pYawMOutInfo = &(pEem_Data->Eem_MainYAWMOutInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_Yaw_Timeout, pYawMOutInfo->YAWM_Timeout_Err, &bErr_Yaw_Timeout, &bSus_Yaw_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_Yaw_Invalid, pYawMOutInfo->YAWM_Invalid_Err, &bErr_Yaw_Invalid, &bSus_Yaw_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_Yaw_CheckSum, pYawMOutInfo->YAWM_CRC_Err, &bErr_Yaw_CheckSum, &bSus_Yaw_CheckSum);
    ret = EEM_SetErrorInfo(EEM_Err_id_Yaw_RollingCnt, pYawMOutInfo->YAWM_Rolling_Err, &bErr_Yaw_RollingCnt, &bSus_Yaw_RollingCnt);
    ret = EEM_SetErrorInfo(EEM_Err_id_Yaw_Temperature, pYawMOutInfo->YAWM_Temperature_Err, &bErr_Yaw_Temperature, &bSus_Yaw_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_Yaw_Range, pYawMOutInfo->YAWM_Range_Err, &bErr_Yaw_Range, &bSus_Yaw_Range);

    /* TODO : Plausibility Check */


    /* Fail Flag */
    gEEM_FailInfo.bYaw = FALSE;
    if((bErr_Yaw_Timeout == TRUE) || (bErr_Yaw_Invalid == TRUE) || (bErr_Yaw_CheckSum == TRUE) ||
       (bErr_Yaw_RollingCnt == TRUE) || (bErr_Yaw_Temperature == TRUE) || (bErr_Yaw_Range == TRUE)
      )
    {
        gEEM_FailInfo.bYaw = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_Yaw = gEEM_FailInfo.bYaw;


    /* Suspect Flag */
    gEEM_SuspectInfo.bYaw = FALSE;
    if((bSus_Yaw_Timeout == TRUE) || (bSus_Yaw_Invalid == TRUE) || (bSus_Yaw_CheckSum == TRUE) ||
       (bSus_Yaw_RollingCnt == TRUE) || (bSus_Yaw_Temperature == TRUE) || (bSus_Yaw_Range == TRUE)
      )
    {
        gEEM_SuspectInfo.bYaw = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_Yaw = gEEM_SuspectInfo.bYaw;    
}

static void EEM_CheckAyError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    AYMOutInfo_t *pAyMOutInfo = NULL;

    boolean bErr_Ay_Timeout = FALSE, bErr_Ay_Invalid = FALSE, bErr_Ay_CheckSum = FALSE;
    boolean bErr_Ay_RollingCnt = FALSE, bErr_Ay_Temperature = FALSE, bErr_Ay_Range = FALSE;

    boolean bSus_Ay_Timeout = FALSE, bSus_Ay_Invalid = FALSE, bSus_Ay_CheckSum = FALSE;
    boolean bSus_Ay_RollingCnt = FALSE, bSus_Ay_Temperature = FALSE, bSus_Ay_Range = FALSE;

    pAyMOutInfo = &(pEem_Data->Eem_MainAYMOuitInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_Ay_Timeout, pAyMOutInfo->AYM_Timeout_Err, &bErr_Ay_Timeout, &bSus_Ay_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ay_Invalid, pAyMOutInfo->AYM_Invalid_Err, &bErr_Ay_Invalid, &bSus_Ay_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ay_CheckSum, pAyMOutInfo->AYM_CRC_Err, &bErr_Ay_CheckSum, &bSus_Ay_CheckSum);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ay_RollingCnt, pAyMOutInfo->AYM_Rolling_Err, &bErr_Ay_RollingCnt, &bSus_Ay_RollingCnt);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ay_Temperature, pAyMOutInfo->AYM_Temperature_Err, &bErr_Ay_Temperature, &bSus_Ay_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ay_Range, pAyMOutInfo->AYM_Range_Err, &bErr_Ay_Range, &bSus_Ay_Range);

    /* TODO : Plausibility Check */


    /* Fail Flag */
    gEEM_FailInfo.bAy = FALSE;
    if((bErr_Ay_Timeout == TRUE) || (bErr_Ay_Invalid == TRUE) || (bErr_Ay_CheckSum == TRUE) ||
       (bErr_Ay_RollingCnt == TRUE) || (bErr_Ay_Temperature == TRUE) || (bErr_Ay_Range == TRUE)
      )
    {
        gEEM_FailInfo.bAy = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_Ay = gEEM_FailInfo.bAy;


    /* Suspect Flag */
    gEEM_SuspectInfo.bAy = FALSE;
    if((bSus_Ay_Timeout == TRUE) || (bSus_Ay_Invalid == TRUE) || (bSus_Ay_CheckSum == TRUE) ||
       (bSus_Ay_RollingCnt == TRUE) || (bSus_Ay_Temperature == TRUE) || (bSus_Ay_Range == TRUE)
      )
    {
        gEEM_SuspectInfo.bAy = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_Ay = gEEM_SuspectInfo.bAy;    
}

static void EEM_CheckAxError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    AXMOutInfo_t *pAxMOutInfo = NULL;

    boolean bErr_Ax_Timeout = FALSE, bErr_Ax_Invalid = FALSE, bErr_Ax_CheckSum = FALSE;
    boolean bErr_Ax_RollingCnt = FALSE, bErr_Ax_Temperature = FALSE, bErr_Ax_Range = FALSE;

    boolean bSus_Ax_Timeout = FALSE, bSus_Ax_Invalid = FALSE, bSus_Ax_CheckSum = FALSE;
    boolean bSus_Ax_RollingCnt = FALSE, bSus_Ax_Temperature = FALSE, bSus_Ax_Range = FALSE;

    pAxMOutInfo = &(pEem_Data->Eem_MainAXMOutInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_Ax_Timeout, pAxMOutInfo->AXM_Timeout_Err, &bErr_Ax_Timeout, &bSus_Ax_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ax_Invalid, pAxMOutInfo->AXM_Invalid_Err, &bErr_Ax_Invalid, &bSus_Ax_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ax_CheckSum, pAxMOutInfo->AXM_CRC_Err, &bErr_Ax_CheckSum, &bSus_Ax_CheckSum);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ax_RollingCnt, pAxMOutInfo->AXM_Rolling_Err, &bErr_Ax_RollingCnt, &bSus_Ax_RollingCnt);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ax_Temperature, pAxMOutInfo->AXM_Temperature_Err, &bErr_Ax_Temperature, &bSus_Ax_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_Ax_Range, pAxMOutInfo->AXM_Range_Err, &bErr_Ax_Range, &bSus_Ax_Range);

    /* TODO : Plausibility Check */


    /* Fail Flag */
    gEEM_FailInfo.bAx = FALSE;
    if((bErr_Ax_Timeout == TRUE) || (bErr_Ax_Invalid == TRUE) || (bErr_Ax_CheckSum == TRUE) ||
       (bErr_Ax_RollingCnt == TRUE) || (bErr_Ax_Temperature == TRUE) || (bErr_Ax_Range == TRUE)
      )
    {
        gEEM_FailInfo.bAx = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_Ax = gEEM_FailInfo.bAx;


    /* Suspect Flag */
    gEEM_SuspectInfo.bAx = FALSE;
    if((bSus_Ax_Timeout == TRUE) || (bSus_Ax_Invalid == TRUE) || (bSus_Ax_CheckSum == TRUE) ||
       (bSus_Ax_RollingCnt == TRUE) || (bSus_Ax_Temperature == TRUE) || (bSus_Ax_Range == TRUE)
      )
    {
        gEEM_SuspectInfo.bAx = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_Ax = gEEM_SuspectInfo.bAx;    
}

static void EEM_CheckSASError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    SASMOutInfo_t *pSASMOutInfo = NULL;
    SasPlauOutput_t *pSASPlauOutput = NULL;

    boolean bErr_SAS_Timeout = FALSE, bErr_SAS_Invalid = FALSE, bErr_SAS_CheckSum = FALSE;
    boolean bErr_SAS_RollingCnt = FALSE, bErr_SAS_Range = FALSE;
    boolean bErr_SAS_ShortTerm_Offset = FALSE, bErr_SAS_LongTerm_Offset = FALSE;
    boolean bErr_SAS_ModelMay = FALSE, bErr_SAS_Stick = FALSE;

    boolean bSus_SAS_Timeout = FALSE, bSus_SAS_Invalid = FALSE, bSus_SAS_CheckSum = FALSE;
    boolean bSus_SAS_RollingCnt = FALSE, bSus_SAS_Range = FALSE;
    boolean bSus_SAS_ShortTerm_Offset = FALSE, bSus_SAS_LongTerm_Offset = FALSE;
    boolean bSus_SAS_ModelMay = FALSE, bSus_SAS_Stick = FALSE;    

    pSASMOutInfo = &(pEem_Data->Eem_MainSASMOutInfo);
    pSASPlauOutput = &(pEem_Data->Eem_MainSasPlauOutput);

    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_Timeout, pSASMOutInfo->SASM_Timeout_Err, &bErr_SAS_Timeout, &bSus_SAS_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_Invalid, pSASMOutInfo->SASM_Invalid_Err, &bErr_SAS_Invalid, &bSus_SAS_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_CheckSum, pSASMOutInfo->SASM_CRC_Err, &bErr_SAS_CheckSum, &bSus_SAS_CheckSum);
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_RollingCnt, pSASMOutInfo->SASM_Rolling_Err, &bErr_SAS_RollingCnt, &bSus_SAS_RollingCnt);
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_Range, pSASMOutInfo->SASM_Range_Err, &bErr_SAS_Range, &bSus_SAS_Range);

    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_ShortTerm_Offset, pSASPlauOutput->SasPlauOffsetErr, &bErr_SAS_ShortTerm_Offset, &bSus_SAS_ShortTerm_Offset);
    /* TODO
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_LongTerm_Offset, &bErr_SAS_LongTerm_Offset, 0);
    */
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_ModelMay, pSASPlauOutput->SasPlauModelErr, &bErr_SAS_ModelMay, &bSus_SAS_ModelMay);
    ret = EEM_SetErrorInfo(EEM_Err_id_SAS_Stick, pSASPlauOutput->SasPlauStickErr, &bErr_SAS_Stick, &bSus_SAS_Stick);


    /* Fail Flag */
    gEEM_FailInfo.bSAS = FALSE;
    if((bErr_SAS_Timeout == TRUE) || (bErr_SAS_Invalid == TRUE) || (bErr_SAS_CheckSum == TRUE) ||
       (bErr_SAS_RollingCnt == TRUE) || (bErr_SAS_Range == TRUE) || (bErr_SAS_ShortTerm_Offset == TRUE) ||
       (bErr_SAS_LongTerm_Offset == TRUE) || (bErr_SAS_ModelMay == TRUE) || (bErr_SAS_Stick == TRUE)
      )
    {
        gEEM_FailInfo.bSAS = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_Str = gEEM_FailInfo.bSAS;


    /* Suspect Flag */
    gEEM_SuspectInfo.bSAS = FALSE;
    if((bSus_SAS_Timeout == TRUE) || (bSus_SAS_Invalid == TRUE) || (bSus_SAS_CheckSum == TRUE) ||
       (bSus_SAS_RollingCnt == TRUE) || (bSus_SAS_Range == TRUE) || (bSus_SAS_ShortTerm_Offset == TRUE) ||
       (bSus_SAS_LongTerm_Offset == TRUE) || (bSus_SAS_ModelMay == TRUE) || (bSus_SAS_Stick == TRUE)
      )
    {
        gEEM_SuspectInfo.bSAS = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_Str = gEEM_SuspectInfo.bSAS;    
}

static void EEM_CheckPressureError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    PresMonFaultInfo_t *pPresMonFaultInfo = NULL;
    PressPInfo_t *pPressPInfo = NULL;

    boolean bErr_CIRP1_FC_Fault = FALSE, bErr_CIRP1_FC_Mismatch = FALSE, bErr_CIRP1_FC_CRC = FALSE;
    boolean bErr_CIRP1_SC_Temperature = FALSE, bErr_CIRP1_SC_Mismatch = FALSE, bErr_CIRP1_SC_CRC = FALSE;
    boolean bErr_CIRP2_FC_Fault = FALSE, bErr_CIRP2_FC_Mismatch = FALSE, bErr_CIRP2_FC_CRC = FALSE;
    boolean bErr_CIRP2_SC_Temperature = FALSE, bErr_CIRP2_SC_Mismatch = FALSE, bErr_CIRP2_SC_CRC = FALSE;
    boolean bErr_SIMP_FC_Fault = FALSE, bErr_SIMP_FC_Mismatch = FALSE, bErr_SIMP_FC_CRC = FALSE;
    boolean bErr_SIMP_SC_Temperature = FALSE, bErr_SIMP_SC_Mismatch = FALSE, bErr_SIMP_SC_CRC = FALSE;

    boolean bErr_CIRP1_Noise = FALSE, bErr_CIRP2_Noise = FALSE, bErr_SIMP_Noise = FALSE;

    boolean bSus_CIRP1_FC_Fault = FALSE, bSus_CIRP1_FC_Mismatch = FALSE, bSus_CIRP1_FC_CRC = FALSE;
    boolean bSus_CIRP1_SC_Temperature = FALSE, bSus_CIRP1_SC_Mismatch = FALSE, bSus_CIRP1_SC_CRC = FALSE;
    boolean bSus_CIRP2_FC_Fault = FALSE, bSus_CIRP2_FC_Mismatch = FALSE, bSus_CIRP2_FC_CRC = FALSE;
    boolean bSus_CIRP2_SC_Temperature = FALSE, bSus_CIRP2_SC_Mismatch = FALSE, bSus_CIRP2_SC_CRC = FALSE;
    boolean bSus_SIMP_FC_Fault = FALSE, bSus_SIMP_FC_Mismatch = FALSE, bSus_SIMP_FC_CRC = FALSE;
    boolean bSus_SIMP_SC_Temperature = FALSE, bSus_SIMP_SC_Mismatch = FALSE, bSus_SIMP_SC_CRC = FALSE;

    boolean bSus_CIRP1_Noise = FALSE, bSus_CIRP2_Noise = FALSE, bSus_SIMP_Noise = FALSE;    

    pPresMonFaultInfo = &(pEem_Data->Eem_MainPresMonFaultInfo);
    pPressPInfo = &(pEem_Data->Eem_MainPressPInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_FC_Fault, pPresMonFaultInfo->CIRP1_FC_Fault_Err, &bErr_CIRP1_FC_Fault, &bSus_CIRP1_FC_Fault);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_FC_Mismatch, pPresMonFaultInfo->CIRP1_FC_Mismatch_Err, &bErr_CIRP1_FC_Mismatch, &bSus_CIRP1_FC_Mismatch);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_FC_CRC, pPresMonFaultInfo->CIRP1_FC_CRC_Err, &bErr_CIRP1_FC_CRC, &bSus_CIRP1_FC_CRC);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_SC_Temperature, pPresMonFaultInfo->CIRP1_SC_Temp_Err, &bErr_CIRP1_SC_Temperature, &bSus_CIRP1_SC_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_SC_Mismatch, pPresMonFaultInfo->CIRP1_SC_Mismatch_Err, &bErr_CIRP1_SC_Mismatch, &bSus_CIRP1_SC_Mismatch);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_SC_CRC, pPresMonFaultInfo->CIRP1_SC_CRC_Err, &bErr_CIRP1_SC_CRC, &bSus_CIRP1_SC_CRC);

    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP1_Noise, pPressPInfo->CirP1_Noise_Err, &bErr_CIRP1_Noise, &bSus_CIRP1_Noise);

    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_FC_Fault, pPresMonFaultInfo->CIRP2_FC_Fault_Err, &bErr_CIRP2_FC_Fault, &bSus_CIRP2_FC_Fault);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_FC_Mismatch, pPresMonFaultInfo->CIRP2_FC_Mismatch_Err, &bErr_CIRP2_FC_Mismatch, &bSus_CIRP2_FC_Mismatch);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_FC_CRC, pPresMonFaultInfo->CIRP2_FC_CRC_Err, &bErr_CIRP2_FC_CRC, &bSus_CIRP2_FC_CRC);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_SC_Temperature, pPresMonFaultInfo->CIRP2_SC_Temp_Err, &bErr_CIRP2_SC_Temperature, &bSus_CIRP2_SC_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_SC_Mismatch, pPresMonFaultInfo->CIRP2_SC_Mismatch_Err, &bErr_CIRP2_SC_Mismatch, &bSus_CIRP2_SC_Mismatch);
    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_SC_CRC, pPresMonFaultInfo->CIRP2_SC_CRC_Err, &bErr_CIRP2_SC_CRC, &bSus_CIRP2_SC_CRC);

    ret = EEM_SetErrorInfo(EEM_Err_id_CIRP2_Noise, pPressPInfo->CirP2_Noise_Err, &bErr_CIRP2_Noise, &bSus_CIRP2_Noise);
    
    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_FC_Fault, pPresMonFaultInfo->SIMP_FC_Fault_Err, &bErr_SIMP_FC_Fault, &bSus_SIMP_FC_Fault);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_FC_Mismatch, pPresMonFaultInfo->SIMP_FC_Mismatch_Err, &bErr_SIMP_FC_Mismatch, &bSus_SIMP_FC_Mismatch);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_FC_CRC, pPresMonFaultInfo->SIMP_FC_CRC_Err, &bErr_SIMP_FC_CRC, &bSus_SIMP_FC_CRC);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_SC_Temperature, pPresMonFaultInfo->SIMP_SC_Temp_Err, &bErr_SIMP_SC_Temperature, &bSus_SIMP_SC_Temperature);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_SC_Mismatch, pPresMonFaultInfo->SIMP_SC_Mismatch_Err, &bErr_SIMP_SC_Mismatch, &bSus_SIMP_SC_Mismatch);
    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_SC_CRC, pPresMonFaultInfo->SIMP_SC_CRC_Err, &bErr_SIMP_SC_CRC, &bSus_SIMP_SC_CRC);

    ret = EEM_SetErrorInfo(EEM_Err_id_SIMP_Noise, pPressPInfo->SimP_Noise_Err, &bErr_SIMP_Noise, &bSus_SIMP_Noise);


    /* Fail Flag */
    gEEM_FailInfo.bCIRP1 = FALSE;
    if((bErr_CIRP1_FC_Fault == TRUE) || (bErr_CIRP1_FC_Mismatch == TRUE) || (bErr_CIRP1_FC_CRC == TRUE) ||
       (bErr_CIRP1_SC_Temperature == TRUE) || (bErr_CIRP1_SC_Mismatch == TRUE) || (bErr_CIRP1_SC_CRC == TRUE) ||
       (bErr_CIRP1_Noise == TRUE)
      )
    {
        gEEM_FailInfo.bCIRP1 = TRUE;
    }

    gEEM_FailInfo.bCIRP2 = FALSE;
    if((bErr_CIRP2_FC_Fault == TRUE) || (bErr_CIRP2_FC_Mismatch == TRUE) || (bErr_CIRP2_FC_CRC == TRUE) ||
       (bErr_CIRP2_SC_Temperature == TRUE) || (bErr_CIRP2_SC_Mismatch == TRUE) || (bErr_CIRP2_SC_CRC == TRUE) ||
       (bErr_CIRP2_Noise == TRUE)
      )
    {
        gEEM_FailInfo.bCIRP2 = TRUE;
    }

    gEEM_FailInfo.bSIMP = FALSE;
    if((bErr_SIMP_FC_Fault == TRUE) || (bErr_SIMP_FC_Mismatch == TRUE) || (bErr_SIMP_FC_CRC == TRUE) ||
       (bErr_SIMP_SC_Temperature == TRUE) || (bErr_SIMP_SC_Mismatch == TRUE) || (bErr_SIMP_SC_CRC == TRUE) ||
       (bErr_SIMP_Noise == TRUE)
      )
    {
        gEEM_FailInfo.bSIMP = TRUE;
    }    

    pEem_Data->Eem_MainEemFailData.Eem_Fail_CirP1 = gEEM_FailInfo.bCIRP1;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_CirP2 = gEEM_FailInfo.bCIRP2;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_SimP = gEEM_FailInfo.bSIMP;


    /* Suspect Flag */
    gEEM_SuspectInfo.bCIRP1 = FALSE;
    if((bSus_CIRP1_FC_Fault == TRUE) || (bSus_CIRP1_FC_Mismatch == TRUE) || (bSus_CIRP1_FC_CRC == TRUE) ||
       (bSus_CIRP1_SC_Temperature == TRUE) || (bSus_CIRP1_SC_Mismatch == TRUE) || (bSus_CIRP1_SC_CRC == TRUE) ||
       (bSus_CIRP1_Noise == TRUE)
      )
    {
        gEEM_SuspectInfo.bCIRP1 = TRUE;
    }

    gEEM_SuspectInfo.bCIRP2 = FALSE;
    if((bSus_CIRP2_FC_Fault == TRUE) || (bSus_CIRP2_FC_Mismatch == TRUE) || (bSus_CIRP2_FC_CRC == TRUE) ||
       (bSus_CIRP2_SC_Temperature == TRUE) || (bSus_CIRP2_SC_Mismatch == TRUE) || (bSus_CIRP2_SC_CRC == TRUE) ||
       (bSus_CIRP2_Noise == TRUE)
      )
    {
        gEEM_SuspectInfo.bCIRP2 = TRUE;
    }

    gEEM_SuspectInfo.bSIMP = FALSE;
    if((bSus_SIMP_FC_Fault == TRUE) || (bSus_SIMP_FC_Mismatch == TRUE) || (bSus_SIMP_FC_CRC == TRUE) ||
       (bSus_SIMP_SC_Temperature == TRUE) || (bSus_SIMP_SC_Mismatch == TRUE) || (bSus_SIMP_SC_CRC == TRUE) ||
       (bSus_SIMP_Noise == TRUE)
      )
    {
        gEEM_SuspectInfo.bSIMP = TRUE;
    }    

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_CirP1 = gEEM_SuspectInfo.bCIRP1;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_CirP2 = gEEM_SuspectInfo.bCIRP2;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_SimP = gEEM_SuspectInfo.bSIMP;    
}

static void EEM_CheckSwitchError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    SwtMonFaultInfo_t *pSwtMonFaultInfo = NULL;
    SwtPFaultInfo_t *pSwtPFaultInfo_t = NULL;

    boolean bErr_ESCSw_Invalid = FALSE, bErr_HDCSw_Invalid = FALSE, bErr_AVHSw_Invalid = FALSE;
    boolean bErr_BLS_HighStick = FALSE, bErr_BLS_LowStick = FALSE, bErr_BS_HighStick = FALSE;
    boolean bErr_BS_LowStick = FALSE, bErr_BFL_Invalid = FALSE, bErr_PB_Invalid = FALSE;

    boolean bSus_ESCSw_Invalid = FALSE, bSus_HDCSw_Invalid = FALSE, bSus_AVHSw_Invalid = FALSE;
    boolean bSus_BLS_HighStick = FALSE, bSus_BLS_LowStick = FALSE, bSus_BS_HighStick = FALSE;
    boolean bSus_BS_LowStick = FALSE, bSus_BFL_Invalid = FALSE, bSus_PB_Invalid = FALSE;

    pSwtMonFaultInfo = &(pEem_Data->Eem_MainSwtMonFaultInfo);
    pSwtPFaultInfo_t = &(pEem_Data->Eem_MainSwtPFaultInfo);

    ret = EEM_SetErrorInfo(EEM_Err_id_EscOffSwitch_Invalid, pSwtMonFaultInfo->SWM_ESCOFF_Sw_Err, &bErr_ESCSw_Invalid, &bSus_ESCSw_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_HDCSwitch_Invalid, pSwtMonFaultInfo->SWM_HDC_Sw_Err, &bErr_HDCSw_Invalid, &bSus_HDCSw_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_AvhSwitch_Invalid, pSwtMonFaultInfo->SWM_AVH_Sw_Err, &bErr_AVHSw_Invalid, &bSus_AVHSw_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_BLSSwitch_HighStick, pSwtPFaultInfo_t->SWM_BLS_Sw_HighStick_Err, &bErr_BLS_HighStick, &bSus_BLS_HighStick);
    ret = EEM_SetErrorInfo(EEM_Err_id_BLSSwitch_LowStick, pSwtPFaultInfo_t->SWM_BLS_Sw_LowStick_Err, &bErr_BLS_LowStick, &bSus_BLS_LowStick);
    ret = EEM_SetErrorInfo(EEM_Err_id_BSSwitch_HighStick, pSwtPFaultInfo_t->SWM_BS_Sw_HighStick_Err, &bErr_BS_HighStick, &bSus_BS_HighStick);
    ret = EEM_SetErrorInfo(EEM_Err_id_BSSwitch_LowStick, pSwtPFaultInfo_t->SWM_BS_Sw_LowStick_Err, &bErr_BS_LowStick, &bSus_BS_LowStick);
    ret = EEM_SetErrorInfo(EEM_Err_id_BFLSwitch_Invalid, pSwtMonFaultInfo->SWM_BFL_Sw_Err, &bErr_BFL_Invalid, &bSus_BFL_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_PBSwitch_Invalid, pSwtMonFaultInfo->SWM_PB_Sw_Err, &bErr_PB_Invalid, &bSus_PB_Invalid);


    /* Fail Flag */
    gEEM_FailInfo.bESCSw = FALSE;
    if(bErr_ESCSw_Invalid == TRUE)
    {
        gEEM_FailInfo.bESCSw = TRUE;
    }

    gEEM_FailInfo.bHDCSw = FALSE;
    if(bErr_HDCSw_Invalid == TRUE)
    {
        gEEM_FailInfo.bHDCSw = TRUE;
    }

    gEEM_FailInfo.bAVHSw = FALSE;
    if(bErr_AVHSw_Invalid == TRUE)
    {
        gEEM_FailInfo.bAVHSw = TRUE;
    }

    gEEM_FailInfo.bBLS = FALSE;
    if((bErr_BLS_HighStick == TRUE) || (bErr_BLS_LowStick == TRUE))
    {
        gEEM_FailInfo.bBLS = TRUE;
    }

    gEEM_FailInfo.bBS = FALSE;
    if((bErr_BS_HighStick == TRUE) || (bErr_BS_LowStick == TRUE))
    {
        gEEM_FailInfo.bBS = TRUE;
    }

    gEEM_FailInfo.bBrakeFluid = FALSE;
    if(bErr_BFL_Invalid == TRUE)
    {
        gEEM_FailInfo.bBrakeFluid = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_ESCSw = gEEM_FailInfo.bESCSw;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_HDCSw = gEEM_FailInfo.bHDCSw;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_AVHSw = gEEM_FailInfo.bAVHSw;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_BLS = gEEM_FailInfo.bBLS;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_BrakeFluid = gEEM_FailInfo.bBrakeFluid;


    /* Suspect Flag */
    gEEM_SuspectInfo.bESCSw = FALSE;
    if(bSus_ESCSw_Invalid == TRUE)
    {
        gEEM_SuspectInfo.bESCSw = TRUE;
    }

    gEEM_SuspectInfo.bHDCSw = FALSE;
    if(bSus_HDCSw_Invalid == TRUE)
    {
        gEEM_SuspectInfo.bHDCSw = TRUE;
    }

    gEEM_SuspectInfo.bAVHSw = FALSE;
    if(bSus_AVHSw_Invalid == TRUE)
    {
        gEEM_SuspectInfo.bAVHSw = TRUE;
    }

    gEEM_SuspectInfo.bBLS = FALSE;
    if((bSus_BLS_HighStick == TRUE) || (bSus_BLS_LowStick == TRUE))
    {
        gEEM_SuspectInfo.bBLS = TRUE;
    }

    gEEM_SuspectInfo.bBS = FALSE;
    if((bSus_BS_HighStick == TRUE) || (bSus_BS_LowStick == TRUE))
    {
        gEEM_SuspectInfo.bBS = TRUE;
    }

    gEEM_SuspectInfo.bBrakeFluid = FALSE;
    if(bSus_BFL_Invalid == TRUE)
    {
        gEEM_SuspectInfo.bBrakeFluid = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_ESCSw = gEEM_SuspectInfo.bESCSw;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_HDCSw = gEEM_SuspectInfo.bHDCSw;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_AVHSw = gEEM_SuspectInfo.bAVHSw;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_BLS = gEEM_SuspectInfo.bBLS;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid = gEEM_SuspectInfo.bBrakeFluid;    
}

static void EEM_CheckPedalError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    PedalMData_t *pPedalMData = NULL;

    boolean bErr_PTS1_Open = FALSE, bErr_PTS1_Short = FALSE, bErr_PTS2_Open = FALSE, bErr_PTS2_Short = FALSE;
    boolean bErr_PTS1_SupplyPwr = FALSE, bErr_PTS2_SupplyPwr = FALSE;
    boolean bErr_VDD3_OverVolt = FALSE, bErr_VDD3_UnderVolt = FALSE, bErr_VDD3_OverCurrent = FALSE;
    boolean bErr_Pedal_Calibration = FALSE, bErr_Pedal_Offset = FALSE, bErr_Pedal_Stick = FALSE, bErr_Pedal_Noise = FALSE;

    boolean bSus_PTS1_Open = FALSE, bSus_PTS1_Short = FALSE, bSus_PTS2_Open = FALSE, bSus_PTS2_Short = FALSE;
    boolean bSus_PTS1_SupplyPwr = FALSE, bSus_PTS2_SupplyPwr = FALSE;
    boolean bSus_VDD3_OverVolt = FALSE, bSus_VDD3_UnderVolt = FALSE, bSus_VDD3_OverCurrent = FALSE;
    boolean bSus_Pedal_Calibration = FALSE, bSus_Pedal_Offset = FALSE, bSus_Pedal_Stick = FALSE, bSus_Pedal_Noise = FALSE;

    pPedalMData = &(pEem_Data->Eem_MainPedalMData);

    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_PTS1_Open, pPedalMData->PedalM_PTS1_Open_Err, &bErr_PTS1_Open, &bSus_PTS1_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_PTS1_Short, pPedalMData->PedalM_PTS1_Short_Err, &bErr_PTS1_Short, &bSus_PTS1_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_PTS2_Open, pPedalMData->PedalM_PTS2_Open_Err, &bErr_PTS2_Open, &bSus_PTS2_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_PTS2_Short, pPedalMData->PedalM_PTS2_Short_Err, &bErr_PTS2_Short, &bSus_PTS2_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_PTS1_SupplyPower_Open, pPedalMData->PedalM_PTS1_SupplyPower_Err, &bErr_PTS1_SupplyPwr, &bSus_PTS1_SupplyPwr);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_PTS2_SupplyPower_Open, pPedalMData->PedalM_PTS2_SupplyPower_Err, &bErr_PTS2_SupplyPwr, &bSus_PTS2_SupplyPwr);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_VDD3_OverVolt, pPedalMData->PedalM_VDD3_OverVolt_Err, &bErr_VDD3_OverVolt, &bSus_VDD3_OverVolt);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_VDD3_UnderVolt, pPedalMData->PedalM_VDD3_UnderVolt_Err, &bErr_VDD3_UnderVolt, &bSus_VDD3_UnderVolt);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_VDD3_OverCurrent, pPedalMData->PedalM_VDD3_OverCurrent_Err, &bErr_VDD3_OverCurrent, &bSus_VDD3_OverCurrent);

    /* TODO
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_Calibration, 0, 0, &bFail_Pedal_Calibration);

    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_Offset, 0, 0, &bFail_Pedal_Offset);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_Stick, 0, 0, &bFail_Pedal_Stick);
    ret = EEM_SetErrorInfo(EEM_Err_id_Pedal_Noise, 0, 0, &bFail_Pedal_Noise);
    */


    /* Fail Flag */
    gEEM_FailInfo.bPedal_PTS1 = FALSE;
    if((bErr_PTS1_Open == TRUE) || (bErr_PTS1_Short == TRUE) || (bErr_PTS1_SupplyPwr == TRUE) ||
       (bErr_VDD3_OverVolt == TRUE) || (bErr_VDD3_UnderVolt == TRUE) || (bErr_VDD3_OverCurrent == TRUE) ||
       (bErr_Pedal_Offset == TRUE) || (bErr_Pedal_Stick == TRUE) || (bErr_Pedal_Noise == TRUE) ||
       (bErr_Pedal_Calibration == TRUE)
      )
    {
        gEEM_FailInfo.bPedal_PTS1 = TRUE;
    }

    gEEM_FailInfo.bPedal_PTS2 = FALSE;
    if((bErr_PTS2_Open == TRUE) || (bErr_PTS2_Short == TRUE) || (bErr_PTS2_SupplyPwr == TRUE) ||
       (bErr_VDD3_OverVolt == TRUE) || (bErr_VDD3_UnderVolt == TRUE) || (bErr_VDD3_OverCurrent == TRUE) ||
       (bErr_Pedal_Offset == TRUE) || (bErr_Pedal_Stick == TRUE) || (bErr_Pedal_Noise == TRUE) ||
       (bErr_Pedal_Calibration == TRUE)
      )
    {
        gEEM_FailInfo.bPedal_PTS2 = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_PedalPDT = gEEM_FailInfo.bPedal_PTS1;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_PedalPDF = gEEM_FailInfo.bPedal_PTS2;


    /* Suspect Flag */
    gEEM_SuspectInfo.bPedal_PTS1 = FALSE;
    if((bSus_PTS1_Open == TRUE) || (bSus_PTS1_Short == TRUE) || (bSus_PTS1_SupplyPwr == TRUE) ||
       (bSus_VDD3_OverVolt == TRUE) || (bSus_VDD3_UnderVolt == TRUE) || (bSus_VDD3_OverCurrent == TRUE) ||
       (bSus_Pedal_Offset == TRUE) || (bSus_Pedal_Stick == TRUE) || (bSus_Pedal_Noise == TRUE) ||
       (bSus_Pedal_Calibration == TRUE)
      )
    {
        gEEM_SuspectInfo.bPedal_PTS1 = TRUE;
    }

    gEEM_SuspectInfo.bPedal_PTS2 = FALSE;
    if((bSus_PTS2_Open == TRUE) || (bSus_PTS2_Short == TRUE) || (bSus_PTS2_SupplyPwr == TRUE) ||
       (bSus_VDD3_OverVolt == TRUE) || (bSus_VDD3_UnderVolt == TRUE) || (bSus_VDD3_OverCurrent == TRUE) ||
       (bSus_Pedal_Offset == TRUE) || (bSus_Pedal_Stick == TRUE) || (bSus_Pedal_Noise == TRUE) ||
       (bSus_Pedal_Calibration == TRUE)
      )
    {
        gEEM_SuspectInfo.bPedal_PTS2 = TRUE;
    }

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_PedalPDT = gEEM_SuspectInfo.bPedal_PTS1;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_PedalPDF = gEEM_SuspectInfo.bPedal_PTS2;    
}

static void EEM_CheckRelayError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    RelayMonitor_t *pRelayMonData = NULL;

    boolean bErr_Relay1_Open = FALSE, bErr_Relay1_Short = FALSE, bErr_Relay2_Open = FALSE, bErr_Relay2_Short = FALSE;
    boolean bSus_Relay1_Open = FALSE, bSus_Relay1_Short = FALSE, bSus_Relay2_Open = FALSE, bSus_Relay2_Short = FALSE;
    
    pRelayMonData = &(pEem_Data->Eem_MainRelayMonitorData);

    ret = EEM_SetErrorInfo(EEM_Err_id_Relay1_Open, pRelayMonData->RlyM_HDCRelay_Open_Err, &bErr_Relay1_Open, &bSus_Relay1_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_Relay1_Short, pRelayMonData->RlyM_HDCRelay_Short_Err, &bErr_Relay1_Short, &bSus_Relay1_Short);
    ret = EEM_SetErrorInfo(EEM_Err_id_Relay2_Open, pRelayMonData->RlyM_ESSRelay_Open_Err, &bErr_Relay2_Open, &bSus_Relay2_Open);
    ret = EEM_SetErrorInfo(EEM_Err_id_Relay2_Short, pRelayMonData->RlyM_ESSRelay_Short_Err, &bErr_Relay2_Short, &bSus_Relay2_Short);


    /* Fail Flag */
    gEEM_FailInfo.bBrakeLampRelay = FALSE;
    if((bErr_Relay1_Open == TRUE) || (bErr_Relay1_Short == TRUE))
    {
        gEEM_FailInfo.bBrakeLampRelay = TRUE;
    }

    gEEM_FailInfo.bESSRelay = FALSE;
    if((bErr_Relay2_Open == TRUE) || (bErr_Relay2_Short == TRUE))
    {
        gEEM_FailInfo.bESSRelay = TRUE;
    }

    pEem_Data->Eem_MainEemFailData.Eem_Fail_BrakeLampRelay = gEEM_FailInfo.bBrakeLampRelay;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_EssRelay = gEEM_FailInfo.bESSRelay;


    /* Suspect Flag */
    gEEM_SuspectInfo.bBrakeLampRelay = FALSE;
    if((bSus_Relay1_Open == TRUE) || (bSus_Relay1_Short == TRUE))
    {
        gEEM_SuspectInfo.bBrakeLampRelay = TRUE;
    }

    gEEM_SuspectInfo.bESSRelay = FALSE;
    if((bSus_Relay2_Open == TRUE) || (bSus_Relay2_Short == TRUE))
    {
        gEEM_SuspectInfo.bESSRelay = TRUE;
    }    

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay = gEEM_SuspectInfo.bBrakeLampRelay;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_EssRelay = gEEM_SuspectInfo.bESSRelay;
}

static void EEM_CheckTempError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    /* TODO 
    LogicTempMonitor_t *pLogicTempMonData = NULL;

    pLogicTempMonData = &(pEem_Data->Eem_MainLogicTempMonData);

    ret = EEM_SetErrorInfo(EEM_Err_id_TCSTemp, pLogicTempMonData->LTemp_TCS_Temp_Er);
    ret = EEM_SetErrorInfo(EEM_Err_id_HDCTemp, pLogicTempMonData->LTemp_HDC_Temp_Err);
    ret = EEM_SetErrorInfo(EEM_Err_id_SCCTemp, pLogicTempMonData->LTemp_SCC_Temp_Err);
    ret = EEM_SetErrorInfo(EEM_Err_id_TVBBTemp, pLogicTempMonData->LTemp_TVbb_Temp_Err);
    */
}

static void EEM_CheckCANError(Eem_Main_HdrBusType *pEem_Data)
{
    Std_ReturnType ret = E_OK;
    CanMonData_t *pCanMonData = NULL;

    boolean bErr_MainCanBusOff = FALSE, bErr_SubCanBusOff = FALSE, bErr_MainCanOverRun = FALSE, bErr_SubCanOverRun = FALSE;
    boolean bErr_EMS1_Timeout = FALSE, bErr_EMS2_Timeout = FALSE, bErr_TCU1_Timeout = FALSE, bErr_TCU5_Timeout = FALSE;
    boolean bErr_FWD1_Timeout = FALSE, bErr_HCU1_Timeout = FALSE, bErr_HCU2_Timeout = FALSE, bErr_HCU3_Timeout = FALSE;
    boolean bErr_VSM2_Timeout = FALSE, bErr_EMS_Invalid = FALSE, bErr_TCU_Invalid = FALSE;

    boolean bSus_MainCanBusOff = FALSE, bSus_SubCanBusOff = FALSE, bSus_MainCanOverRun = FALSE, bSus_SubCanOverRun = FALSE;
    boolean bSus_EMS1_Timeout = FALSE, bSus_EMS2_Timeout = FALSE, bSus_TCU1_Timeout = FALSE, bSus_TCU5_Timeout = FALSE;
    boolean bSus_FWD1_Timeout = FALSE, bSus_HCU1_Timeout = FALSE, bSus_HCU2_Timeout = FALSE, bSus_HCU3_Timeout = FALSE;
    boolean bSus_VSM2_Timeout = FALSE, bSus_EMS_Invalid = FALSE, bSus_TCU_Invalid = FALSE;

    pCanMonData = &(pEem_Data->Eem_MainCanMonData);

    ret = EEM_SetErrorInfo(EEM_Err_id_MainCan_BusOff, pCanMonData->CanM_MainBusOff_Err, &bErr_MainCanBusOff, &bSus_MainCanBusOff);
    ret = EEM_SetErrorInfo(EEM_Err_id_SubCan_BusOff, pCanMonData->CanM_SubBusOff_Err, &bErr_SubCanBusOff, &bSus_SubCanBusOff);
    ret = EEM_SetErrorInfo(EEM_Err_id_MainCan_OverRun, pCanMonData->CanM_MainOverRun_Err, &bErr_MainCanOverRun, &bSus_MainCanOverRun);
    ret = EEM_SetErrorInfo(EEM_Err_id_SubCan_OverRun, pCanMonData->CanM_SubOverRun_Err, &bErr_SubCanOverRun, &bSus_SubCanOverRun);
    ret = EEM_SetErrorInfo(EEM_Err_id_EMS1_Timeout, pCanMonData->CanM_EMS1_Tout_Err, &bErr_EMS1_Timeout, &bSus_EMS1_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_EMS2_Timeout, pCanMonData->CanM_EMS2_Tout_Err, &bErr_EMS2_Timeout, &bSus_EMS2_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_TCU1_Timeout, pCanMonData->CanM_TCU1_Tout_Err, &bErr_TCU1_Timeout, &bSus_TCU1_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_TCU5_Timeout, pCanMonData->CanM_TCU5_Tout_Err, &bErr_TCU5_Timeout, &bSus_TCU5_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_FWD1_Timeout, pCanMonData->CanM_FWD1_Tout_Err, &bErr_FWD1_Timeout, &bSus_FWD1_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_HCU1_Timeout, pCanMonData->CanM_HCU1_Tout_Err, &bErr_HCU1_Timeout, &bSus_HCU1_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_HCU2_Timeout, pCanMonData->CanM_HCU2_Tout_Err, &bErr_HCU2_Timeout, &bSus_HCU2_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_HCU3_Timeout, pCanMonData->CanM_HCU3_Tout_Err, &bErr_HCU3_Timeout, &bSus_HCU3_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_VSM2_Timeout, pCanMonData->CanM_VSM2_Tout_Err, &bErr_VSM2_Timeout, &bSus_VSM2_Timeout);
    ret = EEM_SetErrorInfo(EEM_Err_id_EMS_Invalid, pCanMonData->CanM_EMS_Invalid_Err, &bErr_EMS_Invalid, &bSus_EMS_Invalid);
    ret = EEM_SetErrorInfo(EEM_Err_id_TCU_Invalid, pCanMonData->CanM_TCU_Invalid_Err, &bErr_TCU_Invalid, &bSus_TCU_Invalid);


    /* Fail Flag */
    gEEM_FailInfo.bMainCanLine = FALSE;
    if((bErr_MainCanBusOff == TRUE) || (bErr_MainCanOverRun == TRUE))
    {
        gEEM_FailInfo.bMainCanLine = TRUE;
    }

    gEEM_FailInfo.bSubCanLine = FALSE;
    if((bErr_SubCanBusOff == TRUE) || (bErr_SubCanOverRun == TRUE))
    {
        gEEM_FailInfo.bSubCanLine = TRUE;
    }

    gEEM_FailInfo.bEMS_Timeout = FALSE;
    if((bErr_EMS1_Timeout == TRUE) || (bErr_EMS2_Timeout == TRUE) || (bErr_EMS_Invalid == TRUE))
    {
        gEEM_FailInfo.bEMS_Timeout = TRUE;
    }

    gEEM_FailInfo.bTCU_Timeout = FALSE;
    if((bErr_TCU1_Timeout == TRUE) || (bErr_TCU5_Timeout == TRUE) || (bErr_TCU_Invalid == TRUE))
    {
        gEEM_FailInfo.bTCU_Timeout = TRUE;
    }

    gEEM_FailInfo.bFWD_Timeout = FALSE;
    if(bErr_FWD1_Timeout == TRUE)
    {
        gEEM_FailInfo.bFWD_Timeout = TRUE;
    }

    gEEM_FailInfo.bHCU_Timeout = FALSE;
    if((bErr_HCU1_Timeout == TRUE) || (bErr_HCU2_Timeout == TRUE) || (bErr_HCU3_Timeout == TRUE))
    {
        gEEM_FailInfo.bHCU_Timeout = TRUE;
    }

    gEEM_FailInfo.bMCU_Timeout = FALSE; /* TODO */

    pEem_Data->Eem_MainEemFailData.Eem_Fail_MainCanLine = gEEM_FailInfo.bMainCanLine;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_SubCanLine = gEEM_FailInfo.bSubCanLine;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_EMSTimeOut = gEEM_FailInfo.bEMS_Timeout;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_TCUTimeOut = gEEM_FailInfo.bTCU_Timeout;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_FWDTimeOut = gEEM_FailInfo.bFWD_Timeout;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_HCUTimeOut = gEEM_FailInfo.bHCU_Timeout;
    pEem_Data->Eem_MainEemFailData.Eem_Fail_MCUTimeOut = gEEM_FailInfo.bMCU_Timeout;


    /* Suspect Flag */
    gEEM_SuspectInfo.bMainCanLine = FALSE;
    if((bSus_MainCanBusOff == TRUE) || (bSus_MainCanOverRun == TRUE))
    {
        gEEM_SuspectInfo.bMainCanLine = TRUE;
    }

    gEEM_SuspectInfo.bSubCanLine = FALSE;
    if((bSus_SubCanBusOff == TRUE) || (bSus_SubCanOverRun == TRUE))
    {
        gEEM_SuspectInfo.bSubCanLine = TRUE;
    }

    gEEM_SuspectInfo.bEMS_Timeout = FALSE;
    if((bSus_EMS1_Timeout == TRUE) || (bSus_EMS2_Timeout == TRUE) || (bSus_EMS_Invalid == TRUE))
    {
        gEEM_SuspectInfo.bEMS_Timeout = TRUE;
    }

    gEEM_SuspectInfo.bTCU_Timeout = FALSE;
    if((bSus_TCU1_Timeout == TRUE) || (bSus_TCU5_Timeout == TRUE) || (bSus_TCU_Invalid == TRUE))
    {
        gEEM_SuspectInfo.bTCU_Timeout = TRUE;
    }

    gEEM_SuspectInfo.bFWD_Timeout = FALSE;
    if(bSus_FWD1_Timeout == TRUE)
    {
        gEEM_SuspectInfo.bFWD_Timeout = TRUE;
    }

    gEEM_SuspectInfo.bHCU_Timeout = FALSE;
    if((bSus_HCU1_Timeout == TRUE) || (bSus_HCU2_Timeout == TRUE) || (bSus_HCU3_Timeout == TRUE))
    {
        gEEM_SuspectInfo.bHCU_Timeout = TRUE;
    }

    gEEM_SuspectInfo.bMCU_Timeout = FALSE; /* TODO */    

    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_MainCanLine = gEEM_SuspectInfo.bMainCanLine;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_SubCanLine = gEEM_SuspectInfo.bSubCanLine;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut = gEEM_SuspectInfo.bEMS_Timeout;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut = gEEM_SuspectInfo.bTCU_Timeout;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut = gEEM_SuspectInfo.bFWD_Timeout;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut = gEEM_SuspectInfo.bHCU_Timeout;
    pEem_Data->Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut = gEEM_SuspectInfo.bMCU_Timeout;    
}

static void EEM_CheckCodingError(Eem_Main_HdrBusType *pEem_Data)
{
    /* TODO */
}

static Std_ReturnType EEM_SetErrorInfo(EEM_ErrIdType id, uint8 u8EER_Err, boolean *pIntErrFlg, boolean *pIntSusFlg)
{
    uint16 index = 0;
    Std_ReturnType ret = E_OK;

    index = EEM_FindIndexByErrId(id);
    if(index == EEM_NOT_FOUND)
    {
        ret = E_NOT_OK;
    }
    else
    {
        gEEM_ErrInfoTable[index].EEM_Err = u8EER_Err;

        if((gEEM_IntErrTable[index].EEM_Attr == EEM_ATTR_PERMANENT_FAIL) &&
           (gEEM_FDCInfoTable[index].EEM_Status == ERR_FAILED)
          )
        {
            if(pIntErrFlg != NULL)
            {
                *pIntErrFlg = TRUE;
            }

            if(pIntSusFlg != NULL)
            {
                *pIntSusFlg = TRUE;
            }
        }
        else
        {
            EEM_ProcessFDCDebounce(index);

            if(pIntErrFlg != NULL)
            {
                if(gEEM_FDCInfoTable[index].EEM_Status == ERR_FAILED)
                {
                    *pIntErrFlg = TRUE;
                }
                else
                {
                    *pIntErrFlg = FALSE;
                }
            }

            if(pIntSusFlg != NULL)
            {
                if(gEEM_FDCInfoTable[index].EEM_FDC > 0)
                {
                    *pIntSusFlg = TRUE;
                }
                else
                {
                    *pIntSusFlg = FALSE;
                }
            }
        }
    }
        
    return ret;
}

static boolean EEM_GetErrorFlag(EEM_ErrIdType id)
{
    uint16 index = 0;
    boolean bError = FALSE;

    index = EEM_FindIndexByErrId(id);
    if(index != EEM_NOT_FOUND)
    {
        if(gEEM_FDCInfoTable[index].EEM_Status == ERR_FAILED)
        {
            bError = TRUE;
        }
    }

    return bError;
}

static boolean EEM_GetSuspectFlag(EEM_ErrIdType id)
{
    uint16 index = 0;
    boolean bSuspect = FALSE;

    index = EEM_FindIndexByErrId(id);
    if(index != EEM_NOT_FOUND)
    {
        if(gEEM_FDCInfoTable[index].EEM_FDC > 0)
        {
            bSuspect = TRUE;
        }
    }

    return bSuspect;
}

static Std_ReturnType EEM_ResetErrorInfo(EEM_ErrIdType id)
{
    uint16 index = 0;
    Std_ReturnType ret = E_OK;

    index = EEM_FindIndexByErrId(id);
    if(index == EEM_NOT_FOUND)
    {
        ret = E_NOT_OK;
    }
    else
    {
        gEEM_FDCInfoTable[index].EEM_FDC    = 0;
        gEEM_FDCInfoTable[index].EEM_Status = ERR_NONE;
    }

    return ret;
}

static void EEM_ProcessFDCDebounce(EEM_ErrIdType id)
{   
    uint16 index = 0;
    uint8  bRunFDC = 0;

    index = EEM_FindIndexByErrId(id);
    if(index != EEM_NOT_FOUND)
    {
        /* check error received */
        bRunFDC = 0;
        
        switch(gEEM_IntErrTable[index].EEM_Cycletime)
        {
        case EEM_CYCLETIME_5:
            bRunFDC = 1;
            break;

        case EEM_CYCLETIME_10_0:
            if((gEEM_u16LoopCount == 0) || (gEEM_u16LoopCount == 2))
            {
                bRunFDC = 1;
            }
            break;

        case EEM_CYCLETIME_10_1:
            if((gEEM_u16LoopCount == 1) || (gEEM_u16LoopCount == 3))
            {
                bRunFDC = 1;
            }
            break;

        case EEM_CYCLETIME_20_0:
            if(gEEM_u16LoopCount == 0)
            {
                bRunFDC = 1;
            }
            break;

        case EEM_CYCLETIME_20_1:
            if(gEEM_u16LoopCount == 1)
            {
                bRunFDC = 1;
            }
            break;

        case EEM_CYCLETIME_20_2:
            if(gEEM_u16LoopCount == 2)
            {
                bRunFDC = 1;
            }
            break;            
                
        case EEM_CYCLETIME_20_3:
            if(gEEM_u16LoopCount == 3)
            {
                bRunFDC = 1;
            }
            break;            

        default:
            break;
        }

        if(bRunFDC == 1)
        {
            EEM_CalcFDCDebounce(index);
        }
    }
}

static void EEM_CalcFDCDebounce(uint16 i)
{
    sint32 tempFDC = 0;
    sint16 tempThres = 0;
    
    if((gEEM_ErrInfoTable[i].EEM_Err == ERR_INHIBIT) /* ||
       (EEM_ClearReq == TRUE) */
       )
    {
        gEEM_FDCInfoTable[i].EEM_FDC = 0;
    }
    else
    {
        switch(gEEM_ErrInfoTable[i].EEM_Err)
        {
        case ERR_PASSED :
            gEEM_FDCInfoTable[i].EEM_FDC = gEEM_IntErrTable[i].EEM_FDCPassThres;
            break;

        case ERR_FAILED :
            gEEM_FDCInfoTable[i].EEM_FDC = gEEM_IntErrTable[i].EEM_FDCFailThres;                
            break;

        case ERR_PREPASSED :
            tempFDC = (sint32)gEEM_FDCInfoTable[i].EEM_FDC;
            if(tempFDC > 0)
            {
                tempFDC = 0;
            }
            
            tempFDC += gEEM_IntErrTable[i].EEM_FDCDecrement;
            if(tempFDC < EEM_FDC_LOW_LIMIT)
            {
                tempFDC = EEM_FDC_LOW_LIMIT;
            }

            tempThres = gEEM_IntErrTable[i].EEM_FDCPassThres;
            if(tempFDC < tempThres)
            {
                tempFDC = tempThres;
            }

            gEEM_FDCInfoTable[i].EEM_FDC = (sint16)tempFDC;
            break;

        case ERR_PREFAILED :
            tempFDC = (sint32)gEEM_FDCInfoTable[i].EEM_FDC;
            if(tempFDC < 0)
            {
                tempFDC = 0;
            }
            
            tempFDC += gEEM_IntErrTable[i].EEM_FDCIncrement;
            if(tempFDC > EEM_FDC_HIGH_LIMIT)
            {
                tempFDC = EEM_FDC_HIGH_LIMIT;
            }

            tempThres = gEEM_IntErrTable[i].EEM_FDCFailThres;
            if(tempFDC > tempThres)
            {
                tempFDC = tempThres;
            }

            gEEM_FDCInfoTable[i].EEM_FDC = (sint16)tempFDC;
            break;

        default :
            break;
        }
    }
    
    gEEM_ErrInfoTable[i].EEM_Err = ERR_NONE;
}

static void EEM_ProcessDemProxy(void)
{
    uint16 i = 0;

    for(i = 0; i < EEM_Err_id_Max; i++)
    {
        if(gEEM_FDCInfoTable[i].EEM_Status != ERR_FAILED)
        {
            if(gEEM_FDCInfoTable[i].EEM_FDC >= gEEM_IntErrTable[i].EEM_FDCFailThres)
            {
                gEEM_FDCInfoTable[i].EEM_Status = ERR_FAILED;
                //Dem_ReportErrorStatus(DemErrId, DEM_EVENT_STATUS_FAILED);
                //FE_vWriteErrorCode(gEEM_IntErrTable[i].EEM_DemErrId, gEEM_IntErrTable[i].EEM_Ftb);
                FE_vWriteErrorCode(gEEM_IntErrTable[i].EEM_IntErrId + 1, gEEM_IntErrTable[i].EEM_Ftb); /* Temporary Internal DTC Exposes */
            }
        }

        if(gEEM_FDCInfoTable[i].EEM_Status != ERR_PASSED)
        {
            if(gEEM_FDCInfoTable[i].EEM_FDC <= gEEM_IntErrTable[i].EEM_FDCPassThres)
            {
                gEEM_FDCInfoTable[i].EEM_Status = ERR_PASSED;
                //Dem_ReportErrorStatus(DemErrId, DEM_EVENT_STATUS_PASSED);
            }
        }
    }

    if(clear_all_flg == 0)
    {
        FE_vWriteErrorEEPROM();
    }
    else
    {
        EEM_ClearAllError();
    }
}

static void EEM_ClearAllError(void)
{
    uint16 i = 0;

    for(i = 0; i < EEM_Err_id_Max; i++)
    {
        EEM_ResetErrorInfo(i);
    }

    EEM_ClearFailRelatedInfo();

    FE_vClearAllError();    
}

static void EEM_LogicDisableFlag(Eem_Main_HdrBusType *pEem_Data)
{
    #if (FLEX_SWT_APPLY == ENABLE)
        if(Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon == 0)
        {
            if(gFlexSwCnt < 30) /* 150ms */
            {
                gFlexSwCnt++;
            }
            else
            {
                gFlexSwState = 1;
            }
        }
        else
        {
            gFlexSwCnt = 0;
            gFlexSwState = 0;
        }
    #endif

    memset(&gEEM_CtrlIhbInfo, 0x00, sizeof(gEEM_CtrlIhbInfo));
    
    if((gEEM_FailInfo.bEcuHW == TRUE) || (gEEM_FailInfo.bHigherVolt == TRUE) || (gEEM_FailInfo.bLowerVolt == TRUE) ||
       (gEEM_FailInfo.bBBSSol == TRUE) || (gEEM_FailInfo.bBBSValveRelay == TRUE) || (gEEM_FailInfo.bMotor == TRUE) || 
       (gEEM_FailInfo.bMGD == TRUE) || (gEEM_FailInfo.bMPS == TRUE) || (gEEM_FailInfo.bCIRP1 == TRUE) || 
       (gEEM_FailInfo.bCIRP2 == TRUE) || (gEEM_FailInfo.bSIMP == TRUE)
       #if (FLEX_SWT_APPLY == ENABLE)
       /* for winter test */
       /*|| (gFlexSwState == 1) */
       #endif
       || (u8IdbCtrlInhibitFlg == 1)
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_BbsAllCtrlInhibit = TRUE;
        gEEM_CtrlIhbInfo.bCtrlIhb_BbsDefectiveModeFail = TRUE;
        gEEM_CtrlIhbInfo.bCtrlIhb_BbsDegradeModeFail = TRUE;
    }

    if((gEEM_FailInfo.bEcuHW == TRUE) || (gEEM_FailInfo.bHigherVolt == TRUE) || (gEEM_FailInfo.bLowerVolt == TRUE) ||
      (gEEM_FailInfo.bBBSSol == TRUE) || (gEEM_FailInfo.bBBSValveRelay == TRUE) 
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_BbsDiagControlInhibit = TRUE;
    }

    if((gEEM_FailInfo.bEcuHW == TRUE) || (gEEM_FailInfo.bBBSSol == TRUE) || (gEEM_FailInfo.bMotor == TRUE) || 
       (gEEM_FailInfo.bMGD == TRUE) || (gEEM_FailInfo.bMPS == TRUE) ||
       (gEEM_FailInfo.bBBSValveRelay == TRUE) || (gEEM_FailInfo.bOverVolt == TRUE) || /* TODO : --> Higher Volt */ 
       (gEEM_CtrlIhbInfo.bCtrlIhb_BbsDefectiveModeFail == TRUE)
       )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Bbs = TRUE;
    }

    if((gEEM_FailInfo.bEcuHW == TRUE) || (gEEM_FailInfo.bSameSideWss == TRUE) || (gEEM_FailInfo.bDiagonalWss == TRUE) || 
       (gEEM_FailInfo.bOverVolt == TRUE) || (gEEM_FailInfo.bESCSol == TRUE) || (gEEM_FailInfo.bBBSSol == TRUE) || 
       (gEEM_FailInfo.bVariantCoding == TRUE) || (gEEM_FailInfo.bHigherVolt == TRUE) || (gEEM_FailInfo.bOverVolt == TRUE) ||
       (gEEM_FailInfo.bLowVolt == TRUE) || (gEEM_FailInfo.bLowerVolt == TRUE) || 
       (gEEM_FailInfo.bBBSValveRelay == TRUE) || (gEEM_FailInfo.bESCValveRelay == TRUE) 
       /* TODO : Pedal Cal, Press Cal */
       )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Ebd = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Abs = gEEM_CtrlIhbInfo.bCtrlIhb_Abs;
    if((gEEM_FailInfo.bWssFL == TRUE) || (gEEM_FailInfo.bWssFR == TRUE) ||
       (gEEM_FailInfo.bWssRL == TRUE) || (gEEM_FailInfo.bWssRR == TRUE) ||
       /* (ece_flg == 0) */
       (gEEM_FailInfo.bAx == TRUE) || /* if 4WD */
       /* (fu1TurnOnABShwl == 1) */
       (gEEM_FailInfo.bUnderVolt == TRUE) ||
       (gEEM_FailInfo.bBrakeFluid == TRUE)
    ) /* TODO Pressure Fail Management */
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Abs = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Edc = gEEM_CtrlIhbInfo.bCtrlIhb_Abs;
    if((gEEM_FailInfo.bMainCanLine == TRUE) || (gEEM_FailInfo.bEMS_Timeout == TRUE) ||
       (gEEM_FailInfo.bTCU_Timeout == TRUE)
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Edc = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Tcs = gEEM_CtrlIhbInfo.bCtrlIhb_Abs;
    if((gEEM_FailInfo.bTCSTemp == TRUE) || (gEEM_FailInfo.bBrakeFluid == TRUE) ||
       (gEEM_FailInfo.bMainCanLine == TRUE) || (gEEM_FailInfo.bSubCanLine == TRUE) ||
       (gEEM_FailInfo.bHCU_Timeout == TRUE)
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Tcs = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Vdc = gEEM_CtrlIhbInfo.bCtrlIhb_Abs;
    if((gEEM_FailInfo.bYaw == TRUE) || (gEEM_FailInfo.bAy == TRUE) || (gEEM_FailInfo.bSAS == TRUE) ||
       (gEEM_FailInfo.bPedal_PTS1 == TRUE) || (gEEM_FailInfo.bPedal_PTS2 == TRUE) || (gEEM_FailInfo.bSIMP == TRUE) ||
       (gEEM_FailInfo.bEMS_Timeout == TRUE) || (gEEM_FailInfo.bMainCanLine == TRUE) ||
       (gEEM_FailInfo.bAx == TRUE) || /* if 4WD */
       /*(gEEM_FailInfo.bBLS == TRUE) */
       /*(gEEM_FailInfo.bESCSw == TRUE) */
       (gEEM_FailInfo.bTCU_Timeout == TRUE) || 
       (gEEM_FailInfo.bFWD_Timeout == TRUE) ||
       /* (fu1TurnOnESChwl == 1) */
       (gEEM_FailInfo.bBrakeFluid == TRUE) || 
       (gEEM_FailInfo.bHCU_Timeout == TRUE) ||
       (gEEM_FailInfo.bMCU_Timeout == TRUE)
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Vdc = TRUE;
    }

    /*
    if((fsu1BcpPriLeakFaultDet==1)||(fsu1BcpSecLeakFaultDet==1))
    {
        vdc_error_flg=1;
    }    
    */    

    /* if HMC */
    if((gEEM_CtrlIhbInfo.bCtrlIhb_Tcs == TRUE) || (gEEM_CtrlIhbInfo.bCtrlIhb_Vdc == TRUE))
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Tcs = TRUE;
        gEEM_CtrlIhbInfo.bCtrlIhb_Vdc = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Hsa = gEEM_CtrlIhbInfo.bCtrlIhb_Vdc;
    /* TODO Process Ax */
    if(gEEM_FailInfo.bAx == TRUE)
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Hsa = TRUE;
    }

    /* TODO : Gear, Clutch */
    
    /* TODO : BLS */

    gEEM_CtrlIhbInfo.bCtrlIhb_Hdc = gEEM_CtrlIhbInfo.bCtrlIhb_Hsa;
    if((gEEM_FailInfo.bHDCSw == TRUE) ||
       (gEEM_FailInfo.bEMS_Timeout == TRUE) ||
       (gEEM_FailInfo.bTCU_Timeout == TRUE)
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Hdc = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Pba = gEEM_CtrlIhbInfo.bCtrlIhb_Abs;
    if((gEEM_FailInfo.bPedal_PTS1 == TRUE) || (gEEM_FailInfo.bPedal_PTS2 == TRUE) ||
       (gEEM_FailInfo.bSIMP == TRUE)
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Pba = TRUE;
    }

    gEEM_CtrlIhbInfo.bCtrlIhb_Avh = gEEM_CtrlIhbInfo.bCtrlIhb_Vdc;
    if((gEEM_FailInfo.bAVHSw == TRUE) || (gEEM_FailInfo.bAx == TRUE) ||
       (gEEM_FailInfo.bBrakeLampRelay == TRUE) || 
       (gEEM_FailInfo.bEMS_Timeout == TRUE) || (gEEM_FailInfo.bTCU_Timeout == TRUE)
       /* TODO : || gEEM_FailInfo.bCLU2  */
      )
    {
        gEEM_CtrlIhbInfo.bCtrlIhb_Avh = TRUE;
    }

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Ebd == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd  = FALSE;
    }

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Abs == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs  = FALSE;
    }

    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs  = gEEM_CtrlIhbInfo.bCtrlIhb_Bbs;
    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc  = gEEM_CtrlIhbInfo.bCtrlIhb_Edc;

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Tcs == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs = TRUE;
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs = TRUE;
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs = FALSE;
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs = FALSE;
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs  = FALSE;
    }

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Vdc == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc  = FALSE;
    }

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Hsa == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa  = FALSE;
    }

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Hdc == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc  = FALSE;
    }        

    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba  = gEEM_CtrlIhbInfo.bCtrlIhb_Pba;

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Avh == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh  = FALSE;
    }  

    if((gEEM_CtrlIhbInfo.bCtrlIhb_Moc == TRUE) || (gEEM_FailInfo.bUnderVolt == TRUE) || (gEEM_FailInfo.bLowVolt == TRUE))
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc  = TRUE;
    }
    else
    {
        pEem_Data->Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc  = FALSE;
    }        
    
    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit  = gEEM_CtrlIhbInfo.bCtrlIhb_BbsAllCtrlInhibit;
    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = gEEM_CtrlIhbInfo.bCtrlIhb_BbsDiagControlInhibit;
    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail    = gEEM_CtrlIhbInfo.bCtrlIhb_BbsDegradeModeFail;
    pEem_Data->Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail  = gEEM_CtrlIhbInfo.bCtrlIhb_BbsDefectiveModeFail;
}

static void EEM_MakeInternalDTC(void)
{

}

static void EEM_MakeTestComplete(void)
{

}

static void EEM_FMVSS126Proc(void)
{

}

static void EEM_LampRequest(Eem_Main_HdrBusType *pEem_Data)
{
    boolean bIgnOnLampRequest = OFF;
    boolean bOnDiag = OFF, bIgnOn = OFF;
    uint16  u16StandStill_LampOnTime = 0, u16Driving_LampOnTime = 0;

    u16StandStill_LampOnTime = STANDSTILL_LAMP_ON_TIME / EEM_CYCLETIME;
    u16Driving_LampOnTime = DRIVING_LAMP_ON_TIME / EEM_CYCLETIME;

    bOnDiag = pEem_Data->Eem_MainDiagSci;
    bIgnOn  = pEem_Data->Eem_MainIgnOnOffSts;

    if((pEem_Data->Eem_MainIgnOnOffSts == 1) &&
       (pEem_Data->Eem_MainIgnEdgeSts == 1)
      )
    {
        gEEM_LampOnCount = 0;
        gEEM_IgnONCount = 0;    
    }

    if(gEEM_LampOnCount < u16StandStill_LampOnTime)
    {
        bIgnOnLampRequest = ON;
        gEEM_LampOnCount++;
    }

    if((Wss_SenWssSpeedOut.WssMin > DRIVING_LAMP_ON_SPEED) &&
       (gEEM_LampOnCount > u16Driving_LampOnTime)
      )
    {
        gEEM_LampOnCount = u16StandStill_LampOnTime;
    }

    EEM_BBSLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);
    
    EEM_ABSEBDLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);

    EEM_VDCLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);

    EEM_VDCFunctionLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);

    EEM_VDCOffLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);

    EEM_HDCLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);

    EEM_AVHLampRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);

    EEM_BuzzerRequest(bIgnOnLampRequest, bIgnOn, bOnDiag, pEem_Data);
}

static void EEM_BBSLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    boolean bWheel4Err = FALSE;
    boolean bAHBWLampReq = OFF, bRBCSLampReq = OFF;
    boolean bOnAhbDiag = pEem_Data->Eem_MainDiagAhbSci;
    uint8   u8ServiceLampReq = OFF;

    if((gEEM_FailInfo.bWssFL == TRUE) &&
       (gEEM_FailInfo.bWssFL == TRUE) &&
       (gEEM_FailInfo.bWssFL == TRUE) &&
       (gEEM_FailInfo.bWssFL == TRUE))
    {
        bWheel4Err = TRUE;
    }
    
    if((gEEM_CtrlIhbInfo.bCtrlIhb_BbsDefectiveModeFail == TRUE) ||
       (bWheel4Err == TRUE)
      )
    {
        bAHBWLampReq = ON;
        bRBCSLampReq = ON;
    }
    else
    {
        bAHBWLampReq = OFF;
        bRBCSLampReq = OFF;

        if((gEEM_FailInfo.bHCU_Timeout == TRUE) || (gEEM_FailInfo.bTCU_Timeout == TRUE) ||
           (gEEM_FailInfo.bEMS_Timeout == TRUE) || (gEEM_FailInfo.bMCU_Timeout == TRUE) ||
           (gEEM_FailInfo.bMainCanLine == TRUE) || (gEEM_FailInfo.bFrontSol == TRUE) ||
           (gEEM_FailInfo.bRearSol == TRUE) || (gEEM_FailInfo.bESCValveRelay == TRUE) ||
           (gEEM_FailInfo.bBBSValveRelay == TRUE) || (gEEM_FailInfo.bWssFL == TRUE) ||
           (gEEM_FailInfo.bWssFR == TRUE) || (gEEM_FailInfo.bWssRL == TRUE) || (gEEM_FailInfo.bWssRR == TRUE) ||
           (gEEM_FailInfo.bOverVolt == TRUE) || (gEEM_FailInfo.bBLS == TRUE)
          )
        {
            bRBCSLampReq = ON;
        }
    }

    if(gEEM_CtrlIhbInfo.bCtrlIhb_BbsDefectiveModeFail == TRUE)
    {
        u8ServiceLampReq = SERVICE_LAMP_DEFECTIVE;
    }
    else if(gEEM_CtrlIhbInfo.bCtrlIhb_BbsDegradeModeFail == TRUE)
    {
        u8ServiceLampReq = SERVICE_LAMP_DEGRADED;
    }
    else
    {
        u8ServiceLampReq = SERVICE_LAMP_OK;
    }

    if(bOnAhbDiag == TRUE)
    {
        bAHBWLampReq = ON;
        bRBCSLampReq = ON;
    }

    /* TODO : New Concept & Interface
        if(fu8EnterBleedingMode==1)
        {
            flu1AHBWLampReq=1;
            flu1RBCSLampReq=1;
            flu1ABSLampRequest = 1;
            flu1EBDLampRequest = 1;
        }
    */

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_AHBLampRequest = bAHBWLampReq;
    pEem_Data->Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest = bRBCSLampReq;
    pEem_Data->Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest = u8ServiceLampReq;
}

static void EEM_ABSEBDLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    boolean bEBDLampRequest = OFF, bABSLampRequest = OFF;

    if((bIgnOnLampRequest == ON) || (bOnDiag == TRUE))
    {
        bEBDLampRequest = TRUE;
        bABSLampRequest = TRUE;
    }
    else if(bIgnOn == OFF)
    {
        ;
    }
    else 
    {
        if(gEEM_CtrlIhbInfo.bCtrlIhb_Ebd == TRUE)
        {
            bEBDLampRequest = ON; /* Why EBD On when brake fluiid is high */
        }

        if(((gEEM_CtrlIhbInfo.bCtrlIhb_Abs == TRUE) && (pEem_Data->Eem_MainAbsCtrlInfo.AbsActFlg == FALSE))
           || (gEEM_CtrlIhbInfo.bCtrlIhb_Edc == TRUE)
           )
        {
            bABSLampRequest = ON;
        }

        if(gEEM_FailInfo.bBrakeFluid == TRUE)
        {
            bEBDLampRequest = ON;
        }
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_EBDLampRequest = bEBDLampRequest;
    pEem_Data->Eem_MainEemLampData.Eem_Lamp_ABSLampRequest = bABSLampRequest;
}

static void EEM_VDCLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    boolean bTCSLampRequest = OFF, bVDCLampRequest = OFF;

    if((bIgnOnLampRequest == TRUE) || (bOnDiag == TRUE))
    {
        bTCSLampRequest = TRUE;
        bVDCLampRequest = TRUE;
    }
    else if(bIgnOn == OFF)
    {
        ;
    }
    else
    {
        /* TODO Add TCS Lamp */
        if((gEEM_CtrlIhbInfo.bCtrlIhb_Edc == TRUE) || (gEEM_CtrlIhbInfo.bCtrlIhb_Tcs == TRUE))
        {
            bTCSLampRequest = ON;
        }

        /* TODO : Detail */
        if(gEEM_CtrlIhbInfo.bCtrlIhb_Vdc == TRUE)
        {
            bVDCLampRequest = ON;
        }

        /* VDC Lamp Request by HSA */
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_TCSLampRequest = bTCSLampRequest;
    pEem_Data->Eem_MainEemLampData.Eem_Lamp_VDCLampRequest = bVDCLampRequest;
}

static void EEM_TCSOffLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    boolean bTCSOffLampRequest = OFF;

    if((bIgnOnLampRequest == TRUE) || (bOnDiag == TRUE))
    {
        bTCSOffLampRequest = TRUE;
    }
    else if(bIgnOn == OFF)
    {
        ;
    }
    else
    {
        if(pEem_Data->Eem_MainEscSwtStInfo.TcsDisabledBySwt == TRUE)
        {
            bTCSOffLampRequest = TRUE;
        }
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest = bTCSOffLampRequest;
}

static void EEM_VDCOffLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    boolean bVDCOffLampRequest = OFF;

    if((bIgnOnLampRequest == TRUE) || (bOnDiag == TRUE))
    {
        bVDCOffLampRequest = TRUE;
    }
    else if(bIgnOn == OFF)
    {
        ;
    }
    else
    {
        if((pEem_Data->Eem_MainEscSwtStInfo.EscDisabledBySwt == TRUE) 
            || (pEem_Data->Eem_MainEscSwtStInfo.TcsDisabledBySwt == TRUE)
           )
        {
            bVDCOffLampRequest = ON;            
        }
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest = bVDCOffLampRequest;
}

static void EEM_VDCFunctionLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    uint8 u8VdcFuncLampRequest = 0;

    if(bIgnOnLampRequest == TRUE)
    {
        gEEM_FuncLampOffCount = 0;

        u8VdcFuncLampRequest = FUNC_LAMP_ON;
    }
    else if(bIgnOn == OFF)
    {
        gEEM_FuncLampOffCount = 0;
    }
    else
    {
        if((pEem_Data->Eem_MainEemLampData.Eem_Lamp_VDCLampRequest == OFF)
           && (pEem_Data->Eem_MainEscSwtStInfo.TcsDisabledBySwt == 0)
          )
        {
            if(Abc_CtrlFunctionLamp == TRUE)
            {
                if(gEEM_FuncLampOnCount < (FUNC_LAMP_ON_TIME / EEM_CYCLETIME))
                {
                    gEEM_FuncLampOnCount++;
                }
                else
                {
                    u8VdcFuncLampRequest = FUNC_LAMP_BLINKING; 
                    gEEM_FuncLampOffCount = (FUNC_LAMP_OFF_TIME / EEM_CYCLETIME); /* TODO : T/P */
                }
            }
            else if(gEEM_FuncLampOffCount > 0)
            {
                u8VdcFuncLampRequest = FUNC_LAMP_BLINKING; 
                gEEM_FuncLampOffCount--;
            }
            else
            {
                gEEM_FuncLampOnCount = 0;
                gEEM_FuncLampOffCount = 0;
                u8VdcFuncLampRequest = FUNC_LAMP_OFF;
            }
        }
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest = u8VdcFuncLampRequest;
}

static void EEM_HDCLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    uint8   u8HDCLampRequest = FALSE;
    boolean bHDCOn = FALSE; /* TODO */

    if((bIgnOnLampRequest == TRUE) || (bOnDiag == TRUE))
    {
        u8HDCLampRequest = 4; /* MACRO */
    }
    else if(bIgnOn == OFF)
    {
        ;
    }
    else
    {
        if(gEEM_CtrlIhbInfo.bCtrlIhb_Hdc == TRUE)
        {
            u8HDCLampRequest = HDC_LAMP_ERROR; /* W/Lamp Drive */
        }
        else if(pEem_Data->Eem_MainEscSwtStInfo.HdcEnabledBySwt == 0)
        {
            u8HDCLampRequest = HDC_LAMP_OFF; /* Off */
        }
        else
        {
            if(bHDCOn == TRUE)
            {
                if(gEEM_LampInfo.u16HDCActOnTime < (HDC_ACT_LAMP_ON_TIME / EEM_CYCLETIME)) /* cycletime define */
                {
                    gEEM_LampInfo.u16HDCActOnTime++;
                }
                else
                {
                    u8HDCLampRequest = HDC_LAMP_ACTIVE; /* Active */
                    gEEM_LampInfo.u16HDCActOffTime = (HDC_ACT_LAMP_OFF_TIME / EEM_CYCLETIME);
                }
            }
            else if(gEEM_LampInfo.u16HDCActOffTime > 0)
            {
                gEEM_LampInfo.u16HDCActOffTime--;
            }
            else
            {
                u8HDCLampRequest = HDC_LAMP_READY; /* Ready */
                gEEM_LampInfo.u16HDCActOnTime  = 0;
                gEEM_LampInfo.u16HDCActOffTime = 0;

            }
        }
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_HDCLampRequest = u8HDCLampRequest;    
}

static void EEM_AVHLampRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    uint8 u8AVHILampReq = AVH_I_LAMP_STATUS_OFF, u8AVHLampReq = OFF; 
    
    if((bIgnOnLampRequest == TRUE) || (bOnDiag == TRUE))
    {
        u8AVHILampReq = AVH_I_LAMP_STATUS_OFF;
        u8AVHLampReq = AVH_LAMP_IS_OFF;
    }
    else if(bIgnOn == OFF)
    {
        u8AVHILampReq = AVH_I_LAMP_STATUS_OFF;
        u8AVHLampReq = AVH_LAMP_IS_OFF;
    }
    else
    {
        #if 0
        if((gEEM_CtrlIhbInfo.bCtrlIhb_Avh == TRUE) || (gEEM_CtrlIhbInfo.bCtrlIhb_Moc == TRUE))
        {
            u8AVHILampReq = lcu1AvhILamp; /* TODO : Excel In/Out */
            u8AVHLampReq  = AVH_LAMP_IS_FAILED;
        }
        else
        {
            u8AVHILampReq = lcu1AvhILamp;            
            if(lcu1AvhLampOnReq == ON)
            {
                u8AVHLampReq = AVH_LAMP_IS_ON;
            }
            else
            {
                u8AVHLampReq = AVH_LAMP_IS_READY;
            }        
        }
        #endif
    }

    pEem_Data->Eem_MainEemLampData.Eem_Lamp_AVHILampRequest = u8AVHILampReq;
    pEem_Data->Eem_MainEemLampData.Eem_Lamp_AVHLampRequest = u8AVHLampReq;
}

static void EEM_BuzzerRequest(boolean bIgnOnLampRequest, boolean bIgnOn, boolean bOnDiag, Eem_Main_HdrBusType *pEem_Data)
{
    boolean bAhbBuzzor = OFF;

    if((gEEM_CtrlIhbInfo.bCtrlIhb_BbsDefectiveModeFail == TRUE) ||
       (gEEM_FailInfo.bLowVolt == TRUE) 
        /* || (fsu1HpaLowPrsFaultDet == 1) */ 
      )
    {
        bAhbBuzzor = ON;
    }

    pEem_Data->Eem_MainEemLampData.Eem_Buzzor_On = bAhbBuzzor;
}

#define EEM_MAIN_STOP_SEC_CODE
#include "Eem_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
