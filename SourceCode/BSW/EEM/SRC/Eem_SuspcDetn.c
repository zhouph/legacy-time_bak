/**
 * @defgroup Eem_SuspcDetn Eem_SuspcDetn
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_SuspcDetn.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_SuspcDetn.h"
#include "Eem_SuspcDetn_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define EEM_SUSPCDETN_START_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define EEM_SUSPCDETN_STOP_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_SUSPCDETN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Eem_SuspcDetn_HdrBusType Eem_SuspcDetnBus;

/* Version Info */
const SwcVersionInfo_t Eem_SuspcDetnVersionInfo = 
{   
    EEM_SUSPCDETN_MODULE_ID,           /* Eem_SuspcDetnVersionInfo.ModuleId */
    EEM_SUSPCDETN_MAJOR_VERSION,       /* Eem_SuspcDetnVersionInfo.MajorVer */
    EEM_SUSPCDETN_MINOR_VERSION,       /* Eem_SuspcDetnVersionInfo.MinorVer */
    EEM_SUSPCDETN_PATCH_VERSION,       /* Eem_SuspcDetnVersionInfo.PatchVer */
    EEM_SUSPCDETN_BRANCH_VERSION       /* Eem_SuspcDetnVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Eem_SuspcDetnEcuModeSts;
Diag_HndlrDiagClr_t Eem_SuspcDetnDiagClrSrs;

/* Output Data Element */
Eem_SuspcDetnSwtStsFSInfo_t Eem_SuspcDetnSwtStsFSInfo;
Eem_SuspcDetnCanTimeOutStInfo_t Eem_SuspcDetnCanTimeOutStInfo;
Eem_SuspcDetnFuncInhibitVlvSts_t Eem_SuspcDetnFuncInhibitVlvSts;
Eem_SuspcDetnFuncInhibitIocSts_t Eem_SuspcDetnFuncInhibitIocSts;
Eem_SuspcDetnFuncInhibitGdSts_t Eem_SuspcDetnFuncInhibitGdSts;
Eem_SuspcDetnFuncInhibitProxySts_t Eem_SuspcDetnFuncInhibitProxySts;
Eem_SuspcDetnFuncInhibitDiagSts_t Eem_SuspcDetnFuncInhibitDiagSts;
Eem_SuspcDetnFuncInhibitFsrSts_t Eem_SuspcDetnFuncInhibitFsrSts;
Eem_SuspcDetnFuncInhibitAcmioSts_t Eem_SuspcDetnFuncInhibitAcmioSts;
Eem_SuspcDetnFuncInhibitAcmctlSts_t Eem_SuspcDetnFuncInhibitAcmctlSts;
Eem_SuspcDetnFuncInhibitMspSts_t Eem_SuspcDetnFuncInhibitMspSts;
Eem_SuspcDetnFuncInhibitNvmSts_t Eem_SuspcDetnFuncInhibitNvmSts;
Eem_SuspcDetnFuncInhibitWssSts_t Eem_SuspcDetnFuncInhibitWssSts;
Eem_SuspcDetnFuncInhibitPedalSts_t Eem_SuspcDetnFuncInhibitPedalSts;
Eem_SuspcDetnFuncInhibitPressSts_t Eem_SuspcDetnFuncInhibitPressSts;
Eem_SuspcDetnFuncInhibitRlySts_t Eem_SuspcDetnFuncInhibitRlySts;
Eem_SuspcDetnFuncInhibitSesSts_t Eem_SuspcDetnFuncInhibitSesSts;
Eem_SuspcDetnFuncInhibitSwtSts_t Eem_SuspcDetnFuncInhibitSwtSts;
Eem_SuspcDetnFuncInhibitPrlySts_t Eem_SuspcDetnFuncInhibitPrlySts;
Eem_SuspcDetnFuncInhibitMomSts_t Eem_SuspcDetnFuncInhibitMomSts;
Eem_SuspcDetnFuncInhibitVlvdSts_t Eem_SuspcDetnFuncInhibitVlvdSts;
Eem_SuspcDetnFuncInhibitSpimSts_t Eem_SuspcDetnFuncInhibitSpimSts;
Eem_SuspcDetnFuncInhibitAdcifSts_t Eem_SuspcDetnFuncInhibitAdcifSts;
Eem_SuspcDetnFuncInhibitIcuSts_t Eem_SuspcDetnFuncInhibitIcuSts;
Eem_SuspcDetnFuncInhibitMpsSts_t Eem_SuspcDetnFuncInhibitMpsSts;
Eem_SuspcDetnFuncInhibitRegSts_t Eem_SuspcDetnFuncInhibitRegSts;
Eem_SuspcDetnFuncInhibitAsicSts_t Eem_SuspcDetnFuncInhibitAsicSts;
Eem_SuspcDetnFuncInhibitCanTrcvSts_t Eem_SuspcDetnFuncInhibitCanTrcvSts;
Eem_SuspcDetnFuncInhibitBbcSts_t Eem_SuspcDetnFuncInhibitBbcSts;
Eem_SuspcDetnFuncInhibitRbcSts_t Eem_SuspcDetnFuncInhibitRbcSts;
Eem_SuspcDetnFuncInhibitArbiSts_t Eem_SuspcDetnFuncInhibitArbiSts;
Eem_SuspcDetnFuncInhibitPctrlSts_t Eem_SuspcDetnFuncInhibitPctrlSts;
Eem_SuspcDetnFuncInhibitMccSts_t Eem_SuspcDetnFuncInhibitMccSts;
Eem_SuspcDetnFuncInhibitVlvActSts_t Eem_SuspcDetnFuncInhibitVlvActSts;

uint32 Eem_SuspcDetn_Timer_Start;
uint32 Eem_SuspcDetn_Timer_Elapsed;

#define EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_SUSPCDETN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_SUSPCDETN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_SUSPCDETN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_SUSPCDETN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_SUSPCDETN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_SUSPCDETN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_SUSPCDETN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_SUSPCDETN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define EEM_SUSPCDETN_START_SEC_CODE
#include "Eem_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Eem_SuspcDetn_Init(void)
{
    /* Initialize internal bus */
    Eem_SuspcDetnBus.Eem_SuspcDetnEcuModeSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnDiagClrSrs = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnSwtStsFSInfo.FlexBrkSwtFaultDet = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitVlvSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitIocSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitGdSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitProxySts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitDiagSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitFsrSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAcmioSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAcmctlSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMspSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitNvmSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitWssSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPedalSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPressSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitRlySts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitSesSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitSwtSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPrlySts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMomSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitVlvdSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitSpimSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAdcifSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitIcuSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMpsSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitRegSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAsicSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitCanTrcvSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitBbcSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitRbcSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitArbiSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPctrlSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMccSts = 0;
    Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitVlvActSts = 0;
}

void Eem_SuspcDetn(void)
{
    Eem_SuspcDetn_Timer_Start = STM0_TIM0.U;

    /* Input */
    Eem_SuspcDetn_Read_Eem_SuspcDetnEcuModeSts(&Eem_SuspcDetnBus.Eem_SuspcDetnEcuModeSts);
    Eem_SuspcDetn_Read_Eem_SuspcDetnDiagClrSrs(&Eem_SuspcDetnBus.Eem_SuspcDetnDiagClrSrs);

    /* Process */

    /* Output */
    Eem_SuspcDetn_Write_Eem_SuspcDetnSwtStsFSInfo(&Eem_SuspcDetnBus.Eem_SuspcDetnSwtStsFSInfo);
    /*==============================================================================
    * Members of structure Eem_SuspcDetnSwtStsFSInfo 
     : Eem_SuspcDetnSwtStsFSInfo.FlexBrkSwtFaultDet;
     =============================================================================*/
    
    Eem_SuspcDetn_Write_Eem_SuspcDetnCanTimeOutStInfo(&Eem_SuspcDetnBus.Eem_SuspcDetnCanTimeOutStInfo);
    /*==============================================================================
    * Members of structure Eem_SuspcDetnCanTimeOutStInfo 
     : Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet;
     : Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet;
     : Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet;
     =============================================================================*/
    
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitVlvSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitVlvSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitIocSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitIocSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitGdSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitGdSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitProxySts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitProxySts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitDiagSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitDiagSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitFsrSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitFsrSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAcmioSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAcmioSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAcmctlSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAcmctlSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMspSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMspSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitNvmSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitNvmSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitWssSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitWssSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPedalSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPedalSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPressSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPressSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitRlySts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitRlySts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitSesSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitSesSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitSwtSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitSwtSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPrlySts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPrlySts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMomSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMomSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitVlvdSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitVlvdSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitSpimSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitSpimSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAdcifSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAdcifSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitIcuSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitIcuSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMpsSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMpsSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitRegSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitRegSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAsicSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitAsicSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitCanTrcvSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitCanTrcvSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitBbcSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitBbcSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitRbcSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitRbcSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitArbiSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitArbiSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPctrlSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitPctrlSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMccSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitMccSts);
    Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitVlvActSts(&Eem_SuspcDetnBus.Eem_SuspcDetnFuncInhibitVlvActSts);

    Eem_SuspcDetn_Timer_Elapsed = STM0_TIM0.U - Eem_SuspcDetn_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define EEM_SUSPCDETN_STOP_SEC_CODE
#include "Eem_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
