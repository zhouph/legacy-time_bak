/*
 * ErP.c
 *
 *  Created on: 2015. 1. 26.
 *      Author: jinsu.park
 */
#include <string.h>
#include "Asw_Types.h"
#include "ErP.h"
#include "ErP_DtcInformIndex.h"
#include "ErP_FsTestComplete.h"
#include "../../NVM/Hdr/NvMIf.h"
//#include "PwrM.h"
#include "CDS_ActControl.h"
//#include "EEM.h"
//#include "ApplCom.h"
//#include "Diag_ErrProc.h" //comfile for PJS

uint8 EMS1_Timeout_err,TCU1_Timeout_err1,HCU_Timeout_err1,CLU2_Timeout_err1;
uint8 EMS2_Timeout_err,EMS2_Timeout_err1;
struct ERROR_FLAG *EF,*FsTest;
ERROR_FLAG_COUNTER error_counter;
uint8  error_byte[ERROR_BYTE_NO];
uint8  dup_error_byte[ERROR_BYTE_NO];
uint8  feu8TestComplete[ERROR_BYTE_NO];
ErrHandle_t    ErrHandle;
uint8            number_of_error;
uint8            feu8ErrBufLastAddr;
ErrStatus_t         ErrStatus;
REPRESENT_ERROR_FLAG  REF_ERROR_FLG;
uint8            sector_cnt;
ReferWssErr_t  WssErrRefer;
ReferActErr_t  ActErrRefer;
ReferErr1_t    ErrRefer1;
uint8            fu8mec;
fu1CLogicBit_t              fu1CLogicBit;
ErrFlg_t     ErrFlg;
fsTempFlg2_t                fsTempFlg2;
FSCanSASMsg_t           FSCanSASMsg;
IGNCheck_t      fbIGNCheck;
fu1SenSwSuspcs_t		fu1SenSwSuspcs;
uchar16_t           feu16DtcEepaddr;
uint16_t            feu16AccessPointer;
uchar8_t            feu8ErrSectorCnt;

#define FE_mSetBit(var,bit)      ((var) |= (uint8)(0x01 << (bit)))
#define FE_mClearBit(var,bit)    ((var) &= (uint8)(~(0x01 << (bit))))


void Diag_5ms_ErrorHandle(void);
static void FE_vLoadErrorCode(void);
void FE_vArrangeErrorCode(UINT u16MandoDtc);
static UCHAR FE_u8CountingDTC(void);
static void FE_vArrangeHMCDTC(UINT u16MandoDtc);
/*static*/ void FE_vWriteErrorCode(UINT e_code, UCHAR u8DtcFtb);
static void FE_vSetHMCDTCInform(UCHAR SortCnt);
static void FE_vArrangeAutoRecoveryStatus(void);
static void FE_vGetHistoryInformation(UCHAR e_code);
/*static*/ void FE_vClearAllError(void);
static void FE_vReferErrorByte(void);
static void FE_vUpdateError(void);
static void FE_vClearError(UCHAR e_code, UCHAR Ftb);
static void FE_vSortingErrPointer(UCHAR SortAddr, UCHAR NewAddr);
static void FE_vClearAllErrorFlag(void);
static void F_vClearAllErrorVariable(void);
static void FE_vErrVariableClear(void);
static void FE_vInitializeErrorData(void);
static void FE_vCheckEepromErrBufferData(void);
static void FE_vCheckEepromErrPointerData(void);
void Diag_5ms_FaultDetect(void);
/*=================================================================================
  Function : void FE_vLoadErrorCode(void)
===================================================================================
  Description : 초기 IGN ON 시 EEPROM의 Error code 정보 및 SOD, FTB를 읽어옴.
=================================================================================*/
static void FE_vLoadErrorCode(void)
{
    UCHAR i=0;
    /* UCHAR j=0; */

    for(i=0;i<U8NUM_MAX_ERROR_NUM;i++)
    {
        if(feu8ErrorPointer[i]!=0xFF)
        {
                switch(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_ERRCODE])
                {
                    case U8_FL_AIRGAP:
                    case U8_FR_AIRGAP:
                    case U8_RL_AIRGAP:
                    case U8_RR_AIRGAP:
                    case U8_FL_SPD_JUMP:
                    case U8_FR_SPD_JUMP:
                    case U8_RL_SPD_JUMP:
                    case U8_RR_SPD_JUMP:
                        FE_mSetBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],7); /* MIL ON */
                        break;

                    case U8_G_SEN_SIGNAL_FAIL:
                        FE_mSetBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],7); /* MIL OFF */
                        break;

                    /*============================================================
                    fmvss126 eep handler
                    =============================================================*/
                    case U8_YAW_LG_SIGNAL_FAIL:
                    case U8_STR_SIGNAL_FAIL:
                    case U8_MCP_SIGNAL_FAIL:
                    case U8_MOTOR_FAIL:
                    case U8_BLS_FAIL:
                        FE_mSetBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],7); /* MIL ON */ /*DTC 부가정보 항목중 W/Lamp : On / Off 여부 기록부분.*/
                        break;
                    default:
                        FE_mClearBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],7); /* MIL OFF */
                        break;
                }
                FE_mClearBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],6); /* History */
                FE_mSetBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],5);   /* History */
                FE_mSetBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],4);   /* Test Not Complete */
        }
        else
        {
            ;
        }
    }
}


void ErP_Init(void)
{
    uint8 i=0, j=0;

    EF = (struct ERROR_FLAG*) &error_byte;
    FsTest = (struct ERROR_FLAG*) &feu8TestComplete;

    error_byte[ECE_ERROR_BYTE_1]=weu8CopyEepBuff[ECE_ERROR_BYTE_1];
    error_byte[ECE_ERROR_BYTE_0]=weu8CopyEepBuff[ECE_ERROR_BYTE_0];

    /* Clear Error Bytes */
    for(i=0;i<ERROR_BYTE_NO;i++)
    {
        error_byte[i]=0xff;

        if(clear_all_flg==1)
        {
                feu8TestComplete[i]=0;

            if(i<U8NUM_MAX_ERROR_NUM)
            {
                for(j=0;j<U8NUM_DTC_INFORM;j++)
                {
                    if(j==U8_DTC_INFORM_SOD) /* SOD Byte */
                    {
                        ErrBuffer[i].ErrMember[j]=0x10; /* Test Not Complete */
                    }
                    else
                    {
                        ErrBuffer[i].ErrMember[j]=0;
                    }
                }

                feu8ErrorPointer[i]=0xff;
            }
            else
            {
                ;
            }
            number_of_error=0;
        }
        else
        {
            ;
        }
    }

    if(clear_all_flg==0)
    {
        FE_vLoadErrorCode();
    }
    else
    {
        ;
    }


    asic_error_flg=1;
    ecu_error_flg =1;
    BLS_error_flg =1;
        str_sw_err_flg=1;
        yaw_sw_err_flg=1;
        lg_sw_err_flg =1;
        mp_sw_err_flg =1;
        str_hw_err_flg=1;
        yaw_hw_err_flg=1;
        lg_hw_err_flg =1;
        mp_hw_err_flg =1;
      //  fu1FailsafeCheckEnable=1; /*Failsafe Check Loop Enable*/
    fu8mec = 0xFF; /* 0xFF means "Not Init" */
	//EF = (struct ERROR_FLAG*) &weu8CopyEepBuff;
}

void Diag_5ms_ErrorHandle(void)
{
    number_of_error=FE_u8CountingDTC();

    //FE_vGetGsnsrInform();

    if((((error_byte[ECU_ERROR_BYTE_0]&0xff)!=0xff))||
        (error_byte[ECU_ERROR_BYTE_1]!=0xff)
    )
    {
        ecu_error_flg=0;
    }
    else
    {
        ecu_error_flg=1;
    }

    if((error_byte[ASIC_ERROR_BYTE_0]!=0xff)||(error_byte[ASIC_ERROR_BYTE_1]!=0xff))
    {
        asic_error_flg=0;
    }
    else
    {
        asic_error_flg=1;
    }


        if( (feu1AX_SW_Noise_err_flg==0)||
            (feu1AX_SW_StandOffset_err_flg==0)||
            (feu1AX_SW_ShortTermDrvingOffset_err_flg==0)||
            (feu1AX_SW_LongTermDrvingOffset_err_flg==0)||
            (feu1AX_SW_Stick_err_flg==0)
          )
        {
         //   g_ece_flg=0;
        }
        else
        {
            ;
        }

        if(fu1TemperatureErrDet==1)
        {
            if(tc_temp_error_flg==1)
            {
                FE_vArrangeErrorCode(U16_TEMPERATURE_ERR);
            }
            else
            {
                ;
            }
            tc_temp_error_flg=0;
        }
        else
        {
            //feu1TcTempTestFlg=1;
        }




    if(((error_byte[CAN_ERROR_BYTE_0]|0x80)!=0xff)||(error_byte[CAN_ERROR_BYTE_1]!=0xff)||(error_byte[CAN_ERROR_BYTE_2]!=0xff)
        ||(error_byte[CAN_ERROR_BYTE_3]!=0xff)||(error_byte[CAN_ERROR_BYTE_4]!=0xff)||(error_byte[CAN_ERROR_BYTE_5]!=0xff)
        ||(CAN_hw_err_flg==0)

    ) /* CAN ERROR DETECT */
    {
        can_error_flg=1;
    }
    else
    {
        can_error_flg=0;
    }

    if(error_byte[BLS_ERROR_BYTE_0] !=0xff)
    {
        BLS_error_flg=0;
    }
    else
    {
        BLS_error_flg=1;
    }
#if 0
    #if __VDC==ENABLE
        /* BUS OFF??, HMC인 경우??*/
        if(feu1IMU_HW_Line_err_flg==0)
        {
            yaw_hw_err_flg=0;
            lg_hw_err_flg=0;
        }
        else
        {
            yaw_hw_err_flg=1;
            lg_hw_err_flg=1;
        }


        #if __STEER_SENSOR_TYPE==CAN_TYPE

            if(((error_byte[STEER_ERROR_BYTE_0]|0x01)!=0xff)||(error_byte[STEER_ERROR_BYTE_1]!=0xff))
            {
                str_sw_err_flg=0;
            }
            else
            {
                str_sw_err_flg=1;
            }

            if(feu1SAS_HW_Line_err_flg==0)
            {
                str_hw_err_flg=0;
            }
            else
            {
                str_hw_err_flg=1;
            }
        #endif

        if((error_byte[YAW_ERROR_BYTE_0]!=0xff)||((error_byte[YAW_ERROR_BYTE_1]|MASK_YAW_ERROR_BYTE_1)!=0xff)||((error_byte[IMU_ERROR_BYTE]|1)!=0xff))
        {
            yaw_sw_err_flg=0;
        }
        else
        {
            yaw_sw_err_flg=1;
        }

        if(((error_byte[AY_ERROR_BYTE_0]|MASK_AY_ERROR_BYTE_0)!=0xff)||(error_byte[AY_ERROR_BYTE_1]!=0xff)||((error_byte[IMU_ERROR_BYTE]|1)!=0xff))
        {
            lg_sw_err_flg=0;
        }
        else
        {
            lg_sw_err_flg=1;
        }


        if(feu1CANSAS_SW_Calibration_err_flg==0)
        {
            vdc_error_flg=1;
        }
        else
        {
            if(fcu1LWSCal==1)
            {
                /*=======================================================================
                   FE_vClearError 함수 구조 변경에 따른 호출 함수 코드 수정
                ========================================================================*/
                FE_vClearError(U8_STR_CAL_ERROR, U8_INVALID_SIGNAL);
            }
            else
            {
                ;
            }
        }


	/*============================================================
    ECE Handler
    =============================================================*/
     /***** FMVSS ECE *****/
     #if __FMVSS126_ESC_ECE==ENABLE
         if(fu1eepWriteReqSas==1)
         {
            sas_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqYaw==1)
         {
            yaw_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqAy==1)
         {
            ay_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqPedal==1)
         {
            pedal_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqHpa==1)
         {
            hpa_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqBcp1==1)
         {
            bcp1_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqBcp2==1)
         {
            bcp2_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqPsp==1)
         {
            psp_ece_flg=0;
         }
         else
         {
            ;
         }

         if(fu1eepWriteReqMotor==1)
         {
            motor_ece_flg=0;
         }
         else
         {
            ;
         }

         #if (BLS_ERROR_SET==F_ESC_ERROR_FLG_SET)||(BLS_HMC_STD==ENABLE)
         if(fu1eepWriteReqBls==1)
         {
            bls_ece_flg=0;
         }
         else
         {
            ;
         }
         #endif
     #endif

    #endif /* VDC==ENABLE */

	if((under_v_flg==1)
		&&((weu8CopyEepBuff[INPUT_VOLTAGE_ERROR_BYTE]&0x01)==1)
	)
    {
        /*=======================================================================
           FE_vClearError 함수 구조 변경에 따른 호출 함수 코드 수정
        ========================================================================*/

        FE_vClearError(U8_UNDER_POWER, U8_MIN_THRD);

    }
    else
    {
        ;
    }

    if(((error_byte[WHEEL_FL_ERROR_BYTE]!=0xff)||(error_byte[WHEEL_FR_ERROR_BYTE]!=0xff)||(error_byte[WHEEL_RL_ERROR_BYTE]!=0xff)
    ||(error_byte[WHEEL_RR_ERROR_BYTE]!=0xff))||(error_byte[WHEEL_RESERVED_ERROR_BYTE]!=0xff)||(feu1AbsLongTermErr==0))
    {
    // jjong   ece_flg=0;      /* sensor error except H/W error -> ece regulation */
    // jjong    driving_flg=0;
    }

    FE_vReferErrorByte();
#endif
     /*=======================================================================
         Error Flag에 따라 Test Complete Flag를 Clear한 후,
          	               Test 수행 여부를 확인하도록 호출 구조 변경
     ========================================================================*/

    if(number_of_error>0)
    {
        #if __CUSTOMER_DTC==HMC_DTC
        //FE_vArrangeTestStatus();
        #endif

        #if __CUSTOMER_DTC==HMC_DTC
            FE_vArrangeAutoRecoveryStatus();
        #endif
    }
    else
    {
        ;
    }

    if((weu1EraseSector==0)&&(weu1WriteSector==0))
    {
        if(clear_all_flg==1)
        {
            FE_vClearAllError();
            FE_vReferErrorByte();
        }
        else
        {
            #if (__NM_SLEEP_ENABLE==ENABLE)
                if(mbu1NormalMode==1)
                {
                    FE_vUpdateError();
                }
                else
                {
                    ;
                }
            #else
                FE_vUpdateError();
            #endif
        }
    }
}

/*=================================================================================
    Function : void FE_vClearError(UINT e_code)
===================================================================================
    Description : error buffer에서 error code를 소거하는 함수.
=================================================================================*/
static void FE_vClearError(UCHAR e_code, UCHAR Ftb)
{
    UCHAR i=0, j=0, Sort=0, u8SortAddr=0, u8DelAddr=0;

    Sort=0;

    if(e_code!=0)
    {
        for(i=0;i<U8NUM_MAX_ERROR_NUM;i++)
        {
            #if __CUSTOMER_DTC==GM_DTC
            if((ErrBuffer[i].ErrMember[U8_DTC_INFORM_ERRCODE]==e_code)&&(ErrBuffer[i].ErrMember[U8_DTC_INFORM_FTB]==Ftb))
            #elif __CUSTOMER_DTC==HMC_DTC
            if(ErrBuffer[i].ErrMember[U8_DTC_INFORM_ERRCODE]==e_code)
            #endif
            {
                for(j=0;j<U8NUM_DTC_INFORM;j++)
                {
                    ErrBuffer[i].ErrMember[j]=0;
                }

                #if __CUSTOMER_DTC==HMC_DTC
                    #if (__DIAG_TYPE==UDS_CAN_LINE)
                    ErrBuffer[i].ErrMember[U8_DTC_INFORM_SOD]=UDS_SOD_TEST_NOT_COMPLETE_THIS_OPERATION_CYCLE; /* Set: TestNotCompleteThisOprationCycle */
                    #else
                    ErrBuffer[i].ErrMember[U8_DTC_INFORM_SOD]=0x10; /* Test Not Complete */
                    #endif
                #elif __CUSTOMER_DTC==GM_DTC
                    ErrBuffer[i].ErrMember[U8_DTC_INFORM_SOD]=0x24; /* Test Not Complete Since pwr up, Test Not Complete Since Clear DTC */
                #endif

                u8SortAddr=i;
                u8DelAddr=0xFF;
                Sort=1;
                break;
            }
            else
            {
                ;
            }
        }

        if(Sort==1)
        {
            FE_vSortingErrPointer(u8SortAddr, u8DelAddr);
        }
        else
        {
            ;
        }

    }
    else
    {
        ;
    }
}

/*=================================================================================
  Function : void FE_vSortingErrPointer(UCHAR SortAddr, UCHAR NewAddr)
===================================================================================
  Description : Sorting Error Information Address Buffer
=================================================================================*/
static void FE_vSortingErrPointer(UCHAR SortAddr, UCHAR NewAddr)
{
    UCHAR i=0, ft_u8return=0;

    for(i=0;i<9;i++)
    {
        if(feu8ErrorPointer[i]==SortAddr)
        {
            for(;i<9;i++)
            {
                if(feu8ErrorPointer[i+1]!=0xff)
                {
                    feu8ErrorPointer[i]=feu8ErrorPointer[i+1];
                }
                else
                {
                    ft_u8return=1;
                    break;
                }
            }

            if(ft_u8return==1)
            {
                break;
            }
        }
        else
        {
            ;
        }
    }
    if(i>9)
    {
        i=9;
    }


    feu8ErrorPointer[i]=NewAddr;
}

/*=================================================================================*/
/*  함수  void FE_vUpdateError(void)                                               */
/*=================================================================================*/
/*  설명: eeprom update 함수.                                                      */
/*=================================================================================*/
static void FE_vUpdateError(void)
{

#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
	uint16_t u16Temp1 = 0x0000;
	uint16_t u16Temp2 = 0x0000;
	uint16_t u16TempMerge = 0x0000;
#endif


    UCHAR i,u8ErrTemp1,u8ErrTemp2;

    UCHAR u8ErrTemp4;



    UCHAR u8SuppErrEep[4] = { 0, 0, 0, 0 };


    u8ErrTemp1=0;
    u8ErrTemp2=error_byte[INPUT_VOLTAGE_ERROR_BYTE];
    if((under_v_flg==0)&&(fbu1UnderVoltErrUpdate==0))
    {

            error_byte[INPUT_VOLTAGE_ERROR_BYTE]|=0x05;

        u8ErrTemp1=0xff;
    }
    else
    {
        ;
    }



        u8ErrTemp4=error_byte[STEER_ERROR_BYTE_0];

        if(feu1CANSAS_SW_Calibration_err_flg==0)
        {
            error_byte[STEER_ERROR_BYTE_0]|=0x02;
        }
        else
        {
            ;
        }


    for(i=0;i<=(ERROR_BYTE_NO-4);i+=4)
    {
        if(feu1WriteDupErrToEepReq == 0)
        {
            weu16EepBuff[0]=*(UINT*)&weu8CopyEepBuff[i] & *(UINT*)&error_byte[i];
            weu16EepBuff[1]=*(UINT*)&weu8CopyEepBuff[i+2] & *(UINT*)&error_byte[i+2];
        }
        else
        {
            *(UINT*)&dup_error_byte[i]=*(UINT*)&dup_error_byte[i] & *(UINT*)&error_byte[i];
            *(UINT*)&dup_error_byte[i+2]=*(UINT*)&dup_error_byte[i+2] & *(UINT*)&error_byte[i+2];

            weu16EepBuff[0]=*(UINT*)&weu8CopyEepBuff[i] & *(UINT*)&dup_error_byte[i];
            weu16EepBuff[1]=*(UINT*)&weu8CopyEepBuff[i+2] & *(UINT*)&dup_error_byte[i+2];
        }

        if((*(UINT*)&weu8CopyEepBuff[i]!=weu16EepBuff[0])||(*(UINT*)&weu8CopyEepBuff[i+2]!=weu16EepBuff[1]))
        {
            weu1WriteSector=1;


            if(fu8mec == 0) /* After EOL, Write Supplier DTC */
            {
                if(feu1WriteDupErrToEepReq == 0)
                {
                    /* Write Error To Ram */
                    if (((*(UINT*)&error_byte[i]) != (*(UINT*)&weu8CopySuppEepBuff[i]))
                        || ((*(UINT*)&error_byte[i+2]) != (*(UINT*)&weu8CopySuppEepBuff[i+2])))
                    {
                        (*(UINT*)&weu8CopySuppEepBuff[i])   = (*(UINT*)&weu8CopySuppEepBuff[i]) & (*(UINT*)&error_byte[i]);
                        (*(UINT*)&weu8CopySuppEepBuff[i+2]) = (*(UINT*)&weu8CopySuppEepBuff[i+2]) & (*(UINT*)&error_byte[i+2]);
                        feu1WriteSuppDtcToEepReq=1;
                    }
                }
                else
                {
                    /* Write Error To Ram */
                    if (((*(UINT*)&dup_error_byte[i]) != (*(UINT*)&weu8CopySuppEepBuff[i]))
                        || ((*(UINT*)&dup_error_byte[i+2]) != (*(UINT*)&weu8CopySuppEepBuff[i+2])))
                    {
                        (*(UINT*)&weu8CopySuppEepBuff[i])   = (*(UINT*)&weu8CopySuppEepBuff[i]) & (*(UINT*)&dup_error_byte[i]);
                        (*(UINT*)&weu8CopySuppEepBuff[i+2]) = (*(UINT*)&weu8CopySuppEepBuff[i+2]) & (*(UINT*)&dup_error_byte[i+2]);
                        feu1WriteSuppDtcToEepReq=1;
                    }
                }
            }


            break;
        }
        else
        {
            ;
        }
    }

    if(weu1WriteSector == 0)
    {
        feu1WriteDupErrToEepReq = 0;
    }

    if(weu1WriteSector==1)
    {

            weu16Eep_addr=(uint16_t)(U16_EEP_ERROR_CODE_ADDR+i);

        *(UINT*)&weu8CopyEepBuff[i]  =weu16EepBuff[0];
        *(UINT*)&weu8CopyEepBuff[i+2]=weu16EepBuff[1];
    }

    else if(feu1WriteSuppDtcToEepReq==1)
    {
        for(i=0;i<ERROR_BYTE_NO;i+=4)
        {
            if (WE_u8ReadEEP_Byte((uint16_t)(U16_EEP_ERROR2_CODE_ADDR + i), u8SuppErrEep, 4) == 1)
            {
                if(((*(UINT*)&weu8CopySuppEepBuff[i])!=(*(UINT*)&u8SuppErrEep[0]))
                    ||((*(UINT*)&weu8CopySuppEepBuff[i+2])!=(*(UINT*)&u8SuppErrEep[2])))
                {
                    break;
                }
            }
        }

        if(i<ERROR_BYTE_NO)
        {

                weu16Eep_addr=(uint16_t)(U16_EEP_ERROR2_CODE_ADDR+i);


            weu16EepBuff[0]=*(UINT*)&weu8CopySuppEepBuff[i];
            weu16EepBuff[1]=*(UINT*)&weu8CopySuppEepBuff[i+2];

            weu1WriteSector=1;
        }
        else
        {
            feu1WriteSuppDtcToEepReq=0;
        }
    }
    else
    {
        ;
    }

    if((UCHAR)u8ErrTemp1==0xff)
    {
        error_byte[INPUT_VOLTAGE_ERROR_BYTE]=u8ErrTemp2;
    }
    else
    {
        ;
    }


        error_byte[STEER_ERROR_BYTE_0]=u8ErrTemp4;



}

/*=================================================================================
  Function : void FE_vReferErrorByte(void)
===================================================================================
  Description : Error Status Reference Flag Set
=================================================================================*/
static void FE_vReferErrorByte(void)
{
    if((error_byte[WHEEL_FL_ERROR_BYTE]==0xFF)&&(feu1AbsLongTermErr==1)&&(fl_WheelTypeSwap_Err_flg==1))
    {
        feu1FlWssSigErrFlg=0;
    }
    else
    {
        feu1FlWssSigErrFlg=1;
    }

    if((error_byte[WHEEL_FR_ERROR_BYTE]==0xFF)&&(feu1AbsLongTermErr==1)&&(fr_WheelTypeSwap_Err_flg==1))
    {
        feu1FrWssSigErrFlg=0;
    }
    else
    {
        feu1FrWssSigErrFlg=1;
    }

    if((error_byte[WHEEL_RL_ERROR_BYTE]==0xFF)&&(feu1AbsLongTermErr==1)&&(rl_WheelTypeSwap_Err_flg==1))
    {
        feu1RlWssSigErrFlg=0;
    }
    else
    {
        feu1RlWssSigErrFlg=1;
    }

    if((error_byte[WHEEL_RR_ERROR_BYTE]==0xFF)&&(feu1AbsLongTermErr==1)&&(rr_WheelTypeSwap_Err_flg==1))
    {
        feu1RrWssSigErrFlg=0;
    }
    else
    {
        feu1RrWssSigErrFlg=1;
    }

    if((feu1FlWssSigErrFlg==0)&&(fl_hw_open_flg==1)&&(fl_hw_short_flg==1)&&(fl_hw_overtemp_flg==1)&&(fl_lkg_flg==1))
    {
        feu1FlWssErrFlg=0;
    }
    else
    {
        feu1FlWssErrFlg=1;
    }

    if((feu1FrWssSigErrFlg==0)&&(fr_hw_open_flg==1)&&(fr_hw_short_flg==1)&&(fr_hw_overtemp_flg==1)&&(fr_lkg_flg==1))
    {
        feu1FrWssErrFlg=0;
    }
    else
    {
        feu1FrWssErrFlg=1;
    }

    if((feu1RlWssSigErrFlg==0)&&(rl_hw_open_flg==1)&&(rl_hw_short_flg==1)&&(rl_hw_overtemp_flg==1)&&(rl_lkg_flg==1))
    {
        feu1RlWssErrFlg=0;
    }
    else
    {
        feu1RlWssErrFlg=1;
    }

    if((feu1RrWssSigErrFlg==0)&&(rr_hw_open_flg==1)&&(rr_hw_short_flg==1)&&(rr_hw_overtemp_flg==1)&&(rr_lkg_flg==1))
    {
        feu1RrWssErrFlg=0;
    }
    else
    {
        feu1RrWssErrFlg=1;
    }

    if((error_byte[SOLENOID_FRONT_ERROR_BYTE]==0xFF)&&((error_byte[SOLENOID_RESERVED_ERROR_BYTE_1]&0x0F)==0x0F)&&
        ((error_byte[SOLENOID_RESERVED_ERROR_BYTE_2]&0x0F)==0x0F))
    {
        feu1FrontSolErrFlg=0;
    }
    else
    {
        feu1FrontSolErrFlg=1;
    }

    if((error_byte[SOLENOID_REAR_ERROR_BYTE]==0xFF)&&((error_byte[SOLENOID_RESERVED_ERROR_BYTE_1]&0xF0)==0xF0)&&
         ((error_byte[SOLENOID_RESERVED_ERROR_BYTE_2]&0xF0)==0xF0))
    {
        feu1RearSolErrFlg=0;
    }
    else
    {
        feu1RearSolErrFlg=1;
    }
    /*****************AHB Valve Error 처리 ******************/
    feu1APVSolenoidErrFlg =0;
    feu1RLVSolenoidErrFlg =0;
    feu1CUTSolenoidErrFlg =0;
    feu1SIMSolenoidErrFlg =0;

	if((error_byte[SOLENOID_APV_RLV_ERROR_BYTE_0]&0x0F)!=0x0F)
	{
	   feu1APVSolenoidErrFlg=1;
	}

    if((error_byte[SOLENOID_APV_RLV_ERROR_BYTE_0]&0xF0)!=0xF0)
	{
	   feu1RLVSolenoidErrFlg=1;
	}

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_5]&0x0F)!=0x0F)
	{
	   feu1APVSolenoidErrFlg=1;
	}

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_5]&0xF0)!=0xF0)
	{
	   feu1RLVSolenoidErrFlg=1;
	}

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_6]&0x0F)!=0x0F)
	{
	   feu1APVSolenoidErrFlg=1;
	}

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_6]&0xF0)!=0xF0)
	{
	   feu1RLVSolenoidErrFlg=1;
	}

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_3]&0x4F)!=0x4F)
    {
       feu1CUTSolenoidErrFlg=1;
    }

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_3]&0x30)!=0x30)
    {
       feu1SIMSolenoidErrFlg=1;
    }

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_4]&0x0F)!=0x0F)
    {
        feu1CUTSolenoidErrFlg=1;
    }

    if((error_byte[SOLENOID_RESERVED_ERROR_BYTE_4]&0x70)!=0x70)
    {
        feu1SIMSolenoidErrFlg=1;
    }
    /***********************************************************/

    if((feu1FrontSolErrFlg==1)||(feu1RearSolErrFlg==1))  /*Need Correction*/
    {
        feu1AbsSolenoidErrFlg=1;
    }
    else
    {
        feu1AbsSolenoidErrFlg=0;
    }

	if((feu1APVSolenoidErrFlg == 1) || (feu1RLVSolenoidErrFlg == 1)
	 || (feu1CUTSolenoidErrFlg == 1) || (feu1SIMSolenoidErrFlg == 1))
	{
	    feu1AhbSolenoidErrFlg=1;
	}
	else
	{
	    feu1AhbSolenoidErrFlg=0;
	}

    if((feu1AbsSolenoidErrFlg == 1)||(feu1AhbSolenoidErrFlg == 1))
    {
        feu1SolenoidErrFlg=1;
    }
    else
    {
        feu1SolenoidErrFlg=0;
    }

	if((vr_bypass_flg==1)&&(vr_open_flg==1)&&(vr_short_g_flg==1)&&(vr_lkg_flg==1)&&(abs_vr_shutdown_flg==1)&&(feu1BATT1FuseOpen==1)&&(abs_vr_oc_flg==1))
    {
        feu1VrErrFlg=0;
    }
    else
    {
        feu1VrErrFlg=1;
    }

	if((ahb_vr_oc_flg==1)&&(ahb_vr_open_flg==1)/*&&(ahb_vr_shutdown_flg==1)*/&&(feu1BATT1FuseOpen==1))
    {
        feu1AhbVrErrFlg=0;
    }
    else
    {
        feu1AhbVrErrFlg=1;
    }

	if((motor_fuse_open_flg==1)&&(motor_short_flg==1)&&(motor_open_flg==1)
	&&(mr_open_flg==1)&&(motor_lock_flg==1)&&(motor_lsd_open_flg==1)&&(motor_lsd_short_flg==1)
	&& (VBB_Power_error_flg==1)
    #if __ECU==ESP_ECU_1
    	&&(motor_overtemp_flg==1)
    #endif
	)
    {
        feu1MotErrFlg=0;
    }
    else
    {
        feu1MotErrFlg=1;
    }

	feu1MotErrFlg = 0; //msw

    if((feu1VrErrFlg==0)&&(feu1MotErrFlg==0)&&(ecu_error_flg==1)
    &&(over_v_flg==1)&&(higher_v_flg==1)&&(feu1BATT1FuseOpen==1))
    {
        feu1VrOnEnaFlg=1;
    }
    else
    {
        feu1VrOnEnaFlg=0;
    }

    if((feu1VrOnEnaFlg==1)&&(under_v_flg==1)&&(low_v_flg==1)&&(feu1AbsSolenoidErrFlg==0)&&(feu1AhbSolenoidErrFlg==0))
    {
        feu1DiagVrOnEnaFlg=1;
    }
    else
    {
        feu1DiagVrOnEnaFlg=0;
    }

    if((under_v_flg==1)&&(low_v_flg==1))
    {
        feu1UnderPwrFlg=0;
    }
    else
    {
        feu1UnderPwrFlg=1;
    }
}

/*=================================================================================*/
/*  함수  void FE_vClearAllError(void)                                              */
/*=================================================================================*/
/*  설명: error buffer와 eeprom의 error를 모두 소거하는 함수.                      */
/*=================================================================================*/
/*static*/ void FE_vClearAllError(void)
{
    uint8_t Temp_Return_Value=0;
    if(sector_cnt<ERROR_BYTE_NO)
    {
            weu16Eep_addr=(uint16_t)(U16_EEP_ERROR_CODE_ADDR+sector_cnt);


        weu1EraseSector=1;
        sector_cnt+=4;
    }

    else if((sector_cnt<(ERROR_BYTE_NO*2)) && (cdu1ClearDtcSupplier==1))
    {

                weu16Eep_addr=(uint16_t)(U16_EEP_ERROR2_CODE_ADDR+(sector_cnt-ERROR_BYTE_NO));


        weu1EraseSector=1;
        sector_cnt+=4;
    }

    else
    {
        if(WE_u8ReadEEP_Byte(U16_EEP_ERROR_CODE_ADDR,&weu8CopyEepBuff[0],ERROR_BYTE_NO)==1) /* copy eeprom */
        {

            if (cdu1ClearDtcSupplier == 1)
            {
                Temp_Return_Value=WE_u8ReadEEP_Byte(U16_EEP_ERROR2_CODE_ADDR,&weu8CopySuppEepBuff[0],ERROR_BYTE_NO);
            }

            FE_vClearAllErrorFlag();

            clear_all_flg=0;
            cdu1ClearDtcSupplier = 0;

            sector_cnt=0;
        }
    }
}


/*=================================================================================*/
/*  함수  void FE_vClearAllError(void)                                              */
/*=================================================================================*/
/*  설명: error buffer와 eeprom의 error를 모두 소거하는 함수.                      */
/*=================================================================================*/
static void FE_vClearAllErrorFlag(void)
{
    F_vClearAllErrorVariable(); /* Clear All Error Count */

    FE_vReferErrorByte();

    //F_vActErrorClear(); Compile PJS

    snsr_2_error_flg=0;
    fbu1UnderVoltErrUpdate=0;
    abs_error_flg=0;
    ebd_error_flg=0;
#if (U1_VOLTAGE_FLUCTUATION_DETECTION==ENABLE)
    fbu1LowVoltErrUpdate=0;
    /*DTC Clear시 fbu1LowVoltErrUpdate변수가 Clear되어야 저전압 DTC Clear 됨*/
#endif

#if ((defined(FS_VARIANT_CODE_ENABLE))||(defined(FS_INLINE_CODING_ENABLE)))
    ccu1VariantErrSetFlg=0;
#endif

#if __BTC==ENABLE || __TCS==ENABLE
    tcs_error_flg=0;
#endif

#if __VDC==ENABLE
    #if __STEER_SENSOR_TYPE==ANALOG_TYPE
        if(fu1StrErrorDet==1)
        {
            FAA_vSteerCenterReset();
        }
        else
        {
            ;
        }
    #endif

    vdc_error_flg=0;
#endif

    //FO_vSenPowerReset(); /* Sensor Power Reset Check */ Compile PJS

#if __HDC==ENABLE
    hdc_error_flg=0;
#endif

#if __HSA==ENABLE /* 9326 : LM P2 & MSATER */
    hsa_error_flg=0;
  //  flu1VDCLampRequestByHSA=0; /*Clear Lamp Request By HSA*/
#endif

#if __AVH==ENABLE
    avh_error_flg=0;
#endif

#if __MGH80_MOCi==ENABLE
    moc_error_flg=0;
#endif
   // sol_error_flg=0; Compile PJS
   // abs_fsr_error_flg=0; Compile PJS

   // ahb_fsr_error_flg=0; Compile PJS
   // mr_error_flg=0; Compile PJS
   // motor_error_flg=0; Compile PJS
#if (__PEDAL_SENSOR_TYPE!=NONE)&&(__USE_PDT_SIGNAL==ENABLE)
   // fsu1PdtSenFaultDet=0; Compile PJS
#endif

    feu8ErrBufLastAddr=0;
    feu1FullErrBuffFlg=0;
    feu1ClearDtcFlg=1;

#if GMLAN_ENABLE==ENABLE
    W_vClearCanError(1);
    CC_vClearProgErrData();
#else
    #if __CAN_MSG_CHK_ENABLE==1
        CC_vClearCanError(1,3); /*Daul CAN 적용*/
    #endif
#endif
        CC_vClearCanError(1,3); /*Daul CAN 적용*/


#if __CUSTOMER_DTC==GM_DTC
    FE_vClearTestFlgSpu();
#endif

    #if __GEAR_SWITCH_TYPE==ANALOG_TYPE
        fsu1GearRSwitch_error_flg=0;
    #endif

    #if __CLUTCH_SWITCH_TYPE==ANALOG_TYPE
        fsu1ClutchSwitch_error_flg=0;
    #endif
}

static void F_vClearAllErrorVariable(void)
{
    //FW_vWssVariableClear(); Compile PJS
    //FL_vLampVariableClear(); Compile PJS
    //FB_vIgnVariableClear(); Compile PJS
    //FR_vValveVariableClear(); Compile PJS
    //FT_vMotorVariableClear(); Compile PJS
    //FV_vSolAtvVariableClear(); Compile PJS
    //FV_vSolPsvVariableClear(); Compile PJS
    //FO_vSenPwrVariableClear(); Compile PJS
	//FS_vChkPwrVariableClear(); Compile PJS
    //FI_vAsicVariableClear(); Compile PJS
    //F_vVariableClear(); Compile PJS

	//EEM_ClearAllError();

    FE_vErrVariableClear();
    #if (__NM_SLEEP_ENABLE==ENABLE)
    FE_vClearVarByPwrMd();
	#endif
    #if (__4WD_VEHICLE==ENABLE) || (__G_SENSOR_TYPE!=NONE)
    //FG_vAxVariableClear(); Compile PJS
    #endif

	/*===============================================================
      REVISION#3 : 매크로 명칭 변경
    =================================================================*/
    #if __VACUUM_SENSOR_ENABLE
    FC_vVacuumVariableClear();
    #endif

    #if (__PEDAL_SENSOR_TYPE!=NONE)
    //FC_vPedalVariableClear(); Compile PJS
    #endif

    #if __ECU==ESP_ECU_1
    //FP_vMcpBlsSwVariableClear(); Compile PJS
    //FP_vMcpHwVariableClear(); Compile PJS
    #if __STEER_SENSOR_TYPE==ANALOG_TYPE
    FAA_vSteerVariableClear();
    FAA_vStrHwVariableClear();
    #elif __STEER_SENSOR_TYPE==CAN_TYPE
    //FAD_vStrVariableClear(); Compile PJS
    //FAD_vStrHwVariableClear(); Compile PJS
    #endif
    //FS_vSwitchVariableClear(); Compile PJS
    //FY_vLgYawVariableClear(); Compile PJS
    //FY_vLgYawHwVariableClear(); Compile PJS
    #endif
}

static void FE_vErrVariableClear(void)
{
    asic_error_flg=1;
    ecu_error_flg =1;
    BLS_error_flg =1;
    #if __ECU==ESP_ECU_1
    str_sw_err_flg=1;
    yaw_sw_err_flg=1;
    lg_sw_err_flg =1;
    mp_sw_err_flg =1;
    str_hw_err_flg=1;
    yaw_hw_err_flg=1;
    lg_hw_err_flg =1;
    mp_hw_err_flg =1;
    #endif

    abs_error_flg=0;
    ebd_error_flg=0;
    tcs_error_flg=0;
    vdc_error_flg=0;
    can_error_flg=0;
    hdc_error_flg=0;
    cbs_error_flg=0;
    #if __AVH==ENABLE
    avh_error_flg=0;
    #endif
    #if __MGH80_MOCi==ENABLE
    moc_error_flg=0;
    #endif

    snsr_2_error_flg=0;

    FE_vInitializeErrorData();
    (void)memset(feu8TestComplete,0x00,ERROR_BYTE_NO);
}


/*=================================================================================
    Function : void FE_vInitializeErrorData(void)
===================================================================================
    Description : error_buffer update and error_byte clear.
=================================================================================*/
static void FE_vInitializeErrorData(void)
{
    UCHAR i=0, j=0;

    EF = (struct ERROR_FLAG*) &error_byte;
    FsTest = (struct ERROR_FLAG*) &feu8TestComplete;

    error_byte[ECE_ERROR_BYTE_1]=weu8CopyEepBuff[ECE_ERROR_BYTE_1];
    if(ece_flg==1)
    {
    //    ee_ece_flg=0;
    }
    else
    {
    //    ee_ece_flg=1;
    }

    /*============================================================
    fmvss126 eep handler
    =============================================================*/
    #if (__G_SENSOR_TYPE!=NONE) || (__FMVSS126_ESC_ECE==ENABLE)
    error_byte[ECE_ERROR_BYTE_0]=weu8CopyEepBuff[ECE_ERROR_BYTE_0];
    #endif

    #if __G_SENSOR_TYPE!=NONE
    if(g_ece_flg==0)
    {
    //    ee_g_ece_flg=1;
    }
    else
    {
    //    ee_g_ece_flg=0;
    }

    ee_g_ece_flg = 0;//jjong
    #endif
    /*============================================================
    fmvss126 add 9701 by kkk
    : eep handler
    =============================================================*/
    #if 0 // __FMVSS126_ESC_ECE==ENABLE
        /***** FMVSS ECE *****/
        if(yaw_ece_flg==0)
        {
            ee_yaw_ece_flg=1;  /* pre-cycle error detected */
        }
        else
        {
            ee_yaw_ece_flg=0;
        }

        if(ay_ece_flg==0)
        {
            ee_ay_ece_flg=1;
        }
        else
        {
            ee_ay_ece_flg=0;
        }

        if(sas_ece_flg==0)
        {
            ee_sas_ece_flg=1;
        }
        else
        {
            ee_sas_ece_flg=0;
        }

        if(motor_ece_flg==0)
        {
            ee_motor_ece_flg=1;
        }
        else
        {
            ee_motor_ece_flg=0;
        }

        if(pedal_ece_flg==0)
        {
            ee_pedal_ece_flg=1;
        }
        else
        {
            ee_pedal_ece_flg=0;
        }

        if(hpa_ece_flg==0)
        {
            ee_hpa_ece_flg=1;
        }
        else
        {
            ee_hpa_ece_flg=0;
        }

        if(bcp1_ece_flg==0)
        {
            ee_bcp1_ece_flg=1;
        }
        else
        {
            ee_bcp1_ece_flg=0;
        }

        if(bcp2_ece_flg==0)
        {
            ee_bcp2_ece_flg=1;
        }
        else
        {
            ee_bcp2_ece_flg=0;
        }

        if(psp_ece_flg==0)
        {
            ee_psp_ece_flg=1;
        }
        else
        {
            ee_psp_ece_flg=0;
        }

        #if defined(U1_FM_GEAR_R_SWITCH_ECE_ENABLE)
        if(gearR_ece_flg==0)
        {
            ee_gearR_ece_flg=1;
        }
        else
        {
            ee_gearR_ece_flg=0;
        }
        #endif

        #if defined(U1_FM_CLUTCH_SWITCH_ECE_ENABLE)
        if(clutch_ece_flg==0)
        {
            ee_clutch_ece_flg=1;
        }
        else
        {
            ee_clutch_ece_flg=0;
        }
        #endif

        #if (BLS_ERROR_SET==F_ESC_ERROR_FLG_SET)||(BLS_HMC_STD==ENABLE)
        if(bls_ece_flg==0)
        {
            ee_bls_ece_flg=1;
        }
        else
        {
            ee_bls_ece_flg=0;
        }
        #endif

        #if defined(U1_FM_VDC_SWITCH_ECE_ENABLE)
        if(vdc_switch_ece_flg==0)
        {
            ee_vdc_switch_ece_flg=1;
        }
        else
        {
            ee_vdc_switch_ece_flg=0;
        }
        #endif

        #if defined(U1_FM_HDC_SWITCH_ECE_ENABLE)
        if(hdc_switch_ece_flg==0)
        {
            ee_hdc_switch_ece_flg=1;
        }
        else
        {
            ee_hdc_switch_ece_flg=0;
        }
        #endif
    #endif

    /* Clear Error Bytes */
    for(i=0;i<ERROR_BYTE_NO;i++)
    {
        error_byte[i]=0xff;

        if(clear_all_flg==1)
        {
            #if __CUSTOMER_DTC==HMC_DTC
                feu8TestComplete[i]=0;
            #endif

            if(i<U8NUM_MAX_ERROR_NUM)
            {
                for(j=0;j<U8NUM_DTC_INFORM;j++)
                {
                    if(j==U8_DTC_INFORM_SOD) /* SOD Byte */
                    {
                        #if __CUSTOMER_DTC==HMC_DTC
                        #if (__DIAG_TYPE==UDS_CAN_LINE)
                            ErrBuffer[i].ErrMember[j]=UDS_SOD_TEST_NOT_COMPLETE_THIS_OPERATION_CYCLE; /* Test Not Complete */
                        #else
                            ErrBuffer[i].ErrMember[j]=0x10; /* Test Not Complete */
                        #endif
                        #elif __CUSTOMER_DTC==GM_DTC
                            ErrBuffer[i].ErrMember[j]=0x24; /* Test Not Complete Since pwr up, Test Not Complete Since Clear DTC */
                        #endif
                    }
                    else
                    {
                        ErrBuffer[i].ErrMember[j]=0;
                    }
                }

                feu8ErrorPointer[i]=0xff;
            }
            else
            {
                ;
            }
            number_of_error=0;
        }
        else
        {
            ;
        }
    }

    if(clear_all_flg==0)
    {
        FE_vLoadErrorCode();
    }
    else
    {
        ;
    }

    #if __FLASH
        write_flash_buffer();
    #endif
}


static void FE_vArrangeAutoRecoveryStatus(void)
{
    if(over_v_flg==1)
    {
        FE_vGetHistoryInformation(U8_OVER_POWER);
    }
}


static void FE_vGetHistoryInformation(UCHAR e_code)
{
    UCHAR i=0, u8ErrCode=0, u8ErrSod=0;

    for(i=0;i<number_of_error;i++)
    {
        u8ErrCode=ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_ERRCODE];

        if(u8ErrCode==e_code)
        {
            FE_mClearBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],7); /* W/Lamp Off */
            FE_mClearBit(ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_SOD],6); /* Clear Current DTC Flag */
        }
    }
}

/*=================================================================================
  Function : void FE_vArrangeErrorCode(UINT u16MandoDtc)
===================================================================================
  Description : MANDO DTC Index에 대한 Customer DTC Index 및 FTB 설정
=================================================================================*/
void FE_vArrangeErrorCode(UINT u16MandoDtc)
{
    UCHAR u8NumErr=0;
    //PwrM_IgnStatusType IgnState;
    u8NumErr=FE_u8CountingDTC();

    if(u8NumErr>=10)
    {
        feu8ErrBufLastAddr=9;
        feu1FullErrBuffFlg=1;
    }
    else
    {
        feu8ErrBufLastAddr=u8NumErr;
        feu1FullErrBuffFlg=0;
    }

    //IgnState = PwrM_GetIgnStatus();
    if(/*Diag_ErrProcBus.Diag_ErrProcIgnOnOffSts comfile for PJS*/1==ON/*CE_ON_STATE8*/)
    {
         FE_vArrangeHMCDTC(u16MandoDtc);
    }
}

/*=================================================================================
    Function : UCHAR FE_u8CountingDTC(void)
===================================================================================
    Description : error code의 갯수를 세는 함수.
=================================================================================*/
static UCHAR FE_u8CountingDTC(void)
{
    uint8_t i=0;

    for(i=0;i<U8NUM_MAX_ERROR_NUM;i++)
    {
        if(feu8ErrorPointer[i]==0xff)
        {
            break;
        }
        else
        {
            ;
        }
    }

    return i;
}

static void FE_vArrangeHMCDTC(UINT u16MandoDtc)
{
    switch(u16MandoDtc)
    {
        case U16_FL_OPEN_ERR:
            FE_vWriteErrorCode(U8_FL_HW_FAULT, U8_MIN_THRD);
        break;

        case U16_FR_OPEN_ERR:
            FE_vWriteErrorCode(U8_FR_HW_FAULT, U8_MIN_THRD);
        break;

        case U16_RL_OPEN_ERR:
            FE_vWriteErrorCode(U8_RL_HW_FAULT, U8_MIN_THRD);
        break;

        case U16_RR_OPEN_ERR:
            FE_vWriteErrorCode(U8_RR_HW_FAULT, U8_MIN_THRD);
        break;

        case U16_FL_SHORT_ERR:
            FE_vWriteErrorCode(U8_FL_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_FR_SHORT_ERR:
            FE_vWriteErrorCode(U8_FR_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_RL_SHORT_ERR:
            FE_vWriteErrorCode(U8_RL_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_RR_SHORT_ERR:
            FE_vWriteErrorCode(U8_RR_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_FL_OVERTEMP_ERR:
            FE_vWriteErrorCode(U8_FL_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_FR_OVERTEMP_ERR:
            FE_vWriteErrorCode(U8_FR_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_RL_OVERTEMP_ERR:
            FE_vWriteErrorCode(U8_RL_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_RR_OVERTEMP_ERR:
            FE_vWriteErrorCode(U8_RR_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_FL_LEAKAGE_ERR:
            FE_vWriteErrorCode(U8_FL_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_FR_LEAKAGE_ERR:
            FE_vWriteErrorCode(U8_FR_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_RL_LEAKAGE_ERR:
            FE_vWriteErrorCode(U8_RL_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_RR_LEAKAGE_ERR:
            FE_vWriteErrorCode(U8_RR_HW_FAULT, U8_MAX_THRD);
        break;

        case U16_FL_AIRGAP_ERR:
        case U16_FL_PHASE_ERR:
            FE_vWriteErrorCode(U8_FL_AIRGAP, U8_NO_SIGNAL);
        break;

        case U16_FR_AIRGAP_ERR:
        case U16_FR_PHASE_ERR:
            FE_vWriteErrorCode(U8_FR_AIRGAP, U8_NO_SIGNAL);
        break;

        case U16_RL_AIRGAP_ERR:
        case U16_RL_PHASE_ERR:
            FE_vWriteErrorCode(U8_RL_AIRGAP, U8_NO_SIGNAL);
        break;

        case U16_RR_AIRGAP_ERR:
        case U16_RR_PHASE_ERR:
            FE_vWriteErrorCode(U8_RR_AIRGAP, U8_NO_SIGNAL);
        break;

        case U16_FL_PJUMP_ERR:
        case U16_FL_MJUMP_ERR:
        case U16_FL_EXCITER_H_ERR:
        case U16_FL_EXCITER_L_ERR:
        case U16_FL_BROKEN_T_ERR:
        case U16_FL_SPLIT_T_ERR:
        case U16_FL_SWAP_ERR:
            FE_vWriteErrorCode(U8_FL_SPD_JUMP, U8_INVALID_SIGNAL);
        break;

        case U16_FR_PJUMP_ERR:
        case U16_FR_MJUMP_ERR:
        case U16_FR_EXCITER_H_ERR:
        case U16_FR_EXCITER_L_ERR:
        case U16_FR_BROKEN_T_ERR:
        case U16_FR_SPLIT_T_ERR:
        case U16_FR_SWAP_ERR:
            FE_vWriteErrorCode(U8_FR_SPD_JUMP, U8_INVALID_SIGNAL);
        break;

        case U16_RL_PJUMP_ERR:
        case U16_RL_MJUMP_ERR:
        case U16_RL_EXCITER_H_ERR:
        case U16_RL_EXCITER_L_ERR:
        case U16_RL_BROKEN_T_ERR:
        case U16_RL_SPLIT_T_ERR:
        case U16_RL_SWAP_ERR:
            FE_vWriteErrorCode(U8_RL_SPD_JUMP, U8_INVALID_SIGNAL);
        break;

        case U16_RR_PJUMP_ERR:
        case U16_RR_MJUMP_ERR:
        case U16_RR_EXCITER_H_ERR:
        case U16_RR_EXCITER_L_ERR:
        case U16_RR_BROKEN_T_ERR:
        case U16_RR_SPLIT_T_ERR:
        case U16_RR_SWAP_ERR:
            FE_vWriteErrorCode(U8_RR_SPD_JUMP, U8_INVALID_SIGNAL);
        break;


        case U16_FLO_OPEN_ERR:
        case U16_FRO_OPEN_ERR:
        case U16_RLO_OPEN_ERR:
        case U16_RRO_OPEN_ERR:
        case U16_FLI_OPEN_ERR:
        case U16_FRI_OPEN_ERR:
        case U16_RLI_OPEN_ERR:
        case U16_RRI_OPEN_ERR:
        case U16_APV1_OPEN_ERR:
        case U16_APV2_OPEN_ERR:
        case U16_RLV1_OPEN_ERR:
        case U16_RLV2_OPEN_ERR:
        case U16_CUT1_OPEN_ERR:
        case U16_CUT2_OPEN_ERR:
        case U16_SIM_OPEN_ERR:
            FE_vWriteErrorCode(U8_SOLENOID_FAIL, U8_MIN_THRD);
        break;

        case U16_FLO_SHORT_ERR:
        case U16_FRO_SHORT_ERR:
        case U16_RLO_SHORT_ERR:
        case U16_RRO_SHORT_ERR:
        case U16_FLI_SHORT_ERR:
        case U16_FRI_SHORT_ERR:
        case U16_RLI_SHORT_ERR:
        case U16_RRI_SHORT_ERR:
        case U16_APV1_SHORT_ERR:
        case U16_APV2_SHORT_ERR:
        case U16_RLV1_SHORT_ERR:
        case U16_RLV2_SHORT_ERR:
        case U16_CUT1_SHORT_ERR:
        case U16_CUT2_SHORT_ERR:
        case U16_SIM_SHORT_ERR:

            FE_vWriteErrorCode(U8_SOLENOID_FAIL, U8_MAX_THRD);
        break;

        case U16_FLO_OVERTEMP_ERR:
        case U16_FRO_OVERTEMP_ERR:
        case U16_RLO_OVERTEMP_ERR:
        case U16_RRO_OVERTEMP_ERR:
        case U16_FLI_OVERTEMP_ERR:
        case U16_FRI_OVERTEMP_ERR:
        case U16_RLI_OVERTEMP_ERR:
        case U16_RRI_OVERTEMP_ERR:
            FE_vWriteErrorCode(U8_SOLENOID_FAIL, U8_MAX_THRD);
        break;

        case U16_FLI_CURREG_ERR:
        case U16_FRI_CURREG_ERR:
        case U16_RLI_CURREG_ERR:
        case U16_RRI_CURREG_ERR:
            FE_vWriteErrorCode(U8_SOLENOID_FAIL, U8_MAX_THRD);
        break;



        case U16_VR_BYPASS_ERR:
            FE_vWriteErrorCode(U8_VALVE_RALEY_FAIL, U8_MAX_THRD);
        break;

        case U16_VR_OPEN_ERR:
        case U16_VR_TO_GND_ERR:
        case U16_VR_TO_LKG_ERR:
        case U16_BAT1_FUSE_OPEN:
            FE_vWriteErrorCode(U8_VALVE_RALEY_FAIL, U8_MIN_THRD);
        break;

        case U16_MOT_FUSE_OPEN_ERR:
        case U16_MOT_OPEN_ERR:
        case U16_VBB_PWR_ERR:
            FE_vWriteErrorCode(U8_MOTOR_FAIL, U8_MIN_THRD);
        break;

        case U16_MR_OPEN_ERR:
        case U16_MOT_SHORT_ERR:
        case U16_MOT_OVERTEMP_ERR:
            FE_vWriteErrorCode(U8_MOTOR_FAIL, U8_MAX_THRD);
        break;

        case U16_MOT_LOCK_ERR:
            FE_vWriteErrorCode(U8_MOTOR_FAIL, U8_INVALID_SIGNAL);
        break;
        case U16_MOT_LSD_OPEN_ERR:
            FE_vWriteErrorCode(U8_MOTOR_FAIL, U8_MAX_THRD);
        break;
        case U16_MOT_LSD_SHORT_ERR:
            FE_vWriteErrorCode(U8_MOTOR_FAIL, U8_MIN_THRD);
        break;

        case U16_UNDER_PWR_ERR:
        case U16_LOW_PWR_ERR:
            FE_vWriteErrorCode(U8_UNDER_POWER, U8_MIN_THRD);
        break;

        case U16_OVER_PWR_ERR:
            FE_vWriteErrorCode(U8_OVER_POWER, U8_MAX_THRD);
        break;


        case U16_TEMPERATURE_ERR:
            FE_vWriteErrorCode(U8_TEMPERATURE_ERROR, U8_MAX_THRD);
        break;


        case U16_ADC_ERR:
        case U16_ROM_ERR:
        case U16_RAM_ERR:
        case U16_CHECKSUM_ERR:
        case U16_CYCLETIME_ERR:
        case U16_COM_ERR:
        case U16_EEPROM_ERR:
        case U16_VDD_IC_ERR:
        case U16_IL_ISR_ERR:
        case U16_ASIC_UNDER_V_ERR:
        case U16_ASIC_OVER_V_ERR:
        case U16_ASIC_FGND_ERR:
        case U16_ASIC_FREQ_ERR:
        case U16_ASIC_OSTD_ERR:
        case U16_ASIC_VERSION_ERR:
        case U16_COMMASIC_INIT_ERR:
        case U16_ESCASIC_INIT_ERR:
        case U16_SUBMCU_ERR:
        case U16_ROLLING_CNT_ERR:
        case U16_SEN_PPOWER_ERR:
            FE_vWriteErrorCode(U8_ECU_HW_FAIL, U8_INVALID_SIGNAL);
        break;
            case U16_5V_PDT_PWR_ERR:
            case U16_5V_PDF_PWR_ERR:
            case U16_5V_PWR_ERR:
                FE_vWriteErrorCode(U8_5VSEN_PWR_FAIL, U8_MAX_THRD);
            break;

            case U16_AX_CALIBRATION_ERR:
                 FE_vWriteErrorCode(U8_G_SEN_CAL_FAIL, U8_INVALID_SIGNAL);
            break;



        case U16_CAN_OVERRUN_ERR:
            FE_vWriteErrorCode(U8_CAN_OVERRUN, U8_NO_SIGNAL);
        break;

        case U16_BUS_OFF_ERR:
            FE_vWriteErrorCode(U8_CAN_BUS_OFF, U8_INVALID_SIGNAL);
        break;



        case U16_EMS1_TIMEOUT_ERR:
        case U16_EMS2_TIMEOUT_ERR:
            FE_vWriteErrorCode(U8_EMS_TIMEOUT, U8_NO_SIGNAL);
        break;

        case U16_TCU1_TIMEOUT_ERR:
            FE_vWriteErrorCode(U8_TCU_TIMEOUT, U8_NO_SIGNAL);
        break;

        case U16_TCU1_UNMATCH_ERR:
            FE_vWriteErrorCode(U8_CAN_MSG_UNMATCH, U8_INVALID_SIGNAL);
        break;

        case U16_CLU2_TIMEOUT_ERR:
            FE_vWriteErrorCode(U8_CLU2_TIMEOUT, U8_NO_SIGNAL);
        break;

        case U16_CLU2_SIGNAL_ERR:
            FE_vWriteErrorCode(U8_CLU2_SIGNAL_FAIL, U8_INVALID_SIGNAL);
        break;

        /* fcu1PVAVError = 1 */
        case U16_EMSSIGINVALID_ERR:
            FE_vWriteErrorCode(U8_EMSINVALID_FAIL, U8_INVALID_SIGNAL);
        break;


        case U16_VDC_SW_ERR:
            FE_vWriteErrorCode(U8_VDC_SW_FAIL, U8_MAX_THRD);
        break;

        case U16_HDC_SW_ERR:
            FE_vWriteErrorCode(U8_HDC_SW_FAIL, U8_MAX_THRD);
        break;

        case U16_IMU_HW_LINE_ERR:

        case U16_AX_HW_LINE_ERR:

            FE_vWriteErrorCode(U8_YAW_LG_ELECTIC_FAIL, U8_NO_SIGNAL);

        break;


        case U16_YAW_SEN_BUS_OFF_ERR:


            FE_vWriteErrorCode(U8_YAW_LG_ELECTIC_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_YAW_SEN_OVERRUN_ERR:
            FE_vWriteErrorCode(U8_YAW_LG_OVERRUN, U8_INVALID_SIGNAL);
        break;



        case U16_YAW_INTERNAL_ERR:
        case U16_IMU_CAN_FUNC_ERR:
        case U16_IMU_ROLLING_CNT_ERR:
        case U16_IMU_CHECKSUM_ERR:
        case U16_IMU_RASTER_FREQ_ERR:            /* ACU Temperature Sensor Error */
        case U16_YAW_CBIT_ERR:
        case U16_LG_CBIT_ERR:
        case U16_YAW_STD_OFFSET_ERR:
        case U16_YAW_SHORT_DRV_OFFSET_ERR:
        case U16_YAW_LONG_DRV_OFFSET_ERR:
        case U16_YAW_MDL_MAY_ERR:
        case U16_YAW_MDL_MUST_ERR:
        case U16_YAW_SHOCK_ERR:
        case U16_YAW_STICK_ERR:
        case U16_YAW_NOISE_ERR:
        case U16_YAW_REVERSE_ERR:
        case U16_YAW_RANGE_ERR:
        case U16_LG_STD_OFFSET_ERR:
        case U16_LG_SHORT_DRV_OFFSET_ERR:
        case U16_LG_LONG_DRV_OFFSET_ERR:
        case U16_LG_MDL_MAY_ERR:
        case U16_LG_MDL_MUST_ERR:
        case U16_LG_SHOCK_ERR:
        case U16_LG_STICK_ERR:
        case U16_LG_NOISE_ERR:
        case U16_LG_REVERSE_ERR:
        case U16_LG_RANGE_ERR:
        case U16_LG_WRONG_INSTALLED_ERR:

            case U16_AX_STD_OFFSET_ERR:
            case U16_AX_SHORT_DRV_OFFSET_ERR:
            case U16_AX_LONG_DRV_OFFSET_ERR:
            case U16_AX_STICK_ERR:
            case U16_AX_NOISE_ERR:
            case U16_AX_REVERSE_ERR:
            case U16_AX_RANGE_ERR:
            case U16_AX_CBIT_ERR:

            case U16_AX_CANFUNC_ERR:
            case U16_AX_CAN_INTERNAL_ERR:
            case U16_AX_CAN_ROLLING_CNT_ERR:

            FE_vWriteErrorCode(U8_YAW_LG_SIGNAL_FAIL, U8_INVALID_SIGNAL);
        break;

            case U16_SAS_HW_LINE_ERR:
                FE_vWriteErrorCode(U8_STR_HW_FAIL, U8_NO_SIGNAL);
            break;

            case U16_SAD_CHECKSUM_ERR:
            case U16_SAD_ROLLING_CNT_ERR:
            case U16_SAD_INTERNAL_ERR:
            case U16_SAD_PROTECTION_ERR:
            case U16_SAD_RANGE_ERR:
            case U16_SAD_SHORT_DRV_OFFSET_ERR:
            case U16_SAD_LONG_DRV_OFFSET_ERR:
            case U16_SAD_MDL_MAY_ERR:
            case U16_SAD_SHOCK_ERR:
            case U16_SAD_STICK_ERR:
            case U16_SAD_RATE_ERR:
            case U16_SAD_REVERSE_ERR:
                FE_vWriteErrorCode(U8_STR_SIGNAL_FAIL, U8_INVALID_SIGNAL);
            break;

            case U16_SAD_CALIBRATION_ERR:
                FE_vWriteErrorCode(U8_STR_CAL_ERROR, U8_INVALID_SIGNAL);
            break;

        case U16_HPA_HW_ERR:
        case U16_MP_AVERAGE_ERR:
            FE_vWriteErrorCode(U8_HPA_HW_FAIL, U8_MAX_THRD);
        break;

        case U16_BCP_HW_ERR:
            FE_vWriteErrorCode(U8_BCP_SEN_HW_FAIL, U8_MAX_THRD);
        break;

        case U16_PSP_HW_ERR:
            FE_vWriteErrorCode(U8_PSP_SEN_HW_FAIL, U8_MAX_THRD);
        break;

        case U16_MP_OFFSET_ERR:
        case U16_MP_NOISE_ERR:
        case U16_MP_VREF_ERR:
        case U16_MP_OFFSET_10BAR_ERR:
            FE_vWriteErrorCode(U8_HPA_SIGNAL_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_HPA_SHORTTERM_CHARGE_ERR:
            FE_vWriteErrorCode(U8_HPA_SHORTTERM_CHARGE_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_HPA_LONGTERM_CHARGE_ERR:
            FE_vWriteErrorCode(U8_HPA_LONGTERM_CHARGE_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_HPA_LOW_PRESSURE_ERR:
            FE_vWriteErrorCode(U8_HPA_LOW_PRESSURE_FAIL, U8_INVALID_SIGNAL);
        break;


        case U16_BCP_ABNORMAL_PRESSURE_ERR:
            FE_vWriteErrorCode(U8_BCP_ABNORMAL_PRESSURE_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_PSP_ABNORMAL_PRESSURE_ERR:
            FE_vWriteErrorCode(U8_PSP_ABNORMAL_PRESSURE_FAIL, U8_INVALID_SIGNAL);
        break;


        case U16_PRS_CAL_ERR :
            FE_vWriteErrorCode(U8_PRESSURE_SENSOR_CAL_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_IGN_LINE_OPEN_ERR :
            FE_vWriteErrorCode(U8_IGN_LINE_OPEN_FAIL, U8_MIN_THRD);
        break;

        case U16_BCP_LEAK_ERR :
        case U16_BCS_LEAK_ERR :
            FE_vWriteErrorCode(U8_BOOSTER_CIRCUIT_LEAK_FAIL, U8_INVALID_SIGNAL);
        break;


        case U16_BLS_BS_ERR:
        case U16_BLS_HIGH_ERR:
        case U16_BLS_LOW_ERR:
        case U16_BLS_VREF_ERR:
        case U16_BLS_NOISE_ERR:
        case U16_BS_NOISE_ERR:
            FE_vWriteErrorCode(U8_BLS_FAIL, U8_INVALID_SIGNAL);
        break;

        case U16_12V_SEN_OPEN_ERR:
            FE_vWriteErrorCode(U8_12VSEN_PWR_FAIL, U8_MIN_THRD);
        break;

        case U16_12V_SEN_SHORT_ERR:
            FE_vWriteErrorCode(U8_12VSEN_PWR_FAIL, U8_MAX_THRD);
        break;


        case U16_HCU_TIMEOUT_ERR:
            FE_vWriteErrorCode(U8_HCU_TIMEOUT, U8_NO_SIGNAL);
        break;

        /*@@@=========================================================================
        REVISION 1.13.1 : 9326 Master
        VSM적용(고객 요청 의거) 따라, 에러 핸들링 및 Test Complete 신규 추가
        ==============================================================================*/


            case U16_PEDAL_PDF_P1_OPEN_ERR:  /*pdt hw open*/
            case U16_PEDAL_PDF_P1_SHORT_ERR: /*pdt hw short*/
            case U16_PEDAL_PDF_P2_OPEN_ERR:  /*pdf hw open*/
            case U16_PEDAL_PDF_P2_SHORT_ERR: /*pdf hw short*/
                FE_vWriteErrorCode(U8_PEDAL_SENSOR_HW_FAIL, U8_NO_SIGNAL);
            break;

            case U16_PEDAL_PDF_MISMATCH_ERR:
            case U16_PEDAL_PDF_SIGNAL_ERR:
                FE_vWriteErrorCode(U8_PEDAL_SENSOR_SW_FAIL, U8_INVALID_SIGNAL);
            break;

            case U16_PDT_OFFSET_ERR:
            case U16_PDF_OFFSET_ERR:
            case U16_PDT_STICK_ERR:
            case U16_PDF_STICK_ERR:
            case U16_PDT_NOISE_ERR:
            case U16_PDF_NOISE_ERR:
            case U16_PDT_NOCHANGE_ERR:
                FE_vWriteErrorCode(U8_PEDAL_SIGNAL_ABNORMAL, U8_INVALID_SIGNAL);
            break;

            case U16_PEDAL_CAL_ERR:
                FE_vWriteErrorCode(U8_PEDAL_SENSOR_CAL_FAIL, U8_INVALID_SIGNAL);
                break;



            /* SCC Timeout, Rolling Conuter  */
            case U16_SCCTIMEOUT_ERR:
                FE_vWriteErrorCode(U8_SCC1_SCC2_CAN_TIMEOUT_FAIL, U8_NO_SIGNAL);
            break;

            /* aReqMax, aReqMin Invalid Data */
            case U16_SCCMSGINVALID_ERR:
                FE_vWriteErrorCode(U8_SCC1_SCC2_CAN_ABNORM_FAIL, U8_INVALID_SIGNAL);
            break;

            /* Acceleration Invalid of SCC */
            case U16_SCC_ACCEL_ABNORM_ERR:
                FE_vWriteErrorCode(U8_SCC_ACCEL_ABNORM_FAIL, U8_INVALID_SIGNAL);
            break;

            /* F_N_ENG=0x01 of EMS1 */
            case U16_EMS1MSGINVALID_ERR:
                FE_vWriteErrorCode(U8_EMS1_CAN_ABNORM_FAIL, U8_INVALID_SIGNAL);
            break;

            /* EMS5 Timeout, Invalid Signal of EMS5 */
            case U16_EMS5TIMEOUT_ERR:
            case U16_EMS5MSGINVALID_ERR:
                FE_vWriteErrorCode(U8_EMS5_CAN_TIMEOUT_FAIL, U8_NO_SIGNAL);
            break;

             /* TCU_Fault=0x01~0x03 of TCU1 */
            case U16_TCU1MSGINVALID_ERR:
                FE_vWriteErrorCode(U8_TCU_SCC_CAN_FAIL_FAIL, U8_INVALID_SIGNAL);
            break;

            /* lespu1TCU_G_SEL_DISP_ERR=0xFFFF */
            case U16_TCUGEARINVALID_ERR:
                FE_vWriteErrorCode(U8_TCUGEARINVALID_FAIL, U8_INVALID_SIGNAL);
            break;


        case U16_MCU_TIMEOUT_ERR:
            FE_vWriteErrorCode(U8_MCU_TIMEOUT, U8_NO_SIGNAL);
        break;

        case U16_MOC_APPLY_SWITCH_OPEN:
        case U16_MOC_APPLY_SWITCH_SHORT:
        case U16_MOC_RELEASE_SWITCH_OPEN:
        case U16_MOC_RELEASE_SWITCH_SHORT:
        case U16_MOC_SWITCH_LINE_ERR:
        case U16_MOC_SWITCH_INVALID_ERR:
        case U16_MOC_SWITCH_DOUBLE_ON_ERR:
            FE_vWriteErrorCode(U8_MOC_SWITCH_ERR, U8_INVALID_SIGNAL);
            break;

        case U16_MOC_CURRENT_L_HW_ERR:
        case U16_MOC_CURRENT_L_OFFSET_ERR:
        case U16_MOC_CURRENT_L_OVER_VOLTAGE:
        case U16_MOC_CURRENT_L_STICK_ERR:
        case U16_MOC_CURRENT_L_LIMIT_ERR:
        case U16_MOC_CURRENT_R_HW_ERR:
        case U16_MOC_CURRENT_R_OFFSET_ERR:
        case U16_MOC_CURRENT_R_OVER_VOLTAGE:
        case U16_MOC_CURRENT_R_STICK_ERR:
        case U16_MOC_CURRENT_R_LIMIT_ERR:
            FE_vWriteErrorCode(U8_MOC_CURRENT_ERR, U8_INVALID_SIGNAL);
            break;

        case U16_MOC_FORCE_CAL_ERR:
        case U16_MOC_FORCE_INVALID_ERR:
        case U16_MOC_FORCE_STICK_ERR:
        case U16_MOC_FORCE_GRAD_ERR:
            FE_vWriteErrorCode(U8_MOC_FORCE_ERR, U8_INVALID_SIGNAL);
            break;

        case U16_MOC_ACTUATOR_L_MTR_FET_ERR:
        case U16_MOC_ACTUATOR_L_APPLY_DRV_ERR:
        case U16_MOC_ACTUATOR_L_RELEASE_DRV_ERR:
        case U16_MOC_ACTUATOR_L_NON_DRV_ERR:
        case U16_MOC_ACTUATOR_L_UNDER_CTRL_ERR:
        case U16_MOC_ACTUATOR_VDD_TOGGLE_ERR:
            FE_vWriteErrorCode(U8_MOC_ACTUATOR_L_LINE_ERR, U8_INVALID_SIGNAL);
            break;

        case U16_MOC_ACTUATOR_L_LONG_CONTROL:
            FE_vWriteErrorCode(U8_MOC_ACTUATOR_L_TIMEOUT_ERR, U8_MAX_THRD);
            break;

        case U16_MOC_ACTUATOR_R_MTR_FET_ERR:
        case U16_MOC_ACTUATOR_R_APPLY_DRV_ERR:
        case U16_MOC_ACTUATOR_R_RELEASE_DRV_ERR:
        case U16_MOC_ACTUATOR_R_NON_DRV_ERR:
        case U16_MOC_ACTUATOR_R_UNDER_CTRL_ERR:
            FE_vWriteErrorCode(U8_MOC_ACTUATOR_R_LINE_ERR, U8_INVALID_SIGNAL);
            break;

        case U16_MOC_ACTUATOR_R_LONG_CONTROL:
            FE_vWriteErrorCode(U8_MOC_ACTUATOR_R_TIMEOUT_ERR, U8_MAX_THRD);
            break;

        case U16_MOC_CONTROL_NOT_FINISHED:
            FE_vWriteErrorCode(U8_MOC_CONTROL_NOT_FINISHED, U8_MAX_THRD);
            break;

        case U16_MOC_HALL_PWR_OPEN_SHORT_ERR:
            FE_vWriteErrorCode(U8_MOC_HALLPWR_ERR, U8_MAX_THRD);
            break;

        default:
        break;
    }
}

/*=================================================================================
    Function : void FE_vWriteErrorCode(UINT e_code, UCHAR u8DtcFtb)
===================================================================================
    Description : error buffer에 error code 및 SOD, FTB를 기록, 정렬하는 함수.
=================================================================================*/
/*static*/ void FE_vWriteErrorCode(UINT e_code, UCHAR u8DtcFtb)
{
    UCHAR i, u8ErrCase, u8WriteAddr;
    UCHAR u8NumErr=0;
    
    //PwrM_IgnStatusType IgnState;
    u8NumErr=FE_u8CountingDTC();

    if(u8NumErr>=10)
    {
        feu8ErrBufLastAddr=9;
        feu1FullErrBuffFlg=1;
    }
    else
    {
        feu8ErrBufLastAddr=u8NumErr;
        feu1FullErrBuffFlg=0;
    }    

    u8ErrCase=u8WriteAddr=0;
    feu1SaveNewDtcFlg=0;

    for(i=0;i<U8NUM_MAX_ERROR_NUM;i++)
    {
        if(ErrBuffer[i].ErrMember[U8_DTC_INFORM_ERRCODE]==e_code)
        {

            if(((ErrBuffer[i].ErrMember[U8_DTC_INFORM_SOD])&0x40)!=0x40) /* Current DTC */
            {
                u8ErrCase=1; /* History DTC */
                u8WriteAddr=i;
            }
            else
            {
                u8ErrCase=0; /* Current DTC */
                u8WriteAddr=i;
            }
            break;
        }
        else
        {
            if(u8ErrCase!=2)
            {
                if(ErrBuffer[i].ErrMember[U8_DTC_INFORM_ERRCODE]==0)
                {
                    u8ErrCase=2;
                    u8WriteAddr=i;
                    feu1SaveNewDtcFlg=1;
                }
                else
                {
                    if(i==9)
                    {
                        u8ErrCase=3;
                        u8WriteAddr=feu8ErrorPointer[0];
                        feu1SaveNewDtcFlg=1;
                    }
                }
            }
            else
            {
                ;
            }
        }
    }

    if(u8ErrCase!=0) /* Find New DTC */
    {
        ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_INFORM_ERRCODE]=e_code;
        ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_INFORM_FTB]=u8DtcFtb;

        /* TODO */
        ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_RESERVED]=(e_code >> 8);


            FE_vSetHMCDTCInform(u8WriteAddr);


                //FE_vSaveFreezeFrame(u8WriteAddr);


    }
    else
    {
        ;
    }

    switch(u8ErrCase)
    {
        case 1: /* Same DTC (Stored DTC is History DTC) */

                if(ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_INFORM_OCCNUM]<0xFF)
                {
                    ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_INFORM_OCCNUM]+=1; /* Increase Occurence Number */
                }
                else
                {
                    ;
                }
            FE_vSortingErrPointer(u8WriteAddr, u8WriteAddr);
        break;

        case 2: /* New DTC, Find empty buffer */
                ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_INFORM_OCCNUM]=1; /* Initialize Occurence Number */

            feu8ErrorPointer[feu8ErrBufLastAddr]=u8WriteAddr;
        break;

        case 3: /* New DTC, Error buffers are FULL */

                ErrBuffer[u8WriteAddr].ErrMember[U8_DTC_INFORM_OCCNUM]=1; /* Initialize Occurence Number */
            FE_vSortingErrPointer(u8WriteAddr, u8WriteAddr);
        break;

        default:
        break;
    }

    number_of_error=FE_u8CountingDTC();
}

/*=================================================================================
  Function : void FE_vSetHMCDTCInform(void)
===================================================================================
  Description : 신규 Error 발생 시 SOD 정보 구성 (HMC)
=================================================================================*/
static void FE_vSetHMCDTCInform(UCHAR SortCnt)
{
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_IGN_CNT]=0; /* Clear IGN Counter */
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_SOD]|=(UCHAR)0xE0; /* W/Lamp ON, Current */
}

/*=================================================================================
  Function : void FE_vWriteErrorEEPROM(void)
===================================================================================
  Description : Wrtie Error Code, SOD and FTB to EEPROM.
=================================================================================*/
void FE_vWriteErrorEEPROM(void)
{
    if((weu1EraseSector==0)&&(weu1WriteSector==0)&&(feu1WriteErrPtrtoEep==0))
    {
        if(feu1WriteDtctoEep==0)
        {
            FE_vCheckEepromErrBufferData();
        }
        else
        {
            if(feu16DtcEepaddr!=0)
            {
                weu16Eep_addr=feu16DtcEepaddr+feu8ErrSectorCnt;

                if(feu16AccessPointer!=0xFF)
                {
                    weu16EepBuff[0]=*(UINT*)&(ErrBuffer[feu16AccessPointer].ErrMember[U8_DTC_INFORM_ERRCODE+feu8ErrSectorCnt]);
                    weu16EepBuff[1]=*(UINT*)&(ErrBuffer[feu16AccessPointer].ErrMember[U8_DTC_INFORM_SOD+feu8ErrSectorCnt]);
                }
                else
                {
                    weu16EepBuff[0]=weu16EepBuff[1]=0x00;
                }

                weu1WriteSector=1;
                feu8ErrSectorCnt+=4;
            }
            else
            {
                ;
            }

            if(feu8ErrSectorCnt>=U8NUM_DTC_INFORM)
            {
                feu1WriteDtctoEep=0;
                feu8ErrSectorCnt=0;
                feu16DtcEepaddr=0;
            }
            else
            {
                ;
            }
        }
    }
    else
    {
        ;
    }

    if((weu1EraseSector==0)&&(weu1WriteSector==0)&&(feu1WriteDtctoEep==0))
    {
        if(feu1WriteErrPtrtoEep==0)
        {
            FE_vCheckEepromErrPointerData();
        }
        else
        {
            feu1WriteErrPtrtoEep=0;
        }
    }
    else
    {
        ;
    }
}


/*=================================================================================
  Function : void FE_vCheckEepromErrBufferData(void)
===================================================================================
  Description : Wrtie Error Code, SOD and FTB to EEPROM.
=================================================================================*/
static void FE_vCheckEepromErrBufferData(void)
{
    UCHAR u8DtcIndex=0, u8DtcFtb=0, u8DtcSod=0, u8DtcIgnCnt=0;
    uint16_t i=0, j=0;
    #if __FREEZE_FRAME_ENABLE==1
        UCHAR u8DtcOccNum=0, u8DtcSubCode=0;
        /* UCHAR u8ErrPtrEep[U8NUM_X10_DTC_INFORM]={0}; */
        UCHAR u8ErrEep[U8NUM_DTC_INFORM]={0};
    #else
        UCHAR u8ErrEep[U8NUM_DTC_INFORM]={0};
    #endif

    for(i=0;i<U8NUM_X10_DTC_INFORM;i+=U8NUM_DTC_INFORM)
    {
       j=(UCHAR)(i/U8NUM_DTC_INFORM);

       u8DtcIndex  =ErrBuffer[j].ErrMember[U8_DTC_INFORM_ERRCODE];
       u8DtcFtb    =ErrBuffer[j].ErrMember[U8_DTC_INFORM_FTB];
       u8DtcIgnCnt =ErrBuffer[j].ErrMember[U8_DTC_INFORM_IGN_CNT];
    #if __CUSTOMER_DTC==HMC_DTC
       u8DtcSod=0;
    #elif __CUSTOMER_DTC==GM_DTC
       u8DtcSod=ErrBuffer[j].ErrMember[U8_DTC_INFORM_SOD]|0xFB;
    #endif
    #if __FREEZE_FRAME_ENABLE==1
       u8DtcOccNum =ErrBuffer[j].ErrMember[U8_DTC_INFORM_OCCNUM];
       u8DtcSubCode=ErrBuffer[j].ErrMember[U8_DTC_INFORM_SUBCODE];
    #endif

       if(WE_u8ReadEEP_Byte((uint16_t)(U16_EEP_DTC_DATA_ADDR+i), &u8ErrEep[0], U8NUM_DTC_INFORM)==1)
       {
            if(u8DtcSod==0)
            {
               u8DtcSod=u8ErrEep[U8_DTC_INFORM_SOD]|(UCHAR)0xFB;
            }

            #if __FREEZE_FRAME_ENABLE==1
            if((u8ErrEep[U8_DTC_INFORM_ERRCODE]!=u8DtcIndex)||(u8ErrEep[U8_DTC_INFORM_FTB]!=u8DtcFtb)||
                (u8ErrEep[U8_DTC_INFORM_IGN_CNT]!=u8DtcIgnCnt)||(u8ErrEep[U8_DTC_INFORM_OCCNUM]!=u8DtcOccNum)||
                (u8ErrEep[U8_DTC_INFORM_SUBCODE]!=u8DtcSubCode)
                #if __CUSTOMER_DTC==GM_DTC
                    || ((u8ErrEep[U8_DTC_INFORM_SOD]|0xFB)!=u8DtcSod)
                #endif
              )
            #else
            if((u8ErrEep[U8_DTC_INFORM_ERRCODE]!=u8DtcIndex)||(u8ErrEep[U8_DTC_INFORM_FTB]!=u8DtcFtb)||
                (u8ErrEep[U8_DTC_INFORM_IGN_CNT]!=u8DtcIgnCnt)
                #if __CUSTOMER_DTC==GM_DTC
                    ||((u8ErrEep[U8_DTC_INFORM_SOD]|0xFB)!=u8DtcSod)
                #endif
              )
            #endif
            {
                feu16DtcEepaddr=(uchar16_t)(U16_EEP_DTC_DATA_ADDR+i);
                feu16AccessPointer=j;
                feu1WriteDtctoEep=1;
                break;
            }
            else
            {
                feu16DtcEepaddr=0;
            }
       }
       else
       {
           ;
       }
    }

    if(i>=U8NUM_X10_DTC_INFORM)
    {
        feu1WrDtcToEepComplete=1;
    }
    else
    {
        feu1WrDtcToEepComplete=0;
    }
}


static void FE_vCheckEepromErrPointerData(void)
{
    UCHAR i=0, j=0;
    UCHAR u8ErrPtr[U8NUM_SIZE_ERROR_PTR]={0};

    if(WE_u8ReadEEP_Byte(U16_EEP_DTC_PTR_ADDR, &u8ErrPtr[0], U8NUM_SIZE_ERROR_PTR)==1)
    {
        for (i=0;i<U8NUM_MAX_ERROR_NUM;i++)
        {
            if (feu8ErrorPointer[i]!=u8ErrPtr[i])
            {
                j=i/4;
                weu16Eep_addr=(uint16_t)(U16_EEP_DTC_PTR_ADDR+(j*4));

                weu16EepBuff[0]=*(UINT*)&(feu8ErrorPointer[(j*4)]);
                weu16EepBuff[1]=*(UINT*)&(feu8ErrorPointer[(j*4)+2]);

                feu1WriteErrPtrtoEep=1;
                weu1WriteSector=1;
                break;
            }
            else
            {
                ;
            }
        }
    }
    else
    {
        ;
    }

    if (i>=U8NUM_MAX_ERROR_NUM)
    {
        feu1WrErrPtrToEepComplte=1;
    }
    else
    {
        feu1WrErrPtrToEepComplte=0;
    }
}

/*=================================================================================
  Function : void FE_vSaveFreezeFrame(void)
===================================================================================
  Description : Freeze Frame 구성
=================================================================================*/
#if __FREEZE_FRAME_ENABLE==1
void FE_vSaveFreezeFrame(UCHAR SortCnt)
{
    /* UCHAR u8NumErr; */

    /* u8NumErr=FE_u8CountingDTC(); */

    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG]=0;

    /* Brake Switch, 1/0 */
    if(fu1BLSSignal==1)
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 0);
    }
    else
    {
        ;
    }

    #if __ECU==ESP_ECU_1
        #if __BTC==ENABLE
            /* TCS Available - Driver Intent, 1/2 */
            if(fu1TcsDriverIntent==0)
            {
                FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 2);
            }
            else
            {
               ;
            }

            /* VSE Available - Driver Intent, 1/1 */
            #if __VDC==ENABLE
            if(fu1EscDriverIntent==0)
            {
                FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 1);
            }
            else
            {
                ;
            }
            #endif
        #endif
    #endif

    #if __EDC==ENABLE
        /* EDC Active, 1/3 */
        if(fu1EDCOn==1)
        {
            FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 3);
        }
        else
        {
          ;
        }
    #endif

    /* DRP Active, 1/7 */
    if((fu1EBDOn==1)||(flu1AHBWLampReq==1))
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 7);
    }
    else
    {
        ;
    }

    /* ABS Active, 1/4 */
    if(fu1ABSOn==1)
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 4);
    }
    else
    {
        ;
    }

    #if __VDC==ENABLE
    /* TCS Active, 1/5 */
    if((fu1BTCSOn==1)||(fu1ETCSOn==1))
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 5);
    }
    else
    {
        ;
    }

    /* VSE Active, 1/6 */
    if((fu1ESPOn==1)||(fu1TCSESPOn==1))
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_FLAG], 6);
    }
    else
    {
        ;
    }
    #endif

    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_WSS_FL]=(UCHAR)(fsu16SpeedOfResol64_fl/64);
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_WSS_FR]=(UCHAR)(fsu16SpeedOfResol64_fr/64);
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_WSS_RL]=(UCHAR)(fsu16SpeedOfResol64_rl/64);
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_WSS_RR]=(UCHAR)(fsu16SpeedOfResol64_rr/64);

    #if __G_SENSOR_TYPE!=NONE
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_AX]=(UCHAR)((CHAR)(a_long_1_1000g/10)); /* E=N*0.01 */
    #else
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_AX]=0;
    #endif

    #if __VDC==ENABLE
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_AY]=(UCHAR)((CHAR)(a_lat_1_1000g/10)); /* E=N*0.01 */
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_YAW]=(UCHAR)((CHAR)(yaw_1_100deg/59));  /* E=N*0.590 */
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_SAS]=(UCHAR)((CHAR)((((LONG)steer_1_10deg)*10)/567)); /* E=N*5.669 */
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_HPA]=(UCHAR)(fs16PosPress1stSignal_1_100Bar/100); /* E=N*1 */
        #ifdef FREEZE_FRAME_ADD
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_BCPP]=(UCHAR)(fs16PosPress2ndSignal_1_100Bar/100); /* E=N*1 */
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_BCPS]=(UCHAR)(fs16PosPress4thSignal_1_100Bar/100); /* E=N*1 */
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_PSP]=(UCHAR)(fs16PosPress3rdSignal_1_100Bar/100); /* E=N*1 */
        #endif
    #else
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_AY]=0;
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_YAW]=0;
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_SAS]=0;
        ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_HPA]=0;
    #endif
    #ifdef FREEZE_FRAME_ADD
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM_MTP]=(UCHAR)(fu16CalVoltMOTOR/100);
    ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM2_FLAG]=0;
    if(fsu1RBCSWLampOnReq==1)
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM2_FLAG],0);
    }
    if(fsu1BuzzerOn==1)
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM2_FLAG], 1);
    }
    if(fsu1AhbDegradedModeFail==1)
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM2_FLAG], 2);
    }
    if(fsu1AhbDefectiveModeFail==1)
    {
        FE_mSetBit(ErrBuffer[SortCnt].ErrMember[U8_DTC_INFORM2_FLAG], 3);
    }
    #endif


    #if __EXTENDED_FREEZE_FRAME_ENABLE==1
        FE_vSaveExtendedFreezeFrame(SortCnt);
    #endif
}

void Diag_5ms_FaultDetect(void)
{
#if 0 //comfile for PJS
	if((Diag_ErrProcBus.Diag_ErrProcIgnOnOffSts==ON)&&(Diag_ErrProcBus.Diag_ErrProcResidualModeEntryFlg == 0))
	{
		if(Diag_ErrProcBus.Diag_ErrProcNormVlvSts.PrimCutPErr==1)
		{
			if(cut1_short_by_curr == 1)
			{
				FE_vArrangeErrorCode(U16_CUT1_SHORT_ERR);
				cut1_short_by_curr = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcNormVlvSts.SecdCutPErr==1)
		{
			if(cut2_short_by_curr == 1)
			{
				FE_vArrangeErrorCode(U16_CUT2_SHORT_ERR);
				cut2_short_by_curr = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.FlOvErr==1)
		{
			if (flo_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_FLO_SHORT_ERR);
				flo_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.FlIvErr==1)
		{
			if (fri_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_FLI_SHORT_ERR);
				fri_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.FrOvErr==1)
		{
			if (fro_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_FRO_SHORT_ERR);
				fro_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.FrIvErr==1)
		{
			if (fri_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_FRI_SHORT_ERR);
				fri_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.RlOvErr==1)
		{
			if (rlo_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_RLO_SHORT_ERR);
				rlo_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.RlIvErr==1)
		{
			if (rli_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_RLI_SHORT_ERR);
				rli_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.RrOvErr==1)
		{
			if (rro_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_RRO_SHORT_ERR);
				rro_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcWhlVlvSts.RrIvErr==1)
		{
			if (rri_short_flg==1)
			{
				FE_vArrangeErrorCode(U16_RRI_SHORT_ERR);
				rri_short_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcTiOut.Ems1TiOutErr==1)
		{
			if (EMS1_timeout_err_flg==1)
			{
				FE_vArrangeErrorCode(U16_EMS1_TIMEOUT_ERR);
				EMS1_timeout_err_flg=0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcTiOut.Ems2TiOutErr==1)
		{
			if (EMS2_timeout_err_flg==1)
			{
				FE_vArrangeErrorCode(U16_EMS2_TIMEOUT_ERR);
				EMS2_timeout_err_flg=0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcTiOut.HcuTiOutErr==1)
		{
			if (HCU_timeout_err_flg ==1)
			{
				FE_vArrangeErrorCode(U16_HCU_TIMEOUT_ERR);
				HCU_timeout_err_flg = 0;
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcEscSwtFail==1)
		{
			if (tcs_vdc_sw_error_flg==1)
			{
				tcs_vdc_sw_error_flg=0;	/* For DTC */
				FE_vArrangeErrorCode(U16_VDC_SW_ERR);
			}
		}
		if(Diag_ErrProcBus.Diag_ErrProcAsicErr==1)
		{
	    	if (comm_asic_over_v_err_flg == 1)
	    	{
	    		FE_vArrangeErrorCode(U16_ASIC_OVER_V_ERR);
	    		comm_asic_over_v_err_flg = 0;
	    	}
		}
		if(Diag_ErrProcBus.Diag_ErrProcVddIcVoltErr==1)
		{
			if (vdd_ic_err_flg==1)
			{
				FE_vArrangeErrorCode(U16_VDD_IC_ERR);
				vdd_ic_err_flg = 0;
			}
		}		
	}
#endif //comfile for PJS	
}

#endif
