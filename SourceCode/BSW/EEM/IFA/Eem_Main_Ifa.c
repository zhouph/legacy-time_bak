/**
 * @defgroup Eem_Main_Ifa Eem_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define EEM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Eem_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Eem_MemMap.h"
#define EEM_MAIN_START_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/** Variable Section (32BIT)**/


#define EEM_MAIN_STOP_SEC_VAR_32BIT
#include "Eem_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define EEM_MAIN_START_SEC_CODE
#include "Eem_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define EEM_MAIN_STOP_SEC_CODE
#include "Eem_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
