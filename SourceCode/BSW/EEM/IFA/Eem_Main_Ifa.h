/**
 * @defgroup Eem_Main_Ifa Eem_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_MAIN_IFA_H_
#define EEM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Eem_Main_Read_Eem_MainBBSVlvErrInfo(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData(data) do \
{ \
    *data = Eem_MainAbsVlvMData; \
}while(0);

#define Eem_Main_Read_Eem_MainEscSwtStInfo(data) do \
{ \
    *data = Eem_MainEscSwtStInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainWhlSpdInfo(data) do \
{ \
    *data = Eem_MainWhlSpdInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsCtrlInfo(data) do \
{ \
    *data = Eem_MainAbsCtrlInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainEscCtrlInfo(data) do \
{ \
    *data = Eem_MainEscCtrlInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainHdcCtrlInfo(data) do \
{ \
    *data = Eem_MainHdcCtrlInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainTcsCtrlInfo(data) do \
{ \
    *data = Eem_MainTcsCtrlInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainRelayMonitorData(data) do \
{ \
    *data = Eem_MainRelayMonitorData; \
}while(0);

#define Eem_Main_Read_Eem_MainSenPwrMonitorData(data) do \
{ \
    *data = Eem_MainSenPwrMonitorData; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData(data) do \
{ \
    *data = Eem_MainPedalMData; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtMonFaultInfo(data) do \
{ \
    *data = Eem_MainSwtMonFaultInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtPFaultInfo(data) do \
{ \
    *data = Eem_MainSwtPFaultInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo(data) do \
{ \
    *data = Eem_MainYAWMOutInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo(data) do \
{ \
    *data = Eem_MainAYMOuitInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo(data) do \
{ \
    *data = Eem_MainAXMOutInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainSASMOutInfo(data) do \
{ \
    *data = Eem_MainSASMOutInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo(data) do \
{ \
    *data = Eem_MainMTRMOutInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData(data) do \
{ \
    *data = Eem_MainSysPwrMonData; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData(data) do \
{ \
    *data = Eem_MainWssMonData; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData(data) do \
{ \
    *data = Eem_MainCanMonData; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo(data) do \
{ \
    *data = Eem_MainMgdErrInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainYawPlauOutput(data) do \
{ \
    *data = Eem_MainYawPlauOutput; \
}while(0);

#define Eem_Main_Read_Eem_MainAxPlauOutput(data) do \
{ \
    *data = Eem_MainAxPlauOutput; \
}while(0);

#define Eem_Main_Read_Eem_MainAyPlauOutput(data) do \
{ \
    *data = Eem_MainAyPlauOutput; \
}while(0);

#define Eem_Main_Read_Eem_MainSasPlauOutput(data) do \
{ \
    *data = Eem_MainSasPlauOutput; \
}while(0);

#define Eem_Main_Read_Eem_MainPressPInfo(data) do \
{ \
    *data = Eem_MainPressPInfo; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_Batt1Fuse_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.Batt1Fuse_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_S2G_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_S2B_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_Short_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_OverTemp_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_ShutdownLine_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlvRelay_CP_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Sim_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Sim_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Sim_Short_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Sim_CurReg_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutP_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutP_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutP_Short_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutP_CurReg_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutS_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutS_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutS_Short_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CutS_CurReg_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Rlv_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Rlv_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Rlv_Short_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_Rlv_CurReg_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CircVlv_Open_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CircVlv_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CircVlv_Short_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainBBSVlvErrInfo_BbsVlv_CircVlv_CurReg_Err(data) do \
{ \
    *data = Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_S2G_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_S2B_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_CP_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_Batt1FuseOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRelay_ShutdownLine_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNO_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNO_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNO_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNO_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNO_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNO_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNO_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNO_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNO_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNO_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNO_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNO_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNO_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNO_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNO_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNO_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNO_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNO_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNO_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNO_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveBAL_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveBAL_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveBAL_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveBAL_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveBAL_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValvePD_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValvePD_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValvePD_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValvePD_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValvePD_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRESP_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRESP_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRESP_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRESP_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRESP_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNC_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNC_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNC_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNC_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFLNC_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNC_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNC_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNC_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNC_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveFRNC_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNC_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNC_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNC_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNC_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRLNC_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNC_Open_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNC_PsvOpen_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNC_Short_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNC_OverTemp_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsVlvMData_AbsVlvM_ValveRRNC_CurReg_Err(data) do \
{ \
    *data = Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Eem_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Eem_Main_Read_Eem_MainEscSwtStInfo_TcsDisabledBySwt(data) do \
{ \
    *data = Eem_MainEscSwtStInfo.TcsDisabledBySwt; \
}while(0);

#define Eem_Main_Read_Eem_MainEscSwtStInfo_HdcEnabledBySwt(data) do \
{ \
    *data = Eem_MainEscSwtStInfo.HdcEnabledBySwt; \
}while(0);

#define Eem_Main_Read_Eem_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Eem_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Eem_Main_Read_Eem_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Eem_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Eem_Main_Read_Eem_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Eem_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Eem_Main_Read_Eem_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Eem_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Eem_Main_Read_Eem_MainAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Eem_MainAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Eem_Main_Read_Eem_MainEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = Eem_MainEscCtrlInfo.EscActFlg; \
}while(0);

#define Eem_Main_Read_Eem_MainHdcCtrlInfo_HdcActFlg(data) do \
{ \
    *data = Eem_MainHdcCtrlInfo.HdcActFlg; \
}while(0);

#define Eem_Main_Read_Eem_MainTcsCtrlInfo_TcsActFlg(data) do \
{ \
    *data = Eem_MainTcsCtrlInfo.TcsActFlg; \
}while(0);

#define Eem_Main_Read_Eem_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    *data = Eem_MainRelayMonitorData.RlyM_HDCRelay_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    *data = Eem_MainRelayMonitorData.RlyM_HDCRelay_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    *data = Eem_MainRelayMonitorData.RlyM_ESSRelay_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    *data = Eem_MainRelayMonitorData.RlyM_ESSRelay_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    *data = Eem_MainSenPwrMonitorData.SenPwrM_5V_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    *data = Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    *data = Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS1_Open_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_PTS1_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS1_Short_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_PTS1_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS2_Open_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_PTS2_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS2_Short_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_PTS2_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS1_SupplyPower_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_PTS1_SupplyPower_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_PTS2_SupplyPower_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_PTS2_SupplyPower_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_VDD3_OverVolt_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_VDD3_OverVolt_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_VDD3_UnderVolt_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_VDD3_UnderVolt_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPedalMData_PedalM_VDD3_OverCurrent_Err(data) do \
{ \
    *data = Eem_MainPedalMData.PedalM_VDD3_OverCurrent_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_FC_Fault_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_FC_Fault_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_FC_Mismatch_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_FC_CRC_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_FC_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_SC_Temp_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_SC_Temp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_SC_Mismatch_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_SC_CRC_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_SC_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP1_SC_Diag_Info(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP1_SC_Diag_Info; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_FC_Fault_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_FC_Fault_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_FC_Mismatch_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_FC_CRC_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_FC_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_SC_Temp_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_SC_Temp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_SC_Mismatch_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_SC_CRC_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_SC_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_CIRP2_SC_Diag_Info(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.CIRP2_SC_Diag_Info; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_FC_Fault_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_FC_Fault_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_FC_Mismatch_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_FC_CRC_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_FC_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_SC_Temp_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_SC_Temp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_SC_Mismatch_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_SC_CRC_Err(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_SC_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPresMonFaultInfo_SIMP_SC_Diag_Info(data) do \
{ \
    *data = Eem_MainPresMonFaultInfo.SIMP_SC_Diag_Info; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err_Info(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err_Info(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err_Info(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicComm_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicComm_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicOsc_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicOsc_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicNvm_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicNvm_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAsicMonFaultInfo_ASICM_AsicGroundLoss_Err(data) do \
{ \
    *data = Eem_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    *data = Eem_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    *data = Eem_MainSwtMonFaultInfo.SWM_HDC_Sw_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    *data = Eem_MainSwtMonFaultInfo.SWM_AVH_Sw_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    *data = Eem_MainSwtMonFaultInfo.SWM_BFL_Sw_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    *data = Eem_MainSwtMonFaultInfo.SWM_PB_Sw_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    *data = Eem_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    *data = Eem_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    *data = Eem_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    *data = Eem_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    *data = Eem_MainYAWMOutInfo.YAWM_Timeout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    *data = Eem_MainYAWMOutInfo.YAWM_Invalid_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    *data = Eem_MainYAWMOutInfo.YAWM_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    *data = Eem_MainYAWMOutInfo.YAWM_Rolling_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    *data = Eem_MainYAWMOutInfo.YAWM_Temperature_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    *data = Eem_MainYAWMOutInfo.YAWM_Range_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    *data = Eem_MainAYMOuitInfo.AYM_Timeout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    *data = Eem_MainAYMOuitInfo.AYM_Invalid_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    *data = Eem_MainAYMOuitInfo.AYM_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    *data = Eem_MainAYMOuitInfo.AYM_Rolling_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    *data = Eem_MainAYMOuitInfo.AYM_Temperature_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    *data = Eem_MainAYMOuitInfo.AYM_Range_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    *data = Eem_MainAXMOutInfo.AXM_Timeout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    *data = Eem_MainAXMOutInfo.AXM_Invalid_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    *data = Eem_MainAXMOutInfo.AXM_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    *data = Eem_MainAXMOutInfo.AXM_Rolling_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    *data = Eem_MainAXMOutInfo.AXM_Temperature_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    *data = Eem_MainAXMOutInfo.AXM_Range_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    *data = Eem_MainSASMOutInfo.SASM_Timeout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    *data = Eem_MainSASMOutInfo.SASM_Invalid_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    *data = Eem_MainSASMOutInfo.SASM_CRC_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    *data = Eem_MainSASMOutInfo.SASM_Rolling_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    *data = Eem_MainSASMOutInfo.SASM_Range_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Power_Open_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Power_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Open_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Phase_U_OverCurret_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Phase_V_OverCurret_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Phase_W_OverCurret_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Calibration_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Calibration_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Short_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Short_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMTRMOutInfo_MTRM_Driver_Err(data) do \
{ \
    *data = Eem_MainMTRMOutInfo.MTRM_Driver_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_OverVolt_1st_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_OverVolt_2st_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_UnderVolt_1st_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_UnderVolt_2st_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_UnderVolt_3st_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_Asic_Vdd_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_Asic_OverVolt_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_Asic_UnderVolt_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_Asic_OverTemp_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_Asic_Gndloss_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_Asic_Comm_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_Asic_Comm_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainSysPwrMonData_SysPwrM_IgnLine_Open_Err(data) do \
{ \
    *data = Eem_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FlOpen_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FlOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FlShort_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FlShort_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FlOverTemp_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FlOverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FlLeakage_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FlLeakage_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FrOpen_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FrOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FrShort_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FrShort_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FrOverTemp_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FrOverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_FrLeakage_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_FrLeakage_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RlOpen_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RlOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RlShort_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RlShort_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RlOverTemp_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RlOverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RlLeakage_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RlLeakage_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RrOpen_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RrOpen_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RrShort_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RrShort_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RrOverTemp_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RrOverTemp_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_RrLeakage_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_RrLeakage_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainWssMonData_WssM_Asic_Comm_Err(data) do \
{ \
    *data = Eem_MainWssMonData.WssM_Asic_Comm_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_MainBusOff_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_SubBusOff_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_MainOverRun_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_SubOverRun_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_EMS1_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_EMS2_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_TCU1_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_TCU5_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_FWD1_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_HCU1_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_HCU2_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_HCU3_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_VSM2_Tout_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_EMS_Invalid_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    *data = Eem_MainCanMonData.CanM_TCU_Invalid_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdInterPwrSuppMonErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdInterPwrSuppMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdClkMonErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdClkMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdBISTErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdBISTErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdFlashMemoryErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdFlashMemoryErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdRAMErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdRAMErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdConfigRegiErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdConfigRegiErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdInputPattMonErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdInputPattMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdOverTempErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdOverTempErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdCPmpVMonErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdCPmpVMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdHBuffCapVErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdHBuffCapVErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdInOutPlauErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdInOutPlauErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMgdErrInfo_MgdCtrlSigMonErr(data) do \
{ \
    *data = Eem_MainMgdErrInfo.MgdCtrlSigMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_ExtPWRSuppMonErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.ExtPWRSuppMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_IntPWRSuppMonErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.IntPWRSuppMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_WatchDogErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.WatchDogErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_FlashMemoryErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.FlashMemoryErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_StartUpTestErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.StartUpTestErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_ConfigRegiTestErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.ConfigRegiTestErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_HWIntegConsisErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.HWIntegConsisErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD1ErrInfo_MagnetLossErr(data) do \
{ \
    *data = Eem_MainMpsD1ErrInfo.MagnetLossErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_ExtPWRSuppMonErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.ExtPWRSuppMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_IntPWRSuppMonErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.IntPWRSuppMonErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_WatchDogErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.WatchDogErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_FlashMemoryErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.FlashMemoryErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_StartUpTestErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.StartUpTestErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_ConfigRegiTestErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.ConfigRegiTestErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_HWIntegConsisErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.HWIntegConsisErr; \
}while(0);

#define Eem_Main_Read_Eem_MainMpsD2ErrInfo_MagnetLossErr(data) do \
{ \
    *data = Eem_MainMpsD2ErrInfo.MagnetLossErr; \
}while(0);

#define Eem_Main_Read_Eem_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    *data = Eem_MainYawPlauOutput.YawPlauModelErr; \
}while(0);

#define Eem_Main_Read_Eem_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    *data = Eem_MainYawPlauOutput.YawPlauNoisekErr; \
}while(0);

#define Eem_Main_Read_Eem_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    *data = Eem_MainYawPlauOutput.YawPlauShockErr; \
}while(0);

#define Eem_Main_Read_Eem_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    *data = Eem_MainYawPlauOutput.YawPlauRangeErr; \
}while(0);

#define Eem_Main_Read_Eem_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    *data = Eem_MainYawPlauOutput.YawPlauStandStillErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    *data = Eem_MainAxPlauOutput.AxPlauOffsetErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    *data = Eem_MainAxPlauOutput.AxPlauDrivingOffsetErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    *data = Eem_MainAxPlauOutput.AxPlauStickErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    *data = Eem_MainAxPlauOutput.AxPlauNoiseErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    *data = Eem_MainAyPlauOutput.AyPlauNoiselErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    *data = Eem_MainAyPlauOutput.AyPlauModelErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    *data = Eem_MainAyPlauOutput.AyPlauShockErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    *data = Eem_MainAyPlauOutput.AyPlauRangeErr; \
}while(0);

#define Eem_Main_Read_Eem_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    *data = Eem_MainAyPlauOutput.AyPlauStandStillErr; \
}while(0);

#define Eem_Main_Read_Eem_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    *data = Eem_MainSasPlauOutput.SasPlauModelErr; \
}while(0);

#define Eem_Main_Read_Eem_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    *data = Eem_MainSasPlauOutput.SasPlauOffsetErr; \
}while(0);

#define Eem_Main_Read_Eem_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    *data = Eem_MainSasPlauOutput.SasPlauStickErr; \
}while(0);

#define Eem_Main_Read_Eem_MainPressPInfo_SimP_Noise_Err(data) do \
{ \
    *data = Eem_MainPressPInfo.SimP_Noise_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPressPInfo_CirP1_Noise_Err(data) do \
{ \
    *data = Eem_MainPressPInfo.CirP1_Noise_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainPressPInfo_CirP2_Noise_Err(data) do \
{ \
    *data = Eem_MainPressPInfo.CirP2_Noise_Err; \
}while(0);

#define Eem_Main_Read_Eem_MainEcuModeSts(data) do \
{ \
    *data = Eem_MainEcuModeSts; \
}while(0);

#define Eem_Main_Read_Eem_MainIgnOnOffSts(data) do \
{ \
    *data = Eem_MainIgnOnOffSts; \
}while(0);

#define Eem_Main_Read_Eem_MainIgnEdgeSts(data) do \
{ \
    *data = Eem_MainIgnEdgeSts; \
}while(0);

#define Eem_Main_Read_Eem_MainDiagClrSrs(data) do \
{ \
    *data = Eem_MainDiagClrSrs; \
}while(0);

#define Eem_Main_Read_Eem_MainDiagSci(data) do \
{ \
    *data = Eem_MainDiagSci; \
}while(0);

#define Eem_Main_Read_Eem_MainDiagAhbSci(data) do \
{ \
    *data = Eem_MainDiagAhbSci; \
}while(0);


/* Set Output DE MAcro Function */
#define Eem_Main_Write_Eem_MainEemFailData(data) do \
{ \
    Eem_MainEemFailData = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData(data) do \
{ \
    Eem_MainEemCtrlInhibitData = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData(data) do \
{ \
    Eem_MainEemSuspectData = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData(data) do \
{ \
    Eem_MainEemEceData = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData(data) do \
{ \
    Eem_MainEemLampData = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_BBSSol(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_BBSSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_ESCSol(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_ESCSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_FrontSol(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_FrontSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_RearSol(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_RearSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_Motor(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_Motor = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_MPS(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_MPS = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_MGD(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_MGD = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_BBSValveRelay(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_BBSValveRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_ESCValveRelay(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_ESCValveRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_ECUHw(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_ECUHw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_ASIC(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_ASIC = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_OverVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_UnderVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_LowVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_LowerVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_SenPwr_12V = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_SenPwr_5V = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_Yaw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_Ay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_Ax(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_Ax = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_Str = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_CirP1(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_CirP1 = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_CirP2(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_CirP2 = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_SimP(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_SimP = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_BLS(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_BLS = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_ESCSw(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_ESCSw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_HDCSw(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_HDCSw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_AVHSw(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_AVHSw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_BrakeLampRelay(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_BrakeLampRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_EssRelay(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_EssRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_GearR(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_GearR = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_Clutch(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_Clutch = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_ParkBrake = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_PedalPDT = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_PedalPDF = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_BrakeFluid(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_BrakeFluid = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_TCSTemp(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_TCSTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_HDCTemp(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_HDCTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_SCCTemp(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_SCCTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_TVBBTemp(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_TVBBTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_MainCanLine(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_MainCanLine = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_SubCanLine(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_SubCanLine = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_EMSTimeOut(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_EMSTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_FWDTimeOut(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_FWDTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_TCUTimeOut(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_TCUTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_HCUTimeOut(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_HCUTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_MCUTimeOut(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_MCUTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_VariantCoding(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_VariantCoding = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_WssFL = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_WssFR = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_WssRL = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_WssRR = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_SameSideWss(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_SameSideWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_DiagonalWss(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_DiagonalWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_FrontWss(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_FrontWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemFailData_Eem_Fail_RearWss(data) do \
{ \
    Eem_MainEemFailData.Eem_Fail_RearWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Cbs(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Ebd(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Abs(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Edc(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Btcs(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Etcs(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Tcs(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Vdc(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Hsa(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Hdc(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Pba(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Avh(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_CtrlIhb_Moc(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_BBS_DiagControlInhibit(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_BBS_DegradeModeFail(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_WssFL = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_WssFR = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_WssRL = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_WssRR = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_SameSideWss(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_SameSideWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_DiagonalWss(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_FrontWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_RearWss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_BBSSol(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_BBSSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_ESCSol(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_ESCSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_FrontSol(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_FrontSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_RearSol(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_RearSol = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_Motor(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_Motor = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_MPS(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_MPS = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_MGD(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_MGD = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_BBSValveRelay(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_ESCValveRelay(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_ECUHw(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_ECUHw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_ASIC(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_ASIC = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_OverVolt(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_OverVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_UnderVolt(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_UnderVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_LowVolt(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_LowVolt = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_SenPwr_5V(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_Yaw(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_Yaw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_Ay(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_Ay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_Ax(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_Ax = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_Str(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_Str = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_CirP1(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_CirP1 = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_CirP2(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_CirP2 = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_SimP(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_SimP = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_BS(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_BS = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_BLS(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_BLS = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_ESCSw(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_ESCSw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_HDCSw(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_HDCSw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_AVHSw(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_AVHSw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_BrakeLampRelay(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_EssRelay(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_EssRelay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_GearR(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_GearR = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_Clutch(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_Clutch = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_ParkBrake(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_ParkBrake = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_PedalPDT(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_PedalPDT = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_PedalPDF(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_PedalPDF = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_BrakeFluid(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_TCSTemp(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_TCSTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_HDCTemp(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_HDCTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_SCCTemp(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_SCCTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_TVBBTemp(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_TVBBTemp = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_MainCanLine(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_MainCanLine = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_SubCanLine(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_SubCanLine = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_EMSTimeOut(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_FWDTimeOut(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_TCUTimeOut(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_HCUTimeOut(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_MCUTimeOut(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemSuspectData_Eem_Suspect_VariantCoding(data) do \
{ \
    Eem_MainEemSuspectData.Eem_Suspect_VariantCoding = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Wss(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Wss = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Yaw(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Yaw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Ay(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Ay = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Ax(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Ax = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Cir1P(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Cir1P = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Cir2P(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Cir2P = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_SimP(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_SimP = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Bls(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Bls = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Pedal(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Pedal = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Motor(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Motor = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Vdc_Sw(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Vdc_Sw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Hdc_Sw(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Hdc_Sw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_GearR_Sw(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_GearR_Sw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemEceData_Eem_Ece_Clutch_Sw(data) do \
{ \
    Eem_MainEemEceData.Eem_Ece_Clutch_Sw = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_EBDLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_EBDLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_ABSLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_ABSLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_TCSLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_TCSLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_TCSOffLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_VDCLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_VDCLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_VDCOffLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_HDCLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_HDCLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_BBSBuzzorRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_BBSBuzzorRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_RBCSLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_AHBLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_AHBLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_ServiceLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_TCSFuncLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_TCSFuncLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_VDCFuncLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_AVHLampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_AVHLampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_AVHILampRequest(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_AVHILampRequest = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_ACCEnable(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_ACCEnable = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Lamp_CDMEnable(data) do \
{ \
    Eem_MainEemLampData.Eem_Lamp_CDMEnable = *data; \
}while(0);

#define Eem_Main_Write_Eem_MainEemLampData_Eem_Buzzor_On(data) do \
{ \
    Eem_MainEemLampData.Eem_Buzzor_On = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_MAIN_IFA_H_ */
/** @} */
