/**
 * @defgroup Eem_SuspcDetn_Ifa Eem_SuspcDetn_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_SuspcDetn_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_SUSPCDETN_IFA_H_
#define EEM_SUSPCDETN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Eem_SuspcDetn_Read_Eem_SuspcDetnEcuModeSts(data) do \
{ \
    *data = Eem_SuspcDetnEcuModeSts; \
}while(0);

#define Eem_SuspcDetn_Read_Eem_SuspcDetnDiagClrSrs(data) do \
{ \
    *data = Eem_SuspcDetnDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define Eem_SuspcDetn_Write_Eem_SuspcDetnSwtStsFSInfo(data) do \
{ \
    Eem_SuspcDetnSwtStsFSInfo = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnCanTimeOutStInfo(data) do \
{ \
    Eem_SuspcDetnCanTimeOutStInfo = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnSwtStsFSInfo_FlexBrkSwtFaultDet(data) do \
{ \
    Eem_SuspcDetnSwtStsFSInfo.FlexBrkSwtFaultDet = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnCanTimeOutStInfo_MaiCanSusDet(data) do \
{ \
    Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnCanTimeOutStInfo_EmsTiOutSusDet(data) do \
{ \
    Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnCanTimeOutStInfo_TcuTiOutSusDet(data) do \
{ \
    Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitVlvSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitVlvSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitIocSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitIocSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitGdSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitGdSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitProxySts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitProxySts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitDiagSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitDiagSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitFsrSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitFsrSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAcmioSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitAcmioSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAcmctlSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitAcmctlSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMspSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitMspSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitNvmSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitNvmSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitWssSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitWssSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPedalSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitPedalSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPressSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitPressSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitRlySts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitRlySts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitSesSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitSesSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitSwtSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitSwtSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPrlySts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitPrlySts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMomSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitMomSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitVlvdSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitVlvdSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitSpimSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitSpimSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAdcifSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitAdcifSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitIcuSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitIcuSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMpsSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitMpsSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitRegSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitRegSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitAsicSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitAsicSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitCanTrcvSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitCanTrcvSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitBbcSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitBbcSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitRbcSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitRbcSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitArbiSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitArbiSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitPctrlSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitPctrlSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitMccSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitMccSts = *data; \
}while(0);

#define Eem_SuspcDetn_Write_Eem_SuspcDetnFuncInhibitVlvActSts(data) do \
{ \
    Eem_SuspcDetnFuncInhibitVlvActSts = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_SUSPCDETN_IFA_H_ */
/** @} */
