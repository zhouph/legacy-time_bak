/**
 * @defgroup Eem_Types Eem_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_TYPES_H_
#define EEM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Eem_SuspcDetnEcuModeSts;
    Diag_HndlrDiagClr_t Eem_SuspcDetnDiagClrSrs;

/* Output Data Element */
    Eem_SuspcDetnSwtStsFSInfo_t Eem_SuspcDetnSwtStsFSInfo;
    Eem_SuspcDetnCanTimeOutStInfo_t Eem_SuspcDetnCanTimeOutStInfo;
    Eem_SuspcDetnFuncInhibitVlvSts_t Eem_SuspcDetnFuncInhibitVlvSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Eem_SuspcDetnFuncInhibitIocSts;
    Eem_SuspcDetnFuncInhibitGdSts_t Eem_SuspcDetnFuncInhibitGdSts;
    Eem_SuspcDetnFuncInhibitProxySts_t Eem_SuspcDetnFuncInhibitProxySts;
    Eem_SuspcDetnFuncInhibitDiagSts_t Eem_SuspcDetnFuncInhibitDiagSts;
    Eem_SuspcDetnFuncInhibitFsrSts_t Eem_SuspcDetnFuncInhibitFsrSts;
    Eem_SuspcDetnFuncInhibitAcmioSts_t Eem_SuspcDetnFuncInhibitAcmioSts;
    Eem_SuspcDetnFuncInhibitAcmctlSts_t Eem_SuspcDetnFuncInhibitAcmctlSts;
    Eem_SuspcDetnFuncInhibitMspSts_t Eem_SuspcDetnFuncInhibitMspSts;
    Eem_SuspcDetnFuncInhibitNvmSts_t Eem_SuspcDetnFuncInhibitNvmSts;
    Eem_SuspcDetnFuncInhibitWssSts_t Eem_SuspcDetnFuncInhibitWssSts;
    Eem_SuspcDetnFuncInhibitPedalSts_t Eem_SuspcDetnFuncInhibitPedalSts;
    Eem_SuspcDetnFuncInhibitPressSts_t Eem_SuspcDetnFuncInhibitPressSts;
    Eem_SuspcDetnFuncInhibitRlySts_t Eem_SuspcDetnFuncInhibitRlySts;
    Eem_SuspcDetnFuncInhibitSesSts_t Eem_SuspcDetnFuncInhibitSesSts;
    Eem_SuspcDetnFuncInhibitSwtSts_t Eem_SuspcDetnFuncInhibitSwtSts;
    Eem_SuspcDetnFuncInhibitPrlySts_t Eem_SuspcDetnFuncInhibitPrlySts;
    Eem_SuspcDetnFuncInhibitMomSts_t Eem_SuspcDetnFuncInhibitMomSts;
    Eem_SuspcDetnFuncInhibitVlvdSts_t Eem_SuspcDetnFuncInhibitVlvdSts;
    Eem_SuspcDetnFuncInhibitSpimSts_t Eem_SuspcDetnFuncInhibitSpimSts;
    Eem_SuspcDetnFuncInhibitAdcifSts_t Eem_SuspcDetnFuncInhibitAdcifSts;
    Eem_SuspcDetnFuncInhibitIcuSts_t Eem_SuspcDetnFuncInhibitIcuSts;
    Eem_SuspcDetnFuncInhibitMpsSts_t Eem_SuspcDetnFuncInhibitMpsSts;
    Eem_SuspcDetnFuncInhibitRegSts_t Eem_SuspcDetnFuncInhibitRegSts;
    Eem_SuspcDetnFuncInhibitAsicSts_t Eem_SuspcDetnFuncInhibitAsicSts;
    Eem_SuspcDetnFuncInhibitCanTrcvSts_t Eem_SuspcDetnFuncInhibitCanTrcvSts;
    Eem_SuspcDetnFuncInhibitBbcSts_t Eem_SuspcDetnFuncInhibitBbcSts;
    Eem_SuspcDetnFuncInhibitRbcSts_t Eem_SuspcDetnFuncInhibitRbcSts;
    Eem_SuspcDetnFuncInhibitArbiSts_t Eem_SuspcDetnFuncInhibitArbiSts;
    Eem_SuspcDetnFuncInhibitPctrlSts_t Eem_SuspcDetnFuncInhibitPctrlSts;
    Eem_SuspcDetnFuncInhibitMccSts_t Eem_SuspcDetnFuncInhibitMccSts;
    Eem_SuspcDetnFuncInhibitVlvActSts_t Eem_SuspcDetnFuncInhibitVlvActSts;
}Eem_SuspcDetn_HdrBusType;

typedef struct
{
/* Input Data Element */
    BbsVlvM_MainBBSVlvErrInfo_t Eem_MainBBSVlvErrInfo;
    AbsVlvM_MainAbsVlvMData_t Eem_MainAbsVlvMData;
    Swt_SenEscSwtStInfo_t Eem_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t Eem_MainWhlSpdInfo;
    Abc_CtrlAbsCtrlInfo_t Eem_MainAbsCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Eem_MainEscCtrlInfo;
    Abc_CtrlHdcCtrlInfo_t Eem_MainHdcCtrlInfo;
    Abc_CtrlTcsCtrlInfo_t Eem_MainTcsCtrlInfo;
    RlyM_MainRelayMonitor_t Eem_MainRelayMonitorData;
    SenPwrM_MainSenPwrMonitor_t Eem_MainSenPwrMonitorData;
    PedalM_MainPedalMData_t Eem_MainPedalMData;
    PressM_MainPresMonFaultInfo_t Eem_MainPresMonFaultInfo;
    EcuHwCtrlM_MainAsicMonFaultInfo_t Eem_MainAsicMonFaultInfo;
    SwtM_MainSwtMonFaultInfo_t Eem_MainSwtMonFaultInfo;
    SwtP_MainSwtPFaultInfo_t Eem_MainSwtPFaultInfo;
    YawM_MainYAWMOutInfo_t Eem_MainYAWMOutInfo;
    AyM_MainAYMOutInfo_t Eem_MainAYMOuitInfo;
    AxM_MainAXMOutInfo_t Eem_MainAXMOutInfo;
    SasM_MainSASMOutInfo_t Eem_MainSASMOutInfo;
    MtrM_MainMTRMOutInfo_t Eem_MainMTRMOutInfo;
    SysPwrM_MainSysPwrMonData_t Eem_MainSysPwrMonData;
    WssM_MainWssMonData_t Eem_MainWssMonData;
    CanM_MainCanMonData_t Eem_MainCanMonData;
    MtrM_MainMgdErrInfo_t Eem_MainMgdErrInfo;
    Mps_TLE5012M_MainMpsD1ErrInfo_t Eem_MainMpsD1ErrInfo;
    Mps_TLE5012M_MainMpsD2ErrInfo_t Eem_MainMpsD2ErrInfo;
    YawP_MainYawPlauOutput_t Eem_MainYawPlauOutput;
    AxP_MainAxPlauOutput_t Eem_MainAxPlauOutput;
    AyP_MainAyPlauOutput_t Eem_MainAyPlauOutput;
    SasP_MainSasPlauOutput_t Eem_MainSasPlauOutput;
    PressP_MainPressPInfo_t Eem_MainPressPInfo;
    Mom_HndlrEcuModeSts_t Eem_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Eem_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Eem_MainIgnEdgeSts;
    Diag_HndlrDiagClr_t Eem_MainDiagClrSrs;
    Diag_HndlrDiagSci_t Eem_MainDiagSci;
    Diag_HndlrDiagAhbSci_t Eem_MainDiagAhbSci;

/* Output Data Element */
    Eem_MainEemFailData_t Eem_MainEemFailData;
    Eem_MainEemCtrlInhibitData_t Eem_MainEemCtrlInhibitData;
    Eem_MainEemSuspectData_t Eem_MainEemSuspectData;
    Eem_MainEemEceData_t Eem_MainEemEceData;
    Eem_MainEemLampData_t Eem_MainEemLampData;
}Eem_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_TYPES_H_ */
/** @} */
