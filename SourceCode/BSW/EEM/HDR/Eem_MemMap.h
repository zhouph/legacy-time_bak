/**
 * @defgroup Eem_MemMap Eem_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_MEMMAP_H_
#define EEM_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (EEM_START_SEC_CODE)
  #undef EEM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_CODE)
  #undef EEM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_CODE_STARTED
    #error "EEM_SEC_CODE not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (EEM_START_SEC_CONST_UNSPECIFIED)
  #undef EEM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_CONST_UNSPECIFIED)
  #undef EEM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_CONST_UNSPECIFIED_STARTED
    #error "EEM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (EEM_START_SEC_VAR_UNSPECIFIED)
  #undef EEM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_VAR_UNSPECIFIED)
  #undef EEM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_VAR_UNSPECIFIED_STARTED
    #error "EEM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_START_SEC_VAR_32BIT)
  #undef EEM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_VAR_32BIT)
  #undef EEM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_VAR_32BIT_STARTED
    #error "EEM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef EEM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef EEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "EEM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_START_SEC_VAR_NOINIT_32BIT)
  #undef EEM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef EEM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "EEM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (EEM_START_SEC_CALIB_UNSPECIFIED)
  #undef EEM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SEC_STARTED
    #error "EEM section not closed"
  #endif
  #define CHK_EEM_SEC_STARTED
  #define CHK_EEM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef EEM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "EEM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SEC_STARTED
  #undef CHK_EEM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (EEM_SUSPCDETN_START_SEC_CODE)
  #undef EEM_SUSPCDETN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_CODE)
  #undef EEM_SUSPCDETN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_CODE_STARTED
    #error "EEM_SUSPCDETN_SEC_CODE not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (EEM_SUSPCDETN_START_SEC_CONST_UNSPECIFIED)
  #undef EEM_SUSPCDETN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_CONST_UNSPECIFIED)
  #undef EEM_SUSPCDETN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_CONST_UNSPECIFIED_STARTED
    #error "EEM_SUSPCDETN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (EEM_SUSPCDETN_START_SEC_VAR_UNSPECIFIED)
  #undef EEM_SUSPCDETN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_VAR_UNSPECIFIED)
  #undef EEM_SUSPCDETN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_VAR_UNSPECIFIED_STARTED
    #error "EEM_SUSPCDETN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_START_SEC_VAR_32BIT)
  #undef EEM_SUSPCDETN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_VAR_32BIT)
  #undef EEM_SUSPCDETN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_VAR_32BIT_STARTED
    #error "EEM_SUSPCDETN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef EEM_SUSPCDETN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "EEM_SUSPCDETN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_START_SEC_VAR_NOINIT_32BIT)
  #undef EEM_SUSPCDETN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef EEM_SUSPCDETN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "EEM_SUSPCDETN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (EEM_SUSPCDETN_START_SEC_CALIB_UNSPECIFIED)
  #undef EEM_SUSPCDETN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_SUSPCDETN_SEC_STARTED
    #error "EEM_SUSPCDETN section not closed"
  #endif
  #define CHK_EEM_SUSPCDETN_SEC_STARTED
  #define CHK_EEM_SUSPCDETN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_SUSPCDETN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef EEM_SUSPCDETN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_SUSPCDETN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "EEM_SUSPCDETN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_SUSPCDETN_SEC_STARTED
  #undef CHK_EEM_SUSPCDETN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (EEM_MAIN_START_SEC_CODE)
  #undef EEM_MAIN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_CODE)
  #undef EEM_MAIN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_CODE_STARTED
    #error "EEM_MAIN_SEC_CODE not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (EEM_MAIN_START_SEC_CONST_UNSPECIFIED)
  #undef EEM_MAIN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_CONST_UNSPECIFIED)
  #undef EEM_MAIN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
    #error "EEM_MAIN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (EEM_MAIN_START_SEC_VAR_UNSPECIFIED)
  #undef EEM_MAIN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED)
  #undef EEM_MAIN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
    #error "EEM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_START_SEC_VAR_32BIT)
  #undef EEM_MAIN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_VAR_32BIT)
  #undef EEM_MAIN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_VAR_32BIT_STARTED
    #error "EEM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef EEM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef EEM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "EEM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_START_SEC_VAR_NOINIT_32BIT)
  #undef EEM_MAIN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef EEM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "EEM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (EEM_MAIN_START_SEC_CALIB_UNSPECIFIED)
  #undef EEM_MAIN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_EEM_MAIN_SEC_STARTED
    #error "EEM_MAIN section not closed"
  #endif
  #define CHK_EEM_MAIN_SEC_STARTED
  #define CHK_EEM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (EEM_MAIN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef EEM_MAIN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_EEM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "EEM_MAIN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_EEM_MAIN_SEC_STARTED
  #undef CHK_EEM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_MEMMAP_H_ */
/** @} */
