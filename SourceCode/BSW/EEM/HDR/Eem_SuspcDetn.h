/**
 * @defgroup Eem_SuspcDetn Eem_SuspcDetn
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_SuspcDetn.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_SUSPCDETN_H_
#define EEM_SUSPCDETN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Types.h"
#include "Eem_Cfg.h"
#include "Eem_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define EEM_SUSPCDETN_MODULE_ID      (0)
 #define EEM_SUSPCDETN_MAJOR_VERSION  (2)
 #define EEM_SUSPCDETN_MINOR_VERSION  (0)
 #define EEM_SUSPCDETN_PATCH_VERSION  (0)
 #define EEM_SUSPCDETN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Eem_SuspcDetn_HdrBusType Eem_SuspcDetnBus;

/* Version Info */
extern const SwcVersionInfo_t Eem_SuspcDetnVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Eem_SuspcDetnEcuModeSts;
extern Diag_HndlrDiagClr_t Eem_SuspcDetnDiagClrSrs;

/* Output Data Element */
extern Eem_SuspcDetnSwtStsFSInfo_t Eem_SuspcDetnSwtStsFSInfo;
extern Eem_SuspcDetnCanTimeOutStInfo_t Eem_SuspcDetnCanTimeOutStInfo;
extern Eem_SuspcDetnFuncInhibitVlvSts_t Eem_SuspcDetnFuncInhibitVlvSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Eem_SuspcDetnFuncInhibitIocSts;
extern Eem_SuspcDetnFuncInhibitGdSts_t Eem_SuspcDetnFuncInhibitGdSts;
extern Eem_SuspcDetnFuncInhibitProxySts_t Eem_SuspcDetnFuncInhibitProxySts;
extern Eem_SuspcDetnFuncInhibitDiagSts_t Eem_SuspcDetnFuncInhibitDiagSts;
extern Eem_SuspcDetnFuncInhibitFsrSts_t Eem_SuspcDetnFuncInhibitFsrSts;
extern Eem_SuspcDetnFuncInhibitAcmioSts_t Eem_SuspcDetnFuncInhibitAcmioSts;
extern Eem_SuspcDetnFuncInhibitAcmctlSts_t Eem_SuspcDetnFuncInhibitAcmctlSts;
extern Eem_SuspcDetnFuncInhibitMspSts_t Eem_SuspcDetnFuncInhibitMspSts;
extern Eem_SuspcDetnFuncInhibitNvmSts_t Eem_SuspcDetnFuncInhibitNvmSts;
extern Eem_SuspcDetnFuncInhibitWssSts_t Eem_SuspcDetnFuncInhibitWssSts;
extern Eem_SuspcDetnFuncInhibitPedalSts_t Eem_SuspcDetnFuncInhibitPedalSts;
extern Eem_SuspcDetnFuncInhibitPressSts_t Eem_SuspcDetnFuncInhibitPressSts;
extern Eem_SuspcDetnFuncInhibitRlySts_t Eem_SuspcDetnFuncInhibitRlySts;
extern Eem_SuspcDetnFuncInhibitSesSts_t Eem_SuspcDetnFuncInhibitSesSts;
extern Eem_SuspcDetnFuncInhibitSwtSts_t Eem_SuspcDetnFuncInhibitSwtSts;
extern Eem_SuspcDetnFuncInhibitPrlySts_t Eem_SuspcDetnFuncInhibitPrlySts;
extern Eem_SuspcDetnFuncInhibitMomSts_t Eem_SuspcDetnFuncInhibitMomSts;
extern Eem_SuspcDetnFuncInhibitVlvdSts_t Eem_SuspcDetnFuncInhibitVlvdSts;
extern Eem_SuspcDetnFuncInhibitSpimSts_t Eem_SuspcDetnFuncInhibitSpimSts;
extern Eem_SuspcDetnFuncInhibitAdcifSts_t Eem_SuspcDetnFuncInhibitAdcifSts;
extern Eem_SuspcDetnFuncInhibitIcuSts_t Eem_SuspcDetnFuncInhibitIcuSts;
extern Eem_SuspcDetnFuncInhibitMpsSts_t Eem_SuspcDetnFuncInhibitMpsSts;
extern Eem_SuspcDetnFuncInhibitRegSts_t Eem_SuspcDetnFuncInhibitRegSts;
extern Eem_SuspcDetnFuncInhibitAsicSts_t Eem_SuspcDetnFuncInhibitAsicSts;
extern Eem_SuspcDetnFuncInhibitCanTrcvSts_t Eem_SuspcDetnFuncInhibitCanTrcvSts;
extern Eem_SuspcDetnFuncInhibitBbcSts_t Eem_SuspcDetnFuncInhibitBbcSts;
extern Eem_SuspcDetnFuncInhibitRbcSts_t Eem_SuspcDetnFuncInhibitRbcSts;
extern Eem_SuspcDetnFuncInhibitArbiSts_t Eem_SuspcDetnFuncInhibitArbiSts;
extern Eem_SuspcDetnFuncInhibitPctrlSts_t Eem_SuspcDetnFuncInhibitPctrlSts;
extern Eem_SuspcDetnFuncInhibitMccSts_t Eem_SuspcDetnFuncInhibitMccSts;
extern Eem_SuspcDetnFuncInhibitVlvActSts_t Eem_SuspcDetnFuncInhibitVlvActSts;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Eem_SuspcDetn_Init(void);
extern void Eem_SuspcDetn(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_SUSPCDETN_H_ */
/** @} */
