/**
 * @defgroup Eem_Main Eem_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Eem_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EEM_MAIN_H_
#define EEM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Eem_Types.h"
#include "Eem_Cfg.h"
#include "Eem_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define EEM_MAIN_MODULE_ID      (0)
 #define EEM_MAIN_MAJOR_VERSION  (2)
 #define EEM_MAIN_MINOR_VERSION  (0)
 #define EEM_MAIN_PATCH_VERSION  (0)
 #define EEM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Eem_Main_HdrBusType Eem_MainBus;

/* Version Info */
extern const SwcVersionInfo_t Eem_MainVersionInfo;

/* Input Data Element */
extern BbsVlvM_MainBBSVlvErrInfo_t Eem_MainBBSVlvErrInfo;
extern AbsVlvM_MainAbsVlvMData_t Eem_MainAbsVlvMData;
extern Swt_SenEscSwtStInfo_t Eem_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Eem_MainWhlSpdInfo;
extern Abc_CtrlAbsCtrlInfo_t Eem_MainAbsCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Eem_MainEscCtrlInfo;
extern Abc_CtrlHdcCtrlInfo_t Eem_MainHdcCtrlInfo;
extern Abc_CtrlTcsCtrlInfo_t Eem_MainTcsCtrlInfo;
extern RlyM_MainRelayMonitor_t Eem_MainRelayMonitorData;
extern SenPwrM_MainSenPwrMonitor_t Eem_MainSenPwrMonitorData;
extern PedalM_MainPedalMData_t Eem_MainPedalMData;
extern PressM_MainPresMonFaultInfo_t Eem_MainPresMonFaultInfo;
extern EcuHwCtrlM_MainAsicMonFaultInfo_t Eem_MainAsicMonFaultInfo;
extern SwtM_MainSwtMonFaultInfo_t Eem_MainSwtMonFaultInfo;
extern SwtP_MainSwtPFaultInfo_t Eem_MainSwtPFaultInfo;
extern YawM_MainYAWMOutInfo_t Eem_MainYAWMOutInfo;
extern AyM_MainAYMOutInfo_t Eem_MainAYMOuitInfo;
extern AxM_MainAXMOutInfo_t Eem_MainAXMOutInfo;
extern SasM_MainSASMOutInfo_t Eem_MainSASMOutInfo;
extern MtrM_MainMTRMOutInfo_t Eem_MainMTRMOutInfo;
extern SysPwrM_MainSysPwrMonData_t Eem_MainSysPwrMonData;
extern WssM_MainWssMonData_t Eem_MainWssMonData;
extern CanM_MainCanMonData_t Eem_MainCanMonData;
extern MtrM_MainMgdErrInfo_t Eem_MainMgdErrInfo;
extern Mps_TLE5012M_MainMpsD1ErrInfo_t Eem_MainMpsD1ErrInfo;
extern Mps_TLE5012M_MainMpsD2ErrInfo_t Eem_MainMpsD2ErrInfo;
extern YawP_MainYawPlauOutput_t Eem_MainYawPlauOutput;
extern AxP_MainAxPlauOutput_t Eem_MainAxPlauOutput;
extern AyP_MainAyPlauOutput_t Eem_MainAyPlauOutput;
extern SasP_MainSasPlauOutput_t Eem_MainSasPlauOutput;
extern PressP_MainPressPInfo_t Eem_MainPressPInfo;
extern Mom_HndlrEcuModeSts_t Eem_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Eem_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Eem_MainIgnEdgeSts;
extern Diag_HndlrDiagClr_t Eem_MainDiagClrSrs;
extern Diag_HndlrDiagSci_t Eem_MainDiagSci;
extern Diag_HndlrDiagAhbSci_t Eem_MainDiagAhbSci;

/* Output Data Element */
extern Eem_MainEemFailData_t Eem_MainEemFailData;
extern Eem_MainEemCtrlInhibitData_t Eem_MainEemCtrlInhibitData;
extern Eem_MainEemSuspectData_t Eem_MainEemSuspectData;
extern Eem_MainEemEceData_t Eem_MainEemEceData;
extern Eem_MainEemLampData_t Eem_MainEemLampData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Eem_Main_Init(void);
extern void Eem_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EEM_MAIN_H_ */
/** @} */
