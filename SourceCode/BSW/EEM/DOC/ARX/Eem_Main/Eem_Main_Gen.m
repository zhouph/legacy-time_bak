Eem_MainBBSVlvErrInfo = Simulink.Bus;
DeList={
    'Batt1Fuse_Open_Err'
    'BbsVlvRelay_Open_Err'
    'BbsVlvRelay_S2G_Err'
    'BbsVlvRelay_S2B_Err'
    'BbsVlvRelay_Short_Err'
    'BbsVlvRelay_OverTemp_Err'
    'BbsVlvRelay_ShutdownLine_Err'
    'BbsVlvRelay_CP_Err'
    'BbsVlv_Sim_Open_Err'
    'BbsVlv_Sim_PsvOpen_Err'
    'BbsVlv_Sim_Short_Err'
    'BbsVlv_Sim_CurReg_Err'
    'BbsVlv_CutP_Open_Err'
    'BbsVlv_CutP_PsvOpen_Err'
    'BbsVlv_CutP_Short_Err'
    'BbsVlv_CutP_CurReg_Err'
    'BbsVlv_CutS_Open_Err'
    'BbsVlv_CutS_PsvOpen_Err'
    'BbsVlv_CutS_Short_Err'
    'BbsVlv_CutS_CurReg_Err'
    'BbsVlv_Rlv_Open_Err'
    'BbsVlv_Rlv_PsvOpen_Err'
    'BbsVlv_Rlv_Short_Err'
    'BbsVlv_Rlv_CurReg_Err'
    'BbsVlv_CircVlv_Open_Err'
    'BbsVlv_CircVlv_PsvOpen_Err'
    'BbsVlv_CircVlv_Short_Err'
    'BbsVlv_CircVlv_CurReg_Err'
    };
Eem_MainBBSVlvErrInfo = CreateBus(Eem_MainBBSVlvErrInfo, DeList);
clear DeList;

Eem_MainAbsVlvMData = Simulink.Bus;
DeList={
    'AbsVlvM_ValveRelay_Open_Err'
    'AbsVlvM_ValveRelay_S2G_Err'
    'AbsVlvM_ValveRelay_S2B_Err'
    'AbsVlvM_ValveRelay_CP_Err'
    'AbsVlvM_ValveRelay_Short_Err'
    'AbsVlvM_ValveRelay_Batt1FuseOpen_Err'
    'AbsVlvM_ValveRelay_OverTemp_Err'
    'AbsVlvM_ValveRelay_ShutdownLine_Err'
    'AbsVlvM_ValveFLNO_Open_Err'
    'AbsVlvM_ValveFLNO_PsvOpen_Err'
    'AbsVlvM_ValveFLNO_Short_Err'
    'AbsVlvM_ValveFLNO_OverTemp_Err'
    'AbsVlvM_ValveFLNO_CurReg_Err'
    'AbsVlvM_ValveFRNO_Open_Err'
    'AbsVlvM_ValveFRNO_PsvOpen_Err'
    'AbsVlvM_ValveFRNO_Short_Err'
    'AbsVlvM_ValveFRNO_OverTemp_Err'
    'AbsVlvM_ValveFRNO_CurReg_Err'
    'AbsVlvM_ValveRLNO_Open_Err'
    'AbsVlvM_ValveRLNO_PsvOpen_Err'
    'AbsVlvM_ValveRLNO_Short_Err'
    'AbsVlvM_ValveRLNO_OverTemp_Err'
    'AbsVlvM_ValveRLNO_CurReg_Err'
    'AbsVlvM_ValveRRNO_Open_Err'
    'AbsVlvM_ValveRRNO_PsvOpen_Err'
    'AbsVlvM_ValveRRNO_Short_Err'
    'AbsVlvM_ValveRRNO_OverTemp_Err'
    'AbsVlvM_ValveRRNO_CurReg_Err'
    'AbsVlvM_ValveBAL_Open_Err'
    'AbsVlvM_ValveBAL_PsvOpen_Err'
    'AbsVlvM_ValveBAL_Short_Err'
    'AbsVlvM_ValveBAL_OverTemp_Err'
    'AbsVlvM_ValveBAL_CurReg_Err'
    'AbsVlvM_ValvePD_Open_Err'
    'AbsVlvM_ValvePD_PsvOpen_Err'
    'AbsVlvM_ValvePD_Short_Err'
    'AbsVlvM_ValvePD_OverTemp_Err'
    'AbsVlvM_ValvePD_CurReg_Err'
    'AbsVlvM_ValveRESP_Open_Err'
    'AbsVlvM_ValveRESP_PsvOpen_Err'
    'AbsVlvM_ValveRESP_Short_Err'
    'AbsVlvM_ValveRESP_OverTemp_Err'
    'AbsVlvM_ValveRESP_CurReg_Err'
    'AbsVlvM_ValveFLNC_Open_Err'
    'AbsVlvM_ValveFLNC_PsvOpen_Err'
    'AbsVlvM_ValveFLNC_Short_Err'
    'AbsVlvM_ValveFLNC_OverTemp_Err'
    'AbsVlvM_ValveFLNC_CurReg_Err'
    'AbsVlvM_ValveFRNC_Open_Err'
    'AbsVlvM_ValveFRNC_PsvOpen_Err'
    'AbsVlvM_ValveFRNC_Short_Err'
    'AbsVlvM_ValveFRNC_OverTemp_Err'
    'AbsVlvM_ValveFRNC_CurReg_Err'
    'AbsVlvM_ValveRLNC_Open_Err'
    'AbsVlvM_ValveRLNC_PsvOpen_Err'
    'AbsVlvM_ValveRLNC_Short_Err'
    'AbsVlvM_ValveRLNC_OverTemp_Err'
    'AbsVlvM_ValveRLNC_CurReg_Err'
    'AbsVlvM_ValveRRNC_Open_Err'
    'AbsVlvM_ValveRRNC_PsvOpen_Err'
    'AbsVlvM_ValveRRNC_Short_Err'
    'AbsVlvM_ValveRRNC_OverTemp_Err'
    'AbsVlvM_ValveRRNC_CurReg_Err'
    };
Eem_MainAbsVlvMData = CreateBus(Eem_MainAbsVlvMData, DeList);
clear DeList;

Eem_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
Eem_MainEscSwtStInfo = CreateBus(Eem_MainEscSwtStInfo, DeList);
clear DeList;

Eem_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Eem_MainWhlSpdInfo = CreateBus(Eem_MainWhlSpdInfo, DeList);
clear DeList;

Eem_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    };
Eem_MainAbsCtrlInfo = CreateBus(Eem_MainAbsCtrlInfo, DeList);
clear DeList;

Eem_MainEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    };
Eem_MainEscCtrlInfo = CreateBus(Eem_MainEscCtrlInfo, DeList);
clear DeList;

Eem_MainHdcCtrlInfo = Simulink.Bus;
DeList={
    'HdcActFlg'
    };
Eem_MainHdcCtrlInfo = CreateBus(Eem_MainHdcCtrlInfo, DeList);
clear DeList;

Eem_MainTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    };
Eem_MainTcsCtrlInfo = CreateBus(Eem_MainTcsCtrlInfo, DeList);
clear DeList;

Eem_MainRelayMonitorData = Simulink.Bus;
DeList={
    'RlyM_HDCRelay_Open_Err'
    'RlyM_HDCRelay_Short_Err'
    'RlyM_ESSRelay_Open_Err'
    'RlyM_ESSRelay_Short_Err'
    };
Eem_MainRelayMonitorData = CreateBus(Eem_MainRelayMonitorData, DeList);
clear DeList;

Eem_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
Eem_MainSenPwrMonitorData = CreateBus(Eem_MainSenPwrMonitorData, DeList);
clear DeList;

Eem_MainPedalMData = Simulink.Bus;
DeList={
    'PedalM_PTS1_Open_Err'
    'PedalM_PTS1_Short_Err'
    'PedalM_PTS2_Open_Err'
    'PedalM_PTS2_Short_Err'
    'PedalM_PTS1_SupplyPower_Err'
    'PedalM_PTS2_SupplyPower_Err'
    'PedalM_VDD3_OverVolt_Err'
    'PedalM_VDD3_UnderVolt_Err'
    'PedalM_VDD3_OverCurrent_Err'
    };
Eem_MainPedalMData = CreateBus(Eem_MainPedalMData, DeList);
clear DeList;

Eem_MainPresMonFaultInfo = Simulink.Bus;
DeList={
    'CIRP1_FC_Fault_Err'
    'CIRP1_FC_Mismatch_Err'
    'CIRP1_FC_CRC_Err'
    'CIRP1_SC_Temp_Err'
    'CIRP1_SC_Mismatch_Err'
    'CIRP1_SC_CRC_Err'
    'CIRP1_SC_Diag_Info'
    'CIRP2_FC_Fault_Err'
    'CIRP2_FC_Mismatch_Err'
    'CIRP2_FC_CRC_Err'
    'CIRP2_SC_Temp_Err'
    'CIRP2_SC_Mismatch_Err'
    'CIRP2_SC_CRC_Err'
    'CIRP2_SC_Diag_Info'
    'SIMP_FC_Fault_Err'
    'SIMP_FC_Mismatch_Err'
    'SIMP_FC_CRC_Err'
    'SIMP_SC_Temp_Err'
    'SIMP_SC_Mismatch_Err'
    'SIMP_SC_CRC_Err'
    'SIMP_SC_Diag_Info'
    };
Eem_MainPresMonFaultInfo = CreateBus(Eem_MainPresMonFaultInfo, DeList);
clear DeList;

Eem_MainAsicMonFaultInfo = Simulink.Bus;
DeList={
    'ASICM_AsicOverVolt_Err'
    'ASICM_AsicOverVolt_Err_Info'
    'ASICM_AsicUnderVolt_Err'
    'ASICM_AsicUnderVolt_Err_Info'
    'ASICM_AsicOverTemp_Err'
    'ASICM_AsicOverTemp_Err_Info'
    'ASICM_AsicComm_Err'
    'ASICM_AsicOsc_Err'
    'ASICM_AsicNvm_Err'
    'ASICM_AsicGroundLoss_Err'
    };
Eem_MainAsicMonFaultInfo = CreateBus(Eem_MainAsicMonFaultInfo, DeList);
clear DeList;

Eem_MainSwtMonFaultInfo = Simulink.Bus;
DeList={
    'SWM_ESCOFF_Sw_Err'
    'SWM_HDC_Sw_Err'
    'SWM_AVH_Sw_Err'
    'SWM_BFL_Sw_Err'
    'SWM_PB_Sw_Err'
    };
Eem_MainSwtMonFaultInfo = CreateBus(Eem_MainSwtMonFaultInfo, DeList);
clear DeList;

Eem_MainSwtPFaultInfo = Simulink.Bus;
DeList={
    'SWM_BLS_Sw_HighStick_Err'
    'SWM_BLS_Sw_LowStick_Err'
    'SWM_BS_Sw_HighStick_Err'
    'SWM_BS_Sw_LowStick_Err'
    };
Eem_MainSwtPFaultInfo = CreateBus(Eem_MainSwtPFaultInfo, DeList);
clear DeList;

Eem_MainYAWMOutInfo = Simulink.Bus;
DeList={
    'YAWM_Timeout_Err'
    'YAWM_Invalid_Err'
    'YAWM_CRC_Err'
    'YAWM_Rolling_Err'
    'YAWM_Temperature_Err'
    'YAWM_Range_Err'
    };
Eem_MainYAWMOutInfo = CreateBus(Eem_MainYAWMOutInfo, DeList);
clear DeList;

Eem_MainAYMOuitInfo = Simulink.Bus;
DeList={
    'AYM_Timeout_Err'
    'AYM_Invalid_Err'
    'AYM_CRC_Err'
    'AYM_Rolling_Err'
    'AYM_Temperature_Err'
    'AYM_Range_Err'
    };
Eem_MainAYMOuitInfo = CreateBus(Eem_MainAYMOuitInfo, DeList);
clear DeList;

Eem_MainAXMOutInfo = Simulink.Bus;
DeList={
    'AXM_Timeout_Err'
    'AXM_Invalid_Err'
    'AXM_CRC_Err'
    'AXM_Rolling_Err'
    'AXM_Temperature_Err'
    'AXM_Range_Err'
    };
Eem_MainAXMOutInfo = CreateBus(Eem_MainAXMOutInfo, DeList);
clear DeList;

Eem_MainSASMOutInfo = Simulink.Bus;
DeList={
    'SASM_Timeout_Err'
    'SASM_Invalid_Err'
    'SASM_CRC_Err'
    'SASM_Rolling_Err'
    'SASM_Range_Err'
    };
Eem_MainSASMOutInfo = CreateBus(Eem_MainSASMOutInfo, DeList);
clear DeList;

Eem_MainMTRMOutInfo = Simulink.Bus;
DeList={
    'MTRM_Power_Open_Err'
    'MTRM_Open_Err'
    'MTRM_Phase_U_OverCurret_Err'
    'MTRM_Phase_V_OverCurret_Err'
    'MTRM_Phase_W_OverCurret_Err'
    'MTRM_Calibration_Err'
    'MTRM_Short_Err'
    'MTRM_Driver_Err'
    };
Eem_MainMTRMOutInfo = CreateBus(Eem_MainMTRMOutInfo, DeList);
clear DeList;

Eem_MainSysPwrMonData = Simulink.Bus;
DeList={
    'SysPwrM_OverVolt_1st_Err'
    'SysPwrM_OverVolt_2st_Err'
    'SysPwrM_UnderVolt_1st_Err'
    'SysPwrM_UnderVolt_2st_Err'
    'SysPwrM_UnderVolt_3st_Err'
    'SysPwrM_Asic_Vdd_Err'
    'SysPwrM_Asic_OverVolt_Err'
    'SysPwrM_Asic_UnderVolt_Err'
    'SysPwrM_Asic_OverTemp_Err'
    'SysPwrM_Asic_Gndloss_Err'
    'SysPwrM_Asic_Comm_Err'
    'SysPwrM_IgnLine_Open_Err'
    };
Eem_MainSysPwrMonData = CreateBus(Eem_MainSysPwrMonData, DeList);
clear DeList;

Eem_MainWssMonData = Simulink.Bus;
DeList={
    'WssM_FlOpen_Err'
    'WssM_FlShort_Err'
    'WssM_FlOverTemp_Err'
    'WssM_FlLeakage_Err'
    'WssM_FrOpen_Err'
    'WssM_FrShort_Err'
    'WssM_FrOverTemp_Err'
    'WssM_FrLeakage_Err'
    'WssM_RlOpen_Err'
    'WssM_RlShort_Err'
    'WssM_RlOverTemp_Err'
    'WssM_RlLeakage_Err'
    'WssM_RrOpen_Err'
    'WssM_RrShort_Err'
    'WssM_RrOverTemp_Err'
    'WssM_RrLeakage_Err'
    'WssM_Asic_Comm_Err'
    };
Eem_MainWssMonData = CreateBus(Eem_MainWssMonData, DeList);
clear DeList;

Eem_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    'CanM_SubBusOff_Err'
    'CanM_MainOverRun_Err'
    'CanM_SubOverRun_Err'
    'CanM_EMS1_Tout_Err'
    'CanM_EMS2_Tout_Err'
    'CanM_TCU1_Tout_Err'
    'CanM_TCU5_Tout_Err'
    'CanM_FWD1_Tout_Err'
    'CanM_HCU1_Tout_Err'
    'CanM_HCU2_Tout_Err'
    'CanM_HCU3_Tout_Err'
    'CanM_VSM2_Tout_Err'
    'CanM_EMS_Invalid_Err'
    'CanM_TCU_Invalid_Err'
    };
Eem_MainCanMonData = CreateBus(Eem_MainCanMonData, DeList);
clear DeList;

Eem_MainMgdErrInfo = Simulink.Bus;
DeList={
    'MgdInterPwrSuppMonErr'
    'MgdClkMonErr'
    'MgdBISTErr'
    'MgdFlashMemoryErr'
    'MgdRAMErr'
    'MgdConfigRegiErr'
    'MgdInputPattMonErr'
    'MgdOverTempErr'
    'MgdCPmpVMonErr'
    'MgdHBuffCapVErr'
    'MgdInOutPlauErr'
    'MgdCtrlSigMonErr'
    };
Eem_MainMgdErrInfo = CreateBus(Eem_MainMgdErrInfo, DeList);
clear DeList;

Eem_MainMpsD1ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Eem_MainMpsD1ErrInfo = CreateBus(Eem_MainMpsD1ErrInfo, DeList);
clear DeList;

Eem_MainMpsD2ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Eem_MainMpsD2ErrInfo = CreateBus(Eem_MainMpsD2ErrInfo, DeList);
clear DeList;

Eem_MainYawPlauOutput = Simulink.Bus;
DeList={
    'YawPlauModelErr'
    'YawPlauNoisekErr'
    'YawPlauShockErr'
    'YawPlauRangeErr'
    'YawPlauStandStillErr'
    };
Eem_MainYawPlauOutput = CreateBus(Eem_MainYawPlauOutput, DeList);
clear DeList;

Eem_MainAxPlauOutput = Simulink.Bus;
DeList={
    'AxPlauOffsetErr'
    'AxPlauDrivingOffsetErr'
    'AxPlauStickErr'
    'AxPlauNoiseErr'
    };
Eem_MainAxPlauOutput = CreateBus(Eem_MainAxPlauOutput, DeList);
clear DeList;

Eem_MainAyPlauOutput = Simulink.Bus;
DeList={
    'AyPlauNoiselErr'
    'AyPlauModelErr'
    'AyPlauShockErr'
    'AyPlauRangeErr'
    'AyPlauStandStillErr'
    };
Eem_MainAyPlauOutput = CreateBus(Eem_MainAyPlauOutput, DeList);
clear DeList;

Eem_MainSasPlauOutput = Simulink.Bus;
DeList={
    'SasPlauModelErr'
    'SasPlauOffsetErr'
    'SasPlauStickErr'
    };
Eem_MainSasPlauOutput = CreateBus(Eem_MainSasPlauOutput, DeList);
clear DeList;

Eem_MainPressPInfo = Simulink.Bus;
DeList={
    'SimP_Noise_Err'
    'CirP1_Noise_Err'
    'CirP2_Noise_Err'
    };
Eem_MainPressPInfo = CreateBus(Eem_MainPressPInfo, DeList);
clear DeList;

Eem_MainEcuModeSts = Simulink.Bus;
DeList={'Eem_MainEcuModeSts'};
Eem_MainEcuModeSts = CreateBus(Eem_MainEcuModeSts, DeList);
clear DeList;

Eem_MainIgnOnOffSts = Simulink.Bus;
DeList={'Eem_MainIgnOnOffSts'};
Eem_MainIgnOnOffSts = CreateBus(Eem_MainIgnOnOffSts, DeList);
clear DeList;

Eem_MainIgnEdgeSts = Simulink.Bus;
DeList={'Eem_MainIgnEdgeSts'};
Eem_MainIgnEdgeSts = CreateBus(Eem_MainIgnEdgeSts, DeList);
clear DeList;

Eem_MainDiagClrSrs = Simulink.Bus;
DeList={'Eem_MainDiagClrSrs'};
Eem_MainDiagClrSrs = CreateBus(Eem_MainDiagClrSrs, DeList);
clear DeList;

Eem_MainDiagSci = Simulink.Bus;
DeList={'Eem_MainDiagSci'};
Eem_MainDiagSci = CreateBus(Eem_MainDiagSci, DeList);
clear DeList;

Eem_MainDiagAhbSci = Simulink.Bus;
DeList={'Eem_MainDiagAhbSci'};
Eem_MainDiagAhbSci = CreateBus(Eem_MainDiagAhbSci, DeList);
clear DeList;

Eem_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Eem_MainEemFailData = CreateBus(Eem_MainEemFailData, DeList);
clear DeList;

Eem_MainEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Eem_MainEemCtrlInhibitData = CreateBus(Eem_MainEemCtrlInhibitData, DeList);
clear DeList;

Eem_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
Eem_MainEemSuspectData = CreateBus(Eem_MainEemSuspectData, DeList);
clear DeList;

Eem_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
Eem_MainEemEceData = CreateBus(Eem_MainEemEceData, DeList);
clear DeList;

Eem_MainEemLampData = Simulink.Bus;
DeList={
    'Eem_Lamp_EBDLampRequest'
    'Eem_Lamp_ABSLampRequest'
    'Eem_Lamp_TCSLampRequest'
    'Eem_Lamp_TCSOffLampRequest'
    'Eem_Lamp_VDCLampRequest'
    'Eem_Lamp_VDCOffLampRequest'
    'Eem_Lamp_HDCLampRequest'
    'Eem_Lamp_BBSBuzzorRequest'
    'Eem_Lamp_RBCSLampRequest'
    'Eem_Lamp_AHBLampRequest'
    'Eem_Lamp_ServiceLampRequest'
    'Eem_Lamp_TCSFuncLampRequest'
    'Eem_Lamp_VDCFuncLampRequest'
    'Eem_Lamp_AVHLampRequest'
    'Eem_Lamp_AVHILampRequest'
    'Eem_Lamp_ACCEnable'
    'Eem_Lamp_CDMEnable'
    'Eem_Buzzor_On'
    };
Eem_MainEemLampData = CreateBus(Eem_MainEemLampData, DeList);
clear DeList;

