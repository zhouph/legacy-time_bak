Eem_SuspcDetnEcuModeSts = Simulink.Bus;
DeList={'Eem_SuspcDetnEcuModeSts'};
Eem_SuspcDetnEcuModeSts = CreateBus(Eem_SuspcDetnEcuModeSts, DeList);
clear DeList;

Eem_SuspcDetnDiagClrSrs = Simulink.Bus;
DeList={'Eem_SuspcDetnDiagClrSrs'};
Eem_SuspcDetnDiagClrSrs = CreateBus(Eem_SuspcDetnDiagClrSrs, DeList);
clear DeList;

Eem_SuspcDetnSwtStsFSInfo = Simulink.Bus;
DeList={
    'FlexBrkSwtFaultDet'
    };
Eem_SuspcDetnSwtStsFSInfo = CreateBus(Eem_SuspcDetnSwtStsFSInfo, DeList);
clear DeList;

Eem_SuspcDetnCanTimeOutStInfo = Simulink.Bus;
DeList={
    'MaiCanSusDet'
    'EmsTiOutSusDet'
    'TcuTiOutSusDet'
    };
Eem_SuspcDetnCanTimeOutStInfo = CreateBus(Eem_SuspcDetnCanTimeOutStInfo, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvSts'};
Eem_SuspcDetnFuncInhibitVlvSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitIocSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitIocSts'};
Eem_SuspcDetnFuncInhibitIocSts = CreateBus(Eem_SuspcDetnFuncInhibitIocSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitGdSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitGdSts'};
Eem_SuspcDetnFuncInhibitGdSts = CreateBus(Eem_SuspcDetnFuncInhibitGdSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitProxySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitProxySts'};
Eem_SuspcDetnFuncInhibitProxySts = CreateBus(Eem_SuspcDetnFuncInhibitProxySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitDiagSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitDiagSts'};
Eem_SuspcDetnFuncInhibitDiagSts = CreateBus(Eem_SuspcDetnFuncInhibitDiagSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitFsrSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitFsrSts'};
Eem_SuspcDetnFuncInhibitFsrSts = CreateBus(Eem_SuspcDetnFuncInhibitFsrSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAcmioSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAcmioSts'};
Eem_SuspcDetnFuncInhibitAcmioSts = CreateBus(Eem_SuspcDetnFuncInhibitAcmioSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAcmctlSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAcmctlSts'};
Eem_SuspcDetnFuncInhibitAcmctlSts = CreateBus(Eem_SuspcDetnFuncInhibitAcmctlSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMspSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMspSts'};
Eem_SuspcDetnFuncInhibitMspSts = CreateBus(Eem_SuspcDetnFuncInhibitMspSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitNvmSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitNvmSts'};
Eem_SuspcDetnFuncInhibitNvmSts = CreateBus(Eem_SuspcDetnFuncInhibitNvmSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitWssSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitWssSts'};
Eem_SuspcDetnFuncInhibitWssSts = CreateBus(Eem_SuspcDetnFuncInhibitWssSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPedalSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPedalSts'};
Eem_SuspcDetnFuncInhibitPedalSts = CreateBus(Eem_SuspcDetnFuncInhibitPedalSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPressSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPressSts'};
Eem_SuspcDetnFuncInhibitPressSts = CreateBus(Eem_SuspcDetnFuncInhibitPressSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitRlySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitRlySts'};
Eem_SuspcDetnFuncInhibitRlySts = CreateBus(Eem_SuspcDetnFuncInhibitRlySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitSesSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitSesSts'};
Eem_SuspcDetnFuncInhibitSesSts = CreateBus(Eem_SuspcDetnFuncInhibitSesSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitSwtSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitSwtSts'};
Eem_SuspcDetnFuncInhibitSwtSts = CreateBus(Eem_SuspcDetnFuncInhibitSwtSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPrlySts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPrlySts'};
Eem_SuspcDetnFuncInhibitPrlySts = CreateBus(Eem_SuspcDetnFuncInhibitPrlySts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMomSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMomSts'};
Eem_SuspcDetnFuncInhibitMomSts = CreateBus(Eem_SuspcDetnFuncInhibitMomSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvdSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvdSts'};
Eem_SuspcDetnFuncInhibitVlvdSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvdSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitSpimSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitSpimSts'};
Eem_SuspcDetnFuncInhibitSpimSts = CreateBus(Eem_SuspcDetnFuncInhibitSpimSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAdcifSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAdcifSts'};
Eem_SuspcDetnFuncInhibitAdcifSts = CreateBus(Eem_SuspcDetnFuncInhibitAdcifSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitIcuSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitIcuSts'};
Eem_SuspcDetnFuncInhibitIcuSts = CreateBus(Eem_SuspcDetnFuncInhibitIcuSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMpsSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMpsSts'};
Eem_SuspcDetnFuncInhibitMpsSts = CreateBus(Eem_SuspcDetnFuncInhibitMpsSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitRegSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitRegSts'};
Eem_SuspcDetnFuncInhibitRegSts = CreateBus(Eem_SuspcDetnFuncInhibitRegSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitAsicSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitAsicSts'};
Eem_SuspcDetnFuncInhibitAsicSts = CreateBus(Eem_SuspcDetnFuncInhibitAsicSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitCanTrcvSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitCanTrcvSts'};
Eem_SuspcDetnFuncInhibitCanTrcvSts = CreateBus(Eem_SuspcDetnFuncInhibitCanTrcvSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitBbcSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitBbcSts'};
Eem_SuspcDetnFuncInhibitBbcSts = CreateBus(Eem_SuspcDetnFuncInhibitBbcSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitRbcSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitRbcSts'};
Eem_SuspcDetnFuncInhibitRbcSts = CreateBus(Eem_SuspcDetnFuncInhibitRbcSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitArbiSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitArbiSts'};
Eem_SuspcDetnFuncInhibitArbiSts = CreateBus(Eem_SuspcDetnFuncInhibitArbiSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitPctrlSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitPctrlSts'};
Eem_SuspcDetnFuncInhibitPctrlSts = CreateBus(Eem_SuspcDetnFuncInhibitPctrlSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitMccSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitMccSts'};
Eem_SuspcDetnFuncInhibitMccSts = CreateBus(Eem_SuspcDetnFuncInhibitMccSts, DeList);
clear DeList;

Eem_SuspcDetnFuncInhibitVlvActSts = Simulink.Bus;
DeList={'Eem_SuspcDetnFuncInhibitVlvActSts'};
Eem_SuspcDetnFuncInhibitVlvActSts = CreateBus(Eem_SuspcDetnFuncInhibitVlvActSts, DeList);
clear DeList;

