/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Eem_SuspcDetn.h"
#include "Eem_Main.h"

int main(void)
{
    Eem_SuspcDetn_Init();
    Eem_Main_Init();

    while(1)
    {
        Eem_SuspcDetn();
        Eem_Main();
    }
}