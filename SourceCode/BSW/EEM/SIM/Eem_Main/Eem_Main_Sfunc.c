#define S_FUNCTION_NAME      Eem_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          288
#define WidthOutputPort         161

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Eem_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Eem_MainBBSVlvErrInfo.Batt1Fuse_Open_Err = input[0];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_Open_Err = input[1];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2G_Err = input[2];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_S2B_Err = input[3];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_Short_Err = input[4];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_OverTemp_Err = input[5];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_ShutdownLine_Err = input[6];
    Eem_MainBBSVlvErrInfo.BbsVlvRelay_CP_Err = input[7];
    Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Open_Err = input[8];
    Eem_MainBBSVlvErrInfo.BbsVlv_Sim_PsvOpen_Err = input[9];
    Eem_MainBBSVlvErrInfo.BbsVlv_Sim_Short_Err = input[10];
    Eem_MainBBSVlvErrInfo.BbsVlv_Sim_CurReg_Err = input[11];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Open_Err = input[12];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutP_PsvOpen_Err = input[13];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutP_Short_Err = input[14];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutP_CurReg_Err = input[15];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Open_Err = input[16];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutS_PsvOpen_Err = input[17];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutS_Short_Err = input[18];
    Eem_MainBBSVlvErrInfo.BbsVlv_CutS_CurReg_Err = input[19];
    Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Open_Err = input[20];
    Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_PsvOpen_Err = input[21];
    Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_Short_Err = input[22];
    Eem_MainBBSVlvErrInfo.BbsVlv_Rlv_CurReg_Err = input[23];
    Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Open_Err = input[24];
    Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_PsvOpen_Err = input[25];
    Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_Short_Err = input[26];
    Eem_MainBBSVlvErrInfo.BbsVlv_CircVlv_CurReg_Err = input[27];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Open_Err = input[28];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2G_Err = input[29];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_S2B_Err = input[30];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_CP_Err = input[31];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Short_Err = input[32];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_Batt1FuseOpen_Err = input[33];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_OverTemp_Err = input[34];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRelay_ShutdownLine_Err = input[35];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Open_Err = input[36];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_PsvOpen_Err = input[37];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_Short_Err = input[38];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_OverTemp_Err = input[39];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNO_CurReg_Err = input[40];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Open_Err = input[41];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_PsvOpen_Err = input[42];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_Short_Err = input[43];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_OverTemp_Err = input[44];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNO_CurReg_Err = input[45];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Open_Err = input[46];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_PsvOpen_Err = input[47];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_Short_Err = input[48];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_OverTemp_Err = input[49];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNO_CurReg_Err = input[50];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Open_Err = input[51];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_PsvOpen_Err = input[52];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_Short_Err = input[53];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_OverTemp_Err = input[54];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNO_CurReg_Err = input[55];
    Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Open_Err = input[56];
    Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_PsvOpen_Err = input[57];
    Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_Short_Err = input[58];
    Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_OverTemp_Err = input[59];
    Eem_MainAbsVlvMData.AbsVlvM_ValveBAL_CurReg_Err = input[60];
    Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Open_Err = input[61];
    Eem_MainAbsVlvMData.AbsVlvM_ValvePD_PsvOpen_Err = input[62];
    Eem_MainAbsVlvMData.AbsVlvM_ValvePD_Short_Err = input[63];
    Eem_MainAbsVlvMData.AbsVlvM_ValvePD_OverTemp_Err = input[64];
    Eem_MainAbsVlvMData.AbsVlvM_ValvePD_CurReg_Err = input[65];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Open_Err = input[66];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_PsvOpen_Err = input[67];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_Short_Err = input[68];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_OverTemp_Err = input[69];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRESP_CurReg_Err = input[70];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Open_Err = input[71];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_PsvOpen_Err = input[72];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_Short_Err = input[73];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_OverTemp_Err = input[74];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFLNC_CurReg_Err = input[75];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Open_Err = input[76];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_PsvOpen_Err = input[77];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_Short_Err = input[78];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_OverTemp_Err = input[79];
    Eem_MainAbsVlvMData.AbsVlvM_ValveFRNC_CurReg_Err = input[80];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Open_Err = input[81];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_PsvOpen_Err = input[82];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_Short_Err = input[83];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_OverTemp_Err = input[84];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRLNC_CurReg_Err = input[85];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Open_Err = input[86];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_PsvOpen_Err = input[87];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_Short_Err = input[88];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_OverTemp_Err = input[89];
    Eem_MainAbsVlvMData.AbsVlvM_ValveRRNC_CurReg_Err = input[90];
    Eem_MainEscSwtStInfo.EscDisabledBySwt = input[91];
    Eem_MainEscSwtStInfo.TcsDisabledBySwt = input[92];
    Eem_MainEscSwtStInfo.HdcEnabledBySwt = input[93];
    Eem_MainWhlSpdInfo.FlWhlSpd = input[94];
    Eem_MainWhlSpdInfo.FrWhlSpd = input[95];
    Eem_MainWhlSpdInfo.RlWhlSpd = input[96];
    Eem_MainWhlSpdInfo.RrlWhlSpd = input[97];
    Eem_MainAbsCtrlInfo.AbsActFlg = input[98];
    Eem_MainEscCtrlInfo.EscActFlg = input[99];
    Eem_MainHdcCtrlInfo.HdcActFlg = input[100];
    Eem_MainTcsCtrlInfo.TcsActFlg = input[101];
    Eem_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = input[102];
    Eem_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = input[103];
    Eem_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = input[104];
    Eem_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = input[105];
    Eem_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[106];
    Eem_MainSenPwrMonitorData.SenPwrM_5V_Err = input[107];
    Eem_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = input[108];
    Eem_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = input[109];
    Eem_MainPedalMData.PedalM_PTS1_Open_Err = input[110];
    Eem_MainPedalMData.PedalM_PTS1_Short_Err = input[111];
    Eem_MainPedalMData.PedalM_PTS2_Open_Err = input[112];
    Eem_MainPedalMData.PedalM_PTS2_Short_Err = input[113];
    Eem_MainPedalMData.PedalM_PTS1_SupplyPower_Err = input[114];
    Eem_MainPedalMData.PedalM_PTS2_SupplyPower_Err = input[115];
    Eem_MainPedalMData.PedalM_VDD3_OverVolt_Err = input[116];
    Eem_MainPedalMData.PedalM_VDD3_UnderVolt_Err = input[117];
    Eem_MainPedalMData.PedalM_VDD3_OverCurrent_Err = input[118];
    Eem_MainPresMonFaultInfo.CIRP1_FC_Fault_Err = input[119];
    Eem_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = input[120];
    Eem_MainPresMonFaultInfo.CIRP1_FC_CRC_Err = input[121];
    Eem_MainPresMonFaultInfo.CIRP1_SC_Temp_Err = input[122];
    Eem_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err = input[123];
    Eem_MainPresMonFaultInfo.CIRP1_SC_CRC_Err = input[124];
    Eem_MainPresMonFaultInfo.CIRP1_SC_Diag_Info = input[125];
    Eem_MainPresMonFaultInfo.CIRP2_FC_Fault_Err = input[126];
    Eem_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = input[127];
    Eem_MainPresMonFaultInfo.CIRP2_FC_CRC_Err = input[128];
    Eem_MainPresMonFaultInfo.CIRP2_SC_Temp_Err = input[129];
    Eem_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err = input[130];
    Eem_MainPresMonFaultInfo.CIRP2_SC_CRC_Err = input[131];
    Eem_MainPresMonFaultInfo.CIRP2_SC_Diag_Info = input[132];
    Eem_MainPresMonFaultInfo.SIMP_FC_Fault_Err = input[133];
    Eem_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err = input[134];
    Eem_MainPresMonFaultInfo.SIMP_FC_CRC_Err = input[135];
    Eem_MainPresMonFaultInfo.SIMP_SC_Temp_Err = input[136];
    Eem_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err = input[137];
    Eem_MainPresMonFaultInfo.SIMP_SC_CRC_Err = input[138];
    Eem_MainPresMonFaultInfo.SIMP_SC_Diag_Info = input[139];
    Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = input[140];
    Eem_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info = input[141];
    Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = input[142];
    Eem_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info = input[143];
    Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = input[144];
    Eem_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info = input[145];
    Eem_MainAsicMonFaultInfo.ASICM_AsicComm_Err = input[146];
    Eem_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = input[147];
    Eem_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = input[148];
    Eem_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = input[149];
    Eem_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = input[150];
    Eem_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = input[151];
    Eem_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = input[152];
    Eem_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = input[153];
    Eem_MainSwtMonFaultInfo.SWM_PB_Sw_Err = input[154];
    Eem_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = input[155];
    Eem_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = input[156];
    Eem_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = input[157];
    Eem_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = input[158];
    Eem_MainYAWMOutInfo.YAWM_Timeout_Err = input[159];
    Eem_MainYAWMOutInfo.YAWM_Invalid_Err = input[160];
    Eem_MainYAWMOutInfo.YAWM_CRC_Err = input[161];
    Eem_MainYAWMOutInfo.YAWM_Rolling_Err = input[162];
    Eem_MainYAWMOutInfo.YAWM_Temperature_Err = input[163];
    Eem_MainYAWMOutInfo.YAWM_Range_Err = input[164];
    Eem_MainAYMOuitInfo.AYM_Timeout_Err = input[165];
    Eem_MainAYMOuitInfo.AYM_Invalid_Err = input[166];
    Eem_MainAYMOuitInfo.AYM_CRC_Err = input[167];
    Eem_MainAYMOuitInfo.AYM_Rolling_Err = input[168];
    Eem_MainAYMOuitInfo.AYM_Temperature_Err = input[169];
    Eem_MainAYMOuitInfo.AYM_Range_Err = input[170];
    Eem_MainAXMOutInfo.AXM_Timeout_Err = input[171];
    Eem_MainAXMOutInfo.AXM_Invalid_Err = input[172];
    Eem_MainAXMOutInfo.AXM_CRC_Err = input[173];
    Eem_MainAXMOutInfo.AXM_Rolling_Err = input[174];
    Eem_MainAXMOutInfo.AXM_Temperature_Err = input[175];
    Eem_MainAXMOutInfo.AXM_Range_Err = input[176];
    Eem_MainSASMOutInfo.SASM_Timeout_Err = input[177];
    Eem_MainSASMOutInfo.SASM_Invalid_Err = input[178];
    Eem_MainSASMOutInfo.SASM_CRC_Err = input[179];
    Eem_MainSASMOutInfo.SASM_Rolling_Err = input[180];
    Eem_MainSASMOutInfo.SASM_Range_Err = input[181];
    Eem_MainMTRMOutInfo.MTRM_Power_Open_Err = input[182];
    Eem_MainMTRMOutInfo.MTRM_Open_Err = input[183];
    Eem_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err = input[184];
    Eem_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err = input[185];
    Eem_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err = input[186];
    Eem_MainMTRMOutInfo.MTRM_Calibration_Err = input[187];
    Eem_MainMTRMOutInfo.MTRM_Short_Err = input[188];
    Eem_MainMTRMOutInfo.MTRM_Driver_Err = input[189];
    Eem_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = input[190];
    Eem_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = input[191];
    Eem_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err = input[192];
    Eem_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = input[193];
    Eem_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = input[194];
    Eem_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err = input[195];
    Eem_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err = input[196];
    Eem_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err = input[197];
    Eem_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err = input[198];
    Eem_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err = input[199];
    Eem_MainSysPwrMonData.SysPwrM_Asic_Comm_Err = input[200];
    Eem_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = input[201];
    Eem_MainWssMonData.WssM_FlOpen_Err = input[202];
    Eem_MainWssMonData.WssM_FlShort_Err = input[203];
    Eem_MainWssMonData.WssM_FlOverTemp_Err = input[204];
    Eem_MainWssMonData.WssM_FlLeakage_Err = input[205];
    Eem_MainWssMonData.WssM_FrOpen_Err = input[206];
    Eem_MainWssMonData.WssM_FrShort_Err = input[207];
    Eem_MainWssMonData.WssM_FrOverTemp_Err = input[208];
    Eem_MainWssMonData.WssM_FrLeakage_Err = input[209];
    Eem_MainWssMonData.WssM_RlOpen_Err = input[210];
    Eem_MainWssMonData.WssM_RlShort_Err = input[211];
    Eem_MainWssMonData.WssM_RlOverTemp_Err = input[212];
    Eem_MainWssMonData.WssM_RlLeakage_Err = input[213];
    Eem_MainWssMonData.WssM_RrOpen_Err = input[214];
    Eem_MainWssMonData.WssM_RrShort_Err = input[215];
    Eem_MainWssMonData.WssM_RrOverTemp_Err = input[216];
    Eem_MainWssMonData.WssM_RrLeakage_Err = input[217];
    Eem_MainWssMonData.WssM_Asic_Comm_Err = input[218];
    Eem_MainCanMonData.CanM_MainBusOff_Err = input[219];
    Eem_MainCanMonData.CanM_SubBusOff_Err = input[220];
    Eem_MainCanMonData.CanM_MainOverRun_Err = input[221];
    Eem_MainCanMonData.CanM_SubOverRun_Err = input[222];
    Eem_MainCanMonData.CanM_EMS1_Tout_Err = input[223];
    Eem_MainCanMonData.CanM_EMS2_Tout_Err = input[224];
    Eem_MainCanMonData.CanM_TCU1_Tout_Err = input[225];
    Eem_MainCanMonData.CanM_TCU5_Tout_Err = input[226];
    Eem_MainCanMonData.CanM_FWD1_Tout_Err = input[227];
    Eem_MainCanMonData.CanM_HCU1_Tout_Err = input[228];
    Eem_MainCanMonData.CanM_HCU2_Tout_Err = input[229];
    Eem_MainCanMonData.CanM_HCU3_Tout_Err = input[230];
    Eem_MainCanMonData.CanM_VSM2_Tout_Err = input[231];
    Eem_MainCanMonData.CanM_EMS_Invalid_Err = input[232];
    Eem_MainCanMonData.CanM_TCU_Invalid_Err = input[233];
    Eem_MainMgdErrInfo.MgdInterPwrSuppMonErr = input[234];
    Eem_MainMgdErrInfo.MgdClkMonErr = input[235];
    Eem_MainMgdErrInfo.MgdBISTErr = input[236];
    Eem_MainMgdErrInfo.MgdFlashMemoryErr = input[237];
    Eem_MainMgdErrInfo.MgdRAMErr = input[238];
    Eem_MainMgdErrInfo.MgdConfigRegiErr = input[239];
    Eem_MainMgdErrInfo.MgdInputPattMonErr = input[240];
    Eem_MainMgdErrInfo.MgdOverTempErr = input[241];
    Eem_MainMgdErrInfo.MgdCPmpVMonErr = input[242];
    Eem_MainMgdErrInfo.MgdHBuffCapVErr = input[243];
    Eem_MainMgdErrInfo.MgdInOutPlauErr = input[244];
    Eem_MainMgdErrInfo.MgdCtrlSigMonErr = input[245];
    Eem_MainMpsD1ErrInfo.ExtPWRSuppMonErr = input[246];
    Eem_MainMpsD1ErrInfo.IntPWRSuppMonErr = input[247];
    Eem_MainMpsD1ErrInfo.WatchDogErr = input[248];
    Eem_MainMpsD1ErrInfo.FlashMemoryErr = input[249];
    Eem_MainMpsD1ErrInfo.StartUpTestErr = input[250];
    Eem_MainMpsD1ErrInfo.ConfigRegiTestErr = input[251];
    Eem_MainMpsD1ErrInfo.HWIntegConsisErr = input[252];
    Eem_MainMpsD1ErrInfo.MagnetLossErr = input[253];
    Eem_MainMpsD2ErrInfo.ExtPWRSuppMonErr = input[254];
    Eem_MainMpsD2ErrInfo.IntPWRSuppMonErr = input[255];
    Eem_MainMpsD2ErrInfo.WatchDogErr = input[256];
    Eem_MainMpsD2ErrInfo.FlashMemoryErr = input[257];
    Eem_MainMpsD2ErrInfo.StartUpTestErr = input[258];
    Eem_MainMpsD2ErrInfo.ConfigRegiTestErr = input[259];
    Eem_MainMpsD2ErrInfo.HWIntegConsisErr = input[260];
    Eem_MainMpsD2ErrInfo.MagnetLossErr = input[261];
    Eem_MainYawPlauOutput.YawPlauModelErr = input[262];
    Eem_MainYawPlauOutput.YawPlauNoisekErr = input[263];
    Eem_MainYawPlauOutput.YawPlauShockErr = input[264];
    Eem_MainYawPlauOutput.YawPlauRangeErr = input[265];
    Eem_MainYawPlauOutput.YawPlauStandStillErr = input[266];
    Eem_MainAxPlauOutput.AxPlauOffsetErr = input[267];
    Eem_MainAxPlauOutput.AxPlauDrivingOffsetErr = input[268];
    Eem_MainAxPlauOutput.AxPlauStickErr = input[269];
    Eem_MainAxPlauOutput.AxPlauNoiseErr = input[270];
    Eem_MainAyPlauOutput.AyPlauNoiselErr = input[271];
    Eem_MainAyPlauOutput.AyPlauModelErr = input[272];
    Eem_MainAyPlauOutput.AyPlauShockErr = input[273];
    Eem_MainAyPlauOutput.AyPlauRangeErr = input[274];
    Eem_MainAyPlauOutput.AyPlauStandStillErr = input[275];
    Eem_MainSasPlauOutput.SasPlauModelErr = input[276];
    Eem_MainSasPlauOutput.SasPlauOffsetErr = input[277];
    Eem_MainSasPlauOutput.SasPlauStickErr = input[278];
    Eem_MainPressPInfo.SimP_Noise_Err = input[279];
    Eem_MainPressPInfo.CirP1_Noise_Err = input[280];
    Eem_MainPressPInfo.CirP2_Noise_Err = input[281];
    Eem_MainEcuModeSts = input[282];
    Eem_MainIgnOnOffSts = input[283];
    Eem_MainIgnEdgeSts = input[284];
    Eem_MainDiagClrSrs = input[285];
    Eem_MainDiagSci = input[286];
    Eem_MainDiagAhbSci = input[287];

    Eem_Main();


    output[0] = Eem_MainEemFailData.Eem_Fail_BBSSol;
    output[1] = Eem_MainEemFailData.Eem_Fail_ESCSol;
    output[2] = Eem_MainEemFailData.Eem_Fail_FrontSol;
    output[3] = Eem_MainEemFailData.Eem_Fail_RearSol;
    output[4] = Eem_MainEemFailData.Eem_Fail_Motor;
    output[5] = Eem_MainEemFailData.Eem_Fail_MPS;
    output[6] = Eem_MainEemFailData.Eem_Fail_MGD;
    output[7] = Eem_MainEemFailData.Eem_Fail_BBSValveRelay;
    output[8] = Eem_MainEemFailData.Eem_Fail_ESCValveRelay;
    output[9] = Eem_MainEemFailData.Eem_Fail_ECUHw;
    output[10] = Eem_MainEemFailData.Eem_Fail_ASIC;
    output[11] = Eem_MainEemFailData.Eem_Fail_OverVolt;
    output[12] = Eem_MainEemFailData.Eem_Fail_UnderVolt;
    output[13] = Eem_MainEemFailData.Eem_Fail_LowVolt;
    output[14] = Eem_MainEemFailData.Eem_Fail_LowerVolt;
    output[15] = Eem_MainEemFailData.Eem_Fail_SenPwr_12V;
    output[16] = Eem_MainEemFailData.Eem_Fail_SenPwr_5V;
    output[17] = Eem_MainEemFailData.Eem_Fail_Yaw;
    output[18] = Eem_MainEemFailData.Eem_Fail_Ay;
    output[19] = Eem_MainEemFailData.Eem_Fail_Ax;
    output[20] = Eem_MainEemFailData.Eem_Fail_Str;
    output[21] = Eem_MainEemFailData.Eem_Fail_CirP1;
    output[22] = Eem_MainEemFailData.Eem_Fail_CirP2;
    output[23] = Eem_MainEemFailData.Eem_Fail_SimP;
    output[24] = Eem_MainEemFailData.Eem_Fail_BLS;
    output[25] = Eem_MainEemFailData.Eem_Fail_ESCSw;
    output[26] = Eem_MainEemFailData.Eem_Fail_HDCSw;
    output[27] = Eem_MainEemFailData.Eem_Fail_AVHSw;
    output[28] = Eem_MainEemFailData.Eem_Fail_BrakeLampRelay;
    output[29] = Eem_MainEemFailData.Eem_Fail_EssRelay;
    output[30] = Eem_MainEemFailData.Eem_Fail_GearR;
    output[31] = Eem_MainEemFailData.Eem_Fail_Clutch;
    output[32] = Eem_MainEemFailData.Eem_Fail_ParkBrake;
    output[33] = Eem_MainEemFailData.Eem_Fail_PedalPDT;
    output[34] = Eem_MainEemFailData.Eem_Fail_PedalPDF;
    output[35] = Eem_MainEemFailData.Eem_Fail_BrakeFluid;
    output[36] = Eem_MainEemFailData.Eem_Fail_TCSTemp;
    output[37] = Eem_MainEemFailData.Eem_Fail_HDCTemp;
    output[38] = Eem_MainEemFailData.Eem_Fail_SCCTemp;
    output[39] = Eem_MainEemFailData.Eem_Fail_TVBBTemp;
    output[40] = Eem_MainEemFailData.Eem_Fail_MainCanLine;
    output[41] = Eem_MainEemFailData.Eem_Fail_SubCanLine;
    output[42] = Eem_MainEemFailData.Eem_Fail_EMSTimeOut;
    output[43] = Eem_MainEemFailData.Eem_Fail_FWDTimeOut;
    output[44] = Eem_MainEemFailData.Eem_Fail_TCUTimeOut;
    output[45] = Eem_MainEemFailData.Eem_Fail_HCUTimeOut;
    output[46] = Eem_MainEemFailData.Eem_Fail_MCUTimeOut;
    output[47] = Eem_MainEemFailData.Eem_Fail_VariantCoding;
    output[48] = Eem_MainEemFailData.Eem_Fail_WssFL;
    output[49] = Eem_MainEemFailData.Eem_Fail_WssFR;
    output[50] = Eem_MainEemFailData.Eem_Fail_WssRL;
    output[51] = Eem_MainEemFailData.Eem_Fail_WssRR;
    output[52] = Eem_MainEemFailData.Eem_Fail_SameSideWss;
    output[53] = Eem_MainEemFailData.Eem_Fail_DiagonalWss;
    output[54] = Eem_MainEemFailData.Eem_Fail_FrontWss;
    output[55] = Eem_MainEemFailData.Eem_Fail_RearWss;
    output[56] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
    output[57] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
    output[58] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Abs;
    output[59] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Edc;
    output[60] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
    output[61] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
    output[62] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
    output[63] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
    output[64] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
    output[65] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
    output[66] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Pba;
    output[67] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Avh;
    output[68] = Eem_MainEemCtrlInhibitData.Eem_CtrlIhb_Moc;
    output[69] = Eem_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
    output[70] = Eem_MainEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
    output[71] = Eem_MainEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
    output[72] = Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
    output[73] = Eem_MainEemSuspectData.Eem_Suspect_WssFL;
    output[74] = Eem_MainEemSuspectData.Eem_Suspect_WssFR;
    output[75] = Eem_MainEemSuspectData.Eem_Suspect_WssRL;
    output[76] = Eem_MainEemSuspectData.Eem_Suspect_WssRR;
    output[77] = Eem_MainEemSuspectData.Eem_Suspect_SameSideWss;
    output[78] = Eem_MainEemSuspectData.Eem_Suspect_DiagonalWss;
    output[79] = Eem_MainEemSuspectData.Eem_Suspect_FrontWss;
    output[80] = Eem_MainEemSuspectData.Eem_Suspect_RearWss;
    output[81] = Eem_MainEemSuspectData.Eem_Suspect_BBSSol;
    output[82] = Eem_MainEemSuspectData.Eem_Suspect_ESCSol;
    output[83] = Eem_MainEemSuspectData.Eem_Suspect_FrontSol;
    output[84] = Eem_MainEemSuspectData.Eem_Suspect_RearSol;
    output[85] = Eem_MainEemSuspectData.Eem_Suspect_Motor;
    output[86] = Eem_MainEemSuspectData.Eem_Suspect_MPS;
    output[87] = Eem_MainEemSuspectData.Eem_Suspect_MGD;
    output[88] = Eem_MainEemSuspectData.Eem_Suspect_BBSValveRelay;
    output[89] = Eem_MainEemSuspectData.Eem_Suspect_ESCValveRelay;
    output[90] = Eem_MainEemSuspectData.Eem_Suspect_ECUHw;
    output[91] = Eem_MainEemSuspectData.Eem_Suspect_ASIC;
    output[92] = Eem_MainEemSuspectData.Eem_Suspect_OverVolt;
    output[93] = Eem_MainEemSuspectData.Eem_Suspect_UnderVolt;
    output[94] = Eem_MainEemSuspectData.Eem_Suspect_LowVolt;
    output[95] = Eem_MainEemSuspectData.Eem_Suspect_SenPwr_12V;
    output[96] = Eem_MainEemSuspectData.Eem_Suspect_SenPwr_5V;
    output[97] = Eem_MainEemSuspectData.Eem_Suspect_Yaw;
    output[98] = Eem_MainEemSuspectData.Eem_Suspect_Ay;
    output[99] = Eem_MainEemSuspectData.Eem_Suspect_Ax;
    output[100] = Eem_MainEemSuspectData.Eem_Suspect_Str;
    output[101] = Eem_MainEemSuspectData.Eem_Suspect_CirP1;
    output[102] = Eem_MainEemSuspectData.Eem_Suspect_CirP2;
    output[103] = Eem_MainEemSuspectData.Eem_Suspect_SimP;
    output[104] = Eem_MainEemSuspectData.Eem_Suspect_BS;
    output[105] = Eem_MainEemSuspectData.Eem_Suspect_BLS;
    output[106] = Eem_MainEemSuspectData.Eem_Suspect_ESCSw;
    output[107] = Eem_MainEemSuspectData.Eem_Suspect_HDCSw;
    output[108] = Eem_MainEemSuspectData.Eem_Suspect_AVHSw;
    output[109] = Eem_MainEemSuspectData.Eem_Suspect_BrakeLampRelay;
    output[110] = Eem_MainEemSuspectData.Eem_Suspect_EssRelay;
    output[111] = Eem_MainEemSuspectData.Eem_Suspect_GearR;
    output[112] = Eem_MainEemSuspectData.Eem_Suspect_Clutch;
    output[113] = Eem_MainEemSuspectData.Eem_Suspect_ParkBrake;
    output[114] = Eem_MainEemSuspectData.Eem_Suspect_PedalPDT;
    output[115] = Eem_MainEemSuspectData.Eem_Suspect_PedalPDF;
    output[116] = Eem_MainEemSuspectData.Eem_Suspect_BrakeFluid;
    output[117] = Eem_MainEemSuspectData.Eem_Suspect_TCSTemp;
    output[118] = Eem_MainEemSuspectData.Eem_Suspect_HDCTemp;
    output[119] = Eem_MainEemSuspectData.Eem_Suspect_SCCTemp;
    output[120] = Eem_MainEemSuspectData.Eem_Suspect_TVBBTemp;
    output[121] = Eem_MainEemSuspectData.Eem_Suspect_MainCanLine;
    output[122] = Eem_MainEemSuspectData.Eem_Suspect_SubCanLine;
    output[123] = Eem_MainEemSuspectData.Eem_Suspect_EMSTimeOut;
    output[124] = Eem_MainEemSuspectData.Eem_Suspect_FWDTimeOut;
    output[125] = Eem_MainEemSuspectData.Eem_Suspect_TCUTimeOut;
    output[126] = Eem_MainEemSuspectData.Eem_Suspect_HCUTimeOut;
    output[127] = Eem_MainEemSuspectData.Eem_Suspect_MCUTimeOut;
    output[128] = Eem_MainEemSuspectData.Eem_Suspect_VariantCoding;
    output[129] = Eem_MainEemEceData.Eem_Ece_Wss;
    output[130] = Eem_MainEemEceData.Eem_Ece_Yaw;
    output[131] = Eem_MainEemEceData.Eem_Ece_Ay;
    output[132] = Eem_MainEemEceData.Eem_Ece_Ax;
    output[133] = Eem_MainEemEceData.Eem_Ece_Cir1P;
    output[134] = Eem_MainEemEceData.Eem_Ece_Cir2P;
    output[135] = Eem_MainEemEceData.Eem_Ece_SimP;
    output[136] = Eem_MainEemEceData.Eem_Ece_Bls;
    output[137] = Eem_MainEemEceData.Eem_Ece_Pedal;
    output[138] = Eem_MainEemEceData.Eem_Ece_Motor;
    output[139] = Eem_MainEemEceData.Eem_Ece_Vdc_Sw;
    output[140] = Eem_MainEemEceData.Eem_Ece_Hdc_Sw;
    output[141] = Eem_MainEemEceData.Eem_Ece_GearR_Sw;
    output[142] = Eem_MainEemEceData.Eem_Ece_Clutch_Sw;
    output[143] = Eem_MainEemLampData.Eem_Lamp_EBDLampRequest;
    output[144] = Eem_MainEemLampData.Eem_Lamp_ABSLampRequest;
    output[145] = Eem_MainEemLampData.Eem_Lamp_TCSLampRequest;
    output[146] = Eem_MainEemLampData.Eem_Lamp_TCSOffLampRequest;
    output[147] = Eem_MainEemLampData.Eem_Lamp_VDCLampRequest;
    output[148] = Eem_MainEemLampData.Eem_Lamp_VDCOffLampRequest;
    output[149] = Eem_MainEemLampData.Eem_Lamp_HDCLampRequest;
    output[150] = Eem_MainEemLampData.Eem_Lamp_BBSBuzzorRequest;
    output[151] = Eem_MainEemLampData.Eem_Lamp_RBCSLampRequest;
    output[152] = Eem_MainEemLampData.Eem_Lamp_AHBLampRequest;
    output[153] = Eem_MainEemLampData.Eem_Lamp_ServiceLampRequest;
    output[154] = Eem_MainEemLampData.Eem_Lamp_TCSFuncLampRequest;
    output[155] = Eem_MainEemLampData.Eem_Lamp_VDCFuncLampRequest;
    output[156] = Eem_MainEemLampData.Eem_Lamp_AVHLampRequest;
    output[157] = Eem_MainEemLampData.Eem_Lamp_AVHILampRequest;
    output[158] = Eem_MainEemLampData.Eem_Lamp_ACCEnable;
    output[159] = Eem_MainEemLampData.Eem_Lamp_CDMEnable;
    output[160] = Eem_MainEemLampData.Eem_Buzzor_On;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Eem_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
