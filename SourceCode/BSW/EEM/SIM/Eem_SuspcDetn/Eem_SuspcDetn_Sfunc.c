#define S_FUNCTION_NAME      Eem_SuspcDetn_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          2
#define WidthOutputPort         36

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Eem_SuspcDetn.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Eem_SuspcDetnEcuModeSts = input[0];
    Eem_SuspcDetnDiagClrSrs = input[1];

    Eem_SuspcDetn();


    output[0] = Eem_SuspcDetnSwtStsFSInfo.FlexBrkSwtFaultDet;
    output[1] = Eem_SuspcDetnCanTimeOutStInfo.MaiCanSusDet;
    output[2] = Eem_SuspcDetnCanTimeOutStInfo.EmsTiOutSusDet;
    output[3] = Eem_SuspcDetnCanTimeOutStInfo.TcuTiOutSusDet;
    output[4] = Eem_SuspcDetnFuncInhibitVlvSts;
    output[5] = Eem_SuspcDetnFuncInhibitIocSts;
    output[6] = Eem_SuspcDetnFuncInhibitGdSts;
    output[7] = Eem_SuspcDetnFuncInhibitProxySts;
    output[8] = Eem_SuspcDetnFuncInhibitDiagSts;
    output[9] = Eem_SuspcDetnFuncInhibitFsrSts;
    output[10] = Eem_SuspcDetnFuncInhibitAcmioSts;
    output[11] = Eem_SuspcDetnFuncInhibitAcmctlSts;
    output[12] = Eem_SuspcDetnFuncInhibitMspSts;
    output[13] = Eem_SuspcDetnFuncInhibitNvmSts;
    output[14] = Eem_SuspcDetnFuncInhibitWssSts;
    output[15] = Eem_SuspcDetnFuncInhibitPedalSts;
    output[16] = Eem_SuspcDetnFuncInhibitPressSts;
    output[17] = Eem_SuspcDetnFuncInhibitRlySts;
    output[18] = Eem_SuspcDetnFuncInhibitSesSts;
    output[19] = Eem_SuspcDetnFuncInhibitSwtSts;
    output[20] = Eem_SuspcDetnFuncInhibitPrlySts;
    output[21] = Eem_SuspcDetnFuncInhibitMomSts;
    output[22] = Eem_SuspcDetnFuncInhibitVlvdSts;
    output[23] = Eem_SuspcDetnFuncInhibitSpimSts;
    output[24] = Eem_SuspcDetnFuncInhibitAdcifSts;
    output[25] = Eem_SuspcDetnFuncInhibitIcuSts;
    output[26] = Eem_SuspcDetnFuncInhibitMpsSts;
    output[27] = Eem_SuspcDetnFuncInhibitRegSts;
    output[28] = Eem_SuspcDetnFuncInhibitAsicSts;
    output[29] = Eem_SuspcDetnFuncInhibitCanTrcvSts;
    output[30] = Eem_SuspcDetnFuncInhibitBbcSts;
    output[31] = Eem_SuspcDetnFuncInhibitRbcSts;
    output[32] = Eem_SuspcDetnFuncInhibitArbiSts;
    output[33] = Eem_SuspcDetnFuncInhibitPctrlSts;
    output[34] = Eem_SuspcDetnFuncInhibitMccSts;
    output[35] = Eem_SuspcDetnFuncInhibitVlvActSts;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Eem_SuspcDetn_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
