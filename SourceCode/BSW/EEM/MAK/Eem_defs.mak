# \file
#
# \brief Eem
#
# This file contains the implementation of the SWC
# module Eem.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Eem_CORE_PATH     := $(MANDO_BSW_ROOT)\Eem
Eem_CAL_PATH      := $(Eem_CORE_PATH)\CAL\$(Eem_VARIANT)
Eem_SRC_PATH      := $(Eem_CORE_PATH)\SRC
Eem_CFG_PATH      := $(Eem_CORE_PATH)\CFG\$(Eem_VARIANT)
Eem_HDR_PATH      := $(Eem_CORE_PATH)\HDR
Eem_IFA_PATH      := $(Eem_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Eem_CMN_PATH      := $(Eem_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Eem_UT_PATH		:= $(Eem_CORE_PATH)\UT
	Eem_UNITY_PATH	:= $(Eem_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Eem_UT_PATH)
	CC_INCLUDE_PATH		+= $(Eem_UNITY_PATH)
	Eem_SuspcDetn_PATH 	:= Eem_UT_PATH\Eem_SuspcDetn
	Eem_Main_PATH 	:= Eem_UT_PATH\Eem_Main
endif
CC_INCLUDE_PATH    += $(Eem_CAL_PATH)
CC_INCLUDE_PATH    += $(Eem_SRC_PATH)
CC_INCLUDE_PATH    += $(Eem_CFG_PATH)
CC_INCLUDE_PATH    += $(Eem_HDR_PATH)
CC_INCLUDE_PATH    += $(Eem_IFA_PATH)
CC_INCLUDE_PATH    += $(Eem_CMN_PATH)

