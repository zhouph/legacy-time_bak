# \file
#
# \brief Eem
#
# This file contains the implementation of the SWC
# module Eem.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Eem_src

Eem_src_FILES        += $(Eem_SRC_PATH)\Eem_SuspcDetn.c
Eem_src_FILES        += $(Eem_IFA_PATH)\Eem_SuspcDetn_Ifa.c
Eem_src_FILES        += $(Eem_SRC_PATH)\Eem_Main.c
Eem_src_FILES        += $(Eem_IFA_PATH)\Eem_Main_Ifa.c
Eem_src_FILES        += $(Eem_CFG_PATH)\Eem_Cfg.c
Eem_src_FILES        += $(Eem_CAL_PATH)\Eem_Cal.c
Eem_src_FILES        += $(Eem_SRC_PATH)\Eem_ErrorInfoTable.c
Eem_src_FILES        += $(Eem_SRC_PATH)\Erp.c
Eem_src_FILES        += $(Eem_SRC_PATH)\Eem_ErrorHandling.c
ifeq ($(ICE_COMPILE),true)
Eem_src_FILES        += $(Eem_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Eem_src_FILES        += $(Eem_UNITY_PATH)\unity.c
	Eem_src_FILES        += $(Eem_UNITY_PATH)\unity_fixture.c	
	Eem_src_FILES        += $(Eem_UT_PATH)\main.c
	Eem_src_FILES        += $(Eem_UT_PATH)\Eem_SuspcDetn\Eem_SuspcDetn_UtMain.c
	Eem_src_FILES        += $(Eem_UT_PATH)\Eem_Main\Eem_Main_UtMain.c
endif