/**
 * @defgroup PedalM_Main PedalM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalM_Main.h"
#include "PedalM_Main_Ifa.h"
#include "Common.h"
#include "PedalM_Main_Proc.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
extern Ioc_InputSR1msVBatt1Mon_t Ioc_InputSR1msVBatt1Mon;

void PedalM_Main_Proc(PedalM_Main_HdrBusType *pPedalMInfo);
void Pedal_Vdd3_Check(PedalM_Main_HdrBusType *pPedalMInfo);
void Pedal_SupplyPower_Check(PedalM_Main_HdrBusType *pPedalMInfo);
void Pedal_HW_Check(PedalM_Main_HdrBusType *pPedalMInfo);


#define U16_PEDAL_PTS1_OPEN_VOLTS_THRES           200
#define U16_PEDAL_PTS1_SHORT_VOLTS_THRES         4800
#define U16_PEDAL_PTS2_OPEN_VOLTS_THRES           100
#define U16_PEDAL_PTS2_SHORT_VOLTS_THRES         2400


void PedalM_Main_Proc(PedalM_Main_HdrBusType *pPedalMInfo)
{
    Pedal_Vdd3_Check(&PedalM_MainBus);
    Pedal_SupplyPower_Check(&PedalM_MainBus);
    Pedal_HW_Check(&PedalM_MainBus); 
}

void Pedal_Vdd3_Check(PedalM_Main_HdrBusType *pPedalMInfo)
{
    
    if( (Ioc_InputSR1msVBatt1Mon<U16_BATT1VOLT_7V0) ||(Ioc_InputSR1msVBatt1Mon>U16_BATT1VOLT_17V0) )
    {
       pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err=ERR_INHIBIT;
       pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err=ERR_INHIBIT;
       pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err=ERR_INHIBIT;
    }
    
    if(pPedalMInfo->PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc == 1)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err = ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err = ERR_PREPASSED;
    }

    if(pPedalMInfo->PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv == 1)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err = ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err = ERR_PREPASSED;
    }

    if(pPedalMInfo->PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov == 1)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err = ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err = ERR_PREPASSED;
    }

    if(pPedalMInfo->PedalM_MainDiagClrSrs==1)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err = ERR_NONE;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err = ERR_NONE;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err = ERR_NONE;
    }
}

void Pedal_SupplyPower_Check(PedalM_Main_HdrBusType *pPedalMInfo)
{
    
    if( (pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err == ERR_PREFAILED)
        ||(pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err == ERR_PREFAILED)
        ||(pPedalMInfo->PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err == ERR_PREFAILED) )
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err=ERR_INHIBIT;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err=ERR_INHIBIT;
    }
    /*PTS1 Supply Power Check*/
    if( (pPedalMInfo->PedalM_MainPedlPwrMonInfo.Pdt5vMon < U16_CALCVOLT_4V75) 
        ||(pPedalMInfo->PedalM_MainPedlPwrMonInfo.Pdt5vMon > U16_CALCVOLT_5V25) )
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err=ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err=ERR_PREPASSED;
    }

    /*PTS2 Supply Power Check*/
    if( (pPedalMInfo->PedalM_MainPedlPwrMonInfo.Pdf5vMon < U16_CALCVOLT_4V75) 
        ||(pPedalMInfo->PedalM_MainPedlPwrMonInfo.Pdf5vMon > U16_CALCVOLT_5V25) )
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err=ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err=ERR_PREPASSED;
    }

    if(pPedalMInfo->PedalM_MainDiagClrSrs==1)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err=ERR_NONE;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err=ERR_NONE;
    }
}

void Pedal_HW_Check(PedalM_Main_HdrBusType *pPedalMInfo)
{
        
    if( (pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err == ERR_PREFAILED)
        ||(pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err == ERR_PREFAILED) )
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Short_Err=ERR_INHIBIT;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Open_Err=ERR_INHIBIT;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Short_Err=ERR_INHIBIT;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Open_Err=ERR_INHIBIT;
    }
    /*PTS1 Monitoring*/
    if(pPedalMInfo->PedalM_MainPedlSigMonInfo.PdtSigMon>U16_PEDAL_PTS1_SHORT_VOLTS_THRES)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Short_Err=ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Short_Err=ERR_PREPASSED;
    }
    
    if(pPedalMInfo->PedalM_MainPedlSigMonInfo.PdtSigMon<U16_PEDAL_PTS1_OPEN_VOLTS_THRES)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Open_Err=ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Open_Err=ERR_PREPASSED;
    }

    /*PTS2 Monitoring*/
    if(pPedalMInfo->PedalM_MainPedlSigMonInfo.PdfSigMon>U16_PEDAL_PTS2_SHORT_VOLTS_THRES)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Short_Err=ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Short_Err=ERR_PREPASSED;
    }
    
    if(pPedalMInfo->PedalM_MainPedlSigMonInfo.PdfSigMon<U16_PEDAL_PTS2_OPEN_VOLTS_THRES)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Open_Err=ERR_PREFAILED;
    }
    else
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Open_Err=ERR_PREPASSED;
    }

    if(pPedalMInfo->PedalM_MainDiagClrSrs==1)
    {
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Short_Err=ERR_NONE;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS1_Open_Err=ERR_NONE;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Short_Err=ERR_NONE;
        pPedalMInfo->PedalM_MainPedalMData.PedalM_PTS2_Open_Err=ERR_NONE;
    }
}


    