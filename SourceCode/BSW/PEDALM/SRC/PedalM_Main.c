/**
 * @defgroup PedalM_Main PedalM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalM_Main.h"
#include "PedalM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDALM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
PedalM_Main_HdrBusType PedalM_MainBus;

/* Version Info */
const SwcVersionInfo_t PedalM_MainVersionInfo = 
{   
    PEDALM_MAIN_MODULE_ID,           /* PedalM_MainVersionInfo.ModuleId */
    PEDALM_MAIN_MAJOR_VERSION,       /* PedalM_MainVersionInfo.MajorVer */
    PEDALM_MAIN_MINOR_VERSION,       /* PedalM_MainVersionInfo.MinorVer */
    PEDALM_MAIN_PATCH_VERSION,       /* PedalM_MainVersionInfo.PatchVer */
    PEDALM_MAIN_BRANCH_VERSION       /* PedalM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchSysPwrAsicInfo_t PedalM_MainAchSysPwrAsicInfo;
Ioc_InputSR1msPedlSigMonInfo_t PedalM_MainPedlSigMonInfo;
Ioc_InputSR1msPedlPwrMonInfo_t PedalM_MainPedlPwrMonInfo;
Ach_InputAchAsicInvalidInfo_t PedalM_MainAchAsicInvalid;
Mom_HndlrEcuModeSts_t PedalM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t PedalM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t PedalM_MainIgnEdgeSts;
Diag_HndlrDiagClr_t PedalM_MainDiagClrSrs;
Ioc_InputSR1msVdd3Mon_t PedalM_MainVdd3Mon;

/* Output Data Element */
PedalM_MainPedalMData_t PedalM_MainPedalMData;

uint32 PedalM_Main_Timer_Start;
uint32 PedalM_Main_Timer_Elapsed;

#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_CODE
#include "PedalM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PedalM_Main_Init(void)
{
    /* Initialize internal bus */
    PedalM_MainBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = 0;
    PedalM_MainBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = 0;
    PedalM_MainBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = 0;
    PedalM_MainBus.PedalM_MainPedlSigMonInfo.PdfSigMon = 0;
    PedalM_MainBus.PedalM_MainPedlSigMonInfo.PdtSigMon = 0;
    PedalM_MainBus.PedalM_MainPedlPwrMonInfo.Pdt5vMon = 0;
    PedalM_MainBus.PedalM_MainPedlPwrMonInfo.Pdf5vMon = 0;
    PedalM_MainBus.PedalM_MainAchAsicInvalid = 0;
    PedalM_MainBus.PedalM_MainEcuModeSts = 0;
    PedalM_MainBus.PedalM_MainIgnOnOffSts = 0;
    PedalM_MainBus.PedalM_MainIgnEdgeSts = 0;
    PedalM_MainBus.PedalM_MainDiagClrSrs = 0;
    PedalM_MainBus.PedalM_MainVdd3Mon = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS1_Open_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS1_Short_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS2_Open_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS2_Short_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS1_Check_Ihb = 0;
    PedalM_MainBus.PedalM_MainPedalMData.PedalM_PTS2_Check_Ihb = 0;
}

void PedalM_Main(void)
{
    PedalM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&PedalM_MainBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&PedalM_MainBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&PedalM_MainBus.PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);

    PedalM_Main_Read_PedalM_MainPedlSigMonInfo(&PedalM_MainBus.PedalM_MainPedlSigMonInfo);
    /*==============================================================================
    * Members of structure PedalM_MainPedlSigMonInfo 
     : PedalM_MainPedlSigMonInfo.PdfSigMon;
     : PedalM_MainPedlSigMonInfo.PdtSigMon;
     =============================================================================*/
    
    PedalM_Main_Read_PedalM_MainPedlPwrMonInfo(&PedalM_MainBus.PedalM_MainPedlPwrMonInfo);
    /*==============================================================================
    * Members of structure PedalM_MainPedlPwrMonInfo 
     : PedalM_MainPedlPwrMonInfo.Pdt5vMon;
     : PedalM_MainPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/
    
    PedalM_Main_Read_PedalM_MainAchAsicInvalid(&PedalM_MainBus.PedalM_MainAchAsicInvalid);
    PedalM_Main_Read_PedalM_MainEcuModeSts(&PedalM_MainBus.PedalM_MainEcuModeSts);
    PedalM_Main_Read_PedalM_MainIgnOnOffSts(&PedalM_MainBus.PedalM_MainIgnOnOffSts);
    PedalM_Main_Read_PedalM_MainIgnEdgeSts(&PedalM_MainBus.PedalM_MainIgnEdgeSts);
    PedalM_Main_Read_PedalM_MainDiagClrSrs(&PedalM_MainBus.PedalM_MainDiagClrSrs);
    PedalM_Main_Read_PedalM_MainVdd3Mon(&PedalM_MainBus.PedalM_MainVdd3Mon);

    /* Process */
	PedalM_Main_Proc(&PedalM_MainBus);

    /* Output */
    PedalM_Main_Write_PedalM_MainPedalMData(&PedalM_MainBus.PedalM_MainPedalMData);
    /*==============================================================================
    * Members of structure PedalM_MainPedalMData 
     : PedalM_MainPedalMData.PedalM_PTS1_Open_Err;
     : PedalM_MainPedalMData.PedalM_PTS1_Short_Err;
     : PedalM_MainPedalMData.PedalM_PTS2_Open_Err;
     : PedalM_MainPedalMData.PedalM_PTS2_Short_Err;
     : PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err;
     : PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err;
     : PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err;
     : PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err;
     : PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err;
     : PedalM_MainPedalMData.PedalM_PTS1_Check_Ihb;
     : PedalM_MainPedalMData.PedalM_PTS2_Check_Ihb;
     =============================================================================*/
    

    PedalM_Main_Timer_Elapsed = STM0_TIM0.U - PedalM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDALM_MAIN_STOP_SEC_CODE
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
