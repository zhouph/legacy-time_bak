/**
 * @defgroup PedalM_Main PedalM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Main_Proc.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALM_MAIN_PROC_H_
#define PEDALM_MAIN_PROC_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalM_Types.h"
#include "PedalM_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PEDALM_MAIN_MODULE_ID      (0)
 #define PEDALM_MAIN_MAJOR_VERSION  (2)
 #define PEDALM_MAIN_MINOR_VERSION  (0)
 #define PEDALM_MAIN_PATCH_VERSION  (0)
 #define PEDALM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 #define    U16_CALCVOLT_4V75             4750
 #define    U16_CALCVOLT_5V25             5250
 #define    U16_BATT1VOLT_7V0            (7000)
 #define    U16_BATT1VOLT_17V0          (17000)

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern PedalM_Main_HdrBusType PedalM_MainBus;
extern PedalM_Main_HdrBusType PedalM_MainBusOld;

/* Version Info */
//extern SwcVersionInfo_t PedalM_MainVersionInfo;

/* Input Data Element */
extern Ioc_InputSR1msPedlSigMonInfo_t PedalM_MainPedlSigMonInfo;
extern Ioc_InputSR1msPedlPwrMonInfo_t PedalM_MainPedlPwrMonInfo;
extern Ach_InputAchSysPwrAsicInfo_t PedalM_MainAchSysPwrAsicInfo;
extern Ioc_InputSR1msVdd3Mon_t PedalM_MainVdd3Mon;

/* Output Data Element */
extern PedalM_MainPedalMData_t PedalM_MainPedalMData;
extern void PedalM_Main_Proc(PedalM_Main_HdrBusType *pPedalMInfo);

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALM_MAIN_H_ */
/** @} */

