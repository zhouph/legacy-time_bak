/**
 * @defgroup PedalM_Main PedalM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALM_MAIN_H_
#define PEDALM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalM_Types.h"
#include "PedalM_Cfg.h"
#include "PedalM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PEDALM_MAIN_MODULE_ID      (0)
 #define PEDALM_MAIN_MAJOR_VERSION  (2)
 #define PEDALM_MAIN_MINOR_VERSION  (0)
 #define PEDALM_MAIN_PATCH_VERSION  (0)
 #define PEDALM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern PedalM_Main_HdrBusType PedalM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t PedalM_MainVersionInfo;

/* Input Data Element */
extern Ach_InputAchSysPwrAsicInfo_t PedalM_MainAchSysPwrAsicInfo;
extern Ioc_InputSR1msPedlSigMonInfo_t PedalM_MainPedlSigMonInfo;
extern Ioc_InputSR1msPedlPwrMonInfo_t PedalM_MainPedlPwrMonInfo;
extern Ach_InputAchAsicInvalidInfo_t PedalM_MainAchAsicInvalid;
extern Mom_HndlrEcuModeSts_t PedalM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t PedalM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t PedalM_MainIgnEdgeSts;
extern Diag_HndlrDiagClr_t PedalM_MainDiagClrSrs;
extern Ioc_InputSR1msVdd3Mon_t PedalM_MainVdd3Mon;

/* Output Data Element */
extern PedalM_MainPedalMData_t PedalM_MainPedalMData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void PedalM_Main_Init(void);
extern void PedalM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALM_MAIN_H_ */
/** @} */
