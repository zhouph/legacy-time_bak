/**
 * @defgroup PedalM_Types PedalM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALM_TYPES_H_
#define PEDALM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ach_InputAchSysPwrAsicInfo_t PedalM_MainAchSysPwrAsicInfo;
    Ioc_InputSR1msPedlSigMonInfo_t PedalM_MainPedlSigMonInfo;
    Ioc_InputSR1msPedlPwrMonInfo_t PedalM_MainPedlPwrMonInfo;
    Ach_InputAchAsicInvalidInfo_t PedalM_MainAchAsicInvalid;
    Mom_HndlrEcuModeSts_t PedalM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t PedalM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t PedalM_MainIgnEdgeSts;
    Diag_HndlrDiagClr_t PedalM_MainDiagClrSrs;
    Ioc_InputSR1msVdd3Mon_t PedalM_MainVdd3Mon;

/* Output Data Element */
    PedalM_MainPedalMData_t PedalM_MainPedalMData;
}PedalM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALM_TYPES_H_ */
/** @} */
