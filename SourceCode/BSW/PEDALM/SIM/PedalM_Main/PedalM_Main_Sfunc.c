#define S_FUNCTION_NAME      PedalM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          13
#define WidthOutputPort         11

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "PedalM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = input[0];
    PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = input[1];
    PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = input[2];
    PedalM_MainPedlSigMonInfo.PdfSigMon = input[3];
    PedalM_MainPedlSigMonInfo.PdtSigMon = input[4];
    PedalM_MainPedlPwrMonInfo.Pdt5vMon = input[5];
    PedalM_MainPedlPwrMonInfo.Pdf5vMon = input[6];
    PedalM_MainAchAsicInvalid = input[7];
    PedalM_MainEcuModeSts = input[8];
    PedalM_MainIgnOnOffSts = input[9];
    PedalM_MainIgnEdgeSts = input[10];
    PedalM_MainDiagClrSrs = input[11];
    PedalM_MainVdd3Mon = input[12];

    PedalM_Main();


    output[0] = PedalM_MainPedalMData.PedalM_PTS1_Open_Err;
    output[1] = PedalM_MainPedalMData.PedalM_PTS1_Short_Err;
    output[2] = PedalM_MainPedalMData.PedalM_PTS2_Open_Err;
    output[3] = PedalM_MainPedalMData.PedalM_PTS2_Short_Err;
    output[4] = PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err;
    output[5] = PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err;
    output[6] = PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err;
    output[7] = PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err;
    output[8] = PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err;
    output[9] = PedalM_MainPedalMData.PedalM_PTS1_Check_Ihb;
    output[10] = PedalM_MainPedalMData.PedalM_PTS2_Check_Ihb;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    PedalM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
