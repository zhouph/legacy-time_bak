PedalM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd3_ov'
    };
PedalM_MainAchSysPwrAsicInfo = CreateBus(PedalM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

PedalM_MainPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
PedalM_MainPedlSigMonInfo = CreateBus(PedalM_MainPedlSigMonInfo, DeList);
clear DeList;

PedalM_MainPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
PedalM_MainPedlPwrMonInfo = CreateBus(PedalM_MainPedlPwrMonInfo, DeList);
clear DeList;

PedalM_MainAchAsicInvalid = Simulink.Bus;
DeList={'PedalM_MainAchAsicInvalid'};
PedalM_MainAchAsicInvalid = CreateBus(PedalM_MainAchAsicInvalid, DeList);
clear DeList;

PedalM_MainEcuModeSts = Simulink.Bus;
DeList={'PedalM_MainEcuModeSts'};
PedalM_MainEcuModeSts = CreateBus(PedalM_MainEcuModeSts, DeList);
clear DeList;

PedalM_MainIgnOnOffSts = Simulink.Bus;
DeList={'PedalM_MainIgnOnOffSts'};
PedalM_MainIgnOnOffSts = CreateBus(PedalM_MainIgnOnOffSts, DeList);
clear DeList;

PedalM_MainIgnEdgeSts = Simulink.Bus;
DeList={'PedalM_MainIgnEdgeSts'};
PedalM_MainIgnEdgeSts = CreateBus(PedalM_MainIgnEdgeSts, DeList);
clear DeList;

PedalM_MainDiagClrSrs = Simulink.Bus;
DeList={'PedalM_MainDiagClrSrs'};
PedalM_MainDiagClrSrs = CreateBus(PedalM_MainDiagClrSrs, DeList);
clear DeList;

PedalM_MainVdd3Mon = Simulink.Bus;
DeList={'PedalM_MainVdd3Mon'};
PedalM_MainVdd3Mon = CreateBus(PedalM_MainVdd3Mon, DeList);
clear DeList;

PedalM_MainPedalMData = Simulink.Bus;
DeList={
    'PedalM_PTS1_Open_Err'
    'PedalM_PTS1_Short_Err'
    'PedalM_PTS2_Open_Err'
    'PedalM_PTS2_Short_Err'
    'PedalM_PTS1_SupplyPower_Err'
    'PedalM_PTS2_SupplyPower_Err'
    'PedalM_VDD3_OverVolt_Err'
    'PedalM_VDD3_UnderVolt_Err'
    'PedalM_VDD3_OverCurrent_Err'
    'PedalM_PTS1_Check_Ihb'
    'PedalM_PTS2_Check_Ihb'
    };
PedalM_MainPedalMData = CreateBus(PedalM_MainPedalMData, DeList);
clear DeList;

