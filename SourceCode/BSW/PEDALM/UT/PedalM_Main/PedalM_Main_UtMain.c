#include "unity.h"
#include "unity_fixture.h"
#include "PedalM_Main.h"
#include "PedalM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[MAX_STEP] = PEDALM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OVC;
const Haluint8 UtInput_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[MAX_STEP] = PEDALM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_UV;
const Haluint8 UtInput_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[MAX_STEP] = PEDALM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OV;
const Haluint16 UtInput_PedalM_MainPedlSigMonInfo_PdfSigMon[MAX_STEP] = PEDALM_MAINPEDLSIGMONINFO_PDFSIGMON;
const Haluint16 UtInput_PedalM_MainPedlSigMonInfo_PdtSigMon[MAX_STEP] = PEDALM_MAINPEDLSIGMONINFO_PDTSIGMON;
const Haluint16 UtInput_PedalM_MainPedlPwrMonInfo_Pdt5vMon[MAX_STEP] = PEDALM_MAINPEDLPWRMONINFO_PDT5VMON;
const Haluint16 UtInput_PedalM_MainPedlPwrMonInfo_Pdf5vMon[MAX_STEP] = PEDALM_MAINPEDLPWRMONINFO_PDF5VMON;
const Ach_InputAchAsicInvalidInfo_t UtInput_PedalM_MainAchAsicInvalid[MAX_STEP] = PEDALM_MAINACHASICINVALID;
const Mom_HndlrEcuModeSts_t UtInput_PedalM_MainEcuModeSts[MAX_STEP] = PEDALM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_PedalM_MainIgnOnOffSts[MAX_STEP] = PEDALM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_PedalM_MainIgnEdgeSts[MAX_STEP] = PEDALM_MAINIGNEDGESTS;
const Diag_HndlrDiagClr_t UtInput_PedalM_MainDiagClrSrs[MAX_STEP] = PEDALM_MAINDIAGCLRSRS;
const Ioc_InputSR1msVdd3Mon_t UtInput_PedalM_MainVdd3Mon[MAX_STEP] = PEDALM_MAINVDD3MON;

const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS1_Open_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS1_OPEN_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS1_Short_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS1_SHORT_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS2_Open_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS2_OPEN_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS2_Short_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS2_SHORT_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS1_SupplyPower_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS1_SUPPLYPOWER_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS2_SupplyPower_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS2_SUPPLYPOWER_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_VDD3_OverVolt_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_VDD3_OVERVOLT_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_VDD3_UnderVolt_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_VDD3_UNDERVOLT_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_VDD3_OverCurrent_Err[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_VDD3_OVERCURRENT_ERR;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS1_Check_Ihb[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS1_CHECK_IHB;
const Saluint8 UtExpected_PedalM_MainPedalMData_PedalM_PTS2_Check_Ihb[MAX_STEP] = PEDALM_MAINPEDALMDATA_PEDALM_PTS2_CHECK_IHB;



TEST_GROUP(PedalM_Main);
TEST_SETUP(PedalM_Main)
{
    PedalM_Main_Init();
}

TEST_TEAR_DOWN(PedalM_Main)
{   /* Postcondition */

}

TEST(PedalM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = UtInput_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[i];
        PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = UtInput_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[i];
        PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = UtInput_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[i];
        PedalM_MainPedlSigMonInfo.PdfSigMon = UtInput_PedalM_MainPedlSigMonInfo_PdfSigMon[i];
        PedalM_MainPedlSigMonInfo.PdtSigMon = UtInput_PedalM_MainPedlSigMonInfo_PdtSigMon[i];
        PedalM_MainPedlPwrMonInfo.Pdt5vMon = UtInput_PedalM_MainPedlPwrMonInfo_Pdt5vMon[i];
        PedalM_MainPedlPwrMonInfo.Pdf5vMon = UtInput_PedalM_MainPedlPwrMonInfo_Pdf5vMon[i];
        PedalM_MainAchAsicInvalid = UtInput_PedalM_MainAchAsicInvalid[i];
        PedalM_MainEcuModeSts = UtInput_PedalM_MainEcuModeSts[i];
        PedalM_MainIgnOnOffSts = UtInput_PedalM_MainIgnOnOffSts[i];
        PedalM_MainIgnEdgeSts = UtInput_PedalM_MainIgnEdgeSts[i];
        PedalM_MainDiagClrSrs = UtInput_PedalM_MainDiagClrSrs[i];
        PedalM_MainVdd3Mon = UtInput_PedalM_MainVdd3Mon[i];

        PedalM_Main();

        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS1_Open_Err, UtExpected_PedalM_MainPedalMData_PedalM_PTS1_Open_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS1_Short_Err, UtExpected_PedalM_MainPedalMData_PedalM_PTS1_Short_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS2_Open_Err, UtExpected_PedalM_MainPedalMData_PedalM_PTS2_Open_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS2_Short_Err, UtExpected_PedalM_MainPedalMData_PedalM_PTS2_Short_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err, UtExpected_PedalM_MainPedalMData_PedalM_PTS1_SupplyPower_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err, UtExpected_PedalM_MainPedalMData_PedalM_PTS2_SupplyPower_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err, UtExpected_PedalM_MainPedalMData_PedalM_VDD3_OverVolt_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err, UtExpected_PedalM_MainPedalMData_PedalM_VDD3_UnderVolt_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err, UtExpected_PedalM_MainPedalMData_PedalM_VDD3_OverCurrent_Err[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS1_Check_Ihb, UtExpected_PedalM_MainPedalMData_PedalM_PTS1_Check_Ihb[i]);
        TEST_ASSERT_EQUAL(PedalM_MainPedalMData.PedalM_PTS2_Check_Ihb, UtExpected_PedalM_MainPedalMData_PedalM_PTS2_Check_Ihb[i]);
    }
}

TEST_GROUP_RUNNER(PedalM_Main)
{
    RUN_TEST_CASE(PedalM_Main, All);
}
