/**
 * @defgroup PedalM_Main_Ifa PedalM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PedalM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDALM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDALM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PedalM_MemMap.h"
#define PEDALM_MAIN_START_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDALM_MAIN_STOP_SEC_VAR_32BIT
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDALM_MAIN_START_SEC_CODE
#include "PedalM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDALM_MAIN_STOP_SEC_CODE
#include "PedalM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
