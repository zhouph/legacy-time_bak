/**
 * @defgroup PedalM_Main_Ifa PedalM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PedalM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDALM_MAIN_IFA_H_
#define PEDALM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo(data) do \
{ \
    *data = PedalM_MainAchSysPwrAsicInfo; \
}while(0);

#define PedalM_Main_Read_PedalM_MainPedlSigMonInfo(data) do \
{ \
    *data = PedalM_MainPedlSigMonInfo; \
}while(0);

#define PedalM_Main_Read_PedalM_MainPedlPwrMonInfo(data) do \
{ \
    *data = PedalM_MainPedlPwrMonInfo; \
}while(0);

#define PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(data) do \
{ \
    *data = PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc; \
}while(0);

#define PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(data) do \
{ \
    *data = PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv; \
}while(0);

#define PedalM_Main_Read_PedalM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(data) do \
{ \
    *data = PedalM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov; \
}while(0);

#define PedalM_Main_Read_PedalM_MainPedlSigMonInfo_PdfSigMon(data) do \
{ \
    *data = PedalM_MainPedlSigMonInfo.PdfSigMon; \
}while(0);

#define PedalM_Main_Read_PedalM_MainPedlSigMonInfo_PdtSigMon(data) do \
{ \
    *data = PedalM_MainPedlSigMonInfo.PdtSigMon; \
}while(0);

#define PedalM_Main_Read_PedalM_MainPedlPwrMonInfo_Pdt5vMon(data) do \
{ \
    *data = PedalM_MainPedlPwrMonInfo.Pdt5vMon; \
}while(0);

#define PedalM_Main_Read_PedalM_MainPedlPwrMonInfo_Pdf5vMon(data) do \
{ \
    *data = PedalM_MainPedlPwrMonInfo.Pdf5vMon; \
}while(0);

#define PedalM_Main_Read_PedalM_MainAchAsicInvalid(data) do \
{ \
    *data = PedalM_MainAchAsicInvalid; \
}while(0);

#define PedalM_Main_Read_PedalM_MainEcuModeSts(data) do \
{ \
    *data = PedalM_MainEcuModeSts; \
}while(0);

#define PedalM_Main_Read_PedalM_MainIgnOnOffSts(data) do \
{ \
    *data = PedalM_MainIgnOnOffSts; \
}while(0);

#define PedalM_Main_Read_PedalM_MainIgnEdgeSts(data) do \
{ \
    *data = PedalM_MainIgnEdgeSts; \
}while(0);

#define PedalM_Main_Read_PedalM_MainDiagClrSrs(data) do \
{ \
    *data = PedalM_MainDiagClrSrs; \
}while(0);

#define PedalM_Main_Read_PedalM_MainVdd3Mon(data) do \
{ \
    *data = PedalM_MainVdd3Mon; \
}while(0);


/* Set Output DE MAcro Function */
#define PedalM_Main_Write_PedalM_MainPedalMData(data) do \
{ \
    PedalM_MainPedalMData = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS1_Open_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS1_Open_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS1_Short_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS1_Short_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS2_Open_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS2_Open_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS2_Short_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS2_Short_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS1_SupplyPower_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS1_SupplyPower_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS2_SupplyPower_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS2_SupplyPower_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_VDD3_OverVolt_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_VDD3_OverVolt_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_VDD3_UnderVolt_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_VDD3_UnderVolt_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_VDD3_OverCurrent_Err(data) do \
{ \
    PedalM_MainPedalMData.PedalM_VDD3_OverCurrent_Err = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS1_Check_Ihb(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS1_Check_Ihb = *data; \
}while(0);

#define PedalM_Main_Write_PedalM_MainPedalMData_PedalM_PTS2_Check_Ihb(data) do \
{ \
    PedalM_MainPedalMData.PedalM_PTS2_Check_Ihb = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDALM_MAIN_IFA_H_ */
/** @} */
