# \file
#
# \brief PedalM
#
# This file contains the implementation of the SWC
# module PedalM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += PedalM_src

PedalM_src_FILES        += $(PedalM_SRC_PATH)\PedalM_Main.c
PedalM_src_FILES        += $(PedalM_IFA_PATH)\PedalM_Main_Ifa.c
PedalM_src_FILES        += $(PedalM_CFG_PATH)\PedalM_Cfg.c
PedalM_src_FILES        += $(PedalM_CAL_PATH)\PedalM_Cal.c
PedalM_src_FILES        += $(PedalM_SRC_PATH)\PedalM_Main_Proc.c

ifeq ($(ICE_COMPILE),true)
PedalM_src_FILES        += $(PedalM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	PedalM_src_FILES        += $(PedalM_UNITY_PATH)\unity.c
	PedalM_src_FILES        += $(PedalM_UNITY_PATH)\unity_fixture.c	
	PedalM_src_FILES        += $(PedalM_UT_PATH)\main.c
	PedalM_src_FILES        += $(PedalM_UT_PATH)\PedalM_Main\PedalM_Main_UtMain.c
endif