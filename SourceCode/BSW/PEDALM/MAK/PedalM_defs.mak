# \file
#
# \brief PedalM
#
# This file contains the implementation of the SWC
# module PedalM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

PedalM_CORE_PATH     := $(MANDO_BSW_ROOT)\PedalM
PedalM_CAL_PATH      := $(PedalM_CORE_PATH)\CAL\$(PedalM_VARIANT)
PedalM_SRC_PATH      := $(PedalM_CORE_PATH)\SRC
PedalM_CFG_PATH      := $(PedalM_CORE_PATH)\CFG\$(PedalM_VARIANT)
PedalM_HDR_PATH      := $(PedalM_CORE_PATH)\HDR
PedalM_IFA_PATH      := $(PedalM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    PedalM_CMN_PATH      := $(PedalM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	PedalM_UT_PATH		:= $(PedalM_CORE_PATH)\UT
	PedalM_UNITY_PATH	:= $(PedalM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(PedalM_UT_PATH)
	CC_INCLUDE_PATH		+= $(PedalM_UNITY_PATH)
	PedalM_Main_PATH 	:= PedalM_UT_PATH\PedalM_Main
endif
CC_INCLUDE_PATH    += $(PedalM_CAL_PATH)
CC_INCLUDE_PATH    += $(PedalM_SRC_PATH)
CC_INCLUDE_PATH    += $(PedalM_CFG_PATH)
CC_INCLUDE_PATH    += $(PedalM_HDR_PATH)
CC_INCLUDE_PATH    += $(PedalM_IFA_PATH)
CC_INCLUDE_PATH    += $(PedalM_CMN_PATH)

