/**
 * @defgroup MgdM_Cal MgdM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MgdM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MgdM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MGDM_START_SEC_CALIB_UNSPECIFIED
#include "MgdM_MemMap.h"
/* Global Calibration Section */


#define MGDM_STOP_SEC_CALIB_UNSPECIFIED
#include "MgdM_MemMap.h"

#define MGDM_START_SEC_CONST_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MGDM_STOP_SEC_CONST_UNSPECIFIED
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGDM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MGDM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_START_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGDM_STOP_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
#define MGDM_START_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGDM_STOP_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_START_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (32BIT)**/


#define MGDM_STOP_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGDM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MGDM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_START_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGDM_STOP_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
#define MGDM_START_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGDM_STOP_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_START_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (32BIT)**/


#define MGDM_STOP_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MGDM_START_SEC_CODE
#include "MgdM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MGDM_STOP_SEC_CODE
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
