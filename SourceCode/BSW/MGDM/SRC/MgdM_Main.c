/**
 * @defgroup MgdM_Main MgdM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MgdM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MgdM_Main.h"
#include "MgdM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MGDM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MGDM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGDM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
MgdM_Main_HdrBusType MgdM_MainBus;

/* Version Info */
const SwcVersionInfo_t MgdM_MainVersionInfo = 
{   
    MGDM_MAIN_MODULE_ID,           /* MgdM_MainVersionInfo.ModuleId */
    MGDM_MAIN_MAJOR_VERSION,       /* MgdM_MainVersionInfo.MajorVer */
    MGDM_MAIN_MINOR_VERSION,       /* MgdM_MainVersionInfo.MinorVer */
    MGDM_MAIN_PATCH_VERSION,       /* MgdM_MainVersionInfo.PatchVer */
    MGDM_MAIN_BRANCH_VERSION       /* MgdM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mgd_TLE9180_HndlrMgdDcdInfo_t MgdM_MainMgdDcdInfo;
Mom_HndlrEcuModeSts_t MgdM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t MgdM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t MgdM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t MgdM_MainVBatt1Mon;
Diag_HndlrDiagClr_t MgdM_MainDiagClrSrs;
Mgd_TLE9180_HndlrMgdInvalid_t MgdM_MainMgdInvalid;
Arbitrator_MtrMtrArbDriveState_t MgdM_MainMtrArbDriveState;

/* Output Data Element */

uint32 MgdM_Main_Timer_Start;
uint32 MgdM_Main_Timer_Elapsed;

#define MGDM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGDM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
#define MGDM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGDM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_MAIN_START_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (32BIT)**/


#define MGDM_MAIN_STOP_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGDM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MGDM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGDM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "MgdM_MemMap.h"
#define MGDM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGDM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "MgdM_MemMap.h"
#define MGDM_MAIN_START_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/** Variable Section (32BIT)**/


#define MGDM_MAIN_STOP_SEC_VAR_32BIT
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MGDM_MAIN_START_SEC_CODE
#include "MgdM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void MgdM_Main_Init(void)
{
    /* Initialize internal bus */
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdIdleM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdConfM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdConfLock = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSelfTestM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSoffM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdRectM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdNormM = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdOsf = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdOp = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdScd = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSd = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdIndiag = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdOutp = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdExt = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdInt12 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdRom = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdLimpOn = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdStIncomplete = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdApcAct = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdGtm = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdLfw = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOtW = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvReg1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvVccRom = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvReg4 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvReg6 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvReg6 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvReg5 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvCb = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrClkTrim = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvBs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvBs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvBs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrCp2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrCp1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvBs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvBs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvBs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvVdh = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvVdh = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvVs = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvVs = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrUvVcc = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvVcc = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOvLdVdh = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdDdpStuck = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdCp1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdOvCp = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdClkfail = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdUvCb = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdOvVdh = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdOvVs = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdSdOt = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrScdLs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrScdLs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrScdLs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrScdHs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrScdHs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrScdHs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrIndLs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrIndLs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrIndLs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrIndHs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrIndHs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrIndHs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOsfLs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOsfLs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOsfLs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOsfHs1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOsfHs2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOsfHs3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrSpiFrame = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrSpiTo = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrSpiWd = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrSpiCrc = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdEpiAddInvalid = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdEonfTo = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdEonfSigInvalid = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOcOp1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp1Uv = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp1Ov = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp1Calib = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOcOp2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp2Uv = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp2Ov = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp2Calib = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOcOp3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp3Uv = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp3Ov = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOp3Calib = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOutpErrn = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOutpMiso = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOutpPFB1 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOutpPFB2 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdErrOutpPFB3 = 0;
    MgdM_MainBus.MgdM_MainMgdDcdInfo.mgdTle9180ErrPort = 0;
    MgdM_MainBus.MgdM_MainEcuModeSts = 0;
    MgdM_MainBus.MgdM_MainIgnOnOffSts = 0;
    MgdM_MainBus.MgdM_MainIgnEdgeSts = 0;
    MgdM_MainBus.MgdM_MainVBatt1Mon = 0;
    MgdM_MainBus.MgdM_MainDiagClrSrs = 0;
    MgdM_MainBus.MgdM_MainMgdInvalid = 0;
    MgdM_MainBus.MgdM_MainMtrArbDriveState = 0;
}

void MgdM_Main(void)
{
    MgdM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    MgdM_Main_Read_MgdM_MainMgdDcdInfo(&MgdM_MainBus.MgdM_MainMgdDcdInfo);
    /*==============================================================================
    * Members of structure MgdM_MainMgdDcdInfo 
     : MgdM_MainMgdDcdInfo.mgdIdleM;
     : MgdM_MainMgdDcdInfo.mgdConfM;
     : MgdM_MainMgdDcdInfo.mgdConfLock;
     : MgdM_MainMgdDcdInfo.mgdSelfTestM;
     : MgdM_MainMgdDcdInfo.mgdSoffM;
     : MgdM_MainMgdDcdInfo.mgdErrM;
     : MgdM_MainMgdDcdInfo.mgdRectM;
     : MgdM_MainMgdDcdInfo.mgdNormM;
     : MgdM_MainMgdDcdInfo.mgdOsf;
     : MgdM_MainMgdDcdInfo.mgdOp;
     : MgdM_MainMgdDcdInfo.mgdScd;
     : MgdM_MainMgdDcdInfo.mgdSd;
     : MgdM_MainMgdDcdInfo.mgdIndiag;
     : MgdM_MainMgdDcdInfo.mgdOutp;
     : MgdM_MainMgdDcdInfo.mgdExt;
     : MgdM_MainMgdDcdInfo.mgdInt12;
     : MgdM_MainMgdDcdInfo.mgdRom;
     : MgdM_MainMgdDcdInfo.mgdLimpOn;
     : MgdM_MainMgdDcdInfo.mgdStIncomplete;
     : MgdM_MainMgdDcdInfo.mgdApcAct;
     : MgdM_MainMgdDcdInfo.mgdGtm;
     : MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid;
     : MgdM_MainMgdDcdInfo.mgdLfw;
     : MgdM_MainMgdDcdInfo.mgdErrOtW;
     : MgdM_MainMgdDcdInfo.mgdErrOvReg1;
     : MgdM_MainMgdDcdInfo.mgdErrUvVccRom;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg4;
     : MgdM_MainMgdDcdInfo.mgdErrOvReg6;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg6;
     : MgdM_MainMgdDcdInfo.mgdErrUvReg5;
     : MgdM_MainMgdDcdInfo.mgdErrUvCb;
     : MgdM_MainMgdDcdInfo.mgdErrClkTrim;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs3;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs2;
     : MgdM_MainMgdDcdInfo.mgdErrUvBs1;
     : MgdM_MainMgdDcdInfo.mgdErrCp2;
     : MgdM_MainMgdDcdInfo.mgdErrCp1;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs3;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs2;
     : MgdM_MainMgdDcdInfo.mgdErrOvBs1;
     : MgdM_MainMgdDcdInfo.mgdErrOvVdh;
     : MgdM_MainMgdDcdInfo.mgdErrUvVdh;
     : MgdM_MainMgdDcdInfo.mgdErrOvVs;
     : MgdM_MainMgdDcdInfo.mgdErrUvVs;
     : MgdM_MainMgdDcdInfo.mgdErrUvVcc;
     : MgdM_MainMgdDcdInfo.mgdErrOvVcc;
     : MgdM_MainMgdDcdInfo.mgdErrOvLdVdh;
     : MgdM_MainMgdDcdInfo.mgdSdDdpStuck;
     : MgdM_MainMgdDcdInfo.mgdSdCp1;
     : MgdM_MainMgdDcdInfo.mgdSdOvCp;
     : MgdM_MainMgdDcdInfo.mgdSdClkfail;
     : MgdM_MainMgdDcdInfo.mgdSdUvCb;
     : MgdM_MainMgdDcdInfo.mgdSdOvVdh;
     : MgdM_MainMgdDcdInfo.mgdSdOvVs;
     : MgdM_MainMgdDcdInfo.mgdSdOt;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs3;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs2;
     : MgdM_MainMgdDcdInfo.mgdErrScdLs1;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs3;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs2;
     : MgdM_MainMgdDcdInfo.mgdErrScdHs1;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs3;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs2;
     : MgdM_MainMgdDcdInfo.mgdErrIndLs1;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs3;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs2;
     : MgdM_MainMgdDcdInfo.mgdErrIndHs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs2;
     : MgdM_MainMgdDcdInfo.mgdErrOsfLs3;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs1;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs2;
     : MgdM_MainMgdDcdInfo.mgdErrOsfHs3;
     : MgdM_MainMgdDcdInfo.mgdErrSpiFrame;
     : MgdM_MainMgdDcdInfo.mgdErrSpiTo;
     : MgdM_MainMgdDcdInfo.mgdErrSpiWd;
     : MgdM_MainMgdDcdInfo.mgdErrSpiCrc;
     : MgdM_MainMgdDcdInfo.mgdEpiAddInvalid;
     : MgdM_MainMgdDcdInfo.mgdEonfTo;
     : MgdM_MainMgdDcdInfo.mgdEonfSigInvalid;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp1;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp1Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp2;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp2Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOcOp3;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Uv;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Ov;
     : MgdM_MainMgdDcdInfo.mgdErrOp3Calib;
     : MgdM_MainMgdDcdInfo.mgdErrOutpErrn;
     : MgdM_MainMgdDcdInfo.mgdErrOutpMiso;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB1;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB2;
     : MgdM_MainMgdDcdInfo.mgdErrOutpPFB3;
     : MgdM_MainMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/
    
    MgdM_Main_Read_MgdM_MainEcuModeSts(&MgdM_MainBus.MgdM_MainEcuModeSts);
    MgdM_Main_Read_MgdM_MainIgnOnOffSts(&MgdM_MainBus.MgdM_MainIgnOnOffSts);
    MgdM_Main_Read_MgdM_MainIgnEdgeSts(&MgdM_MainBus.MgdM_MainIgnEdgeSts);
    MgdM_Main_Read_MgdM_MainVBatt1Mon(&MgdM_MainBus.MgdM_MainVBatt1Mon);
    MgdM_Main_Read_MgdM_MainDiagClrSrs(&MgdM_MainBus.MgdM_MainDiagClrSrs);
    MgdM_Main_Read_MgdM_MainMgdInvalid(&MgdM_MainBus.MgdM_MainMgdInvalid);
    MgdM_Main_Read_MgdM_MainMtrArbDriveState(&MgdM_MainBus.MgdM_MainMtrArbDriveState);

    /* Process */

    /* Output */

    MgdM_Main_Timer_Elapsed = STM0_TIM0.U - MgdM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MGDM_MAIN_STOP_SEC_CODE
#include "MgdM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
