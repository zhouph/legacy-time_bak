#define S_FUNCTION_NAME      MgdM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          105
#define WidthOutputPort         0

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "MgdM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    MgdM_MainMgdDcdInfo.mgdIdleM = input[0];
    MgdM_MainMgdDcdInfo.mgdConfM = input[1];
    MgdM_MainMgdDcdInfo.mgdConfLock = input[2];
    MgdM_MainMgdDcdInfo.mgdSelfTestM = input[3];
    MgdM_MainMgdDcdInfo.mgdSoffM = input[4];
    MgdM_MainMgdDcdInfo.mgdErrM = input[5];
    MgdM_MainMgdDcdInfo.mgdRectM = input[6];
    MgdM_MainMgdDcdInfo.mgdNormM = input[7];
    MgdM_MainMgdDcdInfo.mgdOsf = input[8];
    MgdM_MainMgdDcdInfo.mgdOp = input[9];
    MgdM_MainMgdDcdInfo.mgdScd = input[10];
    MgdM_MainMgdDcdInfo.mgdSd = input[11];
    MgdM_MainMgdDcdInfo.mgdIndiag = input[12];
    MgdM_MainMgdDcdInfo.mgdOutp = input[13];
    MgdM_MainMgdDcdInfo.mgdExt = input[14];
    MgdM_MainMgdDcdInfo.mgdInt12 = input[15];
    MgdM_MainMgdDcdInfo.mgdRom = input[16];
    MgdM_MainMgdDcdInfo.mgdLimpOn = input[17];
    MgdM_MainMgdDcdInfo.mgdStIncomplete = input[18];
    MgdM_MainMgdDcdInfo.mgdApcAct = input[19];
    MgdM_MainMgdDcdInfo.mgdGtm = input[20];
    MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid = input[21];
    MgdM_MainMgdDcdInfo.mgdLfw = input[22];
    MgdM_MainMgdDcdInfo.mgdErrOtW = input[23];
    MgdM_MainMgdDcdInfo.mgdErrOvReg1 = input[24];
    MgdM_MainMgdDcdInfo.mgdErrUvVccRom = input[25];
    MgdM_MainMgdDcdInfo.mgdErrUvReg4 = input[26];
    MgdM_MainMgdDcdInfo.mgdErrOvReg6 = input[27];
    MgdM_MainMgdDcdInfo.mgdErrUvReg6 = input[28];
    MgdM_MainMgdDcdInfo.mgdErrUvReg5 = input[29];
    MgdM_MainMgdDcdInfo.mgdErrUvCb = input[30];
    MgdM_MainMgdDcdInfo.mgdErrClkTrim = input[31];
    MgdM_MainMgdDcdInfo.mgdErrUvBs3 = input[32];
    MgdM_MainMgdDcdInfo.mgdErrUvBs2 = input[33];
    MgdM_MainMgdDcdInfo.mgdErrUvBs1 = input[34];
    MgdM_MainMgdDcdInfo.mgdErrCp2 = input[35];
    MgdM_MainMgdDcdInfo.mgdErrCp1 = input[36];
    MgdM_MainMgdDcdInfo.mgdErrOvBs3 = input[37];
    MgdM_MainMgdDcdInfo.mgdErrOvBs2 = input[38];
    MgdM_MainMgdDcdInfo.mgdErrOvBs1 = input[39];
    MgdM_MainMgdDcdInfo.mgdErrOvVdh = input[40];
    MgdM_MainMgdDcdInfo.mgdErrUvVdh = input[41];
    MgdM_MainMgdDcdInfo.mgdErrOvVs = input[42];
    MgdM_MainMgdDcdInfo.mgdErrUvVs = input[43];
    MgdM_MainMgdDcdInfo.mgdErrUvVcc = input[44];
    MgdM_MainMgdDcdInfo.mgdErrOvVcc = input[45];
    MgdM_MainMgdDcdInfo.mgdErrOvLdVdh = input[46];
    MgdM_MainMgdDcdInfo.mgdSdDdpStuck = input[47];
    MgdM_MainMgdDcdInfo.mgdSdCp1 = input[48];
    MgdM_MainMgdDcdInfo.mgdSdOvCp = input[49];
    MgdM_MainMgdDcdInfo.mgdSdClkfail = input[50];
    MgdM_MainMgdDcdInfo.mgdSdUvCb = input[51];
    MgdM_MainMgdDcdInfo.mgdSdOvVdh = input[52];
    MgdM_MainMgdDcdInfo.mgdSdOvVs = input[53];
    MgdM_MainMgdDcdInfo.mgdSdOt = input[54];
    MgdM_MainMgdDcdInfo.mgdErrScdLs3 = input[55];
    MgdM_MainMgdDcdInfo.mgdErrScdLs2 = input[56];
    MgdM_MainMgdDcdInfo.mgdErrScdLs1 = input[57];
    MgdM_MainMgdDcdInfo.mgdErrScdHs3 = input[58];
    MgdM_MainMgdDcdInfo.mgdErrScdHs2 = input[59];
    MgdM_MainMgdDcdInfo.mgdErrScdHs1 = input[60];
    MgdM_MainMgdDcdInfo.mgdErrIndLs3 = input[61];
    MgdM_MainMgdDcdInfo.mgdErrIndLs2 = input[62];
    MgdM_MainMgdDcdInfo.mgdErrIndLs1 = input[63];
    MgdM_MainMgdDcdInfo.mgdErrIndHs3 = input[64];
    MgdM_MainMgdDcdInfo.mgdErrIndHs2 = input[65];
    MgdM_MainMgdDcdInfo.mgdErrIndHs1 = input[66];
    MgdM_MainMgdDcdInfo.mgdErrOsfLs1 = input[67];
    MgdM_MainMgdDcdInfo.mgdErrOsfLs2 = input[68];
    MgdM_MainMgdDcdInfo.mgdErrOsfLs3 = input[69];
    MgdM_MainMgdDcdInfo.mgdErrOsfHs1 = input[70];
    MgdM_MainMgdDcdInfo.mgdErrOsfHs2 = input[71];
    MgdM_MainMgdDcdInfo.mgdErrOsfHs3 = input[72];
    MgdM_MainMgdDcdInfo.mgdErrSpiFrame = input[73];
    MgdM_MainMgdDcdInfo.mgdErrSpiTo = input[74];
    MgdM_MainMgdDcdInfo.mgdErrSpiWd = input[75];
    MgdM_MainMgdDcdInfo.mgdErrSpiCrc = input[76];
    MgdM_MainMgdDcdInfo.mgdEpiAddInvalid = input[77];
    MgdM_MainMgdDcdInfo.mgdEonfTo = input[78];
    MgdM_MainMgdDcdInfo.mgdEonfSigInvalid = input[79];
    MgdM_MainMgdDcdInfo.mgdErrOcOp1 = input[80];
    MgdM_MainMgdDcdInfo.mgdErrOp1Uv = input[81];
    MgdM_MainMgdDcdInfo.mgdErrOp1Ov = input[82];
    MgdM_MainMgdDcdInfo.mgdErrOp1Calib = input[83];
    MgdM_MainMgdDcdInfo.mgdErrOcOp2 = input[84];
    MgdM_MainMgdDcdInfo.mgdErrOp2Uv = input[85];
    MgdM_MainMgdDcdInfo.mgdErrOp2Ov = input[86];
    MgdM_MainMgdDcdInfo.mgdErrOp2Calib = input[87];
    MgdM_MainMgdDcdInfo.mgdErrOcOp3 = input[88];
    MgdM_MainMgdDcdInfo.mgdErrOp3Uv = input[89];
    MgdM_MainMgdDcdInfo.mgdErrOp3Ov = input[90];
    MgdM_MainMgdDcdInfo.mgdErrOp3Calib = input[91];
    MgdM_MainMgdDcdInfo.mgdErrOutpErrn = input[92];
    MgdM_MainMgdDcdInfo.mgdErrOutpMiso = input[93];
    MgdM_MainMgdDcdInfo.mgdErrOutpPFB1 = input[94];
    MgdM_MainMgdDcdInfo.mgdErrOutpPFB2 = input[95];
    MgdM_MainMgdDcdInfo.mgdErrOutpPFB3 = input[96];
    MgdM_MainMgdDcdInfo.mgdTle9180ErrPort = input[97];
    MgdM_MainEcuModeSts = input[98];
    MgdM_MainIgnOnOffSts = input[99];
    MgdM_MainIgnEdgeSts = input[100];
    MgdM_MainVBatt1Mon = input[101];
    MgdM_MainDiagClrSrs = input[102];
    MgdM_MainMgdInvalid = input[103];
    MgdM_MainMtrArbDriveState = input[104];

    MgdM_Main();


    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    MgdM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
