MgdM_MainMgdDcdInfo = Simulink.Bus;
DeList={
    'mgdIdleM'
    'mgdConfM'
    'mgdConfLock'
    'mgdSelfTestM'
    'mgdSoffM'
    'mgdErrM'
    'mgdRectM'
    'mgdNormM'
    'mgdOsf'
    'mgdOp'
    'mgdScd'
    'mgdSd'
    'mgdIndiag'
    'mgdOutp'
    'mgdExt'
    'mgdInt12'
    'mgdRom'
    'mgdLimpOn'
    'mgdStIncomplete'
    'mgdApcAct'
    'mgdGtm'
    'mgdCtrlRegInvalid'
    'mgdLfw'
    'mgdErrOtW'
    'mgdErrOvReg1'
    'mgdErrUvVccRom'
    'mgdErrUvReg4'
    'mgdErrOvReg6'
    'mgdErrUvReg6'
    'mgdErrUvReg5'
    'mgdErrUvCb'
    'mgdErrClkTrim'
    'mgdErrUvBs3'
    'mgdErrUvBs2'
    'mgdErrUvBs1'
    'mgdErrCp2'
    'mgdErrCp1'
    'mgdErrOvBs3'
    'mgdErrOvBs2'
    'mgdErrOvBs1'
    'mgdErrOvVdh'
    'mgdErrUvVdh'
    'mgdErrOvVs'
    'mgdErrUvVs'
    'mgdErrUvVcc'
    'mgdErrOvVcc'
    'mgdErrOvLdVdh'
    'mgdSdDdpStuck'
    'mgdSdCp1'
    'mgdSdOvCp'
    'mgdSdClkfail'
    'mgdSdUvCb'
    'mgdSdOvVdh'
    'mgdSdOvVs'
    'mgdSdOt'
    'mgdErrScdLs3'
    'mgdErrScdLs2'
    'mgdErrScdLs1'
    'mgdErrScdHs3'
    'mgdErrScdHs2'
    'mgdErrScdHs1'
    'mgdErrIndLs3'
    'mgdErrIndLs2'
    'mgdErrIndLs1'
    'mgdErrIndHs3'
    'mgdErrIndHs2'
    'mgdErrIndHs1'
    'mgdErrOsfLs1'
    'mgdErrOsfLs2'
    'mgdErrOsfLs3'
    'mgdErrOsfHs1'
    'mgdErrOsfHs2'
    'mgdErrOsfHs3'
    'mgdErrSpiFrame'
    'mgdErrSpiTo'
    'mgdErrSpiWd'
    'mgdErrSpiCrc'
    'mgdEpiAddInvalid'
    'mgdEonfTo'
    'mgdEonfSigInvalid'
    'mgdErrOcOp1'
    'mgdErrOp1Uv'
    'mgdErrOp1Ov'
    'mgdErrOp1Calib'
    'mgdErrOcOp2'
    'mgdErrOp2Uv'
    'mgdErrOp2Ov'
    'mgdErrOp2Calib'
    'mgdErrOcOp3'
    'mgdErrOp3Uv'
    'mgdErrOp3Ov'
    'mgdErrOp3Calib'
    'mgdErrOutpErrn'
    'mgdErrOutpMiso'
    'mgdErrOutpPFB1'
    'mgdErrOutpPFB2'
    'mgdErrOutpPFB3'
    'mgdTle9180ErrPort'
    };
MgdM_MainMgdDcdInfo = CreateBus(MgdM_MainMgdDcdInfo, DeList);
clear DeList;

MgdM_MainEcuModeSts = Simulink.Bus;
DeList={'MgdM_MainEcuModeSts'};
MgdM_MainEcuModeSts = CreateBus(MgdM_MainEcuModeSts, DeList);
clear DeList;

MgdM_MainIgnOnOffSts = Simulink.Bus;
DeList={'MgdM_MainIgnOnOffSts'};
MgdM_MainIgnOnOffSts = CreateBus(MgdM_MainIgnOnOffSts, DeList);
clear DeList;

MgdM_MainIgnEdgeSts = Simulink.Bus;
DeList={'MgdM_MainIgnEdgeSts'};
MgdM_MainIgnEdgeSts = CreateBus(MgdM_MainIgnEdgeSts, DeList);
clear DeList;

MgdM_MainVBatt1Mon = Simulink.Bus;
DeList={'MgdM_MainVBatt1Mon'};
MgdM_MainVBatt1Mon = CreateBus(MgdM_MainVBatt1Mon, DeList);
clear DeList;

MgdM_MainDiagClrSrs = Simulink.Bus;
DeList={'MgdM_MainDiagClrSrs'};
MgdM_MainDiagClrSrs = CreateBus(MgdM_MainDiagClrSrs, DeList);
clear DeList;

MgdM_MainMgdInvalid = Simulink.Bus;
DeList={'MgdM_MainMgdInvalid'};
MgdM_MainMgdInvalid = CreateBus(MgdM_MainMgdInvalid, DeList);
clear DeList;

MgdM_MainMtrArbDriveState = Simulink.Bus;
DeList={'MgdM_MainMtrArbDriveState'};
MgdM_MainMtrArbDriveState = CreateBus(MgdM_MainMtrArbDriveState, DeList);
clear DeList;

