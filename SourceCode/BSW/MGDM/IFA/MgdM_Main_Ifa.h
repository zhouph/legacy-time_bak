/**
 * @defgroup MgdM_Main_Ifa MgdM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MgdM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGDM_MAIN_IFA_H_
#define MGDM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define MgdM_Main_Read_MgdM_MainMgdDcdInfo(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdIdleM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdIdleM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdConfM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdConfM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdConfLock(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdConfLock; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSelfTestM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSelfTestM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSoffM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSoffM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdRectM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdRectM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdNormM(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdNormM; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdOsf(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdOsf; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdOp(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdOp; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdScd(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdScd; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSd(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSd; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdIndiag(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdIndiag; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdOutp(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdOutp; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdExt(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdExt; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdInt12(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdInt12; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdRom(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdRom; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdLimpOn(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdLimpOn; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdStIncomplete(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdStIncomplete; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdApcAct(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdApcAct; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdGtm(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdGtm; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdCtrlRegInvalid(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdLfw(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdLfw; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOtW(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOtW; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvReg1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvReg1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvVccRom(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvVccRom; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvReg4(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvReg4; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvReg6(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvReg6; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvReg6(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvReg6; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvReg5(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvReg5; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvCb(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvCb; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrClkTrim(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrClkTrim; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvBs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvBs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvBs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvBs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvBs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvBs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrCp2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrCp2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrCp1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrCp1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvBs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvBs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvBs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvBs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvBs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvBs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvVdh(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvVdh; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvVdh(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvVdh; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvVs(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvVs; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvVs(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvVs; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrUvVcc(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrUvVcc; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvVcc(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvVcc; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOvLdVdh(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOvLdVdh; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdDdpStuck(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdDdpStuck; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdCp1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdCp1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdOvCp(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdOvCp; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdClkfail(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdClkfail; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdUvCb(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdUvCb; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdOvVdh(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdOvVdh; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdOvVs(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdOvVs; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdSdOt(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdSdOt; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrScdLs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrScdLs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrScdLs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrScdLs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrScdLs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrScdLs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrScdHs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrScdHs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrScdHs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrScdHs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrScdHs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrScdHs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrIndLs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrIndLs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrIndLs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrIndLs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrIndLs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrIndLs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrIndHs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrIndHs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrIndHs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrIndHs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrIndHs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrIndHs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOsfLs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOsfLs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOsfLs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOsfLs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOsfLs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOsfLs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOsfHs1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOsfHs1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOsfHs2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOsfHs2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOsfHs3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOsfHs3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrSpiFrame(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrSpiFrame; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrSpiTo(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrSpiTo; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrSpiWd(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrSpiWd; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrSpiCrc(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrSpiCrc; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdEpiAddInvalid(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdEpiAddInvalid; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdEonfTo(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdEonfTo; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdEonfSigInvalid(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdEonfSigInvalid; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOcOp1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOcOp1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp1Uv(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp1Uv; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp1Ov(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp1Ov; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp1Calib(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp1Calib; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOcOp2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOcOp2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp2Uv(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp2Uv; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp2Ov(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp2Ov; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp2Calib(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp2Calib; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOcOp3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOcOp3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp3Uv(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp3Uv; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp3Ov(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp3Ov; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOp3Calib(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOp3Calib; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOutpErrn(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOutpErrn; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOutpMiso(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOutpMiso; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOutpPFB1(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOutpPFB1; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOutpPFB2(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOutpPFB2; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdErrOutpPFB3(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdErrOutpPFB3; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdDcdInfo_mgdTle9180ErrPort(data) do \
{ \
    *data = MgdM_MainMgdDcdInfo.mgdTle9180ErrPort; \
}while(0);

#define MgdM_Main_Read_MgdM_MainEcuModeSts(data) do \
{ \
    *data = MgdM_MainEcuModeSts; \
}while(0);

#define MgdM_Main_Read_MgdM_MainIgnOnOffSts(data) do \
{ \
    *data = MgdM_MainIgnOnOffSts; \
}while(0);

#define MgdM_Main_Read_MgdM_MainIgnEdgeSts(data) do \
{ \
    *data = MgdM_MainIgnEdgeSts; \
}while(0);

#define MgdM_Main_Read_MgdM_MainVBatt1Mon(data) do \
{ \
    *data = MgdM_MainVBatt1Mon; \
}while(0);

#define MgdM_Main_Read_MgdM_MainDiagClrSrs(data) do \
{ \
    *data = MgdM_MainDiagClrSrs; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMgdInvalid(data) do \
{ \
    *data = MgdM_MainMgdInvalid; \
}while(0);

#define MgdM_Main_Read_MgdM_MainMtrArbDriveState(data) do \
{ \
    *data = MgdM_MainMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGDM_MAIN_IFA_H_ */
/** @} */
