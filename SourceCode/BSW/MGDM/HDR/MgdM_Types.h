/**
 * @defgroup MgdM_Types MgdM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MgdM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGDM_TYPES_H_
#define MGDM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mgd_TLE9180_HndlrMgdDcdInfo_t MgdM_MainMgdDcdInfo;
    Mom_HndlrEcuModeSts_t MgdM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t MgdM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t MgdM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t MgdM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t MgdM_MainDiagClrSrs;
    Mgd_TLE9180_HndlrMgdInvalid_t MgdM_MainMgdInvalid;
    Arbitrator_MtrMtrArbDriveState_t MgdM_MainMtrArbDriveState;

/* Output Data Element */
}MgdM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGDM_TYPES_H_ */
/** @} */
