/**
 * @defgroup MgdM_MemMap MgdM_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MgdM_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGDM_MEMMAP_H_
#define MGDM_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (MGDM_START_SEC_CODE)
  #undef MGDM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_CODE)
  #undef MGDM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_CODE_STARTED
    #error "MGDM_SEC_CODE not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (MGDM_START_SEC_CONST_UNSPECIFIED)
  #undef MGDM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_CONST_UNSPECIFIED)
  #undef MGDM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_CONST_UNSPECIFIED_STARTED
    #error "MGDM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (MGDM_START_SEC_VAR_UNSPECIFIED)
  #undef MGDM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_VAR_UNSPECIFIED)
  #undef MGDM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_VAR_UNSPECIFIED_STARTED
    #error "MGDM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_START_SEC_VAR_32BIT)
  #undef MGDM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_VAR_32BIT)
  #undef MGDM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_VAR_32BIT_STARTED
    #error "MGDM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MGDM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MGDM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "MGDM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_START_SEC_VAR_NOINIT_32BIT)
  #undef MGDM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef MGDM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "MGDM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (MGDM_START_SEC_CALIB_UNSPECIFIED)
  #undef MGDM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_SEC_STARTED
    #error "MGDM section not closed"
  #endif
  #define CHK_MGDM_SEC_STARTED
  #define CHK_MGDM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef MGDM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "MGDM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_SEC_STARTED
  #undef CHK_MGDM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (MGDM_MAIN_START_SEC_CODE)
  #undef MGDM_MAIN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_CODE)
  #undef MGDM_MAIN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_CODE_STARTED
    #error "MGDM_MAIN_SEC_CODE not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (MGDM_MAIN_START_SEC_CONST_UNSPECIFIED)
  #undef MGDM_MAIN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_CONST_UNSPECIFIED)
  #undef MGDM_MAIN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
    #error "MGDM_MAIN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (MGDM_MAIN_START_SEC_VAR_UNSPECIFIED)
  #undef MGDM_MAIN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_VAR_UNSPECIFIED)
  #undef MGDM_MAIN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
    #error "MGDM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_START_SEC_VAR_32BIT)
  #undef MGDM_MAIN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_VAR_32BIT)
  #undef MGDM_MAIN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_VAR_32BIT_STARTED
    #error "MGDM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MGDM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MGDM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "MGDM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_START_SEC_VAR_NOINIT_32BIT)
  #undef MGDM_MAIN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef MGDM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "MGDM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (MGDM_MAIN_START_SEC_CALIB_UNSPECIFIED)
  #undef MGDM_MAIN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MGDM_MAIN_SEC_STARTED
    #error "MGDM_MAIN section not closed"
  #endif
  #define CHK_MGDM_MAIN_SEC_STARTED
  #define CHK_MGDM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MGDM_MAIN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef MGDM_MAIN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MGDM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "MGDM_MAIN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_MGDM_MAIN_SEC_STARTED
  #undef CHK_MGDM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGDM_MEMMAP_H_ */
/** @} */
