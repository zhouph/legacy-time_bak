/**
 * @defgroup MgdM_Main MgdM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MgdM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGDM_MAIN_H_
#define MGDM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MgdM_Types.h"
#include "MgdM_Cfg.h"
#include "MgdM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MGDM_MAIN_MODULE_ID      (0)
 #define MGDM_MAIN_MAJOR_VERSION  (2)
 #define MGDM_MAIN_MINOR_VERSION  (0)
 #define MGDM_MAIN_PATCH_VERSION  (0)
 #define MGDM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern MgdM_Main_HdrBusType MgdM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t MgdM_MainVersionInfo;

/* Input Data Element */
extern Mgd_TLE9180_HndlrMgdDcdInfo_t MgdM_MainMgdDcdInfo;
extern Mom_HndlrEcuModeSts_t MgdM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t MgdM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t MgdM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t MgdM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t MgdM_MainDiagClrSrs;
extern Mgd_TLE9180_HndlrMgdInvalid_t MgdM_MainMgdInvalid;
extern Arbitrator_MtrMtrArbDriveState_t MgdM_MainMtrArbDriveState;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void MgdM_Main_Init(void);
extern void MgdM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGDM_MAIN_H_ */
/** @} */
