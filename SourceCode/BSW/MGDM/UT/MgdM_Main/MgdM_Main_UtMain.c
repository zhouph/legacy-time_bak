#include "unity.h"
#include "unity_fixture.h"
#include "MgdM_Main.h"
#include "MgdM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdIdleM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDIDLEM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdConfM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDCONFM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdConfLock[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDCONFLOCK;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSelfTestM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSELFTESTM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSoffM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSOFFM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdRectM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDRECTM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdNormM[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDNORMM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdOsf[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDOSF;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdOp[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDOP;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdScd[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSCD;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSd[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSD;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdIndiag[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDINDIAG;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdOutp[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDOUTP;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdExt[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDEXT;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdInt12[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDINT12;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdRom[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDROM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdLimpOn[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDLIMPON;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdStIncomplete[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSTINCOMPLETE;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdApcAct[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDAPCACT;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdGtm[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDGTM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdCtrlRegInvalid[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDCTRLREGINVALID;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdLfw[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDLFW;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOtW[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROTW;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvReg1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVREG1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVccRom[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVVCCROM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvReg4[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVREG4;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvReg6[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVREG6;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvReg6[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVREG6;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvReg5[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVREG5;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvCb[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVCB;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrClkTrim[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRCLKTRIM;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvBs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVBS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvBs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVBS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvBs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVBS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrCp2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRCP2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrCp1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRCP1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvBs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVBS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvBs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVBS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvBs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVBS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvVdh[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVVDH;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVdh[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVVDH;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvVs[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVVS;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVs[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVVS;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVcc[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRUVVCC;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvVcc[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVVCC;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOvLdVdh[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROVLDVDH;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdDdpStuck[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDDDPSTUCK;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdCp1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDCP1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdOvCp[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDOVCP;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdClkfail[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDCLKFAIL;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdUvCb[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDUVCB;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdOvVdh[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDOVVDH;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdOvVs[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDOVVS;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdSdOt[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDSDOT;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrScdLs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSCDLS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrScdLs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSCDLS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrScdLs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSCDLS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrScdHs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSCDHS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrScdHs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSCDHS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrScdHs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSCDHS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrIndLs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRINDLS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrIndLs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRINDLS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrIndLs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRINDLS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrIndHs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRINDHS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrIndHs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRINDHS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrIndHs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRINDHS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfLs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROSFLS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfLs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROSFLS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfLs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROSFLS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfHs1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROSFHS1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfHs2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROSFHS2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfHs3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROSFHS3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiFrame[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSPIFRAME;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiTo[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSPITO;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiWd[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSPIWD;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiCrc[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERRSPICRC;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdEpiAddInvalid[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDEPIADDINVALID;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdEonfTo[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDEONFTO;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdEonfSigInvalid[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDEONFSIGINVALID;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOcOp1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROCOP1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp1Uv[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP1UV;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp1Ov[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP1OV;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp1Calib[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP1CALIB;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOcOp2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROCOP2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp2Uv[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP2UV;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp2Ov[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP2OV;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp2Calib[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP2CALIB;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOcOp3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROCOP3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp3Uv[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP3UV;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp3Ov[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP3OV;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOp3Calib[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROP3CALIB;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpErrn[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROUTPERRN;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpMiso[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROUTPMISO;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpPFB1[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROUTPPFB1;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpPFB2[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROUTPPFB2;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpPFB3[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDERROUTPPFB3;
const Haluint8 UtInput_MgdM_MainMgdDcdInfo_mgdTle9180ErrPort[MAX_STEP] = MGDM_MAINMGDDCDINFO_MGDTLE9180ERRPORT;
const Mom_HndlrEcuModeSts_t UtInput_MgdM_MainEcuModeSts[MAX_STEP] = MGDM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_MgdM_MainIgnOnOffSts[MAX_STEP] = MGDM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_MgdM_MainIgnEdgeSts[MAX_STEP] = MGDM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_MgdM_MainVBatt1Mon[MAX_STEP] = MGDM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_MgdM_MainDiagClrSrs[MAX_STEP] = MGDM_MAINDIAGCLRSRS;
const Mgd_TLE9180_HndlrMgdInvalid_t UtInput_MgdM_MainMgdInvalid[MAX_STEP] = MGDM_MAINMGDINVALID;
const Arbitrator_MtrMtrArbDriveState_t UtInput_MgdM_MainMtrArbDriveState[MAX_STEP] = MGDM_MAINMTRARBDRIVESTATE;




TEST_GROUP(MgdM_Main);
TEST_SETUP(MgdM_Main)
{
    MgdM_Main_Init();
}

TEST_TEAR_DOWN(MgdM_Main)
{   /* Postcondition */

}

TEST(MgdM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        MgdM_MainMgdDcdInfo.mgdIdleM = UtInput_MgdM_MainMgdDcdInfo_mgdIdleM[i];
        MgdM_MainMgdDcdInfo.mgdConfM = UtInput_MgdM_MainMgdDcdInfo_mgdConfM[i];
        MgdM_MainMgdDcdInfo.mgdConfLock = UtInput_MgdM_MainMgdDcdInfo_mgdConfLock[i];
        MgdM_MainMgdDcdInfo.mgdSelfTestM = UtInput_MgdM_MainMgdDcdInfo_mgdSelfTestM[i];
        MgdM_MainMgdDcdInfo.mgdSoffM = UtInput_MgdM_MainMgdDcdInfo_mgdSoffM[i];
        MgdM_MainMgdDcdInfo.mgdErrM = UtInput_MgdM_MainMgdDcdInfo_mgdErrM[i];
        MgdM_MainMgdDcdInfo.mgdRectM = UtInput_MgdM_MainMgdDcdInfo_mgdRectM[i];
        MgdM_MainMgdDcdInfo.mgdNormM = UtInput_MgdM_MainMgdDcdInfo_mgdNormM[i];
        MgdM_MainMgdDcdInfo.mgdOsf = UtInput_MgdM_MainMgdDcdInfo_mgdOsf[i];
        MgdM_MainMgdDcdInfo.mgdOp = UtInput_MgdM_MainMgdDcdInfo_mgdOp[i];
        MgdM_MainMgdDcdInfo.mgdScd = UtInput_MgdM_MainMgdDcdInfo_mgdScd[i];
        MgdM_MainMgdDcdInfo.mgdSd = UtInput_MgdM_MainMgdDcdInfo_mgdSd[i];
        MgdM_MainMgdDcdInfo.mgdIndiag = UtInput_MgdM_MainMgdDcdInfo_mgdIndiag[i];
        MgdM_MainMgdDcdInfo.mgdOutp = UtInput_MgdM_MainMgdDcdInfo_mgdOutp[i];
        MgdM_MainMgdDcdInfo.mgdExt = UtInput_MgdM_MainMgdDcdInfo_mgdExt[i];
        MgdM_MainMgdDcdInfo.mgdInt12 = UtInput_MgdM_MainMgdDcdInfo_mgdInt12[i];
        MgdM_MainMgdDcdInfo.mgdRom = UtInput_MgdM_MainMgdDcdInfo_mgdRom[i];
        MgdM_MainMgdDcdInfo.mgdLimpOn = UtInput_MgdM_MainMgdDcdInfo_mgdLimpOn[i];
        MgdM_MainMgdDcdInfo.mgdStIncomplete = UtInput_MgdM_MainMgdDcdInfo_mgdStIncomplete[i];
        MgdM_MainMgdDcdInfo.mgdApcAct = UtInput_MgdM_MainMgdDcdInfo_mgdApcAct[i];
        MgdM_MainMgdDcdInfo.mgdGtm = UtInput_MgdM_MainMgdDcdInfo_mgdGtm[i];
        MgdM_MainMgdDcdInfo.mgdCtrlRegInvalid = UtInput_MgdM_MainMgdDcdInfo_mgdCtrlRegInvalid[i];
        MgdM_MainMgdDcdInfo.mgdLfw = UtInput_MgdM_MainMgdDcdInfo_mgdLfw[i];
        MgdM_MainMgdDcdInfo.mgdErrOtW = UtInput_MgdM_MainMgdDcdInfo_mgdErrOtW[i];
        MgdM_MainMgdDcdInfo.mgdErrOvReg1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvReg1[i];
        MgdM_MainMgdDcdInfo.mgdErrUvVccRom = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVccRom[i];
        MgdM_MainMgdDcdInfo.mgdErrUvReg4 = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvReg4[i];
        MgdM_MainMgdDcdInfo.mgdErrOvReg6 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvReg6[i];
        MgdM_MainMgdDcdInfo.mgdErrUvReg6 = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvReg6[i];
        MgdM_MainMgdDcdInfo.mgdErrUvReg5 = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvReg5[i];
        MgdM_MainMgdDcdInfo.mgdErrUvCb = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvCb[i];
        MgdM_MainMgdDcdInfo.mgdErrClkTrim = UtInput_MgdM_MainMgdDcdInfo_mgdErrClkTrim[i];
        MgdM_MainMgdDcdInfo.mgdErrUvBs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvBs3[i];
        MgdM_MainMgdDcdInfo.mgdErrUvBs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvBs2[i];
        MgdM_MainMgdDcdInfo.mgdErrUvBs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvBs1[i];
        MgdM_MainMgdDcdInfo.mgdErrCp2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrCp2[i];
        MgdM_MainMgdDcdInfo.mgdErrCp1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrCp1[i];
        MgdM_MainMgdDcdInfo.mgdErrOvBs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvBs3[i];
        MgdM_MainMgdDcdInfo.mgdErrOvBs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvBs2[i];
        MgdM_MainMgdDcdInfo.mgdErrOvBs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvBs1[i];
        MgdM_MainMgdDcdInfo.mgdErrOvVdh = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvVdh[i];
        MgdM_MainMgdDcdInfo.mgdErrUvVdh = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVdh[i];
        MgdM_MainMgdDcdInfo.mgdErrOvVs = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvVs[i];
        MgdM_MainMgdDcdInfo.mgdErrUvVs = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVs[i];
        MgdM_MainMgdDcdInfo.mgdErrUvVcc = UtInput_MgdM_MainMgdDcdInfo_mgdErrUvVcc[i];
        MgdM_MainMgdDcdInfo.mgdErrOvVcc = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvVcc[i];
        MgdM_MainMgdDcdInfo.mgdErrOvLdVdh = UtInput_MgdM_MainMgdDcdInfo_mgdErrOvLdVdh[i];
        MgdM_MainMgdDcdInfo.mgdSdDdpStuck = UtInput_MgdM_MainMgdDcdInfo_mgdSdDdpStuck[i];
        MgdM_MainMgdDcdInfo.mgdSdCp1 = UtInput_MgdM_MainMgdDcdInfo_mgdSdCp1[i];
        MgdM_MainMgdDcdInfo.mgdSdOvCp = UtInput_MgdM_MainMgdDcdInfo_mgdSdOvCp[i];
        MgdM_MainMgdDcdInfo.mgdSdClkfail = UtInput_MgdM_MainMgdDcdInfo_mgdSdClkfail[i];
        MgdM_MainMgdDcdInfo.mgdSdUvCb = UtInput_MgdM_MainMgdDcdInfo_mgdSdUvCb[i];
        MgdM_MainMgdDcdInfo.mgdSdOvVdh = UtInput_MgdM_MainMgdDcdInfo_mgdSdOvVdh[i];
        MgdM_MainMgdDcdInfo.mgdSdOvVs = UtInput_MgdM_MainMgdDcdInfo_mgdSdOvVs[i];
        MgdM_MainMgdDcdInfo.mgdSdOt = UtInput_MgdM_MainMgdDcdInfo_mgdSdOt[i];
        MgdM_MainMgdDcdInfo.mgdErrScdLs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrScdLs3[i];
        MgdM_MainMgdDcdInfo.mgdErrScdLs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrScdLs2[i];
        MgdM_MainMgdDcdInfo.mgdErrScdLs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrScdLs1[i];
        MgdM_MainMgdDcdInfo.mgdErrScdHs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrScdHs3[i];
        MgdM_MainMgdDcdInfo.mgdErrScdHs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrScdHs2[i];
        MgdM_MainMgdDcdInfo.mgdErrScdHs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrScdHs1[i];
        MgdM_MainMgdDcdInfo.mgdErrIndLs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrIndLs3[i];
        MgdM_MainMgdDcdInfo.mgdErrIndLs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrIndLs2[i];
        MgdM_MainMgdDcdInfo.mgdErrIndLs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrIndLs1[i];
        MgdM_MainMgdDcdInfo.mgdErrIndHs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrIndHs3[i];
        MgdM_MainMgdDcdInfo.mgdErrIndHs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrIndHs2[i];
        MgdM_MainMgdDcdInfo.mgdErrIndHs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrIndHs1[i];
        MgdM_MainMgdDcdInfo.mgdErrOsfLs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfLs1[i];
        MgdM_MainMgdDcdInfo.mgdErrOsfLs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfLs2[i];
        MgdM_MainMgdDcdInfo.mgdErrOsfLs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfLs3[i];
        MgdM_MainMgdDcdInfo.mgdErrOsfHs1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfHs1[i];
        MgdM_MainMgdDcdInfo.mgdErrOsfHs2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfHs2[i];
        MgdM_MainMgdDcdInfo.mgdErrOsfHs3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOsfHs3[i];
        MgdM_MainMgdDcdInfo.mgdErrSpiFrame = UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiFrame[i];
        MgdM_MainMgdDcdInfo.mgdErrSpiTo = UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiTo[i];
        MgdM_MainMgdDcdInfo.mgdErrSpiWd = UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiWd[i];
        MgdM_MainMgdDcdInfo.mgdErrSpiCrc = UtInput_MgdM_MainMgdDcdInfo_mgdErrSpiCrc[i];
        MgdM_MainMgdDcdInfo.mgdEpiAddInvalid = UtInput_MgdM_MainMgdDcdInfo_mgdEpiAddInvalid[i];
        MgdM_MainMgdDcdInfo.mgdEonfTo = UtInput_MgdM_MainMgdDcdInfo_mgdEonfTo[i];
        MgdM_MainMgdDcdInfo.mgdEonfSigInvalid = UtInput_MgdM_MainMgdDcdInfo_mgdEonfSigInvalid[i];
        MgdM_MainMgdDcdInfo.mgdErrOcOp1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOcOp1[i];
        MgdM_MainMgdDcdInfo.mgdErrOp1Uv = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp1Uv[i];
        MgdM_MainMgdDcdInfo.mgdErrOp1Ov = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp1Ov[i];
        MgdM_MainMgdDcdInfo.mgdErrOp1Calib = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp1Calib[i];
        MgdM_MainMgdDcdInfo.mgdErrOcOp2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOcOp2[i];
        MgdM_MainMgdDcdInfo.mgdErrOp2Uv = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp2Uv[i];
        MgdM_MainMgdDcdInfo.mgdErrOp2Ov = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp2Ov[i];
        MgdM_MainMgdDcdInfo.mgdErrOp2Calib = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp2Calib[i];
        MgdM_MainMgdDcdInfo.mgdErrOcOp3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOcOp3[i];
        MgdM_MainMgdDcdInfo.mgdErrOp3Uv = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp3Uv[i];
        MgdM_MainMgdDcdInfo.mgdErrOp3Ov = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp3Ov[i];
        MgdM_MainMgdDcdInfo.mgdErrOp3Calib = UtInput_MgdM_MainMgdDcdInfo_mgdErrOp3Calib[i];
        MgdM_MainMgdDcdInfo.mgdErrOutpErrn = UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpErrn[i];
        MgdM_MainMgdDcdInfo.mgdErrOutpMiso = UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpMiso[i];
        MgdM_MainMgdDcdInfo.mgdErrOutpPFB1 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpPFB1[i];
        MgdM_MainMgdDcdInfo.mgdErrOutpPFB2 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpPFB2[i];
        MgdM_MainMgdDcdInfo.mgdErrOutpPFB3 = UtInput_MgdM_MainMgdDcdInfo_mgdErrOutpPFB3[i];
        MgdM_MainMgdDcdInfo.mgdTle9180ErrPort = UtInput_MgdM_MainMgdDcdInfo_mgdTle9180ErrPort[i];
        MgdM_MainEcuModeSts = UtInput_MgdM_MainEcuModeSts[i];
        MgdM_MainIgnOnOffSts = UtInput_MgdM_MainIgnOnOffSts[i];
        MgdM_MainIgnEdgeSts = UtInput_MgdM_MainIgnEdgeSts[i];
        MgdM_MainVBatt1Mon = UtInput_MgdM_MainVBatt1Mon[i];
        MgdM_MainDiagClrSrs = UtInput_MgdM_MainDiagClrSrs[i];
        MgdM_MainMgdInvalid = UtInput_MgdM_MainMgdInvalid[i];
        MgdM_MainMtrArbDriveState = UtInput_MgdM_MainMtrArbDriveState[i];

        MgdM_Main();

    }
}

TEST_GROUP_RUNNER(MgdM_Main)
{
    RUN_TEST_CASE(MgdM_Main, All);
}
