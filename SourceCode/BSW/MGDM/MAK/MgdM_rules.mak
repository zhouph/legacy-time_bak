# \file
#
# \brief MgdM
#
# This file contains the implementation of the SWC
# module MgdM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += MgdM_src

MgdM_src_FILES        += $(MgdM_SRC_PATH)\MgdM_Main.c
MgdM_src_FILES        += $(MgdM_IFA_PATH)\MgdM_Main_Ifa.c
MgdM_src_FILES        += $(MgdM_CFG_PATH)\MgdM_Cfg.c
MgdM_src_FILES        += $(MgdM_CAL_PATH)\MgdM_Cal.c

ifeq ($(ICE_COMPILE),true)
MgdM_src_FILES        += $(MgdM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	MgdM_src_FILES        += $(MgdM_UNITY_PATH)\unity.c
	MgdM_src_FILES        += $(MgdM_UNITY_PATH)\unity_fixture.c	
	MgdM_src_FILES        += $(MgdM_UT_PATH)\main.c
	MgdM_src_FILES        += $(MgdM_UT_PATH)\MgdM_Main\MgdM_Main_UtMain.c
endif