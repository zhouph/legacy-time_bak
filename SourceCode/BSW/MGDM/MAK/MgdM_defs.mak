# \file
#
# \brief MgdM
#
# This file contains the implementation of the SWC
# module MgdM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

MgdM_CORE_PATH     := $(MANDO_BSW_ROOT)\MgdM
MgdM_CAL_PATH      := $(MgdM_CORE_PATH)\CAL\$(MgdM_VARIANT)
MgdM_SRC_PATH      := $(MgdM_CORE_PATH)\SRC
MgdM_CFG_PATH      := $(MgdM_CORE_PATH)\CFG\$(MgdM_VARIANT)
MgdM_HDR_PATH      := $(MgdM_CORE_PATH)\HDR
MgdM_IFA_PATH      := $(MgdM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    MgdM_CMN_PATH      := $(MgdM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	MgdM_UT_PATH		:= $(MgdM_CORE_PATH)\UT
	MgdM_UNITY_PATH	:= $(MgdM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(MgdM_UT_PATH)
	CC_INCLUDE_PATH		+= $(MgdM_UNITY_PATH)
	MgdM_Main_PATH 	:= MgdM_UT_PATH\MgdM_Main
endif
CC_INCLUDE_PATH    += $(MgdM_CAL_PATH)
CC_INCLUDE_PATH    += $(MgdM_SRC_PATH)
CC_INCLUDE_PATH    += $(MgdM_CFG_PATH)
CC_INCLUDE_PATH    += $(MgdM_HDR_PATH)
CC_INCLUDE_PATH    += $(MgdM_IFA_PATH)
CC_INCLUDE_PATH    += $(MgdM_CMN_PATH)

