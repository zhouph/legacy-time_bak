YawM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'YawRate'
    };
YawM_MainCanRxEscInfo = CreateBus(YawM_MainCanRxEscInfo, DeList);
clear DeList;

YawM_MainRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
YawM_MainRxYawSerialInfo = CreateBus(YawM_MainRxYawSerialInfo, DeList);
clear DeList;

YawM_MainRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    };
YawM_MainRxYawAccInfo = CreateBus(YawM_MainRxYawAccInfo, DeList);
clear DeList;

YawM_MainRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    };
YawM_MainRxLongAccInfo = CreateBus(YawM_MainRxLongAccInfo, DeList);
clear DeList;

YawM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'LongAccMsgOkFlg'
    };
YawM_MainRxMsgOkFlgInfo = CreateBus(YawM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

YawM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
YawM_MainSenPwrMonitorData = CreateBus(YawM_MainSenPwrMonitorData, DeList);
clear DeList;

YawM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
YawM_MainEemFailData = CreateBus(YawM_MainEemFailData, DeList);
clear DeList;

YawM_MainEcuModeSts = Simulink.Bus;
DeList={'YawM_MainEcuModeSts'};
YawM_MainEcuModeSts = CreateBus(YawM_MainEcuModeSts, DeList);
clear DeList;

YawM_MainIgnOnOffSts = Simulink.Bus;
DeList={'YawM_MainIgnOnOffSts'};
YawM_MainIgnOnOffSts = CreateBus(YawM_MainIgnOnOffSts, DeList);
clear DeList;

YawM_MainIgnEdgeSts = Simulink.Bus;
DeList={'YawM_MainIgnEdgeSts'};
YawM_MainIgnEdgeSts = CreateBus(YawM_MainIgnEdgeSts, DeList);
clear DeList;

YawM_MainVBatt1Mon = Simulink.Bus;
DeList={'YawM_MainVBatt1Mon'};
YawM_MainVBatt1Mon = CreateBus(YawM_MainVBatt1Mon, DeList);
clear DeList;

YawM_MainDiagClrSrs = Simulink.Bus;
DeList={'YawM_MainDiagClrSrs'};
YawM_MainDiagClrSrs = CreateBus(YawM_MainDiagClrSrs, DeList);
clear DeList;

YawM_MainYAWMSerialInfo = Simulink.Bus;
DeList={
    'YAWM_SerialNumOK_Flg'
    'YAWM_SerialUnMatch_Flg'
    };
YawM_MainYAWMSerialInfo = CreateBus(YawM_MainYAWMSerialInfo, DeList);
clear DeList;

YawM_MainYAWMOutInfo = Simulink.Bus;
DeList={
    'YAWM_Timeout_Err'
    'YAWM_Invalid_Err'
    'YAWM_CRC_Err'
    'YAWM_Rolling_Err'
    'YAWM_Temperature_Err'
    'YAWM_Range_Err'
    };
YawM_MainYAWMOutInfo = CreateBus(YawM_MainYAWMOutInfo, DeList);
clear DeList;

