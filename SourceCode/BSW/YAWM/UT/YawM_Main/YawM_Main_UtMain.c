#include "unity.h"
#include "unity_fixture.h"
#include "YawM_Main.h"
#include "YawM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_YawM_MainCanRxEscInfo_YawRate[MAX_STEP] = YAWM_MAINCANRXESCINFO_YAWRATE;
const Saluint8 UtInput_YawM_MainRxYawSerialInfo_YawSerialNum_0[MAX_STEP] = YAWM_MAINRXYAWSERIALINFO_YAWSERIALNUM_0;
const Saluint8 UtInput_YawM_MainRxYawSerialInfo_YawSerialNum_1[MAX_STEP] = YAWM_MAINRXYAWSERIALINFO_YAWSERIALNUM_1;
const Saluint8 UtInput_YawM_MainRxYawSerialInfo_YawSerialNum_2[MAX_STEP] = YAWM_MAINRXYAWSERIALINFO_YAWSERIALNUM_2;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_YawRateValidData[MAX_STEP] = YAWM_MAINRXYAWACCINFO_YAWRATEVALIDDATA;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_YawRateSelfTestStatus[MAX_STEP] = YAWM_MAINRXYAWACCINFO_YAWRATESELFTESTSTATUS;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_SensorOscFreqDev[MAX_STEP] = YAWM_MAINRXYAWACCINFO_SENSOROSCFREQDEV;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Gyro_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_GYRO_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Raster_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_RASTER_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Eep_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_EEP_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Batt_Range_Err[MAX_STEP] = YAWM_MAINRXYAWACCINFO_BATT_RANGE_ERR;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Asic_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_ASIC_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Accel_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_ACCEL_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Ram_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_RAM_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Rom_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_ROM_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Ad_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_AD_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Osc_Fail[MAX_STEP] = YAWM_MAINRXYAWACCINFO_OSC_FAIL;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Watchdog_Rst[MAX_STEP] = YAWM_MAINRXYAWACCINFO_WATCHDOG_RST;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Plaus_Err_Pst[MAX_STEP] = YAWM_MAINRXYAWACCINFO_PLAUS_ERR_PST;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_RollingCounter[MAX_STEP] = YAWM_MAINRXYAWACCINFO_ROLLINGCOUNTER;
const Saluint8 UtInput_YawM_MainRxYawAccInfo_Can_Func_Err[MAX_STEP] = YAWM_MAINRXYAWACCINFO_CAN_FUNC_ERR;
const Saluint8 UtInput_YawM_MainRxLongAccInfo_IntSenFltSymtmActive[MAX_STEP] = YAWM_MAINRXLONGACCINFO_INTSENFLTSYMTMACTIVE;
const Saluint8 UtInput_YawM_MainRxLongAccInfo_IntSenFaultPresent[MAX_STEP] = YAWM_MAINRXLONGACCINFO_INTSENFAULTPRESENT;
const Saluint8 UtInput_YawM_MainRxLongAccInfo_LongAccSenCirErrPre[MAX_STEP] = YAWM_MAINRXLONGACCINFO_LONGACCSENCIRERRPRE;
const Saluint8 UtInput_YawM_MainRxLongAccInfo_LonACSenRanChkErrPre[MAX_STEP] = YAWM_MAINRXLONGACCINFO_LONACSENRANCHKERRPRE;
const Saluint8 UtInput_YawM_MainRxLongAccInfo_LongRollingCounter[MAX_STEP] = YAWM_MAINRXLONGACCINFO_LONGROLLINGCOUNTER;
const Saluint8 UtInput_YawM_MainRxLongAccInfo_IntTempSensorFault[MAX_STEP] = YAWM_MAINRXLONGACCINFO_INTTEMPSENSORFAULT;
const Saluint8 UtInput_YawM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[MAX_STEP] = YAWM_MAINRXMSGOKFLGINFO_YAWSERIALMSGOKFLG;
const Saluint8 UtInput_YawM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[MAX_STEP] = YAWM_MAINRXMSGOKFLGINFO_YAWACCMSGOKFLG;
const Saluint8 UtInput_YawM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[MAX_STEP] = YAWM_MAINRXMSGOKFLGINFO_LONGACCMSGOKFLG;
const Saluint8 UtInput_YawM_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = YAWM_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_YawM_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = YAWM_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_YawM_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = YAWM_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_YawM_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = YAWM_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_YawM_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = YAWM_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_YawM_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = YAWM_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Mom_HndlrEcuModeSts_t UtInput_YawM_MainEcuModeSts[MAX_STEP] = YAWM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_YawM_MainIgnOnOffSts[MAX_STEP] = YAWM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_YawM_MainIgnEdgeSts[MAX_STEP] = YAWM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_YawM_MainVBatt1Mon[MAX_STEP] = YAWM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_YawM_MainDiagClrSrs[MAX_STEP] = YAWM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg[MAX_STEP] = YAWM_MAINYAWMSERIALINFO_YAWM_SERIALNUMOK_FLG;
const Saluint8 UtExpected_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg[MAX_STEP] = YAWM_MAINYAWMSERIALINFO_YAWM_SERIALUNMATCH_FLG;
const Saluint8 UtExpected_YawM_MainYAWMOutInfo_YAWM_Timeout_Err[MAX_STEP] = YAWM_MAINYAWMOUTINFO_YAWM_TIMEOUT_ERR;
const Saluint8 UtExpected_YawM_MainYAWMOutInfo_YAWM_Invalid_Err[MAX_STEP] = YAWM_MAINYAWMOUTINFO_YAWM_INVALID_ERR;
const Saluint8 UtExpected_YawM_MainYAWMOutInfo_YAWM_CRC_Err[MAX_STEP] = YAWM_MAINYAWMOUTINFO_YAWM_CRC_ERR;
const Saluint8 UtExpected_YawM_MainYAWMOutInfo_YAWM_Rolling_Err[MAX_STEP] = YAWM_MAINYAWMOUTINFO_YAWM_ROLLING_ERR;
const Saluint8 UtExpected_YawM_MainYAWMOutInfo_YAWM_Temperature_Err[MAX_STEP] = YAWM_MAINYAWMOUTINFO_YAWM_TEMPERATURE_ERR;
const Saluint8 UtExpected_YawM_MainYAWMOutInfo_YAWM_Range_Err[MAX_STEP] = YAWM_MAINYAWMOUTINFO_YAWM_RANGE_ERR;



TEST_GROUP(YawM_Main);
TEST_SETUP(YawM_Main)
{
    YawM_Main_Init();
}

TEST_TEAR_DOWN(YawM_Main)
{   /* Postcondition */

}

TEST(YawM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        YawM_MainCanRxEscInfo.YawRate = UtInput_YawM_MainCanRxEscInfo_YawRate[i];
        YawM_MainRxYawSerialInfo.YawSerialNum_0 = UtInput_YawM_MainRxYawSerialInfo_YawSerialNum_0[i];
        YawM_MainRxYawSerialInfo.YawSerialNum_1 = UtInput_YawM_MainRxYawSerialInfo_YawSerialNum_1[i];
        YawM_MainRxYawSerialInfo.YawSerialNum_2 = UtInput_YawM_MainRxYawSerialInfo_YawSerialNum_2[i];
        YawM_MainRxYawAccInfo.YawRateValidData = UtInput_YawM_MainRxYawAccInfo_YawRateValidData[i];
        YawM_MainRxYawAccInfo.YawRateSelfTestStatus = UtInput_YawM_MainRxYawAccInfo_YawRateSelfTestStatus[i];
        YawM_MainRxYawAccInfo.SensorOscFreqDev = UtInput_YawM_MainRxYawAccInfo_SensorOscFreqDev[i];
        YawM_MainRxYawAccInfo.Gyro_Fail = UtInput_YawM_MainRxYawAccInfo_Gyro_Fail[i];
        YawM_MainRxYawAccInfo.Raster_Fail = UtInput_YawM_MainRxYawAccInfo_Raster_Fail[i];
        YawM_MainRxYawAccInfo.Eep_Fail = UtInput_YawM_MainRxYawAccInfo_Eep_Fail[i];
        YawM_MainRxYawAccInfo.Batt_Range_Err = UtInput_YawM_MainRxYawAccInfo_Batt_Range_Err[i];
        YawM_MainRxYawAccInfo.Asic_Fail = UtInput_YawM_MainRxYawAccInfo_Asic_Fail[i];
        YawM_MainRxYawAccInfo.Accel_Fail = UtInput_YawM_MainRxYawAccInfo_Accel_Fail[i];
        YawM_MainRxYawAccInfo.Ram_Fail = UtInput_YawM_MainRxYawAccInfo_Ram_Fail[i];
        YawM_MainRxYawAccInfo.Rom_Fail = UtInput_YawM_MainRxYawAccInfo_Rom_Fail[i];
        YawM_MainRxYawAccInfo.Ad_Fail = UtInput_YawM_MainRxYawAccInfo_Ad_Fail[i];
        YawM_MainRxYawAccInfo.Osc_Fail = UtInput_YawM_MainRxYawAccInfo_Osc_Fail[i];
        YawM_MainRxYawAccInfo.Watchdog_Rst = UtInput_YawM_MainRxYawAccInfo_Watchdog_Rst[i];
        YawM_MainRxYawAccInfo.Plaus_Err_Pst = UtInput_YawM_MainRxYawAccInfo_Plaus_Err_Pst[i];
        YawM_MainRxYawAccInfo.RollingCounter = UtInput_YawM_MainRxYawAccInfo_RollingCounter[i];
        YawM_MainRxYawAccInfo.Can_Func_Err = UtInput_YawM_MainRxYawAccInfo_Can_Func_Err[i];
        YawM_MainRxLongAccInfo.IntSenFltSymtmActive = UtInput_YawM_MainRxLongAccInfo_IntSenFltSymtmActive[i];
        YawM_MainRxLongAccInfo.IntSenFaultPresent = UtInput_YawM_MainRxLongAccInfo_IntSenFaultPresent[i];
        YawM_MainRxLongAccInfo.LongAccSenCirErrPre = UtInput_YawM_MainRxLongAccInfo_LongAccSenCirErrPre[i];
        YawM_MainRxLongAccInfo.LonACSenRanChkErrPre = UtInput_YawM_MainRxLongAccInfo_LonACSenRanChkErrPre[i];
        YawM_MainRxLongAccInfo.LongRollingCounter = UtInput_YawM_MainRxLongAccInfo_LongRollingCounter[i];
        YawM_MainRxLongAccInfo.IntTempSensorFault = UtInput_YawM_MainRxLongAccInfo_IntTempSensorFault[i];
        YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = UtInput_YawM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[i];
        YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = UtInput_YawM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[i];
        YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = UtInput_YawM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[i];
        YawM_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_YawM_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_YawM_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        YawM_MainEemFailData.Eem_Fail_WssFL = UtInput_YawM_MainEemFailData_Eem_Fail_WssFL[i];
        YawM_MainEemFailData.Eem_Fail_WssFR = UtInput_YawM_MainEemFailData_Eem_Fail_WssFR[i];
        YawM_MainEemFailData.Eem_Fail_WssRL = UtInput_YawM_MainEemFailData_Eem_Fail_WssRL[i];
        YawM_MainEemFailData.Eem_Fail_WssRR = UtInput_YawM_MainEemFailData_Eem_Fail_WssRR[i];
        YawM_MainEcuModeSts = UtInput_YawM_MainEcuModeSts[i];
        YawM_MainIgnOnOffSts = UtInput_YawM_MainIgnOnOffSts[i];
        YawM_MainIgnEdgeSts = UtInput_YawM_MainIgnEdgeSts[i];
        YawM_MainVBatt1Mon = UtInput_YawM_MainVBatt1Mon[i];
        YawM_MainDiagClrSrs = UtInput_YawM_MainDiagClrSrs[i];

        YawM_Main();

        TEST_ASSERT_EQUAL(YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg, UtExpected_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg, UtExpected_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMOutInfo.YAWM_Timeout_Err, UtExpected_YawM_MainYAWMOutInfo_YAWM_Timeout_Err[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMOutInfo.YAWM_Invalid_Err, UtExpected_YawM_MainYAWMOutInfo_YAWM_Invalid_Err[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMOutInfo.YAWM_CRC_Err, UtExpected_YawM_MainYAWMOutInfo_YAWM_CRC_Err[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMOutInfo.YAWM_Rolling_Err, UtExpected_YawM_MainYAWMOutInfo_YAWM_Rolling_Err[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMOutInfo.YAWM_Temperature_Err, UtExpected_YawM_MainYAWMOutInfo_YAWM_Temperature_Err[i]);
        TEST_ASSERT_EQUAL(YawM_MainYAWMOutInfo.YAWM_Range_Err, UtExpected_YawM_MainYAWMOutInfo_YAWM_Range_Err[i]);
    }
}

TEST_GROUP_RUNNER(YawM_Main)
{
    RUN_TEST_CASE(YawM_Main, All);
}
