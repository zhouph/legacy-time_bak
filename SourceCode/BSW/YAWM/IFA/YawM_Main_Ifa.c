/**
 * @defgroup YawM_Main_Ifa YawM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "YawM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define YAWM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "YawM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define YAWM_MAIN_START_SEC_CODE
#include "YawM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define YAWM_MAIN_STOP_SEC_CODE
#include "YawM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
