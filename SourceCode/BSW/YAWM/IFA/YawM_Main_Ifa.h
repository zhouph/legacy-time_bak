/**
 * @defgroup YawM_Main_Ifa YawM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWM_MAIN_IFA_H_
#define YAWM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define YawM_Main_Read_YawM_MainCanRxEscInfo(data) do \
{ \
    *data = YawM_MainCanRxEscInfo; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawSerialInfo(data) do \
{ \
    *data = YawM_MainRxYawSerialInfo; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo(data) do \
{ \
    *data = YawM_MainRxYawAccInfo; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo(data) do \
{ \
    *data = YawM_MainRxLongAccInfo; \
}while(0);

#define YawM_Main_Read_YawM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = YawM_MainRxMsgOkFlgInfo; \
}while(0);

#define YawM_Main_Read_YawM_MainSenPwrMonitorData(data) do \
{ \
    *data = YawM_MainSenPwrMonitorData; \
}while(0);

#define YawM_Main_Read_YawM_MainEemFailData(data) do \
{ \
    *data = YawM_MainEemFailData; \
}while(0);

#define YawM_Main_Read_YawM_MainCanRxEscInfo_YawRate(data) do \
{ \
    *data = YawM_MainCanRxEscInfo.YawRate; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = YawM_MainRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = YawM_MainRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = YawM_MainRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.YawRateValidData; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Gyro_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Raster_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Eep_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Asic_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Accel_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Ram_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Rom_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Ad_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Osc_Fail; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.RollingCounter; \
}while(0);

#define YawM_Main_Read_YawM_MainRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = YawM_MainRxYawAccInfo.Can_Func_Err; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = YawM_MainRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = YawM_MainRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = YawM_MainRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = YawM_MainRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = YawM_MainRxLongAccInfo.LongRollingCounter; \
}while(0);

#define YawM_Main_Read_YawM_MainRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = YawM_MainRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define YawM_Main_Read_YawM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define YawM_Main_Read_YawM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define YawM_Main_Read_YawM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define YawM_Main_Read_YawM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = YawM_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define YawM_Main_Read_YawM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = YawM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = YawM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = YawM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = YawM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define YawM_Main_Read_YawM_MainEcuModeSts(data) do \
{ \
    *data = YawM_MainEcuModeSts; \
}while(0);

#define YawM_Main_Read_YawM_MainIgnOnOffSts(data) do \
{ \
    *data = YawM_MainIgnOnOffSts; \
}while(0);

#define YawM_Main_Read_YawM_MainIgnEdgeSts(data) do \
{ \
    *data = YawM_MainIgnEdgeSts; \
}while(0);

#define YawM_Main_Read_YawM_MainVBatt1Mon(data) do \
{ \
    *data = YawM_MainVBatt1Mon; \
}while(0);

#define YawM_Main_Read_YawM_MainDiagClrSrs(data) do \
{ \
    *data = YawM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define YawM_Main_Write_YawM_MainYAWMSerialInfo(data) do \
{ \
    YawM_MainYAWMSerialInfo = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo(data) do \
{ \
    YawM_MainYAWMOutInfo = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define YawM_Main_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* YAWM_MAIN_IFA_H_ */
/** @} */
