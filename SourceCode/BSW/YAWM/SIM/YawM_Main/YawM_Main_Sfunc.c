#define S_FUNCTION_NAME      YawM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          41
#define WidthOutputPort         8

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "YawM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    YawM_MainCanRxEscInfo.YawRate = input[0];
    YawM_MainRxYawSerialInfo.YawSerialNum_0 = input[1];
    YawM_MainRxYawSerialInfo.YawSerialNum_1 = input[2];
    YawM_MainRxYawSerialInfo.YawSerialNum_2 = input[3];
    YawM_MainRxYawAccInfo.YawRateValidData = input[4];
    YawM_MainRxYawAccInfo.YawRateSelfTestStatus = input[5];
    YawM_MainRxYawAccInfo.SensorOscFreqDev = input[6];
    YawM_MainRxYawAccInfo.Gyro_Fail = input[7];
    YawM_MainRxYawAccInfo.Raster_Fail = input[8];
    YawM_MainRxYawAccInfo.Eep_Fail = input[9];
    YawM_MainRxYawAccInfo.Batt_Range_Err = input[10];
    YawM_MainRxYawAccInfo.Asic_Fail = input[11];
    YawM_MainRxYawAccInfo.Accel_Fail = input[12];
    YawM_MainRxYawAccInfo.Ram_Fail = input[13];
    YawM_MainRxYawAccInfo.Rom_Fail = input[14];
    YawM_MainRxYawAccInfo.Ad_Fail = input[15];
    YawM_MainRxYawAccInfo.Osc_Fail = input[16];
    YawM_MainRxYawAccInfo.Watchdog_Rst = input[17];
    YawM_MainRxYawAccInfo.Plaus_Err_Pst = input[18];
    YawM_MainRxYawAccInfo.RollingCounter = input[19];
    YawM_MainRxYawAccInfo.Can_Func_Err = input[20];
    YawM_MainRxLongAccInfo.IntSenFltSymtmActive = input[21];
    YawM_MainRxLongAccInfo.IntSenFaultPresent = input[22];
    YawM_MainRxLongAccInfo.LongAccSenCirErrPre = input[23];
    YawM_MainRxLongAccInfo.LonACSenRanChkErrPre = input[24];
    YawM_MainRxLongAccInfo.LongRollingCounter = input[25];
    YawM_MainRxLongAccInfo.IntTempSensorFault = input[26];
    YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = input[27];
    YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = input[28];
    YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = input[29];
    YawM_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[30];
    YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[31];
    YawM_MainEemFailData.Eem_Fail_WssFL = input[32];
    YawM_MainEemFailData.Eem_Fail_WssFR = input[33];
    YawM_MainEemFailData.Eem_Fail_WssRL = input[34];
    YawM_MainEemFailData.Eem_Fail_WssRR = input[35];
    YawM_MainEcuModeSts = input[36];
    YawM_MainIgnOnOffSts = input[37];
    YawM_MainIgnEdgeSts = input[38];
    YawM_MainVBatt1Mon = input[39];
    YawM_MainDiagClrSrs = input[40];

    YawM_Main();


    output[0] = YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg;
    output[1] = YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
    output[2] = YawM_MainYAWMOutInfo.YAWM_Timeout_Err;
    output[3] = YawM_MainYAWMOutInfo.YAWM_Invalid_Err;
    output[4] = YawM_MainYAWMOutInfo.YAWM_CRC_Err;
    output[5] = YawM_MainYAWMOutInfo.YAWM_Rolling_Err;
    output[6] = YawM_MainYAWMOutInfo.YAWM_Temperature_Err;
    output[7] = YawM_MainYAWMOutInfo.YAWM_Range_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    YawM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
