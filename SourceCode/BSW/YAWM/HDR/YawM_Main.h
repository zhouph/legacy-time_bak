/**
 * @defgroup YawM_Main YawM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWM_MAIN_H_
#define YAWM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawM_Types.h"
#include "YawM_Cfg.h"
#include "YawM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define YAWM_MAIN_MODULE_ID      (0)
 #define YAWM_MAIN_MAJOR_VERSION  (2)
 #define YAWM_MAIN_MINOR_VERSION  (0)
 #define YAWM_MAIN_PATCH_VERSION  (0)
 #define YAWM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern YawM_Main_HdrBusType YawM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t YawM_MainVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxEscInfo_t YawM_MainCanRxEscInfo;
extern Proxy_RxByComRxYawSerialInfo_t YawM_MainRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t YawM_MainRxYawAccInfo;
extern Proxy_RxByComRxLongAccInfo_t YawM_MainRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t YawM_MainRxMsgOkFlgInfo;
extern SenPwrM_MainSenPwrMonitor_t YawM_MainSenPwrMonitorData;
extern Eem_MainEemFailData_t YawM_MainEemFailData;
extern Mom_HndlrEcuModeSts_t YawM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t YawM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t YawM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t YawM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t YawM_MainDiagClrSrs;

/* Output Data Element */
extern YawM_MainYAWMSerialInfo_t YawM_MainYAWMSerialInfo;
extern YawM_MainYAWMOutInfo_t YawM_MainYAWMOutInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void YawM_Main_Init(void);
extern void YawM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* YAWM_MAIN_H_ */
/** @} */
