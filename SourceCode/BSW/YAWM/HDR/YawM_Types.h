/**
 * @defgroup YawM_Types YawM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef YAWM_TYPES_H_
#define YAWM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxEscInfo_t YawM_MainCanRxEscInfo;
    Proxy_RxByComRxYawSerialInfo_t YawM_MainRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t YawM_MainRxYawAccInfo;
    Proxy_RxByComRxLongAccInfo_t YawM_MainRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t YawM_MainRxMsgOkFlgInfo;
    SenPwrM_MainSenPwrMonitor_t YawM_MainSenPwrMonitorData;
    Eem_MainEemFailData_t YawM_MainEemFailData;
    Mom_HndlrEcuModeSts_t YawM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t YawM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t YawM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t YawM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t YawM_MainDiagClrSrs;

/* Output Data Element */
    YawM_MainYAWMSerialInfo_t YawM_MainYAWMSerialInfo;
    YawM_MainYAWMOutInfo_t YawM_MainYAWMOutInfo;
}YawM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* YAWM_TYPES_H_ */
/** @} */
