# \file
#
# \brief YawM
#
# This file contains the implementation of the SWC
# module YawM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += YawM_src

YawM_src_FILES        += $(YawM_SRC_PATH)\YawM_Main.c
YawM_src_FILES        += $(YawM_SRC_PATH)\YawM_Processing.c
YawM_src_FILES        += $(YawM_IFA_PATH)\YawM_Main_Ifa.c
YawM_src_FILES        += $(YawM_CFG_PATH)\YawM_Cfg.c
YawM_src_FILES        += $(YawM_CAL_PATH)\YawM_Cal.c

ifeq ($(ICE_COMPILE),true)
YawM_src_FILES        += $(YawM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	YawM_src_FILES        += $(YawM_UNITY_PATH)\unity.c
	YawM_src_FILES        += $(YawM_UNITY_PATH)\unity_fixture.c	
	YawM_src_FILES        += $(YawM_UT_PATH)\main.c
	YawM_src_FILES        += $(YawM_UT_PATH)\YawM_Main\YawM_Main_UtMain.c
endif