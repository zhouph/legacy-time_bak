# \file
#
# \brief YawM
#
# This file contains the implementation of the SWC
# module YawM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

YawM_CORE_PATH     := $(MANDO_BSW_ROOT)\YawM
YawM_CAL_PATH      := $(YawM_CORE_PATH)\CAL\$(YawM_VARIANT)
YawM_SRC_PATH      := $(YawM_CORE_PATH)\SRC
YawM_CFG_PATH      := $(YawM_CORE_PATH)\CFG\$(YawM_VARIANT)
YawM_HDR_PATH      := $(YawM_CORE_PATH)\HDR
YawM_IFA_PATH      := $(YawM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    YawM_CMN_PATH      := $(YawM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	YawM_UT_PATH		:= $(YawM_CORE_PATH)\UT
	YawM_UNITY_PATH	:= $(YawM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(YawM_UT_PATH)
	CC_INCLUDE_PATH		+= $(YawM_UNITY_PATH)
	YawM_Main_PATH 	:= YawM_UT_PATH\YawM_Main
endif
CC_INCLUDE_PATH    += $(YawM_CAL_PATH)
CC_INCLUDE_PATH    += $(YawM_SRC_PATH)
CC_INCLUDE_PATH    += $(YawM_CFG_PATH)
CC_INCLUDE_PATH    += $(YawM_HDR_PATH)
CC_INCLUDE_PATH    += $(YawM_IFA_PATH)
CC_INCLUDE_PATH    += $(YawM_CMN_PATH)

