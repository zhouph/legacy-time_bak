/**
 * @defgroup YawM_Main YawM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawM_Processing.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawM_Main.h"
#include "YawM_Processing.h"

#include "common.h"

#include <stdlib.h>

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "YawM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define YAWM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "YawM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_32BIT
#include "YawM_MemMap.h"

extern signed int Proxy_fys16EstimatedYaw; /* TODO */
extern Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo; /* TODO */

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_32BIT
#include "YawM_MemMap.h"

static boolean gYawM_bInit;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define YAWM_MAIN_START_SEC_CODE
#include "YawM_MemMap.h"

static boolean YawM_CheckInhibit(YawM_Main_HdrBusType *pYawMInfo);
static void YawM_CheckTimeout(YawM_Main_HdrBusType *pYawMInfo);
static void YawM_CheckSum(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit);
static void YawM_RollingCnt(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit);
static void YawM_Invalid(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit);
static void YawM_Temperature(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit);
static void YawM_Range(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void YawM_Processing(YawM_Main_HdrBusType *pYawMInfo)
{
    boolean bInhibit = FALSE;
        
    if(gYawM_bInit == TRUE)
    {
        YawM_CheckTimeout(pYawMInfo);
        
        bInhibit = YawM_CheckInhibit(pYawMInfo);

        YawM_CheckSum(pYawMInfo, bInhibit);
        YawM_RollingCnt(pYawMInfo, bInhibit);
        YawM_Invalid(pYawMInfo, bInhibit);
        YawM_Temperature(pYawMInfo, bInhibit);
        YawM_Range(pYawMInfo, bInhibit);
    }
}

void YawM_Initialize(void)
{
    gYawM_bInit = TRUE;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static boolean YawM_CheckInhibit(YawM_Main_HdrBusType *pYawMInfo)
{
    boolean bInhibit = FALSE;
    
    if((pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Timeout_Err == ERR_PREFAILED) ||
        (pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Timeout_Err == ERR_INHIBIT)
      )
    {
        bInhibit = TRUE;
    }

    return bInhibit;
}

static void YawM_CheckTimeout(YawM_Main_HdrBusType *pYawMInfo)
{
    boolean inhibit = FALSE;

    if((pYawMInfo->YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable == FALSE) ||
       (Proxy_RxCanRxInfo.SenBusOffFlag == TRUE)
      )
    {
        inhibit = TRUE;
    }
    
    if(pYawMInfo->YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg == 1)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Timeout_Err = ERR_PREPASSED;
    }
    else
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Timeout_Err = ERR_PREFAILED;
    }

    if( inhibit == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Timeout_Err = ERR_INHIBIT;
    }
}

static void YawM_CheckSum(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit)
{
    pYawMInfo->YawM_MainYAWMOutInfo.YAWM_CRC_Err = NONE; /* for GM Variant */

    if( inhibit == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_CRC_Err = ERR_INHIBIT;
    }
}

static void YawM_RollingCnt(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit)
{
    pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Rolling_Err = NONE; /* TODO New Concept */

    if( inhibit == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Rolling_Err = ERR_INHIBIT;
    }
}

static void YawM_Invalid(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit)
{
    boolean bYawInvalid = FALSE;

    if((pYawMInfo->YawM_MainRxYawAccInfo.YawRateValidData == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Can_Func_Err == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Gyro_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Eep_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Batt_Range_Err == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Asic_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Accel_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Ram_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Rom_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Ad_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Osc_Fail == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Plaus_Err_Pst == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Can_Func_Err == 1) ||
       (pYawMInfo->YawM_MainRxYawAccInfo.Raster_Fail == 1)
      )
    {
        bYawInvalid = TRUE;
    }

    if(bYawInvalid == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Invalid_Err = ERR_PREFAILED;
    }
    else
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Invalid_Err = ERR_PREPASSED;
    }
    
    if( inhibit == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Invalid_Err = ERR_INHIBIT;
    }
}

static void YawM_Temperature(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit)
{
    sint8 s8RawValue = 0;

    s8RawValue = (sint8)(pYawMInfo->YawM_MainRxYawAccInfo.SensorOscFreqDev);
    
    if((s8RawValue > YAW_RAST_Hz_TEMP_M55_r0_5) || (s8RawValue < YAW_RAST_Hz_TEMP_P100_r0_5))
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Temperature_Err = ERR_PREFAILED;
    }
    else
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Temperature_Err = ERR_PREPASSED;
    }

    if( inhibit == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Temperature_Err = ERR_INHIBIT;
    }
}

static void YawM_Range(YawM_Main_HdrBusType *pYawMInfo, boolean inhibit)
{
    unsigned int abs_u16EstimatedYaw = 0;

    abs_u16EstimatedYaw = abs(Proxy_fys16EstimatedYaw);

    /* TODO : New Concept
                    if((rev_for_judge_time<=0)&&(fu1CLSTEEROK==1)&&(Temp_Abs_STR_Mdl_0_<U16_STEER_300DEG))
    */
    
    if(abs_u16EstimatedYaw >= YAW_RANGE_ERR_DET_THRES)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Range_Err = ERR_PREFAILED;
    }
    else
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Range_Err = ERR_PREPASSED;
    }
    
    if( inhibit == TRUE)
    {
        pYawMInfo->YawM_MainYAWMOutInfo.YAWM_Range_Err = ERR_INHIBIT;
    }
}

#define YAWM_MAIN_STOP_SEC_CODE
#include "YawM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
