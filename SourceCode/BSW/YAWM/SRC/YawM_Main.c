/**
 * @defgroup YawM_Main YawM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        YawM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "YawM_Main.h"
#include "YawM_Main_Ifa.h"
#include "YawM_Processing.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "YawM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define YAWM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "YawM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
YawM_Main_HdrBusType YawM_MainBus;

/* Version Info */
const SwcVersionInfo_t YawM_MainVersionInfo = 
{   
    YAWM_MAIN_MODULE_ID,           /* YawM_MainVersionInfo.ModuleId */
    YAWM_MAIN_MAJOR_VERSION,       /* YawM_MainVersionInfo.MajorVer */
    YAWM_MAIN_MINOR_VERSION,       /* YawM_MainVersionInfo.MinorVer */
    YAWM_MAIN_PATCH_VERSION,       /* YawM_MainVersionInfo.PatchVer */
    YAWM_MAIN_BRANCH_VERSION       /* YawM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxEscInfo_t YawM_MainCanRxEscInfo;
Proxy_RxByComRxYawSerialInfo_t YawM_MainRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t YawM_MainRxYawAccInfo;
Proxy_RxByComRxLongAccInfo_t YawM_MainRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t YawM_MainRxMsgOkFlgInfo;
SenPwrM_MainSenPwrMonitor_t YawM_MainSenPwrMonitorData;
Eem_MainEemFailData_t YawM_MainEemFailData;
Mom_HndlrEcuModeSts_t YawM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t YawM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t YawM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t YawM_MainVBatt1Mon;
Diag_HndlrDiagClr_t YawM_MainDiagClrSrs;

/* Output Data Element */
YawM_MainYAWMSerialInfo_t YawM_MainYAWMSerialInfo;
YawM_MainYAWMOutInfo_t YawM_MainYAWMOutInfo;

uint32 YawM_Main_Timer_Start;
uint32 YawM_Main_Timer_Elapsed;

#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define YAWM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define YAWM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "YawM_MemMap.h"
#define YAWM_MAIN_START_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/** Variable Section (32BIT)**/


#define YAWM_MAIN_STOP_SEC_VAR_32BIT
#include "YawM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define YAWM_MAIN_START_SEC_CODE
#include "YawM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void YawM_Main_Init(void)
{
    /* Initialize internal bus */
    YawM_MainBus.YawM_MainCanRxEscInfo.YawRate = 0;
    YawM_MainBus.YawM_MainRxYawSerialInfo.YawSerialNum_0 = 0;
    YawM_MainBus.YawM_MainRxYawSerialInfo.YawSerialNum_1 = 0;
    YawM_MainBus.YawM_MainRxYawSerialInfo.YawSerialNum_2 = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.YawRateValidData = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.YawRateSelfTestStatus = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.SensorOscFreqDev = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Gyro_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Raster_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Eep_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Batt_Range_Err = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Asic_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Accel_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Ram_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Rom_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Ad_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Osc_Fail = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Watchdog_Rst = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Plaus_Err_Pst = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.RollingCounter = 0;
    YawM_MainBus.YawM_MainRxYawAccInfo.Can_Func_Err = 0;
    YawM_MainBus.YawM_MainRxLongAccInfo.IntSenFltSymtmActive = 0;
    YawM_MainBus.YawM_MainRxLongAccInfo.IntSenFaultPresent = 0;
    YawM_MainBus.YawM_MainRxLongAccInfo.LongAccSenCirErrPre = 0;
    YawM_MainBus.YawM_MainRxLongAccInfo.LonACSenRanChkErrPre = 0;
    YawM_MainBus.YawM_MainRxLongAccInfo.LongRollingCounter = 0;
    YawM_MainBus.YawM_MainRxLongAccInfo.IntTempSensorFault = 0;
    YawM_MainBus.YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = 0;
    YawM_MainBus.YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = 0;
    YawM_MainBus.YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = 0;
    YawM_MainBus.YawM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    YawM_MainBus.YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssFL = 0;
    YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssFR = 0;
    YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssRL = 0;
    YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssRR = 0;
    YawM_MainBus.YawM_MainEcuModeSts = 0;
    YawM_MainBus.YawM_MainIgnOnOffSts = 0;
    YawM_MainBus.YawM_MainIgnEdgeSts = 0;
    YawM_MainBus.YawM_MainVBatt1Mon = 0;
    YawM_MainBus.YawM_MainDiagClrSrs = 0;
    YawM_MainBus.YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = 0;
    YawM_MainBus.YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = 0;
    YawM_MainBus.YawM_MainYAWMOutInfo.YAWM_Timeout_Err = 0;
    YawM_MainBus.YawM_MainYAWMOutInfo.YAWM_Invalid_Err = 0;
    YawM_MainBus.YawM_MainYAWMOutInfo.YAWM_CRC_Err = 0;
    YawM_MainBus.YawM_MainYAWMOutInfo.YAWM_Rolling_Err = 0;
    YawM_MainBus.YawM_MainYAWMOutInfo.YAWM_Temperature_Err = 0;
    YawM_MainBus.YawM_MainYAWMOutInfo.YAWM_Range_Err = 0;
}

void YawM_Main(void)
{
    YawM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    YawM_Main_Read_YawM_MainCanRxEscInfo_YawRate(&YawM_MainBus.YawM_MainCanRxEscInfo.YawRate);

    YawM_Main_Read_YawM_MainRxYawSerialInfo(&YawM_MainBus.YawM_MainRxYawSerialInfo);
    /*==============================================================================
    * Members of structure YawM_MainRxYawSerialInfo 
     : YawM_MainRxYawSerialInfo.YawSerialNum_0;
     : YawM_MainRxYawSerialInfo.YawSerialNum_1;
     : YawM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/
    
    /* Decomposed structure interface */
    YawM_Main_Read_YawM_MainRxYawAccInfo_YawRateValidData(&YawM_MainBus.YawM_MainRxYawAccInfo.YawRateValidData);
    YawM_Main_Read_YawM_MainRxYawAccInfo_YawRateSelfTestStatus(&YawM_MainBus.YawM_MainRxYawAccInfo.YawRateSelfTestStatus);
    YawM_Main_Read_YawM_MainRxYawAccInfo_SensorOscFreqDev(&YawM_MainBus.YawM_MainRxYawAccInfo.SensorOscFreqDev);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Gyro_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Gyro_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Raster_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Raster_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Eep_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Eep_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Batt_Range_Err(&YawM_MainBus.YawM_MainRxYawAccInfo.Batt_Range_Err);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Asic_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Asic_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Accel_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Accel_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Ram_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Ram_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Rom_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Rom_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Ad_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Ad_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Osc_Fail(&YawM_MainBus.YawM_MainRxYawAccInfo.Osc_Fail);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Watchdog_Rst(&YawM_MainBus.YawM_MainRxYawAccInfo.Watchdog_Rst);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Plaus_Err_Pst(&YawM_MainBus.YawM_MainRxYawAccInfo.Plaus_Err_Pst);
    YawM_Main_Read_YawM_MainRxYawAccInfo_RollingCounter(&YawM_MainBus.YawM_MainRxYawAccInfo.RollingCounter);
    YawM_Main_Read_YawM_MainRxYawAccInfo_Can_Func_Err(&YawM_MainBus.YawM_MainRxYawAccInfo.Can_Func_Err);

    /* Decomposed structure interface */
    YawM_Main_Read_YawM_MainRxLongAccInfo_IntSenFltSymtmActive(&YawM_MainBus.YawM_MainRxLongAccInfo.IntSenFltSymtmActive);
    YawM_Main_Read_YawM_MainRxLongAccInfo_IntSenFaultPresent(&YawM_MainBus.YawM_MainRxLongAccInfo.IntSenFaultPresent);
    YawM_Main_Read_YawM_MainRxLongAccInfo_LongAccSenCirErrPre(&YawM_MainBus.YawM_MainRxLongAccInfo.LongAccSenCirErrPre);
    YawM_Main_Read_YawM_MainRxLongAccInfo_LonACSenRanChkErrPre(&YawM_MainBus.YawM_MainRxLongAccInfo.LonACSenRanChkErrPre);
    YawM_Main_Read_YawM_MainRxLongAccInfo_LongRollingCounter(&YawM_MainBus.YawM_MainRxLongAccInfo.LongRollingCounter);
    YawM_Main_Read_YawM_MainRxLongAccInfo_IntTempSensorFault(&YawM_MainBus.YawM_MainRxLongAccInfo.IntTempSensorFault);

    /* Decomposed structure interface */
    YawM_Main_Read_YawM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(&YawM_MainBus.YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg);
    YawM_Main_Read_YawM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(&YawM_MainBus.YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg);
    YawM_Main_Read_YawM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(&YawM_MainBus.YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg);

    /* Decomposed structure interface */
    YawM_Main_Read_YawM_MainSenPwrMonitorData_SenPwrM_5V_Stable(&YawM_MainBus.YawM_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    YawM_Main_Read_YawM_MainSenPwrMonitorData_SenPwrM_12V_Stable(&YawM_MainBus.YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssFL(&YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssFL);
    YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssFR(&YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssFR);
    YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssRL(&YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssRL);
    YawM_Main_Read_YawM_MainEemFailData_Eem_Fail_WssRR(&YawM_MainBus.YawM_MainEemFailData.Eem_Fail_WssRR);

    YawM_Main_Read_YawM_MainEcuModeSts(&YawM_MainBus.YawM_MainEcuModeSts);
    YawM_Main_Read_YawM_MainIgnOnOffSts(&YawM_MainBus.YawM_MainIgnOnOffSts);
    YawM_Main_Read_YawM_MainIgnEdgeSts(&YawM_MainBus.YawM_MainIgnEdgeSts);
    YawM_Main_Read_YawM_MainVBatt1Mon(&YawM_MainBus.YawM_MainVBatt1Mon);
    YawM_Main_Read_YawM_MainDiagClrSrs(&YawM_MainBus.YawM_MainDiagClrSrs);

    /* Process */
    YawM_Processing(&YawM_MainBus);

    /* Output */
    YawM_Main_Write_YawM_MainYAWMSerialInfo(&YawM_MainBus.YawM_MainYAWMSerialInfo);
    /*==============================================================================
    * Members of structure YawM_MainYAWMSerialInfo 
     : YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg;
     : YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
     =============================================================================*/
    
    YawM_Main_Write_YawM_MainYAWMOutInfo(&YawM_MainBus.YawM_MainYAWMOutInfo);
    /*==============================================================================
    * Members of structure YawM_MainYAWMOutInfo 
     : YawM_MainYAWMOutInfo.YAWM_Timeout_Err;
     : YawM_MainYAWMOutInfo.YAWM_Invalid_Err;
     : YawM_MainYAWMOutInfo.YAWM_CRC_Err;
     : YawM_MainYAWMOutInfo.YAWM_Rolling_Err;
     : YawM_MainYAWMOutInfo.YAWM_Temperature_Err;
     : YawM_MainYAWMOutInfo.YAWM_Range_Err;
     =============================================================================*/
    

    YawM_Main_Timer_Elapsed = STM0_TIM0.U - YawM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define YAWM_MAIN_STOP_SEC_CODE
#include "YawM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
