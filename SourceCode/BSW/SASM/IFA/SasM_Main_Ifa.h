/**
 * @defgroup SasM_Main_Ifa SasM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SASM_MAIN_IFA_H_
#define SASM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SasM_Main_Read_SasM_MainCanRxEscInfo(data) do \
{ \
    *data = SasM_MainCanRxEscInfo; \
}while(0);

#define SasM_Main_Read_SasM_MainCanRxInfo(data) do \
{ \
    *data = SasM_MainCanRxInfo; \
}while(0);

#define SasM_Main_Read_SasM_MainRxSasInfo(data) do \
{ \
    *data = SasM_MainRxSasInfo; \
}while(0);

#define SasM_Main_Read_SasM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = SasM_MainRxMsgOkFlgInfo; \
}while(0);

#define SasM_Main_Read_SasM_MainSenPwrMonitorData(data) do \
{ \
    *data = SasM_MainSenPwrMonitorData; \
}while(0);

#define SasM_Main_Read_SasM_MainEemFailData(data) do \
{ \
    *data = SasM_MainEemFailData; \
}while(0);

#define SasM_Main_Read_SasM_MainCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = SasM_MainCanRxEscInfo.SteeringAngle; \
}while(0);

#define SasM_Main_Read_SasM_MainCanRxInfo_MainBusOffFlag(data) do \
{ \
    *data = SasM_MainCanRxInfo.MainBusOffFlag; \
}while(0);

#define SasM_Main_Read_SasM_MainRxSasInfo_Angle(data) do \
{ \
    *data = SasM_MainRxSasInfo.Angle; \
}while(0);

#define SasM_Main_Read_SasM_MainRxSasInfo_Speed(data) do \
{ \
    *data = SasM_MainRxSasInfo.Speed; \
}while(0);

#define SasM_Main_Read_SasM_MainRxSasInfo_Ok(data) do \
{ \
    *data = SasM_MainRxSasInfo.Ok; \
}while(0);

#define SasM_Main_Read_SasM_MainRxSasInfo_Trim(data) do \
{ \
    *data = SasM_MainRxSasInfo.Trim; \
}while(0);

#define SasM_Main_Read_SasM_MainRxSasInfo_CheckSum(data) do \
{ \
    *data = SasM_MainRxSasInfo.CheckSum; \
}while(0);

#define SasM_Main_Read_SasM_MainRxMsgOkFlgInfo_SasMsgOkFlg(data) do \
{ \
    *data = SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg; \
}while(0);

#define SasM_Main_Read_SasM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = SasM_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define SasM_Main_Read_SasM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = SasM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = SasM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = SasM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = SasM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define SasM_Main_Read_SasM_MainEcuModeSts(data) do \
{ \
    *data = SasM_MainEcuModeSts; \
}while(0);

#define SasM_Main_Read_SasM_MainIgnOnOffSts(data) do \
{ \
    *data = SasM_MainIgnOnOffSts; \
}while(0);

#define SasM_Main_Read_SasM_MainIgnEdgeSts(data) do \
{ \
    *data = SasM_MainIgnEdgeSts; \
}while(0);

#define SasM_Main_Read_SasM_MainVBatt1Mon(data) do \
{ \
    *data = SasM_MainVBatt1Mon; \
}while(0);

#define SasM_Main_Read_SasM_MainDiagClrSrs(data) do \
{ \
    *data = SasM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define SasM_Main_Write_SasM_MainSASMOutInfo(data) do \
{ \
    SasM_MainSASMOutInfo = *data; \
}while(0);

#define SasM_Main_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define SasM_Main_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define SasM_Main_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define SasM_Main_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define SasM_Main_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SASM_MAIN_IFA_H_ */
/** @} */
