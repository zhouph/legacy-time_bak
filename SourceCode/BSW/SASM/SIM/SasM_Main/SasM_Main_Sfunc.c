#define S_FUNCTION_NAME      SasM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          19
#define WidthOutputPort         5

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "SasM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    SasM_MainCanRxEscInfo.SteeringAngle = input[0];
    SasM_MainCanRxInfo.MainBusOffFlag = input[1];
    SasM_MainRxSasInfo.Angle = input[2];
    SasM_MainRxSasInfo.Speed = input[3];
    SasM_MainRxSasInfo.Ok = input[4];
    SasM_MainRxSasInfo.Trim = input[5];
    SasM_MainRxSasInfo.CheckSum = input[6];
    SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg = input[7];
    SasM_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[8];
    SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[9];
    SasM_MainEemFailData.Eem_Fail_WssFL = input[10];
    SasM_MainEemFailData.Eem_Fail_WssFR = input[11];
    SasM_MainEemFailData.Eem_Fail_WssRL = input[12];
    SasM_MainEemFailData.Eem_Fail_WssRR = input[13];
    SasM_MainEcuModeSts = input[14];
    SasM_MainIgnOnOffSts = input[15];
    SasM_MainIgnEdgeSts = input[16];
    SasM_MainVBatt1Mon = input[17];
    SasM_MainDiagClrSrs = input[18];

    SasM_Main();


    output[0] = SasM_MainSASMOutInfo.SASM_Timeout_Err;
    output[1] = SasM_MainSASMOutInfo.SASM_Invalid_Err;
    output[2] = SasM_MainSASMOutInfo.SASM_CRC_Err;
    output[3] = SasM_MainSASMOutInfo.SASM_Rolling_Err;
    output[4] = SasM_MainSASMOutInfo.SASM_Range_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    SasM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
