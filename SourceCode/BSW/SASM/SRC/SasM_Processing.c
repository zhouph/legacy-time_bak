/**
 * @defgroup SasM_Main SasM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasM_Processing.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasM_Main.h"
#include "SasM_Processing.h"

#include "common.h"

#include <stdlib.h>

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SASM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SasM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SASM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SasM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/** Variable Section (32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_32BIT
#include "SasM_MemMap.h"

extern Proxy_RxByComRxSasInfo_t Proxy_RxByComRxSasInfo; /* TODO */
extern Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo; /* TODO */
extern Proxy_RxCanRxInfo_t    Proxy_RxCanRxInfo; /* TODO */
extern SenPwrM_MainSenPwrMonitor_t SenPwrM_MainSenPwrMonitorData;

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/** Variable Section (32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_32BIT
#include "SasM_MemMap.h"

static boolean gSasM_bInit;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SASM_MAIN_START_SEC_CODE
#include "SasM_MemMap.h"

static boolean SasM_CheckInhibit(SasM_Main_HdrBusType *pSasMInfo);
static void SasM_CheckTimeout(SasM_Main_HdrBusType *pSasMInfo);
static void SasM_CheckSum(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit);
static void SasM_RollingCnt(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit);
static void SasM_Invalid(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit);
static void SasM_Range(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit);
static void SasM_Calibration(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit);
static void SasM_Temperature(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SasM_Processing(SasM_Main_HdrBusType *pSasMInfo)
{
    boolean bInhibit = FALSE;
    
    if(gSasM_bInit == TRUE)
    {
        SasM_CheckTimeout(pSasMInfo);

        bInhibit = SasM_CheckInhibit(pSasMInfo);

        SasM_CheckSum(pSasMInfo, bInhibit);
        SasM_RollingCnt(pSasMInfo, bInhibit);
        SasM_Invalid(pSasMInfo, bInhibit);
        SasM_Temperature(pSasMInfo, bInhibit);
        SasM_Range(pSasMInfo, bInhibit);   
        SasM_Calibration(pSasMInfo, bInhibit);
    }


    if(pSasMInfo->SasM_MainDiagClrSrs==1)
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Timeout_Err = ERR_NONE;
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Invalid_Err = ERR_NONE;
        pSasMInfo->SasM_MainSASMOutInfo.SASM_CRC_Err = ERR_NONE;
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Rolling_Err = ERR_NONE;
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Range_Err = ERR_NONE;
    }
}

void SasM_Initialize(void)
{
    gSasM_bInit = FALSE;
    if(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable==1)
    {
        gSasM_bInit = TRUE;
    }
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static boolean SasM_CheckInhibit(SasM_Main_HdrBusType *pSasMInfo)
{
    boolean bInhibit = FALSE;
    
    if((pSasMInfo->SasM_MainSASMOutInfo.SASM_Timeout_Err == ERR_PREFAILED) ||
        (pSasMInfo->SasM_MainSASMOutInfo.SASM_Timeout_Err == ERR_INHIBIT)
      )
    {
        bInhibit = TRUE;
    }

    return bInhibit;
}

static void SasM_CheckTimeout(SasM_Main_HdrBusType *pSasMInfo)
{
    boolean inhibit = FALSE;

    if((pSasMInfo->SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable == FALSE) ||
       (Proxy_RxCanRxInfo.MainBusOffFlag == TRUE)
      )
    {
        inhibit = TRUE;
    }

    /* TODO : Ign Suspect new concept */
    if(Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg == 1)
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Timeout_Err = ERR_PREPASSED;
    }
    else
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Timeout_Err = ERR_PREFAILED;
    }

    pSasMInfo->SasM_MainSASMOutInfo.SASM_Timeout_Err = ERR_INHIBIT;
}

static void SasM_CheckSum(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit)
{
    uint8 u8MsgCheckSum = 0;
    uint8 u8CalcedCheckSum = 0;

    u8MsgCheckSum = Proxy_RxByComRxSasInfo.CheckSum; /* TODO New Concept */

    pSasMInfo->SasM_MainSASMOutInfo.SASM_CRC_Err = NONE; /* TODO New Concept */

    pSasMInfo->SasM_MainSASMOutInfo.SASM_CRC_Err = ERR_INHIBIT;    
}

static void SasM_RollingCnt(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit)
{
    pSasMInfo->SasM_MainSASMOutInfo.SASM_Rolling_Err = NONE; /* TODO New Concept */

    pSasMInfo->SasM_MainSASMOutInfo.SASM_Rolling_Err = ERR_INHIBIT;
}

static void SasM_Invalid(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit)
{
    if((Proxy_RxByComRxSasInfo.Ok == 0) || (Proxy_RxByComRxSasInfo.Trim == 0) ||
       (Proxy_RxByComRxSasInfo.Speed == 0xFF) || (Proxy_RxByComRxSasInfo.Angle == 0x7FFF)
      )
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Invalid_Err = ERR_PREFAILED;
    }
    else
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Invalid_Err = ERR_PREPASSED;
    }

    pSasMInfo->SasM_MainSASMOutInfo.SASM_Invalid_Err = ERR_INHIBIT;
}

static void SasM_Range(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit)
{
    sint16  s16RawValue = 0;
    uint16 abs_u16EstimatedSas = 0;

    s16RawValue = (sint16)(Proxy_RxByComRxSasInfo.Angle);
    abs_u16EstimatedSas = abs(s16RawValue);
  
    if(abs_u16EstimatedSas >= STR_RANGE_DET_ANGLE)
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Range_Err = ERR_PREFAILED;
    }
    else
    {
        pSasMInfo->SasM_MainSASMOutInfo.SASM_Range_Err = ERR_PREPASSED;
    }

    pSasMInfo->SasM_MainSASMOutInfo.SASM_Range_Err = ERR_INHIBIT;
}

static void SasM_Calibration(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit)
{
    /* TODO : New Concept , New Excel Work need
    pSasMInfo->SasM_MainSASMOutInfo.SASM_Calibration_Err = ERR_PREFAILED;

    pSasMInfo->SasM_MainSASMOutInfo.SASM_Calibration_Ihb = FALSE;
    */
}

static void SasM_Temperature(SasM_Main_HdrBusType *pSasMInfo, boolean inhibit)
{
    /* TODO : New Concept , New Excel Work need
    pSasMInfo->SasM_MainSASMOutInfo.SASM_Temperature_Err = ERR_PREFAILED;

    pSasMInfo->SasM_MainSASMOutInfo.SASM_Temperature_Ihb = FALSE;
    */
}

#define SASM_MAIN_STOP_SEC_CODE
#include "SasM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
