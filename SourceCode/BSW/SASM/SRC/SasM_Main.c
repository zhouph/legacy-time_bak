/**
 * @defgroup SasM_Main SasM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasM_Main.h"
#include "SasM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SASM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SasM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SASM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SasM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SasM_Main_HdrBusType SasM_MainBus;

/* Version Info */
const SwcVersionInfo_t SasM_MainVersionInfo = 
{   
    SASM_MAIN_MODULE_ID,           /* SasM_MainVersionInfo.ModuleId */
    SASM_MAIN_MAJOR_VERSION,       /* SasM_MainVersionInfo.MajorVer */
    SASM_MAIN_MINOR_VERSION,       /* SasM_MainVersionInfo.MinorVer */
    SASM_MAIN_PATCH_VERSION,       /* SasM_MainVersionInfo.PatchVer */
    SASM_MAIN_BRANCH_VERSION       /* SasM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxEscInfo_t SasM_MainCanRxEscInfo;
Proxy_RxCanRxInfo_t SasM_MainCanRxInfo;
Proxy_RxByComRxSasInfo_t SasM_MainRxSasInfo;
Proxy_RxByComRxMsgOkFlgInfo_t SasM_MainRxMsgOkFlgInfo;
SenPwrM_MainSenPwrMonitor_t SasM_MainSenPwrMonitorData;
Eem_MainEemFailData_t SasM_MainEemFailData;
Mom_HndlrEcuModeSts_t SasM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t SasM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t SasM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t SasM_MainVBatt1Mon;
Diag_HndlrDiagClr_t SasM_MainDiagClrSrs;

/* Output Data Element */
SasM_MainSASMOutInfo_t SasM_MainSASMOutInfo;

uint32 SasM_Main_Timer_Start;
uint32 SasM_Main_Timer_Elapsed;

#define SASM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/** Variable Section (32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_MAIN_START_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/** Variable Section (32BIT)**/


#define SASM_MAIN_STOP_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SASM_MAIN_START_SEC_CODE
#include "SasM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SasM_Main_Init(void)
{
    /* Initialize internal bus */
    SasM_MainBus.SasM_MainCanRxEscInfo.SteeringAngle = 0;
    SasM_MainBus.SasM_MainCanRxInfo.MainBusOffFlag = 0;
    SasM_MainBus.SasM_MainRxSasInfo.Angle = 0;
    SasM_MainBus.SasM_MainRxSasInfo.Speed = 0;
    SasM_MainBus.SasM_MainRxSasInfo.Ok = 0;
    SasM_MainBus.SasM_MainRxSasInfo.Trim = 0;
    SasM_MainBus.SasM_MainRxSasInfo.CheckSum = 0;
    SasM_MainBus.SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg = 0;
    SasM_MainBus.SasM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    SasM_MainBus.SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssFL = 0;
    SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssFR = 0;
    SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssRL = 0;
    SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssRR = 0;
    SasM_MainBus.SasM_MainEcuModeSts = 0;
    SasM_MainBus.SasM_MainIgnOnOffSts = 0;
    SasM_MainBus.SasM_MainIgnEdgeSts = 0;
    SasM_MainBus.SasM_MainVBatt1Mon = 0;
    SasM_MainBus.SasM_MainDiagClrSrs = 0;
    SasM_MainBus.SasM_MainSASMOutInfo.SASM_Timeout_Err = 0;
    SasM_MainBus.SasM_MainSASMOutInfo.SASM_Invalid_Err = 0;
    SasM_MainBus.SasM_MainSASMOutInfo.SASM_CRC_Err = 0;
    SasM_MainBus.SasM_MainSASMOutInfo.SASM_Rolling_Err = 0;
    SasM_MainBus.SasM_MainSASMOutInfo.SASM_Range_Err = 0;
}

void SasM_Main(void)
{
    SasM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    SasM_Main_Read_SasM_MainCanRxEscInfo_SteeringAngle(&SasM_MainBus.SasM_MainCanRxEscInfo.SteeringAngle);

    /* Decomposed structure interface */
    SasM_Main_Read_SasM_MainCanRxInfo_MainBusOffFlag(&SasM_MainBus.SasM_MainCanRxInfo.MainBusOffFlag);

    /* Decomposed structure interface */
    SasM_Main_Read_SasM_MainRxSasInfo_Angle(&SasM_MainBus.SasM_MainRxSasInfo.Angle);
    SasM_Main_Read_SasM_MainRxSasInfo_Speed(&SasM_MainBus.SasM_MainRxSasInfo.Speed);
    SasM_Main_Read_SasM_MainRxSasInfo_Ok(&SasM_MainBus.SasM_MainRxSasInfo.Ok);
    SasM_Main_Read_SasM_MainRxSasInfo_Trim(&SasM_MainBus.SasM_MainRxSasInfo.Trim);
    SasM_Main_Read_SasM_MainRxSasInfo_CheckSum(&SasM_MainBus.SasM_MainRxSasInfo.CheckSum);

    /* Decomposed structure interface */
    SasM_Main_Read_SasM_MainRxMsgOkFlgInfo_SasMsgOkFlg(&SasM_MainBus.SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg);

    /* Decomposed structure interface */
    SasM_Main_Read_SasM_MainSenPwrMonitorData_SenPwrM_5V_Stable(&SasM_MainBus.SasM_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    SasM_Main_Read_SasM_MainSenPwrMonitorData_SenPwrM_12V_Stable(&SasM_MainBus.SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssFL(&SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssFL);
    SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssFR(&SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssFR);
    SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssRL(&SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssRL);
    SasM_Main_Read_SasM_MainEemFailData_Eem_Fail_WssRR(&SasM_MainBus.SasM_MainEemFailData.Eem_Fail_WssRR);

    SasM_Main_Read_SasM_MainEcuModeSts(&SasM_MainBus.SasM_MainEcuModeSts);
    SasM_Main_Read_SasM_MainIgnOnOffSts(&SasM_MainBus.SasM_MainIgnOnOffSts);
    SasM_Main_Read_SasM_MainIgnEdgeSts(&SasM_MainBus.SasM_MainIgnEdgeSts);
    SasM_Main_Read_SasM_MainVBatt1Mon(&SasM_MainBus.SasM_MainVBatt1Mon);
    SasM_Main_Read_SasM_MainDiagClrSrs(&SasM_MainBus.SasM_MainDiagClrSrs);

    /* Process */
    SasM_Initialize();
    SasM_Processing(&SasM_MainBus);

    /* Output */
    SasM_Main_Write_SasM_MainSASMOutInfo(&SasM_MainBus.SasM_MainSASMOutInfo);
    /*==============================================================================
    * Members of structure SasM_MainSASMOutInfo 
     : SasM_MainSASMOutInfo.SASM_Timeout_Err;
     : SasM_MainSASMOutInfo.SASM_Invalid_Err;
     : SasM_MainSASMOutInfo.SASM_CRC_Err;
     : SasM_MainSASMOutInfo.SASM_Rolling_Err;
     : SasM_MainSASMOutInfo.SASM_Range_Err;
     =============================================================================*/
    

    SasM_Main_Timer_Elapsed = STM0_TIM0.U - SasM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SASM_MAIN_STOP_SEC_CODE
#include "SasM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
