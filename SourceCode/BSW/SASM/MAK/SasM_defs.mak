# \file
#
# \brief SasM
#
# This file contains the implementation of the SWC
# module SasM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SasM_CORE_PATH     := $(MANDO_BSW_ROOT)\SasM
SasM_CAL_PATH      := $(SasM_CORE_PATH)\CAL\$(SasM_VARIANT)
SasM_SRC_PATH      := $(SasM_CORE_PATH)\SRC
SasM_CFG_PATH      := $(SasM_CORE_PATH)\CFG\$(SasM_VARIANT)
SasM_HDR_PATH      := $(SasM_CORE_PATH)\HDR
SasM_IFA_PATH      := $(SasM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SasM_CMN_PATH      := $(SasM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SasM_UT_PATH		:= $(SasM_CORE_PATH)\UT
	SasM_UNITY_PATH	:= $(SasM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SasM_UT_PATH)
	CC_INCLUDE_PATH		+= $(SasM_UNITY_PATH)
	SasM_Main_PATH 	:= SasM_UT_PATH\SasM_Main
endif
CC_INCLUDE_PATH    += $(SasM_CAL_PATH)
CC_INCLUDE_PATH    += $(SasM_SRC_PATH)
CC_INCLUDE_PATH    += $(SasM_CFG_PATH)
CC_INCLUDE_PATH    += $(SasM_HDR_PATH)
CC_INCLUDE_PATH    += $(SasM_IFA_PATH)
CC_INCLUDE_PATH    += $(SasM_CMN_PATH)

