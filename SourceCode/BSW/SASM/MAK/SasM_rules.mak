# \file
#
# \brief SasM
#
# This file contains the implementation of the SWC
# module SasM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SasM_src

SasM_src_FILES        += $(SasM_SRC_PATH)\SasM_Main.c
SasM_src_FILES        += $(SasM_SRC_PATH)\SasM_Processing.c
SasM_src_FILES        += $(SasM_IFA_PATH)\SasM_Main_Ifa.c
SasM_src_FILES        += $(SasM_CFG_PATH)\SasM_Cfg.c
SasM_src_FILES        += $(SasM_CAL_PATH)\SasM_Cal.c

ifeq ($(ICE_COMPILE),true)
SasM_src_FILES        += $(SasM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SasM_src_FILES        += $(SasM_UNITY_PATH)\unity.c
	SasM_src_FILES        += $(SasM_UNITY_PATH)\unity_fixture.c	
	SasM_src_FILES        += $(SasM_UT_PATH)\main.c
	SasM_src_FILES        += $(SasM_UT_PATH)\SasM_Main\SasM_Main_UtMain.c
endif