#include "unity.h"
#include "unity_fixture.h"
#include "SasM_Main.h"
#include "SasM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_SasM_MainCanRxEscInfo_SteeringAngle[MAX_STEP] = SASM_MAINCANRXESCINFO_STEERINGANGLE;
const Saluint8 UtInput_SasM_MainCanRxInfo_MainBusOffFlag[MAX_STEP] = SASM_MAINCANRXINFO_MAINBUSOFFFLAG;
const Saluint16 UtInput_SasM_MainRxSasInfo_Angle[MAX_STEP] = SASM_MAINRXSASINFO_ANGLE;
const Saluint8 UtInput_SasM_MainRxSasInfo_Speed[MAX_STEP] = SASM_MAINRXSASINFO_SPEED;
const Saluint8 UtInput_SasM_MainRxSasInfo_Ok[MAX_STEP] = SASM_MAINRXSASINFO_OK;
const Saluint8 UtInput_SasM_MainRxSasInfo_Trim[MAX_STEP] = SASM_MAINRXSASINFO_TRIM;
const Saluint8 UtInput_SasM_MainRxSasInfo_CheckSum[MAX_STEP] = SASM_MAINRXSASINFO_CHECKSUM;
const Saluint8 UtInput_SasM_MainRxMsgOkFlgInfo_SasMsgOkFlg[MAX_STEP] = SASM_MAINRXMSGOKFLGINFO_SASMSGOKFLG;
const Saluint8 UtInput_SasM_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = SASM_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_SasM_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = SASM_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_SasM_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = SASM_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_SasM_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = SASM_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_SasM_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = SASM_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_SasM_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = SASM_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Mom_HndlrEcuModeSts_t UtInput_SasM_MainEcuModeSts[MAX_STEP] = SASM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_SasM_MainIgnOnOffSts[MAX_STEP] = SASM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_SasM_MainIgnEdgeSts[MAX_STEP] = SASM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_SasM_MainVBatt1Mon[MAX_STEP] = SASM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_SasM_MainDiagClrSrs[MAX_STEP] = SASM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_SasM_MainSASMOutInfo_SASM_Timeout_Err[MAX_STEP] = SASM_MAINSASMOUTINFO_SASM_TIMEOUT_ERR;
const Saluint8 UtExpected_SasM_MainSASMOutInfo_SASM_Invalid_Err[MAX_STEP] = SASM_MAINSASMOUTINFO_SASM_INVALID_ERR;
const Saluint8 UtExpected_SasM_MainSASMOutInfo_SASM_CRC_Err[MAX_STEP] = SASM_MAINSASMOUTINFO_SASM_CRC_ERR;
const Saluint8 UtExpected_SasM_MainSASMOutInfo_SASM_Rolling_Err[MAX_STEP] = SASM_MAINSASMOUTINFO_SASM_ROLLING_ERR;
const Saluint8 UtExpected_SasM_MainSASMOutInfo_SASM_Range_Err[MAX_STEP] = SASM_MAINSASMOUTINFO_SASM_RANGE_ERR;



TEST_GROUP(SasM_Main);
TEST_SETUP(SasM_Main)
{
    SasM_Main_Init();
}

TEST_TEAR_DOWN(SasM_Main)
{   /* Postcondition */

}

TEST(SasM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SasM_MainCanRxEscInfo.SteeringAngle = UtInput_SasM_MainCanRxEscInfo_SteeringAngle[i];
        SasM_MainCanRxInfo.MainBusOffFlag = UtInput_SasM_MainCanRxInfo_MainBusOffFlag[i];
        SasM_MainRxSasInfo.Angle = UtInput_SasM_MainRxSasInfo_Angle[i];
        SasM_MainRxSasInfo.Speed = UtInput_SasM_MainRxSasInfo_Speed[i];
        SasM_MainRxSasInfo.Ok = UtInput_SasM_MainRxSasInfo_Ok[i];
        SasM_MainRxSasInfo.Trim = UtInput_SasM_MainRxSasInfo_Trim[i];
        SasM_MainRxSasInfo.CheckSum = UtInput_SasM_MainRxSasInfo_CheckSum[i];
        SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg = UtInput_SasM_MainRxMsgOkFlgInfo_SasMsgOkFlg[i];
        SasM_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_SasM_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_SasM_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        SasM_MainEemFailData.Eem_Fail_WssFL = UtInput_SasM_MainEemFailData_Eem_Fail_WssFL[i];
        SasM_MainEemFailData.Eem_Fail_WssFR = UtInput_SasM_MainEemFailData_Eem_Fail_WssFR[i];
        SasM_MainEemFailData.Eem_Fail_WssRL = UtInput_SasM_MainEemFailData_Eem_Fail_WssRL[i];
        SasM_MainEemFailData.Eem_Fail_WssRR = UtInput_SasM_MainEemFailData_Eem_Fail_WssRR[i];
        SasM_MainEcuModeSts = UtInput_SasM_MainEcuModeSts[i];
        SasM_MainIgnOnOffSts = UtInput_SasM_MainIgnOnOffSts[i];
        SasM_MainIgnEdgeSts = UtInput_SasM_MainIgnEdgeSts[i];
        SasM_MainVBatt1Mon = UtInput_SasM_MainVBatt1Mon[i];
        SasM_MainDiagClrSrs = UtInput_SasM_MainDiagClrSrs[i];

        SasM_Main();

        TEST_ASSERT_EQUAL(SasM_MainSASMOutInfo.SASM_Timeout_Err, UtExpected_SasM_MainSASMOutInfo_SASM_Timeout_Err[i]);
        TEST_ASSERT_EQUAL(SasM_MainSASMOutInfo.SASM_Invalid_Err, UtExpected_SasM_MainSASMOutInfo_SASM_Invalid_Err[i]);
        TEST_ASSERT_EQUAL(SasM_MainSASMOutInfo.SASM_CRC_Err, UtExpected_SasM_MainSASMOutInfo_SASM_CRC_Err[i]);
        TEST_ASSERT_EQUAL(SasM_MainSASMOutInfo.SASM_Rolling_Err, UtExpected_SasM_MainSASMOutInfo_SASM_Rolling_Err[i]);
        TEST_ASSERT_EQUAL(SasM_MainSASMOutInfo.SASM_Range_Err, UtExpected_SasM_MainSASMOutInfo_SASM_Range_Err[i]);
    }
}

TEST_GROUP_RUNNER(SasM_Main)
{
    RUN_TEST_CASE(SasM_Main, All);
}
