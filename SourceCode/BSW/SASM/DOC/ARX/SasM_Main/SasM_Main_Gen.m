SasM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'SteeringAngle'
    };
SasM_MainCanRxEscInfo = CreateBus(SasM_MainCanRxEscInfo, DeList);
clear DeList;

SasM_MainCanRxInfo = Simulink.Bus;
DeList={
    'MainBusOffFlag'
    };
SasM_MainCanRxInfo = CreateBus(SasM_MainCanRxInfo, DeList);
clear DeList;

SasM_MainRxSasInfo = Simulink.Bus;
DeList={
    'Angle'
    'Speed'
    'Ok'
    'Trim'
    'CheckSum'
    };
SasM_MainRxSasInfo = CreateBus(SasM_MainRxSasInfo, DeList);
clear DeList;

SasM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'SasMsgOkFlg'
    };
SasM_MainRxMsgOkFlgInfo = CreateBus(SasM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

SasM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
SasM_MainSenPwrMonitorData = CreateBus(SasM_MainSenPwrMonitorData, DeList);
clear DeList;

SasM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
SasM_MainEemFailData = CreateBus(SasM_MainEemFailData, DeList);
clear DeList;

SasM_MainEcuModeSts = Simulink.Bus;
DeList={'SasM_MainEcuModeSts'};
SasM_MainEcuModeSts = CreateBus(SasM_MainEcuModeSts, DeList);
clear DeList;

SasM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SasM_MainIgnOnOffSts'};
SasM_MainIgnOnOffSts = CreateBus(SasM_MainIgnOnOffSts, DeList);
clear DeList;

SasM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SasM_MainIgnEdgeSts'};
SasM_MainIgnEdgeSts = CreateBus(SasM_MainIgnEdgeSts, DeList);
clear DeList;

SasM_MainVBatt1Mon = Simulink.Bus;
DeList={'SasM_MainVBatt1Mon'};
SasM_MainVBatt1Mon = CreateBus(SasM_MainVBatt1Mon, DeList);
clear DeList;

SasM_MainDiagClrSrs = Simulink.Bus;
DeList={'SasM_MainDiagClrSrs'};
SasM_MainDiagClrSrs = CreateBus(SasM_MainDiagClrSrs, DeList);
clear DeList;

SasM_MainSASMOutInfo = Simulink.Bus;
DeList={
    'SASM_Timeout_Err'
    'SASM_Invalid_Err'
    'SASM_CRC_Err'
    'SASM_Rolling_Err'
    'SASM_Range_Err'
    };
SasM_MainSASMOutInfo = CreateBus(SasM_MainSASMOutInfo, DeList);
clear DeList;

