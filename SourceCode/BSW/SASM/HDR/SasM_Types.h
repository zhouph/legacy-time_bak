/**
 * @defgroup SasM_Types SasM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SASM_TYPES_H_
#define SASM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxEscInfo_t SasM_MainCanRxEscInfo;
    Proxy_RxCanRxInfo_t SasM_MainCanRxInfo;
    Proxy_RxByComRxSasInfo_t SasM_MainRxSasInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t SasM_MainRxMsgOkFlgInfo;
    SenPwrM_MainSenPwrMonitor_t SasM_MainSenPwrMonitorData;
    Eem_MainEemFailData_t SasM_MainEemFailData;
    Mom_HndlrEcuModeSts_t SasM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SasM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SasM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SasM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t SasM_MainDiagClrSrs;

/* Output Data Element */
    SasM_MainSASMOutInfo_t SasM_MainSASMOutInfo;
}SasM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SASM_TYPES_H_ */
/** @} */
