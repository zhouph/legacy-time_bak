/**
 * @defgroup SasM_Main SasM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SASM_MAIN_H_
#define SASM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasM_Types.h"
#include "SasM_Cfg.h"
#include "SasM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SASM_MAIN_MODULE_ID      (0)
 #define SASM_MAIN_MAJOR_VERSION  (2)
 #define SASM_MAIN_MINOR_VERSION  (0)
 #define SASM_MAIN_PATCH_VERSION  (0)
 #define SASM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SasM_Main_HdrBusType SasM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SasM_MainVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxEscInfo_t SasM_MainCanRxEscInfo;
extern Proxy_RxCanRxInfo_t SasM_MainCanRxInfo;
extern Proxy_RxByComRxSasInfo_t SasM_MainRxSasInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t SasM_MainRxMsgOkFlgInfo;
extern SenPwrM_MainSenPwrMonitor_t SasM_MainSenPwrMonitorData;
extern Eem_MainEemFailData_t SasM_MainEemFailData;
extern Mom_HndlrEcuModeSts_t SasM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t SasM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t SasM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t SasM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t SasM_MainDiagClrSrs;

/* Output Data Element */
extern SasM_MainSASMOutInfo_t SasM_MainSASMOutInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SasM_Main_Init(void);
extern void SasM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SASM_MAIN_H_ */
/** @} */
