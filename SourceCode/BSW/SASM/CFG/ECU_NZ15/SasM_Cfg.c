/**
 * @defgroup SasM_Cfg SasM_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasM_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasM_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SASM_START_SEC_CONST_UNSPECIFIED
#include "SasM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SASM_STOP_SEC_CONST_UNSPECIFIED
#include "SasM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_START_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASM_STOP_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
#define SASM_START_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASM_STOP_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_START_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/** Variable Section (32BIT)**/


#define SASM_STOP_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_START_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASM_STOP_SEC_VAR_NOINIT_32BIT
#include "SasM_MemMap.h"
#define SASM_START_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASM_STOP_SEC_VAR_UNSPECIFIED
#include "SasM_MemMap.h"
#define SASM_START_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/** Variable Section (32BIT)**/


#define SASM_STOP_SEC_VAR_32BIT
#include "SasM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SASM_START_SEC_CODE
#include "SasM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SASM_STOP_SEC_CODE
#include "SasM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
