/**
 * @defgroup Acm_Main_Ifa Acm_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACM_MAIN_START_SEC_CODE
#include "Acm_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACM_MAIN_STOP_SEC_CODE
#include "Acm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
