# \file
#
# \brief Acm
#
# This file contains the implementation of the SWC
# module Acm.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Acm_src

Acm_src_FILES        += $(Acm_SRC_PATH)\Acm_Main.c
Acm_src_FILES        += $(Acm_SRC_PATH)\Acm_Process.c
Acm_src_FILES        += $(Acm_IFA_PATH)\Acm_Main_Ifa.c
Acm_src_FILES        += $(Acm_CFG_PATH)\Acm_Cfg.c
Acm_src_FILES        += $(Acm_CAL_PATH)\Acm_Cal.c

ifeq ($(ICE_COMPILE),true)
Acm_src_FILES        += $(Acm_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Acm_src_FILES        += $(Acm_UNITY_PATH)\unity.c
	Acm_src_FILES        += $(Acm_UNITY_PATH)\unity_fixture.c	
	Acm_src_FILES        += $(Acm_UT_PATH)\main.c
	Acm_src_FILES        += $(Acm_UT_PATH)\Acm_Main\Acm_Main_UtMain.c
endif