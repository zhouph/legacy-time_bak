# \file
#
# \brief Acm
#
# This file contains the implementation of the SWC
# module Acm.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Acm_CORE_PATH     := $(MANDO_BSW_ROOT)\Acm
Acm_CAL_PATH      := $(Acm_CORE_PATH)\CAL\$(Acm_VARIANT)
Acm_SRC_PATH      := $(Acm_CORE_PATH)\SRC
Acm_CFG_PATH      := $(Acm_CORE_PATH)\CFG\$(Acm_VARIANT)
Acm_HDR_PATH      := $(Acm_CORE_PATH)\HDR
Acm_IFA_PATH      := $(Acm_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Acm_CMN_PATH      := $(Acm_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Acm_UT_PATH		:= $(Acm_CORE_PATH)\UT
	Acm_UNITY_PATH	:= $(Acm_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Acm_UT_PATH)
	CC_INCLUDE_PATH		+= $(Acm_UNITY_PATH)
	Acm_Main_PATH 	:= Acm_UT_PATH\Acm_Main
endif
CC_INCLUDE_PATH    += $(Acm_CAL_PATH)
CC_INCLUDE_PATH    += $(Acm_SRC_PATH)
CC_INCLUDE_PATH    += $(Acm_CFG_PATH)
CC_INCLUDE_PATH    += $(Acm_HDR_PATH)
CC_INCLUDE_PATH    += $(Acm_IFA_PATH)
CC_INCLUDE_PATH    += $(Acm_CMN_PATH)

