/**
 * @defgroup Acm_Main Acm_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Main.h"
#include "Acm_Main_Ifa.h"
#include "Acm_Process.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Acm_Main_HdrBusType Acm_MainBus;

/* Version Info */
const SwcVersionInfo_t Acm_MainVersionInfo = 
{   
    ACM_MAIN_MODULE_ID,           /* Acm_MainVersionInfo.ModuleId */
    ACM_MAIN_MAJOR_VERSION,       /* Acm_MainVersionInfo.MajorVer */
    ACM_MAIN_MINOR_VERSION,       /* Acm_MainVersionInfo.MinorVer */
    ACM_MAIN_PATCH_VERSION,       /* Acm_MainVersionInfo.PatchVer */
    ACM_MAIN_BRANCH_VERSION       /* Acm_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */

/* Output Data Element */
Acm_MainAcmAsicInitCompleteFlag_t Acm_MainAcmAsicInitCompleteFlag;

uint32 Acm_Main_Timer_Start;
uint32 Acm_Main_Timer_Elapsed;

#define ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACM_MAIN_START_SEC_CODE
#include "Acm_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Acm_Main_Init(void)
{
    /* Initialize internal bus */
    Acm_MainBus.Acm_MainAcmAsicInitCompleteFlag = 0;
}

void Acm_Main(void)
{
    Acm_Main_Timer_Start = STM0_TIM0.U;

    /* Input */

    /* Process */
    Acm_Process();

    /* Output */
    Acm_Main_Write_Acm_MainAcmAsicInitCompleteFlag(&Acm_MainBus.Acm_MainAcmAsicInitCompleteFlag);

    Acm_Main_Timer_Elapsed = STM0_TIM0.U - Acm_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACM_MAIN_STOP_SEC_CODE
#include "Acm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
