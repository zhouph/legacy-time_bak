/**
 * @defgroup Acm_Main Acm_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Main.h"
#include "Acm_Main_Ifa.h"
#include "Acm_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

ACH_AsicInitDataType     Acm_AsicInitData;
ACH_ValveInitDataType    Acm_ValveInitData;
ACH_MotorInitDataType    Acm_MotorInitData;
ACH_VsoInitDataType      Acm_VsoInitData;
ACH_WssInitDataType      Acm_WssInitData;
ACH_LampShlsInitDataType Acm_LampShlsInitData;

#define ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_MAIN_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/

Acm_AsicInitStateType Acm_AsicInitStatus;
static Acm_ModuleInitStateType ModuleInitStateType;

#define ACM_MAIN_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACM_MAIN_START_SEC_CODE
#include "Acm_MemMap.h"

static void Acm_SetAsicInitData(void);
static void Acm_SetValveInitData(void);
static void Acm_SetMotorInitData(void);
static void Acm_SetVsoInitData(void);
static void Acm_SetWssInitData(void);
static void Acm_SetLampShlsInitData(void);
static void Acm_CheckAsicInitState(void);
static void Acm_CheckAsicInitData(void);
static void Acm_CheckValveInitData(void);
static void Acm_CheckMotorInitData(void);
static void Acm_CheckVsoInitData(void);
static void Acm_CheckWssInitData(void);
static void Acm_CheckLampShlsInitData(void);
static uint8 Acm_CompareData(uint16 Data1, uint16 Data2);


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Acm_Process(void)
{
    static uint8 TimeIndex = 0;
  
    if(Acm_AsicInitStatus == Acm_AsicInitNotComplete)
    {
        switch(TimeIndex)
        {
            case Acm_TimeIndex_0ms:
            {
                Ach_SetReadSpiData_CS(ACH_MSG0_ID_CHIPID);
                break;
            }
            case Acm_TimeIndex_1ms:
            {
                if(ModuleInitStateType.Acm_AsicInitState == 0)
                {
                    Acm_SetAsicInitData();
                    Ach_SetWriteSpiData_CS(ACH_AsicInitMsgSet, &Acm_AsicInitData);
                }
                if(ModuleInitStateType.Acm_MotorInitState == 0)
                {
                    Acm_SetMotorInitData();
                    Ach_SetWriteSpiData_CS(ACH_MotorInitMsgSet, &Acm_MotorInitData);
                }
                if(ModuleInitStateType.Acm_WssInitState == 0)
                {
                    Acm_SetWssInitData();
                    Ach_SetWriteSpiData_CS(ACH_WssInitMsgSet, &Acm_WssInitData);
                }
                break;
            }
            case Acm_TimeIndex_2ms:
            {
                if(ModuleInitStateType.Acm_ValveInitState == 0)
                {
                    Acm_SetValveInitData();
                    Ach_SetWriteSpiData_CS(ACH_ValveInitMsgSet, &Acm_ValveInitData);
                }
                break;
            }
            case Acm_TimeIndex_3ms:
            {
                if(ModuleInitStateType.Acm_WssInitState == 0)
                {
                    Ach_SetWriteSpiData_CS(ACH_WssInitMsgSet, &Acm_WssInitData);       /* Need to send WSS Init twice */
                }
                if(ModuleInitStateType.Acm_LampShlsInitState == 0)
                {
                    Acm_SetLampShlsInitData();
                    Ach_SetWriteSpiData_CS(ACH_LampShlsInitMsgSet, &Acm_LampShlsInitData);
                }
                break;
            }
            case Acm_TimeIndex_4ms:
            {
                Acm_CheckAsicInitState();
                break;
            }
            default:
            {
                break;
            }
        }
    }
    else if(Acm_AsicInitStatus == Acm_AsicInitComplete)
    {
        switch(TimeIndex)
        {
            case Acm_TimeIndex_0ms:
            {
                Ach_SetReadSpiData_CS(ACH_MSG0_ID_CHIPID);
                Ach_SetReadSpiData_CS(ACH_MSG2_ID_GENCFG2);
                Ach_SetReadSpiData_CS(ACH_MSG5_ID_COMM_FAULTS);
                Ach_SetReadSpiData_CS(ACH_MSG11_ID_VDDFLT);
                Ach_SetReadSpiData_CS(ACH_MSG12_ID_VDDSTATUS);
                Ach_SetReadSpiData_CS(ACH_MSG20_ID_RESOUTPUT);
                break;
            }
            case Acm_TimeIndex_1ms:
            {
                Ach_SetReadSpiData_CS(ACH_MSG15_ID_VSOSER);
                Ach_SetReadSpiData_CS(ACH_MSG16_ID_VSODR);
                Ach_SetReadSpiData_CS(ACH_MSG18_ID_WLD1SHLSDR);
                Ach_SetReadSpiData_CS(ACH_MSG51_ID_WSIRSDR1);
                Ach_SetReadSpiData_CS(ACH_MSG55_ID_WSIRSDR2);
                Ach_SetReadSpiData_CS(ACH_MSG59_ID_WSIRSDR3);
                Ach_SetReadSpiData_CS(ACH_MSG63_ID_WSIRSDR4);
                break;
            }
            case Acm_TimeIndex_2ms:
            {
                Ach_SetReadSpiData_CS(ACH_MSG1_ID_GENCFG);
                Ach_SetReadSpiData_CS(ACH_MSG10_ID_PUMPDRD);
                Ach_SetReadSpiData_CS(ACH_MSG128_NO0_0_CHX_FAULT);
                Ach_SetReadSpiData_CS(ACH_MSG129_NO0_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG138_NO0_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG140_NO0_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG142_NO0_14_DELTA_CURR);
                Ach_SetReadSpiData_CS(ACH_MSG144_NO1_0_CHX_FAULT);
                Ach_SetReadSpiData_CS(ACH_MSG145_NO1_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG154_NO1_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG156_NO1_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG158_NO1_14_DELTA_CURR);
                Ach_SetReadSpiData_CS(ACH_MSG160_NO2_0_CHX_FAULT);
                Ach_SetReadSpiData_CS(ACH_MSG161_NO2_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG170_NO2_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG172_NO2_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG174_NO2_14_DELTA_CURR);
                Ach_SetReadSpiData_CS(ACH_MSG176_NO3_0_CHX_FAULT);
                Ach_SetReadSpiData_CS(ACH_MSG177_NO3_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG186_NO3_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG188_NO3_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG190_NO3_14_DELTA_CURR);
                break;
            }
            case Acm_TimeIndex_3ms:
            {
                Ach_SetReadSpiData_CS(ACH_MSG3_ID_GENSTATUS);
                Ach_SetReadSpiData_CS(ACH_MSG68_ID_ONOFF_CH_1_DR);
                Ach_SetReadSpiData_CS(ACH_MSG69_ID_ONOFF_CH_2_DR);
                Ach_SetReadSpiData_CS(ACH_MSG70_ID_ONOFF_CH_3_DR);
                Ach_SetReadSpiData_CS(ACH_MSG71_ID_ONOFF_CH_4_DR);
                Ach_SetReadSpiData_CS(ACH_MSG192_TC0_0_CHX_FAULT);
                Ach_SetReadSpiData_CS(ACH_MSG193_TC0_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG202_TC0_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG204_TC0_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG206_TC0_14_DELTA_CURR);
                Ach_SetReadSpiData_CS(ACH_MSG208_TC1_0_CHX_FAULT);
                Ach_SetReadSpiData_CS(ACH_MSG209_TC1_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG218_TC1_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG220_TC1_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG222_TC1_14_DELTA_CURR);
                Ach_SetReadSpiData_CS(ACH_MSG225_ESV0_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG234_ESV0_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG236_ESV0_12_PWMSENSE_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG241_ESV1_1_EXCEPTIONS_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG250_ESV1_10_AVGCURR_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG252_ESV1_12_PWMSENSE_CHX);
                break;
            }
            case Acm_TimeIndex_4ms:
            {
                Ach_SetReadSpiData_CS(ACH_MSG1_ID_GENCFG);
                Ach_SetReadSpiData_CS(ACH_MSG64_ID_SOLSERVENA_2);
                Ach_SetReadSpiData_CS(ACH_MSG65_ID_SOLENDR);
                Ach_SetReadSpiData_CS(ACH_MSG130_NO0_2_CONFIGURATION_CHX); /* It will be reset when failuer (slew-rate) so need to write again */
                Ach_SetReadSpiData_CS(ACH_MSG146_NO1_2_CONFIGURATION_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG162_NO2_2_CONFIGURATION_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG178_NO3_2_CONFIGURATION_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG194_TC0_2_CONFIGURATION_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG210_TC1_2_CONFIGURATION_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG226_ESV0_2_CONFIGURATION_CHX);
                Ach_SetReadSpiData_CS(ACH_MSG242_ESV1_2_CONFIGURATION_CHX);
                break;
            }
            default:
            {
                break;
            }
        }
    }
    else
    {
        ;
    }
    
    TimeIndex++;
    if(TimeIndex >= LOOP_TIME)
    {
        TimeIndex = 0;
    }
    
}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/


static void Acm_SetAsicInitData(void)
{
    if(Acm_InitList.AsicInit == Acm_Used)
    {       
        Acm_AsicInitData.ACH_Tx1_FS_EN                     = AcmAsicInitWriteData.Acm_Tx1_FS_EN;                  
        Acm_AsicInitData.ACH_Tx1_FS_CMD                    = AcmAsicInitWriteData.Acm_Tx1_FS_CMD;                     
        Acm_AsicInitData.ACH_Tx1_FS_VDS_TH                 = AcmAsicInitWriteData.Acm_Tx1_FS_VDS_TH;                      
        Acm_AsicInitData.ACH_Tx1_PMP_EN                    = AcmAsicInitWriteData.Acm_Tx1_PMP_EN;                     
        Acm_AsicInitData.ACH_Tx1_OSC_SprSpect_DIS          = AcmAsicInitWriteData.Acm_Tx1_OSC_SprSpect_DIS;           
        Acm_AsicInitData.ACH_Tx1_VDD_4_OVC_SD_RESTART_TIME = AcmAsicInitWriteData.Acm_Tx1_VDD_4_OVC_SD_RESTART_TIME;  
        Acm_AsicInitData.ACH_Tx1_VDD4_OVC_FLT_TIME         = AcmAsicInitWriteData.Acm_Tx1_VDD4_OVC_FLT_TIME;          
        Acm_AsicInitData.ACH_Tx1_VDD_3_OVC_SD_RESTART_TIME = AcmAsicInitWriteData.Acm_Tx1_VDD_3_OVC_SD_RESTART_TIME;  
        Acm_AsicInitData.ACH_Tx1_VDD3_OVC_FLT_TIME         = AcmAsicInitWriteData.Acm_Tx1_VDD3_OVC_FLT_TIME;          
        Acm_AsicInitData.ACH_Tx_2_WD_SW_DIS                = AcmAsicInitWriteData.Acm_Tx2_WD_SW_DIS;                  
        Acm_AsicInitData.ACH_Tx2_VDD_2d5_AUTO_SWITCH_OFF   = AcmAsicInitWriteData.Acm_Tx2_VDD_2d5_AUTO_SWITCH_OFF;    
        Acm_AsicInitData.ACH_Tx2_VDD1_DIODELOSS_FILT       = AcmAsicInitWriteData.Acm_Tx2_VDD1_DIODELOSS_FILT;        
        Acm_AsicInitData.ACH_Tx_2_WD_DIS                   = AcmAsicInitWriteData.Acm_Tx2_WD_DIS;                     
        Acm_AsicInitData.ACH_Tx2_VDD5_DIS                  = AcmAsicInitWriteData.Acm_Tx2_VDD5_DIS;                   
        Acm_AsicInitData.ACH_Tx2_VDD2_DIS                  = AcmAsicInitWriteData.Acm_Tx2_VDD2_DIS;                   
        Acm_AsicInitData.ACH_Tx19_E_PVDS_GPO               = AcmAsicInitWriteData.Acm_Tx19_E_PVDS_GPO;                
        Acm_AsicInitData.ACH_Tx19_E_VDD2d5dWSx_OT_GPO      = AcmAsicInitWriteData.Acm_Tx19_E_VDD2d5dWSx_OT_GPO;       
        Acm_AsicInitData.ACH_Tx19_E_FBO_GPO                = AcmAsicInitWriteData.Acm_Tx19_E_FBO_GPO;                 
        Acm_AsicInitData.ACH_Tx19_E_FSPI_GPO               = AcmAsicInitWriteData.Acm_Tx19_E_FSPI_GPO;                
        Acm_AsicInitData.ACH_Tx19_E_EN_GPO                 = AcmAsicInitWriteData.Acm_Tx19_E_EN_GPO;                  
        Acm_AsicInitData.ACH_Tx19_E_FS_VDS_GPO             = AcmAsicInitWriteData.Acm_Tx19_E_FS_VDS_GPO;              
    }
}


static void Acm_SetValveInitData(void)
{

    if(Acm_InitList.Nc0ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx67_CH1_EN                    = AcmValveInitWriteData.Acm_Tx67_CH1_EN;
        Acm_ValveInitData.ACH_Tx67_CH1_DIAG_EN               = AcmValveInitWriteData.Acm_Tx67_CH1_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_Tx67_CH1_TD_BLANK              = AcmValveInitWriteData.Acm_Tx67_CH1_TD_BLANK;         /* 375us, by guide line */
        Acm_ValveInitData.ACH_Tx67_CH1_CMD                   = AcmValveInitWriteData.Acm_Tx67_CH1_CMD;
    }
    if(Acm_InitList.Nc1ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx67_CH2_EN                    = AcmValveInitWriteData.Acm_Tx67_CH2_EN;
        Acm_ValveInitData.ACH_Tx67_CH2_DIAG_EN               = AcmValveInitWriteData.Acm_Tx67_CH2_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_Tx67_CH2_TD_BLANK              = AcmValveInitWriteData.Acm_Tx67_CH2_TD_BLANK;         /* 375us, by guide line */
        Acm_ValveInitData.ACH_Tx67_CH2_CMD                   = AcmValveInitWriteData.Acm_Tx67_CH2_CMD;
    }
    if(Acm_InitList.Nc2ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx67_CH3_EN                    = AcmValveInitWriteData.Acm_Tx67_CH3_EN;
        Acm_ValveInitData.ACH_Tx67_CH3_DIAG_EN               = AcmValveInitWriteData.Acm_Tx67_CH3_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_Tx67_CH3_TD_BLANK              = AcmValveInitWriteData.Acm_Tx67_CH3_TD_BLANK;         /* 375us, by guide line */
        Acm_ValveInitData.ACH_Tx67_CH3_CMD                   = AcmValveInitWriteData.Acm_Tx67_CH3_CMD;
    }
    if(Acm_InitList.Nc3ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx67_CH4_EN                    = AcmValveInitWriteData.Acm_Tx67_CH4_EN;
        Acm_ValveInitData.ACH_Tx67_CH4_DIAG_EN               = AcmValveInitWriteData.Acm_Tx67_CH4_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_Tx67_CH4_TD_BLANK              = AcmValveInitWriteData.Acm_Tx67_CH4_TD_BLANK;         /* 375us, by guide line */
        Acm_ValveInitData.ACH_Tx67_CH4_CMD                   = AcmValveInitWriteData.Acm_Tx67_CH4_CMD;
    }
    
    if(Acm_InitList.No0ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_0         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_0;
        Acm_ValveInitData.ACH_Tx65_CH0_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH0_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH0_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH0_STATUS_H;
        Acm_ValveInitData.ACH_TxNO0_2_SR_SEL                 = AcmValveInitWriteData.Acm_TxNO0_2_SR_SEL;                /* 10V/us, Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_CALIBRATION_DIS        = AcmValveInitWriteData.Acm_TxNO0_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_OFS_CHOP_DIS           = AcmValveInitWriteData.Acm_TxNO0_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_TD_BLANK_SEL           = AcmValveInitWriteData.Acm_TxNO0_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_LS_CLAMP_DIS           = AcmValveInitWriteData.Acm_TxNO0_2_LS_CLAMP_DIS;        /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_LOGIC_BIST_EN          = AcmValveInitWriteData.Acm_TxNO0_2_LOGIC_BIST_EN;        /* TODO */
        Acm_ValveInitData.ACH_TxNO0_2_DIAG_BIST_EN           = AcmValveInitWriteData.Acm_TxNO0_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_ADC_BIST_EN            = AcmValveInitWriteData.Acm_TxNO0_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_OFF_DIAG_EN            = AcmValveInitWriteData.Acm_TxNO0_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_E_SOL_GPO              = AcmValveInitWriteData.Acm_TxNO0_2_E_SOL_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_2_E_LSCLAMP_GPO          = AcmValveInitWriteData.Acm_TxNO0_2_E_LSCLAMP_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_6_PWM_CODE               = AcmValveInitWriteData.Acm_TxNO0_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxNO0_7_FREQ_MOD_STEP          = AcmValveInitWriteData.Acm_TxNO0_7_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxNO0_7_MAX_FREQ_DELTA         = AcmValveInitWriteData.Acm_TxNO0_7_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxNO0_8_KI                     = AcmValveInitWriteData.Acm_TxNO0_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_8_KP                     = AcmValveInitWriteData.Acm_TxNO0_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_9_INTTIME                = AcmValveInitWriteData.Acm_TxNO0_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_9_FILT_TIME              = AcmValveInitWriteData.Acm_TxNO0_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_9_CHOP_TIME              = AcmValveInitWriteData.Acm_TxNO0_9_CHOP_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO0_13_BASE_DELTA_CURENT     = AcmValveInitWriteData.Acm_TxNO0_13_BASE_DELTA_CURENT;                  /* TODO */
    }
    if(Acm_InitList.No1ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_1         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_1;
        Acm_ValveInitData.ACH_Tx65_CH1_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH1_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH1_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH1_STATUS_H;
        Acm_ValveInitData.ACH_TxNO1_2_SR_SEL                 = AcmValveInitWriteData.Acm_TxNO1_2_SR_SEL;                /* 10V/us, Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_CALIBRATION_DIS        = AcmValveInitWriteData.Acm_TxNO1_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_OFS_CHOP_DIS           = AcmValveInitWriteData.Acm_TxNO1_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_TD_BLANK_SEL           = AcmValveInitWriteData.Acm_TxNO1_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_LS_CLAMP_DIS           = AcmValveInitWriteData.Acm_TxNO1_2_LS_CLAMP_DIS;        /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_LOGIC_BIST_EN          = AcmValveInitWriteData.Acm_TxNO1_2_LOGIC_BIST_EN;        /* TODO */
        Acm_ValveInitData.ACH_TxNO1_2_DIAG_BIST_EN           = AcmValveInitWriteData.Acm_TxNO1_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_ADC_BIST_EN            = AcmValveInitWriteData.Acm_TxNO1_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_OFF_DIAG_EN            = AcmValveInitWriteData.Acm_TxNO1_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_E_SOL_GPO              = AcmValveInitWriteData.Acm_TxNO1_2_E_SOL_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_2_E_LSCLAMP_GPO          = AcmValveInitWriteData.Acm_TxNO1_2_E_LSCLAMP_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_6_PWM_CODE               = AcmValveInitWriteData.Acm_TxNO1_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxNO1_7_FREQ_MOD_STEP          = AcmValveInitWriteData.Acm_TxNO1_7_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxNO1_7_MAX_FREQ_DELTA         = AcmValveInitWriteData.Acm_TxNO1_7_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxNO1_8_KI                     = AcmValveInitWriteData.Acm_TxNO1_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_8_KP                     = AcmValveInitWriteData.Acm_TxNO1_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_9_INTTIME                = AcmValveInitWriteData.Acm_TxNO1_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_9_FILT_TIME              = AcmValveInitWriteData.Acm_TxNO1_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_9_CHOP_TIME              = AcmValveInitWriteData.Acm_TxNO1_9_CHOP_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO1_13_BASE_DELTA_CURENT     = AcmValveInitWriteData.Acm_TxNO1_13_BASE_DELTA_CURENT;                  /* TODO */
    }
    if(Acm_InitList.No2ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_2         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_2;
        Acm_ValveInitData.ACH_Tx65_CH2_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH2_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH2_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH2_STATUS_H;
        Acm_ValveInitData.ACH_TxNO2_2_SR_SEL                 = AcmValveInitWriteData.Acm_TxNO2_2_SR_SEL;                /* 10V/us, Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_CALIBRATION_DIS        = AcmValveInitWriteData.Acm_TxNO2_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_OFS_CHOP_DIS           = AcmValveInitWriteData.Acm_TxNO2_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_TD_BLANK_SEL           = AcmValveInitWriteData.Acm_TxNO2_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_LS_CLAMP_DIS           = AcmValveInitWriteData.Acm_TxNO2_2_LS_CLAMP_DIS;        /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_LOGIC_BIST_EN          = AcmValveInitWriteData.Acm_TxNO2_2_LOGIC_BIST_EN;        /* TODO */
        Acm_ValveInitData.ACH_TxNO2_2_DIAG_BIST_EN           = AcmValveInitWriteData.Acm_TxNO2_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_ADC_BIST_EN            = AcmValveInitWriteData.Acm_TxNO2_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_OFF_DIAG_EN            = AcmValveInitWriteData.Acm_TxNO2_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_E_SOL_GPO              = AcmValveInitWriteData.Acm_TxNO2_2_E_SOL_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_2_E_LSCLAMP_GPO          = AcmValveInitWriteData.Acm_TxNO2_2_E_LSCLAMP_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_6_PWM_CODE               = AcmValveInitWriteData.Acm_TxNO2_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxNO2_7_FREQ_MOD_STEP          = AcmValveInitWriteData.Acm_TxNO2_7_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxNO2_7_MAX_FREQ_DELTA         = AcmValveInitWriteData.Acm_TxNO2_7_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxNO2_8_KI                     = AcmValveInitWriteData.Acm_TxNO2_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_8_KP                     = AcmValveInitWriteData.Acm_TxNO2_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_9_INTTIME                = AcmValveInitWriteData.Acm_TxNO2_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_9_FILT_TIME              = AcmValveInitWriteData.Acm_TxNO2_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_9_CHOP_TIME              = AcmValveInitWriteData.Acm_TxNO2_9_CHOP_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO2_13_BASE_DELTA_CURENT     = AcmValveInitWriteData.Acm_TxNO2_13_BASE_DELTA_CURENT;                  /* TODO */
    }
    if(Acm_InitList.No3ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_3         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_3;
        Acm_ValveInitData.ACH_Tx65_CH3_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH3_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH3_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH3_STATUS_H;
        Acm_ValveInitData.ACH_TxNO3_2_SR_SEL                 = AcmValveInitWriteData.Acm_TxNO3_2_SR_SEL;                /* 10V/us, Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_CALIBRATION_DIS        = AcmValveInitWriteData.Acm_TxNO3_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_OFS_CHOP_DIS           = AcmValveInitWriteData.Acm_TxNO3_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_TD_BLANK_SEL           = AcmValveInitWriteData.Acm_TxNO3_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_LS_CLAMP_DIS           = AcmValveInitWriteData.Acm_TxNO3_2_LS_CLAMP_DIS;        /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_LOGIC_BIST_EN          = AcmValveInitWriteData.Acm_TxNO3_2_LOGIC_BIST_EN;        /* TODO */
        Acm_ValveInitData.ACH_TxNO3_2_DIAG_BIST_EN           = AcmValveInitWriteData.Acm_TxNO3_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_ADC_BIST_EN            = AcmValveInitWriteData.Acm_TxNO3_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_OFF_DIAG_EN            = AcmValveInitWriteData.Acm_TxNO3_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_E_SOL_GPO              = AcmValveInitWriteData.Acm_TxNO3_2_E_SOL_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_2_E_LSCLAMP_GPO          = AcmValveInitWriteData.Acm_TxNO3_2_E_LSCLAMP_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_6_PWM_CODE               = AcmValveInitWriteData.Acm_TxNO3_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxNO3_7_FREQ_MOD_STEP          = AcmValveInitWriteData.Acm_TxNO3_7_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxNO3_7_MAX_FREQ_DELTA         = AcmValveInitWriteData.Acm_TxNO3_7_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxNO3_8_KI                     = AcmValveInitWriteData.Acm_TxNO3_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_8_KP                     = AcmValveInitWriteData.Acm_TxNO3_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_9_INTTIME                = AcmValveInitWriteData.Acm_TxNO3_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_9_FILT_TIME              = AcmValveInitWriteData.Acm_TxNO3_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_9_CHOP_TIME              = AcmValveInitWriteData.Acm_TxNO3_9_CHOP_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxNO3_13_BASE_DELTA_CURENT     = AcmValveInitWriteData.Acm_TxNO3_13_BASE_DELTA_CURENT;                  /* TODO */
    }
    if(Acm_InitList.Tc0ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_4         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_4;
        Acm_ValveInitData.ACH_Tx65_CH4_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH4_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH4_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH4_STATUS_H;
        Acm_ValveInitData.ACH_TxTC0_2_SR_SEL                 = AcmValveInitWriteData.Acm_TxTC0_2_SR_SEL;                /* 10V/us, Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_CALIBRATION_DIS        = AcmValveInitWriteData.Acm_TxTC0_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_OFS_CHOP_DIS           = AcmValveInitWriteData.Acm_TxTC0_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_TD_BLANK_SEL           = AcmValveInitWriteData.Acm_TxTC0_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_LS_CLAMP_DIS           = AcmValveInitWriteData.Acm_TxTC0_2_LS_CLAMP_DIS;        /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_LOGIC_BIST_EN          = AcmValveInitWriteData.Acm_TxTC0_2_LOGIC_BIST_EN;        /* TODO */
        Acm_ValveInitData.ACH_TxTC0_2_DIAG_BIST_EN           = AcmValveInitWriteData.Acm_TxTC0_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_ADC_BIST_EN            = AcmValveInitWriteData.Acm_TxTC0_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_OFF_DIAG_EN            = AcmValveInitWriteData.Acm_TxTC0_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_E_SOL_GPO              = AcmValveInitWriteData.Acm_TxTC0_2_E_SOL_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_2_E_LSCLAMP_GPO          = AcmValveInitWriteData.Acm_TxTC0_2_E_LSCLAMP_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_6_PWM_CODE               = AcmValveInitWriteData.Acm_TxTC0_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxTC0_7_FREQ_MOD_STEP          = AcmValveInitWriteData.Acm_TxTC0_7_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxTC0_7_MAX_FREQ_DELTA         = AcmValveInitWriteData.Acm_TxTC0_7_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxTC0_8_KI                     = AcmValveInitWriteData.Acm_TxTC0_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_8_KP                     = AcmValveInitWriteData.Acm_TxTC0_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_9_INTTIME                = AcmValveInitWriteData.Acm_TxTC0_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_9_FILT_TIME              = AcmValveInitWriteData.Acm_TxTC0_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_9_CHOP_TIME              = AcmValveInitWriteData.Acm_TxTC0_9_CHOP_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC0_13_BASE_DELTA_CURENT     = AcmValveInitWriteData.Acm_TxTC0_13_BASE_DELTA_CURENT;                  /* TODO */
    }
    if(Acm_InitList.Tc1ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_5         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_5;
        Acm_ValveInitData.ACH_Tx65_CH5_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH5_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH5_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH5_STATUS_H;
        Acm_ValveInitData.ACH_TxTC1_2_SR_SEL                 = AcmValveInitWriteData.Acm_TxTC1_2_SR_SEL;                /* 10V/us, Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_CALIBRATION_DIS        = AcmValveInitWriteData.Acm_TxTC1_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_OFS_CHOP_DIS           = AcmValveInitWriteData.Acm_TxTC1_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_TD_BLANK_SEL           = AcmValveInitWriteData.Acm_TxTC1_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_LS_CLAMP_DIS           = AcmValveInitWriteData.Acm_TxTC1_2_LS_CLAMP_DIS;        /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_LOGIC_BIST_EN          = AcmValveInitWriteData.Acm_TxTC1_2_LOGIC_BIST_EN;        /* TODO */
        Acm_ValveInitData.ACH_TxTC1_2_DIAG_BIST_EN           = AcmValveInitWriteData.Acm_TxTC1_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_ADC_BIST_EN            = AcmValveInitWriteData.Acm_TxTC1_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_OFF_DIAG_EN            = AcmValveInitWriteData.Acm_TxTC1_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_E_SOL_GPO              = AcmValveInitWriteData.Acm_TxTC1_2_E_SOL_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_2_E_LSCLAMP_GPO          = AcmValveInitWriteData.Acm_TxTC1_2_E_LSCLAMP_GPO;        /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_6_PWM_CODE               = AcmValveInitWriteData.Acm_TxTC1_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxTC1_7_FREQ_MOD_STEP          = AcmValveInitWriteData.Acm_TxTC1_7_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxTC1_7_MAX_FREQ_DELTA         = AcmValveInitWriteData.Acm_TxTC1_7_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxTC1_8_KI                     = AcmValveInitWriteData.Acm_TxTC1_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_8_KP                     = AcmValveInitWriteData.Acm_TxTC1_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_9_INTTIME                = AcmValveInitWriteData.Acm_TxTC1_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_9_FILT_TIME              = AcmValveInitWriteData.Acm_TxTC1_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_9_CHOP_TIME              = AcmValveInitWriteData.Acm_TxTC1_9_CHOP_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxTC1_13_BASE_DELTA_CURENT     = AcmValveInitWriteData.Acm_TxTC1_13_BASE_DELTA_CURENT;                  /* TODO */
    }
    if(Acm_InitList.Esv0ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_6         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_6;
        Acm_ValveInitData.ACH_Tx65_CH6_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH6_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH6_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH6_STATUS_H;
        Acm_ValveInitData.ACH_TxESV0_2_SR_SEL                = AcmValveInitWriteData.Acm_TxESV0_2_SR_SEL;                /* Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_CALIBRATION_DIS       = AcmValveInitWriteData.Acm_TxESV0_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line*/  
        Acm_ValveInitData.ACH_TxESV0_2_OFS_CHOP_DIS          = AcmValveInitWriteData.Acm_TxESV0_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_TD_BLANK_SEL          = AcmValveInitWriteData.Acm_TxESV0_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_LS_CLAMP_DIS          = AcmValveInitWriteData.Acm_TxESV0_2_LS_CLAMP_DIS;                /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_LOGIC_BIST_EN         = AcmValveInitWriteData.Acm_TxESV0_2_LOGIC_BIST_EN; 
        Acm_ValveInitData.ACH_TxESV0_2_DIAG_BIST_EN          = AcmValveInitWriteData.Acm_TxESV0_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_ADC_BIST_EN           = AcmValveInitWriteData.Acm_TxESV0_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_OFF_DIAG_EN           = AcmValveInitWriteData.Acm_TxESV0_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_2_E_SOL_GPO             = AcmValveInitWriteData.Acm_TxESV0_2_E_SOL_GPO; 
        Acm_ValveInitData.ACH_TxESV0_2_E_LSCLAMP_GPO         = AcmValveInitWriteData.Acm_TxESV0_2_E_LSCLAMP_GPO; 
        Acm_ValveInitData.ACH_TxESV0_6_PWM_CODE              = AcmValveInitWriteData.Acm_TxESV0_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxESV0_6_FREQ_MOD_STEP         = AcmValveInitWriteData.Acm_TxESV0_6_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxESV0_6_MAX_FREQ_DELTA        = AcmValveInitWriteData.Acm_TxESV0_6_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxESV0_8_KI                    = AcmValveInitWriteData.Acm_TxESV0_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_8_KP                    = AcmValveInitWriteData.Acm_TxESV0_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_9_INTTIME               = AcmValveInitWriteData.Acm_TxESV0_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_9_FILT_TIME             = AcmValveInitWriteData.Acm_TxESV0_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV0_9_CHOP_TIME             = AcmValveInitWriteData.Acm_TxESV0_9_CHOP_TIME;                  /* by guide line */
    }
    if(Acm_InitList.Esv1ValveInit == Acm_Used)
    {
        Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_7         = AcmValveInitWriteData.Acm_Tx64_SOLENOID_ENABLE_7;
        Acm_ValveInitData.ACH_Tx65_CH7_STATUS_L              = AcmValveInitWriteData.Acm_Tx65_CH7_STATUS_L;
        Acm_ValveInitData.ACH_Tx65_CH7_STATUS_H              = AcmValveInitWriteData.Acm_Tx65_CH7_STATUS_H;
        Acm_ValveInitData.ACH_TxESV1_2_SR_SEL                = AcmValveInitWriteData.Acm_TxESV1_2_SR_SEL;                /* Slewrate Select by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_CALIBRATION_DIS       = AcmValveInitWriteData.Acm_TxESV1_2_CALIBRATION_DIS;        /* Digital Current Sense Enable by guide line*/  
        Acm_ValveInitData.ACH_TxESV1_2_OFS_CHOP_DIS          = AcmValveInitWriteData.Acm_TxESV1_2_OFS_CHOP_DIS;        /* Current Sense Offset Compensation Active by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_TD_BLANK_SEL          = AcmValveInitWriteData.Acm_TxESV1_2_TD_BLANK_SEL;         /* Long Blank Time Selected by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_LS_CLAMP_DIS          = AcmValveInitWriteData.Acm_TxESV1_2_LS_CLAMP_DIS;                /* Low Side Clamp Active by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_LOGIC_BIST_EN         = AcmValveInitWriteData.Acm_TxESV1_2_LOGIC_BIST_EN; 
        Acm_ValveInitData.ACH_TxESV1_2_DIAG_BIST_EN          = AcmValveInitWriteData.Acm_TxESV1_2_DIAG_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_ADC_BIST_EN           = AcmValveInitWriteData.Acm_TxESV1_2_ADC_BIST_EN;        /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_OFF_DIAG_EN           = AcmValveInitWriteData.Acm_TxESV1_2_OFF_DIAG_EN;         /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_2_E_SOL_GPO             = AcmValveInitWriteData.Acm_TxESV1_2_E_SOL_GPO; 
        Acm_ValveInitData.ACH_TxESV1_2_E_LSCLAMP_GPO         = AcmValveInitWriteData.Acm_TxESV1_2_E_LSCLAMP_GPO; 
        Acm_ValveInitData.ACH_TxESV1_6_PWM_CODE              = AcmValveInitWriteData.Acm_TxESV1_6_PWM_CODE;
        Acm_ValveInitData.ACH_TxESV1_6_FREQ_MOD_STEP         = AcmValveInitWriteData.Acm_TxESV1_6_FREQ_MOD_STEP;
        Acm_ValveInitData.ACH_TxESV1_6_MAX_FREQ_DELTA        = AcmValveInitWriteData.Acm_TxESV1_6_MAX_FREQ_DELTA;
        Acm_ValveInitData.ACH_TxESV1_8_KI                    = AcmValveInitWriteData.Acm_TxESV1_8_KI;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_8_KP                    = AcmValveInitWriteData.Acm_TxESV1_8_KP;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_9_INTTIME               = AcmValveInitWriteData.Acm_TxESV1_9_INTTIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_9_FILT_TIME             = AcmValveInitWriteData.Acm_TxESV1_9_FILT_TIME;                  /* by guide line */
        Acm_ValveInitData.ACH_TxESV1_9_CHOP_TIME             = AcmValveInitWriteData.Acm_TxESV1_9_CHOP_TIME;                  /* by guide line */
    }
}
static void Acm_SetMotorInitData(void)
{
    if(Acm_InitList.MotorInit == Acm_Used)
    {
        Acm_MotorInitData.ACH_Tx8_PMP_TCK_PWM                = AcmMotorInitWriteData.Acm_Tx8_PMP_TCK_PWM;
        Acm_MotorInitData.ACH_Tx9_PMP_VDS_TH                 = AcmMotorInitWriteData.Acm_Tx9_PMP_VDS_TH;    /* by guide line */
        Acm_MotorInitData.ACH_Tx9_PMP2_DIS                   = AcmMotorInitWriteData.Acm_Tx9_PMP2_DIS;        /* by guide line */
        Acm_MotorInitData.ACH_Tx9_PMP_VDS_FIL                = AcmMotorInitWriteData.Acm_Tx9_PMP_VDS_FIL;      /* by guide line */
        Acm_MotorInitData.ACH_Tx9_LDACT_DIS                  = AcmMotorInitWriteData.Acm_Tx9_LDACT_DIS;                  /* Load Dump Enable */
        Acm_MotorInitData.ACH_Tx9_PMP1_ISINC                 = AcmMotorInitWriteData.Acm_Tx9_PMP1_ISINC;        /* by guide line */
        Acm_MotorInitData.ACH_Tx9_PMP_DT                     = AcmMotorInitWriteData.Acm_Tx9_PMP_DT;     /* by guide line */
        Acm_MotorInitData.ACH_Tx9_PMP_TEST                   = AcmMotorInitWriteData.Acm_Tx9_PMP_TEST;        /* Errata of 100ASIC BIST not supported by guide line*/
        Acm_MotorInitData.ACH_Tx9_PMP_BIST_EN                = AcmMotorInitWriteData.Acm_Tx9_PMP_BIST_EN;        /* by guide line */
    }
}

static void Acm_SetVsoInitData(void)
{
    if(Acm_InitList.VsoInit == Acm_Used)  
    {
        #if 0 /* TODO VSO setting */
        if(VSO_TYPE == HARD_WIRE_TYPE)
        {
            Acm_VsoInitData.ACH_Tx15_VSO_CONF                = 0;                  /* WSO Mode */
        }
        else if((VSO_TYPE == SW0_GEN_TYPE)||(VSO_TYPE == SW1_GEN_TYPE))
        {
            Acm_VsoInitData.ACH_Tx15_VSO_CONF                = 1;                  /* SW GEN Mode */
        }
        else
        {
            ;
        }
        if(VSO_WHEEL_OUT_SEL == WHEEL_CH0)
        {
            Acm_VsoInitData.ACH_Tx15_WSO_VSO_S               = WHEEL_CH0;
        }
        else if(VSO_WHEEL_OUT_SEL == WHEEL_CH1)
        {
            Acm_VsoInitData.ACH_Tx15_WSO_VSO_S               = WHEEL_CH1;
        }
        else if(VSO_WHEEL_OUT_SEL == WHEEL_CH2)
        {
            Acm_VsoInitData.ACH_Tx15_WSO_VSO_S               = WHEEL_CH2;
        }
        else if(VSO_WHEEL_OUT_SEL == WHEEL_CH3)
        {
            Acm_VsoInitData.ACH_Tx15_WSO_VSO_S               = WHEEL_CH3;
        }
        else
        {
            ;
        }
        Acm_VsoInitData.ACH_Tx15_SSIG_VSO                    = Acm_Disable;        /* Don't Process StandStill Pulse */;
        if(VSO_TYPE == HARD_WIRE_TYPE)
        {
            Acm_VsoInitData.ACH_Tx15_VSO_PTEN                = Acm_Enable;         /* Pass Through Mode Enable */ 
        }
        else
        {
            Acm_VsoInitData.ACH_Tx15_VSO_PTEN                = Acm_Disable;        /* Pass Through Mode Disable */ 
        }
            
        Acm_VsoInitData.ACH_Tx15_VSO_OFF_DIAG_EN             = Acm_Enable;         /* VSO Diag. by guide line */
        Acm_VsoInitData.ACH_Tx15_VSO_LS_CLAMP_DIS            = Acm_Disable;        /* VSO Diag. by guide line */
        Acm_VsoInitData.ACH_Tx15_SEL_CONF                    = 0;
        Acm_VsoInitData.ACH_Tx15_WSO_SEL_S                   = 0;
        Acm_VsoInitData.ACH_Tx15_SSIG_SEL                    = 0;
        Acm_VsoInitData.ACH_Tx15_SEL_OUT_CMD                 = 0;
        Acm_VsoInitData.ACH_Tx15_SEL_OFF_DIAG_EN             = Acm_Enable;         /* SEL Diag. by guide line */
        Acm_VsoInitData.ACH_Tx15_SEL_LS_CLAMP_DIS            = Acm_Disable;        /* SEL Diag. by guide line */
        Acm_VsoInitData.ACH_Tx15_SEL_PTEN                    = 0;
        #endif
    }
}
static void Acm_SetWssInitData(void)
{
    
    if(Acm_InitList.WssInit == Acm_Used)  
    {       
        Acm_WssInitData.ACH_Tx32_CONFIG_RANGE               = AcmWssInitWriteData.Acm_Tx32_CONFIG_RANGE;        /* TEST Mode Disable, TODO */
        Acm_WssInitData.ACH_Tx32_WSO_TEST                   = AcmWssInitWriteData.Acm_Tx32_WSO_TEST;            /* TEST Mode Disable, TODO */
        Acm_WssInitData.ACH_Tx33_WSI_FIRST_THRESHOLD        = AcmWssInitWriteData.Acm_Tx33_WSI_FIRST_THRESHOLD;        /* TEST Mode Disable, TODO */
        Acm_WssInitData.ACH_Tx34_WSI_OFFSET_THRESHOLD       = AcmWssInitWriteData.Acm_Tx34_WSI_OFFSET_THRESHOLD;        /* TEST Mode Disable, TODO */

        /* WSS CH0 */
        Acm_WssInitData.ACH_Tx35_PTEN                       = AcmWssInitWriteData.Acm_Tx35_PTEN;
        Acm_WssInitData.ACH_Tx35_SS_DIS                     = AcmWssInitWriteData.Acm_Tx35_SS_DIS;
        Acm_WssInitData.ACH_Tx35_SENSOR_TYPE_SELECTION      = AcmWssInitWriteData.Acm_Tx35_SENSOR_TYPE_SELECTION;
        Acm_WssInitData.ACH_Tx35_FILTER_SELECTION           = AcmWssInitWriteData.Acm_Tx35_FILTER_SELECTION;    /* by guide line*/
        Acm_WssInitData.ACH_Tx35_FIX_TH                     = AcmWssInitWriteData.Acm_Tx35_FIX_TH;              /* by guide line*/
            
        /* WSS CH1 */
        Acm_WssInitData.ACH_Tx36_PTEN                       = AcmWssInitWriteData.Acm_Tx36_PTEN;
        Acm_WssInitData.ACH_Tx36_SS_DIS                     = AcmWssInitWriteData.Acm_Tx36_SS_DIS;
        Acm_WssInitData.ACH_Tx36_SENSOR_TYPE_SELECTION      = AcmWssInitWriteData.Acm_Tx36_SENSOR_TYPE_SELECTION;
        Acm_WssInitData.ACH_Tx36_FILTER_SELECTION           = AcmWssInitWriteData.Acm_Tx36_FILTER_SELECTION;    /* by guide line*/
        Acm_WssInitData.ACH_Tx36_FIX_TH                     = AcmWssInitWriteData.Acm_Tx36_FIX_TH;              /* by guide line*/
            
        /* WSS CH2 */
        Acm_WssInitData.ACH_Tx37_PTEN                       = AcmWssInitWriteData.Acm_Tx37_PTEN;
        Acm_WssInitData.ACH_Tx37_SS_DIS                     = AcmWssInitWriteData.Acm_Tx37_SS_DIS;
        Acm_WssInitData.ACH_Tx37_SENSOR_TYPE_SELECTION      = AcmWssInitWriteData.Acm_Tx37_SENSOR_TYPE_SELECTION;
        Acm_WssInitData.ACH_Tx37_FILTER_SELECTION           = AcmWssInitWriteData.Acm_Tx37_FILTER_SELECTION;    /* by guide line*/
        Acm_WssInitData.ACH_Tx37_FIX_TH                     = AcmWssInitWriteData.Acm_Tx37_FIX_TH;              /* by guide line*/
            
        /* WSS CH3 */
        Acm_WssInitData.ACH_Tx38_PTEN                       = AcmWssInitWriteData.Acm_Tx38_PTEN;
        Acm_WssInitData.ACH_Tx38_SS_DIS                     = AcmWssInitWriteData.Acm_Tx38_SS_DIS;
        Acm_WssInitData.ACH_Tx38_SENSOR_TYPE_SELECTION      = AcmWssInitWriteData.Acm_Tx38_SENSOR_TYPE_SELECTION;
        Acm_WssInitData.ACH_Tx38_FILTER_SELECTION           = AcmWssInitWriteData.Acm_Tx38_FILTER_SELECTION;        /* by guide line*/
        Acm_WssInitData.ACH_Tx38_FIX_TH                     = AcmWssInitWriteData.Acm_Tx38_FIX_TH;                  /* by guide line*/
            
        Acm_WssInitData.ACH_Tx39_FVPWR_ACT                  = AcmWssInitWriteData.Acm_Tx39_FVPWR_ACT;               /* by guide line */
        Acm_WssInitData.ACH_Tx39_WSI_VDD4_UV                = AcmWssInitWriteData.Acm_Tx39_WSI_VDD4_UV;             /* by guide line */
        Acm_WssInitData.ACH_Tx39_INIT                       = AcmWssInitWriteData.Acm_Tx39_INIT;                    /* by guide line */
        Acm_WssInitData.ACH_Tx39_DIAG                       = AcmWssInitWriteData.Acm_Tx39_DIAG;                    /* by guide line */
        Acm_WssInitData.ACH_Tx39_CH4_EN                     = AcmWssInitWriteData.Acm_Tx39_CH4_EN;                  /* by guide line */
        Acm_WssInitData.ACH_Tx39_CH3_EN                     = AcmWssInitWriteData.Acm_Tx39_CH3_EN;                  /* by guide line */
        Acm_WssInitData.ACH_Tx39_CH2_EN                     = AcmWssInitWriteData.Acm_Tx39_CH2_EN;                  /* by guide line */
        Acm_WssInitData.ACH_Tx39_CH1_EN                     = AcmWssInitWriteData.Acm_Tx39_CH1_EN;                  /* by guide line */
        
        Acm_WssInitData.ACH_Tx44_WSI1_LBIST_EN              = AcmWssInitWriteData.Acm_Tx44_WSI1_LBIST_EN;
        Acm_WssInitData.ACH_Tx44_WSI2_LBIST_EN              = AcmWssInitWriteData.Acm_Tx44_WSI2_LBIST_EN;
        Acm_WssInitData.ACH_Tx44_WSI3_LBIST_EN              = AcmWssInitWriteData.Acm_Tx44_WSI3_LBIST_EN;
        Acm_WssInitData.ACH_Tx44_WSI4_LBIST_EN              = AcmWssInitWriteData.Acm_Tx44_WSI4_LBIST_EN;
    }
}

static void Acm_SetLampShlsInitData(void)
{
    if(Acm_InitList.LampShlsInit == Acm_Used)  
    {
        Acm_LampShlsInitData.ACH_Tx17_WLD_CONF              = AcmLampShlsInitWriteData.Acm_Tx17_WLD_CONF;         /* by guide line */
        Acm_LampShlsInitData.ACH_Tx17_WLD_CMD               = AcmLampShlsInitWriteData.Acm_Tx17_WLD_CMD;
        Acm_LampShlsInitData.ACH_Tx17_WLD_DIAGOFF_EN        = AcmLampShlsInitWriteData.Acm_Tx17_WLD_DIAGOFF_EN;          /* by guide line */
        Acm_LampShlsInitData.ACH_Tx17_WLD_LS_CLAMP_DIS      = AcmLampShlsInitWriteData.Acm_Tx17_WLD_LS_CLAMP_DIS;         /* by guide line */
        Acm_LampShlsInitData.ACH_Tx17_SHLS_CMD              = AcmLampShlsInitWriteData.Acm_Tx17_SHLS_CMD;
        Acm_LampShlsInitData.ACH_Tx17_SHLS_DIAGOFF_EN       = AcmLampShlsInitWriteData.Acm_Tx17_SHLS_DIAGOFF_EN;          /* by guide line */
        Acm_LampShlsInitData.ACH_Tx17_SHLS_CONFIG           = AcmLampShlsInitWriteData.Acm_Tx17_SHLS_CONFIG;                   /* TODO, It will be changed by configuration */
        Acm_LampShlsInitData.ACH_Tx17_SHLS_LS_CLAMP_DIS     = AcmLampShlsInitWriteData.Acm_Tx17_SHLS_LS_CLAMP_DIS;         /* by guide line */
    }
}


static void Acm_CheckAsicInitState(void)
{
    #if 1   /* BECK_TEST TEMP*/
    if(ModuleInitStateType.Acm_AsicInitState == 0)
    {
        Acm_CheckAsicInitData();
    }
    if(ModuleInitStateType.Acm_ValveInitState == 0)
    {
        Acm_CheckValveInitData();
    }
    if(ModuleInitStateType.Acm_MotorInitState == 0)
    {
        Acm_CheckMotorInitData();
    }
    if(ModuleInitStateType.Acm_VsoInitState == 0)
    {
        Acm_CheckVsoInitData();
    }
    if(ModuleInitStateType.Acm_WssInitState == 0)
    {
        Acm_CheckWssInitData();
    }
    if(ModuleInitStateType.Acm_LampShlsInitState == 0)
    {
        Acm_CheckLampShlsInitData();
    }

    if(ModuleInitStateType.Acm_AsicInitState && ModuleInitStateType.Acm_LampShlsInitState && ModuleInitStateType.Acm_MotorInitState  \
        && ModuleInitStateType.Acm_ValveInitState && ModuleInitStateType.Acm_VsoInitState && ModuleInitStateType.Acm_WssInitState)
    {
        Acm_AsicInitStatus = Acm_AsicInitComplete; /* TODO Temp */
        Acm_MainBus.Acm_MainAcmAsicInitCompleteFlag = 1;
    }
    #else
        Acm_AsicInitStatus = Acm_AsicInitComplete; /* TODO Temp */
        Acm_MainBus.Acm_MainAcmAsicInitCompleteFlag = 1;
    
    #endif
}

static void Acm_CheckAsicInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;
    
    if(Acm_InitList.AsicInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_1mu1_Rx_1_FS_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_FS_EN);
        
        ACH_GetSigData_CS(ASIC_RX_MSG_1mu1_Rx_1_FS_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_FS_CMD);
        
        ACH_GetSigData_CS(ASIC_RX_MSG_1mu2_Rx_1_FS_VDS_TH, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_FS_VDS_TH);

        ACH_GetSigData_CS(ASIC_RX_MSG_1mu1_Rx_1_PMP_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_PMP_EN);

        ACH_GetSigData_CS(ASIC_RX_MSG_1mu1_Rx_1_OSC_SpreadSpectrum_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_OSC_SprSpect_DIS);

        ACH_GetSigData_CS(ASIC_RX_MSG_1mu2_Rx_1_VDD_4_OVC_SD_RESTART_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_VDD_4_OVC_SD_RESTART_TIME);

        ACH_GetSigData_CS(ASIC_RX_MSG_1mu2_Rx_1_VDD4_OVC_FLT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_VDD4_OVC_FLT_TIME);

        ACH_GetSigData_CS(ASIC_RX_MSG_1mu2_Rx_1_VDD_3_OVC_SD_RESTART_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_VDD_3_OVC_SD_RESTART_TIME);

        ACH_GetSigData_CS(ASIC_RX_MSG_1mu2_Rx_1_VDD3_OVC_FLT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx1_VDD3_OVC_FLT_TIME);

        ACH_GetSigData_CS(ASIC_RX_MSG_2mu2_Rx_2_WD_SW_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx_2_WD_SW_DIS);

        ACH_GetSigData_CS(ASIC_RX_MSG_2mu1_Rx_2_VDD_2d5_AUTO_SWITCH_OFF, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx2_VDD_2d5_AUTO_SWITCH_OFF);

        ACH_GetSigData_CS(ASIC_RX_MSG_2mu1_Rx_2_VDD1_DIODELOSS_FILT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx2_VDD1_DIODELOSS_FILT);

        ACH_GetSigData_CS(ASIC_RX_MSG_2mu1_Rx_2_WD_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx_2_WD_DIS);

        ACH_GetSigData_CS(ASIC_RX_MSG_2mu1_Rx_2_VDD5_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx2_VDD5_DIS);

        ACH_GetSigData_CS(ASIC_RX_MSG_2mu1_Rx_2_VDD2_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx2_VDD2_DIS);

        ACH_GetSigData_CS(ASIC_RX_MSG_19mu1_Rx_19_E_PVDS_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx19_E_PVDS_GPO);

        ACH_GetSigData_CS(ASIC_RX_MSG_19mu1_Rx_19_E_VDD2d5dWSx_OT_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx19_E_VDD2d5dWSx_OT_GPO);

        ACH_GetSigData_CS(ASIC_RX_MSG_19mu1_Rx_19_E_FBO_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx19_E_FBO_GPO);

        ACH_GetSigData_CS(ASIC_RX_MSG_19mu1_Rx_19_E_FSPI_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx19_E_FSPI_GPO);

        ACH_GetSigData_CS(ASIC_RX_MSG_19mu1_Rx_19_E_EN_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx19_E_EN_GPO);

        ACH_GetSigData_CS(ASIC_RX_MSG_19mu1_Rx_19_E_FS_VDS_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_AsicInitData.ACH_Tx19_E_FS_VDS_GPO);
    }
    if(err == 0)
    {
        ModuleInitStateType.Acm_AsicInitState = 1;
    }
}

static void Acm_CheckValveInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;    

    if(Acm_InitList.Nc0ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH1_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH1_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH1_TD_BLANK, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH1_TD_BLANK);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH1_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH1_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH1_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH1_CMD);
    }
    if(Acm_InitList.Nc1ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH2_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH2_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH_2TD_BLANK, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH2_TD_BLANK);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH2_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH2_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH2_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH2_CMD);   
    }
    if(Acm_InitList.Nc2ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH3_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH3_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH3_TD_BLANK, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH3_TD_BLANK);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH3_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH3_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH3_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH3_CMD);
    }
    if(Acm_InitList.Nc3ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH4_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH4_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH4_TD_BLANK, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH4_TD_BLANK);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH4_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH4_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_67mu1_Rx_67_CH4_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx67_CH4_CMD);
    }
    
    if(Acm_InitList.No0ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_0, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_0);    
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH0_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH0_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH0_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH0_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu2_Rx_NT2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_2mu1_Rx_NT2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_6mu10_Rx_NT6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_7mu8_Rx_NT7_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_7_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_7mu7_Rx_NT7_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_7_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_8mu3_Rx_NT8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_8mu3_Rx_NT8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_9mu2_Rx_NT9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_9mu2_Rx_NT9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_9mu1_Rx_NT9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_9_CHOP_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO0_13mu8_Rx_NT13_BASE_DELTA_CURENT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO0_13_BASE_DELTA_CURENT);
    }
    
    if(Acm_InitList.No1ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_1, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_1);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH1_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH1_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH1_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH1_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu2_Rx_NT2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_2mu1_Rx_NT2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_6mu10_Rx_NT6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_7mu8_Rx_NT7_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_7_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_7mu7_Rx_NT7_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_7_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_8mu3_Rx_NT8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_8mu3_Rx_NT8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_9mu2_Rx_NT9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_9mu2_Rx_NT9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_9mu1_Rx_NT9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_9_CHOP_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO1_13mu8_Rx_NT13_BASE_DELTA_CURENT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO1_13_BASE_DELTA_CURENT);
    }
    
    if(Acm_InitList.No2ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_2, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_2);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH2_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH2_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH2_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH2_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu2_Rx_NT2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_2mu1_Rx_NT2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_6mu10_Rx_NT6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_7mu8_Rx_NT7_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_7_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_7mu7_Rx_NT7_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_7_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_8mu3_Rx_NT8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_8mu3_Rx_NT8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_9mu2_Rx_NT9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_9mu2_Rx_NT9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_9mu1_Rx_NT9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_9_CHOP_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO2_13mu8_Rx_NT13_BASE_DELTA_CURENT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO2_13_BASE_DELTA_CURENT);
    }
    
    if(Acm_InitList.No3ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_3, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_3);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH3_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH3_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH3_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH3_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu2_Rx_NT2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_2mu1_Rx_NT2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_6mu10_Rx_NT6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_7mu8_Rx_NT7_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_7_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_7mu7_Rx_NT7_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_7_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_8mu3_Rx_NT8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_8mu3_Rx_NT8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_9mu2_Rx_NT9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_9mu2_Rx_NT9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_9mu1_Rx_NT9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_9_CHOP_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_NO3_13mu8_Rx_NT13_BASE_DELTA_CURENT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxNO3_13_BASE_DELTA_CURENT);
    }
    
    if(Acm_InitList.Tc0ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_4, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_4);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH4_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH4_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH4_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH4_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu2_Rx_NT2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_2mu1_Rx_NT2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_6mu10_Rx_NT6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_7mu8_Rx_NT7_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_7_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_7mu7_Rx_NT7_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_7_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_8mu3_Rx_NT8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_8mu3_Rx_NT8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_9mu2_Rx_NT9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_9mu2_Rx_NT9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_9mu1_Rx_NT9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_9_CHOP_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC0_13mu8_Rx_NT13_BASE_DELTA_CURENT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC0_13_BASE_DELTA_CURENT);
    }
    
    if(Acm_InitList.Tc1ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_5, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_5);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH5_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH5_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH5_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH5_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu2_Rx_NT2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_2mu1_Rx_NT2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_6mu10_Rx_NT6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_7mu8_Rx_NT7_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_7_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_7mu7_Rx_NT7_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_7_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_8mu3_Rx_NT8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_8mu3_Rx_NT8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_9mu2_Rx_NT9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_9mu2_Rx_NT9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_9mu1_Rx_NT9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_9_CHOP_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_TC1_13mu8_Rx_NT13_BASE_DELTA_CURENT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxTC1_13_BASE_DELTA_CURENT);
    }
    
    if(Acm_InitList.Esv0ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_6, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_6);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH6_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH6_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH6_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH6_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu2_Rx_ESV2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_2mu1_Rx_ESV2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_6mu6_Rx_ESV6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_6mu4_Rx_ESV6_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_6_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_6mu5_Rx_ESV6_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_6_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_8mu3_Rx_ESV8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_8mu3_Rx_ESV8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_9mu2_Rx_ESV9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_9mu2_Rx_ESV9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV0_9mu1_Rx_ESV9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV0_9_CHOP_TIME);
    }
    
    if(Acm_InitList.Esv1ValveInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_64mu1_Rx_64_SOLENOID_ENABLE_7, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx64_SOLENOID_ENABLE_7);    
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH7_STATUS_L, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH7_STATUS_L);
        ACH_GetSigData_CS(ASIC_RX_MSG_65mu1_Rx_65_CH7_STATUS_H, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_Tx65_CH7_STATUS_H);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu2_Rx_ESV2_SR_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_SR_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_CALIBRATION_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_CALIBRATION_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_OFS_CHOP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_OFS_CHOP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_TD_BLANK_SEL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_TD_BLANK_SEL);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_LOGIC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_LOGIC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_DIAG_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_DIAG_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_ADC_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_ADC_BIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_OFF_DIAG_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_OFF_DIAG_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_E_SOL_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_E_SOL_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_2mu1_Rx_ESV2_E_LSCLAMP_GPO, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_2_E_LSCLAMP_GPO);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_6mu6_Rx_ESV6_PWM_CODE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_6_PWM_CODE);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_6mu4_Rx_ESV6_FREQ_MOD_STEP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_6_FREQ_MOD_STEP);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_6mu5_Rx_ESV6_MAX_FREQ_DELTA, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_6_MAX_FREQ_DELTA);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_8mu3_Rx_ESV8_KP, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_8_KP);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_8mu3_Rx_ESV8_KI, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_8_KI);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_9mu2_Rx_ESV9_INTTIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_9_INTTIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_9mu2_Rx_ESV9_FILT_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_9_FILT_TIME);
        ACH_GetSigData_CS(ASIC_RX_MSG_ESV1_9mu1_Rx_ESV9_CHOP_TIME, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_ValveInitData.ACH_TxESV1_9_CHOP_TIME);
    }
    if(err == 0)
    {
        ModuleInitStateType.Acm_ValveInitState = 1;
    }

    
}

static void Acm_CheckMotorInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;
    
    if(Acm_InitList.MotorInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_8mu8_Rx_8_PUMP_TCK_PWM, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx8_PMP_TCK_PWM);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu2_Rx_9_PMP_VDS_TH, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP_VDS_TH);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu1_Rx_9_PMP2_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP2_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu3_Rx_9_PMP_VDS_FIL, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP_VDS_FIL);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu1_Rx_9_LDACT_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_LDACT_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu1_Rx_9_PMP1_ISINC, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP1_ISINC);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu4_Rx_9_PMP_DT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP_DT);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu1_Rx_9_PMP_TEST, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP_TEST);
        ACH_GetSigData_CS(ASIC_RX_MSG_9mu1_Rx_9_PMP_BIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_MotorInitData.ACH_Tx9_PMP_BIST_EN);
    } 
    if(err == 0)
    {
        ModuleInitStateType.Acm_MotorInitState = 1;
    }

}

static void Acm_CheckVsoInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;
    
    if(Acm_InitList.VsoInit == Acm_Used)  
    {
        /* TODO */
    }
    if(err == 0)
    {
        ModuleInitStateType.Acm_VsoInitState = 1;
    }
}

static void Acm_CheckWssInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;
    
    if(Acm_InitList.WssInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_32mu7_Rx_32_CONFIG_RANGE, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx32_CONFIG_RANGE);
        ACH_GetSigData_CS(ASIC_RX_MSG_32mu1_Rx_32_WSO_TEST, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx32_WSO_TEST);
        ACH_GetSigData_CS(ASIC_RX_MSG_33mu8_Rx_33_WSI_FIRST_THRESHOLD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx33_WSI_FIRST_THRESHOLD);
        ACH_GetSigData_CS(ASIC_RX_MSG_34mu8_Rx_34_WSI_OFFSET_THRESHOLD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx34_WSI_OFFSET_THRESHOLD);

        ACH_GetSigData_CS(ASIC_RX_MSG_35mu1_Rx_35_PTEN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx35_PTEN);
        ACH_GetSigData_CS(ASIC_RX_MSG_35mu1_Rx_35_SS_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx35_SS_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_35mu2_Rx_35_SENSOR_TYPE_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx35_SENSOR_TYPE_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_35mu2_Rx_35_FILTER_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx35_FILTER_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_35mu1_Rx_35_FIX_TH, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx35_FIX_TH);

        ACH_GetSigData_CS(ASIC_RX_MSG_36mu1_Rx_36_PTEN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx36_PTEN);
        ACH_GetSigData_CS(ASIC_RX_MSG_36mu1_Rx_36_SS_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx36_SS_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_36mu2_Rx_36_SENSOR_TYPE_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx36_SENSOR_TYPE_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_36mu2_Rx_36_FILTER_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx36_FILTER_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_36mu1_Rx_36_FIX_TH, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx36_FIX_TH);

        ACH_GetSigData_CS(ASIC_RX_MSG_37mu1_Rx_37_PTEN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx37_PTEN);
        ACH_GetSigData_CS(ASIC_RX_MSG_37mu1_Rx_37_SS_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx37_SS_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_37mu2_Rx_37_SENSOR_TYPE_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx37_SENSOR_TYPE_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_37mu2_Rx_37_FILTER_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx37_FILTER_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_37mu1_Rx_37_FIX_TH, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx37_FIX_TH);

        ACH_GetSigData_CS(ASIC_RX_MSG_38mu1_Rx_38_PTEN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx38_PTEN);
        ACH_GetSigData_CS(ASIC_RX_MSG_38mu1_Rx_38_SS_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx38_SS_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_38mu2_Rx_38_SENSOR_TYPE_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx38_SENSOR_TYPE_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_38mu2_Rx_38_FILTER_SELECTION, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx38_FILTER_SELECTION);
        ACH_GetSigData_CS(ASIC_RX_MSG_38mu1_Rx_38_FIX_TH, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx38_FIX_TH);

        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_FVPWR_ACT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_FVPWR_ACT);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_WSI_VDD4_UV, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_WSI_VDD4_UV);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_INIT, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_INIT);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_DIAG, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_DIAG);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_CH4_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_CH4_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_CH3_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_CH3_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_CH2_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_CH2_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_39mu1_Rx_39_CH1_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx39_CH1_EN);
            
        ACH_GetSigData_CS(ASIC_RX_MSG_44mu1_Rx_44_WSI1_LBIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx44_WSI1_LBIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_44mu1_Rx_44_WSI2_LBIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx44_WSI2_LBIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_44mu1_Rx_44_WSI3_LBIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx44_WSI3_LBIST_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_44mu1_Rx_44_WSI4_LBIST_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_WssInitData.ACH_Tx44_WSI4_LBIST_EN);
    }
    if(err == 0)
    {
        ModuleInitStateType.Acm_WssInitState = 1;
    }
}

static void Acm_CheckLampShlsInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;

    if(Acm_InitList.LampShlsInit == Acm_Used)  
    {
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_WLD_CONF, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_WLD_CONF);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_WLD_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_WLD_CMD);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_WLD_DIAGOFF_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_WLD_DIAGOFF_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_WLD_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_WLD_LS_CLAMP_DIS);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_SHLS_CMD, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_SHLS_CMD);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_SHLS_DIAGOFF_EN, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_SHLS_DIAGOFF_EN);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_SHLS_CONFIG, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_SHLS_CONFIG);
        ACH_GetSigData_CS(ASIC_RX_MSG_17mu1_Rx_17_SHLS_LS_CLAMP_DIS, &DataTemp);
        err |= Acm_CompareData(DataTemp, Acm_LampShlsInitData.ACH_Tx17_SHLS_LS_CLAMP_DIS);
    }
    if(err == 0)
    {
        ModuleInitStateType.Acm_LampShlsInitState = 1;
    }
}

static uint8 Acm_CompareData(uint16 Data1, uint16 Data2)
{
    uint8 rtn = 0;

    if(Data1 == Data2)
    {
        rtn = 0;
    }
    else
    {
        rtn = 1;
    }
    
    return rtn;
}

#define ACM_MAIN_STOP_SEC_CODE
#include "Acm_MemMap.h"

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

