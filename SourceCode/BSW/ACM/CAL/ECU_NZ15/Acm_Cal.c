/**
 * @defgroup Acm_Cal Acm_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACM_START_SEC_CALIB_UNSPECIFIED
#include "Acm_MemMap.h"
/* Global Calibration Section */


#define ACM_STOP_SEC_CALIB_UNSPECIFIED
#include "Acm_MemMap.h"

#define ACM_START_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACM_STOP_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACM_START_SEC_CODE
#include "Acm_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACM_STOP_SEC_CODE
#include "Acm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
