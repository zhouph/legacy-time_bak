/**
 * @defgroup Acm_Cfg Acm_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACM_CFG_H_
#define ACM_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

 
/************************************** WHEEL SPEED SENSOR *****************************************/

/*Wheel Sensor Port Set*/
#define WFR             0
#define WFL             1
#define WRR             2
#define WRL             3

/* WHEEL SPEED SENSOR */
#define WSS_PASSIVE     0
#define WSS_ACTIVE      1
#define WSS_SMART       2
#define WSS_VDA         3

#define WSS_CH0                     0
#define WSS_CH1                     1
#define WSS_CH2                     2
#define WSS_CH3                     3
 
/* Wheel Sensor Port Setting */
#define CH0_PORT                    WFL         
#define CH1_PORT                    WFR
#define CH2_PORT                    WRL
#define CH3_PORT                    WRR

/* Wheel Sensor Type Setting */
#define FL_WSS_TYPE                 WSS_ACTIVE   /* WSS_PASSIVE or WSS_ACTIVE or WSS_SMART or WSS_VDA */      
#define FR_WSS_TYPE                 WSS_ACTIVE   /* WSS_PASSIVE or WSS_ACTIVE or WSS_SMART or WSS_VDA */
#define RL_WSS_TYPE                 WSS_ACTIVE   /* WSS_PASSIVE or WSS_ACTIVE or WSS_SMART or WSS_VDA */
#define RR_WSS_TYPE                 WSS_ACTIVE   /* WSS_PASSIVE or WSS_ACTIVE or WSS_SMART or WSS_VDA */

#if (CH0_PORT == WFL)
    #define WFL_INPUT_CH                    WSS_CH0
    #if (FL_WSS_TYPE == WSS_ACTIVE)
        #define CH0_WSS_TYPE                WSS_ACTIVE
    #elif (FL_WSS_TYPE == WSS_SMART)
        #define CH0_WSS_TYPE                WSS_SMART
    #elif (FL_WSS_TYPE == WSS_VDA)
        #define CH0_WSS_TYPE                WSS_VDA
    #endif
#elif (CH0_PORT == WFR)
    #define WFR_INPUT_CH                    WSS_CH0
    #if (FR_WSS_TYPE == WSS_ACTIVE)
        #define CH0_WSS_TYPE                WSS_ACTIVE
    #elif (FR_WSS_TYPE == WSS_SMART)
        #define CH0_WSS_TYPE                WSS_SMART
    #elif (FR_WSS_TYPE == WSS_VDA)
        #define CH0_WSS_TYPE                WSS_VDA
    #endif
#elif (CH0_PORT == WRL)
    #define WRL_INPUT_CH                    WSS_CH0
    #if (RL_WSS_TYPE == WSS_ACTIVE)
        #define CH0_WSS_TYPE                WSS_ACTIVE
    #elif (RL_WSS_TYPE == WSS_SMART)
        #define CH0_WSS_TYPE                WSS_SMART
    #elif (RL_WSS_TYPE == WSS_VDA)
        #define CH0_WSS_TYPE                WSS_VDA
    #endif
#elif (CH0_PORT == WRR)
    #define WRR_INPUT_CH                    WSS_CH0
    #if (RR_WSS_TYPE == WSS_ACTIVE)
        #define CH0_WSS_TYPE                WSS_ACTIVE
    #elif (RR_WSS_TYPE == WSS_SMART)
        #define CH0_WSS_TYPE                WSS_SMART
    #elif (RR_WSS_TYPE == WSS_VDA)
        #define CH0_WSS_TYPE                WSS_VDA
    #endif
#else
    #error "CH0 Wheel Channel Selection Error" 
#endif
    
#if (CH1_PORT == WFL)
    #define WFL_INPUT_CH                    WSS_CH1
    #if (FL_WSS_TYPE == WSS_ACTIVE)
        #define CH1_WSS_TYPE                WSS_ACTIVE
    #elif (FL_WSS_TYPE == WSS_SMART)
        #define CH1_WSS_TYPE                WSS_SMART
    #elif (FL_WSS_TYPE == WSS_VDA)
        #define CH1_WSS_TYPE                WSS_VDA
    #endif
#elif (CH1_PORT == WFR)
    #define WFR_INPUT_CH                    WSS_CH1
    #if (FR_WSS_TYPE == WSS_ACTIVE)
        #define CH1_WSS_TYPE                WSS_ACTIVE
    #elif (FR_WSS_TYPE == WSS_SMART)
        #define CH1_WSS_TYPE                WSS_SMART
    #elif (FR_WSS_TYPE == WSS_VDA)
        #define CH1_WSS_TYPE                WSS_VDA
    #endif
#elif (CH1_PORT == WRL)
    #define WRL_INPUT_CH                    WSS_CH1
    #if (RL_WSS_TYPE == WSS_ACTIVE)
        #define CH1_WSS_TYPE                WSS_ACTIVE
    #elif (RL_WSS_TYPE == WSS_SMART)
        #define CH1_WSS_TYPE                WSS_SMART
    #elif (RL_WSS_TYPE == WSS_VDA)
        #define CH1_WSS_TYPE                WSS_VDA
    #endif
#elif (CH1_PORT == WRR)
    #define WRR_INPUT_CH                    WSS_CH1
    #if (RR_WSS_TYPE == WSS_ACTIVE)
        #define CH1_WSS_TYPE                WSS_ACTIVE
    #elif (RR_WSS_TYPE == WSS_SMART)
        #define CH1_WSS_TYPE                WSS_SMART
    #elif (RR_WSS_TYPE == WSS_VDA)
        #define CH1_WSS_TYPE                WSS_VDA
    #endif
#else
    #error "CH1 Wheel Channel Selection Error" 
#endif
    
#if (CH2_PORT == WFL)
    #define WFL_INPUT_CH                    WSS_CH2
    #if (FL_WSS_TYPE == WSS_ACTIVE)
        #define CH2_WSS_TYPE                WSS_ACTIVE
    #elif (FL_WSS_TYPE == WSS_SMART)
        #define CH2_WSS_TYPE                WSS_SMART
    #elif (FL_WSS_TYPE == WSS_VDA)
        #define CH2_WSS_TYPE                WSS_VDA
    #endif
#elif (CH2_PORT == WFR)
    #define WFR_INPUT_CH                    WSS_CH2
    #if (FR_WSS_TYPE == WSS_ACTIVE)
        #define CH2_WSS_TYPE                WSS_ACTIVE
    #elif (FR_WSS_TYPE == WSS_SMART)
        #define CH2_WSS_TYPE                WSS_SMART
    #elif (FR_WSS_TYPE == WSS_VDA)
        #define CH2_WSS_TYPE                WSS_VDA
    #endif
#elif (CH2_PORT == WRL)
    #define WRL_INPUT_CH                    WSS_CH2
    #if (RL_WSS_TYPE == WSS_ACTIVE)
        #define CH2_WSS_TYPE                WSS_ACTIVE
    #elif (RL_WSS_TYPE == WSS_SMART)
        #define CH2_WSS_TYPE                WSS_SMART
    #elif (RL_WSS_TYPE == WSS_VDA)
        #define CH2_WSS_TYPE                WSS_VDA
    #endif
#elif (CH2_PORT == WRR)
    #define WRR_INPUT_CH                    WSS_CH2
    #if (RR_WSS_TYPE == WSS_ACTIVE)
        #define CH2_WSS_TYPE                WSS_ACTIVE
    #elif (RR_WSS_TYPE == WSS_SMART)
        #define CH2_WSS_TYPE                WSS_SMART
    #elif (RR_WSS_TYPE == WSS_VDA)
        #define CH2_WSS_TYPE                WSS_VDA
    #endif
#else
    #error "CH2 Wheel Channel Selection Error" 
#endif
    
#if (CH3_PORT == WFL)
    #define WFL_INPUT_CH                    WSS_CH3
    #if (FL_WSS_TYPE == WSS_ACTIVE)
        #define CH3_WSS_TYPE                WSS_ACTIVE
    #elif (FL_WSS_TYPE == WSS_SMART)
        #define CH3_WSS_TYPE                WSS_SMART
    #elif (FL_WSS_TYPE == WSS_VDA)
        #define CH3_WSS_TYPE                WSS_VDA
    #endif
#elif (CH3_PORT == WFR)
    #define WFR_INPUT_CH                    WSS_CH3
    #if (FR_WSS_TYPE == WSS_ACTIVE)
        #define CH3_WSS_TYPE                WSS_ACTIVE
    #elif (FR_WSS_TYPE == WSS_SMART)
        #define CH3_WSS_TYPE                WSS_SMART
    #elif (FR_WSS_TYPE == WSS_VDA)
        #define CH3_WSS_TYPE                WSS_VDA
    #endif
#elif (CH3_PORT == WRL)
    #define WRL_INPUT_CH                    WSS_CH3
    #if (RL_WSS_TYPE == WSS_ACTIVE)
        #define CH3_WSS_TYPE                WSS_ACTIVE
    #elif (RL_WSS_TYPE == WSS_SMART)
        #define CH3_WSS_TYPE                WSS_SMART
    #elif (RL_WSS_TYPE == WSS_VDA)
        #define CH3_WSS_TYPE                WSS_VDA
    #endif
#elif (CH3_PORT == WRR)
    #define WRR_INPUT_CH                    WSS_CH3
    #if (RR_WSS_TYPE == WSS_ACTIVE)
        #define CH3_WSS_TYPE                WSS_ACTIVE
    #elif (RR_WSS_TYPE == WSS_SMART)
        #define CH3_WSS_TYPE                WSS_SMART
    #elif (RR_WSS_TYPE == WSS_VDA)
        #define CH3_WSS_TYPE                WSS_VDA
    #endif
#else
    #error "CH3 Wheel Channel Selection Error" 
#endif

#define LOOP_TIME   (5)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef enum
{
    Acm_Used = 0,
    Acm_NotUsed
}InitUsedType;

typedef struct
{
    InitUsedType AsicInit;
    InitUsedType No0ValveInit;
    InitUsedType No1ValveInit;
    InitUsedType No2ValveInit;
    InitUsedType No3ValveInit;
    InitUsedType Nc0ValveInit;
    InitUsedType Nc1ValveInit;
    InitUsedType Nc2ValveInit;
    InitUsedType Nc3ValveInit;
    InitUsedType Tc0ValveInit;
    InitUsedType Tc1ValveInit;
    InitUsedType Esv0ValveInit;
    InitUsedType Esv1ValveInit;
    InitUsedType MotorInit;
    InitUsedType VsoInit;
    InitUsedType WssInit;
    InitUsedType LampShlsInit;
}Acm_InitListType;

typedef struct
{
    uint8 Acm_Tx1_FS_VDS_TH;
    uint8 Acm_Tx1_FS_CMD;
    uint8 Acm_Tx1_FS_EN;
    uint8 Acm_Tx1_PMP_EN;
    uint8 Acm_Tx1_OSC_SprSpect_DIS;
    uint8 Acm_Tx1_VDD_4_OVC_SD_RESTART_TIME;
    uint8 Acm_Tx1_VDD4_OVC_FLT_TIME;
    uint8 Acm_Tx1_VDD_3_OVC_SD_RESTART_TIME;
    uint8 Acm_Tx1_VDD3_OVC_FLT_TIME;
    uint8 Acm_Tx2_WD_SW_DIS;
    uint8 Acm_Tx2_VDD_2d5_AUTO_SWITCH_OFF;
    uint8 Acm_Tx2_VDD1_DIODELOSS_FILT;
    uint8 Acm_Tx2_WD_DIS;
    uint8 Acm_Tx2_VDD5_DIS;
    uint8 Acm_Tx2_VDD2_DIS;
    uint8 Acm_Tx19_E_PVDS_GPO;
    uint8 Acm_Tx19_E_VDD2d5dWSx_OT_GPO;
    uint8 Acm_Tx19_E_FBO_GPO;
    uint8 Acm_Tx19_E_FSPI_GPO;
    uint8 Acm_Tx19_E_EN_GPO;
    uint8 Acm_Tx19_E_FS_VDS_GPO;
}AcmAsicInitWriteDataType;

typedef struct
{
    uint8 Acm_Tx67_CH1_EN;
    uint8 Acm_Tx67_CH1_DIAG_EN;
    uint8 Acm_Tx67_CH1_TD_BLANK;
    uint8 Acm_Tx67_CH1_CMD;
    uint8 Acm_Tx67_CH2_EN;
    uint8 Acm_Tx67_CH2_DIAG_EN;
    uint8 Acm_Tx67_CH2_TD_BLANK;
    uint8 Acm_Tx67_CH2_CMD;
    uint8 Acm_Tx67_CH3_EN;
    uint8 Acm_Tx67_CH3_DIAG_EN;
    uint8 Acm_Tx67_CH3_TD_BLANK;
    uint8 Acm_Tx67_CH3_CMD;
    uint8 Acm_Tx67_CH4_EN;
    uint8 Acm_Tx67_CH4_DIAG_EN;
    uint8 Acm_Tx67_CH4_TD_BLANK;
    uint8 Acm_Tx67_CH4_CMD;
    uint8 Acm_Tx64_SOLENOID_ENABLE_0;
    uint8 Acm_Tx65_CH0_STATUS_L;
    uint8 Acm_Tx65_CH0_STATUS_H;
    uint8 Acm_TxNO0_2_SR_SEL;
    uint8 Acm_TxNO0_2_CALIBRATION_DIS;
    uint8 Acm_TxNO0_2_OFS_CHOP_DIS;
    uint8 Acm_TxNO0_2_TD_BLANK_SEL;
    uint8 Acm_TxNO0_2_LS_CLAMP_DIS;
    uint8 Acm_TxNO0_2_LOGIC_BIST_EN;
    uint8 Acm_TxNO0_2_DIAG_BIST_EN;
    uint8 Acm_TxNO0_2_ADC_BIST_EN;
    uint8 Acm_TxNO0_2_OFF_DIAG_EN;
    uint8 Acm_TxNO0_2_E_SOL_GPO;
    uint8 Acm_TxNO0_2_E_LSCLAMP_GPO;
    uint16 Acm_TxNO0_6_PWM_CODE;
    uint8 Acm_TxNO0_7_FREQ_MOD_STEP;
    uint8 Acm_TxNO0_7_MAX_FREQ_DELTA;
    uint8 Acm_TxNO0_8_KI;
    uint8 Acm_TxNO0_8_KP;
    uint8 Acm_TxNO0_9_INTTIME;
    uint8 Acm_TxNO0_9_FILT_TIME;
    uint8 Acm_TxNO0_9_CHOP_TIME;
    uint8 Acm_TxNO0_13_BASE_DELTA_CURENT;
    uint8 Acm_Tx64_SOLENOID_ENABLE_1;
    uint8 Acm_Tx65_CH1_STATUS_L;
    uint8 Acm_Tx65_CH1_STATUS_H;
    uint8 Acm_TxNO1_2_SR_SEL;
    uint8 Acm_TxNO1_2_CALIBRATION_DIS;
    uint8 Acm_TxNO1_2_OFS_CHOP_DIS;
    uint8 Acm_TxNO1_2_TD_BLANK_SEL;
    uint8 Acm_TxNO1_2_LS_CLAMP_DIS;
    uint8 Acm_TxNO1_2_LOGIC_BIST_EN;
    uint8 Acm_TxNO1_2_DIAG_BIST_EN;
    uint8 Acm_TxNO1_2_ADC_BIST_EN;
    uint8 Acm_TxNO1_2_OFF_DIAG_EN;
    uint8 Acm_TxNO1_2_E_SOL_GPO;
    uint8 Acm_TxNO1_2_E_LSCLAMP_GPO;
    uint16 Acm_TxNO1_6_PWM_CODE;
    uint8 Acm_TxNO1_7_FREQ_MOD_STEP;
    uint8 Acm_TxNO1_7_MAX_FREQ_DELTA;
    uint8 Acm_TxNO1_8_KI;
    uint8 Acm_TxNO1_8_KP;
    uint8 Acm_TxNO1_9_INTTIME;
    uint8 Acm_TxNO1_9_FILT_TIME;
    uint8 Acm_TxNO1_9_CHOP_TIME;
    uint8 Acm_TxNO1_13_BASE_DELTA_CURENT;
    uint8 Acm_Tx64_SOLENOID_ENABLE_2;
    uint8 Acm_Tx65_CH2_STATUS_L;
    uint8 Acm_Tx65_CH2_STATUS_H;
    uint8 Acm_TxNO2_2_SR_SEL;
    uint8 Acm_TxNO2_2_CALIBRATION_DIS;
    uint8 Acm_TxNO2_2_OFS_CHOP_DIS;
    uint8 Acm_TxNO2_2_TD_BLANK_SEL;
    uint8 Acm_TxNO2_2_LS_CLAMP_DIS;
    uint8 Acm_TxNO2_2_LOGIC_BIST_EN;
    uint8 Acm_TxNO2_2_DIAG_BIST_EN;
    uint8 Acm_TxNO2_2_ADC_BIST_EN;
    uint8 Acm_TxNO2_2_OFF_DIAG_EN;
    uint8 Acm_TxNO2_2_E_SOL_GPO;
    uint8 Acm_TxNO2_2_E_LSCLAMP_GPO;
    uint16 Acm_TxNO2_6_PWM_CODE;
    uint8 Acm_TxNO2_7_FREQ_MOD_STEP;
    uint8 Acm_TxNO2_7_MAX_FREQ_DELTA;
    uint8 Acm_TxNO2_8_KI;
    uint8 Acm_TxNO2_8_KP;
    uint8 Acm_TxNO2_9_INTTIME;
    uint8 Acm_TxNO2_9_FILT_TIME;
    uint8 Acm_TxNO2_9_CHOP_TIME;
    uint8 Acm_TxNO2_13_BASE_DELTA_CURENT;
    uint8 Acm_Tx64_SOLENOID_ENABLE_3;
    uint8 Acm_Tx65_CH3_STATUS_L;
    uint8 Acm_Tx65_CH3_STATUS_H;
    uint8 Acm_TxNO3_2_SR_SEL;
    uint8 Acm_TxNO3_2_CALIBRATION_DIS;
    uint8 Acm_TxNO3_2_OFS_CHOP_DIS;
    uint8 Acm_TxNO3_2_TD_BLANK_SEL;
    uint8 Acm_TxNO3_2_LS_CLAMP_DIS;
    uint8 Acm_TxNO3_2_LOGIC_BIST_EN;
    uint8 Acm_TxNO3_2_DIAG_BIST_EN;
    uint8 Acm_TxNO3_2_ADC_BIST_EN;
    uint8 Acm_TxNO3_2_OFF_DIAG_EN;
    uint8 Acm_TxNO3_2_E_SOL_GPO;
    uint8 Acm_TxNO3_2_E_LSCLAMP_GPO;
    uint16 Acm_TxNO3_6_PWM_CODE;
    uint8 Acm_TxNO3_7_FREQ_MOD_STEP;
    uint8 Acm_TxNO3_7_MAX_FREQ_DELTA;
    uint8 Acm_TxNO3_8_KI;
    uint8 Acm_TxNO3_8_KP;
    uint8 Acm_TxNO3_9_INTTIME;
    uint8 Acm_TxNO3_9_FILT_TIME;
    uint8 Acm_TxNO3_9_CHOP_TIME;
    uint8 Acm_TxNO3_13_BASE_DELTA_CURENT;
    uint8 Acm_Tx64_SOLENOID_ENABLE_4;
    uint8 Acm_Tx65_CH4_STATUS_L;
    uint8 Acm_Tx65_CH4_STATUS_H;
    uint8 Acm_TxTC0_2_SR_SEL;
    uint8 Acm_TxTC0_2_CALIBRATION_DIS;
    uint8 Acm_TxTC0_2_OFS_CHOP_DIS;
    uint8 Acm_TxTC0_2_TD_BLANK_SEL;
    uint8 Acm_TxTC0_2_LS_CLAMP_DIS;
    uint8 Acm_TxTC0_2_LOGIC_BIST_EN;
    uint8 Acm_TxTC0_2_DIAG_BIST_EN;
    uint8 Acm_TxTC0_2_ADC_BIST_EN;
    uint8 Acm_TxTC0_2_OFF_DIAG_EN;
    uint8 Acm_TxTC0_2_E_SOL_GPO;
    uint8 Acm_TxTC0_2_E_LSCLAMP_GPO;
    uint16 Acm_TxTC0_6_PWM_CODE;
    uint8 Acm_TxTC0_7_FREQ_MOD_STEP;
    uint8 Acm_TxTC0_7_MAX_FREQ_DELTA;
    uint8 Acm_TxTC0_8_KI;
    uint8 Acm_TxTC0_8_KP;
    uint8 Acm_TxTC0_9_INTTIME;
    uint8 Acm_TxTC0_9_FILT_TIME;
    uint8 Acm_TxTC0_9_CHOP_TIME;
    uint8 Acm_TxTC0_13_BASE_DELTA_CURENT;
    uint8 Acm_Tx64_SOLENOID_ENABLE_5;
    uint8 Acm_Tx65_CH5_STATUS_L;
    uint8 Acm_Tx65_CH5_STATUS_H;
    uint8 Acm_TxTC1_2_SR_SEL;
    uint8 Acm_TxTC1_2_CALIBRATION_DIS;
    uint8 Acm_TxTC1_2_OFS_CHOP_DIS;
    uint8 Acm_TxTC1_2_TD_BLANK_SEL;
    uint8 Acm_TxTC1_2_LS_CLAMP_DIS;
    uint8 Acm_TxTC1_2_LOGIC_BIST_EN;
    uint8 Acm_TxTC1_2_DIAG_BIST_EN;
    uint8 Acm_TxTC1_2_ADC_BIST_EN;
    uint8 Acm_TxTC1_2_OFF_DIAG_EN;
    uint8 Acm_TxTC1_2_E_SOL_GPO;
    uint8 Acm_TxTC1_2_E_LSCLAMP_GPO;
    uint16 Acm_TxTC1_6_PWM_CODE;
    uint8 Acm_TxTC1_7_FREQ_MOD_STEP;
    uint8 Acm_TxTC1_7_MAX_FREQ_DELTA;
    uint8 Acm_TxTC1_8_KI;
    uint8 Acm_TxTC1_8_KP;
    uint8 Acm_TxTC1_9_INTTIME;
    uint8 Acm_TxTC1_9_FILT_TIME;
    uint8 Acm_TxTC1_9_CHOP_TIME;
    uint8 Acm_TxTC1_13_BASE_DELTA_CURENT;
    uint8 Acm_Tx64_SOLENOID_ENABLE_6;
    uint8 Acm_Tx65_CH6_STATUS_L;
    uint8 Acm_Tx65_CH6_STATUS_H;
    uint8 Acm_TxESV0_2_SR_SEL;
    uint8 Acm_TxESV0_2_CALIBRATION_DIS;
    uint8 Acm_TxESV0_2_OFS_CHOP_DIS;
    uint8 Acm_TxESV0_2_TD_BLANK_SEL;
    uint8 Acm_TxESV0_2_LS_CLAMP_DIS;
    uint8 Acm_TxESV0_2_LOGIC_BIST_EN;
    uint8 Acm_TxESV0_2_DIAG_BIST_EN;
    uint8 Acm_TxESV0_2_ADC_BIST_EN;
    uint8 Acm_TxESV0_2_OFF_DIAG_EN;
    uint8 Acm_TxESV0_2_E_SOL_GPO;
    uint8 Acm_TxESV0_2_E_LSCLAMP_GPO;
    uint8 Acm_TxESV0_6_PWM_CODE;
    uint8 Acm_TxESV0_6_FREQ_MOD_STEP;
    uint8 Acm_TxESV0_6_MAX_FREQ_DELTA;
    uint8 Acm_TxESV0_8_KI;
    uint8 Acm_TxESV0_8_KP;
    uint8 Acm_TxESV0_9_INTTIME;
    uint8 Acm_TxESV0_9_FILT_TIME;
    uint8 Acm_TxESV0_9_CHOP_TIME;
    uint8 Acm_Tx64_SOLENOID_ENABLE_7;
    uint8 Acm_Tx65_CH7_STATUS_L;
    uint8 Acm_Tx65_CH7_STATUS_H;
    uint8 Acm_TxESV1_2_SR_SEL;
    uint8 Acm_TxESV1_2_CALIBRATION_DIS;
    uint8 Acm_TxESV1_2_OFS_CHOP_DIS;
    uint8 Acm_TxESV1_2_TD_BLANK_SEL;
    uint8 Acm_TxESV1_2_LS_CLAMP_DIS;
    uint8 Acm_TxESV1_2_LOGIC_BIST_EN;
    uint8 Acm_TxESV1_2_DIAG_BIST_EN;
    uint8 Acm_TxESV1_2_ADC_BIST_EN;
    uint8 Acm_TxESV1_2_OFF_DIAG_EN;
    uint8 Acm_TxESV1_2_E_SOL_GPO;
    uint8 Acm_TxESV1_2_E_LSCLAMP_GPO;
    uint8 Acm_TxESV1_6_PWM_CODE;
    uint8 Acm_TxESV1_6_FREQ_MOD_STEP;
    uint8 Acm_TxESV1_6_MAX_FREQ_DELTA;
    uint8 Acm_TxESV1_8_KI;
    uint8 Acm_TxESV1_8_KP;
    uint8 Acm_TxESV1_9_INTTIME;
    uint8 Acm_TxESV1_9_FILT_TIME;
    uint8 Acm_TxESV1_9_CHOP_TIME;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_0;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_1;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_2;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_3;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_4;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_5;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_6;
    uint8 Acm_Tx64_2_SOLENOID_ENABLE_7;
}AcmValveInitWriteDataType;

typedef struct
{
    uint8 Acm_Tx8_PMP_TCK_PWM; 
    uint8 Acm_Tx9_PMP_VDS_TH;  
    uint8 Acm_Tx9_PMP2_DIS;    
    uint8 Acm_Tx9_PMP_VDS_FIL; 
    uint8 Acm_Tx9_LDACT_DIS;   
    uint8 Acm_Tx9_PMP1_ISINC;  
    uint8 Acm_Tx9_PMP_DT;      
    uint8 Acm_Tx9_PMP_TEST;    
    uint8 Acm_Tx9_PMP_BIST_EN; 
}AcmMotorInitWriteDataType;


typedef struct
{
    uint8 Acm_Tx32_CONFIG_RANGE;
    uint8 Acm_Tx32_WSO_TEST;
    uint8 Acm_Tx33_WSI_FIRST_THRESHOLD;
    uint8 Acm_Tx34_WSI_OFFSET_THRESHOLD;
    uint8 Acm_Tx35_PTEN;
    uint8 Acm_Tx35_SS_DIS;
    uint8 Acm_Tx35_SENSOR_TYPE_SELECTION;
    uint8 Acm_Tx35_FILTER_SELECTION;
    uint8 Acm_Tx35_FIX_TH;
    uint8 Acm_Tx36_PTEN;
    uint8 Acm_Tx36_SS_DIS;
    uint8 Acm_Tx36_SENSOR_TYPE_SELECTION;
    uint8 Acm_Tx36_FILTER_SELECTION;
    uint8 Acm_Tx36_FIX_TH;
    uint8 Acm_Tx37_PTEN;
    uint8 Acm_Tx37_SS_DIS;
    uint8 Acm_Tx37_SENSOR_TYPE_SELECTION;
    uint8 Acm_Tx37_FILTER_SELECTION;
    uint8 Acm_Tx37_FIX_TH;
    uint8 Acm_Tx38_PTEN;
    uint8 Acm_Tx38_SS_DIS;
    uint8 Acm_Tx38_SENSOR_TYPE_SELECTION;
    uint8 Acm_Tx38_FILTER_SELECTION;
    uint8 Acm_Tx38_FIX_TH;
    uint8 Acm_Tx39_FVPWR_ACT;
    uint8 Acm_Tx39_WSI_VDD4_UV;
    uint8 Acm_Tx39_INIT;
    uint8 Acm_Tx39_DIAG;
    uint8 Acm_Tx39_CH4_EN;
    uint8 Acm_Tx39_CH3_EN;
    uint8 Acm_Tx39_CH2_EN;
    uint8 Acm_Tx39_CH1_EN;
    uint8 Acm_Tx44_WSI1_LBIST_EN;
    uint8 Acm_Tx44_WSI2_LBIST_EN;
    uint8 Acm_Tx44_WSI3_LBIST_EN;
    uint8 Acm_Tx44_WSI4_LBIST_EN;
}AcmWssInitWriteDataType;

typedef struct
{
    uint8 Acm_Tx17_WLD_CONF;
    uint8 Acm_Tx17_WLD_CMD;
    uint8 Acm_Tx17_WLD_DIAGOFF_EN;
    uint8 Acm_Tx17_WLD_LS_CLAMP_DIS;
    uint8 Acm_Tx17_SHLS_CMD;
    uint8 Acm_Tx17_SHLS_DIAGOFF_EN;
    uint8 Acm_Tx17_SHLS_CONFIG;
    uint8 Acm_Tx17_SHLS_LS_CLAMP_DIS;
}AcmLampShlsInitWriteDataType;



/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern const Acm_InitListType Acm_InitList;
extern const AcmAsicInitWriteDataType AcmAsicInitWriteData;
extern const AcmValveInitWriteDataType AcmValveInitWriteData;
extern const AcmMotorInitWriteDataType AcmMotorInitWriteData;
extern const AcmWssInitWriteDataType AcmWssInitWriteData;
extern const AcmLampShlsInitWriteDataType AcmLampShlsInitWriteData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACM_CFG_H_ */
/** @} */
