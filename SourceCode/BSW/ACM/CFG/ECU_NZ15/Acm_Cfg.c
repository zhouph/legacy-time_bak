/**
 * @defgroup Acm_Cfg Acm_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACM_START_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACM_STOP_SEC_CONST_UNSPECIFIED
#include "Acm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

const Acm_InitListType Acm_InitList =
{
    (InitUsedType) Acm_Used,      /* AsicInit */
    (InitUsedType) Acm_Used,      /* No0ValveInit */
    (InitUsedType) Acm_Used,      /* No1ValveInit */
    (InitUsedType) Acm_Used,      /* No2ValveInit */
    (InitUsedType) Acm_Used,      /* No3ValveInit */
    (InitUsedType) Acm_Used,      /* Nc0ValveInit */
    (InitUsedType) Acm_Used,      /* Nc1ValveInit */
    (InitUsedType) Acm_Used,      /* Nc2ValveInit */
    (InitUsedType) Acm_Used,      /* Nc3ValveInit */
    (InitUsedType) Acm_Used,      /* Tc0ValveInit */
    (InitUsedType) Acm_Used,      /* Tc1ValveInit */
    (InitUsedType) Acm_NotUsed,   /* Esv0ValveInit */
    (InitUsedType) Acm_NotUsed,   /* Esv1ValveInit */
    (InitUsedType) Acm_Used,      /* MotorInit */   /* for FSR */
    (InitUsedType) Acm_NotUsed,   /* VsoInit */     /* Because of ASIC Errata */
    (InitUsedType) Acm_Used,      /* WssInit */
    (InitUsedType) Acm_Used       /* LampShlsInit */
};

const AcmAsicInitWriteDataType AcmAsicInitWriteData =
{
    (uint8)MSG1_FS_VDS_TH_0_5V,    /* Acm_Tx1_FS_VDS_TH */                      /* FS ENABLE ENABLE */
    (uint8)Acm_Enable,             /* Acm_Tx1_FS_CMD */                         /* FS COMMAND ENABLE */
    (uint8)Acm_Enable,             /* Acm_Tx1_FS_EN */                          /* by guide line */
    (uint8)Acm_Enable,             /* Acm_Tx1_PMP_EN */                         /* PUMP MOTOR ENABLE */
    (uint8)Acm_Disable,            /* Acm_Tx1_OSC_SprSpect_DIS */               /* TODO */
    (uint8)1u,                     /* Acm_Tx1_VDD_4_OVC_SD_RESTART_TIME */      /* 100ms by guide line */
    (uint8)1u,                     /* Acm_Tx1_VDD4_OVC_FLT_TIME */              /* 30ms by guide line */
    (uint8)1u,                     /* Acm_Tx1_VDD_3_OVC_SD_RESTART_TIME */      /* 100ms by guide line */
    (uint8)1u,                     /* Acm_Tx1_VDD3_OVC_FLT_TIME */              /* 30ms by guide line */
    (uint8)1u,                     /* Acm_Tx2_WD_SW_DIS */                      /* TODO */
    (uint8)Acm_Disable,            /* Acm_Tx2_VDD_2d5_AUTO_SWITCH_OFF */        /* by guide line */
    (uint8)0u,                     /* Acm_Tx2_VDD1_DIODELOSS_FILT */            /* 0.5us by guide line */
    (uint8)1u,                     /* Acm_Tx2_WD_DIS */                         /* TODO ASIC HW WATCHDOG DISABLE -> NOTICE : Need to Enable by AsicWatchdog Component */
    (uint8)0u,                     /* Acm_Tx2_VDD5_DIS */                       /* TODO */
    (uint8)0u,                     /* Acm_Tx2_VDD2_DIS */                       /* TODO */
    (uint8)Acm_Disable,            /* Acm_Tx19_E_PVDS_GPO */                    /* by guide line */
    (uint8)Acm_Disable,            /* Acm_Tx19_E_VDD2d5dWSx_OT_GPO */           /* by guide line */
    (uint8)Acm_Disable,            /* Acm_Tx19_E_FBO_GPO */                     /* by guide line */
    (uint8)Acm_Disable,            /* Acm_Tx19_E_FSPI_GPO */                    /* by guide line */
    (uint8)Acm_Disable,            /* Acm_Tx19_E_EN_GPO */                      /* by guide line */
    (uint8)Acm_Disable             /* Acm_Tx19_E_FS_VDS_GPO */                  /* by guide line */
};

const AcmValveInitWriteDataType AcmValveInitWriteData =
{
    (uint8)Acm_Enable,              /* Acm_Tx67_CH1_EN */
    (uint8)Acm_Enable,              /* Acm_Tx67_CH1_DIAG_EN */              /* by guide line */   
    (uint8)Acm_Enable,              /* Acm_Tx67_CH1_TD_BLANK */             /* 375us, by guide line */ 
    (uint8)0u,                      /* Acm_Tx67_CH1_CMD */  
    (uint8)Acm_Enable,              /* Acm_Tx67_CH2_EN */   
    (uint8)Acm_Enable,              /* Acm_Tx67_CH2_DIAG_EN */              /* by guide line */ 
    (uint8)Acm_Enable,              /* Acm_Tx67_CH2_TD_BLANK */             /* 375us, by guide line */
    (uint8)0u,                      /* Acm_Tx67_CH2_CMD */  
    (uint8)Acm_Enable,              /* Acm_Tx67_CH3_EN */   
    (uint8)Acm_Enable,              /* Acm_Tx67_CH3_DIAG_EN */              /* by guide line */   
    (uint8)Acm_Enable,              /* Acm_Tx67_CH3_TD_BLANK */             /* 375us, by guide line */      
    (uint8)0u,                      /* Acm_Tx67_CH3_CMD */  
    (uint8)Acm_Enable,              /* Acm_Tx67_CH4_EN */   
    (uint8)Acm_Enable,              /* Acm_Tx67_CH4_DIAG_EN */              /* by guide line */ 
    (uint8)Acm_Enable,              /* Acm_Tx67_CH4_TD_BLANK */             /* 375us, by guide line */
    (uint8)0u,                      /* Acm_Tx67_CH4_CMD */  
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_0 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH0_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH0_STATUS_H */ 
    (uint8)0x2,                     /* Acm_TxNO0_2_SR_SEL */                /* 10V/us, Slewrate Select by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_CALIBRATION_DIS */       /* Digital Current Sense Enable by guide line */   
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_OFS_CHOP_DIS */          /* Current Sense Offset Compensation Active by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxNO0_2_TD_BLANK_SEL */          /* Long Blank Time Selected by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_LS_CLAMP_DIS */          /* Low Side Clamp Active by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_LOGIC_BIST_EN */         /* TODO */   
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_DIAG_BIST_EN */          /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_ADC_BIST_EN */           /* by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxNO0_2_OFF_DIAG_EN */           /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_E_SOL_GPO */             /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO0_2_E_LSCLAMP_GPO */         /* by guide line */ 
    (uint16)186u,                   /* Acm_TxNO0_6_PWM_CODE */  
    (uint8)Acm_Disable,             /* Acm_TxNO0_7_FREQ_MOD_STEP */ 
    (uint8)0u,                      /* Acm_TxNO0_7_MAX_FREQ_DELTA */    
    (uint8)6u,                      /* Acm_TxNO0_8_KI */                    /* by guide line */ 
    (uint8)4u,                      /* Acm_TxNO0_8_KP */                    /* by guide line */ 
    (uint8)3u,                      /* Acm_TxNO0_9_INTTIME */               /* by guide line */ 
    (uint8)2u,                      /* Acm_TxNO0_9_FILT_TIME */             /* by guide line */ 
    (uint8)0u,                      /* Acm_TxNO0_9_CHOP_TIME */             /* by guide line */ 
    (uint8)0u,                      /* Acm_TxNO0_13_BASE_DELTA_CURENT */    /* TODO */
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_1 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH1_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH1_STATUS_H */ 
    (uint8)0x2,                     /* Acm_TxNO1_2_SR_SEL */                /* 10V/us, Slewrate Select by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_CALIBRATION_DIS */       /* Digital Current Sense Enable by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_OFS_CHOP_DIS */          /* Current Sense Offset Compensation Active by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxNO1_2_TD_BLANK_SEL */          /* Long Blank Time Selected by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_LS_CLAMP_DIS */          /* Low Side Clamp Active by guide line */   
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_LOGIC_BIST_EN */         /* TODO */
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_DIAG_BIST_EN */          /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_ADC_BIST_EN */           /* by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxNO1_2_OFF_DIAG_EN */           /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_E_SOL_GPO */             /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO1_2_E_LSCLAMP_GPO */         /* by guide line */ 
    (uint16)186u,                   /* Acm_TxNO1_6_PWM_CODE */  
    (uint8)Acm_Disable,             /* Acm_TxNO1_7_FREQ_MOD_STEP */ 
    (uint8)0u,                      /* Acm_TxNO1_7_MAX_FREQ_DELTA */    
    (uint8)6u,                      /* Acm_TxNO1_8_KI */                    /* by guide line */   
    (uint8)4u,                      /* Acm_TxNO1_8_KP */                    /* by guide line */   
    (uint8)3u,                      /* Acm_TxNO1_9_INTTIME */               /* by guide line */   
    (uint8)2u,                      /* Acm_TxNO1_9_FILT_TIME */             /* by guide line */   
    (uint8)0u,                      /* Acm_TxNO1_9_CHOP_TIME */             /* by guide line */   
    (uint8)0u,                      /* Acm_TxNO1_13_BASE_DELTA_CURENT */    /* TODO */ 
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_2 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH2_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH2_STATUS_H */ 
    (uint8)0x2u,                    /* Acm_TxNO2_2_SR_SEL */                /* 10V/us, Slewrate Select by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_CALIBRATION_DIS */       /* Digital Current Sense Enable by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_OFS_CHOP_DIS */          /* Current Sense Offset Compensation Active by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxNO2_2_TD_BLANK_SEL */          /* Long Blank Time Selected by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_LS_CLAMP_DIS */          /* Low Side Clamp Active by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_LOGIC_BIST_EN */         /* TODO */  
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_DIAG_BIST_EN */          /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_ADC_BIST_EN */           /* by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxNO2_2_OFF_DIAG_EN */           /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_E_SOL_GPO */             /* by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO2_2_E_LSCLAMP_GPO */         /* by guide line */ 
    (uint16)186u,                   /* Acm_TxNO2_6_PWM_CODE */  
    (uint8)Acm_Disable,             /* Acm_TxNO2_7_FREQ_MOD_STEP */ 
    (uint8)0u,                      /* Acm_TxNO2_7_MAX_FREQ_DELTA */    
    (uint8)6u,                      /* Acm_TxNO2_8_KI */                    /* by guide line */  
    (uint8)4u,                      /* Acm_TxNO2_8_KP */                    /* by guide line */  
    (uint8)3u,                      /* Acm_TxNO2_9_INTTIME */               /* by guide line */  
    (uint8)2u,                      /* Acm_TxNO2_9_FILT_TIME */             /* by guide line */  
    (uint8)0u,                      /* Acm_TxNO2_9_CHOP_TIME */             /* by guide line */  
    (uint8)0u,                      /* Acm_TxNO2_13_BASE_DELTA_CURENT */    /* TODO */
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_3 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH3_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH3_STATUS_H */ 
    (uint8)0x2u,                    /* Acm_TxNO3_2_SR_SEL */                /* 10V/us, Slewrate Select by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_CALIBRATION_DIS */       /* Digital Current Sense Enable by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_OFS_CHOP_DIS */          /* Current Sense Offset Compensation Active by guide line */   
    (uint8)Acm_Enable,              /* Acm_TxNO3_2_TD_BLANK_SEL */          /* Long Blank Time Selected by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_LS_CLAMP_DIS */          /* Low Side Clamp Active by guide line */
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_LOGIC_BIST_EN */         /* TODO */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_DIAG_BIST_EN */          /* by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_ADC_BIST_EN */           /* by guide line */  
    (uint8)Acm_Enable,              /* Acm_TxNO3_2_OFF_DIAG_EN */           /* by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_E_SOL_GPO */             /* by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_2_E_LSCLAMP_GPO */         /* by guide line */  
    (uint16)186u,                   /* Acm_TxNO3_6_PWM_CODE */  
    (uint8)Acm_Disable,             /* Acm_TxNO3_7_FREQ_MOD_STEP */ 
    (uint8)0u,                      /* Acm_TxNO3_7_MAX_FREQ_DELTA */    
    (uint8)6u,                      /* Acm_TxNO3_8_KI */                    /* by guide line */ 
    (uint8)4u,                      /* Acm_TxNO3_8_KP */                    /* by guide line */ 
    (uint8)3u,                      /* Acm_TxNO3_9_INTTIME */               /* by guide line */ 
    (uint8)2u,                      /* Acm_TxNO3_9_FILT_TIME */             /* by guide line */ 
    (uint8)0u,                      /* Acm_TxNO3_9_CHOP_TIME */             /* by guide line */ 
    (uint8)0u,                      /* Acm_TxNO3_13_BASE_DELTA_CURENT */    /* TODO */
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_4 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH4_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH4_STATUS_H */ 
    (uint8)0x2u,                    /* Acm_TxTC0_2_SR_SEL */                /* 10V/us, Slewrate Select by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_CALIBRATION_DIS */       /* Digital Current Sense Enable by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_OFS_CHOP_DIS */          /* Current Sense Offset Compensation Active by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxTC0_2_TD_BLANK_SEL */          /* Long Blank Time Selected by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_LS_CLAMP_DIS */          /* Low Side Clamp Active by guide line */ 
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_LOGIC_BIST_EN */         /* TODO */   
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_DIAG_BIST_EN */          /* by guide line */   
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_ADC_BIST_EN */           /* by guide line */   
    (uint8)Acm_Enable,              /* Acm_TxTC0_2_OFF_DIAG_EN */           /* by guide line */   
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_E_SOL_GPO */             /* by guide line */   
    (uint8)Acm_Disable,             /* Acm_TxTC0_2_E_LSCLAMP_GPO */         /* by guide line */   
    (uint16)186u,                   /* Acm_TxTC0_6_PWM_CODE */  
    (uint8)Acm_Disable,             /* Acm_TxTC0_7_FREQ_MOD_STEP */ 
    (uint8)0u,                      /* Acm_TxTC0_7_MAX_FREQ_DELTA */    
    (uint8)6u,                      /* Acm_TxTC0_8_KI */                    /* by guide line */   
    (uint8)4u,                      /* Acm_TxTC0_8_KP */                    /* by guide line */   
    (uint8)3u,                      /* Acm_TxTC0_9_INTTIME */               /* by guide line */   
    (uint8)2u,                      /* Acm_TxTC0_9_FILT_TIME */             /* by guide line */   
    (uint8)0u,                      /* Acm_TxTC0_9_CHOP_TIME */             /* by guide line */   
    (uint8)0u,                      /* Acm_TxTC0_13_BASE_DELTA_CURENT */    /* TODO */ 
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_5 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH5_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH5_STATUS_H */ 
    (uint8)0x2u,                    /* Acm_TxTC1_2_SR_SEL */                 /* 10V/us, Slewrate Select by guide line */                   
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_CALIBRATION_DIS */        /* Digital Current Sense Enable by guide line */              
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_OFS_CHOP_DIS */           /* Current Sense Offset Compensation Active by guide line */  
    (uint8)Acm_Enable,              /* Acm_TxTC1_2_TD_BLANK_SEL */           /* Long Blank Time Selected by guide line */                  
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_LS_CLAMP_DIS */           /* Low Side Clamp Active by guide line */                     
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_LOGIC_BIST_EN */          /* TODO */                                                    
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_DIAG_BIST_EN */           /* by guide line */                                           
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_ADC_BIST_EN */            /* by guide line */                                           
    (uint8)Acm_Enable,              /* Acm_TxTC1_2_OFF_DIAG_EN */            /* by guide line */                                           
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_E_SOL_GPO */              /* by guide line */                                           
    (uint8)Acm_Disable,             /* Acm_TxTC1_2_E_LSCLAMP_GPO */          /* by guide line */                                           
    (uint16)186u,                   /* Acm_TxTC1_6_PWM_CODE */  
    (uint8)Acm_Disable,             /* Acm_TxTC1_7_FREQ_MOD_STEP */ 
    (uint8)0u,                      /* Acm_TxTC1_7_MAX_FREQ_DELTA */    
    (uint8)6u,                      /* Acm_TxTC1_8_KI */                    /* by guide line */   
    (uint8)4u,                      /* Acm_TxTC1_8_KP */                    /* by guide line */   
    (uint8)3u,                      /* Acm_TxTC1_9_INTTIME */               /* by guide line */   
    (uint8)2u,                      /* Acm_TxTC1_9_FILT_TIME */             /* by guide line */   
    (uint8)0u,                      /* Acm_TxTC1_9_CHOP_TIME */             /* by guide line */   
    (uint8)0u,                      /* Acm_TxTC1_13_BASE_DELTA_CURENT */    /* TODO */   
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_6 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH6_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH6_STATUS_H */ 
    (uint8)0x2u,                    /* Acm_TxESV0_2_SR_SEL */               /* Slewrate Select by guide line */                          
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_CALIBRATION_DIS */      /* Digital Current Sense Enable by guide line*/              
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_OFS_CHOP_DIS */         /* Current Sense Offset Compensation Active by guide line */ 
    (uint8)Acm_Enable,              /* Acm_TxESV0_2_TD_BLANK_SEL */         /* Long Blank Time Selected by guide line */                 
    (uint8)0x0u,                    /* Acm_TxESV0_2_LS_CLAMP_DIS */         /* Low Side Clamp Active by guide line */                    
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_LOGIC_BIST_EN */
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_DIAG_BIST_EN */         /* by guide line */                                          
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_ADC_BIST_EN */          /* by guide line */                                          
    (uint8)Acm_Enable,              /* Acm_TxESV0_2_OFF_DIAG_EN */          /* by guide line */                                          
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_E_SOL_GPO */    
    (uint8)Acm_Disable,             /* Acm_TxESV0_2_E_LSCLAMP_GPO */    
    (uint8)41u,                     /* Acm_TxESV0_6_PWM_CODE */ 
    (uint8)Acm_Disable,             /* Acm_TxESV0_6_FREQ_MOD_STEP */    
    (uint8)0x0u,                    /* Acm_TxESV0_6_MAX_FREQ_DELTA */   
    (uint8)1u,                      /* Acm_TxESV0_8_KI */                   /* by guide line */  
    (uint8)2u,                      /* Acm_TxESV0_8_KP */                   /* by guide line */  
    (uint8)3u,                      /* Acm_TxESV0_9_INTTIME */              /* by guide line */  
    (uint8)2u,                      /* Acm_TxESV0_9_FILT_TIME */            /* by guide line */  
    (uint8)0u,                      /* Acm_TxESV0_9_CHOP_TIME */            /* by guide line */  
    (uint8)Acm_Enable,              /* Acm_Tx64_SOLENOID_ENABLE_7 */    
    (uint8)ACM_VALVE_PWM_MODE,      /* Acm_Tx65_CH7_STATUS_L */ 
    (uint8)ACM_VALVE_ACTIVE_MODE,   /* Acm_Tx65_CH7_STATUS_H */ 
    (uint8)0x2u,                    /* Acm_TxESV1_2_SR_SEL */                /* Slewrate Select by guide line */                            
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_CALIBRATION_DIS */       /* Digital Current Sense Enable by guide line*/                
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_OFS_CHOP_DIS */          /* Current Sense Offset Compensation Active by guide line */   
    (uint8)Acm_Enable,              /* Acm_TxESV1_2_TD_BLANK_SEL */          /* Long Blank Time Selected by guide line */                   
    (uint8)0x0u,                    /* Acm_TxESV1_2_LS_CLAMP_DIS */          /* Low Side Clamp Active by guide line */                      
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_LOGIC_BIST_EN */    
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_DIAG_BIST_EN */         /* by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_ADC_BIST_EN */          /* by guide line */  
    (uint8)Acm_Enable,              /* Acm_TxESV1_2_OFF_DIAG_EN */          /* by guide line */  
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_E_SOL_GPO */    
    (uint8)Acm_Disable,             /* Acm_TxESV1_2_E_LSCLAMP_GPO */    
    (uint8)41u,                     /* Acm_TxESV1_6_PWM_CODE */ 
    (uint8)Acm_Disable,             /* Acm_TxESV1_6_FREQ_MOD_STEP */    
    (uint8)0x0u,                    /* Acm_TxESV1_6_MAX_FREQ_DELTA */   
    (uint8)1u,                      /* Acm_TxESV1_8_KI */                   /* by guide line */ 
    (uint8)2u,                      /* Acm_TxESV1_8_KP */                   /* by guide line */ 
    (uint8)3u,                      /* Acm_TxESV1_9_INTTIME */              /* by guide line */ 
    (uint8)2u,                      /* Acm_TxESV1_9_FILT_TIME */            /* by guide line */ 
    (uint8)0u,                      /* Acm_TxESV1_9_CHOP_TIME */            /* by guide line */ 
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_0 */  
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_1 */  
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_2 */  
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_3 */  
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_4 */  
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_5 */  
    (uint8)Acm_Enable,              /* Acm_Tx64_2_SOLENOID_ENABLE_6 */  
    (uint8)Acm_Enable               /* Acm_Tx64_2_SOLENOID_ENABLE_7 */  
};

const AcmMotorInitWriteDataType AcmMotorInitWriteData = 
{
    (uint8)15u,                /* Acm_Tx8_PMP_TCK_PWM */ 
    (uint8)PMP_VDS_TH_0_5V,    /* Acm_Tx9_PMP_VDS_TH */  
    (uint8)Acm_Disable,        /* Acm_Tx9_PMP2_DIS */    
    (uint8)PMP1_filt_5ms,      /* Acm_Tx9_PMP_VDS_FIL */ 
    (uint8)0u,                 /* Acm_Tx9_LDACT_DIS */   
    (uint8)Acm_Disable,        /* Acm_Tx9_PMP1_ISINC */  
    (uint8)PMP_DT_0_625us,     /* Acm_Tx9_PMP_DT */      
    (uint8)Acm_Disable,        /* Acm_Tx9_PMP_TEST */    
    (uint8)Acm_Disable         /* Acm_Tx9_PMP_BIST_EN */ 

};

const AcmWssInitWriteDataType AcmWssInitWriteData =
{
    (uint8)Acm_Disable,                 /* Acm_Tx32_CONFIG_RANGE */
    (uint8)Acm_Disable,                 /* Acm_Tx32_WSO_TEST */
    (uint8)Acm_Disable,                 /* Acm_Tx33_WSI_FIRST_THRESHOLD */
    (uint8)Acm_Disable,                 /* Acm_Tx34_WSI_OFFSET_THRESHOLD */
    #if(CH0_WSS_TYPE == WSS_ACTIVE)
    (uint8)Acm_Enable,                  /* Acm_Tx35_PTEN */
    (uint8)Acm_Disable,                 /* Acm_Tx35_SS_DIS */
    (uint8)_2LEVEL_STANDARD,             /* Acm_Tx35_SENSOR_TYPE_SELECTION */
    #elif(CH0_WSS_TYPE == WSS_SMART)
    (uint8)Acm_Disable,                 /* Acm_Tx35_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx35_SS_DIS */
    (uint8)PWM_2LEVEL_2EDGE,             /* Acm_Tx35_SENSOR_TYPE_SELECTION */
    #elif(CH0_WSS_TYPE == WSS_VDA)
    (uint8)Acm_Disable,                 /* Acm_Tx35_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx35_SS_DIS */
    (uint8)_3LEVEL_VDA,                 /* Acm_Tx35_SENSOR_TYPE_SELECTION */
    #endif
    (uint8)WSS_FILTER_15us,             /* Acm_Tx35_FILTER_SELECTION */
    (uint8)Acm_Disable,                 /* Acm_Tx35_FIX_TH */
    #if(CH1_WSS_TYPE == WSS_ACTIVE)
    (uint8)Acm_Enable,                  /* Acm_Tx36_PTEN */
    (uint8)Acm_Disable,                 /* Acm_Tx36_SS_DIS */
    (uint8)_2LEVEL_STANDARD,             /* Acm_Tx36_SENSOR_TYPE_SELECTION */
    #elif(CH1_WSS_TYPE == WSS_SMART)
    (uint8)Acm_Disable,                 /* Acm_Tx36_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx36_SS_DIS */
    (uint8)PWM_2LEVEL_2EDGE,             /* Acm_Tx36_SENSOR_TYPE_SELECTION */
    #elif(CH1_WSS_TYPE == WSS_VDA)
    (uint8)Acm_Disable,                 /* Acm_Tx36_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx36_SS_DIS */
    (uint8)_3LEVEL_VDA,                 /* Acm_Tx36_SENSOR_TYPE_SELECTION */
    #endif
    (uint8)WSS_FILTER_15us,             /* Acm_Tx36_FILTER_SELECTION */
    (uint8)Acm_Disable,                 /* Acm_Tx36_FIX_TH */
    #if(CH2_WSS_TYPE == WSS_ACTIVE)
    (uint8)Acm_Enable,                  /* Acm_Tx37_PTEN */
    (uint8)Acm_Disable,                 /* Acm_Tx37_SS_DIS */
    (uint8)_2LEVEL_STANDARD,             /* Acm_Tx37_SENSOR_TYPE_SELECTION */
    #elif(CH2_WSS_TYPE == WSS_SMART)
    (uint8)Acm_Disable,                 /* Acm_Tx37_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx37_SS_DIS */
    (uint8)PWM_2LEVEL_2EDGE,             /* Acm_Tx37_SENSOR_TYPE_SELECTION */
    #elif(CH2_WSS_TYPE == WSS_VDA)
    (uint8)Acm_Disable,                 /* Acm_Tx37_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx37_SS_DIS */
    (uint8)_3LEVEL_VDA,                 /* Acm_Tx37_SENSOR_TYPE_SELECTION */
    #endif
    (uint8)WSS_FILTER_15us,             /* Acm_Tx37_FILTER_SELECTION */
    (uint8)Acm_Disable,                 /* Acm_Tx37_FIX_TH */
    #if(CH3_WSS_TYPE == WSS_ACTIVE)
    (uint8)Acm_Enable,                  /* Acm_Tx38_PTEN */
    (uint8)Acm_Disable,                 /* Acm_Tx38_SS_DIS */
    (uint8)_2LEVEL_STANDARD,             /* Acm_Tx38_SENSOR_TYPE_SELECTION */
    #elif(CH3_WSS_TYPE == WSS_SMART)
    (uint8)Acm_Disable,                 /* Acm_Tx38_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx38_SS_DIS */
    (uint8)PWM_2LEVEL_2EDGE,             /* Acm_Tx38_SENSOR_TYPE_SELECTION */
    #elif(CH3_WSS_TYPE == WSS_VDA)
    (uint8)Acm_Disable,                 /* Acm_Tx38_PTEN */
    (uint8)Acm_Enable,                  /* Acm_Tx38_SS_DIS */
    (uint8)_3LEVEL_VDA,                 /* Acm_Tx38_SENSOR_TYPE_SELECTION */
    #endif
    (uint8)WSS_FILTER_15us,             /* Acm_Tx38_FILTER_SELECTION */
    (uint8)Acm_Disable,                 /* Acm_Tx38_FIX_TH */
    (uint8)Acm_Enable,                  /* Acm_Tx39_FVPWR_ACT */
    (uint8)Acm_Enable,                  /* Acm_Tx39_WSI_VDD4_UV */
    (uint8)Acm_Enable,                  /* Acm_Tx39_INIT */
    (uint8)Acm_Disable,                 /* Acm_Tx39_DIAG */
    (uint8)Acm_Enable,                  /* Acm_Tx39_CH4_EN */
    (uint8)Acm_Enable,                  /* Acm_Tx39_CH3_EN */
    (uint8)Acm_Enable,                  /* Acm_Tx39_CH2_EN */
    (uint8)Acm_Enable,                  /* Acm_Tx39_CH1_EN */
    (uint8)Acm_Disable,                 /* Acm_Tx44_WSI1_LBIST_EN */
    (uint8)Acm_Disable,                 /* Acm_Tx44_WSI2_LBIST_EN */
    (uint8)Acm_Disable,                 /* Acm_Tx44_WSI3_LBIST_EN */
    (uint8)Acm_Disable                  /* Acm_Tx44_WSI4_LBIST_EN */
};

const AcmLampShlsInitWriteDataType AcmLampShlsInitWriteData =
{
    (uint8)Acm_Disable,         /* Acm_Tx17_WLD_CONF */
    (uint8)0u,                   /* Acm_Tx17_WLD_CMD */
    (uint8)Acm_Enable,          /* Acm_Tx17_WLD_DIAGOFF_EN */
    (uint8)Acm_Disable,         /* Acm_Tx17_WLD_LS_CLAMP_DIS */
    (uint8)0u,                   /* Acm_Tx17_SHLS_CMD */
    (uint8)Acm_Enable,          /* Acm_Tx17_SHLS_DIAGOFF_EN */
    (uint8)0u,                   /* Acm_Tx17_SHLS_CONFIG */
    (uint8)Acm_Disable          /* Acm_Tx17_SHLS_LS_CLAMP_DIS */
};

#define ACM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACM_STOP_SEC_VAR_NOINIT_32BIT
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACM_STOP_SEC_VAR_UNSPECIFIED
#include "Acm_MemMap.h"
#define ACM_START_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/** Variable Section (32BIT)**/


#define ACM_STOP_SEC_VAR_32BIT
#include "Acm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACM_START_SEC_CODE
#include "Acm_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACM_STOP_SEC_CODE
#include "Acm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
