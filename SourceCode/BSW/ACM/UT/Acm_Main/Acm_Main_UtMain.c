#include "unity.h"
#include "unity_fixture.h"
#include "Acm_Main.h"
#include "Acm_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP


const Acm_MainAcmAsicInitCompleteFlag_t UtExpected_Acm_MainAcmAsicInitCompleteFlag[MAX_STEP] = ACM_MAINACMASICINITCOMPLETEFLAG;



TEST_GROUP(Acm_Main);
TEST_SETUP(Acm_Main)
{
    Acm_Main_Init();
}

TEST_TEAR_DOWN(Acm_Main)
{   /* Postcondition */

}

TEST(Acm_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {

        Acm_Main();

        TEST_ASSERT_EQUAL(Acm_MainAcmAsicInitCompleteFlag, UtExpected_Acm_MainAcmAsicInitCompleteFlag[i]);
    }
}

TEST_GROUP_RUNNER(Acm_Main)
{
    RUN_TEST_CASE(Acm_Main, All);
}
