/**
 * @defgroup Acm_MemMap Acm_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACM_MEMMAP_H_
#define ACM_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ACM_START_SEC_CODE)
  #undef ACM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_CODE)
  #undef ACM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_CODE_STARTED
    #error "ACM_SEC_CODE not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ACM_START_SEC_CONST_UNSPECIFIED)
  #undef ACM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_CONST_UNSPECIFIED)
  #undef ACM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_CONST_UNSPECIFIED_STARTED
    #error "ACM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ACM_START_SEC_VAR_UNSPECIFIED)
  #undef ACM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_VAR_UNSPECIFIED)
  #undef ACM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_VAR_UNSPECIFIED_STARTED
    #error "ACM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_START_SEC_VAR_32BIT)
  #undef ACM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_VAR_32BIT)
  #undef ACM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_VAR_32BIT_STARTED
    #error "ACM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ACM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_START_SEC_VAR_NOINIT_32BIT)
  #undef ACM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ACM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ACM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ACM_START_SEC_CALIB_UNSPECIFIED)
  #undef ACM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_SEC_STARTED
    #error "ACM section not closed"
  #endif
  #define CHK_ACM_SEC_STARTED
  #define CHK_ACM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ACM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ACM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_SEC_STARTED
  #undef CHK_ACM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ACM_MAIN_START_SEC_CODE)
  #undef ACM_MAIN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_CODE)
  #undef ACM_MAIN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_CODE_STARTED
    #error "ACM_MAIN_SEC_CODE not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ACM_MAIN_START_SEC_CONST_UNSPECIFIED)
  #undef ACM_MAIN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_CONST_UNSPECIFIED)
  #undef ACM_MAIN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
    #error "ACM_MAIN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ACM_MAIN_START_SEC_VAR_UNSPECIFIED)
  #undef ACM_MAIN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED)
  #undef ACM_MAIN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
    #error "ACM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_START_SEC_VAR_32BIT)
  #undef ACM_MAIN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_VAR_32BIT)
  #undef ACM_MAIN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_VAR_32BIT_STARTED
    #error "ACM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ACM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_START_SEC_VAR_NOINIT_32BIT)
  #undef ACM_MAIN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ACM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ACM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ACM_MAIN_START_SEC_CALIB_UNSPECIFIED)
  #undef ACM_MAIN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACM_MAIN_SEC_STARTED
    #error "ACM_MAIN section not closed"
  #endif
  #define CHK_ACM_MAIN_SEC_STARTED
  #define CHK_ACM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACM_MAIN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ACM_MAIN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ACM_MAIN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACM_MAIN_SEC_STARTED
  #undef CHK_ACM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACM_MEMMAP_H_ */
/** @} */
