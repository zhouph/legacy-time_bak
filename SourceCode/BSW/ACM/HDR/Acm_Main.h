/**
 * @defgroup Acm_Main Acm_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACM_MAIN_H_
#define ACM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acm_Types.h"
#include "Acm_Cfg.h"
#include "Acm_Cal.h"
#include "Ach_Input.h"
#include "Ach_Output.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ACM_MAIN_MODULE_ID      (0)
 #define ACM_MAIN_MAJOR_VERSION  (2)
 #define ACM_MAIN_MINOR_VERSION  (0)
 #define ACM_MAIN_PATCH_VERSION  (0)
 #define ACM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Acm_Main_HdrBusType Acm_MainBus;

/* Version Info */
extern const SwcVersionInfo_t Acm_MainVersionInfo;

/* Input Data Element */

/* Output Data Element */
extern Acm_MainAcmAsicInitCompleteFlag_t Acm_MainAcmAsicInitCompleteFlag;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Acm_Main_Init(void);
extern void Acm_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACM_MAIN_H_ */
/** @} */
