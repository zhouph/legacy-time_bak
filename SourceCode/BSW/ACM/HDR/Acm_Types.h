/**
 * @defgroup Acm_Types Acm_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acm_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACM_TYPES_H_
#define ACM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* MSG Parameter Defines */
#define MSG1_FS_VDS_TH_0_5V             (0x0)
#define MSG1_FS_VDS_TH_1_0V             (0x1)
#define MSG1_FS_VDS_TH_1_5V             (0x2)
#define MSG1_FS_VDS_TH_2_0V             (0x3)

/* PMP1 - PMP2 non-overlap time : Dead time between PMP1 turn-off and PMP2 turn-on and opposite */
#define PMP_DT_8_125us              0
#define PMP_DT_6_875us              1
#define PMP_DT_5_625us              2
#define PMP_DT_4_375us              3
#define PMP_DT_3_125us              4
#define PMP_DT_1_875us              5
#define PMP_DT_0_625us              6
#define PMP_DT_30us                 7
#define PMP_DT_60us                 15

/* PMP1 VDS th : PMP1 VDS comparator */
#define PMP_VDS_TH_0_5V             0
#define PMP_VDS_TH_1V               1
#define PMP_VDS_TH_1_5V             2
#define PMP_VDS_TH_2V               3

/* PMP1 VDS filt : PMP1 VDS filter time */
#define PMP1_filt_36_4us            0
#define PMP1_filt_72_8us            1
#define PMP1_filt_145_6us           2
#define PMP1_filt_163_8us           3
#define PMP1_filt_291_2us           4
#define PMP1_filt_550us             5
#define PMP1_filt_1_1ms             6
#define PMP1_filt_5ms               7

#define WSS_FILTER_8us              0x0
#define WSS_FILTER_15us             0x1
#define WSS_FILTER_30us             0x2
#define WSS_FILTER_50us             0x3

#define _2LEVEL_STANDARD            0x0
#define _3LEVEL_VDA                 0x1
#define PWM_2LEVEL_2EDGE            0x2
#define PWM_2LEVEL_1EDGE            0x3

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */

/* Output Data Element */
    Acm_MainAcmAsicInitCompleteFlag_t Acm_MainAcmAsicInitCompleteFlag;
}Acm_Main_HdrBusType;

/* ENUM */
typedef enum
{
    Acm_AsicInitNotComplete = 0u,
    Acm_AsicInitComplete,
    Acm_AsicInitError
}Acm_AsicInitStateType;


enum
{
    Acm_TimeIndex_0ms = 0u,
    Acm_TimeIndex_1ms,
    Acm_TimeIndex_2ms,
    Acm_TimeIndex_3ms,
    Acm_TimeIndex_4ms,
    Acm_TimeIndex_5ms,
    Acm_TimeIndex_6ms,
    Acm_TimeIndex_7ms,
    Acm_TimeIndex_8ms,
    Acm_TimeIndex_9ms
};

enum
{
    Acm_Disable = 0u,
    Acm_Enable
};

enum
{
    ACM_VALVE_PWM_MODE      = 0u,
    ACM_VALVE_FULLON_MODE   = 1u
};

enum
{
    ACM_VALVE_TRISTATE_MODE = 0u,
    ACM_VALVE_ACTIVE_MODE   = 1u
};


/* STRUCT */
typedef struct
{
    uint32 Acm_AsicInitState         : 1;
    uint32 Acm_ValveInitState        : 1;
    uint32 Acm_MotorInitState        : 1;
    uint32 Acm_VsoInitState          : 1;
    uint32 Acm_WssInitState          : 1;
    uint32 Acm_LampShlsInitState     : 1;
    uint32 Acm_Reserved              : 26;
}Acm_ModuleInitStateType;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACM_TYPES_H_ */
/** @} */
