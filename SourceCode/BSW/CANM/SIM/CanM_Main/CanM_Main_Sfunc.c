#define S_FUNCTION_NAME      CanM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          109
#define WidthOutputPort         15

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "CanM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    CanM_MainCanRxRegenInfo.HcuRegenEna = input[0];
    CanM_MainCanRxRegenInfo.HcuRegenBrkTq = input[1];
    CanM_MainCanRxAccelPedlInfo.AccelPedlVal = input[2];
    CanM_MainCanRxAccelPedlInfo.AccelPedlValErr = input[3];
    CanM_MainCanRxIdbInfo.EngMsgFault = input[4];
    CanM_MainCanRxIdbInfo.TarGearPosi = input[5];
    CanM_MainCanRxIdbInfo.GearSelDisp = input[6];
    CanM_MainCanRxIdbInfo.TcuSwiGs = input[7];
    CanM_MainCanRxIdbInfo.HcuServiceMod = input[8];
    CanM_MainCanRxIdbInfo.SubCanBusSigFault = input[9];
    CanM_MainCanRxIdbInfo.CoolantTemp = input[10];
    CanM_MainCanRxIdbInfo.CoolantTempErr = input[11];
    CanM_MainCanRxEngTempInfo.EngTemp = input[12];
    CanM_MainCanRxEngTempInfo.EngTempErr = input[13];
    CanM_MainCanRxEscInfo.Ax = input[14];
    CanM_MainCanRxEscInfo.YawRate = input[15];
    CanM_MainCanRxEscInfo.SteeringAngle = input[16];
    CanM_MainCanRxEscInfo.Ay = input[17];
    CanM_MainCanRxEscInfo.PbSwt = input[18];
    CanM_MainCanRxEscInfo.ClutchSwt = input[19];
    CanM_MainCanRxEscInfo.GearRSwt = input[20];
    CanM_MainCanRxEscInfo.EngActIndTq = input[21];
    CanM_MainCanRxEscInfo.EngRpm = input[22];
    CanM_MainCanRxEscInfo.EngIndTq = input[23];
    CanM_MainCanRxEscInfo.EngFrictionLossTq = input[24];
    CanM_MainCanRxEscInfo.EngStdTq = input[25];
    CanM_MainCanRxEscInfo.TurbineRpm = input[26];
    CanM_MainCanRxEscInfo.ThrottleAngle = input[27];
    CanM_MainCanRxEscInfo.TpsResol1000 = input[28];
    CanM_MainCanRxEscInfo.PvAvCanResol1000 = input[29];
    CanM_MainCanRxEscInfo.EngChr = input[30];
    CanM_MainCanRxEscInfo.EngVol = input[31];
    CanM_MainCanRxEscInfo.GearType = input[32];
    CanM_MainCanRxEscInfo.EngClutchState = input[33];
    CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv = input[34];
    CanM_MainCanRxEscInfo.MotEstTq = input[35];
    CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv = input[36];
    CanM_MainCanRxEscInfo.TqIntvTCU = input[37];
    CanM_MainCanRxEscInfo.TqIntvSlowTCU = input[38];
    CanM_MainCanRxEscInfo.TqIncReq = input[39];
    CanM_MainCanRxEscInfo.DecelReq = input[40];
    CanM_MainCanRxEscInfo.RainSnsStat = input[41];
    CanM_MainCanRxEscInfo.EngSpdErr = input[42];
    CanM_MainCanRxEscInfo.AtType = input[43];
    CanM_MainCanRxEscInfo.MtType = input[44];
    CanM_MainCanRxEscInfo.CvtType = input[45];
    CanM_MainCanRxEscInfo.DctType = input[46];
    CanM_MainCanRxEscInfo.HevAtTcu = input[47];
    CanM_MainCanRxEscInfo.TurbineRpmErr = input[48];
    CanM_MainCanRxEscInfo.ThrottleAngleErr = input[49];
    CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct = input[50];
    CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV = input[51];
    CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct = input[52];
    CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV = input[53];
    CanM_MainCanRxEscInfo.WiperIntSW = input[54];
    CanM_MainCanRxEscInfo.WiperLow = input[55];
    CanM_MainCanRxEscInfo.WiperHigh = input[56];
    CanM_MainCanRxEscInfo.WiperValid = input[57];
    CanM_MainCanRxEscInfo.WiperAuto = input[58];
    CanM_MainCanRxEscInfo.TcuFaultSts = input[59];
    CanM_MainWhlSpdInfo.FlWhlSpd = input[60];
    CanM_MainWhlSpdInfo.FrWhlSpd = input[61];
    CanM_MainWhlSpdInfo.RlWhlSpd = input[62];
    CanM_MainWhlSpdInfo.RrlWhlSpd = input[63];
    CanM_MainCanRxEemInfo.YawRateInvld = input[64];
    CanM_MainCanRxEemInfo.AyInvld = input[65];
    CanM_MainCanRxEemInfo.SteeringAngleRxOk = input[66];
    CanM_MainCanRxEemInfo.AxInvldData = input[67];
    CanM_MainCanRxEemInfo.Ems1RxErr = input[68];
    CanM_MainCanRxEemInfo.Ems2RxErr = input[69];
    CanM_MainCanRxEemInfo.Tcu1RxErr = input[70];
    CanM_MainCanRxEemInfo.Tcu5RxErr = input[71];
    CanM_MainCanRxEemInfo.Hcu1RxErr = input[72];
    CanM_MainCanRxEemInfo.Hcu2RxErr = input[73];
    CanM_MainCanRxEemInfo.Hcu3RxErr = input[74];
    CanM_MainCanTimeOutStInfo.MaiCanSusDet = input[75];
    CanM_MainCanTimeOutStInfo.EmsTiOutSusDet = input[76];
    CanM_MainCanTimeOutStInfo.TcuTiOutSusDet = input[77];
    CanM_MainCanRxInfo.MainBusOffFlag = input[78];
    CanM_MainCanRxInfo.SenBusOffFlag = input[79];
    CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg = input[80];
    CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = input[81];
    CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = input[82];
    CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg = input[83];
    CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg = input[84];
    CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg = input[85];
    CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg = input[86];
    CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg = input[87];
    CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg = input[88];
    CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = input[89];
    CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg = input[90];
    CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg = input[91];
    CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg = input[92];
    CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg = input[93];
    CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg = input[94];
    CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg = input[95];
    CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg = input[96];
    CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg = input[97];
    CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg = input[98];
    CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg = input[99];
    CanM_MainEcuModeSts = input[100];
    CanM_MainIgnOnOffSts = input[101];
    CanM_MainIgnEdgeSts = input[102];
    CanM_MainVBatt1Mon = input[103];
    CanM_MainDiagClrSrs = input[104];
    CanM_MainArbVlvDriveState = input[105];
    CanM_MainCEMon = input[106];
    CanM_MainCanRxGearSelDispErrInfo = input[107];
    CanM_MainMtrArbDriveState = input[108];

    CanM_Main();


    output[0] = CanM_MainCanMonData.CanM_MainBusOff_Err;
    output[1] = CanM_MainCanMonData.CanM_SubBusOff_Err;
    output[2] = CanM_MainCanMonData.CanM_MainOverRun_Err;
    output[3] = CanM_MainCanMonData.CanM_SubOverRun_Err;
    output[4] = CanM_MainCanMonData.CanM_EMS1_Tout_Err;
    output[5] = CanM_MainCanMonData.CanM_EMS2_Tout_Err;
    output[6] = CanM_MainCanMonData.CanM_TCU1_Tout_Err;
    output[7] = CanM_MainCanMonData.CanM_TCU5_Tout_Err;
    output[8] = CanM_MainCanMonData.CanM_FWD1_Tout_Err;
    output[9] = CanM_MainCanMonData.CanM_HCU1_Tout_Err;
    output[10] = CanM_MainCanMonData.CanM_HCU2_Tout_Err;
    output[11] = CanM_MainCanMonData.CanM_HCU3_Tout_Err;
    output[12] = CanM_MainCanMonData.CanM_VSM2_Tout_Err;
    output[13] = CanM_MainCanMonData.CanM_EMS_Invalid_Err;
    output[14] = CanM_MainCanMonData.CanM_TCU_Invalid_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    CanM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
