/**
 * @defgroup CanM_Process CanM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanM_Process.h"
#include "CanM_Main.h"
#include "common.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define U8_CANM_TIME_1SEC 200

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
uint16 u16CanMWheelCheck;
uint16 u16CanMActuator/*temp*/;
uint8 u8CanMInhibitFsChk = 0;/* Not define */
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

uint16 CanM_WheelCheck(CanM_Main_HdrBusType *pCanMWhlCheck)
{
	uint16 u16WheelStatus = 0;
	uint16 u16TempFlWhlSpd = 0;
	uint16 u16TempFrWhlSpd = 0;
	uint16 u16TempRlWhlSpd = 0;
	uint16 u16TempRrWhlSpd = 0;

	u16TempFlWhlSpd = pCanMWhlCheck->CanM_MainWhlSpdInfo.FlWhlSpd;
	u16TempFrWhlSpd = pCanMWhlCheck->CanM_MainWhlSpdInfo.FrWhlSpd;
	u16TempRlWhlSpd = pCanMWhlCheck->CanM_MainWhlSpdInfo.RlWhlSpd;
	u16TempRrWhlSpd = pCanMWhlCheck->CanM_MainWhlSpdInfo.RrlWhlSpd;

	if(u16TempFlWhlSpd>u16TempFrWhlSpd)
	{
		if(u16TempFlWhlSpd>u16TempRlWhlSpd)
		{
			if(u16TempFlWhlSpd>u16TempRrWhlSpd)
			{
				u16WheelStatus = u16TempFlWhlSpd;
			}
			else
			{
				u16WheelStatus = u16TempRrWhlSpd;
			}
		}
		else if(u16TempRlWhlSpd>u16TempRrWhlSpd)
		{
			u16WheelStatus = u16TempRlWhlSpd;
		}
		else
		{
			u16WheelStatus = u16TempRrWhlSpd;
		}
	}
	else if(u16TempFrWhlSpd>u16TempRlWhlSpd)
	{
		if(u16TempFrWhlSpd>u16TempRrWhlSpd)
		{
			u16WheelStatus = u16TempFrWhlSpd;
		}
		else
		{
			u16WheelStatus = u16TempRrWhlSpd;
		}
	}
	else if(u16TempRlWhlSpd>u16TempRrWhlSpd)
	{
		u16WheelStatus = u16TempRlWhlSpd;		
	}
	else
	{
		if(u16TempRrWhlSpd>u16TempFlWhlSpd)
		{
			u16WheelStatus = u16TempRrWhlSpd;		
		}
		else
		{
			u16WheelStatus = u16TempFlWhlSpd;		
		}
	}
	u16WheelStatus = (uint16)(u16WheelStatus/64);
	return u16WheelStatus;
}

uint8_t CanM_Process_InhibitCheck(CanM_Main_HdrBusType *pCanMInhibit)
{

	uint8_t  cc_u8ChkInhibit = 0;
    uint16_t cc_u16IgnVolt = 0;
    static uint16_t cc_u16VoltageCnt =0;
    static uint16_t cc_u16CrankChkCnt=0; 
    static uint16_t cc_u16IGNFallingCnt =0;
            
    /* IGN ON, 5sec */
	if(pCanMInhibit->CanM_MainIgnOnOffSts == 0)/* CE Off State */
	{
		cc_u8ChkInhibit=cc_u8ChkInhibit+1;
        cc_u16CrankChkCnt=0;
        cc_u16VoltageCnt =0;		
	}
	else /* CE On State */
	{
		/* Actuator Status */
		if(((pCanMInhibit->CanM_MainMtrArbDriveState)==0)||((pCanMInhibit->CanM_MainArbVlvDriveState)==0)/* Actuator Off*/)
		{
            cc_u16IgnVolt=U16_CALCVOLT_9V4;
		}
		else /* Actuator On */
		{
            cc_u16IgnVolt=U16_CALCVOLT_8V4;
		}
		
		if((pCanMInhibit->CanM_MainVBatt1Mon>=cc_u16IgnVolt)&&(pCanMInhibit->CanM_MainVBatt1Mon<=U16_CALCVOLT_17V0))
        {
            if(cc_u16VoltageCnt<U8_CANM_TIME_1SEC)/* Count Conform */
            {
                cc_u16VoltageCnt=cc_u16VoltageCnt+1;
                cc_u8ChkInhibit =cc_u8ChkInhibit+1;
            }
            else
            {
                ;
            }
        }
        else
        {
            cc_u16VoltageCnt=0;
            cc_u8ChkInhibit =cc_u8ChkInhibit+1;
        }

		if((((pCanMInhibit->CanM_MainMtrArbDriveState)==0)||((pCanMInhibit->CanM_MainArbVlvDriveState)==0)/* Actuator Off*/)/* Actuator Off */&&(u16CanMWheelCheck<=U16_F64SPEED_6KPH))
		{
			;/*Engine Crank Check : GM Type ?? --> To Do */
		}
	}

    if(pCanMInhibit->CanM_MainIgnEdgeSts == 1) /*Hard Wire IGN의 falling edge 적용*/
    {
        cc_u16IGNFallingCnt=0;
        cc_u8ChkInhibit =cc_u8ChkInhibit+1;
    }
    else
    {
        if(cc_u16IGNFallingCnt<U8_CANM_TIME_1SEC) /*Hard Wire IGN 순간적인 OFF/ON시 오검출 방지*/
        {
            cc_u16IGNFallingCnt=cc_u16IGNFallingCnt+1;
            cc_u8ChkInhibit =cc_u8ChkInhibit+1;
        }
        else
        {
            ;
        }
    }      
    
    if(u8CanMInhibitFsChk==1/* Not define */)
    {
        cc_u8ChkInhibit=cc_u8ChkInhibit+1;      
    }
    else
    {
        ;
    }    
    return cc_u8ChkInhibit; 

}

void CanM_Process_TimeOutCheck(CanM_Main_HdrBusType *pCanMTO)
{
	/* To Do */
	if(pCanMTO->CanM_MainCanRxEemInfo.Ems1RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_EMS1_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_EMS1_Tout_Err = ERR_PASSED;
	}

	if(pCanMTO->CanM_MainCanRxEemInfo.Ems2RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_EMS2_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_EMS2_Tout_Err = ERR_PASSED;
	}

	if(pCanMTO->CanM_MainCanRxEemInfo.Tcu1RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_TCU1_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_TCU1_Tout_Err = ERR_PASSED;
	}

	if(pCanMTO->CanM_MainCanRxEemInfo.Tcu5RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_TCU5_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_TCU5_Tout_Err = ERR_PASSED;
	}

	if(pCanMTO->CanM_MainCanRxEemInfo.Hcu1RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_HCU1_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_HCU1_Tout_Err = ERR_PASSED;
	}

	if(pCanMTO->CanM_MainCanRxEemInfo.Hcu2RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_HCU2_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_HCU2_Tout_Err = ERR_PASSED;
	}

	if(pCanMTO->CanM_MainCanRxEemInfo.Hcu3RxErr == 1)
	{
		pCanMTO->CanM_MainCanMonData.CanM_HCU3_Tout_Err = ERR_FAILED;
	}
	else
	{
		pCanMTO->CanM_MainCanMonData.CanM_HCU3_Tout_Err = ERR_PASSED;
	}

}


void CanM_Process_BusOffCheck(CanM_Main_HdrBusType *pCanMBO)
{
	/* To Do */
	/* Main CAN Bus Off Check */
	if(pCanMBO->CanM_MainCanRxInfo.MainBusOffFlag == 1)
	{
		pCanMBO->CanM_MainCanMonData.CanM_MainBusOff_Err = ERR_PREFAILED;
	}
	else
	{
		pCanMBO->CanM_MainCanMonData.CanM_MainBusOff_Err = ERR_PREPASSED;
	}
	/* Sub CAN Bus Off Check */
	if(pCanMBO->CanM_MainCanRxInfo.SenBusOffFlag == 1)
	{
		pCanMBO->CanM_MainCanMonData.CanM_SubBusOff_Err = ERR_PREFAILED;
	}
	else
	{
		pCanMBO->CanM_MainCanMonData.CanM_SubBusOff_Err = ERR_PREPASSED;
	}
}

void CanM_InhibitSignal_Ctrl(CanM_Main_HdrBusType *pCanMIhb,uint8_t checkValue)
{
	pCanMIhb->CanM_MainCanMonData.CanM_MainBusOff_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_SubBusOff_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_MainOverRun_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_SubOverRun_Err = checkValue;	
	pCanMIhb->CanM_MainCanMonData.CanM_EMS1_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_EMS2_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_TCU1_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_TCU5_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_FWD1_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_HCU1_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_HCU2_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_HCU3_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_VSM2_Tout_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_EMS_Invalid_Err = checkValue;
	pCanMIhb->CanM_MainCanMonData.CanM_TCU_Invalid_Err = checkValue;	
}

void CanM_Process(CanM_Main_HdrBusType *pCanMCheck)
{
	uint8_t u8CanMonInhibitCheck = 0;

	u16CanMWheelCheck = CanM_WheelCheck(pCanMCheck);
	/* Can Inhibit Check */
	u8CanMonInhibitCheck = CanM_Process_InhibitCheck(pCanMCheck);
	if(u8CanMonInhibitCheck > 0 )/* CAN Monitioring Inhibit Set */
	{
		CanM_InhibitSignal_Ctrl(pCanMCheck,ERR_INHIBIT);
	}
	else
	{

		/* Can Bus Off Check */
		CanM_Process_BusOffCheck(pCanMCheck);
		
		/* Can Time out Check */
		CanM_Process_TimeOutCheck(pCanMCheck);
	}
} 
