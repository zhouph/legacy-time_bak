/**
 * @defgroup CanM_Main CanM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanM_Main.h"
#include "CanM_Main_Ifa.h"
#include "IfxStm_reg.h"
#include "CanM_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define CANM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "CanM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define CANM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "CanM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
CanM_Main_HdrBusType CanM_MainBus;

/* Version Info */
const SwcVersionInfo_t CanM_MainVersionInfo = 
{   
    CANM_MAIN_MODULE_ID,           /* CanM_MainVersionInfo.ModuleId */
    CANM_MAIN_MAJOR_VERSION,       /* CanM_MainVersionInfo.MajorVer */
    CANM_MAIN_MINOR_VERSION,       /* CanM_MainVersionInfo.MinorVer */
    CANM_MAIN_PATCH_VERSION,       /* CanM_MainVersionInfo.PatchVer */
    CANM_MAIN_BRANCH_VERSION       /* CanM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxRegenInfo_t CanM_MainCanRxRegenInfo;
Proxy_RxCanRxAccelPedlInfo_t CanM_MainCanRxAccelPedlInfo;
Proxy_RxCanRxIdbInfo_t CanM_MainCanRxIdbInfo;
Proxy_RxCanRxEngTempInfo_t CanM_MainCanRxEngTempInfo;
Proxy_RxCanRxEscInfo_t CanM_MainCanRxEscInfo;
Wss_SenWhlSpdInfo_t CanM_MainWhlSpdInfo;
Proxy_RxCanRxEemInfo_t CanM_MainCanRxEemInfo;
Eem_SuspcDetnCanTimeOutStInfo_t CanM_MainCanTimeOutStInfo;
Proxy_RxCanRxInfo_t CanM_MainCanRxInfo;
Proxy_RxByComRxMsgOkFlgInfo_t CanM_MainRxMsgOkFlgInfo;
Mom_HndlrEcuModeSts_t CanM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t CanM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t CanM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t CanM_MainVBatt1Mon;
Diag_HndlrDiagClr_t CanM_MainDiagClrSrs;
Arbitrator_VlvArbVlvDriveState_t CanM_MainArbVlvDriveState;
Ioc_InputSR1msCEMon_t CanM_MainCEMon;
Proxy_RxCanRxGearSelDispErrInfo_t CanM_MainCanRxGearSelDispErrInfo;
Arbitrator_MtrMtrArbDriveState_t CanM_MainMtrArbDriveState;

/* Output Data Element */
CanM_MainCanMonData_t CanM_MainCanMonData;

uint32 CanM_Main_Timer_Start;
uint32 CanM_Main_Timer_Elapsed;

#define CANM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
#define CANM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_MAIN_START_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/** Variable Section (32BIT)**/


#define CANM_MAIN_STOP_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define CANM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
#define CANM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_MAIN_START_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/** Variable Section (32BIT)**/


#define CANM_MAIN_STOP_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define CANM_MAIN_START_SEC_CODE
#include "CanM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void CanM_Main_Init(void)
{
    /* Initialize internal bus */
    CanM_MainBus.CanM_MainCanRxRegenInfo.HcuRegenEna = 0;
    CanM_MainBus.CanM_MainCanRxRegenInfo.HcuRegenBrkTq = 0;
    CanM_MainBus.CanM_MainCanRxAccelPedlInfo.AccelPedlVal = 0;
    CanM_MainBus.CanM_MainCanRxAccelPedlInfo.AccelPedlValErr = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.EngMsgFault = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.TarGearPosi = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.GearSelDisp = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.TcuSwiGs = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.HcuServiceMod = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.SubCanBusSigFault = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.CoolantTemp = 0;
    CanM_MainBus.CanM_MainCanRxIdbInfo.CoolantTempErr = 0;
    CanM_MainBus.CanM_MainCanRxEngTempInfo.EngTemp = 0;
    CanM_MainBus.CanM_MainCanRxEngTempInfo.EngTempErr = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.Ax = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.YawRate = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.SteeringAngle = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.Ay = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.PbSwt = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.ClutchSwt = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.GearRSwt = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngActIndTq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngRpm = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngIndTq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngFrictionLossTq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngStdTq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TurbineRpm = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.ThrottleAngle = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TpsResol1000 = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.PvAvCanResol1000 = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngChr = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngVol = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.GearType = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngClutchState = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.MotEstTq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TqIntvTCU = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TqIntvSlowTCU = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TqIncReq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.DecelReq = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.RainSnsStat = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.EngSpdErr = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.AtType = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.MtType = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.CvtType = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.DctType = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.HevAtTcu = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TurbineRpmErr = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.ThrottleAngleErr = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.WiperIntSW = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.WiperLow = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.WiperHigh = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.WiperValid = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.WiperAuto = 0;
    CanM_MainBus.CanM_MainCanRxEscInfo.TcuFaultSts = 0;
    CanM_MainBus.CanM_MainWhlSpdInfo.FlWhlSpd = 0;
    CanM_MainBus.CanM_MainWhlSpdInfo.FrWhlSpd = 0;
    CanM_MainBus.CanM_MainWhlSpdInfo.RlWhlSpd = 0;
    CanM_MainBus.CanM_MainWhlSpdInfo.RrlWhlSpd = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.YawRateInvld = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.AyInvld = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.SteeringAngleRxOk = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.AxInvldData = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Ems1RxErr = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Ems2RxErr = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Tcu1RxErr = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Tcu5RxErr = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Hcu1RxErr = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Hcu2RxErr = 0;
    CanM_MainBus.CanM_MainCanRxEemInfo.Hcu3RxErr = 0;
    CanM_MainBus.CanM_MainCanTimeOutStInfo.MaiCanSusDet = 0;
    CanM_MainBus.CanM_MainCanTimeOutStInfo.EmsTiOutSusDet = 0;
    CanM_MainBus.CanM_MainCanTimeOutStInfo.TcuTiOutSusDet = 0;
    CanM_MainBus.CanM_MainCanRxInfo.MainBusOffFlag = 0;
    CanM_MainBus.CanM_MainCanRxInfo.SenBusOffFlag = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg = 0;
    CanM_MainBus.CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg = 0;
    CanM_MainBus.CanM_MainEcuModeSts = 0;
    CanM_MainBus.CanM_MainIgnOnOffSts = 0;
    CanM_MainBus.CanM_MainIgnEdgeSts = 0;
    CanM_MainBus.CanM_MainVBatt1Mon = 0;
    CanM_MainBus.CanM_MainDiagClrSrs = 0;
    CanM_MainBus.CanM_MainArbVlvDriveState = 0;
    CanM_MainBus.CanM_MainCEMon = 0;
    CanM_MainBus.CanM_MainCanRxGearSelDispErrInfo = 0;
    CanM_MainBus.CanM_MainMtrArbDriveState = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_MainBusOff_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_SubBusOff_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_MainOverRun_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_SubOverRun_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_EMS1_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_EMS2_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_TCU1_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_TCU5_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_FWD1_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_HCU1_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_HCU2_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_HCU3_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_VSM2_Tout_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_EMS_Invalid_Err = 0;
    CanM_MainBus.CanM_MainCanMonData.CanM_TCU_Invalid_Err = 0;
}

void CanM_Main(void)
{
    CanM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    CanM_Main_Read_CanM_MainCanRxRegenInfo(&CanM_MainBus.CanM_MainCanRxRegenInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxRegenInfo 
     : CanM_MainCanRxRegenInfo.HcuRegenEna;
     : CanM_MainCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanRxAccelPedlInfo(&CanM_MainBus.CanM_MainCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxAccelPedlInfo 
     : CanM_MainCanRxAccelPedlInfo.AccelPedlVal;
     : CanM_MainCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanRxIdbInfo(&CanM_MainBus.CanM_MainCanRxIdbInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxIdbInfo 
     : CanM_MainCanRxIdbInfo.EngMsgFault;
     : CanM_MainCanRxIdbInfo.TarGearPosi;
     : CanM_MainCanRxIdbInfo.GearSelDisp;
     : CanM_MainCanRxIdbInfo.TcuSwiGs;
     : CanM_MainCanRxIdbInfo.HcuServiceMod;
     : CanM_MainCanRxIdbInfo.SubCanBusSigFault;
     : CanM_MainCanRxIdbInfo.CoolantTemp;
     : CanM_MainCanRxIdbInfo.CoolantTempErr;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanRxEngTempInfo(&CanM_MainBus.CanM_MainCanRxEngTempInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxEngTempInfo 
     : CanM_MainCanRxEngTempInfo.EngTemp;
     : CanM_MainCanRxEngTempInfo.EngTempErr;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanRxEscInfo(&CanM_MainBus.CanM_MainCanRxEscInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxEscInfo 
     : CanM_MainCanRxEscInfo.Ax;
     : CanM_MainCanRxEscInfo.YawRate;
     : CanM_MainCanRxEscInfo.SteeringAngle;
     : CanM_MainCanRxEscInfo.Ay;
     : CanM_MainCanRxEscInfo.PbSwt;
     : CanM_MainCanRxEscInfo.ClutchSwt;
     : CanM_MainCanRxEscInfo.GearRSwt;
     : CanM_MainCanRxEscInfo.EngActIndTq;
     : CanM_MainCanRxEscInfo.EngRpm;
     : CanM_MainCanRxEscInfo.EngIndTq;
     : CanM_MainCanRxEscInfo.EngFrictionLossTq;
     : CanM_MainCanRxEscInfo.EngStdTq;
     : CanM_MainCanRxEscInfo.TurbineRpm;
     : CanM_MainCanRxEscInfo.ThrottleAngle;
     : CanM_MainCanRxEscInfo.TpsResol1000;
     : CanM_MainCanRxEscInfo.PvAvCanResol1000;
     : CanM_MainCanRxEscInfo.EngChr;
     : CanM_MainCanRxEscInfo.EngVol;
     : CanM_MainCanRxEscInfo.GearType;
     : CanM_MainCanRxEscInfo.EngClutchState;
     : CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv;
     : CanM_MainCanRxEscInfo.MotEstTq;
     : CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv;
     : CanM_MainCanRxEscInfo.TqIntvTCU;
     : CanM_MainCanRxEscInfo.TqIntvSlowTCU;
     : CanM_MainCanRxEscInfo.TqIncReq;
     : CanM_MainCanRxEscInfo.DecelReq;
     : CanM_MainCanRxEscInfo.RainSnsStat;
     : CanM_MainCanRxEscInfo.EngSpdErr;
     : CanM_MainCanRxEscInfo.AtType;
     : CanM_MainCanRxEscInfo.MtType;
     : CanM_MainCanRxEscInfo.CvtType;
     : CanM_MainCanRxEscInfo.DctType;
     : CanM_MainCanRxEscInfo.HevAtTcu;
     : CanM_MainCanRxEscInfo.TurbineRpmErr;
     : CanM_MainCanRxEscInfo.ThrottleAngleErr;
     : CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct;
     : CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV;
     : CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct;
     : CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV;
     : CanM_MainCanRxEscInfo.WiperIntSW;
     : CanM_MainCanRxEscInfo.WiperLow;
     : CanM_MainCanRxEscInfo.WiperHigh;
     : CanM_MainCanRxEscInfo.WiperValid;
     : CanM_MainCanRxEscInfo.WiperAuto;
     : CanM_MainCanRxEscInfo.TcuFaultSts;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainWhlSpdInfo(&CanM_MainBus.CanM_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure CanM_MainWhlSpdInfo 
     : CanM_MainWhlSpdInfo.FlWhlSpd;
     : CanM_MainWhlSpdInfo.FrWhlSpd;
     : CanM_MainWhlSpdInfo.RlWhlSpd;
     : CanM_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanRxEemInfo(&CanM_MainBus.CanM_MainCanRxEemInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxEemInfo 
     : CanM_MainCanRxEemInfo.YawRateInvld;
     : CanM_MainCanRxEemInfo.AyInvld;
     : CanM_MainCanRxEemInfo.SteeringAngleRxOk;
     : CanM_MainCanRxEemInfo.AxInvldData;
     : CanM_MainCanRxEemInfo.Ems1RxErr;
     : CanM_MainCanRxEemInfo.Ems2RxErr;
     : CanM_MainCanRxEemInfo.Tcu1RxErr;
     : CanM_MainCanRxEemInfo.Tcu5RxErr;
     : CanM_MainCanRxEemInfo.Hcu1RxErr;
     : CanM_MainCanRxEemInfo.Hcu2RxErr;
     : CanM_MainCanRxEemInfo.Hcu3RxErr;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanTimeOutStInfo(&CanM_MainBus.CanM_MainCanTimeOutStInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanTimeOutStInfo 
     : CanM_MainCanTimeOutStInfo.MaiCanSusDet;
     : CanM_MainCanTimeOutStInfo.EmsTiOutSusDet;
     : CanM_MainCanTimeOutStInfo.TcuTiOutSusDet;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainCanRxInfo(&CanM_MainBus.CanM_MainCanRxInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxInfo 
     : CanM_MainCanRxInfo.MainBusOffFlag;
     : CanM_MainCanRxInfo.SenBusOffFlag;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainRxMsgOkFlgInfo(&CanM_MainBus.CanM_MainRxMsgOkFlgInfo);
    /*==============================================================================
    * Members of structure CanM_MainRxMsgOkFlgInfo 
     : CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/
    
    CanM_Main_Read_CanM_MainEcuModeSts(&CanM_MainBus.CanM_MainEcuModeSts);
    CanM_Main_Read_CanM_MainIgnOnOffSts(&CanM_MainBus.CanM_MainIgnOnOffSts);
    CanM_Main_Read_CanM_MainIgnEdgeSts(&CanM_MainBus.CanM_MainIgnEdgeSts);
    CanM_Main_Read_CanM_MainVBatt1Mon(&CanM_MainBus.CanM_MainVBatt1Mon);
    CanM_Main_Read_CanM_MainDiagClrSrs(&CanM_MainBus.CanM_MainDiagClrSrs);
    CanM_Main_Read_CanM_MainArbVlvDriveState(&CanM_MainBus.CanM_MainArbVlvDriveState);
    CanM_Main_Read_CanM_MainCEMon(&CanM_MainBus.CanM_MainCEMon);
    CanM_Main_Read_CanM_MainCanRxGearSelDispErrInfo(&CanM_MainBus.CanM_MainCanRxGearSelDispErrInfo);
    CanM_Main_Read_CanM_MainMtrArbDriveState(&CanM_MainBus.CanM_MainMtrArbDriveState);

    /* Process */
	CanM_Process(&CanM_MainBus);
    /* Output */
    CanM_Main_Write_CanM_MainCanMonData(&CanM_MainBus.CanM_MainCanMonData);
    /*==============================================================================
    * Members of structure CanM_MainCanMonData 
     : CanM_MainCanMonData.CanM_MainBusOff_Err;
     : CanM_MainCanMonData.CanM_SubBusOff_Err;
     : CanM_MainCanMonData.CanM_MainOverRun_Err;
     : CanM_MainCanMonData.CanM_SubOverRun_Err;
     : CanM_MainCanMonData.CanM_EMS1_Tout_Err;
     : CanM_MainCanMonData.CanM_EMS2_Tout_Err;
     : CanM_MainCanMonData.CanM_TCU1_Tout_Err;
     : CanM_MainCanMonData.CanM_TCU5_Tout_Err;
     : CanM_MainCanMonData.CanM_FWD1_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU1_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU2_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU3_Tout_Err;
     : CanM_MainCanMonData.CanM_VSM2_Tout_Err;
     : CanM_MainCanMonData.CanM_EMS_Invalid_Err;
     : CanM_MainCanMonData.CanM_TCU_Invalid_Err;
     =============================================================================*/
    

    CanM_Main_Timer_Elapsed = STM0_TIM0.U - CanM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define CANM_MAIN_STOP_SEC_CODE
#include "CanM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
