/**
 * @defgroup CanM_Cal CanM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define CANM_START_SEC_CALIB_UNSPECIFIED
#include "CanM_MemMap.h"
/* Global Calibration Section */


#define CANM_STOP_SEC_CALIB_UNSPECIFIED
#include "CanM_MemMap.h"

#define CANM_START_SEC_CONST_UNSPECIFIED
#include "CanM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define CANM_STOP_SEC_CONST_UNSPECIFIED
#include "CanM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define CANM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_START_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANM_STOP_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
#define CANM_START_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANM_STOP_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_START_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/** Variable Section (32BIT)**/


#define CANM_STOP_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define CANM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_START_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANM_STOP_SEC_VAR_NOINIT_32BIT
#include "CanM_MemMap.h"
#define CANM_START_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANM_STOP_SEC_VAR_UNSPECIFIED
#include "CanM_MemMap.h"
#define CANM_START_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/** Variable Section (32BIT)**/


#define CANM_STOP_SEC_VAR_32BIT
#include "CanM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define CANM_START_SEC_CODE
#include "CanM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define CANM_STOP_SEC_CODE
#include "CanM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
