/**
 * @defgroup CanM_Main CanM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef CANM_MAIN_H_
#define CANM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanM_Types.h"
#include "CanM_Cfg.h"
#include "CanM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define CANM_MAIN_MODULE_ID      (0)
 #define CANM_MAIN_MAJOR_VERSION  (2)
 #define CANM_MAIN_MINOR_VERSION  (0)
 #define CANM_MAIN_PATCH_VERSION  (0)
 #define CANM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern CanM_Main_HdrBusType CanM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t CanM_MainVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxRegenInfo_t CanM_MainCanRxRegenInfo;
extern Proxy_RxCanRxAccelPedlInfo_t CanM_MainCanRxAccelPedlInfo;
extern Proxy_RxCanRxIdbInfo_t CanM_MainCanRxIdbInfo;
extern Proxy_RxCanRxEngTempInfo_t CanM_MainCanRxEngTempInfo;
extern Proxy_RxCanRxEscInfo_t CanM_MainCanRxEscInfo;
extern Wss_SenWhlSpdInfo_t CanM_MainWhlSpdInfo;
extern Proxy_RxCanRxEemInfo_t CanM_MainCanRxEemInfo;
extern Eem_SuspcDetnCanTimeOutStInfo_t CanM_MainCanTimeOutStInfo;
extern Proxy_RxCanRxInfo_t CanM_MainCanRxInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t CanM_MainRxMsgOkFlgInfo;
extern Mom_HndlrEcuModeSts_t CanM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t CanM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t CanM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t CanM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t CanM_MainDiagClrSrs;
extern Arbitrator_VlvArbVlvDriveState_t CanM_MainArbVlvDriveState;
extern Ioc_InputSR1msCEMon_t CanM_MainCEMon;
extern Proxy_RxCanRxGearSelDispErrInfo_t CanM_MainCanRxGearSelDispErrInfo;
extern Arbitrator_MtrMtrArbDriveState_t CanM_MainMtrArbDriveState;

/* Output Data Element */
extern CanM_MainCanMonData_t CanM_MainCanMonData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void CanM_Main_Init(void);
extern void CanM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* CANM_MAIN_H_ */
/** @} */
