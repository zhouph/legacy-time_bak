/**
 * @defgroup CanM_MemMap CanM_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef CANM_MEMMAP_H_
#define CANM_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (CANM_START_SEC_CODE)
  #undef CANM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_CODE)
  #undef CANM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_CODE_STARTED
    #error "CANM_SEC_CODE not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (CANM_START_SEC_CONST_UNSPECIFIED)
  #undef CANM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_CONST_UNSPECIFIED)
  #undef CANM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_CONST_UNSPECIFIED_STARTED
    #error "CANM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (CANM_START_SEC_VAR_UNSPECIFIED)
  #undef CANM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_VAR_UNSPECIFIED)
  #undef CANM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_VAR_UNSPECIFIED_STARTED
    #error "CANM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_START_SEC_VAR_32BIT)
  #undef CANM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_VAR_32BIT)
  #undef CANM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_VAR_32BIT_STARTED
    #error "CANM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef CANM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef CANM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "CANM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_START_SEC_VAR_NOINIT_32BIT)
  #undef CANM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef CANM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "CANM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (CANM_START_SEC_CALIB_UNSPECIFIED)
  #undef CANM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_SEC_STARTED
    #error "CANM section not closed"
  #endif
  #define CHK_CANM_SEC_STARTED
  #define CHK_CANM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef CANM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "CANM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_SEC_STARTED
  #undef CHK_CANM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (CANM_MAIN_START_SEC_CODE)
  #undef CANM_MAIN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_CODE)
  #undef CANM_MAIN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_CODE_STARTED
    #error "CANM_MAIN_SEC_CODE not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (CANM_MAIN_START_SEC_CONST_UNSPECIFIED)
  #undef CANM_MAIN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_CONST_UNSPECIFIED)
  #undef CANM_MAIN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
    #error "CANM_MAIN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (CANM_MAIN_START_SEC_VAR_UNSPECIFIED)
  #undef CANM_MAIN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_VAR_UNSPECIFIED)
  #undef CANM_MAIN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
    #error "CANM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_START_SEC_VAR_32BIT)
  #undef CANM_MAIN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_VAR_32BIT)
  #undef CANM_MAIN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_VAR_32BIT_STARTED
    #error "CANM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef CANM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef CANM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "CANM_MAIN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_START_SEC_VAR_NOINIT_32BIT)
  #undef CANM_MAIN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef CANM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "CANM_MAIN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (CANM_MAIN_START_SEC_CALIB_UNSPECIFIED)
  #undef CANM_MAIN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_CANM_MAIN_SEC_STARTED
    #error "CANM_MAIN section not closed"
  #endif
  #define CHK_CANM_MAIN_SEC_STARTED
  #define CHK_CANM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (CANM_MAIN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef CANM_MAIN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_CANM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "CANM_MAIN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_CANM_MAIN_SEC_STARTED
  #undef CHK_CANM_MAIN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* CANM_MEMMAP_H_ */
/** @} */
