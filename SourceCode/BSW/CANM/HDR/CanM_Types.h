/**
 * @defgroup CanM_Types CanM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef CANM_TYPES_H_
#define CANM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxRegenInfo_t CanM_MainCanRxRegenInfo;
    Proxy_RxCanRxAccelPedlInfo_t CanM_MainCanRxAccelPedlInfo;
    Proxy_RxCanRxIdbInfo_t CanM_MainCanRxIdbInfo;
    Proxy_RxCanRxEngTempInfo_t CanM_MainCanRxEngTempInfo;
    Proxy_RxCanRxEscInfo_t CanM_MainCanRxEscInfo;
    Wss_SenWhlSpdInfo_t CanM_MainWhlSpdInfo;
    Proxy_RxCanRxEemInfo_t CanM_MainCanRxEemInfo;
    Eem_SuspcDetnCanTimeOutStInfo_t CanM_MainCanTimeOutStInfo;
    Proxy_RxCanRxInfo_t CanM_MainCanRxInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t CanM_MainRxMsgOkFlgInfo;
    Mom_HndlrEcuModeSts_t CanM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t CanM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t CanM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t CanM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t CanM_MainDiagClrSrs;
    Arbitrator_VlvArbVlvDriveState_t CanM_MainArbVlvDriveState;
    Ioc_InputSR1msCEMon_t CanM_MainCEMon;
    Proxy_RxCanRxGearSelDispErrInfo_t CanM_MainCanRxGearSelDispErrInfo;
    Arbitrator_MtrMtrArbDriveState_t CanM_MainMtrArbDriveState;

/* Output Data Element */
    CanM_MainCanMonData_t CanM_MainCanMonData;
}CanM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* CANM_TYPES_H_ */
/** @} */
