CanM_MainCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
CanM_MainCanRxRegenInfo = CreateBus(CanM_MainCanRxRegenInfo, DeList);
clear DeList;

CanM_MainCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
CanM_MainCanRxAccelPedlInfo = CreateBus(CanM_MainCanRxAccelPedlInfo, DeList);
clear DeList;

CanM_MainCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
CanM_MainCanRxIdbInfo = CreateBus(CanM_MainCanRxIdbInfo, DeList);
clear DeList;

CanM_MainCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
CanM_MainCanRxEngTempInfo = CreateBus(CanM_MainCanRxEngTempInfo, DeList);
clear DeList;

CanM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
CanM_MainCanRxEscInfo = CreateBus(CanM_MainCanRxEscInfo, DeList);
clear DeList;

CanM_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
CanM_MainWhlSpdInfo = CreateBus(CanM_MainWhlSpdInfo, DeList);
clear DeList;

CanM_MainCanRxEemInfo = Simulink.Bus;
DeList={
    'YawRateInvld'
    'AyInvld'
    'SteeringAngleRxOk'
    'AxInvldData'
    'Ems1RxErr'
    'Ems2RxErr'
    'Tcu1RxErr'
    'Tcu5RxErr'
    'Hcu1RxErr'
    'Hcu2RxErr'
    'Hcu3RxErr'
    };
CanM_MainCanRxEemInfo = CreateBus(CanM_MainCanRxEemInfo, DeList);
clear DeList;

CanM_MainCanTimeOutStInfo = Simulink.Bus;
DeList={
    'MaiCanSusDet'
    'EmsTiOutSusDet'
    'TcuTiOutSusDet'
    };
CanM_MainCanTimeOutStInfo = CreateBus(CanM_MainCanTimeOutStInfo, DeList);
clear DeList;

CanM_MainCanRxInfo = Simulink.Bus;
DeList={
    'MainBusOffFlag'
    'SenBusOffFlag'
    };
CanM_MainCanRxInfo = CreateBus(CanM_MainCanRxInfo, DeList);
clear DeList;

CanM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
CanM_MainRxMsgOkFlgInfo = CreateBus(CanM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

CanM_MainEcuModeSts = Simulink.Bus;
DeList={'CanM_MainEcuModeSts'};
CanM_MainEcuModeSts = CreateBus(CanM_MainEcuModeSts, DeList);
clear DeList;

CanM_MainIgnOnOffSts = Simulink.Bus;
DeList={'CanM_MainIgnOnOffSts'};
CanM_MainIgnOnOffSts = CreateBus(CanM_MainIgnOnOffSts, DeList);
clear DeList;

CanM_MainIgnEdgeSts = Simulink.Bus;
DeList={'CanM_MainIgnEdgeSts'};
CanM_MainIgnEdgeSts = CreateBus(CanM_MainIgnEdgeSts, DeList);
clear DeList;

CanM_MainVBatt1Mon = Simulink.Bus;
DeList={'CanM_MainVBatt1Mon'};
CanM_MainVBatt1Mon = CreateBus(CanM_MainVBatt1Mon, DeList);
clear DeList;

CanM_MainDiagClrSrs = Simulink.Bus;
DeList={'CanM_MainDiagClrSrs'};
CanM_MainDiagClrSrs = CreateBus(CanM_MainDiagClrSrs, DeList);
clear DeList;

CanM_MainArbVlvDriveState = Simulink.Bus;
DeList={'CanM_MainArbVlvDriveState'};
CanM_MainArbVlvDriveState = CreateBus(CanM_MainArbVlvDriveState, DeList);
clear DeList;

CanM_MainCEMon = Simulink.Bus;
DeList={'CanM_MainCEMon'};
CanM_MainCEMon = CreateBus(CanM_MainCEMon, DeList);
clear DeList;

CanM_MainCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'CanM_MainCanRxGearSelDispErrInfo'};
CanM_MainCanRxGearSelDispErrInfo = CreateBus(CanM_MainCanRxGearSelDispErrInfo, DeList);
clear DeList;

CanM_MainMtrArbDriveState = Simulink.Bus;
DeList={'CanM_MainMtrArbDriveState'};
CanM_MainMtrArbDriveState = CreateBus(CanM_MainMtrArbDriveState, DeList);
clear DeList;

CanM_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    'CanM_SubBusOff_Err'
    'CanM_MainOverRun_Err'
    'CanM_SubOverRun_Err'
    'CanM_EMS1_Tout_Err'
    'CanM_EMS2_Tout_Err'
    'CanM_TCU1_Tout_Err'
    'CanM_TCU5_Tout_Err'
    'CanM_FWD1_Tout_Err'
    'CanM_HCU1_Tout_Err'
    'CanM_HCU2_Tout_Err'
    'CanM_HCU3_Tout_Err'
    'CanM_VSM2_Tout_Err'
    'CanM_EMS_Invalid_Err'
    'CanM_TCU_Invalid_Err'
    };
CanM_MainCanMonData = CreateBus(CanM_MainCanMonData, DeList);
clear DeList;

