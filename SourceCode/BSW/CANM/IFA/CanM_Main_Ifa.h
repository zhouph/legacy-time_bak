/**
 * @defgroup CanM_Main_Ifa CanM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef CANM_MAIN_IFA_H_
#define CANM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define CanM_Main_Read_CanM_MainCanRxRegenInfo(data) do \
{ \
    *data = CanM_MainCanRxRegenInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxAccelPedlInfo(data) do \
{ \
    *data = CanM_MainCanRxAccelPedlInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEngTempInfo(data) do \
{ \
    *data = CanM_MainCanRxEngTempInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo(data) do \
{ \
    *data = CanM_MainCanRxEscInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainWhlSpdInfo(data) do \
{ \
    *data = CanM_MainWhlSpdInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo(data) do \
{ \
    *data = CanM_MainCanRxEemInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanTimeOutStInfo(data) do \
{ \
    *data = CanM_MainCanTimeOutStInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxInfo(data) do \
{ \
    *data = CanM_MainCanRxInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxRegenInfo_HcuRegenEna(data) do \
{ \
    *data = CanM_MainCanRxRegenInfo.HcuRegenEna; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxRegenInfo_HcuRegenBrkTq(data) do \
{ \
    *data = CanM_MainCanRxRegenInfo.HcuRegenBrkTq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = CanM_MainCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    *data = CanM_MainCanRxAccelPedlInfo.AccelPedlValErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_EngMsgFault(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.EngMsgFault; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_TarGearPosi(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.TarGearPosi; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_GearSelDisp(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.GearSelDisp; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_TcuSwiGs(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.TcuSwiGs; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_HcuServiceMod(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.HcuServiceMod; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_SubCanBusSigFault(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.SubCanBusSigFault; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_CoolantTemp(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.CoolantTemp; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxIdbInfo_CoolantTempErr(data) do \
{ \
    *data = CanM_MainCanRxIdbInfo.CoolantTempErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEngTempInfo_EngTemp(data) do \
{ \
    *data = CanM_MainCanRxEngTempInfo.EngTemp; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEngTempInfo_EngTempErr(data) do \
{ \
    *data = CanM_MainCanRxEngTempInfo.EngTempErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_Ax(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.Ax; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_YawRate(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.YawRate; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.SteeringAngle; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_Ay(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.Ay; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_PbSwt(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.PbSwt; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_ClutchSwt(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.ClutchSwt; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_GearRSwt(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.GearRSwt; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngActIndTq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngActIndTq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngRpm(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngRpm; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngIndTq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngIndTq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngFrictionLossTq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngFrictionLossTq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngStdTq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngStdTq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TurbineRpm(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TurbineRpm; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_ThrottleAngle(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.ThrottleAngle; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TpsResol1000(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TpsResol1000; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_PvAvCanResol1000(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.PvAvCanResol1000; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngChr(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngChr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngVol(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngVol; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_GearType(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.GearType; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngClutchState(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngClutchState; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngTqCmdBeforeIntv(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_MotEstTq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.MotEstTq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_MotTqCmdBeforeIntv(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TqIntvTCU(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TqIntvTCU; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TqIntvSlowTCU(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TqIntvSlowTCU; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TqIncReq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TqIncReq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_DecelReq(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.DecelReq; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_RainSnsStat(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.RainSnsStat; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_EngSpdErr(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.EngSpdErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_AtType(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.AtType; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_MtType(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.MtType; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_CvtType(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.CvtType; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_DctType(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.DctType; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_HevAtTcu(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.HevAtTcu; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TurbineRpmErr(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TurbineRpmErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_ThrottleAngleErr(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.ThrottleAngleErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TopTrvlCltchSwtAct(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TopTrvlCltchSwtActV(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtAct(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtActV(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_WiperIntSW(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.WiperIntSW; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_WiperLow(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.WiperLow; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_WiperHigh(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.WiperHigh; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_WiperValid(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.WiperValid; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_WiperAuto(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.WiperAuto; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEscInfo_TcuFaultSts(data) do \
{ \
    *data = CanM_MainCanRxEscInfo.TcuFaultSts; \
}while(0);

#define CanM_Main_Read_CanM_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = CanM_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define CanM_Main_Read_CanM_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = CanM_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define CanM_Main_Read_CanM_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = CanM_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define CanM_Main_Read_CanM_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = CanM_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_YawRateInvld(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.YawRateInvld; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_AyInvld(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.AyInvld; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_SteeringAngleRxOk(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.SteeringAngleRxOk; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_AxInvldData(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.AxInvldData; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Ems1RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Ems1RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Ems2RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Ems2RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Tcu1RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Tcu1RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Tcu5RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Tcu5RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Hcu1RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Hcu1RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Hcu2RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Hcu2RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxEemInfo_Hcu3RxErr(data) do \
{ \
    *data = CanM_MainCanRxEemInfo.Hcu3RxErr; \
}while(0);

#define CanM_Main_Read_CanM_MainCanTimeOutStInfo_MaiCanSusDet(data) do \
{ \
    *data = CanM_MainCanTimeOutStInfo.MaiCanSusDet; \
}while(0);

#define CanM_Main_Read_CanM_MainCanTimeOutStInfo_EmsTiOutSusDet(data) do \
{ \
    *data = CanM_MainCanTimeOutStInfo.EmsTiOutSusDet; \
}while(0);

#define CanM_Main_Read_CanM_MainCanTimeOutStInfo_TcuTiOutSusDet(data) do \
{ \
    *data = CanM_MainCanTimeOutStInfo.TcuTiOutSusDet; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxInfo_MainBusOffFlag(data) do \
{ \
    *data = CanM_MainCanRxInfo.MainBusOffFlag; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxInfo_SenBusOffFlag(data) do \
{ \
    *data = CanM_MainCanRxInfo.SenBusOffFlag; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Bms1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Tcu6MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Tcu5MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Tcu1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_SasMsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Mcu2MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Mcu1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Hcu5MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Hcu3MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Hcu2MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Hcu1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Fatc1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Ems3MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Ems2MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Ems1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Clu2MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainRxMsgOkFlgInfo_Clu1MsgOkFlg(data) do \
{ \
    *data = CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg; \
}while(0);

#define CanM_Main_Read_CanM_MainEcuModeSts(data) do \
{ \
    *data = CanM_MainEcuModeSts; \
}while(0);

#define CanM_Main_Read_CanM_MainIgnOnOffSts(data) do \
{ \
    *data = CanM_MainIgnOnOffSts; \
}while(0);

#define CanM_Main_Read_CanM_MainIgnEdgeSts(data) do \
{ \
    *data = CanM_MainIgnEdgeSts; \
}while(0);

#define CanM_Main_Read_CanM_MainVBatt1Mon(data) do \
{ \
    *data = CanM_MainVBatt1Mon; \
}while(0);

#define CanM_Main_Read_CanM_MainDiagClrSrs(data) do \
{ \
    *data = CanM_MainDiagClrSrs; \
}while(0);

#define CanM_Main_Read_CanM_MainArbVlvDriveState(data) do \
{ \
    *data = CanM_MainArbVlvDriveState; \
}while(0);

#define CanM_Main_Read_CanM_MainCEMon(data) do \
{ \
    *data = CanM_MainCEMon; \
}while(0);

#define CanM_Main_Read_CanM_MainCanRxGearSelDispErrInfo(data) do \
{ \
    *data = CanM_MainCanRxGearSelDispErrInfo; \
}while(0);

#define CanM_Main_Read_CanM_MainMtrArbDriveState(data) do \
{ \
    *data = CanM_MainMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define CanM_Main_Write_CanM_MainCanMonData(data) do \
{ \
    CanM_MainCanMonData = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define CanM_Main_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* CANM_MAIN_IFA_H_ */
/** @} */
