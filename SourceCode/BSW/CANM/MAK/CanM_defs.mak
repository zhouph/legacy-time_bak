# \file
#
# \brief CanM
#
# This file contains the implementation of the SWC
# module CanM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

CanM_CORE_PATH     := $(MANDO_BSW_ROOT)\CanM
CanM_CAL_PATH      := $(CanM_CORE_PATH)\CAL\$(CanM_VARIANT)
CanM_SRC_PATH      := $(CanM_CORE_PATH)\SRC
CanM_CFG_PATH      := $(CanM_CORE_PATH)\CFG\$(CanM_VARIANT)
CanM_HDR_PATH      := $(CanM_CORE_PATH)\HDR
CanM_IFA_PATH      := $(CanM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    CanM_CMN_PATH      := $(CanM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	CanM_UT_PATH		:= $(CanM_CORE_PATH)\UT
	CanM_UNITY_PATH	:= $(CanM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(CanM_UT_PATH)
	CC_INCLUDE_PATH		+= $(CanM_UNITY_PATH)
	CanM_Main_PATH 	:= CanM_UT_PATH\CanM_Main
endif
CC_INCLUDE_PATH    += $(CanM_CAL_PATH)
CC_INCLUDE_PATH    += $(CanM_SRC_PATH)
CC_INCLUDE_PATH    += $(CanM_CFG_PATH)
CC_INCLUDE_PATH    += $(CanM_HDR_PATH)
CC_INCLUDE_PATH    += $(CanM_IFA_PATH)
CC_INCLUDE_PATH    += $(CanM_CMN_PATH)

