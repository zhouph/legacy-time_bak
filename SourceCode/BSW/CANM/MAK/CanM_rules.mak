# \file
#
# \brief CanM
#
# This file contains the implementation of the SWC
# module CanM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += CanM_src

CanM_src_FILES        += $(CanM_SRC_PATH)\CanM_Main.c
CanM_src_FILES        += $(CanM_IFA_PATH)\CanM_Main_Ifa.c
CanM_src_FILES        += $(CanM_CFG_PATH)\CanM_Cfg.c
CanM_src_FILES        += $(CanM_CAL_PATH)\CanM_Cal.c
CanM_src_FILES        += $(CanM_SRC_PATH)\CanM_Process.c
ifeq ($(ICE_COMPILE),true)
CanM_src_FILES        += $(CanM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	CanM_src_FILES        += $(CanM_UNITY_PATH)\unity.c
	CanM_src_FILES        += $(CanM_UNITY_PATH)\unity_fixture.c	
	CanM_src_FILES        += $(CanM_UT_PATH)\main.c
	CanM_src_FILES        += $(CanM_UT_PATH)\CanM_Main\CanM_Main_UtMain.c
endif