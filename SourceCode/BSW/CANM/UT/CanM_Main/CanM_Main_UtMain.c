#include "unity.h"
#include "unity_fixture.h"
#include "CanM_Main.h"
#include "CanM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_CanM_MainCanRxRegenInfo_HcuRegenEna[MAX_STEP] = CANM_MAINCANRXREGENINFO_HCUREGENENA;
const Saluint16 UtInput_CanM_MainCanRxRegenInfo_HcuRegenBrkTq[MAX_STEP] = CANM_MAINCANRXREGENINFO_HCUREGENBRKTQ;
const Salsint16 UtInput_CanM_MainCanRxAccelPedlInfo_AccelPedlVal[MAX_STEP] = CANM_MAINCANRXACCELPEDLINFO_ACCELPEDLVAL;
const Saluint8 UtInput_CanM_MainCanRxAccelPedlInfo_AccelPedlValErr[MAX_STEP] = CANM_MAINCANRXACCELPEDLINFO_ACCELPEDLVALERR;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_EngMsgFault[MAX_STEP] = CANM_MAINCANRXIDBINFO_ENGMSGFAULT;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_TarGearPosi[MAX_STEP] = CANM_MAINCANRXIDBINFO_TARGEARPOSI;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_GearSelDisp[MAX_STEP] = CANM_MAINCANRXIDBINFO_GEARSELDISP;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_TcuSwiGs[MAX_STEP] = CANM_MAINCANRXIDBINFO_TCUSWIGS;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_HcuServiceMod[MAX_STEP] = CANM_MAINCANRXIDBINFO_HCUSERVICEMOD;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_SubCanBusSigFault[MAX_STEP] = CANM_MAINCANRXIDBINFO_SUBCANBUSSIGFAULT;
const Salsint16 UtInput_CanM_MainCanRxIdbInfo_CoolantTemp[MAX_STEP] = CANM_MAINCANRXIDBINFO_COOLANTTEMP;
const Saluint8 UtInput_CanM_MainCanRxIdbInfo_CoolantTempErr[MAX_STEP] = CANM_MAINCANRXIDBINFO_COOLANTTEMPERR;
const Salsint16 UtInput_CanM_MainCanRxEngTempInfo_EngTemp[MAX_STEP] = CANM_MAINCANRXENGTEMPINFO_ENGTEMP;
const Saluint8 UtInput_CanM_MainCanRxEngTempInfo_EngTempErr[MAX_STEP] = CANM_MAINCANRXENGTEMPINFO_ENGTEMPERR;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_Ax[MAX_STEP] = CANM_MAINCANRXESCINFO_AX;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_YawRate[MAX_STEP] = CANM_MAINCANRXESCINFO_YAWRATE;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_SteeringAngle[MAX_STEP] = CANM_MAINCANRXESCINFO_STEERINGANGLE;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_Ay[MAX_STEP] = CANM_MAINCANRXESCINFO_AY;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_PbSwt[MAX_STEP] = CANM_MAINCANRXESCINFO_PBSWT;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_ClutchSwt[MAX_STEP] = CANM_MAINCANRXESCINFO_CLUTCHSWT;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_GearRSwt[MAX_STEP] = CANM_MAINCANRXESCINFO_GEARRSWT;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_EngActIndTq[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGACTINDTQ;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_EngRpm[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGRPM;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_EngIndTq[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGINDTQ;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_EngFrictionLossTq[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGFRICTIONLOSSTQ;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_EngStdTq[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGSTDTQ;
const Saluint16 UtInput_CanM_MainCanRxEscInfo_TurbineRpm[MAX_STEP] = CANM_MAINCANRXESCINFO_TURBINERPM;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_ThrottleAngle[MAX_STEP] = CANM_MAINCANRXESCINFO_THROTTLEANGLE;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_TpsResol1000[MAX_STEP] = CANM_MAINCANRXESCINFO_TPSRESOL1000;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_PvAvCanResol1000[MAX_STEP] = CANM_MAINCANRXESCINFO_PVAVCANRESOL1000;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_EngChr[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGCHR;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_EngVol[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGVOL;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_GearType[MAX_STEP] = CANM_MAINCANRXESCINFO_GEARTYPE;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_EngClutchState[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGCLUTCHSTATE;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_EngTqCmdBeforeIntv[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGTQCMDBEFOREINTV;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_MotEstTq[MAX_STEP] = CANM_MAINCANRXESCINFO_MOTESTTQ;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_MotTqCmdBeforeIntv[MAX_STEP] = CANM_MAINCANRXESCINFO_MOTTQCMDBEFOREINTV;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_TqIntvTCU[MAX_STEP] = CANM_MAINCANRXESCINFO_TQINTVTCU;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_TqIntvSlowTCU[MAX_STEP] = CANM_MAINCANRXESCINFO_TQINTVSLOWTCU;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_TqIncReq[MAX_STEP] = CANM_MAINCANRXESCINFO_TQINCREQ;
const Salsint16 UtInput_CanM_MainCanRxEscInfo_DecelReq[MAX_STEP] = CANM_MAINCANRXESCINFO_DECELREQ;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_RainSnsStat[MAX_STEP] = CANM_MAINCANRXESCINFO_RAINSNSSTAT;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_EngSpdErr[MAX_STEP] = CANM_MAINCANRXESCINFO_ENGSPDERR;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_AtType[MAX_STEP] = CANM_MAINCANRXESCINFO_ATTYPE;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_MtType[MAX_STEP] = CANM_MAINCANRXESCINFO_MTTYPE;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_CvtType[MAX_STEP] = CANM_MAINCANRXESCINFO_CVTTYPE;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_DctType[MAX_STEP] = CANM_MAINCANRXESCINFO_DCTTYPE;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_HevAtTcu[MAX_STEP] = CANM_MAINCANRXESCINFO_HEVATTCU;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_TurbineRpmErr[MAX_STEP] = CANM_MAINCANRXESCINFO_TURBINERPMERR;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_ThrottleAngleErr[MAX_STEP] = CANM_MAINCANRXESCINFO_THROTTLEANGLEERR;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_TopTrvlCltchSwtAct[MAX_STEP] = CANM_MAINCANRXESCINFO_TOPTRVLCLTCHSWTACT;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_TopTrvlCltchSwtActV[MAX_STEP] = CANM_MAINCANRXESCINFO_TOPTRVLCLTCHSWTACTV;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtAct[MAX_STEP] = CANM_MAINCANRXESCINFO_HILLDESCTRLMDSWTACT;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtActV[MAX_STEP] = CANM_MAINCANRXESCINFO_HILLDESCTRLMDSWTACTV;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_WiperIntSW[MAX_STEP] = CANM_MAINCANRXESCINFO_WIPERINTSW;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_WiperLow[MAX_STEP] = CANM_MAINCANRXESCINFO_WIPERLOW;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_WiperHigh[MAX_STEP] = CANM_MAINCANRXESCINFO_WIPERHIGH;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_WiperValid[MAX_STEP] = CANM_MAINCANRXESCINFO_WIPERVALID;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_WiperAuto[MAX_STEP] = CANM_MAINCANRXESCINFO_WIPERAUTO;
const Saluint8 UtInput_CanM_MainCanRxEscInfo_TcuFaultSts[MAX_STEP] = CANM_MAINCANRXESCINFO_TCUFAULTSTS;
const Saluint16 UtInput_CanM_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = CANM_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_CanM_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = CANM_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_CanM_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = CANM_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_CanM_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = CANM_MAINWHLSPDINFO_RRLWHLSPD;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_YawRateInvld[MAX_STEP] = CANM_MAINCANRXEEMINFO_YAWRATEINVLD;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_AyInvld[MAX_STEP] = CANM_MAINCANRXEEMINFO_AYINVLD;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_SteeringAngleRxOk[MAX_STEP] = CANM_MAINCANRXEEMINFO_STEERINGANGLERXOK;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_AxInvldData[MAX_STEP] = CANM_MAINCANRXEEMINFO_AXINVLDDATA;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Ems1RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_EMS1RXERR;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Ems2RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_EMS2RXERR;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Tcu1RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_TCU1RXERR;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Tcu5RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_TCU5RXERR;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Hcu1RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_HCU1RXERR;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Hcu2RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_HCU2RXERR;
const Saluint8 UtInput_CanM_MainCanRxEemInfo_Hcu3RxErr[MAX_STEP] = CANM_MAINCANRXEEMINFO_HCU3RXERR;
const Salsint32 UtInput_CanM_MainCanTimeOutStInfo_MaiCanSusDet[MAX_STEP] = CANM_MAINCANTIMEOUTSTINFO_MAICANSUSDET;
const Salsint32 UtInput_CanM_MainCanTimeOutStInfo_EmsTiOutSusDet[MAX_STEP] = CANM_MAINCANTIMEOUTSTINFO_EMSTIOUTSUSDET;
const Salsint32 UtInput_CanM_MainCanTimeOutStInfo_TcuTiOutSusDet[MAX_STEP] = CANM_MAINCANTIMEOUTSTINFO_TCUTIOUTSUSDET;
const Saluint8 UtInput_CanM_MainCanRxInfo_MainBusOffFlag[MAX_STEP] = CANM_MAINCANRXINFO_MAINBUSOFFFLAG;
const Saluint8 UtInput_CanM_MainCanRxInfo_SenBusOffFlag[MAX_STEP] = CANM_MAINCANRXINFO_SENBUSOFFFLAG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Bms1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_BMS1MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_YAWSERIALMSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_YAWACCMSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Tcu6MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_TCU6MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Tcu5MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_TCU5MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Tcu1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_TCU1MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_SasMsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_SASMSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Mcu2MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_MCU2MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Mcu1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_MCU1MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_LONGACCMSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Hcu5MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_HCU5MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Hcu3MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_HCU3MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Hcu2MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_HCU2MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Hcu1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_HCU1MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Fatc1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_FATC1MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Ems3MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_EMS3MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Ems2MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_EMS2MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Ems1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_EMS1MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Clu2MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_CLU2MSGOKFLG;
const Saluint8 UtInput_CanM_MainRxMsgOkFlgInfo_Clu1MsgOkFlg[MAX_STEP] = CANM_MAINRXMSGOKFLGINFO_CLU1MSGOKFLG;
const Mom_HndlrEcuModeSts_t UtInput_CanM_MainEcuModeSts[MAX_STEP] = CANM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_CanM_MainIgnOnOffSts[MAX_STEP] = CANM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_CanM_MainIgnEdgeSts[MAX_STEP] = CANM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_CanM_MainVBatt1Mon[MAX_STEP] = CANM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_CanM_MainDiagClrSrs[MAX_STEP] = CANM_MAINDIAGCLRSRS;
const Arbitrator_VlvArbVlvDriveState_t UtInput_CanM_MainArbVlvDriveState[MAX_STEP] = CANM_MAINARBVLVDRIVESTATE;
const Ioc_InputSR1msCEMon_t UtInput_CanM_MainCEMon[MAX_STEP] = CANM_MAINCEMON;
const Proxy_RxCanRxGearSelDispErrInfo_t UtInput_CanM_MainCanRxGearSelDispErrInfo[MAX_STEP] = CANM_MAINCANRXGEARSELDISPERRINFO;
const Arbitrator_MtrMtrArbDriveState_t UtInput_CanM_MainMtrArbDriveState[MAX_STEP] = CANM_MAINMTRARBDRIVESTATE;

const Saluint8 UtExpected_CanM_MainCanMonData_CanM_MainBusOff_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_MAINBUSOFF_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_SubBusOff_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_SUBBUSOFF_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_MainOverRun_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_MAINOVERRUN_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_SubOverRun_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_SUBOVERRUN_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_EMS1_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_EMS1_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_EMS2_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_EMS2_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_TCU1_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_TCU1_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_TCU5_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_TCU5_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_FWD1_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_FWD1_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_HCU1_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_HCU1_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_HCU2_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_HCU2_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_HCU3_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_HCU3_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_VSM2_Tout_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_VSM2_TOUT_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_EMS_Invalid_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_EMS_INVALID_ERR;
const Saluint8 UtExpected_CanM_MainCanMonData_CanM_TCU_Invalid_Err[MAX_STEP] = CANM_MAINCANMONDATA_CANM_TCU_INVALID_ERR;



TEST_GROUP(CanM_Main);
TEST_SETUP(CanM_Main)
{
    CanM_Main_Init();
}

TEST_TEAR_DOWN(CanM_Main)
{   /* Postcondition */

}

TEST(CanM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        CanM_MainCanRxRegenInfo.HcuRegenEna = UtInput_CanM_MainCanRxRegenInfo_HcuRegenEna[i];
        CanM_MainCanRxRegenInfo.HcuRegenBrkTq = UtInput_CanM_MainCanRxRegenInfo_HcuRegenBrkTq[i];
        CanM_MainCanRxAccelPedlInfo.AccelPedlVal = UtInput_CanM_MainCanRxAccelPedlInfo_AccelPedlVal[i];
        CanM_MainCanRxAccelPedlInfo.AccelPedlValErr = UtInput_CanM_MainCanRxAccelPedlInfo_AccelPedlValErr[i];
        CanM_MainCanRxIdbInfo.EngMsgFault = UtInput_CanM_MainCanRxIdbInfo_EngMsgFault[i];
        CanM_MainCanRxIdbInfo.TarGearPosi = UtInput_CanM_MainCanRxIdbInfo_TarGearPosi[i];
        CanM_MainCanRxIdbInfo.GearSelDisp = UtInput_CanM_MainCanRxIdbInfo_GearSelDisp[i];
        CanM_MainCanRxIdbInfo.TcuSwiGs = UtInput_CanM_MainCanRxIdbInfo_TcuSwiGs[i];
        CanM_MainCanRxIdbInfo.HcuServiceMod = UtInput_CanM_MainCanRxIdbInfo_HcuServiceMod[i];
        CanM_MainCanRxIdbInfo.SubCanBusSigFault = UtInput_CanM_MainCanRxIdbInfo_SubCanBusSigFault[i];
        CanM_MainCanRxIdbInfo.CoolantTemp = UtInput_CanM_MainCanRxIdbInfo_CoolantTemp[i];
        CanM_MainCanRxIdbInfo.CoolantTempErr = UtInput_CanM_MainCanRxIdbInfo_CoolantTempErr[i];
        CanM_MainCanRxEngTempInfo.EngTemp = UtInput_CanM_MainCanRxEngTempInfo_EngTemp[i];
        CanM_MainCanRxEngTempInfo.EngTempErr = UtInput_CanM_MainCanRxEngTempInfo_EngTempErr[i];
        CanM_MainCanRxEscInfo.Ax = UtInput_CanM_MainCanRxEscInfo_Ax[i];
        CanM_MainCanRxEscInfo.YawRate = UtInput_CanM_MainCanRxEscInfo_YawRate[i];
        CanM_MainCanRxEscInfo.SteeringAngle = UtInput_CanM_MainCanRxEscInfo_SteeringAngle[i];
        CanM_MainCanRxEscInfo.Ay = UtInput_CanM_MainCanRxEscInfo_Ay[i];
        CanM_MainCanRxEscInfo.PbSwt = UtInput_CanM_MainCanRxEscInfo_PbSwt[i];
        CanM_MainCanRxEscInfo.ClutchSwt = UtInput_CanM_MainCanRxEscInfo_ClutchSwt[i];
        CanM_MainCanRxEscInfo.GearRSwt = UtInput_CanM_MainCanRxEscInfo_GearRSwt[i];
        CanM_MainCanRxEscInfo.EngActIndTq = UtInput_CanM_MainCanRxEscInfo_EngActIndTq[i];
        CanM_MainCanRxEscInfo.EngRpm = UtInput_CanM_MainCanRxEscInfo_EngRpm[i];
        CanM_MainCanRxEscInfo.EngIndTq = UtInput_CanM_MainCanRxEscInfo_EngIndTq[i];
        CanM_MainCanRxEscInfo.EngFrictionLossTq = UtInput_CanM_MainCanRxEscInfo_EngFrictionLossTq[i];
        CanM_MainCanRxEscInfo.EngStdTq = UtInput_CanM_MainCanRxEscInfo_EngStdTq[i];
        CanM_MainCanRxEscInfo.TurbineRpm = UtInput_CanM_MainCanRxEscInfo_TurbineRpm[i];
        CanM_MainCanRxEscInfo.ThrottleAngle = UtInput_CanM_MainCanRxEscInfo_ThrottleAngle[i];
        CanM_MainCanRxEscInfo.TpsResol1000 = UtInput_CanM_MainCanRxEscInfo_TpsResol1000[i];
        CanM_MainCanRxEscInfo.PvAvCanResol1000 = UtInput_CanM_MainCanRxEscInfo_PvAvCanResol1000[i];
        CanM_MainCanRxEscInfo.EngChr = UtInput_CanM_MainCanRxEscInfo_EngChr[i];
        CanM_MainCanRxEscInfo.EngVol = UtInput_CanM_MainCanRxEscInfo_EngVol[i];
        CanM_MainCanRxEscInfo.GearType = UtInput_CanM_MainCanRxEscInfo_GearType[i];
        CanM_MainCanRxEscInfo.EngClutchState = UtInput_CanM_MainCanRxEscInfo_EngClutchState[i];
        CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv = UtInput_CanM_MainCanRxEscInfo_EngTqCmdBeforeIntv[i];
        CanM_MainCanRxEscInfo.MotEstTq = UtInput_CanM_MainCanRxEscInfo_MotEstTq[i];
        CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv = UtInput_CanM_MainCanRxEscInfo_MotTqCmdBeforeIntv[i];
        CanM_MainCanRxEscInfo.TqIntvTCU = UtInput_CanM_MainCanRxEscInfo_TqIntvTCU[i];
        CanM_MainCanRxEscInfo.TqIntvSlowTCU = UtInput_CanM_MainCanRxEscInfo_TqIntvSlowTCU[i];
        CanM_MainCanRxEscInfo.TqIncReq = UtInput_CanM_MainCanRxEscInfo_TqIncReq[i];
        CanM_MainCanRxEscInfo.DecelReq = UtInput_CanM_MainCanRxEscInfo_DecelReq[i];
        CanM_MainCanRxEscInfo.RainSnsStat = UtInput_CanM_MainCanRxEscInfo_RainSnsStat[i];
        CanM_MainCanRxEscInfo.EngSpdErr = UtInput_CanM_MainCanRxEscInfo_EngSpdErr[i];
        CanM_MainCanRxEscInfo.AtType = UtInput_CanM_MainCanRxEscInfo_AtType[i];
        CanM_MainCanRxEscInfo.MtType = UtInput_CanM_MainCanRxEscInfo_MtType[i];
        CanM_MainCanRxEscInfo.CvtType = UtInput_CanM_MainCanRxEscInfo_CvtType[i];
        CanM_MainCanRxEscInfo.DctType = UtInput_CanM_MainCanRxEscInfo_DctType[i];
        CanM_MainCanRxEscInfo.HevAtTcu = UtInput_CanM_MainCanRxEscInfo_HevAtTcu[i];
        CanM_MainCanRxEscInfo.TurbineRpmErr = UtInput_CanM_MainCanRxEscInfo_TurbineRpmErr[i];
        CanM_MainCanRxEscInfo.ThrottleAngleErr = UtInput_CanM_MainCanRxEscInfo_ThrottleAngleErr[i];
        CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct = UtInput_CanM_MainCanRxEscInfo_TopTrvlCltchSwtAct[i];
        CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV = UtInput_CanM_MainCanRxEscInfo_TopTrvlCltchSwtActV[i];
        CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct = UtInput_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtAct[i];
        CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV = UtInput_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtActV[i];
        CanM_MainCanRxEscInfo.WiperIntSW = UtInput_CanM_MainCanRxEscInfo_WiperIntSW[i];
        CanM_MainCanRxEscInfo.WiperLow = UtInput_CanM_MainCanRxEscInfo_WiperLow[i];
        CanM_MainCanRxEscInfo.WiperHigh = UtInput_CanM_MainCanRxEscInfo_WiperHigh[i];
        CanM_MainCanRxEscInfo.WiperValid = UtInput_CanM_MainCanRxEscInfo_WiperValid[i];
        CanM_MainCanRxEscInfo.WiperAuto = UtInput_CanM_MainCanRxEscInfo_WiperAuto[i];
        CanM_MainCanRxEscInfo.TcuFaultSts = UtInput_CanM_MainCanRxEscInfo_TcuFaultSts[i];
        CanM_MainWhlSpdInfo.FlWhlSpd = UtInput_CanM_MainWhlSpdInfo_FlWhlSpd[i];
        CanM_MainWhlSpdInfo.FrWhlSpd = UtInput_CanM_MainWhlSpdInfo_FrWhlSpd[i];
        CanM_MainWhlSpdInfo.RlWhlSpd = UtInput_CanM_MainWhlSpdInfo_RlWhlSpd[i];
        CanM_MainWhlSpdInfo.RrlWhlSpd = UtInput_CanM_MainWhlSpdInfo_RrlWhlSpd[i];
        CanM_MainCanRxEemInfo.YawRateInvld = UtInput_CanM_MainCanRxEemInfo_YawRateInvld[i];
        CanM_MainCanRxEemInfo.AyInvld = UtInput_CanM_MainCanRxEemInfo_AyInvld[i];
        CanM_MainCanRxEemInfo.SteeringAngleRxOk = UtInput_CanM_MainCanRxEemInfo_SteeringAngleRxOk[i];
        CanM_MainCanRxEemInfo.AxInvldData = UtInput_CanM_MainCanRxEemInfo_AxInvldData[i];
        CanM_MainCanRxEemInfo.Ems1RxErr = UtInput_CanM_MainCanRxEemInfo_Ems1RxErr[i];
        CanM_MainCanRxEemInfo.Ems2RxErr = UtInput_CanM_MainCanRxEemInfo_Ems2RxErr[i];
        CanM_MainCanRxEemInfo.Tcu1RxErr = UtInput_CanM_MainCanRxEemInfo_Tcu1RxErr[i];
        CanM_MainCanRxEemInfo.Tcu5RxErr = UtInput_CanM_MainCanRxEemInfo_Tcu5RxErr[i];
        CanM_MainCanRxEemInfo.Hcu1RxErr = UtInput_CanM_MainCanRxEemInfo_Hcu1RxErr[i];
        CanM_MainCanRxEemInfo.Hcu2RxErr = UtInput_CanM_MainCanRxEemInfo_Hcu2RxErr[i];
        CanM_MainCanRxEemInfo.Hcu3RxErr = UtInput_CanM_MainCanRxEemInfo_Hcu3RxErr[i];
        CanM_MainCanTimeOutStInfo.MaiCanSusDet = UtInput_CanM_MainCanTimeOutStInfo_MaiCanSusDet[i];
        CanM_MainCanTimeOutStInfo.EmsTiOutSusDet = UtInput_CanM_MainCanTimeOutStInfo_EmsTiOutSusDet[i];
        CanM_MainCanTimeOutStInfo.TcuTiOutSusDet = UtInput_CanM_MainCanTimeOutStInfo_TcuTiOutSusDet[i];
        CanM_MainCanRxInfo.MainBusOffFlag = UtInput_CanM_MainCanRxInfo_MainBusOffFlag[i];
        CanM_MainCanRxInfo.SenBusOffFlag = UtInput_CanM_MainCanRxInfo_SenBusOffFlag[i];
        CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Bms1MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Tcu6MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Tcu5MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Tcu1MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_SasMsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Mcu2MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Mcu1MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Hcu5MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Hcu3MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Hcu2MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Hcu1MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Fatc1MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Ems3MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Ems2MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Ems1MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Clu2MsgOkFlg[i];
        CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg = UtInput_CanM_MainRxMsgOkFlgInfo_Clu1MsgOkFlg[i];
        CanM_MainEcuModeSts = UtInput_CanM_MainEcuModeSts[i];
        CanM_MainIgnOnOffSts = UtInput_CanM_MainIgnOnOffSts[i];
        CanM_MainIgnEdgeSts = UtInput_CanM_MainIgnEdgeSts[i];
        CanM_MainVBatt1Mon = UtInput_CanM_MainVBatt1Mon[i];
        CanM_MainDiagClrSrs = UtInput_CanM_MainDiagClrSrs[i];
        CanM_MainArbVlvDriveState = UtInput_CanM_MainArbVlvDriveState[i];
        CanM_MainCEMon = UtInput_CanM_MainCEMon[i];
        CanM_MainCanRxGearSelDispErrInfo = UtInput_CanM_MainCanRxGearSelDispErrInfo[i];
        CanM_MainMtrArbDriveState = UtInput_CanM_MainMtrArbDriveState[i];

        CanM_Main();

        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_MainBusOff_Err, UtExpected_CanM_MainCanMonData_CanM_MainBusOff_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_SubBusOff_Err, UtExpected_CanM_MainCanMonData_CanM_SubBusOff_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_MainOverRun_Err, UtExpected_CanM_MainCanMonData_CanM_MainOverRun_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_SubOverRun_Err, UtExpected_CanM_MainCanMonData_CanM_SubOverRun_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_EMS1_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_EMS1_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_EMS2_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_EMS2_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_TCU1_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_TCU1_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_TCU5_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_TCU5_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_FWD1_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_FWD1_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_HCU1_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_HCU1_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_HCU2_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_HCU2_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_HCU3_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_HCU3_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_VSM2_Tout_Err, UtExpected_CanM_MainCanMonData_CanM_VSM2_Tout_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_EMS_Invalid_Err, UtExpected_CanM_MainCanMonData_CanM_EMS_Invalid_Err[i]);
        TEST_ASSERT_EQUAL(CanM_MainCanMonData.CanM_TCU_Invalid_Err, UtExpected_CanM_MainCanMonData_CanM_TCU_Invalid_Err[i]);
    }
}

TEST_GROUP_RUNNER(CanM_Main)
{
    RUN_TEST_CASE(CanM_Main, All);
}
