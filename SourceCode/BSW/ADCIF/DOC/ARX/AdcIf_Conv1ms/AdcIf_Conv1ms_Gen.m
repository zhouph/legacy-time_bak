AdcIf_Conv1msEcuModeSts = Simulink.Bus;
DeList={'AdcIf_Conv1msEcuModeSts'};
AdcIf_Conv1msEcuModeSts = CreateBus(AdcIf_Conv1msEcuModeSts, DeList);
clear DeList;

AdcIf_Conv1msFuncInhibitAdcifSts = Simulink.Bus;
DeList={'AdcIf_Conv1msFuncInhibitAdcifSts'};
AdcIf_Conv1msFuncInhibitAdcifSts = CreateBus(AdcIf_Conv1msFuncInhibitAdcifSts, DeList);
clear DeList;

AdcIf_Conv1msSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Ext5vMon'
    'CEMon'
    'Int5vMon'
    'CSPMon'
    'Vbatt01Mon'
    'Vbatt02Mon'
    'VddMon'
    'Vdd3Mon'
    'Vdd5Mon'
    };
AdcIf_Conv1msSwTrigPwrInfo = CreateBus(AdcIf_Conv1msSwTrigPwrInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigFspInfo = Simulink.Bus;
DeList={
    'FspAbsHMon'
    'FspAbsMon'
    'FspCbsHMon'
    'FspCbsMon'
    };
AdcIf_Conv1msSwTrigFspInfo = CreateBus(AdcIf_Conv1msSwTrigFspInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigTmpInfo = Simulink.Bus;
DeList={
    'LineTestMon'
    'TempMon'
    };
AdcIf_Conv1msSwTrigTmpInfo = CreateBus(AdcIf_Conv1msSwTrigTmpInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
AdcIf_Conv1msSwTrigMotInfo = CreateBus(AdcIf_Conv1msSwTrigMotInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigPdtInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'Pdt5vMon'
    'Pdf5vMon'
    'PdtSigMon'
    };
AdcIf_Conv1msSwTrigPdtInfo = CreateBus(AdcIf_Conv1msSwTrigPdtInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigMocInfo = Simulink.Bus;
DeList={
    'RlIMainMon'
    'RlISubMonFs'
    'RlMocNegMon'
    'RlMocPosMon'
    'RrIMainMon'
    'RrISubMonFs'
    'RrMocNegMon'
    'RrMocPosMon'
    'BFLMon'
    };
AdcIf_Conv1msSwTrigMocInfo = CreateBus(AdcIf_Conv1msSwTrigMocInfo, DeList);
clear DeList;

AdcIf_Conv1msHwTrigVlvInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'VlvCutPMon'
    'VlvCutSMon'
    'VlvSimMon'
    };
AdcIf_Conv1msHwTrigVlvInfo = CreateBus(AdcIf_Conv1msHwTrigVlvInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigEscInfo = Simulink.Bus;
DeList={
    'IF_A7_MON'
    'IF_A2_MON'
    'P1_NEG_MON'
    'IF_A6_MON'
    'MTR_FW_S_MON'
    'IF_A4_MON'
    'MTP_MON'
    'IF_A3_MON'
    'P1_POS_MON'
    'IF_A5_MON'
    'VDD_MON'
    'COOL_MON'
    'CAN_L_MON'
    'P3_NEG_MON'
    'IF_A1_MON'
    'P2_POS_MON'
    'IF_A8_MON'
    'IF_A9_MON'
    'P2_NEG_MON'
    'P3_POS_MON'
    'OP_OUT_MON'
    };
AdcIf_Conv1msSwTrigEscInfo = CreateBus(AdcIf_Conv1msSwTrigEscInfo, DeList);
clear DeList;

AdcIf_Conv1msAdcInvalid = Simulink.Bus;
DeList={'AdcIf_Conv1msAdcInvalid'};
AdcIf_Conv1msAdcInvalid = CreateBus(AdcIf_Conv1msAdcInvalid, DeList);
clear DeList;

