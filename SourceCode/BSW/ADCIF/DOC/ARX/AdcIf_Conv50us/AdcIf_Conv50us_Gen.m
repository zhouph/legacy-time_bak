AdcIf_Conv50usEcuModeSts = Simulink.Bus;
DeList={'AdcIf_Conv50usEcuModeSts'};
AdcIf_Conv50usEcuModeSts = CreateBus(AdcIf_Conv50usEcuModeSts, DeList);
clear DeList;

AdcIf_Conv50usFuncInhibitAdcifSts = Simulink.Bus;
DeList={'AdcIf_Conv50usFuncInhibitAdcifSts'};
AdcIf_Conv50usFuncInhibitAdcifSts = CreateBus(AdcIf_Conv50usFuncInhibitAdcifSts, DeList);
clear DeList;

AdcIf_Conv50usHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
AdcIf_Conv50usHwTrigMotInfo = CreateBus(AdcIf_Conv50usHwTrigMotInfo, DeList);
clear DeList;

