/**
 * @defgroup AdcIf_Cfg AdcIf_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Cfg.h"
#include "IfxVadc_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ADCIF_START_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ADCIF_STOP_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#pragma align 64
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_0[ADCIF_BUFSIZE_TT_0];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_1[ADCIF_BUFSIZE_TT_1];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_2[ADCIF_BUFSIZE_TT_2];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_3[ADCIF_BUFSIZE_TT_3];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_4[ADCIF_BUFSIZE_TT_4];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_5[ADCIF_BUFSIZE_TT_5];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_6[ADCIF_BUFSIZE_TT_6];
const AdcIf_ValueGroupType   AdcIf_DmaBufTt_7[ADCIF_BUFSIZE_TT_7];
#pragma align restore
#define ADCIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
const AdcIf_ConfigType AdcIf_Config[ADCIF_MAX_GROUP_CHANNEL_NUM] =
{
    /* ADC Group 0 : Channel 0 ~ channel 7 */
    {AdcIf_Gr0Ch0, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr0Ch1, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr0Ch2, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr0Ch3, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr0Ch4, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr0Ch5, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr0Ch6, AdcIf_UsgTimeTrigger0 , AdcIf_DmaCh4   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr0Ch7, AdcIf_UsgTimeTrigger0 , AdcIf_DmaCh4   , ADCIF_DMABUFFER_SIZE_16  },
    /* ADC Group 1 : Channel 0 ~ channel 7 */
    {AdcIf_Gr1Ch0, AdcIf_UsgTimeTrigger1 , AdcIf_DmaCh5   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr1Ch1, AdcIf_UsgTimeTrigger1 , AdcIf_DmaCh5   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr1Ch2, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr1Ch3, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr1Ch4, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr1Ch5, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr1Ch6, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr1Ch7, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    /* ADC Group 2 : Channel 0 ~ channel 7 */
    {AdcIf_Gr2Ch0, AdcIf_UsgTimeTrigger2 , AdcIf_DmaCh6   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr2Ch1, AdcIf_UsgTimeTrigger2 , AdcIf_DmaCh6   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr2Ch2, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr2Ch3, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr2Ch4, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr2Ch5, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr2Ch6, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr2Ch7, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    /* ADC Group 3 : Channel 0 ~ channel 7 */
    {AdcIf_Gr3Ch0, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch1, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch2, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch3, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch4, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch5, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch6, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr3Ch7, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    /* ADC Group 4 : Channel 0 ~ channel 7 */
    {AdcIf_Gr4Ch0, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr4Ch1, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr4Ch2, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr4Ch3, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr4Ch4, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr4Ch5, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr4Ch6, AdcIf_UsgTimeTrigger3 , AdcIf_DmaCh3   , ADCIF_DMABUFFER_SIZE_16   },
    {AdcIf_Gr4Ch7, AdcIf_UsgTimeTrigger3 , AdcIf_DmaCh3   , ADCIF_DMABUFFER_SIZE_16   },
    /* ADC Group 5 : Channel 0 ~ channel 7 */
    {AdcIf_Gr5Ch0, AdcIf_UsgTimeTrigger4 , AdcIf_DmaCh7   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr5Ch1, AdcIf_UsgTimeTrigger4 , AdcIf_DmaCh7   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr5Ch2, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr5Ch3, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr5Ch4, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr5Ch5, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr5Ch6, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr5Ch7, AdcIf_UsgNormal       , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    /* ADC Group 6 : Channel 0 ~ channel 7 */
    {AdcIf_Gr6Ch0, AdcIf_UsgNormal 	     , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch1, AdcIf_UsgNormal 	     , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch2, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch3, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch4, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch5, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch6, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr6Ch7, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    /* ADC Group 7 : Channel 0 ~ channel 7 */
    {AdcIf_Gr7Ch0, AdcIf_UsgTimeTrigger5 , AdcIf_DmaCh8   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr7Ch1, AdcIf_UsgTimeTrigger5 , AdcIf_DmaCh8   , ADCIF_DMABUFFER_SIZE_16  },
    {AdcIf_Gr7Ch2, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr7Ch3, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr7Ch4, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr7Ch5, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr7Ch6, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_Gr7Ch7, AdcIf_UsgNone         , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   },
    {AdcIf_NotUsed, AdcIf_UsgNone      , AdcIf_NoDmaCh  , ADCIF_DMABUFFER_SIZE_0   } /* Dummy */
};

/** Global Constant Section (UNSPECIFIED)**/
/* MCU ADC register address. this will be used for source address of DMA  */
const uint32 AdcIf_ResultBufferRegAddr[ADCIF_NUM_OF_GROUP][ADCIF_NUM_OF_CHANNEL] = {
   { (uint32)&VADC_G0_RES0, (uint32)&VADC_G0_RES1, (uint32)&VADC_G0_RES2, (uint32)&VADC_G0_RES3, (uint32)&VADC_G0_RES4, (uint32)&VADC_G0_RES5, (uint32)&VADC_G0_RES6, (uint32)&VADC_G0_RES7},
   { (uint32)&VADC_G1_RES0, (uint32)&VADC_G1_RES1, (uint32)&VADC_G1_RES2, (uint32)&VADC_G1_RES3, (uint32)&VADC_G1_RES4, (uint32)&VADC_G1_RES5, (uint32)&VADC_G1_RES6, (uint32)&VADC_G1_RES7},
   { (uint32)&VADC_G2_RES0, (uint32)&VADC_G2_RES1, (uint32)&VADC_G2_RES2, (uint32)&VADC_G2_RES3, (uint32)&VADC_G2_RES4, (uint32)&VADC_G2_RES5, (uint32)&VADC_G2_RES6, (uint32)&VADC_G2_RES7},
   { (uint32)&VADC_G3_RES0, (uint32)&VADC_G3_RES1, (uint32)&VADC_G3_RES2, (uint32)&VADC_G3_RES3, (uint32)&VADC_G3_RES4, (uint32)&VADC_G3_RES5, (uint32)&VADC_G3_RES6, (uint32)&VADC_G3_RES7},
   { (uint32)&VADC_G4_RES0, (uint32)&VADC_G4_RES1, (uint32)&VADC_G4_RES2, (uint32)&VADC_G4_RES3, (uint32)&VADC_G4_RES4, (uint32)&VADC_G4_RES5, (uint32)&VADC_G4_RES6, (uint32)&VADC_G4_RES7},
   { (uint32)&VADC_G5_RES0, (uint32)&VADC_G5_RES1, (uint32)&VADC_G5_RES2, (uint32)&VADC_G5_RES3, (uint32)&VADC_G5_RES4, (uint32)&VADC_G5_RES5, (uint32)&VADC_G5_RES6, (uint32)&VADC_G5_RES7},
   { (uint32)&VADC_G6_RES0, (uint32)&VADC_G6_RES1, (uint32)&VADC_G6_RES2, (uint32)&VADC_G6_RES3, (uint32)&VADC_G6_RES4, (uint32)&VADC_G6_RES5, (uint32)&VADC_G6_RES6, (uint32)&VADC_G6_RES7},
   { (uint32)&VADC_G7_RES0, (uint32)&VADC_G7_RES1, (uint32)&VADC_G7_RES2, (uint32)&VADC_G7_RES3, (uint32)&VADC_G7_RES4, (uint32)&VADC_G7_RES5, (uint32)&VADC_G7_RES6, (uint32)&VADC_G7_RES7}
};

/* Adc Group Id which is generated by Tresos ADC module */
const AdcIf_GroupType AdcIf_GroupValue_SwTrigger[ADCIF_NUM_OF_GROUP] = {
   ADCIF_GROUP0_SW,
   ADCIF_GROUP1_SW,
   ADCIF_GROUP2_SW,
   ADCIF_GROUP3_SW,
   ADCIF_GROUP4_SW,
   ADCIF_GROUP5_SW,
   ADCIF_GROUP6_SW,
   ADCIF_GROUP7_SW
};

/* Adc Group Id which is generated by Tresos ADC module */
const AdcIf_GroupType AdcIf_GroupValue_HwTrigger[ADCIF_NUM_OF_GROUP] = {
   ADCIF_GROUP0_HW,
   ADCIF_GROUP1_HW,
   ADCIF_GROUP2_HW,
   ADCIF_GROUP3_HW,
   ADCIF_GROUP4_HW,
   ADCIF_GROUP5_HW,
   ADCIF_GROUP6_HW,
   ADCIF_GROUP7_HW
};

#define ADCIF_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ADCIF_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ADCIF_START_SEC_CODE
#include "AdcIf_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ADCIF_STOP_SEC_CODE
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
