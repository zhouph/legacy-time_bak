/**
 * @defgroup AdcIf_Cfg AdcIf_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ADCIF_CFG_H_
#define ADCIF_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Types.h"
#include "Adc.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define ADCIF_DETERR                              ENABLE
#define ADCIF_DMAISR_TEST                         DISABLE





#define ADCIF_DMABUFALIGN	64 /* 64 byte*/
#define ADCIF_SIZEOFADCVALUE 2 /* 12bit resolution ==> 2byte */
#define ADCIF_DMABUF_ADD_ALIGN (ADCIF_DMABUFALIGN / ADCIF_SIZEOFADCVALUE)




#if (ADCIF_DETERR == ENABLE)
/* Det information*/
/* AdcIf Module Id */
#define ADCIF_MODULE_ID                         ((uint16)(1022U))
/* AdcIf Instance Id */
#define ADCIF_INSTANCE_ID                       ((uint8)     0U)
/* AdcIf Api Id */
#define ADCIF_API_INIT                          ((uint8) 0U)
#define ADCIF_API_DECNORMAL                     ((uint8) 1U)
#define ADCIF_API_DECTIMETRIGGER                ((uint8) 2U)
#define ADCIF_API_STARTNORMALGROUPCONVERSION    ((uint8) 3U)
#define ADCIF_API_GETVALUE                      ((uint8) 4U)
#define ADCIF_API_GETVALUEBUF                   ((uint8) 5U)
#define ADCIF_API_INITGROUP                     ((uint8) 6U)
#define ADCIF_API_LOADCONFIG                    ((uint8) 7U)
#define ADCIF_API_CHECKCONFIG                   ((uint8) 8U)
#define ADCIF_API_INITGLOBALVAL                 ((uint8) 9U)
#define ADCIF_API_INITDMA                       ((uint8)10U)
#endif





/******************************************************************************/
/*                 MCU dependent configuration                                */
/******************************************************************************/
/* Number of MCU ADC Group and channel */
#define ADCIF_NUM_OF_GROUP              8
#define ADCIF_NUM_OF_CHANNEL            8
#define ADCIF_NUM_OF_NOT_USED           1
#define ADCIF_MAX_GROUP_CHANNEL_NUM    (ADCIF_NUM_OF_GROUP*ADCIF_NUM_OF_CHANNEL+ADCIF_NUM_OF_NOT_USED) /* 64 + 1 */

/* Number of MCU DMA buffer : Num of beffer should be 2n
 * ex)  OK : 0, 1, 2, 4, 8, 16, 32 ...
 * ex) NOK : 6, 10, 12, 14         ...
 * */
#define ADCIF_DMABUFFER_SIZE_0            0
#define ADCIF_DMABUFFER_SIZE_1            1
#define ADCIF_DMABUFFER_SIZE_2            2
#define ADCIF_DMABUFFER_SIZE_4            4
#define ADCIF_DMABUFFER_SIZE_8            8
#define ADCIF_DMABUFFER_SIZE_16          16
#define ADCIF_DMABUFFER_SIZE_32          32
#define ADCIF_DMABUFFER_SIZE_64          64
#define ADCIF_DMABUFFER_SIZE_128        128
#define ADCIF_MAX_TOTAL_DMABUFFER_SIZE  512



/******************************************************************************/
/*                 MCAL ADC Module dependent config                           */
/******************************************************************************/

/* Macro from MCAL ADC module */
//#define AdcIf_ValueGroupType               Adc_ValueGroupType
//#define AdcIf_GroupType                    Adc_GroupType
#define AdcConf_AdcGroup_AdcGroup_Trig_NotUsed	0xFFFF

/* Macro from MCAL ADC module : Assign value from "AdcIf_Cfg.h" file */
#define ADCIF_GROUP0_SW                       AdcConf_AdcGroup_AdcGroup_0_SW_Trig
#define ADCIF_GROUP1_SW                       AdcConf_AdcGroup_AdcGroup_1_SW_Trig
#define ADCIF_GROUP2_SW                       AdcConf_AdcGroup_AdcGroup_2_SW_Trig
#define ADCIF_GROUP3_SW                       AdcConf_AdcGroup_AdcGroup_3_SW_Trig
#define ADCIF_GROUP4_SW                       AdcConf_AdcGroup_AdcGroup_4_SW_Trig
#define ADCIF_GROUP5_SW                       AdcConf_AdcGroup_AdcGroup_Trig_NotUsed
#define ADCIF_GROUP6_SW                       AdcConf_AdcGroup_AdcGroup_6_SW_Trig
#define ADCIF_GROUP7_SW                       AdcConf_AdcGroup_AdcGroup_Trig_NotUsed
/* Macro from MCAL ADC module : Assign value from "AdcIf_Cfg.h" file */
#define ADCIF_GROUP0_HW                       AdcConf_AdcGroup_AdcGroup_0_HW_Trig
#define ADCIF_GROUP1_HW                       AdcConf_AdcGroup_AdcGroup_1_HW_Trig
#define ADCIF_GROUP2_HW                       AdcConf_AdcGroup_AdcGroup_2_HW_Trig
#define ADCIF_GROUP3_HW                       AdcConf_AdcGroup_AdcGroup_Trig_NotUsed
#define ADCIF_GROUP4_HW                       AdcConf_AdcGroup_AdcGroup_4_HW_Trig
#define ADCIF_GROUP5_HW                       AdcConf_AdcGroup_AdcGroup_5_HW_Trig
#define ADCIF_GROUP6_HW                       AdcConf_AdcGroup_AdcGroup_Trig_NotUsed
#define ADCIF_GROUP7_HW                       AdcConf_AdcGroup_AdcGroup_7_HW_Trig

/******************************************************************************/
/*                 Application dependent config                               */
/******************************************************************************/
/* Mapping Application adc to ADC Group/Channel
 * This mapping is connected to IoHwAb Layer
 * */
#define ADC_CH_NORMAL_PDF_SIG_MON			AdcIf_Gr0Ch0 //SW Trigger
#define ADC_CH_NORMAL_5V_INT_MON			AdcIf_Gr0Ch1 //SW Trigger
#define ADC_CH_NORMAL_VDD5_MON				AdcIf_Gr0Ch2 //SW Trigger
#define ADC_CH_NORMAL_CE_MON 				AdcIf_Gr0Ch3 //SW Trigger
#define ADC_CH_NORMAL_FSP_ABS_MON 			AdcIf_Gr0Ch4 //SW Trigger
#define ADC_CH_NORMAL_VDD1_MON 				AdcIf_Gr0Ch5 //SW Trigger
#define ADC_CH_VALVE_CUTV_S_MON 			AdcIf_Gr0Ch6 //HW Trigger (62.5us)
#define ADC_CH_VALVE_CUTV_P_MON 			AdcIf_Gr0Ch7 //HW Trigger (62.5us)
#define ADC_CH_VALVE_CV_MON 				AdcIf_Gr1Ch0 //HW Trigger (62.5us) /* NZ Quick C : CV */
#define ADC_CH_VALVE_RLV_MON 				AdcIf_Gr1Ch1 //HW Trigger (62.5us) /* NZ Quick C: RLV */
#define ADC_CH_NORMAL_5V_EXT_MON			AdcIf_Gr1Ch2 //SW Trigger
#define ADC_CH_NORMAL_VBATT02_MON			AdcIf_Gr1Ch3 //SW Trigger
#define ADC_CH_NORMAL_FSP_CBS_MON 			AdcIf_Gr1Ch4 //SW Trigger
#define ADC_CH_NORMAL_PDT_5V_MON 			AdcIf_Gr1Ch5 //SW Trigger
#define ADC_CH_NORMAL_VDD3_MON 				AdcIf_Gr1Ch6 //SW Trigger
#define ADC_CH_NORMAL_PDT_SIG_MON 			AdcIf_Gr1Ch7 //SW Trigger
#define ADC_CH_VALVE_SIMV_MON 				AdcIf_Gr2Ch0 //HW Trigger (62.5us)
#define ADC_CH_VALVE_SIMV_DRN_MON           AdcIf_Gr2Ch1 //HW Trigger (62.5us)
#define ADC_CH_NORMAL_MOT_POW_MON 			AdcIf_Gr2Ch2 //SW Trigger
#define ADC_CH_NORMAL_U_OUT_MON 			AdcIf_Gr2Ch3 //SW Trigger
#define ADC_CH_NORMAL_V_OUT_MON 			AdcIf_Gr2Ch4 //SW Trigger
#define ADC_CH_NORMAL_W_OUT_MON 			AdcIf_Gr2Ch5 //SW Trigger
#define ADC_CH_NORMAL_STAR_MON 				AdcIf_Gr2Ch6 //SW Trigger
#define ADC_CH_NORMAL_VBATT01_MON			AdcIf_Gr2Ch7 //SW Trigger
#define ADC_CH_NORMAL_LINE_TEST_MON 		AdcIf_Gr3Ch0 //SW Trigger
#define ADC_CH_NORMAL_TEMP_MON 				AdcIf_Gr3Ch1 //SW Trigger
#define ADC_CH_NORMAL_RL_I_MAIN_MON 		AdcIf_Gr3Ch2 //SW Trigger
#define ADC_CH_NORMAL_FSP_ABS_HMON 			AdcIf_Gr3Ch3 //SW Trigger
#define ADC_CH_NORMAL_RR_I_MAIN_MON 		AdcIf_Gr3Ch4 //SW Trigger
#define ADC_CH_NORMAL_FSP_CBS_HMON 			AdcIf_Gr3Ch5 //SW Trigger
#define ADC_CH_NORMAL_RL_MOC_POS_MON 		AdcIf_Gr3Ch6 //SW Trigger
#define ADC_CH_NORMAL_RL_MOC_NEG_MON 		AdcIf_Gr3Ch7 //SW Trigger
#define ADC_CH_NORMAL_RR_MOC_POS_MON 		AdcIf_Gr4Ch0 //SW Trigger
#define ADC_CH_NORMAL_RR_MOC_NEG_MON 		AdcIf_Gr4Ch1 //SW Trigger
#define ADC_CH_NORMAL_RL_I_SUB_MON_FS 		AdcIf_Gr4Ch2 //SW Trigger
#define ADC_CH_NORMAL_PDF_5V_MON			AdcIf_Gr4Ch3 //SW Trigger
#define ADC_CH_NORMAL_RR_I_SUB_MON_FS 		AdcIf_Gr4Ch4 //SW Trigger
#define ADC_CH_NORMAL_CSP_MON	 			AdcIf_Gr4Ch5 //SW Trigger
#define ADC_CH_VALVE_INRR_MON               AdcIf_Gr4Ch6 //HW Trigger (62.5us)
#define ADC_CH_VALVE_INRL_MON               AdcIf_Gr4Ch7 //HW Trigger (62.5us)
#define ADC_CH_MOTOR_U_PHASE_0_MON 			AdcIf_Gr5Ch0 //HW Trigger (6.25us)
#define ADC_CH_MOTOR_V_PHASE_0_MON 			AdcIf_Gr5Ch1 //HW Trigger (6.25us)
#define ADC_CH_NORMAL_MOC_POW_MON           AdcIf_Gr6Ch0 //SW Trigger
#define ADC_CH_VALVE_BFL_MON                AdcIf_Gr6Ch1 //SW Trigger
#define ADC_CH_MOTOR_V_PHASE_1_MON 			AdcIf_Gr7Ch0 //HW Trigger (6.25us)
#define ADC_CH_MOTOR_U_PHASE_1_MON			AdcIf_Gr7Ch1 //HW Trigger (6.25us)

/* ESC */
#define ADC_CH_P2_POS_MON                   AdcIf_NotUsed
#define ADC_CH_P3_POS_MON                   AdcIf_NotUsed
#define ADC_CH_IF_A1_MON                    AdcIf_NotUsed
#define ADC_CH_P2_NEG_MON                   AdcIf_NotUsed
#define ADC_CH_P3_NEG_MON                   AdcIf_NotUsed
#define ADC_CH_CAN_L_MON                    AdcIf_NotUsed
#define ADC_CH_COOL_MON                     AdcIf_NotUsed
#define ADC_CH_VDD_MON                      AdcIf_NotUsed
#define ADC_CH_IF_A9_MON                    AdcIf_NotUsed
#define ADC_CH_IF_A8_MON                    AdcIf_NotUsed
#define ADC_CH_IF_A5_MON                    AdcIf_NotUsed
#define ADC_CH_P1_POS_MON                   AdcIf_NotUsed
#define ADC_CH_IF_A3_MON                    AdcIf_NotUsed
#define ADC_CH_IF_A4_MON                    AdcIf_NotUsed
#define ADC_CH_MTP_MON                      AdcIf_NotUsed
#define ADC_CH_MTR_FW_S_MON                 AdcIf_NotUsed
#define ADC_CH_OP_OUT_MON                   AdcIf_NotUsed
#define ADC_CH_P1_NEG_MON                   AdcIf_NotUsed
#define ADC_CH_IF_A6_MON                    AdcIf_NotUsed
#define ADC_CH_IF_A7_MON                    AdcIf_NotUsed
#define ADC_CH_IF_A2_MON                    AdcIf_NotUsed

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern const uint32                 AdcIf_ResultBufferRegAddr[ADCIF_NUM_OF_GROUP][ADCIF_NUM_OF_CHANNEL];
extern const uint32                 AdcIf_DmaAddrCtrRegValue[ADCIF_NUM_OF_GROUP];
extern const AdcIf_GroupType        AdcIf_GroupValue_SwTrigger[ADCIF_NUM_OF_GROUP];
extern const AdcIf_GroupType        AdcIf_GroupValue_HwTrigger[ADCIF_NUM_OF_GROUP];
extern const AdcIf_ConfigType       AdcIf_Config[ADCIF_MAX_GROUP_CHANNEL_NUM];

#define ADCIF_BUFSIZE_TT_0  32
#define ADCIF_BUFSIZE_TT_1  32
#define ADCIF_BUFSIZE_TT_2  32
#define ADCIF_BUFSIZE_TT_3  32
#define ADCIF_BUFSIZE_TT_4  32
#define ADCIF_BUFSIZE_TT_5  32
#define ADCIF_BUFSIZE_TT_6  32
#define ADCIF_BUFSIZE_TT_7  32

extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_0[ADCIF_BUFSIZE_TT_0];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_1[ADCIF_BUFSIZE_TT_1];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_2[ADCIF_BUFSIZE_TT_2];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_3[ADCIF_BUFSIZE_TT_3];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_4[ADCIF_BUFSIZE_TT_4];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_5[ADCIF_BUFSIZE_TT_5];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_6[ADCIF_BUFSIZE_TT_6];
extern const AdcIf_ValueGroupType   AdcIf_DmaBufTt_7[ADCIF_BUFSIZE_TT_7];
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ADCIF_CFG_H_ */
/** @} */
