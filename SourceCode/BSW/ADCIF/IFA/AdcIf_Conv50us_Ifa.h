/**
 * @defgroup AdcIf_Conv50us_Ifa AdcIf_Conv50us_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Conv50us_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ADCIF_CONV50US_IFA_H_
#define ADCIF_CONV50US_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AdcIf_Conv50us_Read_AdcIf_Conv50usEcuModeSts(data) do \
{ \
    *data = AdcIf_Conv50usEcuModeSts; \
}while(0);

#define AdcIf_Conv50us_Read_AdcIf_Conv50usFuncInhibitAdcifSts(data) do \
{ \
    *data = AdcIf_Conv50usFuncInhibitAdcifSts; \
}while(0);


/* Set Output DE MAcro Function */
#define AdcIf_Conv50us_Write_AdcIf_Conv50usHwTrigMotInfo(data) do \
{ \
    AdcIf_Conv50usHwTrigMotInfo = *data; \
}while(0);

#define AdcIf_Conv50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define AdcIf_Conv50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define AdcIf_Conv50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define AdcIf_Conv50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ADCIF_CONV50US_IFA_H_ */
/** @} */
