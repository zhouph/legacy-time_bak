/**
 * @defgroup AdcIf_Conv1ms_Ifa AdcIf_Conv1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Conv1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ADCIF_CONV1MS_IFA_H_
#define ADCIF_CONV1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AdcIf_Conv1ms_Read_AdcIf_Conv1msEcuModeSts(data) do \
{ \
    *data = AdcIf_Conv1msEcuModeSts; \
}while(0);

#define AdcIf_Conv1ms_Read_AdcIf_Conv1msFuncInhibitAdcifSts(data) do \
{ \
    *data = AdcIf_Conv1msFuncInhibitAdcifSts; \
}while(0);


/* Set Output DE MAcro Function */
#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigFspInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigFspInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigTmpInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigTmpInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigMotInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPdtInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigPdtInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo(data) do \
{ \
    AdcIf_Conv1msHwTrigVlvInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_Ext5vMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_CEMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.CEMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_Int5vMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.Int5vMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_CSPMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.CSPMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_Vbatt01Mon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_Vbatt02Mon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_VddMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.VddMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_Vdd3Mon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo_Vdd5Mon(data) do \
{ \
    AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigFspInfo_FspAbsHMon(data) do \
{ \
    AdcIf_Conv1msSwTrigFspInfo.FspAbsHMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigFspInfo_FspAbsMon(data) do \
{ \
    AdcIf_Conv1msSwTrigFspInfo.FspAbsMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigFspInfo_FspCbsHMon(data) do \
{ \
    AdcIf_Conv1msSwTrigFspInfo.FspCbsHMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigFspInfo_FspCbsMon(data) do \
{ \
    AdcIf_Conv1msSwTrigFspInfo.FspCbsMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigTmpInfo_LineTestMon(data) do \
{ \
    AdcIf_Conv1msSwTrigTmpInfo.LineTestMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigTmpInfo_TempMon(data) do \
{ \
    AdcIf_Conv1msSwTrigTmpInfo.TempMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo_MotPwrMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMotInfo.MotPwrMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo_StarMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMotInfo.StarMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo_UoutMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMotInfo.UoutMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo_VoutMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMotInfo.VoutMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo_WoutMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMotInfo.WoutMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPdtInfo_PdfSigMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPdtInfo.PdfSigMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPdtInfo_Pdt5vMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPdtInfo.Pdt5vMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPdtInfo_Pdf5vMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPdtInfo.Pdf5vMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPdtInfo_PdtSigMon(data) do \
{ \
    AdcIf_Conv1msSwTrigPdtInfo.PdtSigMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RlIMainMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RlIMainMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RlISubMonFs(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RlISubMonFs = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RlMocNegMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RlMocNegMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RlMocPosMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RlMocPosMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RrIMainMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RrIMainMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RrISubMonFs(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RrISubMonFs = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RrMocNegMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RrMocNegMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_RrMocPosMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.RrMocPosMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo_BFLMon(data) do \
{ \
    AdcIf_Conv1msSwTrigMocInfo.BFLMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo_VlvCVMon(data) do \
{ \
    AdcIf_Conv1msHwTrigVlvInfo.VlvCVMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo_VlvRLVMon(data) do \
{ \
    AdcIf_Conv1msHwTrigVlvInfo.VlvRLVMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo_VlvCutPMon(data) do \
{ \
    AdcIf_Conv1msHwTrigVlvInfo.VlvCutPMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo_VlvCutSMon(data) do \
{ \
    AdcIf_Conv1msHwTrigVlvInfo.VlvCutSMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo_VlvSimMon(data) do \
{ \
    AdcIf_Conv1msHwTrigVlvInfo.VlvSimMon = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A7_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A7_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A2_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A2_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_P1_NEG_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.P1_NEG_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A6_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A6_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_MTR_FW_S_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.MTR_FW_S_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A4_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A4_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_MTP_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.MTP_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A3_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A3_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_P1_POS_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.P1_POS_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A5_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A5_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_VDD_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.VDD_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_COOL_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.COOL_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_CAN_L_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.CAN_L_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_P3_NEG_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.P3_NEG_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A1_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A1_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_P2_POS_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.P2_POS_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A8_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A8_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_IF_A9_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.IF_A9_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_P2_NEG_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.P2_NEG_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_P3_POS_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.P3_POS_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo_OP_OUT_MON(data) do \
{ \
    AdcIf_Conv1msSwTrigEscInfo.OP_OUT_MON = *data; \
}while(0);

#define AdcIf_Conv1ms_Write_AdcIf_Conv1msAdcInvalid(data) do \
{ \
    AdcIf_Conv1msAdcInvalid = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ADCIF_CONV1MS_IFA_H_ */
/** @} */
