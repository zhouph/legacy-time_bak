/**
 * @defgroup AdcIf_Cal AdcIf_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ADCIF_START_SEC_CALIB_UNSPECIFIED
#include "AdcIf_MemMap.h"
/* Global Calibration Section */


#define ADCIF_STOP_SEC_CALIB_UNSPECIFIED
#include "AdcIf_MemMap.h"

#define ADCIF_START_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ADCIF_STOP_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ADCIF_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ADCIF_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ADCIF_START_SEC_CODE
#include "AdcIf_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ADCIF_STOP_SEC_CODE
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
