# \file
#
# \brief AdcIf
#
# This file contains the implementation of the SWC
# module AdcIf.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

AdcIf_CORE_PATH     := $(MANDO_BSW_ROOT)\AdcIf
AdcIf_CAL_PATH      := $(AdcIf_CORE_PATH)\CAL\$(AdcIf_VARIANT)
AdcIf_SRC_PATH      := $(AdcIf_CORE_PATH)\SRC
AdcIf_CFG_PATH      := $(AdcIf_CORE_PATH)\CFG\$(AdcIf_VARIANT)
AdcIf_HDR_PATH      := $(AdcIf_CORE_PATH)\HDR
AdcIf_IFA_PATH      := $(AdcIf_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    AdcIf_CMN_PATH      := $(AdcIf_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	AdcIf_UT_PATH		:= $(AdcIf_CORE_PATH)\UT
	AdcIf_UNITY_PATH	:= $(AdcIf_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(AdcIf_UT_PATH)
	CC_INCLUDE_PATH		+= $(AdcIf_UNITY_PATH)
	AdcIf_Conv50us_PATH 	:= AdcIf_UT_PATH\AdcIf_Conv50us
	AdcIf_Conv1ms_PATH 	:= AdcIf_UT_PATH\AdcIf_Conv1ms
endif
CC_INCLUDE_PATH    += $(AdcIf_CAL_PATH)
CC_INCLUDE_PATH    += $(AdcIf_SRC_PATH)
CC_INCLUDE_PATH    += $(AdcIf_CFG_PATH)
CC_INCLUDE_PATH    += $(AdcIf_HDR_PATH)
CC_INCLUDE_PATH    += $(AdcIf_IFA_PATH)
CC_INCLUDE_PATH    += $(AdcIf_CMN_PATH)

