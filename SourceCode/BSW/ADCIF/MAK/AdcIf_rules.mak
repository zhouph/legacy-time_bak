# \file
#
# \brief AdcIf
#
# This file contains the implementation of the SWC
# module AdcIf.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += AdcIf_src

AdcIf_src_FILES        += $(AdcIf_SRC_PATH)\AdcIf_Conv50us.c
AdcIf_src_FILES        += $(AdcIf_IFA_PATH)\AdcIf_Conv50us_Ifa.c
AdcIf_src_FILES        += $(AdcIf_SRC_PATH)\AdcIf_Conv1ms.c
AdcIf_src_FILES        += $(AdcIf_IFA_PATH)\AdcIf_Conv1ms_Ifa.c
AdcIf_src_FILES        += $(AdcIf_CFG_PATH)\AdcIf_Cfg.c
AdcIf_src_FILES        += $(AdcIf_CAL_PATH)\AdcIf_Cal.c

ifeq ($(ICE_COMPILE),true)
AdcIf_src_FILES        += $(AdcIf_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	AdcIf_src_FILES        += $(AdcIf_UNITY_PATH)\unity.c
	AdcIf_src_FILES        += $(AdcIf_UNITY_PATH)\unity_fixture.c	
	AdcIf_src_FILES        += $(AdcIf_UT_PATH)\main.c
	AdcIf_src_FILES        += $(AdcIf_UT_PATH)\AdcIf_Conv50us\AdcIf_Conv50us_UtMain.c
	AdcIf_src_FILES        += $(AdcIf_UT_PATH)\AdcIf_Conv1ms\AdcIf_Conv1ms_UtMain.c
endif