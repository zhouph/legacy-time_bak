/**
 * @defgroup AdcIf_Conv1ms AdcIf_Conv1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Conv1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ADCIF_CONV1MS_H_
#define ADCIF_CONV1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Types.h"
#include "AdcIf_Cfg.h"
#include "AdcIf_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ADCIF_CONV1MS_MODULE_ID      (0)
 #define ADCIF_CONV1MS_MAJOR_VERSION  (2)
 #define ADCIF_CONV1MS_MINOR_VERSION  (0)
 #define ADCIF_CONV1MS_PATCH_VERSION  (0)
 #define ADCIF_CONV1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AdcIf_Conv1ms_HdrBusType AdcIf_Conv1msBus;

/* Version Info */
extern const SwcVersionInfo_t AdcIf_Conv1msVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t AdcIf_Conv1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv1msFuncInhibitAdcifSts;

/* Output Data Element */
extern AdcIf_Conv1msSwTrigPwrInfo_t AdcIf_Conv1msSwTrigPwrInfo;
extern AdcIf_Conv1msSwTrigFspInfo_t AdcIf_Conv1msSwTrigFspInfo;
extern AdcIf_Conv1msSwTrigTmpInfo_t AdcIf_Conv1msSwTrigTmpInfo;
extern AdcIf_Conv1msSwTrigMotInfo_t AdcIf_Conv1msSwTrigMotInfo;
extern AdcIf_Conv1msSwTrigPdtInfo_t AdcIf_Conv1msSwTrigPdtInfo;
extern AdcIf_Conv1msSwTrigMocInfo_t AdcIf_Conv1msSwTrigMocInfo;
extern AdcIf_Conv1msHwTrigVlvInfo_t AdcIf_Conv1msHwTrigVlvInfo;
#if (ADCIF_VARIANT == ADCIF_VARIANT_IDB)
extern AdcIf_Conv1msSwTrigEscInfo_t AdcIf_Conv1msSwTrigEscInfo;
#endif
extern AdcIf_Conv1msAdcInvalid_t AdcIf_Conv1msAdcInvalid;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AdcIf_Conv1ms_Init(void);
extern void AdcIf_Conv1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ADCIF_CONV1MS_H_ */
/** @} */
