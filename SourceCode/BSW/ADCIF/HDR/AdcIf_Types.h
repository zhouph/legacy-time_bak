/**
 * @defgroup AdcIf_Types AdcIf_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ADCIF_TYPES_H_
#define ADCIF_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t AdcIf_Conv50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv50usFuncInhibitAdcifSts;

/* Output Data Element */
    AdcIf_Conv50usHwTrigMotInfo_t AdcIf_Conv50usHwTrigMotInfo;
}AdcIf_Conv50us_HdrBusType;

typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t AdcIf_Conv1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv1msFuncInhibitAdcifSts;

/* Output Data Element */
    AdcIf_Conv1msSwTrigPwrInfo_t AdcIf_Conv1msSwTrigPwrInfo;
    AdcIf_Conv1msSwTrigFspInfo_t AdcIf_Conv1msSwTrigFspInfo;
    AdcIf_Conv1msSwTrigTmpInfo_t AdcIf_Conv1msSwTrigTmpInfo;
    AdcIf_Conv1msSwTrigMotInfo_t AdcIf_Conv1msSwTrigMotInfo;
    AdcIf_Conv1msSwTrigPdtInfo_t AdcIf_Conv1msSwTrigPdtInfo;
    AdcIf_Conv1msSwTrigMocInfo_t AdcIf_Conv1msSwTrigMocInfo;
    AdcIf_Conv1msHwTrigVlvInfo_t AdcIf_Conv1msHwTrigVlvInfo;
    AdcIf_Conv1msSwTrigEscInfo_t AdcIf_Conv1msSwTrigEscInfo;
    AdcIf_Conv1msAdcInvalid_t AdcIf_Conv1msAdcInvalid;
}AdcIf_Conv1ms_HdrBusType;

typedef uint16 AdcIf_ValueGroupType;
typedef uint16 AdcIf_GroupType;
/* This enum is same as Dma_ChannelType in "Mcal_DmaLib.h" */
typedef enum {
  AdcIf_DmaCh0,
  AdcIf_DmaCh1,
  AdcIf_DmaCh2,
  AdcIf_DmaCh3,
  AdcIf_DmaCh4,
  AdcIf_DmaCh5,
  AdcIf_DmaCh6,
  AdcIf_DmaCh7,
  AdcIf_DmaCh8,
  AdcIf_DmaCh9,
  AdcIf_DmaCh10,
  AdcIf_DmaCh11,
  AdcIf_DmaCh12,
  AdcIf_DmaCh13,
  AdcIf_DmaCh14,
  AdcIf_DmaCh15,
  AdcIf_DmaCh16,
  AdcIf_DmaCh17,
  AdcIf_DmaCh18,
  AdcIf_DmaCh19,
  AdcIf_DmaCh20,
  AdcIf_DmaCh21,
  AdcIf_DmaCh22,
  AdcIf_DmaCh23,
  AdcIf_DmaCh24,
  AdcIf_DmaCh25,
  AdcIf_DmaCh26,
  AdcIf_DmaCh27,
  AdcIf_DmaCh28,
  AdcIf_DmaCh29,
  AdcIf_DmaCh30,
  AdcIf_DmaCh31,
  AdcIf_DmaCh32,
  AdcIf_DmaCh33,
  AdcIf_DmaCh34,
  AdcIf_DmaCh35,
  AdcIf_DmaCh36,
  AdcIf_DmaCh37,
  AdcIf_DmaCh38,
  AdcIf_DmaCh39,
  AdcIf_DmaCh40,
  AdcIf_DmaCh41,
  AdcIf_DmaCh42,
  AdcIf_DmaCh43,
  AdcIf_DmaCh44,
  AdcIf_DmaCh45,
  AdcIf_DmaCh46,
  AdcIf_DmaCh47,
  AdcIf_DmaCh48,
  AdcIf_DmaCh49,
  AdcIf_DmaCh50,
  AdcIf_DmaCh51,
  AdcIf_DmaCh52,
  AdcIf_DmaCh53,
  AdcIf_DmaCh54,
  AdcIf_DmaCh55,
  AdcIf_DmaCh56,
  AdcIf_DmaCh57,
  AdcIf_DmaCh58,
  AdcIf_DmaCh59,
  AdcIf_DmaCh60,
  AdcIf_DmaCh61,
  AdcIf_DmaCh62,
  AdcIf_DmaCh63,
  AdcIf_DmaCh64,
  AdcIf_DmaCh65,
  AdcIf_DmaCh66,
  AdcIf_DmaCh67,
  AdcIf_DmaCh68,
  AdcIf_DmaCh69,
  AdcIf_DmaCh70,
  AdcIf_DmaCh71,
  AdcIf_DmaCh72,
  AdcIf_DmaCh73,
  AdcIf_DmaCh74,
  AdcIf_DmaCh75,
  AdcIf_DmaCh76,
  AdcIf_DmaCh77,
  AdcIf_DmaCh78,
  AdcIf_DmaCh79,
  AdcIf_DmaCh80,
  AdcIf_DmaCh81,
  AdcIf_DmaCh82,
  AdcIf_DmaCh83,
  AdcIf_DmaCh84,
  AdcIf_DmaCh85,
  AdcIf_DmaCh86,
  AdcIf_DmaCh87,
  AdcIf_DmaCh88,
  AdcIf_DmaCh89,
  AdcIf_DmaCh90,
  AdcIf_DmaCh91,
  AdcIf_DmaCh92,
  AdcIf_DmaCh93,
  AdcIf_DmaCh94,
  AdcIf_DmaCh95,
  AdcIf_DmaCh96,
  AdcIf_DmaCh97,
  AdcIf_DmaCh98,
  AdcIf_DmaCh99,
  AdcIf_DmaCh100,
  AdcIf_DmaCh101,
  AdcIf_DmaCh102,
  AdcIf_DmaCh103,
  AdcIf_DmaCh104,
  AdcIf_DmaCh105,
  AdcIf_DmaCh106,
  AdcIf_DmaCh107,
  AdcIf_DmaCh108,
  AdcIf_DmaCh109,
  AdcIf_DmaCh110,
  AdcIf_DmaCh111,
  AdcIf_DmaCh112,
  AdcIf_DmaCh113,
  AdcIf_DmaCh114,
  AdcIf_DmaCh115,
  AdcIf_DmaCh116,
  AdcIf_DmaCh117,
  AdcIf_DmaCh118,
  AdcIf_DmaCh119,
  AdcIf_DmaCh120,
  AdcIf_DmaCh121,
  AdcIf_DmaCh122,
  AdcIf_DmaCh123,
  AdcIf_DmaCh124,
  AdcIf_DmaCh125,
  AdcIf_DmaCh126,
  AdcIf_DmaCh127,
  AdcIf_NoDmaCh = 0xFF /* do not change, Place holder */
}AdcIf_DmaChannelType;

/* This structure will be used for DMA register */
typedef struct{
    AdcIf_DmaChannelType   AdcIfDmaChId;
    uint32                  AdcIfDmaDestAddr;
    uint32                  AdcIfDmaTxCnt;
    uint8                   AdcIfDmaNoOfMoves;
}AdcIf_DmaSetParamType;



typedef enum
{
	/* ADC Group 0 : Channel 0 ~ channel 7 */
    AdcIf_Gr0Ch0 ,
    AdcIf_Gr0Ch1 ,
    AdcIf_Gr0Ch2 ,
    AdcIf_Gr0Ch3 ,
    AdcIf_Gr0Ch4 ,
    AdcIf_Gr0Ch5 ,
    AdcIf_Gr0Ch6 ,
    AdcIf_Gr0Ch7 ,
    /* ADC Group 1 : Channel 0 ~ channel 7 */
    AdcIf_Gr1Ch0 ,
    AdcIf_Gr1Ch1 ,
    AdcIf_Gr1Ch2 ,
    AdcIf_Gr1Ch3 ,
    AdcIf_Gr1Ch4 ,
    AdcIf_Gr1Ch5 ,
    AdcIf_Gr1Ch6 ,
    AdcIf_Gr1Ch7 ,
    /* ADC Group 2 : Channel 0 ~ channel 7 */
    AdcIf_Gr2Ch0 ,
    AdcIf_Gr2Ch1 ,
    AdcIf_Gr2Ch2 ,
    AdcIf_Gr2Ch3 ,
    AdcIf_Gr2Ch4 ,
    AdcIf_Gr2Ch5 ,
    AdcIf_Gr2Ch6 ,
    AdcIf_Gr2Ch7 ,
    /* ADC Group 3 : Channel 0 ~ channel 7 */
    AdcIf_Gr3Ch0 ,
    AdcIf_Gr3Ch1 ,
    AdcIf_Gr3Ch2 ,
    AdcIf_Gr3Ch3 ,
    AdcIf_Gr3Ch4 ,
    AdcIf_Gr3Ch5 ,
    AdcIf_Gr3Ch6 ,
    AdcIf_Gr3Ch7 ,
    /* ADC Group 4 : Channel 0 ~ channel 7 */
    AdcIf_Gr4Ch0 ,
    AdcIf_Gr4Ch1 ,
    AdcIf_Gr4Ch2 ,
    AdcIf_Gr4Ch3 ,
    AdcIf_Gr4Ch4 ,
    AdcIf_Gr4Ch5 ,
    AdcIf_Gr4Ch6 ,
    AdcIf_Gr4Ch7 ,
    /* ADC Group 5 : Channel 0 ~ channel 7 */
    AdcIf_Gr5Ch0 ,
    AdcIf_Gr5Ch1 ,
    AdcIf_Gr5Ch2 ,
    AdcIf_Gr5Ch3 ,
    AdcIf_Gr5Ch4 ,
    AdcIf_Gr5Ch5 ,
    AdcIf_Gr5Ch6 ,
    AdcIf_Gr5Ch7 ,
    /* ADC Group 6 : Channel 0 ~ channel 7 */
    AdcIf_Gr6Ch0 ,
    AdcIf_Gr6Ch1 ,
    AdcIf_Gr6Ch2 ,
    AdcIf_Gr6Ch3 ,
    AdcIf_Gr6Ch4 ,
    AdcIf_Gr6Ch5 ,
    AdcIf_Gr6Ch6 ,
    AdcIf_Gr6Ch7 ,
    /* ADC Group 7 : Channel 0 ~ channel 7 */
    AdcIf_Gr7Ch0 ,
    AdcIf_Gr7Ch1 ,
    AdcIf_Gr7Ch2 ,
    AdcIf_Gr7Ch3 ,
    AdcIf_Gr7Ch4 ,
    AdcIf_Gr7Ch5 ,
    AdcIf_Gr7Ch6 ,
    AdcIf_Gr7Ch7 ,
    /* Place hodler */
    AdcIf_NotUsed,           /* for Not Used Channel */
    AdcIf_MaxGrCh,           /* do not change, Place holder */
    AdcIf_InvalidGrCh = 0xFF /* do not change, Place holder */
}AdcIf_GroupChannelType;


typedef enum
{
    AdcIf_UsgNone,
    AdcIf_UsgNormal,
    AdcIf_UsgTimeTriggerStartIndex,	 /* do not change, Place holder */
    AdcIf_UsgTimeTrigger0 = AdcIf_UsgTimeTriggerStartIndex,  /* do not change, Place holder */
    AdcIf_UsgTimeTrigger1,
    AdcIf_UsgTimeTrigger2,
    AdcIf_UsgTimeTrigger3,
    AdcIf_UsgTimeTrigger4,
    AdcIf_UsgTimeTrigger5,
    AdcIf_UsgTimeTrigger6,
    AdcIf_UsgTimeTrigger7,
    AdcIf_UsgTimeTriggerEndIndex /* do not change, Place holder */
}AdcIf_UsageType;

/* MCAL Adc Init information */
/* Bit field meaning of AdcIfChUsgNone, AdcIfChUsgNorm and AdcIfChUsgTt
 *  Bit filed 7, 6, 5, 4, 3, 2, 1, 0
 *            |  |  |  |  |  |  |  |
 *            |  |  |  |  |  |  |  +- Channel 0
 *            |  |  |  |  |  |  +---- Channel 1
 *            |  |  |  |  |  +------- Channel 2
 *            |  |  |  |  +---------- Channel 3
 *            |  |  |  +------------- Channel 4
 *            |  |  +---------------- Channel 5
 *            |  +------------------- Channel 6
 *            +---------------------- Channel 7
 * */
typedef struct {
    uint8                  AdcIfChUsgNone; /* Adc channel which is not used at all          , each bit for each channel */
    uint8                  AdcIfChUsgNorm; /* Adc channel which is used for software trigger, each bit for each channel */
    uint8                  AdcIfChUsgTt;   /* Adc channel which is used for Hardware trigger, each bit for each channel */
    AdcIf_DmaChannelType   AdcIfDmaChId;   /* DMA channel Id for Hardware trigger           , each bit for each channel */
}AdcIf_InitInfoType;

/* Decoding information only for Adc with Hardware Trigger */
typedef struct{
    AdcIf_UsageType    AdcIfChTtType;         /* Hardware Trigger Id (0~7)                            */
    uint8              AdcIfGrId;             /* Adc Group Id                                         */
    uint8              AdcIfChStartId;        /* Hardware Trigger start channel Id in Adc Group       */
    uint8              AdcIfTtChNum;          /* Number of channels in Adc Group for Hardware Trigger */
    uint16             AdcIfTtDmaBufNum;      /* Number of DMA buffer in Adc Group                    */
    uint16             AdcIfTtTotalDmaIndex;  /* Index of DMA buffer in AdcIfTtTotalDmaBuf array      */
}AdcIf_DecTtInfoType;


/* AdcIf Error information */
typedef enum {
    AdcIf_NoConfigError,                                        /* Configuration OK                                 */
    AdcIf_Config_TimeTriggerTypeUsedInDifferentAdcGroup,        /* TimeTrigger should be used in same Adc Group     */
    AdcIf_Config_TimeTriggerTypeUsedWithDifferentDmaChannel,    /* TimeTrigger should use same Dma channel          */
    AdcIf_Config_TimeTriggerTypeUsedOverMaxNum,                 /* TimeTrigger should use same Dma channel          */
    AdcIf_Config_TimeTriggerTypeUsedAdcchannelNotSequentially,  /* TimeTrigger range is 0 ~ 7                       */
    AdcIf_Config_TimeTriggerBuffersizeIsNotEqual,               /* TimeTrigger Buffer size should be same           */
    AdcIf_Config_TimeTriggerBuffersizeIsZero,                   /* TimeTrigger Buffer size should not be 0          */
    AdcIf_Config_TimeTriggerBuffersizeIsNot2Pow,                /* Number of TimeTrigger Buffer should be 2Pow      */
    AdcIf_Config_TotalDmaSizeOverflow,                          /* Used DMA buffer size is over                     */
    AdcIf_Config_WrongAdcIfUsageType,                           /* Assign the value only defined in AdcIf_UsageType */
    AdcIfTimeTrigger_WrongTimeTriggerUsage,                     /* Wrong value of TimeTrigger Id                    */
    AdcIf_GetValue_FunctionWrongUsage,                          /* Wrong usage of Service Api "AdcIf_GetValue"      */
    AdcIf_GetValueBuf_FunctionWrongUsage,                       /* Wrong usage of Service Api "AdcIf_GetValueBuf"   */
    AdcIf_NoOfConfigError                                       /* Place holder, no more meaning                    */
}AdcIf_ConfigErrInfo;

typedef struct{
AdcIf_GroupChannelType      AdcIfGrChId;           /* 0 ~ 63 : Do not Change, MCU dependent */
AdcIf_UsageType             AdcIfUsageType;        /* Usage of Adc Channel None or Normal or TimeTrigger */
AdcIf_DmaChannelType        AdcIfDmaChId;          /* Only for TimeTrigger : Dma channel Id  */
AdcIf_ValueGroupType        AdcIfDmaBufSize;       /* Only for TimeTrigger : Dma buffer size */
}AdcIf_ConfigType;

#define ADCIF_MAX_BUF_LEN                  16
typedef struct{
  uint16   Buf[ADCIF_MAX_BUF_LEN];
  uint8    Len;
}AdcIf_ValueBufType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ADCIF_TYPES_H_ */
/** @} */
