/**
 * @defgroup AdcIf_Conv50us AdcIf_Conv50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Conv50us.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ADCIF_CONV50US_H_
#define ADCIF_CONV50US_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Types.h"
#include "AdcIf_Cfg.h"
#include "AdcIf_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ADCIF_CONV50US_MODULE_ID      (0)
 #define ADCIF_CONV50US_MAJOR_VERSION  (2)
 #define ADCIF_CONV50US_MINOR_VERSION  (0)
 #define ADCIF_CONV50US_PATCH_VERSION  (0)
 #define ADCIF_CONV50US_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AdcIf_Conv50us_HdrBusType AdcIf_Conv50usBus;

/* Version Info */
extern const SwcVersionInfo_t AdcIf_Conv50usVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t AdcIf_Conv50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv50usFuncInhibitAdcifSts;

/* Output Data Element */
extern AdcIf_Conv50usHwTrigMotInfo_t AdcIf_Conv50usHwTrigMotInfo;

extern AdcIf_ValueGroupType    AdcIfDecodingRawAdValue[ADCIF_MAX_GROUP_CHANNEL_NUM];

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AdcIf_Conv50us_Init(void);
extern void AdcIf_Conv50us(void);
inline extern void AdcIf_DecTimeTrigger(AdcIf_UsageType AdcIfUsageTypeId);
/* Start For speed optimization, separate function per Hardware trigger Id */
inline extern void AdcIf_DecTimeTrigger_0(void);
inline extern void AdcIf_DecTimeTrigger_1(void);
inline extern void AdcIf_DecTimeTrigger_2(void);
inline extern void AdcIf_DecTimeTrigger_3(void);
inline extern void AdcIf_DecTimeTrigger_4(void);
inline extern void AdcIf_DecTimeTrigger_5(void);
inline extern void AdcIf_DecTimeTrigger_6(void);
inline extern void AdcIf_DecTimeTrigger_7(void);
/* End */
inline extern void AdcIf_DecNormal(void);
inline extern void AdcIf_StartNormalGroupConversion(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ADCIF_CONV50US_H_ */
/** @} */
