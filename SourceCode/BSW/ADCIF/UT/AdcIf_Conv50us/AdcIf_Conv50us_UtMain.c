#include "unity.h"
#include "unity_fixture.h"
#include "AdcIf_Conv50us.h"
#include "AdcIf_Conv50us_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_AdcIf_Conv50usEcuModeSts[MAX_STEP] = ADCIF_CONV50USECUMODESTS;
const Eem_SuspcDetnFuncInhibitAdcifSts_t UtInput_AdcIf_Conv50usFuncInhibitAdcifSts[MAX_STEP] = ADCIF_CONV50USFUNCINHIBITADCIFSTS;

const Haluint16 UtExpected_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon[MAX_STEP] = ADCIF_CONV50USHWTRIGMOTINFO_UPHASE0MON;
const Haluint16 UtExpected_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon[MAX_STEP] = ADCIF_CONV50USHWTRIGMOTINFO_UPHASE1MON;
const Haluint16 UtExpected_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon[MAX_STEP] = ADCIF_CONV50USHWTRIGMOTINFO_VPHASE0MON;
const Haluint16 UtExpected_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon[MAX_STEP] = ADCIF_CONV50USHWTRIGMOTINFO_VPHASE1MON;



TEST_GROUP(AdcIf_Conv50us);
TEST_SETUP(AdcIf_Conv50us)
{
    AdcIf_Conv50us_Init();
}

TEST_TEAR_DOWN(AdcIf_Conv50us)
{   /* Postcondition */

}

TEST(AdcIf_Conv50us, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        AdcIf_Conv50usEcuModeSts = UtInput_AdcIf_Conv50usEcuModeSts[i];
        AdcIf_Conv50usFuncInhibitAdcifSts = UtInput_AdcIf_Conv50usFuncInhibitAdcifSts[i];

        AdcIf_Conv50us();

        TEST_ASSERT_EQUAL(AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon, UtExpected_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon, UtExpected_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon, UtExpected_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon, UtExpected_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon[i]);
    }
}

TEST_GROUP_RUNNER(AdcIf_Conv50us)
{
    RUN_TEST_CASE(AdcIf_Conv50us, All);
}
