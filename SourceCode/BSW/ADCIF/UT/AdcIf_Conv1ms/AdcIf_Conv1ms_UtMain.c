#include "unity.h"
#include "unity_fixture.h"
#include "AdcIf_Conv1ms.h"
#include "AdcIf_Conv1ms_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_AdcIf_Conv1msEcuModeSts[MAX_STEP] = ADCIF_CONV1MSECUMODESTS;
const Eem_SuspcDetnFuncInhibitAdcifSts_t UtInput_AdcIf_Conv1msFuncInhibitAdcifSts[MAX_STEP] = ADCIF_CONV1MSFUNCINHIBITADCIFSTS;

const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Ext5vMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_EXT5VMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_CEMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_CEMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Int5vMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_INT5VMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_CSPMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_CSPMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vbatt01Mon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_VBATT01MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vbatt02Mon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_VBATT02MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_VddMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_VDDMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vdd3Mon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_VDD3MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vdd5Mon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPWRINFO_VDD5MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspAbsHMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGFSPINFO_FSPABSHMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspAbsMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGFSPINFO_FSPABSMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspCbsHMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGFSPINFO_FSPCBSHMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspCbsMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGFSPINFO_FSPCBSMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigTmpInfo_LineTestMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGTMPINFO_LINETESTMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigTmpInfo_TempMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGTMPINFO_TEMPMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMotInfo_MotPwrMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOTINFO_MOTPWRMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMotInfo_StarMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOTINFO_STARMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMotInfo_UoutMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOTINFO_UOUTMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMotInfo_VoutMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOTINFO_VOUTMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMotInfo_WoutMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOTINFO_WOUTMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPdtInfo_PdfSigMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPDTINFO_PDFSIGMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPdtInfo_Pdt5vMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPDTINFO_PDT5VMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPdtInfo_Pdf5vMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPDTINFO_PDF5VMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigPdtInfo_PdtSigMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGPDTINFO_PDTSIGMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlIMainMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RLIMAINMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlISubMonFs[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RLISUBMONFS;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlMocNegMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RLMOCNEGMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlMocPosMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RLMOCPOSMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrIMainMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RRIMAINMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrISubMonFs[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RRISUBMONFS;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrMocNegMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RRMOCNEGMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrMocPosMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_RRMOCPOSMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigMocInfo_BFLMon[MAX_STEP] = ADCIF_CONV1MSSWTRIGMOCINFO_BFLMON;
const Haluint16 UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvCVMon[MAX_STEP] = ADCIF_CONV1MSHWTRIGVLVINFO_VLVCVMON;
const Haluint16 UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvRLVMon[MAX_STEP] = ADCIF_CONV1MSHWTRIGVLVINFO_VLVRLVMON;
const Haluint16 UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvCutPMon[MAX_STEP] = ADCIF_CONV1MSHWTRIGVLVINFO_VLVCUTPMON;
const Haluint16 UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvCutSMon[MAX_STEP] = ADCIF_CONV1MSHWTRIGVLVINFO_VLVCUTSMON;
const Haluint16 UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvSimMon[MAX_STEP] = ADCIF_CONV1MSHWTRIGVLVINFO_VLVSIMMON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A7_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A7_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A2_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A2_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_P1_NEG_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_P1_NEG_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A6_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A6_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_MTR_FW_S_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_MTR_FW_S_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A4_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A4_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_MTP_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_MTP_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A3_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A3_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_P1_POS_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_P1_POS_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A5_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A5_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_VDD_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_VDD_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_COOL_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_COOL_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_CAN_L_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_CAN_L_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_P3_NEG_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_P3_NEG_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A1_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A1_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_P2_POS_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_P2_POS_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A8_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A8_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A9_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_IF_A9_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_P2_NEG_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_P2_NEG_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_P3_POS_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_P3_POS_MON;
const Haluint16 UtExpected_AdcIf_Conv1msSwTrigEscInfo_OP_OUT_MON[MAX_STEP] = ADCIF_CONV1MSSWTRIGESCINFO_OP_OUT_MON;
const AdcIf_Conv1msAdcInvalid_t UtExpected_AdcIf_Conv1msAdcInvalid[MAX_STEP] = ADCIF_CONV1MSADCINVALID;



TEST_GROUP(AdcIf_Conv1ms);
TEST_SETUP(AdcIf_Conv1ms)
{
    AdcIf_Conv1ms_Init();
}

TEST_TEAR_DOWN(AdcIf_Conv1ms)
{   /* Postcondition */

}

TEST(AdcIf_Conv1ms, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        AdcIf_Conv1msEcuModeSts = UtInput_AdcIf_Conv1msEcuModeSts[i];
        AdcIf_Conv1msFuncInhibitAdcifSts = UtInput_AdcIf_Conv1msFuncInhibitAdcifSts[i];

        AdcIf_Conv1ms();

        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Ext5vMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.CEMon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_CEMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.Int5vMon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Int5vMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.CSPMon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_CSPMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vbatt01Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vbatt02Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.VddMon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_VddMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vdd3Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon, UtExpected_AdcIf_Conv1msSwTrigPwrInfo_Vdd5Mon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigFspInfo.FspAbsHMon, UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspAbsHMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigFspInfo.FspAbsMon, UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspAbsMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigFspInfo.FspCbsHMon, UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspCbsHMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigFspInfo.FspCbsMon, UtExpected_AdcIf_Conv1msSwTrigFspInfo_FspCbsMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigTmpInfo.LineTestMon, UtExpected_AdcIf_Conv1msSwTrigTmpInfo_LineTestMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigTmpInfo.TempMon, UtExpected_AdcIf_Conv1msSwTrigTmpInfo_TempMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMotInfo.MotPwrMon, UtExpected_AdcIf_Conv1msSwTrigMotInfo_MotPwrMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMotInfo.StarMon, UtExpected_AdcIf_Conv1msSwTrigMotInfo_StarMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMotInfo.UoutMon, UtExpected_AdcIf_Conv1msSwTrigMotInfo_UoutMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMotInfo.VoutMon, UtExpected_AdcIf_Conv1msSwTrigMotInfo_VoutMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMotInfo.WoutMon, UtExpected_AdcIf_Conv1msSwTrigMotInfo_WoutMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPdtInfo.PdfSigMon, UtExpected_AdcIf_Conv1msSwTrigPdtInfo_PdfSigMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPdtInfo.Pdt5vMon, UtExpected_AdcIf_Conv1msSwTrigPdtInfo_Pdt5vMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPdtInfo.Pdf5vMon, UtExpected_AdcIf_Conv1msSwTrigPdtInfo_Pdf5vMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigPdtInfo.PdtSigMon, UtExpected_AdcIf_Conv1msSwTrigPdtInfo_PdtSigMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RlIMainMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlIMainMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RlISubMonFs, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlISubMonFs[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RlMocNegMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlMocNegMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RlMocPosMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RlMocPosMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RrIMainMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrIMainMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RrISubMonFs, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrISubMonFs[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RrMocNegMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrMocNegMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.RrMocPosMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_RrMocPosMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigMocInfo.BFLMon, UtExpected_AdcIf_Conv1msSwTrigMocInfo_BFLMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msHwTrigVlvInfo.VlvCVMon, UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvCVMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msHwTrigVlvInfo.VlvRLVMon, UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvRLVMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msHwTrigVlvInfo.VlvCutPMon, UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvCutPMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msHwTrigVlvInfo.VlvCutSMon, UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvCutSMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msHwTrigVlvInfo.VlvSimMon, UtExpected_AdcIf_Conv1msHwTrigVlvInfo_VlvSimMon[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A7_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A7_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A2_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A2_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.P1_NEG_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_P1_NEG_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A6_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A6_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.MTR_FW_S_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_MTR_FW_S_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A4_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A4_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.MTP_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_MTP_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A3_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A3_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.P1_POS_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_P1_POS_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A5_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A5_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.VDD_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_VDD_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.COOL_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_COOL_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.CAN_L_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_CAN_L_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.P3_NEG_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_P3_NEG_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A1_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A1_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.P2_POS_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_P2_POS_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A8_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A8_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.IF_A9_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_IF_A9_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.P2_NEG_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_P2_NEG_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.P3_POS_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_P3_POS_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msSwTrigEscInfo.OP_OUT_MON, UtExpected_AdcIf_Conv1msSwTrigEscInfo_OP_OUT_MON[i]);
        TEST_ASSERT_EQUAL(AdcIf_Conv1msAdcInvalid, UtExpected_AdcIf_Conv1msAdcInvalid[i]);
    }
}

TEST_GROUP_RUNNER(AdcIf_Conv1ms)
{
    RUN_TEST_CASE(AdcIf_Conv1ms, All);
}
