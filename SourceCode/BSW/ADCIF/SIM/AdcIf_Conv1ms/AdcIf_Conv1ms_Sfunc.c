#define S_FUNCTION_NAME      AdcIf_Conv1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          2
#define WidthOutputPort         60

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "AdcIf_Conv1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    AdcIf_Conv1msEcuModeSts = input[0];
    AdcIf_Conv1msFuncInhibitAdcifSts = input[1];

    AdcIf_Conv1ms();


    output[0] = AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon;
    output[1] = AdcIf_Conv1msSwTrigPwrInfo.CEMon;
    output[2] = AdcIf_Conv1msSwTrigPwrInfo.Int5vMon;
    output[3] = AdcIf_Conv1msSwTrigPwrInfo.CSPMon;
    output[4] = AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon;
    output[5] = AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon;
    output[6] = AdcIf_Conv1msSwTrigPwrInfo.VddMon;
    output[7] = AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon;
    output[8] = AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon;
    output[9] = AdcIf_Conv1msSwTrigFspInfo.FspAbsHMon;
    output[10] = AdcIf_Conv1msSwTrigFspInfo.FspAbsMon;
    output[11] = AdcIf_Conv1msSwTrigFspInfo.FspCbsHMon;
    output[12] = AdcIf_Conv1msSwTrigFspInfo.FspCbsMon;
    output[13] = AdcIf_Conv1msSwTrigTmpInfo.LineTestMon;
    output[14] = AdcIf_Conv1msSwTrigTmpInfo.TempMon;
    output[15] = AdcIf_Conv1msSwTrigMotInfo.MotPwrMon;
    output[16] = AdcIf_Conv1msSwTrigMotInfo.StarMon;
    output[17] = AdcIf_Conv1msSwTrigMotInfo.UoutMon;
    output[18] = AdcIf_Conv1msSwTrigMotInfo.VoutMon;
    output[19] = AdcIf_Conv1msSwTrigMotInfo.WoutMon;
    output[20] = AdcIf_Conv1msSwTrigPdtInfo.PdfSigMon;
    output[21] = AdcIf_Conv1msSwTrigPdtInfo.Pdt5vMon;
    output[22] = AdcIf_Conv1msSwTrigPdtInfo.Pdf5vMon;
    output[23] = AdcIf_Conv1msSwTrigPdtInfo.PdtSigMon;
    output[24] = AdcIf_Conv1msSwTrigMocInfo.RlIMainMon;
    output[25] = AdcIf_Conv1msSwTrigMocInfo.RlISubMonFs;
    output[26] = AdcIf_Conv1msSwTrigMocInfo.RlMocNegMon;
    output[27] = AdcIf_Conv1msSwTrigMocInfo.RlMocPosMon;
    output[28] = AdcIf_Conv1msSwTrigMocInfo.RrIMainMon;
    output[29] = AdcIf_Conv1msSwTrigMocInfo.RrISubMonFs;
    output[30] = AdcIf_Conv1msSwTrigMocInfo.RrMocNegMon;
    output[31] = AdcIf_Conv1msSwTrigMocInfo.RrMocPosMon;
    output[32] = AdcIf_Conv1msSwTrigMocInfo.BFLMon;
    output[33] = AdcIf_Conv1msHwTrigVlvInfo.VlvInFRMon;
    output[34] = AdcIf_Conv1msHwTrigVlvInfo.VlvInFLMon;
    output[35] = AdcIf_Conv1msHwTrigVlvInfo.VlvCutPMon;
    output[36] = AdcIf_Conv1msHwTrigVlvInfo.VlvCutSMon;
    output[37] = AdcIf_Conv1msHwTrigVlvInfo.VlvSimMon;
    output[38] = AdcIf_Conv1msSwTrigEscInfo.IF_A7_MON;
    output[39] = AdcIf_Conv1msSwTrigEscInfo.IF_A2_MON;
    output[40] = AdcIf_Conv1msSwTrigEscInfo.P1_NEG_MON;
    output[41] = AdcIf_Conv1msSwTrigEscInfo.IF_A6_MON;
    output[42] = AdcIf_Conv1msSwTrigEscInfo.MTR_FW_S_MON;
    output[43] = AdcIf_Conv1msSwTrigEscInfo.IF_A4_MON;
    output[44] = AdcIf_Conv1msSwTrigEscInfo.MTP_MON;
    output[45] = AdcIf_Conv1msSwTrigEscInfo.IF_A3_MON;
    output[46] = AdcIf_Conv1msSwTrigEscInfo.P1_POS_MON;
    output[47] = AdcIf_Conv1msSwTrigEscInfo.IF_A5_MON;
    output[48] = AdcIf_Conv1msSwTrigEscInfo.VDD_MON;
    output[49] = AdcIf_Conv1msSwTrigEscInfo.COOL_MON;
    output[50] = AdcIf_Conv1msSwTrigEscInfo.CAN_L_MON;
    output[51] = AdcIf_Conv1msSwTrigEscInfo.P3_NEG_MON;
    output[52] = AdcIf_Conv1msSwTrigEscInfo.IF_A1_MON;
    output[53] = AdcIf_Conv1msSwTrigEscInfo.P2_POS_MON;
    output[54] = AdcIf_Conv1msSwTrigEscInfo.IF_A8_MON;
    output[55] = AdcIf_Conv1msSwTrigEscInfo.IF_A9_MON;
    output[56] = AdcIf_Conv1msSwTrigEscInfo.P2_NEG_MON;
    output[57] = AdcIf_Conv1msSwTrigEscInfo.P3_POS_MON;
    output[58] = AdcIf_Conv1msSwTrigEscInfo.OP_OUT_MON;
    output[59] = AdcIf_Conv1msAdcInvalid;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    AdcIf_Conv1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
