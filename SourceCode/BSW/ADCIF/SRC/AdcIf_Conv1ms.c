/**
 * @defgroup AdcIf_Conv1ms AdcIf_Conv1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Conv1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Conv1ms.h"
#include "AdcIf_Conv1ms_Ifa.h"
#include "IfxStm_reg.h"
#include "AdcIf_Conv50us.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ADCIF_CONV1MS_START_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ADCIF_CONV1MS_STOP_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_CONV1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AdcIf_Conv1ms_HdrBusType AdcIf_Conv1msBus;

/* Version Info */
/* Version Info */
const SwcVersionInfo_t AdcIf_Conv1msVersionInfo = 
{   
    ADCIF_CONV1MS_MODULE_ID,           /* AdcIf_Conv1msVersionInfo.ModuleId */
    ADCIF_CONV1MS_MAJOR_VERSION,       /* AdcIf_Conv1msVersionInfo.MajorVer */
    ADCIF_CONV1MS_MINOR_VERSION,       /* AdcIf_Conv1msVersionInfo.MinorVer */
    ADCIF_CONV1MS_PATCH_VERSION,       /* AdcIf_Conv1msVersionInfo.PatchVer */
    ADCIF_CONV1MS_BRANCH_VERSION       /* AdcIf_Conv1msVersionInfo.BranchVer */
};

/* Input Data Element */
Mom_HndlrEcuModeSts_t AdcIf_Conv1msEcuModeSts;
Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv1msFuncInhibitAdcifSts;

/* Output Data Element */
AdcIf_Conv1msSwTrigPwrInfo_t AdcIf_Conv1msSwTrigPwrInfo;
AdcIf_Conv1msSwTrigFspInfo_t AdcIf_Conv1msSwTrigFspInfo;
AdcIf_Conv1msSwTrigTmpInfo_t AdcIf_Conv1msSwTrigTmpInfo;
AdcIf_Conv1msSwTrigMotInfo_t AdcIf_Conv1msSwTrigMotInfo;
AdcIf_Conv1msSwTrigPdtInfo_t AdcIf_Conv1msSwTrigPdtInfo;
AdcIf_Conv1msSwTrigMocInfo_t AdcIf_Conv1msSwTrigMocInfo;
AdcIf_Conv1msHwTrigVlvInfo_t AdcIf_Conv1msHwTrigVlvInfo;
#if (ADCIF_VARIANT == ADCIF_VARIANT_IDB)
/* KCLim : To prevent compiler error for IDB
   ToDo : AdcIf for Esc variant has same code with IDB
   Resove? 
		 1) Change all prefix of variables and functions from AdcIf_ to AdcIfEsc_ 
*/

AdcIf_Conv1msSwTrigEscInfo_t AdcIf_Conv1msSwTrigEscInfo;
#endif
AdcIf_Conv1msAdcInvalid_t AdcIf_Conv1msAdcInvalid;

uint32 AdcIf_Conv1ms_Timer_Start;
uint32 AdcIf_Conv1ms_Timer_Elapsed;

#define ADCIF_CONV1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV1MS_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_CONV1MS_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV1MS_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_CONV1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV1MS_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_CONV1MS_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV1MS_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_CONV1MS_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ADCIF_CONV1MS_START_SEC_CODE
#include "AdcIf_MemMap.h"

//LEJ
#include "TimE.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AdcIf_Conv1ms_Init(void)
{
    /* Initialize internal bus */
    AdcIf_Conv1msBus.AdcIf_Conv1msEcuModeSts = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msFuncInhibitAdcifSts = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.CEMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Int5vMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.CSPMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.VddMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspAbsHMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspAbsMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspCbsHMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspCbsMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigTmpInfo.LineTestMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigTmpInfo.TempMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.MotPwrMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.StarMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.UoutMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.VoutMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.WoutMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.PdfSigMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.Pdt5vMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.Pdf5vMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.PdtSigMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlIMainMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlISubMonFs = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlMocNegMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlMocPosMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrIMainMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrISubMonFs = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrMocNegMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrMocPosMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.BFLMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvCVMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvRLVMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvCutPMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvCutSMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvSimMon = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A7_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A2_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P1_NEG_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A6_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.MTR_FW_S_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A4_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.MTP_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A3_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P1_POS_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A5_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.VDD_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.COOL_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.CAN_L_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P3_NEG_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A1_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P2_POS_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A8_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A9_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P2_NEG_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P3_POS_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.OP_OUT_MON = 0;
    AdcIf_Conv1msBus.AdcIf_Conv1msAdcInvalid = 0;
}

void AdcIf_Conv1ms(void)
{
    #if (ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)
	   ASresult[TIME_AS_CHECKPOINT_FOR_1MS] = SetAlivesupervisionCheckpoint(TIME_AS_CHECKPOINT_FOR_1MS);	
    #elif (ALIVESUPERVISION_INDICATION == STD_ON)
	   ASresult[TIME_AS_CHECKPOINT_FOR_1MS] = SetAlivesupervisionCheckpoint0(TIME_AS_CHECKPOINT_FOR_1MS);	
    #endif 
       
    AdcIf_Conv1ms_Timer_Start = STM0_TIM0.U;	

    /* Input */
    AdcIf_Conv1ms_Read_AdcIf_Conv1msEcuModeSts(&AdcIf_Conv1msBus.AdcIf_Conv1msEcuModeSts);
    AdcIf_Conv1ms_Read_AdcIf_Conv1msFuncInhibitAdcifSts(&AdcIf_Conv1msBus.AdcIf_Conv1msFuncInhibitAdcifSts);

    /* Process */
    AdcIf_DecNormal();
    AdcIf_StartNormalGroupConversion();
	
    AdcIf_DecTimeTrigger_0();
    AdcIf_DecTimeTrigger_1();
    AdcIf_DecTimeTrigger_2();

    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon        = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_5V_EXT_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.CEMon           = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_CE_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Int5vMon        = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_5V_INT_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.CSPMon          = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_CSP_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon      = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_VBATT01_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon      = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_VBATT02_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.VddMon          = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_VDD1_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_VDD3_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_VDD5_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspAbsHMon      = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_FSP_ABS_HMON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspAbsMon       = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_FSP_ABS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspCbsHMon      = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_FSP_CBS_HMON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo.FspCbsMon       = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_FSP_CBS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigTmpInfo.LineTestMon     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_LINE_TEST_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigTmpInfo.TempMon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_TEMP_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.MotPwrMon       = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_MOT_POW_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.StarMon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_STAR_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.UoutMon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_U_OUT_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.VoutMon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_V_OUT_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo.WoutMon         = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_W_OUT_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.PdfSigMon       = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_PDF_SIG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.Pdt5vMon        = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_PDT_5V_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.Pdf5vMon        = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_PDF_5V_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo.PdtSigMon       = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_PDT_SIG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlIMainMon      = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RL_I_MAIN_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlISubMonFs     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RL_I_SUB_MON_FS];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlMocNegMon     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RL_MOC_NEG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RlMocPosMon     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RL_MOC_POS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrIMainMon      = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RR_I_MAIN_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrISubMonFs     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RR_I_SUB_MON_FS];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrMocNegMon     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RR_MOC_NEG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.RrMocPosMon     = AdcIfDecodingRawAdValue[ADC_CH_NORMAL_RR_MOC_POS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.BFLMon          = AdcIfDecodingRawAdValue[ADC_CH_VALVE_BFL_MON];
    /* NZ Quick C : CV */
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvCVMon      = AdcIfDecodingRawAdValue[ADC_CH_VALVE_CV_MON];
    /* NZ Quick C: RLV */
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvRLVMon      = AdcIfDecodingRawAdValue[ADC_CH_VALVE_RLV_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvCutPMon      = AdcIfDecodingRawAdValue[ADC_CH_VALVE_CUTV_P_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvCutSMon      = AdcIfDecodingRawAdValue[ADC_CH_VALVE_CUTV_S_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo.VlvSimMon       = AdcIfDecodingRawAdValue[ADC_CH_VALVE_SIMV_MON];

    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A7_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A7_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A2_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A2_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P1_NEG_MON      = AdcIfDecodingRawAdValue[ADC_CH_P1_NEG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A6_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A6_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.MTR_FW_S_MON    = AdcIfDecodingRawAdValue[ADC_CH_MTR_FW_S_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A4_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A4_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.MTP_MON         = AdcIfDecodingRawAdValue[ADC_CH_MTP_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A3_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A3_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P1_POS_MON      = AdcIfDecodingRawAdValue[ADC_CH_P1_POS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A5_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A5_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.VDD_MON         = AdcIfDecodingRawAdValue[ADC_CH_VDD_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.COOL_MON        = AdcIfDecodingRawAdValue[ADC_CH_COOL_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.CAN_L_MON       = AdcIfDecodingRawAdValue[ADC_CH_CAN_L_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P3_NEG_MON      = AdcIfDecodingRawAdValue[ADC_CH_P3_NEG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A1_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A1_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P2_POS_MON      = AdcIfDecodingRawAdValue[ADC_CH_P2_POS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A8_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A8_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.IF_A9_MON       = AdcIfDecodingRawAdValue[ADC_CH_IF_A9_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P2_NEG_MON      = AdcIfDecodingRawAdValue[ADC_CH_P2_NEG_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.P3_POS_MON      = AdcIfDecodingRawAdValue[ADC_CH_P3_POS_MON];
    AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo.OP_OUT_MON      = AdcIfDecodingRawAdValue[ADC_CH_OP_OUT_MON];
    /*
    VlvInfo_INRRMon = AdcIfDecodingRawAdValue[ADC_CH_VALVE_INRR_MON];
    VlvInfo_INRLMon = AdcIfDecodingRawAdValue[ADC_CH_VALVE_INRL_MON];

    Simv_DRNMon = AdcIfDecodingRawAdValue[ADC_CH_VALVE_SIMV_DRN_MON];
    */

    /* Output */
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPwrInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPwrInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigPwrInfo 
     : AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon;
     : AdcIf_Conv1msSwTrigPwrInfo.CEMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Int5vMon;
     : AdcIf_Conv1msSwTrigPwrInfo.CSPMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.VddMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigFspInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigFspInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigFspInfo 
     : AdcIf_Conv1msSwTrigFspInfo.FspAbsHMon;
     : AdcIf_Conv1msSwTrigFspInfo.FspAbsMon;
     : AdcIf_Conv1msSwTrigFspInfo.FspCbsHMon;
     : AdcIf_Conv1msSwTrigFspInfo.FspCbsMon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigTmpInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigTmpInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigTmpInfo 
     : AdcIf_Conv1msSwTrigTmpInfo.LineTestMon;
     : AdcIf_Conv1msSwTrigTmpInfo.TempMon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMotInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMotInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigMotInfo 
     : AdcIf_Conv1msSwTrigMotInfo.MotPwrMon;
     : AdcIf_Conv1msSwTrigMotInfo.StarMon;
     : AdcIf_Conv1msSwTrigMotInfo.UoutMon;
     : AdcIf_Conv1msSwTrigMotInfo.VoutMon;
     : AdcIf_Conv1msSwTrigMotInfo.WoutMon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigPdtInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigPdtInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigPdtInfo 
     : AdcIf_Conv1msSwTrigPdtInfo.PdfSigMon;
     : AdcIf_Conv1msSwTrigPdtInfo.Pdt5vMon;
     : AdcIf_Conv1msSwTrigPdtInfo.Pdf5vMon;
     : AdcIf_Conv1msSwTrigPdtInfo.PdtSigMon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigMocInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigMocInfo 
     : AdcIf_Conv1msSwTrigMocInfo.RlIMainMon;
     : AdcIf_Conv1msSwTrigMocInfo.RlISubMonFs;
     : AdcIf_Conv1msSwTrigMocInfo.RlMocNegMon;
     : AdcIf_Conv1msSwTrigMocInfo.RlMocPosMon;
     : AdcIf_Conv1msSwTrigMocInfo.RrIMainMon;
     : AdcIf_Conv1msSwTrigMocInfo.RrISubMonFs;
     : AdcIf_Conv1msSwTrigMocInfo.RrMocNegMon;
     : AdcIf_Conv1msSwTrigMocInfo.RrMocPosMon;
     : AdcIf_Conv1msSwTrigMocInfo.BFLMon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msHwTrigVlvInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msHwTrigVlvInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msHwTrigVlvInfo 
     : AdcIf_Conv1msHwTrigVlvInfo.VlvCVMon;
     : AdcIf_Conv1msHwTrigVlvInfo.VlvRLVMon;
     : AdcIf_Conv1msHwTrigVlvInfo.VlvCutPMon;
     : AdcIf_Conv1msHwTrigVlvInfo.VlvCutSMon;
     : AdcIf_Conv1msHwTrigVlvInfo.VlvSimMon;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msSwTrigEscInfo(&AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigEscInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigEscInfo 
     : AdcIf_Conv1msSwTrigEscInfo.IF_A7_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A2_MON;
     : AdcIf_Conv1msSwTrigEscInfo.P1_NEG_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A6_MON;
     : AdcIf_Conv1msSwTrigEscInfo.MTR_FW_S_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A4_MON;
     : AdcIf_Conv1msSwTrigEscInfo.MTP_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A3_MON;
     : AdcIf_Conv1msSwTrigEscInfo.P1_POS_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A5_MON;
     : AdcIf_Conv1msSwTrigEscInfo.VDD_MON;
     : AdcIf_Conv1msSwTrigEscInfo.COOL_MON;
     : AdcIf_Conv1msSwTrigEscInfo.CAN_L_MON;
     : AdcIf_Conv1msSwTrigEscInfo.P3_NEG_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A1_MON;
     : AdcIf_Conv1msSwTrigEscInfo.P2_POS_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A8_MON;
     : AdcIf_Conv1msSwTrigEscInfo.IF_A9_MON;
     : AdcIf_Conv1msSwTrigEscInfo.P2_NEG_MON;
     : AdcIf_Conv1msSwTrigEscInfo.P3_POS_MON;
     : AdcIf_Conv1msSwTrigEscInfo.OP_OUT_MON;
     =============================================================================*/
    
    AdcIf_Conv1ms_Write_AdcIf_Conv1msAdcInvalid(&AdcIf_Conv1msBus.AdcIf_Conv1msAdcInvalid);

    AdcIf_Conv1ms_Timer_Elapsed = STM0_TIM0.U - AdcIf_Conv1ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ADCIF_CONV1MS_STOP_SEC_CODE
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
