/**
 * @defgroup AdcIf_Conv50us AdcIf_Conv50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AdcIf_Conv50us.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AdcIf_Conv50us.h"
#include "AdcIf_Conv50us_Ifa.h"
#include "IfxSrc_regdef.h"
#include "IfxStm_reg.h"
#include "IfxDma_reg.h"
#include "IfxSrc_reg.h"
#include "Mcal_DmaLib.h"
 #include "Adc.h"

/* Inclusion structure */
#include "Adc_Utility.h"
#include "IfxCpu_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define ADCIF_SET_BIT(num)                 (0x01 << num)
#define ADC_MAX_NUM_TIMETRIGGER             (AdcIf_UsgTimeTriggerEndIndex - AdcIf_UsgTimeTriggerStartIndex)
#define ADC_NUM_OF_TIMETRIGGER_LIMIT        8

#define ADCIF_DMA_MExADCIR_BITMASK_IRDV(x)      ((x<<28)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_INTCT(x)     ((x<<26)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_WRPDE(x)     ((x<<25)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_WRPSE(x)     ((x<<24)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_ETRL(x)      ((x<<23)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_STAMP(x)     ((x<<22)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_DCBE(x)      ((x<<21)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_SCBE(x)      ((x<<20)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_SHCT(x)      ((x<<16)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_CBLD(x)      ((x<<12)&0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_CBLS(x)      ((x<<8) &0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_INCD(x)      ((x<<7) &0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_DMF(x)       ((x<<4) &0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_INCS(x)      ((x<<3) &0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITMASK_SMF(x)       ((x<<0) &0xFFFFFFFF)
#define ADCIF_DMA_MExADCIR_BITLENGTH            32

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
#define ADCIF_NUM_CONFIG_ERRORTYPE   AdcIf_NoOfConfigError

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ADCIF_CONV50US_START_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ADCIF_CONV50US_STOP_SEC_CONST_UNSPECIFIED
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_CONV50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AdcIf_Conv50us_HdrBusType AdcIf_Conv50usBus;

/* Version Info */
const SwcVersionInfo_t AdcIf_Conv50usVersionInfo = 
{   
    ADCIF_CONV50US_MODULE_ID,           /* AdcIf_Conv50usVersionInfo.ModuleId */
    ADCIF_CONV50US_MAJOR_VERSION,       /* AdcIf_Conv50usVersionInfo.MajorVer */
    ADCIF_CONV50US_MINOR_VERSION,       /* AdcIf_Conv50usVersionInfo.MinorVer */
    ADCIF_CONV50US_PATCH_VERSION,       /* AdcIf_Conv50usVersionInfo.PatchVer */
    ADCIF_CONV50US_BRANCH_VERSION       /* AdcIf_Conv50usVersionInfo.BranchVer */
};

/* Input Data Element */
Mom_HndlrEcuModeSts_t AdcIf_Conv50usEcuModeSts;
Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv50usFuncInhibitAdcifSts;

/* Output Data Element */
AdcIf_Conv50usHwTrigMotInfo_t AdcIf_Conv50usHwTrigMotInfo;

uint32 AdcIf_Conv50us_Timer_Start;
uint32 AdcIf_Conv50us_Timer_Elapsed;

#define ADCIF_CONV50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV50US_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ADCIF_CONV50US_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_CONV50US_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_CONV50US_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV50US_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_CONV50US_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ADCIF_CONV50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ADCIF_CONV50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV50US_START_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
/* return value for each ADC group : E_OK or E_NOTOK */
static Std_ReturnType          AdcIfResultBufferRet[ADCIF_NUM_OF_GROUP];

/* This array buffer will be filled by ADC module */
static AdcIf_ValueGroupType    AdcIfGrResultBuffer[ADCIF_NUM_OF_GROUP][ADCIF_NUM_OF_CHANNEL];
static AdcIf_ValueGroupType    AdcIfGrTempBuffer[ADCIF_NUM_OF_GROUP][ADCIF_NUM_OF_CHANNEL];

/* For Adc and Dma init data */
static AdcIf_InitInfoType      AdcIfInitInfo       [ADCIF_NUM_OF_GROUP];

/* For Hardware trigger ADC  */
static AdcIf_DecTtInfoType     AdcIfDecTtInfo      [ADCIF_NUM_OF_GROUP];

/* Buffer for AdcIf_GetValue() function. this buffer will gather Adc conversion value from AdcIfGrResultBuffer[][] */
AdcIf_ValueGroupType    AdcIfDecodingRawAdValue[ADCIF_MAX_GROUP_CHANNEL_NUM];

/* Index of AdcIfTtTotalDmaBuf[] buffer, this is temporary value */
static AdcIf_u16TotalDmaIndex;

/* DMA buffer should be aligned by 64 according to current concept of Hardware Trigger */
#pragma align 64
/* DMA buffer */
static AdcIf_ValueGroupType    AdcIfTtTotalDmaBuf  [ADCIF_MAX_TOTAL_DMABUFFER_SIZE];
#pragma align restore

static uint8 AdcTtGrIdx[AdcIf_UsgTimeTriggerEndIndex];

static uint8 AdcNumofTt = 0;

#define ADCIF_CONV50US_STOP_SEC_VAR_NOINIT_32BIT
#include "AdcIf_MemMap.h"
#define ADCIF_CONV50US_START_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ADCIF_CONV50US_STOP_SEC_VAR_UNSPECIFIED
#include "AdcIf_MemMap.h"
#define ADCIF_CONV50US_START_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/** Variable Section (32BIT)**/


#define ADCIF_CONV50US_STOP_SEC_VAR_32BIT
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ADCIF_CONV50US_START_SEC_CODE
#include "AdcIf_MemMap.h"

//LEJ
#include "TimE.h"

static void AdcIf_vLoadConfig(void);
static void AdcIf_vCheckConfig(void);
static void AdcIf_vInitGlobalVal(void);
static void AdcIf_vInitGroup(void);
static void AdcIf_vInitDma(uint8 u8AdcGrId, uint8 u8AdcChId, AdcIf_DmaSetParamType AdcIfDmaSetParam);
static void AdcIf_InitGrTtIdx(AdcIf_UsageType AdcIfUsageTypeId);
inline void AdcIf_DecTimeTrigger(AdcIf_UsageType AdcIfUsageTypeId);
void AdcIf_vGetDmaAdcirRegVal(uint8 u8AdcGrId, uint32 *RegisterValue);
void AdcIf_StartNormalGroupConversion(void);
inline void AdcIf_DecNormal(void);
void Adc_StartGroupConversion_Direct(Adc_GroupType Group);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AdcIf_Conv50us_Init(void)
{
    /* Init01 : Initialize module global variables */
    AdcIf_vInitGlobalVal();

    /* Init02 : make required information(AdcIfInitInfo and AdcIfDecTtInfo)
                 for AUTOSAR ADC module and MCAL DMA initialize from AdcIf_Config */
    AdcIf_vLoadConfig();

    /* Init03 : Check configuration of AdcIf_Config.
                 If configuration is not correct, it will indicate wrong config point in DetErr module  */
    
    AdcIf_vCheckConfig();

    /* Init04 : Init AUTOSAR ADC module and MCAL DMA based on AdcIf_vLoadConfig() result by AdcIfInitInfo and AdcIfDecTtInfo */
    AdcIf_vInitGroup();
    
    /* Initialize internal bus */
    AdcIf_Conv50usBus.AdcIf_Conv50usEcuModeSts = 0;
    AdcIf_Conv50usBus.AdcIf_Conv50usFuncInhibitAdcifSts = 0;
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = 0;
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = 0;
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = 0;
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = 0;
}

void AdcIf_Conv50us(void)
{
  #if (ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)
 	  ASresult[TIME_AS_CHECKPOINT_FOR_50MICROS] = SetAlivesupervisionCheckpoint(TIME_AS_CHECKPOINT_FOR_50MICROS);	
  #elif (ALIVESUPERVISION_INDICATION == STD_ON)
	  ASresult[TIME_AS_CHECKPOINT_FOR_50MICROS] = SetAlivesupervisionCheckpoint0(TIME_AS_CHECKPOINT_FOR_50MICROS);
  #endif 


    AdcIf_Conv50us_Timer_Start = STM0_TIM0.U;

    /* Input */
    AdcIf_Conv50us_Read_AdcIf_Conv50usEcuModeSts(&AdcIf_Conv50usBus.AdcIf_Conv50usEcuModeSts);
    AdcIf_Conv50us_Read_AdcIf_Conv50usFuncInhibitAdcifSts(&AdcIf_Conv50usBus.AdcIf_Conv50usFuncInhibitAdcifSts);

    /* Process */
    AdcIf_DecTimeTrigger_4();
    AdcIf_DecTimeTrigger_5();

    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = AdcIfDecodingRawAdValue[ADC_CH_MOTOR_U_PHASE_0_MON];
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = AdcIfDecodingRawAdValue[ADC_CH_MOTOR_U_PHASE_1_MON];
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = AdcIfDecodingRawAdValue[ADC_CH_MOTOR_V_PHASE_0_MON];
    AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = AdcIfDecodingRawAdValue[ADC_CH_MOTOR_V_PHASE_1_MON];
    
    /* Output */
    AdcIf_Conv50us_Write_AdcIf_Conv50usHwTrigMotInfo(&AdcIf_Conv50usBus.AdcIf_Conv50usHwTrigMotInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv50usHwTrigMotInfo 
     : AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/
    

    AdcIf_Conv50us_Timer_Elapsed = STM0_TIM0.U - AdcIf_Conv50us_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/* Code Start For ADCIF Init */
static void AdcIf_vInitGlobalVal(void)
{
    AdcIf_u16TotalDmaIndex = 0;
}

static void AdcIf_vLoadConfig(void)
{
    /*

    */
    uint8 u8i = 0x00;
    uint8 u8iPrev = 0x00;
    uint8 u8iIsFind = 0x00;
  uint8 u8AdcGrIndex = 0x00;
  uint8 u8AdcChIndex = 0x00;
  uint8 u8AdcChIndexPrev = 0x00;
  uint8 u8IsAdcChTtIndexFind = 0x00;
  uint8 u8AdcIf_Config_Index = 0x00;
  uint8 u8AdcGrTtSum = 0x00;
  uint16 u16AdcGrTtDmaBufSum = 0x0000;
  uint16 u16AdcChTtDmaBufSize = 0x0000;
  uint16 u16AdcChTtDmaBufSizePrev = 0x0000;
  uint16 u16AdcTotalDmaBufIndex = 0x0000;
  uint8 u8AdcTemp = 0x00;
  AdcIf_ConfigErrInfo ErrIndex = AdcIf_NoConfigError;
  uint16 u16NumForDmaBufAlign;
  uint8 groupDmaBufAlignRemainder = 0x00;


  /* Init02_LoadConfig01 : Make AdcIfInitInfo[] data for MCAL ADC Init by parsing AdcIf_Config[] configuration */
  for (u8AdcIf_Config_Index = 0x00; u8AdcIf_Config_Index < (ADCIF_NUM_OF_GROUP*ADCIF_NUM_OF_CHANNEL) ; u8AdcIf_Config_Index++)
  {
    u8AdcGrIndex = u8AdcIf_Config_Index / ADCIF_NUM_OF_GROUP;
    u8AdcChIndex = u8AdcIf_Config_Index % ADCIF_NUM_OF_GROUP;
    u8AdcTemp = 0x00;

    /* Catch not used Adc group and channel */
    if (AdcIf_Config[u8AdcIf_Config_Index].AdcIfUsageType == AdcIf_UsgNone)
    {
        /* Set bit for chaanel */
        u8AdcTemp = AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgNone;
        u8AdcTemp = (u8AdcTemp | ADCIF_SET_BIT(u8AdcChIndex));
        AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgNone = u8AdcTemp;
    }
    /* Catch Software Trigger used Adc group and channel */
    else if (AdcIf_Config[u8AdcIf_Config_Index].AdcIfUsageType == AdcIf_UsgNormal)
    {
        /* Set bit for chaanel */
        u8AdcTemp = AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgNorm;
        u8AdcTemp = (u8AdcTemp | ADCIF_SET_BIT(u8AdcChIndex));
        AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgNorm = u8AdcTemp;
    }
    /* Catch Hardware Trigger used Adc group and channel */
    else
    {
        /* Set bit for chaanel */
        u8AdcTemp = AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgTt;
        u8AdcTemp = (u8AdcTemp | ADCIF_SET_BIT(u8AdcChIndex));
        AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgTt = u8AdcTemp;

        AdcIfDecTtInfo[u8AdcGrIndex].AdcIfGrId = u8AdcGrIndex;
        AdcIfDecTtInfo[u8AdcGrIndex].AdcIfChTtType = AdcIf_Config[u8AdcIf_Config_Index].AdcIfUsageType;
        AdcIfInitInfo[u8AdcGrIndex].AdcIfDmaChId = AdcIf_Config[u8AdcIf_Config_Index].AdcIfDmaChId;

        /* Make DMA buffer start index for each Hardware Trigger */
		if (u8AdcIf_Config_Index > 0)
		{
			/* Store DMA buffer index for hardware trigger */
				AdcIf_u16TotalDmaIndex = AdcIf_u16TotalDmaIndex + AdcIf_Config[u8AdcIf_Config_Index].AdcIfDmaBufSize;
		}
		else
		{
				AdcIf_u16TotalDmaIndex = 0;
		}
    }
  }

  /* Init02_LoadConfig02 : Make AdcIfInitInfo[] data for MCAL ADC Init by parsing AdcIf_Config[] configuration */
  for (u8AdcGrIndex = 0x00; u8AdcGrIndex < ADCIF_NUM_OF_GROUP; u8AdcGrIndex++)
  {
    u8AdcTemp = AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgTt;
    u8AdcGrTtSum = 0x00;
    u16AdcGrTtDmaBufSum = 0x0000;
    u8IsAdcChTtIndexFind = 0x00;
    u8iIsFind = 0x00;

    /* Iteration for adc channels (0~7) in one adc group,
     *     1) Make AdcIfDecTtInfo[].AdcIfChStartId
     *     2) Make AdcIfDecTtInfo[].AdcIfTtChNum
     *     3) Make AdcIfDecTtInfo[].AdcIfTtDmaBufNum
     *
     * Also check configuration for
     *     1) AdcIf_Config_TimeTriggerBuffersizeIsNot2Pow
     *     2) AdcIf_Config_TimeTriggerTypeUsedAdcchannelNotSequentially
     *     3) AdcIf_Config_TimeTriggerBuffersizeIsNotEqual
     *     4) AdcIf_Config_TimeTriggerBuffersizeIsZero
     */
    for (u8AdcChIndex = 0x00; u8AdcChIndex < ADCIF_NUM_OF_CHANNEL; u8AdcChIndex++)
    {
        /* Catch Adc channel in each group which is used for Hardware Trigger */
        if ((u8AdcTemp & ADCIF_SET_BIT(u8AdcChIndex)) > 0)
        {
            u8AdcGrTtSum++;
            u16AdcChTtDmaBufSize =AdcIf_Config[(u8AdcGrIndex*ADCIF_NUM_OF_CHANNEL)+u8AdcChIndex].AdcIfDmaBufSize;
            u16AdcGrTtDmaBufSum += u16AdcChTtDmaBufSize;

            /* First channel in Group which used for Hardware Trigger? */
            if (u8IsAdcChTtIndexFind == 0x00)
            {
                AdcIfDecTtInfo[u8AdcGrIndex].AdcIfChStartId = u8AdcChIndex;
                u8IsAdcChTtIndexFind = 0x01;
                u8AdcChIndexPrev =u8AdcChIndex;
                u16AdcChTtDmaBufSizePrev = u16AdcChTtDmaBufSize;

                /* Constraint Check of DMA buffer number (2^x) for each channel */
                for (u8i = 0x00; u8i < 16; u8i++)
                {
                    if ((u16AdcChTtDmaBufSize>>u8i) & 0x0001)
                    {
                        if (u8iIsFind == 0x00)
                        {
                            u8iIsFind = 0x01;
                        }
                        else
                        {
                            #if (ADCIF_DETERR == ENABLE)
                            DetErr_Report(ADCIF_MODULE_ID,
                                            ADCIF_INSTANCE_ID,
                                            ADCIF_API_LOADCONFIG,
                                            AdcIf_Config_TimeTriggerBuffersizeIsNot2Pow
                                            );
                            #endif /* #if (ADCIF_DETERR == ENABLE) */
                        }
                    }
                    else
                    {

                    }
                }
            }
            /* Not first channel in Group which used for Hardware Trigger? */
            else
            {
                /* Configuration OK :  Hardware Trigger channel is used sequentially */
                if ((u8AdcChIndex - u8AdcChIndexPrev) == 1)
                {
                    u8AdcChIndexPrev =u8AdcChIndex;
                }
                /* Configuration Not OK : Hardware Trigger channel is not used sequentially, report wrong configuration */
                else
                {
                    #if (ADCIF_DETERR == ENABLE)
                    DetErr_Report(ADCIF_MODULE_ID,
                                    ADCIF_INSTANCE_ID,
                                    ADCIF_API_LOADCONFIG,
                                    AdcIf_Config_TimeTriggerTypeUsedAdcchannelNotSequentially
                                    );
                    #endif /* #if (ADCIF_DETERR == ENABLE) */
                }

                /* Configuration OK : Dma buffer size are configured same length for each channels in one group  */
                if (u16AdcChTtDmaBufSizePrev == u16AdcChTtDmaBufSize)
                {
                    u16AdcChTtDmaBufSizePrev = u16AdcChTtDmaBufSize;
                }
                /* Configuration Not OK : If More than 2 channels are used in one group, DMA buffer size should be equal for all channels*/
                else
                {
                    #if (ADCIF_DETERR == ENABLE)
                    DetErr_Report(ADCIF_MODULE_ID,
                                    ADCIF_INSTANCE_ID,
                                    ADCIF_API_LOADCONFIG,
                                    AdcIf_Config_TimeTriggerBuffersizeIsNotEqual
                                    );
                    #endif /* #if (ADCIF_DETERR == ENABLE) */
                }
            }

            /* Configuration Not OK : DMA buffer size should not be 0 if Hardware trigger usage */
            if (u16AdcChTtDmaBufSize == 0)
            {
                #if (ADCIF_DETERR == ENABLE)
                DetErr_Report(ADCIF_MODULE_ID,
                                ADCIF_INSTANCE_ID,
                                ADCIF_API_LOADCONFIG,
                                AdcIf_Config_TimeTriggerBuffersizeIsZero
                                );
                #endif /* #if (ADCIF_DETERR == ENABLE) */
            }
            /* Configuration OK */
            else
            {
                /* Do nothing  */
            }
        }
        else
        {
            /* do nothing for AdcIf_UsgNone, AdcIf_UsgNormal */
        }
    }

    /* Store result of parsing for Hardware Trigger */
    AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtChNum = u8AdcGrTtSum;
    AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtDmaBufNum = u16AdcGrTtDmaBufSum;

    /* Store Index of AdcIfTtTotalDmaBuf[] for each Adc Group, This value will be used for DMA destination Address  */
    if (u8AdcGrIndex > 0x00)
    {
		/* Apply LSP : */
		groupDmaBufAlignRemainder = (AdcIfDecTtInfo[u8AdcGrIndex-1].AdcIfTtTotalDmaIndex + AdcIfDecTtInfo[u8AdcGrIndex-1].AdcIfTtDmaBufNum) % AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtDmaBufNum;
		if(groupDmaBufAlignRemainder == 0 )
		{
			u16NumForDmaBufAlign = 0;
		}
		else
		{
			u16NumForDmaBufAlign = AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtDmaBufNum  - groupDmaBufAlignRemainder;
		}
        AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtTotalDmaIndex = (AdcIfDecTtInfo[u8AdcGrIndex-1].AdcIfTtTotalDmaIndex + AdcIfDecTtInfo[u8AdcGrIndex-1].AdcIfTtDmaBufNum + u16NumForDmaBufAlign);
    }
    else
    {
      /* If channel 0 is selected, set AdcIfTtTotalDmaIndex to "0" */
    }

  }
}

static void AdcIf_vCheckConfig(void)
{
    AdcIf_GroupChannelType  AdcIf_Config_Index = 0x00;
    AdcIf_UsageType         AdcIfTimeTrigger = AdcIf_UsgNone;
    AdcIf_UsageType         NumOfAdcIfTimeTrigger[ADC_MAX_NUM_TIMETRIGGER] = {0x00};
    AdcIf_GroupChannelType  AdcIfTimeTriggerAdGroup[ADC_MAX_NUM_TIMETRIGGER] = {0xff};
    AdcIf_DmaChannelType   AdcIfTimeTriggerDmaChId[ADC_MAX_NUM_TIMETRIGGER] = {0xff};
  uint8  u8AdcGrIndex = 0x00;
  uint8  u8AdcChIndex = 0x00;
  uint16 u16TotalUsedDmaBufSize = 0x0000;
  uint8 u8i = 0x00;
  AdcIf_ConfigErrInfo ErrIndex = AdcIf_NoConfigError;

  /* Init03_CheckConfig01 : Init local variable */
  for (u8i = 0x00; u8i < ADC_MAX_NUM_TIMETRIGGER; u8i++)
  {
    NumOfAdcIfTimeTrigger[u8i] = 0x00;
    AdcIfTimeTriggerAdGroup[u8i] = 0xFF; /* Intentionally set 0xFF */
    AdcIfTimeTriggerDmaChId[u8i] = 0xff; /* Intentionally set 0xFF */
  }

  /* Init03_CheckConfig02 : Check Configuration for
   *       1) AdcIf_Config_TimeTriggerTypeUsedInDifferentAdcGroup
   *       2) AdcIf_Config_TimeTriggerTypeUsedWithDifferentDmaChannel
   *       3) AdcIf_Config_TimeTriggerTypeUsedOverMaxNum
   *       4) AdcIf_Config_TotalDmaSizeOverflow
   *       5) AdcIf_Config_WrongAdcIfUsageType
   *  */
  for (AdcIf_Config_Index = 0x00; AdcIf_Config_Index < (ADCIF_NUM_OF_GROUP*ADCIF_NUM_OF_CHANNEL) ; AdcIf_Config_Index++)
  {
      u8AdcGrIndex = AdcIf_Config_Index / ADCIF_NUM_OF_GROUP;
      u8AdcChIndex = AdcIf_Config_Index % ADCIF_NUM_OF_GROUP;

      if ((AdcIf_UsgTimeTriggerStartIndex <= AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType) && (AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType < AdcIf_UsgTimeTriggerEndIndex))
      {
        /* Configuration check : AdcIf_Config_TimeTriggerTypeUsedInDifferentAdcGroup */
          if (AdcIfTimeTriggerAdGroup[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] == 0xFF)
          {
              AdcIfTimeTriggerAdGroup[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] = u8AdcGrIndex;
          }
          else
          {
            if ( AdcIfTimeTriggerAdGroup[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] != u8AdcGrIndex )
            {
                  #if (ADCIF_DETERR == ENABLE)
                DetErr_Report(ADCIF_MODULE_ID,
                                ADCIF_INSTANCE_ID,
                                ADCIF_API_CHECKCONFIG,
                                AdcIf_Config_TimeTriggerTypeUsedInDifferentAdcGroup
                                );
                  #endif /* #if (ADCIF_DETERR == ENABLE) */
            }
          }

          /* Configuration check : AdcIf_Config_TimeTriggerTypeUsedWithDifferentDmaChannel */
          if (AdcIfTimeTriggerDmaChId[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] == 0xFF)
          {
            AdcIfTimeTriggerDmaChId[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] = AdcIf_Config[AdcIf_Config_Index].AdcIfDmaChId;
          }
          else
          {
            if ( AdcIfTimeTriggerDmaChId[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] != AdcIf_Config[AdcIf_Config_Index].AdcIfDmaChId )
            {
                  #if (ADCIF_DETERR == ENABLE)
                DetErr_Report(ADCIF_MODULE_ID,
                                ADCIF_INSTANCE_ID,
                                ADCIF_API_CHECKCONFIG,
                                AdcIf_Config_TimeTriggerTypeUsedWithDifferentDmaChannel
                                );
                  #endif /* #if (ADCIF_DETERR == ENABLE) */
            }
          }

        /* Configuration check : AdcIf_Config_TimeTriggerTypeUsedOverMaxNum */
        NumOfAdcIfTimeTrigger[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex]++;
          if (NumOfAdcIfTimeTrigger[AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType - AdcIf_UsgTimeTriggerStartIndex] >= ADC_NUM_OF_TIMETRIGGER_LIMIT)
          {
              #if (ADCIF_DETERR == ENABLE)
            DetErr_Report(ADCIF_MODULE_ID,
                            ADCIF_INSTANCE_ID,
                            ADCIF_API_CHECKCONFIG,
                            AdcIf_Config_TimeTriggerTypeUsedOverMaxNum
                            );
              #endif /* #if (ADCIF_DETERR == ENABLE) */
          }
          else
          {
            /* Do nothing */
          }
      }
      else
      {
        /* Do nothing */
      }

      /* Configuration check : AdcIf_Config_TotalDmaSizeOverflow */
    u16TotalUsedDmaBufSize += AdcIf_Config[AdcIf_Config_Index].AdcIfDmaBufSize;
    if (u16TotalUsedDmaBufSize > ADCIF_MAX_TOTAL_DMABUFFER_SIZE)
    {
          #if (ADCIF_DETERR == ENABLE)
        DetErr_Report(ADCIF_MODULE_ID,
                        ADCIF_INSTANCE_ID,
                        ADCIF_API_CHECKCONFIG,
                        AdcIf_Config_TotalDmaSizeOverflow
                        );
          #endif /* #if (ADCIF_DETERR == ENABLE) */
    }
      else
      {
        /* Do nothing */
      }

      /* Configuration check : AdcIf_Config_WrongAdcIfUsageType */
      if (AdcIf_Config[AdcIf_Config_Index].AdcIfUsageType >= AdcIf_UsgTimeTriggerEndIndex)
      {
          #if (ADCIF_DETERR == ENABLE)
        DetErr_Report(ADCIF_MODULE_ID,
                        ADCIF_INSTANCE_ID,
                        ADCIF_API_CHECKCONFIG,
                        AdcIf_Config_WrongAdcIfUsageType
                        );
          #endif /* #if (ADCIF_DETERR == ENABLE) */
      }
      else
      {
        /* Do nothing */
      }
  }
}

static void AdcIf_vInitGroup (void)
{
    uint8 u8AdcGrIndex = 0x00;
    uint8 u8AdcChIndex = 0x00;
    uint16 u16AdcTtTotalDmaBufferIndex = 0x0000;
    uint8 u8AdcTxCnt = 0x00;
    AdcIf_DmaSetParamType AdcIfDmaSetParam;
    AdcIf_UsageType AdcGrTtidx = AdcIf_UsgTimeTriggerStartIndex;

    /* Init04_InitGroup01 : Set AUTOSAR ADC module and MCAL DMA according to configuration */
    for (u8AdcGrIndex = 0x00; u8AdcGrIndex < ADCIF_NUM_OF_GROUP; u8AdcGrIndex++)
    {
        /* Set ADC module for Software Trigger */
      if ((AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgNorm > 0x00) || (AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgTt > 0x00))
      {
        AdcIfResultBufferRet[u8AdcGrIndex] = Adc_SetupResultBuffer(AdcIf_GroupValue_SwTrigger[u8AdcGrIndex], &AdcIfGrResultBuffer[u8AdcGrIndex][0]);

        /*
        if (AdcIfResultBufferRet[u8AdcGrIndex] == E_OK)
        {
            Adc_EnableGroupNotification(AdcIf_GroupValue_SwTrigger[u8AdcGrIndex]);
        }
        else
        {
          
               Error handling or report error
          
        }
        */
      }
      else
      {
          /* do nothing */
      }

      /* Set ADC module and DMA for Hardware Trigger */
      if (AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgTt > 0x00)
      {

        /* Make base data for DMA register value */
        u16AdcTtTotalDmaBufferIndex = AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtTotalDmaIndex;
        u8AdcTxCnt = (AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtDmaBufNum) / (AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtChNum);
        u8AdcChIndex = AdcIfDecTtInfo[u8AdcGrIndex].AdcIfChStartId;
        AdcIfDmaSetParam.AdcIfDmaChId      = AdcIfInitInfo[u8AdcGrIndex].AdcIfDmaChId;
        //AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIfTtTotalDmaBuf[u16AdcTtTotalDmaBufferIndex]);
        //if(0 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_0[0]);}
        //if(1 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_1[0]);}
        //if(2 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_2[0]);}
        //if(3 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_3[0]);}
        //if(4 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_4[0]);}
        //if(5 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_5[0]);}
        //if(6 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_6[0]);}
        //if(7 == u8AdcGrIndex) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_7[0]);}
        if(AdcIf_UsgTimeTrigger0 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_0[0]);}
        if(AdcIf_UsgTimeTrigger1 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_1[0]);}
        if(AdcIf_UsgTimeTrigger2 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_2[0]);}
        if(AdcIf_UsgTimeTrigger3 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_3[0]);}
        if(AdcIf_UsgTimeTrigger4 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_4[0]);}
        if(AdcIf_UsgTimeTrigger5 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_5[0]);}
        if(AdcIf_UsgTimeTrigger6 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_6[0]);}
        if(AdcIf_UsgTimeTrigger7 == AdcIf_Config[((u8AdcGrIndex)*8)+u8AdcChIndex].AdcIfUsageType) {AdcIfDmaSetParam.AdcIfDmaDestAddr  = (uint32)&(AdcIf_DmaBufTt_7[0]);}
        
        AdcIfDmaSetParam.AdcIfDmaTxCnt     = u8AdcTxCnt;
        AdcIfDmaSetParam.AdcIfDmaNoOfMoves = AdcIfDecTtInfo[u8AdcGrIndex].AdcIfTtChNum;

        /* Call function for make DMA register value */
        AdcIf_vInitDma(u8AdcGrIndex, u8AdcChIndex, AdcIfDmaSetParam);
        
         /* Set ADC module */
        AdcIfResultBufferRet[u8AdcGrIndex] = Adc_SetupResultBuffer(AdcIf_GroupValue_HwTrigger[u8AdcGrIndex], &AdcIfGrResultBuffer[u8AdcGrIndex][0]);
        Adc_EnableHardwareTrigger(AdcIf_GroupValue_HwTrigger[u8AdcGrIndex]);
      }
      else
      {
          /* do nothing */
      }
      /* Initializing Number of Time Trigger of Group */
      if ((AdcIfDecTtInfo[u8AdcGrIndex].AdcIfChTtType >= AdcIf_UsgTimeTriggerStartIndex) && (AdcIfDecTtInfo[u8AdcGrIndex].AdcIfChTtType < AdcIf_UsgTimeTriggerEndIndex))
      {
          AdcNumofTt++;
      }
      else
      {
          /* do nothing */
      }
    }
    for(AdcGrTtidx = AdcIf_UsgTimeTriggerStartIndex ; AdcGrTtidx < AdcIf_UsgTimeTriggerStartIndex + AdcNumofTt ; AdcGrTtidx++)
    {
        AdcIf_InitGrTtIdx(AdcGrTtidx);
    }

    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0_SW_Trig);
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_1_SW_Trig);
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_2_SW_Trig);
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_3_SW_Trig);
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_4_SW_Trig);
	Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_6_SW_Trig);

}

static void AdcIf_vInitDma (uint8 u8AdcGrId, uint8 u8AdcChId, AdcIf_DmaSetParamType AdcIfDmaSetParam)
{
    uint32 u32DmaSrcAddr = 0x00000000;
    uint32 u32DmaDestAddr = 0x00000000;
    uint32 u32DmaACDIRVal = 0x00000000;
    uint8  u8DmaChId = 0x00;
    uint8  u8DmaNumOfMove = 0x00;
    uint32 u32DmaTxCnt = 0x00000000;

#if (ADCIF_DMAISR_TEST == ENABLE)
    volatile Ifx_SRC_SRCR *MoveEngCh0BaseAddr;
    volatile Ifx_SRC_SRCR *MoveEngTargetAddr;
    Ifx_SRC_SRCR EnableServiceReq;
#endif


    /* Start Section of ADC DMA */
    /* Step1: Assign Channel, Source address and Destination Address , Modify for new DMA */
    u8DmaChId = AdcIfDmaSetParam.AdcIfDmaChId;
    u32DmaSrcAddr = (AdcIf_ResultBufferRegAddr[u8AdcGrId][u8AdcChId]);
    u32DmaDestAddr = AdcIfDmaSetParam.AdcIfDmaDestAddr;
    u8DmaNumOfMove = AdcIfDmaSetParam.AdcIfDmaNoOfMoves;
    u32DmaTxCnt = AdcIfDmaSetParam.AdcIfDmaTxCnt;

    /* Step2: Load Source address and Destination Address to required DMA Channel*/
    Mcal_DmaSetSourceAddr(u8DmaChId, u32DmaSrcAddr);
    Mcal_DmaSetDestAddr(u8DmaChId, u32DmaDestAddr);

    /* Step3: Clear CHCR register */
    Mcal_DmaChClrCtlCfg(u8DmaChId);

    /* Step4: Set DMA Channel Configuration register : DMA_CHCFGRz */
    Mcal_DmaSetDataWidth(u8DmaChId, 1U);    // Set Channel Data Width: CHDW, 0=8bit, 1=16bit, 2=32bit ...
    Mcal_DmaSetTxCount(u8DmaChId, u32DmaTxCnt); // Set Transfer Reload Value: TREL, 1=one DMA transfer for DMA transaction.
    Mcal_DmaCfgNoOfMovesPerTransfer(u8DmaChId, u8DmaNumOfMove);  // Set Block Mode: BLKM, 1,2,4,8,16,3,5,9
    MODULE_DMA.CH[u8DmaChId].CHCFGR.B.CHMODE     = 1U;       // Set Channel Operation Mode : 0=Single, 1=Continuous
    MODULE_DMA.CH[u8DmaChId].CHCFGR.B.PRSEL  = 0U;   // Peripheral Request Select : 0=HW trigger selected, 1=Daisy chain selected.

    /* Step5: Set DMA Channel Address and Interrupt Control register : DMA_ADICR */
    AdcIf_vGetDmaAdcirRegVal(u8AdcGrId, &u32DmaACDIRVal);
    Mcal_DmaSetAdicr(u8DmaChId, u32DmaACDIRVal);

    /* Step6: Clear Interrupt on DMA channel */
    Mcal_DmaChClrIntrFlags(u8DmaChId);                       // Set DMA_CHCSRz.CICH,CWRP=1 : Clear interrupt flags for channel z
                                                                    // Set DMA_TSRz.CTL=1 : Clear DMA channel transaction request lost flag
    /* Step7: Set Interrupt Control and Interrupt Pointer */
    Mcal_DmaCfgIntControl(u8DmaChId, 2U);                        // Set DMA_ADICRx.INTCT : 0,1=No interrupt, 2=Interrupt generated...

#if (ADCIF_DMAISR_TEST == ENABLE)
    MoveEngCh0BaseAddr = ((Ifx_SRC_SRCR volatile *)(void *)&SRC_DMA_DMA0_CH0);
    MoveEngTargetAddr = ((Ifx_SRC_SRCR volatile *)(void *)&SRC_DMA_DMA0_CH0 + u8DmaChId);
    EnableServiceReq = *((volatile Ifx_SRC_SRCR *)(void *)MoveEngTargetAddr);

    //IntrSrc =          *((volatile Ifx_SRC_SRCR_Bits *)(void *)MoveEngCh0BaseAddr + Channel);
    EnableServiceReq.U |= 0x0400;
    *((volatile Ifx_SRC_SRCR *)(void *)MoveEngTargetAddr) = EnableServiceReq;
#endif


    //  Irq_InstallInterruptHandler ((uint32)IRQ_DMA_CHANNEL5_SR_PRIO, (uint32)&ADC5_DMA_ISR);
    /* Enable ADC HW trigger on selected DMA Channel */
    Mcal_DmaEnableHwTransfer(u8DmaChId); /* Replace below register control to MCAL DMA library */
    //DMA_TSR5.B.DCH = 0;     //Enable hardware transfer request, ECH=1 and DCH=0
    //DMA_TSR5.B.ECH = 1;     //Enable hardware transfer request, ECH=1 and DCH=0

    /* End Section of ADC DMA  */
}

static void AdcIf_InitGrTtIdx(AdcIf_UsageType AdcIfUsageTypeId)
{
    uint8  u8Index = 0x00;
    uint8  u8GrIndex = 0xFF;
    AdcIf_ConfigErrInfo ErrIndex = AdcIf_NoConfigError;

    /* find TimeTrigger Id */
    for (u8Index = 0x00; u8Index < ADCIF_NUM_OF_GROUP; u8Index++)
    {
        if (AdcIfDecTtInfo[u8Index].AdcIfChTtType == AdcIfUsageTypeId)
        {
            u8GrIndex = u8Index;
        }
    }

    if (u8GrIndex == 0xFF)
    {
          #if (ADCIF_DETERR == ENABLE)
        /* Report Error */
      //ErrIndex = AdcIfTimeTrigger_WrongTimeTriggerUsage;
      DetErr_Report(ADCIF_MODULE_ID,
                  ADCIF_INSTANCE_ID,
                  ADCIF_API_DECTIMETRIGGER,
                  AdcIfTimeTrigger_WrongTimeTriggerUsage
                  );
          #endif /* #if (ADCIF_DETERR == ENABLE) */
    }
    else
    {
        AdcTtGrIdx[AdcIfUsageTypeId] = u8GrIndex;
    }

}

/* Code End For ADCIF Init */

inline void AdcIf_DecTimeTrigger(AdcIf_UsageType AdcIfUsageTypeId)
{
      uint8  u8Index;
    uint8  u8GrIndex = AdcTtGrIdx[AdcIfUsageTypeId];
    uint8  u8AdcIfConfigIndex;
    uint16 u16DmaTtBufIndexStart;
    uint16 u16DmaTtBufIndexEnd;
    uint16 u16IndexCh;
    uint32 u32TempVal[ADCIF_NUM_OF_CHANNEL] = {0};
    uint8  TotalNumOfCh;
    uint16 TotalNumOfDmaBuf;
    uint16 NumOfDmaChBuf;

    TotalNumOfCh = AdcIfDecTtInfo[u8GrIndex].AdcIfTtChNum;
    TotalNumOfDmaBuf = AdcIfDecTtInfo[u8GrIndex].AdcIfTtDmaBufNum;
    u16DmaTtBufIndexStart = AdcIfDecTtInfo[u8GrIndex].AdcIfTtTotalDmaIndex;
    u16DmaTtBufIndexEnd = AdcIfDecTtInfo[u8GrIndex].AdcIfTtTotalDmaIndex + AdcIfDecTtInfo[u8GrIndex].AdcIfTtDmaBufNum;
    u8AdcIfConfigIndex = (u8GrIndex*ADCIF_NUM_OF_CHANNEL)+AdcIfDecTtInfo[u8GrIndex].AdcIfChStartId;

    /* Gather and accumulate requested adc value which are stored in DMA buffer */
    for (u16DmaTtBufIndexStart; u16DmaTtBufIndexStart < u16DmaTtBufIndexEnd; u16DmaTtBufIndexStart++)
    {
        u16IndexCh = u16DmaTtBufIndexStart % TotalNumOfCh;
        u32TempVal[u16IndexCh] = u32TempVal[u16IndexCh] + AdcIfTtTotalDmaBuf[u16DmaTtBufIndexStart];
    }

    /* Make average of accumulated adc value */
	if(TotalNumOfCh!=0)/* Apply LSP : Defencive code, to prevent divided by zero */
	{
	    NumOfDmaChBuf = TotalNumOfDmaBuf/TotalNumOfCh;
	}
    
    for (u8Index = 0x00; u8Index < TotalNumOfCh; u8Index++)
    {
         u32TempVal[u8Index] = (u32TempVal[u8Index] / NumOfDmaChBuf);
         AdcIfDecodingRawAdValue[u8AdcIfConfigIndex] = (uint16)u32TempVal[u8Index];
         u8AdcIfConfigIndex++;
    }


}

inline void AdcIf_DecTimeTrigger_0(void)
{
    uint32 u32TempVal1 = 0;
    uint32 u32TempVal2 = 0;
    
    u32TempVal1 = AdcIf_DmaBufTt_0[0] + AdcIf_DmaBufTt_0[2] + AdcIf_DmaBufTt_0[4] + AdcIf_DmaBufTt_0[6] + AdcIf_DmaBufTt_0[8] + AdcIf_DmaBufTt_0[10] + AdcIf_DmaBufTt_0[12] + AdcIf_DmaBufTt_0[14] + AdcIf_DmaBufTt_0[16] + AdcIf_DmaBufTt_0[18] + AdcIf_DmaBufTt_0[20] + AdcIf_DmaBufTt_0[22] + AdcIf_DmaBufTt_0[24] + AdcIf_DmaBufTt_0[26] + AdcIf_DmaBufTt_0[28] + AdcIf_DmaBufTt_0[30];
    u32TempVal2 = AdcIf_DmaBufTt_0[1] + AdcIf_DmaBufTt_0[3] + AdcIf_DmaBufTt_0[5] + AdcIf_DmaBufTt_0[7] + AdcIf_DmaBufTt_0[9] + AdcIf_DmaBufTt_0[11] + AdcIf_DmaBufTt_0[13] + AdcIf_DmaBufTt_0[15] + AdcIf_DmaBufTt_0[17] + AdcIf_DmaBufTt_0[19] + AdcIf_DmaBufTt_0[21] + AdcIf_DmaBufTt_0[23] + AdcIf_DmaBufTt_0[25] + AdcIf_DmaBufTt_0[27] + AdcIf_DmaBufTt_0[29] + AdcIf_DmaBufTt_0[31];
    
    u32TempVal1 = u32TempVal1 >> 4;
    u32TempVal2 = u32TempVal2 >> 4;
    
    AdcIfDecodingRawAdValue[AdcIf_Gr0Ch6] = u32TempVal1;
    AdcIfDecodingRawAdValue[AdcIf_Gr0Ch7] = u32TempVal2;

}

inline void AdcIf_DecTimeTrigger_1(void)
{
    uint32 u32TempVal1 = 0;
    uint32 u32TempVal2 = 0;
    
    u32TempVal1 = AdcIf_DmaBufTt_1[0] + AdcIf_DmaBufTt_1[2] + AdcIf_DmaBufTt_1[4] + AdcIf_DmaBufTt_1[6] + AdcIf_DmaBufTt_1[8] + AdcIf_DmaBufTt_1[10] + AdcIf_DmaBufTt_1[12] + AdcIf_DmaBufTt_1[14] + AdcIf_DmaBufTt_1[16] + AdcIf_DmaBufTt_1[18] + AdcIf_DmaBufTt_1[20] + AdcIf_DmaBufTt_1[22] + AdcIf_DmaBufTt_1[24] + AdcIf_DmaBufTt_1[26] + AdcIf_DmaBufTt_1[28] + AdcIf_DmaBufTt_1[30];
    u32TempVal2 = AdcIf_DmaBufTt_1[1] + AdcIf_DmaBufTt_1[3] + AdcIf_DmaBufTt_1[5] + AdcIf_DmaBufTt_1[7] + AdcIf_DmaBufTt_1[9] + AdcIf_DmaBufTt_1[11] + AdcIf_DmaBufTt_1[13] + AdcIf_DmaBufTt_1[15] + AdcIf_DmaBufTt_1[17] + AdcIf_DmaBufTt_1[19] + AdcIf_DmaBufTt_1[21] + AdcIf_DmaBufTt_1[23] + AdcIf_DmaBufTt_1[25] + AdcIf_DmaBufTt_1[27] + AdcIf_DmaBufTt_1[29] + AdcIf_DmaBufTt_1[31];
    
    u32TempVal1 = u32TempVal1 >> 4;
    u32TempVal2 = u32TempVal2 >> 4;
    
    AdcIfDecodingRawAdValue[AdcIf_Gr1Ch0] = u32TempVal1;
    AdcIfDecodingRawAdValue[AdcIf_Gr1Ch1] = u32TempVal2;
}

inline void AdcIf_DecTimeTrigger_2(void)
{
    uint32 u32TempVal1 = 0;
    uint32 u32TempVal2 = 0;

    
    u32TempVal1 = AdcIf_DmaBufTt_2[0] + AdcIf_DmaBufTt_2[2] + AdcIf_DmaBufTt_2[4] + AdcIf_DmaBufTt_2[6] + AdcIf_DmaBufTt_2[8] + AdcIf_DmaBufTt_2[10] + AdcIf_DmaBufTt_2[12] + AdcIf_DmaBufTt_2[14] + AdcIf_DmaBufTt_2[16] + AdcIf_DmaBufTt_2[18] + AdcIf_DmaBufTt_2[20] + AdcIf_DmaBufTt_2[22] + AdcIf_DmaBufTt_2[24] + AdcIf_DmaBufTt_2[26] + AdcIf_DmaBufTt_2[28] + AdcIf_DmaBufTt_2[30];
    u32TempVal2 = AdcIf_DmaBufTt_2[1] + AdcIf_DmaBufTt_2[3] + AdcIf_DmaBufTt_2[5] + AdcIf_DmaBufTt_2[7] + AdcIf_DmaBufTt_2[9] + AdcIf_DmaBufTt_2[11] + AdcIf_DmaBufTt_2[13] + AdcIf_DmaBufTt_2[15] + AdcIf_DmaBufTt_2[17] + AdcIf_DmaBufTt_2[19] + AdcIf_DmaBufTt_2[21] + AdcIf_DmaBufTt_2[23] + AdcIf_DmaBufTt_2[25] + AdcIf_DmaBufTt_2[27] + AdcIf_DmaBufTt_2[29] + AdcIf_DmaBufTt_2[31];

    
    u32TempVal1 = u32TempVal1 >> 4;
    u32TempVal2 = u32TempVal2 >> 4;
    
    AdcIfDecodingRawAdValue[AdcIf_Gr2Ch0] = u32TempVal1;
    AdcIfDecodingRawAdValue[AdcIf_Gr2Ch1] = u32TempVal2;
}

inline void AdcIf_DecTimeTrigger_3(void)
{
    uint32 u32TempVal1 = 0;
    uint32 u32TempVal2 = 0;
    
    u32TempVal1 = AdcIf_DmaBufTt_3[0] + AdcIf_DmaBufTt_3[2] + AdcIf_DmaBufTt_3[4] + AdcIf_DmaBufTt_3[6] + AdcIf_DmaBufTt_3[8] + AdcIf_DmaBufTt_3[10] + AdcIf_DmaBufTt_3[12] + AdcIf_DmaBufTt_3[14] + AdcIf_DmaBufTt_3[16] + AdcIf_DmaBufTt_3[18] + AdcIf_DmaBufTt_3[20] + AdcIf_DmaBufTt_3[22] + AdcIf_DmaBufTt_3[24] + AdcIf_DmaBufTt_3[26] + AdcIf_DmaBufTt_3[28] + AdcIf_DmaBufTt_3[30];
    u32TempVal2 = AdcIf_DmaBufTt_3[1] + AdcIf_DmaBufTt_3[3] + AdcIf_DmaBufTt_3[5] + AdcIf_DmaBufTt_3[7] + AdcIf_DmaBufTt_3[9] + AdcIf_DmaBufTt_3[11] + AdcIf_DmaBufTt_3[13] + AdcIf_DmaBufTt_3[15] + AdcIf_DmaBufTt_3[17] + AdcIf_DmaBufTt_3[19] + AdcIf_DmaBufTt_3[21] + AdcIf_DmaBufTt_3[23] + AdcIf_DmaBufTt_3[25] + AdcIf_DmaBufTt_3[27] + AdcIf_DmaBufTt_3[29] + AdcIf_DmaBufTt_3[31];
    
    u32TempVal1 = u32TempVal1 >> 4;
    u32TempVal2 = u32TempVal2 >> 4;
    
    AdcIfDecodingRawAdValue[AdcIf_Gr4Ch6] = u32TempVal1;
    AdcIfDecodingRawAdValue[AdcIf_Gr4Ch7] = u32TempVal2;
}

inline void AdcIf_DecTimeTrigger_4(void)
{
    uint32 u32TempVal1 = 0;
    uint32 u32TempVal2 = 0;
    
    u32TempVal1 = AdcIf_DmaBufTt_4[0] + AdcIf_DmaBufTt_4[2] + AdcIf_DmaBufTt_4[4] + AdcIf_DmaBufTt_4[6] + AdcIf_DmaBufTt_4[8] + AdcIf_DmaBufTt_4[10] + AdcIf_DmaBufTt_4[12] + AdcIf_DmaBufTt_4[14] + AdcIf_DmaBufTt_4[16] + AdcIf_DmaBufTt_4[18] + AdcIf_DmaBufTt_4[20] + AdcIf_DmaBufTt_4[22] + AdcIf_DmaBufTt_4[24] + AdcIf_DmaBufTt_4[26] + AdcIf_DmaBufTt_4[28] + AdcIf_DmaBufTt_4[30];
    u32TempVal2 = AdcIf_DmaBufTt_4[1] + AdcIf_DmaBufTt_4[3] + AdcIf_DmaBufTt_4[5] + AdcIf_DmaBufTt_4[7] + AdcIf_DmaBufTt_4[9] + AdcIf_DmaBufTt_4[11] + AdcIf_DmaBufTt_4[13] + AdcIf_DmaBufTt_4[15] + AdcIf_DmaBufTt_4[17] + AdcIf_DmaBufTt_4[19] + AdcIf_DmaBufTt_4[21] + AdcIf_DmaBufTt_4[23] + AdcIf_DmaBufTt_4[25] + AdcIf_DmaBufTt_4[27] + AdcIf_DmaBufTt_4[29] + AdcIf_DmaBufTt_4[31];
    
    u32TempVal1 = u32TempVal1 >> 4;
    u32TempVal2 = u32TempVal2 >> 4;
    
    AdcIfDecodingRawAdValue[AdcIf_Gr5Ch0] = u32TempVal1;
    AdcIfDecodingRawAdValue[AdcIf_Gr5Ch1] = u32TempVal2;
}

inline void AdcIf_DecTimeTrigger_5(void)
{
    uint32 u32TempVal1 = 0;
    uint32 u32TempVal2 = 0;
    
    u32TempVal1 = AdcIf_DmaBufTt_5[0] + AdcIf_DmaBufTt_5[2] + AdcIf_DmaBufTt_5[4] + AdcIf_DmaBufTt_5[6] + AdcIf_DmaBufTt_5[8] + AdcIf_DmaBufTt_5[10] + AdcIf_DmaBufTt_5[12] + AdcIf_DmaBufTt_5[14] + AdcIf_DmaBufTt_5[16] + AdcIf_DmaBufTt_5[18] + AdcIf_DmaBufTt_5[20] + AdcIf_DmaBufTt_5[22] + AdcIf_DmaBufTt_5[24] + AdcIf_DmaBufTt_5[26] + AdcIf_DmaBufTt_5[28] + AdcIf_DmaBufTt_5[30];
    u32TempVal2 = AdcIf_DmaBufTt_5[1] + AdcIf_DmaBufTt_5[3] + AdcIf_DmaBufTt_5[5] + AdcIf_DmaBufTt_5[7] + AdcIf_DmaBufTt_5[9] + AdcIf_DmaBufTt_5[11] + AdcIf_DmaBufTt_5[13] + AdcIf_DmaBufTt_5[15] + AdcIf_DmaBufTt_5[17] + AdcIf_DmaBufTt_5[19] + AdcIf_DmaBufTt_5[21] + AdcIf_DmaBufTt_5[23] + AdcIf_DmaBufTt_5[25] + AdcIf_DmaBufTt_5[27] + AdcIf_DmaBufTt_5[29] + AdcIf_DmaBufTt_5[31];
    
    u32TempVal1 = u32TempVal1 >> 4;
    u32TempVal2 = u32TempVal2 >> 4;
    
    AdcIfDecodingRawAdValue[AdcIf_Gr7Ch0] = u32TempVal1;
    AdcIfDecodingRawAdValue[AdcIf_Gr7Ch1] = u32TempVal2;
}

inline void AdcIf_DecTimeTrigger_6(void)
{

}

inline void AdcIf_DecTimeTrigger_7(void)
{

}

void AdcIf_StartNormalGroupConversion(void)
{
    Adc_StartGroupConversion_Direct(AdcConf_AdcGroup_AdcGroup_0_SW_Trig);
    Adc_StartGroupConversion_Direct(AdcConf_AdcGroup_AdcGroup_1_SW_Trig);
    Adc_StartGroupConversion_Direct(AdcConf_AdcGroup_AdcGroup_2_SW_Trig);
    Adc_StartGroupConversion_Direct(AdcConf_AdcGroup_AdcGroup_3_SW_Trig);
    Adc_StartGroupConversion_Direct(AdcConf_AdcGroup_AdcGroup_4_SW_Trig);
	Adc_StartGroupConversion_Direct(AdcConf_AdcGroup_AdcGroup_6_SW_Trig);
}

inline void AdcIf_DecNormal(void)
{
    uint8 u8Index = 0x00;
    uint8 u8AdcGrIndex = 0x00;
    uint8 u8AdcChIndex = 0x00;
    uint8 HwTtChNum = 0x00;
    uint8 SwNormChFlg = 0x00;
    
    /* DecNormal01 : Gather Software trigger adc value and store value in AdcIfDecodingRawAdValue
     *                AdcIf_GetValue function will pass this value to the caller */
    for(u8AdcGrIndex = 0 ; u8AdcGrIndex < ADCIF_NUM_OF_GROUP ; u8AdcGrIndex++)
    {
      for(u8AdcChIndex = 0 ; u8AdcChIndex < ADCIF_NUM_OF_CHANNEL ; u8AdcChIndex++)
      {
        SwNormChFlg  = (AdcIfInitInfo[u8AdcGrIndex].AdcIfChUsgNorm >> u8AdcChIndex)&0x01;
        if(SwNormChFlg == 1)
        {
	      Adc_IsrSrn1AdcRS1(u8AdcGrIndex);/* Calling MCAL Adc Api to remove Adc ISR */			  
          AdcIfGrTempBuffer[u8AdcGrIndex][u8AdcChIndex] = AdcIfGrResultBuffer[u8AdcGrIndex][u8AdcChIndex-HwTtChNum];   
		  u8Index = (u8AdcGrIndex*ADCIF_NUM_OF_CHANNEL) + u8AdcChIndex;
		  AdcIfDecodingRawAdValue[u8Index] = AdcIfGrTempBuffer[u8AdcGrIndex][u8AdcChIndex];		  
        }
        else
        {
          HwTtChNum++;
        }

      }
      HwTtChNum = 0;
    }

}

void AdcIf_vGetDmaAdcirRegVal(uint8 u8AdcGrId, uint32 *RegisterValue)
{
    uint32 u32RegisterValueTemp = 0;
    uint32 u32Temp              = 0;
    uint8  u8i                  = 0;

    /* 1. Set SMF */
    /*
     Source Address Modification Factor
     This bit field and the data width as defined in
     MExCHCR.CHDW determine an address offset value by
     which the source address is modified after each DMA move.
     000B Address offset is 1 x MExCHCR.CHDW
     001B Address offset is 2 x MExCHCR.CHDW
     010B Address offset is 4 x MExCHCR.CHDW
     011B Address offset is 8 x MExCHCR.CHDW
     100B Address offset is 16 x MExCHCR.CHDW
     101B Address offset is 32 x MExCHCR.CHDW
     110B Address offset is 64 x MExCHCR.CHDW
     111B Address offset is 128 x MExCHCR.CHDW
    */
    u32Temp = AdcIfDecTtInfo[u8AdcGrId].AdcIfTtChNum;
	/* Apply LSP : Address offset set to 1
    for (u8i = 0; u8i < ADCIF_DMA_MExADCIR_BITLENGTH; u8i++)
    {
    	if ((u32Temp >> u8i)==1)
    	{
    		u32Temp = u8i;
    		break;
    	}
    }
	*/
	u32Temp = 1;
	
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_SMF(u32Temp);

    /* 2. Set INCS */
    /*
     Increment of Source Address
     This bit determines whether the address offset as
     selected by SMF will be added to or subtracted from the
     source address after each DMA move. The source
     address is not modified if CBLS = 0000B.
     0B Address offset will be subtracted
     1B Address offset will be added.
     */
    /* Set value to 1 */
    u32Temp = 1;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_INCS(u32Temp);

    /* 3. Set DMF  */
    /*
     Destination Address Modification Factor
     This bit field and the data width as defined in
     MExCHCR.CHDW determines an address offset value
     by which the destination address is modified after each
     DMA move. The destination address is not modified if
     CBLD = 0000B.
     000B Address offset is 1 x MExCHCR.CHDW
     001B Address offset is 2 x MExCHCR.CHDW
     010B Address offset is 4 x MExCHCR.CHDW
     011B Address offset is 8 x MExCHCR.CHDW
     100B Address offset is 16 x MExCHCR.CHDW
     101B Address offset is 32 x MExCHCR.CHDW
     110B Address offset is 64 x MExCHCR.CHDW
     111B Address offset is 128 x MExCHCR.CHDW
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_DMF(u32Temp);

    /* 4. Set INCD  */
    /*
     Increment of Destination Address
     This bit determines whether the address offset as
     selected by DMF will be added to or subtracted from the
     destination address after each DMA move. The
     destination address is not modified if CBLD = 0000B.
     0B Address offset will be subtracted
     1B Address offset will be added
    */
    u32Temp = 1;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_INCD(u32Temp);

    /* 5. Set CBLS  */
    /*
     Circular Buffer Length Source
     This bit field determines which part of the 32-bit source
     address register remains unchanged and is not updated
     after a DMA move operation
     Therefore, CBLS also determines the size of the circular
     source buffer.
     0000B Source address SADR[31:0] is not updated
     0001B Source address SADR[31:1] is not updated
     0010B Source address SADR[31:2] is not updated
     0011B Source address SADR[31:3] is not updated
     ...B ...
     1110B Source address SADR[31:14] is not updated
     1111B Source address SADR[31:15] is not updated
    */
    u32Temp = AdcIfDecTtInfo[u8AdcGrId].AdcIfTtChNum;
    
	/* Apply LSP : */
	/*u32Temp = (u32Temp *2) - 1;*/    
	
    u32Temp = u32Temp*2;
    for (u8i = 0; u8i < ADCIF_DMA_MExADCIR_BITLENGTH; u8i++)
    {
    	if ((u32Temp >> u8i)==1)
    	{
    		u32Temp = u8i + 1;
    		break;
    	}
    }
    
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_CBLS(u32Temp);

    /* 6. Set CBLD  */
    /*
     Circular Buffer Length Destination
     This bit field determines which part of the 32-bit
     destination address register remains unchanged and is
     not updated after a DMA move operation.
     Therefore, CBLD also determines the size
     of the circular destination buffer.
     0000B Destination address DADR[31:0] is not updated
     0001B Destination address DADR[31:1] is not updated
     0010B Destination address DADR[31:2] is not updated
     0011B Destination address DADR[31:3] is not updated
     ...B ...
     1110B Destination address DADR[31:14] is not updated
     1111B Destination address DADR[31:15] is not updated
    */
	/* Apply LSP : */
    u32Temp = AdcIfDecTtInfo[u8AdcGrId].AdcIfTtDmaBufNum;
    for (u8i = 0; u8i < ADCIF_DMA_MExADCIR_BITLENGTH; u8i++)
    {
    	if ((u32Temp >> u8i)==1)
    	{
    		u32Temp = u8i + 1;
    		break;
    	}
    }    
    /*u32Temp++;*/
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_CBLD(u32Temp);

    /* 7. Set SHCT  */
    /*
     Shadow Control
     This bit field determines the function of the shadow
     address register.
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_SHCT(u32Temp);

    /* 8. Set SCBE  */
    /*
     Source Circular Buffer Enable
     0B Source circular buffer disabled
     1B Source circular buffer enabled
    */
    u32Temp = 1;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_SCBE(u32Temp);

    /* 9. Set DCBE  */
    /*
     Destination Circular Buffer Enable
     0B Destination circular buffer disabled
     1B Destination circular buffer enabled
    */
    u32Temp = 1;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_DCBE(u32Temp);

    /* 10. Set STAMP  */
    /*
     Time Stamp
     This bit enables the appendage of a timestamp after the
     end of the last DMA Move during a DMA transaction.
     0B No action.
     1B Enable the appendage of a 32-bit timestamp.
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_STAMP(u32Temp);

    /* 11. Set ETRL  */
    /*
     Enable Transaction Request Lost Interrupt
     This bit enables the generation of an interrupt when
     TSRz.TRLz is set.
     0B The interrupt generation for a request lost event for channel z is disabled.
     1B The interrupt generation for a request lost event for channel z is enabled.
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_ETRL(u32Temp);

    /* 12. Set WRPSE  */
    /*
     Wrap Source Enable
     0B Wrap source buffer interrupt trigger disabled
     1B Wrap source buffer interrupt trigger enabled
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_WRPSE(u32Temp);

    /* 13. Set WRPDE  */
    /*
     Wrap Destination Enable
     0B Wrap destination buffer interrupt trigger disabled
     1B Wrap destination buffer interrupt trigger enabled
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_WRPDE(u32Temp);

    /* 14. Set INTCT  */
    /*
     Interrupt Control
     00B No interrupt trigger will be generated on changing
     the TCOUNT value. The bit CHSRz.ICH is set
     when TCOUNT equals IRDV.
     01B No interrupt trigger will be generated on changing
     the TCOUNT value. The bit CHSRz.ICH is set
     when TCOUNT is decremented
     10B Interrupt trigger is generated and bit CHSRz.ICH
     is set on changing the TCOUNT value and
     TCOUNT equals IRDV
     11B Interrupt trigger is generated and bit CHSRz.ICH
     is set each time TCOUNT is decremented
    */
    u32Temp = 2;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_INTCT(u32Temp);

    /* 15. Set IRDV  */
    /*
     Interrupt Raise Detect Value
     Defines the Threshold Limit of CHSRz.TCOUNT for
     which a channel interrupt trigger will be raised.
    */
    u32Temp = 0;
    u32RegisterValueTemp |= ADCIF_DMA_MExADCIR_BITMASK_IRDV(u32Temp);

    /* return ADCIR register value */
    *RegisterValue = u32RegisterValueTemp;
}

void Adc_StartGroupConversion_Direct(Adc_GroupType Group)
{
  Adc_GlobalDataType *DataPtr;
  const Adc_GroupCfgType    *GrpPtr;
  uint32 GrpRequestSource;
  Adc_GroupType GroupId;
  uint8 Kernel;


  /* Get Kernel number from Group ID */
  Kernel = (uint8)Adc_lGetAdcKernel(Group);
  /* Get the Group Id for perticular kernel */
  GroupId = (Adc_GroupType)Adc_lGetKernelGroupId(Group);

    /* Assign the address of ASIL or QM signal variable to global data pointer*/
    DataPtr = Adc_lGetDataAddress(Kernel);
    /* Get the Adc Group Configuration Pointer */
  GrpPtr = Adc_kConfigPtr->CfgPtr[Kernel]->GrpCfgPtr + GroupId;
  /* Get the configured Group request source */
  GrpRequestSource = (uint32)(GrpPtr->GrpRequestSrc);


  /*-------------------------------**
  **  Update all Status Variables  **
  **-------------------------------*/
  /* Group status of given group is made ADC_BUSY */
    Adc_lSetGroupStatusBusyAtomic(DataPtr,GroupId);
    /* GrpId is updated with the requested Group */
    DataPtr->TriggSrcData[GrpRequestSource].GrpId = GroupId;


    /* Clear ADC Group Result Status */
    Adc_lClrGroupResultAtomic(DataPtr,GroupId);
    /* Initialize the number of conversion result set available to 0 */
    DataPtr->NumValidConRes[GroupId] = (Adc_StreamNumSampleType)0;


    /* Reset the End result status to 0 */
    Adc_lClrResBuffEndStatusAtomic(DataPtr,GroupId);


  switch (GrpRequestSource)
  {
  /* Background SCAN request source */
  case ADC_REQSRC2_BGND_SCAN:
    /* Conversion mode register is programmed for SW trigger */
    MODULE_VADC.BRSMR.B.LDEV = 1;
  break; /* Background Scan Request source */


  /* SCAN request source */
  case ADC_REQSRC1_NCH_SCAN:
    /* Conversion mode register is programmed for SW trigger */
    ADC_MODULE[Kernel].ASMR.B.LDEV = 1;
  break; /* Scan Request source */


  /* Queue request source ADC_REQSRC0_8STG_QUE is used as default case */
  default:
    /* Queue trigger by software */
    ADC_MODULE[Kernel].QMR0.B.TREV = 1;
  break;
  }
}
#define ADCIF_CONV50US_STOP_SEC_CODE
#include "AdcIf_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
