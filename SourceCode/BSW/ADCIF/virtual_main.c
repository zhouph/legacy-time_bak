/* Virtual main for linking */
/* purpose : Generation of map file */
#include "AdcIf_Conv50us.h"
#include "AdcIf_Conv1ms.h"

int main(void)
{
    AdcIf_Conv50us_Init();
    AdcIf_Conv1ms_Init();

    while(1)
    {
        AdcIf_Conv50us();
        AdcIf_Conv1ms();
    }
}