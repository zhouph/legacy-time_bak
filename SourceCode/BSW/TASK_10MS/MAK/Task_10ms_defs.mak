# \file
#
# \brief Task_10ms
#
# This file contains the implementation of the SWC
# module Task_10ms.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Task_10ms_CORE_PATH     := $(MANDO_BSW_ROOT)\Task_10ms
Task_10ms_CAL_PATH      := $(Task_10ms_CORE_PATH)\CAL
Task_10ms_SRC_PATH      := $(Task_10ms_CORE_PATH)\SRC
Task_10ms_CFG_PATH      := $(Task_10ms_CORE_PATH)\CFG\$(Task_10ms_VARIANT)
Task_10ms_HDR_PATH      := $(Task_10ms_CORE_PATH)\HDR
Task_10ms_IFA_PATH      := $(Task_10ms_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Task_10ms_CMN_PATH      := $(Task_10ms_CORE_PATH)\ICE\CMN
endif

CC_INCLUDE_PATH    += $(Task_10ms_CAL_PATH)
CC_INCLUDE_PATH    += $(Task_10ms_SRC_PATH)
CC_INCLUDE_PATH    += $(Task_10ms_CFG_PATH)
CC_INCLUDE_PATH    += $(Task_10ms_HDR_PATH)
CC_INCLUDE_PATH    += $(Task_10ms_IFA_PATH)
CC_INCLUDE_PATH    += $(Task_10ms_CMN_PATH)

