# \file
#
# \brief Task_10ms
#
# This file contains the implementation of the SWC
# module Task_10ms.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Task_10ms_src

Task_10ms_src_FILES        += $(Task_10ms_SRC_PATH)\Task_10ms.c
Task_10ms_src_FILES        += $(Task_10ms_IFA_PATH)\Task_10ms_Ifa.c
Task_10ms_src_FILES        += $(Task_10ms_CFG_PATH)\Task_10ms_Cfg.c
Task_10ms_src_FILES        += $(Task_10ms_CAL_PATH)\Task_10ms_Cal.c
