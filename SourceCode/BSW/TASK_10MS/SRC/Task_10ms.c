/**
 * @defgroup Task_10ms Task_10ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_10ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_10ms.h"
#include "Task_10ms_Ifa.h"
#include "Task_1ms.h"
#include "Task_5ms.h"
#include "Task_50us.h"
#include "Brake_Main.h"
#include "Ses_Ctrl.h"
/* Include runnables mapped to this task */
#include "Diag_Hndlr.h"
#include "CanM_Main.h"
#include "SenPwrM_Main.h"
#include "YawM_Main.h"
#include "AyM_Main.h"
#include "AxM_Main.h"
#include "SasM_Main.h"
#include "YawP_Main.h"
#include "AyP_Main.h"
#include "AxP_Main.h"
#include "SasP_Main.h"
#include "SwtM_Main.h"
#include "SwtP_Main.h"
#include "RlyM_Main.h"
#include "Proxy_Tx.h"
#include "Proxy_TxByCom.h"
#include "IfxCpu_reg.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TASK_10MS_START_SEC_CONST_UNSPECIFIED
#include "Task_10ms_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define TASK_10MS_STOP_SEC_CONST_UNSPECIFIED
#include "Task_10ms_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_10MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_10ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Task_10ms_HdrBusType Task_10msBus;

/*motor Internal flag*/
sint32 Int_10ms_IdbMotOrgSetFlg;
uint32_t	TimerSet_Abc_Before;
uint32_t	TimerSet_Abc_Now;
uint32_t	TimerSet_Abc;
uint32_t	TimerSet_Abc_after;
uint32_t 	TimerSet_Abc_elapsed;

uint32_t	TimerSet_Abcout_Before;
uint32_t	TimerSet_Abcout_Now;
uint32_t	TimerSet_Abcout;
uint32_t	TimerSet_Abcout_after;
uint32_t 	TimerSet_Abcout_elapsed;
uint32_t 	TimerSet_Abc_totalelapsed;


uint32_t	TimerSet_Abc_Logic_Before,TimerSet_Abc_Logic_elapsed,TimerSet_Abc_Logic_after;
uint32_t	TimerSet_Abc_Logic_Now;
uint32_t	TimerSet_Abc_Logic;
/* Version Info */
const SwcVersionInfo_t Task_10msVersionInfo = 
{
    TASK_10MS_MODULE_ID,       /* Task_10msVersionInfo.ModuleId */
    TASK_10MS_MAJOR_VERSION,   /* Task_10msVersionInfo.MajorVer */
    TASK_10MS_MINOR_VERSION,   /* Task_10msVersionInfo.MinorVer */
    TASK_10MS_PATCH_VERSION,   /* Task_10msVersionInfo.PatchVer */
    TASK_10MS_BRANCH_VERSION   /* Task_10msVersionInfo.BranchVer */
};

    
/* Input Data Element */
Wss_SenWhlSpdInfo_t Task_10ms_Diag_HndlrWhlSpdInfo;
Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo;
Proxy_RxCanRxRegenInfo_t Task_10ms_CanM_MainCanRxRegenInfo;
Proxy_RxCanRxAccelPedlInfo_t Task_10ms_CanM_MainCanRxAccelPedlInfo;
Proxy_RxCanRxIdbInfo_t Task_10ms_CanM_MainCanRxIdbInfo;
Proxy_RxCanRxEngTempInfo_t Task_10ms_CanM_MainCanRxEngTempInfo;
Proxy_RxCanRxEscInfo_t Task_10ms_CanM_MainCanRxEscInfo;
Wss_SenWhlSpdInfo_t Task_10ms_CanM_MainWhlSpdInfo;
Proxy_RxCanRxEemInfo_t Task_10ms_CanM_MainCanRxEemInfo;
Eem_SuspcDetnCanTimeOutStInfo_t Task_10ms_CanM_MainCanTimeOutStInfo;
Proxy_RxCanRxInfo_t Task_10ms_CanM_MainCanRxInfo;
Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_CanM_MainRxMsgOkFlgInfo;
Eem_MainEemFailData_t Task_10ms_SenPwrM_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_YawM_MainCanRxEscInfo;
Proxy_RxByComRxYawSerialInfo_t Task_10ms_YawM_MainRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t Task_10ms_YawM_MainRxYawAccInfo;
Proxy_RxByComRxLongAccInfo_t Task_10ms_YawM_MainRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_YawM_MainRxMsgOkFlgInfo;
Eem_MainEemFailData_t Task_10ms_YawM_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_AyM_MainCanRxEscInfo;
Proxy_RxByComRxYawSerialInfo_t Task_10ms_AyM_MainRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t Task_10ms_AyM_MainRxYawAccInfo;
Proxy_RxByComRxLongAccInfo_t Task_10ms_AyM_MainRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_AyM_MainRxMsgOkFlgInfo;
Eem_MainEemFailData_t Task_10ms_AyM_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_AxM_MainCanRxEscInfo;
Proxy_RxByComRxYawSerialInfo_t Task_10ms_AxM_MainRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t Task_10ms_AxM_MainRxYawAccInfo;
Proxy_RxByComRxLongAccInfo_t Task_10ms_AxM_MainRxLongAccInfo;
Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_AxM_MainRxMsgOkFlgInfo;
Eem_MainEemFailData_t Task_10ms_AxM_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_SasM_MainCanRxEscInfo;
Proxy_RxCanRxInfo_t Task_10ms_SasM_MainCanRxInfo;
Proxy_RxByComRxSasInfo_t Task_10ms_SasM_MainRxSasInfo;
Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_SasM_MainRxMsgOkFlgInfo;
Eem_MainEemFailData_t Task_10ms_SasM_MainEemFailData;
Eem_MainEemFailData_t Task_10ms_YawP_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_YawP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t Task_10ms_YawP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t Task_10ms_YawP_MainWhlSpdInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t Task_10ms_YawP_MainBaseBrkCtrlModInfo;
Eem_MainEemSuspectData_t Task_10ms_YawP_MainEemSuspectData;
Eem_MainEemEceData_t Task_10ms_YawP_MainEemEceData;
Proxy_RxIMUCalc_t Task_10ms_YawP_MainIMUCalcInfo;
Wss_SenWssSpeedOut_t Task_10ms_YawP_MainWssSpeedOut;
Wss_SenWssCalc_t Task_10ms_YawP_MainWssCalcInfo;
Eem_MainEemFailData_t Task_10ms_AyP_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_AyP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t Task_10ms_AyP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t Task_10ms_AyP_MainWhlSpdInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t Task_10ms_AyP_MainBaseBrkCtrlModInfo;
Eem_MainEemSuspectData_t Task_10ms_AyP_MainEemSuspectData;
Eem_MainEemEceData_t Task_10ms_AyP_MainEemEceData;
Proxy_RxIMUCalc_t Task_10ms_AyP_MainIMUCalcInfo;
Wss_SenWssSpeedOut_t Task_10ms_AyP_MainWssSpeedOut;
Wss_SenWssCalc_t Task_10ms_AyP_MainWssCalcInfo;
Eem_MainEemFailData_t Task_10ms_AxP_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_AxP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t Task_10ms_AxP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t Task_10ms_AxP_MainWhlSpdInfo;
Abc_CtrlAbsCtrlInfo_t Task_10ms_AxP_MainAbsCtrlInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t Task_10ms_AxP_MainBaseBrkCtrlModInfo;
Eem_MainEemSuspectData_t Task_10ms_AxP_MainEemSuspectData;
Eem_MainEemEceData_t Task_10ms_AxP_MainEemEceData;
Wss_SenWssSpeedOut_t Task_10ms_AxP_MainWssSpeedOut;
Eem_MainEemFailData_t Task_10ms_SasP_MainEemFailData;
Proxy_RxCanRxEscInfo_t Task_10ms_SasP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t Task_10ms_SasP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t Task_10ms_SasP_MainWhlSpdInfo;
Abc_CtrlAbsCtrlInfo_t Task_10ms_SasP_MainAbsCtrlInfo;
Abc_CtrlEscCtrlInfo_t Task_10ms_SasP_MainEscCtrlInfo;
Abc_CtrlTcsCtrlInfo_t Task_10ms_SasP_MainTcsCtrlInfo;
Eem_MainEemSuspectData_t Task_10ms_SasP_MainEemSuspectData;
Wss_SenWssSpeedOut_t Task_10ms_SasP_MainWssSpeedOut;
Ioc_InputCS1msSwtMonInfo_t Task_10ms_SwtM_MainSwtMonInfo;
Eem_MainEemFailData_t Task_10ms_SwtP_MainEemFailData;
Eem_MainEemCtrlInhibitData_t Task_10ms_SwtP_MainEemCtrlInhibitData;
AdcIf_Conv1msSwTrigPwrInfo_t Task_10ms_SwtP_MainSwTrigPwrInfo;
Pedal_SenSyncPdf5msRawInfo_t Task_10ms_SwtP_MainPdf5msRawInfo;
Ioc_InputCS1msSwtMonInfo_t Task_10ms_SwtP_MainSwtMonInfo;
Eem_MainEemSuspectData_t Task_10ms_SwtP_MainEemSuspectData;
Rly_ActrRlyDrvInfo_t Task_10ms_RlyM_MainRlyDrvInfo;
Ioc_InputCS1msRlyMonInfo_t Task_10ms_RlyM_MainRlyMonInfo;
Eem_MainEemCtrlInhibitData_t Task_10ms_Proxy_TxEemCtrlInhibitData;
Abc_CtrlCanTxInfo_t Task_10ms_Proxy_TxCanTxInfo;
Pedal_SenSyncPdt5msRawInfo_t Task_10ms_Proxy_TxPdt5msRawInfo;
Pedal_SenSyncPdf5msRawInfo_t Task_10ms_Proxy_TxPdf5msRawInfo;
Rbc_CtrlTarRgnBrkTqInfo_t Task_10ms_Proxy_TxTarRgnBrkTqInfo;
Swt_SenEscSwtStInfo_t Task_10ms_Proxy_TxEscSwtStInfo;
Wss_SenWhlSpdInfo_t Task_10ms_Proxy_TxWhlSpdInfo;
Wss_SenWhlEdgeCntInfo_t Task_10ms_Proxy_TxWhlEdgeCntInfo;
Press_SenSyncPistP5msRawInfo_t Task_10ms_Proxy_TxPistP5msRawInfo;
Nvm_HndlrLogicEepDataInfo_t Task_10ms_Proxy_TxLogicEepDataInfo;
Eem_MainEemLampData_t Task_10ms_Proxy_TxEemLampData;
Mom_HndlrEcuModeSts_t Task_10ms_Diag_HndlrEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_Diag_HndlrIgnOnOffSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_Diag_HndlrVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Task_10ms_Diag_HndlrVBatt2Mon;
Ioc_InputSR1msFspCbsMon_t Task_10ms_Diag_HndlrFspCbsMon;
Ioc_InputSR1msCEMon_t Task_10ms_Diag_HndlrCEMon;
Ioc_InputSR1msVddMon_t Task_10ms_Diag_HndlrVddMon;
Ioc_InputSR1msCspMon_t Task_10ms_Diag_HndlrCspMon;
Eem_SuspcDetnFuncInhibitDiagSts_t Task_10ms_Diag_HndlrFuncInhibitDiagSts;
Mtr_Processing_DiagMtrDiagState_t Task_10ms_Diag_HndlrMtrDiagState;
Mom_HndlrEcuModeSts_t Task_10ms_CanM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_CanM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_CanM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_CanM_MainVBatt1Mon;
Arbitrator_VlvArbVlvDriveState_t Task_10ms_CanM_MainArbVlvDriveState;
Ioc_InputSR1msCEMon_t Task_10ms_CanM_MainCEMon;
Proxy_RxCanRxGearSelDispErrInfo_t Task_10ms_CanM_MainCanRxGearSelDispErrInfo;
Arbitrator_MtrMtrArbDriveState_t Task_10ms_CanM_MainMtrArbDriveState;
Mom_HndlrEcuModeSts_t Task_10ms_SenPwrM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_SenPwrM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_SenPwrM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_SenPwrM_MainVBatt1Mon;
Arbitrator_VlvArbVlvDriveState_t Task_10ms_SenPwrM_MainArbVlvDriveState;
Ioc_InputSR1msExt5vMon_t Task_10ms_SenPwrM_MainExt5vMon;
Ioc_InputSR1msCspMon_t Task_10ms_SenPwrM_MainCspMon;
Arbitrator_MtrMtrArbDriveState_t Task_10ms_SenPwrM_MainMtrArbDriveState;
Mom_HndlrEcuModeSts_t Task_10ms_YawM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_YawM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_YawM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_YawM_MainVBatt1Mon;
Mom_HndlrEcuModeSts_t Task_10ms_AyM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_AyM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_AyM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_AyM_MainVBatt1Mon;
Mom_HndlrEcuModeSts_t Task_10ms_AxM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_AxM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_AxM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_AxM_MainVBatt1Mon;
Mom_HndlrEcuModeSts_t Task_10ms_SasM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_SasM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_SasM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_SasM_MainVBatt1Mon;
Mom_HndlrEcuModeSts_t Task_10ms_YawP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_YawP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_YawP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_YawP_MainVBatt1Mon;
Abc_CtrlVehSpd_t Task_10ms_YawP_MainVehSpd;
Mom_HndlrEcuModeSts_t Task_10ms_AyP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_AyP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_AyP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_AyP_MainVBatt1Mon;
Abc_CtrlVehSpd_t Task_10ms_AyP_MainVehSpd;
Mom_HndlrEcuModeSts_t Task_10ms_AxP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_AxP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_AxP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_AxP_MainVBatt1Mon;
Abc_CtrlVehSpd_t Task_10ms_AxP_MainVehSpd;
Mom_HndlrEcuModeSts_t Task_10ms_SasP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_SasP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_SasP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_SasP_MainVBatt1Mon;
Abc_CtrlVehSpd_t Task_10ms_SasP_MainVehSpd;
Mom_HndlrEcuModeSts_t Task_10ms_SwtM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_SwtM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_SwtM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_SwtM_MainVBatt1Mon;
Mom_HndlrEcuModeSts_t Task_10ms_SwtP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_SwtP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_SwtP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_SwtP_MainVBatt1Mon;
Det_5msCtrlVehSpdFild_t Task_10ms_SwtP_MainVehSpdFild;
Mom_HndlrEcuModeSts_t Task_10ms_RlyM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_RlyM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_10ms_RlyM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Task_10ms_RlyM_MainVBatt1Mon;
Mom_HndlrEcuModeSts_t Task_10ms_Proxy_TxEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_10ms_Proxy_TxIgnOnOffSts;
Eem_SuspcDetnFuncInhibitProxySts_t Task_10ms_Proxy_TxFuncInhibitProxySts;

/* Output Data Element */
Diag_HndlrDiagVlvActr_t Task_10ms_Diag_HndlrDiagVlvActrInfo;
Diag_HndlrMotReqDataDiagInfo_t Task_10ms_Diag_HndlrMotReqDataDiagInfo;
Diag_HndlrSasCalInfo_t Task_10ms_Diag_HndlrSasCalInfo;
Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
Diag_HndlrDiagHndlRequest_t Task_10ms_Diag_HndlrDiagHndlRequest;
CanM_MainCanMonData_t Task_10ms_CanM_MainCanMonData;
SenPwrM_MainSenPwrMonitor_t Task_10ms_SenPwrM_MainSenPwrMonitorData;
YawM_MainYAWMSerialInfo_t Task_10ms_YawM_MainYAWMSerialInfo;
YawM_MainYAWMOutInfo_t Task_10ms_YawM_MainYAWMOutInfo;
AyM_MainAYMOutInfo_t Task_10ms_AyM_MainAYMOuitInfo;
AxM_MainAXMOutInfo_t Task_10ms_AxM_MainAXMOutInfo;
SasM_MainSASMOutInfo_t Task_10ms_SasM_MainSASMOutInfo;
YawP_MainYawPlauOutput_t Task_10ms_YawP_MainYawPlauOutput;
AyP_MainAyPlauOutput_t Task_10ms_AyP_MainAyPlauOutput;
AxP_MainAxPlauOutput_t Task_10ms_AxP_MainAxPlauOutput;
SasP_MainSasPlauOutput_t Task_10ms_SasP_MainSasPlauOutput;
SwtM_MainSwtMonFaultInfo_t Task_10ms_SwtM_MainSwtMonFaultInfo;
SwtP_MainSwtPFaultInfo_t Task_10ms_SwtP_MainSwtPFaultInfo;
RlyM_MainRelayMonitor_t Task_10ms_RlyM_MainRelayMonitorData;
Proxy_TxTxESCSensorInfo_t Task_10ms_Proxy_TxTxESCSensorInfo;
Diag_HndlrDiagClr_t Task_10ms_Diag_HndlrDiagClrSrs;
Diag_HndlrDiagSci_t Task_10ms_Diag_HndlrDiagSci;
Diag_HndlrDiagAhbSci_t Task_10ms_Diag_HndlrDiagAhbSci;

#define TASK_10MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_10ms_MemMap.h"
#define TASK_10MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_10ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_10MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_10ms_MemMap.h"
#define TASK_10MS_START_SEC_VAR_UNSPECIFIED
#include "Task_10ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_10MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_10ms_MemMap.h"
#define TASK_10MS_START_SEC_VAR_32BIT
#include "Task_10ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_10MS_STOP_SEC_VAR_32BIT
#include "Task_10ms_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_10MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_10ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TASK_10MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_10ms_MemMap.h"
#define TASK_10MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_10ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_10MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_10ms_MemMap.h"
#define TASK_10MS_START_SEC_VAR_UNSPECIFIED
#include "Task_10ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_10MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_10ms_MemMap.h"
#define TASK_10MS_START_SEC_VAR_32BIT
#include "Task_10ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_10MS_STOP_SEC_VAR_32BIT
#include "Task_10ms_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TASK_10MS_START_SEC_CODE
#include "Task_10ms_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Task_10ms_Init(void)
{    
    Diag_Hndlr_Init();
    CanM_Main_Init();
    SenPwrM_Main_Init();
    YawM_Main_Init();
    AyM_Main_Init();
    AxM_Main_Init();
    SasM_Main_Init();
    YawP_Main_Init();
    AyP_Main_Init();
    AxP_Main_Init();
    SasP_Main_Init();
    SwtM_Main_Init();
    SwtP_Main_Init();
    RlyM_Main_Init();
    Proxy_Tx_Init();
    Proxy_TxByCom_Init();
    
    /* Initialize internal bus */
}

void Task_10ms(void)
{
    uint16 i;
    /* "Read Interfaces" from <other Task> or <ISR Event>
    Assembly connection event between Tasks or Task and ISR
    for example #1) Task1msInterface1 = Task10msInterface1;
    for example #2) Task1msInterface1.Signal1 = Task10msInterface1.Signal1;
    for example #3) In case of Queue at <Shared RAM>, Task1msInterface1 = deQueue(&q);
    Therefore, it's possible to avoid memory violation.*/
    /* Sample interface */
    /* Structure interface */
    Task_10ms_Diag_HndlrWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo = Task_5ms_Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
    Task_10ms_CanM_MainCanRxRegenInfo = Task_5ms_Proxy_RxCanRxRegenInfo;
    Task_10ms_CanM_MainCanRxAccelPedlInfo = Task_5ms_Proxy_RxCanRxAccelPedlInfo;
    Task_10ms_CanM_MainCanRxIdbInfo = Task_5ms_Proxy_RxCanRxIdbInfo;
    Task_10ms_CanM_MainCanRxEngTempInfo = Task_5ms_Proxy_RxCanRxEngTempInfo;
    Task_10ms_CanM_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_CanM_MainWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_CanM_MainCanRxEemInfo = Task_5ms_Proxy_RxCanRxEemInfo;
    Task_10ms_CanM_MainCanTimeOutStInfo = Task_5ms_Eem_SuspcDetnCanTimeOutStInfo;
    Task_10ms_CanM_MainCanRxInfo = Task_5ms_Proxy_RxCanRxInfo;
    Task_10ms_CanM_MainRxMsgOkFlgInfo = Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
    Task_10ms_SenPwrM_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_YawM_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_YawM_MainRxYawSerialInfo = Task_5ms_Proxy_RxByComRxYawSerialInfo;
    Task_10ms_YawM_MainRxYawAccInfo = Task_5ms_Proxy_RxByComRxYawAccInfo;
    Task_10ms_YawM_MainRxLongAccInfo = Task_5ms_Proxy_RxByComRxLongAccInfo;
    Task_10ms_YawM_MainRxMsgOkFlgInfo = Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
    Task_10ms_YawM_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_AyM_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_AyM_MainRxYawSerialInfo = Task_5ms_Proxy_RxByComRxYawSerialInfo;
    Task_10ms_AyM_MainRxYawAccInfo = Task_5ms_Proxy_RxByComRxYawAccInfo;
    Task_10ms_AyM_MainRxLongAccInfo = Task_5ms_Proxy_RxByComRxLongAccInfo;
    Task_10ms_AyM_MainRxMsgOkFlgInfo = Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
    Task_10ms_AyM_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_AxM_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_AxM_MainRxYawSerialInfo = Task_5ms_Proxy_RxByComRxYawSerialInfo;
    Task_10ms_AxM_MainRxYawAccInfo = Task_5ms_Proxy_RxByComRxYawAccInfo;
    Task_10ms_AxM_MainRxLongAccInfo = Task_5ms_Proxy_RxByComRxLongAccInfo;
    Task_10ms_AxM_MainRxMsgOkFlgInfo = Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
    Task_10ms_AxM_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_SasM_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_SasM_MainCanRxInfo = Task_5ms_Proxy_RxCanRxInfo;
    Task_10ms_SasM_MainRxSasInfo = Task_5ms_Proxy_RxByComRxSasInfo;
    Task_10ms_SasM_MainRxMsgOkFlgInfo = Task_5ms_Proxy_RxByComRxMsgOkFlgInfo;
    Task_10ms_SasM_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_YawP_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_YawP_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_YawP_MainEscSwtStInfo = Task_5ms_Swt_SenEscSwtStInfo;
    Task_10ms_YawP_MainWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_YawP_MainBaseBrkCtrlModInfo = Task_5ms_Bbc_CtrlBaseBrkCtrlModInfo;
    Task_10ms_YawP_MainEemSuspectData = Task_5ms_Eem_MainEemSuspectData;
    Task_10ms_YawP_MainEemEceData = Task_5ms_Eem_MainEemEceData;
    Task_10ms_YawP_MainIMUCalcInfo = Task_5ms_Proxy_RxIMUCalcInfo;
    Task_10ms_YawP_MainWssSpeedOut = Task_5ms_Wss_SenWssSpeedOut;
    Task_10ms_YawP_MainWssCalcInfo = Task_5ms_Wss_SenWssCalcInfo;
    Task_10ms_AyP_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_AyP_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_AyP_MainEscSwtStInfo = Task_5ms_Swt_SenEscSwtStInfo;
    Task_10ms_AyP_MainWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_AyP_MainBaseBrkCtrlModInfo = Task_5ms_Bbc_CtrlBaseBrkCtrlModInfo;
    Task_10ms_AyP_MainEemSuspectData = Task_5ms_Eem_MainEemSuspectData;
    Task_10ms_AyP_MainEemEceData = Task_5ms_Eem_MainEemEceData;
    Task_10ms_AyP_MainIMUCalcInfo = Task_5ms_Proxy_RxIMUCalcInfo;
    Task_10ms_AyP_MainWssSpeedOut = Task_5ms_Wss_SenWssSpeedOut;
    Task_10ms_AyP_MainWssCalcInfo = Task_5ms_Wss_SenWssCalcInfo;
    Task_10ms_AxP_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_AxP_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_AxP_MainEscSwtStInfo = Task_5ms_Swt_SenEscSwtStInfo;
    Task_10ms_AxP_MainWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_AxP_MainAbsCtrlInfo = Task_5ms_Abc_CtrlAbsCtrlInfo;
    Task_10ms_AxP_MainBaseBrkCtrlModInfo = Task_5ms_Bbc_CtrlBaseBrkCtrlModInfo;
    Task_10ms_AxP_MainEemSuspectData = Task_5ms_Eem_MainEemSuspectData;
    Task_10ms_AxP_MainEemEceData = Task_5ms_Eem_MainEemEceData;
    Task_10ms_AxP_MainWssSpeedOut = Task_5ms_Wss_SenWssSpeedOut;
    Task_10ms_SasP_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_10ms_SasP_MainCanRxEscInfo = Task_5ms_Proxy_RxCanRxEscInfo;
    Task_10ms_SasP_MainEscSwtStInfo = Task_5ms_Swt_SenEscSwtStInfo;
    Task_10ms_SasP_MainWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_SasP_MainAbsCtrlInfo = Task_5ms_Abc_CtrlAbsCtrlInfo;
    Task_10ms_SasP_MainEscCtrlInfo = Task_5ms_Abc_CtrlEscCtrlInfo;
    Task_10ms_SasP_MainTcsCtrlInfo = Task_5ms_Abc_CtrlTcsCtrlInfo;
    Task_10ms_SasP_MainEemSuspectData = Task_5ms_Eem_MainEemSuspectData;
    Task_10ms_SasP_MainWssSpeedOut = Task_5ms_Wss_SenWssSpeedOut;
    Task_10ms_SwtM_MainSwtMonInfo = Task_1ms_Ioc_InputCS1msSwtMonInfo;
    Task_10ms_SwtP_MainEemFailData = Task_5ms_Eem_MainEemFailData;
    //Task_10ms_SwtP_MainEemCtrlInhibitData = Task_5ms_Eem_MainEemCtrlInhibitData;
    Task_10ms_SwtP_MainSwTrigPwrInfo = Task_1ms_AdcIf_Conv1msSwTrigPwrInfo;
    Task_10ms_SwtP_MainPdf5msRawInfo = Task_5ms_Pedal_SenSyncPdf5msRawInfo;
    Task_10ms_SwtP_MainSwtMonInfo = Task_1ms_Ioc_InputCS1msSwtMonInfo;
    Task_10ms_SwtP_MainEemSuspectData = Task_5ms_Eem_MainEemSuspectData;
    Task_10ms_RlyM_MainRlyDrvInfo = Task_5ms_Rly_ActrRlyDrvInfo;
    Task_10ms_RlyM_MainRlyMonInfo = Task_1ms_Ioc_InputCS1msRlyMonInfo;
    //Task_10ms_Proxy_TxEemCtrlInhibitData = Task_5ms_Eem_MainEemCtrlInhibitData;
    Task_10ms_Proxy_TxCanTxInfo = Task_5ms_Abc_CtrlCanTxInfo;
    Task_10ms_Proxy_TxPdt5msRawInfo = Task_5ms_Pedal_SenSyncPdt5msRawInfo;
    Task_10ms_Proxy_TxPdf5msRawInfo = Task_5ms_Pedal_SenSyncPdf5msRawInfo;
    Task_10ms_Proxy_TxTarRgnBrkTqInfo = Task_5ms_Rbc_CtrlTarRgnBrkTqInfo;
    Task_10ms_Proxy_TxEscSwtStInfo = Task_5ms_Swt_SenEscSwtStInfo;
    Task_10ms_Proxy_TxWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_10ms_Proxy_TxWhlEdgeCntInfo = Task_5ms_Wss_SenWhlEdgeCntInfo;
    Task_10ms_Proxy_TxPistP5msRawInfo = Task_5ms_Press_SenSyncPistP5msRawInfo;
    Task_10ms_Proxy_TxLogicEepDataInfo = Task_5ms_Nvm_HndlrLogicEepDataInfo;
    Task_10ms_Proxy_TxEemLampData = Task_5ms_Eem_MainEemLampData;

    /* Single interface */
    //Task_10ms_Diag_HndlrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_Diag_HndlrIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_Diag_HndlrVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_Diag_HndlrVBatt2Mon = Task_1ms_Ioc_InputSR1msVBatt2Mon;
    Task_10ms_Diag_HndlrFspCbsMon = Task_1ms_Ioc_InputSR1msFspCbsMon;
    Task_10ms_Diag_HndlrCEMon = Task_1ms_Ioc_InputSR1msCEMon;
    Task_10ms_Diag_HndlrVddMon = Task_1ms_Ioc_InputSR1msVddMon;
    Task_10ms_Diag_HndlrCspMon = Task_1ms_Ioc_InputSR1msCspMon;
    //Task_10ms_Diag_HndlrFuncInhibitDiagSts = Task_5ms_Eem_SuspcDetnFuncInhibitDiagSts;
    Task_10ms_Diag_HndlrMtrDiagState = Task_1ms_Mtr_Processing_DiagMtrDiagState;
    //Task_10ms_CanM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_CanM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_CanM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_CanM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_CanM_MainArbVlvDriveState = Task_5ms_Arbitrator_VlvArbVlvDriveState;
    Task_10ms_CanM_MainCEMon = Task_1ms_Ioc_InputSR1msCEMon;
    Task_10ms_CanM_MainCanRxGearSelDispErrInfo = Task_5ms_Proxy_RxCanRxGearSelDispErrInfo;
    Task_10ms_CanM_MainMtrArbDriveState = Task_1ms_Arbitrator_MtrMtrArbDriveState;
    //Task_10ms_SenPwrM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_SenPwrM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_SenPwrM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_SenPwrM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_SenPwrM_MainArbVlvDriveState = Task_5ms_Arbitrator_VlvArbVlvDriveState;
    Task_10ms_SenPwrM_MainExt5vMon = Task_1ms_Ioc_InputSR1msExt5vMon;
    Task_10ms_SenPwrM_MainCspMon = Task_1ms_Ioc_InputSR1msCspMon;
    Task_10ms_SenPwrM_MainMtrArbDriveState = Task_1ms_Arbitrator_MtrMtrArbDriveState;
    //Task_10ms_YawM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_YawM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_YawM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_YawM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    //Task_10ms_AyM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_AyM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_AyM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_AyM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    //Task_10ms_AxM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_AxM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_AxM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_AxM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    //Task_10ms_SasM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_SasM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_SasM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_SasM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    //Task_10ms_YawP_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_YawP_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_YawP_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_YawP_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_YawP_MainVehSpd = Task_5ms_Abc_CtrlVehSpd;
    //Task_10ms_AyP_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_AyP_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_AyP_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_AyP_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_AyP_MainVehSpd = Task_5ms_Abc_CtrlVehSpd;
    //Task_10ms_AxP_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_AxP_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_AxP_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_AxP_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_AxP_MainVehSpd = Task_5ms_Abc_CtrlVehSpd;
    //Task_10ms_SasP_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_SasP_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_SasP_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_SasP_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_SasP_MainVehSpd = Task_5ms_Abc_CtrlVehSpd;
    //Task_10ms_SwtM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_SwtM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_SwtM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_SwtM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    //Task_10ms_SwtP_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_SwtP_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_SwtP_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_SwtP_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    Task_10ms_SwtP_MainVehSpdFild = Task_5ms_Det_5msCtrlVehSpdFild;
    //Task_10ms_RlyM_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_RlyM_MainIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_10ms_RlyM_MainIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_10ms_RlyM_MainVBatt1Mon = Task_1ms_Ioc_InputSR1msVBatt1Mon;
    //Task_10ms_Proxy_TxEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_10ms_Proxy_TxIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    //Task_10ms_Proxy_TxFuncInhibitProxySts = Task_5ms_Eem_SuspcDetnFuncInhibitProxySts;

    /* End of Sample interface

    /* Input */
    /* Structure interface */
    Task_10ms_Read_Diag_HndlrWhlSpdInfo(&Task_10msBus.Diag_HndlrWhlSpdInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrWhlSpdInfo 
     : Diag_HndlrWhlSpdInfo.FlWhlSpd;
     : Diag_HndlrWhlSpdInfo.FrWhlSpd;
     : Diag_HndlrWhlSpdInfo.RlWhlSpd;
     : Diag_HndlrWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Task_10ms_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo(&Task_10msBus.Diag_HndlrIdbSnsrEolOfsCalcInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrIdbSnsrEolOfsCalcInfo 
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxRegenInfo(&Task_10msBus.CanM_MainCanRxRegenInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxRegenInfo 
     : CanM_MainCanRxRegenInfo.HcuRegenEna;
     : CanM_MainCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxAccelPedlInfo(&Task_10msBus.CanM_MainCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxAccelPedlInfo 
     : CanM_MainCanRxAccelPedlInfo.AccelPedlVal;
     : CanM_MainCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxIdbInfo(&Task_10msBus.CanM_MainCanRxIdbInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxIdbInfo 
     : CanM_MainCanRxIdbInfo.EngMsgFault;
     : CanM_MainCanRxIdbInfo.TarGearPosi;
     : CanM_MainCanRxIdbInfo.GearSelDisp;
     : CanM_MainCanRxIdbInfo.TcuSwiGs;
     : CanM_MainCanRxIdbInfo.HcuServiceMod;
     : CanM_MainCanRxIdbInfo.SubCanBusSigFault;
     : CanM_MainCanRxIdbInfo.CoolantTemp;
     : CanM_MainCanRxIdbInfo.CoolantTempErr;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxEngTempInfo(&Task_10msBus.CanM_MainCanRxEngTempInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxEngTempInfo 
     : CanM_MainCanRxEngTempInfo.EngTemp;
     : CanM_MainCanRxEngTempInfo.EngTempErr;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxEscInfo(&Task_10msBus.CanM_MainCanRxEscInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxEscInfo 
     : CanM_MainCanRxEscInfo.Ax;
     : CanM_MainCanRxEscInfo.YawRate;
     : CanM_MainCanRxEscInfo.SteeringAngle;
     : CanM_MainCanRxEscInfo.Ay;
     : CanM_MainCanRxEscInfo.PbSwt;
     : CanM_MainCanRxEscInfo.ClutchSwt;
     : CanM_MainCanRxEscInfo.GearRSwt;
     : CanM_MainCanRxEscInfo.EngActIndTq;
     : CanM_MainCanRxEscInfo.EngRpm;
     : CanM_MainCanRxEscInfo.EngIndTq;
     : CanM_MainCanRxEscInfo.EngFrictionLossTq;
     : CanM_MainCanRxEscInfo.EngStdTq;
     : CanM_MainCanRxEscInfo.TurbineRpm;
     : CanM_MainCanRxEscInfo.ThrottleAngle;
     : CanM_MainCanRxEscInfo.TpsResol1000;
     : CanM_MainCanRxEscInfo.PvAvCanResol1000;
     : CanM_MainCanRxEscInfo.EngChr;
     : CanM_MainCanRxEscInfo.EngVol;
     : CanM_MainCanRxEscInfo.GearType;
     : CanM_MainCanRxEscInfo.EngClutchState;
     : CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv;
     : CanM_MainCanRxEscInfo.MotEstTq;
     : CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv;
     : CanM_MainCanRxEscInfo.TqIntvTCU;
     : CanM_MainCanRxEscInfo.TqIntvSlowTCU;
     : CanM_MainCanRxEscInfo.TqIncReq;
     : CanM_MainCanRxEscInfo.DecelReq;
     : CanM_MainCanRxEscInfo.RainSnsStat;
     : CanM_MainCanRxEscInfo.EngSpdErr;
     : CanM_MainCanRxEscInfo.AtType;
     : CanM_MainCanRxEscInfo.MtType;
     : CanM_MainCanRxEscInfo.CvtType;
     : CanM_MainCanRxEscInfo.DctType;
     : CanM_MainCanRxEscInfo.HevAtTcu;
     : CanM_MainCanRxEscInfo.TurbineRpmErr;
     : CanM_MainCanRxEscInfo.ThrottleAngleErr;
     : CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct;
     : CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV;
     : CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct;
     : CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV;
     : CanM_MainCanRxEscInfo.WiperIntSW;
     : CanM_MainCanRxEscInfo.WiperLow;
     : CanM_MainCanRxEscInfo.WiperHigh;
     : CanM_MainCanRxEscInfo.WiperValid;
     : CanM_MainCanRxEscInfo.WiperAuto;
     : CanM_MainCanRxEscInfo.TcuFaultSts;
     =============================================================================*/

    Task_10ms_Read_CanM_MainWhlSpdInfo(&Task_10msBus.CanM_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure CanM_MainWhlSpdInfo 
     : CanM_MainWhlSpdInfo.FlWhlSpd;
     : CanM_MainWhlSpdInfo.FrWhlSpd;
     : CanM_MainWhlSpdInfo.RlWhlSpd;
     : CanM_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxEemInfo(&Task_10msBus.CanM_MainCanRxEemInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxEemInfo 
     : CanM_MainCanRxEemInfo.YawRateInvld;
     : CanM_MainCanRxEemInfo.AyInvld;
     : CanM_MainCanRxEemInfo.SteeringAngleRxOk;
     : CanM_MainCanRxEemInfo.AxInvldData;
     : CanM_MainCanRxEemInfo.Ems1RxErr;
     : CanM_MainCanRxEemInfo.Ems2RxErr;
     : CanM_MainCanRxEemInfo.Tcu1RxErr;
     : CanM_MainCanRxEemInfo.Tcu5RxErr;
     : CanM_MainCanRxEemInfo.Hcu1RxErr;
     : CanM_MainCanRxEemInfo.Hcu2RxErr;
     : CanM_MainCanRxEemInfo.Hcu3RxErr;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanTimeOutStInfo(&Task_10msBus.CanM_MainCanTimeOutStInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanTimeOutStInfo 
     : CanM_MainCanTimeOutStInfo.MaiCanSusDet;
     : CanM_MainCanTimeOutStInfo.EmsTiOutSusDet;
     : CanM_MainCanTimeOutStInfo.TcuTiOutSusDet;
     =============================================================================*/

    Task_10ms_Read_CanM_MainCanRxInfo(&Task_10msBus.CanM_MainCanRxInfo);
    /*==============================================================================
    * Members of structure CanM_MainCanRxInfo 
     : CanM_MainCanRxInfo.MainBusOffFlag;
     : CanM_MainCanRxInfo.SenBusOffFlag;
     =============================================================================*/

    Task_10ms_Read_CanM_MainRxMsgOkFlgInfo(&Task_10msBus.CanM_MainRxMsgOkFlgInfo);
    /*==============================================================================
    * Members of structure CanM_MainRxMsgOkFlgInfo 
     : CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_12V(&Task_10msBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V);
    Task_10ms_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_5V(&Task_10msBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V);

    /* Decomposed structure interface */
    Task_10ms_Read_YawM_MainCanRxEscInfo_YawRate(&Task_10msBus.YawM_MainCanRxEscInfo.YawRate);

    Task_10ms_Read_YawM_MainRxYawSerialInfo(&Task_10msBus.YawM_MainRxYawSerialInfo);
    /*==============================================================================
    * Members of structure YawM_MainRxYawSerialInfo 
     : YawM_MainRxYawSerialInfo.YawSerialNum_0;
     : YawM_MainRxYawSerialInfo.YawSerialNum_1;
     : YawM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_YawM_MainRxYawAccInfo_YawRateValidData(&Task_10msBus.YawM_MainRxYawAccInfo.YawRateValidData);
    Task_10ms_Read_YawM_MainRxYawAccInfo_YawRateSelfTestStatus(&Task_10msBus.YawM_MainRxYawAccInfo.YawRateSelfTestStatus);
    Task_10ms_Read_YawM_MainRxYawAccInfo_SensorOscFreqDev(&Task_10msBus.YawM_MainRxYawAccInfo.SensorOscFreqDev);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Gyro_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Gyro_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Raster_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Raster_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Eep_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Eep_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Batt_Range_Err(&Task_10msBus.YawM_MainRxYawAccInfo.Batt_Range_Err);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Asic_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Asic_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Accel_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Accel_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Ram_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Ram_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Rom_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Rom_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Ad_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Ad_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Osc_Fail(&Task_10msBus.YawM_MainRxYawAccInfo.Osc_Fail);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Watchdog_Rst(&Task_10msBus.YawM_MainRxYawAccInfo.Watchdog_Rst);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Plaus_Err_Pst(&Task_10msBus.YawM_MainRxYawAccInfo.Plaus_Err_Pst);
    Task_10ms_Read_YawM_MainRxYawAccInfo_RollingCounter(&Task_10msBus.YawM_MainRxYawAccInfo.RollingCounter);
    Task_10ms_Read_YawM_MainRxYawAccInfo_Can_Func_Err(&Task_10msBus.YawM_MainRxYawAccInfo.Can_Func_Err);

    /* Decomposed structure interface */
    Task_10ms_Read_YawM_MainRxLongAccInfo_IntSenFltSymtmActive(&Task_10msBus.YawM_MainRxLongAccInfo.IntSenFltSymtmActive);
    Task_10ms_Read_YawM_MainRxLongAccInfo_IntSenFaultPresent(&Task_10msBus.YawM_MainRxLongAccInfo.IntSenFaultPresent);
    Task_10ms_Read_YawM_MainRxLongAccInfo_LongAccSenCirErrPre(&Task_10msBus.YawM_MainRxLongAccInfo.LongAccSenCirErrPre);
    Task_10ms_Read_YawM_MainRxLongAccInfo_LonACSenRanChkErrPre(&Task_10msBus.YawM_MainRxLongAccInfo.LonACSenRanChkErrPre);
    Task_10ms_Read_YawM_MainRxLongAccInfo_LongRollingCounter(&Task_10msBus.YawM_MainRxLongAccInfo.LongRollingCounter);
    Task_10ms_Read_YawM_MainRxLongAccInfo_IntTempSensorFault(&Task_10msBus.YawM_MainRxLongAccInfo.IntTempSensorFault);

    /* Decomposed structure interface */
    Task_10ms_Read_YawM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(&Task_10msBus.YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg);
    Task_10ms_Read_YawM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(&Task_10msBus.YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg);
    Task_10ms_Read_YawM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(&Task_10msBus.YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_AyM_MainCanRxEscInfo_Ay(&Task_10msBus.AyM_MainCanRxEscInfo.Ay);

    Task_10ms_Read_AyM_MainRxYawSerialInfo(&Task_10msBus.AyM_MainRxYawSerialInfo);
    /*==============================================================================
    * Members of structure AyM_MainRxYawSerialInfo 
     : AyM_MainRxYawSerialInfo.YawSerialNum_0;
     : AyM_MainRxYawSerialInfo.YawSerialNum_1;
     : AyM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_AyM_MainRxYawAccInfo_YawRateValidData(&Task_10msBus.AyM_MainRxYawAccInfo.YawRateValidData);
    Task_10ms_Read_AyM_MainRxYawAccInfo_YawRateSelfTestStatus(&Task_10msBus.AyM_MainRxYawAccInfo.YawRateSelfTestStatus);
    Task_10ms_Read_AyM_MainRxYawAccInfo_SensorOscFreqDev(&Task_10msBus.AyM_MainRxYawAccInfo.SensorOscFreqDev);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Gyro_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Gyro_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Raster_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Raster_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Eep_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Eep_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Batt_Range_Err(&Task_10msBus.AyM_MainRxYawAccInfo.Batt_Range_Err);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Asic_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Asic_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Accel_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Accel_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Ram_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Ram_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Rom_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Rom_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Ad_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Ad_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Osc_Fail(&Task_10msBus.AyM_MainRxYawAccInfo.Osc_Fail);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Watchdog_Rst(&Task_10msBus.AyM_MainRxYawAccInfo.Watchdog_Rst);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Plaus_Err_Pst(&Task_10msBus.AyM_MainRxYawAccInfo.Plaus_Err_Pst);
    Task_10ms_Read_AyM_MainRxYawAccInfo_RollingCounter(&Task_10msBus.AyM_MainRxYawAccInfo.RollingCounter);
    Task_10ms_Read_AyM_MainRxYawAccInfo_Can_Func_Err(&Task_10msBus.AyM_MainRxYawAccInfo.Can_Func_Err);
    Task_10ms_Read_AyM_MainRxYawAccInfo_LatAccValidData(&Task_10msBus.AyM_MainRxYawAccInfo.LatAccValidData);
    Task_10ms_Read_AyM_MainRxYawAccInfo_LatAccSelfTestStatus(&Task_10msBus.AyM_MainRxYawAccInfo.LatAccSelfTestStatus);

    /* Decomposed structure interface */
    Task_10ms_Read_AyM_MainRxLongAccInfo_IntSenFltSymtmActive(&Task_10msBus.AyM_MainRxLongAccInfo.IntSenFltSymtmActive);
    Task_10ms_Read_AyM_MainRxLongAccInfo_IntSenFaultPresent(&Task_10msBus.AyM_MainRxLongAccInfo.IntSenFaultPresent);
    Task_10ms_Read_AyM_MainRxLongAccInfo_LongAccSenCirErrPre(&Task_10msBus.AyM_MainRxLongAccInfo.LongAccSenCirErrPre);
    Task_10ms_Read_AyM_MainRxLongAccInfo_LonACSenRanChkErrPre(&Task_10msBus.AyM_MainRxLongAccInfo.LonACSenRanChkErrPre);
    Task_10ms_Read_AyM_MainRxLongAccInfo_LongRollingCounter(&Task_10msBus.AyM_MainRxLongAccInfo.LongRollingCounter);
    Task_10ms_Read_AyM_MainRxLongAccInfo_IntTempSensorFault(&Task_10msBus.AyM_MainRxLongAccInfo.IntTempSensorFault);

    /* Decomposed structure interface */
    Task_10ms_Read_AyM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(&Task_10msBus.AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg);
    Task_10ms_Read_AyM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(&Task_10msBus.AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg);
    Task_10ms_Read_AyM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(&Task_10msBus.AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_AxM_MainCanRxEscInfo_Ax(&Task_10msBus.AxM_MainCanRxEscInfo.Ax);

    Task_10ms_Read_AxM_MainRxYawSerialInfo(&Task_10msBus.AxM_MainRxYawSerialInfo);
    /*==============================================================================
    * Members of structure AxM_MainRxYawSerialInfo 
     : AxM_MainRxYawSerialInfo.YawSerialNum_0;
     : AxM_MainRxYawSerialInfo.YawSerialNum_1;
     : AxM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_AxM_MainRxYawAccInfo_YawRateValidData(&Task_10msBus.AxM_MainRxYawAccInfo.YawRateValidData);
    Task_10ms_Read_AxM_MainRxYawAccInfo_YawRateSelfTestStatus(&Task_10msBus.AxM_MainRxYawAccInfo.YawRateSelfTestStatus);
    Task_10ms_Read_AxM_MainRxYawAccInfo_SensorOscFreqDev(&Task_10msBus.AxM_MainRxYawAccInfo.SensorOscFreqDev);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Gyro_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Gyro_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Raster_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Raster_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Eep_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Eep_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Batt_Range_Err(&Task_10msBus.AxM_MainRxYawAccInfo.Batt_Range_Err);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Asic_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Asic_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Accel_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Accel_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Ram_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Ram_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Rom_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Rom_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Ad_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Ad_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Osc_Fail(&Task_10msBus.AxM_MainRxYawAccInfo.Osc_Fail);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Watchdog_Rst(&Task_10msBus.AxM_MainRxYawAccInfo.Watchdog_Rst);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Plaus_Err_Pst(&Task_10msBus.AxM_MainRxYawAccInfo.Plaus_Err_Pst);
    Task_10ms_Read_AxM_MainRxYawAccInfo_RollingCounter(&Task_10msBus.AxM_MainRxYawAccInfo.RollingCounter);
    Task_10ms_Read_AxM_MainRxYawAccInfo_Can_Func_Err(&Task_10msBus.AxM_MainRxYawAccInfo.Can_Func_Err);

    /* Decomposed structure interface */
    Task_10ms_Read_AxM_MainRxLongAccInfo_IntSenFltSymtmActive(&Task_10msBus.AxM_MainRxLongAccInfo.IntSenFltSymtmActive);
    Task_10ms_Read_AxM_MainRxLongAccInfo_IntSenFaultPresent(&Task_10msBus.AxM_MainRxLongAccInfo.IntSenFaultPresent);
    Task_10ms_Read_AxM_MainRxLongAccInfo_LongAccSenCirErrPre(&Task_10msBus.AxM_MainRxLongAccInfo.LongAccSenCirErrPre);
    Task_10ms_Read_AxM_MainRxLongAccInfo_LonACSenRanChkErrPre(&Task_10msBus.AxM_MainRxLongAccInfo.LonACSenRanChkErrPre);
    Task_10ms_Read_AxM_MainRxLongAccInfo_LongRollingCounter(&Task_10msBus.AxM_MainRxLongAccInfo.LongRollingCounter);
    Task_10ms_Read_AxM_MainRxLongAccInfo_IntTempSensorFault(&Task_10msBus.AxM_MainRxLongAccInfo.IntTempSensorFault);
    Task_10ms_Read_AxM_MainRxLongAccInfo_LongAccInvalidData(&Task_10msBus.AxM_MainRxLongAccInfo.LongAccInvalidData);
    Task_10ms_Read_AxM_MainRxLongAccInfo_LongAccSelfTstStatus(&Task_10msBus.AxM_MainRxLongAccInfo.LongAccSelfTstStatus);

    /* Decomposed structure interface */
    Task_10ms_Read_AxM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(&Task_10msBus.AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg);
    Task_10ms_Read_AxM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(&Task_10msBus.AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg);
    Task_10ms_Read_AxM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(&Task_10msBus.AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_SasM_MainCanRxEscInfo_SteeringAngle(&Task_10msBus.SasM_MainCanRxEscInfo.SteeringAngle);

    /* Decomposed structure interface */
    Task_10ms_Read_SasM_MainCanRxInfo_MainBusOffFlag(&Task_10msBus.SasM_MainCanRxInfo.MainBusOffFlag);

    /* Decomposed structure interface */
    Task_10ms_Read_SasM_MainRxSasInfo_Angle(&Task_10msBus.SasM_MainRxSasInfo.Angle);
    Task_10ms_Read_SasM_MainRxSasInfo_Speed(&Task_10msBus.SasM_MainRxSasInfo.Speed);
    Task_10ms_Read_SasM_MainRxSasInfo_Ok(&Task_10msBus.SasM_MainRxSasInfo.Ok);
    Task_10ms_Read_SasM_MainRxSasInfo_Trim(&Task_10msBus.SasM_MainRxSasInfo.Trim);
    Task_10ms_Read_SasM_MainRxSasInfo_CheckSum(&Task_10msBus.SasM_MainRxSasInfo.CheckSum);

    /* Decomposed structure interface */
    Task_10ms_Read_SasM_MainRxMsgOkFlgInfo_SasMsgOkFlg(&Task_10msBus.SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_SenPwr_12V(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_SenPwr_12V);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_Yaw(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_Yaw);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_Ay(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_Ay);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_Str(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_Str);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_YawP_MainCanRxEscInfo_YawRate(&Task_10msBus.YawP_MainCanRxEscInfo.YawRate);

    /* Decomposed structure interface */
    Task_10ms_Read_YawP_MainEscSwtStInfo_EscDisabledBySwt(&Task_10msBus.YawP_MainEscSwtStInfo.EscDisabledBySwt);

    Task_10ms_Read_YawP_MainWhlSpdInfo(&Task_10msBus.YawP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure YawP_MainWhlSpdInfo 
     : YawP_MainWhlSpdInfo.FlWhlSpd;
     : YawP_MainWhlSpdInfo.FrWhlSpd;
     : YawP_MainWhlSpdInfo.RlWhlSpd;
     : YawP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_YawP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(&Task_10msBus.YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFL(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssFL);
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFR(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssFR);
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRL(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssRL);
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRR(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssRR);
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_FrontWss(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_FrontWss);
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_RearWss(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_RearWss);
    Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(&Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V);

    /* Decomposed structure interface */
    Task_10ms_Read_YawP_MainEemEceData_Eem_Ece_Yaw(&Task_10msBus.YawP_MainEemEceData.Eem_Ece_Yaw);

    Task_10ms_Read_YawP_MainIMUCalcInfo(&Task_10msBus.YawP_MainIMUCalcInfo);
    /*==============================================================================
    * Members of structure YawP_MainIMUCalcInfo 
     : YawP_MainIMUCalcInfo.Reverse_Gear_flg;
     : YawP_MainIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/

    Task_10ms_Read_YawP_MainWssSpeedOut(&Task_10msBus.YawP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure YawP_MainWssSpeedOut 
     : YawP_MainWssSpeedOut.WssMax;
     : YawP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    Task_10ms_Read_YawP_MainWssCalcInfo(&Task_10msBus.YawP_MainWssCalcInfo);
    /*==============================================================================
    * Members of structure YawP_MainWssCalcInfo 
     : YawP_MainWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_SenPwr_12V(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_SenPwr_12V);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_Yaw(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_Yaw);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_Ay(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_Ay);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_Str(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_Str);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_AyP_MainCanRxEscInfo_Ay(&Task_10msBus.AyP_MainCanRxEscInfo.Ay);

    /* Decomposed structure interface */
    Task_10ms_Read_AyP_MainEscSwtStInfo_EscDisabledBySwt(&Task_10msBus.AyP_MainEscSwtStInfo.EscDisabledBySwt);

    Task_10ms_Read_AyP_MainWhlSpdInfo(&Task_10msBus.AyP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure AyP_MainWhlSpdInfo 
     : AyP_MainWhlSpdInfo.FlWhlSpd;
     : AyP_MainWhlSpdInfo.FrWhlSpd;
     : AyP_MainWhlSpdInfo.RlWhlSpd;
     : AyP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_AyP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(&Task_10msBus.AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFL(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssFL);
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFR(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssFR);
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRL(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssRL);
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRR(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssRR);
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_FrontWss(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_FrontWss);
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_RearWss(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_RearWss);
    Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(&Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V);

    /* Decomposed structure interface */
    Task_10ms_Read_AyP_MainEemEceData_Eem_Ece_Ay(&Task_10msBus.AyP_MainEemEceData.Eem_Ece_Ay);

    Task_10ms_Read_AyP_MainIMUCalcInfo(&Task_10msBus.AyP_MainIMUCalcInfo);
    /*==============================================================================
    * Members of structure AyP_MainIMUCalcInfo 
     : AyP_MainIMUCalcInfo.Reverse_Gear_flg;
     : AyP_MainIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/

    Task_10ms_Read_AyP_MainWssSpeedOut(&Task_10msBus.AyP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure AyP_MainWssSpeedOut 
     : AyP_MainWssSpeedOut.WssMax;
     : AyP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    Task_10ms_Read_AyP_MainWssCalcInfo(&Task_10msBus.AyP_MainWssCalcInfo);
    /*==============================================================================
    * Members of structure AyP_MainWssCalcInfo 
     : AyP_MainWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_12V(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_SenPwr_12V);
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_5V(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_SenPwr_5V);
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_Ax(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_Ax);
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainCanRxEscInfo_Ax(&Task_10msBus.AxP_MainCanRxEscInfo.Ax);

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainEscSwtStInfo_EscDisabledBySwt(&Task_10msBus.AxP_MainEscSwtStInfo.EscDisabledBySwt);

    Task_10ms_Read_AxP_MainWhlSpdInfo(&Task_10msBus.AxP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure AxP_MainWhlSpdInfo 
     : AxP_MainWhlSpdInfo.FlWhlSpd;
     : AxP_MainWhlSpdInfo.FrWhlSpd;
     : AxP_MainWhlSpdInfo.RlWhlSpd;
     : AxP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainAbsCtrlInfo_AbsActFlg(&Task_10msBus.AxP_MainAbsCtrlInfo.AbsActFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(&Task_10msBus.AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFL(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssFL);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFR(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssFR);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRL(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssRL);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRR(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssRR);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_FrontWss(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_FrontWss);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_RearWss(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_RearWss);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V);
    Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_5V(&Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V);

    /* Decomposed structure interface */
    Task_10ms_Read_AxP_MainEemEceData_Eem_Ece_Ax(&Task_10msBus.AxP_MainEemEceData.Eem_Ece_Ax);

    Task_10ms_Read_AxP_MainWssSpeedOut(&Task_10msBus.AxP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure AxP_MainWssSpeedOut 
     : AxP_MainWssSpeedOut.WssMax;
     : AxP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_SenPwr_12V(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_SenPwr_12V);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_Yaw(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_Yaw);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_Ay(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_Ay);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_Str(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_Str);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssFL(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssFL);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssFR(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssFR);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssRL(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssRL);
    Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssRR(&Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainCanRxEscInfo_SteeringAngle(&Task_10msBus.SasP_MainCanRxEscInfo.SteeringAngle);

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainEscSwtStInfo_EscDisabledBySwt(&Task_10msBus.SasP_MainEscSwtStInfo.EscDisabledBySwt);

    Task_10ms_Read_SasP_MainWhlSpdInfo(&Task_10msBus.SasP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure SasP_MainWhlSpdInfo 
     : SasP_MainWhlSpdInfo.FlWhlSpd;
     : SasP_MainWhlSpdInfo.FrWhlSpd;
     : SasP_MainWhlSpdInfo.RlWhlSpd;
     : SasP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainAbsCtrlInfo_AbsActFlg(&Task_10msBus.SasP_MainAbsCtrlInfo.AbsActFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainEscCtrlInfo_EscActFlg(&Task_10msBus.SasP_MainEscCtrlInfo.EscActFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainTcsCtrlInfo_TcsActFlg(&Task_10msBus.SasP_MainTcsCtrlInfo.TcsActFlg);

    /* Decomposed structure interface */
    Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFL(&Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssFL);
    Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFR(&Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssFR);
    Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRL(&Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssRL);
    Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRR(&Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssRR);
    Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_FrontWss(&Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_FrontWss);
    Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_RearWss(&Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_RearWss);

    Task_10ms_Read_SasP_MainWssSpeedOut(&Task_10msBus.SasP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure SasP_MainWssSpeedOut 
     : SasP_MainWssSpeedOut.WssMax;
     : SasP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_SwtM_MainSwtMonInfo_AvhSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.AvhSwtMon);
    Task_10ms_Read_SwtM_MainSwtMonInfo_BflSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.BflSwtMon);
    Task_10ms_Read_SwtM_MainSwtMonInfo_BlsSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.BlsSwtMon);
    Task_10ms_Read_SwtM_MainSwtMonInfo_BsSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.BsSwtMon);
    Task_10ms_Read_SwtM_MainSwtMonInfo_EscSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.EscSwtMon);
    Task_10ms_Read_SwtM_MainSwtMonInfo_HdcSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.HdcSwtMon);
    Task_10ms_Read_SwtM_MainSwtMonInfo_PbSwtMon(&Task_10msBus.SwtM_MainSwtMonInfo.PbSwtMon);

    /* Decomposed structure interface */
    Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_SimP(&Task_10msBus.SwtP_MainEemFailData.Eem_Fail_SimP);
    Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_ParkBrake(&Task_10msBus.SwtP_MainEemFailData.Eem_Fail_ParkBrake);
    Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDT(&Task_10msBus.SwtP_MainEemFailData.Eem_Fail_PedalPDT);
    Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDF(&Task_10msBus.SwtP_MainEemFailData.Eem_Fail_PedalPDF);

    /* Decomposed structure interface */
    //Task_10ms_Read_SwtP_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit(&Task_10msBus.SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit);

    /* Decomposed structure interface */
    Task_10ms_Read_SwtP_MainSwTrigPwrInfo_Vbatt01Mon(&Task_10msBus.SwtP_MainSwTrigPwrInfo.Vbatt01Mon);

    /* Decomposed structure interface */
    Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSig(&Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSig);
    Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(&Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset);
    Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSig(&Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSig);
    Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(&Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset);

    /* Decomposed structure interface */
    Task_10ms_Read_SwtP_MainSwtMonInfo_BlsSwtMon(&Task_10msBus.SwtP_MainSwtMonInfo.BlsSwtMon);
    Task_10ms_Read_SwtP_MainSwtMonInfo_BsSwtMon(&Task_10msBus.SwtP_MainSwtMonInfo.BsSwtMon);

    /* Decomposed structure interface */
    Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_BS(&Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_BS);
    Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_BLS(&Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_BLS);
    Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_ParkBrake(&Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake);
    Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDT(&Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT);
    Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDF(&Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF);

    Task_10ms_Read_RlyM_MainRlyDrvInfo(&Task_10msBus.RlyM_MainRlyDrvInfo);
    /*==============================================================================
    * Members of structure RlyM_MainRlyDrvInfo 
     : RlyM_MainRlyDrvInfo.RlyDbcDrv;
     : RlyM_MainRlyDrvInfo.RlyEssDrv;
     =============================================================================*/

    Task_10ms_Read_RlyM_MainRlyMonInfo(&Task_10msBus.RlyM_MainRlyMonInfo);
    /*==============================================================================
    * Members of structure RlyM_MainRlyMonInfo 
     : RlyM_MainRlyMonInfo.RlyDbcMon;
     : RlyM_MainRlyMonInfo.RlyEssMon;
     : RlyM_MainRlyMonInfo.RlyFault;
     =============================================================================*/

    /* Decomposed structure interface */
    //Task_10ms_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DegradeModeFail(&Task_10msBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail);
    //Task_10ms_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(&Task_10msBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail);

    Task_10ms_Read_Proxy_TxCanTxInfo(&Task_10msBus.Proxy_TxCanTxInfo);
    /*==============================================================================
    * Members of structure Proxy_TxCanTxInfo 
     : Proxy_TxCanTxInfo.TqIntvTCS;
     : Proxy_TxCanTxInfo.TqIntvMsr;
     : Proxy_TxCanTxInfo.TqIntvSlowTCS;
     : Proxy_TxCanTxInfo.MinGear;
     : Proxy_TxCanTxInfo.MaxGear;
     : Proxy_TxCanTxInfo.TcsReq;
     : Proxy_TxCanTxInfo.TcsCtrl;
     : Proxy_TxCanTxInfo.AbsAct;
     : Proxy_TxCanTxInfo.TcsGearShiftChr;
     : Proxy_TxCanTxInfo.EspCtrl;
     : Proxy_TxCanTxInfo.MsrReq;
     : Proxy_TxCanTxInfo.TcsProductInfo;
     =============================================================================*/

    Task_10ms_Read_Proxy_TxPdt5msRawInfo(&Task_10msBus.Proxy_TxPdt5msRawInfo);
    /*==============================================================================
    * Members of structure Proxy_TxPdt5msRawInfo 
     : Proxy_TxPdt5msRawInfo.PdtSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_Proxy_TxPdf5msRawInfo_PdfSig(&Task_10msBus.Proxy_TxPdf5msRawInfo.PdfSig);

    /* Decomposed structure interface */
    Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_TarRgnBrkTq(&Task_10msBus.Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq);
    Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_EstTotBrkForce(&Task_10msBus.Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce);
    Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_EstHydBrkForce(&Task_10msBus.Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce);
    Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_EhbStat(&Task_10msBus.Proxy_TxTarRgnBrkTqInfo.EhbStat);

    /* Decomposed structure interface */
    Task_10ms_Read_Proxy_TxEscSwtStInfo_EscDisabledBySwt(&Task_10msBus.Proxy_TxEscSwtStInfo.EscDisabledBySwt);
    Task_10ms_Read_Proxy_TxEscSwtStInfo_TcsDisabledBySwt(&Task_10msBus.Proxy_TxEscSwtStInfo.TcsDisabledBySwt);

    Task_10ms_Read_Proxy_TxWhlSpdInfo(&Task_10msBus.Proxy_TxWhlSpdInfo);
    /*==============================================================================
    * Members of structure Proxy_TxWhlSpdInfo 
     : Proxy_TxWhlSpdInfo.FlWhlSpd;
     : Proxy_TxWhlSpdInfo.FrWhlSpd;
     : Proxy_TxWhlSpdInfo.RlWhlSpd;
     : Proxy_TxWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Task_10ms_Read_Proxy_TxWhlEdgeCntInfo(&Task_10msBus.Proxy_TxWhlEdgeCntInfo);
    /*==============================================================================
    * Members of structure Proxy_TxWhlEdgeCntInfo 
     : Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt;
     =============================================================================*/

    Task_10ms_Read_Proxy_TxPistP5msRawInfo(&Task_10msBus.Proxy_TxPistP5msRawInfo);
    /*==============================================================================
    * Members of structure Proxy_TxPistP5msRawInfo 
     : Proxy_TxPistP5msRawInfo.PistPSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_10ms_Read_Proxy_TxLogicEepDataInfo_KPdtOffsEolReadVal(&Task_10msBus.Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal);
    Task_10ms_Read_Proxy_TxLogicEepDataInfo_ReadInvld(&Task_10msBus.Proxy_TxLogicEepDataInfo.ReadInvld);

    Task_10ms_Read_Proxy_TxEemLampData(&Task_10msBus.Proxy_TxEemLampData);
    /*==============================================================================
    * Members of structure Proxy_TxEemLampData 
     : Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest;
     : Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ACCEnable;
     : Proxy_TxEemLampData.Eem_Lamp_CDMEnable;
     : Proxy_TxEemLampData.Eem_Buzzor_On;
     =============================================================================*/

    /* Single interface */
    //Task_10ms_Read_Diag_HndlrEcuModeSts(&Task_10msBus.Diag_HndlrEcuModeSts);
    Task_10ms_Read_Diag_HndlrIgnOnOffSts(&Task_10msBus.Diag_HndlrIgnOnOffSts);
    Task_10ms_Read_Diag_HndlrVBatt1Mon(&Task_10msBus.Diag_HndlrVBatt1Mon);
    Task_10ms_Read_Diag_HndlrVBatt2Mon(&Task_10msBus.Diag_HndlrVBatt2Mon);
    Task_10ms_Read_Diag_HndlrFspCbsMon(&Task_10msBus.Diag_HndlrFspCbsMon);
    Task_10ms_Read_Diag_HndlrCEMon(&Task_10msBus.Diag_HndlrCEMon);
    Task_10ms_Read_Diag_HndlrVddMon(&Task_10msBus.Diag_HndlrVddMon);
    Task_10ms_Read_Diag_HndlrCspMon(&Task_10msBus.Diag_HndlrCspMon);
    //Task_10ms_Read_Diag_HndlrFuncInhibitDiagSts(&Task_10msBus.Diag_HndlrFuncInhibitDiagSts);
    Task_10ms_Read_Diag_HndlrMtrDiagState(&Task_10msBus.Diag_HndlrMtrDiagState);
    //Task_10ms_Read_CanM_MainEcuModeSts(&Task_10msBus.CanM_MainEcuModeSts);
    Task_10ms_Read_CanM_MainIgnOnOffSts(&Task_10msBus.CanM_MainIgnOnOffSts);
    Task_10ms_Read_CanM_MainIgnEdgeSts(&Task_10msBus.CanM_MainIgnEdgeSts);
    Task_10ms_Read_CanM_MainVBatt1Mon(&Task_10msBus.CanM_MainVBatt1Mon);
    Task_10ms_Read_CanM_MainArbVlvDriveState(&Task_10msBus.CanM_MainArbVlvDriveState);
    Task_10ms_Read_CanM_MainCEMon(&Task_10msBus.CanM_MainCEMon);
    Task_10ms_Read_CanM_MainCanRxGearSelDispErrInfo(&Task_10msBus.CanM_MainCanRxGearSelDispErrInfo);
    Task_10ms_Read_CanM_MainMtrArbDriveState(&Task_10msBus.CanM_MainMtrArbDriveState);
    //Task_10ms_Read_SenPwrM_MainEcuModeSts(&Task_10msBus.SenPwrM_MainEcuModeSts);
    Task_10ms_Read_SenPwrM_MainIgnOnOffSts(&Task_10msBus.SenPwrM_MainIgnOnOffSts);
    Task_10ms_Read_SenPwrM_MainIgnEdgeSts(&Task_10msBus.SenPwrM_MainIgnEdgeSts);
    Task_10ms_Read_SenPwrM_MainVBatt1Mon(&Task_10msBus.SenPwrM_MainVBatt1Mon);
    Task_10ms_Read_SenPwrM_MainArbVlvDriveState(&Task_10msBus.SenPwrM_MainArbVlvDriveState);
    Task_10ms_Read_SenPwrM_MainExt5vMon(&Task_10msBus.SenPwrM_MainExt5vMon);
    Task_10ms_Read_SenPwrM_MainCspMon(&Task_10msBus.SenPwrM_MainCspMon);
    Task_10ms_Read_SenPwrM_MainMtrArbDriveState(&Task_10msBus.SenPwrM_MainMtrArbDriveState);
    //Task_10ms_Read_YawM_MainEcuModeSts(&Task_10msBus.YawM_MainEcuModeSts);
    Task_10ms_Read_YawM_MainIgnOnOffSts(&Task_10msBus.YawM_MainIgnOnOffSts);
    Task_10ms_Read_YawM_MainIgnEdgeSts(&Task_10msBus.YawM_MainIgnEdgeSts);
    Task_10ms_Read_YawM_MainVBatt1Mon(&Task_10msBus.YawM_MainVBatt1Mon);
    //Task_10ms_Read_AyM_MainEcuModeSts(&Task_10msBus.AyM_MainEcuModeSts);
    Task_10ms_Read_AyM_MainIgnOnOffSts(&Task_10msBus.AyM_MainIgnOnOffSts);
    Task_10ms_Read_AyM_MainIgnEdgeSts(&Task_10msBus.AyM_MainIgnEdgeSts);
    Task_10ms_Read_AyM_MainVBatt1Mon(&Task_10msBus.AyM_MainVBatt1Mon);
    //Task_10ms_Read_AxM_MainEcuModeSts(&Task_10msBus.AxM_MainEcuModeSts);
    Task_10ms_Read_AxM_MainIgnOnOffSts(&Task_10msBus.AxM_MainIgnOnOffSts);
    Task_10ms_Read_AxM_MainIgnEdgeSts(&Task_10msBus.AxM_MainIgnEdgeSts);
    Task_10ms_Read_AxM_MainVBatt1Mon(&Task_10msBus.AxM_MainVBatt1Mon);
    //Task_10ms_Read_SasM_MainEcuModeSts(&Task_10msBus.SasM_MainEcuModeSts);
    Task_10ms_Read_SasM_MainIgnOnOffSts(&Task_10msBus.SasM_MainIgnOnOffSts);
    Task_10ms_Read_SasM_MainIgnEdgeSts(&Task_10msBus.SasM_MainIgnEdgeSts);
    Task_10ms_Read_SasM_MainVBatt1Mon(&Task_10msBus.SasM_MainVBatt1Mon);
    //Task_10ms_Read_YawP_MainEcuModeSts(&Task_10msBus.YawP_MainEcuModeSts);
    Task_10ms_Read_YawP_MainIgnOnOffSts(&Task_10msBus.YawP_MainIgnOnOffSts);
    Task_10ms_Read_YawP_MainIgnEdgeSts(&Task_10msBus.YawP_MainIgnEdgeSts);
    Task_10ms_Read_YawP_MainVBatt1Mon(&Task_10msBus.YawP_MainVBatt1Mon);
    Task_10ms_Read_YawP_MainVehSpd(&Task_10msBus.YawP_MainVehSpd);
    //Task_10ms_Read_AyP_MainEcuModeSts(&Task_10msBus.AyP_MainEcuModeSts);
    Task_10ms_Read_AyP_MainIgnOnOffSts(&Task_10msBus.AyP_MainIgnOnOffSts);
    Task_10ms_Read_AyP_MainIgnEdgeSts(&Task_10msBus.AyP_MainIgnEdgeSts);
    Task_10ms_Read_AyP_MainVBatt1Mon(&Task_10msBus.AyP_MainVBatt1Mon);
    Task_10ms_Read_AyP_MainVehSpd(&Task_10msBus.AyP_MainVehSpd);
    //Task_10ms_Read_AxP_MainEcuModeSts(&Task_10msBus.AxP_MainEcuModeSts);
    Task_10ms_Read_AxP_MainIgnOnOffSts(&Task_10msBus.AxP_MainIgnOnOffSts);
    Task_10ms_Read_AxP_MainIgnEdgeSts(&Task_10msBus.AxP_MainIgnEdgeSts);
    Task_10ms_Read_AxP_MainVBatt1Mon(&Task_10msBus.AxP_MainVBatt1Mon);
    Task_10ms_Read_AxP_MainVehSpd(&Task_10msBus.AxP_MainVehSpd);
    //Task_10ms_Read_SasP_MainEcuModeSts(&Task_10msBus.SasP_MainEcuModeSts);
    Task_10ms_Read_SasP_MainIgnOnOffSts(&Task_10msBus.SasP_MainIgnOnOffSts);
    Task_10ms_Read_SasP_MainIgnEdgeSts(&Task_10msBus.SasP_MainIgnEdgeSts);
    Task_10ms_Read_SasP_MainVBatt1Mon(&Task_10msBus.SasP_MainVBatt1Mon);
    Task_10ms_Read_SasP_MainVehSpd(&Task_10msBus.SasP_MainVehSpd);
    //Task_10ms_Read_SwtM_MainEcuModeSts(&Task_10msBus.SwtM_MainEcuModeSts);
    Task_10ms_Read_SwtM_MainIgnOnOffSts(&Task_10msBus.SwtM_MainIgnOnOffSts);
    Task_10ms_Read_SwtM_MainIgnEdgeSts(&Task_10msBus.SwtM_MainIgnEdgeSts);
    Task_10ms_Read_SwtM_MainVBatt1Mon(&Task_10msBus.SwtM_MainVBatt1Mon);
    //Task_10ms_Read_SwtP_MainEcuModeSts(&Task_10msBus.SwtP_MainEcuModeSts);
    Task_10ms_Read_SwtP_MainIgnOnOffSts(&Task_10msBus.SwtP_MainIgnOnOffSts);
    Task_10ms_Read_SwtP_MainIgnEdgeSts(&Task_10msBus.SwtP_MainIgnEdgeSts);
    Task_10ms_Read_SwtP_MainVBatt1Mon(&Task_10msBus.SwtP_MainVBatt1Mon);
    Task_10ms_Read_SwtP_MainVehSpdFild(&Task_10msBus.SwtP_MainVehSpdFild);
    //Task_10ms_Read_RlyM_MainEcuModeSts(&Task_10msBus.RlyM_MainEcuModeSts);
    Task_10ms_Read_RlyM_MainIgnOnOffSts(&Task_10msBus.RlyM_MainIgnOnOffSts);
    Task_10ms_Read_RlyM_MainIgnEdgeSts(&Task_10msBus.RlyM_MainIgnEdgeSts);
    Task_10ms_Read_RlyM_MainVBatt1Mon(&Task_10msBus.RlyM_MainVBatt1Mon);
    //Task_10ms_Read_Proxy_TxEcuModeSts(&Task_10msBus.Proxy_TxEcuModeSts);
    Task_10ms_Read_Proxy_TxIgnOnOffSts(&Task_10msBus.Proxy_TxIgnOnOffSts);
    //Task_10ms_Read_Proxy_TxFuncInhibitProxySts(&Task_10msBus.Proxy_TxFuncInhibitProxySts);

    /* Process */
    /**********************************************************************************************
     Diag_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    Diag_HndlrWhlSpdInfo = Task_10msBus.Diag_HndlrWhlSpdInfo;
    /*==============================================================================
    * Members of structure Diag_HndlrWhlSpdInfo 
     : Diag_HndlrWhlSpdInfo.FlWhlSpd;
     : Diag_HndlrWhlSpdInfo.FrWhlSpd;
     : Diag_HndlrWhlSpdInfo.RlWhlSpd;
     : Diag_HndlrWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Diag_HndlrIdbSnsrEolOfsCalcInfo = Task_10msBus.Diag_HndlrIdbSnsrEolOfsCalcInfo;
    /*==============================================================================
    * Members of structure Diag_HndlrIdbSnsrEolOfsCalcInfo 
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
     : Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
     =============================================================================*/

    /* Single interface */
    //Diag_HndlrEcuModeSts = Task_10msBus.Diag_HndlrEcuModeSts;
    Diag_HndlrIgnOnOffSts = Task_10msBus.Diag_HndlrIgnOnOffSts;
    Diag_HndlrVBatt1Mon = Task_10msBus.Diag_HndlrVBatt1Mon;
    Diag_HndlrVBatt2Mon = Task_10msBus.Diag_HndlrVBatt2Mon;
    Diag_HndlrFspCbsMon = Task_10msBus.Diag_HndlrFspCbsMon;
    Diag_HndlrCEMon = Task_10msBus.Diag_HndlrCEMon;
    Diag_HndlrVddMon = Task_10msBus.Diag_HndlrVddMon;
    Diag_HndlrCspMon = Task_10msBus.Diag_HndlrCspMon;
    //Diag_HndlrFuncInhibitDiagSts = Task_10msBus.Diag_HndlrFuncInhibitDiagSts;
    Diag_HndlrMtrDiagState = Task_10msBus.Diag_HndlrMtrDiagState;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */

    if (wmu1LoopTime10ms_0 == 1) Diag_Hndlr();

    /* Structure interface */
    Task_10msBus.Diag_HndlrDiagVlvActrInfo = Diag_HndlrDiagVlvActrInfo;
    /*==============================================================================
    * Members of structure Diag_HndlrDiagVlvActrInfo 
     : Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty;
     : Diag_HndlrDiagVlvActrInfo.FlOvReqData;
     : Diag_HndlrDiagVlvActrInfo.FlIvReqData;
     : Diag_HndlrDiagVlvActrInfo.FrOvReqData;
     : Diag_HndlrDiagVlvActrInfo.FrIvReqData;
     : Diag_HndlrDiagVlvActrInfo.RlOvReqData;
     : Diag_HndlrDiagVlvActrInfo.RlIvReqData;
     : Diag_HndlrDiagVlvActrInfo.RrOvReqData;
     : Diag_HndlrDiagVlvActrInfo.RrIvReqData;
     : Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.SimVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.ResPVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.BalVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.CircVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.PressDumpReqData;
     : Diag_HndlrDiagVlvActrInfo.RelsVlvReqData;
     =============================================================================*/

    Task_10msBus.Diag_HndlrMotReqDataDiagInfo = Diag_HndlrMotReqDataDiagInfo;
    /*==============================================================================
    * Members of structure Diag_HndlrMotReqDataDiagInfo 
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData;
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData;
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData;
     : Diag_HndlrMotReqDataDiagInfo.MotReq;
     =============================================================================*/

    Task_10msBus.Diag_HndlrSasCalInfo = Diag_HndlrSasCalInfo;
    /*==============================================================================
    * Members of structure Diag_HndlrSasCalInfo 
     : Diag_HndlrSasCalInfo.DiagSasCaltoAppl;
     =============================================================================*/

    Task_10msBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
    /*==============================================================================
    * Members of structure Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo 
     : Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
     : Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
     =============================================================================*/

    Task_10msBus.Diag_HndlrDiagHndlRequest = Diag_HndlrDiagHndlRequest;
    /*==============================================================================
    * Members of structure Diag_HndlrDiagHndlRequest 
     : Diag_HndlrDiagHndlRequest.DiagRequest_Order;
     : Diag_HndlrDiagHndlRequest.DiagRequest_Iq;
     : Diag_HndlrDiagHndlRequest.DiagRequest_Id;
     : Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM;
     : Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM;
     : Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM;
     =============================================================================*/

    /* Single interface */
    Task_10msBus.Diag_HndlrDiagClrSrs = Diag_HndlrDiagClrSrs;
    Task_10msBus.Diag_HndlrDiagSci = Diag_HndlrDiagSci;
    Task_10msBus.Diag_HndlrDiagAhbSci = Diag_HndlrDiagAhbSci;

    /**********************************************************************************************
     Diag_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     CanM_Main start
     **********************************************************************************************/

     /* Structure interface */
    CanM_MainCanRxRegenInfo = Task_10msBus.CanM_MainCanRxRegenInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxRegenInfo 
     : CanM_MainCanRxRegenInfo.HcuRegenEna;
     : CanM_MainCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/

    CanM_MainCanRxAccelPedlInfo = Task_10msBus.CanM_MainCanRxAccelPedlInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxAccelPedlInfo 
     : CanM_MainCanRxAccelPedlInfo.AccelPedlVal;
     : CanM_MainCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/

    CanM_MainCanRxIdbInfo = Task_10msBus.CanM_MainCanRxIdbInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxIdbInfo 
     : CanM_MainCanRxIdbInfo.EngMsgFault;
     : CanM_MainCanRxIdbInfo.TarGearPosi;
     : CanM_MainCanRxIdbInfo.GearSelDisp;
     : CanM_MainCanRxIdbInfo.TcuSwiGs;
     : CanM_MainCanRxIdbInfo.HcuServiceMod;
     : CanM_MainCanRxIdbInfo.SubCanBusSigFault;
     : CanM_MainCanRxIdbInfo.CoolantTemp;
     : CanM_MainCanRxIdbInfo.CoolantTempErr;
     =============================================================================*/

    CanM_MainCanRxEngTempInfo = Task_10msBus.CanM_MainCanRxEngTempInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxEngTempInfo 
     : CanM_MainCanRxEngTempInfo.EngTemp;
     : CanM_MainCanRxEngTempInfo.EngTempErr;
     =============================================================================*/

    CanM_MainCanRxEscInfo = Task_10msBus.CanM_MainCanRxEscInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxEscInfo 
     : CanM_MainCanRxEscInfo.Ax;
     : CanM_MainCanRxEscInfo.YawRate;
     : CanM_MainCanRxEscInfo.SteeringAngle;
     : CanM_MainCanRxEscInfo.Ay;
     : CanM_MainCanRxEscInfo.PbSwt;
     : CanM_MainCanRxEscInfo.ClutchSwt;
     : CanM_MainCanRxEscInfo.GearRSwt;
     : CanM_MainCanRxEscInfo.EngActIndTq;
     : CanM_MainCanRxEscInfo.EngRpm;
     : CanM_MainCanRxEscInfo.EngIndTq;
     : CanM_MainCanRxEscInfo.EngFrictionLossTq;
     : CanM_MainCanRxEscInfo.EngStdTq;
     : CanM_MainCanRxEscInfo.TurbineRpm;
     : CanM_MainCanRxEscInfo.ThrottleAngle;
     : CanM_MainCanRxEscInfo.TpsResol1000;
     : CanM_MainCanRxEscInfo.PvAvCanResol1000;
     : CanM_MainCanRxEscInfo.EngChr;
     : CanM_MainCanRxEscInfo.EngVol;
     : CanM_MainCanRxEscInfo.GearType;
     : CanM_MainCanRxEscInfo.EngClutchState;
     : CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv;
     : CanM_MainCanRxEscInfo.MotEstTq;
     : CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv;
     : CanM_MainCanRxEscInfo.TqIntvTCU;
     : CanM_MainCanRxEscInfo.TqIntvSlowTCU;
     : CanM_MainCanRxEscInfo.TqIncReq;
     : CanM_MainCanRxEscInfo.DecelReq;
     : CanM_MainCanRxEscInfo.RainSnsStat;
     : CanM_MainCanRxEscInfo.EngSpdErr;
     : CanM_MainCanRxEscInfo.AtType;
     : CanM_MainCanRxEscInfo.MtType;
     : CanM_MainCanRxEscInfo.CvtType;
     : CanM_MainCanRxEscInfo.DctType;
     : CanM_MainCanRxEscInfo.HevAtTcu;
     : CanM_MainCanRxEscInfo.TurbineRpmErr;
     : CanM_MainCanRxEscInfo.ThrottleAngleErr;
     : CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct;
     : CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV;
     : CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct;
     : CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV;
     : CanM_MainCanRxEscInfo.WiperIntSW;
     : CanM_MainCanRxEscInfo.WiperLow;
     : CanM_MainCanRxEscInfo.WiperHigh;
     : CanM_MainCanRxEscInfo.WiperValid;
     : CanM_MainCanRxEscInfo.WiperAuto;
     : CanM_MainCanRxEscInfo.TcuFaultSts;
     =============================================================================*/

    CanM_MainWhlSpdInfo = Task_10msBus.CanM_MainWhlSpdInfo;
    /*==============================================================================
    * Members of structure CanM_MainWhlSpdInfo 
     : CanM_MainWhlSpdInfo.FlWhlSpd;
     : CanM_MainWhlSpdInfo.FrWhlSpd;
     : CanM_MainWhlSpdInfo.RlWhlSpd;
     : CanM_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    CanM_MainCanRxEemInfo = Task_10msBus.CanM_MainCanRxEemInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxEemInfo 
     : CanM_MainCanRxEemInfo.YawRateInvld;
     : CanM_MainCanRxEemInfo.AyInvld;
     : CanM_MainCanRxEemInfo.SteeringAngleRxOk;
     : CanM_MainCanRxEemInfo.AxInvldData;
     : CanM_MainCanRxEemInfo.Ems1RxErr;
     : CanM_MainCanRxEemInfo.Ems2RxErr;
     : CanM_MainCanRxEemInfo.Tcu1RxErr;
     : CanM_MainCanRxEemInfo.Tcu5RxErr;
     : CanM_MainCanRxEemInfo.Hcu1RxErr;
     : CanM_MainCanRxEemInfo.Hcu2RxErr;
     : CanM_MainCanRxEemInfo.Hcu3RxErr;
     =============================================================================*/

    CanM_MainCanTimeOutStInfo = Task_10msBus.CanM_MainCanTimeOutStInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanTimeOutStInfo 
     : CanM_MainCanTimeOutStInfo.MaiCanSusDet;
     : CanM_MainCanTimeOutStInfo.EmsTiOutSusDet;
     : CanM_MainCanTimeOutStInfo.TcuTiOutSusDet;
     =============================================================================*/

    CanM_MainCanRxInfo = Task_10msBus.CanM_MainCanRxInfo;
    /*==============================================================================
    * Members of structure CanM_MainCanRxInfo 
     : CanM_MainCanRxInfo.MainBusOffFlag;
     : CanM_MainCanRxInfo.SenBusOffFlag;
     =============================================================================*/

    CanM_MainRxMsgOkFlgInfo = Task_10msBus.CanM_MainRxMsgOkFlgInfo;
    /*==============================================================================
    * Members of structure CanM_MainRxMsgOkFlgInfo 
     : CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/

    /* Single interface */
    //CanM_MainEcuModeSts = Task_10msBus.CanM_MainEcuModeSts;
    CanM_MainIgnOnOffSts = Task_10msBus.CanM_MainIgnOnOffSts;
    CanM_MainIgnEdgeSts = Task_10msBus.CanM_MainIgnEdgeSts;
    CanM_MainVBatt1Mon = Task_10msBus.CanM_MainVBatt1Mon;
    CanM_MainArbVlvDriveState = Task_10msBus.CanM_MainArbVlvDriveState;
    CanM_MainCEMon = Task_10msBus.CanM_MainCEMon;
    CanM_MainCanRxGearSelDispErrInfo = Task_10msBus.CanM_MainCanRxGearSelDispErrInfo;
    CanM_MainMtrArbDriveState = Task_10msBus.CanM_MainMtrArbDriveState;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    CanM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    CanM_Main();

    /* Structure interface */
    Task_10msBus.CanM_MainCanMonData = CanM_MainCanMonData;
    /*==============================================================================
    * Members of structure CanM_MainCanMonData 
     : CanM_MainCanMonData.CanM_MainBusOff_Err;
     : CanM_MainCanMonData.CanM_SubBusOff_Err;
     : CanM_MainCanMonData.CanM_MainOverRun_Err;
     : CanM_MainCanMonData.CanM_SubOverRun_Err;
     : CanM_MainCanMonData.CanM_EMS1_Tout_Err;
     : CanM_MainCanMonData.CanM_EMS2_Tout_Err;
     : CanM_MainCanMonData.CanM_TCU1_Tout_Err;
     : CanM_MainCanMonData.CanM_TCU5_Tout_Err;
     : CanM_MainCanMonData.CanM_FWD1_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU1_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU2_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU3_Tout_Err;
     : CanM_MainCanMonData.CanM_VSM2_Tout_Err;
     : CanM_MainCanMonData.CanM_EMS_Invalid_Err;
     : CanM_MainCanMonData.CanM_TCU_Invalid_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     CanM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     SenPwrM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V = Task_10msBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V;
    SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V = Task_10msBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V;

    /* Single interface */
    //SenPwrM_MainEcuModeSts = Task_10msBus.SenPwrM_MainEcuModeSts;
    SenPwrM_MainIgnOnOffSts = Task_10msBus.SenPwrM_MainIgnOnOffSts;
    SenPwrM_MainIgnEdgeSts = Task_10msBus.SenPwrM_MainIgnEdgeSts;
    SenPwrM_MainVBatt1Mon = Task_10msBus.SenPwrM_MainVBatt1Mon;
    SenPwrM_MainArbVlvDriveState = Task_10msBus.SenPwrM_MainArbVlvDriveState;
    SenPwrM_MainExt5vMon = Task_10msBus.SenPwrM_MainExt5vMon;
    SenPwrM_MainCspMon = Task_10msBus.SenPwrM_MainCspMon;
    SenPwrM_MainMtrArbDriveState = Task_10msBus.SenPwrM_MainMtrArbDriveState;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    SenPwrM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    SenPwrM_Main();

    /* Structure interface */
    Task_10msBus.SenPwrM_MainSenPwrMonitorData = SenPwrM_MainSenPwrMonitorData;
    /*==============================================================================
    * Members of structure SenPwrM_MainSenPwrMonitorData 
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     SenPwrM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     YawM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    YawM_MainCanRxEscInfo.YawRate = Task_10msBus.YawM_MainCanRxEscInfo.YawRate;

    YawM_MainRxYawSerialInfo = Task_10msBus.YawM_MainRxYawSerialInfo;
    /*==============================================================================
    * Members of structure YawM_MainRxYawSerialInfo 
     : YawM_MainRxYawSerialInfo.YawSerialNum_0;
     : YawM_MainRxYawSerialInfo.YawSerialNum_1;
     : YawM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    /* Decomposed structure interface */
    YawM_MainRxYawAccInfo.YawRateValidData = Task_10msBus.YawM_MainRxYawAccInfo.YawRateValidData;
    YawM_MainRxYawAccInfo.YawRateSelfTestStatus = Task_10msBus.YawM_MainRxYawAccInfo.YawRateSelfTestStatus;
    YawM_MainRxYawAccInfo.SensorOscFreqDev = Task_10msBus.YawM_MainRxYawAccInfo.SensorOscFreqDev;
    YawM_MainRxYawAccInfo.Gyro_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Gyro_Fail;
    YawM_MainRxYawAccInfo.Raster_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Raster_Fail;
    YawM_MainRxYawAccInfo.Eep_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Eep_Fail;
    YawM_MainRxYawAccInfo.Batt_Range_Err = Task_10msBus.YawM_MainRxYawAccInfo.Batt_Range_Err;
    YawM_MainRxYawAccInfo.Asic_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Asic_Fail;
    YawM_MainRxYawAccInfo.Accel_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Accel_Fail;
    YawM_MainRxYawAccInfo.Ram_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Ram_Fail;
    YawM_MainRxYawAccInfo.Rom_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Rom_Fail;
    YawM_MainRxYawAccInfo.Ad_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Ad_Fail;
    YawM_MainRxYawAccInfo.Osc_Fail = Task_10msBus.YawM_MainRxYawAccInfo.Osc_Fail;
    YawM_MainRxYawAccInfo.Watchdog_Rst = Task_10msBus.YawM_MainRxYawAccInfo.Watchdog_Rst;
    YawM_MainRxYawAccInfo.Plaus_Err_Pst = Task_10msBus.YawM_MainRxYawAccInfo.Plaus_Err_Pst;
    YawM_MainRxYawAccInfo.RollingCounter = Task_10msBus.YawM_MainRxYawAccInfo.RollingCounter;
    YawM_MainRxYawAccInfo.Can_Func_Err = Task_10msBus.YawM_MainRxYawAccInfo.Can_Func_Err;

    /* Decomposed structure interface */
    YawM_MainRxLongAccInfo.IntSenFltSymtmActive = Task_10msBus.YawM_MainRxLongAccInfo.IntSenFltSymtmActive;
    YawM_MainRxLongAccInfo.IntSenFaultPresent = Task_10msBus.YawM_MainRxLongAccInfo.IntSenFaultPresent;
    YawM_MainRxLongAccInfo.LongAccSenCirErrPre = Task_10msBus.YawM_MainRxLongAccInfo.LongAccSenCirErrPre;
    YawM_MainRxLongAccInfo.LonACSenRanChkErrPre = Task_10msBus.YawM_MainRxLongAccInfo.LonACSenRanChkErrPre;
    YawM_MainRxLongAccInfo.LongRollingCounter = Task_10msBus.YawM_MainRxLongAccInfo.LongRollingCounter;
    YawM_MainRxLongAccInfo.IntTempSensorFault = Task_10msBus.YawM_MainRxLongAccInfo.IntTempSensorFault;

    /* Decomposed structure interface */
    YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = Task_10msBus.YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg;
    YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = Task_10msBus.YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg;
    YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = Task_10msBus.YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg;

    /* Decomposed structure interface */
    YawM_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssFL;
    YawM_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssFR;
    YawM_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssRL;
    YawM_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.YawM_MainEemFailData.Eem_Fail_WssRR;

    /* Single interface */
    //YawM_MainEcuModeSts = Task_10msBus.YawM_MainEcuModeSts;
    YawM_MainIgnOnOffSts = Task_10msBus.YawM_MainIgnOnOffSts;
    YawM_MainIgnEdgeSts = Task_10msBus.YawM_MainIgnEdgeSts;
    YawM_MainVBatt1Mon = Task_10msBus.YawM_MainVBatt1Mon;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    YawM_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    YawM_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Inter-Runnable single interface */
    YawM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    YawM_Main();

    /* Structure interface */
    Task_10msBus.YawM_MainYAWMSerialInfo = YawM_MainYAWMSerialInfo;
    /*==============================================================================
    * Members of structure YawM_MainYAWMSerialInfo 
     : YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg;
     : YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
     =============================================================================*/

    Task_10msBus.YawM_MainYAWMOutInfo = YawM_MainYAWMOutInfo;
    /*==============================================================================
    * Members of structure YawM_MainYAWMOutInfo 
     : YawM_MainYAWMOutInfo.YAWM_Timeout_Err;
     : YawM_MainYAWMOutInfo.YAWM_Invalid_Err;
     : YawM_MainYAWMOutInfo.YAWM_CRC_Err;
     : YawM_MainYAWMOutInfo.YAWM_Rolling_Err;
     : YawM_MainYAWMOutInfo.YAWM_Temperature_Err;
     : YawM_MainYAWMOutInfo.YAWM_Range_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     YawM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     AyM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    AyM_MainCanRxEscInfo.Ay = Task_10msBus.AyM_MainCanRxEscInfo.Ay;

    AyM_MainRxYawSerialInfo = Task_10msBus.AyM_MainRxYawSerialInfo;
    /*==============================================================================
    * Members of structure AyM_MainRxYawSerialInfo 
     : AyM_MainRxYawSerialInfo.YawSerialNum_0;
     : AyM_MainRxYawSerialInfo.YawSerialNum_1;
     : AyM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    /* Decomposed structure interface */
    AyM_MainRxYawAccInfo.YawRateValidData = Task_10msBus.AyM_MainRxYawAccInfo.YawRateValidData;
    AyM_MainRxYawAccInfo.YawRateSelfTestStatus = Task_10msBus.AyM_MainRxYawAccInfo.YawRateSelfTestStatus;
    AyM_MainRxYawAccInfo.SensorOscFreqDev = Task_10msBus.AyM_MainRxYawAccInfo.SensorOscFreqDev;
    AyM_MainRxYawAccInfo.Gyro_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Gyro_Fail;
    AyM_MainRxYawAccInfo.Raster_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Raster_Fail;
    AyM_MainRxYawAccInfo.Eep_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Eep_Fail;
    AyM_MainRxYawAccInfo.Batt_Range_Err = Task_10msBus.AyM_MainRxYawAccInfo.Batt_Range_Err;
    AyM_MainRxYawAccInfo.Asic_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Asic_Fail;
    AyM_MainRxYawAccInfo.Accel_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Accel_Fail;
    AyM_MainRxYawAccInfo.Ram_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Ram_Fail;
    AyM_MainRxYawAccInfo.Rom_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Rom_Fail;
    AyM_MainRxYawAccInfo.Ad_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Ad_Fail;
    AyM_MainRxYawAccInfo.Osc_Fail = Task_10msBus.AyM_MainRxYawAccInfo.Osc_Fail;
    AyM_MainRxYawAccInfo.Watchdog_Rst = Task_10msBus.AyM_MainRxYawAccInfo.Watchdog_Rst;
    AyM_MainRxYawAccInfo.Plaus_Err_Pst = Task_10msBus.AyM_MainRxYawAccInfo.Plaus_Err_Pst;
    AyM_MainRxYawAccInfo.RollingCounter = Task_10msBus.AyM_MainRxYawAccInfo.RollingCounter;
    AyM_MainRxYawAccInfo.Can_Func_Err = Task_10msBus.AyM_MainRxYawAccInfo.Can_Func_Err;
    AyM_MainRxYawAccInfo.LatAccValidData = Task_10msBus.AyM_MainRxYawAccInfo.LatAccValidData;
    AyM_MainRxYawAccInfo.LatAccSelfTestStatus = Task_10msBus.AyM_MainRxYawAccInfo.LatAccSelfTestStatus;

    /* Decomposed structure interface */
    AyM_MainRxLongAccInfo.IntSenFltSymtmActive = Task_10msBus.AyM_MainRxLongAccInfo.IntSenFltSymtmActive;
    AyM_MainRxLongAccInfo.IntSenFaultPresent = Task_10msBus.AyM_MainRxLongAccInfo.IntSenFaultPresent;
    AyM_MainRxLongAccInfo.LongAccSenCirErrPre = Task_10msBus.AyM_MainRxLongAccInfo.LongAccSenCirErrPre;
    AyM_MainRxLongAccInfo.LonACSenRanChkErrPre = Task_10msBus.AyM_MainRxLongAccInfo.LonACSenRanChkErrPre;
    AyM_MainRxLongAccInfo.LongRollingCounter = Task_10msBus.AyM_MainRxLongAccInfo.LongRollingCounter;
    AyM_MainRxLongAccInfo.IntTempSensorFault = Task_10msBus.AyM_MainRxLongAccInfo.IntTempSensorFault;

    /* Decomposed structure interface */
    AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = Task_10msBus.AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg;
    AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = Task_10msBus.AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg;
    AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = Task_10msBus.AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg;

    /* Decomposed structure interface */
    AyM_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssFL;
    AyM_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssFR;
    AyM_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssRL;
    AyM_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.AyM_MainEemFailData.Eem_Fail_WssRR;

    /* Single interface */
    //AyM_MainEcuModeSts = Task_10msBus.AyM_MainEcuModeSts;
    AyM_MainIgnOnOffSts = Task_10msBus.AyM_MainIgnOnOffSts;
    AyM_MainIgnEdgeSts = Task_10msBus.AyM_MainIgnEdgeSts;
    AyM_MainVBatt1Mon = Task_10msBus.AyM_MainVBatt1Mon;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    AyM_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    AyM_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Inter-Runnable single interface */
    AyM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    AyM_Main();

    /* Structure interface */
    Task_10msBus.AyM_MainAYMOuitInfo = AyM_MainAYMOuitInfo;
    /*==============================================================================
    * Members of structure AyM_MainAYMOuitInfo 
     : AyM_MainAYMOuitInfo.AYM_Timeout_Err;
     : AyM_MainAYMOuitInfo.AYM_Invalid_Err;
     : AyM_MainAYMOuitInfo.AYM_CRC_Err;
     : AyM_MainAYMOuitInfo.AYM_Rolling_Err;
     : AyM_MainAYMOuitInfo.AYM_Temperature_Err;
     : AyM_MainAYMOuitInfo.AYM_Range_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     AyM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     AxM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    AxM_MainCanRxEscInfo.Ax = Task_10msBus.AxM_MainCanRxEscInfo.Ax;

    AxM_MainRxYawSerialInfo = Task_10msBus.AxM_MainRxYawSerialInfo;
    /*==============================================================================
    * Members of structure AxM_MainRxYawSerialInfo 
     : AxM_MainRxYawSerialInfo.YawSerialNum_0;
     : AxM_MainRxYawSerialInfo.YawSerialNum_1;
     : AxM_MainRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/

    /* Decomposed structure interface */
    AxM_MainRxYawAccInfo.YawRateValidData = Task_10msBus.AxM_MainRxYawAccInfo.YawRateValidData;
    AxM_MainRxYawAccInfo.YawRateSelfTestStatus = Task_10msBus.AxM_MainRxYawAccInfo.YawRateSelfTestStatus;
    AxM_MainRxYawAccInfo.SensorOscFreqDev = Task_10msBus.AxM_MainRxYawAccInfo.SensorOscFreqDev;
    AxM_MainRxYawAccInfo.Gyro_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Gyro_Fail;
    AxM_MainRxYawAccInfo.Raster_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Raster_Fail;
    AxM_MainRxYawAccInfo.Eep_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Eep_Fail;
    AxM_MainRxYawAccInfo.Batt_Range_Err = Task_10msBus.AxM_MainRxYawAccInfo.Batt_Range_Err;
    AxM_MainRxYawAccInfo.Asic_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Asic_Fail;
    AxM_MainRxYawAccInfo.Accel_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Accel_Fail;
    AxM_MainRxYawAccInfo.Ram_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Ram_Fail;
    AxM_MainRxYawAccInfo.Rom_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Rom_Fail;
    AxM_MainRxYawAccInfo.Ad_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Ad_Fail;
    AxM_MainRxYawAccInfo.Osc_Fail = Task_10msBus.AxM_MainRxYawAccInfo.Osc_Fail;
    AxM_MainRxYawAccInfo.Watchdog_Rst = Task_10msBus.AxM_MainRxYawAccInfo.Watchdog_Rst;
    AxM_MainRxYawAccInfo.Plaus_Err_Pst = Task_10msBus.AxM_MainRxYawAccInfo.Plaus_Err_Pst;
    AxM_MainRxYawAccInfo.RollingCounter = Task_10msBus.AxM_MainRxYawAccInfo.RollingCounter;
    AxM_MainRxYawAccInfo.Can_Func_Err = Task_10msBus.AxM_MainRxYawAccInfo.Can_Func_Err;

    /* Decomposed structure interface */
    AxM_MainRxLongAccInfo.IntSenFltSymtmActive = Task_10msBus.AxM_MainRxLongAccInfo.IntSenFltSymtmActive;
    AxM_MainRxLongAccInfo.IntSenFaultPresent = Task_10msBus.AxM_MainRxLongAccInfo.IntSenFaultPresent;
    AxM_MainRxLongAccInfo.LongAccSenCirErrPre = Task_10msBus.AxM_MainRxLongAccInfo.LongAccSenCirErrPre;
    AxM_MainRxLongAccInfo.LonACSenRanChkErrPre = Task_10msBus.AxM_MainRxLongAccInfo.LonACSenRanChkErrPre;
    AxM_MainRxLongAccInfo.LongRollingCounter = Task_10msBus.AxM_MainRxLongAccInfo.LongRollingCounter;
    AxM_MainRxLongAccInfo.IntTempSensorFault = Task_10msBus.AxM_MainRxLongAccInfo.IntTempSensorFault;
    AxM_MainRxLongAccInfo.LongAccInvalidData = Task_10msBus.AxM_MainRxLongAccInfo.LongAccInvalidData;
    AxM_MainRxLongAccInfo.LongAccSelfTstStatus = Task_10msBus.AxM_MainRxLongAccInfo.LongAccSelfTstStatus;

    /* Decomposed structure interface */
    AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg = Task_10msBus.AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg;
    AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg = Task_10msBus.AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg;
    AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg = Task_10msBus.AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg;

    /* Decomposed structure interface */
    AxM_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssFL;
    AxM_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssFR;
    AxM_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssRL;
    AxM_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.AxM_MainEemFailData.Eem_Fail_WssRR;

    /* Single interface */
    //AxM_MainEcuModeSts = Task_10msBus.AxM_MainEcuModeSts;
    AxM_MainIgnOnOffSts = Task_10msBus.AxM_MainIgnOnOffSts;
    AxM_MainIgnEdgeSts = Task_10msBus.AxM_MainIgnEdgeSts;
    AxM_MainVBatt1Mon = Task_10msBus.AxM_MainVBatt1Mon;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    AxM_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    AxM_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Inter-Runnable single interface */
    AxM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    AxM_Main();

    /* Structure interface */
    Task_10msBus.AxM_MainAXMOutInfo = AxM_MainAXMOutInfo;
    /*==============================================================================
    * Members of structure AxM_MainAXMOutInfo 
     : AxM_MainAXMOutInfo.AXM_Timeout_Err;
     : AxM_MainAXMOutInfo.AXM_Invalid_Err;
     : AxM_MainAXMOutInfo.AXM_CRC_Err;
     : AxM_MainAXMOutInfo.AXM_Rolling_Err;
     : AxM_MainAXMOutInfo.AXM_Temperature_Err;
     : AxM_MainAXMOutInfo.AXM_Range_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     AxM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     SasM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    SasM_MainCanRxEscInfo.SteeringAngle = Task_10msBus.SasM_MainCanRxEscInfo.SteeringAngle;

    /* Decomposed structure interface */
    SasM_MainCanRxInfo.MainBusOffFlag = Task_10msBus.SasM_MainCanRxInfo.MainBusOffFlag;

    /* Decomposed structure interface */
    SasM_MainRxSasInfo.Angle = Task_10msBus.SasM_MainRxSasInfo.Angle;
    SasM_MainRxSasInfo.Speed = Task_10msBus.SasM_MainRxSasInfo.Speed;
    SasM_MainRxSasInfo.Ok = Task_10msBus.SasM_MainRxSasInfo.Ok;
    SasM_MainRxSasInfo.Trim = Task_10msBus.SasM_MainRxSasInfo.Trim;
    SasM_MainRxSasInfo.CheckSum = Task_10msBus.SasM_MainRxSasInfo.CheckSum;

    /* Decomposed structure interface */
    SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg = Task_10msBus.SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg;

    /* Decomposed structure interface */
    SasM_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssFL;
    SasM_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssFR;
    SasM_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssRL;
    SasM_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.SasM_MainEemFailData.Eem_Fail_WssRR;

    /* Single interface */
    //SasM_MainEcuModeSts = Task_10msBus.SasM_MainEcuModeSts;
    SasM_MainIgnOnOffSts = Task_10msBus.SasM_MainIgnOnOffSts;
    SasM_MainIgnEdgeSts = Task_10msBus.SasM_MainIgnEdgeSts;
    SasM_MainVBatt1Mon = Task_10msBus.SasM_MainVBatt1Mon;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    SasM_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    SasM_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Inter-Runnable single interface */
    SasM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    SasM_Main();

    /* Structure interface */
    Task_10msBus.SasM_MainSASMOutInfo = SasM_MainSASMOutInfo;
    /*==============================================================================
    * Members of structure SasM_MainSASMOutInfo 
     : SasM_MainSASMOutInfo.SASM_Timeout_Err;
     : SasM_MainSASMOutInfo.SASM_Invalid_Err;
     : SasM_MainSASMOutInfo.SASM_CRC_Err;
     : SasM_MainSASMOutInfo.SASM_Rolling_Err;
     : SasM_MainSASMOutInfo.SASM_Range_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     SasM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     YawP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    YawP_MainEemFailData.Eem_Fail_SenPwr_12V = Task_10msBus.YawP_MainEemFailData.Eem_Fail_SenPwr_12V;
    YawP_MainEemFailData.Eem_Fail_Yaw = Task_10msBus.YawP_MainEemFailData.Eem_Fail_Yaw;
    YawP_MainEemFailData.Eem_Fail_Ay = Task_10msBus.YawP_MainEemFailData.Eem_Fail_Ay;
    YawP_MainEemFailData.Eem_Fail_Str = Task_10msBus.YawP_MainEemFailData.Eem_Fail_Str;
    YawP_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssFL;
    YawP_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssFR;
    YawP_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssRL;
    YawP_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.YawP_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    YawP_MainCanRxEscInfo.YawRate = Task_10msBus.YawP_MainCanRxEscInfo.YawRate;

    /* Decomposed structure interface */
    YawP_MainEscSwtStInfo.EscDisabledBySwt = Task_10msBus.YawP_MainEscSwtStInfo.EscDisabledBySwt;

    YawP_MainWhlSpdInfo = Task_10msBus.YawP_MainWhlSpdInfo;
    /*==============================================================================
    * Members of structure YawP_MainWhlSpdInfo 
     : YawP_MainWhlSpdInfo.FlWhlSpd;
     : YawP_MainWhlSpdInfo.FrWhlSpd;
     : YawP_MainWhlSpdInfo.RlWhlSpd;
     : YawP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = Task_10msBus.YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg;

    /* Decomposed structure interface */
    YawP_MainEemSuspectData.Eem_Suspect_WssFL = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssFL;
    YawP_MainEemSuspectData.Eem_Suspect_WssFR = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssFR;
    YawP_MainEemSuspectData.Eem_Suspect_WssRL = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssRL;
    YawP_MainEemSuspectData.Eem_Suspect_WssRR = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_WssRR;
    YawP_MainEemSuspectData.Eem_Suspect_FrontWss = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_FrontWss;
    YawP_MainEemSuspectData.Eem_Suspect_RearWss = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_RearWss;
    YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = Task_10msBus.YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V;

    /* Decomposed structure interface */
    YawP_MainEemEceData.Eem_Ece_Yaw = Task_10msBus.YawP_MainEemEceData.Eem_Ece_Yaw;

    YawP_MainIMUCalcInfo = Task_10msBus.YawP_MainIMUCalcInfo;
    /*==============================================================================
    * Members of structure YawP_MainIMUCalcInfo 
     : YawP_MainIMUCalcInfo.Reverse_Gear_flg;
     : YawP_MainIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/

    YawP_MainWssSpeedOut = Task_10msBus.YawP_MainWssSpeedOut;
    /*==============================================================================
    * Members of structure YawP_MainWssSpeedOut 
     : YawP_MainWssSpeedOut.WssMax;
     : YawP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    YawP_MainWssCalcInfo = Task_10msBus.YawP_MainWssCalcInfo;
    /*==============================================================================
    * Members of structure YawP_MainWssCalcInfo 
     : YawP_MainWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/

    /* Single interface */
    //YawP_MainEcuModeSts = Task_10msBus.YawP_MainEcuModeSts;
    YawP_MainIgnOnOffSts = Task_10msBus.YawP_MainIgnOnOffSts;
    YawP_MainIgnEdgeSts = Task_10msBus.YawP_MainIgnEdgeSts;
    YawP_MainVBatt1Mon = Task_10msBus.YawP_MainVBatt1Mon;
    YawP_MainVehSpd = Task_10msBus.YawP_MainVehSpd;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    YawP_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    YawP_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Decomposed structure interface */
    YawP_MainCanMonData.CanM_SubBusOff_Err = CanM_MainCanMonData.CanM_SubBusOff_Err;

    /* Inter-Runnable single interface */
    YawP_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    YawP_Main();

    /* Structure interface */
    Task_10msBus.YawP_MainYawPlauOutput = YawP_MainYawPlauOutput;
    /*==============================================================================
    * Members of structure YawP_MainYawPlauOutput 
     : YawP_MainYawPlauOutput.YawPlauModelErr;
     : YawP_MainYawPlauOutput.YawPlauNoisekErr;
     : YawP_MainYawPlauOutput.YawPlauShockErr;
     : YawP_MainYawPlauOutput.YawPlauRangeErr;
     : YawP_MainYawPlauOutput.YawPlauStandStillErr;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     YawP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     AyP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    AyP_MainEemFailData.Eem_Fail_SenPwr_12V = Task_10msBus.AyP_MainEemFailData.Eem_Fail_SenPwr_12V;
    AyP_MainEemFailData.Eem_Fail_Yaw = Task_10msBus.AyP_MainEemFailData.Eem_Fail_Yaw;
    AyP_MainEemFailData.Eem_Fail_Ay = Task_10msBus.AyP_MainEemFailData.Eem_Fail_Ay;
    AyP_MainEemFailData.Eem_Fail_Str = Task_10msBus.AyP_MainEemFailData.Eem_Fail_Str;
    AyP_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssFL;
    AyP_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssFR;
    AyP_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssRL;
    AyP_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.AyP_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    AyP_MainCanRxEscInfo.Ay = Task_10msBus.AyP_MainCanRxEscInfo.Ay;

    /* Decomposed structure interface */
    AyP_MainEscSwtStInfo.EscDisabledBySwt = Task_10msBus.AyP_MainEscSwtStInfo.EscDisabledBySwt;

    AyP_MainWhlSpdInfo = Task_10msBus.AyP_MainWhlSpdInfo;
    /*==============================================================================
    * Members of structure AyP_MainWhlSpdInfo 
     : AyP_MainWhlSpdInfo.FlWhlSpd;
     : AyP_MainWhlSpdInfo.FrWhlSpd;
     : AyP_MainWhlSpdInfo.RlWhlSpd;
     : AyP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = Task_10msBus.AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg;

    /* Decomposed structure interface */
    AyP_MainEemSuspectData.Eem_Suspect_WssFL = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssFL;
    AyP_MainEemSuspectData.Eem_Suspect_WssFR = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssFR;
    AyP_MainEemSuspectData.Eem_Suspect_WssRL = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssRL;
    AyP_MainEemSuspectData.Eem_Suspect_WssRR = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_WssRR;
    AyP_MainEemSuspectData.Eem_Suspect_FrontWss = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_FrontWss;
    AyP_MainEemSuspectData.Eem_Suspect_RearWss = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_RearWss;
    AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = Task_10msBus.AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V;

    /* Decomposed structure interface */
    AyP_MainEemEceData.Eem_Ece_Ay = Task_10msBus.AyP_MainEemEceData.Eem_Ece_Ay;

    AyP_MainIMUCalcInfo = Task_10msBus.AyP_MainIMUCalcInfo;
    /*==============================================================================
    * Members of structure AyP_MainIMUCalcInfo 
     : AyP_MainIMUCalcInfo.Reverse_Gear_flg;
     : AyP_MainIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/

    AyP_MainWssSpeedOut = Task_10msBus.AyP_MainWssSpeedOut;
    /*==============================================================================
    * Members of structure AyP_MainWssSpeedOut 
     : AyP_MainWssSpeedOut.WssMax;
     : AyP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    AyP_MainWssCalcInfo = Task_10msBus.AyP_MainWssCalcInfo;
    /*==============================================================================
    * Members of structure AyP_MainWssCalcInfo 
     : AyP_MainWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/

    /* Single interface */
    //AyP_MainEcuModeSts = Task_10msBus.AyP_MainEcuModeSts;
    AyP_MainIgnOnOffSts = Task_10msBus.AyP_MainIgnOnOffSts;
    AyP_MainIgnEdgeSts = Task_10msBus.AyP_MainIgnEdgeSts;
    AyP_MainVBatt1Mon = Task_10msBus.AyP_MainVBatt1Mon;
    AyP_MainVehSpd = Task_10msBus.AyP_MainVehSpd;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    AyP_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Decomposed structure interface */
    AyP_MainCanMonData.CanM_SubBusOff_Err = CanM_MainCanMonData.CanM_SubBusOff_Err;

    /* Inter-Runnable single interface */
    AyP_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    AyP_Main();

    /* Structure interface */
    Task_10msBus.AyP_MainAyPlauOutput = AyP_MainAyPlauOutput;
    /*==============================================================================
    * Members of structure AyP_MainAyPlauOutput 
     : AyP_MainAyPlauOutput.AyPlauNoiselErr;
     : AyP_MainAyPlauOutput.AyPlauModelErr;
     : AyP_MainAyPlauOutput.AyPlauShockErr;
     : AyP_MainAyPlauOutput.AyPlauRangeErr;
     : AyP_MainAyPlauOutput.AyPlauStandStillErr;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     AyP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     AxP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    AxP_MainEemFailData.Eem_Fail_SenPwr_12V = Task_10msBus.AxP_MainEemFailData.Eem_Fail_SenPwr_12V;
    AxP_MainEemFailData.Eem_Fail_SenPwr_5V = Task_10msBus.AxP_MainEemFailData.Eem_Fail_SenPwr_5V;
    AxP_MainEemFailData.Eem_Fail_Ax = Task_10msBus.AxP_MainEemFailData.Eem_Fail_Ax;
    AxP_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssFL;
    AxP_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssFR;
    AxP_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssRL;
    AxP_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.AxP_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    AxP_MainCanRxEscInfo.Ax = Task_10msBus.AxP_MainCanRxEscInfo.Ax;

    /* Decomposed structure interface */
    AxP_MainEscSwtStInfo.EscDisabledBySwt = Task_10msBus.AxP_MainEscSwtStInfo.EscDisabledBySwt;

    AxP_MainWhlSpdInfo = Task_10msBus.AxP_MainWhlSpdInfo;
    /*==============================================================================
    * Members of structure AxP_MainWhlSpdInfo 
     : AxP_MainWhlSpdInfo.FlWhlSpd;
     : AxP_MainWhlSpdInfo.FrWhlSpd;
     : AxP_MainWhlSpdInfo.RlWhlSpd;
     : AxP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    AxP_MainAbsCtrlInfo.AbsActFlg = Task_10msBus.AxP_MainAbsCtrlInfo.AbsActFlg;

    /* Decomposed structure interface */
    AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = Task_10msBus.AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg;

    /* Decomposed structure interface */
    AxP_MainEemSuspectData.Eem_Suspect_WssFL = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssFL;
    AxP_MainEemSuspectData.Eem_Suspect_WssFR = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssFR;
    AxP_MainEemSuspectData.Eem_Suspect_WssRL = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssRL;
    AxP_MainEemSuspectData.Eem_Suspect_WssRR = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_WssRR;
    AxP_MainEemSuspectData.Eem_Suspect_FrontWss = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_FrontWss;
    AxP_MainEemSuspectData.Eem_Suspect_RearWss = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_RearWss;
    AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V;
    AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V = Task_10msBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V;

    /* Decomposed structure interface */
    AxP_MainEemEceData.Eem_Ece_Ax = Task_10msBus.AxP_MainEemEceData.Eem_Ece_Ax;

    AxP_MainWssSpeedOut = Task_10msBus.AxP_MainWssSpeedOut;
    /*==============================================================================
    * Members of structure AxP_MainWssSpeedOut 
     : AxP_MainWssSpeedOut.WssMax;
     : AxP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    /* Single interface */
    //AxP_MainEcuModeSts = Task_10msBus.AxP_MainEcuModeSts;
    AxP_MainIgnOnOffSts = Task_10msBus.AxP_MainIgnOnOffSts;
    AxP_MainIgnEdgeSts = Task_10msBus.AxP_MainIgnEdgeSts;
    AxP_MainVBatt1Mon = Task_10msBus.AxP_MainVBatt1Mon;
    AxP_MainVehSpd = Task_10msBus.AxP_MainVehSpd;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    AxP_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Decomposed structure interface */
    AxP_MainCanMonData.CanM_SubBusOff_Err = CanM_MainCanMonData.CanM_SubBusOff_Err;

    /* Inter-Runnable single interface */
    AxP_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    AxP_Main();

    /* Structure interface */
    Task_10msBus.AxP_MainAxPlauOutput = AxP_MainAxPlauOutput;
    /*==============================================================================
    * Members of structure AxP_MainAxPlauOutput 
     : AxP_MainAxPlauOutput.AxPlauOffsetErr;
     : AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr;
     : AxP_MainAxPlauOutput.AxPlauStickErr;
     : AxP_MainAxPlauOutput.AxPlauNoiseErr;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     AxP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     SasP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    SasP_MainEemFailData.Eem_Fail_SenPwr_12V = Task_10msBus.SasP_MainEemFailData.Eem_Fail_SenPwr_12V;
    SasP_MainEemFailData.Eem_Fail_Yaw = Task_10msBus.SasP_MainEemFailData.Eem_Fail_Yaw;
    SasP_MainEemFailData.Eem_Fail_Ay = Task_10msBus.SasP_MainEemFailData.Eem_Fail_Ay;
    SasP_MainEemFailData.Eem_Fail_Str = Task_10msBus.SasP_MainEemFailData.Eem_Fail_Str;
    SasP_MainEemFailData.Eem_Fail_WssFL = Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssFL;
    SasP_MainEemFailData.Eem_Fail_WssFR = Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssFR;
    SasP_MainEemFailData.Eem_Fail_WssRL = Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssRL;
    SasP_MainEemFailData.Eem_Fail_WssRR = Task_10msBus.SasP_MainEemFailData.Eem_Fail_WssRR;

    /* Decomposed structure interface */
    SasP_MainCanRxEscInfo.SteeringAngle = Task_10msBus.SasP_MainCanRxEscInfo.SteeringAngle;

    /* Decomposed structure interface */
    SasP_MainEscSwtStInfo.EscDisabledBySwt = Task_10msBus.SasP_MainEscSwtStInfo.EscDisabledBySwt;

    SasP_MainWhlSpdInfo = Task_10msBus.SasP_MainWhlSpdInfo;
    /*==============================================================================
    * Members of structure SasP_MainWhlSpdInfo 
     : SasP_MainWhlSpdInfo.FlWhlSpd;
     : SasP_MainWhlSpdInfo.FrWhlSpd;
     : SasP_MainWhlSpdInfo.RlWhlSpd;
     : SasP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    SasP_MainAbsCtrlInfo.AbsActFlg = Task_10msBus.SasP_MainAbsCtrlInfo.AbsActFlg;

    /* Decomposed structure interface */
    SasP_MainEscCtrlInfo.EscActFlg = Task_10msBus.SasP_MainEscCtrlInfo.EscActFlg;

    /* Decomposed structure interface */
    SasP_MainTcsCtrlInfo.TcsActFlg = Task_10msBus.SasP_MainTcsCtrlInfo.TcsActFlg;

    /* Decomposed structure interface */
    SasP_MainEemSuspectData.Eem_Suspect_WssFL = Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssFL;
    SasP_MainEemSuspectData.Eem_Suspect_WssFR = Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssFR;
    SasP_MainEemSuspectData.Eem_Suspect_WssRL = Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssRL;
    SasP_MainEemSuspectData.Eem_Suspect_WssRR = Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_WssRR;
    SasP_MainEemSuspectData.Eem_Suspect_FrontWss = Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_FrontWss;
    SasP_MainEemSuspectData.Eem_Suspect_RearWss = Task_10msBus.SasP_MainEemSuspectData.Eem_Suspect_RearWss;

    SasP_MainWssSpeedOut = Task_10msBus.SasP_MainWssSpeedOut;
    /*==============================================================================
    * Members of structure SasP_MainWssSpeedOut 
     : SasP_MainWssSpeedOut.WssMax;
     : SasP_MainWssSpeedOut.WssMin;
     =============================================================================*/

    /* Single interface */
    //SasP_MainEcuModeSts = Task_10msBus.SasP_MainEcuModeSts;
    SasP_MainIgnOnOffSts = Task_10msBus.SasP_MainIgnOnOffSts;
    SasP_MainIgnEdgeSts = Task_10msBus.SasP_MainIgnEdgeSts;
    SasP_MainVBatt1Mon = Task_10msBus.SasP_MainVBatt1Mon;
    SasP_MainVehSpd = Task_10msBus.SasP_MainVehSpd;

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    SasP_MainSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    SasP_MainSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    /* Decomposed structure interface */
    SasP_MainCanMonData.CanM_MainBusOff_Err = CanM_MainCanMonData.CanM_MainBusOff_Err;

    /* Inter-Runnable single interface */
    SasP_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    SasP_Main();

    /* Structure interface */
    Task_10msBus.SasP_MainSasPlauOutput = SasP_MainSasPlauOutput;
    /*==============================================================================
    * Members of structure SasP_MainSasPlauOutput 
     : SasP_MainSasPlauOutput.SasPlauModelErr;
     : SasP_MainSasPlauOutput.SasPlauOffsetErr;
     : SasP_MainSasPlauOutput.SasPlauStickErr;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     SasP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     SwtM_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    SwtM_MainSwtMonInfo.AvhSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.AvhSwtMon;
    SwtM_MainSwtMonInfo.BflSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.BflSwtMon;
    SwtM_MainSwtMonInfo.BlsSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.BlsSwtMon;
    SwtM_MainSwtMonInfo.BsSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.BsSwtMon;
    SwtM_MainSwtMonInfo.EscSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.EscSwtMon;
    SwtM_MainSwtMonInfo.HdcSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.HdcSwtMon;
    SwtM_MainSwtMonInfo.PbSwtMon = Task_10msBus.SwtM_MainSwtMonInfo.PbSwtMon;

    /* Single interface */
    //SwtM_MainEcuModeSts = Task_10msBus.SwtM_MainEcuModeSts;
    SwtM_MainIgnOnOffSts = Task_10msBus.SwtM_MainIgnOnOffSts;
    SwtM_MainIgnEdgeSts = Task_10msBus.SwtM_MainIgnEdgeSts;
    SwtM_MainVBatt1Mon = Task_10msBus.SwtM_MainVBatt1Mon;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    SwtM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    SwtM_Main();

    /* Structure interface */
    Task_10msBus.SwtM_MainSwtMonFaultInfo = SwtM_MainSwtMonFaultInfo;
    /*==============================================================================
    * Members of structure SwtM_MainSwtMonFaultInfo 
     : SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     SwtM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     SwtP_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    SwtP_MainEemFailData.Eem_Fail_SimP = Task_10msBus.SwtP_MainEemFailData.Eem_Fail_SimP;
    SwtP_MainEemFailData.Eem_Fail_ParkBrake = Task_10msBus.SwtP_MainEemFailData.Eem_Fail_ParkBrake;
    SwtP_MainEemFailData.Eem_Fail_PedalPDT = Task_10msBus.SwtP_MainEemFailData.Eem_Fail_PedalPDT;
    SwtP_MainEemFailData.Eem_Fail_PedalPDF = Task_10msBus.SwtP_MainEemFailData.Eem_Fail_PedalPDF;

    /* Decomposed structure interface */
    //SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit = Task_10msBus.SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit;

    /* Decomposed structure interface */
    SwtP_MainSwTrigPwrInfo.Vbatt01Mon = Task_10msBus.SwtP_MainSwTrigPwrInfo.Vbatt01Mon;

    /* Decomposed structure interface */
    SwtP_MainPdf5msRawInfo.MoveAvrPdfSig = Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSig;
    SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset;
    SwtP_MainPdf5msRawInfo.MoveAvrPdtSig = Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSig;
    SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = Task_10msBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset;

    /* Decomposed structure interface */
    SwtP_MainSwtMonInfo.BlsSwtMon = Task_10msBus.SwtP_MainSwtMonInfo.BlsSwtMon;
    SwtP_MainSwtMonInfo.BsSwtMon = Task_10msBus.SwtP_MainSwtMonInfo.BsSwtMon;

    /* Decomposed structure interface */
    SwtP_MainEemSuspectData.Eem_Suspect_BS = Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_BS;
    SwtP_MainEemSuspectData.Eem_Suspect_BLS = Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_BLS;
    SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake = Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake;
    SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT = Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT;
    SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF = Task_10msBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF;

    /* Single interface */
    //SwtP_MainEcuModeSts = Task_10msBus.SwtP_MainEcuModeSts;
    SwtP_MainIgnOnOffSts = Task_10msBus.SwtP_MainIgnOnOffSts;
    SwtP_MainIgnEdgeSts = Task_10msBus.SwtP_MainIgnEdgeSts;
    SwtP_MainVBatt1Mon = Task_10msBus.SwtP_MainVBatt1Mon;
    SwtP_MainVehSpdFild = Task_10msBus.SwtP_MainVehSpdFild;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    SwtP_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    SwtP_Main();

    /* Structure interface */
    Task_10msBus.SwtP_MainSwtPFaultInfo = SwtP_MainSwtPFaultInfo;
    /*==============================================================================
    * Members of structure SwtP_MainSwtPFaultInfo 
     : SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     SwtP_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     RlyM_Main start
     **********************************************************************************************/

     /* Structure interface */
    RlyM_MainRlyDrvInfo = Task_10msBus.RlyM_MainRlyDrvInfo;
    /*==============================================================================
    * Members of structure RlyM_MainRlyDrvInfo 
     : RlyM_MainRlyDrvInfo.RlyDbcDrv;
     : RlyM_MainRlyDrvInfo.RlyEssDrv;
     =============================================================================*/

    RlyM_MainRlyMonInfo = Task_10msBus.RlyM_MainRlyMonInfo;
    /*==============================================================================
    * Members of structure RlyM_MainRlyMonInfo 
     : RlyM_MainRlyMonInfo.RlyDbcMon;
     : RlyM_MainRlyMonInfo.RlyEssMon;
     : RlyM_MainRlyMonInfo.RlyFault;
     =============================================================================*/

    /* Single interface */
    //RlyM_MainEcuModeSts = Task_10msBus.RlyM_MainEcuModeSts;
    RlyM_MainIgnOnOffSts = Task_10msBus.RlyM_MainIgnOnOffSts;
    RlyM_MainIgnEdgeSts = Task_10msBus.RlyM_MainIgnEdgeSts;
    RlyM_MainVBatt1Mon = Task_10msBus.RlyM_MainVBatt1Mon;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    RlyM_MainDiagClrSrs = Diag_HndlrDiagClrSrs;

    /* Execute  runnable */
    RlyM_Main();

    /* Structure interface */
    Task_10msBus.RlyM_MainRelayMonitorData = RlyM_MainRelayMonitorData;
    /*==============================================================================
    * Members of structure RlyM_MainRelayMonitorData 
     : RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err;
     : RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err;
     : RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err;
     : RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     RlyM_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Proxy_Tx start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    //Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail = Task_10msBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
    //Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = Task_10msBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;

    Proxy_TxCanTxInfo = Task_10msBus.Proxy_TxCanTxInfo;
    /*==============================================================================
    * Members of structure Proxy_TxCanTxInfo 
     : Proxy_TxCanTxInfo.TqIntvTCS;
     : Proxy_TxCanTxInfo.TqIntvMsr;
     : Proxy_TxCanTxInfo.TqIntvSlowTCS;
     : Proxy_TxCanTxInfo.MinGear;
     : Proxy_TxCanTxInfo.MaxGear;
     : Proxy_TxCanTxInfo.TcsReq;
     : Proxy_TxCanTxInfo.TcsCtrl;
     : Proxy_TxCanTxInfo.AbsAct;
     : Proxy_TxCanTxInfo.TcsGearShiftChr;
     : Proxy_TxCanTxInfo.EspCtrl;
     : Proxy_TxCanTxInfo.MsrReq;
     : Proxy_TxCanTxInfo.TcsProductInfo;
     =============================================================================*/

    Proxy_TxPdt5msRawInfo = Task_10msBus.Proxy_TxPdt5msRawInfo;
    /*==============================================================================
    * Members of structure Proxy_TxPdt5msRawInfo 
     : Proxy_TxPdt5msRawInfo.PdtSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_TxPdf5msRawInfo.PdfSig = Task_10msBus.Proxy_TxPdf5msRawInfo.PdfSig;

    /* Decomposed structure interface */
    Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq = Task_10msBus.Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq;
    Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce = Task_10msBus.Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce;
    Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce = Task_10msBus.Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce;
    Proxy_TxTarRgnBrkTqInfo.EhbStat = Task_10msBus.Proxy_TxTarRgnBrkTqInfo.EhbStat;

    /* Decomposed structure interface */
    Proxy_TxEscSwtStInfo.EscDisabledBySwt = Task_10msBus.Proxy_TxEscSwtStInfo.EscDisabledBySwt;
    Proxy_TxEscSwtStInfo.TcsDisabledBySwt = Task_10msBus.Proxy_TxEscSwtStInfo.TcsDisabledBySwt;

    Proxy_TxWhlSpdInfo = Task_10msBus.Proxy_TxWhlSpdInfo;
    /*==============================================================================
    * Members of structure Proxy_TxWhlSpdInfo 
     : Proxy_TxWhlSpdInfo.FlWhlSpd;
     : Proxy_TxWhlSpdInfo.FrWhlSpd;
     : Proxy_TxWhlSpdInfo.RlWhlSpd;
     : Proxy_TxWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Proxy_TxWhlEdgeCntInfo = Task_10msBus.Proxy_TxWhlEdgeCntInfo;
    /*==============================================================================
    * Members of structure Proxy_TxWhlEdgeCntInfo 
     : Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt;
     =============================================================================*/

    Proxy_TxPistP5msRawInfo = Task_10msBus.Proxy_TxPistP5msRawInfo;
    /*==============================================================================
    * Members of structure Proxy_TxPistP5msRawInfo 
     : Proxy_TxPistP5msRawInfo.PistPSig;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal = Task_10msBus.Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal;
    Proxy_TxLogicEepDataInfo.ReadInvld = Task_10msBus.Proxy_TxLogicEepDataInfo.ReadInvld;

    Proxy_TxEemLampData = Task_10msBus.Proxy_TxEemLampData;
    /*==============================================================================
    * Members of structure Proxy_TxEemLampData 
     : Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest;
     : Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ACCEnable;
     : Proxy_TxEemLampData.Eem_Lamp_CDMEnable;
     : Proxy_TxEemLampData.Eem_Buzzor_On;
     =============================================================================*/

    /* Single interface */
    //Proxy_TxEcuModeSts = Task_10msBus.Proxy_TxEcuModeSts;
    Proxy_TxIgnOnOffSts = Task_10msBus.Proxy_TxIgnOnOffSts;
    //Proxy_TxFuncInhibitProxySts = Task_10msBus.Proxy_TxFuncInhibitProxySts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    Proxy_TxDiagSci = Diag_HndlrDiagSci;
    Proxy_TxDiagAhbSci = Diag_HndlrDiagAhbSci;

    /* Execute  runnable */
    if (wmu1LoopTime10ms_1 == 1) Proxy_Tx();

    /* Structure interface */
    Task_10msBus.Proxy_TxTxESCSensorInfo = Proxy_TxTxESCSensorInfo;
    /*==============================================================================
    * Members of structure Proxy_TxTxESCSensorInfo 
     : Proxy_TxTxESCSensorInfo.EstimatedYaw;
     : Proxy_TxTxESCSensorInfo.EstimatedAY;
     : Proxy_TxTxESCSensorInfo.A_long_1_1000g;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Proxy_Tx end
     **********************************************************************************************/

    /**********************************************************************************************
     Proxy_TxByCom start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    Proxy_TxByComTxYawCbitInfo = Proxy_TxTxYawCbitInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxYawCbitInfo 
     : Proxy_TxByComTxYawCbitInfo.TestYawRateSensor;
     : Proxy_TxByComTxYawCbitInfo.TestAcclSensor;
     : Proxy_TxByComTxYawCbitInfo.YawSerialNumberReq;
     =============================================================================*/

    Proxy_TxByComTxWhlSpdInfo = Proxy_TxTxWhlSpdInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxWhlSpdInfo 
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdFL;
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdFR;
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdRL;
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdRR;
     =============================================================================*/

    Proxy_TxByComTxWhlPulInfo = Proxy_TxTxWhlPulInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxWhlPulInfo 
     : Proxy_TxByComTxWhlPulInfo.WhlPulFL;
     : Proxy_TxByComTxWhlPulInfo.WhlPulFR;
     : Proxy_TxByComTxWhlPulInfo.WhlPulRL;
     : Proxy_TxByComTxWhlPulInfo.WhlPulRR;
     =============================================================================*/

    Proxy_TxByComTxTcs5Info = Proxy_TxTxTcs5Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxTcs5Info 
     : Proxy_TxByComTxTcs5Info.CfBrkAbsWLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkEbdWLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkTcsWLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkTcsFLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkAbsDIAG;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelFL_KMH;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelFR_KMH;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelRL_KMH;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelRR_KMH;
     =============================================================================*/

    Proxy_TxByComTxTcs1Info = Proxy_TxTxTcs1Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxTcs1Info 
     : Proxy_TxByComTxTcs1Info.CfBrkTcsREQ;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsPAS;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsCTL;
     : Proxy_TxByComTxTcs1Info.CfBrkAbsACT;
     : Proxy_TxByComTxTcs1Info.CfBrkAbsDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkEbdDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsGSC;
     : Proxy_TxByComTxTcs1Info.CfBrkEspPAS;
     : Proxy_TxByComTxTcs1Info.CfBrkEspDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkEspCTL;
     : Proxy_TxByComTxTcs1Info.CfBrkMsrREQ;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsMFRN;
     : Proxy_TxByComTxTcs1Info.CfBrkMinGEAR;
     : Proxy_TxByComTxTcs1Info.CfBrkMaxGEAR;
     : Proxy_TxByComTxTcs1Info.BrakeLight;
     : Proxy_TxByComTxTcs1Info.HacCtl;
     : Proxy_TxByComTxTcs1Info.HacPas;
     : Proxy_TxByComTxTcs1Info.HacDef;
     : Proxy_TxByComTxTcs1Info.CrBrkTQI_PC;
     : Proxy_TxByComTxTcs1Info.CrBrkTQIMSR_PC;
     : Proxy_TxByComTxTcs1Info.CrBrkTQISLW_PC;
     : Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM;
     =============================================================================*/

    Proxy_TxByComTxSasCalInfo = Proxy_TxTxSasCalInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxSasCalInfo 
     : Proxy_TxByComTxSasCalInfo.CalSas_CCW;
     : Proxy_TxByComTxSasCalInfo.CalSas_Lws_CID;
     =============================================================================*/

    Proxy_TxByComTxEsp2Info = Proxy_TxTxEsp2Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxEsp2Info 
     : Proxy_TxByComTxEsp2Info.LatAccel;
     : Proxy_TxByComTxEsp2Info.LatAccelStat;
     : Proxy_TxByComTxEsp2Info.LatAccelDiag;
     : Proxy_TxByComTxEsp2Info.LongAccel;
     : Proxy_TxByComTxEsp2Info.LongAccelStat;
     : Proxy_TxByComTxEsp2Info.LongAccelDiag;
     : Proxy_TxByComTxEsp2Info.YawRate;
     : Proxy_TxByComTxEsp2Info.YawRateStat;
     : Proxy_TxByComTxEsp2Info.YawRateDiag;
     : Proxy_TxByComTxEsp2Info.CylPres;
     : Proxy_TxByComTxEsp2Info.CylPresStat;
     : Proxy_TxByComTxEsp2Info.CylPresDiag;
     =============================================================================*/

    Proxy_TxByComTxEbs1Info = Proxy_TxTxEbs1Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxEbs1Info 
     : Proxy_TxByComTxEbs1Info.CfBrkEbsStat;
     : Proxy_TxByComTxEbs1Info.CrBrkRegenTQLimit_NM;
     : Proxy_TxByComTxEbs1Info.CrBrkEstTot_NM;
     : Proxy_TxByComTxEbs1Info.CfBrkSnsFail;
     : Proxy_TxByComTxEbs1Info.CfBrkRbsWLamp;
     : Proxy_TxByComTxEbs1Info.CfBrkVacuumSysDef;
     : Proxy_TxByComTxEbs1Info.CrBrkEstHyd_NM;
     : Proxy_TxByComTxEbs1Info.CrBrkStkDep_PC;
     : Proxy_TxByComTxEbs1Info.CfBrkPgmRun1;
     =============================================================================*/

    Proxy_TxByComTxAhb1Info = Proxy_TxTxAhb1Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComTxAhb1Info 
     : Proxy_TxByComTxAhb1Info.CfAhbWLMP;
     : Proxy_TxByComTxAhb1Info.CfAhbDiag;
     : Proxy_TxByComTxAhb1Info.CfAhbAct;
     : Proxy_TxByComTxAhb1Info.CfAhbDef;
     : Proxy_TxByComTxAhb1Info.CfAhbSLMP;
     : Proxy_TxByComTxAhb1Info.CfBrkAhbSTDep_PC;
     : Proxy_TxByComTxAhb1Info.CfBrkBuzzR;
     : Proxy_TxByComTxAhb1Info.CfBrkPedalCalStatus;
     : Proxy_TxByComTxAhb1Info.CfBrkAhbSnsFail;
     : Proxy_TxByComTxAhb1Info.WhlSpdFLAhb;
     : Proxy_TxByComTxAhb1Info.WhlSpdFRAhb;
     : Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum;
     =============================================================================*/

    /* Decomposed structure interface */
    Proxy_TxByComSenPwrMonitorData.SenPwrM_5V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    Proxy_TxByComSenPwrMonitorData.SenPwrM_12V_Stable = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;

    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo 
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB;
     =============================================================================*/

    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo 
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH;
     =============================================================================*/

    Proxy_TxByComAVL_BRTORQ_WHLInfo = Proxy_TxAVL_BRTORQ_WHLInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_BRTORQ_WHLInfo 
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH;
     =============================================================================*/

    Proxy_TxByComAVL_RPM_WHLInfo = Proxy_TxAVL_RPM_WHLInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_RPM_WHLInfo 
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH;
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH;
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH;
     =============================================================================*/

    Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo = Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo 
     : Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP;
     : Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP;
     : Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP;
     =============================================================================*/

    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info 
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS;
     =============================================================================*/

    Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo 
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA;
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD;
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA;
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA;
     =============================================================================*/

    Proxy_TxByComAVL_QUAN_EES_WHLInfo = Proxy_TxAVL_QUAN_EES_WHLInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_QUAN_EES_WHLInfo 
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH;
     =============================================================================*/

    Proxy_TxByComTAR_LTRQD_BAXInfo = Proxy_TxTAR_LTRQD_BAXInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComTAR_LTRQD_BAXInfo 
     : Proxy_TxByComTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX;
     : Proxy_TxByComTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX;
     =============================================================================*/

    Proxy_TxByComST_YMRInfo = Proxy_TxST_YMRInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComST_YMRInfo 
     : Proxy_TxByComST_YMRInfo.QU_SER_CLCTR_YMR;
     =============================================================================*/

    Proxy_TxByComWEGSTRECKEInfo = Proxy_TxWEGSTRECKEInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComWEGSTRECKEInfo 
     : Proxy_TxByComWEGSTRECKEInfo.MILE_FLH;
     : Proxy_TxByComWEGSTRECKEInfo.MILE_FRH;
     : Proxy_TxByComWEGSTRECKEInfo.MILE_INTM_TOMSRM;
     : Proxy_TxByComWEGSTRECKEInfo.MILE;
     =============================================================================*/

    Proxy_TxByComDISP_CC_DRDY_00Info = Proxy_TxDISP_CC_DRDY_00Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComDISP_CC_DRDY_00Info 
     : Proxy_TxByComDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00;
     : Proxy_TxByComDISP_CC_DRDY_00Info.ST_CC_DRDY_00;
     : Proxy_TxByComDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00;
     : Proxy_TxByComDISP_CC_DRDY_00Info.NO_CC_DRDY_00;
     =============================================================================*/

    Proxy_TxByComDISP_CC_BYPA_00Info = Proxy_TxDISP_CC_BYPA_00Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComDISP_CC_BYPA_00Info 
     : Proxy_TxByComDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00;
     : Proxy_TxByComDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00;
     : Proxy_TxByComDISP_CC_BYPA_00Info.ST_CC_BYPA_00;
     : Proxy_TxByComDISP_CC_BYPA_00Info.NO_CC_BYPA_00;
     =============================================================================*/

    Proxy_TxByComST_VHSSInfo = Proxy_TxST_VHSSInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComST_VHSSInfo 
     : Proxy_TxByComST_VHSSInfo.ST_VEHSS;
     =============================================================================*/

    Proxy_TxByComDIAG_OBD_HYB_1Info = Proxy_TxDIAG_OBD_HYB_1Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComDIAG_OBD_HYB_1Info 
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME;
     =============================================================================*/

    Proxy_TxByComSU_DSCInfo = Proxy_TxSU_DSCInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComSU_DSCInfo 
     : Proxy_TxByComSU_DSCInfo.SU_DSC_WSS;
     =============================================================================*/

    Proxy_TxByComST_TYR_RDCInfo = Proxy_TxST_TYR_RDCInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComST_TYR_RDCInfo 
     : Proxy_TxByComST_TYR_RDCInfo.QU_TPL_RDC;
     : Proxy_TxByComST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC;
     : Proxy_TxByComST_TYR_RDCInfo.QU_TFAI_RDC;
     =============================================================================*/

    Proxy_TxByComST_TYR_RPAInfo = Proxy_TxST_TYR_RPAInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComST_TYR_RPAInfo 
     : Proxy_TxByComST_TYR_RPAInfo.QU_TFAI_RPA;
     : Proxy_TxByComST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA;
     : Proxy_TxByComST_TYR_RPAInfo.QU_TPL_RPA;
     =============================================================================*/

    Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info = Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info 
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2;
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1;
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3;
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4;
     =============================================================================*/

    Proxy_TxByComST_TYR_2Info = Proxy_TxST_TYR_2Info;
    /*==============================================================================
    * Members of structure Proxy_TxByComST_TYR_2Info 
     : Proxy_TxByComST_TYR_2Info.QU_RDC_INIT_DISP;
     =============================================================================*/

    Proxy_TxByComAVL_T_TYRInfo = Proxy_TxAVL_T_TYRInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_T_TYRInfo 
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RRH;
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FLH;
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RLH;
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FRH;
     =============================================================================*/

    Proxy_TxByComTEMP_BRKInfo = Proxy_TxTEMP_BRKInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComTEMP_BRKInfo 
     : Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD;
     : Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD;
     : Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD;
     : Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD;
     =============================================================================*/

    Proxy_TxByComAVL_P_TYRInfo = Proxy_TxAVL_P_TYRInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_P_TYRInfo 
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FRH;
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FLH;
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RRH;
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RLH;
     =============================================================================*/

    Proxy_TxByComFR_DBG_DSCInfo = Proxy_TxFR_DBG_DSCInfo;
    /*==============================================================================
    * Members of structure Proxy_TxByComFR_DBG_DSCInfo 
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_13;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_04;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_05;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_06;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_01;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_02;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_03;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_07;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_11;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_10;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_09;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_15;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_14;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_12;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_00;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FR;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RL;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RR;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_08;
     : Proxy_TxByComFR_DBG_DSCInfo.ST_STATE;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FL;
     : Proxy_TxByComFR_DBG_DSCInfo.ABSRef;
     : Proxy_TxByComFR_DBG_DSCInfo.LOW_VOLTAGE;
     : Proxy_TxByComFR_DBG_DSCInfo.DEBUG_PCIB;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_DSC;
     : Proxy_TxByComFR_DBG_DSCInfo.LATACC;
     : Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_FS;
     : Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_IS;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    if (wmu1LoopTime10ms_1 == 1) Proxy_TxByCom();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Proxy_TxByCom end
     **********************************************************************************************/


    /* Output */
    Task_10ms_Write_Diag_HndlrDiagVlvActrInfo(&Task_10msBus.Diag_HndlrDiagVlvActrInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrDiagVlvActrInfo 
     : Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty;
     : Diag_HndlrDiagVlvActrInfo.FlOvReqData;
     : Diag_HndlrDiagVlvActrInfo.FlIvReqData;
     : Diag_HndlrDiagVlvActrInfo.FrOvReqData;
     : Diag_HndlrDiagVlvActrInfo.FrIvReqData;
     : Diag_HndlrDiagVlvActrInfo.RlOvReqData;
     : Diag_HndlrDiagVlvActrInfo.RlIvReqData;
     : Diag_HndlrDiagVlvActrInfo.RrOvReqData;
     : Diag_HndlrDiagVlvActrInfo.RrIvReqData;
     : Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.SimVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.ResPVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.BalVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.CircVlvReqData;
     : Diag_HndlrDiagVlvActrInfo.PressDumpReqData;
     : Diag_HndlrDiagVlvActrInfo.RelsVlvReqData;
     =============================================================================*/

    Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo(&Task_10msBus.Diag_HndlrMotReqDataDiagInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrMotReqDataDiagInfo 
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData;
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData;
     : Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData;
     : Diag_HndlrMotReqDataDiagInfo.MotReq;
     =============================================================================*/

    Task_10ms_Write_Diag_HndlrSasCalInfo(&Task_10msBus.Diag_HndlrSasCalInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrSasCalInfo 
     : Diag_HndlrSasCalInfo.DiagSasCaltoAppl;
     =============================================================================*/

    Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo(&Task_10msBus.Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo);
    /*==============================================================================
    * Members of structure Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo 
     : Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
     : Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
     =============================================================================*/

    Task_10ms_Write_Diag_HndlrDiagHndlRequest(&Task_10msBus.Diag_HndlrDiagHndlRequest);
    /*==============================================================================
    * Members of structure Diag_HndlrDiagHndlRequest 
     : Diag_HndlrDiagHndlRequest.DiagRequest_Order;
     : Diag_HndlrDiagHndlRequest.DiagRequest_Iq;
     : Diag_HndlrDiagHndlRequest.DiagRequest_Id;
     : Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM;
     : Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM;
     : Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM;
     =============================================================================*/

    Task_10ms_Write_CanM_MainCanMonData(&Task_10msBus.CanM_MainCanMonData);
    /*==============================================================================
    * Members of structure CanM_MainCanMonData 
     : CanM_MainCanMonData.CanM_MainBusOff_Err;
     : CanM_MainCanMonData.CanM_SubBusOff_Err;
     : CanM_MainCanMonData.CanM_MainOverRun_Err;
     : CanM_MainCanMonData.CanM_SubOverRun_Err;
     : CanM_MainCanMonData.CanM_EMS1_Tout_Err;
     : CanM_MainCanMonData.CanM_EMS2_Tout_Err;
     : CanM_MainCanMonData.CanM_TCU1_Tout_Err;
     : CanM_MainCanMonData.CanM_TCU5_Tout_Err;
     : CanM_MainCanMonData.CanM_FWD1_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU1_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU2_Tout_Err;
     : CanM_MainCanMonData.CanM_HCU3_Tout_Err;
     : CanM_MainCanMonData.CanM_VSM2_Tout_Err;
     : CanM_MainCanMonData.CanM_EMS_Invalid_Err;
     : CanM_MainCanMonData.CanM_TCU_Invalid_Err;
     =============================================================================*/

    Task_10ms_Write_SenPwrM_MainSenPwrMonitorData(&Task_10msBus.SenPwrM_MainSenPwrMonitorData);
    /*==============================================================================
    * Members of structure SenPwrM_MainSenPwrMonitorData 
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err;
     =============================================================================*/

    Task_10ms_Write_YawM_MainYAWMSerialInfo(&Task_10msBus.YawM_MainYAWMSerialInfo);
    /*==============================================================================
    * Members of structure YawM_MainYAWMSerialInfo 
     : YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg;
     : YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
     =============================================================================*/

    Task_10ms_Write_YawM_MainYAWMOutInfo(&Task_10msBus.YawM_MainYAWMOutInfo);
    /*==============================================================================
    * Members of structure YawM_MainYAWMOutInfo 
     : YawM_MainYAWMOutInfo.YAWM_Timeout_Err;
     : YawM_MainYAWMOutInfo.YAWM_Invalid_Err;
     : YawM_MainYAWMOutInfo.YAWM_CRC_Err;
     : YawM_MainYAWMOutInfo.YAWM_Rolling_Err;
     : YawM_MainYAWMOutInfo.YAWM_Temperature_Err;
     : YawM_MainYAWMOutInfo.YAWM_Range_Err;
     =============================================================================*/

    Task_10ms_Write_AyM_MainAYMOuitInfo(&Task_10msBus.AyM_MainAYMOuitInfo);
    /*==============================================================================
    * Members of structure AyM_MainAYMOuitInfo 
     : AyM_MainAYMOuitInfo.AYM_Timeout_Err;
     : AyM_MainAYMOuitInfo.AYM_Invalid_Err;
     : AyM_MainAYMOuitInfo.AYM_CRC_Err;
     : AyM_MainAYMOuitInfo.AYM_Rolling_Err;
     : AyM_MainAYMOuitInfo.AYM_Temperature_Err;
     : AyM_MainAYMOuitInfo.AYM_Range_Err;
     =============================================================================*/

    Task_10ms_Write_AxM_MainAXMOutInfo(&Task_10msBus.AxM_MainAXMOutInfo);
    /*==============================================================================
    * Members of structure AxM_MainAXMOutInfo 
     : AxM_MainAXMOutInfo.AXM_Timeout_Err;
     : AxM_MainAXMOutInfo.AXM_Invalid_Err;
     : AxM_MainAXMOutInfo.AXM_CRC_Err;
     : AxM_MainAXMOutInfo.AXM_Rolling_Err;
     : AxM_MainAXMOutInfo.AXM_Temperature_Err;
     : AxM_MainAXMOutInfo.AXM_Range_Err;
     =============================================================================*/

    Task_10ms_Write_SasM_MainSASMOutInfo(&Task_10msBus.SasM_MainSASMOutInfo);
    /*==============================================================================
    * Members of structure SasM_MainSASMOutInfo 
     : SasM_MainSASMOutInfo.SASM_Timeout_Err;
     : SasM_MainSASMOutInfo.SASM_Invalid_Err;
     : SasM_MainSASMOutInfo.SASM_CRC_Err;
     : SasM_MainSASMOutInfo.SASM_Rolling_Err;
     : SasM_MainSASMOutInfo.SASM_Range_Err;
     =============================================================================*/

    Task_10ms_Write_YawP_MainYawPlauOutput(&Task_10msBus.YawP_MainYawPlauOutput);
    /*==============================================================================
    * Members of structure YawP_MainYawPlauOutput 
     : YawP_MainYawPlauOutput.YawPlauModelErr;
     : YawP_MainYawPlauOutput.YawPlauNoisekErr;
     : YawP_MainYawPlauOutput.YawPlauShockErr;
     : YawP_MainYawPlauOutput.YawPlauRangeErr;
     : YawP_MainYawPlauOutput.YawPlauStandStillErr;
     =============================================================================*/

    Task_10ms_Write_AyP_MainAyPlauOutput(&Task_10msBus.AyP_MainAyPlauOutput);
    /*==============================================================================
    * Members of structure AyP_MainAyPlauOutput 
     : AyP_MainAyPlauOutput.AyPlauNoiselErr;
     : AyP_MainAyPlauOutput.AyPlauModelErr;
     : AyP_MainAyPlauOutput.AyPlauShockErr;
     : AyP_MainAyPlauOutput.AyPlauRangeErr;
     : AyP_MainAyPlauOutput.AyPlauStandStillErr;
     =============================================================================*/

    Task_10ms_Write_AxP_MainAxPlauOutput(&Task_10msBus.AxP_MainAxPlauOutput);
    /*==============================================================================
    * Members of structure AxP_MainAxPlauOutput 
     : AxP_MainAxPlauOutput.AxPlauOffsetErr;
     : AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr;
     : AxP_MainAxPlauOutput.AxPlauStickErr;
     : AxP_MainAxPlauOutput.AxPlauNoiseErr;
     =============================================================================*/

    Task_10ms_Write_SasP_MainSasPlauOutput(&Task_10msBus.SasP_MainSasPlauOutput);
    /*==============================================================================
    * Members of structure SasP_MainSasPlauOutput 
     : SasP_MainSasPlauOutput.SasPlauModelErr;
     : SasP_MainSasPlauOutput.SasPlauOffsetErr;
     : SasP_MainSasPlauOutput.SasPlauStickErr;
     =============================================================================*/

    Task_10ms_Write_SwtM_MainSwtMonFaultInfo(&Task_10msBus.SwtM_MainSwtMonFaultInfo);
    /*==============================================================================
    * Members of structure SwtM_MainSwtMonFaultInfo 
     : SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err;
     =============================================================================*/

    Task_10ms_Write_SwtP_MainSwtPFaultInfo(&Task_10msBus.SwtP_MainSwtPFaultInfo);
    /*==============================================================================
    * Members of structure SwtP_MainSwtPFaultInfo 
     : SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
     =============================================================================*/

    Task_10ms_Write_RlyM_MainRelayMonitorData(&Task_10msBus.RlyM_MainRelayMonitorData);
    /*==============================================================================
    * Members of structure RlyM_MainRelayMonitorData 
     : RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err;
     : RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err;
     : RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err;
     : RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err;
     =============================================================================*/

    Task_10ms_Write_Proxy_TxTxESCSensorInfo(&Task_10msBus.Proxy_TxTxESCSensorInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTxESCSensorInfo 
     : Proxy_TxTxESCSensorInfo.EstimatedYaw;
     : Proxy_TxTxESCSensorInfo.EstimatedAY;
     : Proxy_TxTxESCSensorInfo.A_long_1_1000g;
     =============================================================================*/

    Task_10ms_Write_Diag_HndlrDiagClrSrs(&Task_10msBus.Diag_HndlrDiagClrSrs);
    Task_10ms_Write_Diag_HndlrDiagSci(&Task_10msBus.Diag_HndlrDiagSci);
    Task_10ms_Write_Diag_HndlrDiagAhbSci(&Task_10msBus.Diag_HndlrDiagAhbSci);
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TASK_10MS_STOP_SEC_CODE
#include "Task_10ms_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
