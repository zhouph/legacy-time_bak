/**
 * @defgroup Task_10ms_Ifa Task_10ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_10ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_10MS_IFA_H_
#define TASK_10MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Task_10ms_Read_Diag_HndlrWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_Diag_HndlrWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo(data) do \
{ \
    *data = Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxRegenInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxRegenInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxAccelPedlInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxAccelPedlInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEngTempInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEngTempInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanTimeOutStInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanTimeOutStInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainEemFailData(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainEemFailData; \
}while(0);

#define Task_10ms_Read_YawM_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_YawM_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawSerialInfo(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawSerialInfo; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo; \
}while(0);

#define Task_10ms_Read_YawM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = Task_10ms_YawM_MainRxMsgOkFlgInfo; \
}while(0);

#define Task_10ms_Read_YawM_MainEemFailData(data) do \
{ \
    *data = Task_10ms_YawM_MainEemFailData; \
}while(0);

#define Task_10ms_Read_AyM_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_AyM_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawSerialInfo(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawSerialInfo; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo; \
}while(0);

#define Task_10ms_Read_AyM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = Task_10ms_AyM_MainRxMsgOkFlgInfo; \
}while(0);

#define Task_10ms_Read_AyM_MainEemFailData(data) do \
{ \
    *data = Task_10ms_AyM_MainEemFailData; \
}while(0);

#define Task_10ms_Read_AxM_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_AxM_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawSerialInfo(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawSerialInfo; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo; \
}while(0);

#define Task_10ms_Read_AxM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = Task_10ms_AxM_MainRxMsgOkFlgInfo; \
}while(0);

#define Task_10ms_Read_AxM_MainEemFailData(data) do \
{ \
    *data = Task_10ms_AxM_MainEemFailData; \
}while(0);

#define Task_10ms_Read_SasM_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_SasM_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_SasM_MainCanRxInfo(data) do \
{ \
    *data = Task_10ms_SasM_MainCanRxInfo; \
}while(0);

#define Task_10ms_Read_SasM_MainRxSasInfo(data) do \
{ \
    *data = Task_10ms_SasM_MainRxSasInfo; \
}while(0);

#define Task_10ms_Read_SasM_MainRxMsgOkFlgInfo(data) do \
{ \
    *data = Task_10ms_SasM_MainRxMsgOkFlgInfo; \
}while(0);

#define Task_10ms_Read_SasM_MainEemFailData(data) do \
{ \
    *data = Task_10ms_SasM_MainEemFailData; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData; \
}while(0);

#define Task_10ms_Read_YawP_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_YawP_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_YawP_MainEscSwtStInfo(data) do \
{ \
    *data = Task_10ms_YawP_MainEscSwtStInfo; \
}while(0);

#define Task_10ms_Read_YawP_MainWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_YawP_MainWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_YawP_MainBaseBrkCtrlModInfo(data) do \
{ \
    *data = Task_10ms_YawP_MainBaseBrkCtrlModInfo; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData; \
}while(0);

#define Task_10ms_Read_YawP_MainEemEceData(data) do \
{ \
    *data = Task_10ms_YawP_MainEemEceData; \
}while(0);

#define Task_10ms_Read_YawP_MainIMUCalcInfo(data) do \
{ \
    *data = Task_10ms_YawP_MainIMUCalcInfo; \
}while(0);

#define Task_10ms_Read_YawP_MainWssSpeedOut(data) do \
{ \
    *data = Task_10ms_YawP_MainWssSpeedOut; \
}while(0);

#define Task_10ms_Read_YawP_MainWssCalcInfo(data) do \
{ \
    *data = Task_10ms_YawP_MainWssCalcInfo; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData; \
}while(0);

#define Task_10ms_Read_AyP_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_AyP_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_AyP_MainEscSwtStInfo(data) do \
{ \
    *data = Task_10ms_AyP_MainEscSwtStInfo; \
}while(0);

#define Task_10ms_Read_AyP_MainWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_AyP_MainWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_AyP_MainBaseBrkCtrlModInfo(data) do \
{ \
    *data = Task_10ms_AyP_MainBaseBrkCtrlModInfo; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData; \
}while(0);

#define Task_10ms_Read_AyP_MainEemEceData(data) do \
{ \
    *data = Task_10ms_AyP_MainEemEceData; \
}while(0);

#define Task_10ms_Read_AyP_MainIMUCalcInfo(data) do \
{ \
    *data = Task_10ms_AyP_MainIMUCalcInfo; \
}while(0);

#define Task_10ms_Read_AyP_MainWssSpeedOut(data) do \
{ \
    *data = Task_10ms_AyP_MainWssSpeedOut; \
}while(0);

#define Task_10ms_Read_AyP_MainWssCalcInfo(data) do \
{ \
    *data = Task_10ms_AyP_MainWssCalcInfo; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData; \
}while(0);

#define Task_10ms_Read_AxP_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_AxP_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_AxP_MainEscSwtStInfo(data) do \
{ \
    *data = Task_10ms_AxP_MainEscSwtStInfo; \
}while(0);

#define Task_10ms_Read_AxP_MainWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_AxP_MainWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_AxP_MainAbsCtrlInfo(data) do \
{ \
    *data = Task_10ms_AxP_MainAbsCtrlInfo; \
}while(0);

#define Task_10ms_Read_AxP_MainBaseBrkCtrlModInfo(data) do \
{ \
    *data = Task_10ms_AxP_MainBaseBrkCtrlModInfo; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData; \
}while(0);

#define Task_10ms_Read_AxP_MainEemEceData(data) do \
{ \
    *data = Task_10ms_AxP_MainEemEceData; \
}while(0);

#define Task_10ms_Read_AxP_MainWssSpeedOut(data) do \
{ \
    *data = Task_10ms_AxP_MainWssSpeedOut; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData; \
}while(0);

#define Task_10ms_Read_SasP_MainCanRxEscInfo(data) do \
{ \
    *data = Task_10ms_SasP_MainCanRxEscInfo; \
}while(0);

#define Task_10ms_Read_SasP_MainEscSwtStInfo(data) do \
{ \
    *data = Task_10ms_SasP_MainEscSwtStInfo; \
}while(0);

#define Task_10ms_Read_SasP_MainWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_SasP_MainWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_SasP_MainAbsCtrlInfo(data) do \
{ \
    *data = Task_10ms_SasP_MainAbsCtrlInfo; \
}while(0);

#define Task_10ms_Read_SasP_MainEscCtrlInfo(data) do \
{ \
    *data = Task_10ms_SasP_MainEscCtrlInfo; \
}while(0);

#define Task_10ms_Read_SasP_MainTcsCtrlInfo(data) do \
{ \
    *data = Task_10ms_SasP_MainTcsCtrlInfo; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData; \
}while(0);

#define Task_10ms_Read_SasP_MainWssSpeedOut(data) do \
{ \
    *data = Task_10ms_SasP_MainWssSpeedOut; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemFailData(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemFailData; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemCtrlInhibitData(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemCtrlInhibitData; \
}while(0);

#define Task_10ms_Read_SwtP_MainSwTrigPwrInfo(data) do \
{ \
    *data = Task_10ms_SwtP_MainSwTrigPwrInfo; \
}while(0);

#define Task_10ms_Read_SwtP_MainPdf5msRawInfo(data) do \
{ \
    *data = Task_10ms_SwtP_MainPdf5msRawInfo; \
}while(0);

#define Task_10ms_Read_SwtP_MainSwtMonInfo(data) do \
{ \
    *data = Task_10ms_SwtP_MainSwtMonInfo; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemSuspectData(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemSuspectData; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyDrvInfo(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyDrvInfo; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyMonInfo(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyMonInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemCtrlInhibitData(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemCtrlInhibitData; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxPdt5msRawInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxPdt5msRawInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxPdf5msRawInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxPdf5msRawInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxTarRgnBrkTqInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxEscSwtStInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxEscSwtStInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlSpdInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlSpdInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlEdgeCntInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlEdgeCntInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxPistP5msRawInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxPistP5msRawInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxLogicEepDataInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxLogicEepDataInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData; \
}while(0);

#define Task_10ms_Read_Diag_HndlrWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_Diag_HndlrWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_Diag_HndlrWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_Diag_HndlrWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_Diag_HndlrWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_Diag_HndlrWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_Diag_HndlrWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_Diag_HndlrWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk(data) do \
{ \
    *data = Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk; \
}while(0);

#define Task_10ms_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd(data) do \
{ \
    *data = Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd; \
}while(0);

#define Task_10ms_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk(data) do \
{ \
    *data = Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk; \
}while(0);

#define Task_10ms_Read_Diag_HndlrIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd(data) do \
{ \
    *data = Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxRegenInfo_HcuRegenEna(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxRegenInfo.HcuRegenEna; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxRegenInfo_HcuRegenBrkTq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxRegenInfo.HcuRegenBrkTq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxAccelPedlInfo.AccelPedlValErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_EngMsgFault(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.EngMsgFault; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_TarGearPosi(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.TarGearPosi; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_GearSelDisp(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.GearSelDisp; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_TcuSwiGs(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.TcuSwiGs; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_HcuServiceMod(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.HcuServiceMod; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_SubCanBusSigFault(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.SubCanBusSigFault; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_CoolantTemp(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.CoolantTemp; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxIdbInfo_CoolantTempErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxIdbInfo.CoolantTempErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEngTempInfo_EngTemp(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEngTempInfo.EngTemp; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEngTempInfo_EngTempErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEngTempInfo.EngTempErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_Ax(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.Ax; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_YawRate(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.YawRate; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.SteeringAngle; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_Ay(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.Ay; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_PbSwt(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.PbSwt; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_ClutchSwt(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.ClutchSwt; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_GearRSwt(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.GearRSwt; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngActIndTq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngActIndTq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngRpm(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngRpm; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngIndTq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngIndTq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngFrictionLossTq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngFrictionLossTq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngStdTq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngStdTq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TurbineRpm(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TurbineRpm; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_ThrottleAngle(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.ThrottleAngle; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TpsResol1000(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TpsResol1000; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_PvAvCanResol1000(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.PvAvCanResol1000; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngChr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngChr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngVol(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngVol; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_GearType(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.GearType; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngClutchState(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngClutchState; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngTqCmdBeforeIntv(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngTqCmdBeforeIntv; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_MotEstTq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.MotEstTq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_MotTqCmdBeforeIntv(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.MotTqCmdBeforeIntv; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TqIntvTCU(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TqIntvTCU; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TqIntvSlowTCU(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TqIntvSlowTCU; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TqIncReq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TqIncReq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_DecelReq(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.DecelReq; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_RainSnsStat(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.RainSnsStat; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_EngSpdErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.EngSpdErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_AtType(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.AtType; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_MtType(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.MtType; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_CvtType(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.CvtType; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_DctType(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.DctType; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_HevAtTcu(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.HevAtTcu; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TurbineRpmErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TurbineRpmErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_ThrottleAngleErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.ThrottleAngleErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TopTrvlCltchSwtAct(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TopTrvlCltchSwtAct; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TopTrvlCltchSwtActV(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TopTrvlCltchSwtActV; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtAct(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.HillDesCtrlMdSwtAct; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_HillDesCtrlMdSwtActV(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.HillDesCtrlMdSwtActV; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_WiperIntSW(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.WiperIntSW; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_WiperLow(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.WiperLow; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_WiperHigh(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.WiperHigh; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_WiperValid(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.WiperValid; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_WiperAuto(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.WiperAuto; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEscInfo_TcuFaultSts(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEscInfo.TcuFaultSts; \
}while(0);

#define Task_10ms_Read_CanM_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_CanM_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_CanM_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_CanM_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_CanM_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_CanM_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_CanM_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_CanM_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_YawRateInvld(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.YawRateInvld; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_AyInvld(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.AyInvld; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_SteeringAngleRxOk(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.SteeringAngleRxOk; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_AxInvldData(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.AxInvldData; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Ems1RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Ems1RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Ems2RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Ems2RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Tcu1RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Tcu1RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Tcu5RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Tcu5RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Hcu1RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Hcu1RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Hcu2RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Hcu2RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxEemInfo_Hcu3RxErr(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxEemInfo.Hcu3RxErr; \
}while(0);

#define Task_10ms_Read_CanM_MainCanTimeOutStInfo_MaiCanSusDet(data) do \
{ \
    *data = Task_10ms_CanM_MainCanTimeOutStInfo.MaiCanSusDet; \
}while(0);

#define Task_10ms_Read_CanM_MainCanTimeOutStInfo_EmsTiOutSusDet(data) do \
{ \
    *data = Task_10ms_CanM_MainCanTimeOutStInfo.EmsTiOutSusDet; \
}while(0);

#define Task_10ms_Read_CanM_MainCanTimeOutStInfo_TcuTiOutSusDet(data) do \
{ \
    *data = Task_10ms_CanM_MainCanTimeOutStInfo.TcuTiOutSusDet; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxInfo_MainBusOffFlag(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxInfo.MainBusOffFlag; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxInfo_SenBusOffFlag(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxInfo.SenBusOffFlag; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Bms1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Bms1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Tcu6MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Tcu6MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Tcu5MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Tcu5MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Tcu1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Tcu1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_SasMsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.SasMsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Mcu2MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Mcu2MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Mcu1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Mcu1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Hcu5MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Hcu5MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Hcu3MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Hcu3MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Hcu2MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Hcu2MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Hcu1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Hcu1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Fatc1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Fatc1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Ems3MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Ems3MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Ems2MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Ems2MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Ems1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Ems1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Clu2MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Clu2MsgOkFlg; \
}while(0);

#define Task_10ms_Read_CanM_MainRxMsgOkFlgInfo_Clu1MsgOkFlg(data) do \
{ \
    *data = Task_10ms_CanM_MainRxMsgOkFlgInfo.Clu1MsgOkFlg; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define Task_10ms_Read_YawM_MainCanRxEscInfo_YawRate(data) do \
{ \
    *data = Task_10ms_YawM_MainCanRxEscInfo.YawRate; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.YawRateValidData; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Gyro_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Raster_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Eep_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Asic_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Accel_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Ram_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Rom_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Ad_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Osc_Fail; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.RollingCounter; \
}while(0);

#define Task_10ms_Read_YawM_MainRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = Task_10ms_YawM_MainRxYawAccInfo.Can_Func_Err; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo.LongRollingCounter; \
}while(0);

#define Task_10ms_Read_YawM_MainRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = Task_10ms_YawM_MainRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define Task_10ms_Read_YawM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = Task_10ms_YawM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define Task_10ms_Read_YawM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_YawM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_YawM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_YawM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_YawM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_YawM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_YawM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_YawM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_YawM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_AyM_MainCanRxEscInfo_Ay(data) do \
{ \
    *data = Task_10ms_AyM_MainCanRxEscInfo.Ay; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.YawRateValidData; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Gyro_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Raster_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Eep_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Asic_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Accel_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Ram_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Rom_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Ad_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Osc_Fail; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.RollingCounter; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.Can_Func_Err; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_LatAccValidData(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.LatAccValidData; \
}while(0);

#define Task_10ms_Read_AyM_MainRxYawAccInfo_LatAccSelfTestStatus(data) do \
{ \
    *data = Task_10ms_AyM_MainRxYawAccInfo.LatAccSelfTestStatus; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo.LongRollingCounter; \
}while(0);

#define Task_10ms_Read_AyM_MainRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = Task_10ms_AyM_MainRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define Task_10ms_Read_AyM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = Task_10ms_AyM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define Task_10ms_Read_AyM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_AyM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_AyM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_AyM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_AyM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_AyM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_AyM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_AyM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_AyM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_AxM_MainCanRxEscInfo_Ax(data) do \
{ \
    *data = Task_10ms_AxM_MainCanRxEscInfo.Ax; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.YawRateValidData; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Gyro_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Raster_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Eep_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Asic_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Accel_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Ram_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Rom_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Ad_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Osc_Fail; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.RollingCounter; \
}while(0);

#define Task_10ms_Read_AxM_MainRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = Task_10ms_AxM_MainRxYawAccInfo.Can_Func_Err; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.LongRollingCounter; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_LongAccInvalidData(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.LongAccInvalidData; \
}while(0);

#define Task_10ms_Read_AxM_MainRxLongAccInfo_LongAccSelfTstStatus(data) do \
{ \
    *data = Task_10ms_AxM_MainRxLongAccInfo.LongAccSelfTstStatus; \
}while(0);

#define Task_10ms_Read_AxM_MainRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = Task_10ms_AxM_MainRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define Task_10ms_Read_AxM_MainRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_AxM_MainRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_AxM_MainRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = Task_10ms_AxM_MainRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_AxM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_AxM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_AxM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_AxM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_AxM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_SasM_MainCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = Task_10ms_SasM_MainCanRxEscInfo.SteeringAngle; \
}while(0);

#define Task_10ms_Read_SasM_MainCanRxInfo_MainBusOffFlag(data) do \
{ \
    *data = Task_10ms_SasM_MainCanRxInfo.MainBusOffFlag; \
}while(0);

#define Task_10ms_Read_SasM_MainRxSasInfo_Angle(data) do \
{ \
    *data = Task_10ms_SasM_MainRxSasInfo.Angle; \
}while(0);

#define Task_10ms_Read_SasM_MainRxSasInfo_Speed(data) do \
{ \
    *data = Task_10ms_SasM_MainRxSasInfo.Speed; \
}while(0);

#define Task_10ms_Read_SasM_MainRxSasInfo_Ok(data) do \
{ \
    *data = Task_10ms_SasM_MainRxSasInfo.Ok; \
}while(0);

#define Task_10ms_Read_SasM_MainRxSasInfo_Trim(data) do \
{ \
    *data = Task_10ms_SasM_MainRxSasInfo.Trim; \
}while(0);

#define Task_10ms_Read_SasM_MainRxSasInfo_CheckSum(data) do \
{ \
    *data = Task_10ms_SasM_MainRxSasInfo.CheckSum; \
}while(0);

#define Task_10ms_Read_SasM_MainRxMsgOkFlgInfo_SasMsgOkFlg(data) do \
{ \
    *data = Task_10ms_SasM_MainRxMsgOkFlgInfo.SasMsgOkFlg; \
}while(0);

#define Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_SasM_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_SasM_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_SasM_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_SasM_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_SasM_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_Ay; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_Str; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_YawP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_YawP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_YawP_MainCanRxEscInfo_YawRate(data) do \
{ \
    *data = Task_10ms_YawP_MainCanRxEscInfo.YawRate; \
}while(0);

#define Task_10ms_Read_YawP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Task_10ms_YawP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Task_10ms_Read_YawP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_YawP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_YawP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_YawP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_YawP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_YawP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_YawP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_YawP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_YawP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = Task_10ms_YawP_MainBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define Task_10ms_Read_YawP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_YawP_MainEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_YawP_MainEemEceData_Eem_Ece_Yaw(data) do \
{ \
    *data = Task_10ms_YawP_MainEemEceData.Eem_Ece_Yaw; \
}while(0);

#define Task_10ms_Read_YawP_MainIMUCalcInfo_Reverse_Gear_flg(data) do \
{ \
    *data = Task_10ms_YawP_MainIMUCalcInfo.Reverse_Gear_flg; \
}while(0);

#define Task_10ms_Read_YawP_MainIMUCalcInfo_Reverse_Judge_Time(data) do \
{ \
    *data = Task_10ms_YawP_MainIMUCalcInfo.Reverse_Judge_Time; \
}while(0);

#define Task_10ms_Read_YawP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = Task_10ms_YawP_MainWssSpeedOut.WssMax; \
}while(0);

#define Task_10ms_Read_YawP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = Task_10ms_YawP_MainWssSpeedOut.WssMin; \
}while(0);

#define Task_10ms_Read_YawP_MainWssCalcInfo_Rough_Sus_Flg(data) do \
{ \
    *data = Task_10ms_YawP_MainWssCalcInfo.Rough_Sus_Flg; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_Ay; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_Str; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_AyP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_AyP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_AyP_MainCanRxEscInfo_Ay(data) do \
{ \
    *data = Task_10ms_AyP_MainCanRxEscInfo.Ay; \
}while(0);

#define Task_10ms_Read_AyP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Task_10ms_AyP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Task_10ms_Read_AyP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_AyP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_AyP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_AyP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_AyP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_AyP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_AyP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_AyP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_AyP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = Task_10ms_AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define Task_10ms_Read_AyP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_AyP_MainEemEceData_Eem_Ece_Ay(data) do \
{ \
    *data = Task_10ms_AyP_MainEemEceData.Eem_Ece_Ay; \
}while(0);

#define Task_10ms_Read_AyP_MainIMUCalcInfo_Reverse_Gear_flg(data) do \
{ \
    *data = Task_10ms_AyP_MainIMUCalcInfo.Reverse_Gear_flg; \
}while(0);

#define Task_10ms_Read_AyP_MainIMUCalcInfo_Reverse_Judge_Time(data) do \
{ \
    *data = Task_10ms_AyP_MainIMUCalcInfo.Reverse_Judge_Time; \
}while(0);

#define Task_10ms_Read_AyP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = Task_10ms_AyP_MainWssSpeedOut.WssMax; \
}while(0);

#define Task_10ms_Read_AyP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = Task_10ms_AyP_MainWssSpeedOut.WssMin; \
}while(0);

#define Task_10ms_Read_AyP_MainWssCalcInfo_Rough_Sus_Flg(data) do \
{ \
    *data = Task_10ms_AyP_MainWssCalcInfo.Rough_Sus_Flg; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_Ax(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_Ax; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_AxP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_AxP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_AxP_MainCanRxEscInfo_Ax(data) do \
{ \
    *data = Task_10ms_AxP_MainCanRxEscInfo.Ax; \
}while(0);

#define Task_10ms_Read_AxP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Task_10ms_AxP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Task_10ms_Read_AxP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_AxP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_AxP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_AxP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_AxP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_AxP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_AxP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_AxP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_AxP_MainAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Task_10ms_AxP_MainAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Task_10ms_Read_AxP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = Task_10ms_AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_5V(data) do \
{ \
    *data = Task_10ms_AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V; \
}while(0);

#define Task_10ms_Read_AxP_MainEemEceData_Eem_Ece_Ax(data) do \
{ \
    *data = Task_10ms_AxP_MainEemEceData.Eem_Ece_Ax; \
}while(0);

#define Task_10ms_Read_AxP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = Task_10ms_AxP_MainWssSpeedOut.WssMax; \
}while(0);

#define Task_10ms_Read_AxP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = Task_10ms_AxP_MainWssSpeedOut.WssMin; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_Ay; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_Str; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Task_10ms_Read_SasP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Task_10ms_SasP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Task_10ms_Read_SasP_MainCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = Task_10ms_SasP_MainCanRxEscInfo.SteeringAngle; \
}while(0);

#define Task_10ms_Read_SasP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Task_10ms_SasP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Task_10ms_Read_SasP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_SasP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_SasP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_SasP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_SasP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_SasP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_SasP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_SasP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_SasP_MainAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Task_10ms_SasP_MainAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Task_10ms_Read_SasP_MainEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = Task_10ms_SasP_MainEscCtrlInfo.EscActFlg; \
}while(0);

#define Task_10ms_Read_SasP_MainTcsCtrlInfo_TcsActFlg(data) do \
{ \
    *data = Task_10ms_SasP_MainTcsCtrlInfo.TcsActFlg; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define Task_10ms_Read_SasP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = Task_10ms_SasP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define Task_10ms_Read_SasP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = Task_10ms_SasP_MainWssSpeedOut.WssMax; \
}while(0);

#define Task_10ms_Read_SasP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = Task_10ms_SasP_MainWssSpeedOut.WssMin; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_AvhSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.AvhSwtMon; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_BflSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.BflSwtMon; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_BlsSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.BlsSwtMon; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_BsSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.BsSwtMon; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_EscSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.EscSwtMon; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_HdcSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.HdcSwtMon; \
}while(0);

#define Task_10ms_Read_SwtM_MainSwtMonInfo_PbSwtMon(data) do \
{ \
    *data = Task_10ms_SwtM_MainSwtMonInfo.PbSwtMon; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemFailData.Eem_Fail_SimP; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemFailData.Eem_Fail_ParkBrake; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit; \
}while(0);

#define Task_10ms_Read_SwtP_MainSwTrigPwrInfo_Vbatt01Mon(data) do \
{ \
    *data = Task_10ms_SwtP_MainSwTrigPwrInfo.Vbatt01Mon; \
}while(0);

#define Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSig(data) do \
{ \
    *data = Task_10ms_SwtP_MainPdf5msRawInfo.MoveAvrPdfSig; \
}while(0);

#define Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(data) do \
{ \
    *data = Task_10ms_SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset; \
}while(0);

#define Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSig(data) do \
{ \
    *data = Task_10ms_SwtP_MainPdf5msRawInfo.MoveAvrPdtSig; \
}while(0);

#define Task_10ms_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(data) do \
{ \
    *data = Task_10ms_SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset; \
}while(0);

#define Task_10ms_Read_SwtP_MainSwtMonInfo_BlsSwtMon(data) do \
{ \
    *data = Task_10ms_SwtP_MainSwtMonInfo.BlsSwtMon; \
}while(0);

#define Task_10ms_Read_SwtP_MainSwtMonInfo_BsSwtMon(data) do \
{ \
    *data = Task_10ms_SwtP_MainSwtMonInfo.BsSwtMon; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_BS(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemSuspectData.Eem_Suspect_BS; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_BLS(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemSuspectData.Eem_Suspect_BLS; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_ParkBrake(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDT(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT; \
}while(0);

#define Task_10ms_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDF(data) do \
{ \
    *data = Task_10ms_SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyDrvInfo_RlyDbcDrv(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyDrvInfo.RlyDbcDrv; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyDrvInfo_RlyEssDrv(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyDrvInfo.RlyEssDrv; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyMonInfo_RlyDbcMon(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyMonInfo.RlyDbcMon; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyMonInfo_RlyEssMon(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyMonInfo.RlyEssMon; \
}while(0);

#define Task_10ms_Read_RlyM_MainRlyMonInfo_RlyFault(data) do \
{ \
    *data = Task_10ms_RlyM_MainRlyMonInfo.RlyFault; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DegradeModeFail(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TqIntvTCS(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TqIntvTCS; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TqIntvMsr(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TqIntvMsr; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TqIntvSlowTCS(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TqIntvSlowTCS; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_MinGear(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.MinGear; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_MaxGear(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.MaxGear; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TcsReq(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TcsReq; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TcsCtrl(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TcsCtrl; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_AbsAct(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.AbsAct; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TcsGearShiftChr(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TcsGearShiftChr; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_EspCtrl(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.EspCtrl; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_MsrReq(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.MsrReq; \
}while(0);

#define Task_10ms_Read_Proxy_TxCanTxInfo_TcsProductInfo(data) do \
{ \
    *data = Task_10ms_Proxy_TxCanTxInfo.TcsProductInfo; \
}while(0);

#define Task_10ms_Read_Proxy_TxPdt5msRawInfo_PdtSig(data) do \
{ \
    *data = Task_10ms_Proxy_TxPdt5msRawInfo.PdtSig; \
}while(0);

#define Task_10ms_Read_Proxy_TxPdf5msRawInfo_PdfSig(data) do \
{ \
    *data = Task_10ms_Proxy_TxPdf5msRawInfo.PdfSig; \
}while(0);

#define Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_TarRgnBrkTq(data) do \
{ \
    *data = Task_10ms_Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq; \
}while(0);

#define Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_EstTotBrkForce(data) do \
{ \
    *data = Task_10ms_Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce; \
}while(0);

#define Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_EstHydBrkForce(data) do \
{ \
    *data = Task_10ms_Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce; \
}while(0);

#define Task_10ms_Read_Proxy_TxTarRgnBrkTqInfo_EhbStat(data) do \
{ \
    *data = Task_10ms_Proxy_TxTarRgnBrkTqInfo.EhbStat; \
}while(0);

#define Task_10ms_Read_Proxy_TxEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Task_10ms_Proxy_TxEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Task_10ms_Read_Proxy_TxEscSwtStInfo_TcsDisabledBySwt(data) do \
{ \
    *data = Task_10ms_Proxy_TxEscSwtStInfo.TcsDisabledBySwt; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlEdgeCntInfo_FlWhlEdgeCnt(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlEdgeCntInfo_FrWhlEdgeCnt(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlEdgeCntInfo_RlWhlEdgeCnt(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt; \
}while(0);

#define Task_10ms_Read_Proxy_TxWhlEdgeCntInfo_RrWhlEdgeCnt(data) do \
{ \
    *data = Task_10ms_Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt; \
}while(0);

#define Task_10ms_Read_Proxy_TxPistP5msRawInfo_PistPSig(data) do \
{ \
    *data = Task_10ms_Proxy_TxPistP5msRawInfo.PistPSig; \
}while(0);

#define Task_10ms_Read_Proxy_TxLogicEepDataInfo_KPdtOffsEolReadVal(data) do \
{ \
    *data = Task_10ms_Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal; \
}while(0);

#define Task_10ms_Read_Proxy_TxLogicEepDataInfo_ReadInvld(data) do \
{ \
    *data = Task_10ms_Proxy_TxLogicEepDataInfo.ReadInvld; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_EBDLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_ABSLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_TCSLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_TCSOffLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_VDCLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_VDCOffLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_HDCLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_BBSBuzzorRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_RBCSLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_AHBLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_ServiceLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_TCSFuncLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_VDCFuncLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_AVHLampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_AVHILampRequest(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_ACCEnable(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_ACCEnable; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Lamp_CDMEnable(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Lamp_CDMEnable; \
}while(0);

#define Task_10ms_Read_Proxy_TxEemLampData_Eem_Buzzor_On(data) do \
{ \
    *data = Task_10ms_Proxy_TxEemLampData.Eem_Buzzor_On; \
}while(0);

#define Task_10ms_Read_Diag_HndlrEcuModeSts(data) do \
{ \
    *data = Task_10ms_Diag_HndlrEcuModeSts; \
}while(0);

#define Task_10ms_Read_Diag_HndlrIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_Diag_HndlrIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_Diag_HndlrVBatt1Mon(data) do \
{ \
    *data = Task_10ms_Diag_HndlrVBatt1Mon; \
}while(0);

#define Task_10ms_Read_Diag_HndlrVBatt2Mon(data) do \
{ \
    *data = Task_10ms_Diag_HndlrVBatt2Mon; \
}while(0);

#define Task_10ms_Read_Diag_HndlrFspCbsMon(data) do \
{ \
    *data = Task_10ms_Diag_HndlrFspCbsMon; \
}while(0);

#define Task_10ms_Read_Diag_HndlrCEMon(data) do \
{ \
    *data = Task_10ms_Diag_HndlrCEMon; \
}while(0);

#define Task_10ms_Read_Diag_HndlrVddMon(data) do \
{ \
    *data = Task_10ms_Diag_HndlrVddMon; \
}while(0);

#define Task_10ms_Read_Diag_HndlrCspMon(data) do \
{ \
    *data = Task_10ms_Diag_HndlrCspMon; \
}while(0);

#define Task_10ms_Read_Diag_HndlrFuncInhibitDiagSts(data) do \
{ \
    *data = Task_10ms_Diag_HndlrFuncInhibitDiagSts; \
}while(0);

#define Task_10ms_Read_Diag_HndlrMtrDiagState(data) do \
{ \
    *data = Task_10ms_Diag_HndlrMtrDiagState; \
}while(0);

#define Task_10ms_Read_CanM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_CanM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_CanM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_CanM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_CanM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_CanM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_CanM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_CanM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_CanM_MainArbVlvDriveState(data) do \
{ \
    *data = Task_10ms_CanM_MainArbVlvDriveState; \
}while(0);

#define Task_10ms_Read_CanM_MainCEMon(data) do \
{ \
    *data = Task_10ms_CanM_MainCEMon; \
}while(0);

#define Task_10ms_Read_CanM_MainCanRxGearSelDispErrInfo(data) do \
{ \
    *data = Task_10ms_CanM_MainCanRxGearSelDispErrInfo; \
}while(0);

#define Task_10ms_Read_CanM_MainMtrArbDriveState(data) do \
{ \
    *data = Task_10ms_CanM_MainMtrArbDriveState; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainArbVlvDriveState(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainArbVlvDriveState; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainExt5vMon(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainExt5vMon; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainCspMon(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainCspMon; \
}while(0);

#define Task_10ms_Read_SenPwrM_MainMtrArbDriveState(data) do \
{ \
    *data = Task_10ms_SenPwrM_MainMtrArbDriveState; \
}while(0);

#define Task_10ms_Read_YawM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_YawM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_YawM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_YawM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_YawM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_YawM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_YawM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_YawM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_AyM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_AyM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_AyM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_AyM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_AyM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_AyM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_AyM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_AyM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_AxM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_AxM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_AxM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_AxM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_AxM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_AxM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_AxM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_AxM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_SasM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_SasM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_SasM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_SasM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_SasM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_SasM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_SasM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_SasM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_YawP_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_YawP_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_YawP_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_YawP_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_YawP_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_YawP_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_YawP_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_YawP_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_YawP_MainVehSpd(data) do \
{ \
    *data = Task_10ms_YawP_MainVehSpd; \
}while(0);

#define Task_10ms_Read_AyP_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_AyP_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_AyP_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_AyP_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_AyP_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_AyP_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_AyP_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_AyP_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_AyP_MainVehSpd(data) do \
{ \
    *data = Task_10ms_AyP_MainVehSpd; \
}while(0);

#define Task_10ms_Read_AxP_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_AxP_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_AxP_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_AxP_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_AxP_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_AxP_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_AxP_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_AxP_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_AxP_MainVehSpd(data) do \
{ \
    *data = Task_10ms_AxP_MainVehSpd; \
}while(0);

#define Task_10ms_Read_SasP_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_SasP_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_SasP_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_SasP_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_SasP_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_SasP_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_SasP_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_SasP_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_SasP_MainVehSpd(data) do \
{ \
    *data = Task_10ms_SasP_MainVehSpd; \
}while(0);

#define Task_10ms_Read_SwtM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_SwtM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_SwtM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_SwtM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_SwtM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_SwtM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_SwtM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_SwtM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_SwtP_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_SwtP_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_SwtP_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_SwtP_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_SwtP_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_SwtP_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_SwtP_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_SwtP_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_SwtP_MainVehSpdFild(data) do \
{ \
    *data = Task_10ms_SwtP_MainVehSpdFild; \
}while(0);

#define Task_10ms_Read_RlyM_MainEcuModeSts(data) do \
{ \
    *data = Task_10ms_RlyM_MainEcuModeSts; \
}while(0);

#define Task_10ms_Read_RlyM_MainIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_RlyM_MainIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_RlyM_MainIgnEdgeSts(data) do \
{ \
    *data = Task_10ms_RlyM_MainIgnEdgeSts; \
}while(0);

#define Task_10ms_Read_RlyM_MainVBatt1Mon(data) do \
{ \
    *data = Task_10ms_RlyM_MainVBatt1Mon; \
}while(0);

#define Task_10ms_Read_Proxy_TxEcuModeSts(data) do \
{ \
    *data = Task_10ms_Proxy_TxEcuModeSts; \
}while(0);

#define Task_10ms_Read_Proxy_TxIgnOnOffSts(data) do \
{ \
    *data = Task_10ms_Proxy_TxIgnOnOffSts; \
}while(0);

#define Task_10ms_Read_Proxy_TxFuncInhibitProxySts(data) do \
{ \
    *data = Task_10ms_Proxy_TxFuncInhibitProxySts; \
}while(0);


/* Set Output DE MAcro Function */
#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData(data) do \
{ \
    Task_10ms_CanM_MainCanMonData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SolenoidDrvDuty = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.FrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RlIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrOvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RrIvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PrimCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SecdCutVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.SimVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.ResPVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.BalVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.CircVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.PressDumpReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    Task_10ms_Diag_HndlrDiagVlvActrInfo.RelsVlvReqData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhUData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhVData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotPwmPhWData = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrMotReqDataDiagInfo_MotReq(data) do \
{ \
    Task_10ms_Diag_HndlrMotReqDataDiagInfo.MotReq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    Task_10ms_Diag_HndlrSasCalInfo.DiagSasCaltoAppl = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Order(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Order = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Iq(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Iq = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_Id(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_Id = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_U_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_U_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_V_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_V_PWM = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagHndlRequest_DiagRequest_W_PWM(data) do \
{ \
    Task_10ms_Diag_HndlrDiagHndlRequest.DiagRequest_W_PWM = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubBusOff_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_MainOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_MainOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_SubOverRun_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_SubOverRun_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU5_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU5_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_FWD1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_FWD1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU1_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU1_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_HCU3_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_HCU3_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_VSM2_Tout_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_VSM2_Tout_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_EMS_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_EMS_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_CanM_MainCanMonData_CanM_TCU_Invalid_Err(data) do \
{ \
    Task_10ms_CanM_MainCanMonData.CanM_TCU_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    Task_10ms_SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialNumOK_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    Task_10ms_YawM_MainYAWMSerialInfo.YAWM_SerialUnMatch_Flg = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Timeout_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Invalid_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_CRC_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Rolling_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_YawM_MainYAWMOutInfo_YAWM_Range_Err(data) do \
{ \
    Task_10ms_YawM_MainYAWMOutInfo.YAWM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Timeout_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Invalid_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_CRC_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Rolling_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Temperature_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AyM_MainAYMOuitInfo_AYM_Range_Err(data) do \
{ \
    Task_10ms_AyM_MainAYMOuitInfo.AYM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Timeout_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Invalid_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_CRC_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Rolling_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Temperature_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Temperature_Err = *data; \
}while(0);

#define Task_10ms_Write_AxM_MainAXMOutInfo_AXM_Range_Err(data) do \
{ \
    Task_10ms_AxM_MainAXMOutInfo.AXM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Timeout_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Timeout_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Invalid_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Invalid_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_CRC_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_CRC_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Rolling_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Rolling_Err = *data; \
}while(0);

#define Task_10ms_Write_SasM_MainSASMOutInfo_SASM_Range_Err(data) do \
{ \
    Task_10ms_SasM_MainSASMOutInfo.SASM_Range_Err = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauModelErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauNoisekErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauNoisekErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauShockErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauRangeErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_YawP_MainYawPlauOutput_YawPlauStandStillErr(data) do \
{ \
    Task_10ms_YawP_MainYawPlauOutput.YawPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define Task_10ms_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    Task_10ms_AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    Task_10ms_AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define Task_10ms_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    Task_10ms_SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    Task_10ms_SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define Task_10ms_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    Task_10ms_SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define Task_10ms_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    Task_10ms_RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Task_10ms_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Task_10ms_Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagClrSrs(data) do \
{ \
    Task_10ms_Diag_HndlrDiagClrSrs = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagSci(data) do \
{ \
    Task_10ms_Diag_HndlrDiagSci = *data; \
}while(0);

#define Task_10ms_Write_Diag_HndlrDiagAhbSci(data) do \
{ \
    Task_10ms_Diag_HndlrDiagAhbSci = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_10MS_IFA_H_ */
/** @} */
