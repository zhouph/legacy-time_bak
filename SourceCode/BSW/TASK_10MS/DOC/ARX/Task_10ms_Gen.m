Diag_HndlrWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Diag_HndlrWhlSpdInfo = CreateBus(Diag_HndlrWhlSpdInfo, DeList);
clear DeList;

Diag_HndlrIdbSnsrEolOfsCalcInfo = Simulink.Bus;
DeList={
    'PedalEolOfsCalcOk'
    'PedalEolOfsCalcEnd'
    'PressEolOfsCalcOk'
    'PressEolOfsCalcEnd'
    };
Diag_HndlrIdbSnsrEolOfsCalcInfo = CreateBus(Diag_HndlrIdbSnsrEolOfsCalcInfo, DeList);
clear DeList;

CanM_MainCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
CanM_MainCanRxRegenInfo = CreateBus(CanM_MainCanRxRegenInfo, DeList);
clear DeList;

CanM_MainCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
CanM_MainCanRxAccelPedlInfo = CreateBus(CanM_MainCanRxAccelPedlInfo, DeList);
clear DeList;

CanM_MainCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
CanM_MainCanRxIdbInfo = CreateBus(CanM_MainCanRxIdbInfo, DeList);
clear DeList;

CanM_MainCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
CanM_MainCanRxEngTempInfo = CreateBus(CanM_MainCanRxEngTempInfo, DeList);
clear DeList;

CanM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
CanM_MainCanRxEscInfo = CreateBus(CanM_MainCanRxEscInfo, DeList);
clear DeList;

CanM_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
CanM_MainWhlSpdInfo = CreateBus(CanM_MainWhlSpdInfo, DeList);
clear DeList;

CanM_MainCanRxEemInfo = Simulink.Bus;
DeList={
    'YawRateInvld'
    'AyInvld'
    'SteeringAngleRxOk'
    'AxInvldData'
    'Ems1RxErr'
    'Ems2RxErr'
    'Tcu1RxErr'
    'Tcu5RxErr'
    'Hcu1RxErr'
    'Hcu2RxErr'
    'Hcu3RxErr'
    };
CanM_MainCanRxEemInfo = CreateBus(CanM_MainCanRxEemInfo, DeList);
clear DeList;

CanM_MainCanTimeOutStInfo = Simulink.Bus;
DeList={
    'MaiCanSusDet'
    'EmsTiOutSusDet'
    'TcuTiOutSusDet'
    };
CanM_MainCanTimeOutStInfo = CreateBus(CanM_MainCanTimeOutStInfo, DeList);
clear DeList;

CanM_MainCanRxInfo = Simulink.Bus;
DeList={
    'MainBusOffFlag'
    'SenBusOffFlag'
    };
CanM_MainCanRxInfo = CreateBus(CanM_MainCanRxInfo, DeList);
clear DeList;

CanM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
CanM_MainRxMsgOkFlgInfo = CreateBus(CanM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

SenPwrM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
SenPwrM_MainEemFailData = CreateBus(SenPwrM_MainEemFailData, DeList);
clear DeList;

YawM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
YawM_MainCanRxEscInfo = CreateBus(YawM_MainCanRxEscInfo, DeList);
clear DeList;

YawM_MainRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
YawM_MainRxYawSerialInfo = CreateBus(YawM_MainRxYawSerialInfo, DeList);
clear DeList;

YawM_MainRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
YawM_MainRxYawAccInfo = CreateBus(YawM_MainRxYawAccInfo, DeList);
clear DeList;

YawM_MainRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
YawM_MainRxLongAccInfo = CreateBus(YawM_MainRxLongAccInfo, DeList);
clear DeList;

YawM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
YawM_MainRxMsgOkFlgInfo = CreateBus(YawM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

YawM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
YawM_MainEemFailData = CreateBus(YawM_MainEemFailData, DeList);
clear DeList;

AyM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
AyM_MainCanRxEscInfo = CreateBus(AyM_MainCanRxEscInfo, DeList);
clear DeList;

AyM_MainRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
AyM_MainRxYawSerialInfo = CreateBus(AyM_MainRxYawSerialInfo, DeList);
clear DeList;

AyM_MainRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
AyM_MainRxYawAccInfo = CreateBus(AyM_MainRxYawAccInfo, DeList);
clear DeList;

AyM_MainRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
AyM_MainRxLongAccInfo = CreateBus(AyM_MainRxLongAccInfo, DeList);
clear DeList;

AyM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
AyM_MainRxMsgOkFlgInfo = CreateBus(AyM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

AyM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
AyM_MainEemFailData = CreateBus(AyM_MainEemFailData, DeList);
clear DeList;

AxM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
AxM_MainCanRxEscInfo = CreateBus(AxM_MainCanRxEscInfo, DeList);
clear DeList;

AxM_MainRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
AxM_MainRxYawSerialInfo = CreateBus(AxM_MainRxYawSerialInfo, DeList);
clear DeList;

AxM_MainRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
AxM_MainRxYawAccInfo = CreateBus(AxM_MainRxYawAccInfo, DeList);
clear DeList;

AxM_MainRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
AxM_MainRxLongAccInfo = CreateBus(AxM_MainRxLongAccInfo, DeList);
clear DeList;

AxM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
AxM_MainRxMsgOkFlgInfo = CreateBus(AxM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

AxM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
AxM_MainEemFailData = CreateBus(AxM_MainEemFailData, DeList);
clear DeList;

SasM_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
SasM_MainCanRxEscInfo = CreateBus(SasM_MainCanRxEscInfo, DeList);
clear DeList;

SasM_MainCanRxInfo = Simulink.Bus;
DeList={
    'MainBusOffFlag'
    'SenBusOffFlag'
    };
SasM_MainCanRxInfo = CreateBus(SasM_MainCanRxInfo, DeList);
clear DeList;

SasM_MainRxSasInfo = Simulink.Bus;
DeList={
    'Angle'
    'Speed'
    'Ok'
    'Cal'
    'Trim'
    'CheckSum'
    'MsgCount'
    };
SasM_MainRxSasInfo = CreateBus(SasM_MainRxSasInfo, DeList);
clear DeList;

SasM_MainRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
SasM_MainRxMsgOkFlgInfo = CreateBus(SasM_MainRxMsgOkFlgInfo, DeList);
clear DeList;

SasM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
SasM_MainEemFailData = CreateBus(SasM_MainEemFailData, DeList);
clear DeList;

YawP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
YawP_MainEemFailData = CreateBus(YawP_MainEemFailData, DeList);
clear DeList;

YawP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
YawP_MainCanRxEscInfo = CreateBus(YawP_MainCanRxEscInfo, DeList);
clear DeList;

YawP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
YawP_MainEscSwtStInfo = CreateBus(YawP_MainEscSwtStInfo, DeList);
clear DeList;

YawP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
YawP_MainWhlSpdInfo = CreateBus(YawP_MainWhlSpdInfo, DeList);
clear DeList;

YawP_MainBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'LowSpdCtrlInhibitFlg'
    'StopdVehCtrlModFlg'
    'VehStandStillStFlg'
    };
YawP_MainBaseBrkCtrlModInfo = CreateBus(YawP_MainBaseBrkCtrlModInfo, DeList);
clear DeList;

YawP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
YawP_MainEemSuspectData = CreateBus(YawP_MainEemSuspectData, DeList);
clear DeList;

YawP_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
YawP_MainEemEceData = CreateBus(YawP_MainEemEceData, DeList);
clear DeList;

YawP_MainIMUCalcInfo = Simulink.Bus;
DeList={
    'Reverse_Gear_flg'
    'Reverse_Judge_Time'
    };
YawP_MainIMUCalcInfo = CreateBus(YawP_MainIMUCalcInfo, DeList);
clear DeList;

YawP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
YawP_MainWssSpeedOut = CreateBus(YawP_MainWssSpeedOut, DeList);
clear DeList;

YawP_MainWssCalcInfo = Simulink.Bus;
DeList={
    'Rough_Sus_Flg'
    };
YawP_MainWssCalcInfo = CreateBus(YawP_MainWssCalcInfo, DeList);
clear DeList;

AyP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
AyP_MainEemFailData = CreateBus(AyP_MainEemFailData, DeList);
clear DeList;

AyP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
AyP_MainCanRxEscInfo = CreateBus(AyP_MainCanRxEscInfo, DeList);
clear DeList;

AyP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
AyP_MainEscSwtStInfo = CreateBus(AyP_MainEscSwtStInfo, DeList);
clear DeList;

AyP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
AyP_MainWhlSpdInfo = CreateBus(AyP_MainWhlSpdInfo, DeList);
clear DeList;

AyP_MainBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'LowSpdCtrlInhibitFlg'
    'StopdVehCtrlModFlg'
    'VehStandStillStFlg'
    };
AyP_MainBaseBrkCtrlModInfo = CreateBus(AyP_MainBaseBrkCtrlModInfo, DeList);
clear DeList;

AyP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
AyP_MainEemSuspectData = CreateBus(AyP_MainEemSuspectData, DeList);
clear DeList;

AyP_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
AyP_MainEemEceData = CreateBus(AyP_MainEemEceData, DeList);
clear DeList;

AyP_MainIMUCalcInfo = Simulink.Bus;
DeList={
    'Reverse_Gear_flg'
    'Reverse_Judge_Time'
    };
AyP_MainIMUCalcInfo = CreateBus(AyP_MainIMUCalcInfo, DeList);
clear DeList;

AyP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
AyP_MainWssSpeedOut = CreateBus(AyP_MainWssSpeedOut, DeList);
clear DeList;

AyP_MainWssCalcInfo = Simulink.Bus;
DeList={
    'Rough_Sus_Flg'
    };
AyP_MainWssCalcInfo = CreateBus(AyP_MainWssCalcInfo, DeList);
clear DeList;

AxP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
AxP_MainEemFailData = CreateBus(AxP_MainEemFailData, DeList);
clear DeList;

AxP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
AxP_MainCanRxEscInfo = CreateBus(AxP_MainCanRxEscInfo, DeList);
clear DeList;

AxP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
AxP_MainEscSwtStInfo = CreateBus(AxP_MainEscSwtStInfo, DeList);
clear DeList;

AxP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
AxP_MainWhlSpdInfo = CreateBus(AxP_MainWhlSpdInfo, DeList);
clear DeList;

AxP_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
AxP_MainAbsCtrlInfo = CreateBus(AxP_MainAbsCtrlInfo, DeList);
clear DeList;

AxP_MainBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'LowSpdCtrlInhibitFlg'
    'StopdVehCtrlModFlg'
    'VehStandStillStFlg'
    };
AxP_MainBaseBrkCtrlModInfo = CreateBus(AxP_MainBaseBrkCtrlModInfo, DeList);
clear DeList;

AxP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
AxP_MainEemSuspectData = CreateBus(AxP_MainEemSuspectData, DeList);
clear DeList;

AxP_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
AxP_MainEemEceData = CreateBus(AxP_MainEemEceData, DeList);
clear DeList;

AxP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
AxP_MainWssSpeedOut = CreateBus(AxP_MainWssSpeedOut, DeList);
clear DeList;

SasP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
SasP_MainEemFailData = CreateBus(SasP_MainEemFailData, DeList);
clear DeList;

SasP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
SasP_MainCanRxEscInfo = CreateBus(SasP_MainCanRxEscInfo, DeList);
clear DeList;

SasP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
SasP_MainEscSwtStInfo = CreateBus(SasP_MainEscSwtStInfo, DeList);
clear DeList;

SasP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
SasP_MainWhlSpdInfo = CreateBus(SasP_MainWhlSpdInfo, DeList);
clear DeList;

SasP_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
SasP_MainAbsCtrlInfo = CreateBus(SasP_MainAbsCtrlInfo, DeList);
clear DeList;

SasP_MainEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    'EscCtrlModeFrntLe'
    'EscCtrlModeFrntRi'
    'EscCtrlModeReLe'
    'EscCtrlModeReRi'
    'EscTarPRateFrntLe'
    'EscTarPRateFrntRi'
    'EscTarPRateReLe'
    'EscTarPRateReRi'
    'EscPrioFrntLe'
    'EscPrioFrntRi'
    'EscPrioReLe'
    'EscPrioReRi'
    'EscPreCtrlModeFrntLe'
    'EscPreCtrlModeFrntRi'
    'EscPreCtrlModeReLe'
    'EscPreCtrlModeReRi'
    'EscDelTarPFrntLe'
    'EscDelTarPFrntRi'
    'EscDelTarPReLe'
    'EscDelTarPReRi'
    };
SasP_MainEscCtrlInfo = CreateBus(SasP_MainEscCtrlInfo, DeList);
clear DeList;

SasP_MainTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    'BrkCtrlFctFrntLeByWspc'
    'BrkCtrlFctFrntRiByWspc'
    'BrkCtrlFctReLeByWspc'
    'BrkCtrlFctReRiByWspc'
    'BrkTqGrdtReqFrntLeByWspc'
    'BrkTqGrdtReqFrntRiByWspc'
    'BrkTqGrdtReqReLeByWspc'
    'BrkTqGrdtReqReRiByWspc'
    'TcsDelTarPFrntLe'
    'TcsDelTarPFrntRi'
    'TcsDelTarPReLe'
    'TcsDelTarPReRi'
    };
SasP_MainTcsCtrlInfo = CreateBus(SasP_MainTcsCtrlInfo, DeList);
clear DeList;

SasP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
SasP_MainEemSuspectData = CreateBus(SasP_MainEemSuspectData, DeList);
clear DeList;

SasP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
SasP_MainWssSpeedOut = CreateBus(SasP_MainWssSpeedOut, DeList);
clear DeList;

SwtM_MainSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BflSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'DoorSwtMon'
    'EscSwtMon'
    'FlexBrkASwtMon'
    'FlexBrkBSwtMon'
    'HzrdSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
SwtM_MainSwtMonInfo = CreateBus(SwtM_MainSwtMonInfo, DeList);
clear DeList;

SwtP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
SwtP_MainEemFailData = CreateBus(SwtP_MainEemFailData, DeList);
clear DeList;

SwtP_MainEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
SwtP_MainEemCtrlInhibitData = CreateBus(SwtP_MainEemCtrlInhibitData, DeList);
clear DeList;

SwtP_MainSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Ext5vMon'
    'CEMon'
    'Int5vMon'
    'CSPMon'
    'Vbatt01Mon'
    'Vbatt02Mon'
    'VddMon'
    'Vdd3Mon'
    'Vdd5Mon'
    };
SwtP_MainSwTrigPwrInfo = CreateBus(SwtP_MainSwTrigPwrInfo, DeList);
clear DeList;

SwtP_MainPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
SwtP_MainPdf5msRawInfo = CreateBus(SwtP_MainPdf5msRawInfo, DeList);
clear DeList;

SwtP_MainSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BflSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'DoorSwtMon'
    'EscSwtMon'
    'FlexBrkASwtMon'
    'FlexBrkBSwtMon'
    'HzrdSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
SwtP_MainSwtMonInfo = CreateBus(SwtP_MainSwtMonInfo, DeList);
clear DeList;

SwtP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
SwtP_MainEemSuspectData = CreateBus(SwtP_MainEemSuspectData, DeList);
clear DeList;

RlyM_MainRlyDrvInfo = Simulink.Bus;
DeList={
    'RlyDbcDrv'
    'RlyEssDrv'
    };
RlyM_MainRlyDrvInfo = CreateBus(RlyM_MainRlyDrvInfo, DeList);
clear DeList;

RlyM_MainRlyMonInfo = Simulink.Bus;
DeList={
    'RlyDbcMon'
    'RlyEssMon'
    'RlyFault'
    };
RlyM_MainRlyMonInfo = CreateBus(RlyM_MainRlyMonInfo, DeList);
clear DeList;

Proxy_TxEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Proxy_TxEemCtrlInhibitData = CreateBus(Proxy_TxEemCtrlInhibitData, DeList);
clear DeList;

Proxy_TxCanTxInfo = Simulink.Bus;
DeList={
    'TqIntvTCS'
    'TqIntvMsr'
    'TqIntvSlowTCS'
    'MinGear'
    'MaxGear'
    'TcsReq'
    'TcsCtrl'
    'AbsAct'
    'TcsGearShiftChr'
    'EspCtrl'
    'MsrReq'
    'TcsProductInfo'
    };
Proxy_TxCanTxInfo = CreateBus(Proxy_TxCanTxInfo, DeList);
clear DeList;

Proxy_TxPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Proxy_TxPdt5msRawInfo = CreateBus(Proxy_TxPdt5msRawInfo, DeList);
clear DeList;

Proxy_TxPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
Proxy_TxPdf5msRawInfo = CreateBus(Proxy_TxPdf5msRawInfo, DeList);
clear DeList;

Proxy_TxTarRgnBrkTqInfo = Simulink.Bus;
DeList={
    'TarRgnBrkTq'
    'VirtStkDep'
    'VirtStkValid'
    'EstTotBrkForce'
    'EstHydBrkForce'
    'EhbStat'
    };
Proxy_TxTarRgnBrkTqInfo = CreateBus(Proxy_TxTarRgnBrkTqInfo, DeList);
clear DeList;

Proxy_TxEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    'HdcEnabledBySwt'
    };
Proxy_TxEscSwtStInfo = CreateBus(Proxy_TxEscSwtStInfo, DeList);
clear DeList;

Proxy_TxWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Proxy_TxWhlSpdInfo = CreateBus(Proxy_TxWhlSpdInfo, DeList);
clear DeList;

Proxy_TxWhlEdgeCntInfo = Simulink.Bus;
DeList={
    'FlWhlEdgeCnt'
    'FrWhlEdgeCnt'
    'RlWhlEdgeCnt'
    'RrWhlEdgeCnt'
    };
Proxy_TxWhlEdgeCntInfo = CreateBus(Proxy_TxWhlEdgeCntInfo, DeList);
clear DeList;

Proxy_TxPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Proxy_TxPistP5msRawInfo = CreateBus(Proxy_TxPistP5msRawInfo, DeList);
clear DeList;

Proxy_TxLogicEepDataInfo = Simulink.Bus;
DeList={
    'KPdtOffsEolReadVal'
    'KPdfOffsEolReadVal'
    'KPdtOffsDrvgReadVal'
    'KPdfOffsDrvgReadVal'
    'KPedlSimPOffsEolReadVal'
    'KPedlSimPOffsDrvgReadVal'
    'KPistPOffsEolReadVal'
    'KPistPOffsDrvgReadVal'
    'KPrimCircPOffsEolReadVal'
    'KPrimCircPOffsDrvgReadVal'
    'KSecdCircPOffsEolReadVal'
    'KSecdCircPOffsDrvgReadVal'
    'KSteerEepOffs'
    'KYawEepOffs'
    'KLatEepOffs'
    'KFsYawEepOffs'
    'KYawEepMax'
    'KYawEepMin'
    'KSteerEepMax'
    'KSteerEepMin'
    'KLatEepMax'
    'KLatEepMin'
    'KYawStillEepMax'
    'KYawStillEepMin'
    'KYawStandEepOffs'
    'KYawTmpEepMap_array_0'
    'KYawTmpEepMap_array_1'
    'KYawTmpEepMap_array_2'
    'KYawTmpEepMap_array_3'
    'KYawTmpEepMap_array_4'
    'KYawTmpEepMap_array_5'
    'KYawTmpEepMap_array_6'
    'KYawTmpEepMap_array_7'
    'KYawTmpEepMap_array_8'
    'KYawTmpEepMap_array_9'
    'KYawTmpEepMap_array_10'
    'KYawTmpEepMap_array_11'
    'KYawTmpEepMap_array_12'
    'KYawTmpEepMap_array_13'
    'KYawTmpEepMap_array_14'
    'KYawTmpEepMap_array_15'
    'KYawTmpEepMap_array_16'
    'KYawTmpEepMap_array_17'
    'KYawTmpEepMap_array_18'
    'KYawTmpEepMap_array_19'
    'KYawTmpEepMap_array_20'
    'KYawTmpEepMap_array_21'
    'KYawTmpEepMap_array_22'
    'KYawTmpEepMap_array_23'
    'KYawTmpEepMap_array_24'
    'KYawTmpEepMap_array_25'
    'KYawTmpEepMap_array_26'
    'KYawTmpEepMap_array_27'
    'KYawTmpEepMap_array_28'
    'KYawTmpEepMap_array_29'
    'KYawTmpEepMap_array_30'
    'KAdpvVehMdlVchEep'
    'KAdpvVehMdlVcrtRatEep'
    'KFlBtcTmpEep'
    'KFrBtcTmpEep'
    'KRlBtcTmpEep'
    'KRrBtcTmpEep'
    'KEeBtcsDataEep'
    'KLgtSnsrEolEepOffs'
    'KLgtSnsrDrvgEepOffs'
    'ReadInvld'
    'WrReqCmpld'
    };
Proxy_TxLogicEepDataInfo = CreateBus(Proxy_TxLogicEepDataInfo, DeList);
clear DeList;

Proxy_TxEemLampData = Simulink.Bus;
DeList={
    'Eem_Lamp_EBDLampRequest'
    'Eem_Lamp_ABSLampRequest'
    'Eem_Lamp_TCSLampRequest'
    'Eem_Lamp_TCSOffLampRequest'
    'Eem_Lamp_VDCLampRequest'
    'Eem_Lamp_VDCOffLampRequest'
    'Eem_Lamp_HDCLampRequest'
    'Eem_Lamp_BBSBuzzorRequest'
    'Eem_Lamp_RBCSLampRequest'
    'Eem_Lamp_AHBLampRequest'
    'Eem_Lamp_ServiceLampRequest'
    'Eem_Lamp_TCSFuncLampRequest'
    'Eem_Lamp_VDCFuncLampRequest'
    'Eem_Lamp_AVHLampRequest'
    'Eem_Lamp_AVHILampRequest'
    'Eem_Lamp_ACCEnable'
    'Eem_Lamp_CDMEnable'
    'Eem_Buzzor_On'
    };
Proxy_TxEemLampData = CreateBus(Proxy_TxEemLampData, DeList);
clear DeList;

Diag_HndlrEcuModeSts = Simulink.Bus;
DeList={'Diag_HndlrEcuModeSts'};
Diag_HndlrEcuModeSts = CreateBus(Diag_HndlrEcuModeSts, DeList);
clear DeList;

Diag_HndlrIgnOnOffSts = Simulink.Bus;
DeList={'Diag_HndlrIgnOnOffSts'};
Diag_HndlrIgnOnOffSts = CreateBus(Diag_HndlrIgnOnOffSts, DeList);
clear DeList;

Diag_HndlrVBatt1Mon = Simulink.Bus;
DeList={'Diag_HndlrVBatt1Mon'};
Diag_HndlrVBatt1Mon = CreateBus(Diag_HndlrVBatt1Mon, DeList);
clear DeList;

Diag_HndlrVBatt2Mon = Simulink.Bus;
DeList={'Diag_HndlrVBatt2Mon'};
Diag_HndlrVBatt2Mon = CreateBus(Diag_HndlrVBatt2Mon, DeList);
clear DeList;

Diag_HndlrFspCbsMon = Simulink.Bus;
DeList={'Diag_HndlrFspCbsMon'};
Diag_HndlrFspCbsMon = CreateBus(Diag_HndlrFspCbsMon, DeList);
clear DeList;

Diag_HndlrCEMon = Simulink.Bus;
DeList={'Diag_HndlrCEMon'};
Diag_HndlrCEMon = CreateBus(Diag_HndlrCEMon, DeList);
clear DeList;

Diag_HndlrVddMon = Simulink.Bus;
DeList={'Diag_HndlrVddMon'};
Diag_HndlrVddMon = CreateBus(Diag_HndlrVddMon, DeList);
clear DeList;

Diag_HndlrCspMon = Simulink.Bus;
DeList={'Diag_HndlrCspMon'};
Diag_HndlrCspMon = CreateBus(Diag_HndlrCspMon, DeList);
clear DeList;

Diag_HndlrFuncInhibitDiagSts = Simulink.Bus;
DeList={'Diag_HndlrFuncInhibitDiagSts'};
Diag_HndlrFuncInhibitDiagSts = CreateBus(Diag_HndlrFuncInhibitDiagSts, DeList);
clear DeList;

Diag_HndlrMtrDiagState = Simulink.Bus;
DeList={'Diag_HndlrMtrDiagState'};
Diag_HndlrMtrDiagState = CreateBus(Diag_HndlrMtrDiagState, DeList);
clear DeList;

CanM_MainEcuModeSts = Simulink.Bus;
DeList={'CanM_MainEcuModeSts'};
CanM_MainEcuModeSts = CreateBus(CanM_MainEcuModeSts, DeList);
clear DeList;

CanM_MainIgnOnOffSts = Simulink.Bus;
DeList={'CanM_MainIgnOnOffSts'};
CanM_MainIgnOnOffSts = CreateBus(CanM_MainIgnOnOffSts, DeList);
clear DeList;

CanM_MainIgnEdgeSts = Simulink.Bus;
DeList={'CanM_MainIgnEdgeSts'};
CanM_MainIgnEdgeSts = CreateBus(CanM_MainIgnEdgeSts, DeList);
clear DeList;

CanM_MainVBatt1Mon = Simulink.Bus;
DeList={'CanM_MainVBatt1Mon'};
CanM_MainVBatt1Mon = CreateBus(CanM_MainVBatt1Mon, DeList);
clear DeList;

CanM_MainArbVlvDriveState = Simulink.Bus;
DeList={'CanM_MainArbVlvDriveState'};
CanM_MainArbVlvDriveState = CreateBus(CanM_MainArbVlvDriveState, DeList);
clear DeList;

CanM_MainCEMon = Simulink.Bus;
DeList={'CanM_MainCEMon'};
CanM_MainCEMon = CreateBus(CanM_MainCEMon, DeList);
clear DeList;

CanM_MainCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'CanM_MainCanRxGearSelDispErrInfo'};
CanM_MainCanRxGearSelDispErrInfo = CreateBus(CanM_MainCanRxGearSelDispErrInfo, DeList);
clear DeList;

CanM_MainMtrArbDriveState = Simulink.Bus;
DeList={'CanM_MainMtrArbDriveState'};
CanM_MainMtrArbDriveState = CreateBus(CanM_MainMtrArbDriveState, DeList);
clear DeList;

SenPwrM_MainEcuModeSts = Simulink.Bus;
DeList={'SenPwrM_MainEcuModeSts'};
SenPwrM_MainEcuModeSts = CreateBus(SenPwrM_MainEcuModeSts, DeList);
clear DeList;

SenPwrM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SenPwrM_MainIgnOnOffSts'};
SenPwrM_MainIgnOnOffSts = CreateBus(SenPwrM_MainIgnOnOffSts, DeList);
clear DeList;

SenPwrM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SenPwrM_MainIgnEdgeSts'};
SenPwrM_MainIgnEdgeSts = CreateBus(SenPwrM_MainIgnEdgeSts, DeList);
clear DeList;

SenPwrM_MainVBatt1Mon = Simulink.Bus;
DeList={'SenPwrM_MainVBatt1Mon'};
SenPwrM_MainVBatt1Mon = CreateBus(SenPwrM_MainVBatt1Mon, DeList);
clear DeList;

SenPwrM_MainArbVlvDriveState = Simulink.Bus;
DeList={'SenPwrM_MainArbVlvDriveState'};
SenPwrM_MainArbVlvDriveState = CreateBus(SenPwrM_MainArbVlvDriveState, DeList);
clear DeList;

SenPwrM_MainExt5vMon = Simulink.Bus;
DeList={'SenPwrM_MainExt5vMon'};
SenPwrM_MainExt5vMon = CreateBus(SenPwrM_MainExt5vMon, DeList);
clear DeList;

SenPwrM_MainCspMon = Simulink.Bus;
DeList={'SenPwrM_MainCspMon'};
SenPwrM_MainCspMon = CreateBus(SenPwrM_MainCspMon, DeList);
clear DeList;

SenPwrM_MainMtrArbDriveState = Simulink.Bus;
DeList={'SenPwrM_MainMtrArbDriveState'};
SenPwrM_MainMtrArbDriveState = CreateBus(SenPwrM_MainMtrArbDriveState, DeList);
clear DeList;

YawM_MainEcuModeSts = Simulink.Bus;
DeList={'YawM_MainEcuModeSts'};
YawM_MainEcuModeSts = CreateBus(YawM_MainEcuModeSts, DeList);
clear DeList;

YawM_MainIgnOnOffSts = Simulink.Bus;
DeList={'YawM_MainIgnOnOffSts'};
YawM_MainIgnOnOffSts = CreateBus(YawM_MainIgnOnOffSts, DeList);
clear DeList;

YawM_MainIgnEdgeSts = Simulink.Bus;
DeList={'YawM_MainIgnEdgeSts'};
YawM_MainIgnEdgeSts = CreateBus(YawM_MainIgnEdgeSts, DeList);
clear DeList;

YawM_MainVBatt1Mon = Simulink.Bus;
DeList={'YawM_MainVBatt1Mon'};
YawM_MainVBatt1Mon = CreateBus(YawM_MainVBatt1Mon, DeList);
clear DeList;

AyM_MainEcuModeSts = Simulink.Bus;
DeList={'AyM_MainEcuModeSts'};
AyM_MainEcuModeSts = CreateBus(AyM_MainEcuModeSts, DeList);
clear DeList;

AyM_MainIgnOnOffSts = Simulink.Bus;
DeList={'AyM_MainIgnOnOffSts'};
AyM_MainIgnOnOffSts = CreateBus(AyM_MainIgnOnOffSts, DeList);
clear DeList;

AyM_MainIgnEdgeSts = Simulink.Bus;
DeList={'AyM_MainIgnEdgeSts'};
AyM_MainIgnEdgeSts = CreateBus(AyM_MainIgnEdgeSts, DeList);
clear DeList;

AyM_MainVBatt1Mon = Simulink.Bus;
DeList={'AyM_MainVBatt1Mon'};
AyM_MainVBatt1Mon = CreateBus(AyM_MainVBatt1Mon, DeList);
clear DeList;

AxM_MainEcuModeSts = Simulink.Bus;
DeList={'AxM_MainEcuModeSts'};
AxM_MainEcuModeSts = CreateBus(AxM_MainEcuModeSts, DeList);
clear DeList;

AxM_MainIgnOnOffSts = Simulink.Bus;
DeList={'AxM_MainIgnOnOffSts'};
AxM_MainIgnOnOffSts = CreateBus(AxM_MainIgnOnOffSts, DeList);
clear DeList;

AxM_MainIgnEdgeSts = Simulink.Bus;
DeList={'AxM_MainIgnEdgeSts'};
AxM_MainIgnEdgeSts = CreateBus(AxM_MainIgnEdgeSts, DeList);
clear DeList;

AxM_MainVBatt1Mon = Simulink.Bus;
DeList={'AxM_MainVBatt1Mon'};
AxM_MainVBatt1Mon = CreateBus(AxM_MainVBatt1Mon, DeList);
clear DeList;

SasM_MainEcuModeSts = Simulink.Bus;
DeList={'SasM_MainEcuModeSts'};
SasM_MainEcuModeSts = CreateBus(SasM_MainEcuModeSts, DeList);
clear DeList;

SasM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SasM_MainIgnOnOffSts'};
SasM_MainIgnOnOffSts = CreateBus(SasM_MainIgnOnOffSts, DeList);
clear DeList;

SasM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SasM_MainIgnEdgeSts'};
SasM_MainIgnEdgeSts = CreateBus(SasM_MainIgnEdgeSts, DeList);
clear DeList;

SasM_MainVBatt1Mon = Simulink.Bus;
DeList={'SasM_MainVBatt1Mon'};
SasM_MainVBatt1Mon = CreateBus(SasM_MainVBatt1Mon, DeList);
clear DeList;

YawP_MainEcuModeSts = Simulink.Bus;
DeList={'YawP_MainEcuModeSts'};
YawP_MainEcuModeSts = CreateBus(YawP_MainEcuModeSts, DeList);
clear DeList;

YawP_MainIgnOnOffSts = Simulink.Bus;
DeList={'YawP_MainIgnOnOffSts'};
YawP_MainIgnOnOffSts = CreateBus(YawP_MainIgnOnOffSts, DeList);
clear DeList;

YawP_MainIgnEdgeSts = Simulink.Bus;
DeList={'YawP_MainIgnEdgeSts'};
YawP_MainIgnEdgeSts = CreateBus(YawP_MainIgnEdgeSts, DeList);
clear DeList;

YawP_MainVBatt1Mon = Simulink.Bus;
DeList={'YawP_MainVBatt1Mon'};
YawP_MainVBatt1Mon = CreateBus(YawP_MainVBatt1Mon, DeList);
clear DeList;

YawP_MainVehSpd = Simulink.Bus;
DeList={'YawP_MainVehSpd'};
YawP_MainVehSpd = CreateBus(YawP_MainVehSpd, DeList);
clear DeList;

AyP_MainEcuModeSts = Simulink.Bus;
DeList={'AyP_MainEcuModeSts'};
AyP_MainEcuModeSts = CreateBus(AyP_MainEcuModeSts, DeList);
clear DeList;

AyP_MainIgnOnOffSts = Simulink.Bus;
DeList={'AyP_MainIgnOnOffSts'};
AyP_MainIgnOnOffSts = CreateBus(AyP_MainIgnOnOffSts, DeList);
clear DeList;

AyP_MainIgnEdgeSts = Simulink.Bus;
DeList={'AyP_MainIgnEdgeSts'};
AyP_MainIgnEdgeSts = CreateBus(AyP_MainIgnEdgeSts, DeList);
clear DeList;

AyP_MainVBatt1Mon = Simulink.Bus;
DeList={'AyP_MainVBatt1Mon'};
AyP_MainVBatt1Mon = CreateBus(AyP_MainVBatt1Mon, DeList);
clear DeList;

AyP_MainVehSpd = Simulink.Bus;
DeList={'AyP_MainVehSpd'};
AyP_MainVehSpd = CreateBus(AyP_MainVehSpd, DeList);
clear DeList;

AxP_MainEcuModeSts = Simulink.Bus;
DeList={'AxP_MainEcuModeSts'};
AxP_MainEcuModeSts = CreateBus(AxP_MainEcuModeSts, DeList);
clear DeList;

AxP_MainIgnOnOffSts = Simulink.Bus;
DeList={'AxP_MainIgnOnOffSts'};
AxP_MainIgnOnOffSts = CreateBus(AxP_MainIgnOnOffSts, DeList);
clear DeList;

AxP_MainIgnEdgeSts = Simulink.Bus;
DeList={'AxP_MainIgnEdgeSts'};
AxP_MainIgnEdgeSts = CreateBus(AxP_MainIgnEdgeSts, DeList);
clear DeList;

AxP_MainVBatt1Mon = Simulink.Bus;
DeList={'AxP_MainVBatt1Mon'};
AxP_MainVBatt1Mon = CreateBus(AxP_MainVBatt1Mon, DeList);
clear DeList;

AxP_MainVehSpd = Simulink.Bus;
DeList={'AxP_MainVehSpd'};
AxP_MainVehSpd = CreateBus(AxP_MainVehSpd, DeList);
clear DeList;

SasP_MainEcuModeSts = Simulink.Bus;
DeList={'SasP_MainEcuModeSts'};
SasP_MainEcuModeSts = CreateBus(SasP_MainEcuModeSts, DeList);
clear DeList;

SasP_MainIgnOnOffSts = Simulink.Bus;
DeList={'SasP_MainIgnOnOffSts'};
SasP_MainIgnOnOffSts = CreateBus(SasP_MainIgnOnOffSts, DeList);
clear DeList;

SasP_MainIgnEdgeSts = Simulink.Bus;
DeList={'SasP_MainIgnEdgeSts'};
SasP_MainIgnEdgeSts = CreateBus(SasP_MainIgnEdgeSts, DeList);
clear DeList;

SasP_MainVBatt1Mon = Simulink.Bus;
DeList={'SasP_MainVBatt1Mon'};
SasP_MainVBatt1Mon = CreateBus(SasP_MainVBatt1Mon, DeList);
clear DeList;

SasP_MainVehSpd = Simulink.Bus;
DeList={'SasP_MainVehSpd'};
SasP_MainVehSpd = CreateBus(SasP_MainVehSpd, DeList);
clear DeList;

SwtM_MainEcuModeSts = Simulink.Bus;
DeList={'SwtM_MainEcuModeSts'};
SwtM_MainEcuModeSts = CreateBus(SwtM_MainEcuModeSts, DeList);
clear DeList;

SwtM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SwtM_MainIgnOnOffSts'};
SwtM_MainIgnOnOffSts = CreateBus(SwtM_MainIgnOnOffSts, DeList);
clear DeList;

SwtM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SwtM_MainIgnEdgeSts'};
SwtM_MainIgnEdgeSts = CreateBus(SwtM_MainIgnEdgeSts, DeList);
clear DeList;

SwtM_MainVBatt1Mon = Simulink.Bus;
DeList={'SwtM_MainVBatt1Mon'};
SwtM_MainVBatt1Mon = CreateBus(SwtM_MainVBatt1Mon, DeList);
clear DeList;

SwtP_MainEcuModeSts = Simulink.Bus;
DeList={'SwtP_MainEcuModeSts'};
SwtP_MainEcuModeSts = CreateBus(SwtP_MainEcuModeSts, DeList);
clear DeList;

SwtP_MainIgnOnOffSts = Simulink.Bus;
DeList={'SwtP_MainIgnOnOffSts'};
SwtP_MainIgnOnOffSts = CreateBus(SwtP_MainIgnOnOffSts, DeList);
clear DeList;

SwtP_MainIgnEdgeSts = Simulink.Bus;
DeList={'SwtP_MainIgnEdgeSts'};
SwtP_MainIgnEdgeSts = CreateBus(SwtP_MainIgnEdgeSts, DeList);
clear DeList;

SwtP_MainVBatt1Mon = Simulink.Bus;
DeList={'SwtP_MainVBatt1Mon'};
SwtP_MainVBatt1Mon = CreateBus(SwtP_MainVBatt1Mon, DeList);
clear DeList;

SwtP_MainVehSpdFild = Simulink.Bus;
DeList={'SwtP_MainVehSpdFild'};
SwtP_MainVehSpdFild = CreateBus(SwtP_MainVehSpdFild, DeList);
clear DeList;

RlyM_MainEcuModeSts = Simulink.Bus;
DeList={'RlyM_MainEcuModeSts'};
RlyM_MainEcuModeSts = CreateBus(RlyM_MainEcuModeSts, DeList);
clear DeList;

RlyM_MainIgnOnOffSts = Simulink.Bus;
DeList={'RlyM_MainIgnOnOffSts'};
RlyM_MainIgnOnOffSts = CreateBus(RlyM_MainIgnOnOffSts, DeList);
clear DeList;

RlyM_MainIgnEdgeSts = Simulink.Bus;
DeList={'RlyM_MainIgnEdgeSts'};
RlyM_MainIgnEdgeSts = CreateBus(RlyM_MainIgnEdgeSts, DeList);
clear DeList;

RlyM_MainVBatt1Mon = Simulink.Bus;
DeList={'RlyM_MainVBatt1Mon'};
RlyM_MainVBatt1Mon = CreateBus(RlyM_MainVBatt1Mon, DeList);
clear DeList;

Proxy_TxEcuModeSts = Simulink.Bus;
DeList={'Proxy_TxEcuModeSts'};
Proxy_TxEcuModeSts = CreateBus(Proxy_TxEcuModeSts, DeList);
clear DeList;

Proxy_TxIgnOnOffSts = Simulink.Bus;
DeList={'Proxy_TxIgnOnOffSts'};
Proxy_TxIgnOnOffSts = CreateBus(Proxy_TxIgnOnOffSts, DeList);
clear DeList;

Proxy_TxFuncInhibitProxySts = Simulink.Bus;
DeList={'Proxy_TxFuncInhibitProxySts'};
Proxy_TxFuncInhibitProxySts = CreateBus(Proxy_TxFuncInhibitProxySts, DeList);
clear DeList;

Diag_HndlrDiagVlvActrInfo = Simulink.Bus;
DeList={
    'SolenoidDrvDuty'
    'FlOvReqData'
    'FlIvReqData'
    'FrOvReqData'
    'FrIvReqData'
    'RlOvReqData'
    'RlIvReqData'
    'RrOvReqData'
    'RrIvReqData'
    'PrimCutVlvReqData'
    'SecdCutVlvReqData'
    'SimVlvReqData'
    'ResPVlvReqData'
    'BalVlvReqData'
    'CircVlvReqData'
    'PressDumpReqData'
    'RelsVlvReqData'
    };
Diag_HndlrDiagVlvActrInfo = CreateBus(Diag_HndlrDiagVlvActrInfo, DeList);
clear DeList;

Diag_HndlrMotReqDataDiagInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Diag_HndlrMotReqDataDiagInfo = CreateBus(Diag_HndlrMotReqDataDiagInfo, DeList);
clear DeList;

Diag_HndlrSasCalInfo = Simulink.Bus;
DeList={
    'DiagSasCaltoAppl'
    };
Diag_HndlrSasCalInfo = CreateBus(Diag_HndlrSasCalInfo, DeList);
clear DeList;

Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = Simulink.Bus;
DeList={
    'PedlTrvlOffsEolCalcReqFlg'
    'PSnsrOffsEolCalcReqFlg'
    };
Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo = CreateBus(Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo, DeList);
clear DeList;

Diag_HndlrDiagHndlRequest = Simulink.Bus;
DeList={
    'DiagRequest_Order'
    'DiagRequest_Iq'
    'DiagRequest_Id'
    'DiagRequest_U_PWM'
    'DiagRequest_V_PWM'
    'DiagRequest_W_PWM'
    };
Diag_HndlrDiagHndlRequest = CreateBus(Diag_HndlrDiagHndlRequest, DeList);
clear DeList;

CanM_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    'CanM_SubBusOff_Err'
    'CanM_MainOverRun_Err'
    'CanM_SubOverRun_Err'
    'CanM_EMS1_Tout_Err'
    'CanM_EMS2_Tout_Err'
    'CanM_TCU1_Tout_Err'
    'CanM_TCU5_Tout_Err'
    'CanM_FWD1_Tout_Err'
    'CanM_HCU1_Tout_Err'
    'CanM_HCU2_Tout_Err'
    'CanM_HCU3_Tout_Err'
    'CanM_VSM2_Tout_Err'
    'CanM_EMS_Invalid_Err'
    'CanM_TCU_Invalid_Err'
    };
CanM_MainCanMonData = CreateBus(CanM_MainCanMonData, DeList);
clear DeList;

SenPwrM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
SenPwrM_MainSenPwrMonitorData = CreateBus(SenPwrM_MainSenPwrMonitorData, DeList);
clear DeList;

YawM_MainYAWMSerialInfo = Simulink.Bus;
DeList={
    'YAWM_SerialNumOK_Flg'
    'YAWM_SerialUnMatch_Flg'
    };
YawM_MainYAWMSerialInfo = CreateBus(YawM_MainYAWMSerialInfo, DeList);
clear DeList;

YawM_MainYAWMOutInfo = Simulink.Bus;
DeList={
    'YAWM_Timeout_Err'
    'YAWM_Invalid_Err'
    'YAWM_CRC_Err'
    'YAWM_Rolling_Err'
    'YAWM_Temperature_Err'
    'YAWM_Range_Err'
    };
YawM_MainYAWMOutInfo = CreateBus(YawM_MainYAWMOutInfo, DeList);
clear DeList;

AyM_MainAYMOuitInfo = Simulink.Bus;
DeList={
    'AYM_Timeout_Err'
    'AYM_Invalid_Err'
    'AYM_CRC_Err'
    'AYM_Rolling_Err'
    'AYM_Temperature_Err'
    'AYM_Range_Err'
    };
AyM_MainAYMOuitInfo = CreateBus(AyM_MainAYMOuitInfo, DeList);
clear DeList;

AxM_MainAXMOutInfo = Simulink.Bus;
DeList={
    'AXM_Timeout_Err'
    'AXM_Invalid_Err'
    'AXM_CRC_Err'
    'AXM_Rolling_Err'
    'AXM_Temperature_Err'
    'AXM_Range_Err'
    };
AxM_MainAXMOutInfo = CreateBus(AxM_MainAXMOutInfo, DeList);
clear DeList;

SasM_MainSASMOutInfo = Simulink.Bus;
DeList={
    'SASM_Timeout_Err'
    'SASM_Invalid_Err'
    'SASM_CRC_Err'
    'SASM_Rolling_Err'
    'SASM_Range_Err'
    };
SasM_MainSASMOutInfo = CreateBus(SasM_MainSASMOutInfo, DeList);
clear DeList;

YawP_MainYawPlauOutput = Simulink.Bus;
DeList={
    'YawPlauModelErr'
    'YawPlauNoisekErr'
    'YawPlauShockErr'
    'YawPlauRangeErr'
    'YawPlauStandStillErr'
    };
YawP_MainYawPlauOutput = CreateBus(YawP_MainYawPlauOutput, DeList);
clear DeList;

AyP_MainAyPlauOutput = Simulink.Bus;
DeList={
    'AyPlauNoiselErr'
    'AyPlauModelErr'
    'AyPlauShockErr'
    'AyPlauRangeErr'
    'AyPlauStandStillErr'
    };
AyP_MainAyPlauOutput = CreateBus(AyP_MainAyPlauOutput, DeList);
clear DeList;

AxP_MainAxPlauOutput = Simulink.Bus;
DeList={
    'AxPlauOffsetErr'
    'AxPlauDrivingOffsetErr'
    'AxPlauStickErr'
    'AxPlauNoiseErr'
    };
AxP_MainAxPlauOutput = CreateBus(AxP_MainAxPlauOutput, DeList);
clear DeList;

SasP_MainSasPlauOutput = Simulink.Bus;
DeList={
    'SasPlauModelErr'
    'SasPlauOffsetErr'
    'SasPlauStickErr'
    };
SasP_MainSasPlauOutput = CreateBus(SasP_MainSasPlauOutput, DeList);
clear DeList;

SwtM_MainSwtMonFaultInfo = Simulink.Bus;
DeList={
    'SWM_ESCOFF_Sw_Err'
    'SWM_HDC_Sw_Err'
    'SWM_AVH_Sw_Err'
    'SWM_BFL_Sw_Err'
    'SWM_PB_Sw_Err'
    };
SwtM_MainSwtMonFaultInfo = CreateBus(SwtM_MainSwtMonFaultInfo, DeList);
clear DeList;

SwtP_MainSwtPFaultInfo = Simulink.Bus;
DeList={
    'SWM_BLS_Sw_HighStick_Err'
    'SWM_BLS_Sw_LowStick_Err'
    'SWM_BS_Sw_HighStick_Err'
    'SWM_BS_Sw_LowStick_Err'
    };
SwtP_MainSwtPFaultInfo = CreateBus(SwtP_MainSwtPFaultInfo, DeList);
clear DeList;

RlyM_MainRelayMonitorData = Simulink.Bus;
DeList={
    'RlyM_HDCRelay_Open_Err'
    'RlyM_HDCRelay_Short_Err'
    'RlyM_ESSRelay_Open_Err'
    'RlyM_ESSRelay_Short_Err'
    };
RlyM_MainRelayMonitorData = CreateBus(RlyM_MainRelayMonitorData, DeList);
clear DeList;

Proxy_TxTxESCSensorInfo = Simulink.Bus;
DeList={
    'EstimatedYaw'
    'EstimatedAY'
    'A_long_1_1000g'
    };
Proxy_TxTxESCSensorInfo = CreateBus(Proxy_TxTxESCSensorInfo, DeList);
clear DeList;

Diag_HndlrDiagClrSrs = Simulink.Bus;
DeList={'Diag_HndlrDiagClrSrs'};
Diag_HndlrDiagClrSrs = CreateBus(Diag_HndlrDiagClrSrs, DeList);
clear DeList;

Diag_HndlrDiagSci = Simulink.Bus;
DeList={'Diag_HndlrDiagSci'};
Diag_HndlrDiagSci = CreateBus(Diag_HndlrDiagSci, DeList);
clear DeList;

Diag_HndlrDiagAhbSci = Simulink.Bus;
DeList={'Diag_HndlrDiagAhbSci'};
Diag_HndlrDiagAhbSci = CreateBus(Diag_HndlrDiagAhbSci, DeList);
clear DeList;




Diag_HndlrDiagClrSrs = Simulink.Bus;
DeList={'Diag_HndlrDiagClrSrs'};
Diag_HndlrDiagClrSrs = CreateBus(Diag_HndlrDiagClrSrs, DeList);
clear DeList;

Diag_HndlrDiagSci = Simulink.Bus;
DeList={'Diag_HndlrDiagSci'};
Diag_HndlrDiagSci = CreateBus(Diag_HndlrDiagSci, DeList);
clear DeList;

Diag_HndlrDiagAhbSci = Simulink.Bus;
DeList={'Diag_HndlrDiagAhbSci'};
Diag_HndlrDiagAhbSci = CreateBus(Diag_HndlrDiagAhbSci, DeList);
clear DeList;


CanM_MainDiagClrSrs = Simulink.Bus;
DeList={'CanM_MainDiagClrSrs'};
CanM_MainDiagClrSrs = CreateBus(CanM_MainDiagClrSrs, DeList);
clear DeList;

CanM_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    'CanM_SubBusOff_Err'
    'CanM_MainOverRun_Err'
    'CanM_SubOverRun_Err'
    'CanM_EMS1_Tout_Err'
    'CanM_EMS2_Tout_Err'
    'CanM_TCU1_Tout_Err'
    'CanM_TCU5_Tout_Err'
    'CanM_FWD1_Tout_Err'
    'CanM_HCU1_Tout_Err'
    'CanM_HCU2_Tout_Err'
    'CanM_HCU3_Tout_Err'
    'CanM_VSM2_Tout_Err'
    'CanM_EMS_Invalid_Err'
    'CanM_TCU_Invalid_Err'
    };
CanM_MainCanMonData = CreateBus(CanM_MainCanMonData, DeList);
clear DeList;


SenPwrM_MainDiagClrSrs = Simulink.Bus;
DeList={'SenPwrM_MainDiagClrSrs'};
SenPwrM_MainDiagClrSrs = CreateBus(SenPwrM_MainDiagClrSrs, DeList);
clear DeList;

SenPwrM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
SenPwrM_MainSenPwrMonitorData = CreateBus(SenPwrM_MainSenPwrMonitorData, DeList);
clear DeList;


YawM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
YawM_MainSenPwrMonitorData = CreateBus(YawM_MainSenPwrMonitorData, DeList);
clear DeList;

YawM_MainDiagClrSrs = Simulink.Bus;
DeList={'YawM_MainDiagClrSrs'};
YawM_MainDiagClrSrs = CreateBus(YawM_MainDiagClrSrs, DeList);
clear DeList;


AyM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AyM_MainSenPwrMonitorData = CreateBus(AyM_MainSenPwrMonitorData, DeList);
clear DeList;

AyM_MainDiagClrSrs = Simulink.Bus;
DeList={'AyM_MainDiagClrSrs'};
AyM_MainDiagClrSrs = CreateBus(AyM_MainDiagClrSrs, DeList);
clear DeList;


AxM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AxM_MainSenPwrMonitorData = CreateBus(AxM_MainSenPwrMonitorData, DeList);
clear DeList;

AxM_MainDiagClrSrs = Simulink.Bus;
DeList={'AxM_MainDiagClrSrs'};
AxM_MainDiagClrSrs = CreateBus(AxM_MainDiagClrSrs, DeList);
clear DeList;


SasM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
SasM_MainSenPwrMonitorData = CreateBus(SasM_MainSenPwrMonitorData, DeList);
clear DeList;

SasM_MainDiagClrSrs = Simulink.Bus;
DeList={'SasM_MainDiagClrSrs'};
SasM_MainDiagClrSrs = CreateBus(SasM_MainDiagClrSrs, DeList);
clear DeList;


YawP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
YawP_MainSenPwrMonitorData = CreateBus(YawP_MainSenPwrMonitorData, DeList);
clear DeList;

YawP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_SubBusOff_Err'
    };
YawP_MainCanMonData = CreateBus(YawP_MainCanMonData, DeList);
clear DeList;

YawP_MainDiagClrSrs = Simulink.Bus;
DeList={'YawP_MainDiagClrSrs'};
YawP_MainDiagClrSrs = CreateBus(YawP_MainDiagClrSrs, DeList);
clear DeList;


AyP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AyP_MainSenPwrMonitorData = CreateBus(AyP_MainSenPwrMonitorData, DeList);
clear DeList;

AyP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_SubBusOff_Err'
    };
AyP_MainCanMonData = CreateBus(AyP_MainCanMonData, DeList);
clear DeList;

AyP_MainDiagClrSrs = Simulink.Bus;
DeList={'AyP_MainDiagClrSrs'};
AyP_MainDiagClrSrs = CreateBus(AyP_MainDiagClrSrs, DeList);
clear DeList;


AxP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AxP_MainSenPwrMonitorData = CreateBus(AxP_MainSenPwrMonitorData, DeList);
clear DeList;

AxP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_SubBusOff_Err'
    };
AxP_MainCanMonData = CreateBus(AxP_MainCanMonData, DeList);
clear DeList;

AxP_MainDiagClrSrs = Simulink.Bus;
DeList={'AxP_MainDiagClrSrs'};
AxP_MainDiagClrSrs = CreateBus(AxP_MainDiagClrSrs, DeList);
clear DeList;


SasP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
SasP_MainSenPwrMonitorData = CreateBus(SasP_MainSenPwrMonitorData, DeList);
clear DeList;

SasP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    };
SasP_MainCanMonData = CreateBus(SasP_MainCanMonData, DeList);
clear DeList;

SasP_MainDiagClrSrs = Simulink.Bus;
DeList={'SasP_MainDiagClrSrs'};
SasP_MainDiagClrSrs = CreateBus(SasP_MainDiagClrSrs, DeList);
clear DeList;


SwtM_MainDiagClrSrs = Simulink.Bus;
DeList={'SwtM_MainDiagClrSrs'};
SwtM_MainDiagClrSrs = CreateBus(SwtM_MainDiagClrSrs, DeList);
clear DeList;


SwtP_MainDiagClrSrs = Simulink.Bus;
DeList={'SwtP_MainDiagClrSrs'};
SwtP_MainDiagClrSrs = CreateBus(SwtP_MainDiagClrSrs, DeList);
clear DeList;


RlyM_MainDiagClrSrs = Simulink.Bus;
DeList={'RlyM_MainDiagClrSrs'};
RlyM_MainDiagClrSrs = CreateBus(RlyM_MainDiagClrSrs, DeList);
clear DeList;


Proxy_TxDiagSci = Simulink.Bus;
DeList={'Proxy_TxDiagSci'};
Proxy_TxDiagSci = CreateBus(Proxy_TxDiagSci, DeList);
clear DeList;

Proxy_TxDiagAhbSci = Simulink.Bus;
DeList={'Proxy_TxDiagAhbSci'};
Proxy_TxDiagAhbSci = CreateBus(Proxy_TxDiagAhbSci, DeList);
clear DeList;

Proxy_TxTxYawCbitInfo = Simulink.Bus;
DeList={
    'TestYawRateSensor'
    'TestAcclSensor'
    'YawSerialNumberReq'
    };
Proxy_TxTxYawCbitInfo = CreateBus(Proxy_TxTxYawCbitInfo, DeList);
clear DeList;

Proxy_TxTxWhlSpdInfo = Simulink.Bus;
DeList={
    'WhlSpdFL'
    'WhlSpdFR'
    'WhlSpdRL'
    'WhlSpdRR'
    };
Proxy_TxTxWhlSpdInfo = CreateBus(Proxy_TxTxWhlSpdInfo, DeList);
clear DeList;

Proxy_TxTxWhlPulInfo = Simulink.Bus;
DeList={
    'WhlPulFL'
    'WhlPulFR'
    'WhlPulRL'
    'WhlPulRR'
    };
Proxy_TxTxWhlPulInfo = CreateBus(Proxy_TxTxWhlPulInfo, DeList);
clear DeList;

Proxy_TxTxTcs5Info = Simulink.Bus;
DeList={
    'CfBrkAbsWLMP'
    'CfBrkEbdWLMP'
    'CfBrkTcsWLMP'
    'CfBrkTcsFLMP'
    'CfBrkAbsDIAG'
    'CrBrkWheelFL_KMH'
    'CrBrkWheelFR_KMH'
    'CrBrkWheelRL_KMH'
    'CrBrkWheelRR_KMH'
    };
Proxy_TxTxTcs5Info = CreateBus(Proxy_TxTxTcs5Info, DeList);
clear DeList;

Proxy_TxTxTcs1Info = Simulink.Bus;
DeList={
    'CfBrkTcsREQ'
    'CfBrkTcsPAS'
    'CfBrkTcsDEF'
    'CfBrkTcsCTL'
    'CfBrkAbsACT'
    'CfBrkAbsDEF'
    'CfBrkEbdDEF'
    'CfBrkTcsGSC'
    'CfBrkEspPAS'
    'CfBrkEspDEF'
    'CfBrkEspCTL'
    'CfBrkMsrREQ'
    'CfBrkTcsMFRN'
    'CfBrkMinGEAR'
    'CfBrkMaxGEAR'
    'BrakeLight'
    'HacCtl'
    'HacPas'
    'HacDef'
    'CrBrkTQI_PC'
    'CrBrkTQIMSR_PC'
    'CrBrkTQISLW_PC'
    'CrEbsMSGCHKSUM'
    };
Proxy_TxTxTcs1Info = CreateBus(Proxy_TxTxTcs1Info, DeList);
clear DeList;

Proxy_TxTxSasCalInfo = Simulink.Bus;
DeList={
    'CalSas_CCW'
    'CalSas_Lws_CID'
    };
Proxy_TxTxSasCalInfo = CreateBus(Proxy_TxTxSasCalInfo, DeList);
clear DeList;

Proxy_TxTxEsp2Info = Simulink.Bus;
DeList={
    'LatAccel'
    'LatAccelStat'
    'LatAccelDiag'
    'LongAccel'
    'LongAccelStat'
    'LongAccelDiag'
    'YawRate'
    'YawRateStat'
    'YawRateDiag'
    'CylPres'
    'CylPresStat'
    'CylPresDiag'
    };
Proxy_TxTxEsp2Info = CreateBus(Proxy_TxTxEsp2Info, DeList);
clear DeList;

Proxy_TxTxEbs1Info = Simulink.Bus;
DeList={
    'CfBrkEbsStat'
    'CrBrkRegenTQLimit_NM'
    'CrBrkEstTot_NM'
    'CfBrkSnsFail'
    'CfBrkRbsWLamp'
    'CfBrkVacuumSysDef'
    'CrBrkEstHyd_NM'
    'CrBrkStkDep_PC'
    'CfBrkPgmRun1'
    };
Proxy_TxTxEbs1Info = CreateBus(Proxy_TxTxEbs1Info, DeList);
clear DeList;

Proxy_TxTxAhb1Info = Simulink.Bus;
DeList={
    'CfAhbWLMP'
    'CfAhbDiag'
    'CfAhbAct'
    'CfAhbDef'
    'CfAhbSLMP'
    'CfBrkAhbSTDep_PC'
    'CfBrkBuzzR'
    'CfBrkPedalCalStatus'
    'CfBrkAhbSnsFail'
    'WhlSpdFLAhb'
    'WhlSpdFRAhb'
    'CrAhbMsgChkSum'
    };
Proxy_TxTxAhb1Info = CreateBus(Proxy_TxTxAhb1Info, DeList);
clear DeList;

Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = Simulink.Bus;
DeList={
    'QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_WMOM_PT_SUM_STAB_FAST'
    'ST_ECU_RQ_WMOM_PT_SUM_STAB'
    'QU_RQ_ILK_PT'
    'QU_TAR_WMOM_PT_SUM_STAB'
    'TAR_WMOM_PT_SUM_STAB'
    };
Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = CreateBus(Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo, DeList);
clear DeList;

Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = Simulink.Bus;
DeList={
    'ST_ECU_AVL_BRTORQ_SUM'
    'QU_SER_PRMSN_DBC'
    'QU_SER_PRF_BRK'
    'AVL_BRTORQ_SUM_DVCH'
    'AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM_DVCH'
    };
Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = CreateBus(Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo, DeList);
clear DeList;

Proxy_TxAVL_BRTORQ_WHLInfo = Simulink.Bus;
DeList={
    'QU_AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RRH'
    'QU_AVL_BRTORQ_WHL_FRH'
    'AVL_BRTORQ_WHL_RRH'
    'AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FRH'
    };
Proxy_TxAVL_BRTORQ_WHLInfo = CreateBus(Proxy_TxAVL_BRTORQ_WHLInfo, DeList);
clear DeList;

Proxy_TxAVL_RPM_WHLInfo = Simulink.Bus;
DeList={
    'AVL_RPM_WHL_RLH'
    'AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FRH'
    'QU_AVL_RPM_WHL_RLH'
    'QU_AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FRH'
    };
Proxy_TxAVL_RPM_WHLInfo = CreateBus(Proxy_TxAVL_RPM_WHLInfo, DeList);
clear DeList;

Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo = Simulink.Bus;
DeList={
    'QU_TAR_WMOM_PT_SUM_RECUP'
    'TAR_WMOM_PT_SUM_RECUP'
    'ST_ECU_RQ_WMOM_PT_SUM_RECUP'
    };
Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo = CreateBus(Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo, DeList);
clear DeList;

Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info = Simulink.Bus;
DeList={
    'ST_ECU_ST_STAB_DSC'
    'ST_BRG_MSA'
    'ST_BRG_DV'
    'ST_UNRU_DSC'
    'QU_FN_FDR'
    'QU_FN_ABS'
    'QU_FN_ASC'
    'PRD_TIM_VEHSS'
    };
Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info = CreateBus(Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info, DeList);
clear DeList;

Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = Simulink.Bus;
DeList={
    'QU_SER_ECBA'
    'TAR_BRTORQ_SUM_COOTD'
    'AVL_WAY_BRKFA'
    'AVL_FORC_BRKFA'
    };
Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = CreateBus(Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo, DeList);
clear DeList;

Proxy_TxAVL_QUAN_EES_WHLInfo = Simulink.Bus;
DeList={
    'AVL_QUAN_EES_WHL_RLH'
    'AVL_QUAN_EES_WHL_RRH'
    'AVL_QUAN_EES_WHL_FLH'
    'AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_FLH'
    'QU_AVL_QUAN_EES_WHL_RRH'
    'QU_AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_RLH'
    };
Proxy_TxAVL_QUAN_EES_WHLInfo = CreateBus(Proxy_TxAVL_QUAN_EES_WHLInfo, DeList);
clear DeList;

Proxy_TxTAR_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'TAR_LTRQD_BAX'
    'LIM_MAX_LTRQD_BAX'
    };
Proxy_TxTAR_LTRQD_BAXInfo = CreateBus(Proxy_TxTAR_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_TxST_YMRInfo = Simulink.Bus;
DeList={
    'QU_SER_CLCTR_YMR'
    };
Proxy_TxST_YMRInfo = CreateBus(Proxy_TxST_YMRInfo, DeList);
clear DeList;

Proxy_TxWEGSTRECKEInfo = Simulink.Bus;
DeList={
    'MILE_FLH'
    'MILE_FRH'
    'MILE_INTM_TOMSRM'
    'MILE'
    };
Proxy_TxWEGSTRECKEInfo = CreateBus(Proxy_TxWEGSTRECKEInfo, DeList);
clear DeList;

Proxy_TxDISP_CC_DRDY_00Info = Simulink.Bus;
DeList={
    'ST_IDC_CC_DRDY_00'
    'ST_CC_DRDY_00'
    'TRANF_CC_DRDY_00'
    'NO_CC_DRDY_00'
    };
Proxy_TxDISP_CC_DRDY_00Info = CreateBus(Proxy_TxDISP_CC_DRDY_00Info, DeList);
clear DeList;

Proxy_TxDISP_CC_BYPA_00Info = Simulink.Bus;
DeList={
    'TRANF_CC_BYPA_00'
    'ST_IDC_CC_BYPA_00'
    'ST_CC_BYPA_00'
    'NO_CC_BYPA_00'
    };
Proxy_TxDISP_CC_BYPA_00Info = CreateBus(Proxy_TxDISP_CC_BYPA_00Info, DeList);
clear DeList;

Proxy_TxST_VHSSInfo = Simulink.Bus;
DeList={
    'ST_VEHSS'
    };
Proxy_TxST_VHSSInfo = CreateBus(Proxy_TxST_VHSSInfo, DeList);
clear DeList;

Proxy_TxDIAG_OBD_HYB_1Info = Simulink.Bus;
DeList={
    'QU_FN_OBD_1_SEN_1_ERR'
    'QU_FN_OBD_1_SEN_3_ERR'
    'QU_FN_OBD_1_SEN_4_ERR'
    'DIAG_OBD_HYB_1_MUX_MAX'
    'QU_FN_OBD_1_SEN_2_ERR'
    'DIAG_OBD_HYB_1_MUX_IMME'
    };
Proxy_TxDIAG_OBD_HYB_1Info = CreateBus(Proxy_TxDIAG_OBD_HYB_1Info, DeList);
clear DeList;

Proxy_TxSU_DSCInfo = Simulink.Bus;
DeList={
    'SU_DSC_WSS'
    };
Proxy_TxSU_DSCInfo = CreateBus(Proxy_TxSU_DSCInfo, DeList);
clear DeList;

Proxy_TxST_TYR_RDCInfo = Simulink.Bus;
DeList={
    'QU_TPL_RDC'
    'QU_FN_TYR_INFO_RDC'
    'QU_TFAI_RDC'
    };
Proxy_TxST_TYR_RDCInfo = CreateBus(Proxy_TxST_TYR_RDCInfo, DeList);
clear DeList;

Proxy_TxST_TYR_RPAInfo = Simulink.Bus;
DeList={
    'QU_TFAI_RPA'
    'QU_FN_TYR_INFO_RPA'
    'QU_TPL_RPA'
    };
Proxy_TxST_TYR_RPAInfo = CreateBus(Proxy_TxST_TYR_RPAInfo, DeList);
clear DeList;

Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info = Simulink.Bus;
DeList={
    'TYR_ID_2'
    'TYR_ID_1'
    'TYR_ID_3'
    'TYR_ID_4'
    };
Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info = CreateBus(Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info, DeList);
clear DeList;

Proxy_TxST_TYR_2Info = Simulink.Bus;
DeList={
    'QU_RDC_INIT_DISP'
    };
Proxy_TxST_TYR_2Info = CreateBus(Proxy_TxST_TYR_2Info, DeList);
clear DeList;

Proxy_TxAVL_T_TYRInfo = Simulink.Bus;
DeList={
    'AVL_TEMP_TYR_RRH'
    'AVL_TEMP_TYR_FLH'
    'AVL_TEMP_TYR_RLH'
    'AVL_TEMP_TYR_FRH'
    };
Proxy_TxAVL_T_TYRInfo = CreateBus(Proxy_TxAVL_T_TYRInfo, DeList);
clear DeList;

Proxy_TxTEMP_BRKInfo = Simulink.Bus;
DeList={
    'TEMP_BRDSK_RLH_VRFD'
    'QU_TEMP_BRDSK_RLH_VRFD'
    'TEMP_BRDSK_RRH_VRFD'
    'QU_TEMP_BRDSK_RRH_VRFD'
    };
Proxy_TxTEMP_BRKInfo = CreateBus(Proxy_TxTEMP_BRKInfo, DeList);
clear DeList;

Proxy_TxAVL_P_TYRInfo = Simulink.Bus;
DeList={
    'AVL_P_TYR_FRH'
    'AVL_P_TYR_FLH'
    'AVL_P_TYR_RRH'
    'AVL_P_TYR_RLH'
    };
Proxy_TxAVL_P_TYRInfo = CreateBus(Proxy_TxAVL_P_TYRInfo, DeList);
clear DeList;

Proxy_TxFR_DBG_DSCInfo = Simulink.Bus;
DeList={
    'DBG_RDCi_13'
    'DBG_RDCi_04'
    'DBG_RDCi_05'
    'DBG_RDCi_06'
    'DBG_RDCi_01'
    'DBG_RDCi_02'
    'DBG_RDCi_03'
    'DBG_RDCi_07'
    'DBG_RDCi_11'
    'DBG_RDCi_10'
    'DBG_RDCi_09'
    'DBG_RDCi_15'
    'DBG_RDCi_14'
    'DBG_RDCi_12'
    'DBG_RDCi_00'
    'BDTM_TA_FR'
    'BDTM_TA_RL'
    'BDTM_TA_RR'
    'DBG_RDCi_08'
    'ST_STATE'
    'BDTM_TA_FL'
    'ABSRef'
    'LOW_VOLTAGE'
    'DEBUG_PCIB'
    'DBG_DSC'
    'LATACC'
    'TRIGGER_FS'
    'TRIGGER_IS'
    };
Proxy_TxFR_DBG_DSCInfo = CreateBus(Proxy_TxFR_DBG_DSCInfo, DeList);
clear DeList;


Proxy_TxByComTxYawCbitInfo = Simulink.Bus;
DeList={
    'TestYawRateSensor'
    'TestAcclSensor'
    'YawSerialNumberReq'
    };
Proxy_TxByComTxYawCbitInfo = CreateBus(Proxy_TxByComTxYawCbitInfo, DeList);
clear DeList;

Proxy_TxByComTxWhlSpdInfo = Simulink.Bus;
DeList={
    'WhlSpdFL'
    'WhlSpdFR'
    'WhlSpdRL'
    'WhlSpdRR'
    };
Proxy_TxByComTxWhlSpdInfo = CreateBus(Proxy_TxByComTxWhlSpdInfo, DeList);
clear DeList;

Proxy_TxByComTxWhlPulInfo = Simulink.Bus;
DeList={
    'WhlPulFL'
    'WhlPulFR'
    'WhlPulRL'
    'WhlPulRR'
    };
Proxy_TxByComTxWhlPulInfo = CreateBus(Proxy_TxByComTxWhlPulInfo, DeList);
clear DeList;

Proxy_TxByComTxTcs5Info = Simulink.Bus;
DeList={
    'CfBrkAbsWLMP'
    'CfBrkEbdWLMP'
    'CfBrkTcsWLMP'
    'CfBrkTcsFLMP'
    'CfBrkAbsDIAG'
    'CrBrkWheelFL_KMH'
    'CrBrkWheelFR_KMH'
    'CrBrkWheelRL_KMH'
    'CrBrkWheelRR_KMH'
    };
Proxy_TxByComTxTcs5Info = CreateBus(Proxy_TxByComTxTcs5Info, DeList);
clear DeList;

Proxy_TxByComTxTcs1Info = Simulink.Bus;
DeList={
    'CfBrkTcsREQ'
    'CfBrkTcsPAS'
    'CfBrkTcsDEF'
    'CfBrkTcsCTL'
    'CfBrkAbsACT'
    'CfBrkAbsDEF'
    'CfBrkEbdDEF'
    'CfBrkTcsGSC'
    'CfBrkEspPAS'
    'CfBrkEspDEF'
    'CfBrkEspCTL'
    'CfBrkMsrREQ'
    'CfBrkTcsMFRN'
    'CfBrkMinGEAR'
    'CfBrkMaxGEAR'
    'BrakeLight'
    'HacCtl'
    'HacPas'
    'HacDef'
    'CrBrkTQI_PC'
    'CrBrkTQIMSR_PC'
    'CrBrkTQISLW_PC'
    'CrEbsMSGCHKSUM'
    };
Proxy_TxByComTxTcs1Info = CreateBus(Proxy_TxByComTxTcs1Info, DeList);
clear DeList;

Proxy_TxByComTxSasCalInfo = Simulink.Bus;
DeList={
    'CalSas_CCW'
    'CalSas_Lws_CID'
    };
Proxy_TxByComTxSasCalInfo = CreateBus(Proxy_TxByComTxSasCalInfo, DeList);
clear DeList;

Proxy_TxByComTxEsp2Info = Simulink.Bus;
DeList={
    'LatAccel'
    'LatAccelStat'
    'LatAccelDiag'
    'LongAccel'
    'LongAccelStat'
    'LongAccelDiag'
    'YawRate'
    'YawRateStat'
    'YawRateDiag'
    'CylPres'
    'CylPresStat'
    'CylPresDiag'
    };
Proxy_TxByComTxEsp2Info = CreateBus(Proxy_TxByComTxEsp2Info, DeList);
clear DeList;

Proxy_TxByComTxEbs1Info = Simulink.Bus;
DeList={
    'CfBrkEbsStat'
    'CrBrkRegenTQLimit_NM'
    'CrBrkEstTot_NM'
    'CfBrkSnsFail'
    'CfBrkRbsWLamp'
    'CfBrkVacuumSysDef'
    'CrBrkEstHyd_NM'
    'CrBrkStkDep_PC'
    'CfBrkPgmRun1'
    };
Proxy_TxByComTxEbs1Info = CreateBus(Proxy_TxByComTxEbs1Info, DeList);
clear DeList;

Proxy_TxByComTxAhb1Info = Simulink.Bus;
DeList={
    'CfAhbWLMP'
    'CfAhbDiag'
    'CfAhbAct'
    'CfAhbDef'
    'CfAhbSLMP'
    'CfBrkAhbSTDep_PC'
    'CfBrkBuzzR'
    'CfBrkPedalCalStatus'
    'CfBrkAhbSnsFail'
    'WhlSpdFLAhb'
    'WhlSpdFRAhb'
    'CrAhbMsgChkSum'
    };
Proxy_TxByComTxAhb1Info = CreateBus(Proxy_TxByComTxAhb1Info, DeList);
clear DeList;

Proxy_TxByComSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
Proxy_TxByComSenPwrMonitorData = CreateBus(Proxy_TxByComSenPwrMonitorData, DeList);
clear DeList;

Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = Simulink.Bus;
DeList={
    'QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_WMOM_PT_SUM_STAB_FAST'
    'ST_ECU_RQ_WMOM_PT_SUM_STAB'
    'QU_RQ_ILK_PT'
    'QU_TAR_WMOM_PT_SUM_STAB'
    'TAR_WMOM_PT_SUM_STAB'
    };
Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = CreateBus(Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo, DeList);
clear DeList;

Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = Simulink.Bus;
DeList={
    'ST_ECU_AVL_BRTORQ_SUM'
    'QU_SER_PRMSN_DBC'
    'QU_SER_PRF_BRK'
    'AVL_BRTORQ_SUM_DVCH'
    'AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM_DVCH'
    };
Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = CreateBus(Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo, DeList);
clear DeList;

Proxy_TxByComAVL_BRTORQ_WHLInfo = Simulink.Bus;
DeList={
    'QU_AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RRH'
    'QU_AVL_BRTORQ_WHL_FRH'
    'AVL_BRTORQ_WHL_RRH'
    'AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FRH'
    };
Proxy_TxByComAVL_BRTORQ_WHLInfo = CreateBus(Proxy_TxByComAVL_BRTORQ_WHLInfo, DeList);
clear DeList;

Proxy_TxByComAVL_RPM_WHLInfo = Simulink.Bus;
DeList={
    'AVL_RPM_WHL_RLH'
    'AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FRH'
    'QU_AVL_RPM_WHL_RLH'
    'QU_AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FRH'
    };
Proxy_TxByComAVL_RPM_WHLInfo = CreateBus(Proxy_TxByComAVL_RPM_WHLInfo, DeList);
clear DeList;

Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo = Simulink.Bus;
DeList={
    'QU_TAR_WMOM_PT_SUM_RECUP'
    'TAR_WMOM_PT_SUM_RECUP'
    'ST_ECU_RQ_WMOM_PT_SUM_RECUP'
    };
Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo = CreateBus(Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo, DeList);
clear DeList;

Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info = Simulink.Bus;
DeList={
    'ST_ECU_ST_STAB_DSC'
    'ST_BRG_MSA'
    'ST_BRG_DV'
    'ST_UNRU_DSC'
    'QU_FN_FDR'
    'QU_FN_ABS'
    'QU_FN_ASC'
    'PRD_TIM_VEHSS'
    };
Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info = CreateBus(Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info, DeList);
clear DeList;

Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = Simulink.Bus;
DeList={
    'QU_SER_ECBA'
    'TAR_BRTORQ_SUM_COOTD'
    'AVL_WAY_BRKFA'
    'AVL_FORC_BRKFA'
    };
Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = CreateBus(Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo, DeList);
clear DeList;

Proxy_TxByComAVL_QUAN_EES_WHLInfo = Simulink.Bus;
DeList={
    'AVL_QUAN_EES_WHL_RLH'
    'AVL_QUAN_EES_WHL_RRH'
    'AVL_QUAN_EES_WHL_FLH'
    'AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_FLH'
    'QU_AVL_QUAN_EES_WHL_RRH'
    'QU_AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_RLH'
    };
Proxy_TxByComAVL_QUAN_EES_WHLInfo = CreateBus(Proxy_TxByComAVL_QUAN_EES_WHLInfo, DeList);
clear DeList;

Proxy_TxByComTAR_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'TAR_LTRQD_BAX'
    'LIM_MAX_LTRQD_BAX'
    };
Proxy_TxByComTAR_LTRQD_BAXInfo = CreateBus(Proxy_TxByComTAR_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_TxByComST_YMRInfo = Simulink.Bus;
DeList={
    'QU_SER_CLCTR_YMR'
    };
Proxy_TxByComST_YMRInfo = CreateBus(Proxy_TxByComST_YMRInfo, DeList);
clear DeList;

Proxy_TxByComWEGSTRECKEInfo = Simulink.Bus;
DeList={
    'MILE_FLH'
    'MILE_FRH'
    'MILE_INTM_TOMSRM'
    'MILE'
    };
Proxy_TxByComWEGSTRECKEInfo = CreateBus(Proxy_TxByComWEGSTRECKEInfo, DeList);
clear DeList;

Proxy_TxByComDISP_CC_DRDY_00Info = Simulink.Bus;
DeList={
    'ST_IDC_CC_DRDY_00'
    'ST_CC_DRDY_00'
    'TRANF_CC_DRDY_00'
    'NO_CC_DRDY_00'
    };
Proxy_TxByComDISP_CC_DRDY_00Info = CreateBus(Proxy_TxByComDISP_CC_DRDY_00Info, DeList);
clear DeList;

Proxy_TxByComDISP_CC_BYPA_00Info = Simulink.Bus;
DeList={
    'TRANF_CC_BYPA_00'
    'ST_IDC_CC_BYPA_00'
    'ST_CC_BYPA_00'
    'NO_CC_BYPA_00'
    };
Proxy_TxByComDISP_CC_BYPA_00Info = CreateBus(Proxy_TxByComDISP_CC_BYPA_00Info, DeList);
clear DeList;

Proxy_TxByComST_VHSSInfo = Simulink.Bus;
DeList={
    'ST_VEHSS'
    };
Proxy_TxByComST_VHSSInfo = CreateBus(Proxy_TxByComST_VHSSInfo, DeList);
clear DeList;

Proxy_TxByComDIAG_OBD_HYB_1Info = Simulink.Bus;
DeList={
    'QU_FN_OBD_1_SEN_1_ERR'
    'QU_FN_OBD_1_SEN_3_ERR'
    'QU_FN_OBD_1_SEN_4_ERR'
    'DIAG_OBD_HYB_1_MUX_MAX'
    'QU_FN_OBD_1_SEN_2_ERR'
    'DIAG_OBD_HYB_1_MUX_IMME'
    };
Proxy_TxByComDIAG_OBD_HYB_1Info = CreateBus(Proxy_TxByComDIAG_OBD_HYB_1Info, DeList);
clear DeList;

Proxy_TxByComSU_DSCInfo = Simulink.Bus;
DeList={
    'SU_DSC_WSS'
    };
Proxy_TxByComSU_DSCInfo = CreateBus(Proxy_TxByComSU_DSCInfo, DeList);
clear DeList;

Proxy_TxByComST_TYR_RDCInfo = Simulink.Bus;
DeList={
    'QU_TPL_RDC'
    'QU_FN_TYR_INFO_RDC'
    'QU_TFAI_RDC'
    };
Proxy_TxByComST_TYR_RDCInfo = CreateBus(Proxy_TxByComST_TYR_RDCInfo, DeList);
clear DeList;

Proxy_TxByComST_TYR_RPAInfo = Simulink.Bus;
DeList={
    'QU_TFAI_RPA'
    'QU_FN_TYR_INFO_RPA'
    'QU_TPL_RPA'
    };
Proxy_TxByComST_TYR_RPAInfo = CreateBus(Proxy_TxByComST_TYR_RPAInfo, DeList);
clear DeList;

Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info = Simulink.Bus;
DeList={
    'TYR_ID_2'
    'TYR_ID_1'
    'TYR_ID_3'
    'TYR_ID_4'
    };
Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info = CreateBus(Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info, DeList);
clear DeList;

Proxy_TxByComST_TYR_2Info = Simulink.Bus;
DeList={
    'QU_RDC_INIT_DISP'
    };
Proxy_TxByComST_TYR_2Info = CreateBus(Proxy_TxByComST_TYR_2Info, DeList);
clear DeList;

Proxy_TxByComAVL_T_TYRInfo = Simulink.Bus;
DeList={
    'AVL_TEMP_TYR_RRH'
    'AVL_TEMP_TYR_FLH'
    'AVL_TEMP_TYR_RLH'
    'AVL_TEMP_TYR_FRH'
    };
Proxy_TxByComAVL_T_TYRInfo = CreateBus(Proxy_TxByComAVL_T_TYRInfo, DeList);
clear DeList;

Proxy_TxByComTEMP_BRKInfo = Simulink.Bus;
DeList={
    'TEMP_BRDSK_RLH_VRFD'
    'QU_TEMP_BRDSK_RLH_VRFD'
    'TEMP_BRDSK_RRH_VRFD'
    'QU_TEMP_BRDSK_RRH_VRFD'
    };
Proxy_TxByComTEMP_BRKInfo = CreateBus(Proxy_TxByComTEMP_BRKInfo, DeList);
clear DeList;

Proxy_TxByComAVL_P_TYRInfo = Simulink.Bus;
DeList={
    'AVL_P_TYR_FRH'
    'AVL_P_TYR_FLH'
    'AVL_P_TYR_RRH'
    'AVL_P_TYR_RLH'
    };
Proxy_TxByComAVL_P_TYRInfo = CreateBus(Proxy_TxByComAVL_P_TYRInfo, DeList);
clear DeList;

Proxy_TxByComFR_DBG_DSCInfo = Simulink.Bus;
DeList={
    'DBG_RDCi_13'
    'DBG_RDCi_04'
    'DBG_RDCi_05'
    'DBG_RDCi_06'
    'DBG_RDCi_01'
    'DBG_RDCi_02'
    'DBG_RDCi_03'
    'DBG_RDCi_07'
    'DBG_RDCi_11'
    'DBG_RDCi_10'
    'DBG_RDCi_09'
    'DBG_RDCi_15'
    'DBG_RDCi_14'
    'DBG_RDCi_12'
    'DBG_RDCi_00'
    'BDTM_TA_FR'
    'BDTM_TA_RL'
    'BDTM_TA_RR'
    'DBG_RDCi_08'
    'ST_STATE'
    'BDTM_TA_FL'
    'ABSRef'
    'LOW_VOLTAGE'
    'DEBUG_PCIB'
    'DBG_DSC'
    'LATACC'
    'TRIGGER_FS'
    'TRIGGER_IS'
    };
Proxy_TxByComFR_DBG_DSCInfo = CreateBus(Proxy_TxByComFR_DBG_DSCInfo, DeList);
clear DeList;


