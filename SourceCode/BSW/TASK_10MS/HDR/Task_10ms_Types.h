/**
 * @defgroup Task_10ms_Types Task_10ms_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_10ms_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_10MS_TYPES_H_
#define TASK_10MS_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Wss_SenWhlSpdInfo_t Diag_HndlrWhlSpdInfo;
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Diag_HndlrIdbSnsrEolOfsCalcInfo;
    Proxy_RxCanRxRegenInfo_t CanM_MainCanRxRegenInfo;
    Proxy_RxCanRxAccelPedlInfo_t CanM_MainCanRxAccelPedlInfo;
    Proxy_RxCanRxIdbInfo_t CanM_MainCanRxIdbInfo;
    Proxy_RxCanRxEngTempInfo_t CanM_MainCanRxEngTempInfo;
    Proxy_RxCanRxEscInfo_t CanM_MainCanRxEscInfo;
    Wss_SenWhlSpdInfo_t CanM_MainWhlSpdInfo;
    Proxy_RxCanRxEemInfo_t CanM_MainCanRxEemInfo;
    Eem_SuspcDetnCanTimeOutStInfo_t CanM_MainCanTimeOutStInfo;
    Proxy_RxCanRxInfo_t CanM_MainCanRxInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t CanM_MainRxMsgOkFlgInfo;
    Eem_MainEemFailData_t SenPwrM_MainEemFailData;
    Proxy_RxCanRxEscInfo_t YawM_MainCanRxEscInfo;
    Proxy_RxByComRxYawSerialInfo_t YawM_MainRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t YawM_MainRxYawAccInfo;
    Proxy_RxByComRxLongAccInfo_t YawM_MainRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t YawM_MainRxMsgOkFlgInfo;
    Eem_MainEemFailData_t YawM_MainEemFailData;
    Proxy_RxCanRxEscInfo_t AyM_MainCanRxEscInfo;
    Proxy_RxByComRxYawSerialInfo_t AyM_MainRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t AyM_MainRxYawAccInfo;
    Proxy_RxByComRxLongAccInfo_t AyM_MainRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t AyM_MainRxMsgOkFlgInfo;
    Eem_MainEemFailData_t AyM_MainEemFailData;
    Proxy_RxCanRxEscInfo_t AxM_MainCanRxEscInfo;
    Proxy_RxByComRxYawSerialInfo_t AxM_MainRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t AxM_MainRxYawAccInfo;
    Proxy_RxByComRxLongAccInfo_t AxM_MainRxLongAccInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t AxM_MainRxMsgOkFlgInfo;
    Eem_MainEemFailData_t AxM_MainEemFailData;
    Proxy_RxCanRxEscInfo_t SasM_MainCanRxEscInfo;
    Proxy_RxCanRxInfo_t SasM_MainCanRxInfo;
    Proxy_RxByComRxSasInfo_t SasM_MainRxSasInfo;
    Proxy_RxByComRxMsgOkFlgInfo_t SasM_MainRxMsgOkFlgInfo;
    Eem_MainEemFailData_t SasM_MainEemFailData;
    Eem_MainEemFailData_t YawP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t YawP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t YawP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t YawP_MainWhlSpdInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t YawP_MainBaseBrkCtrlModInfo;
    Eem_MainEemSuspectData_t YawP_MainEemSuspectData;
    Eem_MainEemEceData_t YawP_MainEemEceData;
    Proxy_RxIMUCalc_t YawP_MainIMUCalcInfo;
    Wss_SenWssSpeedOut_t YawP_MainWssSpeedOut;
    Wss_SenWssCalc_t YawP_MainWssCalcInfo;
    Eem_MainEemFailData_t AyP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t AyP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t AyP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t AyP_MainWhlSpdInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t AyP_MainBaseBrkCtrlModInfo;
    Eem_MainEemSuspectData_t AyP_MainEemSuspectData;
    Eem_MainEemEceData_t AyP_MainEemEceData;
    Proxy_RxIMUCalc_t AyP_MainIMUCalcInfo;
    Wss_SenWssSpeedOut_t AyP_MainWssSpeedOut;
    Wss_SenWssCalc_t AyP_MainWssCalcInfo;
    Eem_MainEemFailData_t AxP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t AxP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t AxP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t AxP_MainWhlSpdInfo;
    Abc_CtrlAbsCtrlInfo_t AxP_MainAbsCtrlInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t AxP_MainBaseBrkCtrlModInfo;
    Eem_MainEemSuspectData_t AxP_MainEemSuspectData;
    Eem_MainEemEceData_t AxP_MainEemEceData;
    Wss_SenWssSpeedOut_t AxP_MainWssSpeedOut;
    Eem_MainEemFailData_t SasP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t SasP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t SasP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t SasP_MainWhlSpdInfo;
    Abc_CtrlAbsCtrlInfo_t SasP_MainAbsCtrlInfo;
    Abc_CtrlEscCtrlInfo_t SasP_MainEscCtrlInfo;
    Abc_CtrlTcsCtrlInfo_t SasP_MainTcsCtrlInfo;
    Eem_MainEemSuspectData_t SasP_MainEemSuspectData;
    Wss_SenWssSpeedOut_t SasP_MainWssSpeedOut;
    Ioc_InputCS1msSwtMonInfo_t SwtM_MainSwtMonInfo;
    Eem_MainEemFailData_t SwtP_MainEemFailData;
    Eem_MainEemCtrlInhibitData_t SwtP_MainEemCtrlInhibitData;
    AdcIf_Conv1msSwTrigPwrInfo_t SwtP_MainSwTrigPwrInfo;
    Pedal_SenSyncPdf5msRawInfo_t SwtP_MainPdf5msRawInfo;
    Ioc_InputCS1msSwtMonInfo_t SwtP_MainSwtMonInfo;
    Eem_MainEemSuspectData_t SwtP_MainEemSuspectData;
    Rly_ActrRlyDrvInfo_t RlyM_MainRlyDrvInfo;
    Ioc_InputCS1msRlyMonInfo_t RlyM_MainRlyMonInfo;
    Eem_MainEemCtrlInhibitData_t Proxy_TxEemCtrlInhibitData;
    Abc_CtrlCanTxInfo_t Proxy_TxCanTxInfo;
    Pedal_SenSyncPdt5msRawInfo_t Proxy_TxPdt5msRawInfo;
    Pedal_SenSyncPdf5msRawInfo_t Proxy_TxPdf5msRawInfo;
    Rbc_CtrlTarRgnBrkTqInfo_t Proxy_TxTarRgnBrkTqInfo;
    Swt_SenEscSwtStInfo_t Proxy_TxEscSwtStInfo;
    Wss_SenWhlSpdInfo_t Proxy_TxWhlSpdInfo;
    Wss_SenWhlEdgeCntInfo_t Proxy_TxWhlEdgeCntInfo;
    Press_SenSyncPistP5msRawInfo_t Proxy_TxPistP5msRawInfo;
    Nvm_HndlrLogicEepDataInfo_t Proxy_TxLogicEepDataInfo;
    Eem_MainEemLampData_t Proxy_TxEemLampData;
    Mom_HndlrEcuModeSts_t Diag_HndlrEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Diag_HndlrIgnOnOffSts;
    Ioc_InputSR1msVBatt1Mon_t Diag_HndlrVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t Diag_HndlrVBatt2Mon;
    Ioc_InputSR1msFspCbsMon_t Diag_HndlrFspCbsMon;
    Ioc_InputSR1msCEMon_t Diag_HndlrCEMon;
    Ioc_InputSR1msVddMon_t Diag_HndlrVddMon;
    Ioc_InputSR1msCspMon_t Diag_HndlrCspMon;
    Eem_SuspcDetnFuncInhibitDiagSts_t Diag_HndlrFuncInhibitDiagSts;
    Mtr_Processing_DiagMtrDiagState_t Diag_HndlrMtrDiagState;
    Mom_HndlrEcuModeSts_t CanM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t CanM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t CanM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t CanM_MainVBatt1Mon;
    Arbitrator_VlvArbVlvDriveState_t CanM_MainArbVlvDriveState;
    Ioc_InputSR1msCEMon_t CanM_MainCEMon;
    Proxy_RxCanRxGearSelDispErrInfo_t CanM_MainCanRxGearSelDispErrInfo;
    Arbitrator_MtrMtrArbDriveState_t CanM_MainMtrArbDriveState;
    Mom_HndlrEcuModeSts_t SenPwrM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SenPwrM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SenPwrM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SenPwrM_MainVBatt1Mon;
    Arbitrator_VlvArbVlvDriveState_t SenPwrM_MainArbVlvDriveState;
    Ioc_InputSR1msExt5vMon_t SenPwrM_MainExt5vMon;
    Ioc_InputSR1msCspMon_t SenPwrM_MainCspMon;
    Arbitrator_MtrMtrArbDriveState_t SenPwrM_MainMtrArbDriveState;
    Mom_HndlrEcuModeSts_t YawM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t YawM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t YawM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t YawM_MainVBatt1Mon;
    Mom_HndlrEcuModeSts_t AyM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AyM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AyM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AyM_MainVBatt1Mon;
    Mom_HndlrEcuModeSts_t AxM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AxM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AxM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AxM_MainVBatt1Mon;
    Mom_HndlrEcuModeSts_t SasM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SasM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SasM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SasM_MainVBatt1Mon;
    Mom_HndlrEcuModeSts_t YawP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t YawP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t YawP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t YawP_MainVBatt1Mon;
    Abc_CtrlVehSpd_t YawP_MainVehSpd;
    Mom_HndlrEcuModeSts_t AyP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AyP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AyP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AyP_MainVBatt1Mon;
    Abc_CtrlVehSpd_t AyP_MainVehSpd;
    Mom_HndlrEcuModeSts_t AxP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AxP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AxP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AxP_MainVBatt1Mon;
    Abc_CtrlVehSpd_t AxP_MainVehSpd;
    Mom_HndlrEcuModeSts_t SasP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SasP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SasP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SasP_MainVBatt1Mon;
    Abc_CtrlVehSpd_t SasP_MainVehSpd;
    Mom_HndlrEcuModeSts_t SwtM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SwtM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SwtM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SwtM_MainVBatt1Mon;
    Mom_HndlrEcuModeSts_t SwtP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SwtP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SwtP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SwtP_MainVBatt1Mon;
    Det_5msCtrlVehSpdFild_t SwtP_MainVehSpdFild;
    Mom_HndlrEcuModeSts_t RlyM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t RlyM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t RlyM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t RlyM_MainVBatt1Mon;
    Mom_HndlrEcuModeSts_t Proxy_TxEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Proxy_TxIgnOnOffSts;
    Eem_SuspcDetnFuncInhibitProxySts_t Proxy_TxFuncInhibitProxySts;

/* Output Data Element */
    Diag_HndlrDiagVlvActr_t Diag_HndlrDiagVlvActrInfo;
    Diag_HndlrMotReqDataDiagInfo_t Diag_HndlrMotReqDataDiagInfo;
    Diag_HndlrSasCalInfo_t Diag_HndlrSasCalInfo;
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
    Diag_HndlrDiagHndlRequest_t Diag_HndlrDiagHndlRequest;
    CanM_MainCanMonData_t CanM_MainCanMonData;
    SenPwrM_MainSenPwrMonitor_t SenPwrM_MainSenPwrMonitorData;
    YawM_MainYAWMSerialInfo_t YawM_MainYAWMSerialInfo;
    YawM_MainYAWMOutInfo_t YawM_MainYAWMOutInfo;
    AyM_MainAYMOutInfo_t AyM_MainAYMOuitInfo;
    AxM_MainAXMOutInfo_t AxM_MainAXMOutInfo;
    SasM_MainSASMOutInfo_t SasM_MainSASMOutInfo;
    YawP_MainYawPlauOutput_t YawP_MainYawPlauOutput;
    AyP_MainAyPlauOutput_t AyP_MainAyPlauOutput;
    AxP_MainAxPlauOutput_t AxP_MainAxPlauOutput;
    SasP_MainSasPlauOutput_t SasP_MainSasPlauOutput;
    SwtM_MainSwtMonFaultInfo_t SwtM_MainSwtMonFaultInfo;
    SwtP_MainSwtPFaultInfo_t SwtP_MainSwtPFaultInfo;
    RlyM_MainRelayMonitor_t RlyM_MainRelayMonitorData;
    Proxy_TxTxESCSensorInfo_t Proxy_TxTxESCSensorInfo;
    Diag_HndlrDiagClr_t Diag_HndlrDiagClrSrs;
    Diag_HndlrDiagSci_t Diag_HndlrDiagSci;
    Diag_HndlrDiagAhbSci_t Diag_HndlrDiagAhbSci;
}Task_10ms_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_10MS_TYPES_H_ */
/** @} */
