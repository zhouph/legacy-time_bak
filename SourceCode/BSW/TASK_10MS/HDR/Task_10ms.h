/**
 * @defgroup Task_10ms Task_10ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_10ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_10MS_H_
#define TASK_10MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_10ms_Types.h"
#include "Task_10ms_Cfg.h"
#include "Task_10ms_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define TASK_10MS_MODULE_ID      (0)
 #define TASK_10MS_MAJOR_VERSION  (2)
 #define TASK_10MS_MINOR_VERSION  (0)
 #define TASK_10MS_PATCH_VERSION  (0)
 #define TASK_10MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Task_10ms_HdrBusType Task_10msBus;

/* Version Info */
extern const SwcVersionInfo_t Task_10msVersionInfo;

/* Input Data Element */
extern Wss_SenWhlSpdInfo_t Task_10ms_Diag_HndlrWhlSpdInfo;
extern Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Task_10ms_Diag_HndlrIdbSnsrEolOfsCalcInfo;
extern Proxy_RxCanRxRegenInfo_t Task_10ms_CanM_MainCanRxRegenInfo;
extern Proxy_RxCanRxAccelPedlInfo_t Task_10ms_CanM_MainCanRxAccelPedlInfo;
extern Proxy_RxCanRxIdbInfo_t Task_10ms_CanM_MainCanRxIdbInfo;
extern Proxy_RxCanRxEngTempInfo_t Task_10ms_CanM_MainCanRxEngTempInfo;
extern Proxy_RxCanRxEscInfo_t Task_10ms_CanM_MainCanRxEscInfo;
extern Wss_SenWhlSpdInfo_t Task_10ms_CanM_MainWhlSpdInfo;
extern Proxy_RxCanRxEemInfo_t Task_10ms_CanM_MainCanRxEemInfo;
extern Eem_SuspcDetnCanTimeOutStInfo_t Task_10ms_CanM_MainCanTimeOutStInfo;
extern Proxy_RxCanRxInfo_t Task_10ms_CanM_MainCanRxInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_CanM_MainRxMsgOkFlgInfo;
extern Eem_MainEemFailData_t Task_10ms_SenPwrM_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_YawM_MainCanRxEscInfo;
extern Proxy_RxByComRxYawSerialInfo_t Task_10ms_YawM_MainRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t Task_10ms_YawM_MainRxYawAccInfo;
extern Proxy_RxByComRxLongAccInfo_t Task_10ms_YawM_MainRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_YawM_MainRxMsgOkFlgInfo;
extern Eem_MainEemFailData_t Task_10ms_YawM_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_AyM_MainCanRxEscInfo;
extern Proxy_RxByComRxYawSerialInfo_t Task_10ms_AyM_MainRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t Task_10ms_AyM_MainRxYawAccInfo;
extern Proxy_RxByComRxLongAccInfo_t Task_10ms_AyM_MainRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_AyM_MainRxMsgOkFlgInfo;
extern Eem_MainEemFailData_t Task_10ms_AyM_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_AxM_MainCanRxEscInfo;
extern Proxy_RxByComRxYawSerialInfo_t Task_10ms_AxM_MainRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t Task_10ms_AxM_MainRxYawAccInfo;
extern Proxy_RxByComRxLongAccInfo_t Task_10ms_AxM_MainRxLongAccInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_AxM_MainRxMsgOkFlgInfo;
extern Eem_MainEemFailData_t Task_10ms_AxM_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_SasM_MainCanRxEscInfo;
extern Proxy_RxCanRxInfo_t Task_10ms_SasM_MainCanRxInfo;
extern Proxy_RxByComRxSasInfo_t Task_10ms_SasM_MainRxSasInfo;
extern Proxy_RxByComRxMsgOkFlgInfo_t Task_10ms_SasM_MainRxMsgOkFlgInfo;
extern Eem_MainEemFailData_t Task_10ms_SasM_MainEemFailData;
extern Eem_MainEemFailData_t Task_10ms_YawP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_YawP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t Task_10ms_YawP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Task_10ms_YawP_MainWhlSpdInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t Task_10ms_YawP_MainBaseBrkCtrlModInfo;
extern Eem_MainEemSuspectData_t Task_10ms_YawP_MainEemSuspectData;
extern Eem_MainEemEceData_t Task_10ms_YawP_MainEemEceData;
extern Proxy_RxIMUCalc_t Task_10ms_YawP_MainIMUCalcInfo;
extern Wss_SenWssSpeedOut_t Task_10ms_YawP_MainWssSpeedOut;
extern Wss_SenWssCalc_t Task_10ms_YawP_MainWssCalcInfo;
extern Eem_MainEemFailData_t Task_10ms_AyP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_AyP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t Task_10ms_AyP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Task_10ms_AyP_MainWhlSpdInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t Task_10ms_AyP_MainBaseBrkCtrlModInfo;
extern Eem_MainEemSuspectData_t Task_10ms_AyP_MainEemSuspectData;
extern Eem_MainEemEceData_t Task_10ms_AyP_MainEemEceData;
extern Proxy_RxIMUCalc_t Task_10ms_AyP_MainIMUCalcInfo;
extern Wss_SenWssSpeedOut_t Task_10ms_AyP_MainWssSpeedOut;
extern Wss_SenWssCalc_t Task_10ms_AyP_MainWssCalcInfo;
extern Eem_MainEemFailData_t Task_10ms_AxP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_AxP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t Task_10ms_AxP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Task_10ms_AxP_MainWhlSpdInfo;
extern Abc_CtrlAbsCtrlInfo_t Task_10ms_AxP_MainAbsCtrlInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t Task_10ms_AxP_MainBaseBrkCtrlModInfo;
extern Eem_MainEemSuspectData_t Task_10ms_AxP_MainEemSuspectData;
extern Eem_MainEemEceData_t Task_10ms_AxP_MainEemEceData;
extern Wss_SenWssSpeedOut_t Task_10ms_AxP_MainWssSpeedOut;
extern Eem_MainEemFailData_t Task_10ms_SasP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t Task_10ms_SasP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t Task_10ms_SasP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Task_10ms_SasP_MainWhlSpdInfo;
extern Abc_CtrlAbsCtrlInfo_t Task_10ms_SasP_MainAbsCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Task_10ms_SasP_MainEscCtrlInfo;
extern Abc_CtrlTcsCtrlInfo_t Task_10ms_SasP_MainTcsCtrlInfo;
extern Eem_MainEemSuspectData_t Task_10ms_SasP_MainEemSuspectData;
extern Wss_SenWssSpeedOut_t Task_10ms_SasP_MainWssSpeedOut;
extern Ioc_InputCS1msSwtMonInfo_t Task_10ms_SwtM_MainSwtMonInfo;
extern Eem_MainEemFailData_t Task_10ms_SwtP_MainEemFailData;
extern Eem_MainEemCtrlInhibitData_t Task_10ms_SwtP_MainEemCtrlInhibitData;
extern AdcIf_Conv1msSwTrigPwrInfo_t Task_10ms_SwtP_MainSwTrigPwrInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Task_10ms_SwtP_MainPdf5msRawInfo;
extern Ioc_InputCS1msSwtMonInfo_t Task_10ms_SwtP_MainSwtMonInfo;
extern Eem_MainEemSuspectData_t Task_10ms_SwtP_MainEemSuspectData;
extern Rly_ActrRlyDrvInfo_t Task_10ms_RlyM_MainRlyDrvInfo;
extern Ioc_InputCS1msRlyMonInfo_t Task_10ms_RlyM_MainRlyMonInfo;
extern Eem_MainEemCtrlInhibitData_t Task_10ms_Proxy_TxEemCtrlInhibitData;
extern Abc_CtrlCanTxInfo_t Task_10ms_Proxy_TxCanTxInfo;
extern Pedal_SenSyncPdt5msRawInfo_t Task_10ms_Proxy_TxPdt5msRawInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Task_10ms_Proxy_TxPdf5msRawInfo;
extern Rbc_CtrlTarRgnBrkTqInfo_t Task_10ms_Proxy_TxTarRgnBrkTqInfo;
extern Swt_SenEscSwtStInfo_t Task_10ms_Proxy_TxEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Task_10ms_Proxy_TxWhlSpdInfo;
extern Wss_SenWhlEdgeCntInfo_t Task_10ms_Proxy_TxWhlEdgeCntInfo;
extern Press_SenSyncPistP5msRawInfo_t Task_10ms_Proxy_TxPistP5msRawInfo;
extern Nvm_HndlrLogicEepDataInfo_t Task_10ms_Proxy_TxLogicEepDataInfo;
extern Eem_MainEemLampData_t Task_10ms_Proxy_TxEemLampData;
extern Mom_HndlrEcuModeSts_t Task_10ms_Diag_HndlrEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_Diag_HndlrIgnOnOffSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_Diag_HndlrVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Task_10ms_Diag_HndlrVBatt2Mon;
extern Ioc_InputSR1msFspCbsMon_t Task_10ms_Diag_HndlrFspCbsMon;
extern Ioc_InputSR1msCEMon_t Task_10ms_Diag_HndlrCEMon;
extern Ioc_InputSR1msVddMon_t Task_10ms_Diag_HndlrVddMon;
extern Ioc_InputSR1msCspMon_t Task_10ms_Diag_HndlrCspMon;
extern Eem_SuspcDetnFuncInhibitDiagSts_t Task_10ms_Diag_HndlrFuncInhibitDiagSts;
extern Mtr_Processing_DiagMtrDiagState_t Task_10ms_Diag_HndlrMtrDiagState;
extern Mom_HndlrEcuModeSts_t Task_10ms_CanM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_CanM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_CanM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_CanM_MainVBatt1Mon;
extern Arbitrator_VlvArbVlvDriveState_t Task_10ms_CanM_MainArbVlvDriveState;
extern Ioc_InputSR1msCEMon_t Task_10ms_CanM_MainCEMon;
extern Proxy_RxCanRxGearSelDispErrInfo_t Task_10ms_CanM_MainCanRxGearSelDispErrInfo;
extern Arbitrator_MtrMtrArbDriveState_t Task_10ms_CanM_MainMtrArbDriveState;
extern Mom_HndlrEcuModeSts_t Task_10ms_SenPwrM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_SenPwrM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_SenPwrM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_SenPwrM_MainVBatt1Mon;
extern Arbitrator_VlvArbVlvDriveState_t Task_10ms_SenPwrM_MainArbVlvDriveState;
extern Ioc_InputSR1msExt5vMon_t Task_10ms_SenPwrM_MainExt5vMon;
extern Ioc_InputSR1msCspMon_t Task_10ms_SenPwrM_MainCspMon;
extern Arbitrator_MtrMtrArbDriveState_t Task_10ms_SenPwrM_MainMtrArbDriveState;
extern Mom_HndlrEcuModeSts_t Task_10ms_YawM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_YawM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_YawM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_YawM_MainVBatt1Mon;
extern Mom_HndlrEcuModeSts_t Task_10ms_AyM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_AyM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_AyM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_AyM_MainVBatt1Mon;
extern Mom_HndlrEcuModeSts_t Task_10ms_AxM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_AxM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_AxM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_AxM_MainVBatt1Mon;
extern Mom_HndlrEcuModeSts_t Task_10ms_SasM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_SasM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_SasM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_SasM_MainVBatt1Mon;
extern Mom_HndlrEcuModeSts_t Task_10ms_YawP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_YawP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_YawP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_YawP_MainVBatt1Mon;
extern Abc_CtrlVehSpd_t Task_10ms_YawP_MainVehSpd;
extern Mom_HndlrEcuModeSts_t Task_10ms_AyP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_AyP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_AyP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_AyP_MainVBatt1Mon;
extern Abc_CtrlVehSpd_t Task_10ms_AyP_MainVehSpd;
extern Mom_HndlrEcuModeSts_t Task_10ms_AxP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_AxP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_AxP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_AxP_MainVBatt1Mon;
extern Abc_CtrlVehSpd_t Task_10ms_AxP_MainVehSpd;
extern Mom_HndlrEcuModeSts_t Task_10ms_SasP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_SasP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_SasP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_SasP_MainVBatt1Mon;
extern Abc_CtrlVehSpd_t Task_10ms_SasP_MainVehSpd;
extern Mom_HndlrEcuModeSts_t Task_10ms_SwtM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_SwtM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_SwtM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_SwtM_MainVBatt1Mon;
extern Mom_HndlrEcuModeSts_t Task_10ms_SwtP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_SwtP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_SwtP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_SwtP_MainVBatt1Mon;
extern Det_5msCtrlVehSpdFild_t Task_10ms_SwtP_MainVehSpdFild;
extern Mom_HndlrEcuModeSts_t Task_10ms_RlyM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_RlyM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_10ms_RlyM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Task_10ms_RlyM_MainVBatt1Mon;
extern Mom_HndlrEcuModeSts_t Task_10ms_Proxy_TxEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_10ms_Proxy_TxIgnOnOffSts;
extern Eem_SuspcDetnFuncInhibitProxySts_t Task_10ms_Proxy_TxFuncInhibitProxySts;

/* Output Data Element */
extern Diag_HndlrDiagVlvActr_t Task_10ms_Diag_HndlrDiagVlvActrInfo;
extern Diag_HndlrMotReqDataDiagInfo_t Task_10ms_Diag_HndlrMotReqDataDiagInfo;
extern Diag_HndlrSasCalInfo_t Task_10ms_Diag_HndlrSasCalInfo;
extern Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Task_10ms_Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo;
extern Diag_HndlrDiagHndlRequest_t Task_10ms_Diag_HndlrDiagHndlRequest;
extern CanM_MainCanMonData_t Task_10ms_CanM_MainCanMonData;
extern SenPwrM_MainSenPwrMonitor_t Task_10ms_SenPwrM_MainSenPwrMonitorData;
extern YawM_MainYAWMSerialInfo_t Task_10ms_YawM_MainYAWMSerialInfo;
extern YawM_MainYAWMOutInfo_t Task_10ms_YawM_MainYAWMOutInfo;
extern AyM_MainAYMOutInfo_t Task_10ms_AyM_MainAYMOuitInfo;
extern AxM_MainAXMOutInfo_t Task_10ms_AxM_MainAXMOutInfo;
extern SasM_MainSASMOutInfo_t Task_10ms_SasM_MainSASMOutInfo;
extern YawP_MainYawPlauOutput_t Task_10ms_YawP_MainYawPlauOutput;
extern AyP_MainAyPlauOutput_t Task_10ms_AyP_MainAyPlauOutput;
extern AxP_MainAxPlauOutput_t Task_10ms_AxP_MainAxPlauOutput;
extern SasP_MainSasPlauOutput_t Task_10ms_SasP_MainSasPlauOutput;
extern SwtM_MainSwtMonFaultInfo_t Task_10ms_SwtM_MainSwtMonFaultInfo;
extern SwtP_MainSwtPFaultInfo_t Task_10ms_SwtP_MainSwtPFaultInfo;
extern RlyM_MainRelayMonitor_t Task_10ms_RlyM_MainRelayMonitorData;
extern Proxy_TxTxESCSensorInfo_t Task_10ms_Proxy_TxTxESCSensorInfo;
extern Diag_HndlrDiagClr_t Task_10ms_Diag_HndlrDiagClrSrs;
extern Diag_HndlrDiagSci_t Task_10ms_Diag_HndlrDiagSci;
extern Diag_HndlrDiagAhbSci_t Task_10ms_Diag_HndlrDiagAhbSci;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Task_10ms_Init(void);
extern void Task_10ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_10MS_H_ */
/** @} */
