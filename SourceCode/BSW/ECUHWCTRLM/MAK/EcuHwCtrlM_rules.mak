# \file
#
# \brief EcuHwCtrlM
#
# This file contains the implementation of the SWC
# module EcuHwCtrlM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += EcuHwCtrlM_src

EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_SRC_PATH)\EcuHwCtrlM_Main.c
EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_IFA_PATH)\EcuHwCtrlM_Main_Ifa.c
EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_CFG_PATH)\EcuHwCtrlM_Cfg.c
EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_CAL_PATH)\EcuHwCtrlM_Cal.c
EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_SRC_PATH)\EcuHwCtrlM_Process.c

ifeq ($(ICE_COMPILE),true)
EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_UNITY_PATH)\unity.c
	EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_UNITY_PATH)\unity_fixture.c	
	EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_UT_PATH)\main.c
	EcuHwCtrlM_src_FILES        += $(EcuHwCtrlM_UT_PATH)\EcuHwCtrlM_Main\EcuHwCtrlM_Main_UtMain.c
endif