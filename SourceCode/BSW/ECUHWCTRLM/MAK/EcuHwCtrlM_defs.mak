# \file
#
# \brief EcuHwCtrlM
#
# This file contains the implementation of the SWC
# module EcuHwCtrlM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

EcuHwCtrlM_CORE_PATH     := $(MANDO_BSW_ROOT)\EcuHwCtrlM
EcuHwCtrlM_CAL_PATH      := $(EcuHwCtrlM_CORE_PATH)\CAL\$(EcuHwCtrlM_VARIANT)
EcuHwCtrlM_SRC_PATH      := $(EcuHwCtrlM_CORE_PATH)\SRC
EcuHwCtrlM_CFG_PATH      := $(EcuHwCtrlM_CORE_PATH)\CFG\$(EcuHwCtrlM_VARIANT)
EcuHwCtrlM_HDR_PATH      := $(EcuHwCtrlM_CORE_PATH)\HDR
EcuHwCtrlM_IFA_PATH      := $(EcuHwCtrlM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    EcuHwCtrlM_CMN_PATH      := $(EcuHwCtrlM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	EcuHwCtrlM_UT_PATH		:= $(EcuHwCtrlM_CORE_PATH)\UT
	EcuHwCtrlM_UNITY_PATH	:= $(EcuHwCtrlM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(EcuHwCtrlM_UT_PATH)
	CC_INCLUDE_PATH		+= $(EcuHwCtrlM_UNITY_PATH)
	EcuHwCtrlM_Main_PATH 	:= EcuHwCtrlM_UT_PATH\EcuHwCtrlM_Main
endif
CC_INCLUDE_PATH    += $(EcuHwCtrlM_CAL_PATH)
CC_INCLUDE_PATH    += $(EcuHwCtrlM_SRC_PATH)
CC_INCLUDE_PATH    += $(EcuHwCtrlM_CFG_PATH)
CC_INCLUDE_PATH    += $(EcuHwCtrlM_HDR_PATH)
CC_INCLUDE_PATH    += $(EcuHwCtrlM_IFA_PATH)
CC_INCLUDE_PATH    += $(EcuHwCtrlM_CMN_PATH)

