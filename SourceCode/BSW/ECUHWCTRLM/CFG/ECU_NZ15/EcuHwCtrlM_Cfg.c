/**
 * @defgroup EcuHwCtrlM_Cfg EcuHwCtrlM_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "EcuHwCtrlM_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ECUHWCTRLM_START_SEC_CONST_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ECUHWCTRLM_STOP_SEC_CONST_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ECUHWCTRLM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ECUHWCTRLM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_START_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ECUHWCTRLM_STOP_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_START_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ECUHWCTRLM_STOP_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_START_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (32BIT)**/


#define ECUHWCTRLM_STOP_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ECUHWCTRLM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ECUHWCTRLM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_START_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ECUHWCTRLM_STOP_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_START_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ECUHWCTRLM_STOP_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_START_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (32BIT)**/


#define ECUHWCTRLM_STOP_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ECUHWCTRLM_START_SEC_CODE
#include "EcuHwCtrlM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ECUHWCTRLM_STOP_SEC_CODE
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
