/**
 * @defgroupEcuHwCtrlM_Process EcuHwCtrlM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Process.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef EcuHwCtrlM_PROCESS_H_
#define EcuHwCtrlM_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "EcuHwCtrlM_Types.h"
#include "EcuHwCtrlM_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	unsigned char Reg_Vdd1RedunMon;
	unsigned char Reg_Vdd2RedunMon;
	unsigned char Reg_Vdd3RedunMon;
	unsigned char Reg_Vdd4RedunMon;
	unsigned char Reg_Vdd5RedunMon;
}Regulator_RedundancyMon;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
#define REGULATOR_REDUN_VDDOVER	(1)
#define REGULATOR_REDUN_VDDUNDER (2)
#define REGULATOR_REDUN_VDDNORMAL (0)
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */


/* Version Info */


/* Input Data Element */


/* Output Data Element */


/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void EcuHwCtrlM_Process_Init(void);
extern void EcuHwCtrlM_Process(EcuHwCtrlM_Main_HdrBusType *pAsigMgh100Mon);
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* EcuHwCtrlM_PROCESS_H_ */
