/**
 * @defgroup EcuHwCtrlM_Main EcuHwCtrlM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "EcuHwCtrlM_Main.h"
#include "EcuHwCtrlM_Main_Ifa.h"
#include "IfxStm_reg.h"
#include "EcuHwCtrlM_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ECUHWCTRLM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ECUHWCTRLM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ECUHWCTRLM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
EcuHwCtrlM_Main_HdrBusType EcuHwCtrlM_MainBus;

/* Version Info */
const SwcVersionInfo_t EcuHwCtrlM_MainVersionInfo = 
{   
    ECUHWCTRLM_MAIN_MODULE_ID,           /* EcuHwCtrlM_MainVersionInfo.ModuleId */
    ECUHWCTRLM_MAIN_MAJOR_VERSION,       /* EcuHwCtrlM_MainVersionInfo.MajorVer */
    ECUHWCTRLM_MAIN_MINOR_VERSION,       /* EcuHwCtrlM_MainVersionInfo.MinorVer */
    ECUHWCTRLM_MAIN_PATCH_VERSION,       /* EcuHwCtrlM_MainVersionInfo.PatchVer */
    ECUHWCTRLM_MAIN_BRANCH_VERSION       /* EcuHwCtrlM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchSysPwrAsicInfo_t EcuHwCtrlM_MainAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t EcuHwCtrlM_MainAchValveAsicInfo;
Ach_InputAchAsicInvalidInfo_t EcuHwCtrlM_MainAchAsicInvalid;
Mom_HndlrEcuModeSts_t EcuHwCtrlM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t EcuHwCtrlM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t EcuHwCtrlM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t EcuHwCtrlM_MainVBatt1Mon;
Diag_HndlrDiagClr_t EcuHwCtrlM_MainDiagClrSrs;
Acm_MainAcmAsicInitCompleteFlag_t EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
Ioc_InputSR1msVdd1Mon_t EcuHwCtrlM_MainVdd1Mon;
Ioc_InputSR1msVdd2Mon_t EcuHwCtrlM_MainVdd2Mon;
Ioc_InputSR1msVdd3Mon_t EcuHwCtrlM_MainVdd3Mon;
Ioc_InputSR1msVdd4Mon_t EcuHwCtrlM_MainVdd4Mon;
Ioc_InputSR1msVdd5Mon_t EcuHwCtrlM_MainVdd5Mon;
AdcIf_Conv1msAdcInvalid_t EcuHwCtrlM_MainAdcInvalid;

/* Output Data Element */
EcuHwCtrlM_MainAsicMonFaultInfo_t EcuHwCtrlM_MainAsicMonFaultInfo;

uint32 EcuHwCtrlM_Main_Timer_Start;
uint32 EcuHwCtrlM_Main_Timer_Elapsed;

#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_MAIN_START_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (32BIT)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ECUHWCTRLM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "EcuHwCtrlM_MemMap.h"
#define ECUHWCTRLM_MAIN_START_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/** Variable Section (32BIT)**/


#define ECUHWCTRLM_MAIN_STOP_SEC_VAR_32BIT
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ECUHWCTRLM_MAIN_START_SEC_CODE
#include "EcuHwCtrlM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void EcuHwCtrlM_Main_Init(void)
{
    /* Initialize internal bus */
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchAsicInvalid = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainEcuModeSts = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainIgnOnOffSts = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainIgnEdgeSts = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVBatt1Mon = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainDiagClrSrs = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAcmAsicInitCompleteFlag = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd1Mon = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd2Mon = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd3Mon = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd4Mon = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd5Mon = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAdcInvalid = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = 0;
    EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = 0;

	EcuHwCtrlM_Process_Init();
}

void EcuHwCtrlM_Main(void)
{
    EcuHwCtrlM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchSysPwrAsicInfo);
    /*==============================================================================
    * Members of structure EcuHwCtrlM_MainAchSysPwrAsicInfo 
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
     : EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
     =============================================================================*/
    
    /* Decomposed structure interface */
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_OV(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_UV(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT);

    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchAsicInvalid(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAchAsicInvalid);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainEcuModeSts(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainEcuModeSts);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainIgnOnOffSts(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainIgnOnOffSts);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainIgnEdgeSts(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainIgnEdgeSts);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVBatt1Mon(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVBatt1Mon);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainDiagClrSrs(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainDiagClrSrs);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAcmAsicInitCompleteFlag(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAcmAsicInitCompleteFlag);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd1Mon(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd1Mon);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd2Mon(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd2Mon);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd3Mon(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd3Mon);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd4Mon(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd4Mon);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd5Mon(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainVdd5Mon);
    EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAdcInvalid(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAdcInvalid);

    /* Process */
	EcuHwCtrlM_Process(&EcuHwCtrlM_MainBus);
    /* Output */
    EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo(&EcuHwCtrlM_MainBus.EcuHwCtrlM_MainAsicMonFaultInfo);
    /*==============================================================================
    * Members of structure EcuHwCtrlM_MainAsicMonFaultInfo 
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err;
     : EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err;
     =============================================================================*/
    

    EcuHwCtrlM_Main_Timer_Elapsed = STM0_TIM0.U - EcuHwCtrlM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ECUHWCTRLM_MAIN_STOP_SEC_CODE
#include "EcuHwCtrlM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
