/**
 * @defgroup EcuHwCtrlM_Process EcuHwCtrlM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "EcuHwCtrlM_Process.h"
#include "EcuHwCtrlM_Main.h"
#include "Common.h"

/* Temp */
#include "Ioc_InputSR1ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/



/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
Regulator_RedundancyMon regulatorRedunCheck;

/*Temp IO*/
#include "Ach_Input.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
 
  void EcuHwCtrlM_Process_Init(void)
  {
	 ;
  }
 
 void EcuHwCtrlM_InhibitFlagSet(EcuHwCtrlM_Main_HdrBusType *pAsicInhibit, uint8_t ihbState)
 {
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = ihbState;
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = ihbState;
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = ihbState;
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err = ihbState;
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = ihbState;
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = ihbState;
	 pAsicInhibit->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = ihbState;
 }
 
 void EcuHwCtrlM_SpiCommCheck(EcuHwCtrlM_Main_HdrBusType *pAsicSpiComm)
 {
	 /* 100Asic MSG Num #5 : Communication Faults */
	 if((pAsicSpiComm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add==1)||(pAsicSpiComm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt==1)||\
		(pAsicSpiComm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc==1)||(pAsicSpiComm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short==1)||\
		(pAsicSpiComm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long==1)||(pAsicSpiComm->EcuHwCtrlM_MainAchAsicInvalid == 1))
	 {
		 pAsicSpiComm->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err = ERR_PREFAILED;
	 }
	 else
	 {
		 pAsicSpiComm->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err = ERR_PREPASSED;
	 }
 }
 
 void EcuHwCtrlM_RegulatorRedunCheck(EcuHwCtrlM_Main_HdrBusType *pAsicRegulatorRedun)
 {
	 /* Over Voltage Check : Range	 Under Voltage Check : Range
		 VDD1 : 6.174V (typ 5.8V)		 VDD1 : 4.5V (typ 5.8V)
	 */
	 if(Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.VddMon > U16_CALCVOLT_6V25) /* Over */
	 {
		 regulatorRedunCheck.Reg_Vdd1RedunMon = REGULATOR_REDUN_VDDOVER;
	 }
	 else if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd1Mon < U16_CALCVOLT_4V5) /* Under */
	 {
		 regulatorRedunCheck.Reg_Vdd1RedunMon = REGULATOR_REDUN_VDDUNDER;
	 }
	 else /* Normal */
	 {
		 regulatorRedunCheck.Reg_Vdd1RedunMon = REGULATOR_REDUN_VDDNORMAL;
	 }
	 /* Over Voltage Check : Range	 Under Voltage Check : Range
		 VDD2 : 5.9V (typ 5V)			 VDD2 : 4.5V (typ 5V)
	 */
 
	 if(Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon > U16_CALCVOLT_5V9) /* Over */
	 {
		 regulatorRedunCheck.Reg_Vdd2RedunMon = REGULATOR_REDUN_VDDOVER;
	 }
	 else if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd2Mon < U16_CALCVOLT_4V5) /* Under */
	 {
		 regulatorRedunCheck.Reg_Vdd2RedunMon = REGULATOR_REDUN_VDDUNDER;
	 }
	 else /* Normal */
	 {
		 regulatorRedunCheck.Reg_Vdd2RedunMon = REGULATOR_REDUN_VDDNORMAL;
	 }
	 
	 /* Over Voltage Check : Range	 Under Voltage Check : Range
		 VDD3 : 3.894V (typ 3.3V)		 VDD3 : 2.97V (typ 3.3V)
	 */
	 if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd3Mon > U16_CALCVOLT_3V9) /* Over */
	 {
		 regulatorRedunCheck.Reg_Vdd3RedunMon = REGULATOR_REDUN_VDDOVER;
	 }
	 else if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd3Mon < U16_CALCVOLT_2V9) /* Under */
	 {
		 regulatorRedunCheck.Reg_Vdd3RedunMon = REGULATOR_REDUN_VDDUNDER;
	 }
	 else /* Normal */
	 {
		 regulatorRedunCheck.Reg_Vdd3RedunMon = REGULATOR_REDUN_VDDNORMAL;
	 }
	 
	 /* Over Voltage Check : Range	 Under Voltage Check : Range
		 VDD4 : 5.9V (typ 5V)			 VDD4 : 4.5V (typ 5V)
	 */
	 if(Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Int5vMon > U16_CALCVOLT_5V9) /* Over */
	 {
		 regulatorRedunCheck.Reg_Vdd4RedunMon = REGULATOR_REDUN_VDDOVER;
	 }
	 else if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd4Mon < U16_CALCVOLT_4V5) /* Under */
	 {
		 regulatorRedunCheck.Reg_Vdd4RedunMon = REGULATOR_REDUN_VDDUNDER;
	 }
	 else /* Normal */
	 {
		 regulatorRedunCheck.Reg_Vdd4RedunMon = REGULATOR_REDUN_VDDNORMAL;
	 }
	 
	 /* Over Voltage Check : Range	 Under Voltage Check : Range
		 VDD5 : 3.894V (typ 3.3V)		 VDD5 : 2.97V (typ 3.3V)
	 */
	 if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd5Mon > U16_CALCVOLT_3V9) /* Over */
	 {
		 regulatorRedunCheck.Reg_Vdd4RedunMon = REGULATOR_REDUN_VDDOVER;
	 }
	 else if(pAsicRegulatorRedun->EcuHwCtrlM_MainVdd4Mon < U16_CALCVOLT_2V9) /* Under */
	 {
		 regulatorRedunCheck.Reg_Vdd4RedunMon = REGULATOR_REDUN_VDDUNDER;
	 }
	 else /* Normal */
	 {
		 regulatorRedunCheck.Reg_Vdd4RedunMon = REGULATOR_REDUN_VDDNORMAL;
	 }
 }
 
 void EcuHwCtrlM_RegulatorCheck(EcuHwCtrlM_Main_HdrBusType *pAsicRegulator)
 {
	 /*Over Voltage */
	 if((pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov==1))
	 {
		 pAsicRegulator->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = ERR_PREFAILED;
	 }
	 else
	 {
		 pAsicRegulator->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = ERR_PREPASSED;
	 }
	 /* Under Voltage */
	 if((pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv==1))
	 {
		 pAsicRegulator->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = ERR_PREFAILED;
	 }
	 else
	 {
		 pAsicRegulator->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = ERR_PREPASSED;
	 }
 
	 /*Over Current */
	 if((pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc==1))
	 {
		 ;
	 }
	 else
	 {
		 ;
	 }
 
	 /*Over Themal Shut Down */
	 if((pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd==1))
	 {
		 pAsicRegulator->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = ERR_PREFAILED;;
	 }
	 else
	 {
		 pAsicRegulator->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = ERR_PREPASSED;
	 }
 
	 /* etc */
	 if((pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg==1)||\
		(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr==1)||(pAsicRegulator->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg==1))
	 {
		 ;
	 }
	 else
	 {
		 ;
	 }
 }
 
 uint8_t EcuHwCtrlM_PwrCheck(EcuHwCtrlM_Main_HdrBusType *pAsicPwr)
 {
	 uint8_t u8PwrState = 0;
	 if(pAsicPwr->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv==1)
	 {
		 ;
	 }
	 else if(pAsicPwr->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov==1)
	 {
		 ;
	 }
	 else
	 {
		 ;
	 }
	 
	 if(pAsicPwr->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov==1)
	 {
		 ;
	 }
	 else if(pAsicPwr->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv==1)
	 {
		 ;
	 }
	 else
	 {
		 ;
	 }
	 
	 if(pAsicPwr->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss==1)
	 {
		 pAsicPwr->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = ERR_PREFAILED;
	 }
	 else
	 {
		 pAsicPwr->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = ERR_PREPASSED;
	 }
	 
	 if((pAsicPwr->EcuHwCtrlM_MainVBatt1Mon < U16_CALCVOLT_5V5) || (pAsicPwr->EcuHwCtrlM_MainVBatt1Mon > U16_CALCVOLT_19V0))
	 {
		 u8PwrState = 1; /* Error State */
	 }
	 else
	 {
		 u8PwrState = 0; /* Normal State */
	 }
 
	 return u8PwrState;
 }
 
 
 void EcuHwCtrlM_NvmCheck(EcuHwCtrlM_Main_HdrBusType *pAsicNvm)
 {
	 if((pAsicNvm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT==1)||(pAsicNvm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT==1)||\
		(pAsicNvm->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY==1))
	 {
		 pAsicNvm->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = ERR_PREFAILED;
	 }
	 else
	 {
		 pAsicNvm->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = ERR_PREPASSED;
	 }
 
 }
 
 void EcuHwCtrlM_OscCheck(EcuHwCtrlM_Main_HdrBusType *pAsicOsc)
 {
	 if((pAsicOsc->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON==1)||(pAsicOsc->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON==1)||\
		(pAsicOsc->EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO==1))
	 {
		 pAsicOsc->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = ERR_PREFAILED;
	 }
	 else
	 {
		 pAsicOsc->EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = ERR_PREPASSED;
	 }
 }
 
 
 void EcuHwCtrlM_Process(EcuHwCtrlM_Main_HdrBusType *pAsigMgh100Mon)
 {
	 uint8_t u8AsicInitFlag = 0;
	 uint8_t u8PwrCheckFlag = 0;
 
	 u8PwrCheckFlag = EcuHwCtrlM_PwrCheck(pAsigMgh100Mon);
	 
 /* SPI Communication Error Detection */
	 EcuHwCtrlM_SpiCommCheck(pAsigMgh100Mon);
 
 /* Regulator Redundancy Check */
	 EcuHwCtrlM_RegulatorRedunCheck(pAsigMgh100Mon);
 
 /* Asic Initialization Check */
	 u8AsicInitFlag = pAsigMgh100Mon->EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
	 if((u8AsicInitFlag == 1)&&(u8PwrCheckFlag == 0))/*Init Complete */
	 {
		 /* Asic Regulator Voltage & Redundancy Voltage Check */
		 EcuHwCtrlM_RegulatorCheck(pAsigMgh100Mon);
		 /* Asic Nvm Check */
		 EcuHwCtrlM_NvmCheck(pAsigMgh100Mon);
		 /* Asic OSC Chekc */
		 EcuHwCtrlM_OscCheck(pAsigMgh100Mon);
	 }
	 else
	 {
		 if(u8PwrCheckFlag == 0)
		 {
			 EcuHwCtrlM_InhibitFlagSet(pAsigMgh100Mon,ERR_INHIBIT); /* All Inhibit Set */	 
		 }
		 else
		 {
			 EcuHwCtrlM_InhibitFlagSet(pAsigMgh100Mon,ERR_INHIBIT); /* All Inhibit Set */
		 }
	 }
 }

