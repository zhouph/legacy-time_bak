#include "unity.h"
#include "unity_fixture.h"
#include "EcuHwCtrlM_Main.h"
#include "EcuHwCtrlM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_OVC;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_OUT_OF_REG;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OVC;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_OVC;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_DIODE_LOSS;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_REV_CURR;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_T_SD;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_T_SD;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VINT_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VINT_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_DGNDLOSS;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VPWR_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VPWR_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_LONG;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_SHORT;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_ADD;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_FCNT;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_CRC;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_T_SD;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_OUT_OF_REG;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_OSC_STUCK_MON[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_3_OSC_STUCK_MON;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_OSC_FRQ_MON[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_3_OSC_FRQ_MON;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_WDOG_TO[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_3_WDOG_TO;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_AN_TRIM_CRC_RESULT[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_7_AN_TRIM_CRC_RESULT;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_NVM_CRC_RESULT[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_7_NVM_CRC_RESULT;
const Haluint8 UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_NVM_BUSY[MAX_STEP] = ECUHWCTRLM_MAINACHSYSPWRASICINFO_ACH_ASIC_7_NVM_BUSY;
const Haluint8 UtInput_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_OV[MAX_STEP] = ECUHWCTRLM_MAINACHVALVEASICINFO_ACH_ASIC_CP_OV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_UV[MAX_STEP] = ECUHWCTRLM_MAINACHVALVEASICINFO_ACH_ASIC_CP_UV;
const Haluint8 UtInput_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT[MAX_STEP] = ECUHWCTRLM_MAINACHVALVEASICINFO_ACH_ASIC_T_SD_INT;
const Ach_InputAchAsicInvalidInfo_t UtInput_EcuHwCtrlM_MainAchAsicInvalid[MAX_STEP] = ECUHWCTRLM_MAINACHASICINVALID;
const Mom_HndlrEcuModeSts_t UtInput_EcuHwCtrlM_MainEcuModeSts[MAX_STEP] = ECUHWCTRLM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_EcuHwCtrlM_MainIgnOnOffSts[MAX_STEP] = ECUHWCTRLM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_EcuHwCtrlM_MainIgnEdgeSts[MAX_STEP] = ECUHWCTRLM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_EcuHwCtrlM_MainVBatt1Mon[MAX_STEP] = ECUHWCTRLM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_EcuHwCtrlM_MainDiagClrSrs[MAX_STEP] = ECUHWCTRLM_MAINDIAGCLRSRS;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_EcuHwCtrlM_MainAcmAsicInitCompleteFlag[MAX_STEP] = ECUHWCTRLM_MAINACMASICINITCOMPLETEFLAG;
const Ioc_InputSR1msVdd1Mon_t UtInput_EcuHwCtrlM_MainVdd1Mon[MAX_STEP] = ECUHWCTRLM_MAINVDD1MON;
const Ioc_InputSR1msVdd2Mon_t UtInput_EcuHwCtrlM_MainVdd2Mon[MAX_STEP] = ECUHWCTRLM_MAINVDD2MON;
const Ioc_InputSR1msVdd3Mon_t UtInput_EcuHwCtrlM_MainVdd3Mon[MAX_STEP] = ECUHWCTRLM_MAINVDD3MON;
const Ioc_InputSR1msVdd4Mon_t UtInput_EcuHwCtrlM_MainVdd4Mon[MAX_STEP] = ECUHWCTRLM_MAINVDD4MON;
const Ioc_InputSR1msVdd5Mon_t UtInput_EcuHwCtrlM_MainVdd5Mon[MAX_STEP] = ECUHWCTRLM_MAINVDD5MON;
const AdcIf_Conv1msAdcInvalid_t UtInput_EcuHwCtrlM_MainAdcInvalid[MAX_STEP] = ECUHWCTRLM_MAINADCINVALID;

const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICOVERVOLT_ERR;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err_Info[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICOVERVOLT_ERR_INFO;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICUNDERVOLT_ERR;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err_Info[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICUNDERVOLT_ERR_INFO;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICOVERTEMP_ERR;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err_Info[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICOVERTEMP_ERR_INFO;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicComm_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICCOMM_ERR;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOsc_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICOSC_ERR;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicNvm_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICNVM_ERR;
const Saluint8 UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicGroundLoss_Err[MAX_STEP] = ECUHWCTRLM_MAINASICMONFAULTINFO_ASICM_ASICGROUNDLOSS_ERR;



TEST_GROUP(EcuHwCtrlM_Main);
TEST_SETUP(EcuHwCtrlM_Main)
{
    EcuHwCtrlM_Main_Init();
}

TEST_TEAR_DOWN(EcuHwCtrlM_Main)
{   /* Postcondition */

}

TEST(EcuHwCtrlM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_OSC_STUCK_MON[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_OSC_FRQ_MON[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_WDOG_TO[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_AN_TRIM_CRC_RESULT[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_NVM_CRC_RESULT[i];
        EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY = UtInput_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_NVM_BUSY[i];
        EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV = UtInput_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_OV[i];
        EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV = UtInput_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_UV[i];
        EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = UtInput_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT[i];
        EcuHwCtrlM_MainAchAsicInvalid = UtInput_EcuHwCtrlM_MainAchAsicInvalid[i];
        EcuHwCtrlM_MainEcuModeSts = UtInput_EcuHwCtrlM_MainEcuModeSts[i];
        EcuHwCtrlM_MainIgnOnOffSts = UtInput_EcuHwCtrlM_MainIgnOnOffSts[i];
        EcuHwCtrlM_MainIgnEdgeSts = UtInput_EcuHwCtrlM_MainIgnEdgeSts[i];
        EcuHwCtrlM_MainVBatt1Mon = UtInput_EcuHwCtrlM_MainVBatt1Mon[i];
        EcuHwCtrlM_MainDiagClrSrs = UtInput_EcuHwCtrlM_MainDiagClrSrs[i];
        EcuHwCtrlM_MainAcmAsicInitCompleteFlag = UtInput_EcuHwCtrlM_MainAcmAsicInitCompleteFlag[i];
        EcuHwCtrlM_MainVdd1Mon = UtInput_EcuHwCtrlM_MainVdd1Mon[i];
        EcuHwCtrlM_MainVdd2Mon = UtInput_EcuHwCtrlM_MainVdd2Mon[i];
        EcuHwCtrlM_MainVdd3Mon = UtInput_EcuHwCtrlM_MainVdd3Mon[i];
        EcuHwCtrlM_MainVdd4Mon = UtInput_EcuHwCtrlM_MainVdd4Mon[i];
        EcuHwCtrlM_MainVdd5Mon = UtInput_EcuHwCtrlM_MainVdd5Mon[i];
        EcuHwCtrlM_MainAdcInvalid = UtInput_EcuHwCtrlM_MainAdcInvalid[i];

        EcuHwCtrlM_Main();

        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err_Info[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err_Info[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err_Info[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicComm_Err[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOsc_Err[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicNvm_Err[i]);
        TEST_ASSERT_EQUAL(EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err, UtExpected_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicGroundLoss_Err[i]);
    }
}

TEST_GROUP_RUNNER(EcuHwCtrlM_Main)
{
    RUN_TEST_CASE(EcuHwCtrlM_Main, All);
}
