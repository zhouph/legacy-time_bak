/**
 * @defgroup EcuHwCtrlM_Main_Ifa EcuHwCtrlM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ECUHWCTRLM_MAIN_IFA_H_
#define ECUHWCTRLM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo(data) do \
{ \
    *data = EcuHwCtrlM_MainAchValveAsicInfo; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_OSC_STUCK_MON(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_OSC_FRQ_MON(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_3_WDOG_TO(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_AN_TRIM_CRC_RESULT(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_NVM_CRC_RESULT(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchSysPwrAsicInfo_Ach_Asic_7_NVM_BUSY(data) do \
{ \
    *data = EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_OV(data) do \
{ \
    *data = EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_CP_UV(data) do \
{ \
    *data = EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchValveAsicInfo_Ach_Asic_T_SD_INT(data) do \
{ \
    *data = EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAchAsicInvalid(data) do \
{ \
    *data = EcuHwCtrlM_MainAchAsicInvalid; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainEcuModeSts(data) do \
{ \
    *data = EcuHwCtrlM_MainEcuModeSts; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainIgnOnOffSts(data) do \
{ \
    *data = EcuHwCtrlM_MainIgnOnOffSts; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainIgnEdgeSts(data) do \
{ \
    *data = EcuHwCtrlM_MainIgnEdgeSts; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVBatt1Mon(data) do \
{ \
    *data = EcuHwCtrlM_MainVBatt1Mon; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainDiagClrSrs(data) do \
{ \
    *data = EcuHwCtrlM_MainDiagClrSrs; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAcmAsicInitCompleteFlag(data) do \
{ \
    *data = EcuHwCtrlM_MainAcmAsicInitCompleteFlag; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd1Mon(data) do \
{ \
    *data = EcuHwCtrlM_MainVdd1Mon; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd2Mon(data) do \
{ \
    *data = EcuHwCtrlM_MainVdd2Mon; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd3Mon(data) do \
{ \
    *data = EcuHwCtrlM_MainVdd3Mon; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd4Mon(data) do \
{ \
    *data = EcuHwCtrlM_MainVdd4Mon; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainVdd5Mon(data) do \
{ \
    *data = EcuHwCtrlM_MainVdd5Mon; \
}while(0);

#define EcuHwCtrlM_Main_Read_EcuHwCtrlM_MainAdcInvalid(data) do \
{ \
    *data = EcuHwCtrlM_MainAdcInvalid; \
}while(0);


/* Set Output DE MAcro Function */
#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverVolt_Err_Info(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicUnderVolt_Err_Info(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOverTemp_Err_Info(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicComm_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicOsc_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicNvm_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err = *data; \
}while(0);

#define EcuHwCtrlM_Main_Write_EcuHwCtrlM_MainAsicMonFaultInfo_ASICM_AsicGroundLoss_Err(data) do \
{ \
    EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ECUHWCTRLM_MAIN_IFA_H_ */
/** @} */
