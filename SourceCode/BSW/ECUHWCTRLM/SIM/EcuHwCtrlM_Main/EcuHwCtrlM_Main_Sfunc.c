#define S_FUNCTION_NAME      EcuHwCtrlM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          52
#define WidthOutputPort         10

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "EcuHwCtrlM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = input[0];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = input[1];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = input[2];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = input[3];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = input[4];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = input[5];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = input[6];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = input[7];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = input[8];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = input[9];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = input[10];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = input[11];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = input[12];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = input[13];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = input[14];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = input[15];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = input[16];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = input[17];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = input[18];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = input[19];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = input[20];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = input[21];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = input[22];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = input[23];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = input[24];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = input[25];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = input[26];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = input[27];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = input[28];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = input[29];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON = input[30];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON = input[31];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO = input[32];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT = input[33];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT = input[34];
    EcuHwCtrlM_MainAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY = input[35];
    EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_OV = input[36];
    EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_CP_UV = input[37];
    EcuHwCtrlM_MainAchValveAsicInfo.Ach_Asic_T_SD_INT = input[38];
    EcuHwCtrlM_MainAchAsicInvalid = input[39];
    EcuHwCtrlM_MainEcuModeSts = input[40];
    EcuHwCtrlM_MainIgnOnOffSts = input[41];
    EcuHwCtrlM_MainIgnEdgeSts = input[42];
    EcuHwCtrlM_MainVBatt1Mon = input[43];
    EcuHwCtrlM_MainDiagClrSrs = input[44];
    EcuHwCtrlM_MainAcmAsicInitCompleteFlag = input[45];
    EcuHwCtrlM_MainVdd1Mon = input[46];
    EcuHwCtrlM_MainVdd2Mon = input[47];
    EcuHwCtrlM_MainVdd3Mon = input[48];
    EcuHwCtrlM_MainVdd4Mon = input[49];
    EcuHwCtrlM_MainVdd5Mon = input[50];
    EcuHwCtrlM_MainAdcInvalid = input[51];

    EcuHwCtrlM_Main();


    output[0] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err;
    output[1] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverVolt_Err_Info;
    output[2] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err;
    output[3] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicUnderVolt_Err_Info;
    output[4] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err;
    output[5] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOverTemp_Err_Info;
    output[6] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicComm_Err;
    output[7] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicOsc_Err;
    output[8] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicNvm_Err;
    output[9] = EcuHwCtrlM_MainAsicMonFaultInfo.ASICM_AsicGroundLoss_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    EcuHwCtrlM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
