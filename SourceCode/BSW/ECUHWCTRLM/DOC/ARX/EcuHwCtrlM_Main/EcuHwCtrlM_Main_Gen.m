EcuHwCtrlM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
EcuHwCtrlM_MainAchSysPwrAsicInfo = CreateBus(EcuHwCtrlM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

EcuHwCtrlM_MainAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_T_SD_INT'
    };
EcuHwCtrlM_MainAchValveAsicInfo = CreateBus(EcuHwCtrlM_MainAchValveAsicInfo, DeList);
clear DeList;

EcuHwCtrlM_MainAchAsicInvalid = Simulink.Bus;
DeList={'EcuHwCtrlM_MainAchAsicInvalid'};
EcuHwCtrlM_MainAchAsicInvalid = CreateBus(EcuHwCtrlM_MainAchAsicInvalid, DeList);
clear DeList;

EcuHwCtrlM_MainEcuModeSts = Simulink.Bus;
DeList={'EcuHwCtrlM_MainEcuModeSts'};
EcuHwCtrlM_MainEcuModeSts = CreateBus(EcuHwCtrlM_MainEcuModeSts, DeList);
clear DeList;

EcuHwCtrlM_MainIgnOnOffSts = Simulink.Bus;
DeList={'EcuHwCtrlM_MainIgnOnOffSts'};
EcuHwCtrlM_MainIgnOnOffSts = CreateBus(EcuHwCtrlM_MainIgnOnOffSts, DeList);
clear DeList;

EcuHwCtrlM_MainIgnEdgeSts = Simulink.Bus;
DeList={'EcuHwCtrlM_MainIgnEdgeSts'};
EcuHwCtrlM_MainIgnEdgeSts = CreateBus(EcuHwCtrlM_MainIgnEdgeSts, DeList);
clear DeList;

EcuHwCtrlM_MainVBatt1Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVBatt1Mon'};
EcuHwCtrlM_MainVBatt1Mon = CreateBus(EcuHwCtrlM_MainVBatt1Mon, DeList);
clear DeList;

EcuHwCtrlM_MainDiagClrSrs = Simulink.Bus;
DeList={'EcuHwCtrlM_MainDiagClrSrs'};
EcuHwCtrlM_MainDiagClrSrs = CreateBus(EcuHwCtrlM_MainDiagClrSrs, DeList);
clear DeList;

EcuHwCtrlM_MainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'EcuHwCtrlM_MainAcmAsicInitCompleteFlag'};
EcuHwCtrlM_MainAcmAsicInitCompleteFlag = CreateBus(EcuHwCtrlM_MainAcmAsicInitCompleteFlag, DeList);
clear DeList;

EcuHwCtrlM_MainVdd1Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd1Mon'};
EcuHwCtrlM_MainVdd1Mon = CreateBus(EcuHwCtrlM_MainVdd1Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd2Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd2Mon'};
EcuHwCtrlM_MainVdd2Mon = CreateBus(EcuHwCtrlM_MainVdd2Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd3Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd3Mon'};
EcuHwCtrlM_MainVdd3Mon = CreateBus(EcuHwCtrlM_MainVdd3Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd4Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd4Mon'};
EcuHwCtrlM_MainVdd4Mon = CreateBus(EcuHwCtrlM_MainVdd4Mon, DeList);
clear DeList;

EcuHwCtrlM_MainVdd5Mon = Simulink.Bus;
DeList={'EcuHwCtrlM_MainVdd5Mon'};
EcuHwCtrlM_MainVdd5Mon = CreateBus(EcuHwCtrlM_MainVdd5Mon, DeList);
clear DeList;

EcuHwCtrlM_MainAdcInvalid = Simulink.Bus;
DeList={'EcuHwCtrlM_MainAdcInvalid'};
EcuHwCtrlM_MainAdcInvalid = CreateBus(EcuHwCtrlM_MainAdcInvalid, DeList);
clear DeList;

EcuHwCtrlM_MainAsicMonFaultInfo = Simulink.Bus;
DeList={
    'ASICM_AsicOverVolt_Err'
    'ASICM_AsicOverVolt_Err_Info'
    'ASICM_AsicUnderVolt_Err'
    'ASICM_AsicUnderVolt_Err_Info'
    'ASICM_AsicOverTemp_Err'
    'ASICM_AsicOverTemp_Err_Info'
    'ASICM_AsicComm_Err'
    'ASICM_AsicOsc_Err'
    'ASICM_AsicNvm_Err'
    'ASICM_AsicGroundLoss_Err'
    };
EcuHwCtrlM_MainAsicMonFaultInfo = CreateBus(EcuHwCtrlM_MainAsicMonFaultInfo, DeList);
clear DeList;

