/**
 * @defgroup EcuHwCtrlM_Main EcuHwCtrlM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ECUHWCTRLM_MAIN_H_
#define ECUHWCTRLM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "EcuHwCtrlM_Types.h"
#include "EcuHwCtrlM_Cfg.h"
#include "EcuHwCtrlM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ECUHWCTRLM_MAIN_MODULE_ID      (0)
 #define ECUHWCTRLM_MAIN_MAJOR_VERSION  (2)
 #define ECUHWCTRLM_MAIN_MINOR_VERSION  (0)
 #define ECUHWCTRLM_MAIN_PATCH_VERSION  (0)
 #define ECUHWCTRLM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern EcuHwCtrlM_Main_HdrBusType EcuHwCtrlM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t EcuHwCtrlM_MainVersionInfo;

/* Input Data Element */
extern Ach_InputAchSysPwrAsicInfo_t EcuHwCtrlM_MainAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t EcuHwCtrlM_MainAchValveAsicInfo;
extern Ach_InputAchAsicInvalidInfo_t EcuHwCtrlM_MainAchAsicInvalid;
extern Mom_HndlrEcuModeSts_t EcuHwCtrlM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t EcuHwCtrlM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t EcuHwCtrlM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t EcuHwCtrlM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t EcuHwCtrlM_MainDiagClrSrs;
extern Acm_MainAcmAsicInitCompleteFlag_t EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
extern Ioc_InputSR1msVdd1Mon_t EcuHwCtrlM_MainVdd1Mon;
extern Ioc_InputSR1msVdd2Mon_t EcuHwCtrlM_MainVdd2Mon;
extern Ioc_InputSR1msVdd3Mon_t EcuHwCtrlM_MainVdd3Mon;
extern Ioc_InputSR1msVdd4Mon_t EcuHwCtrlM_MainVdd4Mon;
extern Ioc_InputSR1msVdd5Mon_t EcuHwCtrlM_MainVdd5Mon;
extern AdcIf_Conv1msAdcInvalid_t EcuHwCtrlM_MainAdcInvalid;

/* Output Data Element */
extern EcuHwCtrlM_MainAsicMonFaultInfo_t EcuHwCtrlM_MainAsicMonFaultInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void EcuHwCtrlM_Main_Init(void);
extern void EcuHwCtrlM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ECUHWCTRLM_MAIN_H_ */
/** @} */
