/**
 * @defgroup EcuHwCtrlM_Types EcuHwCtrlM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        EcuHwCtrlM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ECUHWCTRLM_TYPES_H_
#define ECUHWCTRLM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ach_InputAchSysPwrAsicInfo_t EcuHwCtrlM_MainAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t EcuHwCtrlM_MainAchValveAsicInfo;
    Ach_InputAchAsicInvalidInfo_t EcuHwCtrlM_MainAchAsicInvalid;
    Mom_HndlrEcuModeSts_t EcuHwCtrlM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t EcuHwCtrlM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t EcuHwCtrlM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t EcuHwCtrlM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t EcuHwCtrlM_MainDiagClrSrs;
    Acm_MainAcmAsicInitCompleteFlag_t EcuHwCtrlM_MainAcmAsicInitCompleteFlag;
    Ioc_InputSR1msVdd1Mon_t EcuHwCtrlM_MainVdd1Mon;
    Ioc_InputSR1msVdd2Mon_t EcuHwCtrlM_MainVdd2Mon;
    Ioc_InputSR1msVdd3Mon_t EcuHwCtrlM_MainVdd3Mon;
    Ioc_InputSR1msVdd4Mon_t EcuHwCtrlM_MainVdd4Mon;
    Ioc_InputSR1msVdd5Mon_t EcuHwCtrlM_MainVdd5Mon;
    AdcIf_Conv1msAdcInvalid_t EcuHwCtrlM_MainAdcInvalid;

/* Output Data Element */
    EcuHwCtrlM_MainAsicMonFaultInfo_t EcuHwCtrlM_MainAsicMonFaultInfo;
}EcuHwCtrlM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ECUHWCTRLM_TYPES_H_ */
/** @} */
