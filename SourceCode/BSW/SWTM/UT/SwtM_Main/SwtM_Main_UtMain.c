#include "unity.h"
#include "unity_fixture.h"
#include "SwtM_Main.h"
#include "SwtM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_SwtM_MainSwtMonInfo_AvhSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_AVHSWTMON;
const Haluint8 UtInput_SwtM_MainSwtMonInfo_BflSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_BFLSWTMON;
const Haluint8 UtInput_SwtM_MainSwtMonInfo_BlsSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_BLSSWTMON;
const Haluint8 UtInput_SwtM_MainSwtMonInfo_BsSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_BSSWTMON;
const Haluint8 UtInput_SwtM_MainSwtMonInfo_EscSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_ESCSWTMON;
const Haluint8 UtInput_SwtM_MainSwtMonInfo_HdcSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_HDCSWTMON;
const Haluint8 UtInput_SwtM_MainSwtMonInfo_PbSwtMon[MAX_STEP] = SWTM_MAINSWTMONINFO_PBSWTMON;
const Mom_HndlrEcuModeSts_t UtInput_SwtM_MainEcuModeSts[MAX_STEP] = SWTM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_SwtM_MainIgnOnOffSts[MAX_STEP] = SWTM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_SwtM_MainIgnEdgeSts[MAX_STEP] = SWTM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_SwtM_MainVBatt1Mon[MAX_STEP] = SWTM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_SwtM_MainDiagClrSrs[MAX_STEP] = SWTM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err[MAX_STEP] = SWTM_MAINSWTMONFAULTINFO_SWM_ESCOFF_SW_ERR;
const Saluint8 UtExpected_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err[MAX_STEP] = SWTM_MAINSWTMONFAULTINFO_SWM_HDC_SW_ERR;
const Saluint8 UtExpected_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err[MAX_STEP] = SWTM_MAINSWTMONFAULTINFO_SWM_AVH_SW_ERR;
const Saluint8 UtExpected_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err[MAX_STEP] = SWTM_MAINSWTMONFAULTINFO_SWM_BFL_SW_ERR;
const Saluint8 UtExpected_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err[MAX_STEP] = SWTM_MAINSWTMONFAULTINFO_SWM_PB_SW_ERR;



TEST_GROUP(SwtM_Main);
TEST_SETUP(SwtM_Main)
{
    SwtM_Main_Init();
}

TEST_TEAR_DOWN(SwtM_Main)
{   /* Postcondition */

}

TEST(SwtM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SwtM_MainSwtMonInfo.AvhSwtMon = UtInput_SwtM_MainSwtMonInfo_AvhSwtMon[i];
        SwtM_MainSwtMonInfo.BflSwtMon = UtInput_SwtM_MainSwtMonInfo_BflSwtMon[i];
        SwtM_MainSwtMonInfo.BlsSwtMon = UtInput_SwtM_MainSwtMonInfo_BlsSwtMon[i];
        SwtM_MainSwtMonInfo.BsSwtMon = UtInput_SwtM_MainSwtMonInfo_BsSwtMon[i];
        SwtM_MainSwtMonInfo.EscSwtMon = UtInput_SwtM_MainSwtMonInfo_EscSwtMon[i];
        SwtM_MainSwtMonInfo.HdcSwtMon = UtInput_SwtM_MainSwtMonInfo_HdcSwtMon[i];
        SwtM_MainSwtMonInfo.PbSwtMon = UtInput_SwtM_MainSwtMonInfo_PbSwtMon[i];
        SwtM_MainEcuModeSts = UtInput_SwtM_MainEcuModeSts[i];
        SwtM_MainIgnOnOffSts = UtInput_SwtM_MainIgnOnOffSts[i];
        SwtM_MainIgnEdgeSts = UtInput_SwtM_MainIgnEdgeSts[i];
        SwtM_MainVBatt1Mon = UtInput_SwtM_MainVBatt1Mon[i];
        SwtM_MainDiagClrSrs = UtInput_SwtM_MainDiagClrSrs[i];

        SwtM_Main();

        TEST_ASSERT_EQUAL(SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err, UtExpected_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err[i]);
        TEST_ASSERT_EQUAL(SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err, UtExpected_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err[i]);
        TEST_ASSERT_EQUAL(SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err, UtExpected_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err[i]);
        TEST_ASSERT_EQUAL(SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err, UtExpected_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err[i]);
        TEST_ASSERT_EQUAL(SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err, UtExpected_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err[i]);
    }
}

TEST_GROUP_RUNNER(SwtM_Main)
{
    RUN_TEST_CASE(SwtM_Main, All);
}
