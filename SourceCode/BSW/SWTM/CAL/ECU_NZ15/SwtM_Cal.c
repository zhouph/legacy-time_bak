/**
 * @defgroup SwtM_Cal SwtM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWTM_START_SEC_CALIB_UNSPECIFIED
#include "SwtM_MemMap.h"
/* Global Calibration Section */


#define SWTM_STOP_SEC_CALIB_UNSPECIFIED
#include "SwtM_MemMap.h"

#define SWTM_START_SEC_CONST_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWTM_STOP_SEC_CONST_UNSPECIFIED
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTM_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTM_STOP_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTM_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTM_STOP_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWTM_START_SEC_CODE
#include "SwtM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWTM_STOP_SEC_CODE
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
