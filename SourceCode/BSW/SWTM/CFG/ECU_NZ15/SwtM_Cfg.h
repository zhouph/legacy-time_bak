/**
 * @defgroup SwtM_Cfg SwtM_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTM_CFG_H_
#define SWTM_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtM_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 
/* Switch type */
#define SWTM_NONE           (0)
#define SWTM_ANALOG_TYPE    (1)
#define SWTM_CAN_TYPE       (2)

/* Switch variant */
#define SWTM_HKMC          	(1)
#define SWTM_VOLVO_V40		(2)

/*Runable cycle time(ms)*/
#define SWTM_CYCLE_TIME		(10)

/*Fault detecting time(sec)*/
#define SWTM_ESCOFF_SWITCH_ERR_TIME (60000) /* 60 sec */
#define SWTM_HDC_SWITCH_ERR_TIME    (60000) /* 60 sec */
#define SWTM_AVH_SWITCH_ERR_TIME    (60000) /* 60 sec */

#define SWTM_BFL_SWITCH_ERR_TIME	(1000)  /*1 sec*/

/*Ignition power*/
#define SWTM_7V0			(7000)
#define SWTM_17V0			(17000)

/*Switch State*/
#define SWTM_ON				(1)
#define SWTM_OFF			(0)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
	{
		uint8 SwtM_escoff_sw_type;	   
		uint8 SwtM_escoff_sw_variant;  
	
		uint8 SwtM_hdc_sw_type;
		uint8 SwtM_hdc_sw_variant;
	
		uint8 SwtM_avh_sw_type;
		uint8 SwtM_avh_sw_variant;
	
		uint8 SwtM_bls_sw_type;
		uint8 SwtM_bls_sw_variant;

		uint8 SwtM_bs_sw_type;
		uint8 SwtM_bs_sw_variant;

		uint8 SwtM_bfl_sw_type;
		uint8 SwtM_bfl_sw_variant;

		uint8 SwtM_pb_sw_type;
		uint8 SwtM_pb_sw_variant;
	
		uint8 SwtM_clutch_sw_type;
		uint8 SwtM_clutch_sw_variant;
	
		uint8 SwtM_gearR_sw_type;
		uint8 SwtM_gearR_sw_variant;
} SwtMCfg;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SwtM_InitCfg(void);
extern SwtMCfg* SwtM_GetCfg(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTM_CFG_H_ */
/** @} */
