/**
 * @defgroup SwtM_Cfg SwtM_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtM_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWTM_START_SEC_CONST_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWTM_STOP_SEC_CONST_UNSPECIFIED
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTM_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTM_STOP_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTM_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTM_STOP_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_START_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTM_STOP_SEC_VAR_32BIT
#include "SwtM_MemMap.h"

static SwtMCfg SwtM_Cfg;


/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWTM_START_SEC_CODE
#include "SwtM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void SwtM_InitCfg(void)
{
    /* TODO */
    SwtM_Cfg.SwtM_escoff_sw_type    = SWTM_ANALOG_TYPE;
    SwtM_Cfg.SwtM_escoff_sw_variant = SWTM_HKMC; 

    SwtM_Cfg.SwtM_hdc_sw_type       = SWTM_ANALOG_TYPE;
    SwtM_Cfg.SwtM_hdc_sw_variant    = SWTM_HKMC;

    SwtM_Cfg.SwtM_avh_sw_type       = SWTM_ANALOG_TYPE;
    SwtM_Cfg.SwtM_avh_sw_variant    = SWTM_HKMC;

    SwtM_Cfg.SwtM_bls_sw_type       = SWTM_ANALOG_TYPE;
    SwtM_Cfg.SwtM_bls_sw_variant    = SWTM_HKMC;

    SwtM_Cfg.SwtM_bs_sw_type      	= SWTM_ANALOG_TYPE;
    SwtM_Cfg.SwtM_bs_sw_variant    	= SWTM_HKMC;

    SwtM_Cfg.SwtM_bfl_sw_type       = SWTM_CAN_TYPE; 
    SwtM_Cfg.SwtM_bfl_sw_variant    = SWTM_HKMC;

    SwtM_Cfg.SwtM_pb_sw_type        = SWTM_NONE;
    SwtM_Cfg.SwtM_pb_sw_variant     = SWTM_NONE;
}


SwtMCfg* SwtM_GetCfg(void)
{
    return &SwtM_Cfg;
}


/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWTM_STOP_SEC_CODE
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
