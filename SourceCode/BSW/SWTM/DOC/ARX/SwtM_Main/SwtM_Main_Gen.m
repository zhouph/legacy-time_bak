SwtM_MainSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BflSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'EscSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
SwtM_MainSwtMonInfo = CreateBus(SwtM_MainSwtMonInfo, DeList);
clear DeList;

SwtM_MainEcuModeSts = Simulink.Bus;
DeList={'SwtM_MainEcuModeSts'};
SwtM_MainEcuModeSts = CreateBus(SwtM_MainEcuModeSts, DeList);
clear DeList;

SwtM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SwtM_MainIgnOnOffSts'};
SwtM_MainIgnOnOffSts = CreateBus(SwtM_MainIgnOnOffSts, DeList);
clear DeList;

SwtM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SwtM_MainIgnEdgeSts'};
SwtM_MainIgnEdgeSts = CreateBus(SwtM_MainIgnEdgeSts, DeList);
clear DeList;

SwtM_MainVBatt1Mon = Simulink.Bus;
DeList={'SwtM_MainVBatt1Mon'};
SwtM_MainVBatt1Mon = CreateBus(SwtM_MainVBatt1Mon, DeList);
clear DeList;

SwtM_MainDiagClrSrs = Simulink.Bus;
DeList={'SwtM_MainDiagClrSrs'};
SwtM_MainDiagClrSrs = CreateBus(SwtM_MainDiagClrSrs, DeList);
clear DeList;

SwtM_MainSwtMonFaultInfo = Simulink.Bus;
DeList={
    'SWM_ESCOFF_Sw_Err'
    'SWM_HDC_Sw_Err'
    'SWM_AVH_Sw_Err'
    'SWM_BFL_Sw_Err'
    'SWM_PB_Sw_Err'
    };
SwtM_MainSwtMonFaultInfo = CreateBus(SwtM_MainSwtMonFaultInfo, DeList);
clear DeList;

