# \file
#
# \brief AUTOSAR ApplTemplates
#
# This file contains the implementation of the AUTOSAR
# module ApplTemplates.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# BOARD:
# This variable defines the used hardware (it is called board because
# of the usage of evaluation boards).
#
# For example:
# BOARD = eva168_2
#
BOARD       ?= TriboardTC277

# TARGET:
# default target of this project
TARGET      ?= TRICORE
DERIVATE    ?= TC277

# COMPIlER:
# The build structure of the SSC allows to switch the compiler by
# changing the used compiler plugin. A compiler plugin is a set
# of makefile that implements the "Compiler Makefile Interface".
# Up to four makefile belongs to such an interface. These makefiles are:
#
# $(PROJECT_ROOT)\util\<compiler_plugin>_cfg.mak
# $(PROJECT_ROOT)\core\plugins\<compiler_plugin>_check.mak
# $(PROJECT_ROOT)\core\plugins\<compiler_plugin>_defs.mak
# $(PROJECT_ROOT)\core\plugins\<compiler_plugin>_rules.mak
#
# The variable COMPILER must contain the name of one <compiler_plugin>.
#
# For example:
# COMPILER = tricore_tasking
#
# Tasking is the default toolchain for TRICORE
#
ifeq ($(UNIT_TEST),true)
TOOLCHAIN    ?= mingw
COMPILER     ?=TRICORE_TC277_mingw
else
TOOLCHAIN    ?= tasking
COMPILER     ?=TRICORE_TC277_tasking
endif

# ASM_FILES_TO_BUILD:
# The variable ASM_FILES_TO_BUILD should contain a list of assembler files.
# These files will be built to object files.
#
# For example:
# ASM_FILES_TO_BUILD = $(PROJECT_ROOT)\source\asm\myasmfile.asm vectortable.asm
# or
# ASM_FILES_TO_BUILD = $(PROJECT_ROOT)\source\asm\myasmfile.asm #	$(PROJECT_ROOT)\source\application\vectortable.asm
#
# The suffix _ASM_OPT allows the definition of special options for the
# assembler.
#
# myasmfile_ASM_OPT = ->special assembler options for a file
#
ASM_FILES_TO_BUILD += 

# EXCLUDE_MAKE_DEPEND :
# exclude files from dependency processing
#
EXCLUDE_MAKE_DEPEND += 
