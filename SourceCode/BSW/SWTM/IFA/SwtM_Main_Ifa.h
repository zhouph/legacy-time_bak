/**
 * @defgroup SwtM_Main_Ifa SwtM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTM_MAIN_IFA_H_
#define SWTM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SwtM_Main_Read_SwtM_MainSwtMonInfo(data) do \
{ \
    *data = SwtM_MainSwtMonInfo; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_AvhSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.AvhSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_BflSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.BflSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_BlsSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.BlsSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_BsSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.BsSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_EscSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.EscSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_HdcSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.HdcSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainSwtMonInfo_PbSwtMon(data) do \
{ \
    *data = SwtM_MainSwtMonInfo.PbSwtMon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainEcuModeSts(data) do \
{ \
    *data = SwtM_MainEcuModeSts; \
}while(0);

#define SwtM_Main_Read_SwtM_MainIgnOnOffSts(data) do \
{ \
    *data = SwtM_MainIgnOnOffSts; \
}while(0);

#define SwtM_Main_Read_SwtM_MainIgnEdgeSts(data) do \
{ \
    *data = SwtM_MainIgnEdgeSts; \
}while(0);

#define SwtM_Main_Read_SwtM_MainVBatt1Mon(data) do \
{ \
    *data = SwtM_MainVBatt1Mon; \
}while(0);

#define SwtM_Main_Read_SwtM_MainDiagClrSrs(data) do \
{ \
    *data = SwtM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define SwtM_Main_Write_SwtM_MainSwtMonFaultInfo(data) do \
{ \
    SwtM_MainSwtMonFaultInfo = *data; \
}while(0);

#define SwtM_Main_Write_SwtM_MainSwtMonFaultInfo_SWM_ESCOFF_Sw_Err(data) do \
{ \
    SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = *data; \
}while(0);

#define SwtM_Main_Write_SwtM_MainSwtMonFaultInfo_SWM_HDC_Sw_Err(data) do \
{ \
    SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = *data; \
}while(0);

#define SwtM_Main_Write_SwtM_MainSwtMonFaultInfo_SWM_AVH_Sw_Err(data) do \
{ \
    SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = *data; \
}while(0);

#define SwtM_Main_Write_SwtM_MainSwtMonFaultInfo_SWM_BFL_Sw_Err(data) do \
{ \
    SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = *data; \
}while(0);

#define SwtM_Main_Write_SwtM_MainSwtMonFaultInfo_SWM_PB_Sw_Err(data) do \
{ \
    SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTM_MAIN_IFA_H_ */
/** @} */
