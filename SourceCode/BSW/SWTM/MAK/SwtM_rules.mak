# \file
#
# \brief SwtM
#
# This file contains the implementation of the SWC
# module SwtM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SwtM_src

SwtM_src_FILES        += $(SwtM_SRC_PATH)\SwtM_Main.c
SwtM_src_FILES        += $(SwtM_SRC_PATH)\SwtM_Process.c
SwtM_src_FILES        += $(SwtM_IFA_PATH)\SwtM_Main_Ifa.c
SwtM_src_FILES        += $(SwtM_CFG_PATH)\SwtM_Cfg.c
SwtM_src_FILES        += $(SwtM_CAL_PATH)\SwtM_Cal.c

ifeq ($(ICE_COMPILE),true)
SwtM_src_FILES        += $(SwtM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SwtM_src_FILES        += $(SwtM_UNITY_PATH)\unity.c
	SwtM_src_FILES        += $(SwtM_UNITY_PATH)\unity_fixture.c	
	SwtM_src_FILES        += $(SwtM_UT_PATH)\main.c
	SwtM_src_FILES        += $(SwtM_UT_PATH)\SwtM_Main\SwtM_Main_UtMain.c
endif