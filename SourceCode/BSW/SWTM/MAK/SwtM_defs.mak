# \file
#
# \brief SwtM
#
# This file contains the implementation of the SWC
# module SwtM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SwtM_CORE_PATH     := $(MANDO_BSW_ROOT)\SwtM
SwtM_CAL_PATH      := $(SwtM_CORE_PATH)\CAL\$(SwtM_VARIANT)
SwtM_SRC_PATH      := $(SwtM_CORE_PATH)\SRC
SwtM_CFG_PATH      := $(SwtM_CORE_PATH)\CFG\$(SwtM_VARIANT)
SwtM_HDR_PATH      := $(SwtM_CORE_PATH)\HDR
SwtM_IFA_PATH      := $(SwtM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SwtM_CMN_PATH      := $(SwtM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SwtM_UT_PATH		:= $(SwtM_CORE_PATH)\UT
	SwtM_UNITY_PATH	:= $(SwtM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SwtM_UT_PATH)
	CC_INCLUDE_PATH		+= $(SwtM_UNITY_PATH)
	SwtM_Main_PATH 	:= SwtM_UT_PATH\SwtM_Main
endif
CC_INCLUDE_PATH    += $(SwtM_CAL_PATH)
CC_INCLUDE_PATH    += $(SwtM_SRC_PATH)
CC_INCLUDE_PATH    += $(SwtM_CFG_PATH)
CC_INCLUDE_PATH    += $(SwtM_HDR_PATH)
CC_INCLUDE_PATH    += $(SwtM_IFA_PATH)
CC_INCLUDE_PATH    += $(SwtM_CMN_PATH)

