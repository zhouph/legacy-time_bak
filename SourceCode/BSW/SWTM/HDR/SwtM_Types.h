/**
 * @defgroup SwtM_Types SwtM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTM_TYPES_H_
#define SWTM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputCS1msSwtMonInfo_t SwtM_MainSwtMonInfo;
    Mom_HndlrEcuModeSts_t SwtM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SwtM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SwtM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SwtM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t SwtM_MainDiagClrSrs;

/* Output Data Element */
    SwtM_MainSwtMonFaultInfo_t SwtM_MainSwtMonFaultInfo;
}SwtM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTM_TYPES_H_ */
/** @} */
