/**
 * @defgroup SwtM_Main SwtM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTM_MAIN_H_
#define SWTM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtM_Types.h"
#include "SwtM_Cfg.h"
#include "SwtM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SWTM_MAIN_MODULE_ID      (0)
 #define SWTM_MAIN_MAJOR_VERSION  (2)
 #define SWTM_MAIN_MINOR_VERSION  (0)
 #define SWTM_MAIN_PATCH_VERSION  (0)
 #define SWTM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SwtM_Main_HdrBusType SwtM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SwtM_MainVersionInfo;

/* Input Data Element */
extern Ioc_InputCS1msSwtMonInfo_t SwtM_MainSwtMonInfo;
extern Mom_HndlrEcuModeSts_t SwtM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t SwtM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t SwtM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t SwtM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t SwtM_MainDiagClrSrs;

/* Output Data Element */
extern SwtM_MainSwtMonFaultInfo_t SwtM_MainSwtMonFaultInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SwtM_Main_Init(void);
extern void SwtM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTM_MAIN_H_ */
/** @} */
