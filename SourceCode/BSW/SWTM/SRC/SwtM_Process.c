/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Common.h"
#include "SwtM_Main.h"
#include "SwtM_Cfg.h"

#include "AdcIf_Conv1ms.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

static SwtMCfg* pSwtM_Cfg;


static uint16 SwtM_EscOffSwErrCnt;
static uint16 SwtM_HdcSwErrCnt;
static uint16 SwtM_AvhSwErrCnt;
static uint16 SwtM_BflSwErrCnt;


/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void SwtM_CheckEscOffSwLongInput(void);
static void SwtM_EscOffSwCheck(void);
static void SwtM_CheckHdcSwLongInput(void);
static void SwtM_HdcSwCheck(void);
static void SwtM_CheckAvhSwLongInput(void);
static void SwtM_AvhSwCheck(void);
static void SwtM_BflCheck(void);
static void SwtM_ParkBrakeCheck(void);
static void SwtM_Inhibit(void);


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void SwtM_Process(void)
{
	pSwtM_Cfg = SwtM_GetCfg();
	if(SwtM_MainBus.SwtM_MainVBatt1Mon < SWTM_7V0 || SwtM_MainBus.SwtM_MainVBatt1Mon > SWTM_17V0)
	{
        SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = ERR_INHIBIT;
        SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err    = ERR_INHIBIT;
        SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err    = ERR_INHIBIT;
        SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err    = ERR_INHIBIT;
        SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err     = ERR_INHIBIT;

	}
	else
	{
		SwtM_EscOffSwCheck();
		SwtM_HdcSwCheck();
		SwtM_AvhSwCheck();
		SwtM_BflCheck();
		SwtM_ParkBrakeCheck();
	}
}

void SwtM_Init()
{
	SwtM_InitCfg();

	SwtM_EscOffSwErrCnt = 0;
	SwtM_HdcSwErrCnt	= 0;
	SwtM_AvhSwErrCnt	= 0;
	SwtM_BflSwErrCnt	= 0;

    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = ERR_NONE;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err    = ERR_NONE;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err    = ERR_NONE;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err    = ERR_NONE;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err     = ERR_NONE;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void SwtM_CheckEscOffSwLongInput(void)
{
	uint16 	EscSwErrTime; 	
	uint8 	EscSwitchSignal; 

	EscSwErrTime 	= (uint16)((uint16)SWTM_ESCOFF_SWITCH_ERR_TIME/SWTM_CYCLE_TIME);
	EscSwitchSignal	= SwtM_MainBus.SwtM_MainSwtMonInfo.EscSwtMon;

	if(EscSwitchSignal == SWTM_ON)
	{
		if(SwtM_EscOffSwErrCnt > EscSwErrTime)
		{
			SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = ERR_FAILED;
		}
		else
		{
			SwtM_EscOffSwErrCnt++;
		}
	}
	else
	{
		SwtM_EscOffSwErrCnt = 0;
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = ERR_PASSED;
	}
}

static void SwtM_EscOffSwCheck(void)
{
	SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = ERR_NONE;
	if(pSwtM_Cfg->SwtM_escoff_sw_type == SWTM_ANALOG_TYPE && pSwtM_Cfg->SwtM_escoff_sw_variant == SWTM_HKMC)
	{
		SwtM_CheckEscOffSwLongInput();	
	}
	else
	{
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = ERR_INHIBIT;
	}
}


static void SwtM_CheckHdcSwLongInput(void)
{
	uint16 	HdcSwErrTime; 	
	uint8 	HdcSwitchSignal; 

	HdcSwErrTime 	= (uint16)((uint16)SWTM_HDC_SWITCH_ERR_TIME/SWTM_CYCLE_TIME);
	HdcSwitchSignal	= SwtM_MainBus.SwtM_MainSwtMonInfo.HdcSwtMon;

	if(HdcSwitchSignal == SWTM_ON)
	{
		if(SwtM_HdcSwErrCnt > HdcSwErrTime)
		{
			SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = ERR_FAILED;
		}
		else
		{
			SwtM_HdcSwErrCnt ++;
		}
	}
	else
	{
		SwtM_HdcSwErrCnt = 0;
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = ERR_PASSED;
	}
}

static void SwtM_HdcSwCheck(void)
{
	SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = ERR_NONE;
	if(pSwtM_Cfg->SwtM_hdc_sw_type == SWTM_ANALOG_TYPE && pSwtM_Cfg->SwtM_hdc_sw_variant == SWTM_HKMC)
	{
		SwtM_CheckHdcSwLongInput();	
	}
	else
	{
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = ERR_INHIBIT;
	}
}

static void SwtM_CheckAvhSwLongInput(void)
{
	uint16 	AvhSwErrTime; 	
	uint8 	AvhSwitchSignal; 

	AvhSwErrTime 	= (uint16)((uint16)SWTM_AVH_SWITCH_ERR_TIME/SWTM_CYCLE_TIME);
	AvhSwitchSignal	= SwtM_MainBus.SwtM_MainSwtMonInfo.AvhSwtMon;

	if(AvhSwitchSignal == SWTM_ON)
	{
		if(SwtM_AvhSwErrCnt > AvhSwErrTime)
		{
			SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = ERR_FAILED;
		}
		else
		{
			SwtM_AvhSwErrCnt++;
		}
	}
	else
	{
		SwtM_AvhSwErrCnt = 0;
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = ERR_PASSED;
	}
}

static void SwtM_AvhSwCheck(void)
{
	SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = ERR_NONE;
	if(pSwtM_Cfg->SwtM_avh_sw_type == SWTM_ANALOG_TYPE && pSwtM_Cfg->SwtM_avh_sw_variant== SWTM_HKMC)
	{
		SwtM_CheckAvhSwLongInput();	
	}
	else
	{
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = ERR_INHIBIT;
	}
}

static void SwtM_BflCheck(void)
{
	uint16 	BflSwErrTime; 	
	uint8 	BflSwitchSignal; 
	uint16	BflRawValue;
	uint16	BflSwtVolt;
	/*
	retVal = ((Haluint32)rawVal * mulFactor)/ divFactor;
	return retVal;
	(Ioc_MulFactorType) ((300+51)*78),		 //MulFactor : (R1 + R2) * IOC_VOLT_MUL 
	(Ioc_DivFactorType) ((51)*64) 			//divFactor
	*/	
	
	SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = ERR_NONE;

	BflSwErrTime 	= (uint16)((uint16)SWTM_BFL_SWITCH_ERR_TIME/SWTM_CYCLE_TIME);

	/*To indentify BFL switch whether on or off, BFL ADC raw value is exported from AdcIf_Conv1ms.*/
	BflRawValue		= AdcIf_Conv1msBus.AdcIf_Conv1msSwTrigMocInfo.BFLMon;
	BflSwtVolt		= (BflRawValue*(2000+1200)*78)/(1200*64);
	/*BLF switch is low side active, so BFL is activated when monitoring voltage is under 7V.*/
	if(BflSwtVolt > 7000)	
	{
		BflSwitchSignal = SWTM_ON;
	}
	else
	{
		BflSwitchSignal = SWTM_OFF;
	}
	
	/*BflSwitchSignal	= SwtM_MainBus.SwtM_MainSwtMonInfo.BflSwtMon;*/

	if(pSwtM_Cfg->SwtM_bfl_sw_type == SWTM_ANALOG_TYPE && pSwtM_Cfg->SwtM_bfl_sw_variant == SWTM_HKMC)
	{
		if(BflSwitchSignal == SWTM_ON)
		{
			if(SwtM_BflSwErrCnt > BflSwErrTime)
			{
				SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = ERR_FAILED;
			}
			else
			{
				SwtM_BflSwErrCnt++;
			}
		}
		else
		{
			SwtM_BflSwErrCnt = 0;
			SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = ERR_PASSED;
		}
	}
	else
	{
		SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = ERR_INHIBIT;
	}
}

static void SwtM_ParkBrakeCheck(void)
{
	/* To do*/
}

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
