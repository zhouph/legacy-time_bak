/**
 * @defgroup SwtM_Main SwtM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtM_Main.h"
#include "SwtM_Process.h"
#include "SwtM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWTM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWTM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SwtM_Main_HdrBusType SwtM_MainBus;

/* Version Info */
const SwcVersionInfo_t SwtM_MainVersionInfo = 
{   
    SWTM_MAIN_MODULE_ID,           /* SwtM_MainVersionInfo.ModuleId */
    SWTM_MAIN_MAJOR_VERSION,       /* SwtM_MainVersionInfo.MajorVer */
    SWTM_MAIN_MINOR_VERSION,       /* SwtM_MainVersionInfo.MinorVer */
    SWTM_MAIN_PATCH_VERSION,       /* SwtM_MainVersionInfo.PatchVer */
    SWTM_MAIN_BRANCH_VERSION       /* SwtM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputCS1msSwtMonInfo_t SwtM_MainSwtMonInfo;
Mom_HndlrEcuModeSts_t SwtM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t SwtM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t SwtM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t SwtM_MainVBatt1Mon;
Diag_HndlrDiagClr_t SwtM_MainDiagClrSrs;

/* Output Data Element */
SwtM_MainSwtMonFaultInfo_t SwtM_MainSwtMonFaultInfo;

uint32 SwtM_Main_Timer_Start;
uint32 SwtM_Main_Timer_Elapsed;

#define SWTM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
#define SWTM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_MAIN_START_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTM_MAIN_STOP_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtM_MemMap.h"
#define SWTM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SwtM_MemMap.h"
#define SWTM_MAIN_START_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTM_MAIN_STOP_SEC_VAR_32BIT
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWTM_MAIN_START_SEC_CODE
#include "SwtM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SwtM_Main_Init(void)
{
    /* Initialize internal bus */
    SwtM_MainBus.SwtM_MainSwtMonInfo.AvhSwtMon = 0;
    SwtM_MainBus.SwtM_MainSwtMonInfo.BflSwtMon = 0;
    SwtM_MainBus.SwtM_MainSwtMonInfo.BlsSwtMon = 0;
    SwtM_MainBus.SwtM_MainSwtMonInfo.BsSwtMon = 0;
    SwtM_MainBus.SwtM_MainSwtMonInfo.EscSwtMon = 0;
    SwtM_MainBus.SwtM_MainSwtMonInfo.HdcSwtMon = 0;
    SwtM_MainBus.SwtM_MainSwtMonInfo.PbSwtMon = 0;
    SwtM_MainBus.SwtM_MainEcuModeSts = 0;
    SwtM_MainBus.SwtM_MainIgnOnOffSts = 0;
    SwtM_MainBus.SwtM_MainIgnEdgeSts = 0;
    SwtM_MainBus.SwtM_MainVBatt1Mon = 0;
    SwtM_MainBus.SwtM_MainDiagClrSrs = 0;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err = 0;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err = 0;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err = 0;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err = 0;
    SwtM_MainBus.SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err = 0;
	SwtM_Init();
}

void SwtM_Main(void)
{
    SwtM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    SwtM_Main_Read_SwtM_MainSwtMonInfo_AvhSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.AvhSwtMon);
    SwtM_Main_Read_SwtM_MainSwtMonInfo_BflSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.BflSwtMon);
    SwtM_Main_Read_SwtM_MainSwtMonInfo_BlsSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.BlsSwtMon);
    SwtM_Main_Read_SwtM_MainSwtMonInfo_BsSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.BsSwtMon);
    SwtM_Main_Read_SwtM_MainSwtMonInfo_EscSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.EscSwtMon);
    SwtM_Main_Read_SwtM_MainSwtMonInfo_HdcSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.HdcSwtMon);
    SwtM_Main_Read_SwtM_MainSwtMonInfo_PbSwtMon(&SwtM_MainBus.SwtM_MainSwtMonInfo.PbSwtMon);

    SwtM_Main_Read_SwtM_MainEcuModeSts(&SwtM_MainBus.SwtM_MainEcuModeSts);
    SwtM_Main_Read_SwtM_MainIgnOnOffSts(&SwtM_MainBus.SwtM_MainIgnOnOffSts);
    SwtM_Main_Read_SwtM_MainIgnEdgeSts(&SwtM_MainBus.SwtM_MainIgnEdgeSts);
    SwtM_Main_Read_SwtM_MainVBatt1Mon(&SwtM_MainBus.SwtM_MainVBatt1Mon);
    SwtM_Main_Read_SwtM_MainDiagClrSrs(&SwtM_MainBus.SwtM_MainDiagClrSrs);

    /* Process */
	if(SwtM_MainBus.SwtM_MainDiagClrSrs == 1)
	{
		SwtM_Main_Init();
	}
	SwtM_Process();
	
    /* Output */
    SwtM_Main_Write_SwtM_MainSwtMonFaultInfo(&SwtM_MainBus.SwtM_MainSwtMonFaultInfo);
    /*==============================================================================
    * Members of structure SwtM_MainSwtMonFaultInfo 
     : SwtM_MainSwtMonFaultInfo.SWM_ESCOFF_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_HDC_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_AVH_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_BFL_Sw_Err;
     : SwtM_MainSwtMonFaultInfo.SWM_PB_Sw_Err;
     =============================================================================*/
    

    SwtM_Main_Timer_Elapsed = STM0_TIM0.U - SwtM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWTM_MAIN_STOP_SEC_CODE
#include "SwtM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
