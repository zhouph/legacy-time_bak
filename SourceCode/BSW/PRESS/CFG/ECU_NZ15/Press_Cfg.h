/**
 * @defgroup Press_Cfg Press_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_CFG_H_
#define PRESS_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 /* Pressure sensor port symbolic names */
 #define PRESS_PORT_MCP           (Press_PortType) 0
 #define PRESS_PORT_MCP2          (Press_PortType) 1
 #define PRESS_PORT_PSP           (Press_PortType) 2
 #define PRESS_PORT_MAX_NUM       (Press_PortType) 3

 /* Pressure sensor for Sent Type port syumbolic names */
 #define PRESS_SENT_PORT_MCP           (Press_PortType) 0
 #define PRESS_SENT_PORT_FLP       	   (Press_PortType) 1
 #define PRESS_SENT_PORT_FRP           (Press_PortType) 2
 #define PRESS_SENT_PORT_MAX_NUM       (Press_PortType) 3

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 /* Pressure sensor conversion rule config */
 extern const Press_ConfigType Press_Config[PRESS_PORT_MAX_NUM];

 extern const Press_ConfigSentType	Press_Config_Sent[PRESS_SENT_PORT_MAX_NUM];

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_CFG_H_ */
/** @} */
