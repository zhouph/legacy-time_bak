/**
 * @defgroup Press_Cfg Press_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESS_START_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Press sensor config table */
 const Press_ConfigType Press_Config[PRESS_PORT_MAX_NUM] = 
 {
    /* PRESS_PORT_MCP */
    {
        (Press_PercentType) 1000,                                    /* ZeroBarPercent */
        (Press_PercentType) 9000,                                    /* MaxBarPercent */
        (Press_BarType)     25000,                                   /* MaxBar */
    },
    /* PRESS_PORT_MCP2 */
    {
        (Press_PercentType) 1000,                                   /* ZeroBarPercent */
        (Press_PercentType) 9000,                                   /* MaxBarPercent */
        (Press_BarType)     25000,                                  /* MaxBar */
    },
    /* PRESS_PORT_PSP */
    {
        (Press_PercentType) 1000,                                 /* ZeroBarPercent */
        (Press_PercentType) 9000,                                 /* MaxBarPercent */
        (Press_BarType)     25000,                                /* MaxBar */
    },
 };

 const Press_ConfigSentType Press_Config_Sent[PRESS_PORT_MAX_NUM] = 
 {
    /* PRESS_SENT_PORT_MCP */
    {
        (Press_SentType) 3686,                                    /* Max Sent Value */
        (Press_SentType) 410,                                     /* Min Sent Value */
        (Press_BarType)  25000,                                   /* MaxBar */
    },
    /* PRESS_SENT_PORT_FLP */
    {
        (Press_SentType) 3686,                                    /* ZeroBarPercent */
        (Press_SentType) 410,                                     /* MaxBarPercent */
        (Press_BarType)  25000,                                   /* MaxBar */
    },
    /* PRESS_SENT_PORT_FRP */
    {
        (Press_SentType) 3686,                                    /* ZeroBarPercent */
        (Press_SentType) 410,                                     /* MaxBarPercent */
        (Press_BarType)  25000,                                   /* MaxBar */
    },
    
    #if 0
    /* PRESS_SENT_PORT_RLP */
    {
        (Press_SentType) 3686,                                    /* ZeroBarPercent */
        (Press_SentType) 410,                                     /* MaxBarPercent */
        (Press_BarType)  25000,                                   /* MaxBar */
    },
    /* PRESS_SENT_PORT_RRP */
    {
        (Press_SentType) 3686,                                    /* ZeroBarPercent */
        (Press_SentType) 410,                                     /* MaxBarPercent */
        (Press_BarType)  25000,                                   /* MaxBar */
    },
    #endif
 };

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESS_STOP_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESS_START_SEC_CODE
#include "Press_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESS_STOP_SEC_CODE
#include "Press_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
