/**
 * @defgroup Press_Cal Press_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESS_START_SEC_CALIB_UNSPECIFIED
#include "Press_MemMap.h"
/* Global Calibration Section */


#define PRESS_STOP_SEC_CALIB_UNSPECIFIED
#include "Press_MemMap.h"

#define PRESS_START_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESS_STOP_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESS_START_SEC_CODE
#include "Press_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESS_STOP_SEC_CODE
#include "Press_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
