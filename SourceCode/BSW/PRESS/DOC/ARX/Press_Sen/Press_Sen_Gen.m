Press_SenHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
Press_SenHalPressureInfo = CreateBus(Press_SenHalPressureInfo, DeList);
clear DeList;

Press_SenEcuModeSts = Simulink.Bus;
DeList={'Press_SenEcuModeSts'};
Press_SenEcuModeSts = CreateBus(Press_SenEcuModeSts, DeList);
clear DeList;

Press_SenFuncInhibitPressSts = Simulink.Bus;
DeList={'Press_SenFuncInhibitPressSts'};
Press_SenFuncInhibitPressSts = CreateBus(Press_SenFuncInhibitPressSts, DeList);
clear DeList;

Press_SenCircP1msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Press_SenCircP1msRawInfo = CreateBus(Press_SenCircP1msRawInfo, DeList);
clear DeList;

Press_SenPistP1msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Press_SenPistP1msRawInfo = CreateBus(Press_SenPistP1msRawInfo, DeList);
clear DeList;

Press_SenPresCalcEsc = Simulink.Bus;
DeList={
    'Pres_MCP_1_100_bar_Esc'
    'Pres_FLP_1_100_bar_Esc'
    'Pres_FRP_1_100_bar_Esc'
    'Pres_RLP_1_100_bar_Esc'
    'Pres_RRP_1_100_bar_Esc'
    'Pres_MCP_1_100_temp_Esc'
    'Pres_FLP_1_100_temp_Esc'
    'Pres_FRP_1_100_temp_Esc'
    'Pres_RLP_1_100_temp_Esc'
    'Pres_RRP_1_100_temp_Esc'
    };
Press_SenPresCalcEsc = CreateBus(Press_SenPresCalcEsc, DeList);
clear DeList;

Press_SenCircPBufInfo = Simulink.Bus;
DeList={
    'PrimCircPRaw_array_0'
    'PrimCircPRaw_array_1'
    'PrimCircPRaw_array_2'
    'PrimCircPRaw_array_3'
    'PrimCircPRaw_array_4'
    'SecdCircPRaw_array_0'
    'SecdCircPRaw_array_1'
    'SecdCircPRaw_array_2'
    'SecdCircPRaw_array_3'
    'SecdCircPRaw_array_4'
    };
Press_SenCircPBufInfo = CreateBus(Press_SenCircPBufInfo, DeList);
clear DeList;

Press_SenPistPBufInfo = Simulink.Bus;
DeList={
    'PistPRaw_array_0'
    'PistPRaw_array_1'
    'PistPRaw_array_2'
    'PistPRaw_array_3'
    'PistPRaw_array_4'
    };
Press_SenPistPBufInfo = CreateBus(Press_SenPistPBufInfo, DeList);
clear DeList;

Press_SenPspBufInfo = Simulink.Bus;
DeList={
    'PedlSimPRaw_array_0'
    'PedlSimPRaw_array_1'
    'PedlSimPRaw_array_2'
    'PedlSimPRaw_array_3'
    'PedlSimPRaw_array_4'
    };
Press_SenPspBufInfo = CreateBus(Press_SenPspBufInfo, DeList);
clear DeList;

Press_SenPressCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimP_1_100_bar'
    'PressP_CirP1_1_100_bar'
    'PressP_CirP2_1_100_bar'
    'SimPMoveAvr'
    'CirPMoveAvr'
    'CirP2MoveAvr'
    };
Press_SenPressCalcInfo = CreateBus(Press_SenPressCalcInfo, DeList);
clear DeList;

