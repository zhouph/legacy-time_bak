Press_SenSyncCircPBufInfo = Simulink.Bus;
DeList={
    'PrimCircPRaw_array_0'
    'PrimCircPRaw_array_1'
    'PrimCircPRaw_array_2'
    'PrimCircPRaw_array_3'
    'PrimCircPRaw_array_4'
    'SecdCircPRaw_array_0'
    'SecdCircPRaw_array_1'
    'SecdCircPRaw_array_2'
    'SecdCircPRaw_array_3'
    'SecdCircPRaw_array_4'
    };
Press_SenSyncCircPBufInfo = CreateBus(Press_SenSyncCircPBufInfo, DeList);
clear DeList;

Press_SenSyncPistPBufInfo = Simulink.Bus;
DeList={
    'PistPRaw_array_0'
    'PistPRaw_array_1'
    'PistPRaw_array_2'
    'PistPRaw_array_3'
    'PistPRaw_array_4'
    };
Press_SenSyncPistPBufInfo = CreateBus(Press_SenSyncPistPBufInfo, DeList);
clear DeList;

Press_SenSyncPspBufInfo = Simulink.Bus;
DeList={
    'PedlSimPRaw_array_0'
    'PedlSimPRaw_array_1'
    'PedlSimPRaw_array_2'
    'PedlSimPRaw_array_3'
    'PedlSimPRaw_array_4'
    };
Press_SenSyncPspBufInfo = CreateBus(Press_SenSyncPspBufInfo, DeList);
clear DeList;

Press_SenSyncEcuModeSts = Simulink.Bus;
DeList={'Press_SenSyncEcuModeSts'};
Press_SenSyncEcuModeSts = CreateBus(Press_SenSyncEcuModeSts, DeList);
clear DeList;

Press_SenSyncFuncInhibitPressSts = Simulink.Bus;
DeList={'Press_SenSyncFuncInhibitPressSts'};
Press_SenSyncFuncInhibitPressSts = CreateBus(Press_SenSyncFuncInhibitPressSts, DeList);
clear DeList;

Press_SenSyncCircP5msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Press_SenSyncCircP5msRawInfo = CreateBus(Press_SenSyncCircP5msRawInfo, DeList);
clear DeList;

Press_SenSyncPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Press_SenSyncPistP5msRawInfo = CreateBus(Press_SenSyncPistP5msRawInfo, DeList);
clear DeList;

Press_SenSyncPressSenCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimPMoveAve'
    'PressP_CirP1MoveAve'
    'PressP_CirP2MoveAve'
    'PressP_SimPMoveAveEolOfs'
    'PressP_CirP1MoveAveEolOfs'
    'PressP_CirP2MoveAveEolOfs'
    };
Press_SenSyncPressSenCalcInfo = CreateBus(Press_SenSyncPressSenCalcInfo, DeList);
clear DeList;

Press_SenSyncPedlSimPRaw = Simulink.Bus;
DeList={'Press_SenSyncPedlSimPRaw'};
Press_SenSyncPedlSimPRaw = CreateBus(Press_SenSyncPedlSimPRaw, DeList);
clear DeList;

