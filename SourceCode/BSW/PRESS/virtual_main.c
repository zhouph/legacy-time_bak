/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Press_Sen.h"
#include "Press_SenSync.h"

int main(void)
{
    Press_Sen_Init();
    Press_SenSync_Init();

    while(1)
    {
        Press_Sen();
        Press_SenSync();
    }
}