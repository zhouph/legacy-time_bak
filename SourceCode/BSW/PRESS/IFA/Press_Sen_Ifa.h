/**
 * @defgroup Press_Sen_Ifa Press_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Sen_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_SEN_IFA_H_
#define PRESS_SEN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Press_Sen_Read_Press_SenHalPressureInfo(data) do \
{ \
    *data = Press_SenHalPressureInfo; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpPresData1(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpPresData1; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpPresData2(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpPresData2; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpSentCrc(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpSentCrc; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpSentData(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpSentData; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpSentMsgId(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpSentMsgId; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpSentSerialCrc(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpSentSerialCrc; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_McpSentConfig(data) do \
{ \
    *data = Press_SenHalPressureInfo.McpSentConfig; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1PresData1(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1PresData1; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1PresData2(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1PresData2; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1SentCrc(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1SentCrc; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1SentData(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1SentData; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1SentMsgId(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1SentMsgId; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1SentSerialCrc(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1SentSerialCrc; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp1SentConfig(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp1SentConfig; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2PresData1(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2PresData1; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2PresData2(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2PresData2; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2SentCrc(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2SentCrc; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2SentData(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2SentData; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2SentMsgId(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2SentMsgId; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2SentSerialCrc(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2SentSerialCrc; \
}while(0);

#define Press_Sen_Read_Press_SenHalPressureInfo_Wlp2SentConfig(data) do \
{ \
    *data = Press_SenHalPressureInfo.Wlp2SentConfig; \
}while(0);

#define Press_Sen_Read_Press_SenEcuModeSts(data) do \
{ \
    *data = Press_SenEcuModeSts; \
}while(0);

#define Press_Sen_Read_Press_SenFuncInhibitPressSts(data) do \
{ \
    *data = Press_SenFuncInhibitPressSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Press_Sen_Write_Press_SenCircP1msRawInfo(data) do \
{ \
    Press_SenCircP1msRawInfo = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPistP1msRawInfo(data) do \
{ \
    Press_SenPistP1msRawInfo = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc(data) do \
{ \
    Press_SenPresCalcEsc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenCircPBufInfo(data) do \
{ \
    Press_SenCircPBufInfo = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPistPBufInfo(data) do \
{ \
    Press_SenPistPBufInfo = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPspBufInfo(data) do \
{ \
    Press_SenPspBufInfo = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPressCalcInfo(data) do \
{ \
    Press_SenPressCalcInfo = *data; \
}while(0);

#define Press_Sen_Write_Press_SenCircP1msRawInfo_PrimCircPSig(data) do \
{ \
    Press_SenCircP1msRawInfo.PrimCircPSig = *data; \
}while(0);

#define Press_Sen_Write_Press_SenCircP1msRawInfo_SecdCircPSig(data) do \
{ \
    Press_SenCircP1msRawInfo.SecdCircPSig = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPistP1msRawInfo_PistPSig(data) do \
{ \
    Press_SenPistP1msRawInfo.PistPSig = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_MCP_1_100_bar_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_MCP_1_100_bar_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_FLP_1_100_bar_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_FLP_1_100_bar_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_FRP_1_100_bar_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_FRP_1_100_bar_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_RLP_1_100_bar_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_RLP_1_100_bar_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_RRP_1_100_bar_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_RRP_1_100_bar_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_MCP_1_100_temp_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_MCP_1_100_temp_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_FLP_1_100_temp_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_FLP_1_100_temp_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_FRP_1_100_temp_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_FRP_1_100_temp_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_RLP_1_100_temp_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_RLP_1_100_temp_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPresCalcEsc_Pres_RRP_1_100_temp_Esc(data) do \
{ \
    Press_SenPresCalcEsc.Pres_RRP_1_100_temp_Esc = *data; \
}while(0);

#define Press_Sen_Write_Press_SenCircPBufInfo_PrimCircPRaw(data) \
{ \
    for(i=0;i<5;i++) Press_SenCircPBufInfo.PrimCircPRaw[i] = *data[i]; \
}
#define Press_Sen_Write_Press_SenCircPBufInfo_SecdCircPRaw(data) \
{ \
    for(i=0;i<5;i++) Press_SenCircPBufInfo.SecdCircPRaw[i] = *data[i]; \
}
#define Press_Sen_Write_Press_SenPistPBufInfo_PistPRaw(data) \
{ \
    for(i=0;i<5;i++) Press_SenPistPBufInfo.PistPRaw[i] = *data[i]; \
}
#define Press_Sen_Write_Press_SenPspBufInfo_PedlSimPRaw(data) \
{ \
    for(i=0;i<5;i++) Press_SenPspBufInfo.PedlSimPRaw[i] = *data[i]; \
}
#define Press_Sen_Write_Press_SenPressCalcInfo_PressP_SimP_1_100_bar(data) do \
{ \
    Press_SenPressCalcInfo.PressP_SimP_1_100_bar = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPressCalcInfo_PressP_CirP1_1_100_bar(data) do \
{ \
    Press_SenPressCalcInfo.PressP_CirP1_1_100_bar = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPressCalcInfo_PressP_CirP2_1_100_bar(data) do \
{ \
    Press_SenPressCalcInfo.PressP_CirP2_1_100_bar = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPressCalcInfo_SimPMoveAvr(data) do \
{ \
    Press_SenPressCalcInfo.SimPMoveAvr = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPressCalcInfo_CirPMoveAvr(data) do \
{ \
    Press_SenPressCalcInfo.CirPMoveAvr = *data; \
}while(0);

#define Press_Sen_Write_Press_SenPressCalcInfo_CirP2MoveAvr(data) do \
{ \
    Press_SenPressCalcInfo.CirP2MoveAvr = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_SEN_IFA_H_ */
/** @} */
