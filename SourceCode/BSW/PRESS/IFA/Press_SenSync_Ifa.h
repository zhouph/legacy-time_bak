/**
 * @defgroup Press_SenSync_Ifa Press_SenSync_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_SenSync_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_SENSYNC_IFA_H_
#define PRESS_SENSYNC_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Press_SenSync_Read_Press_SenSyncCircPBufInfo(data) do \
{ \
    *data = Press_SenSyncCircPBufInfo; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncPistPBufInfo(data) do \
{ \
    *data = Press_SenSyncPistPBufInfo; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncPspBufInfo(data) do \
{ \
    *data = Press_SenSyncPspBufInfo; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncCircPBufInfo_PrimCircPRaw(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Press_SenSyncCircPBufInfo.PrimCircPRaw[i]; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncCircPBufInfo_SecdCircPRaw(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Press_SenSyncCircPBufInfo.SecdCircPRaw[i]; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncPistPBufInfo_PistPRaw(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Press_SenSyncPistPBufInfo.PistPRaw[i]; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncPspBufInfo_PedlSimPRaw(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Press_SenSyncPspBufInfo.PedlSimPRaw[i]; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncEcuModeSts(data) do \
{ \
    *data = Press_SenSyncEcuModeSts; \
}while(0);

#define Press_SenSync_Read_Press_SenSyncFuncInhibitPressSts(data) do \
{ \
    *data = Press_SenSyncFuncInhibitPressSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Press_SenSync_Write_Press_SenSyncCircP5msRawInfo(data) do \
{ \
    Press_SenSyncCircP5msRawInfo = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPistP5msRawInfo(data) do \
{ \
    Press_SenSyncPistP5msRawInfo = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo(data) do \
{ \
    Press_SenSyncPressSenCalcInfo = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncCircP5msRawInfo_PrimCircPSig(data) do \
{ \
    Press_SenSyncCircP5msRawInfo.PrimCircPSig = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncCircP5msRawInfo_SecdCircPSig(data) do \
{ \
    Press_SenSyncCircP5msRawInfo.SecdCircPSig = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPistP5msRawInfo_PistPSig(data) do \
{ \
    Press_SenSyncPistP5msRawInfo.PistPSig = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo_PressP_SimPMoveAve(data) do \
{ \
    Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAve = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo_PressP_CirP1MoveAve(data) do \
{ \
    Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAve = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo_PressP_CirP2MoveAve(data) do \
{ \
    Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAve = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo_PressP_SimPMoveAveEolOfs(data) do \
{ \
    Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo_PressP_CirP1MoveAveEolOfs(data) do \
{ \
    Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPressSenCalcInfo_PressP_CirP2MoveAveEolOfs(data) do \
{ \
    Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = *data; \
}while(0);

#define Press_SenSync_Write_Press_SenSyncPedlSimPRaw(data) do \
{ \
    Press_SenSyncPedlSimPRaw = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_SENSYNC_IFA_H_ */
/** @} */
