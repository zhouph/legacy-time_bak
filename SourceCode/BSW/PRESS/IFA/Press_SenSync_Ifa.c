/**
 * @defgroup Press_SenSync_Ifa Press_SenSync_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_SenSync_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_SenSync_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESS_SENSYNC_STOP_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_CODE
#include "Press_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESS_SENSYNC_STOP_SEC_CODE
#include "Press_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
