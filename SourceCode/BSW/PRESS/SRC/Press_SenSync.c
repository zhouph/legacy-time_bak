/**
 * @defgroup Press_SenSync Press_SenSync
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_SenSync.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_SenSync.h"
#include "Press_SenSync_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESS_SENSYNC_STOP_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Press_SenSync_HdrBusType Press_SenSyncBus;

/* Version Info */
const SwcVersionInfo_t Press_SenSyncVersionInfo = 
{   
    PRESS_SENSYNC_MODULE_ID,           /* Press_SenSyncVersionInfo.ModuleId */
    PRESS_SENSYNC_MAJOR_VERSION,       /* Press_SenSyncVersionInfo.MajorVer */
    PRESS_SENSYNC_MINOR_VERSION,       /* Press_SenSyncVersionInfo.MinorVer */
    PRESS_SENSYNC_PATCH_VERSION,       /* Press_SenSyncVersionInfo.PatchVer */
    PRESS_SENSYNC_BRANCH_VERSION       /* Press_SenSyncVersionInfo.BranchVer */
};
    
/* Input Data Element */
Press_SenCircPBufInfo_t Press_SenSyncCircPBufInfo;
Press_SenPistPBufInfo_t Press_SenSyncPistPBufInfo;
Press_SenPspBufInfo_t Press_SenSyncPspBufInfo;
Mom_HndlrEcuModeSts_t Press_SenSyncEcuModeSts;
Eem_SuspcDetnFuncInhibitPressSts_t Press_SenSyncFuncInhibitPressSts;

/* Output Data Element */
Press_SenSyncCircP5msRawInfo_t Press_SenSyncCircP5msRawInfo;
Press_SenSyncPistP5msRawInfo_t Press_SenSyncPistP5msRawInfo;
Press_SenSyncPressSenCalcInfo_t Press_SenSyncPressSenCalcInfo;
Press_SenSyncPedlSimPRaw_t Press_SenSyncPedlSimPRaw;

uint32 Press_SenSync_Timer_Start;
uint32 Press_SenSync_Timer_Elapsed;

#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SENSYNC_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_SENSYNC_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

static Press_RawDataType MinMaxSelectAlgoForAvg(uint8 ArraySize, Press_RawDataType* Press_RawArray);
#define PRESS_SENSYNC_START_SEC_CODE
#include "Press_MemMap.h"
static void Press_SenSync_GetPressure(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Press_SenSync_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<5;i++) Press_SenSyncBus.Press_SenSyncCircPBufInfo.PrimCircPRaw[i] = 0;   
    for(i=0;i<5;i++) Press_SenSyncBus.Press_SenSyncCircPBufInfo.SecdCircPRaw[i] = 0;   
    for(i=0;i<5;i++) Press_SenSyncBus.Press_SenSyncPistPBufInfo.PistPRaw[i] = 0;   
    for(i=0;i<5;i++) Press_SenSyncBus.Press_SenSyncPspBufInfo.PedlSimPRaw[i] = 0;   
    Press_SenSyncBus.Press_SenSyncEcuModeSts = 0;
    Press_SenSyncBus.Press_SenSyncFuncInhibitPressSts = 0;
    Press_SenSyncBus.Press_SenSyncCircP5msRawInfo.PrimCircPSig = 0;
    Press_SenSyncBus.Press_SenSyncCircP5msRawInfo.SecdCircPSig = 0;
    Press_SenSyncBus.Press_SenSyncPistP5msRawInfo.PistPSig = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAve = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAve = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAve = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = 0;
    Press_SenSyncBus.Press_SenSyncPedlSimPRaw = 0;
}

void Press_SenSync(void)
{
    uint16 i;
    
    Press_SenSync_Timer_Start = STM0_TIM0.U;

    /* Input */
    Press_SenSync_Read_Press_SenSyncCircPBufInfo(&Press_SenSyncBus.Press_SenSyncCircPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncCircPBufInfo 
     : Press_SenSyncCircPBufInfo.PrimCircPRaw;
     : Press_SenSyncCircPBufInfo.SecdCircPRaw;
     =============================================================================*/
    
    Press_SenSync_Read_Press_SenSyncPistPBufInfo(&Press_SenSyncBus.Press_SenSyncPistPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPistPBufInfo 
     : Press_SenSyncPistPBufInfo.PistPRaw;
     =============================================================================*/
    
    Press_SenSync_Read_Press_SenSyncPspBufInfo(&Press_SenSyncBus.Press_SenSyncPspBufInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPspBufInfo 
     : Press_SenSyncPspBufInfo.PedlSimPRaw;
     =============================================================================*/
    
    Press_SenSync_Read_Press_SenSyncEcuModeSts(&Press_SenSyncBus.Press_SenSyncEcuModeSts);
    Press_SenSync_Read_Press_SenSyncFuncInhibitPressSts(&Press_SenSyncBus.Press_SenSyncFuncInhibitPressSts);

    /* Process */
	Press_SenSync_GetPressure();
    /* Output */
    Press_SenSync_Write_Press_SenSyncCircP5msRawInfo(&Press_SenSyncBus.Press_SenSyncCircP5msRawInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncCircP5msRawInfo 
     : Press_SenSyncCircP5msRawInfo.PrimCircPSig;
     : Press_SenSyncCircP5msRawInfo.SecdCircPSig;
     =============================================================================*/
    
    Press_SenSync_Write_Press_SenSyncPistP5msRawInfo(&Press_SenSyncBus.Press_SenSyncPistP5msRawInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPistP5msRawInfo 
     : Press_SenSyncPistP5msRawInfo.PistPSig;
     =============================================================================*/
    
    Press_SenSync_Write_Press_SenSyncPressSenCalcInfo(&Press_SenSyncBus.Press_SenSyncPressSenCalcInfo);
    /*==============================================================================
    * Members of structure Press_SenSyncPressSenCalcInfo 
     : Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAve;
     : Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAve;
     : Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAve;
     : Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs;
     : Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAveEolOfs;
     : Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAveEolOfs;
     =============================================================================*/
    
    Press_SenSync_Write_Press_SenSyncPedlSimPRaw(&Press_SenSyncBus.Press_SenSyncPedlSimPRaw);

    Press_SenSync_Timer_Elapsed = STM0_TIM0.U - Press_SenSync_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Press_SenSync_GetPressure(void)
{
	Press_PortType port;
    Press_BufLengthType bufIdx;
#if 0
    for(port = 0; port < PRESS_PORT_MAX_NUM; port++)
    {
        for(bufIdx = 0; bufIdx < PRESS_BUFFER_LEN; bufIdx++)
        {
			if(port == 0)
			{
        		Press_SenSyncBus.Press_5msPistPRawInfo.PistPRaw1ms[bufIdx] = Press_SenSyncBus.Press_5msBarBufInfo.PrimCircPRaw[bufIdx];
				Press_SenSyncBus.Press_5msCircPRawInfo.PrimCircPRaw1ms[bufIdx] = Press_SenSyncBus.Press_5msBarBufInfo.PrimCircPRaw[bufIdx];
			}
			else if(port == 1)
			{
				Press_SenSyncBus.Press_5msCircPRawInfo.SecdCircPRaw1ms[bufIdx] = Press_SenSyncBus.Press_5msBarBufInfo.SecdCircPRaw[bufIdx];
			}
			else ;
        }
    }
#endif
	Press_SenSyncBus.Press_SenSyncPistP5msRawInfo.PistPSig = Press_SenSyncBus.Press_SenSyncPistPBufInfo.PistPRaw[4];
	Press_SenSyncBus.Press_SenSyncCircP5msRawInfo.PrimCircPSig = Press_SenSyncBus.Press_SenSyncCircPBufInfo.PrimCircPRaw[4];
	Press_SenSyncBus.Press_SenSyncCircP5msRawInfo.SecdCircPSig = Press_SenSyncBus.Press_SenSyncCircPBufInfo.SecdCircPRaw[4];
	Press_SenSyncBus.Press_SenSyncPedlSimPRaw = Press_SenSyncBus.Press_SenSyncPspBufInfo.PedlSimPRaw[4];
    //Press_Counter = 0;
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs = \
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAve = MinMaxSelectAlgoForAvg(PRESS_BUFFER_LEN, Press_SenSyncBus.Press_SenSyncPspBufInfo.PedlSimPRaw);

    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = \
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAve = MinMaxSelectAlgoForAvg(PRESS_BUFFER_LEN, Press_SenSyncBus.Press_SenSyncCircPBufInfo.PrimCircPRaw);

    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = \
    Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAve = MinMaxSelectAlgoForAvg(PRESS_BUFFER_LEN, Press_SenSyncBus.Press_SenSyncCircPBufInfo.SecdCircPRaw);
}

Press_RawDataType MinMaxSelectAlgoForAvg(uint8 ArraySize, Press_RawDataType* Press_RawArray)
{
    Press_RawDataType iMax = Press_RawArray[0];
    Press_RawDataType iMin = Press_RawArray[0];
    Press_RawDataType Ret_Avg = Press_RawArray[0];

    uint8 i;
    for (i=1; i<ArraySize; i++)
    {
        if (Press_RawArray[i] < iMin)
        {
            iMin = Press_RawArray[i];
        }    
        if (Press_RawArray[i] > iMax)
        {
            iMax = Press_RawArray[i];
        }

        Ret_Avg += Press_RawArray[i];
    }

    Ret_Avg = (Ret_Avg - iMin - iMax) / (ArraySize - 2);

    return Ret_Avg;
}

#define PRESS_SENSYNC_STOP_SEC_CODE
#include "Press_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
