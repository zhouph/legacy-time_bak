/**
 * @defgroup Press_Sen Press_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Sen.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_Sen.h"
#include "Press_Sen_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define PRESSURE_POWER     5000  /* 5V Power */
#define MAX_100_PERCENT    10000
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESS_SEN_START_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESS_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Press_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Press_Sen_HdrBusType Press_SenBus;

/* Version Info */
const SwcVersionInfo_t Press_SenVersionInfo = 
{   
    PRESS_SEN_MODULE_ID,           /* Press_SenVersionInfo.ModuleId */
    PRESS_SEN_MAJOR_VERSION,       /* Press_SenVersionInfo.MajorVer */
    PRESS_SEN_MINOR_VERSION,       /* Press_SenVersionInfo.MinorVer */
    PRESS_SEN_PATCH_VERSION,       /* Press_SenVersionInfo.PatchVer */
    PRESS_SEN_BRANCH_VERSION       /* Press_SenVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputSR1msHalPressureInfo_t Press_SenHalPressureInfo;
Mom_HndlrEcuModeSts_t Press_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitPressSts_t Press_SenFuncInhibitPressSts;

/* Output Data Element */
Press_SenCircP1msRawInfo_t Press_SenCircP1msRawInfo;
Press_SenPistP1msRawInfo_t Press_SenPistP1msRawInfo;
Press_SenPresCalcEsc_t Press_SenPresCalcEsc;
Press_SenCircPBufInfo_t Press_SenCircPBufInfo;
Press_SenPistPBufInfo_t Press_SenPistPBufInfo;
Press_SenPspBufInfo_t Press_SenPspBufInfo;
Press_SenPressCalcInfo_t Press_SenPressCalcInfo;

uint32 Press_Sen_Timer_Start;
uint32 Press_Sen_Timer_Elapsed;

#define PRESS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_SEN_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESS_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SEN_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_SEN_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESS_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Press_MemMap.h"
#define PRESS_SEN_START_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
 static uint8 Press_Counter = 0;

#define PRESS_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Press_MemMap.h"
#define PRESS_SEN_START_SEC_VAR_32BIT
#include "Press_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESS_SEN_STOP_SEC_VAR_32BIT
#include "Press_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESS_SEN_START_SEC_CODE
#include "Press_MemMap.h"

static void Press_Sen_Calculation(void);
static void Press_Sent_Sen_Calculation(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Press_Sen_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Press_SenBus.Press_SenHalPressureInfo.McpPresData1 = 0;
    Press_SenBus.Press_SenHalPressureInfo.McpPresData2 = 0;
    Press_SenBus.Press_SenHalPressureInfo.McpSentCrc = 0;
    Press_SenBus.Press_SenHalPressureInfo.McpSentData = 0;
    Press_SenBus.Press_SenHalPressureInfo.McpSentMsgId = 0;
    Press_SenBus.Press_SenHalPressureInfo.McpSentSerialCrc = 0;
    Press_SenBus.Press_SenHalPressureInfo.McpSentConfig = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1PresData1 = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1PresData2 = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1SentCrc = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1SentData = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1SentMsgId = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1SentSerialCrc = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp1SentConfig = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2PresData1 = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2PresData2 = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2SentCrc = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2SentData = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2SentMsgId = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2SentSerialCrc = 0;
    Press_SenBus.Press_SenHalPressureInfo.Wlp2SentConfig = 0;
    Press_SenBus.Press_SenEcuModeSts = 0;
    Press_SenBus.Press_SenFuncInhibitPressSts = 0;
    Press_SenBus.Press_SenCircP1msRawInfo.PrimCircPSig = 0;
    Press_SenBus.Press_SenCircP1msRawInfo.SecdCircPSig = 0;
    Press_SenBus.Press_SenPistP1msRawInfo.PistPSig = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_MCP_1_100_bar_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_FLP_1_100_bar_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_FRP_1_100_bar_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_RLP_1_100_bar_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_RRP_1_100_bar_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_MCP_1_100_temp_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_FLP_1_100_temp_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_FRP_1_100_temp_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_RLP_1_100_temp_Esc = 0;
    Press_SenBus.Press_SenPresCalcEsc.Pres_RRP_1_100_temp_Esc = 0;
    for(i=0;i<5;i++) Press_SenBus.Press_SenCircPBufInfo.PrimCircPRaw[i] = 0;   
    for(i=0;i<5;i++) Press_SenBus.Press_SenCircPBufInfo.SecdCircPRaw[i] = 0;   
    for(i=0;i<5;i++) Press_SenBus.Press_SenPistPBufInfo.PistPRaw[i] = 0;   
    for(i=0;i<5;i++) Press_SenBus.Press_SenPspBufInfo.PedlSimPRaw[i] = 0;   
    Press_SenBus.Press_SenPressCalcInfo.PressP_SimP_1_100_bar = 0;
    Press_SenBus.Press_SenPressCalcInfo.PressP_CirP1_1_100_bar = 0;
    Press_SenBus.Press_SenPressCalcInfo.PressP_CirP2_1_100_bar = 0;
    Press_SenBus.Press_SenPressCalcInfo.SimPMoveAvr = 0;
    Press_SenBus.Press_SenPressCalcInfo.CirPMoveAvr = 0;
    Press_SenBus.Press_SenPressCalcInfo.CirP2MoveAvr = 0;
}

void Press_Sen(void)
{
    uint16 i;
    
    Press_Sen_Timer_Start = STM0_TIM0.U;

    /* Input */
    Press_Sen_Read_Press_SenHalPressureInfo(&Press_SenBus.Press_SenHalPressureInfo);
    /*==============================================================================
    * Members of structure Press_SenHalPressureInfo 
     : Press_SenHalPressureInfo.McpPresData1;
     : Press_SenHalPressureInfo.McpPresData2;
     : Press_SenHalPressureInfo.McpSentCrc;
     : Press_SenHalPressureInfo.McpSentData;
     : Press_SenHalPressureInfo.McpSentMsgId;
     : Press_SenHalPressureInfo.McpSentSerialCrc;
     : Press_SenHalPressureInfo.McpSentConfig;
     : Press_SenHalPressureInfo.Wlp1PresData1;
     : Press_SenHalPressureInfo.Wlp1PresData2;
     : Press_SenHalPressureInfo.Wlp1SentCrc;
     : Press_SenHalPressureInfo.Wlp1SentData;
     : Press_SenHalPressureInfo.Wlp1SentMsgId;
     : Press_SenHalPressureInfo.Wlp1SentSerialCrc;
     : Press_SenHalPressureInfo.Wlp1SentConfig;
     : Press_SenHalPressureInfo.Wlp2PresData1;
     : Press_SenHalPressureInfo.Wlp2PresData2;
     : Press_SenHalPressureInfo.Wlp2SentCrc;
     : Press_SenHalPressureInfo.Wlp2SentData;
     : Press_SenHalPressureInfo.Wlp2SentMsgId;
     : Press_SenHalPressureInfo.Wlp2SentSerialCrc;
     : Press_SenHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/
    
    Press_Sen_Read_Press_SenEcuModeSts(&Press_SenBus.Press_SenEcuModeSts);
    Press_Sen_Read_Press_SenFuncInhibitPressSts(&Press_SenBus.Press_SenFuncInhibitPressSts);

    /* Process */
	Press_Sen_Calculation();
    Press_Sent_Sen_Calculation();

    /* Output */
    Press_Sen_Write_Press_SenCircP1msRawInfo(&Press_SenBus.Press_SenCircP1msRawInfo);
    /*==============================================================================
    * Members of structure Press_SenCircP1msRawInfo 
     : Press_SenCircP1msRawInfo.PrimCircPSig;
     : Press_SenCircP1msRawInfo.SecdCircPSig;
     =============================================================================*/
    
    Press_Sen_Write_Press_SenPistP1msRawInfo(&Press_SenBus.Press_SenPistP1msRawInfo);
    /*==============================================================================
    * Members of structure Press_SenPistP1msRawInfo 
     : Press_SenPistP1msRawInfo.PistPSig;
     =============================================================================*/
    
    Press_Sen_Write_Press_SenPresCalcEsc(&Press_SenBus.Press_SenPresCalcEsc);
    /*==============================================================================
    * Members of structure Press_SenPresCalcEsc 
     : Press_SenPresCalcEsc.Pres_MCP_1_100_bar_Esc;
     : Press_SenPresCalcEsc.Pres_FLP_1_100_bar_Esc;
     : Press_SenPresCalcEsc.Pres_FRP_1_100_bar_Esc;
     : Press_SenPresCalcEsc.Pres_RLP_1_100_bar_Esc;
     : Press_SenPresCalcEsc.Pres_RRP_1_100_bar_Esc;
     : Press_SenPresCalcEsc.Pres_MCP_1_100_temp_Esc;
     : Press_SenPresCalcEsc.Pres_FLP_1_100_temp_Esc;
     : Press_SenPresCalcEsc.Pres_FRP_1_100_temp_Esc;
     : Press_SenPresCalcEsc.Pres_RLP_1_100_temp_Esc;
     : Press_SenPresCalcEsc.Pres_RRP_1_100_temp_Esc;
     =============================================================================*/
    
    Press_Sen_Write_Press_SenCircPBufInfo(&Press_SenBus.Press_SenCircPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenCircPBufInfo 
     : Press_SenCircPBufInfo.PrimCircPRaw;
     : Press_SenCircPBufInfo.SecdCircPRaw;
     =============================================================================*/
    
    Press_Sen_Write_Press_SenPistPBufInfo(&Press_SenBus.Press_SenPistPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenPistPBufInfo 
     : Press_SenPistPBufInfo.PistPRaw;
     =============================================================================*/
    
    Press_Sen_Write_Press_SenPspBufInfo(&Press_SenBus.Press_SenPspBufInfo);
    /*==============================================================================
    * Members of structure Press_SenPspBufInfo 
     : Press_SenPspBufInfo.PedlSimPRaw;
     =============================================================================*/
    
    Press_Sen_Write_Press_SenPressCalcInfo(&Press_SenBus.Press_SenPressCalcInfo);
    /*==============================================================================
    * Members of structure Press_SenPressCalcInfo 
     : Press_SenPressCalcInfo.PressP_SimP_1_100_bar;
     : Press_SenPressCalcInfo.PressP_CirP1_1_100_bar;
     : Press_SenPressCalcInfo.PressP_CirP2_1_100_bar;
     : Press_SenPressCalcInfo.SimPMoveAvr;
     : Press_SenPressCalcInfo.CirPMoveAvr;
     : Press_SenPressCalcInfo.CirP2MoveAvr;
     =============================================================================*/
    

    Press_Sen_Timer_Elapsed = STM0_TIM0.U - Press_Sen_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/****************************************************************************
| NAME:             Press_Calculation
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Press sensor 1ms periodic main function
****************************************************************************/
 static void Press_Sen_Calculation(void)
 {
#if 0   /* NZ15 variant not using ADC type pressure sensor */
    Press_PortType port;
    Press_VoltageType sigVolt, zeroVolt, maxVolt;
    Press_BarType press;

    if(Press_Counter >= PRESS_BUFFER_LEN)
    {
        Press_Counter=0;// Press_ReportError(Error_Timing);
    }
    
    for(port = 0; port<PRESS_PORT_MAX_NUM; port++)
    {
        /* Input limitation */
		if(port == 0)
		{
        	sigVolt = Press_SenBus.Press_SenPressMonInfo.PrimCircPPosMon;
		}
		else if(port == 1)
		{
			sigVolt = Press_SenBus.Press_SenPressMonInfo.SecdCircPPosMon;
		}
		else if(port == 2)
		{
			sigVolt = Press_SenBus.Press_SenPressMonInfo.PedlSimPPosMon;
		}
		else ;
		
        zeroVolt = ((sint32)PRESSURE_POWER) * Press_Config[port].ZeroBarPercent / MAX_100_PERCENT;
        maxVolt = ((sint32)PRESSURE_POWER) * Press_Config[port].MaxBarPercent / MAX_100_PERCENT;
        press = (Press_BarType)(((sint32)sigVolt) - zeroVolt) \
                * Press_Config[port].MaxBar \
                / (maxVolt - zeroVolt);

		if(port == 0)
		{
        	Press_SenBus.Press_SenCircPBufInfo.PrimCircPRaw[Press_Counter] = press;
			Press_SenBus.Press_SenPistPBufInfo.PistPRaw[Press_Counter] = press;
		}
		else if(port == 1)
		{
			Press_SenBus.Press_SenCircPBufInfo.SecdCircPRaw[Press_Counter] = press;
		}
		else if(port == 2)
		{
			Press_SenBus.Press_SenPspBufInfo.PedlSimPRaw[Press_Counter] = press;
		}
		else ;
    }
    Press_Counter++;
	Press_SenBus.Press_SenCircP1msRawInfo.PrimCircPSig = Press_SenBus.Press_SenCircPBufInfo.PrimCircPRaw[4];
	Press_SenBus.Press_SenCircP1msRawInfo.SecdCircPSig = Press_SenBus.Press_SenCircPBufInfo.SecdCircPRaw[4];
	Press_SenBus.Press_SenPistP1msRawInfo.PistPSig = Press_SenBus.Press_SenPistPBufInfo.PistPRaw[4];
#endif
 }


static void Press_Sent_Sen_Calculation(void)
{
    Haluint16 PressInData[PRESS_SENT_PORT_MAX_NUM];
    Haluint16 PressInTempData[PRESS_SENT_PORT_MAX_NUM];
    Haluint8  Press_Sent_Serial_Id[PRESS_SENT_PORT_MAX_NUM];
    Salsint16 PressOutData[PRESS_SENT_PORT_MAX_NUM];
    static Salsint16 PressOutTempData[PRESS_SENT_PORT_MAX_NUM];
    Press_PortType port;
    sint32 temp;
    
    if(Press_Counter >= PRESS_BUFFER_LEN)
    {
        Press_Counter=0;// Press_ReportError(Error_Timing);
    }
    
    /* Input Copy for standized processing */

    PressInData[PRESS_SENT_PORT_MCP] = Press_SenBus.Press_SenHalPressureInfo.McpPresData1;
    PressInData[PRESS_SENT_PORT_FLP] = Press_SenBus.Press_SenHalPressureInfo.Wlp1PresData1;
    PressInData[PRESS_SENT_PORT_FRP] = Press_SenBus.Press_SenHalPressureInfo.Wlp2PresData1;

    PressInTempData[PRESS_SENT_PORT_MCP] = Press_SenBus.Press_SenHalPressureInfo.McpSentData;
    PressInTempData[PRESS_SENT_PORT_FLP] = Press_SenBus.Press_SenHalPressureInfo.Wlp1SentData;
    PressInTempData[PRESS_SENT_PORT_FRP] = Press_SenBus.Press_SenHalPressureInfo.Wlp2SentData; 

    Press_Sent_Serial_Id[PRESS_SENT_PORT_MCP] = Press_SenBus.Press_SenHalPressureInfo.McpSentMsgId;
    Press_Sent_Serial_Id[PRESS_SENT_PORT_FLP] = Press_SenBus.Press_SenHalPressureInfo.Wlp1SentMsgId;
    Press_Sent_Serial_Id[PRESS_SENT_PORT_FRP] = Press_SenBus.Press_SenHalPressureInfo.Wlp2SentMsgId;

    /* Standized processing */

    for(port = 0; port<PRESS_SENT_PORT_MAX_NUM; port++)
    {
    	temp = (sint32)(PressInData[port] - Press_Config_Sent[port].MinSentValue);
        /*
        if(PressInData[port] > Press_Config_Sent[port].MinSentValue)
        {
        temp = (sint32)(PressInData[port] - Press_Config_Sent[port].MinSentValue);
        }
        else
        {
            temp = 0;
        }
        */
        temp = (sint32)((temp * Press_Config_Sent[port].MaxBar) / (Press_Config_Sent[port].MaxSentValue - Press_Config_Sent[port].MinSentValue));
        PressOutData[port] = (sint16)temp;

        if (PRES_SENT_SERIAL_ID_TEMP1 == Press_Sent_Serial_Id[port])
        {
            temp = (sint32)(PressInTempData[port] - 265);
            temp = (sint32)(((temp*25) >> 1) - 4000);
            PressOutTempData[port] = (sint16)temp;
        }
    }

    /* Output Copy for standized processing */
    Press_SenBus.Press_SenPresCalcEsc.Pres_MCP_1_100_bar_Esc = PressOutData[PRESS_SENT_PORT_MCP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_FLP_1_100_bar_Esc = PressOutData[PRESS_SENT_PORT_FLP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_FRP_1_100_bar_Esc = PressOutData[PRESS_SENT_PORT_FRP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_RLP_1_100_bar_Esc = PressOutData[PRESS_SENT_PORT_FLP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_RRP_1_100_bar_Esc = PressOutData[PRESS_SENT_PORT_FRP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_MCP_1_100_temp_Esc = PressOutTempData[PRESS_SENT_PORT_MCP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_FLP_1_100_temp_Esc = PressOutTempData[PRESS_SENT_PORT_FLP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_FRP_1_100_temp_Esc = PressOutTempData[PRESS_SENT_PORT_FRP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_RLP_1_100_temp_Esc = PressOutTempData[PRESS_SENT_PORT_FLP];
    Press_SenBus.Press_SenPresCalcEsc.Pres_RRP_1_100_temp_Esc = PressOutTempData[PRESS_SENT_PORT_FRP];

    Press_SenBus.Press_SenCircP1msRawInfo.PrimCircPSig = \
    Press_SenBus.Press_SenCircPBufInfo.PrimCircPRaw[Press_Counter]  = PressOutData[PRESS_SENT_PORT_MCP];
    
    Press_SenBus.Press_SenPistP1msRawInfo.PistPSig = \
    Press_SenBus.Press_SenPistPBufInfo.PistPRaw[Press_Counter]      = PressOutData[PRESS_SENT_PORT_MCP];
    
    Press_SenBus.Press_SenCircP1msRawInfo.SecdCircPSig = \
    Press_SenBus.Press_SenCircPBufInfo.SecdCircPRaw[Press_Counter]  = PressOutData[PRESS_SENT_PORT_FLP];

    Press_SenBus.Press_SenPspBufInfo.PedlSimPRaw[Press_Counter]     = PressOutData[PRESS_SENT_PORT_FRP];

    Press_Counter++;    
}

#define PRESS_SEN_STOP_SEC_CODE
#include "Press_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
