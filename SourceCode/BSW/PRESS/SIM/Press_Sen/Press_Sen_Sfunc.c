#define S_FUNCTION_NAME      Press_Sen_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          23
#define WidthOutputPort         39

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Press_Sen.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Press_SenHalPressureInfo.McpPresData1 = input[0];
    Press_SenHalPressureInfo.McpPresData2 = input[1];
    Press_SenHalPressureInfo.McpSentCrc = input[2];
    Press_SenHalPressureInfo.McpSentData = input[3];
    Press_SenHalPressureInfo.McpSentMsgId = input[4];
    Press_SenHalPressureInfo.McpSentSerialCrc = input[5];
    Press_SenHalPressureInfo.McpSentConfig = input[6];
    Press_SenHalPressureInfo.Wlp1PresData1 = input[7];
    Press_SenHalPressureInfo.Wlp1PresData2 = input[8];
    Press_SenHalPressureInfo.Wlp1SentCrc = input[9];
    Press_SenHalPressureInfo.Wlp1SentData = input[10];
    Press_SenHalPressureInfo.Wlp1SentMsgId = input[11];
    Press_SenHalPressureInfo.Wlp1SentSerialCrc = input[12];
    Press_SenHalPressureInfo.Wlp1SentConfig = input[13];
    Press_SenHalPressureInfo.Wlp2PresData1 = input[14];
    Press_SenHalPressureInfo.Wlp2PresData2 = input[15];
    Press_SenHalPressureInfo.Wlp2SentCrc = input[16];
    Press_SenHalPressureInfo.Wlp2SentData = input[17];
    Press_SenHalPressureInfo.Wlp2SentMsgId = input[18];
    Press_SenHalPressureInfo.Wlp2SentSerialCrc = input[19];
    Press_SenHalPressureInfo.Wlp2SentConfig = input[20];
    Press_SenEcuModeSts = input[21];
    Press_SenFuncInhibitPressSts = input[22];

    Press_Sen();


    output[0] = Press_SenCircP1msRawInfo.PrimCircPSig;
    output[1] = Press_SenCircP1msRawInfo.SecdCircPSig;
    output[2] = Press_SenPistP1msRawInfo.PistPSig;
    output[3] = Press_SenPresCalcEsc.Pres_MCP_1_100_bar_Esc;
    output[4] = Press_SenPresCalcEsc.Pres_FLP_1_100_bar_Esc;
    output[5] = Press_SenPresCalcEsc.Pres_FRP_1_100_bar_Esc;
    output[6] = Press_SenPresCalcEsc.Pres_RLP_1_100_bar_Esc;
    output[7] = Press_SenPresCalcEsc.Pres_RRP_1_100_bar_Esc;
    output[8] = Press_SenPresCalcEsc.Pres_MCP_1_100_temp_Esc;
    output[9] = Press_SenPresCalcEsc.Pres_FLP_1_100_temp_Esc;
    output[10] = Press_SenPresCalcEsc.Pres_FRP_1_100_temp_Esc;
    output[11] = Press_SenPresCalcEsc.Pres_RLP_1_100_temp_Esc;
    output[12] = Press_SenPresCalcEsc.Pres_RRP_1_100_temp_Esc;
    output[13] = Press_SenCircPBufInfo.PrimCircPRaw[0];
    output[14] = Press_SenCircPBufInfo.PrimCircPRaw[1];
    output[15] = Press_SenCircPBufInfo.PrimCircPRaw[2];
    output[16] = Press_SenCircPBufInfo.PrimCircPRaw[3];
    output[17] = Press_SenCircPBufInfo.PrimCircPRaw[4];
    output[18] = Press_SenCircPBufInfo.SecdCircPRaw[0];
    output[19] = Press_SenCircPBufInfo.SecdCircPRaw[1];
    output[20] = Press_SenCircPBufInfo.SecdCircPRaw[2];
    output[21] = Press_SenCircPBufInfo.SecdCircPRaw[3];
    output[22] = Press_SenCircPBufInfo.SecdCircPRaw[4];
    output[23] = Press_SenPistPBufInfo.PistPRaw[0];
    output[24] = Press_SenPistPBufInfo.PistPRaw[1];
    output[25] = Press_SenPistPBufInfo.PistPRaw[2];
    output[26] = Press_SenPistPBufInfo.PistPRaw[3];
    output[27] = Press_SenPistPBufInfo.PistPRaw[4];
    output[28] = Press_SenPspBufInfo.PedlSimPRaw[0];
    output[29] = Press_SenPspBufInfo.PedlSimPRaw[1];
    output[30] = Press_SenPspBufInfo.PedlSimPRaw[2];
    output[31] = Press_SenPspBufInfo.PedlSimPRaw[3];
    output[32] = Press_SenPspBufInfo.PedlSimPRaw[4];
    output[33] = Press_SenPressCalcInfo.PressP_SimP_1_100_bar;
    output[34] = Press_SenPressCalcInfo.PressP_CirP1_1_100_bar;
    output[35] = Press_SenPressCalcInfo.PressP_CirP2_1_100_bar;
    output[36] = Press_SenPressCalcInfo.SimPMoveAvr;
    output[37] = Press_SenPressCalcInfo.CirPMoveAvr;
    output[38] = Press_SenPressCalcInfo.CirP2MoveAvr;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Press_Sen_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
