#define S_FUNCTION_NAME      Press_SenSync_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          22
#define WidthOutputPort         10

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Press_SenSync.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Press_SenSyncCircPBufInfo.PrimCircPRaw[0] = input[0];
    Press_SenSyncCircPBufInfo.PrimCircPRaw[1] = input[1];
    Press_SenSyncCircPBufInfo.PrimCircPRaw[2] = input[2];
    Press_SenSyncCircPBufInfo.PrimCircPRaw[3] = input[3];
    Press_SenSyncCircPBufInfo.PrimCircPRaw[4] = input[4];
    Press_SenSyncCircPBufInfo.SecdCircPRaw[0] = input[5];
    Press_SenSyncCircPBufInfo.SecdCircPRaw[1] = input[6];
    Press_SenSyncCircPBufInfo.SecdCircPRaw[2] = input[7];
    Press_SenSyncCircPBufInfo.SecdCircPRaw[3] = input[8];
    Press_SenSyncCircPBufInfo.SecdCircPRaw[4] = input[9];
    Press_SenSyncPistPBufInfo.PistPRaw[0] = input[10];
    Press_SenSyncPistPBufInfo.PistPRaw[1] = input[11];
    Press_SenSyncPistPBufInfo.PistPRaw[2] = input[12];
    Press_SenSyncPistPBufInfo.PistPRaw[3] = input[13];
    Press_SenSyncPistPBufInfo.PistPRaw[4] = input[14];
    Press_SenSyncPspBufInfo.PedlSimPRaw[0] = input[15];
    Press_SenSyncPspBufInfo.PedlSimPRaw[1] = input[16];
    Press_SenSyncPspBufInfo.PedlSimPRaw[2] = input[17];
    Press_SenSyncPspBufInfo.PedlSimPRaw[3] = input[18];
    Press_SenSyncPspBufInfo.PedlSimPRaw[4] = input[19];
    Press_SenSyncEcuModeSts = input[20];
    Press_SenSyncFuncInhibitPressSts = input[21];

    Press_SenSync();


    output[0] = Press_SenSyncCircP5msRawInfo.PrimCircPSig;
    output[1] = Press_SenSyncCircP5msRawInfo.SecdCircPSig;
    output[2] = Press_SenSyncPistP5msRawInfo.PistPSig;
    output[3] = Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAve;
    output[4] = Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAve;
    output[5] = Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAve;
    output[6] = Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs;
    output[7] = Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAveEolOfs;
    output[8] = Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAveEolOfs;
    output[9] = Press_SenSyncPedlSimPRaw;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Press_SenSync_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
