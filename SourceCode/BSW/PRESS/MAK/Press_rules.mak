# \file
#
# \brief Press
#
# This file contains the implementation of the SWC
# module Press.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Press_src

Press_src_FILES        += $(Press_SRC_PATH)\Press_Sen.c
Press_src_FILES        += $(Press_IFA_PATH)\Press_Sen_Ifa.c
Press_src_FILES        += $(Press_SRC_PATH)\Press_SenSync.c
Press_src_FILES        += $(Press_IFA_PATH)\Press_SenSync_Ifa.c
Press_src_FILES        += $(Press_CFG_PATH)\Press_Cfg.c
Press_src_FILES        += $(Press_CAL_PATH)\Press_Cal.c

ifeq ($(ICE_COMPILE),true)
Press_src_FILES        += $(Press_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Press_src_FILES        += $(Press_UNITY_PATH)\unity.c
	Press_src_FILES        += $(Press_UNITY_PATH)\unity_fixture.c	
	Press_src_FILES        += $(Press_UT_PATH)\main.c
	Press_src_FILES        += $(Press_UT_PATH)\Press_Sen\Press_Sen_UtMain.c
	Press_src_FILES        += $(Press_UT_PATH)\Press_SenSync\Press_SenSync_UtMain.c
endif