# \file
#
# \brief Press
#
# This file contains the implementation of the SWC
# module Press.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Press_CORE_PATH     := $(MANDO_BSW_ROOT)\Press
Press_CAL_PATH      := $(Press_CORE_PATH)\CAL\$(Press_VARIANT)
Press_SRC_PATH      := $(Press_CORE_PATH)\SRC
Press_CFG_PATH      := $(Press_CORE_PATH)\CFG\$(Press_VARIANT)
Press_HDR_PATH      := $(Press_CORE_PATH)\HDR
Press_IFA_PATH      := $(Press_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Press_CMN_PATH      := $(Press_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Press_UT_PATH		:= $(Press_CORE_PATH)\UT
	Press_UNITY_PATH	:= $(Press_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Press_UT_PATH)
	CC_INCLUDE_PATH		+= $(Press_UNITY_PATH)
	Press_Sen_PATH 	:= Press_UT_PATH\Press_Sen
	Press_SenSync_PATH 	:= Press_UT_PATH\Press_SenSync
endif
CC_INCLUDE_PATH    += $(Press_CAL_PATH)
CC_INCLUDE_PATH    += $(Press_SRC_PATH)
CC_INCLUDE_PATH    += $(Press_CFG_PATH)
CC_INCLUDE_PATH    += $(Press_HDR_PATH)
CC_INCLUDE_PATH    += $(Press_IFA_PATH)
CC_INCLUDE_PATH    += $(Press_CMN_PATH)

