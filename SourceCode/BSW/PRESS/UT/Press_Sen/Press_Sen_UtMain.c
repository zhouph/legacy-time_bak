#include "unity.h"
#include "unity_fixture.h"
#include "Press_Sen.h"
#include "Press_Sen_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint16 UtInput_Press_SenHalPressureInfo_McpPresData1[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPPRESDATA1;
const Haluint16 UtInput_Press_SenHalPressureInfo_McpPresData2[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPPRESDATA2;
const Haluint8 UtInput_Press_SenHalPressureInfo_McpSentCrc[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPSENTCRC;
const Haluint16 UtInput_Press_SenHalPressureInfo_McpSentData[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPSENTDATA;
const Haluint8 UtInput_Press_SenHalPressureInfo_McpSentMsgId[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPSENTMSGID;
const Haluint8 UtInput_Press_SenHalPressureInfo_McpSentSerialCrc[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPSENTSERIALCRC;
const Haluint8 UtInput_Press_SenHalPressureInfo_McpSentConfig[MAX_STEP] = PRESS_SENHALPRESSUREINFO_MCPSENTCONFIG;
const Haluint16 UtInput_Press_SenHalPressureInfo_Wlp1PresData1[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1PRESDATA1;
const Haluint16 UtInput_Press_SenHalPressureInfo_Wlp1PresData2[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1PRESDATA2;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp1SentCrc[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1SENTCRC;
const Haluint16 UtInput_Press_SenHalPressureInfo_Wlp1SentData[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1SENTDATA;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp1SentMsgId[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1SENTMSGID;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp1SentSerialCrc[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1SENTSERIALCRC;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp1SentConfig[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP1SENTCONFIG;
const Haluint16 UtInput_Press_SenHalPressureInfo_Wlp2PresData1[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2PRESDATA1;
const Haluint16 UtInput_Press_SenHalPressureInfo_Wlp2PresData2[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2PRESDATA2;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp2SentCrc[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2SENTCRC;
const Haluint16 UtInput_Press_SenHalPressureInfo_Wlp2SentData[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2SENTDATA;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp2SentMsgId[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2SENTMSGID;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp2SentSerialCrc[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2SENTSERIALCRC;
const Haluint8 UtInput_Press_SenHalPressureInfo_Wlp2SentConfig[MAX_STEP] = PRESS_SENHALPRESSUREINFO_WLP2SENTCONFIG;
const Mom_HndlrEcuModeSts_t UtInput_Press_SenEcuModeSts[MAX_STEP] = PRESS_SENECUMODESTS;
const Eem_SuspcDetnFuncInhibitPressSts_t UtInput_Press_SenFuncInhibitPressSts[MAX_STEP] = PRESS_SENFUNCINHIBITPRESSSTS;

const Salsint16 UtExpected_Press_SenCircP1msRawInfo_PrimCircPSig[MAX_STEP] = PRESS_SENCIRCP1MSRAWINFO_PRIMCIRCPSIG;
const Salsint16 UtExpected_Press_SenCircP1msRawInfo_SecdCircPSig[MAX_STEP] = PRESS_SENCIRCP1MSRAWINFO_SECDCIRCPSIG;
const Salsint16 UtExpected_Press_SenPistP1msRawInfo_PistPSig[MAX_STEP] = PRESS_SENPISTP1MSRAWINFO_PISTPSIG;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_MCP_1_100_bar_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_MCP_1_100_BAR_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_FLP_1_100_bar_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_FLP_1_100_BAR_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_FRP_1_100_bar_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_FRP_1_100_BAR_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_RLP_1_100_bar_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_RLP_1_100_BAR_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_RRP_1_100_bar_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_RRP_1_100_BAR_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_MCP_1_100_temp_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_MCP_1_100_TEMP_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_FLP_1_100_temp_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_FLP_1_100_TEMP_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_FRP_1_100_temp_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_FRP_1_100_TEMP_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_RLP_1_100_temp_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_RLP_1_100_TEMP_ESC;
const Salsint16 UtExpected_Press_SenPresCalcEsc_Pres_RRP_1_100_temp_Esc[MAX_STEP] = PRESS_SENPRESCALCESC_PRES_RRP_1_100_TEMP_ESC;
const Salsint16 UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_0[MAX_STEP] = PRESS_SENCIRCPBUFINFO_PRIMCIRCPRAW_0;
const Salsint16 UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_1[MAX_STEP] = PRESS_SENCIRCPBUFINFO_PRIMCIRCPRAW_1;
const Salsint16 UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_2[MAX_STEP] = PRESS_SENCIRCPBUFINFO_PRIMCIRCPRAW_2;
const Salsint16 UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_3[MAX_STEP] = PRESS_SENCIRCPBUFINFO_PRIMCIRCPRAW_3;
const Salsint16 UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_4[MAX_STEP] = PRESS_SENCIRCPBUFINFO_PRIMCIRCPRAW_4;
const Salsint16 UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_0[MAX_STEP] = PRESS_SENCIRCPBUFINFO_SECDCIRCPRAW_0;
const Salsint16 UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_1[MAX_STEP] = PRESS_SENCIRCPBUFINFO_SECDCIRCPRAW_1;
const Salsint16 UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_2[MAX_STEP] = PRESS_SENCIRCPBUFINFO_SECDCIRCPRAW_2;
const Salsint16 UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_3[MAX_STEP] = PRESS_SENCIRCPBUFINFO_SECDCIRCPRAW_3;
const Salsint16 UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_4[MAX_STEP] = PRESS_SENCIRCPBUFINFO_SECDCIRCPRAW_4;
const Salsint16 UtExpected_Press_SenPistPBufInfo_PistPRaw_0[MAX_STEP] = PRESS_SENPISTPBUFINFO_PISTPRAW_0;
const Salsint16 UtExpected_Press_SenPistPBufInfo_PistPRaw_1[MAX_STEP] = PRESS_SENPISTPBUFINFO_PISTPRAW_1;
const Salsint16 UtExpected_Press_SenPistPBufInfo_PistPRaw_2[MAX_STEP] = PRESS_SENPISTPBUFINFO_PISTPRAW_2;
const Salsint16 UtExpected_Press_SenPistPBufInfo_PistPRaw_3[MAX_STEP] = PRESS_SENPISTPBUFINFO_PISTPRAW_3;
const Salsint16 UtExpected_Press_SenPistPBufInfo_PistPRaw_4[MAX_STEP] = PRESS_SENPISTPBUFINFO_PISTPRAW_4;
const Salsint16 UtExpected_Press_SenPspBufInfo_PedlSimPRaw_0[MAX_STEP] = PRESS_SENPSPBUFINFO_PEDLSIMPRAW_0;
const Salsint16 UtExpected_Press_SenPspBufInfo_PedlSimPRaw_1[MAX_STEP] = PRESS_SENPSPBUFINFO_PEDLSIMPRAW_1;
const Salsint16 UtExpected_Press_SenPspBufInfo_PedlSimPRaw_2[MAX_STEP] = PRESS_SENPSPBUFINFO_PEDLSIMPRAW_2;
const Salsint16 UtExpected_Press_SenPspBufInfo_PedlSimPRaw_3[MAX_STEP] = PRESS_SENPSPBUFINFO_PEDLSIMPRAW_3;
const Salsint16 UtExpected_Press_SenPspBufInfo_PedlSimPRaw_4[MAX_STEP] = PRESS_SENPSPBUFINFO_PEDLSIMPRAW_4;
const Saluint16 UtExpected_Press_SenPressCalcInfo_PressP_SimP_1_100_bar[MAX_STEP] = PRESS_SENPRESSCALCINFO_PRESSP_SIMP_1_100_BAR;
const Saluint16 UtExpected_Press_SenPressCalcInfo_PressP_CirP1_1_100_bar[MAX_STEP] = PRESS_SENPRESSCALCINFO_PRESSP_CIRP1_1_100_BAR;
const Saluint16 UtExpected_Press_SenPressCalcInfo_PressP_CirP2_1_100_bar[MAX_STEP] = PRESS_SENPRESSCALCINFO_PRESSP_CIRP2_1_100_BAR;
const Saluint16 UtExpected_Press_SenPressCalcInfo_SimPMoveAvr[MAX_STEP] = PRESS_SENPRESSCALCINFO_SIMPMOVEAVR;
const Saluint16 UtExpected_Press_SenPressCalcInfo_CirPMoveAvr[MAX_STEP] = PRESS_SENPRESSCALCINFO_CIRPMOVEAVR;
const Saluint16 UtExpected_Press_SenPressCalcInfo_CirP2MoveAvr[MAX_STEP] = PRESS_SENPRESSCALCINFO_CIRP2MOVEAVR;



TEST_GROUP(Press_Sen);
TEST_SETUP(Press_Sen)
{
    Press_Sen_Init();
}

TEST_TEAR_DOWN(Press_Sen)
{   /* Postcondition */

}

TEST(Press_Sen, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Press_SenHalPressureInfo.McpPresData1 = UtInput_Press_SenHalPressureInfo_McpPresData1[i];
        Press_SenHalPressureInfo.McpPresData2 = UtInput_Press_SenHalPressureInfo_McpPresData2[i];
        Press_SenHalPressureInfo.McpSentCrc = UtInput_Press_SenHalPressureInfo_McpSentCrc[i];
        Press_SenHalPressureInfo.McpSentData = UtInput_Press_SenHalPressureInfo_McpSentData[i];
        Press_SenHalPressureInfo.McpSentMsgId = UtInput_Press_SenHalPressureInfo_McpSentMsgId[i];
        Press_SenHalPressureInfo.McpSentSerialCrc = UtInput_Press_SenHalPressureInfo_McpSentSerialCrc[i];
        Press_SenHalPressureInfo.McpSentConfig = UtInput_Press_SenHalPressureInfo_McpSentConfig[i];
        Press_SenHalPressureInfo.Wlp1PresData1 = UtInput_Press_SenHalPressureInfo_Wlp1PresData1[i];
        Press_SenHalPressureInfo.Wlp1PresData2 = UtInput_Press_SenHalPressureInfo_Wlp1PresData2[i];
        Press_SenHalPressureInfo.Wlp1SentCrc = UtInput_Press_SenHalPressureInfo_Wlp1SentCrc[i];
        Press_SenHalPressureInfo.Wlp1SentData = UtInput_Press_SenHalPressureInfo_Wlp1SentData[i];
        Press_SenHalPressureInfo.Wlp1SentMsgId = UtInput_Press_SenHalPressureInfo_Wlp1SentMsgId[i];
        Press_SenHalPressureInfo.Wlp1SentSerialCrc = UtInput_Press_SenHalPressureInfo_Wlp1SentSerialCrc[i];
        Press_SenHalPressureInfo.Wlp1SentConfig = UtInput_Press_SenHalPressureInfo_Wlp1SentConfig[i];
        Press_SenHalPressureInfo.Wlp2PresData1 = UtInput_Press_SenHalPressureInfo_Wlp2PresData1[i];
        Press_SenHalPressureInfo.Wlp2PresData2 = UtInput_Press_SenHalPressureInfo_Wlp2PresData2[i];
        Press_SenHalPressureInfo.Wlp2SentCrc = UtInput_Press_SenHalPressureInfo_Wlp2SentCrc[i];
        Press_SenHalPressureInfo.Wlp2SentData = UtInput_Press_SenHalPressureInfo_Wlp2SentData[i];
        Press_SenHalPressureInfo.Wlp2SentMsgId = UtInput_Press_SenHalPressureInfo_Wlp2SentMsgId[i];
        Press_SenHalPressureInfo.Wlp2SentSerialCrc = UtInput_Press_SenHalPressureInfo_Wlp2SentSerialCrc[i];
        Press_SenHalPressureInfo.Wlp2SentConfig = UtInput_Press_SenHalPressureInfo_Wlp2SentConfig[i];
        Press_SenEcuModeSts = UtInput_Press_SenEcuModeSts[i];
        Press_SenFuncInhibitPressSts = UtInput_Press_SenFuncInhibitPressSts[i];

        Press_Sen();

        TEST_ASSERT_EQUAL(Press_SenCircP1msRawInfo.PrimCircPSig, UtExpected_Press_SenCircP1msRawInfo_PrimCircPSig[i]);
        TEST_ASSERT_EQUAL(Press_SenCircP1msRawInfo.SecdCircPSig, UtExpected_Press_SenCircP1msRawInfo_SecdCircPSig[i]);
        TEST_ASSERT_EQUAL(Press_SenPistP1msRawInfo.PistPSig, UtExpected_Press_SenPistP1msRawInfo_PistPSig[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_MCP_1_100_bar_Esc, UtExpected_Press_SenPresCalcEsc_Pres_MCP_1_100_bar_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_FLP_1_100_bar_Esc, UtExpected_Press_SenPresCalcEsc_Pres_FLP_1_100_bar_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_FRP_1_100_bar_Esc, UtExpected_Press_SenPresCalcEsc_Pres_FRP_1_100_bar_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_RLP_1_100_bar_Esc, UtExpected_Press_SenPresCalcEsc_Pres_RLP_1_100_bar_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_RRP_1_100_bar_Esc, UtExpected_Press_SenPresCalcEsc_Pres_RRP_1_100_bar_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_MCP_1_100_temp_Esc, UtExpected_Press_SenPresCalcEsc_Pres_MCP_1_100_temp_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_FLP_1_100_temp_Esc, UtExpected_Press_SenPresCalcEsc_Pres_FLP_1_100_temp_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_FRP_1_100_temp_Esc, UtExpected_Press_SenPresCalcEsc_Pres_FRP_1_100_temp_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_RLP_1_100_temp_Esc, UtExpected_Press_SenPresCalcEsc_Pres_RLP_1_100_temp_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenPresCalcEsc.Pres_RRP_1_100_temp_Esc, UtExpected_Press_SenPresCalcEsc_Pres_RRP_1_100_temp_Esc[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.PrimCircPRaw[0], UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_0[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.PrimCircPRaw[1], UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_1[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.PrimCircPRaw[2], UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_2[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.PrimCircPRaw[3], UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_3[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.PrimCircPRaw[4], UtExpected_Press_SenCircPBufInfo_PrimCircPRaw_4[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.SecdCircPRaw[0], UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_0[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.SecdCircPRaw[1], UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_1[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.SecdCircPRaw[2], UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_2[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.SecdCircPRaw[3], UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_3[i]);
        TEST_ASSERT_EQUAL(Press_SenCircPBufInfo.SecdCircPRaw[4], UtExpected_Press_SenCircPBufInfo_SecdCircPRaw_4[i]);
        TEST_ASSERT_EQUAL(Press_SenPistPBufInfo.PistPRaw[0], UtExpected_Press_SenPistPBufInfo_PistPRaw_0[i]);
        TEST_ASSERT_EQUAL(Press_SenPistPBufInfo.PistPRaw[1], UtExpected_Press_SenPistPBufInfo_PistPRaw_1[i]);
        TEST_ASSERT_EQUAL(Press_SenPistPBufInfo.PistPRaw[2], UtExpected_Press_SenPistPBufInfo_PistPRaw_2[i]);
        TEST_ASSERT_EQUAL(Press_SenPistPBufInfo.PistPRaw[3], UtExpected_Press_SenPistPBufInfo_PistPRaw_3[i]);
        TEST_ASSERT_EQUAL(Press_SenPistPBufInfo.PistPRaw[4], UtExpected_Press_SenPistPBufInfo_PistPRaw_4[i]);
        TEST_ASSERT_EQUAL(Press_SenPspBufInfo.PedlSimPRaw[0], UtExpected_Press_SenPspBufInfo_PedlSimPRaw_0[i]);
        TEST_ASSERT_EQUAL(Press_SenPspBufInfo.PedlSimPRaw[1], UtExpected_Press_SenPspBufInfo_PedlSimPRaw_1[i]);
        TEST_ASSERT_EQUAL(Press_SenPspBufInfo.PedlSimPRaw[2], UtExpected_Press_SenPspBufInfo_PedlSimPRaw_2[i]);
        TEST_ASSERT_EQUAL(Press_SenPspBufInfo.PedlSimPRaw[3], UtExpected_Press_SenPspBufInfo_PedlSimPRaw_3[i]);
        TEST_ASSERT_EQUAL(Press_SenPspBufInfo.PedlSimPRaw[4], UtExpected_Press_SenPspBufInfo_PedlSimPRaw_4[i]);
        TEST_ASSERT_EQUAL(Press_SenPressCalcInfo.PressP_SimP_1_100_bar, UtExpected_Press_SenPressCalcInfo_PressP_SimP_1_100_bar[i]);
        TEST_ASSERT_EQUAL(Press_SenPressCalcInfo.PressP_CirP1_1_100_bar, UtExpected_Press_SenPressCalcInfo_PressP_CirP1_1_100_bar[i]);
        TEST_ASSERT_EQUAL(Press_SenPressCalcInfo.PressP_CirP2_1_100_bar, UtExpected_Press_SenPressCalcInfo_PressP_CirP2_1_100_bar[i]);
        TEST_ASSERT_EQUAL(Press_SenPressCalcInfo.SimPMoveAvr, UtExpected_Press_SenPressCalcInfo_SimPMoveAvr[i]);
        TEST_ASSERT_EQUAL(Press_SenPressCalcInfo.CirPMoveAvr, UtExpected_Press_SenPressCalcInfo_CirPMoveAvr[i]);
        TEST_ASSERT_EQUAL(Press_SenPressCalcInfo.CirP2MoveAvr, UtExpected_Press_SenPressCalcInfo_CirP2MoveAvr[i]);
    }
}

TEST_GROUP_RUNNER(Press_Sen)
{
    RUN_TEST_CASE(Press_Sen, All);
}
