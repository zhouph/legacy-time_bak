#include "unity.h"
#include "unity_fixture.h"
#include "Press_SenSync.h"
#include "Press_SenSync_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_0[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_PRIMCIRCPRAW_0;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_1[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_PRIMCIRCPRAW_1;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_2[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_PRIMCIRCPRAW_2;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_3[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_PRIMCIRCPRAW_3;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_4[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_PRIMCIRCPRAW_4;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_0[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_SECDCIRCPRAW_0;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_1[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_SECDCIRCPRAW_1;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_2[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_SECDCIRCPRAW_2;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_3[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_SECDCIRCPRAW_3;
const Salsint16 UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_4[MAX_STEP] = PRESS_SENSYNCCIRCPBUFINFO_SECDCIRCPRAW_4;
const Salsint16 UtInput_Press_SenSyncPistPBufInfo_PistPRaw_0[MAX_STEP] = PRESS_SENSYNCPISTPBUFINFO_PISTPRAW_0;
const Salsint16 UtInput_Press_SenSyncPistPBufInfo_PistPRaw_1[MAX_STEP] = PRESS_SENSYNCPISTPBUFINFO_PISTPRAW_1;
const Salsint16 UtInput_Press_SenSyncPistPBufInfo_PistPRaw_2[MAX_STEP] = PRESS_SENSYNCPISTPBUFINFO_PISTPRAW_2;
const Salsint16 UtInput_Press_SenSyncPistPBufInfo_PistPRaw_3[MAX_STEP] = PRESS_SENSYNCPISTPBUFINFO_PISTPRAW_3;
const Salsint16 UtInput_Press_SenSyncPistPBufInfo_PistPRaw_4[MAX_STEP] = PRESS_SENSYNCPISTPBUFINFO_PISTPRAW_4;
const Salsint16 UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_0[MAX_STEP] = PRESS_SENSYNCPSPBUFINFO_PEDLSIMPRAW_0;
const Salsint16 UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_1[MAX_STEP] = PRESS_SENSYNCPSPBUFINFO_PEDLSIMPRAW_1;
const Salsint16 UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_2[MAX_STEP] = PRESS_SENSYNCPSPBUFINFO_PEDLSIMPRAW_2;
const Salsint16 UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_3[MAX_STEP] = PRESS_SENSYNCPSPBUFINFO_PEDLSIMPRAW_3;
const Salsint16 UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_4[MAX_STEP] = PRESS_SENSYNCPSPBUFINFO_PEDLSIMPRAW_4;
const Mom_HndlrEcuModeSts_t UtInput_Press_SenSyncEcuModeSts[MAX_STEP] = PRESS_SENSYNCECUMODESTS;
const Eem_SuspcDetnFuncInhibitPressSts_t UtInput_Press_SenSyncFuncInhibitPressSts[MAX_STEP] = PRESS_SENSYNCFUNCINHIBITPRESSSTS;

const Salsint16 UtExpected_Press_SenSyncCircP5msRawInfo_PrimCircPSig[MAX_STEP] = PRESS_SENSYNCCIRCP5MSRAWINFO_PRIMCIRCPSIG;
const Salsint16 UtExpected_Press_SenSyncCircP5msRawInfo_SecdCircPSig[MAX_STEP] = PRESS_SENSYNCCIRCP5MSRAWINFO_SECDCIRCPSIG;
const Salsint16 UtExpected_Press_SenSyncPistP5msRawInfo_PistPSig[MAX_STEP] = PRESS_SENSYNCPISTP5MSRAWINFO_PISTPSIG;
const Saluint16 UtExpected_Press_SenSyncPressSenCalcInfo_PressP_SimPMoveAve[MAX_STEP] = PRESS_SENSYNCPRESSSENCALCINFO_PRESSP_SIMPMOVEAVE;
const Saluint16 UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP1MoveAve[MAX_STEP] = PRESS_SENSYNCPRESSSENCALCINFO_PRESSP_CIRP1MOVEAVE;
const Saluint16 UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP2MoveAve[MAX_STEP] = PRESS_SENSYNCPRESSSENCALCINFO_PRESSP_CIRP2MOVEAVE;
const Saluint16 UtExpected_Press_SenSyncPressSenCalcInfo_PressP_SimPMoveAveEolOfs[MAX_STEP] = PRESS_SENSYNCPRESSSENCALCINFO_PRESSP_SIMPMOVEAVEEOLOFS;
const Saluint16 UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP1MoveAveEolOfs[MAX_STEP] = PRESS_SENSYNCPRESSSENCALCINFO_PRESSP_CIRP1MOVEAVEEOLOFS;
const Saluint16 UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP2MoveAveEolOfs[MAX_STEP] = PRESS_SENSYNCPRESSSENCALCINFO_PRESSP_CIRP2MOVEAVEEOLOFS;
const Press_SenSyncPedlSimPRaw_t UtExpected_Press_SenSyncPedlSimPRaw[MAX_STEP] = PRESS_SENSYNCPEDLSIMPRAW;



TEST_GROUP(Press_SenSync);
TEST_SETUP(Press_SenSync)
{
    Press_SenSync_Init();
}

TEST_TEAR_DOWN(Press_SenSync)
{   /* Postcondition */

}

TEST(Press_SenSync, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Press_SenSyncCircPBufInfo.PrimCircPRaw[0] = UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_0[i];
        Press_SenSyncCircPBufInfo.PrimCircPRaw[1] = UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_1[i];
        Press_SenSyncCircPBufInfo.PrimCircPRaw[2] = UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_2[i];
        Press_SenSyncCircPBufInfo.PrimCircPRaw[3] = UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_3[i];
        Press_SenSyncCircPBufInfo.PrimCircPRaw[4] = UtInput_Press_SenSyncCircPBufInfo_PrimCircPRaw_4[i];
        Press_SenSyncCircPBufInfo.SecdCircPRaw[0] = UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_0[i];
        Press_SenSyncCircPBufInfo.SecdCircPRaw[1] = UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_1[i];
        Press_SenSyncCircPBufInfo.SecdCircPRaw[2] = UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_2[i];
        Press_SenSyncCircPBufInfo.SecdCircPRaw[3] = UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_3[i];
        Press_SenSyncCircPBufInfo.SecdCircPRaw[4] = UtInput_Press_SenSyncCircPBufInfo_SecdCircPRaw_4[i];
        Press_SenSyncPistPBufInfo.PistPRaw[0] = UtInput_Press_SenSyncPistPBufInfo_PistPRaw_0[i];
        Press_SenSyncPistPBufInfo.PistPRaw[1] = UtInput_Press_SenSyncPistPBufInfo_PistPRaw_1[i];
        Press_SenSyncPistPBufInfo.PistPRaw[2] = UtInput_Press_SenSyncPistPBufInfo_PistPRaw_2[i];
        Press_SenSyncPistPBufInfo.PistPRaw[3] = UtInput_Press_SenSyncPistPBufInfo_PistPRaw_3[i];
        Press_SenSyncPistPBufInfo.PistPRaw[4] = UtInput_Press_SenSyncPistPBufInfo_PistPRaw_4[i];
        Press_SenSyncPspBufInfo.PedlSimPRaw[0] = UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_0[i];
        Press_SenSyncPspBufInfo.PedlSimPRaw[1] = UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_1[i];
        Press_SenSyncPspBufInfo.PedlSimPRaw[2] = UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_2[i];
        Press_SenSyncPspBufInfo.PedlSimPRaw[3] = UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_3[i];
        Press_SenSyncPspBufInfo.PedlSimPRaw[4] = UtInput_Press_SenSyncPspBufInfo_PedlSimPRaw_4[i];
        Press_SenSyncEcuModeSts = UtInput_Press_SenSyncEcuModeSts[i];
        Press_SenSyncFuncInhibitPressSts = UtInput_Press_SenSyncFuncInhibitPressSts[i];

        Press_SenSync();

        TEST_ASSERT_EQUAL(Press_SenSyncCircP5msRawInfo.PrimCircPSig, UtExpected_Press_SenSyncCircP5msRawInfo_PrimCircPSig[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncCircP5msRawInfo.SecdCircPSig, UtExpected_Press_SenSyncCircP5msRawInfo_SecdCircPSig[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPistP5msRawInfo.PistPSig, UtExpected_Press_SenSyncPistP5msRawInfo_PistPSig[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAve, UtExpected_Press_SenSyncPressSenCalcInfo_PressP_SimPMoveAve[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAve, UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP1MoveAve[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAve, UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP2MoveAve[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs, UtExpected_Press_SenSyncPressSenCalcInfo_PressP_SimPMoveAveEolOfs[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPressSenCalcInfo.PressP_CirP1MoveAveEolOfs, UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP1MoveAveEolOfs[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPressSenCalcInfo.PressP_CirP2MoveAveEolOfs, UtExpected_Press_SenSyncPressSenCalcInfo_PressP_CirP2MoveAveEolOfs[i]);
        TEST_ASSERT_EQUAL(Press_SenSyncPedlSimPRaw, UtExpected_Press_SenSyncPedlSimPRaw[i]);
    }
}

TEST_GROUP_RUNNER(Press_SenSync)
{
    RUN_TEST_CASE(Press_SenSync, All);
}
