/**
 * @defgroup Press_Types Press_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_TYPES_H_
#define PRESS_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define PRESS_BUFFER_LEN   5

#define PRES_SENT_SERIAL_ID_TEMP1   (0x10)
#define PRES_SENT_SERIAL_ID_TEMP2   (0x15)
#define PRES_SENT_SERIAL_ID_DIAG    (0x01)
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputSR1msHalPressureInfo_t Press_SenHalPressureInfo;
    Mom_HndlrEcuModeSts_t Press_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitPressSts_t Press_SenFuncInhibitPressSts;

/* Output Data Element */
    Press_SenCircP1msRawInfo_t Press_SenCircP1msRawInfo;
    Press_SenPistP1msRawInfo_t Press_SenPistP1msRawInfo;
    Press_SenPresCalcEsc_t Press_SenPresCalcEsc;
    Press_SenCircPBufInfo_t Press_SenCircPBufInfo;
    Press_SenPistPBufInfo_t Press_SenPistPBufInfo;
    Press_SenPspBufInfo_t Press_SenPspBufInfo;
    Press_SenPressCalcInfo_t Press_SenPressCalcInfo;
}Press_Sen_HdrBusType;

typedef struct
{
/* Input Data Element */
    Press_SenCircPBufInfo_t Press_SenSyncCircPBufInfo;
    Press_SenPistPBufInfo_t Press_SenSyncPistPBufInfo;
    Press_SenPspBufInfo_t Press_SenSyncPspBufInfo;
    Mom_HndlrEcuModeSts_t Press_SenSyncEcuModeSts;
    Eem_SuspcDetnFuncInhibitPressSts_t Press_SenSyncFuncInhibitPressSts;

/* Output Data Element */
    Press_SenSyncCircP5msRawInfo_t Press_SenSyncCircP5msRawInfo;
    Press_SenSyncPistP5msRawInfo_t Press_SenSyncPistP5msRawInfo;
    Press_SenSyncPressSenCalcInfo_t Press_SenSyncPressSenCalcInfo;
    Press_SenSyncPedlSimPRaw_t Press_SenSyncPedlSimPRaw;
}Press_SenSync_HdrBusType;
 /* Voltage sense data type */
 typedef uint16 Press_VoltageType;
 /* Requesting buffer length type */
 typedef uint8 Press_BufLengthType;
 /* Percent Type */
 typedef uint16 Press_PercentType;

 /* Sent Type */
 typedef uint16 Press_SentType;
 
 /* Bar Type */
 typedef sint16 Press_BarType;
 /* ECU Signal port type */
 typedef uint16 Press_SigPortType;
 /* Press port type */
 typedef uint8 Press_PortType;
 /* Press raw data type */
 typedef sint16 Press_RawDataType;

 typedef struct
 {
    Press_PercentType   ZeroBarPercent;
    Press_PercentType   MaxBarPercent;
    Press_BarType       MaxBar;
 }Press_ConfigType;

typedef struct
{
    Press_SentType      MaxSentValue;
    Press_SentType      MinSentValue;
    Press_BarType       MaxBar;
    
}Press_ConfigSentType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_TYPES_H_ */
/** @} */
