/**
 * @defgroup Press_SenSync Press_SenSync
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_SenSync.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_SENSYNC_H_
#define PRESS_SENSYNC_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_Types.h"
#include "Press_Cfg.h"
#include "Press_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PRESS_SENSYNC_MODULE_ID      (0)
 #define PRESS_SENSYNC_MAJOR_VERSION  (2)
 #define PRESS_SENSYNC_MINOR_VERSION  (0)
 #define PRESS_SENSYNC_PATCH_VERSION  (0)
 #define PRESS_SENSYNC_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Press_SenSync_HdrBusType Press_SenSyncBus;

/* Version Info */
extern const SwcVersionInfo_t Press_SenSyncVersionInfo;

/* Input Data Element */
extern Press_SenCircPBufInfo_t Press_SenSyncCircPBufInfo;
extern Press_SenPistPBufInfo_t Press_SenSyncPistPBufInfo;
extern Press_SenPspBufInfo_t Press_SenSyncPspBufInfo;
extern Mom_HndlrEcuModeSts_t Press_SenSyncEcuModeSts;
extern Eem_SuspcDetnFuncInhibitPressSts_t Press_SenSyncFuncInhibitPressSts;

/* Output Data Element */
extern Press_SenSyncCircP5msRawInfo_t Press_SenSyncCircP5msRawInfo;
extern Press_SenSyncPistP5msRawInfo_t Press_SenSyncPistP5msRawInfo;
extern Press_SenSyncPressSenCalcInfo_t Press_SenSyncPressSenCalcInfo;
extern Press_SenSyncPedlSimPRaw_t Press_SenSyncPedlSimPRaw;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Press_SenSync_Init(void);
extern void Press_SenSync(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_SENSYNC_H_ */
/** @} */
