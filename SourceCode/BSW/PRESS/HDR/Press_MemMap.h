/**
 * @defgroup Press_MemMap Press_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_MEMMAP_H_
#define PRESS_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PRESS_START_SEC_CODE)
  #undef PRESS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_CODE)
  #undef PRESS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_CODE_STARTED
    #error "PRESS_SEC_CODE not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PRESS_START_SEC_CONST_UNSPECIFIED)
  #undef PRESS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_CONST_UNSPECIFIED)
  #undef PRESS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_CONST_UNSPECIFIED_STARTED
    #error "PRESS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PRESS_START_SEC_VAR_UNSPECIFIED)
  #undef PRESS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_VAR_UNSPECIFIED)
  #undef PRESS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_VAR_UNSPECIFIED_STARTED
    #error "PRESS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_START_SEC_VAR_32BIT)
  #undef PRESS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_VAR_32BIT)
  #undef PRESS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_VAR_32BIT_STARTED
    #error "PRESS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PRESS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PRESS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PRESS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_START_SEC_VAR_NOINIT_32BIT)
  #undef PRESS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PRESS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PRESS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PRESS_START_SEC_CALIB_UNSPECIFIED)
  #undef PRESS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEC_STARTED
    #error "PRESS section not closed"
  #endif
  #define CHK_PRESS_SEC_STARTED
  #define CHK_PRESS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PRESS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PRESS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEC_STARTED
  #undef CHK_PRESS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PRESS_SEN_START_SEC_CODE)
  #undef PRESS_SEN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_CODE)
  #undef PRESS_SEN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_CODE_STARTED
    #error "PRESS_SEN_SEC_CODE not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PRESS_SEN_START_SEC_CONST_UNSPECIFIED)
  #undef PRESS_SEN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_CONST_UNSPECIFIED)
  #undef PRESS_SEN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_CONST_UNSPECIFIED_STARTED
    #error "PRESS_SEN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PRESS_SEN_START_SEC_VAR_UNSPECIFIED)
  #undef PRESS_SEN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_VAR_UNSPECIFIED)
  #undef PRESS_SEN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_VAR_UNSPECIFIED_STARTED
    #error "PRESS_SEN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_START_SEC_VAR_32BIT)
  #undef PRESS_SEN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_VAR_32BIT)
  #undef PRESS_SEN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_VAR_32BIT_STARTED
    #error "PRESS_SEN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PRESS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PRESS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PRESS_SEN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_START_SEC_VAR_NOINIT_32BIT)
  #undef PRESS_SEN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PRESS_SEN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PRESS_SEN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PRESS_SEN_START_SEC_CALIB_UNSPECIFIED)
  #undef PRESS_SEN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SEN_SEC_STARTED
    #error "PRESS_SEN section not closed"
  #endif
  #define CHK_PRESS_SEN_SEC_STARTED
  #define CHK_PRESS_SEN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SEN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PRESS_SEN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SEN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PRESS_SEN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SEN_SEC_STARTED
  #undef CHK_PRESS_SEN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PRESS_SENSYNC_START_SEC_CODE)
  #undef PRESS_SENSYNC_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_CODE)
  #undef PRESS_SENSYNC_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_CODE_STARTED
    #error "PRESS_SENSYNC_SEC_CODE not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PRESS_SENSYNC_START_SEC_CONST_UNSPECIFIED)
  #undef PRESS_SENSYNC_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_CONST_UNSPECIFIED)
  #undef PRESS_SENSYNC_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_CONST_UNSPECIFIED_STARTED
    #error "PRESS_SENSYNC_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PRESS_SENSYNC_START_SEC_VAR_UNSPECIFIED)
  #undef PRESS_SENSYNC_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_VAR_UNSPECIFIED)
  #undef PRESS_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_VAR_UNSPECIFIED_STARTED
    #error "PRESS_SENSYNC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_START_SEC_VAR_32BIT)
  #undef PRESS_SENSYNC_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_VAR_32BIT)
  #undef PRESS_SENSYNC_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_VAR_32BIT_STARTED
    #error "PRESS_SENSYNC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PRESS_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PRESS_SENSYNC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_START_SEC_VAR_NOINIT_32BIT)
  #undef PRESS_SENSYNC_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PRESS_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PRESS_SENSYNC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PRESS_SENSYNC_START_SEC_CALIB_UNSPECIFIED)
  #undef PRESS_SENSYNC_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PRESS_SENSYNC_SEC_STARTED
    #error "PRESS_SENSYNC section not closed"
  #endif
  #define CHK_PRESS_SENSYNC_SEC_STARTED
  #define CHK_PRESS_SENSYNC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PRESS_SENSYNC_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PRESS_SENSYNC_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PRESS_SENSYNC_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PRESS_SENSYNC_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PRESS_SENSYNC_SEC_STARTED
  #undef CHK_PRESS_SENSYNC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_MEMMAP_H_ */
/** @} */
