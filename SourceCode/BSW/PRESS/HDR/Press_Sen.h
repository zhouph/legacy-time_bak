/**
 * @defgroup Press_Sen Press_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Press_Sen.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESS_SEN_H_
#define PRESS_SEN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Press_Types.h"
#include "Press_Cfg.h"
#include "Press_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PRESS_SEN_MODULE_ID      (0)
 #define PRESS_SEN_MAJOR_VERSION  (2)
 #define PRESS_SEN_MINOR_VERSION  (0)
 #define PRESS_SEN_PATCH_VERSION  (0)
 #define PRESS_SEN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Press_Sen_HdrBusType Press_SenBus;

/* Version Info */
extern const SwcVersionInfo_t Press_SenVersionInfo;

/* Input Data Element */
extern Ioc_InputSR1msHalPressureInfo_t Press_SenHalPressureInfo;
extern Mom_HndlrEcuModeSts_t Press_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitPressSts_t Press_SenFuncInhibitPressSts;

/* Output Data Element */
extern Press_SenCircP1msRawInfo_t Press_SenCircP1msRawInfo;
extern Press_SenPistP1msRawInfo_t Press_SenPistP1msRawInfo;
extern Press_SenPresCalcEsc_t Press_SenPresCalcEsc;
extern Press_SenCircPBufInfo_t Press_SenCircPBufInfo;
extern Press_SenPistPBufInfo_t Press_SenPistPBufInfo;
extern Press_SenPspBufInfo_t Press_SenPspBufInfo;
extern Press_SenPressCalcInfo_t Press_SenPressCalcInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Press_Sen_Init(void);
extern void Press_Sen(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESS_SEN_H_ */
/** @} */
