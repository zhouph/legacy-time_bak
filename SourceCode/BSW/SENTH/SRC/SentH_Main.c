/**
 * @defgroup SentH_Main SentH_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SentH_Main.h"
#include "SentH_Main_Ifa.h"
#include "IfxStm_reg.h"
#include "SentH_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SentH_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SENTH_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SentH_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SentH_Main_HdrBusType SentH_MainBus;

/* Version Info */
const SwcVersionInfo_t SentH_MainVersionInfo = 
{   
    SENTH_MAIN_MODULE_ID,           /* SentH_MainVersionInfo.ModuleId */
    SENTH_MAIN_MAJOR_VERSION,       /* SentH_MainVersionInfo.MajorVer */
    SENTH_MAIN_MINOR_VERSION,       /* SentH_MainVersionInfo.MinorVer */
    SENTH_MAIN_PATCH_VERSION,       /* SentH_MainVersionInfo.PatchVer */
    SENTH_MAIN_BRANCH_VERSION       /* SentH_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t SentH_MainEcuModeSts;

/* Output Data Element */
SentH_MainSentHPressureInfo_t SentH_MainSentHPressureInfo;

uint32 SentH_Main_Timer_Start;
uint32 SentH_Main_Timer_Elapsed;

#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/** Variable Section (32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/** Variable Section (32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SENTH_MAIN_START_SEC_CODE
#include "SentH_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SentH_Main_Init(void)
{
    /* Initialize internal bus */
    SentH_MainBus.SentH_MainEcuModeSts = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpPresData1 = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpPresData2 = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentCrc = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentData = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentMsgId = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentSerialCrc = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentConfig = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentInvalid = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.MCPSentSerialInvalid = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1PresData1 = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1PresData2 = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentCrc = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentData = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentMsgId = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentSerialCrc = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentConfig = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentInvalid = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2PresData1 = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2PresData2 = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentCrc = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentData = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentMsgId = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentSerialCrc = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentConfig = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentInvalid = 0;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid = 0;
	SentH_Init();
}

void SentH_Main(void)
{
    SentH_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    SentH_Main_Read_SentH_MainEcuModeSts(&SentH_MainBus.SentH_MainEcuModeSts);

    /* Process */
    SentH_Process();

    /* Output */
    SentH_Main_Write_SentH_MainSentHPressureInfo(&SentH_MainBus.SentH_MainSentHPressureInfo);
    /*==============================================================================
    * Members of structure SentH_MainSentHPressureInfo 
     : SentH_MainSentHPressureInfo.McpPresData1;
     : SentH_MainSentHPressureInfo.McpPresData2;
     : SentH_MainSentHPressureInfo.McpSentCrc;
     : SentH_MainSentHPressureInfo.McpSentData;
     : SentH_MainSentHPressureInfo.McpSentMsgId;
     : SentH_MainSentHPressureInfo.McpSentSerialCrc;
     : SentH_MainSentHPressureInfo.McpSentConfig;
     : SentH_MainSentHPressureInfo.McpSentInvalid;
     : SentH_MainSentHPressureInfo.MCPSentSerialInvalid;
     : SentH_MainSentHPressureInfo.Wlp1PresData1;
     : SentH_MainSentHPressureInfo.Wlp1PresData2;
     : SentH_MainSentHPressureInfo.Wlp1SentCrc;
     : SentH_MainSentHPressureInfo.Wlp1SentData;
     : SentH_MainSentHPressureInfo.Wlp1SentMsgId;
     : SentH_MainSentHPressureInfo.Wlp1SentSerialCrc;
     : SentH_MainSentHPressureInfo.Wlp1SentConfig;
     : SentH_MainSentHPressureInfo.Wlp1SentInvalid;
     : SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid;
     : SentH_MainSentHPressureInfo.Wlp2PresData1;
     : SentH_MainSentHPressureInfo.Wlp2PresData2;
     : SentH_MainSentHPressureInfo.Wlp2SentCrc;
     : SentH_MainSentHPressureInfo.Wlp2SentData;
     : SentH_MainSentHPressureInfo.Wlp2SentMsgId;
     : SentH_MainSentHPressureInfo.Wlp2SentSerialCrc;
     : SentH_MainSentHPressureInfo.Wlp2SentConfig;
     : SentH_MainSentHPressureInfo.Wlp2SentInvalid;
     : SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid;
     =============================================================================*/
    

    SentH_Main_Timer_Elapsed = STM0_TIM0.U - SentH_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SENTH_MAIN_STOP_SEC_CODE
#include "SentH_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
