/**
 * @defgroup SentH_Main SentH_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SentH_Main.h"
#include "SentH_Main_Ifa.h"
#include "SentH_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SentH_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SENTH_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SentH_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/** Variable Section (32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

SentH_RxDataType      SentH_RxDataCh0;
SentH_RxDataType      SentH_RxDataCh1;
SentH_RxDataType      SentH_RxDataCh2;

Sent_RxSerialDataType SentH_RxSerialDataCh0;
Sent_RxSerialDataType SentH_RxSerialDataCh1;
Sent_RxSerialDataType SentH_RxSerialDataCh2;

Sent_ChanStatusType   SentH_ChanStatusCh0;
Sent_ChanStatusType   SentH_ChanStatusCh1;
Sent_ChanStatusType   SentH_ChanStatusCh2;

#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/** Variable Section (32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SENTH_MAIN_START_SEC_CODE
#include "SentH_MemMap.h"

static Std_ReturnType SentH_CopyPressureData(void);
static void SentH_InvalidCheck(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void SentH_Init(void)
{
    Sent_SetChannel((Sent_ChannelIdxType)SentH_SentCh0, SENT_ENABLE);
    Sent_SetChannel((Sent_ChannelIdxType)SentH_SentCh1, SENT_ENABLE);
    Sent_SetChannel((Sent_ChannelIdxType)SentH_SentCh2, SENT_ENABLE);
}

void SentH_Process(void)
{
    SentH_RxDataCh0.DATA = Sent_ReadData((Sent_ChannelIdxType)SentH_SentCh0);
    SentH_RxDataCh1.DATA = Sent_ReadData((Sent_ChannelIdxType)SentH_SentCh1);
    SentH_RxDataCh2.DATA = Sent_ReadData((Sent_ChannelIdxType)SentH_SentCh2);
    
    Sent_ReadSerialData((Sent_ChannelIdxType)SentH_SentCh0, &SentH_RxSerialDataCh0);
    Sent_ReadSerialData((Sent_ChannelIdxType)SentH_SentCh1, &SentH_RxSerialDataCh1);
    Sent_ReadSerialData((Sent_ChannelIdxType)SentH_SentCh2, &SentH_RxSerialDataCh2);
    
    Sent_ReadChannelStatus((Sent_ChannelIdxType)SentH_SentCh0, &SentH_ChanStatusCh0);
    Sent_ReadChannelStatus((Sent_ChannelIdxType)SentH_SentCh1, &SentH_ChanStatusCh1);
    Sent_ReadChannelStatus((Sent_ChannelIdxType)SentH_SentCh2, &SentH_ChanStatusCh2);
    
    /*Sent_SetWdgTimer(Sent_ChannelIdxType ChannelId, uint16 WdgTimerReloadVal)*/   /* 
TODO */
    /*Sent_SetAccessEnable(uint32 AccenRegVal)*/
    /*Sent_GetAccessEnable(void)*/
    
    SentH_InvalidCheck();
    
    SentH_CopyPressureData();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/



static Std_ReturnType SentH_CopyPressureData(void)
{
    Std_ReturnType   rtn = E_OK;

    SentH_MainBus.SentH_MainSentHPressureInfo.McpPresData1        = 0xFFF&SentH_RxDataCh0.B.mu12_RxData1;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpPresData2        = 0xFFF&SentH_RxDataCh0.B.mu12_RxData2;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentConfig       = SentH_RxSerialDataCh0.Configuration;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentData         = SentH_RxSerialDataCh0.Data;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentMsgId        = SentH_RxSerialDataCh0.MsgId;
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentSerialCrc    = SentH_RxSerialDataCh0.Crc;    /* NOT USED */
    SentH_MainBus.SentH_MainSentHPressureInfo.McpSentCrc          = SentH_ChanStatusCh0.RxCrc;    /* NOT USED */

    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1PresData1       = 0xFFF&SentH_RxDataCh1.B.mu12_RxData1;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1PresData2       = 0xFFF&SentH_RxDataCh1.B.mu12_RxData2;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentConfig      = SentH_RxSerialDataCh1.Configuration;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentData        = SentH_RxSerialDataCh1.Data;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentMsgId       = SentH_RxSerialDataCh1.MsgId;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentSerialCrc   = SentH_RxSerialDataCh1.Crc;    /* NOT USED */
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentCrc         = SentH_ChanStatusCh1.RxCrc;    /* NOT USED */
    
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2PresData1       = 0xFFF&SentH_RxDataCh2.B.mu12_RxData1;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2PresData2       = 0xFFF&SentH_RxDataCh2.B.mu12_RxData2;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentConfig      = SentH_RxSerialDataCh2.Configuration;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentData        = SentH_RxSerialDataCh2.Data;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentMsgId       = SentH_RxSerialDataCh2.MsgId;
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentSerialCrc   = SentH_RxSerialDataCh2.Crc;    /* NOT USED */
    SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentCrc         = SentH_ChanStatusCh2.RxCrc;    /* NOT USED */

    return rtn;
}

static void SentH_InvalidCheck(void)
{
    static uint32 TimeStampBef0, TimeStampBef1, TimeStampBef2;
    
    if(SentH_ChanStatusCh0.RxTimeStamp == TimeStampBef0)
    {
        SentH_MainBus.SentH_MainSentHPressureInfo.McpSentInvalid        |= SENTH_INV_TIMEOUT;
        SentH_MainBus.SentH_MainSentHPressureInfo.McpSentInvalid        |= SENTH_INV_IS_ERROR;
        SentH_MainBus.SentH_MainSentHPressureInfo.MCPSentSerialInvalid  |= SENTH_INV_TIMEOUT;
        SentH_MainBus.SentH_MainSentHPressureInfo.MCPSentSerialInvalid  |= SENTH_INV_IS_ERROR;
    }
    else
    {
        SentH_MainBus.SentH_MainSentHPressureInfo.McpSentInvalid        = SENTH_INV_NO_FAULT;
        SentH_MainBus.SentH_MainSentHPressureInfo.MCPSentSerialInvalid  = SENTH_INV_NO_FAULT;
    }
    
    if(SentH_ChanStatusCh1.RxTimeStamp == TimeStampBef1)
    {
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentInvalid        |= SENTH_INV_TIMEOUT;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentInvalid        |= SENTH_INV_IS_ERROR;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid  |= SENTH_INV_TIMEOUT;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid  |= SENTH_INV_IS_ERROR;
    }
    else
    {
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentInvalid        = SENTH_INV_NO_FAULT;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid  = SENTH_INV_NO_FAULT;
    }    
    if(SentH_ChanStatusCh2.RxTimeStamp == TimeStampBef2)
    {
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentInvalid        |= SENTH_INV_TIMEOUT;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentInvalid        |= SENTH_INV_IS_ERROR;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid  |= SENTH_INV_TIMEOUT;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid  |= SENTH_INV_IS_ERROR;
    }
    else
    {
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentInvalid        = SENTH_INV_NO_FAULT;
        SentH_MainBus.SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid  = SENTH_INV_NO_FAULT;    
    }
    
    /* TODO CRC CHECK */
    
    /* Save before value */
    TimeStampBef0 = SentH_ChanStatusCh0.RxTimeStamp;
    TimeStampBef1 = SentH_ChanStatusCh1.RxTimeStamp;
    TimeStampBef2 = SentH_ChanStatusCh2.RxTimeStamp;
}

void SentChannel0_CallOut (Sent_ChannelIdxType ChannelId, Sent_NotifType Stat)
{
    ;
}
void SentChannel1_CallOut (Sent_ChannelIdxType ChannelId, Sent_NotifType Stat)
{
    ;
}
void SentChannel2_CallOut (Sent_ChannelIdxType ChannelId, Sent_NotifType Stat)
{
    ;
}

#define SENTH_MAIN_STOP_SEC_CODE
#include "SentH_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
