/**
 * @defgroup SentH_Main SentH_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENTH_MAIN_H_
#define SENTH_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SentH_Types.h"
#include "SentH_Cfg.h"
#include "SentH_Cal.h"
#include "Sent.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SENTH_MAIN_MODULE_ID      (0)
 #define SENTH_MAIN_MAJOR_VERSION  (2)
 #define SENTH_MAIN_MINOR_VERSION  (0)
 #define SENTH_MAIN_PATCH_VERSION  (0)
 #define SENTH_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SentH_Main_HdrBusType SentH_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SentH_MainVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t SentH_MainEcuModeSts;

/* Output Data Element */
extern SentH_MainSentHPressureInfo_t SentH_MainSentHPressureInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SentH_Main_Init(void);
extern void SentH_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENTH_MAIN_H_ */
/** @} */
