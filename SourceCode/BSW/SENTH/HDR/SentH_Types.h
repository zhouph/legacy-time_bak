/**
 * @defgroup SentH_Types SentH_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENTH_TYPES_H_
#define SENTH_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

#define SENTH_INV_NO_FAULT                    (0U)
#define SENTH_INV_IS_ERROR                    (1UL << 0)
#define SENTH_INV_TIMEOUT                     (1UL << 1)
#define SENTH_INV_CHECKSUM                    (1UL << 2)
#define SENTH_INV_INVALID_MSG                 (1UL << 3)
#define SENTH_INV_INCORRECT_FREQ              (1UL << 4)
#define SENTH_INV_ERRATIC                     (1UL << 5)
#define SENTH_INV_ROM_FAIL                    (1UL << 6)
#define SENTH_INV_RAM_FAIL                    (1UL << 7)
#define SENTH_INV_EEPROM_FAIL                 (1UL << 8)
#define SENTH_INV_INTERNAL_ELEC               (1UL << 9)
#define SENTH_INV_INCORRECT_COMPNT            (1UL << 10)
#define SENTH_INV_ALIVE_CNT                   (1UL << 11)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t SentH_MainEcuModeSts;

/* Output Data Element */
    SentH_MainSentHPressureInfo_t SentH_MainSentHPressureInfo;
}SentH_Main_HdrBusType;

enum
{
    SentH_SentCh0 = 0u,
    SentH_SentCh1 = 1u,
    SentH_SentCh2 = 2u
};


typedef union
{
    uint32 DATA;
    struct
    {
        uint32 mu12_RxData1   :16;        /* TODO the bit order should be checked */
        uint32 mu12_RxData2   :16;
    }B;
}SentH_RxDataType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENTH_TYPES_H_ */
/** @} */
