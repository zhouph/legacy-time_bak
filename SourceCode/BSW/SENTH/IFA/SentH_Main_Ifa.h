/**
 * @defgroup SentH_Main_Ifa SentH_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENTH_MAIN_IFA_H_
#define SENTH_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SentH_Main_Read_SentH_MainEcuModeSts(data) do \
{ \
    *data = SentH_MainEcuModeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define SentH_Main_Write_SentH_MainSentHPressureInfo(data) do \
{ \
    SentH_MainSentHPressureInfo = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpPresData1(data) do \
{ \
    SentH_MainSentHPressureInfo.McpPresData1 = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpPresData2(data) do \
{ \
    SentH_MainSentHPressureInfo.McpPresData2 = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpSentCrc(data) do \
{ \
    SentH_MainSentHPressureInfo.McpSentCrc = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpSentData(data) do \
{ \
    SentH_MainSentHPressureInfo.McpSentData = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpSentMsgId(data) do \
{ \
    SentH_MainSentHPressureInfo.McpSentMsgId = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpSentSerialCrc(data) do \
{ \
    SentH_MainSentHPressureInfo.McpSentSerialCrc = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpSentConfig(data) do \
{ \
    SentH_MainSentHPressureInfo.McpSentConfig = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_McpSentInvalid(data) do \
{ \
    SentH_MainSentHPressureInfo.McpSentInvalid = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_MCPSentSerialInvalid(data) do \
{ \
    SentH_MainSentHPressureInfo.MCPSentSerialInvalid = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1PresData1(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1PresData1 = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1PresData2(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1PresData2 = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentCrc(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentCrc = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentData(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentData = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentMsgId(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentMsgId = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentSerialCrc(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentSerialCrc = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentConfig(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentConfig = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentInvalid(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentInvalid = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp1SentSerialInvalid(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2PresData1(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2PresData1 = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2PresData2(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2PresData2 = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentCrc(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentCrc = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentData(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentData = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentMsgId(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentMsgId = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentSerialCrc(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentSerialCrc = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentConfig(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentConfig = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentInvalid(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentInvalid = *data; \
}while(0);

#define SentH_Main_Write_SentH_MainSentHPressureInfo_Wlp2SentSerialInvalid(data) do \
{ \
    SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENTH_MAIN_IFA_H_ */
/** @} */
