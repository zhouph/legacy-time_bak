/**
 * @defgroup SentH_Main_Ifa SentH_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SentH_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SentH_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SENTH_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SentH_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/** Variable Section (32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENTH_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENTH_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SentH_MemMap.h"
#define SENTH_MAIN_START_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/** Variable Section (32BIT)**/


#define SENTH_MAIN_STOP_SEC_VAR_32BIT
#include "SentH_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SENTH_MAIN_START_SEC_CODE
#include "SentH_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SENTH_MAIN_STOP_SEC_CODE
#include "SentH_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
