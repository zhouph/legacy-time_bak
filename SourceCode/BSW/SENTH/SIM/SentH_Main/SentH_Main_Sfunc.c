#define S_FUNCTION_NAME      SentH_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          1
#define WidthOutputPort         27

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "SentH_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    SentH_MainEcuModeSts = input[0];

    SentH_Main();


    output[0] = SentH_MainSentHPressureInfo.McpPresData1;
    output[1] = SentH_MainSentHPressureInfo.McpPresData2;
    output[2] = SentH_MainSentHPressureInfo.McpSentCrc;
    output[3] = SentH_MainSentHPressureInfo.McpSentData;
    output[4] = SentH_MainSentHPressureInfo.McpSentMsgId;
    output[5] = SentH_MainSentHPressureInfo.McpSentSerialCrc;
    output[6] = SentH_MainSentHPressureInfo.McpSentConfig;
    output[7] = SentH_MainSentHPressureInfo.McpSentInvalid;
    output[8] = SentH_MainSentHPressureInfo.MCPSentSerialInvalid;
    output[9] = SentH_MainSentHPressureInfo.Wlp1PresData1;
    output[10] = SentH_MainSentHPressureInfo.Wlp1PresData2;
    output[11] = SentH_MainSentHPressureInfo.Wlp1SentCrc;
    output[12] = SentH_MainSentHPressureInfo.Wlp1SentData;
    output[13] = SentH_MainSentHPressureInfo.Wlp1SentMsgId;
    output[14] = SentH_MainSentHPressureInfo.Wlp1SentSerialCrc;
    output[15] = SentH_MainSentHPressureInfo.Wlp1SentConfig;
    output[16] = SentH_MainSentHPressureInfo.Wlp1SentInvalid;
    output[17] = SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid;
    output[18] = SentH_MainSentHPressureInfo.Wlp2PresData1;
    output[19] = SentH_MainSentHPressureInfo.Wlp2PresData2;
    output[20] = SentH_MainSentHPressureInfo.Wlp2SentCrc;
    output[21] = SentH_MainSentHPressureInfo.Wlp2SentData;
    output[22] = SentH_MainSentHPressureInfo.Wlp2SentMsgId;
    output[23] = SentH_MainSentHPressureInfo.Wlp2SentSerialCrc;
    output[24] = SentH_MainSentHPressureInfo.Wlp2SentConfig;
    output[25] = SentH_MainSentHPressureInfo.Wlp2SentInvalid;
    output[26] = SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    SentH_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
