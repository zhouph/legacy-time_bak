# \file
#
# \brief SentH
#
# This file contains the implementation of the SWC
# module SentH.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SentH_CORE_PATH     := $(MANDO_BSW_ROOT)\SentH
SentH_CAL_PATH      := $(SentH_CORE_PATH)\CAL\$(SentH_VARIANT)
SentH_SRC_PATH      := $(SentH_CORE_PATH)\SRC
SentH_CFG_PATH      := $(SentH_CORE_PATH)\CFG\$(SentH_VARIANT)
SentH_HDR_PATH      := $(SentH_CORE_PATH)\HDR
SentH_IFA_PATH      := $(SentH_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SentH_CMN_PATH      := $(SentH_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SentH_UT_PATH		:= $(SentH_CORE_PATH)\UT
	SentH_UNITY_PATH	:= $(SentH_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SentH_UT_PATH)
	CC_INCLUDE_PATH		+= $(SentH_UNITY_PATH)
	SentH_Main_PATH 	:= SentH_UT_PATH\SentH_Main
endif
CC_INCLUDE_PATH    += $(SentH_CAL_PATH)
CC_INCLUDE_PATH    += $(SentH_SRC_PATH)
CC_INCLUDE_PATH    += $(SentH_CFG_PATH)
CC_INCLUDE_PATH    += $(SentH_HDR_PATH)
CC_INCLUDE_PATH    += $(SentH_IFA_PATH)
CC_INCLUDE_PATH    += $(SentH_CMN_PATH)

