# \file
#
# \brief SentH
#
# This file contains the implementation of the SWC
# module SentH.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SentH_src

SentH_src_FILES        += $(SentH_SRC_PATH)\SentH_Main.c
SentH_src_FILES        += $(SentH_SRC_PATH)\SentH_Process.c
SentH_src_FILES        += $(SentH_IFA_PATH)\SentH_Main_Ifa.c
SentH_src_FILES        += $(SentH_CFG_PATH)\SentH_Cfg.c
SentH_src_FILES        += $(SentH_CAL_PATH)\SentH_Cal.c

ifeq ($(ICE_COMPILE),true)
SentH_src_FILES        += $(SentH_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SentH_src_FILES        += $(SentH_UNITY_PATH)\unity.c
	SentH_src_FILES        += $(SentH_UNITY_PATH)\unity_fixture.c	
	SentH_src_FILES        += $(SentH_UT_PATH)\main.c
	SentH_src_FILES        += $(SentH_UT_PATH)\SentH_Main\SentH_Main_UtMain.c
endif