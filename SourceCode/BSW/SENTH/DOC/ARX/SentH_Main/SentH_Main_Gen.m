SentH_MainEcuModeSts = Simulink.Bus;
DeList={'SentH_MainEcuModeSts'};
SentH_MainEcuModeSts = CreateBus(SentH_MainEcuModeSts, DeList);
clear DeList;

SentH_MainSentHPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
SentH_MainSentHPressureInfo = CreateBus(SentH_MainSentHPressureInfo, DeList);
clear DeList;

