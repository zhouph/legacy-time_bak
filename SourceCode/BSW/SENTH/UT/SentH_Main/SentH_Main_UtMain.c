#include "unity.h"
#include "unity_fixture.h"
#include "SentH_Main.h"
#include "SentH_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_SentH_MainEcuModeSts[MAX_STEP] = SENTH_MAINECUMODESTS;

const Haluint16 UtExpected_SentH_MainSentHPressureInfo_McpPresData1[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPPRESDATA1;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_McpPresData2[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPPRESDATA2;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_McpSentCrc[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTCRC;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_McpSentData[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTDATA;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_McpSentMsgId[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTMSGID;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_McpSentSerialCrc[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTSERIALCRC;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_McpSentConfig[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTCONFIG;
const Haluint32 UtExpected_SentH_MainSentHPressureInfo_McpSentInvalid[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTINVALID;
const Haluint32 UtExpected_SentH_MainSentHPressureInfo_MCPSentSerialInvalid[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_MCPSENTSERIALINVALID;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_Wlp1PresData1[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1PRESDATA1;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_Wlp1PresData2[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1PRESDATA2;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentCrc[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTCRC;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentData[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTDATA;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentMsgId[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTMSGID;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentSerialCrc[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTSERIALCRC;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentConfig[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTCONFIG;
const Haluint32 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentInvalid[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTINVALID;
const Haluint32 UtExpected_SentH_MainSentHPressureInfo_Wlp1SentSerialInvalid[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP1SENTSERIALINVALID;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_Wlp2PresData1[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2PRESDATA1;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_Wlp2PresData2[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2PRESDATA2;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentCrc[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTCRC;
const Haluint16 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentData[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTDATA;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentMsgId[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTMSGID;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentSerialCrc[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTSERIALCRC;
const Haluint8 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentConfig[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTCONFIG;
const Haluint32 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentInvalid[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTINVALID;
const Haluint32 UtExpected_SentH_MainSentHPressureInfo_Wlp2SentSerialInvalid[MAX_STEP] = SENTH_MAINSENTHPRESSUREINFO_WLP2SENTSERIALINVALID;



TEST_GROUP(SentH_Main);
TEST_SETUP(SentH_Main)
{
    SentH_Main_Init();
}

TEST_TEAR_DOWN(SentH_Main)
{   /* Postcondition */

}

TEST(SentH_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SentH_MainEcuModeSts = UtInput_SentH_MainEcuModeSts[i];

        SentH_Main();

        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpPresData1, UtExpected_SentH_MainSentHPressureInfo_McpPresData1[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpPresData2, UtExpected_SentH_MainSentHPressureInfo_McpPresData2[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpSentCrc, UtExpected_SentH_MainSentHPressureInfo_McpSentCrc[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpSentData, UtExpected_SentH_MainSentHPressureInfo_McpSentData[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpSentMsgId, UtExpected_SentH_MainSentHPressureInfo_McpSentMsgId[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpSentSerialCrc, UtExpected_SentH_MainSentHPressureInfo_McpSentSerialCrc[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpSentConfig, UtExpected_SentH_MainSentHPressureInfo_McpSentConfig[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.McpSentInvalid, UtExpected_SentH_MainSentHPressureInfo_McpSentInvalid[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.MCPSentSerialInvalid, UtExpected_SentH_MainSentHPressureInfo_MCPSentSerialInvalid[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1PresData1, UtExpected_SentH_MainSentHPressureInfo_Wlp1PresData1[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1PresData2, UtExpected_SentH_MainSentHPressureInfo_Wlp1PresData2[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentCrc, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentCrc[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentData, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentData[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentMsgId, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentMsgId[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentSerialCrc, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentSerialCrc[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentConfig, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentConfig[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentInvalid, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentInvalid[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid, UtExpected_SentH_MainSentHPressureInfo_Wlp1SentSerialInvalid[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2PresData1, UtExpected_SentH_MainSentHPressureInfo_Wlp2PresData1[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2PresData2, UtExpected_SentH_MainSentHPressureInfo_Wlp2PresData2[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentCrc, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentCrc[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentData, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentData[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentMsgId, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentMsgId[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentSerialCrc, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentSerialCrc[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentConfig, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentConfig[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentInvalid, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentInvalid[i]);
        TEST_ASSERT_EQUAL(SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid, UtExpected_SentH_MainSentHPressureInfo_Wlp2SentSerialInvalid[i]);
    }
}

TEST_GROUP_RUNNER(SentH_Main)
{
    RUN_TEST_CASE(SentH_Main, All);
}
