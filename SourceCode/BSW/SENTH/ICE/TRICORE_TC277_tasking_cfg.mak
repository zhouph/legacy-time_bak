# \file
#
# \brief AUTOSAR ApplTemplates
#
# This file contains the implementation of the AUTOSAR
# module ApplTemplates.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# Toolchain settings
# TOOLPATH_COMPILER:
# Path to the tasking compiler and linker.
# It is checked, if an environment variable
# $(SSC_TRICORE_277_TOOLPATH_COMPILER) exists.
# If so, this variable is taken for TOOLPATH_COMPILER
# If it not exists, TOOLPATH_COMPILER must be set by user
# (modify command in line following the ifeq).
#
# For example:
TOOLPATH_COMPILER ?= C:\Compiler\TASKING\TriCore_v4.2r2\ctc

# TASKING_MODE:
#
# To compile a C source file the C compiler and the assembler must
# be called. This can be done by one call of the control program cctc
# or by a call of the compiler ctc and the assembler astc. The variable
# TASKING_MODE allows to select the kind of the tool chain call.
# Valid values are CONTROL_PROGRAM and COMPILER_ASSEMBLER.
TASKING_MODE = CONTROL_PROGRAM


# Define the options for the compiler

# Set TriCore EABI compliant mode (don't use half-word alignment)
CC_OPT += --eabi-compliant

# 0 byte threshold for putting data in __near field.
# This prevents the compiler from moving data out of sections,
# where the OS expects it to be located for its protection features.
CC_OPT += --default-near-size=0

# Generate symbolic debug information
CC_OPT += -g

# Disable warnings, which are considered harmless from Q-Records:
#  - 515: side effects of 'sizeof' operand will be ignored
#  - 529: overflow in constant expression
#  - 544: unreachable code
#  - 581: usage of pointers to different but compatible types
#  - 588: dead assignment
CC_OPT += -Wc-w515 -Wc-w529 -Wc-w544 -Wc-w581 -Wc-w588

# Due to false positive compiler warnings with -OP (disabled
# "Constant propagation") and -Ol (enabled "Loop transformation")
# suppress 549: condition is always true
# Tasking ticket link: http://issues.tasking.com/?issueid=160-37363
CC_OPT += -Wc-w549

# Select target cpu
CC_OPT += --cpu=tc27x

# Add definition of the used target cpu for the OS
CC_OPT += -DOS_CPU=OS_TC277

# Add definition of the used Tricore architecture for the OS
CC_OPT += -DOS_TRICOREARCH=OS_TRICOREARCH_16EP

# Keep intermediate files.
CC_OPT += -t

# Merge C source code with generated assembly code in the output
CC_OPT += --source

# -OO: Switch off "Forward store" optimization
# Tasking: http://issues.tasking.com/?issueid=160-39613
# -OP: Switch off "Constant propagation" optimization
# Tasking: http://issues.tasking.com/?issueid=160-38438
CC_OPT += -O2ROP
# --integer-enumeration: always treat enum-types as int
# Tasking: http://issues.tasking.com/?issueid=160-39612
CC_OPT += --integer-enumeration

# Compile in ISO-C:90 mode
CC_OPT += --iso=90

# sets to disable the option --no-double, which is set as default by using
# option --cpu with option --no_double, double is converted as float
CC_OPT += --use-double-precision-fp

# Optimize for code size (0=speed, ..., 4=code size)
CC_OPT += --tradeoff=4

# Specify source language option:
# - no ISO C99 comments (double-slash)
# - no gcc extensions
# - optimize across volatile accesses
# - unrelaxed const check for string literals
CC_OPT += --language=-gcc,+volatile,-strings

# Select an Algorithm for switch statements.
CC_OPT += --switch=auto

# Select alignment of data (Default).
CC_OPT += --align=0

# Set threshold values for A0, A1 memory
CC_OPT += --default-a0-size=0
CC_OPT += --default-a1-size=0

# Enable strict ANSI/ISO mode.
CC_OPT += --strict

# Use tasking copytable for data initialization
CC_OPT += -DUSE_TASKING_INIT=1

# use extended Comiler_CfgExt.h
CC_OPT += -DCOMPILERCFG_EXTENSION_MCAL_FILE

# Define the default options for the assembler

# Use the TASKING preprocessor
ASM_OPT += -mt

# Generate symbolic debug information
ASM_OPT += -g

# Select target cpu
ASM_OPT += -D__CPU_TC277__ -D__CPU__=tc27x

# Add debug information
ASM_OPT += -gAHLs

# Emit locals EQU and non-EQU symbols
ASM_OPT += --emit-locals=-equ,-symbols

# Set generic assembly optimizations
ASM_OPT += -Ogs

# Set maximum number of emitted errors
ASM_OPT += --error-limit=42


# Define the options for the linker

# Tell linker about LINK_PLACE options
LINK_OPT += $(LINK_PLACE)

# Link against the runtime library
LINK_OPT += -lrt

# Link against the C library
LINK_OPT += -lc

# Specify include directory for linker scripts
LINK_OPT += -I$(BOARD_PROJECT_PATH)

# Create a map file
LINK_OPT += -M$(MAP_FILE)

# Linker optimizations
# - C do not delete unreferenced sections
# - L do not use a ffd algorithm to locate unrestricted sections
# - t emit smart restrictions to decrease copy table size
# - x delete duplicate code sections
# - Y do not delete duplicate constant data sections (needed for Rte)
LINK_OPT += -OCLtxY

# link library for fpu
LINK_OPT +=--library=fp_fpu

# Enable the usage of our own initialisation code - Mandatory with Safety Os
# Equivalent to --user-provided-initialization-code
#LINK_OPT += -i

# Linker archive options: insert-replace/create/update
AR_OPT += -rcu

# EXT_LOCATOR_FILE:
# specify the name for an external locator file
# if no name is given, a default locator file $(BOARD).ldscript is taken
# which is composed in file <board>.mak
EXT_LOCATOR_FILE +=

# General path setup

# Path where the map file should get generated
MAP_FILE = $(BIN_OUTPUT_PATH)\$(PROJECT).map

# Path where the output file should get generated
OUT_FILE = $(BUILD_DIR)\$(PROJECT).out

# Define the options for preprocessing *.s files before being fed
# into the assembler.

# Add definition of the used Tricore architecture for the OS
ASS_OPT += -DOS_TRICOREARCH=OS_TRICOREARCH_16EP

