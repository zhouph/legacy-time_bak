/**
 * @defgroup SentH_Cal SentH_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SentH_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENTH_CAL_H_
#define SENTH_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SentH_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENTH_CAL_H_ */
/** @} */
