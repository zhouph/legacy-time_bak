/**
 * @defgroup Prly_Cfg Prly_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Prly_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRLY_START_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRLY_STOP_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRLY_START_SEC_CODE
#include "Prly_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/* Power manager module configuration table */
 const Prly_ConfigType Prly_Config = 
 {
    (Prly_IgnVoltType) 10000, /* IgnOnVolt(mV) */
    (Prly_IgnVoltType) 7000, /* IgnOffVolt(mV) */
    (Prly_IgnTimeType) 100 , /* IgnOnfilterTime(ms) */
    (Prly_IgnTimeType) 100 , /* IgnOffFilterTime(ms) */
 };
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRLY_STOP_SEC_CODE
#include "Prly_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
