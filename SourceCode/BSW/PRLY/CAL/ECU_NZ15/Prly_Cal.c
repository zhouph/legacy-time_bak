/**
 * @defgroup Prly_Cal Prly_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Prly_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRLY_START_SEC_CALIB_UNSPECIFIED
#include "Prly_MemMap.h"
/* Global Calibration Section */


#define PRLY_STOP_SEC_CALIB_UNSPECIFIED
#include "Prly_MemMap.h"

#define PRLY_START_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRLY_STOP_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRLY_START_SEC_CODE
#include "Prly_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRLY_STOP_SEC_CODE
#include "Prly_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
