# \file
#
# \brief Prly
#
# This file contains the implementation of the SWC
# module Prly.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Prly_src

Prly_src_FILES        += $(Prly_SRC_PATH)\Prly_Hndlr.c
Prly_src_FILES        += $(Prly_IFA_PATH)\Prly_Hndlr_Ifa.c
Prly_src_FILES        += $(Prly_CFG_PATH)\Prly_Cfg.c
Prly_src_FILES        += $(Prly_CAL_PATH)\Prly_Cal.c

ifeq ($(ICE_COMPILE),true)
Prly_src_FILES        += $(Prly_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Prly_src_FILES        += $(Prly_UNITY_PATH)\unity.c
	Prly_src_FILES        += $(Prly_UNITY_PATH)\unity_fixture.c	
	Prly_src_FILES        += $(Prly_UT_PATH)\main.c
	Prly_src_FILES        += $(Prly_UT_PATH)\Prly_Hndlr\Prly_Hndlr_UtMain.c
endif