# \file
#
# \brief Prly
#
# This file contains the implementation of the SWC
# module Prly.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Prly_CORE_PATH     := $(MANDO_BSW_ROOT)\Prly
Prly_CAL_PATH      := $(Prly_CORE_PATH)\CAL\$(Prly_VARIANT)
Prly_SRC_PATH      := $(Prly_CORE_PATH)\SRC
Prly_CFG_PATH      := $(Prly_CORE_PATH)\CFG\$(Prly_VARIANT)
Prly_HDR_PATH      := $(Prly_CORE_PATH)\HDR
Prly_IFA_PATH      := $(Prly_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Prly_CMN_PATH      := $(Prly_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Prly_UT_PATH		:= $(Prly_CORE_PATH)\UT
	Prly_UNITY_PATH	:= $(Prly_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Prly_UT_PATH)
	CC_INCLUDE_PATH		+= $(Prly_UNITY_PATH)
	Prly_Hndlr_PATH 	:= Prly_UT_PATH\Prly_Hndlr
endif
CC_INCLUDE_PATH    += $(Prly_CAL_PATH)
CC_INCLUDE_PATH    += $(Prly_SRC_PATH)
CC_INCLUDE_PATH    += $(Prly_CFG_PATH)
CC_INCLUDE_PATH    += $(Prly_HDR_PATH)
CC_INCLUDE_PATH    += $(Prly_IFA_PATH)
CC_INCLUDE_PATH    += $(Prly_CMN_PATH)

