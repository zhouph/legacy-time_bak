#include "unity.h"
#include "unity_fixture.h"
#include "Prly_Hndlr.h"
#include "Prly_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Prly_HndlrCanRxClu2Info_IgnRun[MAX_STEP] = PRLY_HNDLRCANRXCLU2INFO_IGNRUN;
const Saluint8 UtInput_Prly_HndlrSenPwrMonitorData_SenPwrM_5V_DriveReq[MAX_STEP] = PRLY_HNDLRSENPWRMONITORDATA_SENPWRM_5V_DRIVEREQ;
const Saluint8 UtInput_Prly_HndlrSenPwrMonitorData_SenPwrM_12V_Drive_Req[MAX_STEP] = PRLY_HNDLRSENPWRMONITORDATA_SENPWRM_12V_DRIVE_REQ;
const Mom_HndlrEcuModeSts_t UtInput_Prly_HndlrEcuModeSts[MAX_STEP] = PRLY_HNDLRECUMODESTS;
const Ioc_InputSR1msCEMon_t UtInput_Prly_HndlrCEMon[MAX_STEP] = PRLY_HNDLRCEMON;
const Mom_HndlrMomEcuInhibit_t UtInput_Prly_HndlrMomEcuInhibit[MAX_STEP] = PRLY_HNDLRMOMECUINHIBIT;
const Eem_SuspcDetnFuncInhibitPrlySts_t UtInput_Prly_HndlrFuncInhibitPrlySts[MAX_STEP] = PRLY_HNDLRFUNCINHIBITPRLYSTS;

const Prly_HndlrIgnOnOffSts_t UtExpected_Prly_HndlrIgnOnOffSts[MAX_STEP] = PRLY_HNDLRIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtExpected_Prly_HndlrIgnEdgeSts[MAX_STEP] = PRLY_HNDLRIGNEDGESTS;
const Prly_HndlrPrlyEcuInhibit_t UtExpected_Prly_HndlrPrlyEcuInhibit[MAX_STEP] = PRLY_HNDLRPRLYECUINHIBIT;



TEST_GROUP(Prly_Hndlr);
TEST_SETUP(Prly_Hndlr)
{
    Prly_Hndlr_Init();
}

TEST_TEAR_DOWN(Prly_Hndlr)
{   /* Postcondition */

}

TEST(Prly_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Prly_HndlrCanRxClu2Info.IgnRun = UtInput_Prly_HndlrCanRxClu2Info_IgnRun[i];
        Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq = UtInput_Prly_HndlrSenPwrMonitorData_SenPwrM_5V_DriveReq[i];
        Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req = UtInput_Prly_HndlrSenPwrMonitorData_SenPwrM_12V_Drive_Req[i];
        Prly_HndlrEcuModeSts = UtInput_Prly_HndlrEcuModeSts[i];
        Prly_HndlrCEMon = UtInput_Prly_HndlrCEMon[i];
        Prly_HndlrMomEcuInhibit = UtInput_Prly_HndlrMomEcuInhibit[i];
        Prly_HndlrFuncInhibitPrlySts = UtInput_Prly_HndlrFuncInhibitPrlySts[i];

        Prly_Hndlr();

        TEST_ASSERT_EQUAL(Prly_HndlrIgnOnOffSts, UtExpected_Prly_HndlrIgnOnOffSts[i]);
        TEST_ASSERT_EQUAL(Prly_HndlrIgnEdgeSts, UtExpected_Prly_HndlrIgnEdgeSts[i]);
        TEST_ASSERT_EQUAL(Prly_HndlrPrlyEcuInhibit, UtExpected_Prly_HndlrPrlyEcuInhibit[i]);
    }
}

TEST_GROUP_RUNNER(Prly_Hndlr)
{
    RUN_TEST_CASE(Prly_Hndlr, All);
}
