/**
 * @defgroup Prly_Hndlr Prly_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Prly_Hndlr.h"
#include "Prly_Hndlr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 #define PRLY_PERIODICTIME_MS   5 /*ms*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRLY_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Prly_Hndlr_HdrBusType Prly_HndlrBus;

/* Version Info */
const SwcVersionInfo_t Prly_HndlrVersionInfo = 
{   
    PRLY_HNDLR_MODULE_ID,           /* Prly_HndlrVersionInfo.ModuleId */
    PRLY_HNDLR_MAJOR_VERSION,       /* Prly_HndlrVersionInfo.MajorVer */
    PRLY_HNDLR_MINOR_VERSION,       /* Prly_HndlrVersionInfo.MinorVer */
    PRLY_HNDLR_PATCH_VERSION,       /* Prly_HndlrVersionInfo.PatchVer */
    PRLY_HNDLR_BRANCH_VERSION       /* Prly_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxClu2Info_t Prly_HndlrCanRxClu2Info;
SenPwrM_MainSenPwrMonitor_t Prly_HndlrSenPwrMonitorData;
Mom_HndlrEcuModeSts_t Prly_HndlrEcuModeSts;
Ioc_InputSR1msCEMon_t Prly_HndlrCEMon;
Mom_HndlrMomEcuInhibit_t Prly_HndlrMomEcuInhibit;
Eem_SuspcDetnFuncInhibitPrlySts_t Prly_HndlrFuncInhibitPrlySts;

/* Output Data Element */
Prly_HndlrIgnOnOffSts_t Prly_HndlrIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Prly_HndlrIgnEdgeSts;
Prly_HndlrPrlyEcuInhibit_t Prly_HndlrPrlyEcuInhibit;

uint32 Prly_Hndlr_Timer_Start;
uint32 Prly_Hndlr_Timer_Elapsed;

#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
uint8 InitialIgnitionCheck;

#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_CODE
#include "Prly_MemMap.h"
 static void Prly_ManageIgn(void);
 static void Prly_ManageReg(void);
 static void Prly_EcuInhibit(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Prly_Hndlr_Init(void)
{
    /* Initialize internal bus */
    Prly_HndlrBus.Prly_HndlrCanRxClu2Info.IgnRun = 0;
    Prly_HndlrBus.Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq = 0;
    Prly_HndlrBus.Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req = 0;
    Prly_HndlrBus.Prly_HndlrEcuModeSts = 0;
    Prly_HndlrBus.Prly_HndlrCEMon = 0;
    Prly_HndlrBus.Prly_HndlrMomEcuInhibit = 0;
    Prly_HndlrBus.Prly_HndlrFuncInhibitPrlySts = 0;
    Prly_HndlrBus.Prly_HndlrIgnOnOffSts = 0;
    Prly_HndlrBus.Prly_HndlrIgnEdgeSts = 0;
    Prly_HndlrBus.Prly_HndlrPrlyEcuInhibit = 0;
}

void Prly_Hndlr(void)
{
    Prly_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Prly_Hndlr_Read_Prly_HndlrCanRxClu2Info(&Prly_HndlrBus.Prly_HndlrCanRxClu2Info);
    /*==============================================================================
    * Members of structure Prly_HndlrCanRxClu2Info 
     : Prly_HndlrCanRxClu2Info.IgnRun;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Prly_Hndlr_Read_Prly_HndlrSenPwrMonitorData_SenPwrM_5V_DriveReq(&Prly_HndlrBus.Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq);
    Prly_Hndlr_Read_Prly_HndlrSenPwrMonitorData_SenPwrM_12V_Drive_Req(&Prly_HndlrBus.Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req);

    Prly_Hndlr_Read_Prly_HndlrEcuModeSts(&Prly_HndlrBus.Prly_HndlrEcuModeSts);
    Prly_Hndlr_Read_Prly_HndlrCEMon(&Prly_HndlrBus.Prly_HndlrCEMon);
    Prly_Hndlr_Read_Prly_HndlrMomEcuInhibit(&Prly_HndlrBus.Prly_HndlrMomEcuInhibit);
    Prly_Hndlr_Read_Prly_HndlrFuncInhibitPrlySts(&Prly_HndlrBus.Prly_HndlrFuncInhibitPrlySts);

    /* Process */
    Prly_ManageIgn();
    Prly_ManageReg();
    Prly_EcuInhibit();

    /* Output */
    Prly_Hndlr_Write_Prly_HndlrIgnOnOffSts(&Prly_HndlrBus.Prly_HndlrIgnOnOffSts);
    Prly_Hndlr_Write_Prly_HndlrIgnEdgeSts(&Prly_HndlrBus.Prly_HndlrIgnEdgeSts);
    Prly_Hndlr_Write_Prly_HndlrPrlyEcuInhibit(&Prly_HndlrBus.Prly_HndlrPrlyEcuInhibit);

    Prly_Hndlr_Timer_Elapsed = STM0_TIM0.U - Prly_Hndlr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/****************************************************************************
| NAME:             Prly_ManageIgn
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Manage regulator IC
****************************************************************************/
 static void Prly_ManageIgn(void)
 {
    static Prly_IgnTimeType Prly_IgnOnCnt, Prly_IgnOffCnt;
    static Prly_IgnVoltType Prly_IgnVoltage;

	
    Prly_HndlrBus.Prly_HndlrIgnEdgeSts = PRLY_NOEDGE;

    Prly_IgnVoltage = Prly_HndlrBus.Prly_HndlrCEMon;

	/* Initial Ignition Get Adc Once*/
	if(InitialIgnitionCheck == 0)
	{
		InitialIgnitionCheck = 1;
		if(Prly_IgnVoltage > Prly_Config.IgnOnVolt)
		{
			  Prly_HndlrBus.Prly_HndlrIgnOnOffSts = ON; /*State change Ign Off to On */
              Prly_HndlrBus.Prly_HndlrIgnEdgeSts = PRLY_RISINGEDGE;
		}
		else
		{
			  Prly_HndlrBus.Prly_HndlrIgnOnOffSts = OFF; /*State change Ign on to Off */
		}
	}

	
    switch(Prly_HndlrBus.Prly_HndlrIgnOnOffSts)
    {
      case OFF :
          if(Prly_IgnVoltage > Prly_Config.IgnOnVolt)
          {
            if(Prly_IgnOnCnt > (Prly_Config.IgnOnfilterTime/PRLY_PERIODICTIME_MS))
            {
              Prly_HndlrBus.Prly_HndlrIgnOnOffSts = ON; /*State change Ign Off to On */
              Prly_HndlrBus.Prly_HndlrIgnEdgeSts = PRLY_RISINGEDGE;
              Prly_IgnOnCnt = 0;
            }
            else
            {
              Prly_IgnOnCnt++;
            }
          }
          else
          {
            ; /*Stay Ign Off State */
          }
          break;
      case ON :
          if(Prly_IgnVoltage < Prly_Config.IgnOffVolt)
          {
            if(Prly_IgnOffCnt > (Prly_Config.IgnOnfilterTime/PRLY_PERIODICTIME_MS))
            {
              Prly_HndlrBus.Prly_HndlrIgnOnOffSts = OFF; /*State change Ign Off to On */
              Prly_HndlrBus.Prly_HndlrIgnEdgeSts = PRLY_FALLINGEDGE;
              Prly_IgnOffCnt = 0;
            }
            else
            {
              Prly_IgnOffCnt++;
            }
          }
          else
          {
            ; /*Stay Ign On State */
          }
          break;
      default :
          break;
    }
}

 /****************************************************************************
| NAME:             Prly_ManageReg
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Get Prly_ Ignition State
****************************************************************************/
 static void Prly_ManageReg(void)
{
    ;
}

static void Prly_EcuInhibit(void)
{
    ;
}
#define PRLY_HNDLR_STOP_SEC_CODE
#include "Prly_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
