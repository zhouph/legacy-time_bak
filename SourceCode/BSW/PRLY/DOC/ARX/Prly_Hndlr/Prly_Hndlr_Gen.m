Prly_HndlrCanRxClu2Info = Simulink.Bus;
DeList={
    'IgnRun'
    };
Prly_HndlrCanRxClu2Info = CreateBus(Prly_HndlrCanRxClu2Info, DeList);
clear DeList;

Prly_HndlrSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    };
Prly_HndlrSenPwrMonitorData = CreateBus(Prly_HndlrSenPwrMonitorData, DeList);
clear DeList;

Prly_HndlrEcuModeSts = Simulink.Bus;
DeList={'Prly_HndlrEcuModeSts'};
Prly_HndlrEcuModeSts = CreateBus(Prly_HndlrEcuModeSts, DeList);
clear DeList;

Prly_HndlrCEMon = Simulink.Bus;
DeList={'Prly_HndlrCEMon'};
Prly_HndlrCEMon = CreateBus(Prly_HndlrCEMon, DeList);
clear DeList;

Prly_HndlrMomEcuInhibit = Simulink.Bus;
DeList={'Prly_HndlrMomEcuInhibit'};
Prly_HndlrMomEcuInhibit = CreateBus(Prly_HndlrMomEcuInhibit, DeList);
clear DeList;

Prly_HndlrFuncInhibitPrlySts = Simulink.Bus;
DeList={'Prly_HndlrFuncInhibitPrlySts'};
Prly_HndlrFuncInhibitPrlySts = CreateBus(Prly_HndlrFuncInhibitPrlySts, DeList);
clear DeList;

Prly_HndlrIgnOnOffSts = Simulink.Bus;
DeList={'Prly_HndlrIgnOnOffSts'};
Prly_HndlrIgnOnOffSts = CreateBus(Prly_HndlrIgnOnOffSts, DeList);
clear DeList;

Prly_HndlrIgnEdgeSts = Simulink.Bus;
DeList={'Prly_HndlrIgnEdgeSts'};
Prly_HndlrIgnEdgeSts = CreateBus(Prly_HndlrIgnEdgeSts, DeList);
clear DeList;

Prly_HndlrPrlyEcuInhibit = Simulink.Bus;
DeList={'Prly_HndlrPrlyEcuInhibit'};
Prly_HndlrPrlyEcuInhibit = CreateBus(Prly_HndlrPrlyEcuInhibit, DeList);
clear DeList;

