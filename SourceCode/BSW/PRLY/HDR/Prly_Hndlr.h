/**
 * @defgroup Prly_Hndlr Prly_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRLY_HNDLR_H_
#define PRLY_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Prly_Types.h"
#include "Prly_Cfg.h"
#include "Prly_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PRLY_HNDLR_MODULE_ID      (0)
 #define PRLY_HNDLR_MAJOR_VERSION  (2)
 #define PRLY_HNDLR_MINOR_VERSION  (0)
 #define PRLY_HNDLR_PATCH_VERSION  (0)
 #define PRLY_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Prly_Hndlr_HdrBusType Prly_HndlrBus;

/* Version Info */
extern const SwcVersionInfo_t Prly_HndlrVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxClu2Info_t Prly_HndlrCanRxClu2Info;
extern SenPwrM_MainSenPwrMonitor_t Prly_HndlrSenPwrMonitorData;
extern Mom_HndlrEcuModeSts_t Prly_HndlrEcuModeSts;
extern Ioc_InputSR1msCEMon_t Prly_HndlrCEMon;
extern Mom_HndlrMomEcuInhibit_t Prly_HndlrMomEcuInhibit;
extern Eem_SuspcDetnFuncInhibitPrlySts_t Prly_HndlrFuncInhibitPrlySts;

/* Output Data Element */
extern Prly_HndlrIgnOnOffSts_t Prly_HndlrIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Prly_HndlrIgnEdgeSts;
extern Prly_HndlrPrlyEcuInhibit_t Prly_HndlrPrlyEcuInhibit;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Prly_Hndlr_Init(void);
extern void Prly_Hndlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRLY_HNDLR_H_ */
/** @} */
