/**
 * @defgroup Prly_Types Prly_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRLY_TYPES_H_
#define PRLY_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 /* Edge Type define */
 #define PRLY_NOEDGE			0
 #define PRLY_RISINGEDGE		1
 #define PRLY_FALLINGEDGE		2
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxClu2Info_t Prly_HndlrCanRxClu2Info;
    SenPwrM_MainSenPwrMonitor_t Prly_HndlrSenPwrMonitorData;
    Mom_HndlrEcuModeSts_t Prly_HndlrEcuModeSts;
    Ioc_InputSR1msCEMon_t Prly_HndlrCEMon;
    Mom_HndlrMomEcuInhibit_t Prly_HndlrMomEcuInhibit;
    Eem_SuspcDetnFuncInhibitPrlySts_t Prly_HndlrFuncInhibitPrlySts;

/* Output Data Element */
    Prly_HndlrIgnOnOffSts_t Prly_HndlrIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Prly_HndlrIgnEdgeSts;
    Prly_HndlrPrlyEcuInhibit_t Prly_HndlrPrlyEcuInhibit;
}Prly_Hndlr_HdrBusType;
//typedef int Prly_IgnStateType;
 typedef uint16 Prly_IgnVoltType;
 typedef uint16 Prly_IgnTimeType;

 typedef struct
 {
    Prly_IgnVoltType IgnOnVolt;
    Prly_IgnVoltType IgnOffVolt;
    Prly_IgnTimeType IgnOnfilterTime;
    Prly_IgnTimeType IgnOffFilterTime;
 }Prly_ConfigType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRLY_TYPES_H_ */
/** @} */
