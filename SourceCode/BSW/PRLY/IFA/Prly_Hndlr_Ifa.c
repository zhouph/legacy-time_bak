/**
 * @defgroup Prly_Hndlr_Ifa Prly_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Hndlr_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Prly_Hndlr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRLY_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Prly_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRLY_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Prly_MemMap.h"
#define PRLY_HNDLR_START_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/** Variable Section (32BIT)**/


#define PRLY_HNDLR_STOP_SEC_VAR_32BIT
#include "Prly_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRLY_HNDLR_START_SEC_CODE
#include "Prly_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRLY_HNDLR_STOP_SEC_CODE
#include "Prly_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
