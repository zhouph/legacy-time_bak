/**
 * @defgroup Prly_Hndlr_Ifa Prly_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Prly_Hndlr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRLY_HNDLR_IFA_H_
#define PRLY_HNDLR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Prly_Hndlr_Read_Prly_HndlrCanRxClu2Info(data) do \
{ \
    *data = Prly_HndlrCanRxClu2Info; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrSenPwrMonitorData(data) do \
{ \
    *data = Prly_HndlrSenPwrMonitorData; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrCanRxClu2Info_IgnRun(data) do \
{ \
    *data = Prly_HndlrCanRxClu2Info.IgnRun; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    *data = Prly_HndlrSenPwrMonitorData.SenPwrM_5V_DriveReq; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    *data = Prly_HndlrSenPwrMonitorData.SenPwrM_12V_Drive_Req; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrEcuModeSts(data) do \
{ \
    *data = Prly_HndlrEcuModeSts; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrCEMon(data) do \
{ \
    *data = Prly_HndlrCEMon; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrMomEcuInhibit(data) do \
{ \
    *data = Prly_HndlrMomEcuInhibit; \
}while(0);

#define Prly_Hndlr_Read_Prly_HndlrFuncInhibitPrlySts(data) do \
{ \
    *data = Prly_HndlrFuncInhibitPrlySts; \
}while(0);


/* Set Output DE MAcro Function */
#define Prly_Hndlr_Write_Prly_HndlrIgnOnOffSts(data) do \
{ \
    Prly_HndlrIgnOnOffSts = *data; \
}while(0);

#define Prly_Hndlr_Write_Prly_HndlrIgnEdgeSts(data) do \
{ \
    Prly_HndlrIgnEdgeSts = *data; \
}while(0);

#define Prly_Hndlr_Write_Prly_HndlrPrlyEcuInhibit(data) do \
{ \
    Prly_HndlrPrlyEcuInhibit = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRLY_HNDLR_IFA_H_ */
/** @} */
