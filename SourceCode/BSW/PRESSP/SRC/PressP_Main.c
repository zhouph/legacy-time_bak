/**
 * @defgroup PressP_Main PressP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PressP_Main.h"
#include "PressP_Process.h"
#include "PressP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESSP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "PressP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESSP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "PressP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
PressP_Main_HdrBusType PressP_MainBus;

/* Version Info */
const SwcVersionInfo_t PressP_MainVersionInfo = 
{   
    PRESSP_MAIN_MODULE_ID,           /* PressP_MainVersionInfo.ModuleId */
    PRESSP_MAIN_MAJOR_VERSION,       /* PressP_MainVersionInfo.MajorVer */
    PRESSP_MAIN_MINOR_VERSION,       /* PressP_MainVersionInfo.MinorVer */
    PRESSP_MAIN_PATCH_VERSION,       /* PressP_MainVersionInfo.PatchVer */
    PRESSP_MAIN_BRANCH_VERSION       /* PressP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Pedal_SenSyncPdf5msRawInfo_t PressP_MainPdf5msRawInfo;
Press_SenPressCalcInfo_t PressP_MainPressCalcInfo;
Press_SenSyncPressSenCalcInfo_t PressP_MainPressSenCalcInfo;
Mom_HndlrEcuModeSts_t PressP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t PressP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t PressP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t PressP_MainVBatt1Mon;
Diag_HndlrDiagClr_t PressP_MainDiagClrSrs;

/* Output Data Element */
PressP_MainPressPInfo_t PressP_MainPressPInfo;

uint32 PressP_Main_Timer_Start;
uint32 PressP_Main_Timer_Elapsed;

#define PRESSP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
#define PRESSP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_MAIN_START_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSP_MAIN_STOP_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESSP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
#define PRESSP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_MAIN_START_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSP_MAIN_STOP_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESSP_MAIN_START_SEC_CODE
#include "PressP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PressP_Main_Init(void)
{
    /* Initialize internal bus */
    PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdfSig = 0;
    PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = 0;
    PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdtSig = 0;
    PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = 0;
    PressP_MainBus.PressP_MainPressCalcInfo.PressP_SimP_1_100_bar = 0;
    PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar = 0;
    PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar = 0;
    PressP_MainBus.PressP_MainPressSenCalcInfo.PressP_SimPMoveAve = 0;
    PressP_MainBus.PressP_MainPressSenCalcInfo.PressP_CirP1MoveAve = 0;
    PressP_MainBus.PressP_MainPressSenCalcInfo.PressP_CirP2MoveAve = 0;
    PressP_MainBus.PressP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs = 0;
    PressP_MainBus.PressP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = 0;
    PressP_MainBus.PressP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = 0;
    PressP_MainBus.PressP_MainEcuModeSts = 0;
    PressP_MainBus.PressP_MainIgnOnOffSts = 0;
    PressP_MainBus.PressP_MainIgnEdgeSts = 0;
    PressP_MainBus.PressP_MainVBatt1Mon = 0;
    PressP_MainBus.PressP_MainDiagClrSrs = 0;
    PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err = 0;
    PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err = 0;
    PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err = 0;
}

void PressP_Main(void)
{
    PressP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdfSig(&PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdfSig);
    PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(&PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset);
    PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdtSig(&PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdtSig);
    PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(&PressP_MainBus.PressP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset);

    /* Decomposed structure interface */
    PressP_Main_Read_PressP_MainPressCalcInfo_PressP_SimP_1_100_bar(&PressP_MainBus.PressP_MainPressCalcInfo.PressP_SimP_1_100_bar);
    PressP_Main_Read_PressP_MainPressCalcInfo_PressP_CirP1_1_100_bar(&PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar);
    PressP_Main_Read_PressP_MainPressCalcInfo_PressP_CirP2_1_100_bar(&PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar);

    PressP_Main_Read_PressP_MainPressSenCalcInfo(&PressP_MainBus.PressP_MainPressSenCalcInfo);
    /*==============================================================================
    * Members of structure PressP_MainPressSenCalcInfo 
     : PressP_MainPressSenCalcInfo.PressP_SimPMoveAve;
     : PressP_MainPressSenCalcInfo.PressP_CirP1MoveAve;
     : PressP_MainPressSenCalcInfo.PressP_CirP2MoveAve;
     : PressP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs;
     : PressP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs;
     : PressP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs;
     =============================================================================*/
    
    PressP_Main_Read_PressP_MainEcuModeSts(&PressP_MainBus.PressP_MainEcuModeSts);
    PressP_Main_Read_PressP_MainIgnOnOffSts(&PressP_MainBus.PressP_MainIgnOnOffSts);
    PressP_Main_Read_PressP_MainIgnEdgeSts(&PressP_MainBus.PressP_MainIgnEdgeSts);
    PressP_Main_Read_PressP_MainVBatt1Mon(&PressP_MainBus.PressP_MainVBatt1Mon);
    PressP_Main_Read_PressP_MainDiagClrSrs(&PressP_MainBus.PressP_MainDiagClrSrs);

    /* Process */
	if(PressP_MainBus.PressP_MainDiagClrSrs == 1)
	{
		PressP_Main_Init();
	}
	PressP_Process();
	
    /* Output */
    PressP_Main_Write_PressP_MainPressPInfo(&PressP_MainBus.PressP_MainPressPInfo);
    /*==============================================================================
    * Members of structure PressP_MainPressPInfo 
     : PressP_MainPressPInfo.SimP_Noise_Err;
     : PressP_MainPressPInfo.CirP1_Noise_Err;
     : PressP_MainPressPInfo.CirP2_Noise_Err;
     =============================================================================*/
    

    PressP_Main_Timer_Elapsed = STM0_TIM0.U - PressP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESSP_MAIN_STOP_SEC_CODE
#include "PressP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
