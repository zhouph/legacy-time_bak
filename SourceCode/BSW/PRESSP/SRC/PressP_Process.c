/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include <stdlib.h>
#include <string.h>
#include "Common.h"
#include "PressP_Main.h"
#include "PressP_Cfg.h"
#include "PressP_Process.h"



/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define 	PRESSURE_30_BAR				3000
#define 	PRESSURE_MINUS_30_BAR		-3000

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

static PressPCfg* pPressP_Cfg;

static sint16 SimPoldVaule;
static sint16 CirP1oldVaule;
static sint16 CirP2oldValue;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void PressP_NoiseCheck(void);
static void PressP_Inhibit(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PressP_Process(void)
{
    PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err		= ERR_NONE;
    PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err 	= ERR_NONE;
    PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err 	= ERR_NONE;

	pPressP_Cfg = PressP_GetCfg();
	if(PressP_MainBus.PressP_MainVBatt1Mon < PRESSP_7V0 || PressP_MainBus.PressP_MainVBatt1Mon > PRESSP_17V0)
	{
        PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err     = ERR_INHIBIT;
        PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err    = ERR_INHIBIT;
        PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err    = ERR_INHIBIT;

	}
	else
	{
		PressP_NoiseCheck();	
	}
}

void PressP_Init(void)
{
	PressP_InitCfg();

	SimPoldVaule	= 0;
	CirP1oldVaule	= 0;	
	CirP2oldValue	= 0;	

    PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err		= ERR_NONE;
    PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err 	= ERR_NONE;
    PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err 	= ERR_NONE;	
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/*Difference of 30bar per 1 cycle(5ms)*/
static void PressP_NoiseCheck(void)
{
	sint16 TempDiffCirP1;
	sint16 TempDiffCirP2;
	sint16 TempDiffSimP;

	TempDiffCirP1 	= PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar - CirP1oldVaule;
	TempDiffCirP2 	= PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar - CirP2oldValue;
	TempDiffSimP	= PressP_MainBus.PressP_MainPressCalcInfo.PressP_SimP_1_100_bar  - SimPoldVaule;

	CirP1oldVaule	= PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar;
	CirP2oldValue	= PressP_MainBus.PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar;	
	SimPoldVaule	= PressP_MainBus.PressP_MainPressCalcInfo.PressP_SimP_1_100_bar;

    PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err = ERR_NONE;
	if(pPressP_Cfg->PressP_CIRP1_type == SENT_TYPE)
	{
		if(TempDiffCirP1 > PRESSURE_30_BAR)
		{
			PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err	= ERR_PREFAILED;
		}
		else
		{
			PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err	= ERR_PREPASSED;
		}
	}
	else
	{
		PressP_MainBus.PressP_MainPressPInfo.CirP1_Noise_Err = ERR_INHIBIT;
	}
		
	PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err = ERR_NONE;
	if(pPressP_Cfg->PressP_CIRP2_type == SENT_TYPE)
	{
		if(TempDiffCirP2 > PRESSURE_30_BAR)
		{
			PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err	= ERR_PREFAILED;
		}
		else
		{
			PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err	= ERR_PREPASSED;
		}		
	}
	else
	{
		PressP_MainBus.PressP_MainPressPInfo.CirP2_Noise_Err = ERR_INHIBIT;
	}

	PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err = ERR_NONE;
	if(pPressP_Cfg->PressP_SIMP_type == SENT_TYPE)
	{
		if(TempDiffSimP > PRESSURE_30_BAR)
		{
			PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err	= ERR_PREFAILED;
		}
		else
		{
			PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err	= ERR_PREPASSED;
		}		
	}
	else
	{
		PressP_MainBus.PressP_MainPressPInfo.SimP_Noise_Err = ERR_INHIBIT;
	}
}





/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
