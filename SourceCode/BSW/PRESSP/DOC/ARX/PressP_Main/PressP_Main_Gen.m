PressP_MainPdf5msRawInfo = Simulink.Bus;
DeList={
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
PressP_MainPdf5msRawInfo = CreateBus(PressP_MainPdf5msRawInfo, DeList);
clear DeList;

PressP_MainPressCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimP_1_100_bar'
    'PressP_CirP1_1_100_bar'
    'PressP_CirP2_1_100_bar'
    };
PressP_MainPressCalcInfo = CreateBus(PressP_MainPressCalcInfo, DeList);
clear DeList;

PressP_MainPressSenCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimPMoveAve'
    'PressP_CirP1MoveAve'
    'PressP_CirP2MoveAve'
    'PressP_SimPMoveAveEolOfs'
    'PressP_CirP1MoveAveEolOfs'
    'PressP_CirP2MoveAveEolOfs'
    };
PressP_MainPressSenCalcInfo = CreateBus(PressP_MainPressSenCalcInfo, DeList);
clear DeList;

PressP_MainEcuModeSts = Simulink.Bus;
DeList={'PressP_MainEcuModeSts'};
PressP_MainEcuModeSts = CreateBus(PressP_MainEcuModeSts, DeList);
clear DeList;

PressP_MainIgnOnOffSts = Simulink.Bus;
DeList={'PressP_MainIgnOnOffSts'};
PressP_MainIgnOnOffSts = CreateBus(PressP_MainIgnOnOffSts, DeList);
clear DeList;

PressP_MainIgnEdgeSts = Simulink.Bus;
DeList={'PressP_MainIgnEdgeSts'};
PressP_MainIgnEdgeSts = CreateBus(PressP_MainIgnEdgeSts, DeList);
clear DeList;

PressP_MainVBatt1Mon = Simulink.Bus;
DeList={'PressP_MainVBatt1Mon'};
PressP_MainVBatt1Mon = CreateBus(PressP_MainVBatt1Mon, DeList);
clear DeList;

PressP_MainDiagClrSrs = Simulink.Bus;
DeList={'PressP_MainDiagClrSrs'};
PressP_MainDiagClrSrs = CreateBus(PressP_MainDiagClrSrs, DeList);
clear DeList;

PressP_MainPressPInfo = Simulink.Bus;
DeList={
    'SimP_Noise_Err'
    'CirP1_Noise_Err'
    'CirP2_Noise_Err'
    };
PressP_MainPressPInfo = CreateBus(PressP_MainPressPInfo, DeList);
clear DeList;

