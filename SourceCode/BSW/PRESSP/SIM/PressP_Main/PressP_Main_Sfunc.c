#define S_FUNCTION_NAME      PressP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          18
#define WidthOutputPort         3

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "PressP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    PressP_MainPdf5msRawInfo.MoveAvrPdfSig = input[0];
    PressP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = input[1];
    PressP_MainPdf5msRawInfo.MoveAvrPdtSig = input[2];
    PressP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = input[3];
    PressP_MainPressCalcInfo.PressP_SimP_1_100_bar = input[4];
    PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar = input[5];
    PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar = input[6];
    PressP_MainPressSenCalcInfo.PressP_SimPMoveAve = input[7];
    PressP_MainPressSenCalcInfo.PressP_CirP1MoveAve = input[8];
    PressP_MainPressSenCalcInfo.PressP_CirP2MoveAve = input[9];
    PressP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs = input[10];
    PressP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = input[11];
    PressP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = input[12];
    PressP_MainEcuModeSts = input[13];
    PressP_MainIgnOnOffSts = input[14];
    PressP_MainIgnEdgeSts = input[15];
    PressP_MainVBatt1Mon = input[16];
    PressP_MainDiagClrSrs = input[17];

    PressP_Main();


    output[0] = PressP_MainPressPInfo.SimP_Noise_Err;
    output[1] = PressP_MainPressPInfo.CirP1_Noise_Err;
    output[2] = PressP_MainPressPInfo.CirP2_Noise_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    PressP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
