/**
 * @defgroup PressP_Cal PressP_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressP_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PressP_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESSP_START_SEC_CALIB_UNSPECIFIED
#include "PressP_MemMap.h"
/* Global Calibration Section */


#define PRESSP_STOP_SEC_CALIB_UNSPECIFIED
#include "PressP_MemMap.h"

#define PRESSP_START_SEC_CONST_UNSPECIFIED
#include "PressP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESSP_STOP_SEC_CONST_UNSPECIFIED
#include "PressP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESSP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_START_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSP_STOP_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
#define PRESSP_START_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSP_STOP_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_START_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSP_STOP_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESSP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_START_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSP_STOP_SEC_VAR_NOINIT_32BIT
#include "PressP_MemMap.h"
#define PRESSP_START_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSP_STOP_SEC_VAR_UNSPECIFIED
#include "PressP_MemMap.h"
#define PRESSP_START_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSP_STOP_SEC_VAR_32BIT
#include "PressP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESSP_START_SEC_CODE
#include "PressP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESSP_STOP_SEC_CODE
#include "PressP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
