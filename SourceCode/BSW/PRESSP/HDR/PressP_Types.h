/**
 * @defgroup PressP_Types PressP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESSP_TYPES_H_
#define PRESSP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Pedal_SenSyncPdf5msRawInfo_t PressP_MainPdf5msRawInfo;
    Press_SenPressCalcInfo_t PressP_MainPressCalcInfo;
    Press_SenSyncPressSenCalcInfo_t PressP_MainPressSenCalcInfo;
    Mom_HndlrEcuModeSts_t PressP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t PressP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t PressP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t PressP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t PressP_MainDiagClrSrs;

/* Output Data Element */
    PressP_MainPressPInfo_t PressP_MainPressPInfo;
}PressP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESSP_TYPES_H_ */
/** @} */
