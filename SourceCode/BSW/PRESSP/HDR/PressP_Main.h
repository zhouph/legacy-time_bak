/**
 * @defgroup PressP_Main PressP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESSP_MAIN_H_
#define PRESSP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PressP_Types.h"
#include "PressP_Cfg.h"
#include "PressP_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PRESSP_MAIN_MODULE_ID      (0)
 #define PRESSP_MAIN_MAJOR_VERSION  (2)
 #define PRESSP_MAIN_MINOR_VERSION  (0)
 #define PRESSP_MAIN_PATCH_VERSION  (0)
 #define PRESSP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern PressP_Main_HdrBusType PressP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t PressP_MainVersionInfo;

/* Input Data Element */
extern Pedal_SenSyncPdf5msRawInfo_t PressP_MainPdf5msRawInfo;
extern Press_SenPressCalcInfo_t PressP_MainPressCalcInfo;
extern Press_SenSyncPressSenCalcInfo_t PressP_MainPressSenCalcInfo;
extern Mom_HndlrEcuModeSts_t PressP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t PressP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t PressP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t PressP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t PressP_MainDiagClrSrs;

/* Output Data Element */
extern PressP_MainPressPInfo_t PressP_MainPressPInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void PressP_Main_Init(void);
extern void PressP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESSP_MAIN_H_ */
/** @} */
