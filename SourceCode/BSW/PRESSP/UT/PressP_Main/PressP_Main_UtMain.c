#include "unity.h"
#include "unity_fixture.h"
#include "PressP_Main.h"
#include "PressP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdfSig[MAX_STEP] = PRESSP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIG;
const Salsint16 UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset[MAX_STEP] = PRESSP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIGEOLOFFSET;
const Salsint16 UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdtSig[MAX_STEP] = PRESSP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIG;
const Salsint16 UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset[MAX_STEP] = PRESSP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIGEOLOFFSET;
const Saluint16 UtInput_PressP_MainPressCalcInfo_PressP_SimP_1_100_bar[MAX_STEP] = PRESSP_MAINPRESSCALCINFO_PRESSP_SIMP_1_100_BAR;
const Saluint16 UtInput_PressP_MainPressCalcInfo_PressP_CirP1_1_100_bar[MAX_STEP] = PRESSP_MAINPRESSCALCINFO_PRESSP_CIRP1_1_100_BAR;
const Saluint16 UtInput_PressP_MainPressCalcInfo_PressP_CirP2_1_100_bar[MAX_STEP] = PRESSP_MAINPRESSCALCINFO_PRESSP_CIRP2_1_100_BAR;
const Saluint16 UtInput_PressP_MainPressSenCalcInfo_PressP_SimPMoveAve[MAX_STEP] = PRESSP_MAINPRESSSENCALCINFO_PRESSP_SIMPMOVEAVE;
const Saluint16 UtInput_PressP_MainPressSenCalcInfo_PressP_CirP1MoveAve[MAX_STEP] = PRESSP_MAINPRESSSENCALCINFO_PRESSP_CIRP1MOVEAVE;
const Saluint16 UtInput_PressP_MainPressSenCalcInfo_PressP_CirP2MoveAve[MAX_STEP] = PRESSP_MAINPRESSSENCALCINFO_PRESSP_CIRP2MOVEAVE;
const Saluint16 UtInput_PressP_MainPressSenCalcInfo_PressP_SimPMoveAveEolOfs[MAX_STEP] = PRESSP_MAINPRESSSENCALCINFO_PRESSP_SIMPMOVEAVEEOLOFS;
const Saluint16 UtInput_PressP_MainPressSenCalcInfo_PressP_CirP1MoveAveEolOfs[MAX_STEP] = PRESSP_MAINPRESSSENCALCINFO_PRESSP_CIRP1MOVEAVEEOLOFS;
const Saluint16 UtInput_PressP_MainPressSenCalcInfo_PressP_CirP2MoveAveEolOfs[MAX_STEP] = PRESSP_MAINPRESSSENCALCINFO_PRESSP_CIRP2MOVEAVEEOLOFS;
const Mom_HndlrEcuModeSts_t UtInput_PressP_MainEcuModeSts[MAX_STEP] = PRESSP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_PressP_MainIgnOnOffSts[MAX_STEP] = PRESSP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_PressP_MainIgnEdgeSts[MAX_STEP] = PRESSP_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_PressP_MainVBatt1Mon[MAX_STEP] = PRESSP_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_PressP_MainDiagClrSrs[MAX_STEP] = PRESSP_MAINDIAGCLRSRS;

const Saluint8 UtExpected_PressP_MainPressPInfo_SimP_Noise_Err[MAX_STEP] = PRESSP_MAINPRESSPINFO_SIMP_NOISE_ERR;
const Saluint8 UtExpected_PressP_MainPressPInfo_CirP1_Noise_Err[MAX_STEP] = PRESSP_MAINPRESSPINFO_CIRP1_NOISE_ERR;
const Saluint8 UtExpected_PressP_MainPressPInfo_CirP2_Noise_Err[MAX_STEP] = PRESSP_MAINPRESSPINFO_CIRP2_NOISE_ERR;



TEST_GROUP(PressP_Main);
TEST_SETUP(PressP_Main)
{
    PressP_Main_Init();
}

TEST_TEAR_DOWN(PressP_Main)
{   /* Postcondition */

}

TEST(PressP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        PressP_MainPdf5msRawInfo.MoveAvrPdfSig = UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdfSig[i];
        PressP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset[i];
        PressP_MainPdf5msRawInfo.MoveAvrPdtSig = UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdtSig[i];
        PressP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = UtInput_PressP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset[i];
        PressP_MainPressCalcInfo.PressP_SimP_1_100_bar = UtInput_PressP_MainPressCalcInfo_PressP_SimP_1_100_bar[i];
        PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar = UtInput_PressP_MainPressCalcInfo_PressP_CirP1_1_100_bar[i];
        PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar = UtInput_PressP_MainPressCalcInfo_PressP_CirP2_1_100_bar[i];
        PressP_MainPressSenCalcInfo.PressP_SimPMoveAve = UtInput_PressP_MainPressSenCalcInfo_PressP_SimPMoveAve[i];
        PressP_MainPressSenCalcInfo.PressP_CirP1MoveAve = UtInput_PressP_MainPressSenCalcInfo_PressP_CirP1MoveAve[i];
        PressP_MainPressSenCalcInfo.PressP_CirP2MoveAve = UtInput_PressP_MainPressSenCalcInfo_PressP_CirP2MoveAve[i];
        PressP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs = UtInput_PressP_MainPressSenCalcInfo_PressP_SimPMoveAveEolOfs[i];
        PressP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs = UtInput_PressP_MainPressSenCalcInfo_PressP_CirP1MoveAveEolOfs[i];
        PressP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs = UtInput_PressP_MainPressSenCalcInfo_PressP_CirP2MoveAveEolOfs[i];
        PressP_MainEcuModeSts = UtInput_PressP_MainEcuModeSts[i];
        PressP_MainIgnOnOffSts = UtInput_PressP_MainIgnOnOffSts[i];
        PressP_MainIgnEdgeSts = UtInput_PressP_MainIgnEdgeSts[i];
        PressP_MainVBatt1Mon = UtInput_PressP_MainVBatt1Mon[i];
        PressP_MainDiagClrSrs = UtInput_PressP_MainDiagClrSrs[i];

        PressP_Main();

        TEST_ASSERT_EQUAL(PressP_MainPressPInfo.SimP_Noise_Err, UtExpected_PressP_MainPressPInfo_SimP_Noise_Err[i]);
        TEST_ASSERT_EQUAL(PressP_MainPressPInfo.CirP1_Noise_Err, UtExpected_PressP_MainPressPInfo_CirP1_Noise_Err[i]);
        TEST_ASSERT_EQUAL(PressP_MainPressPInfo.CirP2_Noise_Err, UtExpected_PressP_MainPressPInfo_CirP2_Noise_Err[i]);
    }
}

TEST_GROUP_RUNNER(PressP_Main)
{
    RUN_TEST_CASE(PressP_Main, All);
}
