# \file
#
# \brief PressP
#
# This file contains the implementation of the SWC
# module PressP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

PressP_CORE_PATH     := $(MANDO_BSW_ROOT)\PressP
PressP_CAL_PATH      := $(PressP_CORE_PATH)\CAL\$(PressP_VARIANT)
PressP_SRC_PATH      := $(PressP_CORE_PATH)\SRC
PressP_CFG_PATH      := $(PressP_CORE_PATH)\CFG\$(PressP_VARIANT)
PressP_HDR_PATH      := $(PressP_CORE_PATH)\HDR
PressP_IFA_PATH      := $(PressP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    PressP_CMN_PATH      := $(PressP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	PressP_UT_PATH		:= $(PressP_CORE_PATH)\UT
	PressP_UNITY_PATH	:= $(PressP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(PressP_UT_PATH)
	CC_INCLUDE_PATH		+= $(PressP_UNITY_PATH)
	PressP_Main_PATH 	:= PressP_UT_PATH\PressP_Main
endif
CC_INCLUDE_PATH    += $(PressP_CAL_PATH)
CC_INCLUDE_PATH    += $(PressP_SRC_PATH)
CC_INCLUDE_PATH    += $(PressP_CFG_PATH)
CC_INCLUDE_PATH    += $(PressP_HDR_PATH)
CC_INCLUDE_PATH    += $(PressP_IFA_PATH)
CC_INCLUDE_PATH    += $(PressP_CMN_PATH)

