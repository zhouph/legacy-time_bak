# \file
#
# \brief PressP
#
# This file contains the implementation of the SWC
# module PressP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += PressP_src

PressP_src_FILES        += $(PressP_SRC_PATH)\PressP_Main.c
PressP_src_FILES        += $(PressP_SRC_PATH)\PressP_Process.c
PressP_src_FILES        += $(PressP_IFA_PATH)\PressP_Main_Ifa.c
PressP_src_FILES        += $(PressP_CFG_PATH)\PressP_Cfg.c
PressP_src_FILES        += $(PressP_CAL_PATH)\PressP_Cal.c

ifeq ($(ICE_COMPILE),true)
PressP_src_FILES        += $(PressP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	PressP_src_FILES        += $(PressP_UNITY_PATH)\unity.c
	PressP_src_FILES        += $(PressP_UNITY_PATH)\unity_fixture.c	
	PressP_src_FILES        += $(PressP_UT_PATH)\main.c
	PressP_src_FILES        += $(PressP_UT_PATH)\PressP_Main\PressP_Main_UtMain.c
endif