/**
 * @defgroup PressP_Main_Ifa PressP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESSP_MAIN_IFA_H_
#define PRESSP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define PressP_Main_Read_PressP_MainPdf5msRawInfo(data) do \
{ \
    *data = PressP_MainPdf5msRawInfo; \
}while(0);

#define PressP_Main_Read_PressP_MainPressCalcInfo(data) do \
{ \
    *data = PressP_MainPressCalcInfo; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo; \
}while(0);

#define PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdfSig(data) do \
{ \
    *data = PressP_MainPdf5msRawInfo.MoveAvrPdfSig; \
}while(0);

#define PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(data) do \
{ \
    *data = PressP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset; \
}while(0);

#define PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdtSig(data) do \
{ \
    *data = PressP_MainPdf5msRawInfo.MoveAvrPdtSig; \
}while(0);

#define PressP_Main_Read_PressP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(data) do \
{ \
    *data = PressP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset; \
}while(0);

#define PressP_Main_Read_PressP_MainPressCalcInfo_PressP_SimP_1_100_bar(data) do \
{ \
    *data = PressP_MainPressCalcInfo.PressP_SimP_1_100_bar; \
}while(0);

#define PressP_Main_Read_PressP_MainPressCalcInfo_PressP_CirP1_1_100_bar(data) do \
{ \
    *data = PressP_MainPressCalcInfo.PressP_CirP1_1_100_bar; \
}while(0);

#define PressP_Main_Read_PressP_MainPressCalcInfo_PressP_CirP2_1_100_bar(data) do \
{ \
    *data = PressP_MainPressCalcInfo.PressP_CirP2_1_100_bar; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo_PressP_SimPMoveAve(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo.PressP_SimPMoveAve; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo_PressP_CirP1MoveAve(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo.PressP_CirP1MoveAve; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo_PressP_CirP2MoveAve(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo.PressP_CirP2MoveAve; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo_PressP_SimPMoveAveEolOfs(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo.PressP_SimPMoveAveEolOfs; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo_PressP_CirP1MoveAveEolOfs(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo.PressP_CirP1MoveAveEolOfs; \
}while(0);

#define PressP_Main_Read_PressP_MainPressSenCalcInfo_PressP_CirP2MoveAveEolOfs(data) do \
{ \
    *data = PressP_MainPressSenCalcInfo.PressP_CirP2MoveAveEolOfs; \
}while(0);

#define PressP_Main_Read_PressP_MainEcuModeSts(data) do \
{ \
    *data = PressP_MainEcuModeSts; \
}while(0);

#define PressP_Main_Read_PressP_MainIgnOnOffSts(data) do \
{ \
    *data = PressP_MainIgnOnOffSts; \
}while(0);

#define PressP_Main_Read_PressP_MainIgnEdgeSts(data) do \
{ \
    *data = PressP_MainIgnEdgeSts; \
}while(0);

#define PressP_Main_Read_PressP_MainVBatt1Mon(data) do \
{ \
    *data = PressP_MainVBatt1Mon; \
}while(0);

#define PressP_Main_Read_PressP_MainDiagClrSrs(data) do \
{ \
    *data = PressP_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define PressP_Main_Write_PressP_MainPressPInfo(data) do \
{ \
    PressP_MainPressPInfo = *data; \
}while(0);

#define PressP_Main_Write_PressP_MainPressPInfo_SimP_Noise_Err(data) do \
{ \
    PressP_MainPressPInfo.SimP_Noise_Err = *data; \
}while(0);

#define PressP_Main_Write_PressP_MainPressPInfo_CirP1_Noise_Err(data) do \
{ \
    PressP_MainPressPInfo.CirP1_Noise_Err = *data; \
}while(0);

#define PressP_Main_Write_PressP_MainPressPInfo_CirP2_Noise_Err(data) do \
{ \
    PressP_MainPressPInfo.CirP2_Noise_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESSP_MAIN_IFA_H_ */
/** @} */
