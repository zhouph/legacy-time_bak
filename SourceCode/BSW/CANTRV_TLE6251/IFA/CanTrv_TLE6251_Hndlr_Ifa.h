/**
 * @defgroup CanTrv_TLE6251_Hndlr_Ifa CanTrv_TLE6251_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanTrv_TLE6251_Hndlr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef CANTRV_TLE6251_HNDLR_IFA_H_
#define CANTRV_TLE6251_HNDLR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define CanTrv_TLE6251_Hndlr_Read_CanTrv_TLE6251_HndlrEcuModeSts(data) do \
{ \
    *data = CanTrv_TLE6251_HndlrEcuModeSts; \
}while(0);

#define CanTrv_TLE6251_Hndlr_Read_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts(data) do \
{ \
    *data = CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts; \
}while(0);


/* Set Output DE MAcro Function */
#define CanTrv_TLE6251_Hndlr_Write_CanTrv_TLE6251_HndlrMainCanEn(data) do \
{ \
    CanTrv_TLE6251_HndlrMainCanEn = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* CANTRV_TLE6251_HNDLR_IFA_H_ */
/** @} */
