#include "unity.h"
#include "unity_fixture.h"
#include "CanTrv_TLE6251_Hndlr.h"
#include "CanTrv_TLE6251_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_CanTrv_TLE6251_HndlrEcuModeSts[MAX_STEP] = CANTRV_TLE6251_HNDLRECUMODESTS;
const Eem_SuspcDetnFuncInhibitCanTrcvSts_t UtInput_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts[MAX_STEP] = CANTRV_TLE6251_HNDLRFUNCINHIBITCANTRCVSTS;

const CanTrv_TLE6251_HndlrMainCanEn_t UtExpected_CanTrv_TLE6251_HndlrMainCanEn[MAX_STEP] = CANTRV_TLE6251_HNDLRMAINCANEN;



TEST_GROUP(CanTrv_TLE6251_Hndlr);
TEST_SETUP(CanTrv_TLE6251_Hndlr)
{
    CanTrv_TLE6251_Hndlr_Init();
}

TEST_TEAR_DOWN(CanTrv_TLE6251_Hndlr)
{   /* Postcondition */

}

TEST(CanTrv_TLE6251_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        CanTrv_TLE6251_HndlrEcuModeSts = UtInput_CanTrv_TLE6251_HndlrEcuModeSts[i];
        CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = UtInput_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts[i];

        CanTrv_TLE6251_Hndlr();

        TEST_ASSERT_EQUAL(CanTrv_TLE6251_HndlrMainCanEn, UtExpected_CanTrv_TLE6251_HndlrMainCanEn[i]);
    }
}

TEST_GROUP_RUNNER(CanTrv_TLE6251_Hndlr)
{
    RUN_TEST_CASE(CanTrv_TLE6251_Hndlr, All);
}
