/**
 * @defgroup CanTrv_TLE6251_Hndlr CanTrv_TLE6251_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanTrv_TLE6251_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef CANTRV_TLE6251_HNDLR_H_
#define CANTRV_TLE6251_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanTrv_TLE6251_Types.h"
#include "CanTrv_TLE6251_Cfg.h"
#include "CanTrv_TLE6251_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define CANTRV_TLE6251_HNDLR_MODULE_ID      (0)
 #define CANTRV_TLE6251_HNDLR_MAJOR_VERSION  (2)
 #define CANTRV_TLE6251_HNDLR_MINOR_VERSION  (0)
 #define CANTRV_TLE6251_HNDLR_PATCH_VERSION  (0)
 #define CANTRV_TLE6251_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern CanTrv_TLE6251_Hndlr_HdrBusType CanTrv_TLE6251_HndlrBus;

/* Version Info */
extern const SwcVersionInfo_t CanTrv_TLE6251_HndlrVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t CanTrv_TLE6251_HndlrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitCanTrcvSts_t CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts;

/* Output Data Element */
extern CanTrv_TLE6251_HndlrMainCanEn_t CanTrv_TLE6251_HndlrMainCanEn;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void CanTrv_TLE6251_Hndlr_Init(void);
extern void CanTrv_TLE6251_Hndlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* CANTRV_TLE6251_HNDLR_H_ */
/** @} */
