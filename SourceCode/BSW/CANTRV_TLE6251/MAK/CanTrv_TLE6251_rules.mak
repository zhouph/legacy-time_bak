# \file
#
# \brief CanTrv_TLE6251
#
# This file contains the implementation of the SWC
# module CanTrv_TLE6251.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += CanTrv_TLE6251_src

CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_SRC_PATH)\CanTrv_TLE6251_Hndlr.c
CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_IFA_PATH)\CanTrv_TLE6251_Hndlr_Ifa.c
CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_CFG_PATH)\CanTrv_TLE6251_Cfg.c
CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_CAL_PATH)\CanTrv_TLE6251_Cal.c

ifeq ($(ICE_COMPILE),true)
CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_UNITY_PATH)\unity.c
	CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_UNITY_PATH)\unity_fixture.c	
	CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_UT_PATH)\main.c
	CanTrv_TLE6251_src_FILES        += $(CanTrv_TLE6251_UT_PATH)\CanTrv_TLE6251_Hndlr\CanTrv_TLE6251_Hndlr_UtMain.c
endif