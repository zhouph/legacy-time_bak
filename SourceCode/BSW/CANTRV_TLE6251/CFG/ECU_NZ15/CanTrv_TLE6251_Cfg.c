/**
 * @defgroup CanTrv_TLE6251_Cfg CanTrv_TLE6251_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanTrv_TLE6251_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanTrv_TLE6251_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define CANTRV_TLE6251_START_SEC_CONST_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define CANTRV_TLE6251_STOP_SEC_CONST_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANTRV_TLE6251_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_START_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_START_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_START_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (32BIT)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANTRV_TLE6251_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_START_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_START_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_START_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (32BIT)**/


#define CANTRV_TLE6251_STOP_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define CANTRV_TLE6251_START_SEC_CODE
#include "CanTrv_TLE6251_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define CANTRV_TLE6251_STOP_SEC_CODE
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
