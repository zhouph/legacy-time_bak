/**
 * @defgroup CanTrv_TLE6251_Hndlr CanTrv_TLE6251_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        CanTrv_TLE6251_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "CanTrv_TLE6251_Hndlr.h"
#include "CanTrv_TLE6251_Hndlr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define CANTRV_TLE6251_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define CANTRV_TLE6251_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
CanTrv_TLE6251_Hndlr_HdrBusType CanTrv_TLE6251_HndlrBus;

/* Version Info */
const SwcVersionInfo_t CanTrv_TLE6251_HndlrVersionInfo = 
{   
    CANTRV_TLE6251_HNDLR_MODULE_ID,           /* CanTrv_TLE6251_HndlrVersionInfo.ModuleId */
    CANTRV_TLE6251_HNDLR_MAJOR_VERSION,       /* CanTrv_TLE6251_HndlrVersionInfo.MajorVer */
    CANTRV_TLE6251_HNDLR_MINOR_VERSION,       /* CanTrv_TLE6251_HndlrVersionInfo.MinorVer */
    CANTRV_TLE6251_HNDLR_PATCH_VERSION,       /* CanTrv_TLE6251_HndlrVersionInfo.PatchVer */
    CANTRV_TLE6251_HNDLR_BRANCH_VERSION       /* CanTrv_TLE6251_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t CanTrv_TLE6251_HndlrEcuModeSts;
Eem_SuspcDetnFuncInhibitCanTrcvSts_t CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts;

/* Output Data Element */
CanTrv_TLE6251_HndlrMainCanEn_t CanTrv_TLE6251_HndlrMainCanEn;

uint32 CanTrv_TLE6251_Hndlr_Timer_Start;
uint32 CanTrv_TLE6251_Hndlr_Timer_Elapsed;

#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (32BIT)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "CanTrv_TLE6251_MemMap.h"
#define CANTRV_TLE6251_HNDLR_START_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/** Variable Section (32BIT)**/


#define CANTRV_TLE6251_HNDLR_STOP_SEC_VAR_32BIT
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define CANTRV_TLE6251_HNDLR_START_SEC_CODE
#include "CanTrv_TLE6251_MemMap.h"
static void CanTrv_TLE6251_PortInit(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void CanTrv_TLE6251_Hndlr_Init(void)
{
    /* Initialize internal bus */
    CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrEcuModeSts = 0;
    CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = 0;
    CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrMainCanEn = 0;
    
    CanTrv_TLE6251_PortInit();
}

void CanTrv_TLE6251_Hndlr(void)
{
    CanTrv_TLE6251_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    CanTrv_TLE6251_Hndlr_Read_CanTrv_TLE6251_HndlrEcuModeSts(&CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrEcuModeSts);
    CanTrv_TLE6251_Hndlr_Read_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts(&CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts);

    /* Process */

    /* Output */
    CanTrv_TLE6251_Hndlr_Write_CanTrv_TLE6251_HndlrMainCanEn(&CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrMainCanEn);

    CanTrv_TLE6251_Hndlr_Timer_Elapsed = STM0_TIM0.U - CanTrv_TLE6251_Hndlr_Timer_Start;
}

static void CanTrv_TLE6251_PortInit(void)
{
	CanTrv_TLE6251_HndlrBus.CanTrv_TLE6251_HndlrMainCanEn = 1;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define CANTRV_TLE6251_HNDLR_STOP_SEC_CODE
#include "CanTrv_TLE6251_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
