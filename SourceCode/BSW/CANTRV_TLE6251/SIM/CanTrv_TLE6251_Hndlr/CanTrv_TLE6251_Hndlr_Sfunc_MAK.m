clc;
clear all;

WORK_DIR = cd;
SRC_DIR = strcat(WORK_DIR,'\..\..\SRC');
IFA_DIR = strcat(WORK_DIR,'\..\..\IFA');
HDR_DIR = strcat(WORK_DIR,'\..\..\HDR');
VAR_DIR = strcat(WORK_DIR,'\..\..\CFG\VariantName');
CAL_DIR = strcat(WORK_DIR,'\..\..\CAL');
CMN_DIR = strcat(WORK_DIR,'\..\..\ICE\CMN');

TAR_FILE={
{SRC_DIR,'CanTrv_TLE6251_Hndlr.c'}
{IFA_DIR,'CanTrv_TLE6251_Hndlr_Ifa.c'}
{IFA_DIR,'CanTrv_TLE6251_Hndlr_Ifa.h'}
{HDR_DIR,'CanTrv_TLE6251_Hndlr.h'}
{HDR_DIR,'CanTrv_TLE6251_MemMap.h'}
{HDR_DIR,'CanTrv_TLE6251_Types.h'}
{VAR_DIR,'CanTrv_TLE6251_Cfg.c'}
{VAR_DIR,'CanTrv_TLE6251_Cfg.h'}
{CAL_DIR,'CanTrv_TLE6251_Cal.c'}
{CAL_DIR,'CanTrv_TLE6251_Cal.h'}
{CMN_DIR,'Bsw_Cfg.h'}
{CMN_DIR,'Bsw_Types.h'}
{CMN_DIR,'Compiler.h'}
{CMN_DIR,'Compiler_Cfg.h'}
{CMN_DIR,'Hal_Types.h'}
{CMN_DIR,'HalDataHandle_Types.h'}
{CMN_DIR,'Platform_Types.h'}
{CMN_DIR,'Sal_Types.h'}
{CMN_DIR,'SalDataHandle_Types.h'}
{CMN_DIR,'Rte_Types.h'}
{CMN_DIR,'RteDataHandle_Types.h'}
{CMN_DIR,'Std_Types.h'}
};

N = length(TAR_FILE);
for i=1:N
    copyfile(fullfile(TAR_FILE{i}{1},TAR_FILE{i}{2}), fullfile(cd));
end

mex -g -v CanTrv_TLE6251_Hndlr_Sfunc.c CanTrv_TLE6251_Hndlr.c CanTrv_TLE6251_Hndlr_Ifa.c CanTrv_TLE6251_Cfg.c CanTrv_TLE6251_Cal.c;

for i=1:N
    delete(TAR_FILE{i}{2});
end
