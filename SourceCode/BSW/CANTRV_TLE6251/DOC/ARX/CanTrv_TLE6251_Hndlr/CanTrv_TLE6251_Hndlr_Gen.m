CanTrv_TLE6251_HndlrEcuModeSts = Simulink.Bus;
DeList={'CanTrv_TLE6251_HndlrEcuModeSts'};
CanTrv_TLE6251_HndlrEcuModeSts = CreateBus(CanTrv_TLE6251_HndlrEcuModeSts, DeList);
clear DeList;

CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = Simulink.Bus;
DeList={'CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts'};
CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = CreateBus(CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts, DeList);
clear DeList;

CanTrv_TLE6251_HndlrMainCanEn = Simulink.Bus;
DeList={'CanTrv_TLE6251_HndlrMainCanEn'};
CanTrv_TLE6251_HndlrMainCanEn = CreateBus(CanTrv_TLE6251_HndlrMainCanEn, DeList);
clear DeList;

