# \file
#
# \brief Scheduler
#
# This file contains the implementation of the BSW
# module Scheduler.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Scheduler_src

Scheduler_src_FILES       += $(Scheduler_CORE_PATH)\Brake_Main.c
Scheduler_src_FILES       += $(Scheduler_CORE_PATH)\IDB_Main.c

#################################################################
