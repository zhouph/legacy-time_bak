# \file
#
# \brief Scheduler
#
# This file contains the implementation of the BSW
# module Scheduler.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

Scheduler_CORE_PATH     := $(MANDO_BSW_ROOT)\Scheduler

CC_INCLUDE_PATH    += $(Scheduler_CORE_PATH)