/******************************************************************************
 | Project Name: IDB
 |    File Name: IDB_Main.c
 |
 |  Description: IDB main routine with Aurix
 |
 |------------------------------------------------------------------------------
 |               A U T H O R   I D E N T I T Y
 |------------------------------------------------------------------------------
 | Initials     Name Jinho.Park            Company   Mando
 | --------     ---------------------     --------------------------------------
 |------------------------------------------------------------------------------
 |               R E V I S I O N   H I S T O R Y
 |------------------------------------------------------------------------------
 | Date         Ver  Author  Description
 | ----------  ----  ------  ---------------------------------------------------
 | 22.05.2014   1.0   Jinho  Creation;
 |                           Starting with Aurix
 |*****************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Std_Types.h"
#include "Mando_Std_Types.h"
#include "Common.h"
#include "Dio.h"
#include "Brake_Main.h"
#include "Prly_Hndlr.h"
#include "Test.h"
#include "Task_50us.h"
#include "Task_1ms.h"
#include "vx1000_tc2xx.h"
#include "Gpt.h"
#include "TomTimerHandler.h"
#include "AdcIf_Conv50us.h"
#include "IfxInt_reg.h"
#include "IfxStm_reg.h"
#include "Irq.h"
#include "Mps_TLE5012_Hndlr_50us.h"
/*
#include "Dio.h"
#include "Brake_Main.h"
#include "Common.h"
#include "TomTimerHandler.h"
#include "PwrM.h"
#include "vx1000_tc2xx.h"
#include "Gpt.h"
#include "AdcIf.h"
#include "VdrM.h"
#include "Test.h"
#include "MotorInverterControl.h"
#include "Msp.h"
#include "MotorCurrentControl.h"
#include "SAL.h"
#include "RegM_TPS65381.h"
#include "CanTrcvM_TLE6251.h"
#include "IoHwAb.h"
#include "EEM.h"
*/

//LEJ 
#include "TimE.h"
void TimE_GlobalSupervision(void);

/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/

/*******************************************************************************
**               Private Macro Definitions                                    **
*******************************************************************************/
#define 	u8s_1MS_TIME_CNT		6250	/* 1tick : 160ns*/

#define _1ms_IRQ_REF_TOM_Module_NUM 	2
#define _1ms_IRQ_REF_TOM_Module_CH_NUM 	0

/* For Test */
#define PORT_HIGH 	((0<<16) | (1<<0)) // OMR register set
#define PORT_LOW 	((1<<16) | (0<<0)) // OMR register set

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
static enum
{
	STS_1ms_TASK_ZERO,			    	/* 1ms Task "0" Sequence Load Valencing */
	STS_1ms_TASK_FIRST,					/* 1ms Task "1" Sequence Load Valencing */
	STS_1ms_TASK_SECOND,	        	/* 1ms Task "2" Sequence Load Valencing */
	STS_1ms_TASK_THIRD,					/* 1ms Task "3" Sequence Load Valencing */
	STS_1ms_TASK_FOURTH					/* 1ms Task "4" Sequence Load Valencing */
}Sts_1msTask_Sync_Balancing;



/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
uint16 system_loop_counter;
uint16 system_5ms_Flag;
uint16 system_5ms_Begin_Flag=0;
uint16 system_5ms_loop_counter;
uint32 system_1ms_loop_counter;
uint32 system_50us_loop_counter;
uint32 system_50us_Core2_loop_counter;
/*******************************************************************************
**                     Private  Variable Definitions                          **
*******************************************************************************/
vuint8_t wtu8Isr1msCnt;
uint32	MTR_CENTER_CNT;


uint32 Timer_5ms_before, Timer_5ms_elapsed; 
uint32 Timer_1ms_before, Timer_1ms_elapsed;
uint32 Timer_50us_before, Timer_50us_elapsed;
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/*******************************************************************************
** Syntax : void IDB_Main_Core1(void)                                         **
**                                                                            **
** Service ID:   : NA                                                         **
**                                                                            **
** Sync/Async:   : Synchronous                                                **
**                                                                            **
** Reentrancy:   : Non Reentrant                                              **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : IDB Main SW								                  **
*******************************************************************************/
void IDB_Main_Core1(void)
{


	MB_vInitSys();
	
	#include "IfxCpu_reg.h"
	__mtcr(CPU_CCTRL,2);
	CPU0_CCTRL.B.CE = 1;
	CPU2_CCTRL.B.CE = 1;
	
	while(1)
	{
		while ( system_5ms_Flag == CTRL_OFF );		//5ms Interrupt Flag Wait
		system_5ms_Flag = CTRL_OFF;

		Timer_5ms_before  = STM0_TIM0.U; 
	
		WM_vStartLoopTime();
		MB_vPreProc();
		
	#if (ALIVESUPERVISION_INDICATION == STD_ON)
		TimE_GlobalSupervision();
	#endif 	
		MB_vNormalMode();
				
		if(Prly_HndlrIgnOnOffSts == OFF)
		{
			MB_vOffMode();
		}
		else
		{
			IgnBlsDoorTimeCounter = 0;
			BackUpShutdownTimeCounter = 0;
		}
		
		//ErP_Mainfunction(); 5ms TASK 포함
		//EEM_MainFunction(); 5ms TASK 포함
		
		Test_MainFunction_5ms();
		Timer_5ms_elapsed = STM0_TIM0.U - Timer_5ms_before; 

		
		//WM_vCheckLoopTime();
		
		//End Check Point of 5ms interrupt(Main Loop)
	

		/*
		Valve_MainFunction_5ms();
		CanTrcvM_TLE6251_MainFunction();
		Pedal_MainFunction_5ms();
		Press_MainFunction_5ms();
		PwrM_MainFunction();
		Relay_MainFunction();
		Sw_MainFunction();
		AsicM_MainFunction_5ms();
		IcuCdd_MainFunction();
		Wss_MainFunction();
		
		//Concept 상 이 위의 함수들은 1ms 첫번째 인터럽트 발생전에 실행이 완료 되어야 함
		
		MB_vPreProc();
		
		MB_vNormalMode();
		
		if(PwrM_GetIgnStatus().OnOffStatus == PwrM_IgnOff)
		{
			MB_vOffMode();
		}
		else
		{
			IgnBlsDoorTimeCounter = 0;
			BackUpShutdownTimeCounter = 0;
		}
		
		ErP_Mainfunction();
		EEM_MainFunction();
		
		Test_MainFunction_5ms();
		
		//WM_vCheckLoopTime();
		
		//End Check Point of 5ms interrupt(Main Loop)
		TimerSet_Main_End = __mfcr(CPU_CCNT);
		TimerSet_Main_Elapsed = TimerSet_Main_End - TimerSet_Main_Start;
		*/
	}

}

/*******************************************************************************
** Syntax : void MTR_PERIOD_CENTER (void)                                     **
**                                                                            **
** Service ID:   : NA                                                         **
**                                                                            **
** Sync/Async:   : Synchronous                                                **
**                                                                            **
** Reentrancy:   : Non Reentrant                                              **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : 50us Motor Controller Interrupt			                  **
*******************************************************************************/
void MTR_PERIOD_CENTER (void)
{

	uint8 i;
	
	// Start Check Point of 50us interrupt
	//P21_OMR.U = (uint32_t)(PORT_HIGH << 5);
	
	//SRC_GPSR10.U |= 0x04000000U;



	Timer_50us_before  = STM0_TIM0.U;


	system_50us_loop_counter++;
	//TimerSet_50us_CUR_Start  = CURRENT_TIME;
/*	
	MpsM_MainFunction_50us();
	
	Msp_MainFunction();
	
	LAMTR_vCallMotICtrlr();
	
	Test_MainFunction_50us();
	
	AdcIf_DecTimeTrigger(AdcIf_UsgTimeTrigger3);
	AdcIf_DecTimeTrigger(AdcIf_UsgTimeTrigger4);
	
	Motor_MainFunction();
*/	//Task_50us 대체
	
	Task_50us();
	
	VX1000_EVENT(0); // measurement 변수 업데이트 주기마다 호출
	//TimerSet_50us_CUR_End = CURRENT_TIME - TimerSet_50us_CUR_Start;
	//TimerSet_50us_CUR_Elapsed = TIME_TICK_US(TimerSet_50us_CUR_End);
	MTR_CENTER_CNT++;
	//End Check Point of 50us interrupt
	//P21_OMR.U = (uint32_t)(PORT_LOW << 5);



	Timer_50us_elapsed = STM0_TIM0.U - Timer_50us_before;

	
	
}
#if 0
#if((IRQ_GPSRGROUP1_SR0_PRIO > 0) || (IRQ_GPSRGROUP1_SR0_CAT == IRQ_CAT23))
void GPSRGROUP1_SR0_ISR(void)
#elif IRQ_GPSRGROUP1_SR0_CAT == IRQ_CAT23
ISR(GPSRGROUP1_SR0_ISR)
#endif
{

	TimerSet_50us_Core_Now  = __mfcr(CPU_CCNT);

	system_50us_Core2_loop_counter++;
	Mps_TLE5012_Hndlr_50us();
    /**********************************************************************************************
     AdcIf_Conv50us start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    AdcIf_Conv50usFuncInhibitAdcifSts = Task_50usBus.AdcIf_Conv50usFuncInhibitAdcifSts;
    AdcIf_Conv50usEcuModeSts = Task_50usBus.AdcIf_Conv50usEcuModeSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    AdcIf_Conv50us();

    /* Structure interface */
    /* Structure interface */
    Task_50usBus.AdcIf_Conv50usHwTrigMotInfo = AdcIf_Conv50usHwTrigMotInfo;
    /*==============================================================================
    * Members of structure AdcIf_Conv50usHwTrigMotInfo 
     : AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/    
	
	
	/* Single interface */

    /**********************************************************************************************
     AdcIf_Conv50us end
     **********************************************************************************************/

	VX1000_EVENT(0); // measurement 변수 업데이트 주기마다 호출

	TimerSet_50us_Core_after = __mfcr(CPU_CCNT);
	TimerSet_50us_Core_elapsed = TimerSet_50us_Core_after - TimerSet_50us_Core_Now;



}
#endif
/*******************************************************************************
** Syntax : void Gpt_Notification_SystemTick_Core0(void)                      **
**                                                                            **
** Service ID:   : NA                                                         **
**                                                                            **
** Sync/Async:   : Synchronous                                                **
**                                                                            **
** Reentrancy:   : Non Reentrant                                              **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : 1ms Interrupt								                  **
*******************************************************************************/
void Gpt_Notification_1msSystemISR_Core1(void)
{

	uint8 i;

	//Start Check Point of 1ms interrupt
	//P21_OMR.U = (uint32_t)(PORT_HIGH << 3);

	system_1ms_loop_counter++;



	Timer_1ms_before  = STM0_TIM0.U;
	Sts_1msTask_Sync_Balancing = wtu8Isr1msCnt;

	switch	(Sts_1msTask_Sync_Balancing)
	{
		case STS_1ms_TASK_ZERO:
		break;

		case STS_1ms_TASK_FIRST:
		break;

		case STS_1ms_TASK_SECOND:
		break;

		case STS_1ms_TASK_THIRD:
		break;

		case STS_1ms_TASK_FOURTH:
		break;

		default:
			Sts_1msTask_Sync_Balancing = STS_1ms_TASK_ZERO;
		break;
    }
/*    
	if(SALbus.MotOrgSetStInfo_st.IdbMotOrgSetFlg == 0)
	{
		Ses_MainFunction();
	}
	else
	{
		LAMTR_vCallMotIRefGenn1ms();
	}	

	AdcIf_DecNormal();
	AdcIf_StartNormalGroupConversion();
	RegM_TPS65381_MainFunction();
	Valve_MainFunction_1ms();
	AsicM_MainFunction_1ms();
	VdrM_MainFunction();
	Pedal_MainFunction_1ms();
    Press_MainFunction_1ms();
    MpsM_MainFunction_1ms();
    GdM_MainFunction();
*/ // Task_1ms 대체
	Task_1ms();
    Test_MainFunction_1ms();
	wtu8Isr1msCnt++;

    if(wtu8Isr1msCnt >= LOOP_TIME)
    {
        wtu8Isr1msCnt=0; // 삭제. 1msec 마다 ADC 를 수행하므로 상기 변수를 ADC 에서 사용함.
        //Gpt_DisableNotification(GptConf_GptChannel_System1msTick_Core1);
        //sGpt_StopTimer(GptConf_GptChannel_System1msTick_Core1);
    }
	//TimerSet_1ms_CUR_End = CURRENT_TIME - TimerSet_1ms_CUR_Start;
	//TimerSet_1ms_CUR_Elapsed = TIME_TICK_US(TimerSet_1ms_CUR_End);
    //End Check Point of 1ms interrupt
    //P21_OMR.U = (uint32_t)(PORT_LOW << 3);


    Timer_1ms_elapsed = STM0_TIM0.U - Timer_1ms_before;


}

void Gpt_Notification_5msSystemISR_Core1(void)
{
	
	//Start Check Point of 5ms interrupt(Main Loop) 
	P20_OMR.U = (uint32_t)(PORT_HIGH << 3);

	system_5ms_Flag = CTRL_ON;
	system_5ms_loop_counter++;

	///아래 코드는 한번만 실행 되는 코드
	if ( system_5ms_Begin_Flag == 0 )
	{
		//1ms Interrupt Enable
		Gpt_StopTimer( GptConf_GptChannel_System1msTick_Core1 );
		Gpt_EnableNotification( GptConf_GptChannel_System1msTick_Core1 );
		Gpt_StartTimer( GptConf_GptChannel_System1msTick_Core1, 6250 );
		Gtm_SetTomCounterCn0(_1ms_IRQ_REF_TOM_Module_NUM, _1ms_IRQ_REF_TOM_Module_CH_NUM, 6247/*4750*/);
		//코드로서는 TOM_2_0 5ms 시작 하자마자 240us 이후에 1ms 인터럽트 Enable 할 수 있도록 설정

		system_5ms_Begin_Flag = 1;
	}
	
}
