/******************************************************************************
 | Project Name  : MGH-80
 | File   Name   : Brake_Main.c
 | Description   : FW Control
 |
 |------------------------------------------------------------------------------
 |               A U T H O R   I D E N T I T Y
 |------------------------------------------------------------------------------
 | Initials     Name                      Company
 | --------     ---------------------     --------------------------------------
 |------------------------------------------------------------------------------
 |               R E V I S I O N   H I S T O R Y
 |------------------------------------------------------------------------------
 | Date          Ver   Author   Description
 | ----------  ----  ------  ---------------------------------------------------
 | 1999-12-02    2.0     Hp    Creation;
 |                           > Starting with V2.0 to distinguish between HGMLAN V1.x
 |                           > and GMLAN > V2.x
 |*****************************************************************************/


/******************************************************************************/
/* Include files                                                              */
/******************************************************************************/
#include "Std_Types.h"
#include "Mando_Std_Types.h"
//#include "WTypedefs.h"
#include "IDB_Main.h"
#include "Brake_Main.h"

#include "DetErr.h"
#include "CDC_DiagService.h"
#include "AdcIf_Conv50us.h"
#include "Com.h"
#include "Icu_InputCapture.h"
#include "Wss_Sen.h"
#include "vx1000_tc2xx.h"
#include "Fsr_Actr.h"
#include "Task_1ms.h"
#include "Task_5ms.h"
#include "Task_50us.h"
#include "Task_10ms.h"
#include "TomTimerHandler.h"
#include "Pwm_17_Gtm.h"
#include "Gpt.h"
#include "Gtm.h"
#include "Prly_Hndlr.h"
#include "Swt_Sen.h"
#include "ErP.h"
//#include "NvM.h"
#include "NvMIf.h"
#include "Proxy_Logger.h"
#include "Dio.h"
/*
#if (TC27X_FEE == ENABLE) //KCLim_Fee : For TC27x Fee
#include "NvM.h"
#include "NvMIf.h"
#endif
#include "Brake_Main.h"
#include "Pwm_17_Gtm.h"
#include "TomTimerHandler.h"
#include "Adc.h"
#include "SpiCdd.h"
#include "IcuCdd.h"
#include "AsicM.h"
#include "RegM_TPS65381.h"
#include "CanTrcvM_TLE6251.h"
#include "GdM.h"
#include "VdrM.h"
#include "MpsM.h"
#include "PwrM.h"
#include "Motor.h"
#include "Valve.h"
#include "AdcIf.h"
#include "IoHwAb.h"
#include "Wss.h"
#include "ApplCom.h"
#include "EEM.h"

#include "DetErr.h"
#include "vx1000_tc2xx.h"

#if GMLAN_ENABLE==ENABLE
    #include "../Gmlan/Appl/ApplCanDesc.H"
    #include "../Gmlan/Appl/AppDiag.H"
    #include "../Gmlan/Appl/ApplCan.H"
    #include "../Gmlan/Appl/ApplChkCanMsg.H"
    #include "../Gmlan/Bsw/Ccl/ccl_inc.h"
#else
    #include "../Diag/CDC_DiagService.h"
    #include "../Diag/CDC_DiagHeader.h"
    #include "../Diag/CDS_ActControl.h"
#endif

#include "../../VIL/_PAR/version/version.par.h"

#include "../SAL/HDR/SAL.h"
#include "../Ses/HDR/Ses.h"
#include "../../../ASW/FAL/ActiveBrakeControl/HDR/ActiveBrakeControl.h"
#include "../../../ASW/FAL/BaseBrakeControl/HDR/BaseBrakeControl.h"
#include "../../../ASW/FAL/IDBDetection/HDR/IDBDetection.h"
#include "../../../ASW/FAL/IDBSignalProcessing/HDR/IDBSignalProcessing.h"
#include "../../../ASW/FAL/IDBTargetPressArbitrator/HDR/IDBTargetPressArbitrator.h"
#include "../../../ASW/FAL/IDBValveActuation/HDR/IDBValveActuation.h"
#include "../../../ASW/FAL/MotorCurrentControl/HDR/MotorCurrentControl.h"
#include "../../../ASW/FAL/PressureControl/HDR/PressureControl.h"
#include "../../../ASW/FAL/RegenBrakeCooperation/HDR/RegenBrakeCooperation.h"
#include "../MotorInverterControl/HDR/MotorInverterControl.h"

#include "ErP.h"
#include "Sw.h"
*/
#include "Gptm_Cdd.h"
#include "Wdg_17_Scu.h"
/******************************************************************************/
/* Defines                                                                    */
/******************************************************************************/
#define CYCLE_ERROR_MASK    0x01U


/***************************************************************************/
/* Macros                                                                  */
/***************************************************************************/
#if (__RAR == ENABLE)||(__IGNOFF_WSS_OUT == ENABLE)
/* RAR mode - 091012 */
#define RAR_CHECK_TIME  LOOPTIME_MIN(3)//(3 * 60 * 1000 / 5)    /* 3 min */
#endif /* #if __RAR == ENABLE */

#if (__AVH == ENABLE)
/* After IGN OFF AVH Control - 101012 */
#define AVH_CHECK_TIME  LOOPTIME_SEC(5)
#endif

/* off mode timeout 시 ECU 강제 shutdown 추가 */
#define OFFMODE_TIMEOUT_TIME    LOOPTIME_MIN(30)
#define IGN_BLS_DOOR_SHUTDOWN
#ifdef IGN_BLS_DOOR_SHUTDOWN
#define IGN_BLS_DOOR_OFF_TIMEOUT_TIME    LOOPTIME_SEC(5)//LOOPTIME_MIN(10) /* 양산 코드는 10분이나, 테스트용으로 5초로 세팅 */
#endif
#ifdef __FW_ONESHOT_FAULT_DETECT
    #define MAX_CYCLETIME_ERROR_COUNT   1
#else
    #define MAX_CYCLETIME_ERROR_COUNT   10
#endif

#ifdef __FW_ONESHOT_FAULT_DETECT
    #define MAX_1ms_TiMER_ERROR_COUNT   1
#else
    #define MAX_1ms_TiMER_ERROR_COUNT    50
#endif

#define INIT_START_TIME_END  0xFFFF

#define _5ms_IRQ_REF_TOM_Module_NUM		2
#define _5ms_IRQ_REF_TOM_Module_CH_NUM	15

/****************************************************************************/
/* Local Constants                                                          */
/****************************************************************************/


/******************************************************************************/
/* Data definitions                                                           */
/******************************************************************************/
uint16_t u16s_CurrentTime;
uint16_t wmu16CycleStartTime;
static uint16_t wmu16FixedCycleTime;
static uint16_t wmu16CycleOverTime;	/* HSH_COMPILE */
uint32_t wmu32DelayTime;
#ifndef __LOOP_TIME_10MS
uint16_t wmu16GenTimeCnt;
#endif
static uint32_t wmu32CycleOverTime;

t_u8Bit wu8LoopTimeDiv;
t_u8Bit wmu8MainContlFlg_0;
t_u8Bit wmu8MainContlFlg_1;

#if __FLEX_BRAKE == ENABLE
uint8_t  mbu8FlexBrakeMode;
#endif

#if (__NM_SLEEP_ENABLE==ENABLE)
uint16_t wmu16SysLoopTimer;
#endif

#ifdef IGN_BLS_SHUTDOWN
uint32_t wmu32IgnBlsTimeCounter;
#endif

uint8_t  wu8InitSysTime;        /* For loop 진입 전 시간(Loop time 횟수) */

uint8_t LOOP_TIME_10MS_TASK;
uint8_t LOOP_TIME_20MS_TASK_0;
uint8_t LOOP_TIME_20MS_TASK_1;


//#define du1LoopTime10ms         du8_looptime.B.bit0
t_u8Bit du8_looptime;

uint32_t wmu32CycleStartTime;
uint32_t IgnBlsDoorTimeCounter;
uint32_t BackUpShutdownTimeCounter;
uint8_t	BackUpTimeOutEnd;
uint8_t	BlsDoorTimeOutEnd;
uint8_t	EEPWriteCompleted;

/****************************************************************************/
/* Prototype                                                          */
/****************************************************************************/
void MB_vInitSys(void);
void MB_vPreProc(void);
void MB_vNormalMode(void);
void MB_vSleepMode(void);
void MB_vOffMode(void);
void MB_vEnterNormalMode(void);
void MB_vEnterIGNOnMode(void);
void MB_vEnterIGNOffMode(void);
void MB_vEnterSleepMode(void);
void WM_vGenLoopTime(void);
void WM_vCheckLoopTime(void);
void WM_vCheckCycleTime(void);
void WM_vStartLoopTime(void);

# if defined(__DURABILITY_TEST_ENABLE)
void WE_vWriteDuraTestData(void);
# endif
extern void cpu1_trap_0 (void);
void (* Func_ptr_1ms_Runnable[1])(void);
/****************************************************************************/
/* Functions                                                                */
/****************************************************************************/

/************************************************************************
* FUNCTION NAME:               MB_vInitSys()
* CALLED BY:          main
* Preconditions:      none 
* PARAMETER:          system_loop_counter
* RETURN VALUE:       none
* Description:        Initialization of registers,communication,eeprom...
*************************************************************************/
void MB_vInitSys(void)
{
	//Init Development error trace for IDB Firmware code
	DetErr_Init();
	Func_ptr_1ms_Runnable[0]=&cpu1_trap_0;
	//cpu1_trap_0();
	//AdcIf_Conv50us_Init();//AdcIf_Init(); 50us Adcif Init에 포함
	CDCS_vInitDiagOnCAN();	
#if (TC27X_FEE == ENABLE) // KCLim_Fee : For TC27x Fee

	//Wrapper function to call NvM_Init()
	NvMCdd_Init();

	//Loading Physical Fee data to RAM buffer
	NvM_ReadAll();

	WE_vInitEEP();

#endif //#if (TC27X_FEE == ENABLE)
    Can_Initialization();

	//Icu_InputCapture_Init();//IcuCdd_Init();
	//Wss_Sen_Init();//Wss_Init();

	VX1000_INIT();
    //Fsr_Actr_Init();//FsrM_Init();
    Task_50us_Init();
	Task_1ms_Init();
	Task_5ms_Init();
	Task_10ms_Init();
	//MpsD1_Gpt12Iif_CompensateAngle(); 
	
	/* Free Run Counter Set for Check cycle time */
	TOM_Timer_Initialization();

	Pwm_MandoGtmSetDutyCycleInit();		/* Pwm_MandoGtmSetDutyCycle(Modified API form Thejaswi)를 사용하기 위해서 꼭 필요 */
    
    /* 50us interrupt enable */
    Pwm_17_Gtm_EnableNotification(Pwm_17_GtmConf_PwmChannel_MTR_Center_REF_PWM, PWM_RISING_EDGE);

    /* 5ms Interrupt Setting */
    Gpt_StopTimer( GptConf_GptChannel_System5msTick_Core1 );
    Gpt_EnableNotification( GptConf_GptChannel_System5msTick_Core1 );
    Gpt_StartTimer( GptConf_GptChannel_System5msTick_Core1, 31250 );    /* 160ns * 31250 = 5ms */


	Gtm_SetTomCounterCn0(_5ms_IRQ_REF_TOM_Module_NUM, _5ms_IRQ_REF_TOM_Module_CH_NUM, 30850); /* 160ns 이후에 5ms 인터럽트 발생 시켜라 */

    ErP_Init();
	Wdg_17_Scu_SetMode(WDGIF_FAST_MODE);
    /*
	//Init Development error trace for IDB Firmware code
	DetErr_Init();

	AdcIf_Init();

	CDCS_vInitDiagOnCAN();

#if (TC27X_FEE == ENABLE) // KCLim_Fee : For TC27x Fee

	//Wrapper function to call NvM_Init()
	NvM_Init();

	//Loading Physical Fee data to RAM buffer
	NvM_ReadAll();

	WE_vInitEEP();

#endif //#if (TC27X_FEE == ENABLE)
	
	//WA_vInitValveParam();

    Can_Initialization();

	IcuCdd_Init();
	Wss_Init();

	VX1000_INIT();
    FsrM_Init();
	PwrM_Init();
    SpiCdd_Init();
    Valve_Init();
    AsicM_Init();
    MpsM_Init();
    GdM_Init();
    RegM_TPS65381_Init();
    CanTrcvM_TLE6251_Init();
    Motor_Init();
    Pedal_Init();
    Press_Init();
    Relay_Init();
    Sw_Init();
	EEM_Init();
    Ses_Init();
    Motor_ReqOn(MOTOR_CLIENT_LOGIC);
    SAL_BusInit();
    Rte_BusInit();
    ErP_Init();

    IDBSignalProcessing_Init();
    IdbDetection_Init();
    BaseBrakeControl_Init();
    RegenBrakeCooperation_Init();
    IDBTargetPressArbitrator_Init();
    PressureControl_Init();
    IDBValveActuation_Init();
    LAMTR_vInitMotIRefGenn1ms();
    LAMTR_vInitMotIRefGenn5ms();
    MotorInverterControl_Init();
    ActiveBrakeControl_Init();
	//Free Run Counter Set for Check cycle time
	TOM_Timer_Initialization();

	Pwm_MandoGtmSetDutyCycleInit();		//Pwm_MandoGtmSetDutyCycle(Modified API form Thejaswi)를 사용하기 위해서 꼭 필요
    
    //50us interrupt enable
    Pwm_17_Gtm_EnableNotification(Pwm_17_GtmConf_PwmChannel_MTR_Center_REF_PWM, PWM_RISING_EDGE);

    //5ms Interrupt Setting
    Gpt_StopTimer( GptConf_GptChannel_System5msTick_Core1 );
    Gpt_EnableNotification( GptConf_GptChannel_System5msTick_Core1 );
    Gpt_StartTimer( GptConf_GptChannel_System5msTick_Core1, 31250 );    //160ns * 31250 = 5ms


	Gtm_SetTomCounterCn0(_5ms_IRQ_REF_TOM_Module_NUM, _5ms_IRQ_REF_TOM_Module_CH_NUM, 30850); //60ns 이후에 5ms 인터럽트 발생 시켜라
*/
}

/************************************************************************
* FUNCTION NAME:               MB_vCommonModeFunc()
* CALLED BY:          main
* Preconditions:      none 
* PARAMETER:          system_loop_counter
* RETURN VALUE:       none
* Description:        Initialization of registers,communication,eeprom...
*************************************************************************/
void MB_vPreProc(void)
{

	if ( Prly_HndlrIgnEdgeSts == PRLY_RISINGEDGE)
    {
        MB_vEnterIGNOnMode();
    }
    else 
    {
		if ( Prly_HndlrIgnEdgeSts == PRLY_FALLINGEDGE)
		{
			MB_vEnterIGNOffMode();
		}
    }

	WM_vGenLoopTime();
	
#if (TC27X_FEE == ENABLE)
	WE_vApplProcessWER();
	//NvM_MainFunction(); 5ms 포함
#endif
	//FE_vWriteErrorEEPROM(); 5ms 포함

}


/************************************************************************
* FUNCTION NAME:               MB_vRunMode()
* CALLED BY:          main
* Preconditions:      none 
* PARAMETER:          system_loop_counter
* RETURN VALUE:       none
* Description:        Initialization of registers,communication,eeprom...
*************************************************************************/
void MB_vNormalMode(void)
{

    if (wmu1LoopTime20ms_0 == STATUS_ON)
    {
		Can_17_MCanP_MainFunction_BusOff();		//Bus Off 20ms 주기
    }
    if ( wmu1LoopTime10ms_1 == STATUS_ON )
    {
        Can_17_MCanP_MainFunction_Read();
    }
    Com_MainFunction();
    Can_17_MCanP_MainFunction_Write();
	Task_5ms();
	if ( wmu1LoopTime10ms_0 == STATUS_ON )
    {
		Task_10ms();
		LS_vCallLogData();
    }
	if ( wmu1LoopTime10ms_1 == STATUS_ON )
    {
		Task_10ms();
	}

    LS_vCallLogDataAHB();
    PROXY_StartLoggerTrans();
	CC_vSetDiagCan();
    CC_vTransDiagData();
    /*
    if(SALbus.MotOrgSetStInfo_st.IdbMotOrgSetFlg == 1)
    {
    	LSIDB_vCallMainSenSigProcessing();
    	LDIDB_vCallMainDetn();
    	LCBBC_vCallMain();
    	RgnBrkCopvCtrlMain();
    	RgnBrkCopnActRgnBrkP();
    	LCIDB_vCallMainTarPArbr();
    	LAIDB_vCallPressureCtrl();
    	LAIDB_vCallMainVlvActn();
    	LAMTR_vCallMotIRefGenn5ms();
    }

     //KCLim Diag
    if ( wmu1LoopTime10ms_0 == STATUS_ON )
    {
        if(SALbus.MotOrgSetStInfo_st.IdbMotOrgSetFlg == 1)
        {
    	   ActiveBrakeControl_MainFunction();
        }
        CDCS_vEnterDiagOnCAN();
    }
		//CC_vDecRxCanData();
        //CC_vLoadCanRxData();
        ApplComDecode();

    if ( wmu1LoopTime10ms_1 == STATUS_ON )
    {
		#if GMLAN_ENABLE==ENABLE
			CC_vILToAppRxMsgSet();
		#else
			ApplComEncode();
            LS_vCallLogData();
			//CC_vLoadCanTxData();
            #if __ECU==ESP_ECU_1
                //CC_vLoadSubCanTxData();
            #endif
			//CC_vStartCan();
			//CI_vTransMainCan();
            ApplCom_vChkCan();
		#endif
    }
	
    LS_vCallLogData();
    LS_vCallLogDataAHB();
    ApplCom_StartLoggerTrans();
    CC_vSetDiagCan();
    CC_vTransDiagData();
    */
}


/************************************************************************
* FUNCTION NAME:               MB_vSleepMode()
* CALLED BY:          main
* Preconditions:      none 
* PARAMETER:          system_loop_counter
* RETURN VALUE:       none
* Description:        sleep mode
*************************************************************************/
void MB_vSleepMode(void)
{
    /*output off*/
    /*initialize Failsafe variables*/ 
    /*fs_vOffValves();*/
    /*WA_vOffValves();*/
}

/************************************************************************
* FUNCTION NAME:               MB_vOffMode()
* CALLED BY:          main
* Preconditions:      none 
* PARAMETER:          system_loop_counter
* RETURN VALUE:       none
* Description:        Initialization of registers,communication,eeprom...
*************************************************************************/
void MB_vOffMode(void)
{
    
	Sw_StatusType Sw_BLS;
	Sw_StatusType Sw_DOOR_SW;

	Sw_BLS	= Swt_SenBlsSwt;
	Sw_DOOR_SW = Swt_SenDoorSwt;

    if ( ( Sw_BLS == SwOff )	// BLS Switch is not pushed
	  && ( Sw_DOOR_SW == SwOn ) ) // Door Status is closed
    {
       if (IgnBlsDoorTimeCounter > IGN_BLS_DOOR_OFF_TIMEOUT_TIME)	//CE OFF 상태에서 BLS, DOOR_SW 가 옆 조건이면 5s 후 Shutdown
       {
    	   BlsDoorTimeOutEnd = 1;
       }
       else
       {
    	   IgnBlsDoorTimeCounter++;
       }
    }
    else
    {
    	IgnBlsDoorTimeCounter = 0;
    }

	if (BlsDoorTimeOutEnd==1)
    {
		WE_vFeeWriteAll();
		NvM_WriteAll();
		Dio_WriteChannel(DIO_CHANNEL_14_8,STD_LOW);
	//IoHwAb_SetLow(IOHWAB_OUT_PORT_INHIBIT); 
}
	else
	{
		if(BackUpShutdownTimeCounter >= OFFMODE_TIMEOUT_TIME)	//SW Fail 시 30분 후 강제 Shutdown
		{
			BackUpTimeOutEnd = 1;
		}
		else
		{
			BackUpShutdownTimeCounter++;
		}

		if (BackUpTimeOutEnd==1)
		{
			WE_vFeeWriteAll();
			NvM_WriteAll();
			Dio_WriteChannel(DIO_CHANNEL_14_8,STD_LOW);
			//IoHwAb_SetLow(IOHWAB_OUT_PORT_INHIBIT); 
		}
	}
    
}


/************************************************************************
* FUNCTION NAME:               WM_vGenLoopTime()
* CALLED BY:          main
* Preconditions:      none 
* PARAMETER:          system_loop_counter
* RETURN VALUE:       none
* Description:        define 10ms loop,20ms loop
*************************************************************************/
void WM_vGenLoopTime(void)
{

#ifdef __LOOP_TIME_10MS
    //10ms loop
    wmu1LoopTime10ms_0 = 1;
    wmu1LoopTime10ms_1 = 1;
  
    //20ms loop
    wmu1LoopTime20ms_0 ^= 1;
    wmu1LoopTime20ms_2=wmu1LoopTime20ms_0;
    
    wmu1LoopTime20ms_1= ~ wmu1LoopTime20ms_0;
    wmu1LoopTime20ms_3=wmu1LoopTime20ms_1;
#else
    wmu1LoopTime5ms = 1;
     
     //10ms loop
    wmu1LoopTime10ms_0 ^= 1;
    wmu1LoopTime10ms_1 = (~wmu1LoopTime10ms_0);
  
  
    //20ms loop
    if((wmu16GenTimeCnt%4)==0)
    {
        wmu1LoopTime20ms_0=1;
        wmu1LoopTime20ms_1=0;
        wmu1LoopTime20ms_2=0;
        wmu1LoopTime20ms_3=0;
    }
    else if((wmu16GenTimeCnt%4)==1)
    {
        wmu1LoopTime20ms_0=0;
        wmu1LoopTime20ms_1=1;
        wmu1LoopTime20ms_2=0;
        wmu1LoopTime20ms_3=0;
    }
    else if((wmu16GenTimeCnt%4)==2)
    {
        wmu1LoopTime20ms_0=0;
        wmu1LoopTime20ms_1=0;
        wmu1LoopTime20ms_2=1;
        wmu1LoopTime20ms_3=0;
    }
    else
    {
        wmu1LoopTime20ms_0=0;
        wmu1LoopTime20ms_1=0;
        wmu1LoopTime20ms_2=0;
        wmu1LoopTime20ms_3=1;    
    }

    LOOP_TIME_10MS_TASK = wmu1LoopTime10ms_0;
	LOOP_TIME_20MS_TASK_0 = wmu1LoopTime20ms_0;
	LOOP_TIME_20MS_TASK_1 = wmu1LoopTime20ms_2;




    wmu16GenTimeCnt++;
#endif
   
}

/************************************************************************
* FUNCTION NAME:               WM_vCheckLoopTime
* CALLED BY:          main()
* Preconditions:      none 
* PARAMETER:          Handle of cycle time parameters
* RETURN VALUE:       none
* Description:        wait until 6.9msec, call WM_StartLoopTime
*************************************************************************/
void WM_vCheckLoopTime(void)
{

    uint16_t u16TempTime=0;
    wmu1CycleTimeOver=0;

#if defined(__LOGGER)
    // Tx Logger
    //ApplCom_StartLoggerTrans();
#endif

    // Infineon MCU의 경우 TOM Free running Timer가 16bit 이기 때문에 16ibt로 모두 Casting 변경 함 20140523 
    u16TempTime=(LOOP_TIME*1000);          // Next Loop Time Set : Loop Time * 1000 ( TCNT 가 usec 로 적용됨 )
    wmu16FixedCycleTime = wmu16CycleStartTime + (uint16_t)TIME_TICK_US(u16TempTime);
	
	if((int16_t)(wmu16FixedCycleTime-(uint16_t)CURRENT_TIME)>0)
    {
        while((int16_t)((uint16_t)(wmu16CycleStartTime + (uint16_t)TIME_TICK_US(u16TempTime))-(uint16_t)CURRENT_TIME)>=0)
        {
        	u16s_CurrentTime = (uint16_t)CURRENT_TIME;      // Loop Time만큼 Waiting 적용
        }
    }
    else
    {
        wmu32CycleOverTime=(uint16_t)CURRENT_TIME;
        wmu1CycleTimeOver=1;
    }

}

void WM_vCheckCycleTime(void)
{
    
    static uint8_t wu8CycleTime10PerErrCnt=0;
    static uint8_t wu8CycleTime30PerErrCnt=0;
    static uint8_t wmu8CycleErrState=0; 

	if(wmu1CycleTimeOver==1)
	{
		wmu16FixedCycleTime=(uint32_t)(wmu16FixedCycleTime-wmu16CycleOverTime);
		if((int32_t)wmu16FixedCycleTime<=(-(int32_t)TIME_TICK_US(500)))          // Loop Time에 10% 이상 10회 
		{
			if((int32_t)wmu16FixedCycleTime<=(-(int32_t)TIME_TICK_US(1500)))     // Loop Time에 30% 이상 누적 10회 
			{
				wu8CycleTime30PerErrCnt++;
				if(wu8CycleTime30PerErrCnt>=MAX_CYCLETIME_ERROR_COUNT)
				{
					wmu8CycleErrState= CYCLE_ERROR_MASK;
				}
			}
			else if(wu8CycleTime10PerErrCnt>=MAX_CYCLETIME_ERROR_COUNT)
			{
				wu8CycleTime10PerErrCnt++;
				wmu8CycleErrState= CYCLE_ERROR_MASK;
			}
			else
			{
				;
			}
		}
		else
		{
			if(wu8CycleTime10PerErrCnt!=0)
			{
				wu8CycleTime10PerErrCnt--;
			}
		}
	}
	else
	{
		if(wu8CycleTime10PerErrCnt!=0)
		{
			wu8CycleTime10PerErrCnt--;
		}
	}

	if((wmu8CycleErrState&CYCLE_ERROR_MASK)==CYCLE_ERROR_MASK)
	{
		wmu8CycleErrState&= (uint8_t)(~CYCLE_ERROR_MASK);
		wu8CycleTime10PerErrCnt=0;
		wu8CycleTime30PerErrCnt=0;
	}
    
}

/************************************************************************
* FUNCTION NAME:               WM_vStartLoopTime
* CALLED BY:          WM_vCheckLoopTime()
* Preconditions:      none 
* PARAMETER:          Handle of cycle time parameters
* RETURN VALUE:       none
* Description:        store current time and check cycle time.
*************************************************************************/
void WM_vStartLoopTime(void)
{

    // Store Loop Start Time
    wmu16CycleStartTime=(uint16_t)CURRENT_TIME;

    system_loop_counter++;
    
    // Check Cycle Time
    WM_vCheckCycleTime();
    
#if (__NM_SLEEP_ENABLE==ENABLE) 
    if(wmu16SysLoopTimer < INIT_START_TIME_END)
    {
        wmu16SysLoopTimer++;
    }
#endif

}


void MB_vEnterIGNOnMode(void)
{
    fu8mec = 0xFF; /* 0xFF means "Not Init" */
}

void MB_vEnterIGNOffMode(void)
{
	;
}

void MB_vEnterNormalMode(void)
{
	;
}

void MB_vEnterSleepMode(void)
{
	;
}

