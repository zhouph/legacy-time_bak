
/*******************************************************************************
 | Project Name: Kernel Implementation GMLAN
 |    File Name: gmnmdef.h
 |
 |  Description: Header file of GM/OPEL/SAAB GMLAN Network Management
 |
 |------------------------------------------------------------------------------
 |               A U T H O R   I D E N T I T Y
 |------------------------------------------------------------------------------
 | Initials     Name                      Company
 | --------     ---------------------     --------------------------------------
 | Eb           Volker Ebner              Vector Informatik GmbH
 |------------------------------------------------------------------------------
 |               R E V I S I O N   H I S T O R Y
 |------------------------------------------------------------------------------
 | Date         Ver   Author  Description
 | ----------  ----  -------  --------------------------------------------------
 | 12.02.1999  2.0     Hp    Creation;
 |                           Starting with V2.0 to distinguish between HGMLAN V1.x
 |                           and GMLAN > V2.x
 |*****************************************************************************/
#ifndef _WMAIN_H_
#define _WMAIN_H_

#include "../../LIB/Common.h"

/*******************************************************************************
 Export  global variables
*******************************************************************************/
extern uint16_t	wmu16CycleStartTime;

extern t_u8Bit wu8LoopTimeDiv;

extern t_u8Bit wmu8MainContlFlg_0, wmu8MainContlFlg_1;

extern uint8_t wu8InitSysTime;   /* For loop 진입 전 시간(Loop time 횟수) */
/* Control Logic 참조 변수 */
extern uint8_t LOOP_TIME_10MS_TASK;
extern uint8_t LOOP_TIME_20MS_TASK_0;
extern uint8_t LOOP_TIME_20MS_TASK_1;


#if (__NM_SLEEP_ENABLE==ENABLE)
extern uint16_t wmu16SysLoopTimer;
#endif

#if __FLEX_BRAKE == ENABLE
extern uint8_t  mbu8FlexBrakeMode;
#endif

#if (__IVSS == ENABLE)
extern uint8_t Ivss_SW_id[19];
#endif

extern uint32_t IgnBlsDoorTimeCounter;
extern uint32_t BackUpShutdownTimeCounter;

/*******************************************************************************
global data types and structures
*******************************************************************************/
#define wmu1LoopTime5ms         wu8LoopTimeDiv.B.bit0             
#define wmu1LoopTime10ms_0      wu8LoopTimeDiv.B.bit1           
#define wmu1LoopTime10ms_1      wu8LoopTimeDiv.B.bit2           
#define wmu1LoopTime20ms_0      wu8LoopTimeDiv.B.bit3           
#define wmu1LoopTime20ms_1      wu8LoopTimeDiv.B.bit4           
#define wmu1LoopTime20ms_2      wu8LoopTimeDiv.B.bit5          
#define wmu1LoopTime20ms_3      wu8LoopTimeDiv.B.bit6          

/* Control Logic 참조 변수 */
//#define LOOP_TIME_10MS_TASK             wmu1LoopTime10ms_0
//#define LOOP_TIME_20MS_TASK_0           wmu1LoopTime20ms_0
//#define LOOP_TIME_20MS_TASK_1           wmu1LoopTime20ms_2

#define wmu1CycleTimeOver       wmu8MainContlFlg_0.B.bit0
#define wmu1ErrCyclTime         wmu8MainContlFlg_0.B.bit1

#define wmu1NotChkCycleTime     wmu8MainContlFlg_1.B.bit0
#define wmu1ComplWEEEP          wmu8MainContlFlg_1.B.bit1
#define wmu1AvhOffCtrlEnd       wmu8MainContlFlg_1.B.bit2
#define wmu1RarModeEnd          wmu8MainContlFlg_1.B.bit3
#define wmu1AhbTimeOutEnd      wmu8MainContlFlg_1.B.bit4

/*******************************************************************************
global macros for export
*******************************************************************************/
#define CHECK_TIME_IN_LOOP(USEC) \
        while((int16_t)((wmu16CycleStartTime + TIME_TICK_US(USEC))-(uint16_t)CURRENT_TIME)>=0)

/******************************************************************************/
/* Typedef and Struct definitions                                             */
/******************************************************************************/
extern uint32_t wmu32CycleStartTime;


/*******************************************************************************
 Import  constants 
*******************************************************************************/

/*******************************************************************************
prototypes of export functions
*******************************************************************************/
extern void MB_vInitSys(void);
extern void MB_vPreProc(void);
extern void MB_vNormalMode(void);
extern void MB_vSleepMode(void);
extern void MB_vOffMode(void);
extern void MB_vEnterNormalMode(void);
extern void MB_vEnterIGNOnMode(void);
extern void MB_vEnterIGNOffMode(void);
extern void MB_vEnterSleepMode(void);
extern void WM_vGenLoopTime(void);
extern void WM_vCheckLoopTime(void);
extern void WM_vStartLoopTime(void);

#endif
/***** end of header file *****************************************************/
