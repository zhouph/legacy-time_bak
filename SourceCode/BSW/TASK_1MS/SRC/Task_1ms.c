/**
 * @defgroup Task_1ms Task_1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_1ms.h"
#include "Task_1ms_Ifa.h"
#include "Task_5ms.h"
#include "Task_10ms.h"
#include "Task_50us.h"
/* Include runnables mapped to this task */
#include "AdcIf_Conv1ms.h"
#include "Ach_Input.h"
#include "SentH_Main.h"
#include "Ioc_InputSR1ms.h"
#include "Ioc_InputCS1ms.h"
#include "CanTrv_TLE6251_Hndlr.h"
#include "Vlvd_A3944_Hndlr.h"
#include "Mps_TLE5012_Hndlr_1ms.h"
#include "Pedal_Sen.h"
#include "Press_Sen.h"
#include "Msp_1msCtrl.h"
#include "Spc_1msCtrl.h"
#include "Det_1msCtrl.h"
#include "Pct_1msCtrl.h"
#include "Mcc_1msCtrl.h"
#include "Ses_Ctrl.h"
#include "Vlv_Actr.h"
#include "Acm_Main.h"
#include "Aka_Main.h"
#include "Awd_main.h"
#include "Mgd_TLE9180_Hndlr.h"
#include "Ioc_OutputSR1ms.h"
#include "Ioc_OutputCS1ms.h"
#include "Ach_Output.h"
#include "Mtr_Processing_Diag.h"
#include "Arbitrator_Mtr.h"

#include "Spim_Cdd.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TASK_1MS_START_SEC_CONST_UNSPECIFIED
#include "Task_1ms_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define TASK_1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Task_1ms_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_1ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Task_1ms_HdrBusType Task_1msBus;

/*motor Internal flag*/
sint32 Int_IdbMotOrgSetFlg;
/* Version Info */
const SwcVersionInfo_t Task_1msVersionInfo = 
{
    TASK_1MS_MODULE_ID,       /* Task_1msVersionInfo.ModuleId */
    TASK_1MS_MAJOR_VERSION,   /* Task_1msVersionInfo.MajorVer */
    TASK_1MS_MINOR_VERSION,   /* Task_1msVersionInfo.MinorVer */
    TASK_1MS_PATCH_VERSION,   /* Task_1msVersionInfo.PatchVer */
    TASK_1MS_BRANCH_VERSION   /* Task_1msVersionInfo.BranchVer */
};

    
/* Input Data Element */
Msp_CtrlMotRotgAgSigInfo_t Task_1ms_Mcc_1msCtrlMotRotgAgSigInfo;
Vat_CtrlIdbVlvActInfo_t Task_1ms_Mcc_1msCtrlIdbVlvActInfo;
Pct_5msCtrlStkRecvryActnIfInfo_t Task_1ms_Mcc_1msCtrlStkRecvryActnIfInfo;
Acmctl_CtrlMotDqIMeasdInfo_t Task_1ms_Ses_CtrlMotDqIMeasdInfo;
Msp_CtrlMotRotgAgSigInfo_t Task_1ms_Ses_CtrlMotRotgAgSigInfo;
Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Task_1ms_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
Vlv_ActrSyncNormVlvReqInfo_t Task_1ms_Vlv_ActrNormVlvReqInfo;
Vlv_ActrSyncWhlVlvReqInfo_t Task_1ms_Vlv_ActrWhlVlvReqInfo;
Vlv_ActrSyncBalVlvReqInfo_t Task_1ms_Vlv_ActrBalVlvReqInfo;
Fsr_ActrFsrCbsDrvInfo_t Task_1ms_Ioc_OutputSR1msFsrCbsDrvInfo;
Fsr_ActrFsrAbsDrvInfo_t Task_1ms_Ioc_OutputSR1msFsrAbsDrvInfo;
Rly_ActrRlyDrvInfo_t Task_1ms_Ioc_OutputCS1msRlyDrvInfo;
Fsr_ActrFsrAbsDrvInfo_t Task_1ms_Ioc_OutputCS1msFsrAbsDrvInfo;
Fsr_ActrFsrCbsDrvInfo_t Task_1ms_Ioc_OutputCS1msFsrCbsDrvInfo;
SenPwrM_MainSenPwrMonitor_t Task_1ms_Ioc_OutputCS1msSenPwrMonitorData;
Arbitrator_VlvArbWhlVlvReqInfo_t Task_1ms_Mtr_Processing_DiagArbWhlVlvReqInfo;
Eem_MainEemFailData_t Task_1ms_Mtr_Processing_DiagEemFailData;
Eem_MainEemCtrlInhibitData_t Task_1ms_Mtr_Processing_DiagEemCtrlInhibitData;
Msp_CtrlMotRotgAgSigInfo_t Task_1ms_Mtr_Processing_DiagMotRotgAgSigInfo;
Wss_SenWhlSpdInfo_t Task_1ms_Mtr_Processing_DiagWhlSpdInfo;
Diag_HndlrDiagHndlRequest_t Task_1ms_Mtr_Processing_DiagDiagHndlRequest;
Mtr_Processing_SigMtrProcessOutInfo_t Task_1ms_Mtr_Processing_DiagMtrProcessOutInfo;
Eem_MainEemFailData_t Task_1ms_Arbitrator_MtrEemFailData;
Eem_MainEemCtrlInhibitData_t Task_1ms_Arbitrator_MtrEemCtrlInhibitData;
Mom_HndlrEcuModeSts_t Task_1ms_AdcIf_Conv1msEcuModeSts;
Eem_SuspcDetnFuncInhibitAdcifSts_t Task_1ms_AdcIf_Conv1msFuncInhibitAdcifSts;
Mom_HndlrEcuModeSts_t Task_1ms_SentH_MainEcuModeSts;
Mom_HndlrEcuModeSts_t Task_1ms_Ioc_InputSR1msEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_InputSR1msFuncInhibitIocSts;
Mom_HndlrEcuModeSts_t Task_1ms_Ioc_InputCS1msEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_InputCS1msFuncInhibitIocSts;
Mom_HndlrEcuModeSts_t Task_1ms_CanTrv_TLE6251_HndlrEcuModeSts;
Eem_SuspcDetnFuncInhibitCanTrcvSts_t Task_1ms_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts;
Mom_HndlrEcuModeSts_t Task_1ms_Vlvd_A3944_HndlrEcuModeSts;
Eem_SuspcDetnFuncInhibitVlvdSts_t Task_1ms_Vlvd_A3944_HndlrFuncInhibitVlvdSts;
Mom_HndlrEcuModeSts_t Task_1ms_Mps_TLE5012_Hndlr_1msEcuModeSts;
Mom_HndlrEcuModeSts_t Task_1ms_Pedal_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitPedalSts_t Task_1ms_Pedal_SenFuncInhibitPedalSts;
Mom_HndlrEcuModeSts_t Task_1ms_Press_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitPressSts_t Task_1ms_Press_SenFuncInhibitPressSts;
Acmio_SenVdcLink_t Task_1ms_Msp_1msCtrlVdcLink;
Mom_HndlrEcuModeSts_t Task_1ms_Spc_1msCtrlEcuModeSts;
Mom_HndlrEcuModeSts_t Task_1ms_Det_1msCtrlEcuModeSts;
Mom_HndlrEcuModeSts_t Task_1ms_Pct_1msCtrlEcuModeSts;
Pct_5msCtrlPCtrlSt_t Task_1ms_Pct_1msCtrlPCtrlSt;
Mom_HndlrEcuModeSts_t Task_1ms_Mcc_1msCtrlEcuModeSts;
Eem_SuspcDetnFuncInhibitMccSts_t Task_1ms_Mcc_1msCtrlFuncInhibitMccSts;
Pct_5msCtrlTgtDeltaStkType_t Task_1ms_Mcc_1msCtrlTgtDeltaStkType;
Pct_5msCtrlPCtrlBoostMod_t Task_1ms_Mcc_1msCtrlPCtrlBoostMod;
Pct_5msCtrlPCtrlFadeoutSt_t Task_1ms_Mcc_1msCtrlPCtrlFadeoutSt;
Pct_5msCtrlPCtrlSt_t Task_1ms_Mcc_1msCtrlPCtrlSt;
Pct_5msCtrlInVlvAllCloseReq_t Task_1ms_Mcc_1msCtrlInVlvAllCloseReq;
Pct_5msCtrlPChamberVolume_t Task_1ms_Mcc_1msCtrlPChamberVolume;
Pct_5msCtrlTarDeltaStk_t Task_1ms_Mcc_1msCtrlTarDeltaStk;
Pct_5msCtrlFinalTarPFromPCtrl_t Task_1ms_Mcc_1msCtrlFinalTarPFromPCtrl;
Mom_HndlrEcuModeSts_t Task_1ms_Ses_CtrlEcuModeSts;
Eem_SuspcDetnFuncInhibitSesSts_t Task_1ms_Ses_CtrlFuncInhibitSesSts;
Mom_HndlrEcuModeSts_t Task_1ms_Vlv_ActrEcuModeSts;
Eem_SuspcDetnFuncInhibitVlvSts_t Task_1ms_Vlv_ActrFuncInhibitVlvSts;
Vlv_ActrSyncVlvSync_t Task_1ms_Vlv_ActrVlvSync;
Prly_HndlrPrlyEcuInhibit_t Task_1ms_Aka_MainPrlyEcuInhibit;
Mom_HndlrEcuModeSts_t Task_1ms_Mgd_TLE9180_HndlrEcuModeSts;
Mom_HndlrEcuModeSts_t Task_1ms_Ioc_OutputSR1msEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_OutputSR1msFuncInhibitIocSts;
Mom_HndlrEcuModeSts_t Task_1ms_Ioc_OutputCS1msEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_OutputCS1msFuncInhibitIocSts;
Fsr_ActrFsrDcMtrShutDwn_t Task_1ms_Ioc_OutputCS1msFsrDcMtrShutDwn;
Fsr_ActrFsrEnDrDrv_t Task_1ms_Ioc_OutputCS1msFsrEnDrDrv;
Mom_HndlrEcuModeSts_t Task_1ms_Arbitrator_MtrEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_1ms_Arbitrator_MtrIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_1ms_Arbitrator_MtrIgnEdgeSts;
Diag_HndlrDiagSci_t Task_1ms_Arbitrator_MtrDiagSci;
Diag_HndlrDiagAhbSci_t Task_1ms_Arbitrator_MtrDiagAhbSci;

/* Output Data Element */
AdcIf_Conv1msSwTrigPwrInfo_t Task_1ms_AdcIf_Conv1msSwTrigPwrInfo;
AdcIf_Conv1msSwTrigMotInfo_t Task_1ms_AdcIf_Conv1msSwTrigMotInfo;
Ach_InputAchSysPwrAsicInfo_t Task_1ms_Ach_InputAchSysPwrAsicInfo;
Ach_InputAchValveAsicInfo_t Task_1ms_Ach_InputAchValveAsicInfo;
Ach_InputAchWssPort0AsicInfo_t Task_1ms_Ach_InputAchWssPort0AsicInfo;
Ach_InputAchWssPort1AsicInfo_t Task_1ms_Ach_InputAchWssPort1AsicInfo;
Ach_InputAchWssPort2AsicInfo_t Task_1ms_Ach_InputAchWssPort2AsicInfo;
Ach_InputAchWssPort3AsicInfo_t Task_1ms_Ach_InputAchWssPort3AsicInfo;
SentH_MainSentHPressureInfo_t Task_1ms_SentH_MainSentHPressureInfo;
Ioc_InputSR1msHalPressureInfo_t Task_1ms_Ioc_InputSR1msHalPressureInfo;
Ioc_InputSR1msMotMonInfo_t Task_1ms_Ioc_InputSR1msMotMonInfo;
Ioc_InputSR1msMotVoltsMonInfo_t Task_1ms_Ioc_InputSR1msMotVoltsMonInfo;
Ioc_InputSR1msPedlSigMonInfo_t Task_1ms_Ioc_InputSR1msPedlSigMonInfo;
Ioc_InputSR1msPedlPwrMonInfo_t Task_1ms_Ioc_InputSR1msPedlPwrMonInfo;
Ioc_InputCS1msSwtMonInfo_t Task_1ms_Ioc_InputCS1msSwtMonInfo;
Ioc_InputCS1msSwtMonInfoEsc_t Task_1ms_Ioc_InputCS1msSwtMonInfoEsc;
Ioc_InputCS1msRlyMonInfo_t Task_1ms_Ioc_InputCS1msRlyMonInfo;
Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Task_1ms_Vlvd_A3944_HndlrVlvdFF0DcdInfo;
Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Task_1ms_Vlvd_A3944_HndlrVlvdFF1DcdInfo;
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Task_1ms_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Task_1ms_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
Pedal_SenPdtBufInfo_t Task_1ms_Pedal_SenPdtBufInfo;
Pedal_SenPdfBufInfo_t Task_1ms_Pedal_SenPdfBufInfo;
Press_SenCircPBufInfo_t Task_1ms_Press_SenCircPBufInfo;
Press_SenPistPBufInfo_t Task_1ms_Press_SenPistPBufInfo;
Press_SenPspBufInfo_t Task_1ms_Press_SenPspBufInfo;
Press_SenPressCalcInfo_t Task_1ms_Press_SenPressCalcInfo;
Spc_1msCtrlPedlTrvlFild1msInfo_t Task_1ms_Spc_1msCtrlPedlTrvlFild1msInfo;
Det_1msCtrlBrkPedlStatus1msInfo_t Task_1ms_Det_1msCtrlBrkPedlStatus1msInfo;
Det_1msCtrlPedlTrvlRate1msInfo_t Task_1ms_Det_1msCtrlPedlTrvlRate1msInfo;
Mcc_1msCtrlMotDqIRefMccInfo_t Task_1ms_Mcc_1msCtrlMotDqIRefMccInfo;
Mcc_1msCtrlIdbMotPosnInfo_t Task_1ms_Mcc_1msCtrlIdbMotPosnInfo;
Mcc_1msCtrlFluxWeakengStInfo_t Task_1ms_Mcc_1msCtrlFluxWeakengStInfo;
Ses_CtrlNormVlvReqSesInfo_t Task_1ms_Ses_CtrlNormVlvReqSesInfo;
Ses_CtrlWhlVlvReqSesInfo_t Task_1ms_Ses_CtrlWhlVlvReqSesInfo;
Ses_CtrlMotOrgSetStInfo_t Task_1ms_Ses_CtrlMotOrgSetStInfo;
Ses_CtrlMotDqIRefSesInfo_t Task_1ms_Ses_CtrlMotDqIRefSesInfo;
Ses_CtrlPwrPistStBfMotOrgSetInfo_t Task_1ms_Ses_CtrlPwrPistStBfMotOrgSetInfo;
Mgd_TLE9180_HndlrMgdDcdInfo_t Task_1ms_Mgd_TLE9180_HndlrMgdDcdInfo;
Mtr_Processing_DiagMtrProcessDataInfo_t Task_1ms_Mtr_Processing_DiagMtrProcessDataInf;
Arbitrator_MtrMtrArbitratorData_t Task_1ms_Arbitrator_MtrMtrArbtratorData;
Arbitrator_MtrMtrArbitratorInfo_t Task_1ms_Arbitrator_MtrMtrArbtratorInfo;
AdcIf_Conv1msAdcInvalid_t Task_1ms_AdcIf_Conv1msAdcInvalid;
Ach_InputAchAsicInvalidInfo_t Task_1ms_Ach_InputAchAsicInvalid;
Ioc_InputSR1msVBatt1Mon_t Task_1ms_Ioc_InputSR1msVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Task_1ms_Ioc_InputSR1msVBatt2Mon;
Ioc_InputSR1msFspCbsHMon_t Task_1ms_Ioc_InputSR1msFspCbsHMon;
Ioc_InputSR1msFspCbsMon_t Task_1ms_Ioc_InputSR1msFspCbsMon;
Ioc_InputSR1msFspAbsHMon_t Task_1ms_Ioc_InputSR1msFspAbsHMon;
Ioc_InputSR1msFspAbsMon_t Task_1ms_Ioc_InputSR1msFspAbsMon;
Ioc_InputSR1msExt5vMon_t Task_1ms_Ioc_InputSR1msExt5vMon;
Ioc_InputSR1msCEMon_t Task_1ms_Ioc_InputSR1msCEMon;
Ioc_InputSR1msVdd1Mon_t Task_1ms_Ioc_InputSR1msVdd1Mon;
Ioc_InputSR1msVdd2Mon_t Task_1ms_Ioc_InputSR1msVdd2Mon;
Ioc_InputSR1msVdd3Mon_t Task_1ms_Ioc_InputSR1msVdd3Mon;
Ioc_InputSR1msVdd4Mon_t Task_1ms_Ioc_InputSR1msVdd4Mon;
Ioc_InputSR1msVdd5Mon_t Task_1ms_Ioc_InputSR1msVdd5Mon;
Ioc_InputSR1msVddMon_t Task_1ms_Ioc_InputSR1msVddMon;
Ioc_InputSR1msCspMon_t Task_1ms_Ioc_InputSR1msCspMon;
Ioc_InputCS1msRsmDbcMon_t Task_1ms_Ioc_InputCS1msRsmDbcMon;
Vlvd_A3944_HndlrVlvdInvalid_t Task_1ms_Vlvd_A3944_HndlrVlvdInvalid;
Mps_TLE5012_Hndlr_1msMpsInvalid_t Task_1ms_Mps_TLE5012_Hndlr_1msMpsInvalid;
Msp_1msCtrlVdcLinkFild_t Task_1ms_Msp_1msCtrlVdcLinkFild;
Pct_1msCtrlPreBoostMod_t Task_1ms_Pct_1msCtrlPreBoostMod;
Mcc_1msCtrlMotCtrlMode_t Task_1ms_Mcc_1msCtrlMotCtrlMode;
Mcc_1msCtrlMotCtrlState_t Task_1ms_Mcc_1msCtrlMotCtrlState;
Mcc_1msCtrlMotICtrlFadeOutState_t Task_1ms_Mcc_1msCtrlMotICtrlFadeOutState;
Mcc_1msCtrlStkRecvryStabnEndOK_t Task_1ms_Mcc_1msCtrlStkRecvryStabnEndOK;
Acm_MainAcmAsicInitCompleteFlag_t Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
Mgd_TLE9180_HndlrMgdInvalid_t Task_1ms_Mgd_TLE9180_HndlrMgdInvalid;
Mtr_Processing_DiagMtrDiagState_t Task_1ms_Mtr_Processing_DiagMtrDiagState;
Arbitrator_MtrMtrArbDriveState_t Task_1ms_Arbitrator_MtrMtrArbDriveState;

#define TASK_1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_1ms_MemMap.h"
#define TASK_1MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_1ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_1ms_MemMap.h"
#define TASK_1MS_START_SEC_VAR_UNSPECIFIED
#include "Task_1ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_1ms_MemMap.h"
#define TASK_1MS_START_SEC_VAR_32BIT
#include "Task_1ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_1MS_STOP_SEC_VAR_32BIT
#include "Task_1ms_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_1ms_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TASK_1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_1ms_MemMap.h"
#define TASK_1MS_START_SEC_VAR_NOINIT_32BIT
#include "Task_1ms_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_1ms_MemMap.h"
#define TASK_1MS_START_SEC_VAR_UNSPECIFIED
#include "Task_1ms_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Task_1ms_MemMap.h"
#define TASK_1MS_START_SEC_VAR_32BIT
#include "Task_1ms_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_1MS_STOP_SEC_VAR_32BIT
#include "Task_1ms_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TASK_1MS_START_SEC_CODE
#include "Task_1ms_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Task_1ms_Init(void)
{    
	Spim_Cdd_Init();
    AdcIf_Conv1ms_Init();
    Ach_Input_Init();
    SentH_Main_Init();
    Ioc_InputSR1ms_Init();
    Ioc_InputCS1ms_Init();
    CanTrv_TLE6251_Hndlr_Init();
    Vlvd_A3944_Hndlr_Init();
    Mgd_TLE9180_Hndlr_Init();
    Mps_TLE5012_Hndlr_1ms_Init();
    Pedal_Sen_Init();
    Press_Sen_Init();
    Msp_1msCtrl_Init();
    Spc_1msCtrl_Init();
    Det_1msCtrl_Init();
    Pct_1msCtrl_Init();
    Mcc_1msCtrl_Init();
    Ses_Ctrl_Init();
    Vlv_Actr_Init();
    Acm_Main_Init();
    Aka_Main_Init();
    Awd_main_Init();
    Ioc_OutputSR1ms_Init();
    Ioc_OutputCS1ms_Init();
    Ach_Output_Init();
    Mtr_Processing_Diag_Init();
    Arbitrator_Mtr_Init();
    
    /* Initialize internal bus */
}

void Task_1ms(void)
{
    uint16 i;
    /* "Read Interfaces" from <other Task> or <ISR Event>
    Assembly connection event between Tasks or Task and ISR
    for example #1) Task1msInterface1 = Task10msInterface1;
    for example #2) Task1msInterface1.Signal1 = Task10msInterface1.Signal1;
    for example #3) In case of Queue at <Shared RAM>, Task1msInterface1 = deQueue(&q);
    Therefore, it's possible to avoid memory violation.*/
    /* Sample interface */
    /* Structure interface */
    Task_1ms_Mcc_1msCtrlMotRotgAgSigInfo = Task_50us_Msp_CtrlMotRotgAgSigInfo;
    Task_1ms_Mcc_1msCtrlIdbVlvActInfo = Task_5ms_Vat_CtrlIdbVlvActInfo;
    Task_1ms_Mcc_1msCtrlStkRecvryActnIfInfo = Task_5ms_Pct_5msCtrlStkRecvryActnIfInfo;
    Task_1ms_Ses_CtrlMotDqIMeasdInfo = Task_50us_Acmctl_CtrlMotDqIMeasdInfo;
    Task_1ms_Ses_CtrlMotRotgAgSigInfo = Task_50us_Msp_CtrlMotRotgAgSigInfo;
    Task_1ms_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo = Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;
    Task_1ms_Vlv_ActrNormVlvReqInfo = Task_5ms_Vlv_ActrSyncNormVlvReqInfo;
    Task_1ms_Vlv_ActrWhlVlvReqInfo = Task_5ms_Vlv_ActrSyncWhlVlvReqInfo;
    Task_1ms_Vlv_ActrBalVlvReqInfo = Task_5ms_Vlv_ActrSyncBalVlvReqInfo;
    Task_1ms_Ioc_OutputSR1msFsrCbsDrvInfo = Task_5ms_Fsr_ActrFsrCbsDrvInfo;
    Task_1ms_Ioc_OutputSR1msFsrAbsDrvInfo = Task_5ms_Fsr_ActrFsrAbsDrvInfo;
    Task_1ms_Ioc_OutputCS1msRlyDrvInfo = Task_5ms_Rly_ActrRlyDrvInfo;
    Task_1ms_Ioc_OutputCS1msFsrAbsDrvInfo = Task_5ms_Fsr_ActrFsrAbsDrvInfo;
    Task_1ms_Ioc_OutputCS1msFsrCbsDrvInfo = Task_5ms_Fsr_ActrFsrCbsDrvInfo;
    Task_1ms_Ioc_OutputCS1msSenPwrMonitorData = Task_10ms_SenPwrM_MainSenPwrMonitorData;
    Task_1ms_Mtr_Processing_DiagArbWhlVlvReqInfo = Task_5ms_Arbitrator_VlvArbWhlVlvReqInfo;
    Task_1ms_Mtr_Processing_DiagEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_1ms_Mtr_Processing_DiagEemCtrlInhibitData = Task_5ms_Eem_MainEemCtrlInhibitData;
    Task_1ms_Mtr_Processing_DiagMotRotgAgSigInfo = Task_50us_Msp_CtrlMotRotgAgSigInfo;
    Task_1ms_Mtr_Processing_DiagWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_1ms_Mtr_Processing_DiagDiagHndlRequest = Task_10ms_Diag_HndlrDiagHndlRequest;
    Task_1ms_Mtr_Processing_DiagMtrProcessOutInfo = Task_50us_Mtr_Processing_SigMtrProcessOutInfo;
    Task_1ms_Arbitrator_MtrEemFailData = Task_5ms_Eem_MainEemFailData;
    //Task_1ms_Arbitrator_MtrEemCtrlInhibitData = Task_5ms_Eem_MainEemCtrlInhibitData;

    /* Single interface */
  /*  Task_1ms_AdcIf_Conv1msEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_AdcIf_Conv1msFuncInhibitAdcifSts = Task_5ms_Eem_SuspcDetnFuncInhibitAdcifSts;
    Task_1ms_SentH_MainEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ioc_InputSR1msEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ioc_InputSR1msFuncInhibitIocSts = Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
    Task_1ms_Ioc_InputCS1msEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ioc_InputCS1msFuncInhibitIocSts = Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
    Task_1ms_CanTrv_TLE6251_HndlrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = Task_5ms_Eem_SuspcDetnFuncInhibitCanTrcvSts;
    Task_1ms_Vlvd_A3944_HndlrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Vlvd_A3944_HndlrFuncInhibitVlvdSts = Task_5ms_Eem_SuspcDetnFuncInhibitVlvdSts;
    Task_1ms_Mps_TLE5012_Hndlr_1msEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Pedal_SenEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Pedal_SenFuncInhibitPedalSts = Task_5ms_Eem_SuspcDetnFuncInhibitPedalSts;
    Task_1ms_Press_SenEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Press_SenFuncInhibitPressSts = Task_5ms_Eem_SuspcDetnFuncInhibitPressSts;*/
    Task_1ms_Msp_1msCtrlVdcLink = Task_50us_Acmio_SenVdcLink;
    /*Task_1ms_Spc_1msCtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Det_1msCtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Pct_1msCtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;*/
    Task_1ms_Pct_1msCtrlPCtrlSt = Task_5ms_Pct_5msCtrlPCtrlSt;
    /*Task_1ms_Mcc_1msCtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Mcc_1msCtrlFuncInhibitMccSts = Task_5ms_Eem_SuspcDetnFuncInhibitMccSts;*/
    Task_1ms_Mcc_1msCtrlTgtDeltaStkType = Task_5ms_Pct_5msCtrlTgtDeltaStkType;
    Task_1ms_Mcc_1msCtrlPCtrlBoostMod = Task_5ms_Pct_5msCtrlPCtrlBoostMod;
    Task_1ms_Mcc_1msCtrlPCtrlFadeoutSt = Task_5ms_Pct_5msCtrlPCtrlFadeoutSt;
    Task_1ms_Mcc_1msCtrlPCtrlSt = Task_5ms_Pct_5msCtrlPCtrlSt;
    Task_1ms_Mcc_1msCtrlInVlvAllCloseReq = Task_5ms_Pct_5msCtrlInVlvAllCloseReq;
    Task_1ms_Mcc_1msCtrlPChamberVolume = Task_5ms_Pct_5msCtrlPChamberVolume;
    Task_1ms_Mcc_1msCtrlTarDeltaStk = Task_5ms_Pct_5msCtrlTarDeltaStk;
    Task_1ms_Mcc_1msCtrlFinalTarPFromPCtrl = Task_5ms_Pct_5msCtrlFinalTarPFromPCtrl;
   /* Task_1ms_Ses_CtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ses_CtrlFuncInhibitSesSts = Task_5ms_Eem_SuspcDetnFuncInhibitSesSts;
    Task_1ms_Vlv_ActrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Vlv_ActrFuncInhibitVlvSts = Task_5ms_Eem_SuspcDetnFuncInhibitVlvSts;*/
    Task_1ms_Vlv_ActrVlvSync = Task_5ms_Vlv_ActrSyncVlvSync;
   /* Task_1ms_Aka_MainPrlyEcuInhibit = Task_5ms_Prly_HndlrPrlyEcuInhibit;
    Task_1ms_Mgd_TLE9180_HndlrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ioc_OutputSR1msEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ioc_OutputSR1msFuncInhibitIocSts = Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
    Task_1ms_Ioc_OutputCS1msEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Ioc_OutputCS1msFuncInhibitIocSts = Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;*/
    Task_1ms_Ioc_OutputCS1msFsrDcMtrShutDwn = Task_5ms_Fsr_ActrFsrDcMtrShutDwn;
    Task_1ms_Ioc_OutputCS1msFsrEnDrDrv = Task_5ms_Fsr_ActrFsrEnDrDrv;
    //Task_1ms_Arbitrator_MtrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_1ms_Arbitrator_MtrIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_1ms_Arbitrator_MtrIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    Task_1ms_Arbitrator_MtrDiagSci = Task_10ms_Diag_HndlrDiagSci;
    Task_1ms_Arbitrator_MtrDiagAhbSci = Task_10ms_Diag_HndlrDiagAhbSci;

    /* End of Sample interface

    /* Input */
    /* Structure interface */
    /* Decomposed structure interface */
    Task_1ms_Read_Mcc_1msCtrlMotRotgAgSigInfo_StkPosnMeasd(&Task_1msBus.Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd);
    Task_1ms_Read_Mcc_1msCtrlMotRotgAgSigInfo_MotMechAngleSpdFild(&Task_1msBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild);

    /* Decomposed structure interface */
    Task_1ms_Read_Mcc_1msCtrlIdbVlvActInfo_FadeoutSt2EndOk(&Task_1msBus.Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk);

    /* Decomposed structure interface */
    Task_1ms_Read_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryCtrlState(&Task_1msBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState);
    Task_1ms_Read_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryRequestedTime(&Task_1msBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime);

    Task_1ms_Read_Ses_CtrlMotDqIMeasdInfo(&Task_1msBus.Ses_CtrlMotDqIMeasdInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotDqIMeasdInfo 
     : Ses_CtrlMotDqIMeasdInfo.MotIqMeasd;
     : Ses_CtrlMotDqIMeasdInfo.MotIdMeasd;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_1ms_Read_Ses_CtrlMotRotgAgSigInfo_StkPosnMeasd(&Task_1msBus.Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd);

    Task_1ms_Read_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo(&Task_1msBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotRotgAgSigBfMotOrgSetInfo 
     : Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
     : Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
     =============================================================================*/

    Task_1ms_Read_Vlv_ActrNormVlvReqInfo(&Task_1msBus.Vlv_ActrNormVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrNormVlvReqInfo 
     : Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrNormVlvReqInfo.SimVlvReqData;
     =============================================================================*/

    Task_1ms_Read_Vlv_ActrWhlVlvReqInfo(&Task_1msBus.Vlv_ActrWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrWhlVlvReqInfo 
     : Vlv_ActrWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    Task_1ms_Read_Vlv_ActrBalVlvReqInfo(&Task_1msBus.Vlv_ActrBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrBalVlvReqInfo 
     : Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData;
     : Vlv_ActrBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_1ms_Read_Ioc_OutputSR1msFsrCbsDrvInfo_FsrCbsDrv(&Task_1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv);

    /* Decomposed structure interface */
    Task_1ms_Read_Ioc_OutputSR1msFsrAbsDrvInfo_FsrAbsDrv(&Task_1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv);

    Task_1ms_Read_Ioc_OutputCS1msRlyDrvInfo(&Task_1msBus.Ioc_OutputCS1msRlyDrvInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msRlyDrvInfo 
     : Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv;
     : Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_1ms_Read_Ioc_OutputCS1msFsrAbsDrvInfo_FsrAbsOff(&Task_1msBus.Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff);

    /* Decomposed structure interface */
    Task_1ms_Read_Ioc_OutputCS1msFsrCbsDrvInfo_FsrCbsOff(&Task_1msBus.Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff);

    /* Decomposed structure interface */
    Task_1ms_Read_Ioc_OutputCS1msSenPwrMonitorData_SenPwrM_12V_Drive_Req(&Task_1msBus.Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req);

    Task_1ms_Read_Mtr_Processing_DiagArbWhlVlvReqInfo(&Task_1msBus.Mtr_Processing_DiagArbWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagArbWhlVlvReqInfo 
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_1ms_Read_Mtr_Processing_DiagEemFailData_Eem_Fail_Motor(&Task_1msBus.Mtr_Processing_DiagEemFailData.Eem_Fail_Motor);

    /* Decomposed structure interface */
    Task_1ms_Read_Mtr_Processing_DiagEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(&Task_1msBus.Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail);

    /* Decomposed structure interface */
    Task_1ms_Read_Mtr_Processing_DiagMotRotgAgSigInfo_MotMechAngleSpdFild(&Task_1msBus.Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild);

    Task_1ms_Read_Mtr_Processing_DiagWhlSpdInfo(&Task_1msBus.Mtr_Processing_DiagWhlSpdInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagWhlSpdInfo 
     : Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Task_1ms_Read_Mtr_Processing_DiagDiagHndlRequest(&Task_1msBus.Mtr_Processing_DiagDiagHndlRequest);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagDiagHndlRequest 
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_1ms_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhUMeasd(&Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd);
    Task_1ms_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhVMeasd(&Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd);
    Task_1ms_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotCurrPhWMeasd(&Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd);
    Task_1ms_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle1_1_100Deg(&Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg);
    Task_1ms_Read_Mtr_Processing_DiagMtrProcessOutInfo_MotPosiAngle2_1_100Deg(&Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg);

    /* Decomposed structure interface */
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_BBSSol(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSSol);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCSol(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSol);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_FrontSol(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_FrontSol);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_RearSol(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_RearSol);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_Motor(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Motor);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_MPS(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MPS);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_MGD(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MGD);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_BBSValveRelay(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCValveRelay(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_ECUHw(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ECUHw);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_ASIC(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ASIC);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_OverVolt(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_OverVolt);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_UnderVolt(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_LowVolt(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_LowVolt);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_LowerVolt(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_12V(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_5V(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_Yaw(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Yaw);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_Ay(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Ay);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_Ax(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Ax);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_Str(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Str);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_CirP1(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP1);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_CirP2(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP2);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_SimP(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SimP);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_BLS(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BLS);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCSw(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSw);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_HDCSw(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCSw);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_AVHSw(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_AVHSw);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_BrakeLampRelay(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_EssRelay(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_EssRelay);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_GearR(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_GearR);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_Clutch(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Clutch);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_ParkBrake(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDT(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDF(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_BrakeFluid(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_TCSTemp(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_HDCTemp(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_SCCTemp(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_TVBBTemp(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_MainCanLine(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_SubCanLine(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_EMSTimeOut(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_FWDTimeOut(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_TCUTimeOut(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_HCUTimeOut(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_MCUTimeOut(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut);
    Task_1ms_Read_Arbitrator_MtrEemFailData_Eem_Fail_VariantCoding(&Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding);

    Task_1ms_Read_Arbitrator_MtrEemCtrlInhibitData(&Task_1msBus.Arbitrator_MtrEemCtrlInhibitData);
    /*==============================================================================
    * Members of structure Arbitrator_MtrEemCtrlInhibitData 
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/

    /* Single interface */
  /*  Task_1ms_Read_AdcIf_Conv1msEcuModeSts(&Task_1msBus.AdcIf_Conv1msEcuModeSts);
    Task_1ms_Read_AdcIf_Conv1msFuncInhibitAdcifSts(&Task_1msBus.AdcIf_Conv1msFuncInhibitAdcifSts);
    Task_1ms_Read_SentH_MainEcuModeSts(&Task_1msBus.SentH_MainEcuModeSts);
    Task_1ms_Read_Ioc_InputSR1msEcuModeSts(&Task_1msBus.Ioc_InputSR1msEcuModeSts);
    Task_1ms_Read_Ioc_InputSR1msFuncInhibitIocSts(&Task_1msBus.Ioc_InputSR1msFuncInhibitIocSts);
    Task_1ms_Read_Ioc_InputCS1msEcuModeSts(&Task_1msBus.Ioc_InputCS1msEcuModeSts);
    Task_1ms_Read_Ioc_InputCS1msFuncInhibitIocSts(&Task_1msBus.Ioc_InputCS1msFuncInhibitIocSts);
    Task_1ms_Read_CanTrv_TLE6251_HndlrEcuModeSts(&Task_1msBus.CanTrv_TLE6251_HndlrEcuModeSts);
    Task_1ms_Read_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts(&Task_1msBus.CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts);
    Task_1ms_Read_Vlvd_A3944_HndlrEcuModeSts(&Task_1msBus.Vlvd_A3944_HndlrEcuModeSts);
    Task_1ms_Read_Vlvd_A3944_HndlrFuncInhibitVlvdSts(&Task_1msBus.Vlvd_A3944_HndlrFuncInhibitVlvdSts);
    Task_1ms_Read_Mps_TLE5012_Hndlr_1msEcuModeSts(&Task_1msBus.Mps_TLE5012_Hndlr_1msEcuModeSts);
    Task_1ms_Read_Pedal_SenEcuModeSts(&Task_1msBus.Pedal_SenEcuModeSts);
    Task_1ms_Read_Pedal_SenFuncInhibitPedalSts(&Task_1msBus.Pedal_SenFuncInhibitPedalSts);
    Task_1ms_Read_Press_SenEcuModeSts(&Task_1msBus.Press_SenEcuModeSts);*/
    Task_1ms_Read_Press_SenFuncInhibitPressSts(&Task_1msBus.Press_SenFuncInhibitPressSts);
    Task_1ms_Read_Msp_1msCtrlVdcLink(&Task_1msBus.Msp_1msCtrlVdcLink);
   /* Task_1ms_Read_Spc_1msCtrlEcuModeSts(&Task_1msBus.Spc_1msCtrlEcuModeSts);
    Task_1ms_Read_Det_1msCtrlEcuModeSts(&Task_1msBus.Det_1msCtrlEcuModeSts);
    Task_1ms_Read_Pct_1msCtrlEcuModeSts(&Task_1msBus.Pct_1msCtrlEcuModeSts);*/
    Task_1ms_Read_Pct_1msCtrlPCtrlSt(&Task_1msBus.Pct_1msCtrlPCtrlSt);
    /*Task_1ms_Read_Mcc_1msCtrlEcuModeSts(&Task_1msBus.Mcc_1msCtrlEcuModeSts);
    Task_1ms_Read_Mcc_1msCtrlFuncInhibitMccSts(&Task_1msBus.Mcc_1msCtrlFuncInhibitMccSts);*/
    Task_1ms_Read_Mcc_1msCtrlTgtDeltaStkType(&Task_1msBus.Mcc_1msCtrlTgtDeltaStkType);
    Task_1ms_Read_Mcc_1msCtrlPCtrlBoostMod(&Task_1msBus.Mcc_1msCtrlPCtrlBoostMod);
    Task_1ms_Read_Mcc_1msCtrlPCtrlFadeoutSt(&Task_1msBus.Mcc_1msCtrlPCtrlFadeoutSt);
    Task_1ms_Read_Mcc_1msCtrlPCtrlSt(&Task_1msBus.Mcc_1msCtrlPCtrlSt);
    Task_1ms_Read_Mcc_1msCtrlInVlvAllCloseReq(&Task_1msBus.Mcc_1msCtrlInVlvAllCloseReq);
    Task_1ms_Read_Mcc_1msCtrlPChamberVolume(&Task_1msBus.Mcc_1msCtrlPChamberVolume);
    Task_1ms_Read_Mcc_1msCtrlTarDeltaStk(&Task_1msBus.Mcc_1msCtrlTarDeltaStk);
    Task_1ms_Read_Mcc_1msCtrlFinalTarPFromPCtrl(&Task_1msBus.Mcc_1msCtrlFinalTarPFromPCtrl);
    /*Task_1ms_Read_Ses_CtrlEcuModeSts(&Task_1msBus.Ses_CtrlEcuModeSts);
    Task_1ms_Read_Ses_CtrlFuncInhibitSesSts(&Task_1msBus.Ses_CtrlFuncInhibitSesSts);
    Task_1ms_Read_Vlv_ActrEcuModeSts(&Task_1msBus.Vlv_ActrEcuModeSts);
    Task_1ms_Read_Vlv_ActrFuncInhibitVlvSts(&Task_1msBus.Vlv_ActrFuncInhibitVlvSts);*/
    Task_1ms_Read_Vlv_ActrVlvSync(&Task_1msBus.Vlv_ActrVlvSync);
   /* Task_1ms_Read_Aka_MainPrlyEcuInhibit(&Task_1msBus.Aka_MainPrlyEcuInhibit);
    Task_1ms_Read_Mgd_TLE9180_HndlrEcuModeSts(&Task_1msBus.Mgd_TLE9180_HndlrEcuModeSts);
    Task_1ms_Read_Ioc_OutputSR1msEcuModeSts(&Task_1msBus.Ioc_OutputSR1msEcuModeSts);
    Task_1ms_Read_Ioc_OutputSR1msFuncInhibitIocSts(&Task_1msBus.Ioc_OutputSR1msFuncInhibitIocSts);
    Task_1ms_Read_Ioc_OutputCS1msEcuModeSts(&Task_1msBus.Ioc_OutputCS1msEcuModeSts);
    Task_1ms_Read_Ioc_OutputCS1msFuncInhibitIocSts(&Task_1msBus.Ioc_OutputCS1msFuncInhibitIocSts);*/
    Task_1ms_Read_Ioc_OutputCS1msFsrDcMtrShutDwn(&Task_1msBus.Ioc_OutputCS1msFsrDcMtrShutDwn);
    Task_1ms_Read_Ioc_OutputCS1msFsrEnDrDrv(&Task_1msBus.Ioc_OutputCS1msFsrEnDrDrv);
    //Task_1ms_Read_Arbitrator_MtrEcuModeSts(&Task_1msBus.Arbitrator_MtrEcuModeSts);
    Task_1ms_Read_Arbitrator_MtrIgnOnOffSts(&Task_1msBus.Arbitrator_MtrIgnOnOffSts);
    Task_1ms_Read_Arbitrator_MtrIgnEdgeSts(&Task_1msBus.Arbitrator_MtrIgnEdgeSts);
    Task_1ms_Read_Arbitrator_MtrDiagSci(&Task_1msBus.Arbitrator_MtrDiagSci);
    Task_1ms_Read_Arbitrator_MtrDiagAhbSci(&Task_1msBus.Arbitrator_MtrDiagAhbSci);

    /* Process */
    /**********************************************************************************************
     AdcIf_Conv1ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //AdcIf_Conv1msEcuModeSts = Task_1msBus.AdcIf_Conv1msEcuModeSts;
    //AdcIf_Conv1msFuncInhibitAdcifSts = Task_1msBus.AdcIf_Conv1msFuncInhibitAdcifSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    AdcIf_Conv1ms();

    /* Structure interface */
    Task_1msBus.AdcIf_Conv1msSwTrigPwrInfo = AdcIf_Conv1msSwTrigPwrInfo;
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigPwrInfo 
     : AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon;
     : AdcIf_Conv1msSwTrigPwrInfo.CEMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Int5vMon;
     : AdcIf_Conv1msSwTrigPwrInfo.CSPMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.VddMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon;
     =============================================================================*/

    Task_1msBus.AdcIf_Conv1msSwTrigMotInfo = AdcIf_Conv1msSwTrigMotInfo;
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigMotInfo 
     : AdcIf_Conv1msSwTrigMotInfo.MotPwrMon;
     : AdcIf_Conv1msSwTrigMotInfo.StarMon;
     : AdcIf_Conv1msSwTrigMotInfo.UoutMon;
     : AdcIf_Conv1msSwTrigMotInfo.VoutMon;
     : AdcIf_Conv1msSwTrigMotInfo.WoutMon;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.AdcIf_Conv1msAdcInvalid = AdcIf_Conv1msAdcInvalid;

    /**********************************************************************************************
     AdcIf_Conv1ms end
     **********************************************************************************************/

    /**********************************************************************************************
     Ach_Input start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Ach_Input();

    /* Structure interface */
    Task_1msBus.Ach_InputAchSysPwrAsicInfo = Ach_InputAchSysPwrAsicInfo;
    /*==============================================================================
    * Members of structure Ach_InputAchSysPwrAsicInfo 
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
     =============================================================================*/

    Task_1msBus.Ach_InputAchValveAsicInfo = Ach_InputAchValveAsicInfo;
    /*==============================================================================
    * Members of structure Ach_InputAchValveAsicInfo 
     : Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
     =============================================================================*/

    Task_1msBus.Ach_InputAchWssPort0AsicInfo = Ach_InputAchWssPort0AsicInfo;
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort0AsicInfo 
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1msBus.Ach_InputAchWssPort1AsicInfo = Ach_InputAchWssPort1AsicInfo;
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort1AsicInfo 
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1msBus.Ach_InputAchWssPort2AsicInfo = Ach_InputAchWssPort2AsicInfo;
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort2AsicInfo 
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1msBus.Ach_InputAchWssPort3AsicInfo = Ach_InputAchWssPort3AsicInfo;
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort3AsicInfo 
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Ach_InputAchAsicInvalid = Ach_InputAchAsicInvalid;

    /**********************************************************************************************
     Ach_Input end
     **********************************************************************************************/

    /**********************************************************************************************
     SentH_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //SentH_MainEcuModeSts = Task_1msBus.SentH_MainEcuModeSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    SentH_Main();

    /* Structure interface */
    Task_1msBus.SentH_MainSentHPressureInfo = SentH_MainSentHPressureInfo;
    /*==============================================================================
    * Members of structure SentH_MainSentHPressureInfo 
     : SentH_MainSentHPressureInfo.McpPresData1;
     : SentH_MainSentHPressureInfo.McpPresData2;
     : SentH_MainSentHPressureInfo.McpSentCrc;
     : SentH_MainSentHPressureInfo.McpSentData;
     : SentH_MainSentHPressureInfo.McpSentMsgId;
     : SentH_MainSentHPressureInfo.McpSentSerialCrc;
     : SentH_MainSentHPressureInfo.McpSentConfig;
     : SentH_MainSentHPressureInfo.McpSentInvalid;
     : SentH_MainSentHPressureInfo.MCPSentSerialInvalid;
     : SentH_MainSentHPressureInfo.Wlp1PresData1;
     : SentH_MainSentHPressureInfo.Wlp1PresData2;
     : SentH_MainSentHPressureInfo.Wlp1SentCrc;
     : SentH_MainSentHPressureInfo.Wlp1SentData;
     : SentH_MainSentHPressureInfo.Wlp1SentMsgId;
     : SentH_MainSentHPressureInfo.Wlp1SentSerialCrc;
     : SentH_MainSentHPressureInfo.Wlp1SentConfig;
     : SentH_MainSentHPressureInfo.Wlp1SentInvalid;
     : SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid;
     : SentH_MainSentHPressureInfo.Wlp2PresData1;
     : SentH_MainSentHPressureInfo.Wlp2PresData2;
     : SentH_MainSentHPressureInfo.Wlp2SentCrc;
     : SentH_MainSentHPressureInfo.Wlp2SentData;
     : SentH_MainSentHPressureInfo.Wlp2SentMsgId;
     : SentH_MainSentHPressureInfo.Wlp2SentSerialCrc;
     : SentH_MainSentHPressureInfo.Wlp2SentConfig;
     : SentH_MainSentHPressureInfo.Wlp2SentInvalid;
     : SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     SentH_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_InputSR1ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Ioc_InputSR1msEcuModeSts = Task_1msBus.Ioc_InputSR1msEcuModeSts;
    //Ioc_InputSR1msFuncInhibitIocSts = Task_1msBus.Ioc_InputSR1msFuncInhibitIocSts;

    /* Inter-Runnable structure interface */
    Ioc_InputSR1msAchAdcData = Ach_InputAchAdcData;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msAchAdcData 
     : Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT;
     : Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY;
     =============================================================================*/

    Ioc_InputSR1msSwTrigPwrInfo = AdcIf_Conv1msSwTrigPwrInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigPwrInfo 
     : Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon;
     : Ioc_InputSR1msSwTrigPwrInfo.CEMon;
     : Ioc_InputSR1msSwTrigPwrInfo.Int5vMon;
     : Ioc_InputSR1msSwTrigPwrInfo.CSPMon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vbatt01Mon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vbatt02Mon;
     : Ioc_InputSR1msSwTrigPwrInfo.VddMon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vdd3Mon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vdd5Mon;
     =============================================================================*/

    Ioc_InputSR1msSwTrigFspInfo = AdcIf_Conv1msSwTrigFspInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigFspInfo 
     : Ioc_InputSR1msSwTrigFspInfo.FspAbsHMon;
     : Ioc_InputSR1msSwTrigFspInfo.FspAbsMon;
     : Ioc_InputSR1msSwTrigFspInfo.FspCbsHMon;
     : Ioc_InputSR1msSwTrigFspInfo.FspCbsMon;
     =============================================================================*/

    Ioc_InputSR1msSwTrigTmpInfo = AdcIf_Conv1msSwTrigTmpInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigTmpInfo 
     : Ioc_InputSR1msSwTrigTmpInfo.LineTestMon;
     : Ioc_InputSR1msSwTrigTmpInfo.TempMon;
     =============================================================================*/

    Ioc_InputSR1msSwTrigMotInfo = AdcIf_Conv1msSwTrigMotInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigMotInfo 
     : Ioc_InputSR1msSwTrigMotInfo.MotPwrMon;
     : Ioc_InputSR1msSwTrigMotInfo.StarMon;
     : Ioc_InputSR1msSwTrigMotInfo.UoutMon;
     : Ioc_InputSR1msSwTrigMotInfo.VoutMon;
     : Ioc_InputSR1msSwTrigMotInfo.WoutMon;
     =============================================================================*/

    Ioc_InputSR1msSwTrigPdtInfo = AdcIf_Conv1msSwTrigPdtInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigPdtInfo 
     : Ioc_InputSR1msSwTrigPdtInfo.PdfSigMon;
     : Ioc_InputSR1msSwTrigPdtInfo.Pdt5vMon;
     : Ioc_InputSR1msSwTrigPdtInfo.Pdf5vMon;
     : Ioc_InputSR1msSwTrigPdtInfo.PdtSigMon;
     =============================================================================*/

    Ioc_InputSR1msSwTrigMocInfo = AdcIf_Conv1msSwTrigMocInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigMocInfo 
     : Ioc_InputSR1msSwTrigMocInfo.RlIMainMon;
     : Ioc_InputSR1msSwTrigMocInfo.RlISubMonFs;
     : Ioc_InputSR1msSwTrigMocInfo.RlMocNegMon;
     : Ioc_InputSR1msSwTrigMocInfo.RlMocPosMon;
     : Ioc_InputSR1msSwTrigMocInfo.RrIMainMon;
     : Ioc_InputSR1msSwTrigMocInfo.RrISubMonFs;
     : Ioc_InputSR1msSwTrigMocInfo.RrMocNegMon;
     : Ioc_InputSR1msSwTrigMocInfo.RrMocPosMon;
     : Ioc_InputSR1msSwTrigMocInfo.BFLMon;
     =============================================================================*/

    Ioc_InputSR1msHwTrigVlvInfo = AdcIf_Conv1msHwTrigVlvInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msHwTrigVlvInfo 
     : Ioc_InputSR1msHwTrigVlvInfo.VlvCVMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvRLVMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvCutPMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvCutSMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvSimMon;
     =============================================================================*/

    Ioc_InputSR1msSwTrigEscInfo = AdcIf_Conv1msSwTrigEscInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigEscInfo 
     : Ioc_InputSR1msSwTrigEscInfo.IF_A7_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A2_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P1_NEG_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A6_MON;
     : Ioc_InputSR1msSwTrigEscInfo.MTR_FW_S_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A4_MON;
     : Ioc_InputSR1msSwTrigEscInfo.MTP_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A3_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P1_POS_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A5_MON;
     : Ioc_InputSR1msSwTrigEscInfo.VDD_MON;
     : Ioc_InputSR1msSwTrigEscInfo.COOL_MON;
     : Ioc_InputSR1msSwTrigEscInfo.CAN_L_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P3_NEG_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A1_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P2_POS_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A8_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A9_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P2_NEG_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P3_POS_MON;
     : Ioc_InputSR1msSwTrigEscInfo.OP_OUT_MON;
     =============================================================================*/

    Ioc_InputSR1msSentHPressureInfo = SentH_MainSentHPressureInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSentHPressureInfo 
     : Ioc_InputSR1msSentHPressureInfo.McpPresData1;
     : Ioc_InputSR1msSentHPressureInfo.McpPresData2;
     : Ioc_InputSR1msSentHPressureInfo.McpSentCrc;
     : Ioc_InputSR1msSentHPressureInfo.McpSentData;
     : Ioc_InputSR1msSentHPressureInfo.McpSentMsgId;
     : Ioc_InputSR1msSentHPressureInfo.McpSentSerialCrc;
     : Ioc_InputSR1msSentHPressureInfo.McpSentConfig;
     : Ioc_InputSR1msSentHPressureInfo.McpSentInvalid;
     : Ioc_InputSR1msSentHPressureInfo.MCPSentSerialInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1PresData1;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1PresData2;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentData;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentMsgId;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentConfig;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2PresData1;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2PresData2;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentData;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentMsgId;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentConfig;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialInvalid;
     =============================================================================*/

    /* Inter-Runnable single interface */
    Ioc_InputSR1msAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;
    Ioc_InputSR1msAdcInvalid = AdcIf_Conv1msAdcInvalid;

    /* Execute  runnable */
    Ioc_InputSR1ms();

    /* Structure interface */
    Task_1msBus.Ioc_InputSR1msHalPressureInfo = Ioc_InputSR1msHalPressureInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msHalPressureInfo 
     : Ioc_InputSR1msHalPressureInfo.McpPresData1;
     : Ioc_InputSR1msHalPressureInfo.McpPresData2;
     : Ioc_InputSR1msHalPressureInfo.McpSentCrc;
     : Ioc_InputSR1msHalPressureInfo.McpSentData;
     : Ioc_InputSR1msHalPressureInfo.McpSentMsgId;
     : Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.McpSentConfig;
     : Ioc_InputSR1msHalPressureInfo.Wlp1PresData1;
     : Ioc_InputSR1msHalPressureInfo.Wlp1PresData2;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentData;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig;
     : Ioc_InputSR1msHalPressureInfo.Wlp2PresData1;
     : Ioc_InputSR1msHalPressureInfo.Wlp2PresData2;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentData;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/

    Task_1msBus.Ioc_InputSR1msMotMonInfo = Ioc_InputSR1msMotMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotMonInfo 
     : Ioc_InputSR1msMotMonInfo.MotPwrVoltMon;
     : Ioc_InputSR1msMotMonInfo.MotStarMon;
     =============================================================================*/

    Task_1msBus.Ioc_InputSR1msMotVoltsMonInfo = Ioc_InputSR1msMotVoltsMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotVoltsMonInfo 
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon;
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon;
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/

    Task_1msBus.Ioc_InputSR1msPedlSigMonInfo = Ioc_InputSR1msPedlSigMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPedlSigMonInfo 
     : Ioc_InputSR1msPedlSigMonInfo.PdfSigMon;
     : Ioc_InputSR1msPedlSigMonInfo.PdtSigMon;
     =============================================================================*/

    Task_1msBus.Ioc_InputSR1msPedlPwrMonInfo = Ioc_InputSR1msPedlPwrMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPedlPwrMonInfo 
     : Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon;
     : Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Ioc_InputSR1msVBatt1Mon = Ioc_InputSR1msVBatt1Mon;
    Task_1msBus.Ioc_InputSR1msVBatt2Mon = Ioc_InputSR1msVBatt2Mon;
    Task_1msBus.Ioc_InputSR1msFspCbsHMon = Ioc_InputSR1msFspCbsHMon;
    Task_1msBus.Ioc_InputSR1msFspCbsMon = Ioc_InputSR1msFspCbsMon;
    Task_1msBus.Ioc_InputSR1msFspAbsHMon = Ioc_InputSR1msFspAbsHMon;
    Task_1msBus.Ioc_InputSR1msFspAbsMon = Ioc_InputSR1msFspAbsMon;
    Task_1msBus.Ioc_InputSR1msExt5vMon = Ioc_InputSR1msExt5vMon;
    Task_1msBus.Ioc_InputSR1msCEMon = Ioc_InputSR1msCEMon;
    Task_1msBus.Ioc_InputSR1msVdd1Mon = Ioc_InputSR1msVdd1Mon;
    Task_1msBus.Ioc_InputSR1msVdd2Mon = Ioc_InputSR1msVdd2Mon;
    Task_1msBus.Ioc_InputSR1msVdd3Mon = Ioc_InputSR1msVdd3Mon;
    Task_1msBus.Ioc_InputSR1msVdd4Mon = Ioc_InputSR1msVdd4Mon;
    Task_1msBus.Ioc_InputSR1msVdd5Mon = Ioc_InputSR1msVdd5Mon;
    Task_1msBus.Ioc_InputSR1msVddMon = Ioc_InputSR1msVddMon;
    Task_1msBus.Ioc_InputSR1msCspMon = Ioc_InputSR1msCspMon;

    /**********************************************************************************************
     Ioc_InputSR1ms end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_InputCS1ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Ioc_InputCS1msEcuModeSts = Task_1msBus.Ioc_InputCS1msEcuModeSts;
    //Ioc_InputCS1msFuncInhibitIocSts = Task_1msBus.Ioc_InputCS1msFuncInhibitIocSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    Ioc_InputCS1msAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /* Execute  runnable */
    Ioc_InputCS1ms();

    /* Structure interface */
    Task_1msBus.Ioc_InputCS1msSwtMonInfo = Ioc_InputCS1msSwtMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwtMonInfo 
     : Ioc_InputCS1msSwtMonInfo.AvhSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BflSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BlsSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BsSwtMon;
     : Ioc_InputCS1msSwtMonInfo.DoorSwtMon;
     : Ioc_InputCS1msSwtMonInfo.EscSwtMon;
     : Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon;
     : Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon;
     : Ioc_InputCS1msSwtMonInfo.HzrdSwtMon;
     : Ioc_InputCS1msSwtMonInfo.HdcSwtMon;
     : Ioc_InputCS1msSwtMonInfo.PbSwtMon;
     =============================================================================*/

    Task_1msBus.Ioc_InputCS1msSwtMonInfoEsc = Ioc_InputCS1msSwtMonInfoEsc;
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwtMonInfoEsc 
     : Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc;
     =============================================================================*/

    Task_1msBus.Ioc_InputCS1msRlyMonInfo = Ioc_InputCS1msRlyMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputCS1msRlyMonInfo 
     : Ioc_InputCS1msRlyMonInfo.RlyDbcMon;
     : Ioc_InputCS1msRlyMonInfo.RlyEssMon;
     : Ioc_InputCS1msRlyMonInfo.RlyFault;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Ioc_InputCS1msRsmDbcMon = Ioc_InputCS1msRsmDbcMon;

    /**********************************************************************************************
     Ioc_InputCS1ms end
     **********************************************************************************************/

    /**********************************************************************************************
     CanTrv_TLE6251_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //CanTrv_TLE6251_HndlrEcuModeSts = Task_1msBus.CanTrv_TLE6251_HndlrEcuModeSts;
    //CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = Task_1msBus.CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    CanTrv_TLE6251_Hndlr();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     CanTrv_TLE6251_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     Vlvd_A3944_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Vlvd_A3944_HndlrEcuModeSts = Task_1msBus.Vlvd_A3944_HndlrEcuModeSts;
    //Vlvd_A3944_HndlrFuncInhibitVlvdSts = Task_1msBus.Vlvd_A3944_HndlrFuncInhibitVlvdSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Vlvd_A3944_Hndlr();

    /* Structure interface */
    Task_1msBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo = Vlvd_A3944_HndlrVlvdFF0DcdInfo;
    /*==============================================================================
    * Members of structure Vlvd_A3944_HndlrVlvdFF0DcdInfo 
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff;
     =============================================================================*/

    Task_1msBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo = Vlvd_A3944_HndlrVlvdFF1DcdInfo;
    /*==============================================================================
    * Members of structure Vlvd_A3944_HndlrVlvdFF1DcdInfo 
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Vlvd_A3944_HndlrVlvdInvalid = Vlvd_A3944_HndlrVlvdInvalid;

    /**********************************************************************************************
     Vlvd_A3944_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     Mps_TLE5012_Hndlr_1ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Mps_TLE5012_Hndlr_1msEcuModeSts = Task_1msBus.Mps_TLE5012_Hndlr_1msEcuModeSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Mps_TLE5012_Hndlr_1ms();

    /* Structure interface */
    Task_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo = Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo 
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV;
     =============================================================================*/

    Task_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo = Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo 
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_VR;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_WD;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ROM;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ADCT;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_DSPU;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_FUSE;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_OV;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Mps_TLE5012_Hndlr_1msMpsInvalid = Mps_TLE5012_Hndlr_1msMpsInvalid;

    /**********************************************************************************************
     Mps_TLE5012_Hndlr_1ms end
     **********************************************************************************************/

    /**********************************************************************************************
     Pedal_Sen start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Pedal_SenEcuModeSts = Task_1msBus.Pedal_SenEcuModeSts;
    //Pedal_SenFuncInhibitPedalSts = Task_1msBus.Pedal_SenFuncInhibitPedalSts;

    /* Inter-Runnable structure interface */
    Pedal_SenPedlSigMonInfo = Ioc_InputSR1msPedlSigMonInfo;
    /*==============================================================================
    * Members of structure Pedal_SenPedlSigMonInfo 
     : Pedal_SenPedlSigMonInfo.PdfSigMon;
     : Pedal_SenPedlSigMonInfo.PdtSigMon;
     =============================================================================*/

    Pedal_SenPedlPwrMonInfo = Ioc_InputSR1msPedlPwrMonInfo;
    /*==============================================================================
    * Members of structure Pedal_SenPedlPwrMonInfo 
     : Pedal_SenPedlPwrMonInfo.Pdt5vMon;
     : Pedal_SenPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Pedal_Sen();

    /* Structure interface */
    Task_1msBus.Pedal_SenPdtBufInfo = Pedal_SenPdtBufInfo;
    /*==============================================================================
    * Members of structure Pedal_SenPdtBufInfo 
     : Pedal_SenPdtBufInfo.PdtRaw;
     =============================================================================*/

    Task_1msBus.Pedal_SenPdfBufInfo = Pedal_SenPdfBufInfo;
    /*==============================================================================
    * Members of structure Pedal_SenPdfBufInfo 
     : Pedal_SenPdfBufInfo.PdfRaw;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Pedal_Sen end
     **********************************************************************************************/

    /**********************************************************************************************
     Press_Sen start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Press_SenEcuModeSts = Task_1msBus.Press_SenEcuModeSts;
    //Press_SenFuncInhibitPressSts = Task_1msBus.Press_SenFuncInhibitPressSts;

    /* Inter-Runnable structure interface */
    Press_SenHalPressureInfo = Ioc_InputSR1msHalPressureInfo;
    /*==============================================================================
    * Members of structure Press_SenHalPressureInfo 
     : Press_SenHalPressureInfo.McpPresData1;
     : Press_SenHalPressureInfo.McpPresData2;
     : Press_SenHalPressureInfo.McpSentCrc;
     : Press_SenHalPressureInfo.McpSentData;
     : Press_SenHalPressureInfo.McpSentMsgId;
     : Press_SenHalPressureInfo.McpSentSerialCrc;
     : Press_SenHalPressureInfo.McpSentConfig;
     : Press_SenHalPressureInfo.Wlp1PresData1;
     : Press_SenHalPressureInfo.Wlp1PresData2;
     : Press_SenHalPressureInfo.Wlp1SentCrc;
     : Press_SenHalPressureInfo.Wlp1SentData;
     : Press_SenHalPressureInfo.Wlp1SentMsgId;
     : Press_SenHalPressureInfo.Wlp1SentSerialCrc;
     : Press_SenHalPressureInfo.Wlp1SentConfig;
     : Press_SenHalPressureInfo.Wlp2PresData1;
     : Press_SenHalPressureInfo.Wlp2PresData2;
     : Press_SenHalPressureInfo.Wlp2SentCrc;
     : Press_SenHalPressureInfo.Wlp2SentData;
     : Press_SenHalPressureInfo.Wlp2SentMsgId;
     : Press_SenHalPressureInfo.Wlp2SentSerialCrc;
     : Press_SenHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Press_Sen();

    /* Structure interface */
    Task_1msBus.Press_SenCircPBufInfo = Press_SenCircPBufInfo;
    /*==============================================================================
    * Members of structure Press_SenCircPBufInfo 
     : Press_SenCircPBufInfo.PrimCircPRaw;
     : Press_SenCircPBufInfo.SecdCircPRaw;
     =============================================================================*/

    Task_1msBus.Press_SenPistPBufInfo = Press_SenPistPBufInfo;
    /*==============================================================================
    * Members of structure Press_SenPistPBufInfo 
     : Press_SenPistPBufInfo.PistPRaw;
     =============================================================================*/

    Task_1msBus.Press_SenPspBufInfo = Press_SenPspBufInfo;
    /*==============================================================================
    * Members of structure Press_SenPspBufInfo 
     : Press_SenPspBufInfo.PedlSimPRaw;
     =============================================================================*/

    Task_1msBus.Press_SenPressCalcInfo = Press_SenPressCalcInfo;
    /*==============================================================================
    * Members of structure Press_SenPressCalcInfo 
     : Press_SenPressCalcInfo.PressP_SimP_1_100_bar;
     : Press_SenPressCalcInfo.PressP_CirP1_1_100_bar;
     : Press_SenPressCalcInfo.PressP_CirP2_1_100_bar;
     : Press_SenPressCalcInfo.SimPMoveAvr;
     : Press_SenPressCalcInfo.CirPMoveAvr;
     : Press_SenPressCalcInfo.CirP2MoveAvr;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Press_Sen end
     **********************************************************************************************/

    /**********************************************************************************************
     Msp_1msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    Msp_1msCtrlVdcLink = Task_1msBus.Msp_1msCtrlVdcLink;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Msp_1msCtrl();

    /* Structure interface */
    /* Single interface */
    Task_1msBus.Msp_1msCtrlVdcLinkFild = Msp_1msCtrlVdcLinkFild;

    /**********************************************************************************************
     Msp_1msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Spc_1msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Spc_1msCtrlEcuModeSts = Task_1msBus.Spc_1msCtrlEcuModeSts;

    /* Inter-Runnable structure interface */
    Spc_1msCtrlCircP1msRawInfo = Press_SenCircP1msRawInfo;
    /*==============================================================================
    * Members of structure Spc_1msCtrlCircP1msRawInfo 
     : Spc_1msCtrlCircP1msRawInfo.PrimCircPSig;
     : Spc_1msCtrlCircP1msRawInfo.SecdCircPSig;
     =============================================================================*/

    Spc_1msCtrlPistP1msRawInfo = Press_SenPistP1msRawInfo;
    /*==============================================================================
    * Members of structure Spc_1msCtrlPistP1msRawInfo 
     : Spc_1msCtrlPistP1msRawInfo.PistPSig;
     =============================================================================*/

    Spc_1msCtrlPdt1msRawInfo = Pedal_SenPdt1msRawInfo;
    /*==============================================================================
    * Members of structure Spc_1msCtrlPdt1msRawInfo 
     : Spc_1msCtrlPdt1msRawInfo.PdtSig;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Spc_1msCtrl();

    /* Structure interface */
    Task_1msBus.Spc_1msCtrlPedlTrvlFild1msInfo = Spc_1msCtrlPedlTrvlFild1msInfo;
    /*==============================================================================
    * Members of structure Spc_1msCtrlPedlTrvlFild1msInfo 
     : Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Spc_1msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Det_1msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Det_1msCtrlEcuModeSts = Task_1msBus.Det_1msCtrlEcuModeSts;

    /* Inter-Runnable structure interface */
    Det_1msCtrlCircPFild1msInfo = Spc_1msCtrlCircPFild1msInfo;
    /*==============================================================================
    * Members of structure Det_1msCtrlCircPFild1msInfo 
     : Det_1msCtrlCircPFild1msInfo.PrimCircPFild1ms;
     : Det_1msCtrlCircPFild1msInfo.SecdCircPFild1ms;
     =============================================================================*/

    Det_1msCtrlPedlTrvlFild1msInfo = Spc_1msCtrlPedlTrvlFild1msInfo;
    /*==============================================================================
    * Members of structure Det_1msCtrlPedlTrvlFild1msInfo 
     : Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/

    Det_1msCtrlPistPFild1msInfo = Spc_1msCtrlPistPFild1msInfo;
    /*==============================================================================
    * Members of structure Det_1msCtrlPistPFild1msInfo 
     : Det_1msCtrlPistPFild1msInfo.PistPFild1ms;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Det_1msCtrl();

    /* Structure interface */
    Task_1msBus.Det_1msCtrlBrkPedlStatus1msInfo = Det_1msCtrlBrkPedlStatus1msInfo;
    /*==============================================================================
    * Members of structure Det_1msCtrlBrkPedlStatus1msInfo 
     : Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/

    Task_1msBus.Det_1msCtrlPedlTrvlRate1msInfo = Det_1msCtrlPedlTrvlRate1msInfo;
    /*==============================================================================
    * Members of structure Det_1msCtrlPedlTrvlRate1msInfo 
     : Det_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Det_1msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Pct_1msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Pct_1msCtrlEcuModeSts = Task_1msBus.Pct_1msCtrlEcuModeSts;
    Pct_1msCtrlPCtrlSt = Task_1msBus.Pct_1msCtrlPCtrlSt;

    /* Inter-Runnable structure interface */
    Pct_1msCtrlPedlTrvlFild1msInfo = Spc_1msCtrlPedlTrvlFild1msInfo;
    /*==============================================================================
    * Members of structure Pct_1msCtrlPedlTrvlFild1msInfo 
     : Pct_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/

    Pct_1msCtrlPedlTrvlRate1msInfo = Det_1msCtrlPedlTrvlRate1msInfo;
    /*==============================================================================
    * Members of structure Pct_1msCtrlPedlTrvlRate1msInfo 
     : Pct_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Pct_1msCtrl();

    /* Structure interface */
    /* Single interface */
    Task_1msBus.Pct_1msCtrlPreBoostMod = Pct_1msCtrlPreBoostMod;

    /**********************************************************************************************
     Pct_1msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Mcc_1msCtrl start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd = Task_1msBus.Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd;
    Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild = Task_1msBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild;

    /* Decomposed structure interface */
    Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk = Task_1msBus.Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk;

    /* Decomposed structure interface */
    Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = Task_1msBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
    Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = Task_1msBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;

    /* Single interface */
    //Mcc_1msCtrlEcuModeSts = Task_1msBus.Mcc_1msCtrlEcuModeSts;
    //Mcc_1msCtrlFuncInhibitMccSts = Task_1msBus.Mcc_1msCtrlFuncInhibitMccSts;
    Mcc_1msCtrlTgtDeltaStkType = Task_1msBus.Mcc_1msCtrlTgtDeltaStkType;
    Mcc_1msCtrlPCtrlBoostMod = Task_1msBus.Mcc_1msCtrlPCtrlBoostMod;
    Mcc_1msCtrlPCtrlFadeoutSt = Task_1msBus.Mcc_1msCtrlPCtrlFadeoutSt;
    Mcc_1msCtrlPCtrlSt = Task_1msBus.Mcc_1msCtrlPCtrlSt;
    Mcc_1msCtrlInVlvAllCloseReq = Task_1msBus.Mcc_1msCtrlInVlvAllCloseReq;
    Mcc_1msCtrlPChamberVolume = Task_1msBus.Mcc_1msCtrlPChamberVolume;
    Mcc_1msCtrlTarDeltaStk = Task_1msBus.Mcc_1msCtrlTarDeltaStk;
    Mcc_1msCtrlFinalTarPFromPCtrl = Task_1msBus.Mcc_1msCtrlFinalTarPFromPCtrl;

    /* Inter-Runnable structure interface */
    Mcc_1msCtrlCircPFild1msInfo = Spc_1msCtrlCircPFild1msInfo;
    /*==============================================================================
    * Members of structure Mcc_1msCtrlCircPFild1msInfo 
     : Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms;
     : Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms;
     =============================================================================*/

    Mcc_1msCtrlPistPFild1msInfo = Spc_1msCtrlPistPFild1msInfo;
    /*==============================================================================
    * Members of structure Mcc_1msCtrlPistPFild1msInfo 
     : Mcc_1msCtrlPistPFild1msInfo.PistPFild1ms;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Mcc_1msCtrl();

    /* Structure interface */
    Task_1msBus.Mcc_1msCtrlMotDqIRefMccInfo = Mcc_1msCtrlMotDqIRefMccInfo;
    /*==============================================================================
    * Members of structure Mcc_1msCtrlMotDqIRefMccInfo 
     : Mcc_1msCtrlMotDqIRefMccInfo.IdRef;
     : Mcc_1msCtrlMotDqIRefMccInfo.IqRef;
     =============================================================================*/

    Task_1msBus.Mcc_1msCtrlIdbMotPosnInfo = Mcc_1msCtrlIdbMotPosnInfo;
    /*==============================================================================
    * Members of structure Mcc_1msCtrlIdbMotPosnInfo 
     : Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn;
     =============================================================================*/

    Task_1msBus.Mcc_1msCtrlFluxWeakengStInfo = Mcc_1msCtrlFluxWeakengStInfo;
    /*==============================================================================
    * Members of structure Mcc_1msCtrlFluxWeakengStInfo 
     : Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg;
     : Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Mcc_1msCtrlMotCtrlMode = Mcc_1msCtrlMotCtrlMode;
    Task_1msBus.Mcc_1msCtrlMotCtrlState = Mcc_1msCtrlMotCtrlState;
    Task_1msBus.Mcc_1msCtrlMotICtrlFadeOutState = Mcc_1msCtrlMotICtrlFadeOutState;
    Task_1msBus.Mcc_1msCtrlStkRecvryStabnEndOK = Mcc_1msCtrlStkRecvryStabnEndOK;

    /**********************************************************************************************
     Mcc_1msCtrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Ses_Ctrl start
     **********************************************************************************************/

     /* Structure interface */
    Ses_CtrlMotDqIMeasdInfo = Task_1msBus.Ses_CtrlMotDqIMeasdInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlMotDqIMeasdInfo 
     : Ses_CtrlMotDqIMeasdInfo.MotIqMeasd;
     : Ses_CtrlMotDqIMeasdInfo.MotIdMeasd;
     =============================================================================*/

    /* Decomposed structure interface */
    Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd = Task_1msBus.Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd;

    Ses_CtrlMotRotgAgSigBfMotOrgSetInfo = Task_1msBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlMotRotgAgSigBfMotOrgSetInfo 
     : Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
     : Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
     =============================================================================*/

    /* Single interface */
    //Ses_CtrlEcuModeSts = Task_1msBus.Ses_CtrlEcuModeSts;
    //Ses_CtrlFuncInhibitSesSts = Task_1msBus.Ses_CtrlFuncInhibitSesSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Ses_Ctrl();

    /* Structure interface */
    Task_1msBus.Ses_CtrlNormVlvReqSesInfo = Ses_CtrlNormVlvReqSesInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlNormVlvReqSesInfo 
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen;
     =============================================================================*/

    Task_1msBus.Ses_CtrlWhlVlvReqSesInfo = Ses_CtrlWhlVlvReqSesInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlWhlVlvReqSesInfo 
     : Ses_CtrlWhlVlvReqSesInfo.FlIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.FlIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen;
     =============================================================================*/

    Task_1msBus.Ses_CtrlMotOrgSetStInfo = Ses_CtrlMotOrgSetStInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlMotOrgSetStInfo 
     : Ses_CtrlMotOrgSetStInfo.MotOrgSetErr;
     : Ses_CtrlMotOrgSetStInfo.MotOrgSet;
     =============================================================================*/

    Task_1msBus.Ses_CtrlMotDqIRefSesInfo = Ses_CtrlMotDqIRefSesInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlMotDqIRefSesInfo 
     : Ses_CtrlMotDqIRefSesInfo.IdRef;
     : Ses_CtrlMotDqIRefSesInfo.IqRef;
     =============================================================================*/

    Task_1msBus.Ses_CtrlPwrPistStBfMotOrgSetInfo = Ses_CtrlPwrPistStBfMotOrgSetInfo;
    /*==============================================================================
    * Members of structure Ses_CtrlPwrPistStBfMotOrgSetInfo 
     : Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
     : Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Ses_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Vlv_Actr start
     **********************************************************************************************/

     /* Structure interface */
    Vlv_ActrNormVlvReqInfo = Task_1msBus.Vlv_ActrNormVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrNormVlvReqInfo 
     : Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrNormVlvReqInfo.SimVlvReqData;
     =============================================================================*/

    Vlv_ActrWhlVlvReqInfo = Task_1msBus.Vlv_ActrWhlVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrWhlVlvReqInfo 
     : Vlv_ActrWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    Vlv_ActrBalVlvReqInfo = Task_1msBus.Vlv_ActrBalVlvReqInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrBalVlvReqInfo 
     : Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData;
     : Vlv_ActrBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/

    /* Single interface */
    //Vlv_ActrEcuModeSts = Task_1msBus.Vlv_ActrEcuModeSts;
    //Vlv_ActrFuncInhibitVlvSts = Task_1msBus.Vlv_ActrFuncInhibitVlvSts;
    Vlv_ActrVlvSync = Task_1msBus.Vlv_ActrVlvSync;

    /* Inter-Runnable structure interface */
    Vlv_ActrVlvMonInfo = Ioc_InputSR1msVlvMonInfo;
    /*==============================================================================
    * Members of structure Vlv_ActrVlvMonInfo 
     : Vlv_ActrVlvMonInfo.VlvCVMon;
     : Vlv_ActrVlvMonInfo.VlvRLVMon;
     : Vlv_ActrVlvMonInfo.PrimCutVlvMon;
     : Vlv_ActrVlvMonInfo.SecdCutVlvMon;
     : Vlv_ActrVlvMonInfo.SimVlvMon;
     =============================================================================*/

    /* Inter-Runnable single interface */
    Vlv_ActrAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /* Execute  runnable */
    Vlv_Actr();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Vlv_Actr end
     **********************************************************************************************/

    /**********************************************************************************************
     Acm_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Acm_Main();

    /* Structure interface */
    /* Single interface */
    Task_1msBus.Acm_MainAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /**********************************************************************************************
     Acm_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Aka_Main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Aka_MainPrlyEcuInhibit = Task_1msBus.Aka_MainPrlyEcuInhibit;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    Aka_MainAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /* Execute  runnable */
    Aka_Main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Aka_Main end
     **********************************************************************************************/

    /**********************************************************************************************
     Awd_main start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */
    Awd_mainAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /* Execute  runnable */
    Awd_main();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Awd_main end
     **********************************************************************************************/

    /**********************************************************************************************
     Mgd_TLE9180_Hndlr start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Mgd_TLE9180_HndlrEcuModeSts = Task_1msBus.Mgd_TLE9180_HndlrEcuModeSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Mgd_TLE9180_Hndlr();

    /* Structure interface */
    Task_1msBus.Mgd_TLE9180_HndlrMgdDcdInfo = Mgd_TLE9180_HndlrMgdDcdInfo;
    /*==============================================================================
    * Members of structure Mgd_TLE9180_HndlrMgdDcdInfo 
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Mgd_TLE9180_HndlrMgdInvalid = Mgd_TLE9180_HndlrMgdInvalid;

    /**********************************************************************************************
     Mgd_TLE9180_Hndlr end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_OutputSR1ms start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv = Task_1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv;

    /* Decomposed structure interface */
    Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv = Task_1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv;

    /* Single interface */
    //Ioc_OutputSR1msEcuModeSts = Task_1msBus.Ioc_OutputSR1msEcuModeSts;
    //Ioc_OutputSR1msFuncInhibitIocSts = Task_1msBus.Ioc_OutputSR1msFuncInhibitIocSts;

    /* Inter-Runnable structure interface */
    Ioc_OutputSR1msWhlVlvDrvInfo = Vlv_ActrWhlVlvDrvInfo;
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msWhlVlvDrvInfo 
     : Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData;
     =============================================================================*/

    Ioc_OutputSR1msBalVlvDrvInfo = Vlv_ActrBalVlvDrvInfo;
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msBalVlvDrvInfo 
     : Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData;
     : Ioc_OutputSR1msBalVlvDrvInfo.BalVlvDrvData;
     =============================================================================*/

    /* Inter-Runnable single interface */
    Ioc_OutputSR1msAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /* Execute  runnable */
    Ioc_OutputSR1ms();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Ioc_OutputSR1ms end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_OutputCS1ms start
     **********************************************************************************************/

     /* Structure interface */
    Ioc_OutputCS1msRlyDrvInfo = Task_1msBus.Ioc_OutputCS1msRlyDrvInfo;
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msRlyDrvInfo 
     : Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv;
     : Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv;
     =============================================================================*/

    /* Decomposed structure interface */
    Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff = Task_1msBus.Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff;

    /* Decomposed structure interface */
    Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff = Task_1msBus.Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff;

    /* Decomposed structure interface */
    Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req = Task_1msBus.Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req;

    /* Single interface */
    //Ioc_OutputCS1msEcuModeSts = Task_1msBus.Ioc_OutputCS1msEcuModeSts;
    //Ioc_OutputCS1msFuncInhibitIocSts = Task_1msBus.Ioc_OutputCS1msFuncInhibitIocSts;
    Ioc_OutputCS1msFsrDcMtrShutDwn = Task_1msBus.Ioc_OutputCS1msFsrDcMtrShutDwn;
    Ioc_OutputCS1msFsrEnDrDrv = Task_1msBus.Ioc_OutputCS1msFsrEnDrDrv;

    /* Inter-Runnable structure interface */
    Ioc_OutputCS1msNormVlvDrvInfo = Vlv_ActrNormVlvDrvInfo;
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msNormVlvDrvInfo 
     : Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData;
     =============================================================================*/

    Ioc_OutputCS1msWhlVlvDrvInfo = Vlv_ActrWhlVlvDrvInfo;
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msWhlVlvDrvInfo 
     : Ioc_OutputCS1msWhlVlvDrvInfo.FlOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.FlIvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.FrOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.FrIvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RlOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RlIvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RrOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RrIvDrvData;
     =============================================================================*/

    /* Inter-Runnable single interface */
    Ioc_OutputCS1msAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;
    Ioc_OutputCS1msMainCanEn = CanTrv_TLE6251_HndlrMainCanEn;
    Ioc_OutputCS1msVlvDrvEnRst = Vlvd_A3944_HndlrVlvDrvEnRst;
    Ioc_OutputCS1msAwdPrnDrv = Awd_mainAwdPrnDrv;

    /* Execute  runnable */
    Ioc_OutputCS1ms();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Ioc_OutputCS1ms end
     **********************************************************************************************/

    /**********************************************************************************************
     Ach_Output start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */

    /* Inter-Runnable structure interface */
    Ach_OutputAkaKeepAliveWriteData = Aka_MainAkaKeepAliveWriteData;
    /*==============================================================================
    * Members of structure Ach_OutputAkaKeepAliveWriteData 
     : Ach_OutputAkaKeepAliveWriteData.KeepAliveDataFlg;
     : Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_PHOLD;
     : Ach_OutputAkaKeepAliveWriteData.ACH_Tx1_KA;
     =============================================================================*/

    Ach_OutputIocDcMtrDutyData = Ioc_OutputSR1msIocDcMtrDutyData;
    /*==============================================================================
    * Members of structure Ach_OutputIocDcMtrDutyData 
     : Ach_OutputIocDcMtrDutyData.MtrDutyDataFlg;
     : Ach_OutputIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM;
     =============================================================================*/

    Ach_OutputIocDcMtrFreqData = Ioc_OutputSR1msIocDcMtrFreqData;
    /*==============================================================================
    * Members of structure Ach_OutputIocDcMtrFreqData 
     : Ach_OutputIocDcMtrFreqData.MtrFreqDataFlg;
     : Ach_OutputIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM;
     =============================================================================*/

    Ach_OutputIocVlvNo0Data = Ioc_OutputSR1msIocVlvNo0Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo0Data 
     : Ach_OutputIocVlvNo0Data.VlvNo0DataFlg;
     : Ach_OutputIocVlvNo0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNo1Data = Ioc_OutputSR1msIocVlvNo1Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo1Data 
     : Ach_OutputIocVlvNo1Data.VlvNo1DataFlg;
     : Ach_OutputIocVlvNo1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNo2Data = Ioc_OutputSR1msIocVlvNo2Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo2Data 
     : Ach_OutputIocVlvNo2Data.VlvNo2DataFlg;
     : Ach_OutputIocVlvNo2Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNo3Data = Ioc_OutputSR1msIocVlvNo3Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNo3Data 
     : Ach_OutputIocVlvNo3Data.VlvNo3DataFlg;
     : Ach_OutputIocVlvNo3Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvTc0Data = Ioc_OutputSR1msIocVlvTc0Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvTc0Data 
     : Ach_OutputIocVlvTc0Data.VlvTc0DataFlg;
     : Ach_OutputIocVlvTc0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvTc1Data = Ioc_OutputSR1msIocVlvTc1Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvTc1Data 
     : Ach_OutputIocVlvTc1Data.VlvTc1DataFlg;
     : Ach_OutputIocVlvTc1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvEsv0Data = Ioc_OutputSR1msIocVlvEsv0Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvEsv0Data 
     : Ach_OutputIocVlvEsv0Data.VlvEsv0DataFlg;
     : Ach_OutputIocVlvEsv0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvEsv1Data = Ioc_OutputSR1msIocVlvEsv1Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvEsv1Data 
     : Ach_OutputIocVlvEsv1Data.VlvEsv1DataFlg;
     : Ach_OutputIocVlvEsv1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNc0Data = Ioc_OutputSR1msIocVlvNc0Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc0Data 
     : Ach_OutputIocVlvNc0Data.VlvNc0DataFlg;
     : Ach_OutputIocVlvNc0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNc1Data = Ioc_OutputSR1msIocVlvNc1Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc1Data 
     : Ach_OutputIocVlvNc1Data.VlvNc1DataFlg;
     : Ach_OutputIocVlvNc1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNc2Data = Ioc_OutputSR1msIocVlvNc2Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc2Data 
     : Ach_OutputIocVlvNc2Data.VlvNc2DataFlg;
     : Ach_OutputIocVlvNc2Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvNc3Data = Ioc_OutputSR1msIocVlvNc3Data;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvNc3Data 
     : Ach_OutputIocVlvNc3Data.VlvNc3DataFlg;
     : Ach_OutputIocVlvNc3Data.ACH_TxValve_SET_POINT;
     =============================================================================*/

    Ach_OutputIocVlvRelayData = Ioc_OutputSR1msIocVlvRelayData;
    /*==============================================================================
    * Members of structure Ach_OutputIocVlvRelayData 
     : Ach_OutputIocVlvRelayData.VlvRelayDataFlg;
     : Ach_OutputIocVlvRelayData.ACH_Tx1_FS_CMD;
     =============================================================================*/

    Ach_OutputIocAdcSelWriteData = Ioc_OutputSR1msIocAdcSelWriteData;
    /*==============================================================================
    * Members of structure Ach_OutputIocAdcSelWriteData 
     : Ach_OutputIocAdcSelWriteData.AdcSelDataFlg;
     : Ach_OutputIocAdcSelWriteData.ACH_Tx13_ADC_SEL;
     =============================================================================*/

    /* Inter-Runnable single interface */
    Ach_OutputAcmAsicInitCompleteFlag = Acm_MainAcmAsicInitCompleteFlag;

    /* Execute  runnable */
    Ach_Output();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Ach_Output end
     **********************************************************************************************/

    /**********************************************************************************************
     Mtr_Processing_Diag start
     **********************************************************************************************/

     /* Structure interface */
    Mtr_Processing_DiagArbWhlVlvReqInfo = Task_1msBus.Mtr_Processing_DiagArbWhlVlvReqInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagArbWhlVlvReqInfo 
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FlOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FlIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FrOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.FrIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RlOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RlIvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RrOvReqData;
     : Mtr_Processing_DiagArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/

    /* Decomposed structure interface */
    Mtr_Processing_DiagEemFailData.Eem_Fail_Motor = Task_1msBus.Mtr_Processing_DiagEemFailData.Eem_Fail_Motor;

    /* Decomposed structure interface */
    Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = Task_1msBus.Mtr_Processing_DiagEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;

    /* Decomposed structure interface */
    Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild = Task_1msBus.Mtr_Processing_DiagMotRotgAgSigInfo.MotMechAngleSpdFild;

    Mtr_Processing_DiagWhlSpdInfo = Task_1msBus.Mtr_Processing_DiagWhlSpdInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagWhlSpdInfo 
     : Mtr_Processing_DiagWhlSpdInfo.FlWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.FrWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.RlWhlSpd;
     : Mtr_Processing_DiagWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    Mtr_Processing_DiagDiagHndlRequest = Task_1msBus.Mtr_Processing_DiagDiagHndlRequest;
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagDiagHndlRequest 
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Order;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Iq;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_Id;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_U_PWM;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_V_PWM;
     : Mtr_Processing_DiagDiagHndlRequest.DiagRequest_W_PWM;
     =============================================================================*/

    /* Decomposed structure interface */
    Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd = Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhUMeasd;
    Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd = Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhVMeasd;
    Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd = Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotCurrPhWMeasd;
    Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg = Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
    Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg = Task_1msBus.Mtr_Processing_DiagMtrProcessOutInfo.MotPosiAngle2_1_100Deg;

    /* Single interface */

    /* Inter-Runnable structure interface */
    /* Decomposed structure interface */
    Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt01Mon_Esc = Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt01Mon_Esc;
    Mtr_Processing_DiagPwrMonInfoEsc.VoltVBatt02Mon_Esc = Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt02Mon_Esc;

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Mtr_Processing_Diag();

    /* Structure interface */
    Task_1msBus.Mtr_Processing_DiagMtrProcessDataInf = Mtr_Processing_DiagMtrProcessDataInf;
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagMtrProcessDataInf 
     : Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState;
     : Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Mtr_Processing_DiagMtrDiagState = Mtr_Processing_DiagMtrDiagState;

    /**********************************************************************************************
     Mtr_Processing_Diag end
     **********************************************************************************************/

    /**********************************************************************************************
     Arbitrator_Mtr start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Arbitrator_MtrEemFailData.Eem_Fail_BBSSol = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSSol;
    Arbitrator_MtrEemFailData.Eem_Fail_ESCSol = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSol;
    Arbitrator_MtrEemFailData.Eem_Fail_FrontSol = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_FrontSol;
    Arbitrator_MtrEemFailData.Eem_Fail_RearSol = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_RearSol;
    Arbitrator_MtrEemFailData.Eem_Fail_Motor = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Motor;
    Arbitrator_MtrEemFailData.Eem_Fail_MPS = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MPS;
    Arbitrator_MtrEemFailData.Eem_Fail_MGD = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MGD;
    Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay;
    Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay;
    Arbitrator_MtrEemFailData.Eem_Fail_ECUHw = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ECUHw;
    Arbitrator_MtrEemFailData.Eem_Fail_ASIC = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ASIC;
    Arbitrator_MtrEemFailData.Eem_Fail_OverVolt = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_OverVolt;
    Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt;
    Arbitrator_MtrEemFailData.Eem_Fail_LowVolt = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_LowVolt;
    Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt;
    Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V;
    Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V;
    Arbitrator_MtrEemFailData.Eem_Fail_Yaw = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Yaw;
    Arbitrator_MtrEemFailData.Eem_Fail_Ay = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Ay;
    Arbitrator_MtrEemFailData.Eem_Fail_Ax = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Ax;
    Arbitrator_MtrEemFailData.Eem_Fail_Str = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Str;
    Arbitrator_MtrEemFailData.Eem_Fail_CirP1 = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP1;
    Arbitrator_MtrEemFailData.Eem_Fail_CirP2 = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP2;
    Arbitrator_MtrEemFailData.Eem_Fail_SimP = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SimP;
    Arbitrator_MtrEemFailData.Eem_Fail_BLS = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BLS;
    Arbitrator_MtrEemFailData.Eem_Fail_ESCSw = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSw;
    Arbitrator_MtrEemFailData.Eem_Fail_HDCSw = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCSw;
    Arbitrator_MtrEemFailData.Eem_Fail_AVHSw = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_AVHSw;
    Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay;
    Arbitrator_MtrEemFailData.Eem_Fail_EssRelay = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_EssRelay;
    Arbitrator_MtrEemFailData.Eem_Fail_GearR = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_GearR;
    Arbitrator_MtrEemFailData.Eem_Fail_Clutch = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_Clutch;
    Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake;
    Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT;
    Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF;
    Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid;
    Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp;
    Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp;
    Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp;
    Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp;
    Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine;
    Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine;
    Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut;
    Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut;
    Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut;
    Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut;
    Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut;
    Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding = Task_1msBus.Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding;

    //Arbitrator_MtrEemCtrlInhibitData = Task_1msBus.Arbitrator_MtrEemCtrlInhibitData;
    /*==============================================================================
    * Members of structure Arbitrator_MtrEemCtrlInhibitData 
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/

    /* Single interface */
    //Arbitrator_MtrEcuModeSts = Task_1msBus.Arbitrator_MtrEcuModeSts;
    Arbitrator_MtrIgnOnOffSts = Task_1msBus.Arbitrator_MtrIgnOnOffSts;
    Arbitrator_MtrIgnEdgeSts = Task_1msBus.Arbitrator_MtrIgnEdgeSts;
    Arbitrator_MtrDiagSci = Task_1msBus.Arbitrator_MtrDiagSci;
    Arbitrator_MtrDiagAhbSci = Task_1msBus.Arbitrator_MtrDiagAhbSci;

    /* Inter-Runnable structure interface */
    Arbitrator_MtrMotDqIRefMccInfo = Mcc_1msCtrlMotDqIRefMccInfo;
    /*==============================================================================
    * Members of structure Arbitrator_MtrMotDqIRefMccInfo 
     : Arbitrator_MtrMotDqIRefMccInfo.IdRef;
     : Arbitrator_MtrMotDqIRefMccInfo.IqRef;
     =============================================================================*/

    Arbitrator_MtrMtrProcessOutData = Mtr_Processing_DiagMtrProcessOutData;
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrProcessOutData 
     : Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe;
     : Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe;
     : Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM;
     : Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM;
     : Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM;
     =============================================================================*/

    /* Decomposed structure interface */
    Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState = Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState;

    /* Inter-Runnable single interface */
    Arbitrator_MtrVdcLinkFild = Msp_1msCtrlVdcLinkFild;
    Arbitrator_MtrMotCtrlMode = Mcc_1msCtrlMotCtrlMode;
    Arbitrator_MtrMotCtrlState = Mcc_1msCtrlMotCtrlState;

    /* Execute  runnable */
    Arbitrator_Mtr();

    /* Structure interface */
    Task_1msBus.Arbitrator_MtrMtrArbtratorData = Arbitrator_MtrMtrArbtratorData;
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrArbtratorData 
     : Arbitrator_MtrMtrArbtratorData.IqSelected;
     : Arbitrator_MtrMtrArbtratorData.IdSelected;
     : Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.MtrCtrlMode;
     =============================================================================*/

    Task_1msBus.Arbitrator_MtrMtrArbtratorInfo = Arbitrator_MtrMtrArbtratorInfo;
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrArbtratorInfo 
     : Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode;
     : Arbitrator_MtrMtrArbtratorInfo.MtrArbState;
     =============================================================================*/

    /* Single interface */
    Task_1msBus.Arbitrator_MtrMtrArbDriveState = Arbitrator_MtrMtrArbDriveState;

    /**********************************************************************************************
     Arbitrator_Mtr end
     **********************************************************************************************/


    /* Output */
    Task_1ms_Write_AdcIf_Conv1msSwTrigPwrInfo(&Task_1msBus.AdcIf_Conv1msSwTrigPwrInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigPwrInfo 
     : AdcIf_Conv1msSwTrigPwrInfo.Ext5vMon;
     : AdcIf_Conv1msSwTrigPwrInfo.CEMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Int5vMon;
     : AdcIf_Conv1msSwTrigPwrInfo.CSPMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vbatt01Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vbatt02Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.VddMon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vdd3Mon;
     : AdcIf_Conv1msSwTrigPwrInfo.Vdd5Mon;
     =============================================================================*/

    Task_1ms_Write_AdcIf_Conv1msSwTrigMotInfo(&Task_1msBus.AdcIf_Conv1msSwTrigMotInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv1msSwTrigMotInfo 
     : AdcIf_Conv1msSwTrigMotInfo.MotPwrMon;
     : AdcIf_Conv1msSwTrigMotInfo.StarMon;
     : AdcIf_Conv1msSwTrigMotInfo.UoutMon;
     : AdcIf_Conv1msSwTrigMotInfo.VoutMon;
     : AdcIf_Conv1msSwTrigMotInfo.WoutMon;
     =============================================================================*/

    Task_1ms_Write_Ach_InputAchSysPwrAsicInfo(&Task_1msBus.Ach_InputAchSysPwrAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchSysPwrAsicInfo 
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd1_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd2_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd3_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd4_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vint_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_dgndloss;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vpwr_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_long;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_too_short;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_ov;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_uv;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_STUCK_MON;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_OSC_FRQ_MON;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_3_WDOG_TO;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_AN_TRIM_CRC_RESULT;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_CRC_RESULT;
     : Ach_InputAchSysPwrAsicInfo.Ach_Asic_7_NVM_BUSY;
     =============================================================================*/

    Task_1ms_Write_Ach_InputAchValveAsicInfo(&Task_1msBus.Ach_InputAchValveAsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchValveAsicInfo 
     : Ach_InputAchValveAsicInfo.Ach_Asic_CP_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_CP_UV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_UV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VPWR_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_PMP_LD_ACT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_TURN_OFF;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_VDS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_FS_RVP_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_VHD_OV;
     : Ach_InputAchValveAsicInfo.Ach_Asic_T_SD_INT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No2_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_No3_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc2_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Nc3_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Tc1_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv0_UNDER_CURR;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_AVG_CURRENT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_PWM_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_CURR_SENSE;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_ADC_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_T_SD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_OPEN_LOAD;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_GND_LOSS;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LVT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_OVC;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_LS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_VGS_HS_FAULT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_HS_SHORT;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_LS_CLAMP_ON;
     : Ach_InputAchValveAsicInfo.Ach_Asic_Esv1_UNDER_CURR;
     =============================================================================*/

    Task_1ms_Write_Ach_InputAchWssPort0AsicInfo(&Task_1msBus.Ach_InputAchWssPort0AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort0AsicInfo 
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1ms_Write_Ach_InputAchWssPort1AsicInfo(&Task_1msBus.Ach_InputAchWssPort1AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort1AsicInfo 
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1ms_Write_Ach_InputAchWssPort2AsicInfo(&Task_1msBus.Ach_InputAchWssPort2AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort2AsicInfo 
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1ms_Write_Ach_InputAchWssPort3AsicInfo(&Task_1msBus.Ach_InputAchWssPort3AsicInfo);
    /*==============================================================================
    * Members of structure Ach_InputAchWssPort3AsicInfo 
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : Ach_InputAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/

    Task_1ms_Write_SentH_MainSentHPressureInfo(&Task_1msBus.SentH_MainSentHPressureInfo);
    /*==============================================================================
    * Members of structure SentH_MainSentHPressureInfo 
     : SentH_MainSentHPressureInfo.McpPresData1;
     : SentH_MainSentHPressureInfo.McpPresData2;
     : SentH_MainSentHPressureInfo.McpSentCrc;
     : SentH_MainSentHPressureInfo.McpSentData;
     : SentH_MainSentHPressureInfo.McpSentMsgId;
     : SentH_MainSentHPressureInfo.McpSentSerialCrc;
     : SentH_MainSentHPressureInfo.McpSentConfig;
     : SentH_MainSentHPressureInfo.McpSentInvalid;
     : SentH_MainSentHPressureInfo.MCPSentSerialInvalid;
     : SentH_MainSentHPressureInfo.Wlp1PresData1;
     : SentH_MainSentHPressureInfo.Wlp1PresData2;
     : SentH_MainSentHPressureInfo.Wlp1SentCrc;
     : SentH_MainSentHPressureInfo.Wlp1SentData;
     : SentH_MainSentHPressureInfo.Wlp1SentMsgId;
     : SentH_MainSentHPressureInfo.Wlp1SentSerialCrc;
     : SentH_MainSentHPressureInfo.Wlp1SentConfig;
     : SentH_MainSentHPressureInfo.Wlp1SentInvalid;
     : SentH_MainSentHPressureInfo.Wlp1SentSerialInvalid;
     : SentH_MainSentHPressureInfo.Wlp2PresData1;
     : SentH_MainSentHPressureInfo.Wlp2PresData2;
     : SentH_MainSentHPressureInfo.Wlp2SentCrc;
     : SentH_MainSentHPressureInfo.Wlp2SentData;
     : SentH_MainSentHPressureInfo.Wlp2SentMsgId;
     : SentH_MainSentHPressureInfo.Wlp2SentSerialCrc;
     : SentH_MainSentHPressureInfo.Wlp2SentConfig;
     : SentH_MainSentHPressureInfo.Wlp2SentInvalid;
     : SentH_MainSentHPressureInfo.Wlp2SentSerialInvalid;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputSR1msHalPressureInfo(&Task_1msBus.Ioc_InputSR1msHalPressureInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msHalPressureInfo 
     : Ioc_InputSR1msHalPressureInfo.McpPresData1;
     : Ioc_InputSR1msHalPressureInfo.McpPresData2;
     : Ioc_InputSR1msHalPressureInfo.McpSentCrc;
     : Ioc_InputSR1msHalPressureInfo.McpSentData;
     : Ioc_InputSR1msHalPressureInfo.McpSentMsgId;
     : Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.McpSentConfig;
     : Ioc_InputSR1msHalPressureInfo.Wlp1PresData1;
     : Ioc_InputSR1msHalPressureInfo.Wlp1PresData2;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentData;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig;
     : Ioc_InputSR1msHalPressureInfo.Wlp2PresData1;
     : Ioc_InputSR1msHalPressureInfo.Wlp2PresData2;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentData;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputSR1msMotMonInfo(&Task_1msBus.Ioc_InputSR1msMotMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotMonInfo 
     : Ioc_InputSR1msMotMonInfo.MotPwrVoltMon;
     : Ioc_InputSR1msMotMonInfo.MotStarMon;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputSR1msMotVoltsMonInfo(&Task_1msBus.Ioc_InputSR1msMotVoltsMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotVoltsMonInfo 
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon;
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon;
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputSR1msPedlSigMonInfo(&Task_1msBus.Ioc_InputSR1msPedlSigMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPedlSigMonInfo 
     : Ioc_InputSR1msPedlSigMonInfo.PdfSigMon;
     : Ioc_InputSR1msPedlSigMonInfo.PdtSigMon;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputSR1msPedlPwrMonInfo(&Task_1msBus.Ioc_InputSR1msPedlPwrMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPedlPwrMonInfo 
     : Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon;
     : Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputCS1msSwtMonInfo(&Task_1msBus.Ioc_InputCS1msSwtMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwtMonInfo 
     : Ioc_InputCS1msSwtMonInfo.AvhSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BflSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BlsSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BsSwtMon;
     : Ioc_InputCS1msSwtMonInfo.DoorSwtMon;
     : Ioc_InputCS1msSwtMonInfo.EscSwtMon;
     : Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon;
     : Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon;
     : Ioc_InputCS1msSwtMonInfo.HzrdSwtMon;
     : Ioc_InputCS1msSwtMonInfo.HdcSwtMon;
     : Ioc_InputCS1msSwtMonInfo.PbSwtMon;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputCS1msSwtMonInfoEsc(&Task_1msBus.Ioc_InputCS1msSwtMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwtMonInfoEsc 
     : Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc;
     =============================================================================*/

    Task_1ms_Write_Ioc_InputCS1msRlyMonInfo(&Task_1msBus.Ioc_InputCS1msRlyMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msRlyMonInfo 
     : Ioc_InputCS1msRlyMonInfo.RlyDbcMon;
     : Ioc_InputCS1msRlyMonInfo.RlyEssMon;
     : Ioc_InputCS1msRlyMonInfo.RlyFault;
     =============================================================================*/

    Task_1ms_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo(&Task_1msBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo);
    /*==============================================================================
    * Members of structure Vlvd_A3944_HndlrVlvdFF0DcdInfo 
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff;
     =============================================================================*/

    Task_1ms_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo(&Task_1msBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo);
    /*==============================================================================
    * Members of structure Vlvd_A3944_HndlrVlvdFF1DcdInfo 
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff;
     =============================================================================*/

    Task_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo(&Task_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo 
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV;
     =============================================================================*/

    Task_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo(&Task_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo 
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_VR;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_WD;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ROM;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ADCT;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_DSPU;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_FUSE;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_OV;
     =============================================================================*/

    Task_1ms_Write_Pedal_SenPdtBufInfo(&Task_1msBus.Pedal_SenPdtBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPdtBufInfo 
     : Pedal_SenPdtBufInfo.PdtRaw;
     =============================================================================*/

    Task_1ms_Write_Pedal_SenPdfBufInfo(&Task_1msBus.Pedal_SenPdfBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPdfBufInfo 
     : Pedal_SenPdfBufInfo.PdfRaw;
     =============================================================================*/

    Task_1ms_Write_Press_SenCircPBufInfo(&Task_1msBus.Press_SenCircPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenCircPBufInfo 
     : Press_SenCircPBufInfo.PrimCircPRaw;
     : Press_SenCircPBufInfo.SecdCircPRaw;
     =============================================================================*/

    Task_1ms_Write_Press_SenPistPBufInfo(&Task_1msBus.Press_SenPistPBufInfo);
    /*==============================================================================
    * Members of structure Press_SenPistPBufInfo 
     : Press_SenPistPBufInfo.PistPRaw;
     =============================================================================*/

    Task_1ms_Write_Press_SenPspBufInfo(&Task_1msBus.Press_SenPspBufInfo);
    /*==============================================================================
    * Members of structure Press_SenPspBufInfo 
     : Press_SenPspBufInfo.PedlSimPRaw;
     =============================================================================*/

    Task_1ms_Write_Press_SenPressCalcInfo(&Task_1msBus.Press_SenPressCalcInfo);
    /*==============================================================================
    * Members of structure Press_SenPressCalcInfo 
     : Press_SenPressCalcInfo.PressP_SimP_1_100_bar;
     : Press_SenPressCalcInfo.PressP_CirP1_1_100_bar;
     : Press_SenPressCalcInfo.PressP_CirP2_1_100_bar;
     : Press_SenPressCalcInfo.SimPMoveAvr;
     : Press_SenPressCalcInfo.CirPMoveAvr;
     : Press_SenPressCalcInfo.CirP2MoveAvr;
     =============================================================================*/

    Task_1ms_Write_Spc_1msCtrlPedlTrvlFild1msInfo(&Task_1msBus.Spc_1msCtrlPedlTrvlFild1msInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlPedlTrvlFild1msInfo 
     : Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/

    Task_1ms_Write_Det_1msCtrlBrkPedlStatus1msInfo(&Task_1msBus.Det_1msCtrlBrkPedlStatus1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlBrkPedlStatus1msInfo 
     : Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/

    Task_1ms_Write_Det_1msCtrlPedlTrvlRate1msInfo(&Task_1msBus.Det_1msCtrlPedlTrvlRate1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlPedlTrvlRate1msInfo 
     : Det_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/

    Task_1ms_Write_Mcc_1msCtrlMotDqIRefMccInfo(&Task_1msBus.Mcc_1msCtrlMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlMotDqIRefMccInfo 
     : Mcc_1msCtrlMotDqIRefMccInfo.IdRef;
     : Mcc_1msCtrlMotDqIRefMccInfo.IqRef;
     =============================================================================*/

    Task_1ms_Write_Mcc_1msCtrlIdbMotPosnInfo(&Task_1msBus.Mcc_1msCtrlIdbMotPosnInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlIdbMotPosnInfo 
     : Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn;
     =============================================================================*/

    Task_1ms_Write_Mcc_1msCtrlFluxWeakengStInfo(&Task_1msBus.Mcc_1msCtrlFluxWeakengStInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlFluxWeakengStInfo 
     : Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg;
     : Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld;
     =============================================================================*/

    Task_1ms_Write_Ses_CtrlNormVlvReqSesInfo(&Task_1msBus.Ses_CtrlNormVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlNormVlvReqSesInfo 
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen;
     =============================================================================*/

    Task_1ms_Write_Ses_CtrlWhlVlvReqSesInfo(&Task_1msBus.Ses_CtrlWhlVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlWhlVlvReqSesInfo 
     : Ses_CtrlWhlVlvReqSesInfo.FlIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.FlIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen;
     =============================================================================*/

    Task_1ms_Write_Ses_CtrlMotOrgSetStInfo(&Task_1msBus.Ses_CtrlMotOrgSetStInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotOrgSetStInfo 
     : Ses_CtrlMotOrgSetStInfo.MotOrgSetErr;
     : Ses_CtrlMotOrgSetStInfo.MotOrgSet;
     =============================================================================*/

    Task_1ms_Write_Ses_CtrlMotDqIRefSesInfo(&Task_1msBus.Ses_CtrlMotDqIRefSesInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotDqIRefSesInfo 
     : Ses_CtrlMotDqIRefSesInfo.IdRef;
     : Ses_CtrlMotDqIRefSesInfo.IqRef;
     =============================================================================*/

    Task_1ms_Write_Ses_CtrlPwrPistStBfMotOrgSetInfo(&Task_1msBus.Ses_CtrlPwrPistStBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlPwrPistStBfMotOrgSetInfo 
     : Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
     : Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
     =============================================================================*/

    Task_1ms_Write_Mgd_TLE9180_HndlrMgdDcdInfo(&Task_1msBus.Mgd_TLE9180_HndlrMgdDcdInfo);
    /*==============================================================================
    * Members of structure Mgd_TLE9180_HndlrMgdDcdInfo 
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/

    Task_1ms_Write_Mtr_Processing_DiagMtrProcessDataInf(&Task_1msBus.Mtr_Processing_DiagMtrProcessDataInf);
    /*==============================================================================
    * Members of structure Mtr_Processing_DiagMtrProcessDataInf 
     : Mtr_Processing_DiagMtrProcessDataInf.MtrProCalibrationState;
     : Mtr_Processing_DiagMtrProcessDataInf.MtrCaliResult;
     =============================================================================*/

    Task_1ms_Write_Arbitrator_MtrMtrArbtratorData(&Task_1msBus.Arbitrator_MtrMtrArbtratorData);
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrArbtratorData 
     : Arbitrator_MtrMtrArbtratorData.IqSelected;
     : Arbitrator_MtrMtrArbtratorData.IdSelected;
     : Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.MtrCtrlMode;
     =============================================================================*/

    Task_1ms_Write_Arbitrator_MtrMtrArbtratorInfo(&Task_1msBus.Arbitrator_MtrMtrArbtratorInfo);
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrArbtratorInfo 
     : Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode;
     : Arbitrator_MtrMtrArbtratorInfo.MtrArbState;
     =============================================================================*/

    Task_1ms_Write_AdcIf_Conv1msAdcInvalid(&Task_1msBus.AdcIf_Conv1msAdcInvalid);
    Task_1ms_Write_Ach_InputAchAsicInvalid(&Task_1msBus.Ach_InputAchAsicInvalid);
    Task_1ms_Write_Ioc_InputSR1msVBatt1Mon(&Task_1msBus.Ioc_InputSR1msVBatt1Mon);
    Task_1ms_Write_Ioc_InputSR1msVBatt2Mon(&Task_1msBus.Ioc_InputSR1msVBatt2Mon);
    Task_1ms_Write_Ioc_InputSR1msFspCbsHMon(&Task_1msBus.Ioc_InputSR1msFspCbsHMon);
    Task_1ms_Write_Ioc_InputSR1msFspCbsMon(&Task_1msBus.Ioc_InputSR1msFspCbsMon);
    Task_1ms_Write_Ioc_InputSR1msFspAbsHMon(&Task_1msBus.Ioc_InputSR1msFspAbsHMon);
    Task_1ms_Write_Ioc_InputSR1msFspAbsMon(&Task_1msBus.Ioc_InputSR1msFspAbsMon);
    Task_1ms_Write_Ioc_InputSR1msExt5vMon(&Task_1msBus.Ioc_InputSR1msExt5vMon);
    Task_1ms_Write_Ioc_InputSR1msCEMon(&Task_1msBus.Ioc_InputSR1msCEMon);
    Task_1ms_Write_Ioc_InputSR1msVdd1Mon(&Task_1msBus.Ioc_InputSR1msVdd1Mon);
    Task_1ms_Write_Ioc_InputSR1msVdd2Mon(&Task_1msBus.Ioc_InputSR1msVdd2Mon);
    Task_1ms_Write_Ioc_InputSR1msVdd3Mon(&Task_1msBus.Ioc_InputSR1msVdd3Mon);
    Task_1ms_Write_Ioc_InputSR1msVdd4Mon(&Task_1msBus.Ioc_InputSR1msVdd4Mon);
    Task_1ms_Write_Ioc_InputSR1msVdd5Mon(&Task_1msBus.Ioc_InputSR1msVdd5Mon);
    Task_1ms_Write_Ioc_InputSR1msVddMon(&Task_1msBus.Ioc_InputSR1msVddMon);
    Task_1ms_Write_Ioc_InputSR1msCspMon(&Task_1msBus.Ioc_InputSR1msCspMon);
    Task_1ms_Write_Ioc_InputCS1msRsmDbcMon(&Task_1msBus.Ioc_InputCS1msRsmDbcMon);
    Task_1ms_Write_Vlvd_A3944_HndlrVlvdInvalid(&Task_1msBus.Vlvd_A3944_HndlrVlvdInvalid);
    Task_1ms_Write_Mps_TLE5012_Hndlr_1msMpsInvalid(&Task_1msBus.Mps_TLE5012_Hndlr_1msMpsInvalid);
    Task_1ms_Write_Msp_1msCtrlVdcLinkFild(&Task_1msBus.Msp_1msCtrlVdcLinkFild);
    Task_1ms_Write_Pct_1msCtrlPreBoostMod(&Task_1msBus.Pct_1msCtrlPreBoostMod);
    Task_1ms_Write_Mcc_1msCtrlMotCtrlMode(&Task_1msBus.Mcc_1msCtrlMotCtrlMode);
    Task_1ms_Write_Mcc_1msCtrlMotCtrlState(&Task_1msBus.Mcc_1msCtrlMotCtrlState);
    Task_1ms_Write_Mcc_1msCtrlMotICtrlFadeOutState(&Task_1msBus.Mcc_1msCtrlMotICtrlFadeOutState);
    Task_1ms_Write_Mcc_1msCtrlStkRecvryStabnEndOK(&Task_1msBus.Mcc_1msCtrlStkRecvryStabnEndOK);
    Task_1ms_Write_Acm_MainAcmAsicInitCompleteFlag(&Task_1msBus.Acm_MainAcmAsicInitCompleteFlag);
    Task_1ms_Write_Mgd_TLE9180_HndlrMgdInvalid(&Task_1msBus.Mgd_TLE9180_HndlrMgdInvalid);
    Task_1ms_Write_Mtr_Processing_DiagMtrDiagState(&Task_1msBus.Mtr_Processing_DiagMtrDiagState);
    Task_1ms_Write_Arbitrator_MtrMtrArbDriveState(&Task_1msBus.Arbitrator_MtrMtrArbDriveState);
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TASK_1MS_STOP_SEC_CODE
#include "Task_1ms_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
