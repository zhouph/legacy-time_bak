/**
 * @defgroup Task_1ms_Types Task_1ms_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_1ms_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_1MS_TYPES_H_
#define TASK_1MS_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Msp_CtrlMotRotgAgSigInfo_t Mcc_1msCtrlMotRotgAgSigInfo;
    Vat_CtrlIdbVlvActInfo_t Mcc_1msCtrlIdbVlvActInfo;
    Pct_5msCtrlStkRecvryActnIfInfo_t Mcc_1msCtrlStkRecvryActnIfInfo;
    Acmctl_CtrlMotDqIMeasdInfo_t Ses_CtrlMotDqIMeasdInfo;
    Msp_CtrlMotRotgAgSigInfo_t Ses_CtrlMotRotgAgSigInfo;
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
    Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrNormVlvReqInfo;
    Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrWhlVlvReqInfo;
    Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrBalVlvReqInfo;
    Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputSR1msFsrCbsDrvInfo;
    Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputSR1msFsrAbsDrvInfo;
    Rly_ActrRlyDrvInfo_t Ioc_OutputCS1msRlyDrvInfo;
    Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputCS1msFsrAbsDrvInfo;
    Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputCS1msFsrCbsDrvInfo;
    SenPwrM_MainSenPwrMonitor_t Ioc_OutputCS1msSenPwrMonitorData;
    Arbitrator_VlvArbWhlVlvReqInfo_t Mtr_Processing_DiagArbWhlVlvReqInfo;
    Eem_MainEemFailData_t Mtr_Processing_DiagEemFailData;
    Eem_MainEemCtrlInhibitData_t Mtr_Processing_DiagEemCtrlInhibitData;
    Msp_CtrlMotRotgAgSigInfo_t Mtr_Processing_DiagMotRotgAgSigInfo;
    Wss_SenWhlSpdInfo_t Mtr_Processing_DiagWhlSpdInfo;
    Diag_HndlrDiagHndlRequest_t Mtr_Processing_DiagDiagHndlRequest;
    Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_DiagMtrProcessOutInfo;
    Eem_MainEemFailData_t Arbitrator_MtrEemFailData;
    Eem_MainEemCtrlInhibitData_t Arbitrator_MtrEemCtrlInhibitData;
    Mom_HndlrEcuModeSts_t AdcIf_Conv1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv1msFuncInhibitAdcifSts;
    Mom_HndlrEcuModeSts_t SentH_MainEcuModeSts;
    Mom_HndlrEcuModeSts_t Ioc_InputSR1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR1msFuncInhibitIocSts;
    Mom_HndlrEcuModeSts_t Ioc_InputCS1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputCS1msFuncInhibitIocSts;
    Mom_HndlrEcuModeSts_t CanTrv_TLE6251_HndlrEcuModeSts;
    Eem_SuspcDetnFuncInhibitCanTrcvSts_t CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts;
    Mom_HndlrEcuModeSts_t Vlvd_A3944_HndlrEcuModeSts;
    Eem_SuspcDetnFuncInhibitVlvdSts_t Vlvd_A3944_HndlrFuncInhibitVlvdSts;
    Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_1msEcuModeSts;
    Mom_HndlrEcuModeSts_t Pedal_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenFuncInhibitPedalSts;
    Mom_HndlrEcuModeSts_t Press_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitPressSts_t Press_SenFuncInhibitPressSts;
    Acmio_SenVdcLink_t Msp_1msCtrlVdcLink;
    Mom_HndlrEcuModeSts_t Spc_1msCtrlEcuModeSts;
    Mom_HndlrEcuModeSts_t Det_1msCtrlEcuModeSts;
    Mom_HndlrEcuModeSts_t Pct_1msCtrlEcuModeSts;
    Pct_5msCtrlPCtrlSt_t Pct_1msCtrlPCtrlSt;
    Mom_HndlrEcuModeSts_t Mcc_1msCtrlEcuModeSts;
    Eem_SuspcDetnFuncInhibitMccSts_t Mcc_1msCtrlFuncInhibitMccSts;
    Pct_5msCtrlTgtDeltaStkType_t Mcc_1msCtrlTgtDeltaStkType;
    Pct_5msCtrlPCtrlBoostMod_t Mcc_1msCtrlPCtrlBoostMod;
    Pct_5msCtrlPCtrlFadeoutSt_t Mcc_1msCtrlPCtrlFadeoutSt;
    Pct_5msCtrlPCtrlSt_t Mcc_1msCtrlPCtrlSt;
    Pct_5msCtrlInVlvAllCloseReq_t Mcc_1msCtrlInVlvAllCloseReq;
    Pct_5msCtrlPChamberVolume_t Mcc_1msCtrlPChamberVolume;
    Pct_5msCtrlTarDeltaStk_t Mcc_1msCtrlTarDeltaStk;
    Pct_5msCtrlFinalTarPFromPCtrl_t Mcc_1msCtrlFinalTarPFromPCtrl;
    Mom_HndlrEcuModeSts_t Ses_CtrlEcuModeSts;
    Eem_SuspcDetnFuncInhibitSesSts_t Ses_CtrlFuncInhibitSesSts;
    Mom_HndlrEcuModeSts_t Vlv_ActrEcuModeSts;
    Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrFuncInhibitVlvSts;
    Vlv_ActrSyncVlvSync_t Vlv_ActrVlvSync;
    Prly_HndlrPrlyEcuInhibit_t Aka_MainPrlyEcuInhibit;
    Mom_HndlrEcuModeSts_t Mgd_TLE9180_HndlrEcuModeSts;
    Mom_HndlrEcuModeSts_t Ioc_OutputSR1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputSR1msFuncInhibitIocSts;
    Mom_HndlrEcuModeSts_t Ioc_OutputCS1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS1msFuncInhibitIocSts;
    Fsr_ActrFsrDcMtrShutDwn_t Ioc_OutputCS1msFsrDcMtrShutDwn;
    Fsr_ActrFsrEnDrDrv_t Ioc_OutputCS1msFsrEnDrDrv;
    Mom_HndlrEcuModeSts_t Arbitrator_MtrEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Arbitrator_MtrIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Arbitrator_MtrIgnEdgeSts;
    Diag_HndlrDiagSci_t Arbitrator_MtrDiagSci;
    Diag_HndlrDiagAhbSci_t Arbitrator_MtrDiagAhbSci;

/* Output Data Element */
    AdcIf_Conv1msSwTrigPwrInfo_t AdcIf_Conv1msSwTrigPwrInfo;
    AdcIf_Conv1msSwTrigMotInfo_t AdcIf_Conv1msSwTrigMotInfo;
    Ach_InputAchSysPwrAsicInfo_t Ach_InputAchSysPwrAsicInfo;
    Ach_InputAchValveAsicInfo_t Ach_InputAchValveAsicInfo;
    Ach_InputAchWssPort0AsicInfo_t Ach_InputAchWssPort0AsicInfo;
    Ach_InputAchWssPort1AsicInfo_t Ach_InputAchWssPort1AsicInfo;
    Ach_InputAchWssPort2AsicInfo_t Ach_InputAchWssPort2AsicInfo;
    Ach_InputAchWssPort3AsicInfo_t Ach_InputAchWssPort3AsicInfo;
    SentH_MainSentHPressureInfo_t SentH_MainSentHPressureInfo;
    Ioc_InputSR1msHalPressureInfo_t Ioc_InputSR1msHalPressureInfo;
    Ioc_InputSR1msMotMonInfo_t Ioc_InputSR1msMotMonInfo;
    Ioc_InputSR1msMotVoltsMonInfo_t Ioc_InputSR1msMotVoltsMonInfo;
    Ioc_InputSR1msPedlSigMonInfo_t Ioc_InputSR1msPedlSigMonInfo;
    Ioc_InputSR1msPedlPwrMonInfo_t Ioc_InputSR1msPedlPwrMonInfo;
    Ioc_InputCS1msSwtMonInfo_t Ioc_InputCS1msSwtMonInfo;
    Ioc_InputCS1msSwtMonInfoEsc_t Ioc_InputCS1msSwtMonInfoEsc;
    Ioc_InputCS1msRlyMonInfo_t Ioc_InputCS1msRlyMonInfo;
    Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Vlvd_A3944_HndlrVlvdFF0DcdInfo;
    Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Vlvd_A3944_HndlrVlvdFF1DcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
    Pedal_SenPdtBufInfo_t Pedal_SenPdtBufInfo;
    Pedal_SenPdfBufInfo_t Pedal_SenPdfBufInfo;
    Press_SenCircPBufInfo_t Press_SenCircPBufInfo;
    Press_SenPistPBufInfo_t Press_SenPistPBufInfo;
    Press_SenPspBufInfo_t Press_SenPspBufInfo;
    Press_SenPressCalcInfo_t Press_SenPressCalcInfo;
    Spc_1msCtrlPedlTrvlFild1msInfo_t Spc_1msCtrlPedlTrvlFild1msInfo;
    Det_1msCtrlBrkPedlStatus1msInfo_t Det_1msCtrlBrkPedlStatus1msInfo;
    Det_1msCtrlPedlTrvlRate1msInfo_t Det_1msCtrlPedlTrvlRate1msInfo;
    Mcc_1msCtrlMotDqIRefMccInfo_t Mcc_1msCtrlMotDqIRefMccInfo;
    Mcc_1msCtrlIdbMotPosnInfo_t Mcc_1msCtrlIdbMotPosnInfo;
    Mcc_1msCtrlFluxWeakengStInfo_t Mcc_1msCtrlFluxWeakengStInfo;
    Ses_CtrlNormVlvReqSesInfo_t Ses_CtrlNormVlvReqSesInfo;
    Ses_CtrlWhlVlvReqSesInfo_t Ses_CtrlWhlVlvReqSesInfo;
    Ses_CtrlMotOrgSetStInfo_t Ses_CtrlMotOrgSetStInfo;
    Ses_CtrlMotDqIRefSesInfo_t Ses_CtrlMotDqIRefSesInfo;
    Ses_CtrlPwrPistStBfMotOrgSetInfo_t Ses_CtrlPwrPistStBfMotOrgSetInfo;
    Mgd_TLE9180_HndlrMgdDcdInfo_t Mgd_TLE9180_HndlrMgdDcdInfo;
    Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_DiagMtrProcessDataInf;
    Arbitrator_MtrMtrArbitratorData_t Arbitrator_MtrMtrArbtratorData;
    Arbitrator_MtrMtrArbitratorInfo_t Arbitrator_MtrMtrArbtratorInfo;
    AdcIf_Conv1msAdcInvalid_t AdcIf_Conv1msAdcInvalid;
    Ach_InputAchAsicInvalidInfo_t Ach_InputAchAsicInvalid;
    Ioc_InputSR1msVBatt1Mon_t Ioc_InputSR1msVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t Ioc_InputSR1msVBatt2Mon;
    Ioc_InputSR1msFspCbsHMon_t Ioc_InputSR1msFspCbsHMon;
    Ioc_InputSR1msFspCbsMon_t Ioc_InputSR1msFspCbsMon;
    Ioc_InputSR1msFspAbsHMon_t Ioc_InputSR1msFspAbsHMon;
    Ioc_InputSR1msFspAbsMon_t Ioc_InputSR1msFspAbsMon;
    Ioc_InputSR1msExt5vMon_t Ioc_InputSR1msExt5vMon;
    Ioc_InputSR1msCEMon_t Ioc_InputSR1msCEMon;
    Ioc_InputSR1msVdd1Mon_t Ioc_InputSR1msVdd1Mon;
    Ioc_InputSR1msVdd2Mon_t Ioc_InputSR1msVdd2Mon;
    Ioc_InputSR1msVdd3Mon_t Ioc_InputSR1msVdd3Mon;
    Ioc_InputSR1msVdd4Mon_t Ioc_InputSR1msVdd4Mon;
    Ioc_InputSR1msVdd5Mon_t Ioc_InputSR1msVdd5Mon;
    Ioc_InputSR1msVddMon_t Ioc_InputSR1msVddMon;
    Ioc_InputSR1msCspMon_t Ioc_InputSR1msCspMon;
    Ioc_InputCS1msRsmDbcMon_t Ioc_InputCS1msRsmDbcMon;
    Vlvd_A3944_HndlrVlvdInvalid_t Vlvd_A3944_HndlrVlvdInvalid;
    Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012_Hndlr_1msMpsInvalid;
    Msp_1msCtrlVdcLinkFild_t Msp_1msCtrlVdcLinkFild;
    Pct_1msCtrlPreBoostMod_t Pct_1msCtrlPreBoostMod;
    Mcc_1msCtrlMotCtrlMode_t Mcc_1msCtrlMotCtrlMode;
    Mcc_1msCtrlMotCtrlState_t Mcc_1msCtrlMotCtrlState;
    Mcc_1msCtrlMotICtrlFadeOutState_t Mcc_1msCtrlMotICtrlFadeOutState;
    Mcc_1msCtrlStkRecvryStabnEndOK_t Mcc_1msCtrlStkRecvryStabnEndOK;
    Acm_MainAcmAsicInitCompleteFlag_t Acm_MainAcmAsicInitCompleteFlag;
    Mgd_TLE9180_HndlrMgdInvalid_t Mgd_TLE9180_HndlrMgdInvalid;
    Mtr_Processing_DiagMtrDiagState_t Mtr_Processing_DiagMtrDiagState;
    Arbitrator_MtrMtrArbDriveState_t Arbitrator_MtrMtrArbDriveState;
}Task_1ms_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_1MS_TYPES_H_ */
/** @} */
