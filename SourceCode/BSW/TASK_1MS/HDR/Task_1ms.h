/**
 * @defgroup Task_1ms Task_1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_1MS_H_
#define TASK_1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_1ms_Types.h"
#include "Task_1ms_Cfg.h"
#include "Task_1ms_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define TASK_1MS_MODULE_ID      (0)
 #define TASK_1MS_MAJOR_VERSION  (2)
 #define TASK_1MS_MINOR_VERSION  (0)
 #define TASK_1MS_PATCH_VERSION  (0)
 #define TASK_1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Task_1ms_HdrBusType Task_1msBus;

/* Version Info */
extern const SwcVersionInfo_t Task_1msVersionInfo;

/* Input Data Element */
extern Msp_CtrlMotRotgAgSigInfo_t Task_1ms_Mcc_1msCtrlMotRotgAgSigInfo;
extern Vat_CtrlIdbVlvActInfo_t Task_1ms_Mcc_1msCtrlIdbVlvActInfo;
extern Pct_5msCtrlStkRecvryActnIfInfo_t Task_1ms_Mcc_1msCtrlStkRecvryActnIfInfo;
extern Acmctl_CtrlMotDqIMeasdInfo_t Task_1ms_Ses_CtrlMotDqIMeasdInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Task_1ms_Ses_CtrlMotRotgAgSigInfo;
extern Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Task_1ms_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
extern Vlv_ActrSyncNormVlvReqInfo_t Task_1ms_Vlv_ActrNormVlvReqInfo;
extern Vlv_ActrSyncWhlVlvReqInfo_t Task_1ms_Vlv_ActrWhlVlvReqInfo;
extern Vlv_ActrSyncBalVlvReqInfo_t Task_1ms_Vlv_ActrBalVlvReqInfo;
extern Fsr_ActrFsrCbsDrvInfo_t Task_1ms_Ioc_OutputSR1msFsrCbsDrvInfo;
extern Fsr_ActrFsrAbsDrvInfo_t Task_1ms_Ioc_OutputSR1msFsrAbsDrvInfo;
extern Rly_ActrRlyDrvInfo_t Task_1ms_Ioc_OutputCS1msRlyDrvInfo;
extern Fsr_ActrFsrAbsDrvInfo_t Task_1ms_Ioc_OutputCS1msFsrAbsDrvInfo;
extern Fsr_ActrFsrCbsDrvInfo_t Task_1ms_Ioc_OutputCS1msFsrCbsDrvInfo;
extern SenPwrM_MainSenPwrMonitor_t Task_1ms_Ioc_OutputCS1msSenPwrMonitorData;
extern Arbitrator_VlvArbWhlVlvReqInfo_t Task_1ms_Mtr_Processing_DiagArbWhlVlvReqInfo;
extern Eem_MainEemFailData_t Task_1ms_Mtr_Processing_DiagEemFailData;
extern Eem_MainEemCtrlInhibitData_t Task_1ms_Mtr_Processing_DiagEemCtrlInhibitData;
extern Msp_CtrlMotRotgAgSigInfo_t Task_1ms_Mtr_Processing_DiagMotRotgAgSigInfo;
extern Wss_SenWhlSpdInfo_t Task_1ms_Mtr_Processing_DiagWhlSpdInfo;
extern Diag_HndlrDiagHndlRequest_t Task_1ms_Mtr_Processing_DiagDiagHndlRequest;
extern Mtr_Processing_SigMtrProcessOutInfo_t Task_1ms_Mtr_Processing_DiagMtrProcessOutInfo;
extern Eem_MainEemFailData_t Task_1ms_Arbitrator_MtrEemFailData;
extern Eem_MainEemCtrlInhibitData_t Task_1ms_Arbitrator_MtrEemCtrlInhibitData;
extern Mom_HndlrEcuModeSts_t Task_1ms_AdcIf_Conv1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAdcifSts_t Task_1ms_AdcIf_Conv1msFuncInhibitAdcifSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_SentH_MainEcuModeSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Ioc_InputSR1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_InputSR1msFuncInhibitIocSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Ioc_InputCS1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_InputCS1msFuncInhibitIocSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_CanTrv_TLE6251_HndlrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitCanTrcvSts_t Task_1ms_CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Vlvd_A3944_HndlrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitVlvdSts_t Task_1ms_Vlvd_A3944_HndlrFuncInhibitVlvdSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Mps_TLE5012_Hndlr_1msEcuModeSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Pedal_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitPedalSts_t Task_1ms_Pedal_SenFuncInhibitPedalSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Press_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitPressSts_t Task_1ms_Press_SenFuncInhibitPressSts;
extern Acmio_SenVdcLink_t Task_1ms_Msp_1msCtrlVdcLink;
extern Mom_HndlrEcuModeSts_t Task_1ms_Spc_1msCtrlEcuModeSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Det_1msCtrlEcuModeSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Pct_1msCtrlEcuModeSts;
extern Pct_5msCtrlPCtrlSt_t Task_1ms_Pct_1msCtrlPCtrlSt;
extern Mom_HndlrEcuModeSts_t Task_1ms_Mcc_1msCtrlEcuModeSts;
extern Eem_SuspcDetnFuncInhibitMccSts_t Task_1ms_Mcc_1msCtrlFuncInhibitMccSts;
extern Pct_5msCtrlTgtDeltaStkType_t Task_1ms_Mcc_1msCtrlTgtDeltaStkType;
extern Pct_5msCtrlPCtrlBoostMod_t Task_1ms_Mcc_1msCtrlPCtrlBoostMod;
extern Pct_5msCtrlPCtrlFadeoutSt_t Task_1ms_Mcc_1msCtrlPCtrlFadeoutSt;
extern Pct_5msCtrlPCtrlSt_t Task_1ms_Mcc_1msCtrlPCtrlSt;
extern Pct_5msCtrlInVlvAllCloseReq_t Task_1ms_Mcc_1msCtrlInVlvAllCloseReq;
extern Pct_5msCtrlPChamberVolume_t Task_1ms_Mcc_1msCtrlPChamberVolume;
extern Pct_5msCtrlTarDeltaStk_t Task_1ms_Mcc_1msCtrlTarDeltaStk;
extern Pct_5msCtrlFinalTarPFromPCtrl_t Task_1ms_Mcc_1msCtrlFinalTarPFromPCtrl;
extern Mom_HndlrEcuModeSts_t Task_1ms_Ses_CtrlEcuModeSts;
extern Eem_SuspcDetnFuncInhibitSesSts_t Task_1ms_Ses_CtrlFuncInhibitSesSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Vlv_ActrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitVlvSts_t Task_1ms_Vlv_ActrFuncInhibitVlvSts;
extern Vlv_ActrSyncVlvSync_t Task_1ms_Vlv_ActrVlvSync;
extern Prly_HndlrPrlyEcuInhibit_t Task_1ms_Aka_MainPrlyEcuInhibit;
extern Mom_HndlrEcuModeSts_t Task_1ms_Mgd_TLE9180_HndlrEcuModeSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Ioc_OutputSR1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_OutputSR1msFuncInhibitIocSts;
extern Mom_HndlrEcuModeSts_t Task_1ms_Ioc_OutputCS1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_1ms_Ioc_OutputCS1msFuncInhibitIocSts;
extern Fsr_ActrFsrDcMtrShutDwn_t Task_1ms_Ioc_OutputCS1msFsrDcMtrShutDwn;
extern Fsr_ActrFsrEnDrDrv_t Task_1ms_Ioc_OutputCS1msFsrEnDrDrv;
extern Mom_HndlrEcuModeSts_t Task_1ms_Arbitrator_MtrEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_1ms_Arbitrator_MtrIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_1ms_Arbitrator_MtrIgnEdgeSts;
extern Diag_HndlrDiagSci_t Task_1ms_Arbitrator_MtrDiagSci;
extern Diag_HndlrDiagAhbSci_t Task_1ms_Arbitrator_MtrDiagAhbSci;

/* Output Data Element */
extern AdcIf_Conv1msSwTrigPwrInfo_t Task_1ms_AdcIf_Conv1msSwTrigPwrInfo;
extern AdcIf_Conv1msSwTrigMotInfo_t Task_1ms_AdcIf_Conv1msSwTrigMotInfo;
extern Ach_InputAchSysPwrAsicInfo_t Task_1ms_Ach_InputAchSysPwrAsicInfo;
extern Ach_InputAchValveAsicInfo_t Task_1ms_Ach_InputAchValveAsicInfo;
extern Ach_InputAchWssPort0AsicInfo_t Task_1ms_Ach_InputAchWssPort0AsicInfo;
extern Ach_InputAchWssPort1AsicInfo_t Task_1ms_Ach_InputAchWssPort1AsicInfo;
extern Ach_InputAchWssPort2AsicInfo_t Task_1ms_Ach_InputAchWssPort2AsicInfo;
extern Ach_InputAchWssPort3AsicInfo_t Task_1ms_Ach_InputAchWssPort3AsicInfo;
extern SentH_MainSentHPressureInfo_t Task_1ms_SentH_MainSentHPressureInfo;
extern Ioc_InputSR1msHalPressureInfo_t Task_1ms_Ioc_InputSR1msHalPressureInfo;
extern Ioc_InputSR1msMotMonInfo_t Task_1ms_Ioc_InputSR1msMotMonInfo;
extern Ioc_InputSR1msMotVoltsMonInfo_t Task_1ms_Ioc_InputSR1msMotVoltsMonInfo;
extern Ioc_InputSR1msPedlSigMonInfo_t Task_1ms_Ioc_InputSR1msPedlSigMonInfo;
extern Ioc_InputSR1msPedlPwrMonInfo_t Task_1ms_Ioc_InputSR1msPedlPwrMonInfo;
extern Ioc_InputCS1msSwtMonInfo_t Task_1ms_Ioc_InputCS1msSwtMonInfo;
extern Ioc_InputCS1msSwtMonInfoEsc_t Task_1ms_Ioc_InputCS1msSwtMonInfoEsc;
extern Ioc_InputCS1msRlyMonInfo_t Task_1ms_Ioc_InputCS1msRlyMonInfo;
extern Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Task_1ms_Vlvd_A3944_HndlrVlvdFF0DcdInfo;
extern Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Task_1ms_Vlvd_A3944_HndlrVlvdFF1DcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Task_1ms_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Task_1ms_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
extern Pedal_SenPdtBufInfo_t Task_1ms_Pedal_SenPdtBufInfo;
extern Pedal_SenPdfBufInfo_t Task_1ms_Pedal_SenPdfBufInfo;
extern Press_SenCircPBufInfo_t Task_1ms_Press_SenCircPBufInfo;
extern Press_SenPistPBufInfo_t Task_1ms_Press_SenPistPBufInfo;
extern Press_SenPspBufInfo_t Task_1ms_Press_SenPspBufInfo;
extern Press_SenPressCalcInfo_t Task_1ms_Press_SenPressCalcInfo;
extern Spc_1msCtrlPedlTrvlFild1msInfo_t Task_1ms_Spc_1msCtrlPedlTrvlFild1msInfo;
extern Det_1msCtrlBrkPedlStatus1msInfo_t Task_1ms_Det_1msCtrlBrkPedlStatus1msInfo;
extern Det_1msCtrlPedlTrvlRate1msInfo_t Task_1ms_Det_1msCtrlPedlTrvlRate1msInfo;
extern Mcc_1msCtrlMotDqIRefMccInfo_t Task_1ms_Mcc_1msCtrlMotDqIRefMccInfo;
extern Mcc_1msCtrlIdbMotPosnInfo_t Task_1ms_Mcc_1msCtrlIdbMotPosnInfo;
extern Mcc_1msCtrlFluxWeakengStInfo_t Task_1ms_Mcc_1msCtrlFluxWeakengStInfo;
extern Ses_CtrlNormVlvReqSesInfo_t Task_1ms_Ses_CtrlNormVlvReqSesInfo;
extern Ses_CtrlWhlVlvReqSesInfo_t Task_1ms_Ses_CtrlWhlVlvReqSesInfo;
extern Ses_CtrlMotOrgSetStInfo_t Task_1ms_Ses_CtrlMotOrgSetStInfo;
extern Ses_CtrlMotDqIRefSesInfo_t Task_1ms_Ses_CtrlMotDqIRefSesInfo;
extern Ses_CtrlPwrPistStBfMotOrgSetInfo_t Task_1ms_Ses_CtrlPwrPistStBfMotOrgSetInfo;
extern Mgd_TLE9180_HndlrMgdDcdInfo_t Task_1ms_Mgd_TLE9180_HndlrMgdDcdInfo;
extern Mtr_Processing_DiagMtrProcessDataInfo_t Task_1ms_Mtr_Processing_DiagMtrProcessDataInf;
extern Arbitrator_MtrMtrArbitratorData_t Task_1ms_Arbitrator_MtrMtrArbtratorData;
extern Arbitrator_MtrMtrArbitratorInfo_t Task_1ms_Arbitrator_MtrMtrArbtratorInfo;
extern AdcIf_Conv1msAdcInvalid_t Task_1ms_AdcIf_Conv1msAdcInvalid;
extern Ach_InputAchAsicInvalidInfo_t Task_1ms_Ach_InputAchAsicInvalid;
extern Ioc_InputSR1msVBatt1Mon_t Task_1ms_Ioc_InputSR1msVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Task_1ms_Ioc_InputSR1msVBatt2Mon;
extern Ioc_InputSR1msFspCbsHMon_t Task_1ms_Ioc_InputSR1msFspCbsHMon;
extern Ioc_InputSR1msFspCbsMon_t Task_1ms_Ioc_InputSR1msFspCbsMon;
extern Ioc_InputSR1msFspAbsHMon_t Task_1ms_Ioc_InputSR1msFspAbsHMon;
extern Ioc_InputSR1msFspAbsMon_t Task_1ms_Ioc_InputSR1msFspAbsMon;
extern Ioc_InputSR1msExt5vMon_t Task_1ms_Ioc_InputSR1msExt5vMon;
extern Ioc_InputSR1msCEMon_t Task_1ms_Ioc_InputSR1msCEMon;
extern Ioc_InputSR1msVdd1Mon_t Task_1ms_Ioc_InputSR1msVdd1Mon;
extern Ioc_InputSR1msVdd2Mon_t Task_1ms_Ioc_InputSR1msVdd2Mon;
extern Ioc_InputSR1msVdd3Mon_t Task_1ms_Ioc_InputSR1msVdd3Mon;
extern Ioc_InputSR1msVdd4Mon_t Task_1ms_Ioc_InputSR1msVdd4Mon;
extern Ioc_InputSR1msVdd5Mon_t Task_1ms_Ioc_InputSR1msVdd5Mon;
extern Ioc_InputSR1msVddMon_t Task_1ms_Ioc_InputSR1msVddMon;
extern Ioc_InputSR1msCspMon_t Task_1ms_Ioc_InputSR1msCspMon;
extern Ioc_InputCS1msRsmDbcMon_t Task_1ms_Ioc_InputCS1msRsmDbcMon;
extern Vlvd_A3944_HndlrVlvdInvalid_t Task_1ms_Vlvd_A3944_HndlrVlvdInvalid;
extern Mps_TLE5012_Hndlr_1msMpsInvalid_t Task_1ms_Mps_TLE5012_Hndlr_1msMpsInvalid;
extern Msp_1msCtrlVdcLinkFild_t Task_1ms_Msp_1msCtrlVdcLinkFild;
extern Pct_1msCtrlPreBoostMod_t Task_1ms_Pct_1msCtrlPreBoostMod;
extern Mcc_1msCtrlMotCtrlMode_t Task_1ms_Mcc_1msCtrlMotCtrlMode;
extern Mcc_1msCtrlMotCtrlState_t Task_1ms_Mcc_1msCtrlMotCtrlState;
extern Mcc_1msCtrlMotICtrlFadeOutState_t Task_1ms_Mcc_1msCtrlMotICtrlFadeOutState;
extern Mcc_1msCtrlStkRecvryStabnEndOK_t Task_1ms_Mcc_1msCtrlStkRecvryStabnEndOK;
extern Acm_MainAcmAsicInitCompleteFlag_t Task_1ms_Acm_MainAcmAsicInitCompleteFlag;
extern Mgd_TLE9180_HndlrMgdInvalid_t Task_1ms_Mgd_TLE9180_HndlrMgdInvalid;
extern Mtr_Processing_DiagMtrDiagState_t Task_1ms_Mtr_Processing_DiagMtrDiagState;
extern Arbitrator_MtrMtrArbDriveState_t Task_1ms_Arbitrator_MtrMtrArbDriveState;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Task_1ms_Init(void);
extern void Task_1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_1MS_H_ */
/** @} */
