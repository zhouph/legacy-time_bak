/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef RTE_TYPES_H_
#define RTE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef sint8 Rtesint8;
typedef uint8 Rteuint8;
typedef sint16 Rtesint16;
typedef uint16 Rteuint16;
typedef sint32 Rtesint32;
typedef uint32 Rteuint32;
typedef float32 Rtefloat32;
typedef float64 Rtefloat64;

typedef struct
{
    Rtesint32 FlIvReqData[10];
    Rtesint32 FrIvReqData[10];
    Rtesint32 RlIvReqData[10];
    Rtesint32 RrIvReqData[10];
    Rtesint32 FlOvReqData[10];
    Rtesint32 FrOvReqData[10];
    Rtesint32 RlOvReqData[10];
    Rtesint32 RrOvReqData[10];
    Rtesint32 FlIvReq;
    Rtesint32 FrIvReq;
    Rtesint32 RlIvReq;
    Rtesint32 RrIvReq;
    Rtesint32 FlOvReq;
    Rtesint32 FrOvReq;
    Rtesint32 RlOvReq;
    Rtesint32 RrOvReq;
    Rteuint8 FlIvDataLen;
    Rteuint8 FrIvDataLen;
    Rteuint8 RlIvDataLen;
    Rteuint8 RrIvDataLen;
    Rteuint8 FlOvDataLen;
    Rteuint8 FrOvDataLen;
    Rteuint8 RlOvDataLen;
    Rteuint8 RrOvDataLen;
}WhlVlvReqAbcInfo_t;

typedef struct
{
    Rteuint16 PrimCutVlvReqData[5];
    Rteuint16 SecdCutVlvReqData[5];
    Rteuint16 PrimCircVlvReqData[5];
    Rteuint16 SecdCircVlvReqData[5];
    Rteuint16 SimVlvReqData[5];
    Rteuint16 CircVlvReqData[5];
    Rteuint16 RelsVlvReqData[5];
    Rteuint16 PressDumpVlvReqData[5];
    Rteuint8 PrimCutVlvReq;
    Rteuint8 SecdCutVlvReq;
    Rteuint8 PrimCircVlvReq;
    Rteuint8 SecdCircVlvReq;
    Rteuint8 SimVlvReq;
    Rteuint8 RelsVlvReq;
    Rteuint8 CircVlvReq;
    Rteuint8 PressDumpVlvReq;
    Rteuint8 PrimCutVlvDataLen;
    Rteuint8 SecdCutVlvDataLen;
    Rteuint8 PrimCircVlvDataLen;
    Rteuint8 SecdCircVlvDataLen;
    Rteuint8 SimVlvDataLen;
    Rteuint8 CircVlvDataLen;
    Rteuint8 RelsVlvDataLen;
    Rteuint8 PressDumpVlvDataLen;
}NormVlvReqVlvActInfo_t;

typedef struct
{
    Rteuint8 FlIvReq;
    Rteuint8 FrIvReq;
    Rteuint8 RlIvReq;
    Rteuint8 RrIvReq;
    Rteuint8 FlOvReq;
    Rteuint8 FrOvReq;
    Rteuint8 RlOvReq;
    Rteuint8 RrOvReq;
    Rteuint8 FlIvDataLen;
    Rteuint8 FrIvDataLen;
    Rteuint8 RlIvDataLen;
    Rteuint8 RrIvDataLen;
    Rteuint8 FlOvDataLen;
    Rteuint8 FrOvDataLen;
    Rteuint8 RlOvDataLen;
    Rteuint8 RrOvDataLen;
    Rtesint32 FlIvReqData[5];
    Rtesint32 FrIvReqData[5];
    Rtesint32 RlIvReqData[5];
    Rtesint32 RrIvReqData[5];
    Rtesint32 FlOvReqData[5];
    Rtesint32 FrOvReqData[5];
    Rtesint32 RlOvReqData[5];
    Rtesint32 RrOvReqData[5];
}WhlVlvReqIdbInfo_t;

typedef struct
{
    Rtesint32 PrimBalVlvReqData[5];
    Rtesint32 SecdBalVlvReqData[5];
    Rtesint32 ChmbBalVlvReqData[5];
    Rteuint8 PrimBalVlvReq;
    Rteuint8 SecdBalVlvReq;
    Rteuint8 ChmbBalVlvReq;
    Rteuint8 PrimBalVlvDataLen;
    Rteuint8 SecdBalVlvDataLen;
    Rteuint8 ChmbBalVlvDataLen;
}IdbBalVlvReqInfo_t;

typedef struct
{
    Rtesint32 PrimCircPFild1ms;
    Rtesint32 SecdCircPFild1ms;
}CircPFild1msInfo_t;

typedef struct
{
    Rtesint32 PdtFild1ms;
}PedlTrvlFild1msInfo_t;

typedef struct
{
    Rtesint32 PistPFild1ms;
}PistPFild1msInfo_t;

typedef struct
{
    Rtesint32 TqIntvTCS;
    Rtesint32 TqIntvMsr;
    Rtesint32 TqIntvSlowTCS;
    Rtesint32 MinGear;
    Rtesint32 MaxGear;
    Rtesint32 TcsReq;
    Rtesint32 TcsCtrl;
    Rtesint32 AbsAct;
    Rtesint32 TcsGearShiftChr;
    Rtesint32 EspCtrl;
    Rtesint32 MsrReq;
    Rtesint32 TcsProductInfo;
}CanTxInfo_t;

typedef struct
{
    Rtesint32 IdRef;
    Rtesint32 IqRef;
}MotDqIRefMccInfo_t;

typedef struct
{
    Rtesint32 TarRgnBrkTq;
    Rtesint32 VirtStkDep;
    Rtesint32 VirtStkValid;
    Rtesint32 EstTotBrkForce;
    Rtesint32 EstHydBrkForce;
    Rtesint32 EhbStat;
}TarRgnBrkTqInfo_t;

typedef struct
{
    Rtesint32 AbsActFlg;
    Rtesint32 AbsDefectFlg;
    Rtesint32 AbsTarPFrntLe;
    Rtesint32 AbsTarPFrntRi;
    Rtesint32 AbsTarPReLe;
    Rtesint32 AbsTarPReRi;
    Rtesint32 FrntWhlSlip;
    Rtesint32 AbsDesTarP;
    Rtesint32 AbsDesTarPReqFlg;
    Rtesint32 AbsCtrlFadeOutFlg;
    Rtesint32 AbsCtrlModeFrntLe;
    Rtesint32 AbsCtrlModeFrntRi;
    Rtesint32 AbsCtrlModeReLe;
    Rtesint32 AbsCtrlModeReRi;
    Rtesint32 AbsTarPRateFrntLe;
    Rtesint32 AbsTarPRateFrntRi;
    Rtesint32 AbsTarPRateReLe;
    Rtesint32 AbsTarPRateReRi;
    Rtesint32 AbsPrioFrntLe;
    Rtesint32 AbsPrioFrntRi;
    Rtesint32 AbsPrioReLe;
    Rtesint32 AbsPrioReRi;
    Rtesint32 AbsDelTarPFrntLe;
    Rtesint32 AbsDelTarPFrntRi;
    Rtesint32 AbsDelTarPReLe;
    Rtesint32 AbsDelTarPReRi;
}AbsCtrlInfo_t;

typedef struct
{
    Rtesint32 AvhActFlg;
    Rtesint32 AvhDefectFlg;
    Rtesint32 AvhTarP;
}AvhCtrlInfo_t;

typedef struct
{
    Rtesint32 BaActFlg;
    Rtesint32 BaCDefectFlg;
    Rtesint32 BaTarP;
}BaCtrlInfo_t;

typedef struct
{
    Rtesint32 EbdActFlg;
    Rtesint32 EbdDefectFlg;
    Rtesint32 EbdTarPFrntLe;
    Rtesint32 EbdTarPFrntRi;
    Rtesint32 EbdTarPReLe;
    Rtesint32 EbdTarPReRi;
    Rtesint32 EbdTarPRateReLe;
    Rtesint32 EbdTarPRateReRi;
    Rtesint32 EbdCtrlModeReLe;
    Rtesint32 EbdCtrlModeReRi;
    Rtesint32 EbdDelTarPReLe;
    Rtesint32 EbdDelTarPReRi;
}EbdCtrlInfo_t;

typedef struct
{
    Rtesint32 EbpActFlg;
    Rtesint32 EbpDefectFlg;
    Rtesint32 EbpTarP;
}EbpCtrlInfo_t;

typedef struct
{
    Rtesint32 EpbiActFlg;
    Rtesint32 EpbiDefectFlg;
    Rtesint32 EpbiTarP;
}EpbiCtrlInfo_t;

typedef struct
{
    Rtesint32 EscActFlg;
    Rtesint32 EscDefectFlg;
    Rtesint32 EscTarPFrntLe;
    Rtesint32 EscTarPFrntRi;
    Rtesint32 EscTarPReLe;
    Rtesint32 EscTarPReRi;
    Rtesint32 EscCtrlModeFrntLe;
    Rtesint32 EscCtrlModeFrntRi;
    Rtesint32 EscCtrlModeReLe;
    Rtesint32 EscCtrlModeReRi;
    Rtesint32 EscTarPRateFrntLe;
    Rtesint32 EscTarPRateFrntRi;
    Rtesint32 EscTarPRateReLe;
    Rtesint32 EscTarPRateReRi;
    Rtesint32 EscPrioFrntLe;
    Rtesint32 EscPrioFrntRi;
    Rtesint32 EscPrioReLe;
    Rtesint32 EscPrioReRi;
    Rtesint32 EscPreCtrlModeFrntLe;
    Rtesint32 EscPreCtrlModeFrntRi;
    Rtesint32 EscPreCtrlModeReLe;
    Rtesint32 EscPreCtrlModeReRi;
    Rtesint32 EscDelTarPFrntLe;
    Rtesint32 EscDelTarPFrntRi;
    Rtesint32 EscDelTarPReLe;
    Rtesint32 EscDelTarPReRi;
}EscCtrlInfo_t;

typedef struct
{
    Rtesint32 HdcActFlg;
    Rtesint32 HdcDefectFlg;
    Rtesint32 HdcTarP;
}HdcCtrlInfo_t;

typedef struct
{
    Rtesint32 HsaActFlg;
    Rtesint32 HsaDefectFlg;
    Rtesint32 HsaTarP;
}HsaCtrlInfo_t;

typedef struct
{
    Rtesint32 SccActFlg;
    Rtesint32 SccDefectFlg;
    Rtesint32 SccTarP;
}SccCtrlInfo_t;

typedef struct
{
    Rtesint16 StkRcvrEscAllowedTime;
    Rtesint16 StkRcvrAbsAllowedTime;
    Rtesint16 StkRcvrTcsAllowedTime;
}StkRecvryCtrlIfInfo_t;

typedef struct
{
    Rtesint32 TcsActFlg;
    Rtesint32 TcsDefectFlg;
    Rtesint32 TcsTarPFrntLe;
    Rtesint32 TcsTarPFrntRi;
    Rtesint32 TcsTarPReLe;
    Rtesint32 TcsTarPReRi;
    Rtesint32 BothDrvgWhlBrkCtlReqFlg;
    Rtesint32 BrkCtrlFctFrntLeByWspc;
    Rtesint32 BrkCtrlFctFrntRiByWspc;
    Rtesint32 BrkCtrlFctReLeByWspc;
    Rtesint32 BrkCtrlFctReRiByWspc;
    Rtesint32 BrkTqGrdtReqFrntLeByWspc;
    Rtesint32 BrkTqGrdtReqFrntRiByWspc;
    Rtesint32 BrkTqGrdtReqReLeByWspc;
    Rtesint32 BrkTqGrdtReqReRiByWspc;
    Rtesint32 TcsDelTarPFrntLe;
    Rtesint32 TcsDelTarPFrntRi;
    Rtesint32 TcsDelTarPReLe;
    Rtesint32 TcsDelTarPReRi;
}TcsCtrlInfo_t;

typedef struct
{
    Rtesint32 TvbbActFlg;
    Rtesint32 TvbbDefectFlg;
    Rtesint32 TvbbTarP;
}TvbbCtrlInfo_t;

typedef struct
{
    Rtesint32 LowSpdCtrlInhibitFlg;
    Rtesint32 StopdVehCtrlModFlg;
    Rtesint32 VehStandStillStFlg;
}BaseBrkCtrlModInfo_t;

typedef struct
{
    Rtesint32 BrkPedlStatus;
    Rtesint32 DrvrIntendToRelsBrk;
    Rtesint32 PanicBrkStFlg;
    Rtesint32 PedlReldStFlg1;
    Rtesint32 PedlReldStFlg2;
    Rtesint32 PedlReldStUsingPedlSimrPFlg;
}BrkPedlStatusInfo_t;

typedef struct
{
    Rtesint32 PanicBrkSt1msFlg;
}BrkPedlStatus1msInfo_t;

typedef struct
{
    Rtesint32 PrimCircPFild;
    Rtesint32 PrimCircPFild_1_100Bar;
    Rtesint32 SecdCircPFild;
    Rtesint32 SecdCircPFild_1_100Bar;
}CircPFildInfo_t;

typedef struct
{
    Rtesint32 PrimCircPOffsCorrd;
    Rtesint32 PrimCircPOffsCorrdStOkFlg;
    Rtesint32 SecdCircPOffsCorrd;
    Rtesint32 SecdCircPOffsCorrdStOkFlg;
}CircPOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PrimCircPChgDurg10ms;
    Rtesint32 PrimCircPChgDurg5ms;
    Rtesint32 PrimCircPRate;
    Rtesint32 PrimCircPRateBarPerSec;
    Rtesint32 SecdCircPChgDurg10ms;
    Rtesint32 SecdCircPChgDurg5ms;
    Rtesint32 SecdCircPRate;
    Rtesint32 SecdCircPRateBarPerSec;
}CircPRateInfo_t;

typedef struct
{
    Rtesint32 PrimCircPRate1msAvg_1_100Bar;
    Rtesint32 SecdCircPRate1msAvg_1_100Bar;
}CircPRate1msInfo_t;

typedef struct
{
    Rtesint32 PedlTrvlRate1msMmPerSec;
}PedlTrvlRate1msInfo_t;

typedef struct
{
    Rtesint32 FrntLeEstimdWhlP;
    Rtesint32 FrntRiEstimdWhlP;
    Rtesint32 ReLeEstimdWhlP;
    Rtesint32 ReRiEstimdWhlP;
}EstimdWhlPInfo_t;

typedef struct
{
    Rtesint32 FinalTarPOfWhlFrntLe;
    Rtesint32 FinalTarPOfWhlFrntRi;
    Rtesint32 FinalTarPOfWhlReLe;
    Rtesint32 FinalTarPOfWhlReRi;
    Rtesint32 FinalTarPRateOfWhlFrntLe;
    Rtesint32 FinalTarPRateOfWhlFrntRi;
    Rtesint32 FinalTarPRateOfWhlReLe;
    Rtesint32 FinalTarPRateOfWhlReRi;
    Rtesint32 PCtrlPrioOfWhlFrntLe;
    Rtesint32 PCtrlPrioOfWhlFrntRi;
    Rtesint32 PCtrlPrioOfWhlReLe;
    Rtesint32 PCtrlPrioOfWhlReRi;
    Rtesint32 CtrlModOfWhlFrntLe;
    Rtesint32 CtrlModOfOfWhlFrntRi;
    Rtesint32 CtrlModOfWhlReLe;
    Rtesint32 CtrlModOfWhlReRi;
    Rtesint32 ReqdPCtrlBoostMod;
    Rtesint32 FinalDelTarPOfWhlFrntLe;
    Rtesint32 FinalDelTarPOfWhlFrntRi;
    Rtesint32 FinalDelTarPOfWhlReLe;
    Rtesint32 FinalDelTarPOfWhlReRi;
    Rtesint32 FinalMaxCircuitTp;
}FinalTarPInfo_t;

typedef struct
{
    Rtesint32 MotTarPosn;
}IdbMotPosnInfo_t;

typedef struct
{
    Rtesint32 FlOutVlvCtrlTar[5];
    Rtesint32 FrOutVlvCtrlTar[5];
    Rtesint32 RlOutVlvCtrlTar[5];
    Rtesint32 RrOutVlvCtrlTar[5];
}WhlOutVlvCtrlTarInfo_t;

typedef struct
{
    Rtesint32 CvVlvSt;
    Rtesint32 RlVlvSt;
    Rtesint32 PdVlvSt;
    Rtesint32 BalVlvSt;
    Rtesint32 CvVlvNoiseCtrlSt;
    Rtesint32 RlVlvNoiseCtrlSt;
    Rtesint32 PdVlvNoiseCtrlSt;
    Rtesint32 BalVlvNoiseCtrlSt;
    Rtesint32 CvVlvHoldTime;
    Rtesint32 RlVlvHoldTime;
    Rtesint32 PdVlvHoldTime;
    Rtesint32 BalVlvHoldTime;
}IdbPCtrllVlvCtrlStInfo_t;

typedef struct
{
    Rtesint32 PrimCutVlvSt;
    Rtesint32 SecdCutVlvSt;
    Rtesint32 SimVlvSt;
    Rtesint32 PrimCutVlvNoiseCtrlSt;
    Rtesint32 SecdCutVlvNoiseCtrlSt;
    Rtesint32 SimVlvNoiseCtrlSt;
    Rtesint32 PrimCutVlvHoldTime;
    Rtesint32 SecdCutVlvHoldTime;
    Rtesint32 SimVlvHoldTime;
    Rtesint32 CutVlvLongOpen;
}IdbPedlFeelVlvCtrlStInfo_t;

typedef struct
{
    Rtesint32 FlPreFillActFlg;
    Rtesint32 FrPreFillActFlg;
    Rtesint32 RlPreFillActFlg;
    Rtesint32 RrPreFillActFlg;
}WhlPreFillInfo_t;

typedef struct
{
    Rtesint32 FlInVlvCtrlTar[5];
    Rtesint32 FrInVlvCtrlTar[5];
    Rtesint32 RlInVlvCtrlTar[5];
    Rtesint32 RrInVlvCtrlTar[5];
}WhlInVlvCtrlTarInfo_t;

typedef struct
{
    Rtesint32 BBCCtrlSt;
}BBCCtrlInfo_t;

typedef struct
{
    Rteuint8 VlvFadeoutEndOK;
    Rteuint8 FadeoutSt2EndOk;
    Rteuint8 CutVlvOpenFlg;
}IdbVlvActInfo_t;

typedef struct
{
    Rtesint32 FluxWeakengFlg;
    Rtesint32 FluxWeakengFlgOld;
}FluxWeakengStInfo_t;

typedef struct
{
    Rtesint32 PedlSimrPFild;
    Rtesint32 PedlSimrPFild_1_100Bar;
}PedlSimrPFildInfo_t;

typedef struct
{
    Rtesint32 PedlSimrPOffsCorrd;
    Rtesint32 PedlSimrPOffsCorrdStOkFlg;
}PedlSimrPOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PdfFild;
    Rtesint32 PdtFild;
}PedlTrvlFildInfo_t;

typedef struct
{
    Rtesint32 PdfOffsCorrdStOkFlg;
    Rtesint32 PdfRawOffsCorrd;
    Rtesint32 PdtOffsCorrdStOkFlg;
    Rtesint32 PdtRawOffsCorrd;
}PedlTrvlOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PedlTrvlRate;
    Rtesint32 PedlTrvlRateMilliMtrPerSec;
}PedlTrvlRateInfo_t;

typedef struct
{
    Rtesint32 PdfSigNoiseSuspcFlgH;
    Rtesint32 PdfSigNoiseSuspcFlgL;
    Rtesint32 PdfSigTag;
    Rtesint32 PdtSigNoiseSuspcFlgH;
    Rtesint32 PdtSigNoiseSuspcFlgL;
    Rtesint32 PdtSigTag;
    Rtesint32 PedlSigTag;
    Rtesint32 PedlSimrPSigNoiseSuspcFlgH;
    Rtesint32 PedlSimrPSigNoiseSuspcFlgL;
}PedlTrvlTagInfo_t;

typedef struct
{
    Rtesint32 PistPFild;
    Rtesint32 PistPFild_1_100Bar;
}PistPFildInfo_t;

typedef struct
{
    Rtesint32 PistPOffsCorrd;
    Rtesint32 PistPOffsCorrdStOkFlg;
}PistPOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PistPChgDurg10ms;
    Rtesint32 PistPChgDurg5ms;
    Rtesint32 PistPRate;
    Rtesint32 PistPRateBarPerSec;
}PistPRateInfo_t;

typedef struct
{
    Rtesint32 PistPRate1msAvg_1_100Bar;
}PistPRate1msInfo_t;

typedef struct
{
    Rtesint32 FrntSlipDetThdSlip;
    Rtesint32 FrntSlipDetThdUDiff;
}RgnBrkCoopWithAbsInfo_t;

typedef struct
{
    Rtesint8 RecommendStkRcvrLvl;
    Rtesint8 StkRecvryCtrlState;
    Rtesint16 StkRecvryRequestedTime;
}StkRecvryActnIfInfo_t;

typedef struct
{
    Rtesint32 MuxCmdExecStOfWhlFrntLe;
    Rtesint32 MuxCmdExecStOfWhlFrntRi;
    Rtesint32 MuxCmdExecStOfWhlReLe;
    Rtesint32 MuxCmdExecStOfWhlReRi;
}MuxCmdExecStInfo_t;

typedef struct
{
    Rtesint32 VehStandStillStFlg;
    Rtesint32 VehStopStFlg;
}VehStopStInfo_t;

typedef struct
{
    Rtesint32 PedlSimrPGendOnFlg;
}VlvActtnReqByBaseBrkCtrlrInfo_t;

typedef struct
{
    Rtesint32 WhlSpdFildFrntLe;
    Rtesint32 WhlSpdFildFrntRi;
    Rtesint32 WhlSpdFildReLe;
    Rtesint32 WhlSpdFildReRi;
}WhlSpdFildInfo_t;

typedef struct
{
    Rteuint8 PedalEolOfsCalcOk;
    Rteuint8 PedalEolOfsCalcEnd;
    Rteuint8 PressEolOfsCalcOk;
    Rteuint8 PressEolOfsCalcEnd;
}IdbSnsrEolOfsCalcInfo_t;


typedef Rtesint32 FunctionLamp_t;
typedef Rtesint32 PCtrlAct_t;
typedef Rtesint32 MotCtrlMode_t;
typedef Rtesint32 MotCtrlState_t;
typedef Rtesint32 ActvBrkCtrlrActFlg_t;
typedef Rtesint32 Ay_t;
typedef Rtesint32 VehSpd_t;
typedef Rtesint32 BaseBrkCtrlrActFlg_t;
typedef Rtesint32 BlsFild_t;
typedef Rtesint32 BrkPRednForBaseBrkCtrlr_t;
typedef Rtesint32 FlexBrkSwtFild_t;
typedef Rtesint32 InitQuickBrkDctFlg_t;
typedef Rtesint32 MotICtrlFadeOutState_t;
typedef Rtesint32 TgtDeltaStkType_t;
typedef Rtesint32 PreBoostMod_t;
typedef Rtesint32 PCtrlBoostMod_t;
typedef Rtesint32 PCtrlFadeoutSt_t;
typedef Rtesint32 PCtrlSt_t;
typedef Rtesint32 InVlvAllCloseReq_t;
typedef Rtesint32 PChamberVolume_t;
typedef Rtesint32 PedlTrvlFinal_t;
typedef Rtesint32 RgnBrkCtlrBlendgFlg_t;
typedef Rtesint32 RgnBrkCtrlrActStFlg_t;
typedef Rtesint32 TarDeltaStk_t;
typedef Rtesint32 FinalTarPFromPCtrl_t;
typedef Rtesint32 TarPFromBaseBrkCtrlr_t;
typedef Rtesint32 TarPRateFromBaseBrkCtrlr_t;
typedef Rtesint32 TarPFromBrkPedl_t;
typedef Rtesint32 VehSpdFild_t;
typedef Rtesint32 StkRecvryStabnEndOK_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RTE_TYPES_H_ */
/** @} */
