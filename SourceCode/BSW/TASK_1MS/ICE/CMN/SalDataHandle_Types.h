/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef SAL_DATAHANDLE_TYPES_H_
#define SAL_DATAHANDLE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef ArbFsrAbsDrvInfo_t Arbitrator_VlvArbFsrAbsDrvInfo_t;
typedef FuncInhibitPedalSts_t Eem_SuspcDetnFuncInhibitPedalSts_t;
typedef PistP1msRawInfo_t Press_SenPistP1msRawInfo_t;
typedef FsrEnDrDrv_t Fsr_ActrFsrEnDrDrv_t;
typedef FSEscVlvInitTest_t AbsVlvM_MainFSEscVlvInitTest_t;
typedef MotDqIMeasdInfo_t Acmctl_CtrlMotDqIMeasdInfo_t;
typedef TxAhb1Info_t Proxy_TxTxAhb1Info_t;
typedef DISP_CC_DRDY_00Info_t Proxy_TxDISP_CC_DRDY_00Info_t;
typedef AVL_STEA_FTAXInfo_t Proxy_RxByComAVL_STEA_FTAXInfo_t;
typedef PresMonFaultInfo_t PressM_MainPresMonFaultInfo_t;
typedef ST_YMRInfo_t Proxy_TxST_YMRInfo_t;
typedef WEGSTRECKEInfo_t Proxy_TxWEGSTRECKEInfo_t;
typedef DiagVlvActr_t Diag_HndlrDiagVlvActr_t;
typedef MTRInitTest_t MtrM_MainMTRInitTest_t;
typedef MtrDiagState_t Mtr_Processing_DiagMtrDiagState_t;
typedef MtrProcessOutData_t Mtr_Processing_DiagMtrProcessOutData_t;
typedef MotDqIRefSesInfo_t Ses_CtrlMotDqIRefSesInfo_t;
typedef CanTimeOutStInfo_t Eem_SuspcDetnCanTimeOutStInfo_t;
typedef WMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t;
typedef QU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t;
typedef SwtPFaultInfo_t SwtP_MainSwtPFaultInfo_t;
typedef ArbBalVlvReqInfo_t Arbitrator_VlvArbBalVlvReqInfo_t;
typedef RxEms2Info_t Proxy_RxByComRxEms2Info_t;
typedef RxYawAccInfo_t Proxy_RxByComRxYawAccInfo_t;
typedef DiagHndlRequest_t Diag_HndlrDiagHndlRequest_t;
typedef CTR_CRInfo_t Proxy_RxByComCTR_CRInfo_t;
typedef FuncInhibitFsrSts_t Eem_SuspcDetnFuncInhibitFsrSts_t;
typedef SenPwrMonitor_t SenPwrM_MainSenPwrMonitor_t;
typedef CircP1msRawInfo_t Press_SenCircP1msRawInfo_t;
typedef FuncInhibitDiagSts_t Eem_SuspcDetnFuncInhibitDiagSts_t;
typedef TEMP_BRKInfo_t Proxy_TxTEMP_BRKInfo_t;
typedef EINHEITEN_BN2020Info_t Proxy_RxByComEINHEITEN_BN2020Info_t;
typedef SysPwrMonData_t SysPwrM_MainSysPwrMonData_t;
typedef A_TEMPInfo_t Proxy_RxByComA_TEMPInfo_t;
typedef RxHcu1Info_t Proxy_RxByComRxHcu1Info_t;
typedef AbsVlvMData_t AbsVlvM_MainAbsVlvMData_t;
typedef BlsSwt_t Swt_SenBlsSwt_t;
typedef CanRxEngTempInfo_t Proxy_RxCanRxEngTempInfo_t;
typedef DT_PT_2Info_t Proxy_RxByComDT_PT_2Info_t;
typedef CanRxRegenInfo_t Proxy_RxCanRxRegenInfo_t;
typedef FuncInhibitProxySts_t Eem_SuspcDetnFuncInhibitProxySts_t;
typedef YAWMOutInfo_t YawM_MainYAWMOutInfo_t;
typedef FSEscVlvActr_t AbsVlvM_MainFSEscVlvActr_t;
typedef EscSwtStInfo_t Swt_SenEscSwtStInfo_t;
typedef DoorSwt_t Swt_SenDoorSwt_t;
typedef CircP5msRawInfo_t Press_SenSyncCircP5msRawInfo_t;
typedef RELATIVZEITInfo_t Proxy_RxByComRELATIVZEITInfo_t;
typedef ST_VHSSInfo_t Proxy_TxST_VHSSInfo_t;
typedef STAT_ANHAENGERInfo_t Proxy_RxByComSTAT_ANHAENGERInfo_t;
typedef AxPlauOutput_t AxP_MainAxPlauOutput_t;
typedef TxWhlPulInfo_t Proxy_TxTxWhlPulInfo_t;
typedef ArbVlvSync_t Arbitrator_VlvArbVlvSync_t;
typedef MgdErrInfo_t MtrM_MainMgdErrInfo_t;
typedef FuncInhibitPctrlSts_t Eem_SuspcDetnFuncInhibitPctrlSts_t;
typedef DcMtrDutyData_t Dcmio_ActrDcMtrDutyData_t;
typedef MtrArbDriveState_t Arbitrator_MtrMtrArbDriveState_t;
typedef SwtStsFlexBrkInfo_t Swt_SenSwtStsFlexBrkInfo_t;
typedef WhlPulseCntInfo_t Wss_SenWhlPulseCntInfo_t;
typedef SPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t;
typedef DISP_CC_BYPA_00Info_t Proxy_TxDISP_CC_BYPA_00Info_t;
typedef Pdt5msRawInfo_t Pedal_SenSyncPdt5msRawInfo_t;
typedef RxHcu2Info_t Proxy_RxByComRxHcu2Info_t;
typedef FuncInhibitNvmSts_t Eem_SuspcDetnFuncInhibitNvmSts_t;
typedef DiagSci_t Diag_HndlrDiagSci_t;
typedef BalVlvDrvInfo_t Vlv_ActrBalVlvDrvInfo_t;
typedef BsSwt_t Swt_SenBsSwt_t;
typedef RxSasInfo_t Proxy_RxByComRxSasInfo_t;
typedef CircPBufInfo_t Press_SenCircPBufInfo_t;
typedef DiagClr_t Diag_HndlrDiagClr_t;
typedef TLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t;
typedef DT_PT_3Info_t Proxy_RxByComDT_PT_3Info_t;
typedef LogicEepDataInfo_t Nvm_HndlrLogicEepDataInfo_t;
typedef IgnEdgeSts_t Prly_HndlrIgnEdgeSts_t;
typedef WhlVlvReqSesInfo_t Ses_CtrlWhlVlvReqSesInfo_t;
typedef ST_ENERG_GENInfo_t Proxy_RxByComST_ENERG_GENInfo_t;
typedef CanRxEemInfo_t Proxy_RxCanRxEemInfo_t;
typedef FuncInhibitAsicSts_t Eem_SuspcDetnFuncInhibitAsicSts_t;
typedef FuncInhibitCanTrcvSts_t Eem_SuspcDetnFuncInhibitCanTrcvSts_t;
typedef VacuumCalcType_t Vacuum_MainVacuumCalcType_t;
typedef CanRxGearSelDispErrInfo_t Proxy_RxCanRxGearSelDispErrInfo_t;
typedef KILOMETERSTANDInfo_t Proxy_RxByComKILOMETERSTANDInfo_t;
typedef FuncInhibitAcmioSts_t Eem_SuspcDetnFuncInhibitAcmioSts_t;
typedef FuncInhibitVlvSts_t Eem_SuspcDetnFuncInhibitVlvSts_t;
typedef FuncInhibitRbcSts_t Eem_SuspcDetnFuncInhibitRbcSts_t;
typedef MotAngle2Info_t Acmio_SenMotAngle2Info_t;
typedef FsrDcMtrShutDwn_t Fsr_ActrFsrDcMtrShutDwn_t;
typedef RxTcu6Info_t Proxy_RxByComRxTcu6Info_t;
typedef WMOM_DRV_7Info_t Proxy_RxByComWMOM_DRV_7Info_t;
typedef DcMtrFreqData_t Dcmio_ActrDcMtrFreqData_t;
typedef SwtInfoEsc_t Swt_SenSwtInfoEsc_t;
typedef RxMcu1Info_t Proxy_RxByComRxMcu1Info_t;
typedef VEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t;
typedef MotOrgSetStInfo_t Ses_CtrlMotOrgSetStInfo_t;
typedef FuncInhibitPrlySts_t Eem_SuspcDetnFuncInhibitPrlySts_t;
typedef PressPInfo_t PressP_MainPressPInfo_t;
typedef FuncInhibitAdcifSts_t Eem_SuspcDetnFuncInhibitAdcifSts_t;
typedef MTRMOutInfo_t MtrM_MainMTRMOutInfo_t;
typedef VdcLinkFild_t Msp_1msCtrlVdcLinkFild_t;
typedef SasPlauOutput_t SasP_MainSasPlauOutput_t;
typedef PistP5msRawInfo_t Press_SenSyncPistP5msRawInfo_t;
typedef MotPwmDataInfo_t Acmio_ActrMotPwmDataInfo_t;
typedef EemFailData_t Eem_MainEemFailData_t;
typedef RxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo_t;
typedef WhlSpdInfo_t Wss_SenWhlSpdInfo_t;
typedef NormVlvDrvInfo_t Vlv_ActrNormVlvDrvInfo_t;
typedef PedlSimPRaw_t Press_SenSyncPedlSimPRaw_t;
typedef IdbSnsrOffsEolCalcReqByDiagInfo_t Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t;
typedef FuncInhibitRegSts_t Eem_SuspcDetnFuncInhibitRegSts_t;
typedef AVL_QUAN_EES_WHLInfo_t Proxy_TxAVL_QUAN_EES_WHLInfo_t;
typedef RxMcu2Info_t Proxy_RxByComRxMcu2Info_t;
typedef RxEms3Info_t Proxy_RxByComRxEms3Info_t;
typedef FuncInhibitIocSts_t Eem_SuspcDetnFuncInhibitIocSts_t;
typedef CanRxEscInfo_t Proxy_RxCanRxEscInfo_t;
typedef EemLampData_t Eem_MainEemLampData_t;
typedef FsrAbsDrvInfo_t Fsr_ActrFsrAbsDrvInfo_t;
typedef EcuModeSts_t Mom_HndlrEcuModeSts_t;
typedef HGLV_VEH_FILTInfo_t Proxy_RxByComHGLV_VEH_FILTInfo_t;
typedef AVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t;
typedef FuncInhibitMspSts_t Eem_SuspcDetnFuncInhibitMspSts_t;
typedef EemEceData_t Eem_MainEemEceData_t;
typedef PbSwt_t Swt_SenPbSwt_t;
typedef FuncInhibitVlvActSts_t Eem_SuspcDetnFuncInhibitVlvActSts_t;
typedef SU_DSCInfo_t Proxy_TxSU_DSCInfo_t;
typedef RxEms1Info_t Proxy_RxByComRxEms1Info_t;
typedef ERRM_BN_UInfo_t Proxy_RxByComERRM_BN_UInfo_t;
typedef V_VEH_V_VEH_2Info_t Proxy_RxByComV_VEH_V_VEH_2Info_t;
typedef AVL_LTRQD_BAXInfo_t Proxy_RxByComAVL_LTRQD_BAXInfo_t;
typedef KLEMMENInfo_t Proxy_RxByComKLEMMENInfo_t;
typedef MtrProcessOutInfo_t Mtr_Processing_SigMtrProcessOutInfo_t;
typedef FZZSTDInfo_t Proxy_RxByComFZZSTDInfo_t;
typedef FuncInhibitPressSts_t Eem_SuspcDetnFuncInhibitPressSts_t;
typedef ST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t;
typedef RDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t;
typedef RQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t;
typedef PressSenCalcInfo_t Press_SenSyncPressSenCalcInfo_t;
typedef RxClu1Info_t Proxy_RxByComRxClu1Info_t;
typedef AYMOutInfo_t AyM_MainAYMOutInfo_t;
typedef FuncInhibitGdSts_t Eem_SuspcDetnFuncInhibitGdSts_t;
typedef ArbVlvDriveState_t Arbitrator_VlvArbVlvDriveState_t;
typedef DT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t;
typedef FuncInhibitSpimSts_t Eem_SuspcDetnFuncInhibitSpimSts_t;
typedef FuncInhibitSesSts_t Eem_SuspcDetnFuncInhibitSesSts_t;
typedef AVL_T_TYRInfo_t Proxy_TxAVL_T_TYRInfo_t;
typedef TxYawCbitInfo_t Proxy_TxTxYawCbitInfo_t;
typedef WssPlauData_t WssP_MainWssPlauData_t;
typedef FuncInhibitArbiSts_t Eem_SuspcDetnFuncInhibitArbiSts_t;
typedef RDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t;
typedef SU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t;
typedef EemSuspectData_t Eem_MainEemSuspectData_t;
typedef MotAngle1Info_t Acmio_SenMotAngle1Info_t;
typedef RxClu2Info_t Proxy_RxByComRxClu2Info_t;
typedef STAT_ENG_STA_AUTOInfo_t Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t;
typedef NormVlvReqInfo_t Vlv_ActrSyncNormVlvReqInfo_t;
typedef TxTcs1Info_t Proxy_TxTxTcs1Info_t;
typedef EscSwt_t Swt_SenEscSwt_t;
typedef CanRxIdbInfo_t Proxy_RxCanRxIdbInfo_t;
typedef FuncInhibitWssSts_t Eem_SuspcDetnFuncInhibitWssSts_t;
typedef PspBufInfo_t Press_SenPspBufInfo_t;
typedef PdtBufInfo_t Pedal_SenPdtBufInfo_t;
typedef TxEsp2Info_t Proxy_TxTxEsp2Info_t;
typedef FuncInhibitSwtSts_t Eem_SuspcDetnFuncInhibitSwtSts_t;
typedef PistPBufInfo_t Press_SenPistPBufInfo_t;
typedef FAHRZEUGTYPInfo_t Proxy_RxByComFAHRZEUGTYPInfo_t;
typedef DT_PT_1Info_t Proxy_RxByComDT_PT_1Info_t;
typedef MotCurrInfo_t Acmio_SenMotCurrInfo_t;
typedef NormVlvCurrInfo_t Vlv_ActrSyncNormVlvCurrInfo_t;
typedef WMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t;
typedef ST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t;
typedef WssMonData_t WssM_MainWssMonData_t;
typedef RxYawSerialInfo_t Proxy_RxByComRxYawSerialInfo_t;
typedef FSFsrAbsDrvInfo_t AbsVlvM_MainFSFsrAbsDrvInfo_t;
typedef Pdf5msRawInfo_t Pedal_SenSyncPdf5msRawInfo_t;
typedef DiagAhbSci_t Diag_HndlrDiagAhbSci_t;
typedef DT_GRDT_DRVInfo_t Proxy_RxByComDT_GRDT_DRVInfo_t;
typedef TAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t;
typedef MomEcuInhibit_t Mom_HndlrMomEcuInhibit_t;
typedef FuncInhibitMomSts_t Eem_SuspcDetnFuncInhibitMomSts_t;
typedef SasCalInfo_t Diag_HndlrSasCalInfo_t;
typedef CanRxAccelPedlInfo_t Proxy_RxCanRxAccelPedlInfo_t;
typedef AyPlauOutput_t AyP_MainAyPlauOutput_t;
typedef RxTcu5Info_t Proxy_RxByComRxTcu5Info_t;
typedef AkaKeepAliveWriteData_t Aka_MainAkaKeepAliveWriteData_t;
typedef DIAG_OBD_HYB_1Info_t Proxy_TxDIAG_OBD_HYB_1Info_t;
typedef WssSpeedOut_t Wss_SenWssSpeedOut_t;
typedef PresCalcEsc_t Press_SenPresCalcEsc_t;
typedef EscVlvReqAswInfo_t Vlv_DummyEscVlvReqAswInfo_t;
typedef AVL_RPM_WHLInfo_t Proxy_TxAVL_RPM_WHLInfo_t;
typedef VdcLink_t Acmio_SenVdcLink_t;
typedef RxHcu3Info_t Proxy_RxByComRxHcu3Info_t;
typedef PdfBufInfo_t Pedal_SenPdfBufInfo_t;
typedef ACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t;
typedef Pdt1msRawInfo_t Pedal_SenPdt1msRawInfo_t;
typedef NormVlvReqSesInfo_t Ses_CtrlNormVlvReqSesInfo_t;
typedef MotReqDataDiagInfo_t Diag_HndlrMotReqDataDiagInfo_t;
typedef RxFact1Info_t Proxy_RxByComRxFact1Info_t;
typedef WhlVlvReqInfo_t Vlv_ActrSyncWhlVlvReqInfo_t;
typedef AVL_BRTORQ_WHLInfo_t Proxy_TxAVL_BRTORQ_WHLInfo_t;
typedef STAT_CT_HABRInfo_t Proxy_RxByComSTAT_CT_HABRInfo_t;
typedef FR_DBG_DSCInfo_t Proxy_TxFR_DBG_DSCInfo_t;
typedef TxTcs5Info_t Proxy_TxTxTcs5Info_t;
typedef FuncInhibitIcuSts_t Eem_SuspcDetnFuncInhibitIcuSts_t;
typedef BalVlvReqInfo_t Vlv_ActrSyncBalVlvReqInfo_t;
typedef FAHRGESTELLNUMMERInfo_t Proxy_RxByComFAHRGESTELLNUMMERInfo_t;
typedef RxLongAccInfo_t Proxy_RxByComRxLongAccInfo_t;
typedef EemCtrlInhibitData_t Eem_MainEemCtrlInhibitData_t;
typedef SwtStsFSInfo_t Eem_SuspcDetnSwtStsFSInfo_t;
typedef MotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo_t;
typedef HzrdSwt_t Swt_SenHzrdSwt_t;
typedef AsicMonFaultInfo_t EcuHwCtrlM_MainAsicMonFaultInfo_t;
typedef WhlEdgeCntInfo_t Wss_SenWhlEdgeCntInfo_t;
typedef WISCHERGESCHWINDIGKEITInfo_t Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t;
typedef DIAG_OBD_ENGMG_ELInfo_t Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t;
typedef AwdPrnDrv_t Awd_mainAwdPrnDrv_t;
typedef ST_TYR_RPAInfo_t Proxy_TxST_TYR_RPAInfo_t;
typedef ArbResPVlvReqInfo_t Arbitrator_VlvArbResPVlvReqInfo_t;
typedef CanRxInfo_t Proxy_RxCanRxInfo_t;
typedef WhlVlvDrvInfo_t Vlv_ActrWhlVlvDrvInfo_t;
typedef TxESCSensorInfo_t Proxy_TxTxESCSensorInfo_t;
typedef TORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t;
typedef ST_TYR_RDCInfo_t Proxy_TxST_TYR_RDCInfo_t;
typedef MtrProcessDataInfo_t Mtr_Processing_DiagMtrProcessDataInfo_t;
typedef BEDIENUNG_WISCHERInfo_t Proxy_RxByComBEDIENUNG_WISCHERInfo_t;
typedef FuncInhibitMccSts_t Eem_SuspcDetnFuncInhibitMccSts_t;
typedef IMUCalc_t Proxy_RxIMUCalc_t;
typedef VlvSync_t Vlv_ActrSyncVlvSync_t;
typedef WssCalc_t Wss_SenWssCalc_t;
typedef TAR_LTRQD_BAXInfo_t Proxy_TxTAR_LTRQD_BAXInfo_t;
typedef FuncInhibitVlvdSts_t Eem_SuspcDetnFuncInhibitVlvdSts_t;
typedef PressCalcInfo_t Press_SenPressCalcInfo_t;
typedef ArbWhlVlvReqInfo_t Arbitrator_VlvArbWhlVlvReqInfo_t;
typedef MotReqDataAcmctlInfo_t Acmctl_CtrlMotReqDataAcmctlInfo_t;
typedef ST_TYR_2Info_t Proxy_TxST_TYR_2Info_t;
typedef TxSasCalInfo_t Proxy_TxTxSasCalInfo_t;
typedef FuncInhibitRlySts_t Eem_SuspcDetnFuncInhibitRlySts_t;
typedef RlyDrvInfo_t Rly_ActrRlyDrvInfo_t;
typedef STAT_DS_VRFDInfo_t Proxy_RxByComSTAT_DS_VRFDInfo_t;
typedef MpsD1ErrInfo_t Mps_TLE5012M_MainMpsD1ErrInfo_t;
typedef YawPlauOutput_t YawP_MainYawPlauOutput_t;
typedef RxTcu1Info_t Proxy_RxByComRxTcu1Info_t;
typedef FSBbsVlvActr_t BbsVlvM_MainFSBbsVlvActr_t;
typedef FuncInhibitMpsSts_t Eem_SuspcDetnFuncInhibitMpsSts_t;
typedef WMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t;
typedef PwrPistStBfMotOrgSetInfo_t Ses_CtrlPwrPistStBfMotOrgSetInfo_t;
typedef HdcSwt_t Swt_SenHdcSwt_t;
typedef TxWhlSpdInfo_t Proxy_TxTxWhlSpdInfo_t;
typedef IgnOnOffSts_t Prly_HndlrIgnOnOffSts_t;
typedef ST_BLT_CT_SOCCUInfo_t Proxy_RxByComST_BLT_CT_SOCCUInfo_t;
typedef ArbNormVlvReqInfo_t Arbitrator_VlvArbNormVlvReqInfo_t;
typedef RxBms1Info_t Proxy_RxByComRxBms1Info_t;
typedef YAWMSerialInfo_t YawM_MainYAWMSerialInfo_t;
typedef AXMOutInfo_t AxM_MainAXMOutInfo_t;
typedef PedalPData_t PedalP_MainPedalPData_t;
typedef WhlSnsrTypeInfo_t Wss_SenWhlSnsrTypeInfo_t;
typedef AvhSwt_t Swt_SenAvhSwt_t;
typedef DT_BRKSYS_ENGMGInfo_t Proxy_RxByComDT_BRKSYS_ENGMGInfo_t;
typedef TxEbs1Info_t Proxy_TxTxEbs1Info_t;
typedef MpsD2ErrInfo_t Mps_TLE5012M_MainMpsD2ErrInfo_t;
typedef CanMonData_t CanM_MainCanMonData_t;
typedef MotorAbsInputEsc_t Dcmio_SenMotorAbsInputEsc_t;
typedef SASMOutInfo_t SasM_MainSASMOutInfo_t;
typedef FuncInhibitBbcSts_t Eem_SuspcDetnFuncInhibitBbcSts_t;
typedef ArbAsicEnDrDrvInfo_t Arbitrator_VlvArbAsicEnDrDrvInfo_t;
typedef BEDIENUNG_FAHRWERKInfo_t Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t;
typedef FuncInhibitAcmctlSts_t Eem_SuspcDetnFuncInhibitAcmctlSts_t;
typedef RxHcu5Info_t Proxy_RxByComRxHcu5Info_t;
typedef MtrArbitratorData_t Arbitrator_MtrMtrArbitratorData_t;
typedef FSBbsVlvInitTest_t BbsVlvM_MainFSBbsVlvInitTest_t;
typedef UHRZEIT_DATUMInfo_t Proxy_RxByComUHRZEIT_DATUMInfo_t;
typedef RQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t;
typedef MtrArbitratorInfo_t Arbitrator_MtrMtrArbitratorInfo_t;
typedef PedalMData_t PedalM_MainPedalMData_t;
typedef RelayMonitor_t RlyM_MainRelayMonitor_t;
typedef MotRotgAgSigBfMotOrgSetInfo_t Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t;
typedef SwtMonFaultInfo_t SwtM_MainSwtMonFaultInfo_t;
typedef FsrCbsDrvInfo_t Fsr_ActrFsrCbsDrvInfo_t;
typedef ArbFsrBbsDrvInfo_t Arbitrator_VlvArbFsrBbsDrvInfo_t;
typedef FSFsrBbsDrvInfo_t BbsVlvM_MainFSFsrBbsDrvInfo_t;
typedef BBSVlvErrInfo_t BbsVlvM_MainBBSVlvErrInfo_t;
typedef CanRxClu2Info_t Proxy_RxCanRxClu2Info_t;
typedef PrlyEcuInhibit_t Prly_HndlrPrlyEcuInhibit_t;
typedef AVL_P_TYRInfo_t Proxy_TxAVL_P_TYRInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SAL_DATAHANDLE_TYPES_H_ */
/** @} */
