Mcc_1msCtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Mcc_1msCtrlMotRotgAgSigInfo = CreateBus(Mcc_1msCtrlMotRotgAgSigInfo, DeList);
clear DeList;

Mcc_1msCtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'VlvFadeoutEndOK'
    'FadeoutSt2EndOk'
    'CutVlvOpenFlg'
    };
Mcc_1msCtrlIdbVlvActInfo = CreateBus(Mcc_1msCtrlIdbVlvActInfo, DeList);
clear DeList;

Mcc_1msCtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'RecommendStkRcvrLvl'
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Mcc_1msCtrlStkRecvryActnIfInfo = CreateBus(Mcc_1msCtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Ses_CtrlMotDqIMeasdInfo = Simulink.Bus;
DeList={
    'MotIqMeasd'
    'MotIdMeasd'
    };
Ses_CtrlMotDqIMeasdInfo = CreateBus(Ses_CtrlMotDqIMeasdInfo, DeList);
clear DeList;

Ses_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Ses_CtrlMotRotgAgSigInfo = CreateBus(Ses_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Ses_CtrlMotRotgAgSigBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'StkPosnMeasdBfMotOrgSet'
    'MotMechAngleSpdBfMotOrgSet'
    };
Ses_CtrlMotRotgAgSigBfMotOrgSetInfo = CreateBus(Ses_CtrlMotRotgAgSigBfMotOrgSetInfo, DeList);
clear DeList;

Vlv_ActrNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    };
Vlv_ActrNormVlvReqInfo = CreateBus(Vlv_ActrNormVlvReqInfo, DeList);
clear DeList;

Vlv_ActrWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Vlv_ActrWhlVlvReqInfo = CreateBus(Vlv_ActrWhlVlvReqInfo, DeList);
clear DeList;

Vlv_ActrBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    };
Vlv_ActrBalVlvReqInfo = CreateBus(Vlv_ActrBalVlvReqInfo, DeList);
clear DeList;

Ioc_OutputSR1msFsrCbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsOff'
    'FsrCbsDrv'
    };
Ioc_OutputSR1msFsrCbsDrvInfo = CreateBus(Ioc_OutputSR1msFsrCbsDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsOff'
    'FsrAbsDrv'
    };
Ioc_OutputSR1msFsrAbsDrvInfo = CreateBus(Ioc_OutputSR1msFsrAbsDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msRlyDrvInfo = Simulink.Bus;
DeList={
    'RlyDbcDrv'
    'RlyEssDrv'
    };
Ioc_OutputCS1msRlyDrvInfo = CreateBus(Ioc_OutputCS1msRlyDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsOff'
    'FsrAbsDrv'
    };
Ioc_OutputCS1msFsrAbsDrvInfo = CreateBus(Ioc_OutputCS1msFsrAbsDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msFsrCbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsOff'
    'FsrCbsDrv'
    };
Ioc_OutputCS1msFsrCbsDrvInfo = CreateBus(Ioc_OutputCS1msFsrCbsDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
Ioc_OutputCS1msSenPwrMonitorData = CreateBus(Ioc_OutputCS1msSenPwrMonitorData, DeList);
clear DeList;

Mtr_Processing_DiagArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Mtr_Processing_DiagArbWhlVlvReqInfo = CreateBus(Mtr_Processing_DiagArbWhlVlvReqInfo, DeList);
clear DeList;

Mtr_Processing_DiagEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Mtr_Processing_DiagEemFailData = CreateBus(Mtr_Processing_DiagEemFailData, DeList);
clear DeList;

Mtr_Processing_DiagEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Mtr_Processing_DiagEemCtrlInhibitData = CreateBus(Mtr_Processing_DiagEemCtrlInhibitData, DeList);
clear DeList;

Mtr_Processing_DiagMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Mtr_Processing_DiagMotRotgAgSigInfo = CreateBus(Mtr_Processing_DiagMotRotgAgSigInfo, DeList);
clear DeList;

Mtr_Processing_DiagWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Mtr_Processing_DiagWhlSpdInfo = CreateBus(Mtr_Processing_DiagWhlSpdInfo, DeList);
clear DeList;

Mtr_Processing_DiagDiagHndlRequest = Simulink.Bus;
DeList={
    'DiagRequest_Order'
    'DiagRequest_Iq'
    'DiagRequest_Id'
    'DiagRequest_U_PWM'
    'DiagRequest_V_PWM'
    'DiagRequest_W_PWM'
    };
Mtr_Processing_DiagDiagHndlRequest = CreateBus(Mtr_Processing_DiagDiagHndlRequest, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Mtr_Processing_DiagMtrProcessOutInfo = CreateBus(Mtr_Processing_DiagMtrProcessOutInfo, DeList);
clear DeList;

Arbitrator_MtrEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Arbitrator_MtrEemFailData = CreateBus(Arbitrator_MtrEemFailData, DeList);
clear DeList;

Arbitrator_MtrEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Arbitrator_MtrEemCtrlInhibitData = CreateBus(Arbitrator_MtrEemCtrlInhibitData, DeList);
clear DeList;

AdcIf_Conv1msEcuModeSts = Simulink.Bus;
DeList={'AdcIf_Conv1msEcuModeSts'};
AdcIf_Conv1msEcuModeSts = CreateBus(AdcIf_Conv1msEcuModeSts, DeList);
clear DeList;

AdcIf_Conv1msFuncInhibitAdcifSts = Simulink.Bus;
DeList={'AdcIf_Conv1msFuncInhibitAdcifSts'};
AdcIf_Conv1msFuncInhibitAdcifSts = CreateBus(AdcIf_Conv1msFuncInhibitAdcifSts, DeList);
clear DeList;

SentH_MainEcuModeSts = Simulink.Bus;
DeList={'SentH_MainEcuModeSts'};
SentH_MainEcuModeSts = CreateBus(SentH_MainEcuModeSts, DeList);
clear DeList;

Ioc_InputSR1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputSR1msEcuModeSts'};
Ioc_InputSR1msEcuModeSts = CreateBus(Ioc_InputSR1msEcuModeSts, DeList);
clear DeList;

Ioc_InputSR1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputSR1msFuncInhibitIocSts'};
Ioc_InputSR1msFuncInhibitIocSts = CreateBus(Ioc_InputSR1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_InputCS1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputCS1msEcuModeSts'};
Ioc_InputCS1msEcuModeSts = CreateBus(Ioc_InputCS1msEcuModeSts, DeList);
clear DeList;

Ioc_InputCS1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputCS1msFuncInhibitIocSts'};
Ioc_InputCS1msFuncInhibitIocSts = CreateBus(Ioc_InputCS1msFuncInhibitIocSts, DeList);
clear DeList;

CanTrv_TLE6251_HndlrEcuModeSts = Simulink.Bus;
DeList={'CanTrv_TLE6251_HndlrEcuModeSts'};
CanTrv_TLE6251_HndlrEcuModeSts = CreateBus(CanTrv_TLE6251_HndlrEcuModeSts, DeList);
clear DeList;

CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = Simulink.Bus;
DeList={'CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts'};
CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts = CreateBus(CanTrv_TLE6251_HndlrFuncInhibitCanTrcvSts, DeList);
clear DeList;

Vlvd_A3944_HndlrEcuModeSts = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrEcuModeSts'};
Vlvd_A3944_HndlrEcuModeSts = CreateBus(Vlvd_A3944_HndlrEcuModeSts, DeList);
clear DeList;

Vlvd_A3944_HndlrFuncInhibitVlvdSts = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrFuncInhibitVlvdSts'};
Vlvd_A3944_HndlrFuncInhibitVlvdSts = CreateBus(Vlvd_A3944_HndlrFuncInhibitVlvdSts, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msEcuModeSts = Simulink.Bus;
DeList={'Mps_TLE5012_Hndlr_1msEcuModeSts'};
Mps_TLE5012_Hndlr_1msEcuModeSts = CreateBus(Mps_TLE5012_Hndlr_1msEcuModeSts, DeList);
clear DeList;

Pedal_SenEcuModeSts = Simulink.Bus;
DeList={'Pedal_SenEcuModeSts'};
Pedal_SenEcuModeSts = CreateBus(Pedal_SenEcuModeSts, DeList);
clear DeList;

Pedal_SenFuncInhibitPedalSts = Simulink.Bus;
DeList={'Pedal_SenFuncInhibitPedalSts'};
Pedal_SenFuncInhibitPedalSts = CreateBus(Pedal_SenFuncInhibitPedalSts, DeList);
clear DeList;

Press_SenEcuModeSts = Simulink.Bus;
DeList={'Press_SenEcuModeSts'};
Press_SenEcuModeSts = CreateBus(Press_SenEcuModeSts, DeList);
clear DeList;

Press_SenFuncInhibitPressSts = Simulink.Bus;
DeList={'Press_SenFuncInhibitPressSts'};
Press_SenFuncInhibitPressSts = CreateBus(Press_SenFuncInhibitPressSts, DeList);
clear DeList;

Msp_1msCtrlVdcLink = Simulink.Bus;
DeList={'Msp_1msCtrlVdcLink'};
Msp_1msCtrlVdcLink = CreateBus(Msp_1msCtrlVdcLink, DeList);
clear DeList;

Spc_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Spc_1msCtrlEcuModeSts'};
Spc_1msCtrlEcuModeSts = CreateBus(Spc_1msCtrlEcuModeSts, DeList);
clear DeList;

Det_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Det_1msCtrlEcuModeSts'};
Det_1msCtrlEcuModeSts = CreateBus(Det_1msCtrlEcuModeSts, DeList);
clear DeList;

Pct_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Pct_1msCtrlEcuModeSts'};
Pct_1msCtrlEcuModeSts = CreateBus(Pct_1msCtrlEcuModeSts, DeList);
clear DeList;

Pct_1msCtrlPCtrlSt = Simulink.Bus;
DeList={'Pct_1msCtrlPCtrlSt'};
Pct_1msCtrlPCtrlSt = CreateBus(Pct_1msCtrlPCtrlSt, DeList);
clear DeList;

Mcc_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Mcc_1msCtrlEcuModeSts'};
Mcc_1msCtrlEcuModeSts = CreateBus(Mcc_1msCtrlEcuModeSts, DeList);
clear DeList;

Mcc_1msCtrlFuncInhibitMccSts = Simulink.Bus;
DeList={'Mcc_1msCtrlFuncInhibitMccSts'};
Mcc_1msCtrlFuncInhibitMccSts = CreateBus(Mcc_1msCtrlFuncInhibitMccSts, DeList);
clear DeList;

Mcc_1msCtrlTgtDeltaStkType = Simulink.Bus;
DeList={'Mcc_1msCtrlTgtDeltaStkType'};
Mcc_1msCtrlTgtDeltaStkType = CreateBus(Mcc_1msCtrlTgtDeltaStkType, DeList);
clear DeList;

Mcc_1msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Mcc_1msCtrlPCtrlBoostMod'};
Mcc_1msCtrlPCtrlBoostMod = CreateBus(Mcc_1msCtrlPCtrlBoostMod, DeList);
clear DeList;

Mcc_1msCtrlPCtrlFadeoutSt = Simulink.Bus;
DeList={'Mcc_1msCtrlPCtrlFadeoutSt'};
Mcc_1msCtrlPCtrlFadeoutSt = CreateBus(Mcc_1msCtrlPCtrlFadeoutSt, DeList);
clear DeList;

Mcc_1msCtrlPCtrlSt = Simulink.Bus;
DeList={'Mcc_1msCtrlPCtrlSt'};
Mcc_1msCtrlPCtrlSt = CreateBus(Mcc_1msCtrlPCtrlSt, DeList);
clear DeList;

Mcc_1msCtrlInVlvAllCloseReq = Simulink.Bus;
DeList={'Mcc_1msCtrlInVlvAllCloseReq'};
Mcc_1msCtrlInVlvAllCloseReq = CreateBus(Mcc_1msCtrlInVlvAllCloseReq, DeList);
clear DeList;

Mcc_1msCtrlPChamberVolume = Simulink.Bus;
DeList={'Mcc_1msCtrlPChamberVolume'};
Mcc_1msCtrlPChamberVolume = CreateBus(Mcc_1msCtrlPChamberVolume, DeList);
clear DeList;

Mcc_1msCtrlTarDeltaStk = Simulink.Bus;
DeList={'Mcc_1msCtrlTarDeltaStk'};
Mcc_1msCtrlTarDeltaStk = CreateBus(Mcc_1msCtrlTarDeltaStk, DeList);
clear DeList;

Mcc_1msCtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Mcc_1msCtrlFinalTarPFromPCtrl'};
Mcc_1msCtrlFinalTarPFromPCtrl = CreateBus(Mcc_1msCtrlFinalTarPFromPCtrl, DeList);
clear DeList;

Ses_CtrlEcuModeSts = Simulink.Bus;
DeList={'Ses_CtrlEcuModeSts'};
Ses_CtrlEcuModeSts = CreateBus(Ses_CtrlEcuModeSts, DeList);
clear DeList;

Ses_CtrlFuncInhibitSesSts = Simulink.Bus;
DeList={'Ses_CtrlFuncInhibitSesSts'};
Ses_CtrlFuncInhibitSesSts = CreateBus(Ses_CtrlFuncInhibitSesSts, DeList);
clear DeList;

Vlv_ActrEcuModeSts = Simulink.Bus;
DeList={'Vlv_ActrEcuModeSts'};
Vlv_ActrEcuModeSts = CreateBus(Vlv_ActrEcuModeSts, DeList);
clear DeList;

Vlv_ActrFuncInhibitVlvSts = Simulink.Bus;
DeList={'Vlv_ActrFuncInhibitVlvSts'};
Vlv_ActrFuncInhibitVlvSts = CreateBus(Vlv_ActrFuncInhibitVlvSts, DeList);
clear DeList;

Vlv_ActrVlvSync = Simulink.Bus;
DeList={'Vlv_ActrVlvSync'};
Vlv_ActrVlvSync = CreateBus(Vlv_ActrVlvSync, DeList);
clear DeList;

Aka_MainPrlyEcuInhibit = Simulink.Bus;
DeList={'Aka_MainPrlyEcuInhibit'};
Aka_MainPrlyEcuInhibit = CreateBus(Aka_MainPrlyEcuInhibit, DeList);
clear DeList;

Mgd_TLE9180_HndlrEcuModeSts = Simulink.Bus;
DeList={'Mgd_TLE9180_HndlrEcuModeSts'};
Mgd_TLE9180_HndlrEcuModeSts = CreateBus(Mgd_TLE9180_HndlrEcuModeSts, DeList);
clear DeList;

Ioc_OutputSR1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputSR1msEcuModeSts'};
Ioc_OutputSR1msEcuModeSts = CreateBus(Ioc_OutputSR1msEcuModeSts, DeList);
clear DeList;

Ioc_OutputSR1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputSR1msFuncInhibitIocSts'};
Ioc_OutputSR1msFuncInhibitIocSts = CreateBus(Ioc_OutputSR1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_OutputCS1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputCS1msEcuModeSts'};
Ioc_OutputCS1msEcuModeSts = CreateBus(Ioc_OutputCS1msEcuModeSts, DeList);
clear DeList;

Ioc_OutputCS1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputCS1msFuncInhibitIocSts'};
Ioc_OutputCS1msFuncInhibitIocSts = CreateBus(Ioc_OutputCS1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_OutputCS1msFsrDcMtrShutDwn = Simulink.Bus;
DeList={'Ioc_OutputCS1msFsrDcMtrShutDwn'};
Ioc_OutputCS1msFsrDcMtrShutDwn = CreateBus(Ioc_OutputCS1msFsrDcMtrShutDwn, DeList);
clear DeList;

Ioc_OutputCS1msFsrEnDrDrv = Simulink.Bus;
DeList={'Ioc_OutputCS1msFsrEnDrDrv'};
Ioc_OutputCS1msFsrEnDrDrv = CreateBus(Ioc_OutputCS1msFsrEnDrDrv, DeList);
clear DeList;

Arbitrator_MtrEcuModeSts = Simulink.Bus;
DeList={'Arbitrator_MtrEcuModeSts'};
Arbitrator_MtrEcuModeSts = CreateBus(Arbitrator_MtrEcuModeSts, DeList);
clear DeList;

Arbitrator_MtrIgnOnOffSts = Simulink.Bus;
DeList={'Arbitrator_MtrIgnOnOffSts'};
Arbitrator_MtrIgnOnOffSts = CreateBus(Arbitrator_MtrIgnOnOffSts, DeList);
clear DeList;

Arbitrator_MtrIgnEdgeSts = Simulink.Bus;
DeList={'Arbitrator_MtrIgnEdgeSts'};
Arbitrator_MtrIgnEdgeSts = CreateBus(Arbitrator_MtrIgnEdgeSts, DeList);
clear DeList;

Arbitrator_MtrDiagSci = Simulink.Bus;
DeList={'Arbitrator_MtrDiagSci'};
Arbitrator_MtrDiagSci = CreateBus(Arbitrator_MtrDiagSci, DeList);
clear DeList;

Arbitrator_MtrDiagAhbSci = Simulink.Bus;
DeList={'Arbitrator_MtrDiagAhbSci'};
Arbitrator_MtrDiagAhbSci = CreateBus(Arbitrator_MtrDiagAhbSci, DeList);
clear DeList;

AdcIf_Conv1msSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Ext5vMon'
    'CEMon'
    'Int5vMon'
    'CSPMon'
    'Vbatt01Mon'
    'Vbatt02Mon'
    'VddMon'
    'Vdd3Mon'
    'Vdd5Mon'
    };
AdcIf_Conv1msSwTrigPwrInfo = CreateBus(AdcIf_Conv1msSwTrigPwrInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
AdcIf_Conv1msSwTrigMotInfo = CreateBus(AdcIf_Conv1msSwTrigMotInfo, DeList);
clear DeList;

Ach_InputAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    'Ach_Asic_3_OSC_STUCK_MON'
    'Ach_Asic_3_OSC_FRQ_MON'
    'Ach_Asic_3_WDOG_TO'
    'Ach_Asic_7_AN_TRIM_CRC_RESULT'
    'Ach_Asic_7_NVM_CRC_RESULT'
    'Ach_Asic_7_NVM_BUSY'
    };
Ach_InputAchSysPwrAsicInfo = CreateBus(Ach_InputAchSysPwrAsicInfo, DeList);
clear DeList;

Ach_InputAchValveAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CP_OV'
    'Ach_Asic_CP_UV'
    'Ach_Asic_VPWR_UV'
    'Ach_Asic_VPWR_OV'
    'Ach_Asic_PMP_LD_ACT'
    'Ach_Asic_FS_TURN_ON'
    'Ach_Asic_FS_TURN_OFF'
    'Ach_Asic_FS_VDS_FAULT'
    'Ach_Asic_FS_RVP_FAULT'
    'Ach_Asic_VHD_OV'
    'Ach_Asic_T_SD_INT'
    'Ach_Asic_No0_AVG_CURRENT'
    'Ach_Asic_No0_PWM_FAULT'
    'Ach_Asic_No0_CURR_SENSE'
    'Ach_Asic_No0_ADC_FAULT'
    'Ach_Asic_No0_T_SD'
    'Ach_Asic_No0_OPEN_LOAD'
    'Ach_Asic_No0_GND_LOSS'
    'Ach_Asic_No0_LVT'
    'Ach_Asic_No0_LS_OVC'
    'Ach_Asic_No0_VGS_LS_FAULT'
    'Ach_Asic_No0_VGS_HS_FAULT'
    'Ach_Asic_No0_HS_SHORT'
    'Ach_Asic_No0_LS_CLAMP_ON'
    'Ach_Asic_No0_UNDER_CURR'
    'Ach_Asic_No1_AVG_CURRENT'
    'Ach_Asic_No1_PWM_FAULT'
    'Ach_Asic_No1_CURR_SENSE'
    'Ach_Asic_No1_ADC_FAULT'
    'Ach_Asic_No1_T_SD'
    'Ach_Asic_No1_OPEN_LOAD'
    'Ach_Asic_No1_GND_LOSS'
    'Ach_Asic_No1_LVT'
    'Ach_Asic_No1_LS_OVC'
    'Ach_Asic_No1_VGS_LS_FAULT'
    'Ach_Asic_No1_VGS_HS_FAULT'
    'Ach_Asic_No1_HS_SHORT'
    'Ach_Asic_No1_LS_CLAMP_ON'
    'Ach_Asic_No1_UNDER_CURR'
    'Ach_Asic_No2_AVG_CURRENT'
    'Ach_Asic_No2_PWM_FAULT'
    'Ach_Asic_No2_CURR_SENSE'
    'Ach_Asic_No2_ADC_FAULT'
    'Ach_Asic_No2_T_SD'
    'Ach_Asic_No2_OPEN_LOAD'
    'Ach_Asic_No2_GND_LOSS'
    'Ach_Asic_No2_LVT'
    'Ach_Asic_No2_LS_OVC'
    'Ach_Asic_No2_VGS_LS_FAULT'
    'Ach_Asic_No2_VGS_HS_FAULT'
    'Ach_Asic_No2_HS_SHORT'
    'Ach_Asic_No2_LS_CLAMP_ON'
    'Ach_Asic_No2_UNDER_CURR'
    'Ach_Asic_No3_AVG_CURRENT'
    'Ach_Asic_No3_PWM_FAULT'
    'Ach_Asic_No3_CURR_SENSE'
    'Ach_Asic_No3_ADC_FAULT'
    'Ach_Asic_No3_T_SD'
    'Ach_Asic_No3_OPEN_LOAD'
    'Ach_Asic_No3_GND_LOSS'
    'Ach_Asic_No3_LVT'
    'Ach_Asic_No3_LS_OVC'
    'Ach_Asic_No3_VGS_LS_FAULT'
    'Ach_Asic_No3_VGS_HS_FAULT'
    'Ach_Asic_No3_HS_SHORT'
    'Ach_Asic_No3_LS_CLAMP_ON'
    'Ach_Asic_No3_UNDER_CURR'
    'Ach_Asic_Nc0_AVG_CURRENT'
    'Ach_Asic_Nc0_PWM_FAULT'
    'Ach_Asic_Nc0_CURR_SENSE'
    'Ach_Asic_Nc0_ADC_FAULT'
    'Ach_Asic_Nc0_T_SD'
    'Ach_Asic_Nc0_OPEN_LOAD'
    'Ach_Asic_Nc0_GND_LOSS'
    'Ach_Asic_Nc0_LVT'
    'Ach_Asic_Nc0_LS_OVC'
    'Ach_Asic_Nc0_VGS_LS_FAULT'
    'Ach_Asic_Nc0_VGS_HS_FAULT'
    'Ach_Asic_Nc0_HS_SHORT'
    'Ach_Asic_Nc0_LS_CLAMP_ON'
    'Ach_Asic_Nc0_UNDER_CURR'
    'Ach_Asic_Nc1_AVG_CURRENT'
    'Ach_Asic_Nc1_PWM_FAULT'
    'Ach_Asic_Nc1_CURR_SENSE'
    'Ach_Asic_Nc1_ADC_FAULT'
    'Ach_Asic_Nc1_T_SD'
    'Ach_Asic_Nc1_OPEN_LOAD'
    'Ach_Asic_Nc1_GND_LOSS'
    'Ach_Asic_Nc1_LVT'
    'Ach_Asic_Nc1_LS_OVC'
    'Ach_Asic_Nc1_VGS_LS_FAULT'
    'Ach_Asic_Nc1_VGS_HS_FAULT'
    'Ach_Asic_Nc1_HS_SHORT'
    'Ach_Asic_Nc1_LS_CLAMP_ON'
    'Ach_Asic_Nc1_UNDER_CURR'
    'Ach_Asic_Nc2_AVG_CURRENT'
    'Ach_Asic_Nc2_PWM_FAULT'
    'Ach_Asic_Nc2_CURR_SENSE'
    'Ach_Asic_Nc2_ADC_FAULT'
    'Ach_Asic_Nc2_T_SD'
    'Ach_Asic_Nc2_OPEN_LOAD'
    'Ach_Asic_Nc2_GND_LOSS'
    'Ach_Asic_Nc2_LVT'
    'Ach_Asic_Nc2_LS_OVC'
    'Ach_Asic_Nc2_VGS_LS_FAULT'
    'Ach_Asic_Nc2_VGS_HS_FAULT'
    'Ach_Asic_Nc2_HS_SHORT'
    'Ach_Asic_Nc2_LS_CLAMP_ON'
    'Ach_Asic_Nc2_UNDER_CURR'
    'Ach_Asic_Nc3_AVG_CURRENT'
    'Ach_Asic_Nc3_PWM_FAULT'
    'Ach_Asic_Nc3_CURR_SENSE'
    'Ach_Asic_Nc3_ADC_FAULT'
    'Ach_Asic_Nc3_T_SD'
    'Ach_Asic_Nc3_OPEN_LOAD'
    'Ach_Asic_Nc3_GND_LOSS'
    'Ach_Asic_Nc3_LVT'
    'Ach_Asic_Nc3_LS_OVC'
    'Ach_Asic_Nc3_VGS_LS_FAULT'
    'Ach_Asic_Nc3_VGS_HS_FAULT'
    'Ach_Asic_Nc3_HS_SHORT'
    'Ach_Asic_Nc3_LS_CLAMP_ON'
    'Ach_Asic_Nc3_UNDER_CURR'
    'Ach_Asic_Tc0_AVG_CURRENT'
    'Ach_Asic_Tc0_PWM_FAULT'
    'Ach_Asic_Tc0_CURR_SENSE'
    'Ach_Asic_Tc0_ADC_FAULT'
    'Ach_Asic_Tc0_T_SD'
    'Ach_Asic_Tc0_OPEN_LOAD'
    'Ach_Asic_Tc0_GND_LOSS'
    'Ach_Asic_Tc0_LVT'
    'Ach_Asic_Tc0_LS_OVC'
    'Ach_Asic_Tc0_VGS_LS_FAULT'
    'Ach_Asic_Tc0_VGS_HS_FAULT'
    'Ach_Asic_Tc0_HS_SHORT'
    'Ach_Asic_Tc0_LS_CLAMP_ON'
    'Ach_Asic_Tc0_UNDER_CURR'
    'Ach_Asic_Tc1_AVG_CURRENT'
    'Ach_Asic_Tc1_PWM_FAULT'
    'Ach_Asic_Tc1_CURR_SENSE'
    'Ach_Asic_Tc1_ADC_FAULT'
    'Ach_Asic_Tc1_T_SD'
    'Ach_Asic_Tc1_OPEN_LOAD'
    'Ach_Asic_Tc1_GND_LOSS'
    'Ach_Asic_Tc1_LVT'
    'Ach_Asic_Tc1_LS_OVC'
    'Ach_Asic_Tc1_VGS_LS_FAULT'
    'Ach_Asic_Tc1_VGS_HS_FAULT'
    'Ach_Asic_Tc1_HS_SHORT'
    'Ach_Asic_Tc1_LS_CLAMP_ON'
    'Ach_Asic_Tc1_UNDER_CURR'
    'Ach_Asic_Esv0_AVG_CURRENT'
    'Ach_Asic_Esv0_PWM_FAULT'
    'Ach_Asic_Esv0_CURR_SENSE'
    'Ach_Asic_Esv0_ADC_FAULT'
    'Ach_Asic_Esv0_T_SD'
    'Ach_Asic_Esv0_OPEN_LOAD'
    'Ach_Asic_Esv0_GND_LOSS'
    'Ach_Asic_Esv0_LVT'
    'Ach_Asic_Esv0_LS_OVC'
    'Ach_Asic_Esv0_VGS_LS_FAULT'
    'Ach_Asic_Esv0_VGS_HS_FAULT'
    'Ach_Asic_Esv0_HS_SHORT'
    'Ach_Asic_Esv0_LS_CLAMP_ON'
    'Ach_Asic_Esv0_UNDER_CURR'
    'Ach_Asic_Esv1_AVG_CURRENT'
    'Ach_Asic_Esv1_PWM_FAULT'
    'Ach_Asic_Esv1_CURR_SENSE'
    'Ach_Asic_Esv1_ADC_FAULT'
    'Ach_Asic_Esv1_T_SD'
    'Ach_Asic_Esv1_OPEN_LOAD'
    'Ach_Asic_Esv1_GND_LOSS'
    'Ach_Asic_Esv1_LVT'
    'Ach_Asic_Esv1_LS_OVC'
    'Ach_Asic_Esv1_VGS_LS_FAULT'
    'Ach_Asic_Esv1_VGS_HS_FAULT'
    'Ach_Asic_Esv1_HS_SHORT'
    'Ach_Asic_Esv1_LS_CLAMP_ON'
    'Ach_Asic_Esv1_UNDER_CURR'
    };
Ach_InputAchValveAsicInfo = CreateBus(Ach_InputAchValveAsicInfo, DeList);
clear DeList;

Ach_InputAchWssPort0AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH0_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH0_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH0_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH0_WSIRSDR3_NODATA'
    'Ach_Asic_CH0_WSIRSDR3_INVALID'
    'Ach_Asic_CH0_WSIRSDR3_OPEN'
    'Ach_Asic_CH0_WSIRSDR3_STB'
    'Ach_Asic_CH0_WSIRSDR3_STG'
    'Ach_Asic_CH0_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH0_WSIRSDR3_CURRENT_HI'
    };
Ach_InputAchWssPort0AsicInfo = CreateBus(Ach_InputAchWssPort0AsicInfo, DeList);
clear DeList;

Ach_InputAchWssPort1AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH1_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH1_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH1_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH1_WSIRSDR3_NODATA'
    'Ach_Asic_CH1_WSIRSDR3_INVALID'
    'Ach_Asic_CH1_WSIRSDR3_OPEN'
    'Ach_Asic_CH1_WSIRSDR3_STB'
    'Ach_Asic_CH1_WSIRSDR3_STG'
    'Ach_Asic_CH1_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH1_WSIRSDR3_CURRENT_HI'
    };
Ach_InputAchWssPort1AsicInfo = CreateBus(Ach_InputAchWssPort1AsicInfo, DeList);
clear DeList;

Ach_InputAchWssPort2AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH2_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH2_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH2_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH2_WSIRSDR3_NODATA'
    'Ach_Asic_CH2_WSIRSDR3_INVALID'
    'Ach_Asic_CH2_WSIRSDR3_OPEN'
    'Ach_Asic_CH2_WSIRSDR3_STB'
    'Ach_Asic_CH2_WSIRSDR3_STG'
    'Ach_Asic_CH2_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH2_WSIRSDR3_CURRENT_HI'
    };
Ach_InputAchWssPort2AsicInfo = CreateBus(Ach_InputAchWssPort2AsicInfo, DeList);
clear DeList;

Ach_InputAchWssPort3AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH3_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH3_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH3_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH3_WSIRSDR3_NODATA'
    'Ach_Asic_CH3_WSIRSDR3_INVALID'
    'Ach_Asic_CH3_WSIRSDR3_OPEN'
    'Ach_Asic_CH3_WSIRSDR3_STB'
    'Ach_Asic_CH3_WSIRSDR3_STG'
    'Ach_Asic_CH3_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH3_WSIRSDR3_CURRENT_HI'
    };
Ach_InputAchWssPort3AsicInfo = CreateBus(Ach_InputAchWssPort3AsicInfo, DeList);
clear DeList;

SentH_MainSentHPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
SentH_MainSentHPressureInfo = CreateBus(SentH_MainSentHPressureInfo, DeList);
clear DeList;

Ioc_InputSR1msHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
Ioc_InputSR1msHalPressureInfo = CreateBus(Ioc_InputSR1msHalPressureInfo, DeList);
clear DeList;

Ioc_InputSR1msMotMonInfo = Simulink.Bus;
DeList={
    'MotPwrVoltMon'
    'MotStarMon'
    };
Ioc_InputSR1msMotMonInfo = CreateBus(Ioc_InputSR1msMotMonInfo, DeList);
clear DeList;

Ioc_InputSR1msMotVoltsMonInfo = Simulink.Bus;
DeList={
    'MotVoltPhUMon'
    'MotVoltPhVMon'
    'MotVoltPhWMon'
    };
Ioc_InputSR1msMotVoltsMonInfo = CreateBus(Ioc_InputSR1msMotVoltsMonInfo, DeList);
clear DeList;

Ioc_InputSR1msPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
Ioc_InputSR1msPedlSigMonInfo = CreateBus(Ioc_InputSR1msPedlSigMonInfo, DeList);
clear DeList;

Ioc_InputSR1msPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
Ioc_InputSR1msPedlPwrMonInfo = CreateBus(Ioc_InputSR1msPedlPwrMonInfo, DeList);
clear DeList;

Ioc_InputCS1msSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BflSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'DoorSwtMon'
    'EscSwtMon'
    'FlexBrkASwtMon'
    'FlexBrkBSwtMon'
    'HzrdSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
Ioc_InputCS1msSwtMonInfo = CreateBus(Ioc_InputCS1msSwtMonInfo, DeList);
clear DeList;

Ioc_InputCS1msSwtMonInfoEsc = Simulink.Bus;
DeList={
    'BlsSwtMon_Esc'
    'AvhSwtMon_Esc'
    'EscSwtMon_Esc'
    'HdcSwtMon_Esc'
    'PbSwtMon_Esc'
    'GearRSwtMon_Esc'
    'BlfSwtMon_Esc'
    'ClutchSwtMon_Esc'
    'ItpmsSwtMon_Esc'
    };
Ioc_InputCS1msSwtMonInfoEsc = CreateBus(Ioc_InputCS1msSwtMonInfoEsc, DeList);
clear DeList;

Ioc_InputCS1msRlyMonInfo = Simulink.Bus;
DeList={
    'RlyDbcMon'
    'RlyEssMon'
    'RlyFault'
    };
Ioc_InputCS1msRlyMonInfo = CreateBus(Ioc_InputCS1msRlyMonInfo, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvdFF0DcdInfo = Simulink.Bus;
DeList={
    'C0ol'
    'C0sb'
    'C0sg'
    'C1ol'
    'C1sb'
    'C1sg'
    'C2ol'
    'C2sb'
    'C2sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
Vlvd_A3944_HndlrVlvdFF0DcdInfo = CreateBus(Vlvd_A3944_HndlrVlvdFF0DcdInfo, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvdFF1DcdInfo = Simulink.Bus;
DeList={
    'C3ol'
    'C3sb'
    'C3sg'
    'C4ol'
    'C4sb'
    'C4sg'
    'C5ol'
    'C5sb'
    'C5sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
Vlvd_A3944_HndlrVlvdFF1DcdInfo = CreateBus(Vlvd_A3944_HndlrVlvdFF1DcdInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo = CreateBus(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo = CreateBus(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo, DeList);
clear DeList;

Pedal_SenPdtBufInfo = Simulink.Bus;
DeList={
    'PdtRaw_array_0'
    'PdtRaw_array_1'
    'PdtRaw_array_2'
    'PdtRaw_array_3'
    'PdtRaw_array_4'
    };
Pedal_SenPdtBufInfo = CreateBus(Pedal_SenPdtBufInfo, DeList);
clear DeList;

Pedal_SenPdfBufInfo = Simulink.Bus;
DeList={
    'PdfRaw_array_0'
    'PdfRaw_array_1'
    'PdfRaw_array_2'
    'PdfRaw_array_3'
    'PdfRaw_array_4'
    };
Pedal_SenPdfBufInfo = CreateBus(Pedal_SenPdfBufInfo, DeList);
clear DeList;

Press_SenCircPBufInfo = Simulink.Bus;
DeList={
    'PrimCircPRaw_array_0'
    'PrimCircPRaw_array_1'
    'PrimCircPRaw_array_2'
    'PrimCircPRaw_array_3'
    'PrimCircPRaw_array_4'
    'SecdCircPRaw_array_0'
    'SecdCircPRaw_array_1'
    'SecdCircPRaw_array_2'
    'SecdCircPRaw_array_3'
    'SecdCircPRaw_array_4'
    };
Press_SenCircPBufInfo = CreateBus(Press_SenCircPBufInfo, DeList);
clear DeList;

Press_SenPistPBufInfo = Simulink.Bus;
DeList={
    'PistPRaw_array_0'
    'PistPRaw_array_1'
    'PistPRaw_array_2'
    'PistPRaw_array_3'
    'PistPRaw_array_4'
    };
Press_SenPistPBufInfo = CreateBus(Press_SenPistPBufInfo, DeList);
clear DeList;

Press_SenPspBufInfo = Simulink.Bus;
DeList={
    'PedlSimPRaw_array_0'
    'PedlSimPRaw_array_1'
    'PedlSimPRaw_array_2'
    'PedlSimPRaw_array_3'
    'PedlSimPRaw_array_4'
    };
Press_SenPspBufInfo = CreateBus(Press_SenPspBufInfo, DeList);
clear DeList;

Press_SenPressCalcInfo = Simulink.Bus;
DeList={
    'PressP_SimP_1_100_bar'
    'PressP_CirP1_1_100_bar'
    'PressP_CirP2_1_100_bar'
    'SimPMoveAvr'
    'CirPMoveAvr'
    'CirP2MoveAvr'
    };
Press_SenPressCalcInfo = CreateBus(Press_SenPressCalcInfo, DeList);
clear DeList;

Spc_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Spc_1msCtrlPedlTrvlFild1msInfo = CreateBus(Spc_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Det_1msCtrlBrkPedlStatus1msInfo = Simulink.Bus;
DeList={
    'PanicBrkSt1msFlg'
    };
Det_1msCtrlBrkPedlStatus1msInfo = CreateBus(Det_1msCtrlBrkPedlStatus1msInfo, DeList);
clear DeList;

Det_1msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Det_1msCtrlPedlTrvlRate1msInfo = CreateBus(Det_1msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;

Mcc_1msCtrlMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Mcc_1msCtrlMotDqIRefMccInfo = CreateBus(Mcc_1msCtrlMotDqIRefMccInfo, DeList);
clear DeList;

Mcc_1msCtrlIdbMotPosnInfo = Simulink.Bus;
DeList={
    'MotTarPosn'
    };
Mcc_1msCtrlIdbMotPosnInfo = CreateBus(Mcc_1msCtrlIdbMotPosnInfo, DeList);
clear DeList;

Mcc_1msCtrlFluxWeakengStInfo = Simulink.Bus;
DeList={
    'FluxWeakengFlg'
    'FluxWeakengFlgOld'
    };
Mcc_1msCtrlFluxWeakengStInfo = CreateBus(Mcc_1msCtrlFluxWeakengStInfo, DeList);
clear DeList;

Ses_CtrlNormVlvReqSesInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    };
Ses_CtrlNormVlvReqSesInfo = CreateBus(Ses_CtrlNormVlvReqSesInfo, DeList);
clear DeList;

Ses_CtrlWhlVlvReqSesInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    };
Ses_CtrlWhlVlvReqSesInfo = CreateBus(Ses_CtrlWhlVlvReqSesInfo, DeList);
clear DeList;

Ses_CtrlMotOrgSetStInfo = Simulink.Bus;
DeList={
    'MotOrgSetErr'
    'MotOrgSet'
    };
Ses_CtrlMotOrgSetStInfo = CreateBus(Ses_CtrlMotOrgSetStInfo, DeList);
clear DeList;

Ses_CtrlMotDqIRefSesInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Ses_CtrlMotDqIRefSesInfo = CreateBus(Ses_CtrlMotDqIRefSesInfo, DeList);
clear DeList;

Ses_CtrlPwrPistStBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'PosiWallTemp'
    'WallDetectedFlg'
    };
Ses_CtrlPwrPistStBfMotOrgSetInfo = CreateBus(Ses_CtrlPwrPistStBfMotOrgSetInfo, DeList);
clear DeList;

Mgd_TLE9180_HndlrMgdDcdInfo = Simulink.Bus;
DeList={
    'mgdIdleM'
    'mgdConfM'
    'mgdConfLock'
    'mgdSelfTestM'
    'mgdSoffM'
    'mgdErrM'
    'mgdRectM'
    'mgdNormM'
    'mgdOsf'
    'mgdOp'
    'mgdScd'
    'mgdSd'
    'mgdIndiag'
    'mgdOutp'
    'mgdExt'
    'mgdInt12'
    'mgdRom'
    'mgdLimpOn'
    'mgdStIncomplete'
    'mgdApcAct'
    'mgdGtm'
    'mgdCtrlRegInvalid'
    'mgdLfw'
    'mgdErrOtW'
    'mgdErrOvReg1'
    'mgdErrUvVccRom'
    'mgdErrUvReg4'
    'mgdErrOvReg6'
    'mgdErrUvReg6'
    'mgdErrUvReg5'
    'mgdErrUvCb'
    'mgdErrClkTrim'
    'mgdErrUvBs3'
    'mgdErrUvBs2'
    'mgdErrUvBs1'
    'mgdErrCp2'
    'mgdErrCp1'
    'mgdErrOvBs3'
    'mgdErrOvBs2'
    'mgdErrOvBs1'
    'mgdErrOvVdh'
    'mgdErrUvVdh'
    'mgdErrOvVs'
    'mgdErrUvVs'
    'mgdErrUvVcc'
    'mgdErrOvVcc'
    'mgdErrOvLdVdh'
    'mgdSdDdpStuck'
    'mgdSdCp1'
    'mgdSdOvCp'
    'mgdSdClkfail'
    'mgdSdUvCb'
    'mgdSdOvVdh'
    'mgdSdOvVs'
    'mgdSdOt'
    'mgdErrScdLs3'
    'mgdErrScdLs2'
    'mgdErrScdLs1'
    'mgdErrScdHs3'
    'mgdErrScdHs2'
    'mgdErrScdHs1'
    'mgdErrIndLs3'
    'mgdErrIndLs2'
    'mgdErrIndLs1'
    'mgdErrIndHs3'
    'mgdErrIndHs2'
    'mgdErrIndHs1'
    'mgdErrOsfLs1'
    'mgdErrOsfLs2'
    'mgdErrOsfLs3'
    'mgdErrOsfHs1'
    'mgdErrOsfHs2'
    'mgdErrOsfHs3'
    'mgdErrSpiFrame'
    'mgdErrSpiTo'
    'mgdErrSpiWd'
    'mgdErrSpiCrc'
    'mgdEpiAddInvalid'
    'mgdEonfTo'
    'mgdEonfSigInvalid'
    'mgdErrOcOp1'
    'mgdErrOp1Uv'
    'mgdErrOp1Ov'
    'mgdErrOp1Calib'
    'mgdErrOcOp2'
    'mgdErrOp2Uv'
    'mgdErrOp2Ov'
    'mgdErrOp2Calib'
    'mgdErrOcOp3'
    'mgdErrOp3Uv'
    'mgdErrOp3Ov'
    'mgdErrOp3Calib'
    'mgdErrOutpErrn'
    'mgdErrOutpMiso'
    'mgdErrOutpPFB1'
    'mgdErrOutpPFB2'
    'mgdErrOutpPFB3'
    'mgdTle9180ErrPort'
    };
Mgd_TLE9180_HndlrMgdDcdInfo = CreateBus(Mgd_TLE9180_HndlrMgdDcdInfo, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    'MtrCaliResult'
    };
Mtr_Processing_DiagMtrProcessDataInf = CreateBus(Mtr_Processing_DiagMtrProcessDataInf, DeList);
clear DeList;

Arbitrator_MtrMtrArbtratorData = Simulink.Bus;
DeList={
    'IqSelected'
    'IdSelected'
    'UPhasePWMSelected'
    'VPhasePWMSelected'
    'WPhasePWMSelected'
    'MtrCtrlMode'
    };
Arbitrator_MtrMtrArbtratorData = CreateBus(Arbitrator_MtrMtrArbtratorData, DeList);
clear DeList;

Arbitrator_MtrMtrArbtratorInfo = Simulink.Bus;
DeList={
    'MtrArbCalMode'
    'MtrArbState'
    };
Arbitrator_MtrMtrArbtratorInfo = CreateBus(Arbitrator_MtrMtrArbtratorInfo, DeList);
clear DeList;

AdcIf_Conv1msAdcInvalid = Simulink.Bus;
DeList={'AdcIf_Conv1msAdcInvalid'};
AdcIf_Conv1msAdcInvalid = CreateBus(AdcIf_Conv1msAdcInvalid, DeList);
clear DeList;

Ach_InputAchAsicInvalid = Simulink.Bus;
DeList={'Ach_InputAchAsicInvalid'};
Ach_InputAchAsicInvalid = CreateBus(Ach_InputAchAsicInvalid, DeList);
clear DeList;

Ioc_InputSR1msVBatt1Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVBatt1Mon'};
Ioc_InputSR1msVBatt1Mon = CreateBus(Ioc_InputSR1msVBatt1Mon, DeList);
clear DeList;

Ioc_InputSR1msVBatt2Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVBatt2Mon'};
Ioc_InputSR1msVBatt2Mon = CreateBus(Ioc_InputSR1msVBatt2Mon, DeList);
clear DeList;

Ioc_InputSR1msFspCbsHMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspCbsHMon'};
Ioc_InputSR1msFspCbsHMon = CreateBus(Ioc_InputSR1msFspCbsHMon, DeList);
clear DeList;

Ioc_InputSR1msFspCbsMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspCbsMon'};
Ioc_InputSR1msFspCbsMon = CreateBus(Ioc_InputSR1msFspCbsMon, DeList);
clear DeList;

Ioc_InputSR1msFspAbsHMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspAbsHMon'};
Ioc_InputSR1msFspAbsHMon = CreateBus(Ioc_InputSR1msFspAbsHMon, DeList);
clear DeList;

Ioc_InputSR1msFspAbsMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspAbsMon'};
Ioc_InputSR1msFspAbsMon = CreateBus(Ioc_InputSR1msFspAbsMon, DeList);
clear DeList;

Ioc_InputSR1msExt5vMon = Simulink.Bus;
DeList={'Ioc_InputSR1msExt5vMon'};
Ioc_InputSR1msExt5vMon = CreateBus(Ioc_InputSR1msExt5vMon, DeList);
clear DeList;

Ioc_InputSR1msCEMon = Simulink.Bus;
DeList={'Ioc_InputSR1msCEMon'};
Ioc_InputSR1msCEMon = CreateBus(Ioc_InputSR1msCEMon, DeList);
clear DeList;

Ioc_InputSR1msVdd1Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd1Mon'};
Ioc_InputSR1msVdd1Mon = CreateBus(Ioc_InputSR1msVdd1Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd2Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd2Mon'};
Ioc_InputSR1msVdd2Mon = CreateBus(Ioc_InputSR1msVdd2Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd3Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd3Mon'};
Ioc_InputSR1msVdd3Mon = CreateBus(Ioc_InputSR1msVdd3Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd4Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd4Mon'};
Ioc_InputSR1msVdd4Mon = CreateBus(Ioc_InputSR1msVdd4Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd5Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd5Mon'};
Ioc_InputSR1msVdd5Mon = CreateBus(Ioc_InputSR1msVdd5Mon, DeList);
clear DeList;

Ioc_InputSR1msVddMon = Simulink.Bus;
DeList={'Ioc_InputSR1msVddMon'};
Ioc_InputSR1msVddMon = CreateBus(Ioc_InputSR1msVddMon, DeList);
clear DeList;

Ioc_InputSR1msCspMon = Simulink.Bus;
DeList={'Ioc_InputSR1msCspMon'};
Ioc_InputSR1msCspMon = CreateBus(Ioc_InputSR1msCspMon, DeList);
clear DeList;

Ioc_InputCS1msRsmDbcMon = Simulink.Bus;
DeList={'Ioc_InputCS1msRsmDbcMon'};
Ioc_InputCS1msRsmDbcMon = CreateBus(Ioc_InputCS1msRsmDbcMon, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvdInvalid = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrVlvdInvalid'};
Vlvd_A3944_HndlrVlvdInvalid = CreateBus(Vlvd_A3944_HndlrVlvdInvalid, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsInvalid = Simulink.Bus;
DeList={'Mps_TLE5012_Hndlr_1msMpsInvalid'};
Mps_TLE5012_Hndlr_1msMpsInvalid = CreateBus(Mps_TLE5012_Hndlr_1msMpsInvalid, DeList);
clear DeList;

Msp_1msCtrlVdcLinkFild = Simulink.Bus;
DeList={'Msp_1msCtrlVdcLinkFild'};
Msp_1msCtrlVdcLinkFild = CreateBus(Msp_1msCtrlVdcLinkFild, DeList);
clear DeList;

Pct_1msCtrlPreBoostMod = Simulink.Bus;
DeList={'Pct_1msCtrlPreBoostMod'};
Pct_1msCtrlPreBoostMod = CreateBus(Pct_1msCtrlPreBoostMod, DeList);
clear DeList;

Mcc_1msCtrlMotCtrlMode = Simulink.Bus;
DeList={'Mcc_1msCtrlMotCtrlMode'};
Mcc_1msCtrlMotCtrlMode = CreateBus(Mcc_1msCtrlMotCtrlMode, DeList);
clear DeList;

Mcc_1msCtrlMotCtrlState = Simulink.Bus;
DeList={'Mcc_1msCtrlMotCtrlState'};
Mcc_1msCtrlMotCtrlState = CreateBus(Mcc_1msCtrlMotCtrlState, DeList);
clear DeList;

Mcc_1msCtrlMotICtrlFadeOutState = Simulink.Bus;
DeList={'Mcc_1msCtrlMotICtrlFadeOutState'};
Mcc_1msCtrlMotICtrlFadeOutState = CreateBus(Mcc_1msCtrlMotICtrlFadeOutState, DeList);
clear DeList;

Mcc_1msCtrlStkRecvryStabnEndOK = Simulink.Bus;
DeList={'Mcc_1msCtrlStkRecvryStabnEndOK'};
Mcc_1msCtrlStkRecvryStabnEndOK = CreateBus(Mcc_1msCtrlStkRecvryStabnEndOK, DeList);
clear DeList;

Acm_MainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Acm_MainAcmAsicInitCompleteFlag'};
Acm_MainAcmAsicInitCompleteFlag = CreateBus(Acm_MainAcmAsicInitCompleteFlag, DeList);
clear DeList;

Mgd_TLE9180_HndlrMgdInvalid = Simulink.Bus;
DeList={'Mgd_TLE9180_HndlrMgdInvalid'};
Mgd_TLE9180_HndlrMgdInvalid = CreateBus(Mgd_TLE9180_HndlrMgdInvalid, DeList);
clear DeList;

Mtr_Processing_DiagMtrDiagState = Simulink.Bus;
DeList={'Mtr_Processing_DiagMtrDiagState'};
Mtr_Processing_DiagMtrDiagState = CreateBus(Mtr_Processing_DiagMtrDiagState, DeList);
clear DeList;

Arbitrator_MtrMtrArbDriveState = Simulink.Bus;
DeList={'Arbitrator_MtrMtrArbDriveState'};
Arbitrator_MtrMtrArbDriveState = CreateBus(Arbitrator_MtrMtrArbDriveState, DeList);
clear DeList;




AdcIf_Conv1msSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Ext5vMon'
    'CEMon'
    'Int5vMon'
    'CSPMon'
    'Vbatt01Mon'
    'Vbatt02Mon'
    'VddMon'
    'Vdd3Mon'
    'Vdd5Mon'
    };
AdcIf_Conv1msSwTrigPwrInfo = CreateBus(AdcIf_Conv1msSwTrigPwrInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigFspInfo = Simulink.Bus;
DeList={
    'FspAbsHMon'
    'FspAbsMon'
    'FspCbsHMon'
    'FspCbsMon'
    };
AdcIf_Conv1msSwTrigFspInfo = CreateBus(AdcIf_Conv1msSwTrigFspInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigTmpInfo = Simulink.Bus;
DeList={
    'LineTestMon'
    'TempMon'
    };
AdcIf_Conv1msSwTrigTmpInfo = CreateBus(AdcIf_Conv1msSwTrigTmpInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
AdcIf_Conv1msSwTrigMotInfo = CreateBus(AdcIf_Conv1msSwTrigMotInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigPdtInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'Pdt5vMon'
    'Pdf5vMon'
    'PdtSigMon'
    };
AdcIf_Conv1msSwTrigPdtInfo = CreateBus(AdcIf_Conv1msSwTrigPdtInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigMocInfo = Simulink.Bus;
DeList={
    'RlIMainMon'
    'RlISubMonFs'
    'RlMocNegMon'
    'RlMocPosMon'
    'RrIMainMon'
    'RrISubMonFs'
    'RrMocNegMon'
    'RrMocPosMon'
    'BFLMon'
    };
AdcIf_Conv1msSwTrigMocInfo = CreateBus(AdcIf_Conv1msSwTrigMocInfo, DeList);
clear DeList;

AdcIf_Conv1msHwTrigVlvInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'VlvCutPMon'
    'VlvCutSMon'
    'VlvSimMon'
    };
AdcIf_Conv1msHwTrigVlvInfo = CreateBus(AdcIf_Conv1msHwTrigVlvInfo, DeList);
clear DeList;

AdcIf_Conv1msSwTrigEscInfo = Simulink.Bus;
DeList={
    'IF_A7_MON'
    'IF_A2_MON'
    'P1_NEG_MON'
    'IF_A6_MON'
    'MTR_FW_S_MON'
    'IF_A4_MON'
    'MTP_MON'
    'IF_A3_MON'
    'P1_POS_MON'
    'IF_A5_MON'
    'VDD_MON'
    'COOL_MON'
    'CAN_L_MON'
    'P3_NEG_MON'
    'IF_A1_MON'
    'P2_POS_MON'
    'IF_A8_MON'
    'IF_A9_MON'
    'P2_NEG_MON'
    'P3_POS_MON'
    'OP_OUT_MON'
    };
AdcIf_Conv1msSwTrigEscInfo = CreateBus(AdcIf_Conv1msSwTrigEscInfo, DeList);
clear DeList;

AdcIf_Conv1msAdcInvalid = Simulink.Bus;
DeList={'AdcIf_Conv1msAdcInvalid'};
AdcIf_Conv1msAdcInvalid = CreateBus(AdcIf_Conv1msAdcInvalid, DeList);
clear DeList;


Ach_InputAchAdcData = Simulink.Bus;
DeList={
    'ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT'
    'ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY'
    };
Ach_InputAchAdcData = CreateBus(Ach_InputAchAdcData, DeList);
clear DeList;


SentH_MainSentHPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
SentH_MainSentHPressureInfo = CreateBus(SentH_MainSentHPressureInfo, DeList);
clear DeList;


Ioc_InputSR1msAchAdcData = Simulink.Bus;
DeList={
    'ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT'
    'ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY'
    };
Ioc_InputSR1msAchAdcData = CreateBus(Ioc_InputSR1msAchAdcData, DeList);
clear DeList;

Ioc_InputSR1msSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Ext5vMon'
    'CEMon'
    'Int5vMon'
    'CSPMon'
    'Vbatt01Mon'
    'Vbatt02Mon'
    'VddMon'
    'Vdd3Mon'
    'Vdd5Mon'
    };
Ioc_InputSR1msSwTrigPwrInfo = CreateBus(Ioc_InputSR1msSwTrigPwrInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigFspInfo = Simulink.Bus;
DeList={
    'FspAbsHMon'
    'FspAbsMon'
    'FspCbsHMon'
    'FspCbsMon'
    };
Ioc_InputSR1msSwTrigFspInfo = CreateBus(Ioc_InputSR1msSwTrigFspInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigTmpInfo = Simulink.Bus;
DeList={
    'LineTestMon'
    'TempMon'
    };
Ioc_InputSR1msSwTrigTmpInfo = CreateBus(Ioc_InputSR1msSwTrigTmpInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
Ioc_InputSR1msSwTrigMotInfo = CreateBus(Ioc_InputSR1msSwTrigMotInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigPdtInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'Pdt5vMon'
    'Pdf5vMon'
    'PdtSigMon'
    };
Ioc_InputSR1msSwTrigPdtInfo = CreateBus(Ioc_InputSR1msSwTrigPdtInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigMocInfo = Simulink.Bus;
DeList={
    'RlIMainMon'
    'RlISubMonFs'
    'RlMocNegMon'
    'RlMocPosMon'
    'RrIMainMon'
    'RrISubMonFs'
    'RrMocNegMon'
    'RrMocPosMon'
    'BFLMon'
    };
Ioc_InputSR1msSwTrigMocInfo = CreateBus(Ioc_InputSR1msSwTrigMocInfo, DeList);
clear DeList;

Ioc_InputSR1msHwTrigVlvInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'VlvCutPMon'
    'VlvCutSMon'
    'VlvSimMon'
    };
Ioc_InputSR1msHwTrigVlvInfo = CreateBus(Ioc_InputSR1msHwTrigVlvInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigEscInfo = Simulink.Bus;
DeList={
    'IF_A7_MON'
    'IF_A2_MON'
    'P1_NEG_MON'
    'IF_A6_MON'
    'MTR_FW_S_MON'
    'IF_A4_MON'
    'MTP_MON'
    'IF_A3_MON'
    'P1_POS_MON'
    'IF_A5_MON'
    'VDD_MON'
    'COOL_MON'
    'CAN_L_MON'
    'P3_NEG_MON'
    'IF_A1_MON'
    'P2_POS_MON'
    'IF_A8_MON'
    'IF_A9_MON'
    'P2_NEG_MON'
    'P3_POS_MON'
    'OP_OUT_MON'
    };
Ioc_InputSR1msSwTrigEscInfo = CreateBus(Ioc_InputSR1msSwTrigEscInfo, DeList);
clear DeList;

Ioc_InputSR1msSentHPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
Ioc_InputSR1msSentHPressureInfo = CreateBus(Ioc_InputSR1msSentHPressureInfo, DeList);
clear DeList;

Ioc_InputSR1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_InputSR1msAcmAsicInitCompleteFlag'};
Ioc_InputSR1msAcmAsicInitCompleteFlag = CreateBus(Ioc_InputSR1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_InputSR1msAdcInvalid = Simulink.Bus;
DeList={'Ioc_InputSR1msAdcInvalid'};
Ioc_InputSR1msAdcInvalid = CreateBus(Ioc_InputSR1msAdcInvalid, DeList);
clear DeList;

Ioc_InputSR1msPwrMonInfoEsc = Simulink.Bus;
DeList={
    'VoltVBatt01Mon_Esc'
    'VoltVBatt02Mon_Esc'
    'VoltGio2Mon_Esc'
    'VoltExt5VSupplyMon_Esc'
    'VoltExt12VSupplyMon_Esc'
    'VoltSwVpwrMon_Esc'
    'VoltVpwrMon_Esc'
    'VoltGio1Mon_Esc'
    'VoltIgnMon_Esc'
    };
Ioc_InputSR1msPwrMonInfoEsc = CreateBus(Ioc_InputSR1msPwrMonInfoEsc, DeList);
clear DeList;

Ioc_InputSR1msHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
Ioc_InputSR1msHalPressureInfo = CreateBus(Ioc_InputSR1msHalPressureInfo, DeList);
clear DeList;

Ioc_InputSR1msPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
Ioc_InputSR1msPedlSigMonInfo = CreateBus(Ioc_InputSR1msPedlSigMonInfo, DeList);
clear DeList;

Ioc_InputSR1msPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
Ioc_InputSR1msPedlPwrMonInfo = CreateBus(Ioc_InputSR1msPedlPwrMonInfo, DeList);
clear DeList;

Ioc_InputSR1msVlvMonInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'PrimCutVlvMon'
    'SecdCutVlvMon'
    'SimVlvMon'
    };
Ioc_InputSR1msVlvMonInfo = CreateBus(Ioc_InputSR1msVlvMonInfo, DeList);
clear DeList;


Ioc_InputCS1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_InputCS1msAcmAsicInitCompleteFlag'};
Ioc_InputCS1msAcmAsicInitCompleteFlag = CreateBus(Ioc_InputCS1msAcmAsicInitCompleteFlag, DeList);
clear DeList;


CanTrv_TLE6251_HndlrMainCanEn = Simulink.Bus;
DeList={'CanTrv_TLE6251_HndlrMainCanEn'};
CanTrv_TLE6251_HndlrMainCanEn = CreateBus(CanTrv_TLE6251_HndlrMainCanEn, DeList);
clear DeList;


Vlvd_A3944_HndlrVlvDrvEnRst = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrVlvDrvEnRst'};
Vlvd_A3944_HndlrVlvDrvEnRst = CreateBus(Vlvd_A3944_HndlrVlvDrvEnRst, DeList);
clear DeList;



Pedal_SenPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
Pedal_SenPedlSigMonInfo = CreateBus(Pedal_SenPedlSigMonInfo, DeList);
clear DeList;

Pedal_SenPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
Pedal_SenPedlPwrMonInfo = CreateBus(Pedal_SenPedlPwrMonInfo, DeList);
clear DeList;

Pedal_SenPdt1msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Pedal_SenPdt1msRawInfo = CreateBus(Pedal_SenPdt1msRawInfo, DeList);
clear DeList;


Press_SenHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
Press_SenHalPressureInfo = CreateBus(Press_SenHalPressureInfo, DeList);
clear DeList;

Press_SenCircP1msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Press_SenCircP1msRawInfo = CreateBus(Press_SenCircP1msRawInfo, DeList);
clear DeList;

Press_SenPistP1msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Press_SenPistP1msRawInfo = CreateBus(Press_SenPistP1msRawInfo, DeList);
clear DeList;


Msp_1msCtrlVdcLinkFild = Simulink.Bus;
DeList={'Msp_1msCtrlVdcLinkFild'};
Msp_1msCtrlVdcLinkFild = CreateBus(Msp_1msCtrlVdcLinkFild, DeList);
clear DeList;


Spc_1msCtrlCircP1msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Spc_1msCtrlCircP1msRawInfo = CreateBus(Spc_1msCtrlCircP1msRawInfo, DeList);
clear DeList;

Spc_1msCtrlPistP1msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Spc_1msCtrlPistP1msRawInfo = CreateBus(Spc_1msCtrlPistP1msRawInfo, DeList);
clear DeList;

Spc_1msCtrlPdt1msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Spc_1msCtrlPdt1msRawInfo = CreateBus(Spc_1msCtrlPdt1msRawInfo, DeList);
clear DeList;

Spc_1msCtrlCircPFild1msInfo = Simulink.Bus;
DeList={
    'PrimCircPFild1ms'
    'SecdCircPFild1ms'
    };
Spc_1msCtrlCircPFild1msInfo = CreateBus(Spc_1msCtrlCircPFild1msInfo, DeList);
clear DeList;

Spc_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Spc_1msCtrlPedlTrvlFild1msInfo = CreateBus(Spc_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Spc_1msCtrlPistPFild1msInfo = Simulink.Bus;
DeList={
    'PistPFild1ms'
    };
Spc_1msCtrlPistPFild1msInfo = CreateBus(Spc_1msCtrlPistPFild1msInfo, DeList);
clear DeList;


Det_1msCtrlCircPFild1msInfo = Simulink.Bus;
DeList={
    'PrimCircPFild1ms'
    'SecdCircPFild1ms'
    };
Det_1msCtrlCircPFild1msInfo = CreateBus(Det_1msCtrlCircPFild1msInfo, DeList);
clear DeList;

Det_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Det_1msCtrlPedlTrvlFild1msInfo = CreateBus(Det_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Det_1msCtrlPistPFild1msInfo = Simulink.Bus;
DeList={
    'PistPFild1ms'
    };
Det_1msCtrlPistPFild1msInfo = CreateBus(Det_1msCtrlPistPFild1msInfo, DeList);
clear DeList;

Det_1msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Det_1msCtrlPedlTrvlRate1msInfo = CreateBus(Det_1msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;


Pct_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Pct_1msCtrlPedlTrvlFild1msInfo = CreateBus(Pct_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Pct_1msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Pct_1msCtrlPedlTrvlRate1msInfo = CreateBus(Pct_1msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;


Mcc_1msCtrlCircPFild1msInfo = Simulink.Bus;
DeList={
    'PrimCircPFild1ms'
    'SecdCircPFild1ms'
    };
Mcc_1msCtrlCircPFild1msInfo = CreateBus(Mcc_1msCtrlCircPFild1msInfo, DeList);
clear DeList;

Mcc_1msCtrlPistPFild1msInfo = Simulink.Bus;
DeList={
    'PistPFild1ms'
    };
Mcc_1msCtrlPistPFild1msInfo = CreateBus(Mcc_1msCtrlPistPFild1msInfo, DeList);
clear DeList;

Mcc_1msCtrlMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Mcc_1msCtrlMotDqIRefMccInfo = CreateBus(Mcc_1msCtrlMotDqIRefMccInfo, DeList);
clear DeList;

Mcc_1msCtrlMotCtrlMode = Simulink.Bus;
DeList={'Mcc_1msCtrlMotCtrlMode'};
Mcc_1msCtrlMotCtrlMode = CreateBus(Mcc_1msCtrlMotCtrlMode, DeList);
clear DeList;

Mcc_1msCtrlMotCtrlState = Simulink.Bus;
DeList={'Mcc_1msCtrlMotCtrlState'};
Mcc_1msCtrlMotCtrlState = CreateBus(Mcc_1msCtrlMotCtrlState, DeList);
clear DeList;



Vlv_ActrVlvMonInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'PrimCutVlvMon'
    'SecdCutVlvMon'
    'SimVlvMon'
    };
Vlv_ActrVlvMonInfo = CreateBus(Vlv_ActrVlvMonInfo, DeList);
clear DeList;

Vlv_ActrAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Vlv_ActrAcmAsicInitCompleteFlag'};
Vlv_ActrAcmAsicInitCompleteFlag = CreateBus(Vlv_ActrAcmAsicInitCompleteFlag, DeList);
clear DeList;

Vlv_ActrNormVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimCutVlvDrvData'
    'RelsVlvDrvData'
    'SecdCutVlvDrvData'
    'CircVlvDrvData'
    'SimVlvDrvData'
    };
Vlv_ActrNormVlvDrvInfo = CreateBus(Vlv_ActrNormVlvDrvInfo, DeList);
clear DeList;

Vlv_ActrWhlVlvDrvInfo = Simulink.Bus;
DeList={
    'FlOvDrvData'
    'FlIvDrvData'
    'FrOvDrvData'
    'FrIvDrvData'
    'RlOvDrvData'
    'RlIvDrvData'
    'RrOvDrvData'
    'RrIvDrvData'
    };
Vlv_ActrWhlVlvDrvInfo = CreateBus(Vlv_ActrWhlVlvDrvInfo, DeList);
clear DeList;

Vlv_ActrBalVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimBalVlvDrvData'
    'PressDumpVlvDrvData'
    'ChmbBalVlvDrvData'
    };
Vlv_ActrBalVlvDrvInfo = CreateBus(Vlv_ActrBalVlvDrvInfo, DeList);
clear DeList;


Acm_MainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Acm_MainAcmAsicInitCompleteFlag'};
Acm_MainAcmAsicInitCompleteFlag = CreateBus(Acm_MainAcmAsicInitCompleteFlag, DeList);
clear DeList;


Aka_MainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Aka_MainAcmAsicInitCompleteFlag'};
Aka_MainAcmAsicInitCompleteFlag = CreateBus(Aka_MainAcmAsicInitCompleteFlag, DeList);
clear DeList;

Aka_MainAkaKeepAliveWriteData = Simulink.Bus;
DeList={
    'KeepAliveDataFlg'
    'ACH_Tx1_PHOLD'
    'ACH_Tx1_KA'
    };
Aka_MainAkaKeepAliveWriteData = CreateBus(Aka_MainAkaKeepAliveWriteData, DeList);
clear DeList;


Awd_mainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Awd_mainAcmAsicInitCompleteFlag'};
Awd_mainAcmAsicInitCompleteFlag = CreateBus(Awd_mainAcmAsicInitCompleteFlag, DeList);
clear DeList;

Awd_mainAwdPrnDrv = Simulink.Bus;
DeList={'Awd_mainAwdPrnDrv'};
Awd_mainAwdPrnDrv = CreateBus(Awd_mainAwdPrnDrv, DeList);
clear DeList;



Ioc_OutputSR1msWhlVlvDrvInfo = Simulink.Bus;
DeList={
    'FlOvDrvData'
    'FlIvDrvData'
    'FrOvDrvData'
    'FrIvDrvData'
    'RlOvDrvData'
    'RlIvDrvData'
    'RrOvDrvData'
    'RrIvDrvData'
    };
Ioc_OutputSR1msWhlVlvDrvInfo = CreateBus(Ioc_OutputSR1msWhlVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msBalVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimBalVlvDrvData'
    'PressDumpVlvDrvData'
    'ChmbBalVlvDrvData'
    };
Ioc_OutputSR1msBalVlvDrvInfo = CreateBus(Ioc_OutputSR1msBalVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_OutputSR1msAcmAsicInitCompleteFlag'};
Ioc_OutputSR1msAcmAsicInitCompleteFlag = CreateBus(Ioc_OutputSR1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_OutputSR1msIocDcMtrDutyData = Simulink.Bus;
DeList={
    'MtrDutyDataFlg'
    'ACH_Tx8_PUMP_DTY_PWM'
    };
Ioc_OutputSR1msIocDcMtrDutyData = CreateBus(Ioc_OutputSR1msIocDcMtrDutyData, DeList);
clear DeList;

Ioc_OutputSR1msIocDcMtrFreqData = Simulink.Bus;
DeList={
    'MtrFreqDataFlg'
    'ACH_Tx8_PUMP_TCK_PWM'
    };
Ioc_OutputSR1msIocDcMtrFreqData = CreateBus(Ioc_OutputSR1msIocDcMtrFreqData, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo0Data = Simulink.Bus;
DeList={
    'VlvNo0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo0Data = CreateBus(Ioc_OutputSR1msIocVlvNo0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo1Data = Simulink.Bus;
DeList={
    'VlvNo1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo1Data = CreateBus(Ioc_OutputSR1msIocVlvNo1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo2Data = Simulink.Bus;
DeList={
    'VlvNo2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo2Data = CreateBus(Ioc_OutputSR1msIocVlvNo2Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo3Data = Simulink.Bus;
DeList={
    'VlvNo3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo3Data = CreateBus(Ioc_OutputSR1msIocVlvNo3Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvTc0Data = Simulink.Bus;
DeList={
    'VlvTc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvTc0Data = CreateBus(Ioc_OutputSR1msIocVlvTc0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvTc1Data = Simulink.Bus;
DeList={
    'VlvTc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvTc1Data = CreateBus(Ioc_OutputSR1msIocVlvTc1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvEsv0Data = Simulink.Bus;
DeList={
    'VlvEsv0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvEsv0Data = CreateBus(Ioc_OutputSR1msIocVlvEsv0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvEsv1Data = Simulink.Bus;
DeList={
    'VlvEsv1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvEsv1Data = CreateBus(Ioc_OutputSR1msIocVlvEsv1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc0Data = Simulink.Bus;
DeList={
    'VlvNc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc0Data = CreateBus(Ioc_OutputSR1msIocVlvNc0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc1Data = Simulink.Bus;
DeList={
    'VlvNc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc1Data = CreateBus(Ioc_OutputSR1msIocVlvNc1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc2Data = Simulink.Bus;
DeList={
    'VlvNc2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc2Data = CreateBus(Ioc_OutputSR1msIocVlvNc2Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc3Data = Simulink.Bus;
DeList={
    'VlvNc3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc3Data = CreateBus(Ioc_OutputSR1msIocVlvNc3Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvRelayData = Simulink.Bus;
DeList={
    'VlvRelayDataFlg'
    'ACH_Tx1_FS_CMD'
    };
Ioc_OutputSR1msIocVlvRelayData = CreateBus(Ioc_OutputSR1msIocVlvRelayData, DeList);
clear DeList;

Ioc_OutputSR1msIocAdcSelWriteData = Simulink.Bus;
DeList={
    'AdcSelDataFlg'
    'ACH_Tx13_ADC_SEL'
    };
Ioc_OutputSR1msIocAdcSelWriteData = CreateBus(Ioc_OutputSR1msIocAdcSelWriteData, DeList);
clear DeList;


Ioc_OutputCS1msNormVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimCutVlvDrvData'
    'RelsVlvDrvData'
    'SecdCutVlvDrvData'
    'CircVlvDrvData'
    'SimVlvDrvData'
    };
Ioc_OutputCS1msNormVlvDrvInfo = CreateBus(Ioc_OutputCS1msNormVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msWhlVlvDrvInfo = Simulink.Bus;
DeList={
    'FlOvDrvData'
    'FlIvDrvData'
    'FrOvDrvData'
    'FrIvDrvData'
    'RlOvDrvData'
    'RlIvDrvData'
    'RrOvDrvData'
    'RrIvDrvData'
    };
Ioc_OutputCS1msWhlVlvDrvInfo = CreateBus(Ioc_OutputCS1msWhlVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_OutputCS1msAcmAsicInitCompleteFlag'};
Ioc_OutputCS1msAcmAsicInitCompleteFlag = CreateBus(Ioc_OutputCS1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_OutputCS1msMainCanEn = Simulink.Bus;
DeList={'Ioc_OutputCS1msMainCanEn'};
Ioc_OutputCS1msMainCanEn = CreateBus(Ioc_OutputCS1msMainCanEn, DeList);
clear DeList;

Ioc_OutputCS1msVlvDrvEnRst = Simulink.Bus;
DeList={'Ioc_OutputCS1msVlvDrvEnRst'};
Ioc_OutputCS1msVlvDrvEnRst = CreateBus(Ioc_OutputCS1msVlvDrvEnRst, DeList);
clear DeList;

Ioc_OutputCS1msAwdPrnDrv = Simulink.Bus;
DeList={'Ioc_OutputCS1msAwdPrnDrv'};
Ioc_OutputCS1msAwdPrnDrv = CreateBus(Ioc_OutputCS1msAwdPrnDrv, DeList);
clear DeList;


Ach_OutputAkaKeepAliveWriteData = Simulink.Bus;
DeList={
    'KeepAliveDataFlg'
    'ACH_Tx1_PHOLD'
    'ACH_Tx1_KA'
    };
Ach_OutputAkaKeepAliveWriteData = CreateBus(Ach_OutputAkaKeepAliveWriteData, DeList);
clear DeList;

Ach_OutputIocDcMtrDutyData = Simulink.Bus;
DeList={
    'MtrDutyDataFlg'
    'ACH_Tx8_PUMP_DTY_PWM'
    };
Ach_OutputIocDcMtrDutyData = CreateBus(Ach_OutputIocDcMtrDutyData, DeList);
clear DeList;

Ach_OutputIocDcMtrFreqData = Simulink.Bus;
DeList={
    'MtrFreqDataFlg'
    'ACH_Tx8_PUMP_TCK_PWM'
    };
Ach_OutputIocDcMtrFreqData = CreateBus(Ach_OutputIocDcMtrFreqData, DeList);
clear DeList;

Ach_OutputIocVlvNo0Data = Simulink.Bus;
DeList={
    'VlvNo0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo0Data = CreateBus(Ach_OutputIocVlvNo0Data, DeList);
clear DeList;

Ach_OutputIocVlvNo1Data = Simulink.Bus;
DeList={
    'VlvNo1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo1Data = CreateBus(Ach_OutputIocVlvNo1Data, DeList);
clear DeList;

Ach_OutputIocVlvNo2Data = Simulink.Bus;
DeList={
    'VlvNo2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo2Data = CreateBus(Ach_OutputIocVlvNo2Data, DeList);
clear DeList;

Ach_OutputIocVlvNo3Data = Simulink.Bus;
DeList={
    'VlvNo3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNo3Data = CreateBus(Ach_OutputIocVlvNo3Data, DeList);
clear DeList;

Ach_OutputIocVlvTc0Data = Simulink.Bus;
DeList={
    'VlvTc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvTc0Data = CreateBus(Ach_OutputIocVlvTc0Data, DeList);
clear DeList;

Ach_OutputIocVlvTc1Data = Simulink.Bus;
DeList={
    'VlvTc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvTc1Data = CreateBus(Ach_OutputIocVlvTc1Data, DeList);
clear DeList;

Ach_OutputIocVlvEsv0Data = Simulink.Bus;
DeList={
    'VlvEsv0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvEsv0Data = CreateBus(Ach_OutputIocVlvEsv0Data, DeList);
clear DeList;

Ach_OutputIocVlvEsv1Data = Simulink.Bus;
DeList={
    'VlvEsv1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvEsv1Data = CreateBus(Ach_OutputIocVlvEsv1Data, DeList);
clear DeList;

Ach_OutputIocVlvNc0Data = Simulink.Bus;
DeList={
    'VlvNc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc0Data = CreateBus(Ach_OutputIocVlvNc0Data, DeList);
clear DeList;

Ach_OutputIocVlvNc1Data = Simulink.Bus;
DeList={
    'VlvNc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc1Data = CreateBus(Ach_OutputIocVlvNc1Data, DeList);
clear DeList;

Ach_OutputIocVlvNc2Data = Simulink.Bus;
DeList={
    'VlvNc2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc2Data = CreateBus(Ach_OutputIocVlvNc2Data, DeList);
clear DeList;

Ach_OutputIocVlvNc3Data = Simulink.Bus;
DeList={
    'VlvNc3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ach_OutputIocVlvNc3Data = CreateBus(Ach_OutputIocVlvNc3Data, DeList);
clear DeList;

Ach_OutputIocVlvRelayData = Simulink.Bus;
DeList={
    'VlvRelayDataFlg'
    'ACH_Tx1_FS_CMD'
    };
Ach_OutputIocVlvRelayData = CreateBus(Ach_OutputIocVlvRelayData, DeList);
clear DeList;

Ach_OutputIocAdcSelWriteData = Simulink.Bus;
DeList={
    'AdcSelDataFlg'
    'ACH_Tx13_ADC_SEL'
    };
Ach_OutputIocAdcSelWriteData = CreateBus(Ach_OutputIocAdcSelWriteData, DeList);
clear DeList;

Ach_OutputAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ach_OutputAcmAsicInitCompleteFlag'};
Ach_OutputAcmAsicInitCompleteFlag = CreateBus(Ach_OutputAcmAsicInitCompleteFlag, DeList);
clear DeList;


Mtr_Processing_DiagPwrMonInfoEsc = Simulink.Bus;
DeList={
    'VoltVBatt01Mon_Esc'
    'VoltVBatt02Mon_Esc'
    };
Mtr_Processing_DiagPwrMonInfoEsc = CreateBus(Mtr_Processing_DiagPwrMonInfoEsc, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessOutData = Simulink.Bus;
DeList={
    'MtrProIqFailsafe'
    'MtrProIdFailsafe'
    'MtrProUPhasePWM'
    'MtrProVPhasePWM'
    'MtrProWPhasePWM'
    };
Mtr_Processing_DiagMtrProcessOutData = CreateBus(Mtr_Processing_DiagMtrProcessOutData, DeList);
clear DeList;

Mtr_Processing_DiagMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    'MtrCaliResult'
    };
Mtr_Processing_DiagMtrProcessDataInf = CreateBus(Mtr_Processing_DiagMtrProcessDataInf, DeList);
clear DeList;


Arbitrator_MtrMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Arbitrator_MtrMotDqIRefMccInfo = CreateBus(Arbitrator_MtrMotDqIRefMccInfo, DeList);
clear DeList;

Arbitrator_MtrMtrProcessOutData = Simulink.Bus;
DeList={
    'MtrProIqFailsafe'
    'MtrProIdFailsafe'
    'MtrProUPhasePWM'
    'MtrProVPhasePWM'
    'MtrProWPhasePWM'
    };
Arbitrator_MtrMtrProcessOutData = CreateBus(Arbitrator_MtrMtrProcessOutData, DeList);
clear DeList;

Arbitrator_MtrMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    };
Arbitrator_MtrMtrProcessDataInf = CreateBus(Arbitrator_MtrMtrProcessDataInf, DeList);
clear DeList;

Arbitrator_MtrVdcLinkFild = Simulink.Bus;
DeList={'Arbitrator_MtrVdcLinkFild'};
Arbitrator_MtrVdcLinkFild = CreateBus(Arbitrator_MtrVdcLinkFild, DeList);
clear DeList;

Arbitrator_MtrMotCtrlMode = Simulink.Bus;
DeList={'Arbitrator_MtrMotCtrlMode'};
Arbitrator_MtrMotCtrlMode = CreateBus(Arbitrator_MtrMotCtrlMode, DeList);
clear DeList;

Arbitrator_MtrMotCtrlState = Simulink.Bus;
DeList={'Arbitrator_MtrMotCtrlState'};
Arbitrator_MtrMotCtrlState = CreateBus(Arbitrator_MtrMotCtrlState, DeList);
clear DeList;


