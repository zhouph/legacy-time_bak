# \file
#
# \brief Task_1ms
#
# This file contains the implementation of the SWC
# module Task_1ms.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Task_1ms_CORE_PATH     := $(MANDO_BSW_ROOT)\Task_1ms
Task_1ms_CAL_PATH      := $(Task_1ms_CORE_PATH)\CAL
Task_1ms_SRC_PATH      := $(Task_1ms_CORE_PATH)\SRC
Task_1ms_CFG_PATH      := $(Task_1ms_CORE_PATH)\CFG\$(Task_1ms_VARIANT)
Task_1ms_HDR_PATH      := $(Task_1ms_CORE_PATH)\HDR
Task_1ms_IFA_PATH      := $(Task_1ms_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Task_1ms_CMN_PATH      := $(Task_1ms_CORE_PATH)\ICE\CMN
endif

CC_INCLUDE_PATH    += $(Task_1ms_CAL_PATH)
CC_INCLUDE_PATH    += $(Task_1ms_SRC_PATH)
CC_INCLUDE_PATH    += $(Task_1ms_CFG_PATH)
CC_INCLUDE_PATH    += $(Task_1ms_HDR_PATH)
CC_INCLUDE_PATH    += $(Task_1ms_IFA_PATH)
CC_INCLUDE_PATH    += $(Task_1ms_CMN_PATH)

