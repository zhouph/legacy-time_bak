# \file
#
# \brief Task_1ms
#
# This file contains the implementation of the SWC
# module Task_1ms.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Task_1ms_src

Task_1ms_src_FILES        += $(Task_1ms_SRC_PATH)\Task_1ms.c
Task_1ms_src_FILES        += $(Task_1ms_IFA_PATH)\Task_1ms_Ifa.c
Task_1ms_src_FILES        += $(Task_1ms_CFG_PATH)\Task_1ms_Cfg.c
Task_1ms_src_FILES        += $(Task_1ms_CAL_PATH)\Task_1ms_Cal.c
