#define S_FUNCTION_NAME      SwtP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          23
#define WidthOutputPort         4

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "SwtP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    SwtP_MainEemFailData.Eem_Fail_SimP = input[0];
    SwtP_MainEemFailData.Eem_Fail_ParkBrake = input[1];
    SwtP_MainEemFailData.Eem_Fail_PedalPDT = input[2];
    SwtP_MainEemFailData.Eem_Fail_PedalPDF = input[3];
    SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit = input[4];
    SwtP_MainSwTrigPwrInfo.Vbatt01Mon = input[5];
    SwtP_MainPdf5msRawInfo.MoveAvrPdfSig = input[6];
    SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = input[7];
    SwtP_MainPdf5msRawInfo.MoveAvrPdtSig = input[8];
    SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = input[9];
    SwtP_MainSwtMonInfo.BlsSwtMon = input[10];
    SwtP_MainSwtMonInfo.BsSwtMon = input[11];
    SwtP_MainEemSuspectData.Eem_Suspect_BS = input[12];
    SwtP_MainEemSuspectData.Eem_Suspect_BLS = input[13];
    SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake = input[14];
    SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT = input[15];
    SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF = input[16];
    SwtP_MainEcuModeSts = input[17];
    SwtP_MainIgnOnOffSts = input[18];
    SwtP_MainIgnEdgeSts = input[19];
    SwtP_MainVBatt1Mon = input[20];
    SwtP_MainDiagClrSrs = input[21];
    SwtP_MainVehSpdFild = input[22];

    SwtP_Main();


    output[0] = SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
    output[1] = SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
    output[2] = SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
    output[3] = SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    SwtP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
