function bus=CreateBus(this,DeList)
this.Description = '';
this.DataScope = 'Auto';
this.Alignment = -1;
numDe = length(DeList);

for i=1:numDe
    saveVarsTmp(i) = Simulink.BusElement;
    saveVarsTmp(i).Name = DeList{i};
    saveVarsTmp(i).Complexity = 'real';
    saveVarsTmp(i).Dimensions = 1;
    saveVarsTmp(i).DataType = 'double';
    saveVarsTmp(i).Min = [];
    saveVarsTmp(i).Max = [];
    saveVarsTmp(i).DimensionsMode = 'Fixed';
    saveVarsTmp(i).SamplingMode = 'Sample based';
    saveVarsTmp(i).SampleTime = -1;
    saveVarsTmp(i).DocUnits = '';
    saveVarsTmp(i).Description = '';

    if i==numDe
        this.Elements = saveVarsTmp;
    end
end
bus = this;

end
