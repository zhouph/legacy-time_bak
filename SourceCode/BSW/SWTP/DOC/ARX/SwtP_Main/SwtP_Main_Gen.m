SwtP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SimP'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    };
SwtP_MainEemFailData = CreateBus(SwtP_MainEemFailData, DeList);
clear DeList;

SwtP_MainEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_BBS_AllControlInhibit'
    };
SwtP_MainEemCtrlInhibitData = CreateBus(SwtP_MainEemCtrlInhibitData, DeList);
clear DeList;

SwtP_MainSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Vbatt01Mon'
    };
SwtP_MainSwTrigPwrInfo = CreateBus(SwtP_MainSwTrigPwrInfo, DeList);
clear DeList;

SwtP_MainPdf5msRawInfo = Simulink.Bus;
DeList={
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
SwtP_MainPdf5msRawInfo = CreateBus(SwtP_MainPdf5msRawInfo, DeList);
clear DeList;

SwtP_MainSwtMonInfo = Simulink.Bus;
DeList={
    'BlsSwtMon'
    'BsSwtMon'
    };
SwtP_MainSwtMonInfo = CreateBus(SwtP_MainSwtMonInfo, DeList);
clear DeList;

SwtP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    };
SwtP_MainEemSuspectData = CreateBus(SwtP_MainEemSuspectData, DeList);
clear DeList;

SwtP_MainEcuModeSts = Simulink.Bus;
DeList={'SwtP_MainEcuModeSts'};
SwtP_MainEcuModeSts = CreateBus(SwtP_MainEcuModeSts, DeList);
clear DeList;

SwtP_MainIgnOnOffSts = Simulink.Bus;
DeList={'SwtP_MainIgnOnOffSts'};
SwtP_MainIgnOnOffSts = CreateBus(SwtP_MainIgnOnOffSts, DeList);
clear DeList;

SwtP_MainIgnEdgeSts = Simulink.Bus;
DeList={'SwtP_MainIgnEdgeSts'};
SwtP_MainIgnEdgeSts = CreateBus(SwtP_MainIgnEdgeSts, DeList);
clear DeList;

SwtP_MainVBatt1Mon = Simulink.Bus;
DeList={'SwtP_MainVBatt1Mon'};
SwtP_MainVBatt1Mon = CreateBus(SwtP_MainVBatt1Mon, DeList);
clear DeList;

SwtP_MainDiagClrSrs = Simulink.Bus;
DeList={'SwtP_MainDiagClrSrs'};
SwtP_MainDiagClrSrs = CreateBus(SwtP_MainDiagClrSrs, DeList);
clear DeList;

SwtP_MainVehSpdFild = Simulink.Bus;
DeList={'SwtP_MainVehSpdFild'};
SwtP_MainVehSpdFild = CreateBus(SwtP_MainVehSpdFild, DeList);
clear DeList;

SwtP_MainSwtPFaultInfo = Simulink.Bus;
DeList={
    'SWM_BLS_Sw_HighStick_Err'
    'SWM_BLS_Sw_LowStick_Err'
    'SWM_BS_Sw_HighStick_Err'
    'SWM_BS_Sw_LowStick_Err'
    };
SwtP_MainSwtPFaultInfo = CreateBus(SwtP_MainSwtPFaultInfo, DeList);
clear DeList;

