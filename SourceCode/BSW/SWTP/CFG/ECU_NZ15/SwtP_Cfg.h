/**
 * @defgroup SwtP_Cfg SwtP_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTP_CFG_H_
#define SWTP_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtP_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/* Switch type */
#define SWTP_NONE           (0)
#define SWTP_ANALOG_TYPE    (1)
#define SWTP_CAN_TYPE       (2)

/* Switch variant */
#define SWTP_HKMC          	(1)
#define SWTP_VOLVO_V40		(2)

/*Runable cycle time(ms)*/
#define SWTP_CYCLE_TIME		(10)

/*Switch State*/
#define SWTP_ON				(1)
#define SWTP_OFF			(0)

/*Ignition power*/
#define SWTP_7V0			(7000)
#define SWTP_17V0			(17000)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
	uint8 SwtP_bls_sw_type;
	uint8 SwtP_bls_sw_variant;
	
	uint8 SwtP_bs_sw_type;
	uint8 SwtP_bs_sw_variant;
} SwtPCfg;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SwtP_InitCfg(void);
extern SwtPCfg* SwtP_GetCfg(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTP_CFG_H_ */
/** @} */
