/**
 * @defgroup SwtP_Cfg SwtP_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtP_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWTP_START_SEC_CONST_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWTP_STOP_SEC_CONST_UNSPECIFIED
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_START_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTP_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
#define SWTP_START_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTP_STOP_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_START_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTP_STOP_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_START_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTP_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
#define SWTP_START_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTP_STOP_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_START_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTP_STOP_SEC_VAR_32BIT
#include "SwtP_MemMap.h"


static SwtPCfg SwtP_Cfg;


/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWTP_START_SEC_CODE
#include "SwtP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SwtP_InitCfg(void)
{
	/* TODO */
	SwtP_Cfg.SwtP_bls_sw_type		= SWTP_ANALOG_TYPE;
	SwtP_Cfg.SwtP_bls_sw_variant	= SWTP_HKMC;
	
	SwtP_Cfg.SwtP_bs_sw_type		= SWTP_ANALOG_TYPE;
	SwtP_Cfg.SwtP_bs_sw_variant		= SWTP_HKMC;
}
	
SwtPCfg* SwtP_GetCfg(void)
{
	return &SwtP_Cfg;
}
	

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWTP_STOP_SEC_CODE
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
