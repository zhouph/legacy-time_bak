# \file
#
# \brief SwtP
#
# This file contains the implementation of the SWC
# module SwtP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SwtP_CORE_PATH     := $(MANDO_BSW_ROOT)\SwtP
SwtP_CAL_PATH      := $(SwtP_CORE_PATH)\CAL\$(SwtP_VARIANT)
SwtP_SRC_PATH      := $(SwtP_CORE_PATH)\SRC
SwtP_CFG_PATH      := $(SwtP_CORE_PATH)\CFG\$(SwtP_VARIANT)
SwtP_HDR_PATH      := $(SwtP_CORE_PATH)\HDR
SwtP_IFA_PATH      := $(SwtP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SwtP_CMN_PATH      := $(SwtP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SwtP_UT_PATH		:= $(SwtP_CORE_PATH)\UT
	SwtP_UNITY_PATH	:= $(SwtP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SwtP_UT_PATH)
	CC_INCLUDE_PATH		+= $(SwtP_UNITY_PATH)
	SwtP_Main_PATH 	:= SwtP_UT_PATH\SwtP_Main
endif
CC_INCLUDE_PATH    += $(SwtP_CAL_PATH)
CC_INCLUDE_PATH    += $(SwtP_SRC_PATH)
CC_INCLUDE_PATH    += $(SwtP_CFG_PATH)
CC_INCLUDE_PATH    += $(SwtP_HDR_PATH)
CC_INCLUDE_PATH    += $(SwtP_IFA_PATH)
CC_INCLUDE_PATH    += $(SwtP_CMN_PATH)

