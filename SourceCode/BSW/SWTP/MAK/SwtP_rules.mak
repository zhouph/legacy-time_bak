# \file
#
# \brief SwtP
#
# This file contains the implementation of the SWC
# module SwtP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SwtP_src

SwtP_src_FILES        += $(SwtP_SRC_PATH)\SwtP_Main.c
SwtP_src_FILES        += $(SwtP_SRC_PATH)\SwtP_Process.c
SwtP_src_FILES        += $(SwtP_IFA_PATH)\SwtP_Main_Ifa.c
SwtP_src_FILES        += $(SwtP_CFG_PATH)\SwtP_Cfg.c
SwtP_src_FILES        += $(SwtP_CAL_PATH)\SwtP_Cal.c

ifeq ($(ICE_COMPILE),true)
SwtP_src_FILES        += $(SwtP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SwtP_src_FILES        += $(SwtP_UNITY_PATH)\unity.c
	SwtP_src_FILES        += $(SwtP_UNITY_PATH)\unity_fixture.c	
	SwtP_src_FILES        += $(SwtP_UT_PATH)\main.c
	SwtP_src_FILES        += $(SwtP_UT_PATH)\SwtP_Main\SwtP_Main_UtMain.c
endif