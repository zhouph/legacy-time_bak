/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Common.h"
#include "SwtP_Main.h"
#include "SwtP_Cfg.h"

#include "Pedal_SenSync.h"  /*To be updated*/
#include "Press_SenSync.h"  /*To be updated*/
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
//#define	TIME_SEC(x)                 (((x) * 1000) / SWTP_CYCLE_TIME)
#define TIME_1_SEC					100
#define TIME_120_SEC				12000


#define PEDAL_2MM					20
#define PEDAL_6MM					60
#define PEDAL_5MM					50
#define PEDAL_25MM					250

#define PRESS_5BAR					500


#define F64SPEED_20KPH          	1280 



/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

static SwtPCfg* pSwtP_Cfg;

static uint16 	PedalStroke_ChkBLS;

static uint16 	MaxPedalStroke1_10mm;
static uint16 	MinPedalStroke1_10mm;

static uint8 	PedalCopyDelayCnt;

static uint16 	BlsHighErrCnt;
static uint8 	BlsHighStickCnt;

static uint16 	BlsLowErrCnt;
static uint8 	BlsLowStickCnt;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void SwtP_GetPedalStrokInfo(void);
static void SwtP_BlsHighStickCheck(void);
static void SwtP_BlsLowStickCheck(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void SwtP_Process(void)
{
	SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err    = ERR_NONE;
	SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err     = ERR_NONE;

	pSwtP_Cfg = SwtP_GetCfg();

	if(SwtP_MainBus.SwtP_MainVBatt1Mon < SWTP_7V0 || SwtP_MainBus.SwtP_MainVBatt1Mon > SWTP_17V0)
	{
	    SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err    = ERR_INHIBIT;
	    SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err     = ERR_INHIBIT;
	}
	else
	{
		SwtP_GetPedalStrokInfo();
		SwtP_BlsHighStickCheck();
		SwtP_BlsLowStickCheck();
	}
}

void SwtP_Init(void)
{
	SwtP_InitCfg();


	PedalStroke_ChkBLS	= 0;	

	MinPedalStroke1_10mm	= 0;
	MaxPedalStroke1_10mm	= 0;

	PedalCopyDelayCnt	= 0;

	BlsHighErrCnt	= 0;
	BlsHighStickCnt	= 0;

	BlsLowErrCnt	= 0;
	BlsLowStickCnt	= 0;

	SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err    = ERR_NONE;
	SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err     = ERR_NONE;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void SwtP_GetPedalStrokInfo(void)
{
	if(PedalCopyDelayCnt >= TIME_1_SEC) /*1Sec*/
	{
		if((SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT == SWTP_OFF) && (SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_PedalPDT == ERR_PASSED))
		{
			PedalStroke_ChkBLS 	= Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;
		}
		else if((SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF == SWTP_OFF) && (SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_PedalPDF == ERR_PASSED))
		{
			PedalStroke_ChkBLS 	= Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
		}
		else
		{
            SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err    = ERR_INHIBIT;
            SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err     = ERR_INHIBIT;
		}

		MinPedalStroke1_10mm = 0;
        MaxPedalStroke1_10mm = 0;

		if(SwtP_MainBus.SwtP_MainSwtMonInfo.BlsSwtMon == SWTP_OFF)
		{
			if(PedalStroke_ChkBLS < MinPedalStroke1_10mm)
            {
                MinPedalStroke1_10mm = PedalStroke_ChkBLS;
            }
			
            if(PedalStroke_ChkBLS > MaxPedalStroke1_10mm)
            {
                MaxPedalStroke1_10mm= PedalStroke_ChkBLS;
            }
		}
		PedalCopyDelayCnt = 0;
	}
	else
	{
		PedalCopyDelayCnt++;
		
	}
}

/*BLS High stick fault is detected when pedal stroke is less than 6mm, pedal strok difference is less than 2mm, and brake lamp switch is high.*/
static void SwtP_BlsHighStickCheck(void)
{
	uint16 DiffPedalChkBlsHigh;

	SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = ERR_NONE;


	DiffPedalChkBlsHigh = PedalStroke_ChkBLS - MinPedalStroke1_10mm;
	if(pSwtP_Cfg->SwtP_bls_sw_type == SWTP_ANALOG_TYPE && pSwtP_Cfg->SwtP_bls_sw_variant == SWTP_HKMC)
	{
		
		if(SwtP_MainBus.SwtP_MainSwtMonInfo.BlsSwtMon == SWTP_ON)
		{
			if((PedalStroke_ChkBLS < PEDAL_6MM) && (DiffPedalChkBlsHigh < PEDAL_2MM) && (Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs < PRESS_5BAR))
				/*&& (SwtP_MainBus.SwtP_MainVehSpdFild > F64SPEED_20KPH*/
			{
				BlsHighErrCnt++;
			}
			else
			{
				BlsHighErrCnt = 0;
			}
		
			if(BlsHighErrCnt>=TIME_120_SEC)
			{
				BlsHighStickCnt++;
				BlsHighErrCnt = 0;
			}
		}
		else
		{
			BlsHighErrCnt = 0;
			BlsHighStickCnt = 0;
			SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = ERR_PASSED;
		}
		
		if(BlsHighStickCnt > 2)
		{
			BlsHighErrCnt = 0;
			BlsHighStickCnt = 0;
			SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = ERR_FAILED;
		}
	}
	else
	{
		BlsHighErrCnt = 0;
		BlsHighStickCnt = 0;
		SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = ERR_INHIBIT;
	}
}


/*BLS low stick fault is detected when pedal stroke is more than 25mm and,  pedal stroke difference is more than 5mm, but brake lamp swith is low.*/
static void SwtP_BlsLowStickCheck(void)
{
	uint16 DiffPedalChkBlsLow;
	

	SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = ERR_NONE;
	
	DiffPedalChkBlsLow = MaxPedalStroke1_10mm - MinPedalStroke1_10mm;
	if(pSwtP_Cfg->SwtP_bls_sw_type == SWTP_ANALOG_TYPE && pSwtP_Cfg->SwtP_bls_sw_variant == SWTP_HKMC)
	{
		if(SwtP_MainBus.SwtP_MainSwtMonInfo.BlsSwtMon == SWTP_OFF)
		{
			if((PedalStroke_ChkBLS > PEDAL_25MM) && (DiffPedalChkBlsLow > PEDAL_5MM)
				&&(Press_SenSyncBus.Press_SenSyncPressSenCalcInfo.PressP_SimPMoveAveEolOfs > PRESS_5BAR))
			{
				if(BlsLowErrCnt < TIME_1_SEC)
				{
					BlsLowErrCnt++;
				}
				else
				{
					BlsLowStickCnt++;
					BlsLowErrCnt = 0;
				}
		
				if(BlsLowStickCnt > 2)
				{
					BlsLowErrCnt = 0;
					BlsLowStickCnt = 0;
					SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = ERR_FAILED;
				}
			}
			else
			{
				BlsLowErrCnt = 0;
				BlsLowStickCnt = 0;
				SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = ERR_PASSED;
			}
		}
		else
		{
			BlsLowErrCnt = 0;
			BlsLowStickCnt = 0;
			SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = ERR_PASSED;
		}
		
	}
	else
	{
		BlsLowErrCnt = 0;
		BlsLowStickCnt = 0;
		SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = ERR_INHIBIT;
	}

}

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
