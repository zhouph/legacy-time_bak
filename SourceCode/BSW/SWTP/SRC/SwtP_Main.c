/**
 * @defgroup SwtP_Main SwtP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtP_Main.h"
#include "SwtP_Process.h"
#include "SwtP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWTP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWTP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SwtP_Main_HdrBusType SwtP_MainBus;

/* Version Info */
const SwcVersionInfo_t SwtP_MainVersionInfo = 
{   
    SWTP_MAIN_MODULE_ID,           /* SwtP_MainVersionInfo.ModuleId */
    SWTP_MAIN_MAJOR_VERSION,       /* SwtP_MainVersionInfo.MajorVer */
    SWTP_MAIN_MINOR_VERSION,       /* SwtP_MainVersionInfo.MinorVer */
    SWTP_MAIN_PATCH_VERSION,       /* SwtP_MainVersionInfo.PatchVer */
    SWTP_MAIN_BRANCH_VERSION       /* SwtP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t SwtP_MainEemFailData;
Eem_MainEemCtrlInhibitData_t SwtP_MainEemCtrlInhibitData;
AdcIf_Conv1msSwTrigPwrInfo_t SwtP_MainSwTrigPwrInfo;
Pedal_SenSyncPdf5msRawInfo_t SwtP_MainPdf5msRawInfo;
Ioc_InputCS1msSwtMonInfo_t SwtP_MainSwtMonInfo;
Eem_MainEemSuspectData_t SwtP_MainEemSuspectData;
Mom_HndlrEcuModeSts_t SwtP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t SwtP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t SwtP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t SwtP_MainVBatt1Mon;
Diag_HndlrDiagClr_t SwtP_MainDiagClrSrs;
Det_5msCtrlVehSpdFild_t SwtP_MainVehSpdFild;

/* Output Data Element */
SwtP_MainSwtPFaultInfo_t SwtP_MainSwtPFaultInfo;

uint32 SwtP_Main_Timer_Start;
uint32 SwtP_Main_Timer_Elapsed;

#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWTP_MAIN_START_SEC_CODE
#include "SwtP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SwtP_Main_Init(void)
{
    /* Initialize internal bus */
    SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_SimP = 0;
    SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_ParkBrake = 0;
    SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_PedalPDT = 0;
    SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_PedalPDF = 0;
    SwtP_MainBus.SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit = 0;
    SwtP_MainBus.SwtP_MainSwTrigPwrInfo.Vbatt01Mon = 0;
    SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSig = 0;
    SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = 0;
    SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSig = 0;
    SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = 0;
    SwtP_MainBus.SwtP_MainSwtMonInfo.BlsSwtMon = 0;
    SwtP_MainBus.SwtP_MainSwtMonInfo.BsSwtMon = 0;
    SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_BS = 0;
    SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_BLS = 0;
    SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake = 0;
    SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT = 0;
    SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF = 0;
    SwtP_MainBus.SwtP_MainEcuModeSts = 0;
    SwtP_MainBus.SwtP_MainIgnOnOffSts = 0;
    SwtP_MainBus.SwtP_MainIgnEdgeSts = 0;
    SwtP_MainBus.SwtP_MainVBatt1Mon = 0;
    SwtP_MainBus.SwtP_MainDiagClrSrs = 0;
    SwtP_MainBus.SwtP_MainVehSpdFild = 0;
    SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = 0;
    SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = 0;
    SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = 0;
    SwtP_MainBus.SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = 0;
 
	SwtP_Init();
}

void SwtP_Main(void)
{
    SwtP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_SimP(&SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_SimP);
    SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_ParkBrake(&SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_ParkBrake);
    SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDT(&SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_PedalPDT);
    SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDF(&SwtP_MainBus.SwtP_MainEemFailData.Eem_Fail_PedalPDF);

    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit(&SwtP_MainBus.SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit);

    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainSwTrigPwrInfo_Vbatt01Mon(&SwtP_MainBus.SwtP_MainSwTrigPwrInfo.Vbatt01Mon);

    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSig(&SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSig);
    SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(&SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset);
    SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSig(&SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSig);
    SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(&SwtP_MainBus.SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset);

    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainSwtMonInfo_BlsSwtMon(&SwtP_MainBus.SwtP_MainSwtMonInfo.BlsSwtMon);
    SwtP_Main_Read_SwtP_MainSwtMonInfo_BsSwtMon(&SwtP_MainBus.SwtP_MainSwtMonInfo.BsSwtMon);

    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_BS(&SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_BS);
    SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_BLS(&SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_BLS);
    SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_ParkBrake(&SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake);
    SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDT(&SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT);
    SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDF(&SwtP_MainBus.SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF);

    /* Decomposed structure interface */
    SwtP_Main_Read_SwtP_MainEcuModeSts(&SwtP_MainBus.SwtP_MainEcuModeSts);
    SwtP_Main_Read_SwtP_MainIgnOnOffSts(&SwtP_MainBus.SwtP_MainIgnOnOffSts);
    SwtP_Main_Read_SwtP_MainIgnEdgeSts(&SwtP_MainBus.SwtP_MainIgnEdgeSts);
    SwtP_Main_Read_SwtP_MainVBatt1Mon(&SwtP_MainBus.SwtP_MainVBatt1Mon);
    SwtP_Main_Read_SwtP_MainDiagClrSrs(&SwtP_MainBus.SwtP_MainDiagClrSrs);
    SwtP_Main_Read_SwtP_MainVehSpdFild(&SwtP_MainBus.SwtP_MainVehSpdFild);

    /* Process */
	if(SwtP_MainBus.SwtP_MainDiagClrSrs == 1)
	{
		SwtP_Main_Init();
	}
	SwtP_Process();

    /* Output */
    SwtP_Main_Write_SwtP_MainSwtPFaultInfo(&SwtP_MainBus.SwtP_MainSwtPFaultInfo);
    /*==============================================================================
    * Members of structure SwtP_MainSwtPFaultInfo 
     : SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err;
     : SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err;
     =============================================================================*/
    

    SwtP_Main_Timer_Elapsed = STM0_TIM0.U - SwtP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWTP_MAIN_STOP_SEC_CODE
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
