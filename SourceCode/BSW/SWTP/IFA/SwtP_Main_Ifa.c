/**
 * @defgroup SwtP_Main_Ifa SwtP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtP_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SWTP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SWTP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SWTP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SWTP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SwtP_MemMap.h"
#define SWTP_MAIN_START_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/** Variable Section (32BIT)**/


#define SWTP_MAIN_STOP_SEC_VAR_32BIT
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SWTP_MAIN_START_SEC_CODE
#include "SwtP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SWTP_MAIN_STOP_SEC_CODE
#include "SwtP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
