/**
 * @defgroup SwtP_Main_Ifa SwtP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTP_MAIN_IFA_H_
#define SWTP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SwtP_Main_Read_SwtP_MainEemFailData(data) do \
{ \
    *data = SwtP_MainEemFailData; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemCtrlInhibitData(data) do \
{ \
    *data = SwtP_MainEemCtrlInhibitData; \
}while(0);

#define SwtP_Main_Read_SwtP_MainSwTrigPwrInfo(data) do \
{ \
    *data = SwtP_MainSwTrigPwrInfo; \
}while(0);

#define SwtP_Main_Read_SwtP_MainPdf5msRawInfo(data) do \
{ \
    *data = SwtP_MainPdf5msRawInfo; \
}while(0);

#define SwtP_Main_Read_SwtP_MainSwtMonInfo(data) do \
{ \
    *data = SwtP_MainSwtMonInfo; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemSuspectData(data) do \
{ \
    *data = SwtP_MainEemSuspectData; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = SwtP_MainEemFailData.Eem_Fail_SimP; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    *data = SwtP_MainEemFailData.Eem_Fail_ParkBrake; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = SwtP_MainEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = SwtP_MainEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit(data) do \
{ \
    *data = SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit; \
}while(0);

#define SwtP_Main_Read_SwtP_MainSwTrigPwrInfo_Vbatt01Mon(data) do \
{ \
    *data = SwtP_MainSwTrigPwrInfo.Vbatt01Mon; \
}while(0);

#define SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSig(data) do \
{ \
    *data = SwtP_MainPdf5msRawInfo.MoveAvrPdfSig; \
}while(0);

#define SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset(data) do \
{ \
    *data = SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset; \
}while(0);

#define SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSig(data) do \
{ \
    *data = SwtP_MainPdf5msRawInfo.MoveAvrPdtSig; \
}while(0);

#define SwtP_Main_Read_SwtP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset(data) do \
{ \
    *data = SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset; \
}while(0);

#define SwtP_Main_Read_SwtP_MainSwtMonInfo_BlsSwtMon(data) do \
{ \
    *data = SwtP_MainSwtMonInfo.BlsSwtMon; \
}while(0);

#define SwtP_Main_Read_SwtP_MainSwtMonInfo_BsSwtMon(data) do \
{ \
    *data = SwtP_MainSwtMonInfo.BsSwtMon; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_BS(data) do \
{ \
    *data = SwtP_MainEemSuspectData.Eem_Suspect_BS; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_BLS(data) do \
{ \
    *data = SwtP_MainEemSuspectData.Eem_Suspect_BLS; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_ParkBrake(data) do \
{ \
    *data = SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDT(data) do \
{ \
    *data = SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDF(data) do \
{ \
    *data = SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF; \
}while(0);

#define SwtP_Main_Read_SwtP_MainEcuModeSts(data) do \
{ \
    *data = SwtP_MainEcuModeSts; \
}while(0);

#define SwtP_Main_Read_SwtP_MainIgnOnOffSts(data) do \
{ \
    *data = SwtP_MainIgnOnOffSts; \
}while(0);

#define SwtP_Main_Read_SwtP_MainIgnEdgeSts(data) do \
{ \
    *data = SwtP_MainIgnEdgeSts; \
}while(0);

#define SwtP_Main_Read_SwtP_MainVBatt1Mon(data) do \
{ \
    *data = SwtP_MainVBatt1Mon; \
}while(0);

#define SwtP_Main_Read_SwtP_MainDiagClrSrs(data) do \
{ \
    *data = SwtP_MainDiagClrSrs; \
}while(0);

#define SwtP_Main_Read_SwtP_MainVehSpdFild(data) do \
{ \
    *data = SwtP_MainVehSpdFild; \
}while(0);


/* Set Output DE MAcro Function */
#define SwtP_Main_Write_SwtP_MainSwtPFaultInfo(data) do \
{ \
    SwtP_MainSwtPFaultInfo = *data; \
}while(0);

#define SwtP_Main_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err(data) do \
{ \
    SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err = *data; \
}while(0);

#define SwtP_Main_Write_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err(data) do \
{ \
    SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err = *data; \
}while(0);

#define SwtP_Main_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err(data) do \
{ \
    SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err = *data; \
}while(0);

#define SwtP_Main_Write_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err(data) do \
{ \
    SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTP_MAIN_IFA_H_ */
/** @} */
