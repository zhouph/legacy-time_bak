/**
 * @defgroup SwtP_Types SwtP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTP_TYPES_H_
#define SWTP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t SwtP_MainEemFailData;
    Eem_MainEemCtrlInhibitData_t SwtP_MainEemCtrlInhibitData;
    AdcIf_Conv1msSwTrigPwrInfo_t SwtP_MainSwTrigPwrInfo;
    Pedal_SenSyncPdf5msRawInfo_t SwtP_MainPdf5msRawInfo;
    Ioc_InputCS1msSwtMonInfo_t SwtP_MainSwtMonInfo;
    Eem_MainEemSuspectData_t SwtP_MainEemSuspectData;
    Mom_HndlrEcuModeSts_t SwtP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SwtP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SwtP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SwtP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t SwtP_MainDiagClrSrs;
    Det_5msCtrlVehSpdFild_t SwtP_MainVehSpdFild;

/* Output Data Element */
    SwtP_MainSwtPFaultInfo_t SwtP_MainSwtPFaultInfo;
}SwtP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTP_TYPES_H_ */
/** @} */
