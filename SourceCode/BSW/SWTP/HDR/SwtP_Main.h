/**
 * @defgroup SwtP_Main SwtP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwtP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SWTP_MAIN_H_
#define SWTP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SwtP_Types.h"
#include "SwtP_Cfg.h"
#include "SwtP_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SWTP_MAIN_MODULE_ID      (0)
 #define SWTP_MAIN_MAJOR_VERSION  (2)
 #define SWTP_MAIN_MINOR_VERSION  (0)
 #define SWTP_MAIN_PATCH_VERSION  (0)
 #define SWTP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SwtP_Main_HdrBusType SwtP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SwtP_MainVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t SwtP_MainEemFailData;
extern Eem_MainEemCtrlInhibitData_t SwtP_MainEemCtrlInhibitData;
extern AdcIf_Conv1msSwTrigPwrInfo_t SwtP_MainSwTrigPwrInfo;
extern Pedal_SenSyncPdf5msRawInfo_t SwtP_MainPdf5msRawInfo;
extern Ioc_InputCS1msSwtMonInfo_t SwtP_MainSwtMonInfo;
extern Eem_MainEemSuspectData_t SwtP_MainEemSuspectData;
extern Mom_HndlrEcuModeSts_t SwtP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t SwtP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t SwtP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t SwtP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t SwtP_MainDiagClrSrs;
extern Det_5msCtrlVehSpdFild_t SwtP_MainVehSpdFild;

/* Output Data Element */
extern SwtP_MainSwtPFaultInfo_t SwtP_MainSwtPFaultInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SwtP_Main_Init(void);
extern void SwtP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SWTP_MAIN_H_ */
/** @} */
