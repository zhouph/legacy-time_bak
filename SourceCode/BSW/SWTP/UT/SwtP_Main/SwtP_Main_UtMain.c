#include "unity.h"
#include "unity_fixture.h"
#include "SwtP_Main.h"
#include "SwtP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_SwtP_MainEemFailData_Eem_Fail_SimP[MAX_STEP] = SWTP_MAINEEMFAILDATA_EEM_FAIL_SIMP;
const Saluint8 UtInput_SwtP_MainEemFailData_Eem_Fail_ParkBrake[MAX_STEP] = SWTP_MAINEEMFAILDATA_EEM_FAIL_PARKBRAKE;
const Saluint8 UtInput_SwtP_MainEemFailData_Eem_Fail_PedalPDT[MAX_STEP] = SWTP_MAINEEMFAILDATA_EEM_FAIL_PEDALPDT;
const Saluint8 UtInput_SwtP_MainEemFailData_Eem_Fail_PedalPDF[MAX_STEP] = SWTP_MAINEEMFAILDATA_EEM_FAIL_PEDALPDF;
const Saluint8 UtInput_SwtP_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit[MAX_STEP] = SWTP_MAINEEMCTRLINHIBITDATA_EEM_BBS_ALLCONTROLINHIBIT;
const Haluint16 UtInput_SwtP_MainSwTrigPwrInfo_Vbatt01Mon[MAX_STEP] = SWTP_MAINSWTRIGPWRINFO_VBATT01MON;
const Salsint16 UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdfSig[MAX_STEP] = SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIG;
const Salsint16 UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset[MAX_STEP] = SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIGEOLOFFSET;
const Salsint16 UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdtSig[MAX_STEP] = SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIG;
const Salsint16 UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset[MAX_STEP] = SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIGEOLOFFSET;
const Haluint8 UtInput_SwtP_MainSwtMonInfo_BlsSwtMon[MAX_STEP] = SWTP_MAINSWTMONINFO_BLSSWTMON;
const Haluint8 UtInput_SwtP_MainSwtMonInfo_BsSwtMon[MAX_STEP] = SWTP_MAINSWTMONINFO_BSSWTMON;
const Saluint8 UtInput_SwtP_MainEemSuspectData_Eem_Suspect_BS[MAX_STEP] = SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_BS;
const Saluint8 UtInput_SwtP_MainEemSuspectData_Eem_Suspect_BLS[MAX_STEP] = SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_BLS;
const Saluint8 UtInput_SwtP_MainEemSuspectData_Eem_Suspect_ParkBrake[MAX_STEP] = SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_PARKBRAKE;
const Saluint8 UtInput_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDT[MAX_STEP] = SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDT;
const Saluint8 UtInput_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDF[MAX_STEP] = SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDF;
const Mom_HndlrEcuModeSts_t UtInput_SwtP_MainEcuModeSts[MAX_STEP] = SWTP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_SwtP_MainIgnOnOffSts[MAX_STEP] = SWTP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_SwtP_MainIgnEdgeSts[MAX_STEP] = SWTP_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_SwtP_MainVBatt1Mon[MAX_STEP] = SWTP_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_SwtP_MainDiagClrSrs[MAX_STEP] = SWTP_MAINDIAGCLRSRS;
const Det_5msCtrlVehSpdFild_t UtInput_SwtP_MainVehSpdFild[MAX_STEP] = SWTP_MAINVEHSPDFILD;

const Saluint8 UtExpected_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err[MAX_STEP] = SWTP_MAINSWTPFAULTINFO_SWM_BLS_SW_HIGHSTICK_ERR;
const Saluint8 UtExpected_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err[MAX_STEP] = SWTP_MAINSWTPFAULTINFO_SWM_BLS_SW_LOWSTICK_ERR;
const Saluint8 UtExpected_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err[MAX_STEP] = SWTP_MAINSWTPFAULTINFO_SWM_BS_SW_HIGHSTICK_ERR;
const Saluint8 UtExpected_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err[MAX_STEP] = SWTP_MAINSWTPFAULTINFO_SWM_BS_SW_LOWSTICK_ERR;



TEST_GROUP(SwtP_Main);
TEST_SETUP(SwtP_Main)
{
    SwtP_Main_Init();
}

TEST_TEAR_DOWN(SwtP_Main)
{   /* Postcondition */

}

TEST(SwtP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SwtP_MainEemFailData.Eem_Fail_SimP = UtInput_SwtP_MainEemFailData_Eem_Fail_SimP[i];
        SwtP_MainEemFailData.Eem_Fail_ParkBrake = UtInput_SwtP_MainEemFailData_Eem_Fail_ParkBrake[i];
        SwtP_MainEemFailData.Eem_Fail_PedalPDT = UtInput_SwtP_MainEemFailData_Eem_Fail_PedalPDT[i];
        SwtP_MainEemFailData.Eem_Fail_PedalPDF = UtInput_SwtP_MainEemFailData_Eem_Fail_PedalPDF[i];
        SwtP_MainEemCtrlInhibitData.Eem_BBS_AllControlInhibit = UtInput_SwtP_MainEemCtrlInhibitData_Eem_BBS_AllControlInhibit[i];
        SwtP_MainSwTrigPwrInfo.Vbatt01Mon = UtInput_SwtP_MainSwTrigPwrInfo_Vbatt01Mon[i];
        SwtP_MainPdf5msRawInfo.MoveAvrPdfSig = UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdfSig[i];
        SwtP_MainPdf5msRawInfo.MoveAvrPdfSigEolOffset = UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdfSigEolOffset[i];
        SwtP_MainPdf5msRawInfo.MoveAvrPdtSig = UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdtSig[i];
        SwtP_MainPdf5msRawInfo.MoveAvrPdtSigEolOffset = UtInput_SwtP_MainPdf5msRawInfo_MoveAvrPdtSigEolOffset[i];
        SwtP_MainSwtMonInfo.BlsSwtMon = UtInput_SwtP_MainSwtMonInfo_BlsSwtMon[i];
        SwtP_MainSwtMonInfo.BsSwtMon = UtInput_SwtP_MainSwtMonInfo_BsSwtMon[i];
        SwtP_MainEemSuspectData.Eem_Suspect_BS = UtInput_SwtP_MainEemSuspectData_Eem_Suspect_BS[i];
        SwtP_MainEemSuspectData.Eem_Suspect_BLS = UtInput_SwtP_MainEemSuspectData_Eem_Suspect_BLS[i];
        SwtP_MainEemSuspectData.Eem_Suspect_ParkBrake = UtInput_SwtP_MainEemSuspectData_Eem_Suspect_ParkBrake[i];
        SwtP_MainEemSuspectData.Eem_Suspect_PedalPDT = UtInput_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDT[i];
        SwtP_MainEemSuspectData.Eem_Suspect_PedalPDF = UtInput_SwtP_MainEemSuspectData_Eem_Suspect_PedalPDF[i];
        SwtP_MainEcuModeSts = UtInput_SwtP_MainEcuModeSts[i];
        SwtP_MainIgnOnOffSts = UtInput_SwtP_MainIgnOnOffSts[i];
        SwtP_MainIgnEdgeSts = UtInput_SwtP_MainIgnEdgeSts[i];
        SwtP_MainVBatt1Mon = UtInput_SwtP_MainVBatt1Mon[i];
        SwtP_MainDiagClrSrs = UtInput_SwtP_MainDiagClrSrs[i];
        SwtP_MainVehSpdFild = UtInput_SwtP_MainVehSpdFild[i];

        SwtP_Main();

        TEST_ASSERT_EQUAL(SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_HighStick_Err, UtExpected_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_HighStick_Err[i]);
        TEST_ASSERT_EQUAL(SwtP_MainSwtPFaultInfo.SWM_BLS_Sw_LowStick_Err, UtExpected_SwtP_MainSwtPFaultInfo_SWM_BLS_Sw_LowStick_Err[i]);
        TEST_ASSERT_EQUAL(SwtP_MainSwtPFaultInfo.SWM_BS_Sw_HighStick_Err, UtExpected_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_HighStick_Err[i]);
        TEST_ASSERT_EQUAL(SwtP_MainSwtPFaultInfo.SWM_BS_Sw_LowStick_Err, UtExpected_SwtP_MainSwtPFaultInfo_SWM_BS_Sw_LowStick_Err[i]);
    }
}

TEST_GROUP_RUNNER(SwtP_Main)
{
    RUN_TEST_CASE(SwtP_Main, All);
}
