#define UT_MAX_STEP 5

#define SWTP_MAINEEMFAILDATA_EEM_FAIL_SIMP {0,0,0,0,0}
#define SWTP_MAINEEMFAILDATA_EEM_FAIL_PARKBRAKE {0,0,0,0,0}
#define SWTP_MAINEEMFAILDATA_EEM_FAIL_PEDALPDT {0,0,0,0,0}
#define SWTP_MAINEEMFAILDATA_EEM_FAIL_PEDALPDF {0,0,0,0,0}
#define SWTP_MAINEEMCTRLINHIBITDATA_EEM_BBS_ALLCONTROLINHIBIT {0,0,0,0,0}
#define SWTP_MAINSWTRIGPWRINFO_VBATT01MON {0,0,0,0,0}
#define SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIG {0,0,0,0,0}
#define SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDFSIGEOLOFFSET {0,0,0,0,0}
#define SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIG {0,0,0,0,0}
#define SWTP_MAINPDF5MSRAWINFO_MOVEAVRPDTSIGEOLOFFSET {0,0,0,0,0}
#define SWTP_MAINSWTMONINFO_BLSSWTMON {0,0,0,0,0}
#define SWTP_MAINSWTMONINFO_BSSWTMON {0,0,0,0,0}
#define SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_BS {0,0,0,0,0}
#define SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_BLS {0,0,0,0,0}
#define SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_PARKBRAKE {0,0,0,0,0}
#define SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDT {0,0,0,0,0}
#define SWTP_MAINEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDF {0,0,0,0,0}
#define SWTP_MAINECUMODESTS  {0,0,0,0,0}
#define SWTP_MAINIGNONOFFSTS  {0,0,0,0,0}
#define SWTP_MAINIGNEDGESTS  {0,0,0,0,0}
#define SWTP_MAINVBATT1MON  {0,0,0,0,0}
#define SWTP_MAINDIAGCLRSRS  {0,0,0,0,0}
#define SWTP_MAINVEHSPDFILD  {0,0,0,0,0}

#define SWTP_MAINSWTPFAULTINFO_SWM_BLS_SW_HIGHSTICK_ERR   {0,0,0,0,0}
#define SWTP_MAINSWTPFAULTINFO_SWM_BLS_SW_LOWSTICK_ERR   {0,0,0,0,0}
#define SWTP_MAINSWTPFAULTINFO_SWM_BS_SW_HIGHSTICK_ERR   {0,0,0,0,0}
#define SWTP_MAINSWTPFAULTINFO_SWM_BS_SW_LOWSTICK_ERR   {0,0,0,0,0}
