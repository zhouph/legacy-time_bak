/**
 * @defgroup SenPwrM_Main_Ifa SenPwrM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SenPwrM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SENPWRM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_CODE
#include "SenPwrM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SENPWRM_MAIN_STOP_SEC_CODE
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
