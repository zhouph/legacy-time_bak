/**
 * @defgroup SenPwrM_Main_Ifa SenPwrM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENPWRM_MAIN_IFA_H_
#define SENPWRM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SenPwrM_Main_Read_SenPwrM_MainEemFailData(data) do \
{ \
    *data = SenPwrM_MainEemFailData; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainEcuModeSts(data) do \
{ \
    *data = SenPwrM_MainEcuModeSts; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainIgnOnOffSts(data) do \
{ \
    *data = SenPwrM_MainIgnOnOffSts; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainIgnEdgeSts(data) do \
{ \
    *data = SenPwrM_MainIgnEdgeSts; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainVBatt1Mon(data) do \
{ \
    *data = SenPwrM_MainVBatt1Mon; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainDiagClrSrs(data) do \
{ \
    *data = SenPwrM_MainDiagClrSrs; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainArbVlvDriveState(data) do \
{ \
    *data = SenPwrM_MainArbVlvDriveState; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainExt5vMon(data) do \
{ \
    *data = SenPwrM_MainExt5vMon; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainCspMon(data) do \
{ \
    *data = SenPwrM_MainCspMon; \
}while(0);

#define SenPwrM_Main_Read_SenPwrM_MainMtrArbDriveState(data) do \
{ \
    *data = SenPwrM_MainMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = *data; \
}while(0);

#define SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err(data) do \
{ \
    SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENPWRM_MAIN_IFA_H_ */
/** @} */
