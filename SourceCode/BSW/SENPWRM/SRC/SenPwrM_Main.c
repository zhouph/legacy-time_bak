/**
 * @defgroup SenPwrM_Main SenPwrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SenPwrM_Main.h"
#include "SenPwrM_Main_Ifa.h"
#include "IfxStm_reg.h"
#include "SenPwrM_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SENPWRM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SenPwrM_Main_HdrBusType SenPwrM_MainBus;

/* Version Info */
const SwcVersionInfo_t SenPwrM_MainVersionInfo = 
{   
    SENPWRM_MAIN_MODULE_ID,           /* SenPwrM_MainVersionInfo.ModuleId */
    SENPWRM_MAIN_MAJOR_VERSION,       /* SenPwrM_MainVersionInfo.MajorVer */
    SENPWRM_MAIN_MINOR_VERSION,       /* SenPwrM_MainVersionInfo.MinorVer */
    SENPWRM_MAIN_PATCH_VERSION,       /* SenPwrM_MainVersionInfo.PatchVer */
    SENPWRM_MAIN_BRANCH_VERSION       /* SenPwrM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t SenPwrM_MainEemFailData;
Mom_HndlrEcuModeSts_t SenPwrM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t SenPwrM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t SenPwrM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t SenPwrM_MainVBatt1Mon;
Diag_HndlrDiagClr_t SenPwrM_MainDiagClrSrs;
Arbitrator_VlvArbVlvDriveState_t SenPwrM_MainArbVlvDriveState;
Ioc_InputSR1msExt5vMon_t SenPwrM_MainExt5vMon;
Ioc_InputSR1msCspMon_t SenPwrM_MainCspMon;
Arbitrator_MtrMtrArbDriveState_t SenPwrM_MainMtrArbDriveState;

/* Output Data Element */
SenPwrM_MainSenPwrMonitor_t SenPwrM_MainSenPwrMonitorData;

uint32 SenPwrM_Main_Timer_Start;
uint32 SenPwrM_Main_Timer_Elapsed;

#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SenPwrM_MemMap.h"
#define SENPWRM_MAIN_START_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SENPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SENPWRM_MAIN_START_SEC_CODE
#include "SenPwrM_MemMap.h"

#include "TimE.h"
TimE_Return_t TimE_ReturnSenPwrM10ms;
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SenPwrM_Main_Init(void)
{
    /* Initialize internal bus */
    SenPwrM_MainBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V = 0;
    SenPwrM_MainBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V = 0;
    SenPwrM_MainBus.SenPwrM_MainEcuModeSts = 0;
    SenPwrM_MainBus.SenPwrM_MainIgnOnOffSts = 0;
    SenPwrM_MainBus.SenPwrM_MainIgnEdgeSts = 0;
    SenPwrM_MainBus.SenPwrM_MainVBatt1Mon = 0;
    SenPwrM_MainBus.SenPwrM_MainDiagClrSrs = 0;
    SenPwrM_MainBus.SenPwrM_MainArbVlvDriveState = 0;
    SenPwrM_MainBus.SenPwrM_MainExt5vMon = 0;
    SenPwrM_MainBus.SenPwrM_MainCspMon = 0;
    SenPwrM_MainBus.SenPwrM_MainMtrArbDriveState = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = 0;
    SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = 0;
	SenPwrM_Process_Init();
}

void SenPwrM_Main(void)
{
    SenPwrM_Main_Timer_Start = STM0_TIM0.U;
	// LEJ TimE_ReturnSenPwrM10ms = SetAlivesupervisionCheckpoint(TIME_AS_CHECKPOINT_FOR_10MS);

    /* Input */
    /* Decomposed structure interface */
    SenPwrM_Main_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_12V(&SenPwrM_MainBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V);
    SenPwrM_Main_Read_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_5V(&SenPwrM_MainBus.SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V);

    SenPwrM_Main_Read_SenPwrM_MainEcuModeSts(&SenPwrM_MainBus.SenPwrM_MainEcuModeSts);
    SenPwrM_Main_Read_SenPwrM_MainIgnOnOffSts(&SenPwrM_MainBus.SenPwrM_MainIgnOnOffSts);
    SenPwrM_Main_Read_SenPwrM_MainIgnEdgeSts(&SenPwrM_MainBus.SenPwrM_MainIgnEdgeSts);
    SenPwrM_Main_Read_SenPwrM_MainVBatt1Mon(&SenPwrM_MainBus.SenPwrM_MainVBatt1Mon);
    SenPwrM_Main_Read_SenPwrM_MainDiagClrSrs(&SenPwrM_MainBus.SenPwrM_MainDiagClrSrs);
    SenPwrM_Main_Read_SenPwrM_MainArbVlvDriveState(&SenPwrM_MainBus.SenPwrM_MainArbVlvDriveState);
    SenPwrM_Main_Read_SenPwrM_MainExt5vMon(&SenPwrM_MainBus.SenPwrM_MainExt5vMon);
    SenPwrM_Main_Read_SenPwrM_MainCspMon(&SenPwrM_MainBus.SenPwrM_MainCspMon);
    SenPwrM_Main_Read_SenPwrM_MainMtrArbDriveState(&SenPwrM_MainBus.SenPwrM_MainMtrArbDriveState);

    /* Process */
	SenPwrM_Process(&SenPwrM_MainBus);
    /* Output */
    SenPwrM_Main_Write_SenPwrM_MainSenPwrMonitorData(&SenPwrM_MainBus.SenPwrM_MainSenPwrMonitorData);
    /*==============================================================================
    * Members of structure SenPwrM_MainSenPwrMonitorData 
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err;
     : SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err;
     =============================================================================*/
    

    SenPwrM_Main_Timer_Elapsed = STM0_TIM0.U - SenPwrM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SENPWRM_MAIN_STOP_SEC_CODE
#include "SenPwrM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
