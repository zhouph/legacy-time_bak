/**
 * @defgroup SenPwrM_Process SenPwrM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SenPwrM_Process.h"
#include "SenPwrM_Main.h"
#include "common.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
#define HAL_VDD_DIODE_DROP_VOLTAGE      (U16_CALCVOLT_0V4)

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
SenPwrMCtrlCfg s8SenPwrMCtrlCfg;


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
uint8 u8Eem_Fail_id_SenPwr_12V = 0;/*Temp Global variable*/
uint8 u8Eem_Fail_id_SenPwr_5V = 0;/*Temp Global variable*/

uint8 u8SenPwrM_12V_ReOnReqTime = 100; /* Init Time 500ms */
static uint16 u16SenPwrM_5V_OnCnt;
static uint16 u16SenPwrM_12V_OnCnt;

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void SenPwrCtrl_5VSenPwrCheck(SenPwrM_Main_HdrBusType *pSenPwrCtrl);
static void SenPwrCtrl_12VSenPwrCheck(SenPwrM_Main_HdrBusType *pSenPwrCtrl);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
 
 void SenPwrM_Process_Init(void)
 {
 	s8SenPwrMCtrlCfg.SenPwrM_5V_support = 0;
	s8SenPwrMCtrlCfg.SenPwrM_12V_support = 1;
 }
 
 void SenPwrM_Process(SenPwrM_Main_HdrBusType *pSenPwrMonitor)
 {
 	SenPwrCtrl_5VSenPwrCheck(pSenPwrMonitor);
	SenPwrCtrl_12VSenPwrCheck(pSenPwrMonitor);
 }

 static void SenPwrCtrl_5VSenPwrCheck(SenPwrM_Main_HdrBusType *pSenPwrCtrl)
 {
	 uint8	Prev_5V_DriveReq = (uint8)pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq;
	 uint16 SenPwr_5V_StableTime = 0;
 
	 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = ERR_NONE;
 
	 if(SENPWRM_CTRL_CYCLETIME > 0)
	 {
		 SenPwr_5V_StableTime = (uint16)(SEPC_5V_PWR_STABLE_TIME / SENPWRM_CTRL_CYCLETIME);
	 }
 
	 if((pSenPwrCtrl->SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V== TRUE) || (s8SenPwrMCtrlCfg.SenPwrM_5V_support == FALSE))
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = 0;
	 }
	 #if 0 /* define Power Mode !! */
	 else if(pInput->SEPC_PwrMode == SENPWRM_PowerMode) /* If Hall Sensor is equipped, re-consider this */
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq = 0;
	 }
	 #endif
	 else
	 {
		 ;
	 }
 
	 if(s8SenPwrMCtrlCfg.SenPwrM_5V_support == TRUE)
	 {
		 if(pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq == 0)
		 {
			 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable= 0;
		 }
		 
		 if(Prev_5V_DriveReq == 1)
		 {
			 if((pSenPwrCtrl->SenPwrM_MainExt5vMon < U16_CALCVOLT_4V75) || (pSenPwrCtrl->SenPwrM_MainExt5vMon > U16_CALCVOLT_5V25))
			 {
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = ERR_PREFAILED;
 
				 u16SenPwrM_5V_OnCnt = 0;
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
			 }
			 else
			 {
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = ERR_PREPASSED;
 
				 if(u16SenPwrM_5V_OnCnt < SenPwr_5V_StableTime)
				 {
					 u16SenPwrM_5V_OnCnt++;
				 }
				 else
				 {
					 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 1;
				 }
			 }
		 }
	 }
	 else
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable = 1;
	 }
 
	 if(s8SenPwrMCtrlCfg.SenPwrM_5V_support == 0)
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err = ERR_INHIBIT;
	 }
 }

 static void SenPwrCtrl_12VSenPwrCheck(SenPwrM_Main_HdrBusType *pSenPwrCtrl)
 {
	 static uint8 SenPwr_12V_OverCnt = 0;
	 uint16 SenPwr_12V_OverTime = 0;
	 
	 uint8	Prev_12V_DriveReq = (uint8) pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req;
	 uint16 SenPwr_12V_StableTime = 0;
	 uint16 IgnBeforeDiode_Volt = 0;
	 uint16 Sen12VLowThreshold	= 0;
	 uint16 Sen12VHighThreshold = 0;
 
	 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = ERR_NONE;
	 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = ERR_NONE; 
 
	 if(SENPWRM_CTRL_CYCLETIME > 0)
	 {
		 SenPwr_12V_StableTime = (uint16)(SEPC_12V_PWR_STABLE_TIME / SENPWRM_CTRL_CYCLETIME);
		 SenPwr_12V_OverTime   = (uint16)(SEPC_12V_PWR_OVER_TIME / SENPWRM_CTRL_CYCLETIME);
	 }		
	 
	 if(pSenPwrCtrl->SenPwrM_MainVBatt1Mon > (U16_CALCVOLT_1V0 + HAL_VDD_DIODE_DROP_VOLTAGE))
	 {
		 IgnBeforeDiode_Volt = pSenPwrCtrl->SenPwrM_MainVBatt1Mon - HAL_VDD_DIODE_DROP_VOLTAGE;
		 Sen12VLowThreshold  = (IgnBeforeDiode_Volt - U16_CALCVOLT_1V0);
		 Sen12VHighThreshold = (IgnBeforeDiode_Volt + U16_CALCVOLT_1V0);   
	 }
 
	 if((pSenPwrCtrl->SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V == TRUE) || (s8SenPwrMCtrlCfg.SenPwrM_12V_support == FALSE))
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = 0;
	 }
	 #if 0 /* define Power Mode !! */
	 else if(pInput->SEPC_PwrMode == Hal_PwrM_OFF)
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_DriveReq = 0;
	 }
	 #endif
	 else if((pSenPwrCtrl->SenPwrM_MainVBatt1Mon < U16_CALCVOLT_6V5) || (pSenPwrCtrl->SenPwrM_MainVBatt1Mon > U16_CALCVOLT_18V5))
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = 0;
	 }
	 else if((pSenPwrCtrl->SenPwrM_MainVBatt1Mon > U16_CALCVOLT_7V0) && (pSenPwrCtrl->SenPwrM_MainVBatt1Mon < U16_CALCVOLT_18V0))
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req = 1;
	 }
	 else
	 {
		 ;
	 }
 
	 if(s8SenPwrMCtrlCfg.SenPwrM_12V_support == TRUE)
	 {
		 if(pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req == 0)
		 {
			 u16SenPwrM_12V_OnCnt = 0;
			 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
			 SenPwr_12V_OverCnt = 0;
		 }
 
		 if(Prev_12V_DriveReq == 1)
		 {
			 if((pSenPwrCtrl->SenPwrM_MainCspMon < Sen12VLowThreshold) || (pSenPwrCtrl->SenPwrM_MainCspMon > Sen12VHighThreshold))
			 {
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = ERR_PREFAILED;
 
				 u16SenPwrM_12V_OnCnt = 0;
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
				 SenPwr_12V_OverCnt = 0;
			 }
 
			 if((pSenPwrCtrl->SenPwrM_MainCspMon > U16_CALCVOLT_8V2) && (pSenPwrCtrl->SenPwrM_MainCspMon < U16_CALCVOLT_16V5))
			 {
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = ERR_PREPASSED;
 
				 if(u16SenPwrM_12V_OnCnt < SenPwr_12V_StableTime)
				 {
					 u16SenPwrM_12V_OnCnt++;
				 }
				 else
				 {
					 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 1;
				 }
			 }
			 else if((pSenPwrCtrl->SenPwrM_MainCspMon < U16_CALCVOLT_8V0) || (pSenPwrCtrl->SenPwrM_MainCspMon < U16_CALCVOLT_16V7))
			 {
				 u16SenPwrM_12V_OnCnt = 0;
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
				 SenPwr_12V_OverCnt = 0;
			 }
			 else
			 {
				 ;
			 }
		 }
		else
	 	{
			/* Short State To Do */
			if(u8SenPwrM_12V_ReOnReqTime > 0)
			{
			   if(u8SenPwrM_12V_ReOnReqTime < 10)
			   {
				   if(pSenPwrCtrl->SenPwrM_MainCspMon < U16_CALCVOLT_1V0)   
				   {
					   u8SenPwrM_12V_ReOnReqTime--;
					   pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = ERR_PREPASSED; 		   
				   }
				   else
				   {
					   pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = ERR_PREFAILED; 		   
				   }  
			   }
			   else
			   {
				   u8SenPwrM_12V_ReOnReqTime--;
			   }

			}
	 	}
	 }
	 else
	 {
		 if((pSenPwrCtrl->SenPwrM_MainVBatt1Mon > U16_CALCVOLT_8V2) && (pSenPwrCtrl->SenPwrM_MainVBatt1Mon < U16_CALCVOLT_16V5))
		 {
			 if(u16SenPwrM_12V_OnCnt < SenPwr_12V_StableTime)
			 {
				 u16SenPwrM_12V_OnCnt++;
			 }
			 else
			 {
				 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 1;
			 }
		 }
		 else if((pSenPwrCtrl->SenPwrM_MainVBatt1Mon < U16_CALCVOLT_8V0) || (pSenPwrCtrl->SenPwrM_MainVBatt1Mon > U16_CALCVOLT_17V0)) 
		 {
			 if(pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable == 0)
			 {
				 u16SenPwrM_12V_OnCnt = 0;
				 SenPwr_12V_OverCnt = 0;
			 }
			 else
			 {
				 if(pSenPwrCtrl->SenPwrM_MainVBatt1Mon > U16_CALCVOLT_17V0)
				 {
					 if(SenPwr_12V_OverCnt >= SenPwr_12V_OverTime)
					 {
						 u16SenPwrM_12V_OnCnt = 0;
						 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
					 }
					 else
					 {
						 SenPwr_12V_OverCnt++;
					 }
				 }
				 else
				 {
					 u16SenPwrM_12V_OnCnt = 0;
					 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
					 SenPwr_12V_OverCnt = 0;
				 }
			 }
		 }
	 }
 
	 if(s8SenPwrMCtrlCfg.SenPwrM_12V_support == 0)
	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = ERR_INHIBIT;
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = ERR_INHIBIT;
	 }
	 /* VDD Fail 시 CSP 오검출 방지를 위하여 저/과 전압시 고장 검출 금지.  To Do*/
	 if(((pSenPwrCtrl->SenPwrM_MainIgnOnOffSts == 0/*CE_OFF_STATE*/ )&&((pSenPwrCtrl->SenPwrM_MainArbVlvDriveState == 1)||(pSenPwrCtrl->SenPwrM_MainMtrArbDriveState == 1)))||(pSenPwrCtrl->SenPwrM_MainVBatt1Mon<U16_CALCVOLT_8V4)||(pSenPwrCtrl->SenPwrM_MainVBatt1Mon>U16_CALCVOLT_17V0))
 	 {
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err = ERR_INHIBIT;
		 pSenPwrCtrl->SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err = ERR_INHIBIT;
		 u16SenPwrM_12V_OnCnt = 0;
		 u8SenPwrM_12V_ReOnReqTime = 0;
 	 }
 }
 
