SenPwrM_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    };
SenPwrM_MainEemFailData = CreateBus(SenPwrM_MainEemFailData, DeList);
clear DeList;

SenPwrM_MainEcuModeSts = Simulink.Bus;
DeList={'SenPwrM_MainEcuModeSts'};
SenPwrM_MainEcuModeSts = CreateBus(SenPwrM_MainEcuModeSts, DeList);
clear DeList;

SenPwrM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SenPwrM_MainIgnOnOffSts'};
SenPwrM_MainIgnOnOffSts = CreateBus(SenPwrM_MainIgnOnOffSts, DeList);
clear DeList;

SenPwrM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SenPwrM_MainIgnEdgeSts'};
SenPwrM_MainIgnEdgeSts = CreateBus(SenPwrM_MainIgnEdgeSts, DeList);
clear DeList;

SenPwrM_MainVBatt1Mon = Simulink.Bus;
DeList={'SenPwrM_MainVBatt1Mon'};
SenPwrM_MainVBatt1Mon = CreateBus(SenPwrM_MainVBatt1Mon, DeList);
clear DeList;

SenPwrM_MainDiagClrSrs = Simulink.Bus;
DeList={'SenPwrM_MainDiagClrSrs'};
SenPwrM_MainDiagClrSrs = CreateBus(SenPwrM_MainDiagClrSrs, DeList);
clear DeList;

SenPwrM_MainArbVlvDriveState = Simulink.Bus;
DeList={'SenPwrM_MainArbVlvDriveState'};
SenPwrM_MainArbVlvDriveState = CreateBus(SenPwrM_MainArbVlvDriveState, DeList);
clear DeList;

SenPwrM_MainExt5vMon = Simulink.Bus;
DeList={'SenPwrM_MainExt5vMon'};
SenPwrM_MainExt5vMon = CreateBus(SenPwrM_MainExt5vMon, DeList);
clear DeList;

SenPwrM_MainCspMon = Simulink.Bus;
DeList={'SenPwrM_MainCspMon'};
SenPwrM_MainCspMon = CreateBus(SenPwrM_MainCspMon, DeList);
clear DeList;

SenPwrM_MainMtrArbDriveState = Simulink.Bus;
DeList={'SenPwrM_MainMtrArbDriveState'};
SenPwrM_MainMtrArbDriveState = CreateBus(SenPwrM_MainMtrArbDriveState, DeList);
clear DeList;

SenPwrM_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    'SenPwrM_5V_DriveReq'
    'SenPwrM_12V_Drive_Req'
    'SenPwrM_5V_Err'
    'SenPwrM_12V_Open_Err'
    'SenPwrM_12V_Short_Err'
    };
SenPwrM_MainSenPwrMonitorData = CreateBus(SenPwrM_MainSenPwrMonitorData, DeList);
clear DeList;

