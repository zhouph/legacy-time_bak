#define S_FUNCTION_NAME      SenPwrM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          11
#define WidthOutputPort         7

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "SenPwrM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V = input[0];
    SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V = input[1];
    SenPwrM_MainEcuModeSts = input[2];
    SenPwrM_MainIgnOnOffSts = input[3];
    SenPwrM_MainIgnEdgeSts = input[4];
    SenPwrM_MainVBatt1Mon = input[5];
    SenPwrM_MainDiagClrSrs = input[6];
    SenPwrM_MainArbVlvDriveState = input[7];
    SenPwrM_MainExt5vMon = input[8];
    SenPwrM_MainCspMon = input[9];
    SenPwrM_MainMtrArbDriveState = input[10];

    SenPwrM_Main();


    output[0] = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable;
    output[1] = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable;
    output[2] = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq;
    output[3] = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req;
    output[4] = SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err;
    output[5] = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err;
    output[6] = SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    SenPwrM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
