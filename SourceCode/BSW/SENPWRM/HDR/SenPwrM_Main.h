/**
 * @defgroup SenPwrM_Main SenPwrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENPWRM_MAIN_H_
#define SENPWRM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SenPwrM_Types.h"
#include "SenPwrM_Cfg.h"
#include "SenPwrM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SENPWRM_MAIN_MODULE_ID      (0)
 #define SENPWRM_MAIN_MAJOR_VERSION  (2)
 #define SENPWRM_MAIN_MINOR_VERSION  (0)
 #define SENPWRM_MAIN_PATCH_VERSION  (0)
 #define SENPWRM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SenPwrM_Main_HdrBusType SenPwrM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SenPwrM_MainVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t SenPwrM_MainEemFailData;
extern Mom_HndlrEcuModeSts_t SenPwrM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t SenPwrM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t SenPwrM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t SenPwrM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t SenPwrM_MainDiagClrSrs;
extern Arbitrator_VlvArbVlvDriveState_t SenPwrM_MainArbVlvDriveState;
extern Ioc_InputSR1msExt5vMon_t SenPwrM_MainExt5vMon;
extern Ioc_InputSR1msCspMon_t SenPwrM_MainCspMon;
extern Arbitrator_MtrMtrArbDriveState_t SenPwrM_MainMtrArbDriveState;

/* Output Data Element */
extern SenPwrM_MainSenPwrMonitor_t SenPwrM_MainSenPwrMonitorData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SenPwrM_Main_Init(void);
extern void SenPwrM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENPWRM_MAIN_H_ */
/** @} */
