/**
 * @defgroup SenPwrM_Types SenPwrM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SENPWRM_TYPES_H_
#define SENPWRM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t SenPwrM_MainEemFailData;
    Mom_HndlrEcuModeSts_t SenPwrM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SenPwrM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SenPwrM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SenPwrM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t SenPwrM_MainDiagClrSrs;
    Arbitrator_VlvArbVlvDriveState_t SenPwrM_MainArbVlvDriveState;
    Ioc_InputSR1msExt5vMon_t SenPwrM_MainExt5vMon;
    Ioc_InputSR1msCspMon_t SenPwrM_MainCspMon;
    Arbitrator_MtrMtrArbDriveState_t SenPwrM_MainMtrArbDriveState;

/* Output Data Element */
    SenPwrM_MainSenPwrMonitor_t SenPwrM_MainSenPwrMonitorData;
}SenPwrM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SENPWRM_TYPES_H_ */
/** @} */
