# \file
#
# \brief SenPwrM
#
# This file contains the implementation of the SWC
# module SenPwrM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SenPwrM_src

SenPwrM_src_FILES        += $(SenPwrM_SRC_PATH)\SenPwrM_Main.c
SenPwrM_src_FILES        += $(SenPwrM_IFA_PATH)\SenPwrM_Main_Ifa.c
SenPwrM_src_FILES        += $(SenPwrM_CFG_PATH)\SenPwrM_Cfg.c
SenPwrM_src_FILES        += $(SenPwrM_CAL_PATH)\SenPwrM_Cal.c
SenPwrM_src_FILES        += $(SenPwrM_SRC_PATH)\SenPwrM_Process.c

ifeq ($(ICE_COMPILE),true)
SenPwrM_src_FILES        += $(SenPwrM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SenPwrM_src_FILES        += $(SenPwrM_UNITY_PATH)\unity.c
	SenPwrM_src_FILES        += $(SenPwrM_UNITY_PATH)\unity_fixture.c	
	SenPwrM_src_FILES        += $(SenPwrM_UT_PATH)\main.c
	SenPwrM_src_FILES        += $(SenPwrM_UT_PATH)\SenPwrM_Main\SenPwrM_Main_UtMain.c
endif