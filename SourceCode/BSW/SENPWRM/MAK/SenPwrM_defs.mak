# \file
#
# \brief SenPwrM
#
# This file contains the implementation of the SWC
# module SenPwrM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SenPwrM_CORE_PATH     := $(MANDO_BSW_ROOT)\SenPwrM
SenPwrM_CAL_PATH      := $(SenPwrM_CORE_PATH)\CAL\$(SenPwrM_VARIANT)
SenPwrM_SRC_PATH      := $(SenPwrM_CORE_PATH)\SRC
SenPwrM_CFG_PATH      := $(SenPwrM_CORE_PATH)\CFG\$(SenPwrM_VARIANT)
SenPwrM_HDR_PATH      := $(SenPwrM_CORE_PATH)\HDR
SenPwrM_IFA_PATH      := $(SenPwrM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SenPwrM_CMN_PATH      := $(SenPwrM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SenPwrM_UT_PATH		:= $(SenPwrM_CORE_PATH)\UT
	SenPwrM_UNITY_PATH	:= $(SenPwrM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SenPwrM_UT_PATH)
	CC_INCLUDE_PATH		+= $(SenPwrM_UNITY_PATH)
	SenPwrM_Main_PATH 	:= SenPwrM_UT_PATH\SenPwrM_Main
endif
CC_INCLUDE_PATH    += $(SenPwrM_CAL_PATH)
CC_INCLUDE_PATH    += $(SenPwrM_SRC_PATH)
CC_INCLUDE_PATH    += $(SenPwrM_CFG_PATH)
CC_INCLUDE_PATH    += $(SenPwrM_HDR_PATH)
CC_INCLUDE_PATH    += $(SenPwrM_IFA_PATH)
CC_INCLUDE_PATH    += $(SenPwrM_CMN_PATH)

