#include "unity.h"
#include "unity_fixture.h"
#include "SenPwrM_Main.h"
#include "SenPwrM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = SENPWRM_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_5V[MAX_STEP] = SENPWRM_MAINEEMFAILDATA_EEM_FAIL_SENPWR_5V;
const Mom_HndlrEcuModeSts_t UtInput_SenPwrM_MainEcuModeSts[MAX_STEP] = SENPWRM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_SenPwrM_MainIgnOnOffSts[MAX_STEP] = SENPWRM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_SenPwrM_MainIgnEdgeSts[MAX_STEP] = SENPWRM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_SenPwrM_MainVBatt1Mon[MAX_STEP] = SENPWRM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_SenPwrM_MainDiagClrSrs[MAX_STEP] = SENPWRM_MAINDIAGCLRSRS;
const Arbitrator_VlvArbVlvDriveState_t UtInput_SenPwrM_MainArbVlvDriveState[MAX_STEP] = SENPWRM_MAINARBVLVDRIVESTATE;
const Ioc_InputSR1msExt5vMon_t UtInput_SenPwrM_MainExt5vMon[MAX_STEP] = SENPWRM_MAINEXT5VMON;
const Ioc_InputSR1msCspMon_t UtInput_SenPwrM_MainCspMon[MAX_STEP] = SENPWRM_MAINCSPMON;
const Arbitrator_MtrMtrArbDriveState_t UtInput_SenPwrM_MainMtrArbDriveState[MAX_STEP] = SENPWRM_MAINMTRARBDRIVESTATE;

const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_5V_DRIVEREQ;
const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_12V_DRIVE_REQ;
const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_5V_ERR;
const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_12V_OPEN_ERR;
const Saluint8 UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err[MAX_STEP] = SENPWRM_MAINSENPWRMONITORDATA_SENPWRM_12V_SHORT_ERR;



TEST_GROUP(SenPwrM_Main);
TEST_SETUP(SenPwrM_Main)
{
    SenPwrM_Main_Init();
}

TEST_TEAR_DOWN(SenPwrM_Main)
{   /* Postcondition */

}

TEST(SenPwrM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SenPwrM_MainEemFailData.Eem_Fail_SenPwr_12V = UtInput_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_12V[i];
        SenPwrM_MainEemFailData.Eem_Fail_SenPwr_5V = UtInput_SenPwrM_MainEemFailData_Eem_Fail_SenPwr_5V[i];
        SenPwrM_MainEcuModeSts = UtInput_SenPwrM_MainEcuModeSts[i];
        SenPwrM_MainIgnOnOffSts = UtInput_SenPwrM_MainIgnOnOffSts[i];
        SenPwrM_MainIgnEdgeSts = UtInput_SenPwrM_MainIgnEdgeSts[i];
        SenPwrM_MainVBatt1Mon = UtInput_SenPwrM_MainVBatt1Mon[i];
        SenPwrM_MainDiagClrSrs = UtInput_SenPwrM_MainDiagClrSrs[i];
        SenPwrM_MainArbVlvDriveState = UtInput_SenPwrM_MainArbVlvDriveState[i];
        SenPwrM_MainExt5vMon = UtInput_SenPwrM_MainExt5vMon[i];
        SenPwrM_MainCspMon = UtInput_SenPwrM_MainCspMon[i];
        SenPwrM_MainMtrArbDriveState = UtInput_SenPwrM_MainMtrArbDriveState[i];

        SenPwrM_Main();

        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Stable, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Stable[i]);
        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Stable[i]);
        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_DriveReq, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_DriveReq[i]);
        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Drive_Req, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Drive_Req[i]);
        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_5V_Err, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_5V_Err[i]);
        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Open_Err, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Open_Err[i]);
        TEST_ASSERT_EQUAL(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Short_Err, UtExpected_SenPwrM_MainSenPwrMonitorData_SenPwrM_12V_Short_Err[i]);
    }
}

TEST_GROUP_RUNNER(SenPwrM_Main)
{
    RUN_TEST_CASE(SenPwrM_Main, All);
}
