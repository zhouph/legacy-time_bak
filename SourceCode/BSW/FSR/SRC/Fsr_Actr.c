/**
 * @defgroup Fsr_Actr Fsr_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Fsr_Actr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Fsr_Actr.h"
#include "Fsr_Actr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define FSR_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define FSR_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define FSR_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Fsr_Actr_HdrBusType Fsr_ActrBus;

/* Version Info */
const SwcVersionInfo_t Fsr_ActrVersionInfo = 
{   
    FSR_ACTR_MODULE_ID,           /* Fsr_ActrVersionInfo.ModuleId */
    FSR_ACTR_MAJOR_VERSION,       /* Fsr_ActrVersionInfo.MajorVer */
    FSR_ACTR_MINOR_VERSION,       /* Fsr_ActrVersionInfo.MinorVer */
    FSR_ACTR_PATCH_VERSION,       /* Fsr_ActrVersionInfo.PatchVer */
    FSR_ACTR_BRANCH_VERSION       /* Fsr_ActrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Arbitrator_VlvArbFsrBbsDrvInfo_t Fsr_ActrArbFsrBbsDrvInfo;
Arbitrator_VlvArbFsrAbsDrvInfo_t Fsr_ActrArbFsrAbsDrvInfo;
Mom_HndlrEcuModeSts_t Fsr_ActrEcuModeSts;
Arbitrator_VlvArbAsicEnDrDrvInfo_t Fsr_ActrAsicEnDrDrv;
Acm_MainAcmAsicInitCompleteFlag_t Fsr_ActrAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitFsrSts_t Fsr_ActrFuncInhibitFsrSts;

/* Output Data Element */
Fsr_ActrFsrAbsDrvInfo_t Fsr_ActrFsrAbsDrvInfo;
Fsr_ActrFsrCbsDrvInfo_t Fsr_ActrFsrCbsDrvInfo;
Fsr_ActrFsrDcMtrShutDwn_t Fsr_ActrFsrDcMtrShutDwn;
Fsr_ActrFsrEnDrDrv_t Fsr_ActrFsrEnDrDrv;

uint32 Fsr_Actr_Timer_Start;
uint32 Fsr_Actr_Timer_Elapsed;

#define FSR_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define FSR_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
#define FSR_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define FSR_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_ACTR_START_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (32BIT)**/


#define FSR_ACTR_STOP_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define FSR_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define FSR_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define FSR_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
#define FSR_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define FSR_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_ACTR_START_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (32BIT)**/


#define FSR_ACTR_STOP_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void Fsr_PortControl(void);
 
#define FSR_ACTR_START_SEC_CODE
#include "Fsr_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Fsr_Actr_Init(void)
{
    /* Initialize internal bus */
    Fsr_ActrBus.Fsr_ActrArbFsrBbsDrvInfo.FsrCbsDrv = 0;
    Fsr_ActrBus.Fsr_ActrArbFsrBbsDrvInfo.FsrCbsOff = 0;
    Fsr_ActrBus.Fsr_ActrArbFsrAbsDrvInfo.FsrAbsDrv = 0;
    Fsr_ActrBus.Fsr_ActrArbFsrAbsDrvInfo.FsrAbsOff = 0;
    Fsr_ActrBus.Fsr_ActrEcuModeSts = 0;
    Fsr_ActrBus.Fsr_ActrAsicEnDrDrv = 0;
    Fsr_ActrBus.Fsr_ActrAcmAsicInitCompleteFlag = 0;
    Fsr_ActrBus.Fsr_ActrFuncInhibitFsrSts = 0;
    Fsr_ActrBus.Fsr_ActrFsrAbsDrvInfo.FsrAbsOff = 0;
    Fsr_ActrBus.Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv = 0;
    Fsr_ActrBus.Fsr_ActrFsrCbsDrvInfo.FsrCbsOff = 0;
    Fsr_ActrBus.Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv = 0;
    Fsr_ActrBus.Fsr_ActrFsrDcMtrShutDwn = 0;
    Fsr_ActrBus.Fsr_ActrFsrEnDrDrv = 0;
}

void Fsr_Actr(void)
{
    Fsr_Actr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Fsr_Actr_Read_Fsr_ActrArbFsrBbsDrvInfo(&Fsr_ActrBus.Fsr_ActrArbFsrBbsDrvInfo);
    /*==============================================================================
    * Members of structure Fsr_ActrArbFsrBbsDrvInfo 
     : Fsr_ActrArbFsrBbsDrvInfo.FsrCbsDrv;
     : Fsr_ActrArbFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/
    
    Fsr_Actr_Read_Fsr_ActrArbFsrAbsDrvInfo(&Fsr_ActrBus.Fsr_ActrArbFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure Fsr_ActrArbFsrAbsDrvInfo 
     : Fsr_ActrArbFsrAbsDrvInfo.FsrAbsDrv;
     : Fsr_ActrArbFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/
    
    Fsr_Actr_Read_Fsr_ActrEcuModeSts(&Fsr_ActrBus.Fsr_ActrEcuModeSts);
    Fsr_Actr_Read_Fsr_ActrAsicEnDrDrv(&Fsr_ActrBus.Fsr_ActrAsicEnDrDrv);
    Fsr_Actr_Read_Fsr_ActrAcmAsicInitCompleteFlag(&Fsr_ActrBus.Fsr_ActrAcmAsicInitCompleteFlag);
    Fsr_Actr_Read_Fsr_ActrFuncInhibitFsrSts(&Fsr_ActrBus.Fsr_ActrFuncInhibitFsrSts);

    /* Process */
    Fsr_PortControl();

    /* Output */
    Fsr_Actr_Write_Fsr_ActrFsrAbsDrvInfo(&Fsr_ActrBus.Fsr_ActrFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure Fsr_ActrFsrAbsDrvInfo 
     : Fsr_ActrFsrAbsDrvInfo.FsrAbsOff;
     : Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv;
     =============================================================================*/
    
    Fsr_Actr_Write_Fsr_ActrFsrCbsDrvInfo(&Fsr_ActrBus.Fsr_ActrFsrCbsDrvInfo);
    /*==============================================================================
    * Members of structure Fsr_ActrFsrCbsDrvInfo 
     : Fsr_ActrFsrCbsDrvInfo.FsrCbsOff;
     : Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv;
     =============================================================================*/
    
    Fsr_Actr_Write_Fsr_ActrFsrDcMtrShutDwn(&Fsr_ActrBus.Fsr_ActrFsrDcMtrShutDwn);
    Fsr_Actr_Write_Fsr_ActrFsrEnDrDrv(&Fsr_ActrBus.Fsr_ActrFsrEnDrDrv);

    Fsr_Actr_Timer_Elapsed = STM0_TIM0.U - Fsr_Actr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Fsr_PortControl(void)
{
    Fsr_ActrBus.Fsr_ActrFsrCbsDrvInfo.FsrCbsOff = Fsr_ActrBus.Fsr_ActrArbFsrBbsDrvInfo.FsrCbsOff;
    Fsr_ActrBus.Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv = Fsr_ActrBus.Fsr_ActrArbFsrBbsDrvInfo.FsrCbsDrv;

    Fsr_ActrBus.Fsr_ActrFsrAbsDrvInfo.FsrAbsOff = Fsr_ActrBus.Fsr_ActrArbFsrAbsDrvInfo.FsrAbsOff;
    Fsr_ActrBus.Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv = Fsr_ActrBus.Fsr_ActrArbFsrAbsDrvInfo.FsrAbsDrv;

    Fsr_ActrBus.Fsr_ActrFsrEnDrDrv = Fsr_ActrBus.Fsr_ActrAsicEnDrDrv;
}

#define FSR_ACTR_STOP_SEC_CODE
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
