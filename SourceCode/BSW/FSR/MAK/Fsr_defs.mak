# \file
#
# \brief Fsr
#
# This file contains the implementation of the SWC
# module Fsr.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Fsr_CORE_PATH     := $(MANDO_BSW_ROOT)\Fsr
Fsr_CAL_PATH      := $(Fsr_CORE_PATH)\CAL\$(Fsr_VARIANT)
Fsr_SRC_PATH      := $(Fsr_CORE_PATH)\SRC
Fsr_CFG_PATH      := $(Fsr_CORE_PATH)\CFG\$(Fsr_VARIANT)
Fsr_HDR_PATH      := $(Fsr_CORE_PATH)\HDR
Fsr_IFA_PATH      := $(Fsr_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Fsr_CMN_PATH      := $(Fsr_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Fsr_UT_PATH		:= $(Fsr_CORE_PATH)\UT
	Fsr_UNITY_PATH	:= $(Fsr_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Fsr_UT_PATH)
	CC_INCLUDE_PATH		+= $(Fsr_UNITY_PATH)
	Fsr_Actr_PATH 	:= Fsr_UT_PATH\Fsr_Actr
endif
CC_INCLUDE_PATH    += $(Fsr_CAL_PATH)
CC_INCLUDE_PATH    += $(Fsr_SRC_PATH)
CC_INCLUDE_PATH    += $(Fsr_CFG_PATH)
CC_INCLUDE_PATH    += $(Fsr_HDR_PATH)
CC_INCLUDE_PATH    += $(Fsr_IFA_PATH)
CC_INCLUDE_PATH    += $(Fsr_CMN_PATH)

