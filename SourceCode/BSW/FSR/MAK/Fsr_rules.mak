# \file
#
# \brief Fsr
#
# This file contains the implementation of the SWC
# module Fsr.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Fsr_src

Fsr_src_FILES        += $(Fsr_SRC_PATH)\Fsr_Actr.c
Fsr_src_FILES        += $(Fsr_IFA_PATH)\Fsr_Actr_Ifa.c
Fsr_src_FILES        += $(Fsr_CFG_PATH)\Fsr_Cfg.c
Fsr_src_FILES        += $(Fsr_CAL_PATH)\Fsr_Cal.c

ifeq ($(ICE_COMPILE),true)
Fsr_src_FILES        += $(Fsr_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Fsr_src_FILES        += $(Fsr_UNITY_PATH)\unity.c
	Fsr_src_FILES        += $(Fsr_UNITY_PATH)\unity_fixture.c	
	Fsr_src_FILES        += $(Fsr_UT_PATH)\main.c
	Fsr_src_FILES        += $(Fsr_UT_PATH)\Fsr_Actr\Fsr_Actr_UtMain.c
endif