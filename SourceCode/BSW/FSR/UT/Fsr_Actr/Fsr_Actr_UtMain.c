#include "unity.h"
#include "unity_fixture.h"
#include "Fsr_Actr.h"
#include "Fsr_Actr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Fsr_ActrArbFsrBbsDrvInfo_FsrCbsDrv[MAX_STEP] = FSR_ACTRARBFSRBBSDRVINFO_FSRCBSDRV;
const Saluint8 UtInput_Fsr_ActrArbFsrBbsDrvInfo_FsrCbsOff[MAX_STEP] = FSR_ACTRARBFSRBBSDRVINFO_FSRCBSOFF;
const Saluint8 UtInput_Fsr_ActrArbFsrAbsDrvInfo_FsrAbsDrv[MAX_STEP] = FSR_ACTRARBFSRABSDRVINFO_FSRABSDRV;
const Saluint8 UtInput_Fsr_ActrArbFsrAbsDrvInfo_FsrAbsOff[MAX_STEP] = FSR_ACTRARBFSRABSDRVINFO_FSRABSOFF;
const Mom_HndlrEcuModeSts_t UtInput_Fsr_ActrEcuModeSts[MAX_STEP] = FSR_ACTRECUMODESTS;
const Arbitrator_VlvArbAsicEnDrDrvInfo_t UtInput_Fsr_ActrAsicEnDrDrv[MAX_STEP] = FSR_ACTRASICENDRDRV;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Fsr_ActrAcmAsicInitCompleteFlag[MAX_STEP] = FSR_ACTRACMASICINITCOMPLETEFLAG;
const Eem_SuspcDetnFuncInhibitFsrSts_t UtInput_Fsr_ActrFuncInhibitFsrSts[MAX_STEP] = FSR_ACTRFUNCINHIBITFSRSTS;

const Saluint8 UtExpected_Fsr_ActrFsrAbsDrvInfo_FsrAbsOff[MAX_STEP] = FSR_ACTRFSRABSDRVINFO_FSRABSOFF;
const Saluint8 UtExpected_Fsr_ActrFsrAbsDrvInfo_FsrAbsDrv[MAX_STEP] = FSR_ACTRFSRABSDRVINFO_FSRABSDRV;
const Saluint8 UtExpected_Fsr_ActrFsrCbsDrvInfo_FsrCbsOff[MAX_STEP] = FSR_ACTRFSRCBSDRVINFO_FSRCBSOFF;
const Saluint8 UtExpected_Fsr_ActrFsrCbsDrvInfo_FsrCbsDrv[MAX_STEP] = FSR_ACTRFSRCBSDRVINFO_FSRCBSDRV;
const Fsr_ActrFsrDcMtrShutDwn_t UtExpected_Fsr_ActrFsrDcMtrShutDwn[MAX_STEP] = FSR_ACTRFSRDCMTRSHUTDWN;
const Fsr_ActrFsrEnDrDrv_t UtExpected_Fsr_ActrFsrEnDrDrv[MAX_STEP] = FSR_ACTRFSRENDRDRV;



TEST_GROUP(Fsr_Actr);
TEST_SETUP(Fsr_Actr)
{
    Fsr_Actr_Init();
}

TEST_TEAR_DOWN(Fsr_Actr)
{   /* Postcondition */

}

TEST(Fsr_Actr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Fsr_ActrArbFsrBbsDrvInfo.FsrCbsDrv = UtInput_Fsr_ActrArbFsrBbsDrvInfo_FsrCbsDrv[i];
        Fsr_ActrArbFsrBbsDrvInfo.FsrCbsOff = UtInput_Fsr_ActrArbFsrBbsDrvInfo_FsrCbsOff[i];
        Fsr_ActrArbFsrAbsDrvInfo.FsrAbsDrv = UtInput_Fsr_ActrArbFsrAbsDrvInfo_FsrAbsDrv[i];
        Fsr_ActrArbFsrAbsDrvInfo.FsrAbsOff = UtInput_Fsr_ActrArbFsrAbsDrvInfo_FsrAbsOff[i];
        Fsr_ActrEcuModeSts = UtInput_Fsr_ActrEcuModeSts[i];
        Fsr_ActrAsicEnDrDrv = UtInput_Fsr_ActrAsicEnDrDrv[i];
        Fsr_ActrAcmAsicInitCompleteFlag = UtInput_Fsr_ActrAcmAsicInitCompleteFlag[i];
        Fsr_ActrFuncInhibitFsrSts = UtInput_Fsr_ActrFuncInhibitFsrSts[i];

        Fsr_Actr();

        TEST_ASSERT_EQUAL(Fsr_ActrFsrAbsDrvInfo.FsrAbsOff, UtExpected_Fsr_ActrFsrAbsDrvInfo_FsrAbsOff[i]);
        TEST_ASSERT_EQUAL(Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv, UtExpected_Fsr_ActrFsrAbsDrvInfo_FsrAbsDrv[i]);
        TEST_ASSERT_EQUAL(Fsr_ActrFsrCbsDrvInfo.FsrCbsOff, UtExpected_Fsr_ActrFsrCbsDrvInfo_FsrCbsOff[i]);
        TEST_ASSERT_EQUAL(Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv, UtExpected_Fsr_ActrFsrCbsDrvInfo_FsrCbsDrv[i]);
        TEST_ASSERT_EQUAL(Fsr_ActrFsrDcMtrShutDwn, UtExpected_Fsr_ActrFsrDcMtrShutDwn[i]);
        TEST_ASSERT_EQUAL(Fsr_ActrFsrEnDrDrv, UtExpected_Fsr_ActrFsrEnDrDrv[i]);
    }
}

TEST_GROUP_RUNNER(Fsr_Actr)
{
    RUN_TEST_CASE(Fsr_Actr, All);
}
