/**
 * @defgroup Fsr_Types Fsr_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Fsr_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef FSR_TYPES_H_
#define FSR_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Arbitrator_VlvArbFsrBbsDrvInfo_t Fsr_ActrArbFsrBbsDrvInfo;
    Arbitrator_VlvArbFsrAbsDrvInfo_t Fsr_ActrArbFsrAbsDrvInfo;
    Mom_HndlrEcuModeSts_t Fsr_ActrEcuModeSts;
    Arbitrator_VlvArbAsicEnDrDrvInfo_t Fsr_ActrAsicEnDrDrv;
    Acm_MainAcmAsicInitCompleteFlag_t Fsr_ActrAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitFsrSts_t Fsr_ActrFuncInhibitFsrSts;

/* Output Data Element */
    Fsr_ActrFsrAbsDrvInfo_t Fsr_ActrFsrAbsDrvInfo;
    Fsr_ActrFsrCbsDrvInfo_t Fsr_ActrFsrCbsDrvInfo;
    Fsr_ActrFsrDcMtrShutDwn_t Fsr_ActrFsrDcMtrShutDwn;
    Fsr_ActrFsrEnDrDrv_t Fsr_ActrFsrEnDrDrv;
}Fsr_Actr_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* FSR_TYPES_H_ */
/** @} */
