/**
 * @defgroup Fsr_Actr Fsr_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Fsr_Actr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef FSR_ACTR_H_
#define FSR_ACTR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Fsr_Types.h"
#include "Fsr_Cfg.h"
#include "Fsr_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define FSR_ACTR_MODULE_ID      (0)
 #define FSR_ACTR_MAJOR_VERSION  (2)
 #define FSR_ACTR_MINOR_VERSION  (0)
 #define FSR_ACTR_PATCH_VERSION  (0)
 #define FSR_ACTR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Fsr_Actr_HdrBusType Fsr_ActrBus;

/* Version Info */
extern const SwcVersionInfo_t Fsr_ActrVersionInfo;

/* Input Data Element */
extern Arbitrator_VlvArbFsrBbsDrvInfo_t Fsr_ActrArbFsrBbsDrvInfo;
extern Arbitrator_VlvArbFsrAbsDrvInfo_t Fsr_ActrArbFsrAbsDrvInfo;
extern Mom_HndlrEcuModeSts_t Fsr_ActrEcuModeSts;
extern Arbitrator_VlvArbAsicEnDrDrvInfo_t Fsr_ActrAsicEnDrDrv;
extern Acm_MainAcmAsicInitCompleteFlag_t Fsr_ActrAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitFsrSts_t Fsr_ActrFuncInhibitFsrSts;

/* Output Data Element */
extern Fsr_ActrFsrAbsDrvInfo_t Fsr_ActrFsrAbsDrvInfo;
extern Fsr_ActrFsrCbsDrvInfo_t Fsr_ActrFsrCbsDrvInfo;
extern Fsr_ActrFsrDcMtrShutDwn_t Fsr_ActrFsrDcMtrShutDwn;
extern Fsr_ActrFsrEnDrDrv_t Fsr_ActrFsrEnDrDrv;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Fsr_Actr_Init(void);
extern void Fsr_Actr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* FSR_ACTR_H_ */
/** @} */
