/**
 * @defgroup Fsr_Cal Fsr_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Fsr_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Fsr_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define FSR_START_SEC_CALIB_UNSPECIFIED
#include "Fsr_MemMap.h"
/* Global Calibration Section */


#define FSR_STOP_SEC_CALIB_UNSPECIFIED
#include "Fsr_MemMap.h"

#define FSR_START_SEC_CONST_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define FSR_STOP_SEC_CONST_UNSPECIFIED
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define FSR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define FSR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_START_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define FSR_STOP_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
#define FSR_START_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define FSR_STOP_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_START_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (32BIT)**/


#define FSR_STOP_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define FSR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define FSR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_START_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define FSR_STOP_SEC_VAR_NOINIT_32BIT
#include "Fsr_MemMap.h"
#define FSR_START_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define FSR_STOP_SEC_VAR_UNSPECIFIED
#include "Fsr_MemMap.h"
#define FSR_START_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/** Variable Section (32BIT)**/


#define FSR_STOP_SEC_VAR_32BIT
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define FSR_START_SEC_CODE
#include "Fsr_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define FSR_STOP_SEC_CODE
#include "Fsr_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
