/**
 * @defgroup Fsr_Actr_Ifa Fsr_Actr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Fsr_Actr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef FSR_ACTR_IFA_H_
#define FSR_ACTR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Fsr_Actr_Read_Fsr_ActrArbFsrBbsDrvInfo(data) do \
{ \
    *data = Fsr_ActrArbFsrBbsDrvInfo; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrArbFsrAbsDrvInfo(data) do \
{ \
    *data = Fsr_ActrArbFsrAbsDrvInfo; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrArbFsrBbsDrvInfo_FsrCbsDrv(data) do \
{ \
    *data = Fsr_ActrArbFsrBbsDrvInfo.FsrCbsDrv; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrArbFsrBbsDrvInfo_FsrCbsOff(data) do \
{ \
    *data = Fsr_ActrArbFsrBbsDrvInfo.FsrCbsOff; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrArbFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    *data = Fsr_ActrArbFsrAbsDrvInfo.FsrAbsDrv; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrArbFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    *data = Fsr_ActrArbFsrAbsDrvInfo.FsrAbsOff; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrEcuModeSts(data) do \
{ \
    *data = Fsr_ActrEcuModeSts; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrAsicEnDrDrv(data) do \
{ \
    *data = Fsr_ActrAsicEnDrDrv; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Fsr_ActrAcmAsicInitCompleteFlag; \
}while(0);

#define Fsr_Actr_Read_Fsr_ActrFuncInhibitFsrSts(data) do \
{ \
    *data = Fsr_ActrFuncInhibitFsrSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Fsr_Actr_Write_Fsr_ActrFsrAbsDrvInfo(data) do \
{ \
    Fsr_ActrFsrAbsDrvInfo = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrCbsDrvInfo(data) do \
{ \
    Fsr_ActrFsrCbsDrvInfo = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    Fsr_ActrFsrAbsDrvInfo.FsrAbsOff = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    Fsr_ActrFsrAbsDrvInfo.FsrAbsDrv = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrCbsDrvInfo_FsrCbsOff(data) do \
{ \
    Fsr_ActrFsrCbsDrvInfo.FsrCbsOff = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrCbsDrvInfo_FsrCbsDrv(data) do \
{ \
    Fsr_ActrFsrCbsDrvInfo.FsrCbsDrv = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrDcMtrShutDwn(data) do \
{ \
    Fsr_ActrFsrDcMtrShutDwn = *data; \
}while(0);

#define Fsr_Actr_Write_Fsr_ActrFsrEnDrDrv(data) do \
{ \
    Fsr_ActrFsrEnDrDrv = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* FSR_ACTR_IFA_H_ */
/** @} */
