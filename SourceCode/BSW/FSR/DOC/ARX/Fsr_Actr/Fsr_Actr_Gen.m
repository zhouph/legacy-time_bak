Fsr_ActrArbFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
Fsr_ActrArbFsrBbsDrvInfo = CreateBus(Fsr_ActrArbFsrBbsDrvInfo, DeList);
clear DeList;

Fsr_ActrArbFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
Fsr_ActrArbFsrAbsDrvInfo = CreateBus(Fsr_ActrArbFsrAbsDrvInfo, DeList);
clear DeList;

Fsr_ActrEcuModeSts = Simulink.Bus;
DeList={'Fsr_ActrEcuModeSts'};
Fsr_ActrEcuModeSts = CreateBus(Fsr_ActrEcuModeSts, DeList);
clear DeList;

Fsr_ActrAsicEnDrDrv = Simulink.Bus;
DeList={'Fsr_ActrAsicEnDrDrv'};
Fsr_ActrAsicEnDrDrv = CreateBus(Fsr_ActrAsicEnDrDrv, DeList);
clear DeList;

Fsr_ActrAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Fsr_ActrAcmAsicInitCompleteFlag'};
Fsr_ActrAcmAsicInitCompleteFlag = CreateBus(Fsr_ActrAcmAsicInitCompleteFlag, DeList);
clear DeList;

Fsr_ActrFuncInhibitFsrSts = Simulink.Bus;
DeList={'Fsr_ActrFuncInhibitFsrSts'};
Fsr_ActrFuncInhibitFsrSts = CreateBus(Fsr_ActrFuncInhibitFsrSts, DeList);
clear DeList;

Fsr_ActrFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsOff'
    'FsrAbsDrv'
    };
Fsr_ActrFsrAbsDrvInfo = CreateBus(Fsr_ActrFsrAbsDrvInfo, DeList);
clear DeList;

Fsr_ActrFsrCbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsOff'
    'FsrCbsDrv'
    };
Fsr_ActrFsrCbsDrvInfo = CreateBus(Fsr_ActrFsrCbsDrvInfo, DeList);
clear DeList;

Fsr_ActrFsrDcMtrShutDwn = Simulink.Bus;
DeList={'Fsr_ActrFsrDcMtrShutDwn'};
Fsr_ActrFsrDcMtrShutDwn = CreateBus(Fsr_ActrFsrDcMtrShutDwn, DeList);
clear DeList;

Fsr_ActrFsrEnDrDrv = Simulink.Bus;
DeList={'Fsr_ActrFsrEnDrDrv'};
Fsr_ActrFsrEnDrDrv = CreateBus(Fsr_ActrFsrEnDrDrv, DeList);
clear DeList;

