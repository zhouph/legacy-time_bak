/**
 * @defgroup Ses_Cal Ses_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ses_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ses_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SES_START_SEC_CALIB_UNSPECIFIED
#include "Ses_MemMap.h"
/* Global Calibration Section */


#define SES_STOP_SEC_CALIB_UNSPECIFIED
#include "Ses_MemMap.h"

#define SES_START_SEC_CONST_UNSPECIFIED
#include "Ses_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SES_STOP_SEC_CONST_UNSPECIFIED
#include "Ses_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SES_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SES_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_START_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SES_STOP_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
#define SES_START_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SES_STOP_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_START_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/** Variable Section (32BIT)**/


#define SES_STOP_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SES_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SES_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_START_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SES_STOP_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
#define SES_START_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SES_STOP_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_START_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/** Variable Section (32BIT)**/


#define SES_STOP_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SES_START_SEC_CODE
#include "Ses_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SES_STOP_SEC_CODE
#include "Ses_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
