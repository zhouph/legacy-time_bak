#define S_FUNCTION_NAME      Ses_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          7
#define WidthOutputPort         69

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ses_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ses_CtrlMotDqIMeasdInfo.MotIqMeasd = input[0];
    Ses_CtrlMotDqIMeasdInfo.MotIdMeasd = input[1];
    Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd = input[2];
    Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = input[3];
    Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = input[4];
    Ses_CtrlEcuModeSts = input[5];
    Ses_CtrlFuncInhibitSesSts = input[6];

    Ses_Ctrl();


    output[0] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[0];
    output[1] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[1];
    output[2] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[2];
    output[3] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[3];
    output[4] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[4];
    output[5] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[0];
    output[6] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[1];
    output[7] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[2];
    output[8] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[3];
    output[9] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[4];
    output[10] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[0];
    output[11] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[1];
    output[12] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[2];
    output[13] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[3];
    output[14] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[4];
    output[15] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[0];
    output[16] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[1];
    output[17] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[2];
    output[18] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[3];
    output[19] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[4];
    output[20] = Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[0];
    output[21] = Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[1];
    output[22] = Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[2];
    output[23] = Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[3];
    output[24] = Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[4];
    output[25] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq;
    output[26] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq;
    output[27] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq;
    output[28] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq;
    output[29] = Ses_CtrlNormVlvReqSesInfo.SimVlvReq;
    output[30] = Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen;
    output[31] = Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen;
    output[32] = Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen;
    output[33] = Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen;
    output[34] = Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen;
    output[35] = Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[0];
    output[36] = Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[1];
    output[37] = Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[2];
    output[38] = Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[3];
    output[39] = Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[4];
    output[40] = Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[0];
    output[41] = Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[1];
    output[42] = Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[2];
    output[43] = Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[3];
    output[44] = Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[4];
    output[45] = Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[0];
    output[46] = Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[1];
    output[47] = Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[2];
    output[48] = Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[3];
    output[49] = Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[4];
    output[50] = Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[0];
    output[51] = Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[1];
    output[52] = Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[2];
    output[53] = Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[3];
    output[54] = Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[4];
    output[55] = Ses_CtrlWhlVlvReqSesInfo.FlIvReq;
    output[56] = Ses_CtrlWhlVlvReqSesInfo.FrIvReq;
    output[57] = Ses_CtrlWhlVlvReqSesInfo.RlIvReq;
    output[58] = Ses_CtrlWhlVlvReqSesInfo.RrIvReq;
    output[59] = Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen;
    output[60] = Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen;
    output[61] = Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen;
    output[62] = Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen;
    output[63] = Ses_CtrlMotOrgSetStInfo.MotOrgSetErr;
    output[64] = Ses_CtrlMotOrgSetStInfo.MotOrgSet;
    output[65] = Ses_CtrlMotDqIRefSesInfo.IdRef;
    output[66] = Ses_CtrlMotDqIRefSesInfo.IqRef;
    output[67] = Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
    output[68] = Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ses_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
