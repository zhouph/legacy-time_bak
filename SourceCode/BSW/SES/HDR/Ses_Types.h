/**
 * @defgroup Ses_Types Ses_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ses_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SES_TYPES_H_
#define SES_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
typedef short int           Ses_Frac16;
#define SES_FRAC16(x)       ((Ses_Frac16)((x) < 0.999969482421875 ? ((x) >= -1 ? (x)*0x8000 : 0x8000) : 0x7FFF))

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Acmctl_CtrlMotDqIMeasdInfo_t Ses_CtrlMotDqIMeasdInfo;
    Msp_CtrlMotRotgAgSigInfo_t Ses_CtrlMotRotgAgSigInfo;
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
    Mom_HndlrEcuModeSts_t Ses_CtrlEcuModeSts;
    Eem_SuspcDetnFuncInhibitSesSts_t Ses_CtrlFuncInhibitSesSts;

/* Output Data Element */
    Ses_CtrlNormVlvReqSesInfo_t Ses_CtrlNormVlvReqSesInfo;
    Ses_CtrlWhlVlvReqSesInfo_t Ses_CtrlWhlVlvReqSesInfo;
    Ses_CtrlMotOrgSetStInfo_t Ses_CtrlMotOrgSetStInfo;
    Ses_CtrlMotDqIRefSesInfo_t Ses_CtrlMotDqIRefSesInfo;
    Ses_CtrlPwrPistStBfMotOrgSetInfo_t Ses_CtrlPwrPistStBfMotOrgSetInfo;
}Ses_Ctrl_HdrBusType;

typedef struct
 {
    sint8 dummy;

 }Ses_ConfigType;

//typedef struct
//{
//  sint32 CutVlv1DrvReqOutpI[5];
//  sint32 CutVlv2DrvReqOutpI[5];
//  sint32 CircVlv1DrvReqOutpI[5];
//  sint32 CircVlv2DrvReqOutpI[5];
//  sint32 SimrVlv1DrvReqOutpI[5];
//
//  sint32 IdbVlvDrvReqFlg;
//} Ses_IdbVlvDrvReqInfo_t;
typedef struct
 {

 //   IdbMotRotgAgSigInfo_t IdbMotRotgAgSigInfo;
 //    MotDqImeasdInfo_t MotDqImeasdInfo;
//output
    sint32 IqRefForMotOrgSet;
    sint32 IdRefForMotOrgSet;
    MotOrgSetStInfo_t MotOrgSetStInfo;
 //   SesVlvDrvReqInfo_t SesVlvDrvReqInfo; // check!!

 }Ses_HDRbusType;

 typedef    struct
 {
    /* SETTING PARAMETERS */
    sint16 s16Kp;
    Ses_Frac16 f16Kp;
    sint16 s16KiTs;
    Ses_Frac16 f16KiTs;
    sint16 s16KdFs;
    Ses_Frac16 f16KdFs;

	sint16	s16UpMin;
	sint16	s16UpMax;
	sint16	s16UiMin;
	sint16	s16UiMax;
	sint16	s16UdMin;
	sint16	s16UdMax;
    sint16 s16UoutMin;
    sint16 s16UoutMax;
    /* SETTING PARAMETERS */

    /* NON-SETTING PARAMETERS */
	sint32 s32Up;
	sint32 s32Ui;
	sint32 s32Ud;
    sint16  s16Uout;

	sint32	s32Err;
	sint32	s32ErrOld;
	sint32 s32DeltaErr;
	sint32 s32ErrUi;

	sint32 s32Ucalc;
	sint32	s32Udiff;
    /* NON-SETTING PARAMETERS */
 }Ses_pid_t;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SES_TYPES_H_ */
/** @} */
