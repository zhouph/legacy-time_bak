/**
 * @defgroup Ses_Ctrl Ses_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ses_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SES_CTRL_H_
#define SES_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ses_Types.h"
#include "Ses_Cfg.h"
#include "Ses_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SES_CTRL_MODULE_ID      (0)
 #define SES_CTRL_MAJOR_VERSION  (2)
 #define SES_CTRL_MINOR_VERSION  (0)
 #define SES_CTRL_PATCH_VERSION  (0)
 #define SES_CTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ses_Ctrl_HdrBusType Ses_CtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Ses_CtrlVersionInfo;

/* Input Data Element */
extern Acmctl_CtrlMotDqIMeasdInfo_t Ses_CtrlMotDqIMeasdInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Ses_CtrlMotRotgAgSigInfo;
extern Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
extern Mom_HndlrEcuModeSts_t Ses_CtrlEcuModeSts;
extern Eem_SuspcDetnFuncInhibitSesSts_t Ses_CtrlFuncInhibitSesSts;

/* Output Data Element */
extern Ses_CtrlNormVlvReqSesInfo_t Ses_CtrlNormVlvReqSesInfo;
extern Ses_CtrlWhlVlvReqSesInfo_t Ses_CtrlWhlVlvReqSesInfo;
extern Ses_CtrlMotOrgSetStInfo_t Ses_CtrlMotOrgSetStInfo;
extern Ses_CtrlMotDqIRefSesInfo_t Ses_CtrlMotDqIRefSesInfo;
extern Ses_CtrlPwrPistStBfMotOrgSetInfo_t Ses_CtrlPwrPistStBfMotOrgSetInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ses_Ctrl_Init(void);
extern void Ses_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SES_CTRL_H_ */
/** @} */
