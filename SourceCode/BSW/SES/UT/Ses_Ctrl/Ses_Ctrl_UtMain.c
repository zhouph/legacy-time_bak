#include "unity.h"
#include "unity_fixture.h"
#include "Ses_Ctrl.h"
#include "Ses_Ctrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint32 UtInput_Ses_CtrlMotDqIMeasdInfo_MotIqMeasd[MAX_STEP] = SES_CTRLMOTDQIMEASDINFO_MOTIQMEASD;
const Salsint32 UtInput_Ses_CtrlMotDqIMeasdInfo_MotIdMeasd[MAX_STEP] = SES_CTRLMOTDQIMEASDINFO_MOTIDMEASD;
const Salsint32 UtInput_Ses_CtrlMotRotgAgSigInfo_StkPosnMeasd[MAX_STEP] = SES_CTRLMOTROTGAGSIGINFO_STKPOSNMEASD;
const Salsint32 UtInput_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet[MAX_STEP] = SES_CTRLMOTROTGAGSIGBFMOTORGSETINFO_STKPOSNMEASDBFMOTORGSET;
const Salsint32 UtInput_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet[MAX_STEP] = SES_CTRLMOTROTGAGSIGBFMOTORGSETINFO_MOTMECHANGLESPDBFMOTORGSET;
const Mom_HndlrEcuModeSts_t UtInput_Ses_CtrlEcuModeSts[MAX_STEP] = SES_CTRLECUMODESTS;
const Eem_SuspcDetnFuncInhibitSesSts_t UtInput_Ses_CtrlFuncInhibitSesSts[MAX_STEP] = SES_CTRLFUNCINHIBITSESSTS;

const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_0[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_1[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_2[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_3[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_4[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_0[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_1[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_2[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_3[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_4[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_0[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_1[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_2[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_3[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_4[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_0[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_1[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_2[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_3[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_4[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_0[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_1[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_2[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_3[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_4[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVREQDATA_4;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReq[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVREQ;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReq[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVREQ;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReq[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVREQ;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReq[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVREQ;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReq[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVREQ;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvDataLen[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCUTVLVDATALEN;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvDataLen[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCUTVLVDATALEN;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvDataLen[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_PRIMCIRCVLVDATALEN;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvDataLen[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SECDCIRCVLVDATALEN;
const Saluint8 UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvDataLen[MAX_STEP] = SES_CTRLNORMVLVREQSESINFO_SIMVLVDATALEN;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_0[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_1[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_2[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_3[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_4[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_0[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_1[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_2[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_3[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_4[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_0[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_1[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_2[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_3[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_4[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVREQDATA_4;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_0[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVREQDATA_0;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_1[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVREQDATA_1;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_2[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVREQDATA_2;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_3[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVREQDATA_3;
const Saluint16 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_4[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVREQDATA_4;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReq[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVREQ;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReq[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVREQ;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReq[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVREQ;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReq[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVREQ;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvDataLen[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FLIVDATALEN;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvDataLen[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_FRIVDATALEN;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvDataLen[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RLIVDATALEN;
const Saluint8 UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvDataLen[MAX_STEP] = SES_CTRLWHLVLVREQSESINFO_RRIVDATALEN;
const Saluint8 UtExpected_Ses_CtrlMotOrgSetStInfo_MotOrgSetErr[MAX_STEP] = SES_CTRLMOTORGSETSTINFO_MOTORGSETERR;
const Saluint8 UtExpected_Ses_CtrlMotOrgSetStInfo_MotOrgSet[MAX_STEP] = SES_CTRLMOTORGSETSTINFO_MOTORGSET;
const Salsint32 UtExpected_Ses_CtrlMotDqIRefSesInfo_IdRef[MAX_STEP] = SES_CTRLMOTDQIREFSESINFO_IDREF;
const Salsint32 UtExpected_Ses_CtrlMotDqIRefSesInfo_IqRef[MAX_STEP] = SES_CTRLMOTDQIREFSESINFO_IQREF;
const Salsint32 UtExpected_Ses_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp[MAX_STEP] = SES_CTRLPWRPISTSTBFMOTORGSETINFO_POSIWALLTEMP;
const Salsint32 UtExpected_Ses_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg[MAX_STEP] = SES_CTRLPWRPISTSTBFMOTORGSETINFO_WALLDETECTEDFLG;



TEST_GROUP(Ses_Ctrl);
TEST_SETUP(Ses_Ctrl)
{
    Ses_Ctrl_Init();
}

TEST_TEAR_DOWN(Ses_Ctrl)
{   /* Postcondition */

}

TEST(Ses_Ctrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ses_CtrlMotDqIMeasdInfo.MotIqMeasd = UtInput_Ses_CtrlMotDqIMeasdInfo_MotIqMeasd[i];
        Ses_CtrlMotDqIMeasdInfo.MotIdMeasd = UtInput_Ses_CtrlMotDqIMeasdInfo_MotIdMeasd[i];
        Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd = UtInput_Ses_CtrlMotRotgAgSigInfo_StkPosnMeasd[i];
        Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = UtInput_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet[i];
        Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = UtInput_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet[i];
        Ses_CtrlEcuModeSts = UtInput_Ses_CtrlEcuModeSts[i];
        Ses_CtrlFuncInhibitSesSts = UtInput_Ses_CtrlFuncInhibitSesSts[i];

        Ses_Ctrl();

        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[0], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[1], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[2], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[3], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[4], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[0], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[1], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[2], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[3], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[4], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[0], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[1], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[2], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[3], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[4], UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[0], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[1], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[2], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[3], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[4], UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[0], UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[1], UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[2], UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[3], UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[4], UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq, UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq, UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq, UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq, UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvReq, UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen, UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen, UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen, UtExpected_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen, UtExpected_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen, UtExpected_Ses_CtrlNormVlvReqSesInfo_SimVlvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[0], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[1], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[2], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[3], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[4], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[0], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[1], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[2], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[3], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[4], UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[0], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[1], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[2], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[3], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[4], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[0], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_0[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[1], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_1[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[2], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_2[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[3], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_3[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[4], UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData_4[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvReq, UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvReq, UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvReq, UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvReq, UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvReq[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen, UtExpected_Ses_CtrlWhlVlvReqSesInfo_FlIvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen, UtExpected_Ses_CtrlWhlVlvReqSesInfo_FrIvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen, UtExpected_Ses_CtrlWhlVlvReqSesInfo_RlIvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen, UtExpected_Ses_CtrlWhlVlvReqSesInfo_RrIvDataLen[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlMotOrgSetStInfo.MotOrgSetErr, UtExpected_Ses_CtrlMotOrgSetStInfo_MotOrgSetErr[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlMotOrgSetStInfo.MotOrgSet, UtExpected_Ses_CtrlMotOrgSetStInfo_MotOrgSet[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlMotDqIRefSesInfo.IdRef, UtExpected_Ses_CtrlMotDqIRefSesInfo_IdRef[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlMotDqIRefSesInfo.IqRef, UtExpected_Ses_CtrlMotDqIRefSesInfo_IqRef[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp, UtExpected_Ses_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp[i]);
        TEST_ASSERT_EQUAL(Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg, UtExpected_Ses_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg[i]);
    }
}

TEST_GROUP_RUNNER(Ses_Ctrl)
{
    RUN_TEST_CASE(Ses_Ctrl, All);
}
