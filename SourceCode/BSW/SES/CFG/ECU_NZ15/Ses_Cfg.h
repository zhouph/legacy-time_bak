/**
 * @defgroup Ses_Cfg Ses_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ses_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SES_CFG_H_
#define SES_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ses_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

#define     Ses_S32_MTRPOSI_RESOL                (1126)        /* 2048pulse / 1.819mm  = 1126*/
#define     Ses_S32_MTRPOSI_0MM                  (0  )
#define     Ses_S32_MTRPOSI_0_2MM                (225)
#define     Ses_S32_MTRPOSI_0_25MM               (281)
#define     Ses_S32_MTRPOSI_0_5MM                (563)
#define     Ses_S32_MTRPOSI_1MM                  (1126)
#define     Ses_S32_MTRPOSI_1_5MM                (1689)
#define     Ses_S32_MTRPOSI_2MM                  (2252)
#define     Ses_S32_MTRPOSI_3MM                  (3378)
#define     Ses_S32_MTRPOSI_4MM                  (4504)
#define     Ses_S32_MTRPOSI_5MM                  (5630)
#define     Ses_S32_MTRPOSI_6MM                  (6756)
#define     Ses_S32_MTRPOSI_7MM                  (7882)
#define     Ses_S32_MTRPOSI_8MM                  (9008)
#define     Ses_S32_MTRPOSI_9MM                  (10134)
#define     Ses_S32_MTRPOSI_10MM                 (11260)
#define     Ses_S32_MTRPOSI_11MM                 (12386)
#define     Ses_S32_MTRPOSI_12MM                 (13512)
#define     Ses_S32_MTRPOSI_13MM                 (14638)
#define     Ses_S32_MTRPOSI_14MM                 (15764)
#define     Ses_S32_MTRPOSI_15MM                 (16890)
#define     Ses_S32_MTRPOSI_16MM                 (18016)
#define     Ses_S32_MTRPOSI_17MM                 (19142)
#define     Ses_S32_MTRPOSI_18MM                 (20268)
#define     Ses_S32_MTRPOSI_19MM                 (21394)
#define     Ses_S32_MTRPOSI_20MM                 (22520)
#define     Ses_S32_MTRPOSI_21MM                 (23646)
#define     Ses_S32_MTRPOSI_22MM                 (24772)
#define     Ses_S32_MTRPOSI_23MM                 (25898)
#define     Ses_S32_MTRPOSI_24MM                 (27024)
#define     Ses_S32_MTRPOSI_25MM                 (28150)
#define     Ses_S32_MTRPOSI_26MM                 (29276)
#define     Ses_S32_MTRPOSI_27MM                 (30402)
#define     Ses_S32_MTRPOSI_28MM                 (31528)
#define     Ses_S32_MTRPOSI_29MM                 (32654)
#define     Ses_S32_MTRPOSI_30MM                 (33780)
#define     Ses_S32_MTRPOSI_31MM                 (34906)
#define     Ses_S32_MTRPOSI_32MM                 (36032)
#define     Ses_S32_MTRPOSI_33MM                 (37158)
#define     Ses_S32_MTRPOSI_34MM                 (38284)
#define     Ses_S32_MTRPOSI_35MM                 (39410)
#define     Ses_S32_MTRPOSI_36MM                 (40536)
#define     Ses_S32_MTRPOSI_37MM                 (41662)
#define     Ses_S32_MTRPOSI_38MM                 (42788)
#define     Ses_S32_MTRPOSI_39MM                 (43914)
#define     Ses_S32_MTRPOSI_40MM                 (45040)
#define     Ses_S32_MTRPOSI_41MM                 (46166)
#define     Ses_S32_MTRPOSI_42MM                 (47292)
#define     Ses_S32_MTRPOSI_43MM                 (48418)
#define     Ses_S32_MTRPOSI_44MM                 (49544)
#define     Ses_S32_MTRPOSI_45MM                 (50670)
#define     Ses_S32_MTRPOSI_46MM                 (51796)
#define     Ses_S32_MTRPOSI_47MM                 (52922)
#define     Ses_S32_MTRPOSI_48MM                 (54048)
#define     Ses_S32_MTRPOSI_49MM                 (55174)
#define     Ses_S32_MTRPOSI_50MM                 (56300)
#define     Ses_S32_MTRPOSI_51MM                 (57426)
#define     Ses_S32_MTRPOSI_52MM                 (58552)
#define     Ses_S32_MTRPOSI_53MM                 (59678)
#define     Ses_S32_MTRPOSI_54MM                 (60804)
#define     Ses_S32_MTRPOSI_55MM                 (61930)


/**********************************************************/
/* MOTOR CURRENT (RESOLUTION 175A)                        */
/**********************************************************/

#define     Ses_S16_MTRCURR_RESOL                100

#define     Ses_S16_MTRCURR_0A                   (0    )
#define     Ses_S16_MTRCURR_1A                   (100  )
#define     Ses_S16_MTRCURR_2A                   (200  )
#define     Ses_S16_MTRCURR_3A                   (300  )
#define     Ses_S16_MTRCURR_4A                   (400  )
#define     Ses_S16_MTRCURR_5A                   (500  )
#define     Ses_S16_MTRCURR_6A                   (600  )
#define     Ses_S16_MTRCURR_7A                   (700  )
#define     Ses_S16_MTRCURR_8A                   (800  )
#define     Ses_S16_MTRCURR_9A                   (900  )
#define     Ses_S16_MTRCURR_10A                  (1000 )
#define     Ses_S16_MTRCURR_11A                  (1100 )
#define     Ses_S16_MTRCURR_12A                  (1200 )
#define     Ses_S16_MTRCURR_13A                  (1300 )
#define     Ses_S16_MTRCURR_14A                  (1400 )
#define     Ses_S16_MTRCURR_15A                  (1500 )
#define     Ses_S16_MTRCURR_16A                  (1600 )
#define     Ses_S16_MTRCURR_17A                  (1700 )
#define     Ses_S16_MTRCURR_18A                  (1800 )
#define     Ses_S16_MTRCURR_19A                  (1900 )
#define     Ses_S16_MTRCURR_20A                  (2000 )
#define     Ses_S16_MTRCURR_21A                  (2100 )
#define     Ses_S16_MTRCURR_22A                  (2200 )
#define     Ses_S16_MTRCURR_23A                  (2300 )
#define     Ses_S16_MTRCURR_24A                  (2400 )
#define     Ses_S16_MTRCURR_25A                  (2500 )
#define     Ses_S16_MTRCURR_26A                  (2600 )
#define     Ses_S16_MTRCURR_27A                  (2700 )
#define     Ses_S16_MTRCURR_28A                  (2800 )
#define     Ses_S16_MTRCURR_29A                  (2900 )
#define     Ses_S16_MTRCURR_30A                  (3000 )
#define     Ses_S16_MTRCURR_31A                  (3100 )
#define     Ses_S16_MTRCURR_32A                  (3200 )
#define     Ses_S16_MTRCURR_33A                  (3300 )
#define     Ses_S16_MTRCURR_34A                  (3400 )
#define     Ses_S16_MTRCURR_35A                  (3500 )
#define     Ses_S16_MTRCURR_36A                  (3600 )
#define     Ses_S16_MTRCURR_37A                  (3700 )
#define     Ses_S16_MTRCURR_38A                  (3800 )
#define     Ses_S16_MTRCURR_39A                  (3900 )
#define     Ses_S16_MTRCURR_40A                  (4000 )
#define     Ses_S16_MTRCURR_41A                  (4100 )
#define     Ses_S16_MTRCURR_42A                  (4200 )
#define     Ses_S16_MTRCURR_43A                  (4300 )
#define     Ses_S16_MTRCURR_44A                  (4400 )
#define     Ses_S16_MTRCURR_45A                  (4500 )
#define     Ses_S16_MTRCURR_46A                  (4600 )
#define     Ses_S16_MTRCURR_47A                  (4700 )
#define     Ses_S16_MTRCURR_48A                  (4800 )
#define     Ses_S16_MTRCURR_49A                  (4900 )
#define     Ses_S16_MTRCURR_50A                  (5000 )
#define     Ses_S16_MTRCURR_51A                  (5100 )
#define     Ses_S16_MTRCURR_52A                  (5200 )
#define     Ses_S16_MTRCURR_53A                  (5300 )
#define     Ses_S16_MTRCURR_54A                  (5400 )
#define     Ses_S16_MTRCURR_55A                  (5500 )
#define     Ses_S16_MTRCURR_56A                  (5600 )
#define     Ses_S16_MTRCURR_57A                  (5700 )
#define     Ses_S16_MTRCURR_58A                  (5800 )
#define     Ses_S16_MTRCURR_59A                  (5900 )
#define     Ses_S16_MTRCURR_60A                  (6000 )
#define     Ses_S16_MTRCURR_61A                  (6100 )
#define     Ses_S16_MTRCURR_62A                  (6200 )
#define     Ses_S16_MTRCURR_63A                  (6300 )
#define     Ses_S16_MTRCURR_64A                  (6400 )
#define     Ses_S16_MTRCURR_65A                  (6500 )
#define     Ses_S16_MTRCURR_66A                  (6600 )
#define     Ses_S16_MTRCURR_67A                  (6700 )
#define     Ses_S16_MTRCURR_68A                  (6800 )
#define     Ses_S16_MTRCURR_69A                  (6900 )
#define     Ses_S16_MTRCURR_70A                  (7000 )
#define     Ses_S16_MTRCURR_71A                  (7100 )
#define     Ses_S16_MTRCURR_72A                  (7200 )
#define     Ses_S16_MTRCURR_73A                  (7300 )
#define     Ses_S16_MTRCURR_74A                  (7400 )
#define     Ses_S16_MTRCURR_75A                  (7500 )
#define     Ses_S16_MTRCURR_76A                  (7600 )
#define     Ses_S16_MTRCURR_77A                  (7700 )
#define     Ses_S16_MTRCURR_78A                  (7800 )
#define     Ses_S16_MTRCURR_79A                  (7900 )
#define     Ses_S16_MTRCURR_80A                  (8000 )
#define     Ses_S16_MTRCURR_81A                  (8100 )
#define     Ses_S16_MTRCURR_82A                  (8200 )
#define     Ses_S16_MTRCURR_83A                  (8300 )
#define     Ses_S16_MTRCURR_84A                  (8400 )
#define     Ses_S16_MTRCURR_85A                  (8500 )
#define     Ses_S16_MTRCURR_86A                  (8600 )
#define     Ses_S16_MTRCURR_87A                  (8700 )
#define     Ses_S16_MTRCURR_88A                  (8800 )
#define     Ses_S16_MTRCURR_89A                  (8900 )
#define     Ses_S16_MTRCURR_90A                  (9000 )
#define     Ses_S16_MTRCURR_91A                  (9100 )
#define     Ses_S16_MTRCURR_92A                  (9200 )
#define     Ses_S16_MTRCURR_93A                  (9300 )
#define     Ses_S16_MTRCURR_94A                  (9400 )
#define     Ses_S16_MTRCURR_95A                  (9500 )
#define     Ses_S16_MTRCURR_96A                  (9600 )
#define     Ses_S16_MTRCURR_97A                  (9700 )
#define     Ses_S16_MTRCURR_98A                  (9800 )
#define     Ses_S16_MTRCURR_99A                  (9900 )
#define     Ses_S16_MTRCURR_100A                 (10000)


#define     Ses_U8_T_0_MS              0
#define     Ses_U8_T_5_MS              1
#define     Ses_U8_T_10_MS             2
#define     Ses_U8_T_15_MS             3
#define     Ses_U8_T_20_MS             4
#define     Ses_U8_T_25_MS             5
#define     Ses_U8_T_30_MS             6
#define     Ses_U8_T_35_MS             7
#define     Ses_U8_T_40_MS             8
#define     Ses_U8_T_45_MS             9
#define     Ses_U8_T_50_MS             10
#define     Ses_U8_T_55_MS             11
#define     Ses_U8_T_60_MS             12
#define     Ses_U8_T_65_MS             13
#define     Ses_U8_T_70_MS             14
#define     Ses_U8_T_75_MS             15
#define     Ses_U8_T_80_MS             16
#define     Ses_U8_T_85_MS             17
#define     Ses_U8_T_90_MS             18
#define     Ses_U8_T_95_MS             19
#define     Ses_U8_T_100_MS            20
#define     Ses_U8_T_110_MS            22
#define     Ses_U8_T_120_MS            24
#define     Ses_U8_T_130_MS            26
#define     Ses_U8_T_140_MS            28
#define     Ses_U8_T_150_MS            30
#define     Ses_U8_T_160_MS            32
#define     Ses_U8_T_170_MS            34
#define     Ses_U8_T_180_MS            36
#define     Ses_U8_T_190_MS            38
#define     Ses_U8_T_200_MS            40
#define     Ses_U8_T_250_MS            50
#define     Ses_U8_T_300_MS            60
#define     Ses_U8_T_350_MS            70
#define     Ses_U8_T_400_MS            80
#define     Ses_U8_T_450_MS            90
#define     Ses_U8_T_500_MS            100
#define     Ses_U8_T_600_MS            120
#define     Ses_U8_T_700_MS            140
#define     Ses_U8_T_800_MS            160
#define     Ses_U8_T_900_MS            180
#define     Ses_U8_T_1000_MS           200
#define     Ses_U8_T_1200_MS           240

#define     Ses_U16_T_1_S               200
#define     Ses_U16_T_2_S               400
#define     Ses_U16_T_3_S               600
#define     Ses_U16_T_4_S               800
#define     Ses_U16_T_5_S               1000
#define     Ses_U16_T_6_S               1200
#define     Ses_U16_T_7_S               1400
#define     Ses_U16_T_8_S               1600
#define     Ses_U16_T_9_S               1800
#define     Ses_U16_T_10_S              2000

#define     Ses_S16_MAX                 32767
#define     Ses_S16_MIN                -32767

#define     Ses_U8_FULL_CYCLETIME       5
#define     Ses_S16_CURRENT_1700_MA               1700
#define     Ses_S32_S32MAX_DIV_4    (536870911)
#define     Ses_S32_S32MIN_DIV_4    (-536870912)
#define     Ses_F16_1               (32768)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SES_CFG_H_ */
/** @} */
