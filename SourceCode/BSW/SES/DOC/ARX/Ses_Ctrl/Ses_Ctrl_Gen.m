Ses_CtrlMotDqIMeasdInfo = Simulink.Bus;
DeList={
    'MotIqMeasd'
    'MotIdMeasd'
    };
Ses_CtrlMotDqIMeasdInfo = CreateBus(Ses_CtrlMotDqIMeasdInfo, DeList);
clear DeList;

Ses_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    };
Ses_CtrlMotRotgAgSigInfo = CreateBus(Ses_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Ses_CtrlMotRotgAgSigBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'StkPosnMeasdBfMotOrgSet'
    'MotMechAngleSpdBfMotOrgSet'
    };
Ses_CtrlMotRotgAgSigBfMotOrgSetInfo = CreateBus(Ses_CtrlMotRotgAgSigBfMotOrgSetInfo, DeList);
clear DeList;

Ses_CtrlEcuModeSts = Simulink.Bus;
DeList={'Ses_CtrlEcuModeSts'};
Ses_CtrlEcuModeSts = CreateBus(Ses_CtrlEcuModeSts, DeList);
clear DeList;

Ses_CtrlFuncInhibitSesSts = Simulink.Bus;
DeList={'Ses_CtrlFuncInhibitSesSts'};
Ses_CtrlFuncInhibitSesSts = CreateBus(Ses_CtrlFuncInhibitSesSts, DeList);
clear DeList;

Ses_CtrlNormVlvReqSesInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    };
Ses_CtrlNormVlvReqSesInfo = CreateBus(Ses_CtrlNormVlvReqSesInfo, DeList);
clear DeList;

Ses_CtrlWhlVlvReqSesInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    };
Ses_CtrlWhlVlvReqSesInfo = CreateBus(Ses_CtrlWhlVlvReqSesInfo, DeList);
clear DeList;

Ses_CtrlMotOrgSetStInfo = Simulink.Bus;
DeList={
    'MotOrgSetErr'
    'MotOrgSet'
    };
Ses_CtrlMotOrgSetStInfo = CreateBus(Ses_CtrlMotOrgSetStInfo, DeList);
clear DeList;

Ses_CtrlMotDqIRefSesInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Ses_CtrlMotDqIRefSesInfo = CreateBus(Ses_CtrlMotDqIRefSesInfo, DeList);
clear DeList;

Ses_CtrlPwrPistStBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'PosiWallTemp'
    'WallDetectedFlg'
    };
Ses_CtrlPwrPistStBfMotOrgSetInfo = CreateBus(Ses_CtrlPwrPistStBfMotOrgSetInfo, DeList);
clear DeList;

