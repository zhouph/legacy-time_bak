# \file
#
# \brief Ses
#
# This file contains the implementation of the SWC
# module Ses.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Ses_CORE_PATH     := $(MANDO_BSW_ROOT)\Ses
Ses_CAL_PATH      := $(Ses_CORE_PATH)\CAL\$(Ses_VARIANT)
Ses_SRC_PATH      := $(Ses_CORE_PATH)\SRC
Ses_CFG_PATH      := $(Ses_CORE_PATH)\CFG\$(Ses_VARIANT)
Ses_HDR_PATH      := $(Ses_CORE_PATH)\HDR
Ses_IFA_PATH      := $(Ses_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Ses_CMN_PATH      := $(Ses_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Ses_UT_PATH		:= $(Ses_CORE_PATH)\UT
	Ses_UNITY_PATH	:= $(Ses_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Ses_UT_PATH)
	CC_INCLUDE_PATH		+= $(Ses_UNITY_PATH)
	Ses_Ctrl_PATH 	:= Ses_UT_PATH\Ses_Ctrl
endif
CC_INCLUDE_PATH    += $(Ses_CAL_PATH)
CC_INCLUDE_PATH    += $(Ses_SRC_PATH)
CC_INCLUDE_PATH    += $(Ses_CFG_PATH)
CC_INCLUDE_PATH    += $(Ses_HDR_PATH)
CC_INCLUDE_PATH    += $(Ses_IFA_PATH)
CC_INCLUDE_PATH    += $(Ses_CMN_PATH)

