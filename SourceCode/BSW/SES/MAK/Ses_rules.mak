# \file
#
# \brief Ses
#
# This file contains the implementation of the SWC
# module Ses.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Ses_src

Ses_src_FILES        += $(Ses_SRC_PATH)\Ses_Ctrl.c
Ses_src_FILES        += $(Ses_IFA_PATH)\Ses_Ctrl_Ifa.c
Ses_src_FILES        += $(Ses_CFG_PATH)\Ses_Cfg.c
Ses_src_FILES        += $(Ses_CAL_PATH)\Ses_Cal.c
Ses_src_FILES        += $(Ses_SRC_PATH)\Ses_OrgSetCtrl.c
ifeq ($(ICE_COMPILE),true)
Ses_src_FILES        += $(Ses_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Ses_src_FILES        += $(Ses_UNITY_PATH)\unity.c
	Ses_src_FILES        += $(Ses_UNITY_PATH)\unity_fixture.c	
	Ses_src_FILES        += $(Ses_UT_PATH)\main.c
	Ses_src_FILES        += $(Ses_UT_PATH)\Ses_Ctrl\Ses_Ctrl_UtMain.c
endif