/**
 * @defgroup Ses_Ctrl Ses_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ses_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ses_Ctrl.h"
#include "Ses_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "Ses_OrgSetCtrl.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SES_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Ses_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SES_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Ses_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SES_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ses_Ctrl_HdrBusType Ses_CtrlBus;

/* Version Info */
const SwcVersionInfo_t Ses_CtrlVersionInfo = 
{   
    SES_CTRL_MODULE_ID,           /* Ses_CtrlVersionInfo.ModuleId */
    SES_CTRL_MAJOR_VERSION,       /* Ses_CtrlVersionInfo.MajorVer */
    SES_CTRL_MINOR_VERSION,       /* Ses_CtrlVersionInfo.MinorVer */
    SES_CTRL_PATCH_VERSION,       /* Ses_CtrlVersionInfo.PatchVer */
    SES_CTRL_BRANCH_VERSION       /* Ses_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Acmctl_CtrlMotDqIMeasdInfo_t Ses_CtrlMotDqIMeasdInfo;
Msp_CtrlMotRotgAgSigInfo_t Ses_CtrlMotRotgAgSigInfo;
Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Ses_CtrlMotRotgAgSigBfMotOrgSetInfo;
Mom_HndlrEcuModeSts_t Ses_CtrlEcuModeSts;
Eem_SuspcDetnFuncInhibitSesSts_t Ses_CtrlFuncInhibitSesSts;

/* Output Data Element */
Ses_CtrlNormVlvReqSesInfo_t Ses_CtrlNormVlvReqSesInfo;
Ses_CtrlWhlVlvReqSesInfo_t Ses_CtrlWhlVlvReqSesInfo;
Ses_CtrlMotOrgSetStInfo_t Ses_CtrlMotOrgSetStInfo;
Ses_CtrlMotDqIRefSesInfo_t Ses_CtrlMotDqIRefSesInfo;
Ses_CtrlPwrPistStBfMotOrgSetInfo_t Ses_CtrlPwrPistStBfMotOrgSetInfo;

uint32 Ses_Ctrl_Timer_Start;
uint32 Ses_Ctrl_Timer_Elapsed;

#define SES_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SES_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
#define SES_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SES_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_CTRL_START_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/** Variable Section (32BIT)**/


#define SES_CTRL_STOP_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SES_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SES_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SES_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Ses_MemMap.h"
#define SES_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SES_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Ses_MemMap.h"
#define SES_CTRL_START_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/** Variable Section (32BIT)**/


#define SES_CTRL_STOP_SEC_VAR_32BIT
#include "Ses_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SES_CTRL_START_SEC_CODE
#include "Ses_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ses_Ctrl_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Ses_CtrlBus.Ses_CtrlMotDqIMeasdInfo.MotIqMeasd = 0;
    Ses_CtrlBus.Ses_CtrlMotDqIMeasdInfo.MotIdMeasd = 0;
    Ses_CtrlBus.Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd = 0;
    Ses_CtrlBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = 0;
    Ses_CtrlBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = 0;
    Ses_CtrlBus.Ses_CtrlEcuModeSts = 0;
    Ses_CtrlBus.Ses_CtrlFuncInhibitSesSts = 0;
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[i] = 0;   
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SimVlvReq = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen = 0;
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[i] = 0;   
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FlIvReq = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FrIvReq = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.RlIvReq = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.RrIvReq = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen = 0;
    Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSetErr = 0;
    Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet = 0;
    Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef = 0;
    Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef = 0;
    Ses_CtrlBus.Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp = 0;
    Ses_CtrlBus.Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg = 0;
	Ses_vInitCallOrgSetCtrl();
}

void Ses_Ctrl(void)
{
    uint16 i;
    
    Ses_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ses_Ctrl_Read_Ses_CtrlMotDqIMeasdInfo(&Ses_CtrlBus.Ses_CtrlMotDqIMeasdInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotDqIMeasdInfo 
     : Ses_CtrlMotDqIMeasdInfo.MotIqMeasd;
     : Ses_CtrlMotDqIMeasdInfo.MotIdMeasd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigInfo_StkPosnMeasd(&Ses_CtrlBus.Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd);

    Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo(&Ses_CtrlBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotRotgAgSigBfMotOrgSetInfo 
     : Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
     : Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
     =============================================================================*/
    
    Ses_Ctrl_Read_Ses_CtrlEcuModeSts(&Ses_CtrlBus.Ses_CtrlEcuModeSts);
    Ses_Ctrl_Read_Ses_CtrlFuncInhibitSesSts(&Ses_CtrlBus.Ses_CtrlFuncInhibitSesSts);

    /* Process */
    Ses_vCallOrgSetCtrl();

    /* Output */
    Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo(&Ses_CtrlBus.Ses_CtrlNormVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlNormVlvReqSesInfo 
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvReqData;
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvReq;
     : Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen;
     : Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen;
     =============================================================================*/
    
    Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo(&Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlWhlVlvReqSesInfo 
     : Ses_CtrlWhlVlvReqSesInfo.FlIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvReqData;
     : Ses_CtrlWhlVlvReqSesInfo.FlIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvReq;
     : Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen;
     : Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen;
     =============================================================================*/
    
    Ses_Ctrl_Write_Ses_CtrlMotOrgSetStInfo(&Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotOrgSetStInfo 
     : Ses_CtrlMotOrgSetStInfo.MotOrgSetErr;
     : Ses_CtrlMotOrgSetStInfo.MotOrgSet;
     =============================================================================*/
    
    Ses_Ctrl_Write_Ses_CtrlMotDqIRefSesInfo(&Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlMotDqIRefSesInfo 
     : Ses_CtrlMotDqIRefSesInfo.IdRef;
     : Ses_CtrlMotDqIRefSesInfo.IqRef;
     =============================================================================*/
    
    Ses_Ctrl_Write_Ses_CtrlPwrPistStBfMotOrgSetInfo(&Ses_CtrlBus.Ses_CtrlPwrPistStBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Ses_CtrlPwrPistStBfMotOrgSetInfo 
     : Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
     : Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
     =============================================================================*/
    

    Ses_Ctrl_Timer_Elapsed = STM0_TIM0.U - Ses_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SES_CTRL_STOP_SEC_CODE
#include "Ses_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
