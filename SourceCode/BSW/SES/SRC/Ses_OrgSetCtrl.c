/*TODO
 1. ASW parameter  def include
 2. Motor posi if--> 구성되기 전에는 자체적으로 posi 계산 할 것
 3. 공유 함수 및 type - ex pid ctrl
 4.library file include path??
 5.Get/Set interface
 */

#include "Ses_OrgSetCtrl.h"
#include "Mps_TLE5012_Hndlr_1ms.h"

#define SES_U16_SKIP_ORGSETCTRL					(0)

#define SES_S16_POSI_WALL						(-Ses_S32_MTRPOSI_2MM)

#define SES_S16_ORGSETSUS_DECISION_POSI_THR		(Ses_S32_MTRPOSI_0_5MM)

#define SES_S16_IQREF_MAX						(Ses_S16_MTRCURR_20A)
#define SES_S16_IQREF_MIN						(-Ses_S16_MTRCURR_20A)

#define	SES_S16_QCURR_STB_DECISION_THR			(Ses_S16_MTRCURR_1A)

#define SES_S16_ORGSETFLG_SETTING_IQ_MAX		(50	)
#define SES_S16_ORGSETFLG_SETTING_IQ_MIN		(-50)
#define SES_S16_ORGSETFLG_SETTING_DPOSI_MAX		(50)
#define SES_S16_ORGSETFLG_SETTING_DPOSI_MIN		(-50)
#define SES_S16_ORGSETFLG_SETTING_CHK_TIME		(Ses_U8_T_20_MS)
#define SES_S16_IQREF_INC_MAX					(10)

#define	SES_S16_TGT_SPD_MIN						(-300)
#define	SES_S16_DELTA_TGT_SPD					(-5)

#define	SES_S16_ORGSETSUS_CNT_MAX				(50)

#define SES_S16_DELTA_POSI_MAX					(1)
#define SES_S16_DELTA_POSI_MIN					(-5)
#define SES_S16_POSI_STB_DECISION_CNT_MAX		(5)
#define SES_S16_TGTPOSI_INC_ABS	(10)
#define SES_S16_TGTPOSI_DEC_ABS	(10)

#define	SES_U16_ORGSET_CTRL_STATE_SPD			(0)
#define	SES_U16_ORGSET_CTRL_STATE_TQ			(1)
#define	SES_U16_ORGSET_CTRL_STATE_POSI			(2)
#define SES_S16_DELTA_IQREF	(50)

#define SES_U16_POSIARRAY_LENGTH				(10)
#define SES_U16_IQARRAY_LENGTH					(10)
#define SES_U16_WRPMARRAY_LENGTH				(10)

#define SES_S16_TQ_2_POSI_CTRL_CHG_SPD_THR		(50)

uint16 lsesu16OrgSetFlg;
uint16 lsesu16OrgSetSusFlg;
uint16 lsesu16WallDetectedFlg;
uint16 lsesu16OrgSetFailFlg;
uint16 lsesu16OrgSetCtrlState;
uint16 lsesu16OrgSetCtrlStateOld;
uint16 lsesu16PosiStbFlg;
uint16 lsesu16PosiStbCnt;
sint16 lsess16IqMovAvg;
uint16 lsesu16WallDetectedSusFlg;

sint16 lsess16PosiStbCnt;
sint16 lsess16OrgSetCheckCnt;
sint16 lsess16WrpmMovAvg;
sint16 lsess16MtrStartModCnt;
sint16 lsess16OrgSusSetCnt;
sint16 lsess16WrpmArray[SES_U16_WRPMARRAY_LENGTH];

sint32 lsess32IqArray[SES_U16_IQARRAY_LENGTH];
sint32 lsess32PosiArray[SES_U16_POSIARRAY_LENGTH];
sint32 lsess32DeltaPosiOrgSet;
sint32 lsess32PosiOld;
sint32 lsess32Posi;
sint32 lsess16IqRefOrgSet;
sint32 lsess32TgtPosiOrgSet;
sint32 lsess32TgtSpdOrgSet;
//sint32 lsess32PosiOrgSet;

Ses_pid_t lsesstPidPosiCtrlOrgSet;
Ses_pid_t lsesstPidSpdCtrlOrgSet;




sint16 lamtru1ValveDriveReq;
sint16 CircVlv1lau16CurrentOutput[Ses_U8_FULL_CYCLETIME];
sint16 CircVlv2lau16CurrentOutput[Ses_U8_FULL_CYCLETIME];
sint16 FlVlvlau16CurrentOutput[Ses_U8_FULL_CYCLETIME];
sint16 FrVlvlau16CurrentOutput[Ses_U8_FULL_CYCLETIME];

typedef struct
{
	sint32 lsess32Posi;
	sint16 lsess16Iq;
	sint16 lsess16WrpmMech;

}GetMotOrgSet_t;

GetMotOrgSet_t GetMotOrgSet;

void Ses_vUpdateArray(sint32 Sig, sint32* pArray, sint16 ArrayLength);
uint16 Ses_u16DetStabililty(sint32* pArray, sint16 ArrayLength, sint16 s16DeltaSigMin, sint16 s16DeltaSigMax);
sint16 Ses_s16CalcMovAvg(sint32* pArray, sint16 ArrayLength);
static void Ses_vCallOrgSetValveCtrl(uint16 u16OrgSetFlg);
static uint16 Ses_u16CurrentBreaker2(sint16 s16IqRef);
static void Ses_vInpIfForMotOrgSetCtrl(void);
static void Ses_vOutpIfForMotOrgSetCtrl(void);
static void Ses_vCallMotIRefCalcnForOrgSet(void);
static sint16 Ses_s16PIDController(sint16 s16Xref, sint16 s16X, Ses_pid_t *pParam);
static void Ses_vInitPid(Ses_pid_t *pParam);

void Ses_vCallOrgSetCtrl(void)
{
	Ses_vInpIfForMotOrgSetCtrl();

#if(SES_U16_SKIP_ORGSETCTRL == 1)
	lsesu16OrgSetFlg = 1;
	GetMotOrgSet.lsess32Posi = 0;
	lsess16IqRefOrgSet = 0;
#else

	if(lsesu16OrgSetFlg==0)
	{
		if(MpsD1_Compen_SetFag_at1ms == 1)
		{
			Ses_vCallMotIRefCalcnForOrgSet();
			/*Ses_vCallOrgSetValveCtrl(lsesu16OrgSetFlg);*/
		}
	}
#endif
	Ses_vOutpIfForMotOrgSetCtrl();
}

void Ses_vInitCallOrgSetCtrl(void)
{
	lsesstPidPosiCtrlOrgSet.s16Kp 			= 1;
	lsesstPidPosiCtrlOrgSet.f16Kp 			= 0;
	lsesstPidPosiCtrlOrgSet.s16KiTs 		= 0;
	lsesstPidPosiCtrlOrgSet.f16KiTs 		= SES_FRAC16(0.02);
	lsesstPidPosiCtrlOrgSet.s16KdFs			= 0;
	lsesstPidPosiCtrlOrgSet.f16KdFs			= 0;
	lsesstPidPosiCtrlOrgSet.s16UpMax 		= Ses_S16_MAX;
	lsesstPidPosiCtrlOrgSet.s16UpMin 		= Ses_S16_MIN;
	lsesstPidPosiCtrlOrgSet.s16UiMax 		= Ses_S16_MAX;
	lsesstPidPosiCtrlOrgSet.s16UiMin 		= Ses_S16_MIN;
	lsesstPidPosiCtrlOrgSet.s16UdMax 		= Ses_S16_MAX;
	lsesstPidPosiCtrlOrgSet.s16UdMin 		= Ses_S16_MIN;
	lsesstPidPosiCtrlOrgSet.s16UoutMax 		= SES_S16_IQREF_MAX;
	lsesstPidPosiCtrlOrgSet.s16UoutMin 		= SES_S16_IQREF_MIN;

	lsesstPidSpdCtrlOrgSet.s16Kp 		= 1;
	lsesstPidSpdCtrlOrgSet.f16Kp 		= 0;
	lsesstPidSpdCtrlOrgSet.s16KiTs 		= 0;
	lsesstPidSpdCtrlOrgSet.f16KiTs 		= SES_FRAC16(0.02);
	lsesstPidSpdCtrlOrgSet.s16KdFs		= 0;
	lsesstPidSpdCtrlOrgSet.f16KdFs		= 0;
	lsesstPidSpdCtrlOrgSet.s16UpMax 	= Ses_S16_MAX;
	lsesstPidSpdCtrlOrgSet.s16UpMin 	= Ses_S16_MIN;
	lsesstPidSpdCtrlOrgSet.s16UiMax 	= Ses_S16_MAX;
	lsesstPidSpdCtrlOrgSet.s16UiMin 	= Ses_S16_MIN;
	lsesstPidSpdCtrlOrgSet.s16UdMax 	= Ses_S16_MAX;
	lsesstPidSpdCtrlOrgSet.s16UdMin 	= Ses_S16_MIN;
	lsesstPidSpdCtrlOrgSet.s16UoutMax 	= SES_S16_IQREF_MAX;
	lsesstPidSpdCtrlOrgSet.s16UoutMin 	= SES_S16_IQREF_MIN;
}

static void Ses_vInpIfForMotOrgSetCtrl(void)
{
	lsess32PosiOld = GetMotOrgSet.lsess32Posi;
//	GetMotOrgSet.lsess32Posi = Ses_CtrlBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
//	GetMotOrgSet.lsess16WrpmMech = Ses_CtrlBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
	GetMotOrgSet.lsess32Posi = Ses_CtrlBus.Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd;
	GetMotOrgSet.lsess16WrpmMech = Ses_CtrlBus.Ses_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild;
	GetMotOrgSet.lsess16Iq = Ses_CtrlBus.Ses_CtrlMotDqIMeasdInfo.MotIqMeasd;

}

static void Ses_vOutpIfForMotOrgSetCtrl(void)
{
	uint8 CurrTim;

	Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef = 0;
	Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef = lsess16IqRefOrgSet;
	Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet = lsesu16OrgSetFlg;
	Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSetErr = lsesu16OrgSetFailFlg;
//	Ses_CtrlBus.Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = lsess32PosiOrgSet;
	Ses_CtrlBus.Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp = SES_S16_POSI_WALL;
	Ses_CtrlBus.Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg = lsesu16WallDetectedFlg;

	for(CurrTim=0; CurrTim<Ses_U8_FULL_CYCLETIME; CurrTim++)
	{
		Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[CurrTim] = FlVlvlau16CurrentOutput[CurrTim];
		Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[CurrTim] = FrVlvlau16CurrentOutput[CurrTim];

	}
	Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen = Ses_U8_FULL_CYCLETIME;
	Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen = Ses_U8_FULL_CYCLETIME;
	Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FlIvReq = lamtru1ValveDriveReq;
	Ses_CtrlBus.Ses_CtrlWhlVlvReqSesInfo.FrIvReq = lamtru1ValveDriveReq;
}

static void Ses_vCallMotIRefCalcnForOrgSet(void)
{
	Ses_vUpdateArray(GetMotOrgSet.lsess32Posi, lsess32PosiArray, SES_U16_POSIARRAY_LENGTH);
	lsesu16PosiStbFlg = Ses_u16DetStabililty(lsess32PosiArray, SES_U16_POSIARRAY_LENGTH, SES_S16_DELTA_POSI_MIN, SES_S16_DELTA_POSI_MAX);

	Ses_vUpdateArray(GetMotOrgSet.lsess16Iq, lsess32IqArray, SES_U16_IQARRAY_LENGTH);
	lsess16IqMovAvg = Ses_s16CalcMovAvg(lsess32IqArray, SES_U16_IQARRAY_LENGTH);

	Ses_vUpdateArray(GetMotOrgSet.lsess16WrpmMech, lsess16WrpmArray, SES_U16_WRPMARRAY_LENGTH);
	lsess16WrpmMovAvg = Ses_s16CalcMovAvg(lsess16WrpmArray, SES_U16_WRPMARRAY_LENGTH);

	lsess32DeltaPosiOrgSet = GetMotOrgSet.lsess32Posi - lsess32PosiOld;

	lsesu16OrgSetCtrlStateOld = lsesu16OrgSetCtrlState;
	if (lsesu16WallDetectedFlg == 0)
	{
		/* position stability decision */
		if (lsesu16PosiStbFlg == 1)
		{
			lsesu16PosiStbCnt = lsesu16PosiStbCnt + 1;
			if (lsesu16PosiStbCnt > SES_S16_POSI_STB_DECISION_CNT_MAX)
			{
				lsesu16PosiStbCnt = SES_S16_POSI_STB_DECISION_CNT_MAX;
				lsesu16WallDetectedSusFlg = 1;
			}
			else
			{
				lsesu16WallDetectedSusFlg = 0;
			}
		}
		else
		{
			lsesu16PosiStbCnt = 0;
			lsesu16WallDetectedSusFlg = 0;
		}
		/* position stability decision */

		/* origin setting control mode decision */
		if ((lsesu16WallDetectedSusFlg == 1) && (lsess16IqRefOrgSet <= SES_S16_IQREF_MIN)
				&& lsess16IqMovAvg >= (-SES_S16_IQREF_MIN - SES_S16_QCURR_STB_DECISION_THR)
				&& lsess16IqMovAvg <= (-SES_S16_IQREF_MIN + SES_S16_QCURR_STB_DECISION_THR))
		{
			lsesu16WallDetectedFlg = 1;
			lsess32TgtPosiOrgSet = GetMotOrgSet.lsess32Posi;

			lsesu16OrgSetCtrlState = SES_U16_ORGSET_CTRL_STATE_TQ;
		}
		else
		{
			lsess32TgtSpdOrgSet = lsess32TgtSpdOrgSet + SES_S16_DELTA_TGT_SPD;
			if (lsess32TgtSpdOrgSet <= SES_S16_TGT_SPD_MIN)
			{
				lsess32TgtSpdOrgSet = SES_S16_TGT_SPD_MIN;
			}

			lsess32TgtPosiOrgSet = GetMotOrgSet.lsess32Posi;

			lsesu16OrgSetCtrlState = SES_U16_ORGSET_CTRL_STATE_SPD;
		}
		/* origin setting control mode decision */
	}
	else
	{
		/* origin setting control mode decision */
		if ((lsesu16OrgSetCtrlState == SES_U16_ORGSET_CTRL_STATE_TQ) && (lsess16WrpmMovAvg > SES_S16_TQ_2_POSI_CTRL_CHG_SPD_THR))
		{
			lsesu16OrgSetCtrlState = SES_U16_ORGSET_CTRL_STATE_POSI;
		}
		/* origin setting control mode decision */

		/* target speed or position calculation */
		if (lsesu16OrgSetCtrlState == SES_U16_ORGSET_CTRL_STATE_TQ)
		{
			lsess32TgtSpdOrgSet = GetMotOrgSet.lsess16WrpmMech;
			lsess32TgtPosiOrgSet = GetMotOrgSet.lsess32Posi;
		}
		else if (lsesu16OrgSetCtrlState == SES_U16_ORGSET_CTRL_STATE_POSI)
		{
			lsess32TgtSpdOrgSet = GetMotOrgSet.lsess16WrpmMech;
			lsess32TgtPosiOrgSet = L_s32LimitDiff(0, GetMotOrgSet.lsess32Posi, SES_S16_TGTPOSI_INC_ABS, SES_S16_TGTPOSI_DEC_ABS);
		}
		else
		{
			;
		}
		/* target speed or position calculation */

		if((GetMotOrgSet.lsess32Posi > -SES_S16_ORGSETSUS_DECISION_POSI_THR) && (GetMotOrgSet.lsess32Posi < SES_S16_ORGSETSUS_DECISION_POSI_THR))
		{
			lsesu16OrgSetSusFlg = 1;
		}
	}


	if (lsesu16OrgSetSusFlg == 0)
	{
		/* CALC IQ REFERENCE */
		if (lsesu16OrgSetCtrlState == SES_U16_ORGSET_CTRL_STATE_SPD)
		{
			if (lsesu16OrgSetCtrlStateOld != SES_U16_ORGSET_CTRL_STATE_SPD)
			{
				Ses_vInitPid(&lsesstPidSpdCtrlOrgSet);
				lsesstPidSpdCtrlOrgSet.s32Ui = lsess16IqRefOrgSet;
			}
			lsess16IqRefOrgSet = Ses_s16PIDController((sint16)lsess32TgtSpdOrgSet , GetMotOrgSet.lsess16WrpmMech, &lsesstPidSpdCtrlOrgSet);
		}
		else if (lsesu16OrgSetCtrlState == SES_U16_ORGSET_CTRL_STATE_TQ)
		{
			lsess16IqRefOrgSet = lsess16IqRefOrgSet + SES_S16_DELTA_IQREF;
		}
		else if (lsesu16OrgSetCtrlState == SES_U16_ORGSET_CTRL_STATE_POSI)
		{
			if (lsesu16OrgSetCtrlStateOld != SES_U16_ORGSET_CTRL_STATE_POSI)
			{
				Ses_vInitPid(&lsesstPidPosiCtrlOrgSet);
				lsesstPidPosiCtrlOrgSet.s32Ui = lsess16IqRefOrgSet;
			}
			lsess16IqRefOrgSet = Ses_s16PIDController((sint16)lsess32TgtPosiOrgSet , (sint16)GetMotOrgSet.lsess32Posi, &lsesstPidPosiCtrlOrgSet);
		}
		else
		{
			;
		}
	}
	else
	{
		if((lsess16IqRefOrgSet != 0) && (lsess16IqRefOrgSet > SES_S16_IQREF_INC_MAX))
		{
			lsess16IqRefOrgSet = lsess16IqRefOrgSet - SES_S16_IQREF_INC_MAX;
		}
		else if ((lsess16IqRefOrgSet != 0) && (lsess16IqRefOrgSet < -SES_S16_IQREF_INC_MAX))
		{
			lsess16IqRefOrgSet = lsess16IqRefOrgSet + SES_S16_IQREF_INC_MAX;
		}
		else
		{
			lsess16IqRefOrgSet = 0;
		}
		/* CALC IQ REFERENCE */

		if ( (lsess16IqRefOrgSet == 0)
				&& ((GetMotOrgSet.lsess16Iq < SES_S16_ORGSETFLG_SETTING_IQ_MAX) && (GetMotOrgSet.lsess16Iq > SES_S16_ORGSETFLG_SETTING_IQ_MIN))
				&& ((lsess32DeltaPosiOrgSet < SES_S16_ORGSETFLG_SETTING_DPOSI_MAX) && (lsess32DeltaPosiOrgSet > SES_S16_ORGSETFLG_SETTING_DPOSI_MIN)) )
		{
			if(lsess16OrgSetCheckCnt > SES_S16_ORGSETFLG_SETTING_CHK_TIME)
			{
				lsesu16OrgSetFlg = 1;
				lsess16OrgSetCheckCnt = SES_S16_ORGSETFLG_SETTING_CHK_TIME;
				lsess32TgtPosiOrgSet  = 0;
			}
			else
			{
				lsess16OrgSetCheckCnt = lsess16OrgSetCheckCnt + 1;
			}
		}
		else
		{
			lsess16OrgSetCheckCnt = 0;
		}

		if(lsess16OrgSusSetCnt < SES_S16_ORGSETSUS_CNT_MAX)
		{
			lsess16OrgSusSetCnt = lsess16OrgSusSetCnt + 1;
		}
		else
		{
			lsess16OrgSusSetCnt = SES_S16_ORGSETSUS_CNT_MAX;
			lsesu16OrgSetFlg = 1;
			lsess16OrgSetCheckCnt = 0;
			lsess32TgtPosiOrgSet = 0;
			lsess32TgtSpdOrgSet = 0;
		}
	}

	/* limitations */
	if (lsess32TgtPosiOrgSet > Ses_S16_MAX)
	{
		lsess32TgtPosiOrgSet = Ses_S16_MAX;
	}
	else if (lsess32TgtPosiOrgSet < Ses_S16_MIN)
	{
		lsess32TgtPosiOrgSet = Ses_S16_MIN;
	}
	else
	{
		;
	}

	if (lsess16IqRefOrgSet > SES_S16_IQREF_MAX)
	{
		lsess16IqRefOrgSet = SES_S16_IQREF_MAX;
	}
	else if (lsess16IqRefOrgSet < SES_S16_IQREF_MIN)
	{
		lsess16IqRefOrgSet = SES_S16_IQREF_MIN;
	}
	else
	{
		;
	}

	lsesu16OrgSetFailFlg = Ses_u16CurrentBreaker2(lsess16IqRefOrgSet);
	/* limitations */
}

void Ses_vUpdateArray(sint32 Sig, sint32* pArray, sint16 ArrayLength)
{
	static sint16 i;

	if (ArrayLength > 1)
	{
		for (i=ArrayLength-1; i>0; i--)
		{
			*(pArray+i) = *(pArray+(i-1));
		}

		*pArray = Sig;
	}
}

uint16 Ses_u16DetStabililty(sint32* pArray, sint16 ArrayLength, sint16 s16DeltaSigMin, sint16 s16DeltaSigMax)
{
	static uint16 u16StbFlg;
	static sint16 s16Cnt;
	static sint16 i;

	s16Cnt = 0;
	for (i = 0; i <= (ArrayLength-2); i++)
	{
		if ((*(pArray+i) - *(pArray+(i+1)) >= s16DeltaSigMin) && (*(pArray+i) - *(pArray+(i+1)) <= s16DeltaSigMax))
		{
			s16Cnt = s16Cnt + 1;
		}
	}

	if (s16Cnt == (ArrayLength-1))
	{
		u16StbFlg = 1;
	}
	else
	{
		u16StbFlg = 0;
	}

	return u16StbFlg;
}

static uint16 Ses_u16CurrentBreaker2(sint16 s16IqRef)
{
	uint16 u16OrgSetFailFlg = 0;
	static uint16 lu16OrgSetFailCnt;

	if ((s16IqRef >= SES_S16_IQREF_MAX) || (s16IqRef <= SES_S16_IQREF_MIN))
	{
		lu16OrgSetFailCnt = lu16OrgSetFailCnt + 1;
		if (lu16OrgSetFailCnt > Ses_U16_T_1_S)
		{
			u16OrgSetFailFlg = 1;
			lu16OrgSetFailCnt = 0;
		}
	}
	else
	{
		lu16OrgSetFailCnt = 0;
	}

	return u16OrgSetFailFlg;
}

sint16 Ses_s16CalcMovAvg(sint32 *pArray, sint16 ArrayLength)
{
	static sint16 	i;
	static sint32 	s32Sum;
	static sint32 	s32MovAvg;

	if (ArrayLength > 0)
	{
		s32Sum = 0;
		for (i=0; i<ArrayLength; i++)
		{
			s32Sum = s32Sum + *(pArray+i);
		}

		s32MovAvg = s32Sum/ArrayLength;
		if (s32MovAvg > Ses_S16_MAX)
		{
			s32MovAvg = Ses_S16_MAX;
		}
		else if (s32MovAvg < Ses_S16_MIN)
		{
			s32MovAvg = Ses_S16_MIN;
		}
		else
		{
			;
		}
	}
	else
	{
		s32MovAvg = 0;
	}

	return (sint16)s32MovAvg;
}

static sint16 Ses_s16PIDController(sint16 xRef, sint16 x, Ses_pid_t *pParam)
{
	static sint32 s32Tmp0;
	static sint32 s32Tmp1;
	static sint32 s32Tmp2;

	/* calculate err */
	pParam->s32ErrOld = pParam->s32Err;
	pParam->s32Err = xRef - x;
	if (pParam->s32Err > Ses_S16_MAX)
	{
		pParam->s32Err = Ses_S16_MAX;
	}
	else if (pParam->s32Err < Ses_S16_MIN)
	{
		pParam->s32Err = Ses_S16_MIN;
	}
	else
	{
		;
	}

	/* calculate Up */
	pParam->s32Up = (sint32)pParam->s16Kp*pParam->s32Err + pParam->f16Kp*pParam->s32Err/Ses_F16_1;
	if (pParam->s32Up > pParam->s16UpMax)
	{
		pParam->s32Up = (sint32)pParam->s16UpMax;
	}
	else if (pParam->s32Up < pParam->s16UpMin)
	{
		pParam->s32Up = (sint32)pParam->s16UpMin;
	}
	else
	{
		;
	}
	/* calculate Up */

	/* back-calculation */
	pParam->s32Udiff = (sint32)pParam->s16Uout - pParam->s32Ucalc;
	if (pParam->s32Udiff > Ses_S16_MAX)
	{
		pParam->s32Udiff = Ses_S16_MAX;
	}
	else if (pParam->s32Udiff < Ses_S16_MIN)
	{
		pParam->s32Udiff = Ses_S16_MIN;
	}
	else
	{
		;
	}

	s32Tmp0 = (sint32)pParam->s16Kp*Ses_F16_1 + pParam->f16Kp;
	if ((s32Tmp0 != 0) && (pParam->s32Udiff != 0))
	{
		pParam->s32ErrUi = pParam->s32Err + (pParam->s32Udiff*Ses_F16_1)/s32Tmp0;
		if (pParam->s32ErrUi > Ses_S16_MAX)
		{
			pParam->s32ErrUi = Ses_S16_MAX;
		}
		else if (pParam->s32ErrUi < Ses_S16_MIN)
		{
			pParam->s32ErrUi = Ses_S16_MIN;
		}
		else
		{
			;
		}
	}
	else
	{
		pParam->s32ErrUi = pParam->s32Err;
	}
	/* back-calculation */

	/* calculate Ui */
	s32Tmp0 = (sint32)pParam->s16KiTs*pParam->s32ErrUi;
	s32Tmp2 = (sint32)pParam->f16KiTs*pParam->s32ErrUi;
	s32Tmp1 = s32Tmp2/Ses_F16_1;
	if ((s32Tmp2 != 0) && (s32Tmp1 == 0))
	{
		if (s32Tmp2 > 0)
		{
			pParam->s32Ui = pParam->s32Ui + s32Tmp0 + 1;
		}
		else if (s32Tmp2 < 0)
		{
			pParam->s32Ui = pParam->s32Ui + s32Tmp0 - 1;
		}
		else
		{
			pParam->s32Ui = pParam->s32Ui + s32Tmp0;
		}
	}
	else
	{
		pParam->s32Ui = pParam->s32Ui + s32Tmp0 + s32Tmp1;
	}

	if (pParam->s32Ui > pParam->s16UiMax)
	{
		pParam->s32Ui = (sint32)pParam->s16UiMax;
	}
	else if (pParam->s32Ui < pParam->s16UiMin)
	{
		pParam->s32Ui = (sint32)pParam->s16UiMin;
	}
	else
	{
		;
	}
	/* calculate Ui */

	/* calculate DeltaErr */
	pParam->s32DeltaErr = pParam->s32Err - pParam->s32ErrOld;
	if (pParam->s32DeltaErr > Ses_S16_MAX)
	{
		pParam->s32DeltaErr = Ses_S16_MAX;
	}
	else if (pParam->s32DeltaErr < Ses_S16_MIN)
	{
		pParam->s32DeltaErr = Ses_S16_MIN;
	}
	else
	{
		;
	}

	pParam->s32Ud = pParam->s16KdFs*pParam->s32DeltaErr + pParam->f16KdFs*pParam->s32DeltaErr/Ses_F16_1;
	if (pParam->s32Ud > pParam->s16UdMax)
	{
		pParam->s32Ud = (sint32)pParam->s16UdMax;
	}
	else if (pParam->s32Ud < pParam->s16UdMin)
	{
		pParam->s32Ud = (sint32)pParam->s16UdMin;
	}
	else
	{
		;
	}
	/* calculate Ud */

	/* constrain DeltaUout */
	pParam->s32Ucalc = pParam->s32Up + pParam->s32Ui + pParam->s32Ud;
	if (pParam->s32Ucalc > pParam->s16UoutMax)		/* calc s16Uout */
	{
		pParam->s16Uout = pParam->s16UoutMax;
	}
	else if (pParam->s32Ucalc < pParam->s16UoutMin)
	{
		pParam->s16Uout = pParam->s16UoutMin;
	}
	else
	{
		pParam->s16Uout = (sint16)pParam->s32Ucalc;
	}
	/* constrain DeltaUout */

	return pParam->s16Uout;
}

static void Ses_vInitPid(Ses_pid_t *pParam)
{
	/* NON-SETTING PARAMETERS */
	pParam->s32Up = 0;
	pParam->s32Ui = 0;
	pParam->s32Ud = 0;
	pParam->s16Uout = 0;

	pParam->s32Err = 0;
	pParam->s32ErrOld = 0;
	pParam->s32DeltaErr = 0;
	pParam->s32ErrUi = 0;

	pParam->s32Ucalc = 0;
	pParam->s32Udiff = 0;
	/* NON-SETTING PARAMETERS */
}

static void Ses_vCallOrgSetValveCtrl(uint16 u16OrgSetFlg)
{
	uint8 CurrTim;

	if(u16OrgSetFlg == 1)
	{
		lamtru1ValveDriveReq = 0;

		for(CurrTim=0; CurrTim<Ses_U8_FULL_CYCLETIME; CurrTim++)
		{
			FlVlvlau16CurrentOutput[CurrTim] = 0;
			FrVlvlau16CurrentOutput[CurrTim] = 0;
		}
	}
	else
	{
		lamtru1ValveDriveReq = 1;

		for(CurrTim=0; CurrTim<Ses_U8_FULL_CYCLETIME; CurrTim++)
		{
			FlVlvlau16CurrentOutput[CurrTim] = Ses_S16_CURRENT_1700_MA;
			FrVlvlau16CurrentOutput[CurrTim] = Ses_S16_CURRENT_1700_MA;
		}
	}
}

