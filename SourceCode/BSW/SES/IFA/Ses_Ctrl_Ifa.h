/**
 * @defgroup Ses_Ctrl_Ifa Ses_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ses_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SES_CTRL_IFA_H_
#define SES_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ses_Ctrl_Read_Ses_CtrlMotDqIMeasdInfo(data) do \
{ \
    *data = Ses_CtrlMotDqIMeasdInfo; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigInfo(data) do \
{ \
    *data = Ses_CtrlMotRotgAgSigInfo; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo(data) do \
{ \
    *data = Ses_CtrlMotRotgAgSigBfMotOrgSetInfo; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    *data = Ses_CtrlMotDqIMeasdInfo.MotIqMeasd; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    *data = Ses_CtrlMotDqIMeasdInfo.MotIdMeasd; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    *data = Ses_CtrlMotRotgAgSigInfo.StkPosnMeasd; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    *data = Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    *data = Ses_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlEcuModeSts(data) do \
{ \
    *data = Ses_CtrlEcuModeSts; \
}while(0);

#define Ses_Ctrl_Read_Ses_CtrlFuncInhibitSesSts(data) do \
{ \
    *data = Ses_CtrlFuncInhibitSesSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlMotOrgSetStInfo(data) do \
{ \
    Ses_CtrlMotOrgSetStInfo = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlMotDqIRefSesInfo(data) do \
{ \
    Ses_CtrlMotDqIRefSesInfo = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlPwrPistStBfMotOrgSetInfo(data) do \
{ \
    Ses_CtrlPwrPistStBfMotOrgSetInfo = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SimVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlNormVlvReqSesInfo.SimVlvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvReq(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.PrimCutVlvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvReq(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.SecdCutVlvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvReq(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.PrimCircVlvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvReq(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.SecdCircVlvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SimVlvReq(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.SimVlvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_PrimCutVlvDataLen(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.PrimCutVlvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SecdCutVlvDataLen(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.SecdCutVlvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_PrimCircVlvDataLen(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.PrimCircVlvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SecdCircVlvDataLen(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.SecdCircVlvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlNormVlvReqSesInfo_SimVlvDataLen(data) do \
{ \
    Ses_CtrlNormVlvReqSesInfo.SimVlvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_FlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlWhlVlvReqSesInfo.FlIvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_FrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlWhlVlvReqSesInfo.FrIvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_RlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlWhlVlvReqSesInfo.RlIvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_RrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Ses_CtrlWhlVlvReqSesInfo.RrIvReqData[i] = *data[i]; \
}
#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_FlIvReq(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.FlIvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_FrIvReq(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.FrIvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_RlIvReq(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.RlIvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_RrIvReq(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.RrIvReq = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_FlIvDataLen(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.FlIvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_FrIvDataLen(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.FrIvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_RlIvDataLen(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.RlIvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlWhlVlvReqSesInfo_RrIvDataLen(data) do \
{ \
    Ses_CtrlWhlVlvReqSesInfo.RrIvDataLen = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlMotOrgSetStInfo_MotOrgSetErr(data) do \
{ \
    Ses_CtrlMotOrgSetStInfo.MotOrgSetErr = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlMotOrgSetStInfo_MotOrgSet(data) do \
{ \
    Ses_CtrlMotOrgSetStInfo.MotOrgSet = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlMotDqIRefSesInfo_IdRef(data) do \
{ \
    Ses_CtrlMotDqIRefSesInfo.IdRef = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlMotDqIRefSesInfo_IqRef(data) do \
{ \
    Ses_CtrlMotDqIRefSesInfo.IqRef = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp(data) do \
{ \
    Ses_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp = *data; \
}while(0);

#define Ses_Ctrl_Write_Ses_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg(data) do \
{ \
    Ses_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SES_CTRL_IFA_H_ */
/** @} */
