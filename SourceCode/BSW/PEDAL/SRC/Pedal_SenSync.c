/**
 * @defgroup Pedal_SenSync Pedal_SenSync
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_SenSync.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_SenSync.h"
#include "Pedal_SenSync_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 #define PEDAL_BUFFER_LEN   5

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDAL_SENSYNC_START_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDAL_SENSYNC_STOP_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Pedal_SenSync_HdrBusType Pedal_SenSyncBus;

/* Version Info */
const SwcVersionInfo_t Pedal_SenSyncVersionInfo = 
{   
    PEDAL_SENSYNC_MODULE_ID,           /* Pedal_SenSyncVersionInfo.ModuleId */
    PEDAL_SENSYNC_MAJOR_VERSION,       /* Pedal_SenSyncVersionInfo.MajorVer */
    PEDAL_SENSYNC_MINOR_VERSION,       /* Pedal_SenSyncVersionInfo.MinorVer */
    PEDAL_SENSYNC_PATCH_VERSION,       /* Pedal_SenSyncVersionInfo.PatchVer */
    PEDAL_SENSYNC_BRANCH_VERSION       /* Pedal_SenSyncVersionInfo.BranchVer */
};
    
/* Input Data Element */
Pedal_SenPdtBufInfo_t Pedal_SenSyncPdtBufInfo;
Pedal_SenPdfBufInfo_t Pedal_SenSyncPdfBufInfo;
Mom_HndlrEcuModeSts_t Pedal_SenSyncEcuModeSts;
Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenSyncFuncInhibitPedalSts;

/* Output Data Element */
Pedal_SenSyncPdt5msRawInfo_t Pedal_SenSyncPdt5msRawInfo;
Pedal_SenSyncPdf5msRawInfo_t Pedal_SenSyncPdf5msRawInfo;

uint32 Pedal_SenSync_Timer_Start;
uint32 Pedal_SenSync_Timer_Elapsed;

#define PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SENSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_SENSYNC_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SENSYNC_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SENSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_SENSYNC_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SENSYNC_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_SENSYNC_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static Pedal_RawType MinMaxSelectAlgoForAvg(uint8 ArraySize, Pedal_RawType* Pedal_RawArray);

#define PEDAL_SENSYNC_START_SEC_CODE
#include "Pedal_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Pedal_SenSync_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<5;i++) Pedal_SenSyncBus.Pedal_SenSyncPdtBufInfo.PdtRaw[i] = 0;   
    for(i=0;i<5;i++) Pedal_SenSyncBus.Pedal_SenSyncPdfBufInfo.PdfRaw[i] = 0;   
    Pedal_SenSyncBus.Pedal_SenSyncEcuModeSts = 0;
    Pedal_SenSyncBus.Pedal_SenSyncFuncInhibitPedalSts = 0;
    Pedal_SenSyncBus.Pedal_SenSyncPdt5msRawInfo.PdtSig = 0;
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.PdfSig = 0;
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig = 0;
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset = 0;
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig = 0;
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset = 0;
}

void Pedal_SenSync(void)
{
    uint16 i;
    
    Pedal_SenSync_Timer_Start = STM0_TIM0.U;

    /* Input */
    Pedal_SenSync_Read_Pedal_SenSyncPdtBufInfo(&Pedal_SenSyncBus.Pedal_SenSyncPdtBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdtBufInfo 
     : Pedal_SenSyncPdtBufInfo.PdtRaw;
     =============================================================================*/
    
    Pedal_SenSync_Read_Pedal_SenSyncPdfBufInfo(&Pedal_SenSyncBus.Pedal_SenSyncPdfBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdfBufInfo 
     : Pedal_SenSyncPdfBufInfo.PdfRaw;
     =============================================================================*/
    
    Pedal_SenSync_Read_Pedal_SenSyncEcuModeSts(&Pedal_SenSyncBus.Pedal_SenSyncEcuModeSts);
    Pedal_SenSync_Read_Pedal_SenSyncFuncInhibitPedalSts(&Pedal_SenSyncBus.Pedal_SenSyncFuncInhibitPedalSts);

    /* Process */
    Pedal_SenSyncBus.Pedal_SenSyncPdt5msRawInfo.PdtSig = Pedal_SenSyncBus.Pedal_SenSyncPdtBufInfo.PdtRaw[4];
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.PdfSig = Pedal_SenSyncBus.Pedal_SenSyncPdfBufInfo.PdfRaw[4];

    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset = \
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig = MinMaxSelectAlgoForAvg(PEDAL_BUFFER_LEN, Pedal_SenSyncBus.Pedal_SenSyncPdtBufInfo.PdtRaw);

    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset = \
    Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig = MinMaxSelectAlgoForAvg(PEDAL_BUFFER_LEN, Pedal_SenSyncBus.Pedal_SenSyncPdfBufInfo.PdfRaw);

    /* Output */
    Pedal_SenSync_Write_Pedal_SenSyncPdt5msRawInfo(&Pedal_SenSyncBus.Pedal_SenSyncPdt5msRawInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdt5msRawInfo 
     : Pedal_SenSyncPdt5msRawInfo.PdtSig;
     =============================================================================*/
    
    Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo(&Pedal_SenSyncBus.Pedal_SenSyncPdf5msRawInfo);
    /*==============================================================================
    * Members of structure Pedal_SenSyncPdf5msRawInfo 
     : Pedal_SenSyncPdf5msRawInfo.PdfSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig;
     : Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;
     =============================================================================*/
    

    Pedal_SenSync_Timer_Elapsed = STM0_TIM0.U - Pedal_SenSync_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
Pedal_RawType MinMaxSelectAlgoForAvg(uint8 ArraySize, Pedal_RawType* Pedal_RawArray)
{
    Pedal_RawType iMax = Pedal_RawArray[0];
    Pedal_RawType iMin = Pedal_RawArray[0];
    Pedal_RawType Ret_Avg = Pedal_RawArray[0];

    uint8 i;
    for (i=1; i < ArraySize; i++)
    {
        if (Pedal_RawArray[i] < iMin)
        {
            iMin = Pedal_RawArray[i];
        }    
        if (Pedal_RawArray[i] > iMax)
        {
            iMax = Pedal_RawArray[i];
        }

        Ret_Avg += Pedal_RawArray[i];
    }

    Ret_Avg = (Ret_Avg - iMin - iMax) / (ArraySize - 2);

    return Ret_Avg;
}

#define PEDAL_SENSYNC_STOP_SEC_CODE
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
