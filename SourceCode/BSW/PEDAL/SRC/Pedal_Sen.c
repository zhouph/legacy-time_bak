/**
 * @defgroup Pedal_Sen Pedal_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Sen.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_Sen.h"
#include "Pedal_Sen_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 #define MAX_100_PERCENT    10000
 #define PEDAL_BUFFER_LEN   5

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDAL_SEN_START_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDAL_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Pedal_Sen_HdrBusType Pedal_SenBus;

/* Version Info */
const SwcVersionInfo_t Pedal_SenVersionInfo = 
{   
    PEDAL_SEN_MODULE_ID,           /* Pedal_SenVersionInfo.ModuleId */
    PEDAL_SEN_MAJOR_VERSION,       /* Pedal_SenVersionInfo.MajorVer */
    PEDAL_SEN_MINOR_VERSION,       /* Pedal_SenVersionInfo.MinorVer */
    PEDAL_SEN_PATCH_VERSION,       /* Pedal_SenVersionInfo.PatchVer */
    PEDAL_SEN_BRANCH_VERSION       /* Pedal_SenVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputSR1msPedlSigMonInfo_t Pedal_SenPedlSigMonInfo;
Ioc_InputSR1msPedlPwrMonInfo_t Pedal_SenPedlPwrMonInfo;
Mom_HndlrEcuModeSts_t Pedal_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenFuncInhibitPedalSts;

/* Output Data Element */
Pedal_SenPdt1msRawInfo_t Pedal_SenPdt1msRawInfo;
Pedal_SenPdtBufInfo_t Pedal_SenPdtBufInfo;
Pedal_SenPdfBufInfo_t Pedal_SenPdfBufInfo;

uint32 Pedal_Sen_Timer_Start;
uint32 Pedal_Sen_Timer_Elapsed;

#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
static Pedal_TravelType Pedal_TravelBuf[PEDAL_PORT_MAX_NUM][PEDAL_BUFFER_LEN];
static VoltageType Pedal_LowValue[PEDAL_PORT_MAX_NUM];
static uint8 Pedal_Counter = 0;

#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
void Pedal_Sen_Process(void);
#define PEDAL_SEN_START_SEC_CODE
#include "Pedal_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Pedal_Sen_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Pedal_SenBus.Pedal_SenPedlSigMonInfo.PdfSigMon = 0;
    Pedal_SenBus.Pedal_SenPedlSigMonInfo.PdtSigMon = 0;
    Pedal_SenBus.Pedal_SenPedlPwrMonInfo.Pdt5vMon = 0;
    Pedal_SenBus.Pedal_SenPedlPwrMonInfo.Pdf5vMon = 0;
    Pedal_SenBus.Pedal_SenEcuModeSts = 0;
    Pedal_SenBus.Pedal_SenFuncInhibitPedalSts = 0;
    Pedal_SenBus.Pedal_SenPdt1msRawInfo.PdtSig = 0;
    for(i=0;i<5;i++) Pedal_SenBus.Pedal_SenPdtBufInfo.PdtRaw[i] = 0;   
    for(i=0;i<5;i++) Pedal_SenBus.Pedal_SenPdfBufInfo.PdfRaw[i] = 0;   
}

void Pedal_Sen(void)
{
    uint16 i;
    
    Pedal_Sen_Timer_Start = STM0_TIM0.U;

    /* Input */
    Pedal_Sen_Read_Pedal_SenPedlSigMonInfo(&Pedal_SenBus.Pedal_SenPedlSigMonInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPedlSigMonInfo 
     : Pedal_SenPedlSigMonInfo.PdfSigMon;
     : Pedal_SenPedlSigMonInfo.PdtSigMon;
     =============================================================================*/
    
    Pedal_Sen_Read_Pedal_SenPedlPwrMonInfo(&Pedal_SenBus.Pedal_SenPedlPwrMonInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPedlPwrMonInfo 
     : Pedal_SenPedlPwrMonInfo.Pdt5vMon;
     : Pedal_SenPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/
    
    Pedal_Sen_Read_Pedal_SenEcuModeSts(&Pedal_SenBus.Pedal_SenEcuModeSts);
    Pedal_Sen_Read_Pedal_SenFuncInhibitPedalSts(&Pedal_SenBus.Pedal_SenFuncInhibitPedalSts);

    /* Process */
	Pedal_Sen_Process();

    /* Output */
    Pedal_Sen_Write_Pedal_SenPdt1msRawInfo(&Pedal_SenBus.Pedal_SenPdt1msRawInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPdt1msRawInfo 
     : Pedal_SenPdt1msRawInfo.PdtSig;
     =============================================================================*/
    
    Pedal_Sen_Write_Pedal_SenPdtBufInfo(&Pedal_SenBus.Pedal_SenPdtBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPdtBufInfo 
     : Pedal_SenPdtBufInfo.PdtRaw;
     =============================================================================*/
    
    Pedal_Sen_Write_Pedal_SenPdfBufInfo(&Pedal_SenBus.Pedal_SenPdfBufInfo);
    /*==============================================================================
    * Members of structure Pedal_SenPdfBufInfo 
     : Pedal_SenPdfBufInfo.PdfRaw;
     =============================================================================*/
    

    Pedal_Sen_Timer_Elapsed = STM0_TIM0.U - Pedal_Sen_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
void Pedal_Sen_Process(void)
{

    uint8 bufIdx;
    Pedal_PortType port;
    Pedal_VoltageType sigVolt, pwrVolt, zeroVolt, maxVolt;
    Pedal_TravelType travel;

    Pedal_LowValue[PEDAL_PORT_PDT].SigVolt = Pedal_SenBus.Pedal_SenPedlSigMonInfo.PdtSigMon;
    Pedal_LowValue[PEDAL_PORT_PDT].PwrVolt = Pedal_SenBus.Pedal_SenPedlPwrMonInfo.Pdt5vMon;
    Pedal_LowValue[PEDAL_PORT_PDF].SigVolt = Pedal_SenBus.Pedal_SenPedlSigMonInfo.PdfSigMon;
    Pedal_LowValue[PEDAL_PORT_PDF].PwrVolt = Pedal_SenBus.Pedal_SenPedlPwrMonInfo.Pdf5vMon;

    for(port = 0; port<PEDAL_PORT_MAX_NUM; port++)
    {
        /* Input limitation */
        sigVolt = Pedal_LowValue[port].SigVolt;
        pwrVolt = Pedal_LowValue[port].PwrVolt;
        zeroVolt = (Pedal_VoltageType)((sint32)pwrVolt) * Pedal_Config[port].ZeroTravelPercent / MAX_100_PERCENT;
        maxVolt = (Pedal_VoltageType)((sint32)pwrVolt) * Pedal_Config[port].MaxTravelPercent / MAX_100_PERCENT;
        travel = (Pedal_TravelType)((sint32)(sigVolt - zeroVolt)) * Pedal_Config[port].MaxStroke / (maxVolt - zeroVolt);

        Pedal_TravelBuf[port][Pedal_Counter] = travel;
    }

    Pedal_SenBus.Pedal_SenPdt1msRawInfo.PdtSig = Pedal_TravelBuf[PEDAL_PORT_PDT][Pedal_Counter];

    Pedal_Counter++;

    if (Pedal_Counter >= 5)
    {
        for(bufIdx = 0; bufIdx < PEDAL_BUFFER_LEN; bufIdx++)
        {
            Pedal_SenBus.Pedal_SenPdtBufInfo.PdtRaw[bufIdx] = Pedal_TravelBuf[PEDAL_PORT_PDT][bufIdx];
            Pedal_SenBus.Pedal_SenPdfBufInfo.PdfRaw[bufIdx] = Pedal_TravelBuf[PEDAL_PORT_PDF][bufIdx];
        }

        Pedal_Counter = 0;
    }
}
#define PEDAL_SEN_STOP_SEC_CODE
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
