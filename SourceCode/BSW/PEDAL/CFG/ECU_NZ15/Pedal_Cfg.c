/**
 * @defgroup Pedal_Cfg Pedal_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDAL_START_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDAL_STOP_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
const Pedal_ConfigType Pedal_Config[PEDAL_PORT_MAX_NUM] = 
 {
    /* PEDAL_PORT_PDT */
    {
        (Pedal_PercentType)     1500,                             /* ZeroTravelPercent */
        (Pedal_PercentType)     8200,                             /* MaxTravelPercent */
        (Pedal_AngleType)       2680,                             /* MaxTravelAngle */
        (Pedal_StrokeType)      1300,                             /* MaxStroke */
    },
    /* PEDAL_PORT_PDF */
    {
        (Pedal_PercentType)     750,                              /* ZeroTravelPercent */
        (Pedal_PercentType)     4100,                             /* MaxTravelPercent */
        (Pedal_AngleType)       2680,                             /* MaxTravelAngle */
        (Pedal_StrokeType)      1300,                             /* MaxStroke */
    },   
 };

#define PEDAL_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDAL_START_SEC_CODE
#include "Pedal_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDAL_STOP_SEC_CODE
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
