#include "unity.h"
#include "unity_fixture.h"
#include "Pedal_Sen.h"
#include "Pedal_Sen_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint16 UtInput_Pedal_SenPedlSigMonInfo_PdfSigMon[MAX_STEP] = PEDAL_SENPEDLSIGMONINFO_PDFSIGMON;
const Haluint16 UtInput_Pedal_SenPedlSigMonInfo_PdtSigMon[MAX_STEP] = PEDAL_SENPEDLSIGMONINFO_PDTSIGMON;
const Haluint16 UtInput_Pedal_SenPedlPwrMonInfo_Pdt5vMon[MAX_STEP] = PEDAL_SENPEDLPWRMONINFO_PDT5VMON;
const Haluint16 UtInput_Pedal_SenPedlPwrMonInfo_Pdf5vMon[MAX_STEP] = PEDAL_SENPEDLPWRMONINFO_PDF5VMON;
const Mom_HndlrEcuModeSts_t UtInput_Pedal_SenEcuModeSts[MAX_STEP] = PEDAL_SENECUMODESTS;
const Eem_SuspcDetnFuncInhibitPedalSts_t UtInput_Pedal_SenFuncInhibitPedalSts[MAX_STEP] = PEDAL_SENFUNCINHIBITPEDALSTS;

const Salsint16 UtExpected_Pedal_SenPdt1msRawInfo_PdtSig[MAX_STEP] = PEDAL_SENPDT1MSRAWINFO_PDTSIG;
const Salsint32 UtExpected_Pedal_SenPdtBufInfo_PdtRaw_0[MAX_STEP] = PEDAL_SENPDTBUFINFO_PDTRAW_0;
const Salsint32 UtExpected_Pedal_SenPdtBufInfo_PdtRaw_1[MAX_STEP] = PEDAL_SENPDTBUFINFO_PDTRAW_1;
const Salsint32 UtExpected_Pedal_SenPdtBufInfo_PdtRaw_2[MAX_STEP] = PEDAL_SENPDTBUFINFO_PDTRAW_2;
const Salsint32 UtExpected_Pedal_SenPdtBufInfo_PdtRaw_3[MAX_STEP] = PEDAL_SENPDTBUFINFO_PDTRAW_3;
const Salsint32 UtExpected_Pedal_SenPdtBufInfo_PdtRaw_4[MAX_STEP] = PEDAL_SENPDTBUFINFO_PDTRAW_4;
const Salsint32 UtExpected_Pedal_SenPdfBufInfo_PdfRaw_0[MAX_STEP] = PEDAL_SENPDFBUFINFO_PDFRAW_0;
const Salsint32 UtExpected_Pedal_SenPdfBufInfo_PdfRaw_1[MAX_STEP] = PEDAL_SENPDFBUFINFO_PDFRAW_1;
const Salsint32 UtExpected_Pedal_SenPdfBufInfo_PdfRaw_2[MAX_STEP] = PEDAL_SENPDFBUFINFO_PDFRAW_2;
const Salsint32 UtExpected_Pedal_SenPdfBufInfo_PdfRaw_3[MAX_STEP] = PEDAL_SENPDFBUFINFO_PDFRAW_3;
const Salsint32 UtExpected_Pedal_SenPdfBufInfo_PdfRaw_4[MAX_STEP] = PEDAL_SENPDFBUFINFO_PDFRAW_4;



TEST_GROUP(Pedal_Sen);
TEST_SETUP(Pedal_Sen)
{
    Pedal_Sen_Init();
}

TEST_TEAR_DOWN(Pedal_Sen)
{   /* Postcondition */

}

TEST(Pedal_Sen, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Pedal_SenPedlSigMonInfo.PdfSigMon = UtInput_Pedal_SenPedlSigMonInfo_PdfSigMon[i];
        Pedal_SenPedlSigMonInfo.PdtSigMon = UtInput_Pedal_SenPedlSigMonInfo_PdtSigMon[i];
        Pedal_SenPedlPwrMonInfo.Pdt5vMon = UtInput_Pedal_SenPedlPwrMonInfo_Pdt5vMon[i];
        Pedal_SenPedlPwrMonInfo.Pdf5vMon = UtInput_Pedal_SenPedlPwrMonInfo_Pdf5vMon[i];
        Pedal_SenEcuModeSts = UtInput_Pedal_SenEcuModeSts[i];
        Pedal_SenFuncInhibitPedalSts = UtInput_Pedal_SenFuncInhibitPedalSts[i];

        Pedal_Sen();

        TEST_ASSERT_EQUAL(Pedal_SenPdt1msRawInfo.PdtSig, UtExpected_Pedal_SenPdt1msRawInfo_PdtSig[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdtBufInfo.PdtRaw[0], UtExpected_Pedal_SenPdtBufInfo_PdtRaw_0[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdtBufInfo.PdtRaw[1], UtExpected_Pedal_SenPdtBufInfo_PdtRaw_1[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdtBufInfo.PdtRaw[2], UtExpected_Pedal_SenPdtBufInfo_PdtRaw_2[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdtBufInfo.PdtRaw[3], UtExpected_Pedal_SenPdtBufInfo_PdtRaw_3[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdtBufInfo.PdtRaw[4], UtExpected_Pedal_SenPdtBufInfo_PdtRaw_4[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdfBufInfo.PdfRaw[0], UtExpected_Pedal_SenPdfBufInfo_PdfRaw_0[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdfBufInfo.PdfRaw[1], UtExpected_Pedal_SenPdfBufInfo_PdfRaw_1[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdfBufInfo.PdfRaw[2], UtExpected_Pedal_SenPdfBufInfo_PdfRaw_2[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdfBufInfo.PdfRaw[3], UtExpected_Pedal_SenPdfBufInfo_PdfRaw_3[i]);
        TEST_ASSERT_EQUAL(Pedal_SenPdfBufInfo.PdfRaw[4], UtExpected_Pedal_SenPdfBufInfo_PdfRaw_4[i]);
    }
}

TEST_GROUP_RUNNER(Pedal_Sen)
{
    RUN_TEST_CASE(Pedal_Sen, All);
}
