#include "unity.h"
#include "unity_fixture.h"
#include "Pedal_SenSync.h"
#include "Pedal_SenSync_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint32 UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_0[MAX_STEP] = PEDAL_SENSYNCPDTBUFINFO_PDTRAW_0;
const Salsint32 UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_1[MAX_STEP] = PEDAL_SENSYNCPDTBUFINFO_PDTRAW_1;
const Salsint32 UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_2[MAX_STEP] = PEDAL_SENSYNCPDTBUFINFO_PDTRAW_2;
const Salsint32 UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_3[MAX_STEP] = PEDAL_SENSYNCPDTBUFINFO_PDTRAW_3;
const Salsint32 UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_4[MAX_STEP] = PEDAL_SENSYNCPDTBUFINFO_PDTRAW_4;
const Salsint32 UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_0[MAX_STEP] = PEDAL_SENSYNCPDFBUFINFO_PDFRAW_0;
const Salsint32 UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_1[MAX_STEP] = PEDAL_SENSYNCPDFBUFINFO_PDFRAW_1;
const Salsint32 UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_2[MAX_STEP] = PEDAL_SENSYNCPDFBUFINFO_PDFRAW_2;
const Salsint32 UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_3[MAX_STEP] = PEDAL_SENSYNCPDFBUFINFO_PDFRAW_3;
const Salsint32 UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_4[MAX_STEP] = PEDAL_SENSYNCPDFBUFINFO_PDFRAW_4;
const Mom_HndlrEcuModeSts_t UtInput_Pedal_SenSyncEcuModeSts[MAX_STEP] = PEDAL_SENSYNCECUMODESTS;
const Eem_SuspcDetnFuncInhibitPedalSts_t UtInput_Pedal_SenSyncFuncInhibitPedalSts[MAX_STEP] = PEDAL_SENSYNCFUNCINHIBITPEDALSTS;

const Salsint16 UtExpected_Pedal_SenSyncPdt5msRawInfo_PdtSig[MAX_STEP] = PEDAL_SENSYNCPDT5MSRAWINFO_PDTSIG;
const Salsint16 UtExpected_Pedal_SenSyncPdf5msRawInfo_PdfSig[MAX_STEP] = PEDAL_SENSYNCPDF5MSRAWINFO_PDFSIG;
const Salsint16 UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdfSig[MAX_STEP] = PEDAL_SENSYNCPDF5MSRAWINFO_MOVEAVRPDFSIG;
const Salsint16 UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdfSigEolOffset[MAX_STEP] = PEDAL_SENSYNCPDF5MSRAWINFO_MOVEAVRPDFSIGEOLOFFSET;
const Salsint16 UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdtSig[MAX_STEP] = PEDAL_SENSYNCPDF5MSRAWINFO_MOVEAVRPDTSIG;
const Salsint16 UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdtSigEolOffset[MAX_STEP] = PEDAL_SENSYNCPDF5MSRAWINFO_MOVEAVRPDTSIGEOLOFFSET;



TEST_GROUP(Pedal_SenSync);
TEST_SETUP(Pedal_SenSync)
{
    Pedal_SenSync_Init();
}

TEST_TEAR_DOWN(Pedal_SenSync)
{   /* Postcondition */

}

TEST(Pedal_SenSync, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Pedal_SenSyncPdtBufInfo.PdtRaw[0] = UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_0[i];
        Pedal_SenSyncPdtBufInfo.PdtRaw[1] = UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_1[i];
        Pedal_SenSyncPdtBufInfo.PdtRaw[2] = UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_2[i];
        Pedal_SenSyncPdtBufInfo.PdtRaw[3] = UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_3[i];
        Pedal_SenSyncPdtBufInfo.PdtRaw[4] = UtInput_Pedal_SenSyncPdtBufInfo_PdtRaw_4[i];
        Pedal_SenSyncPdfBufInfo.PdfRaw[0] = UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_0[i];
        Pedal_SenSyncPdfBufInfo.PdfRaw[1] = UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_1[i];
        Pedal_SenSyncPdfBufInfo.PdfRaw[2] = UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_2[i];
        Pedal_SenSyncPdfBufInfo.PdfRaw[3] = UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_3[i];
        Pedal_SenSyncPdfBufInfo.PdfRaw[4] = UtInput_Pedal_SenSyncPdfBufInfo_PdfRaw_4[i];
        Pedal_SenSyncEcuModeSts = UtInput_Pedal_SenSyncEcuModeSts[i];
        Pedal_SenSyncFuncInhibitPedalSts = UtInput_Pedal_SenSyncFuncInhibitPedalSts[i];

        Pedal_SenSync();

        TEST_ASSERT_EQUAL(Pedal_SenSyncPdt5msRawInfo.PdtSig, UtExpected_Pedal_SenSyncPdt5msRawInfo_PdtSig[i]);
        TEST_ASSERT_EQUAL(Pedal_SenSyncPdf5msRawInfo.PdfSig, UtExpected_Pedal_SenSyncPdf5msRawInfo_PdfSig[i]);
        TEST_ASSERT_EQUAL(Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig, UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdfSig[i]);
        TEST_ASSERT_EQUAL(Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset, UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdfSigEolOffset[i]);
        TEST_ASSERT_EQUAL(Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig, UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdtSig[i]);
        TEST_ASSERT_EQUAL(Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset, UtExpected_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdtSigEolOffset[i]);
    }
}

TEST_GROUP_RUNNER(Pedal_SenSync)
{
    RUN_TEST_CASE(Pedal_SenSync, All);
}
