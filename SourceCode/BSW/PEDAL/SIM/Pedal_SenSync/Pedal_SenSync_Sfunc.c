#define S_FUNCTION_NAME      Pedal_SenSync_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          12
#define WidthOutputPort         6

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Pedal_SenSync.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Pedal_SenSyncPdtBufInfo.PdtRaw[0] = input[0];
    Pedal_SenSyncPdtBufInfo.PdtRaw[1] = input[1];
    Pedal_SenSyncPdtBufInfo.PdtRaw[2] = input[2];
    Pedal_SenSyncPdtBufInfo.PdtRaw[3] = input[3];
    Pedal_SenSyncPdtBufInfo.PdtRaw[4] = input[4];
    Pedal_SenSyncPdfBufInfo.PdfRaw[0] = input[5];
    Pedal_SenSyncPdfBufInfo.PdfRaw[1] = input[6];
    Pedal_SenSyncPdfBufInfo.PdfRaw[2] = input[7];
    Pedal_SenSyncPdfBufInfo.PdfRaw[3] = input[8];
    Pedal_SenSyncPdfBufInfo.PdfRaw[4] = input[9];
    Pedal_SenSyncEcuModeSts = input[10];
    Pedal_SenSyncFuncInhibitPedalSts = input[11];

    Pedal_SenSync();


    output[0] = Pedal_SenSyncPdt5msRawInfo.PdtSig;
    output[1] = Pedal_SenSyncPdf5msRawInfo.PdfSig;
    output[2] = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig;
    output[3] = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset;
    output[4] = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig;
    output[5] = Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Pedal_SenSync_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
