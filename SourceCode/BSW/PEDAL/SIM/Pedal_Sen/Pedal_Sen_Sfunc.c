#define S_FUNCTION_NAME      Pedal_Sen_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          6
#define WidthOutputPort         11

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Pedal_Sen.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Pedal_SenPedlSigMonInfo.PdfSigMon = input[0];
    Pedal_SenPedlSigMonInfo.PdtSigMon = input[1];
    Pedal_SenPedlPwrMonInfo.Pdt5vMon = input[2];
    Pedal_SenPedlPwrMonInfo.Pdf5vMon = input[3];
    Pedal_SenEcuModeSts = input[4];
    Pedal_SenFuncInhibitPedalSts = input[5];

    Pedal_Sen();


    output[0] = Pedal_SenPdt1msRawInfo.PdtSig;
    output[1] = Pedal_SenPdtBufInfo.PdtRaw[0];
    output[2] = Pedal_SenPdtBufInfo.PdtRaw[1];
    output[3] = Pedal_SenPdtBufInfo.PdtRaw[2];
    output[4] = Pedal_SenPdtBufInfo.PdtRaw[3];
    output[5] = Pedal_SenPdtBufInfo.PdtRaw[4];
    output[6] = Pedal_SenPdfBufInfo.PdfRaw[0];
    output[7] = Pedal_SenPdfBufInfo.PdfRaw[1];
    output[8] = Pedal_SenPdfBufInfo.PdfRaw[2];
    output[9] = Pedal_SenPdfBufInfo.PdfRaw[3];
    output[10] = Pedal_SenPdfBufInfo.PdfRaw[4];
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Pedal_Sen_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
