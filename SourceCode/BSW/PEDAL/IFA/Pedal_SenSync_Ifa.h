/**
 * @defgroup Pedal_SenSync_Ifa Pedal_SenSync_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_SenSync_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDAL_SENSYNC_IFA_H_
#define PEDAL_SENSYNC_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Pedal_SenSync_Read_Pedal_SenSyncPdtBufInfo(data) do \
{ \
    *data = Pedal_SenSyncPdtBufInfo; \
}while(0);

#define Pedal_SenSync_Read_Pedal_SenSyncPdfBufInfo(data) do \
{ \
    *data = Pedal_SenSyncPdfBufInfo; \
}while(0);

#define Pedal_SenSync_Read_Pedal_SenSyncPdtBufInfo_PdtRaw(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Pedal_SenSyncPdtBufInfo.PdtRaw[i]; \
}while(0);

#define Pedal_SenSync_Read_Pedal_SenSyncPdfBufInfo_PdfRaw(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Pedal_SenSyncPdfBufInfo.PdfRaw[i]; \
}while(0);

#define Pedal_SenSync_Read_Pedal_SenSyncEcuModeSts(data) do \
{ \
    *data = Pedal_SenSyncEcuModeSts; \
}while(0);

#define Pedal_SenSync_Read_Pedal_SenSyncFuncInhibitPedalSts(data) do \
{ \
    *data = Pedal_SenSyncFuncInhibitPedalSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Pedal_SenSync_Write_Pedal_SenSyncPdt5msRawInfo(data) do \
{ \
    Pedal_SenSyncPdt5msRawInfo = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo(data) do \
{ \
    Pedal_SenSyncPdf5msRawInfo = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdt5msRawInfo_PdtSig(data) do \
{ \
    Pedal_SenSyncPdt5msRawInfo.PdtSig = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo_PdfSig(data) do \
{ \
    Pedal_SenSyncPdf5msRawInfo.PdfSig = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdfSig(data) do \
{ \
    Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSig = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdfSigEolOffset(data) do \
{ \
    Pedal_SenSyncPdf5msRawInfo.MoveAvrPdfSigEolOffset = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdtSig(data) do \
{ \
    Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSig = *data; \
}while(0);

#define Pedal_SenSync_Write_Pedal_SenSyncPdf5msRawInfo_MoveAvrPdtSigEolOffset(data) do \
{ \
    Pedal_SenSyncPdf5msRawInfo.MoveAvrPdtSigEolOffset = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDAL_SENSYNC_IFA_H_ */
/** @} */
