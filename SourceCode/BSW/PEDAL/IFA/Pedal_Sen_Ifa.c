/**
 * @defgroup Pedal_Sen_Ifa Pedal_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Sen_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_Sen_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDAL_SEN_START_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDAL_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_SEN_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_SEN_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDAL_SEN_START_SEC_CODE
#include "Pedal_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDAL_SEN_STOP_SEC_CODE
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
