/**
 * @defgroup Pedal_Sen_Ifa Pedal_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Sen_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDAL_SEN_IFA_H_
#define PEDAL_SEN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Pedal_Sen_Read_Pedal_SenPedlSigMonInfo(data) do \
{ \
    *data = Pedal_SenPedlSigMonInfo; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenPedlPwrMonInfo(data) do \
{ \
    *data = Pedal_SenPedlPwrMonInfo; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenPedlSigMonInfo_PdfSigMon(data) do \
{ \
    *data = Pedal_SenPedlSigMonInfo.PdfSigMon; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenPedlSigMonInfo_PdtSigMon(data) do \
{ \
    *data = Pedal_SenPedlSigMonInfo.PdtSigMon; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenPedlPwrMonInfo_Pdt5vMon(data) do \
{ \
    *data = Pedal_SenPedlPwrMonInfo.Pdt5vMon; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenPedlPwrMonInfo_Pdf5vMon(data) do \
{ \
    *data = Pedal_SenPedlPwrMonInfo.Pdf5vMon; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenEcuModeSts(data) do \
{ \
    *data = Pedal_SenEcuModeSts; \
}while(0);

#define Pedal_Sen_Read_Pedal_SenFuncInhibitPedalSts(data) do \
{ \
    *data = Pedal_SenFuncInhibitPedalSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Pedal_Sen_Write_Pedal_SenPdt1msRawInfo(data) do \
{ \
    Pedal_SenPdt1msRawInfo = *data; \
}while(0);

#define Pedal_Sen_Write_Pedal_SenPdtBufInfo(data) do \
{ \
    Pedal_SenPdtBufInfo = *data; \
}while(0);

#define Pedal_Sen_Write_Pedal_SenPdfBufInfo(data) do \
{ \
    Pedal_SenPdfBufInfo = *data; \
}while(0);

#define Pedal_Sen_Write_Pedal_SenPdt1msRawInfo_PdtSig(data) do \
{ \
    Pedal_SenPdt1msRawInfo.PdtSig = *data; \
}while(0);

#define Pedal_Sen_Write_Pedal_SenPdtBufInfo_PdtRaw(data) \
{ \
    for(i=0;i<5;i++) Pedal_SenPdtBufInfo.PdtRaw[i] = *data[i]; \
}
#define Pedal_Sen_Write_Pedal_SenPdfBufInfo_PdfRaw(data) \
{ \
    for(i=0;i<5;i++) Pedal_SenPdfBufInfo.PdfRaw[i] = *data[i]; \
}
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDAL_SEN_IFA_H_ */
/** @} */
