/**
 * @defgroup Pedal_Cal Pedal_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PEDAL_START_SEC_CALIB_UNSPECIFIED
#include "Pedal_MemMap.h"
/* Global Calibration Section */


#define PEDAL_STOP_SEC_CALIB_UNSPECIFIED
#include "Pedal_MemMap.h"

#define PEDAL_START_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PEDAL_STOP_SEC_CONST_UNSPECIFIED
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PEDAL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PEDAL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PEDAL_STOP_SEC_VAR_UNSPECIFIED
#include "Pedal_MemMap.h"
#define PEDAL_START_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/** Variable Section (32BIT)**/


#define PEDAL_STOP_SEC_VAR_32BIT
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PEDAL_START_SEC_CODE
#include "Pedal_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PEDAL_STOP_SEC_CODE
#include "Pedal_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
