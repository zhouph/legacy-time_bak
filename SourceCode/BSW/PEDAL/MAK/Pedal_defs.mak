# \file
#
# \brief Pedal
#
# This file contains the implementation of the SWC
# module Pedal.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Pedal_CORE_PATH     := $(MANDO_BSW_ROOT)\Pedal
Pedal_CAL_PATH      := $(Pedal_CORE_PATH)\CAL\$(Pedal_VARIANT)
Pedal_SRC_PATH      := $(Pedal_CORE_PATH)\SRC
Pedal_CFG_PATH      := $(Pedal_CORE_PATH)\CFG\$(Pedal_VARIANT)
Pedal_HDR_PATH      := $(Pedal_CORE_PATH)\HDR
Pedal_IFA_PATH      := $(Pedal_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Pedal_CMN_PATH      := $(Pedal_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Pedal_UT_PATH		:= $(Pedal_CORE_PATH)\UT
	Pedal_UNITY_PATH	:= $(Pedal_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Pedal_UT_PATH)
	CC_INCLUDE_PATH		+= $(Pedal_UNITY_PATH)
	Pedal_Sen_PATH 	:= Pedal_UT_PATH\Pedal_Sen
	Pedal_SenSync_PATH 	:= Pedal_UT_PATH\Pedal_SenSync
endif
CC_INCLUDE_PATH    += $(Pedal_CAL_PATH)
CC_INCLUDE_PATH    += $(Pedal_SRC_PATH)
CC_INCLUDE_PATH    += $(Pedal_CFG_PATH)
CC_INCLUDE_PATH    += $(Pedal_HDR_PATH)
CC_INCLUDE_PATH    += $(Pedal_IFA_PATH)
CC_INCLUDE_PATH    += $(Pedal_CMN_PATH)

