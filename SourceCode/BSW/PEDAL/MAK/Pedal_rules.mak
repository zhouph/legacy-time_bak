# \file
#
# \brief Pedal
#
# This file contains the implementation of the SWC
# module Pedal.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Pedal_src

Pedal_src_FILES        += $(Pedal_SRC_PATH)\Pedal_Sen.c
Pedal_src_FILES        += $(Pedal_IFA_PATH)\Pedal_Sen_Ifa.c
Pedal_src_FILES        += $(Pedal_SRC_PATH)\Pedal_SenSync.c
Pedal_src_FILES        += $(Pedal_IFA_PATH)\Pedal_SenSync_Ifa.c
Pedal_src_FILES        += $(Pedal_CFG_PATH)\Pedal_Cfg.c
Pedal_src_FILES        += $(Pedal_CAL_PATH)\Pedal_Cal.c

ifeq ($(ICE_COMPILE),true)
Pedal_src_FILES        += $(Pedal_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Pedal_src_FILES        += $(Pedal_UNITY_PATH)\unity.c
	Pedal_src_FILES        += $(Pedal_UNITY_PATH)\unity_fixture.c	
	Pedal_src_FILES        += $(Pedal_UT_PATH)\main.c
	Pedal_src_FILES        += $(Pedal_UT_PATH)\Pedal_Sen\Pedal_Sen_UtMain.c
	Pedal_src_FILES        += $(Pedal_UT_PATH)\Pedal_SenSync\Pedal_SenSync_UtMain.c
endif