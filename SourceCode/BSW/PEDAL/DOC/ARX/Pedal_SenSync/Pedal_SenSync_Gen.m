Pedal_SenSyncPdtBufInfo = Simulink.Bus;
DeList={
    'PdtRaw_array_0'
    'PdtRaw_array_1'
    'PdtRaw_array_2'
    'PdtRaw_array_3'
    'PdtRaw_array_4'
    };
Pedal_SenSyncPdtBufInfo = CreateBus(Pedal_SenSyncPdtBufInfo, DeList);
clear DeList;

Pedal_SenSyncPdfBufInfo = Simulink.Bus;
DeList={
    'PdfRaw_array_0'
    'PdfRaw_array_1'
    'PdfRaw_array_2'
    'PdfRaw_array_3'
    'PdfRaw_array_4'
    };
Pedal_SenSyncPdfBufInfo = CreateBus(Pedal_SenSyncPdfBufInfo, DeList);
clear DeList;

Pedal_SenSyncEcuModeSts = Simulink.Bus;
DeList={'Pedal_SenSyncEcuModeSts'};
Pedal_SenSyncEcuModeSts = CreateBus(Pedal_SenSyncEcuModeSts, DeList);
clear DeList;

Pedal_SenSyncFuncInhibitPedalSts = Simulink.Bus;
DeList={'Pedal_SenSyncFuncInhibitPedalSts'};
Pedal_SenSyncFuncInhibitPedalSts = CreateBus(Pedal_SenSyncFuncInhibitPedalSts, DeList);
clear DeList;

Pedal_SenSyncPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Pedal_SenSyncPdt5msRawInfo = CreateBus(Pedal_SenSyncPdt5msRawInfo, DeList);
clear DeList;

Pedal_SenSyncPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    'MoveAvrPdfSig'
    'MoveAvrPdfSigEolOffset'
    'MoveAvrPdtSig'
    'MoveAvrPdtSigEolOffset'
    };
Pedal_SenSyncPdf5msRawInfo = CreateBus(Pedal_SenSyncPdf5msRawInfo, DeList);
clear DeList;

