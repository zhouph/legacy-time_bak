Pedal_SenPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
Pedal_SenPedlSigMonInfo = CreateBus(Pedal_SenPedlSigMonInfo, DeList);
clear DeList;

Pedal_SenPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
Pedal_SenPedlPwrMonInfo = CreateBus(Pedal_SenPedlPwrMonInfo, DeList);
clear DeList;

Pedal_SenEcuModeSts = Simulink.Bus;
DeList={'Pedal_SenEcuModeSts'};
Pedal_SenEcuModeSts = CreateBus(Pedal_SenEcuModeSts, DeList);
clear DeList;

Pedal_SenFuncInhibitPedalSts = Simulink.Bus;
DeList={'Pedal_SenFuncInhibitPedalSts'};
Pedal_SenFuncInhibitPedalSts = CreateBus(Pedal_SenFuncInhibitPedalSts, DeList);
clear DeList;

Pedal_SenPdt1msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Pedal_SenPdt1msRawInfo = CreateBus(Pedal_SenPdt1msRawInfo, DeList);
clear DeList;

Pedal_SenPdtBufInfo = Simulink.Bus;
DeList={
    'PdtRaw_array_0'
    'PdtRaw_array_1'
    'PdtRaw_array_2'
    'PdtRaw_array_3'
    'PdtRaw_array_4'
    };
Pedal_SenPdtBufInfo = CreateBus(Pedal_SenPdtBufInfo, DeList);
clear DeList;

Pedal_SenPdfBufInfo = Simulink.Bus;
DeList={
    'PdfRaw_array_0'
    'PdfRaw_array_1'
    'PdfRaw_array_2'
    'PdfRaw_array_3'
    'PdfRaw_array_4'
    };
Pedal_SenPdfBufInfo = CreateBus(Pedal_SenPdfBufInfo, DeList);
clear DeList;

