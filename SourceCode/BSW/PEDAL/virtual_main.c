/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Pedal_Sen.h"
#include "Pedal_SenSync.h"

int main(void)
{
    Pedal_Sen_Init();
    Pedal_SenSync_Init();

    while(1)
    {
        Pedal_Sen();
        Pedal_SenSync();
    }
}