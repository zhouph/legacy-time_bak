/**
 * @defgroup Pedal_SenSync Pedal_SenSync
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_SenSync.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDAL_SENSYNC_H_
#define PEDAL_SENSYNC_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_Types.h"
#include "Pedal_Cfg.h"
#include "Pedal_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PEDAL_SENSYNC_MODULE_ID      (0)
 #define PEDAL_SENSYNC_MAJOR_VERSION  (2)
 #define PEDAL_SENSYNC_MINOR_VERSION  (0)
 #define PEDAL_SENSYNC_PATCH_VERSION  (0)
 #define PEDAL_SENSYNC_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Pedal_SenSync_HdrBusType Pedal_SenSyncBus;

/* Version Info */
extern const SwcVersionInfo_t Pedal_SenSyncVersionInfo;

/* Input Data Element */
extern Pedal_SenPdtBufInfo_t Pedal_SenSyncPdtBufInfo;
extern Pedal_SenPdfBufInfo_t Pedal_SenSyncPdfBufInfo;
extern Mom_HndlrEcuModeSts_t Pedal_SenSyncEcuModeSts;
extern Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenSyncFuncInhibitPedalSts;

/* Output Data Element */
extern Pedal_SenSyncPdt5msRawInfo_t Pedal_SenSyncPdt5msRawInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Pedal_SenSyncPdf5msRawInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Pedal_SenSync_Init(void);
extern void Pedal_SenSync(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDAL_SENSYNC_H_ */
/** @} */
