/**
 * @defgroup Pedal_MemMap Pedal_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDAL_MEMMAP_H_
#define PEDAL_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PEDAL_START_SEC_CODE)
  #undef PEDAL_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_CODE)
  #undef PEDAL_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_CODE_STARTED
    #error "PEDAL_SEC_CODE not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PEDAL_START_SEC_CONST_UNSPECIFIED)
  #undef PEDAL_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_CONST_UNSPECIFIED)
  #undef PEDAL_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_CONST_UNSPECIFIED_STARTED
    #error "PEDAL_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PEDAL_START_SEC_VAR_UNSPECIFIED)
  #undef PEDAL_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_VAR_UNSPECIFIED)
  #undef PEDAL_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_VAR_UNSPECIFIED_STARTED
    #error "PEDAL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_START_SEC_VAR_32BIT)
  #undef PEDAL_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_VAR_32BIT)
  #undef PEDAL_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_VAR_32BIT_STARTED
    #error "PEDAL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PEDAL_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PEDAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PEDAL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_START_SEC_VAR_NOINIT_32BIT)
  #undef PEDAL_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PEDAL_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PEDAL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PEDAL_START_SEC_CALIB_UNSPECIFIED)
  #undef PEDAL_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEC_STARTED
    #error "PEDAL section not closed"
  #endif
  #define CHK_PEDAL_SEC_STARTED
  #define CHK_PEDAL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PEDAL_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PEDAL_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEC_STARTED
  #undef CHK_PEDAL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PEDAL_SEN_START_SEC_CODE)
  #undef PEDAL_SEN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_CODE)
  #undef PEDAL_SEN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_CODE_STARTED
    #error "PEDAL_SEN_SEC_CODE not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PEDAL_SEN_START_SEC_CONST_UNSPECIFIED)
  #undef PEDAL_SEN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_CONST_UNSPECIFIED)
  #undef PEDAL_SEN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_CONST_UNSPECIFIED_STARTED
    #error "PEDAL_SEN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PEDAL_SEN_START_SEC_VAR_UNSPECIFIED)
  #undef PEDAL_SEN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_VAR_UNSPECIFIED)
  #undef PEDAL_SEN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_VAR_UNSPECIFIED_STARTED
    #error "PEDAL_SEN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_START_SEC_VAR_32BIT)
  #undef PEDAL_SEN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_VAR_32BIT)
  #undef PEDAL_SEN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_VAR_32BIT_STARTED
    #error "PEDAL_SEN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PEDAL_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PEDAL_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PEDAL_SEN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_START_SEC_VAR_NOINIT_32BIT)
  #undef PEDAL_SEN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PEDAL_SEN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PEDAL_SEN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PEDAL_SEN_START_SEC_CALIB_UNSPECIFIED)
  #undef PEDAL_SEN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SEN_SEC_STARTED
    #error "PEDAL_SEN section not closed"
  #endif
  #define CHK_PEDAL_SEN_SEC_STARTED
  #define CHK_PEDAL_SEN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SEN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PEDAL_SEN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SEN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PEDAL_SEN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SEN_SEC_STARTED
  #undef CHK_PEDAL_SEN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PEDAL_SENSYNC_START_SEC_CODE)
  #undef PEDAL_SENSYNC_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_CODE)
  #undef PEDAL_SENSYNC_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_CODE_STARTED
    #error "PEDAL_SENSYNC_SEC_CODE not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PEDAL_SENSYNC_START_SEC_CONST_UNSPECIFIED)
  #undef PEDAL_SENSYNC_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_CONST_UNSPECIFIED)
  #undef PEDAL_SENSYNC_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_CONST_UNSPECIFIED_STARTED
    #error "PEDAL_SENSYNC_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PEDAL_SENSYNC_START_SEC_VAR_UNSPECIFIED)
  #undef PEDAL_SENSYNC_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_VAR_UNSPECIFIED)
  #undef PEDAL_SENSYNC_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_VAR_UNSPECIFIED_STARTED
    #error "PEDAL_SENSYNC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_START_SEC_VAR_32BIT)
  #undef PEDAL_SENSYNC_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_VAR_32BIT)
  #undef PEDAL_SENSYNC_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_VAR_32BIT_STARTED
    #error "PEDAL_SENSYNC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PEDAL_SENSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PEDAL_SENSYNC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_START_SEC_VAR_NOINIT_32BIT)
  #undef PEDAL_SENSYNC_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PEDAL_SENSYNC_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PEDAL_SENSYNC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PEDAL_SENSYNC_START_SEC_CALIB_UNSPECIFIED)
  #undef PEDAL_SENSYNC_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PEDAL_SENSYNC_SEC_STARTED
    #error "PEDAL_SENSYNC section not closed"
  #endif
  #define CHK_PEDAL_SENSYNC_SEC_STARTED
  #define CHK_PEDAL_SENSYNC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PEDAL_SENSYNC_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PEDAL_SENSYNC_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PEDAL_SENSYNC_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PEDAL_SENSYNC_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PEDAL_SENSYNC_SEC_STARTED
  #undef CHK_PEDAL_SENSYNC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDAL_MEMMAP_H_ */
/** @} */
