/**
 * @defgroup Pedal_Sen Pedal_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Sen.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDAL_SEN_H_
#define PEDAL_SEN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pedal_Types.h"
#include "Pedal_Cfg.h"
#include "Pedal_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PEDAL_SEN_MODULE_ID      (0)
 #define PEDAL_SEN_MAJOR_VERSION  (2)
 #define PEDAL_SEN_MINOR_VERSION  (0)
 #define PEDAL_SEN_PATCH_VERSION  (0)
 #define PEDAL_SEN_BRANCH_VERSION (0)

 #define PEDAL_BUFFER_LEN   5

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Pedal_Sen_HdrBusType Pedal_SenBus;

/* Version Info */
extern const SwcVersionInfo_t Pedal_SenVersionInfo;

/* Input Data Element */
extern Ioc_InputSR1msPedlSigMonInfo_t Pedal_SenPedlSigMonInfo;
extern Ioc_InputSR1msPedlPwrMonInfo_t Pedal_SenPedlPwrMonInfo;
extern Mom_HndlrEcuModeSts_t Pedal_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenFuncInhibitPedalSts;

/* Output Data Element */
extern Pedal_SenPdt1msRawInfo_t Pedal_SenPdt1msRawInfo;
extern Pedal_SenPdtBufInfo_t Pedal_SenPdtBufInfo;
extern Pedal_SenPdfBufInfo_t Pedal_SenPdfBufInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Pedal_Sen_Init(void);
extern void Pedal_Sen(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDAL_SEN_H_ */
/** @} */
