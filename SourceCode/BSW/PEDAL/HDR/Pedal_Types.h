/**
 * @defgroup Pedal_Types Pedal_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pedal_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PEDAL_TYPES_H_
#define PEDAL_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputSR1msPedlSigMonInfo_t Pedal_SenPedlSigMonInfo;
    Ioc_InputSR1msPedlPwrMonInfo_t Pedal_SenPedlPwrMonInfo;
    Mom_HndlrEcuModeSts_t Pedal_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenFuncInhibitPedalSts;

/* Output Data Element */
    Pedal_SenPdt1msRawInfo_t Pedal_SenPdt1msRawInfo;
    Pedal_SenPdtBufInfo_t Pedal_SenPdtBufInfo;
    Pedal_SenPdfBufInfo_t Pedal_SenPdfBufInfo;
}Pedal_Sen_HdrBusType;

typedef struct
{
/* Input Data Element */
    Pedal_SenPdtBufInfo_t Pedal_SenSyncPdtBufInfo;
    Pedal_SenPdfBufInfo_t Pedal_SenSyncPdfBufInfo;
    Mom_HndlrEcuModeSts_t Pedal_SenSyncEcuModeSts;
    Eem_SuspcDetnFuncInhibitPedalSts_t Pedal_SenSyncFuncInhibitPedalSts;

/* Output Data Element */
    Pedal_SenSyncPdt5msRawInfo_t Pedal_SenSyncPdt5msRawInfo;
    Pedal_SenSyncPdf5msRawInfo_t Pedal_SenSyncPdf5msRawInfo;
}Pedal_SenSync_HdrBusType;
typedef sint16 Pedal_TravelType;
 /* Pedal buffer length type */
 typedef uint8 Pedal_BufLengthType;
 /* Voltage sense data type */
 typedef uint16 Pedal_VoltageType;
 /* percent type */
 typedef uint16 Pedal_PercentType;
 /* angle type */
 typedef uint16 Pedal_AngleType;
 /* stroke type */
 typedef uint16 Pedal_StrokeType;
 /* ECU Signal port type */
 typedef uint16 Pedal_SigPortType;
 /* ECU Power Signal Port Type */
 typedef uint16 Pedal_PwrPortType;
 /* ECU Power Inhibit Port Type */
 typedef uint16 Pedal_InhibitPortType;
 /* Pedal port type */
 typedef uint8 Pedal_PortType;
 /* Pedal raw data type */
 typedef sint32 Pedal_RawType;

 typedef enum
 {
    Pedal_Normal = 0,
    Pedal_ShortGnd,
    Pedal_ShortBat,
    Pedal_Open,
    Pedal_Invalid
 }Pedal_PortStsType;

 /* Pedal actuator config type */
 typedef struct
 {
    Pedal_PercentType       ZeroTravelPercent;
    Pedal_PercentType       MaxTravelPercent;
    Pedal_AngleType         MaxTravelAngle;
    Pedal_StrokeType        MaxStroke;
    Pedal_SigPortType       SigPort;
    Pedal_PwrPortType       PwrPort;
    Pedal_InhibitPortType   InhibitPort;
 }Pedal_ConfigType;

 typedef struct
 {
    Pedal_VoltageType       SigVolt;
    Pedal_VoltageType       PwrVolt;
 }VoltageType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PEDAL_TYPES_H_ */
/** @} */
