/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Arbitrator_Mtr.h"
#include "Arbitrator_Vlv.h"
#include "Arbitrator_Rly.h"

int main(void)
{
    Arbitrator_Mtr_Init();
    Arbitrator_Vlv_Init();
    Arbitrator_Rly_Init();

    while(1)
    {
        Arbitrator_Mtr();
        Arbitrator_Vlv();
        Arbitrator_Rly();
    }
}