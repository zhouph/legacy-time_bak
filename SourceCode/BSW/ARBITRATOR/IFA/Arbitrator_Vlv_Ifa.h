/**
 * @defgroup Arbitrator_Vlv_Ifa Arbitrator_Vlv_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Vlv_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_VLV_IFA_H_
#define ARBITRATOR_VLV_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo(data) do \
{ \
    *data = Arbitrator_VlvFSBbsVlvActrInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrBbsDrvInfo(data) do \
{ \
    *data = Arbitrator_VlvFSFsrBbsDrvInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrAbsDrvInfo(data) do \
{ \
    *data = Arbitrator_VlvFSFsrAbsDrvInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData(data) do \
{ \
    *data = Arbitrator_VlvEemFailData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlSpdInfo(data) do \
{ \
    *data = Arbitrator_VlvWhlSpdInfo; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo_fs_on_sim_time(data) do \
{ \
    *data = Arbitrator_VlvFSBbsVlvActrInfo.fs_on_sim_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo_fs_on_cutp_time(data) do \
{ \
    *data = Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cutp_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo_fs_on_cuts_time(data) do \
{ \
    *data = Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cuts_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo_fs_on_rlv_time(data) do \
{ \
    *data = Arbitrator_VlvFSBbsVlvActrInfo.fs_on_rlv_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo_fs_on_cv_time(data) do \
{ \
    *data = Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cv_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrBbsDrvInfo_FsrCbsDrv(data) do \
{ \
    *data = Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrBbsDrvInfo_FsrCbsOff(data) do \
{ \
    *data = Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_flno_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_flno_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_frno_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_frno_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_rlno_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlno_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_rrno_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrno_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_bal_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_bal_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_pressdump_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_pressdump_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_resp_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_resp_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_flnc_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_flnc_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_frnc_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_frnc_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_rlnc_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlnc_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo_fs_on_rrnc_time(data) do \
{ \
    *data = Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrnc_time; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    *data = Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsDrv; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    *data = Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsOff; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FlIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FrIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RlIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RrIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FlOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FrOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RlOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RrOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FlIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FlIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FrIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FrIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RlIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RlIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RrIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RrIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FlOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FlOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_FrOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.FrOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RlOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RlOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo_RrOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqAbcInfo.RrOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_PrimCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SecdCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_PrimCircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SecdCircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SimVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_PrimCutVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SecdCutVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_PrimCircVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SecdCircVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SimVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_PrimCutVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SecdCutVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_PrimCircVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SecdCircVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo_SimVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqSesInfo.SimVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_FlIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_FrIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_RlIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_RrIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_FlIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.FlIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_FrIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.FrIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_RlIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.RlIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo_RrIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqSesInfo.RrIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PrimCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SecdCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PrimCircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SecdCircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SimVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_CircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_RelsVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PressDumpVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PrimCutVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SecdCutVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PrimCircVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SecdCircVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SimVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_RelsVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_CircVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PressDumpVlvReq(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PrimCutVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SecdCutVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PrimCircVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SecdCircVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_SimVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_CircVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_RelsVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo_PressDumpVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FlIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FrIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RlIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RrIvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FlOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FrOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RlOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RrOvReq(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FlIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FlIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FrIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FrIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RlIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RlIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RrIvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RrIvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FlOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FlOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FrOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.FrOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RlOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RlOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RrOvDataLen(data) do \
{ \
    *data = Arbitrator_VlvWhlVlvReqIdbInfo.RrOvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_PrimBalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_SecdBalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_ChmbBalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[i]; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_PrimBalVlvReq(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_SecdBalVlvReq(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_ChmbBalVlvReq(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_PrimBalVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_SecdBalVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo_ChmbBalVlvDataLen(data) do \
{ \
    *data = Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvDataLen; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BBSSol(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_BBSSol; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ESCSol(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_ESCSol; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_FrontSol(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_FrontSol; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_RearSol(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_RearSol; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_Motor; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MPS(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_MPS; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MGD(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_MGD; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BBSValveRelay(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_BBSValveRelay; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ESCValveRelay(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_ESCValveRelay; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ECUHw(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_ECUHw; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ASIC(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_ASIC; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_OverVolt; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_UnderVolt; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_LowVolt; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_LowerVolt; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_Ay; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Ax(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_Ax; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_Str; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_CirP1(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_CirP1; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_CirP2(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_CirP2; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_SimP; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_BLS; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ESCSw(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_ESCSw; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_HDCSw(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_HDCSw; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_AVHSw(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_AVHSw; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BrakeLampRelay(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_BrakeLampRelay; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_EssRelay(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_EssRelay; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_GearR(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_GearR; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Clutch(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_Clutch; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_ParkBrake; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BrakeFluid(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_BrakeFluid; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_TCSTemp(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_TCSTemp; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_HDCTemp(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_HDCTemp; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SCCTemp(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_SCCTemp; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_TVBBTemp(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_TVBBTemp; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MainCanLine(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_MainCanLine; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SubCanLine(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_SubCanLine; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_EMSTimeOut(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_EMSTimeOut; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_FWDTimeOut(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_FWDTimeOut; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_TCUTimeOut(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_TCUTimeOut; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_HCUTimeOut(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_HCUTimeOut; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MCUTimeOut(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_MCUTimeOut; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_VariantCoding(data) do \
{ \
    *data = Arbitrator_VlvEemFailData.Eem_Fail_VariantCoding; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Cbs(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Cbs; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Ebd(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Ebd; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Abs(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Abs; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Edc(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Edc; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Btcs(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Btcs; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Etcs(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Etcs; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Tcs(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Tcs; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Vdc(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Vdc; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Hsa(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hsa; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Hdc(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hdc; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Pba(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Pba; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Avh(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Avh; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_CtrlIhb_Moc(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Moc; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_BBS_AllControlInhibit(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_AllControlInhibit; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_BBS_DiagControlInhibit(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DiagControlInhibit; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_BBS_DegradeModeFail(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DegradeModeFail; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    *data = Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_SolenoidDrvDuty(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_FlOvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.FlOvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_FlIvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.FlIvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_FrOvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.FrOvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_FrIvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.FrIvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_RlOvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.RlOvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_RlIvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.RlIvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_RrOvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.RrOvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_RrIvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.RrIvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_PrimCutVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_SecdCutVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_SimVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_ResPVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_BalVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_CircVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_PressDumpReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo_RelsVlvReqData(data) do \
{ \
    *data = Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Arbitrator_VlvWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Arbitrator_VlvWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Arbitrator_VlvWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Arbitrator_VlvWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvEcuModeSts(data) do \
{ \
    *data = Arbitrator_VlvEcuModeSts; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIgnOnOffSts(data) do \
{ \
    *data = Arbitrator_VlvIgnOnOffSts; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvIgnEdgeSts(data) do \
{ \
    *data = Arbitrator_VlvIgnEdgeSts; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Arbitrator_VlvAcmAsicInitCompleteFlag; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagSci(data) do \
{ \
    *data = Arbitrator_VlvDiagSci; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvDiagAhbSci(data) do \
{ \
    *data = Arbitrator_VlvDiagAhbSci; \
}while(0);

#define Arbitrator_Vlv_Read_Arbitrator_VlvFuncInhibitVlvSts(data) do \
{ \
    *data = Arbitrator_VlvFuncInhibitVlvSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrBbsDrvInfo(data) do \
{ \
    Arbitrator_VlvArbFsrBbsDrvInfo = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrAbsDrvInfo(data) do \
{ \
    Arbitrator_VlvArbFsrAbsDrvInfo = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo(data) do \
{ \
    Arbitrator_VlvArbWhlVlvReqInfo = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo(data) do \
{ \
    Arbitrator_VlvArbNormVlvReqInfo = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbBalVlvReqInfo(data) do \
{ \
    Arbitrator_VlvArbBalVlvReqInfo = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbResPVlvReqInfo(data) do \
{ \
    Arbitrator_VlvArbResPVlvReqInfo = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrBbsDrvInfo_FsrCbsDrv(data) do \
{ \
    Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrBbsDrvInfo_FsrCbsOff(data) do \
{ \
    Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrAbsDrvInfo_FsrAbsDrv(data) do \
{ \
    Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_FlOvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_FlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_FrOvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_FrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_RlOvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_RlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_RrOvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo_RrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo_PrimCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo_SecdCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo_SimVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo_RelsVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo_CircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo_PressDumpVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbBalVlvReqInfo_BalVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvArbResPVlvReqInfo_ResPVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[i] = *data[i]; \
}
#define Arbitrator_Vlv_Write_Arbitrator_VlvAsicEnDrDrv(data) do \
{ \
    Arbitrator_VlvAsicEnDrDrv = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbVlvSync(data) do \
{ \
    Arbitrator_VlvArbVlvSync = *data; \
}while(0);

#define Arbitrator_Vlv_Write_Arbitrator_VlvArbVlvDriveState(data) do \
{ \
    Arbitrator_VlvArbVlvDriveState = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_VLV_IFA_H_ */
/** @} */
