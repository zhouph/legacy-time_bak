/**
 * @defgroup Arbitrator_Rly_Ifa Arbitrator_Rly_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Rly_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_RLY_IFA_H_
#define ARBITRATOR_RLY_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData(data) do \
{ \
    *data = Arbitrator_RlyEemFailData; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyWhlSpdInfo(data) do \
{ \
    *data = Arbitrator_RlyWhlSpdInfo; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_Motor; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MPS(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_MPS; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MGD(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_MGD; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BBSValveRelay(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_BBSValveRelay; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ESCValveRelay(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_ESCValveRelay; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ECUHw(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_ECUHw; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ASIC(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_ASIC; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_OverVolt; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_UnderVolt; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_LowVolt; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_LowerVolt; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_Ay; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Ax(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_Ax; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_Str; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_CirP1(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_CirP1; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_CirP2(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_CirP2; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_SimP; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_BLS; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ESCSw(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_ESCSw; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_HDCSw(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_HDCSw; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_AVHSw(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_AVHSw; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BrakeLampRelay(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_BrakeLampRelay; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_EssRelay(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_EssRelay; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_GearR(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_GearR; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Clutch(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_Clutch; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_ParkBrake; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BrakeFluid(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_BrakeFluid; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_TCSTemp(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_TCSTemp; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_HDCTemp(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_HDCTemp; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SCCTemp(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_SCCTemp; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_TVBBTemp(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_TVBBTemp; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MainCanLine(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_MainCanLine; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SubCanLine(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_SubCanLine; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_EMSTimeOut(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_EMSTimeOut; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_FWDTimeOut(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_FWDTimeOut; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_TCUTimeOut(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_TCUTimeOut; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_HCUTimeOut(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_HCUTimeOut; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MCUTimeOut(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_MCUTimeOut; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_VariantCoding(data) do \
{ \
    *data = Arbitrator_RlyEemFailData.Eem_Fail_VariantCoding; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Arbitrator_RlyWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Arbitrator_RlyWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Arbitrator_RlyWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Arbitrator_RlyWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyEcuModeSts(data) do \
{ \
    *data = Arbitrator_RlyEcuModeSts; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyIgnOnOffSts(data) do \
{ \
    *data = Arbitrator_RlyIgnOnOffSts; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyIgnEdgeSts(data) do \
{ \
    *data = Arbitrator_RlyIgnEdgeSts; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyDiagSci(data) do \
{ \
    *data = Arbitrator_RlyDiagSci; \
}while(0);

#define Arbitrator_Rly_Read_Arbitrator_RlyDiagAhbSci(data) do \
{ \
    *data = Arbitrator_RlyDiagAhbSci; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_RLY_IFA_H_ */
/** @} */
