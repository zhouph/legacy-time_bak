/**
 * @defgroup Arbitrator_Mtr_Ifa Arbitrator_Mtr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Mtr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_MTR_IFA_H_
#define ARBITRATOR_MTR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData(data) do \
{ \
    *data = Arbitrator_MtrEemFailData; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMotDqIRefMccInfo(data) do \
{ \
    *data = Arbitrator_MtrMotDqIRefMccInfo; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessOutData; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessDataInf(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessDataInf; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BBSSol(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_BBSSol; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCSol(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_ESCSol; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_FrontSol(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_FrontSol; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_RearSol(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_RearSol; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_Motor; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MPS(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_MPS; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MGD(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_MGD; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BBSValveRelay(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCValveRelay(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ECUHw(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_ECUHw; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ASIC(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_ASIC; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_OverVolt; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_LowVolt; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_Ay; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Ax(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_Ax; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_Str; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_CirP1(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_CirP1; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_CirP2(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_CirP2; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_SimP; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_BLS; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCSw(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_ESCSw; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_HDCSw(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_HDCSw; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_AVHSw(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_AVHSw; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BrakeLampRelay(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_EssRelay(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_EssRelay; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_GearR(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_GearR; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Clutch(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_Clutch; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BrakeFluid(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_TCSTemp(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_HDCTemp(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SCCTemp(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_TVBBTemp(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MainCanLine(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SubCanLine(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_EMSTimeOut(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_FWDTimeOut(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_TCUTimeOut(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_HCUTimeOut(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MCUTimeOut(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_VariantCoding(data) do \
{ \
    *data = Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Cbs(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Ebd(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Abs(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Edc(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Btcs(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Etcs(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Tcs(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Vdc(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Hsa(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Hdc(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Pba(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Avh(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Moc(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_AllControlInhibit(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DiagControlInhibit(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DegradeModeFail(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    *data = Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMotDqIRefMccInfo_IdRef(data) do \
{ \
    *data = Arbitrator_MtrMotDqIRefMccInfo.IdRef; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMotDqIRefMccInfo_IqRef(data) do \
{ \
    *data = Arbitrator_MtrMotDqIRefMccInfo.IqRef; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData_MtrProIqFailsafe(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData_MtrProIdFailsafe(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData_MtrProUPhasePWM(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData_MtrProVPhasePWM(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData_MtrProWPhasePWM(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessDataInf_MtrProCalibrationState(data) do \
{ \
    *data = Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrEcuModeSts(data) do \
{ \
    *data = Arbitrator_MtrEcuModeSts; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrIgnOnOffSts(data) do \
{ \
    *data = Arbitrator_MtrIgnOnOffSts; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrIgnEdgeSts(data) do \
{ \
    *data = Arbitrator_MtrIgnEdgeSts; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrDiagSci(data) do \
{ \
    *data = Arbitrator_MtrDiagSci; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrDiagAhbSci(data) do \
{ \
    *data = Arbitrator_MtrDiagAhbSci; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrVdcLinkFild(data) do \
{ \
    *data = Arbitrator_MtrVdcLinkFild; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMotCtrlMode(data) do \
{ \
    *data = Arbitrator_MtrMotCtrlMode; \
}while(0);

#define Arbitrator_Mtr_Read_Arbitrator_MtrMotCtrlState(data) do \
{ \
    *data = Arbitrator_MtrMotCtrlState; \
}while(0);


/* Set Output DE MAcro Function */
#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorInfo(data) do \
{ \
    Arbitrator_MtrMtrArbtratorInfo = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData_IqSelected(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData.IqSelected = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData_IdSelected(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData.IdSelected = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData_UPhasePWMSelected(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData_VPhasePWMSelected(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData_WPhasePWMSelected(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData_MtrCtrlMode(data) do \
{ \
    Arbitrator_MtrMtrArbtratorData.MtrCtrlMode = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorInfo_MtrArbCalMode(data) do \
{ \
    Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorInfo_MtrArbState(data) do \
{ \
    Arbitrator_MtrMtrArbtratorInfo.MtrArbState = *data; \
}while(0);

#define Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbDriveState(data) do \
{ \
    Arbitrator_MtrMtrArbDriveState = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_MTR_IFA_H_ */
/** @} */
