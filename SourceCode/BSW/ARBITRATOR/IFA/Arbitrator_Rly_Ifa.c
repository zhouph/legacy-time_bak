/**
 * @defgroup Arbitrator_Rly_Ifa Arbitrator_Rly_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Rly_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Rly_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARBITRATOR_RLY_STOP_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_CODE
#include "Arbitrator_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ARBITRATOR_RLY_STOP_SEC_CODE
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
