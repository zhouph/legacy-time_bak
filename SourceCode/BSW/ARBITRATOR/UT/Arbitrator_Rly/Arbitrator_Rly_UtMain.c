#include "unity.h"
#include "unity_fixture.h"
#include "Arbitrator_Rly.h"
#include "Arbitrator_Rly_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Motor[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_MOTOR;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MPS[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_MPS;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MGD[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_MGD;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BBSValveRelay[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_BBSVALVERELAY;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ESCValveRelay[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_ESCVALVERELAY;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ECUHw[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_ECUHW;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ASIC[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_ASIC;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_OverVolt[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_OVERVOLT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_UnderVolt[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_UNDERVOLT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_LowVolt[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_LOWVOLT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_LowerVolt[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_LOWERVOLT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_5V[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_SENPWR_5V;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Yaw[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_YAW;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Ay[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_AY;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Ax[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_AX;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Str[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_STR;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_CirP1[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_CIRP1;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_CirP2[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_CIRP2;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SimP[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_SIMP;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BLS[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_BLS;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ESCSw[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_ESCSW;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_HDCSw[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_HDCSW;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_AVHSw[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_AVHSW;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BrakeLampRelay[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_BRAKELAMPRELAY;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_EssRelay[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_ESSRELAY;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_GearR[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_GEARR;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Clutch[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_CLUTCH;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ParkBrake[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_PARKBRAKE;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDT[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_PEDALPDT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDF[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_PEDALPDF;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BrakeFluid[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_BRAKEFLUID;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_TCSTemp[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_TCSTEMP;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_HDCTemp[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_HDCTEMP;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SCCTemp[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_SCCTEMP;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_TVBBTemp[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_TVBBTEMP;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MainCanLine[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_MAINCANLINE;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SubCanLine[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_SUBCANLINE;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_EMSTimeOut[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_EMSTIMEOUT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_FWDTimeOut[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_FWDTIMEOUT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_TCUTimeOut[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_TCUTIMEOUT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_HCUTimeOut[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_HCUTIMEOUT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MCUTimeOut[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_MCUTIMEOUT;
const Saluint8 UtInput_Arbitrator_RlyEemFailData_Eem_Fail_VariantCoding[MAX_STEP] = ARBITRATOR_RLYEEMFAILDATA_EEM_FAIL_VARIANTCODING;
const Saluint16 UtInput_Arbitrator_RlyWhlSpdInfo_FlWhlSpd[MAX_STEP] = ARBITRATOR_RLYWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_Arbitrator_RlyWhlSpdInfo_FrWhlSpd[MAX_STEP] = ARBITRATOR_RLYWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_Arbitrator_RlyWhlSpdInfo_RlWhlSpd[MAX_STEP] = ARBITRATOR_RLYWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_Arbitrator_RlyWhlSpdInfo_RrlWhlSpd[MAX_STEP] = ARBITRATOR_RLYWHLSPDINFO_RRLWHLSPD;
const Mom_HndlrEcuModeSts_t UtInput_Arbitrator_RlyEcuModeSts[MAX_STEP] = ARBITRATOR_RLYECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Arbitrator_RlyIgnOnOffSts[MAX_STEP] = ARBITRATOR_RLYIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_Arbitrator_RlyIgnEdgeSts[MAX_STEP] = ARBITRATOR_RLYIGNEDGESTS;
const Diag_HndlrDiagSci_t UtInput_Arbitrator_RlyDiagSci[MAX_STEP] = ARBITRATOR_RLYDIAGSCI;
const Diag_HndlrDiagAhbSci_t UtInput_Arbitrator_RlyDiagAhbSci[MAX_STEP] = ARBITRATOR_RLYDIAGAHBSCI;




TEST_GROUP(Arbitrator_Rly);
TEST_SETUP(Arbitrator_Rly)
{
    Arbitrator_Rly_Init();
}

TEST_TEAR_DOWN(Arbitrator_Rly)
{   /* Postcondition */

}

TEST(Arbitrator_Rly, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Arbitrator_RlyEemFailData.Eem_Fail_Motor = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Motor[i];
        Arbitrator_RlyEemFailData.Eem_Fail_MPS = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MPS[i];
        Arbitrator_RlyEemFailData.Eem_Fail_MGD = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MGD[i];
        Arbitrator_RlyEemFailData.Eem_Fail_BBSValveRelay = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BBSValveRelay[i];
        Arbitrator_RlyEemFailData.Eem_Fail_ESCValveRelay = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ESCValveRelay[i];
        Arbitrator_RlyEemFailData.Eem_Fail_ECUHw = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ECUHw[i];
        Arbitrator_RlyEemFailData.Eem_Fail_ASIC = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ASIC[i];
        Arbitrator_RlyEemFailData.Eem_Fail_OverVolt = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_OverVolt[i];
        Arbitrator_RlyEemFailData.Eem_Fail_UnderVolt = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_UnderVolt[i];
        Arbitrator_RlyEemFailData.Eem_Fail_LowVolt = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_LowVolt[i];
        Arbitrator_RlyEemFailData.Eem_Fail_LowerVolt = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_LowerVolt[i];
        Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_12V = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_12V[i];
        Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_5V = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_5V[i];
        Arbitrator_RlyEemFailData.Eem_Fail_Yaw = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Yaw[i];
        Arbitrator_RlyEemFailData.Eem_Fail_Ay = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Ay[i];
        Arbitrator_RlyEemFailData.Eem_Fail_Ax = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Ax[i];
        Arbitrator_RlyEemFailData.Eem_Fail_Str = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Str[i];
        Arbitrator_RlyEemFailData.Eem_Fail_CirP1 = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_CirP1[i];
        Arbitrator_RlyEemFailData.Eem_Fail_CirP2 = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_CirP2[i];
        Arbitrator_RlyEemFailData.Eem_Fail_SimP = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SimP[i];
        Arbitrator_RlyEemFailData.Eem_Fail_BLS = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BLS[i];
        Arbitrator_RlyEemFailData.Eem_Fail_ESCSw = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ESCSw[i];
        Arbitrator_RlyEemFailData.Eem_Fail_HDCSw = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_HDCSw[i];
        Arbitrator_RlyEemFailData.Eem_Fail_AVHSw = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_AVHSw[i];
        Arbitrator_RlyEemFailData.Eem_Fail_BrakeLampRelay = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BrakeLampRelay[i];
        Arbitrator_RlyEemFailData.Eem_Fail_EssRelay = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_EssRelay[i];
        Arbitrator_RlyEemFailData.Eem_Fail_GearR = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_GearR[i];
        Arbitrator_RlyEemFailData.Eem_Fail_Clutch = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_Clutch[i];
        Arbitrator_RlyEemFailData.Eem_Fail_ParkBrake = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_ParkBrake[i];
        Arbitrator_RlyEemFailData.Eem_Fail_PedalPDT = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDT[i];
        Arbitrator_RlyEemFailData.Eem_Fail_PedalPDF = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDF[i];
        Arbitrator_RlyEemFailData.Eem_Fail_BrakeFluid = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_BrakeFluid[i];
        Arbitrator_RlyEemFailData.Eem_Fail_TCSTemp = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_TCSTemp[i];
        Arbitrator_RlyEemFailData.Eem_Fail_HDCTemp = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_HDCTemp[i];
        Arbitrator_RlyEemFailData.Eem_Fail_SCCTemp = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SCCTemp[i];
        Arbitrator_RlyEemFailData.Eem_Fail_TVBBTemp = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_TVBBTemp[i];
        Arbitrator_RlyEemFailData.Eem_Fail_MainCanLine = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MainCanLine[i];
        Arbitrator_RlyEemFailData.Eem_Fail_SubCanLine = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_SubCanLine[i];
        Arbitrator_RlyEemFailData.Eem_Fail_EMSTimeOut = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_EMSTimeOut[i];
        Arbitrator_RlyEemFailData.Eem_Fail_FWDTimeOut = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_FWDTimeOut[i];
        Arbitrator_RlyEemFailData.Eem_Fail_TCUTimeOut = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_TCUTimeOut[i];
        Arbitrator_RlyEemFailData.Eem_Fail_HCUTimeOut = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_HCUTimeOut[i];
        Arbitrator_RlyEemFailData.Eem_Fail_MCUTimeOut = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_MCUTimeOut[i];
        Arbitrator_RlyEemFailData.Eem_Fail_VariantCoding = UtInput_Arbitrator_RlyEemFailData_Eem_Fail_VariantCoding[i];
        Arbitrator_RlyWhlSpdInfo.FlWhlSpd = UtInput_Arbitrator_RlyWhlSpdInfo_FlWhlSpd[i];
        Arbitrator_RlyWhlSpdInfo.FrWhlSpd = UtInput_Arbitrator_RlyWhlSpdInfo_FrWhlSpd[i];
        Arbitrator_RlyWhlSpdInfo.RlWhlSpd = UtInput_Arbitrator_RlyWhlSpdInfo_RlWhlSpd[i];
        Arbitrator_RlyWhlSpdInfo.RrlWhlSpd = UtInput_Arbitrator_RlyWhlSpdInfo_RrlWhlSpd[i];
        Arbitrator_RlyEcuModeSts = UtInput_Arbitrator_RlyEcuModeSts[i];
        Arbitrator_RlyIgnOnOffSts = UtInput_Arbitrator_RlyIgnOnOffSts[i];
        Arbitrator_RlyIgnEdgeSts = UtInput_Arbitrator_RlyIgnEdgeSts[i];
        Arbitrator_RlyDiagSci = UtInput_Arbitrator_RlyDiagSci[i];
        Arbitrator_RlyDiagAhbSci = UtInput_Arbitrator_RlyDiagAhbSci[i];

        Arbitrator_Rly();

    }
}

TEST_GROUP_RUNNER(Arbitrator_Rly)
{
    RUN_TEST_CASE(Arbitrator_Rly, All);
}
