#include "unity.h"
#include "unity_fixture.h"
#include "Arbitrator_Mtr.h"
#include "Arbitrator_Mtr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BBSSol[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_BBSSOL;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ESCSol[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_ESCSOL;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_FrontSol[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_FRONTSOL;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_RearSol[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_REARSOL;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Motor[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_MOTOR;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MPS[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_MPS;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MGD[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_MGD;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BBSValveRelay[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_BBSVALVERELAY;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ESCValveRelay[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_ESCVALVERELAY;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ECUHw[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_ECUHW;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ASIC[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_ASIC;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_OverVolt[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_OVERVOLT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_UnderVolt[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_UNDERVOLT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_LowVolt[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_LOWVOLT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_LowerVolt[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_LOWERVOLT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_5V[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_SENPWR_5V;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Yaw[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_YAW;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Ay[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_AY;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Ax[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_AX;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Str[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_STR;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_CirP1[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_CIRP1;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_CirP2[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_CIRP2;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SimP[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_SIMP;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BLS[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_BLS;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ESCSw[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_ESCSW;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_HDCSw[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_HDCSW;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_AVHSw[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_AVHSW;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BrakeLampRelay[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_BRAKELAMPRELAY;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_EssRelay[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_ESSRELAY;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_GearR[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_GEARR;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Clutch[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_CLUTCH;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ParkBrake[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_PARKBRAKE;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDT[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_PEDALPDT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDF[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_PEDALPDF;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BrakeFluid[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_BRAKEFLUID;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_TCSTemp[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_TCSTEMP;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_HDCTemp[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_HDCTEMP;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SCCTemp[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_SCCTEMP;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_TVBBTemp[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_TVBBTEMP;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MainCanLine[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_MAINCANLINE;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SubCanLine[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_SUBCANLINE;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_EMSTimeOut[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_EMSTIMEOUT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_FWDTimeOut[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_FWDTIMEOUT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_TCUTimeOut[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_TCUTIMEOUT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_HCUTimeOut[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_HCUTIMEOUT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MCUTimeOut[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_MCUTIMEOUT;
const Saluint8 UtInput_Arbitrator_MtrEemFailData_Eem_Fail_VariantCoding[MAX_STEP] = ARBITRATOR_MTREEMFAILDATA_EEM_FAIL_VARIANTCODING;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Cbs[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_CBS;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Ebd[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_EBD;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Abs[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_ABS;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Edc[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_EDC;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Btcs[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_BTCS;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Etcs[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_ETCS;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Tcs[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_TCS;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Vdc[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_VDC;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Hsa[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_HSA;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Hdc[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_HDC;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Pba[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_PBA;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Avh[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_AVH;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Moc[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_CTRLIHB_MOC;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_AllControlInhibit[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_BBS_ALLCONTROLINHIBIT;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DiagControlInhibit[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_BBS_DIAGCONTROLINHIBIT;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DegradeModeFail[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_BBS_DEGRADEMODEFAIL;
const Saluint8 UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DefectiveModeFail[MAX_STEP] = ARBITRATOR_MTREEMCTRLINHIBITDATA_EEM_BBS_DEFECTIVEMODEFAIL;
const Rtesint32 UtInput_Arbitrator_MtrMotDqIRefMccInfo_IdRef[MAX_STEP] = ARBITRATOR_MTRMOTDQIREFMCCINFO_IDREF;
const Rtesint32 UtInput_Arbitrator_MtrMotDqIRefMccInfo_IqRef[MAX_STEP] = ARBITRATOR_MTRMOTDQIREFMCCINFO_IQREF;
const Salsint32 UtInput_Arbitrator_MtrMtrProcessOutData_MtrProIqFailsafe[MAX_STEP] = ARBITRATOR_MTRMTRPROCESSOUTDATA_MTRPROIQFAILSAFE;
const Salsint32 UtInput_Arbitrator_MtrMtrProcessOutData_MtrProIdFailsafe[MAX_STEP] = ARBITRATOR_MTRMTRPROCESSOUTDATA_MTRPROIDFAILSAFE;
const Saluint32 UtInput_Arbitrator_MtrMtrProcessOutData_MtrProUPhasePWM[MAX_STEP] = ARBITRATOR_MTRMTRPROCESSOUTDATA_MTRPROUPHASEPWM;
const Saluint32 UtInput_Arbitrator_MtrMtrProcessOutData_MtrProVPhasePWM[MAX_STEP] = ARBITRATOR_MTRMTRPROCESSOUTDATA_MTRPROVPHASEPWM;
const Saluint32 UtInput_Arbitrator_MtrMtrProcessOutData_MtrProWPhasePWM[MAX_STEP] = ARBITRATOR_MTRMTRPROCESSOUTDATA_MTRPROWPHASEPWM;
const Saluint8 UtInput_Arbitrator_MtrMtrProcessDataInf_MtrProCalibrationState[MAX_STEP] = ARBITRATOR_MTRMTRPROCESSDATAINF_MTRPROCALIBRATIONSTATE;
const Mom_HndlrEcuModeSts_t UtInput_Arbitrator_MtrEcuModeSts[MAX_STEP] = ARBITRATOR_MTRECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Arbitrator_MtrIgnOnOffSts[MAX_STEP] = ARBITRATOR_MTRIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_Arbitrator_MtrIgnEdgeSts[MAX_STEP] = ARBITRATOR_MTRIGNEDGESTS;
const Diag_HndlrDiagSci_t UtInput_Arbitrator_MtrDiagSci[MAX_STEP] = ARBITRATOR_MTRDIAGSCI;
const Diag_HndlrDiagAhbSci_t UtInput_Arbitrator_MtrDiagAhbSci[MAX_STEP] = ARBITRATOR_MTRDIAGAHBSCI;
const Msp_1msCtrlVdcLinkFild_t UtInput_Arbitrator_MtrVdcLinkFild[MAX_STEP] = ARBITRATOR_MTRVDCLINKFILD;
const Mcc_1msCtrlMotCtrlMode_t UtInput_Arbitrator_MtrMotCtrlMode[MAX_STEP] = ARBITRATOR_MTRMOTCTRLMODE;
const Mcc_1msCtrlMotCtrlState_t UtInput_Arbitrator_MtrMotCtrlState[MAX_STEP] = ARBITRATOR_MTRMOTCTRLSTATE;

const Salsint32 UtExpected_Arbitrator_MtrMtrArbtratorData_IqSelected[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORDATA_IQSELECTED;
const Salsint32 UtExpected_Arbitrator_MtrMtrArbtratorData_IdSelected[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORDATA_IDSELECTED;
const Salsint32 UtExpected_Arbitrator_MtrMtrArbtratorData_UPhasePWMSelected[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORDATA_UPHASEPWMSELECTED;
const Salsint32 UtExpected_Arbitrator_MtrMtrArbtratorData_VPhasePWMSelected[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORDATA_VPHASEPWMSELECTED;
const Salsint32 UtExpected_Arbitrator_MtrMtrArbtratorData_WPhasePWMSelected[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORDATA_WPHASEPWMSELECTED;
const Salsint32 UtExpected_Arbitrator_MtrMtrArbtratorData_MtrCtrlMode[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORDATA_MTRCTRLMODE;
const Saluint8 UtExpected_Arbitrator_MtrMtrArbtratorInfo_MtrArbCalMode[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORINFO_MTRARBCALMODE;
const Saluint32 UtExpected_Arbitrator_MtrMtrArbtratorInfo_MtrArbState[MAX_STEP] = ARBITRATOR_MTRMTRARBTRATORINFO_MTRARBSTATE;
const Arbitrator_MtrMtrArbDriveState_t UtExpected_Arbitrator_MtrMtrArbDriveState[MAX_STEP] = ARBITRATOR_MTRMTRARBDRIVESTATE;



TEST_GROUP(Arbitrator_Mtr);
TEST_SETUP(Arbitrator_Mtr)
{
    Arbitrator_Mtr_Init();
}

TEST_TEAR_DOWN(Arbitrator_Mtr)
{   /* Postcondition */

}

TEST(Arbitrator_Mtr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Arbitrator_MtrEemFailData.Eem_Fail_BBSSol = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BBSSol[i];
        Arbitrator_MtrEemFailData.Eem_Fail_ESCSol = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ESCSol[i];
        Arbitrator_MtrEemFailData.Eem_Fail_FrontSol = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_FrontSol[i];
        Arbitrator_MtrEemFailData.Eem_Fail_RearSol = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_RearSol[i];
        Arbitrator_MtrEemFailData.Eem_Fail_Motor = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Motor[i];
        Arbitrator_MtrEemFailData.Eem_Fail_MPS = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MPS[i];
        Arbitrator_MtrEemFailData.Eem_Fail_MGD = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MGD[i];
        Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BBSValveRelay[i];
        Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ESCValveRelay[i];
        Arbitrator_MtrEemFailData.Eem_Fail_ECUHw = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ECUHw[i];
        Arbitrator_MtrEemFailData.Eem_Fail_ASIC = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ASIC[i];
        Arbitrator_MtrEemFailData.Eem_Fail_OverVolt = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_OverVolt[i];
        Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_UnderVolt[i];
        Arbitrator_MtrEemFailData.Eem_Fail_LowVolt = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_LowVolt[i];
        Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_LowerVolt[i];
        Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_12V[i];
        Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_5V[i];
        Arbitrator_MtrEemFailData.Eem_Fail_Yaw = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Yaw[i];
        Arbitrator_MtrEemFailData.Eem_Fail_Ay = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Ay[i];
        Arbitrator_MtrEemFailData.Eem_Fail_Ax = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Ax[i];
        Arbitrator_MtrEemFailData.Eem_Fail_Str = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Str[i];
        Arbitrator_MtrEemFailData.Eem_Fail_CirP1 = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_CirP1[i];
        Arbitrator_MtrEemFailData.Eem_Fail_CirP2 = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_CirP2[i];
        Arbitrator_MtrEemFailData.Eem_Fail_SimP = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SimP[i];
        Arbitrator_MtrEemFailData.Eem_Fail_BLS = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BLS[i];
        Arbitrator_MtrEemFailData.Eem_Fail_ESCSw = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ESCSw[i];
        Arbitrator_MtrEemFailData.Eem_Fail_HDCSw = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_HDCSw[i];
        Arbitrator_MtrEemFailData.Eem_Fail_AVHSw = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_AVHSw[i];
        Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BrakeLampRelay[i];
        Arbitrator_MtrEemFailData.Eem_Fail_EssRelay = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_EssRelay[i];
        Arbitrator_MtrEemFailData.Eem_Fail_GearR = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_GearR[i];
        Arbitrator_MtrEemFailData.Eem_Fail_Clutch = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_Clutch[i];
        Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_ParkBrake[i];
        Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDT[i];
        Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDF[i];
        Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_BrakeFluid[i];
        Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_TCSTemp[i];
        Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_HDCTemp[i];
        Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SCCTemp[i];
        Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_TVBBTemp[i];
        Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MainCanLine[i];
        Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_SubCanLine[i];
        Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_EMSTimeOut[i];
        Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_FWDTimeOut[i];
        Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_TCUTimeOut[i];
        Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_HCUTimeOut[i];
        Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_MCUTimeOut[i];
        Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding = UtInput_Arbitrator_MtrEemFailData_Eem_Fail_VariantCoding[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Cbs[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Ebd[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Abs[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Edc[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Btcs[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Etcs[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Tcs[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Vdc[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Hsa[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Hdc[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Pba[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Avh[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_CtrlIhb_Moc[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_AllControlInhibit[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DiagControlInhibit[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DegradeModeFail[i];
        Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = UtInput_Arbitrator_MtrEemCtrlInhibitData_Eem_BBS_DefectiveModeFail[i];
        Arbitrator_MtrMotDqIRefMccInfo.IdRef = UtInput_Arbitrator_MtrMotDqIRefMccInfo_IdRef[i];
        Arbitrator_MtrMotDqIRefMccInfo.IqRef = UtInput_Arbitrator_MtrMotDqIRefMccInfo_IqRef[i];
        Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe = UtInput_Arbitrator_MtrMtrProcessOutData_MtrProIqFailsafe[i];
        Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe = UtInput_Arbitrator_MtrMtrProcessOutData_MtrProIdFailsafe[i];
        Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM = UtInput_Arbitrator_MtrMtrProcessOutData_MtrProUPhasePWM[i];
        Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM = UtInput_Arbitrator_MtrMtrProcessOutData_MtrProVPhasePWM[i];
        Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM = UtInput_Arbitrator_MtrMtrProcessOutData_MtrProWPhasePWM[i];
        Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState = UtInput_Arbitrator_MtrMtrProcessDataInf_MtrProCalibrationState[i];
        Arbitrator_MtrEcuModeSts = UtInput_Arbitrator_MtrEcuModeSts[i];
        Arbitrator_MtrIgnOnOffSts = UtInput_Arbitrator_MtrIgnOnOffSts[i];
        Arbitrator_MtrIgnEdgeSts = UtInput_Arbitrator_MtrIgnEdgeSts[i];
        Arbitrator_MtrDiagSci = UtInput_Arbitrator_MtrDiagSci[i];
        Arbitrator_MtrDiagAhbSci = UtInput_Arbitrator_MtrDiagAhbSci[i];
        Arbitrator_MtrVdcLinkFild = UtInput_Arbitrator_MtrVdcLinkFild[i];
        Arbitrator_MtrMotCtrlMode = UtInput_Arbitrator_MtrMotCtrlMode[i];
        Arbitrator_MtrMotCtrlState = UtInput_Arbitrator_MtrMotCtrlState[i];

        Arbitrator_Mtr();

        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorData.IqSelected, UtExpected_Arbitrator_MtrMtrArbtratorData_IqSelected[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorData.IdSelected, UtExpected_Arbitrator_MtrMtrArbtratorData_IdSelected[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected, UtExpected_Arbitrator_MtrMtrArbtratorData_UPhasePWMSelected[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected, UtExpected_Arbitrator_MtrMtrArbtratorData_VPhasePWMSelected[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected, UtExpected_Arbitrator_MtrMtrArbtratorData_WPhasePWMSelected[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorData.MtrCtrlMode, UtExpected_Arbitrator_MtrMtrArbtratorData_MtrCtrlMode[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode, UtExpected_Arbitrator_MtrMtrArbtratorInfo_MtrArbCalMode[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbtratorInfo.MtrArbState, UtExpected_Arbitrator_MtrMtrArbtratorInfo_MtrArbState[i]);
        TEST_ASSERT_EQUAL(Arbitrator_MtrMtrArbDriveState, UtExpected_Arbitrator_MtrMtrArbDriveState[i]);
    }
}

TEST_GROUP_RUNNER(Arbitrator_Mtr)
{
    RUN_TEST_CASE(Arbitrator_Mtr, All);
}
