/**
 * @defgroup Arbitrator_Vlv Arbitrator_Vlv
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Processing.c
 * @brief       Valve Arbitrator Function
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Vlv.h"
#include "Arbitrator_Vlv_Processing.h"

#include "common.h"

#include <stdlib.h>

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARBITRATOR_VLV_STOP_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"

extern Swt_SenEscSwtStInfo_t Swt_SenEscSwtStInfo;

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"

static boolean gArbVlv_bInit;

static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputFlOvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputFlIvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputFrOvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputFrIvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputRlOvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputRlIvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputRrOvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputRrIvBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputPrimCutBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputPrimCircBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputSecdCutBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputSecdCircBufIdx;
static Arb_Vlv5msBufIdx_t gArb_Vlv5msOutputSimVlvBufIdx;

static uint8 gArb_Vlv_BBSValveRelay_Prev;
static uint8 gArb_Vlv_ESCValveRelay_Prev;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_CODE
#include "Arbitrator_MemMap.h"

static void Arbitrator_Vlv_MediateRequest(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);
static uint8 Arbitrator_Vlv_CheckDrvState(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);
static void Arbitrator_Vlv_SetValveRequest(Arbitrator_Vlv_HdrBusType *pArbVlvInfo, uint8 u8ValveDrvState);
static void Arbitrator_Vlv_SetValveRelayRequest(Arbitrator_Vlv_HdrBusType *pArbVlvInfo, uint8 u8ValveDrvState);
static void Arbitrator_Vlv_ValveClear(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);
static void Arbitrator_Vlv_ValveDiagSet(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);
static void Arbitrator_Vlv_ValveLogicSet(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);
static void Arbitrator_Vlv_ValveFailsafeSet(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Arbitrator_Vlv_Processing(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    if(gArbVlv_bInit == TRUE)
    {
        Arbitrator_Vlv_MediateRequest(pArbVlvInfo);
    }
}

void Arbitrator_Vlv_Initialize(void)
{
    gArbVlv_bInit = TRUE;

    gArb_Vlv5msOutputFlOvBufIdx = 0;
    gArb_Vlv5msOutputFlIvBufIdx = 0;
    gArb_Vlv5msOutputFrOvBufIdx = 0;
    gArb_Vlv5msOutputFrIvBufIdx = 0;
    gArb_Vlv5msOutputRlOvBufIdx = 0;
    gArb_Vlv5msOutputRlIvBufIdx = 0;
    gArb_Vlv5msOutputRrOvBufIdx = 0;
    gArb_Vlv5msOutputRrIvBufIdx = 0;
    gArb_Vlv5msOutputPrimCutBufIdx = 0;
    gArb_Vlv5msOutputPrimCircBufIdx = 0;
    gArb_Vlv5msOutputSecdCutBufIdx = 0;
    gArb_Vlv5msOutputSecdCircBufIdx = 0;
    gArb_Vlv5msOutputSimVlvBufIdx = 0;

    gArb_Vlv_BBSValveRelay_Prev = 0;
    gArb_Vlv_ESCValveRelay_Prev = 0;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Arbitrator_Vlv_MediateRequest(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    uint8 u8ValveDrvState = ARB_VLV_NONE;

    u8ValveDrvState = Arbitrator_Vlv_CheckDrvState(pArbVlvInfo);

    Arbitrator_Vlv_SetValveRequest(pArbVlvInfo, u8ValveDrvState);

    Arbitrator_Vlv_SetValveRelayRequest(pArbVlvInfo, u8ValveDrvState);
}

static uint8 Arbitrator_Vlv_CheckDrvState(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    uint8 u8ValveDrvState = ARB_VLV_NONE;

    if((pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_BBSSol == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_BBSValveRelay == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_ESCSol == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_ESCValveRelay == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_ASIC == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_ECUHw == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_CirP1 == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_CirP2 == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemFailData.Eem_Fail_SimP == TRUE) ||
       (pArbVlvInfo->Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DefectiveModeFail == TRUE)
       #if 0       
       /* For Winter Test */
       || (Swt_SenEscSwtStInfo.EscDisabledBySwt == 1)
       #endif
      )
    {
        u8ValveDrvState = ARB_VLV_RESIDUAL;
    }
    else if((pArbVlvInfo->Arbitrator_VlvDiagSci == 1) ||
            (pArbVlvInfo->Arbitrator_VlvDiagAhbSci == 1)
           )
    {
        u8ValveDrvState = ARB_VLV_DIAG;
    }
    else if((pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq == STD_ON) || /* Inlet Valve - Idb Info */

            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq == STD_ON) || /* Outlet Valve - Abc Info */ 
            
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq == STD_ON) || 
			(pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq  == STD_ON) || 
			(pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq  == STD_ON) || 
			(pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq == STD_ON) || 		/* SES Valve */

            (pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq == STD_ON) ||
            (pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq == STD_ON) || /* Normal Valve */

			(pArbVlvInfo->Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq == STD_ON) /* BAL Valve */
           )
    {
        u8ValveDrvState = ARB_VLV_LOGIC;
    }
    else
    {
        u8ValveDrvState = ARB_VLV_FAILSAFE;
    }

    pArbVlvInfo->Arbitrator_VlvArbVlvDriveState = u8ValveDrvState;

    return u8ValveDrvState;
}

static void Arbitrator_Vlv_SetValveRequest(Arbitrator_Vlv_HdrBusType *pArbVlvInfo, uint8 u8ValveDrvState)
{
    Arbitrator_Vlv_ValveClear(pArbVlvInfo);
    
    switch(u8ValveDrvState)
    {
    case ARB_VLV_RESIDUAL:
        Arbitrator_Vlv_ValveClear(pArbVlvInfo);
        break;

    case ARB_VLV_DIAG:
        Arbitrator_Vlv_ValveDiagSet(pArbVlvInfo);
        break;

    case ARB_VLV_LOGIC:
        Arbitrator_Vlv_ValveLogicSet(pArbVlvInfo);
        break;

    case ARB_VLV_FAILSAFE:
        Arbitrator_Vlv_ValveFailsafeSet(pArbVlvInfo);
        break;

    default:
        Arbitrator_Vlv_ValveClear(pArbVlvInfo);       
        break;
    }
}

static void Arbitrator_Vlv_SetValveRelayRequest(Arbitrator_Vlv_HdrBusType *pArbVlvInfo, uint8 u8ValveDrvState)
{
    switch(u8ValveDrvState)
    {
    case ARB_VLV_RESIDUAL:
        pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv = STD_OFF;
        pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff = STD_ON;

        pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv = STD_OFF;
        pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff = STD_ON;

		pArbVlvInfo->Arbitrator_VlvAsicEnDrDrv = STD_OFF;

        break;

    case ARB_VLV_DIAG:
    case ARB_VLV_LOGIC:        
        pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv = STD_ON;
        pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff = STD_OFF;

        pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv = STD_ON;
        pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff = STD_OFF;

		pArbVlvInfo->Arbitrator_VlvAsicEnDrDrv = STD_ON;		

        break;

    case ARB_VLV_FAILSAFE:
        pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv = pArbVlvInfo->Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv;
        pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff = pArbVlvInfo->Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff;

        pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv = pArbVlvInfo->Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsDrv;
        pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff = pArbVlvInfo->Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsOff;

		pArbVlvInfo->Arbitrator_VlvAsicEnDrDrv = STD_ON;

        break;

    default:
        break;
    }

    if((pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv != gArb_Vlv_BBSValveRelay_Prev) ||
       (pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv != gArb_Vlv_ESCValveRelay_Prev)
      )
    {
        Arbitrator_Vlv_ValveClear(pArbVlvInfo);
    }

    gArb_Vlv_BBSValveRelay_Prev = pArbVlvInfo->Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv;
    gArb_Vlv_ESCValveRelay_Prev = pArbVlvInfo->Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv;
}

static void Arbitrator_Vlv_ValveClear(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    uint8 i = 0;

    for(i = 0; i < 5; i++)
    {
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = 0;

        pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = 0;
		pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i] = 0;
        pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i] = 0;
		pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i] = 0;

        pArbVlvInfo->Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[i] = 0;

		pArbVlvInfo->Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[i] = 0;
    }
}

static void Arbitrator_Vlv_ValveDiagSet(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    uint8 i = 0;

    for(i = 0; i < 5; i++)
    {
        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.FlOvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[i] = ARB_VLV_MAX_DUTY; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }
        
        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.FrOvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[i] = ARB_VLV_MAX_DUTY; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.RlOvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[i] = ARB_VLV_MAX_DUTY; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.RrOvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[i] = ARB_VLV_MAX_DUTY; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }          

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.FlIvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }  

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.FrIvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        } 

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.RlIvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.RrIvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }                

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

        if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

		if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[i] = ARB_VLV_MAX_CURRENT; //pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
        }

		if(pArbVlvInfo->Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData == TRUE)
        {
            pArbVlvInfo->Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }
    }
}

static void Arbitrator_Vlv_ValveLogicSet(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    uint8 i = 0;

    /* Outlet valve */
    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[i];
        }
    }
    else
    {
        ;
    }


    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[i];
        }
    }
    else
    {
        ;
    }

    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[i];
        }
    }
    else
    {
        ;
    }

    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[i];
        }    
    }
    else
    {
        ;
    }

    /* Inlet valve */
    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[i];
        }
    }
    else if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[i];
        }
    }
    else
    {
        ;
    }

    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[i];
        }
    }
    else if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[i];
        }
    }
    else
    {
        ;
    }   

    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[i];
        }
    }
    else if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[i];
        }
    }
    else
    {
        ;
    }

    if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[i];
        }
    }    
    else if(pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = pArbVlvInfo->Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[i];
        }
    }
    else
    {
        ;
    }

    /* Normal valve */
    if(pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[i];
        }
    }
    else if(pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[i];
        }
    }
    else
    {
        ;
    }

    if(pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[i];
        }
    }
    else if(pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[i];
        }
    }
    else
    {
        ;
    }


    if(pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[i];
        }
    }
    else if(pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[i];
        }
    }
    else
    {
        ;
    }

	if(pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq == STD_ON)
	{
		for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[i];
        }	
	}
	else
    {
        ;
    }

	if(pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq == STD_ON)
	{
		for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[i];
        }
	}	
	else
    {
        ;
    }
	
	if(pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq == STD_ON)
	{
		for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[i];
        }			
	}	
	else
    {
        ;
    }
	
    /* Balance valve */
    if(pArbVlvInfo->Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq == STD_ON)
    {
        for(i = 0; i < 5; i++)
        {
            pArbVlvInfo->Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[i] = pArbVlvInfo->Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[i];
        }
    }
	else
    {
        ;
    }	
}

static void Arbitrator_Vlv_ValveFailsafeSet(Arbitrator_Vlv_HdrBusType *pArbVlvInfo)
{
    uint8 i = 0;
    
    for(i = 0; i < 5; i++)
    {
        if(i < pArbVlvInfo->Arbitrator_VlvFSBbsVlvActrInfo.fs_on_sim_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cutp_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cuts_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSBbsVlvActrInfo.fs_on_rlv_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cv_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_flno_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_frno_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlno_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrno_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_bal_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_pressdump_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i] = ARB_VLV_MAX_CURRENT;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_resp_time)
        {
            /* TODO : Not Yet Implemented */
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_flnc_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[i] = ARB_VLV_MAX_DUTY;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_frnc_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[i] = ARB_VLV_MAX_DUTY;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlnc_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[i] = ARB_VLV_MAX_DUTY;
        }

        if(i < pArbVlvInfo->Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrnc_time)
        {
            pArbVlvInfo->Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[i] = ARB_VLV_MAX_DUTY;
        }        
    }
}

#define ARBITRATOR_VLV_STOP_SEC_CODE
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
