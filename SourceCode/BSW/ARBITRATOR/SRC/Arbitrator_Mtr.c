/**
 * @defgroup Arbitrator_Mtr Arbitrator_Mtr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Mtr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Mtr.h"
#include "Arbitrator_Mtr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_MTR_START_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARBITRATOR_MTR_STOP_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_MTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Arbitrator_Mtr_HdrBusType Arbitrator_MtrBus;

/* Version Info */
const SwcVersionInfo_t Arbitrator_MtrVersionInfo = 
{   
    ARBITRATOR_MTR_MODULE_ID,           /* Arbitrator_MtrVersionInfo.ModuleId */
    ARBITRATOR_MTR_MAJOR_VERSION,       /* Arbitrator_MtrVersionInfo.MajorVer */
    ARBITRATOR_MTR_MINOR_VERSION,       /* Arbitrator_MtrVersionInfo.MinorVer */
    ARBITRATOR_MTR_PATCH_VERSION,       /* Arbitrator_MtrVersionInfo.PatchVer */
    ARBITRATOR_MTR_BRANCH_VERSION       /* Arbitrator_MtrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t Arbitrator_MtrEemFailData;
Eem_MainEemCtrlInhibitData_t Arbitrator_MtrEemCtrlInhibitData;
Mcc_1msCtrlMotDqIRefMccInfo_t Arbitrator_MtrMotDqIRefMccInfo;
Mtr_Processing_DiagMtrProcessOutData_t Arbitrator_MtrMtrProcessOutData;
Mtr_Processing_DiagMtrProcessDataInfo_t Arbitrator_MtrMtrProcessDataInf;
Mom_HndlrEcuModeSts_t Arbitrator_MtrEcuModeSts;
Prly_HndlrIgnOnOffSts_t Arbitrator_MtrIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Arbitrator_MtrIgnEdgeSts;
Diag_HndlrDiagSci_t Arbitrator_MtrDiagSci;
Diag_HndlrDiagAhbSci_t Arbitrator_MtrDiagAhbSci;
Msp_1msCtrlVdcLinkFild_t Arbitrator_MtrVdcLinkFild;
Mcc_1msCtrlMotCtrlMode_t Arbitrator_MtrMotCtrlMode;
Mcc_1msCtrlMotCtrlState_t Arbitrator_MtrMotCtrlState;

/* Output Data Element */
Arbitrator_MtrMtrArbitratorData_t Arbitrator_MtrMtrArbtratorData;
Arbitrator_MtrMtrArbitratorInfo_t Arbitrator_MtrMtrArbtratorInfo;
Arbitrator_MtrMtrArbDriveState_t Arbitrator_MtrMtrArbDriveState;

uint32 Arbitrator_Mtr_Timer_Start;
uint32 Arbitrator_Mtr_Timer_Elapsed;

#define ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_MTR_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_MTR_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_MTR_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_MTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_MTR_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_MTR_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_MTR_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_MTR_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARBITRATOR_MTR_START_SEC_CODE
#include "Arbitrator_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Arbitrator_Mtr_Init(void)
{
    /* Initialize internal bus */
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSSol = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSol = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_FrontSol = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_RearSol = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Motor = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MPS = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MGD = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ECUHw = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ASIC = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_OverVolt = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_LowVolt = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Yaw = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Ay = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Ax = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Str = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP1 = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP2 = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SimP = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BLS = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSw = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCSw = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_AVHSw = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_EssRelay = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_GearR = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Clutch = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMotDqIRefMccInfo.IdRef = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMotDqIRefMccInfo.IqRef = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState = 0;
    Arbitrator_MtrBus.Arbitrator_MtrEcuModeSts = 0;
    Arbitrator_MtrBus.Arbitrator_MtrIgnOnOffSts = 0;
    Arbitrator_MtrBus.Arbitrator_MtrIgnEdgeSts = 0;
    Arbitrator_MtrBus.Arbitrator_MtrDiagSci = 0;
    Arbitrator_MtrBus.Arbitrator_MtrDiagAhbSci = 0;
    Arbitrator_MtrBus.Arbitrator_MtrVdcLinkFild = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMotCtrlMode = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMotCtrlState = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData.IqSelected = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData.IdSelected = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData.MtrCtrlMode = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorInfo.MtrArbState = 0;
    Arbitrator_MtrBus.Arbitrator_MtrMtrArbDriveState = 0;
}

void Arbitrator_Mtr(void)
{
    Arbitrator_Mtr_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BBSSol(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSSol);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCSol(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSol);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_FrontSol(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_FrontSol);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_RearSol(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_RearSol);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Motor(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Motor);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MPS(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MPS);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MGD(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MGD);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BBSValveRelay(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCValveRelay(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ECUHw(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ECUHw);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ASIC(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ASIC);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_OverVolt(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_OverVolt);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_UnderVolt(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_LowVolt(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_LowVolt);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_LowerVolt(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_12V(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SenPwr_5V(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Yaw(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Yaw);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Ay(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Ay);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Ax(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Ax);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Str(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Str);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_CirP1(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP1);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_CirP2(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_CirP2);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SimP(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SimP);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BLS(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BLS);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ESCSw(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ESCSw);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_HDCSw(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCSw);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_AVHSw(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_AVHSw);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BrakeLampRelay(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_EssRelay(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_EssRelay);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_GearR(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_GearR);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_Clutch(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_Clutch);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_ParkBrake(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDT(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_PedalPDF(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_BrakeFluid(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_TCSTemp(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_HDCTemp(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SCCTemp(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_TVBBTemp(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MainCanLine(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_SubCanLine(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_EMSTimeOut(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_FWDTimeOut(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_TCUTimeOut(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_HCUTimeOut(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_MCUTimeOut(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut);
    Arbitrator_Mtr_Read_Arbitrator_MtrEemFailData_Eem_Fail_VariantCoding(&Arbitrator_MtrBus.Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding);

    Arbitrator_Mtr_Read_Arbitrator_MtrEemCtrlInhibitData(&Arbitrator_MtrBus.Arbitrator_MtrEemCtrlInhibitData);
    /*==============================================================================
    * Members of structure Arbitrator_MtrEemCtrlInhibitData 
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/
    
    Arbitrator_Mtr_Read_Arbitrator_MtrMotDqIRefMccInfo(&Arbitrator_MtrBus.Arbitrator_MtrMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure Arbitrator_MtrMotDqIRefMccInfo 
     : Arbitrator_MtrMotDqIRefMccInfo.IdRef;
     : Arbitrator_MtrMotDqIRefMccInfo.IqRef;
     =============================================================================*/
    
    Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessOutData(&Arbitrator_MtrBus.Arbitrator_MtrMtrProcessOutData);
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrProcessOutData 
     : Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe;
     : Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe;
     : Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM;
     : Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM;
     : Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Arbitrator_Mtr_Read_Arbitrator_MtrMtrProcessDataInf_MtrProCalibrationState(&Arbitrator_MtrBus.Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState);

    Arbitrator_Mtr_Read_Arbitrator_MtrEcuModeSts(&Arbitrator_MtrBus.Arbitrator_MtrEcuModeSts);
    Arbitrator_Mtr_Read_Arbitrator_MtrIgnOnOffSts(&Arbitrator_MtrBus.Arbitrator_MtrIgnOnOffSts);
    Arbitrator_Mtr_Read_Arbitrator_MtrIgnEdgeSts(&Arbitrator_MtrBus.Arbitrator_MtrIgnEdgeSts);
    Arbitrator_Mtr_Read_Arbitrator_MtrDiagSci(&Arbitrator_MtrBus.Arbitrator_MtrDiagSci);
    Arbitrator_Mtr_Read_Arbitrator_MtrDiagAhbSci(&Arbitrator_MtrBus.Arbitrator_MtrDiagAhbSci);
    Arbitrator_Mtr_Read_Arbitrator_MtrVdcLinkFild(&Arbitrator_MtrBus.Arbitrator_MtrVdcLinkFild);
    Arbitrator_Mtr_Read_Arbitrator_MtrMotCtrlMode(&Arbitrator_MtrBus.Arbitrator_MtrMotCtrlMode);
    Arbitrator_Mtr_Read_Arbitrator_MtrMotCtrlState(&Arbitrator_MtrBus.Arbitrator_MtrMotCtrlState);

    /* Process */
    Arbitrator_Mtr_Process(&Arbitrator_MtrBus);

    /* Output */
    Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorData(&Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorData);
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrArbtratorData 
     : Arbitrator_MtrMtrArbtratorData.IqSelected;
     : Arbitrator_MtrMtrArbtratorData.IdSelected;
     : Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected;
     : Arbitrator_MtrMtrArbtratorData.MtrCtrlMode;
     =============================================================================*/
    
    Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbtratorInfo(&Arbitrator_MtrBus.Arbitrator_MtrMtrArbtratorInfo);
    /*==============================================================================
    * Members of structure Arbitrator_MtrMtrArbtratorInfo 
     : Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode;
     : Arbitrator_MtrMtrArbtratorInfo.MtrArbState;
     =============================================================================*/
    
    Arbitrator_Mtr_Write_Arbitrator_MtrMtrArbDriveState(&Arbitrator_MtrBus.Arbitrator_MtrMtrArbDriveState);

    Arbitrator_Mtr_Timer_Elapsed = STM0_TIM0.U - Arbitrator_Mtr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ARBITRATOR_MTR_STOP_SEC_CODE
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
