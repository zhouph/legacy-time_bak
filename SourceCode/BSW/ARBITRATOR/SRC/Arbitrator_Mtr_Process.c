/**
 * @defgroup Arbitrator_Mtr Arbitrator_Mtr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Mtr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include	"Arbitrator_Mtr.h"
#include	"Arbitrator_Mtr_Ifa.h"
#include	"Arbitrator_Mtr_Process.h"
/* To Do*/
#include "Mcc_Types.h"/*for getting Control State 20150713*/
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define DEF_DIAG_STATE              9000
#define DEF_CURRENT_CONTROL     1
#define DEF_DUTY_CONTROL            0
#define DEF_HALF_DUTY           2499
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_MTR_START_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARBITRATOR_MTR_STOP_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_MTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"

Saluint16 Arbitrator_PWM_A; 
Saluint16 Arbitrator_PWM_B;
Saluint16 Arbitrator_PWM_C;

/* To Do */
extern Mcc_1msCtrl_HdrBusType Mcc_1msCtrlBus; /*for getting Control State 20150713*/

void Arbitrator_Mtr_Process(Arbitrator_Mtr_HdrBusType *pArbitrator_Mtr_HdrBusType)
{
    /* Receive*/
    Saluint8 MtrProcessingState= pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState;
    Salsint32 MtrCtrMode		   = pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMotCtrlMode;
    Saluint32 MtrArbState       = Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlState;
    Saluint32 MtrCalPwm_U	= DEF_HALF_DUTY;
    Saluint32 MtrCalPwm_V	= DEF_HALF_DUTY;
    Saluint32 MtrCalPwm_W	= DEF_HALF_DUTY;    
    Salsint32 MtrIqSelected	= Salsint32Init;
    Salsint32 MtrIdSelected	= Salsint32Init;


    if(pArbitrator_Mtr_HdrBusType->Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit==0)
    {

        if((MtrProcessingState == DEF_MTR_REQUEST_IQ)
        ||(MtrProcessingState ==DEF_MTR_REQUEST_PWM)
        ||(MtrProcessingState ==DEF_MTR_REQUEST_PWM_INIT)
        )
        {
            MtrArbState =DEF_DIAG_STATE;
        }

        if (MtrArbState==DEF_DIAG_STATE)
        {
            if(MtrProcessingState ==DEF_MTR_REQUEST_IQ) 
            {
                MtrCtrMode =DEF_CURRENT_CONTROL;
                MtrIdSelected =pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe;
                MtrIqSelected =pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe;
            }
            else if((MtrProcessingState ==DEF_MTR_REQUEST_PWM)
                ||(MtrProcessingState ==DEF_MTR_REQUEST_PWM_INIT))
            {
                MtrCtrMode =DEF_DUTY_CONTROL;
                MtrCalPwm_U	   	= pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM;
                MtrCalPwm_V	   	= pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM;
                MtrCalPwm_W	   	= pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM;
            }
            else
            {
                ;
            }
        }
        else
        {
            MtrIdSelected =pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMotDqIRefMccInfo.IdRef;
            MtrIqSelected =pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMotDqIRefMccInfo.IqRef;
            if(MtrCtrMode  ==DEF_DUTY_CONTROL)
            {
                MtrCalPwm_U	   	= DEF_HALF_DUTY;
                MtrCalPwm_V	   	= DEF_HALF_DUTY;
                MtrCalPwm_W	   	= DEF_HALF_DUTY;
            }
        }
    }
    else
    {
        MtrCalPwm_U	= Salsint32Init;
        MtrCalPwm_V	= Salsint32Init;
        MtrCalPwm_W= Salsint32Init; 
    }

    /* Output */
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode			=MtrProcessingState;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorData.MtrCtrlMode			=MtrCtrMode;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected 	=MtrCalPwm_U;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected 	=MtrCalPwm_V;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected 	=MtrCalPwm_W;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorData.IdSelected 			=MtrIdSelected;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorData.IqSelected 			=MtrIqSelected;
    pArbitrator_Mtr_HdrBusType->Arbitrator_MtrMtrArbtratorInfo.MtrArbState			=MtrArbState;
}