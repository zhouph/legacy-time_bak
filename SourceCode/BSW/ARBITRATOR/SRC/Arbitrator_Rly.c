/**
 * @defgroup Arbitrator_Rly Arbitrator_Rly
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Rly.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Rly.h"
#include "Arbitrator_Rly_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARBITRATOR_RLY_STOP_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Arbitrator_Rly_HdrBusType Arbitrator_RlyBus;

/* Version Info */
const SwcVersionInfo_t Arbitrator_RlyVersionInfo = 
{   
    ARBITRATOR_RLY_MODULE_ID,           /* Arbitrator_RlyVersionInfo.ModuleId */
    ARBITRATOR_RLY_MAJOR_VERSION,       /* Arbitrator_RlyVersionInfo.MajorVer */
    ARBITRATOR_RLY_MINOR_VERSION,       /* Arbitrator_RlyVersionInfo.MinorVer */
    ARBITRATOR_RLY_PATCH_VERSION,       /* Arbitrator_RlyVersionInfo.PatchVer */
    ARBITRATOR_RLY_BRANCH_VERSION       /* Arbitrator_RlyVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t Arbitrator_RlyEemFailData;
Wss_SenWhlSpdInfo_t Arbitrator_RlyWhlSpdInfo;
Mom_HndlrEcuModeSts_t Arbitrator_RlyEcuModeSts;
Prly_HndlrIgnOnOffSts_t Arbitrator_RlyIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Arbitrator_RlyIgnEdgeSts;
Diag_HndlrDiagSci_t Arbitrator_RlyDiagSci;
Diag_HndlrDiagAhbSci_t Arbitrator_RlyDiagAhbSci;

/* Output Data Element */

uint32 Arbitrator_Rly_Timer_Start;
uint32 Arbitrator_Rly_Timer_Elapsed;

#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_RLY_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_RLY_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARBITRATOR_RLY_START_SEC_CODE
#include "Arbitrator_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Arbitrator_Rly_Init(void)
{
    /* Initialize internal bus */
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Motor = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MPS = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MGD = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BBSValveRelay = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ESCValveRelay = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ECUHw = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ASIC = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_OverVolt = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_UnderVolt = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_LowVolt = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_LowerVolt = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_12V = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_5V = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Yaw = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Ay = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Ax = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Str = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_CirP1 = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_CirP2 = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SimP = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BLS = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ESCSw = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_HDCSw = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_AVHSw = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BrakeLampRelay = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_EssRelay = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_GearR = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Clutch = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ParkBrake = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_PedalPDT = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_PedalPDF = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BrakeFluid = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_TCSTemp = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_HDCTemp = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SCCTemp = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_TVBBTemp = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MainCanLine = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SubCanLine = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_EMSTimeOut = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_FWDTimeOut = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_TCUTimeOut = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_HCUTimeOut = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MCUTimeOut = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_VariantCoding = 0;
    Arbitrator_RlyBus.Arbitrator_RlyWhlSpdInfo.FlWhlSpd = 0;
    Arbitrator_RlyBus.Arbitrator_RlyWhlSpdInfo.FrWhlSpd = 0;
    Arbitrator_RlyBus.Arbitrator_RlyWhlSpdInfo.RlWhlSpd = 0;
    Arbitrator_RlyBus.Arbitrator_RlyWhlSpdInfo.RrlWhlSpd = 0;
    Arbitrator_RlyBus.Arbitrator_RlyEcuModeSts = 0;
    Arbitrator_RlyBus.Arbitrator_RlyIgnOnOffSts = 0;
    Arbitrator_RlyBus.Arbitrator_RlyIgnEdgeSts = 0;
    Arbitrator_RlyBus.Arbitrator_RlyDiagSci = 0;
    Arbitrator_RlyBus.Arbitrator_RlyDiagAhbSci = 0;
}

void Arbitrator_Rly(void)
{
    Arbitrator_Rly_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Motor(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Motor);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MPS(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MPS);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MGD(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MGD);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BBSValveRelay(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BBSValveRelay);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ESCValveRelay(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ESCValveRelay);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ECUHw(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ECUHw);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ASIC(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ASIC);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_OverVolt(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_OverVolt);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_UnderVolt(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_UnderVolt);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_LowVolt(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_LowVolt);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_LowerVolt(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_LowerVolt);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_12V(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_12V);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SenPwr_5V(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_5V);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Yaw(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Yaw);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Ay(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Ay);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Ax(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Ax);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Str(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Str);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_CirP1(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_CirP1);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_CirP2(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_CirP2);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SimP(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SimP);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BLS(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BLS);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ESCSw(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ESCSw);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_HDCSw(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_HDCSw);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_AVHSw(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_AVHSw);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BrakeLampRelay(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BrakeLampRelay);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_EssRelay(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_EssRelay);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_GearR(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_GearR);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_Clutch(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_Clutch);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_ParkBrake(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_ParkBrake);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDT(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_PedalPDT);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_PedalPDF(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_PedalPDF);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_BrakeFluid(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_BrakeFluid);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_TCSTemp(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_TCSTemp);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_HDCTemp(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_HDCTemp);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SCCTemp(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SCCTemp);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_TVBBTemp(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_TVBBTemp);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MainCanLine(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MainCanLine);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_SubCanLine(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_SubCanLine);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_EMSTimeOut(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_EMSTimeOut);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_FWDTimeOut(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_FWDTimeOut);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_TCUTimeOut(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_TCUTimeOut);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_HCUTimeOut(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_HCUTimeOut);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_MCUTimeOut(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_MCUTimeOut);
    Arbitrator_Rly_Read_Arbitrator_RlyEemFailData_Eem_Fail_VariantCoding(&Arbitrator_RlyBus.Arbitrator_RlyEemFailData.Eem_Fail_VariantCoding);

    Arbitrator_Rly_Read_Arbitrator_RlyWhlSpdInfo(&Arbitrator_RlyBus.Arbitrator_RlyWhlSpdInfo);
    /*==============================================================================
    * Members of structure Arbitrator_RlyWhlSpdInfo 
     : Arbitrator_RlyWhlSpdInfo.FlWhlSpd;
     : Arbitrator_RlyWhlSpdInfo.FrWhlSpd;
     : Arbitrator_RlyWhlSpdInfo.RlWhlSpd;
     : Arbitrator_RlyWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Arbitrator_Rly_Read_Arbitrator_RlyEcuModeSts(&Arbitrator_RlyBus.Arbitrator_RlyEcuModeSts);
    Arbitrator_Rly_Read_Arbitrator_RlyIgnOnOffSts(&Arbitrator_RlyBus.Arbitrator_RlyIgnOnOffSts);
    Arbitrator_Rly_Read_Arbitrator_RlyIgnEdgeSts(&Arbitrator_RlyBus.Arbitrator_RlyIgnEdgeSts);
    Arbitrator_Rly_Read_Arbitrator_RlyDiagSci(&Arbitrator_RlyBus.Arbitrator_RlyDiagSci);
    Arbitrator_Rly_Read_Arbitrator_RlyDiagAhbSci(&Arbitrator_RlyBus.Arbitrator_RlyDiagAhbSci);

    /* Process */

    /* Output */

    Arbitrator_Rly_Timer_Elapsed = STM0_TIM0.U - Arbitrator_Rly_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ARBITRATOR_RLY_STOP_SEC_CODE
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
