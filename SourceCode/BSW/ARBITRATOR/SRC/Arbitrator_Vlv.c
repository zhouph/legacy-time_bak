/**
 * @defgroup Arbitrator_Vlv Arbitrator_Vlv
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Vlv.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Vlv.h"
#include "Arbitrator_Vlv_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARBITRATOR_VLV_STOP_SEC_CONST_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Arbitrator_Vlv_HdrBusType Arbitrator_VlvBus;

/* Version Info */
const SwcVersionInfo_t Arbitrator_VlvVersionInfo = 
{   
    ARBITRATOR_VLV_MODULE_ID,           /* Arbitrator_VlvVersionInfo.ModuleId */
    ARBITRATOR_VLV_MAJOR_VERSION,       /* Arbitrator_VlvVersionInfo.MajorVer */
    ARBITRATOR_VLV_MINOR_VERSION,       /* Arbitrator_VlvVersionInfo.MinorVer */
    ARBITRATOR_VLV_PATCH_VERSION,       /* Arbitrator_VlvVersionInfo.PatchVer */
    ARBITRATOR_VLV_BRANCH_VERSION       /* Arbitrator_VlvVersionInfo.BranchVer */
};
    
/* Input Data Element */
BbsVlvM_MainFSBbsVlvActr_t Arbitrator_VlvFSBbsVlvActrInfo;
BbsVlvM_MainFSFsrBbsDrvInfo_t Arbitrator_VlvFSFsrBbsDrvInfo;
AbsVlvM_MainFSEscVlvActr_t Arbitrator_VlvFSEscVlvActrInfo;
AbsVlvM_MainFSFsrAbsDrvInfo_t Arbitrator_VlvFSFsrAbsDrvInfo;
Abc_CtrlWhlVlvReqAbcInfo_t Arbitrator_VlvWhlVlvReqAbcInfo;
Ses_CtrlNormVlvReqSesInfo_t Arbitrator_VlvNormVlvReqSesInfo;
Ses_CtrlWhlVlvReqSesInfo_t Arbitrator_VlvWhlVlvReqSesInfo;
Vat_CtrlNormVlvReqVlvActInfo_t Arbitrator_VlvNormVlvReqVlvActInfo;
Vat_CtrlWhlVlvReqIdbInfo_t Arbitrator_VlvWhlVlvReqIdbInfo;
Vat_CtrlIdbBalVlvReqInfo_t Arbitrator_VlvIdbBalVlvReqInfo;
Eem_MainEemFailData_t Arbitrator_VlvEemFailData;
Eem_MainEemCtrlInhibitData_t Arbitrator_VlvEemCtrlInhibitData;
Diag_HndlrDiagVlvActr_t Arbitrator_VlvDiagVlvActrInfo;
Wss_SenWhlSpdInfo_t Arbitrator_VlvWhlSpdInfo;
Mom_HndlrEcuModeSts_t Arbitrator_VlvEcuModeSts;
Prly_HndlrIgnOnOffSts_t Arbitrator_VlvIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Arbitrator_VlvIgnEdgeSts;
Acm_MainAcmAsicInitCompleteFlag_t Arbitrator_VlvAcmAsicInitCompleteFlag;
Diag_HndlrDiagSci_t Arbitrator_VlvDiagSci;
Diag_HndlrDiagAhbSci_t Arbitrator_VlvDiagAhbSci;
Eem_SuspcDetnFuncInhibitVlvSts_t Arbitrator_VlvFuncInhibitVlvSts;

/* Output Data Element */
Arbitrator_VlvArbFsrBbsDrvInfo_t Arbitrator_VlvArbFsrBbsDrvInfo;
Arbitrator_VlvArbFsrAbsDrvInfo_t Arbitrator_VlvArbFsrAbsDrvInfo;
Arbitrator_VlvArbWhlVlvReqInfo_t Arbitrator_VlvArbWhlVlvReqInfo;
Arbitrator_VlvArbNormVlvReqInfo_t Arbitrator_VlvArbNormVlvReqInfo;
Arbitrator_VlvArbBalVlvReqInfo_t Arbitrator_VlvArbBalVlvReqInfo;
Arbitrator_VlvArbResPVlvReqInfo_t Arbitrator_VlvArbResPVlvReqInfo;
Arbitrator_VlvArbAsicEnDrDrvInfo_t Arbitrator_VlvAsicEnDrDrv;
Arbitrator_VlvArbVlvSync_t Arbitrator_VlvArbVlvSync;
Arbitrator_VlvArbVlvDriveState_t Arbitrator_VlvArbVlvDriveState;

uint32 Arbitrator_Vlv_Timer_Start;
uint32 Arbitrator_Vlv_Timer_Elapsed;

#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_32BIT
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_UNSPECIFIED
#include "Arbitrator_MemMap.h"
#define ARBITRATOR_VLV_START_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/** Variable Section (32BIT)**/


#define ARBITRATOR_VLV_STOP_SEC_VAR_32BIT
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARBITRATOR_VLV_START_SEC_CODE
#include "Arbitrator_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Arbitrator_Vlv_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Arbitrator_VlvBus.Arbitrator_VlvFSBbsVlvActrInfo.fs_on_sim_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cutp_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cuts_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSBbsVlvActrInfo.fs_on_rlv_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cv_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_flno_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_frno_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlno_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrno_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_bal_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_pressdump_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_resp_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_flnc_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_frnc_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlnc_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrnc_time = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsDrv = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsOff = 0;
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[i] = 0;   
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FlIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FrIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RlIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RrIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FlOvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.FrOvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RlOvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo.RrOvDataLen = 0;
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[i] = 0;   
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo.SimVlvDataLen = 0;
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[i] = 0;   
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.FlIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.FrIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.RlIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo.RrIvDataLen = 0;
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[i] = 0;   
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FlIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FrIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RlIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RrIvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FlOvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FrOvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RlOvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RrOvDataLen = 0;
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[i] = 0;   
    Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvDataLen = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BBSSol = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ESCSol = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_FrontSol = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_RearSol = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Motor = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MPS = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MGD = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BBSValveRelay = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ESCValveRelay = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ECUHw = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ASIC = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_OverVolt = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_UnderVolt = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_LowVolt = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_LowerVolt = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_12V = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_5V = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Yaw = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Ay = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Ax = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Str = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_CirP1 = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_CirP2 = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SimP = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BLS = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ESCSw = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_HDCSw = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_AVHSw = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BrakeLampRelay = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_EssRelay = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_GearR = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Clutch = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ParkBrake = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_PedalPDT = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_PedalPDF = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BrakeFluid = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_TCSTemp = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_HDCTemp = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SCCTemp = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_TVBBTemp = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MainCanLine = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SubCanLine = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_EMSTimeOut = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_FWDTimeOut = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_TCUTimeOut = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_HCUTimeOut = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MCUTimeOut = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_VariantCoding = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Cbs = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Ebd = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Abs = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Edc = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Btcs = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Etcs = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Tcs = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Vdc = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hsa = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hdc = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Pba = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Avh = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Moc = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_AllControlInhibit = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DegradeModeFail = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.FlOvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.FlIvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.FrOvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.FrIvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.RlOvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.RlIvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.RrOvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.RrIvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlSpdInfo.FlWhlSpd = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlSpdInfo.FrWhlSpd = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlSpdInfo.RlWhlSpd = 0;
    Arbitrator_VlvBus.Arbitrator_VlvWhlSpdInfo.RrlWhlSpd = 0;
    Arbitrator_VlvBus.Arbitrator_VlvEcuModeSts = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIgnOnOffSts = 0;
    Arbitrator_VlvBus.Arbitrator_VlvIgnEdgeSts = 0;
    Arbitrator_VlvBus.Arbitrator_VlvAcmAsicInitCompleteFlag = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagSci = 0;
    Arbitrator_VlvBus.Arbitrator_VlvDiagAhbSci = 0;
    Arbitrator_VlvBus.Arbitrator_VlvFuncInhibitVlvSts = 0;
    Arbitrator_VlvBus.Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv = 0;
    Arbitrator_VlvBus.Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff = 0;
    Arbitrator_VlvBus.Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv = 0;
    Arbitrator_VlvBus.Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff = 0;
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Arbitrator_VlvBus.Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[i] = 0;   
    Arbitrator_VlvBus.Arbitrator_VlvAsicEnDrDrv = 0;
    Arbitrator_VlvBus.Arbitrator_VlvArbVlvSync = 0;
    Arbitrator_VlvBus.Arbitrator_VlvArbVlvDriveState = 0;

	Arbitrator_Vlv_Initialize();
}

void Arbitrator_Vlv(void)
{
    uint16 i;
    
    Arbitrator_Vlv_Timer_Start = STM0_TIM0.U;

    /* Input */
    Arbitrator_Vlv_Read_Arbitrator_VlvFSBbsVlvActrInfo(&Arbitrator_VlvBus.Arbitrator_VlvFSBbsVlvActrInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSBbsVlvActrInfo 
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_sim_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cutp_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cuts_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_rlv_time;
     : Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cv_time;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrBbsDrvInfo(&Arbitrator_VlvBus.Arbitrator_VlvFSFsrBbsDrvInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSFsrBbsDrvInfo 
     : Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv;
     : Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvFSEscVlvActrInfo(&Arbitrator_VlvBus.Arbitrator_VlvFSEscVlvActrInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSEscVlvActrInfo 
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_flno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_frno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrno_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_bal_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_pressdump_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_resp_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_flnc_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_frnc_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlnc_time;
     : Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrnc_time;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvFSFsrAbsDrvInfo(&Arbitrator_VlvBus.Arbitrator_VlvFSFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvFSFsrAbsDrvInfo 
     : Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsDrv;
     : Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqAbcInfo(&Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqAbcInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqAbcInfo 
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReq;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrIvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FlOvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.FrOvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RlOvDataLen;
     : Arbitrator_VlvWhlVlvReqAbcInfo.RrOvDataLen;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqSesInfo(&Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvNormVlvReqSesInfo 
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqSesInfo.SimVlvDataLen;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqSesInfo(&Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqSesInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqSesInfo 
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqSesInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqSesInfo.RrIvDataLen;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvNormVlvReqVlvActInfo(&Arbitrator_VlvBus.Arbitrator_VlvNormVlvReqVlvActInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvNormVlvReqVlvActInfo 
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvDataLen;
     : Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvDataLen;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvWhlVlvReqIdbInfo(&Arbitrator_VlvBus.Arbitrator_VlvWhlVlvReqIdbInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlVlvReqIdbInfo 
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrIvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrOvDataLen;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData;
     : Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvIdbBalVlvReqInfo(&Arbitrator_VlvBus.Arbitrator_VlvIdbBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvIdbBalVlvReqInfo 
     : Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData;
     : Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData;
     : Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData;
     : Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReq;
     : Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReq;
     : Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq;
     : Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvDataLen;
     : Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvDataLen;
     : Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvDataLen;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BBSSol(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BBSSol);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ESCSol(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ESCSol);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_FrontSol(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_FrontSol);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_RearSol(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_RearSol);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Motor(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Motor);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MPS(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MPS);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MGD(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MGD);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BBSValveRelay(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BBSValveRelay);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ESCValveRelay(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ESCValveRelay);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ECUHw(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ECUHw);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ASIC(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ASIC);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_OverVolt(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_OverVolt);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_UnderVolt(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_UnderVolt);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_LowVolt(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_LowVolt);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_LowerVolt(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_LowerVolt);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SenPwr_12V(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_12V);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SenPwr_5V(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_5V);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Yaw(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Yaw);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Ay(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Ay);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Ax(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Ax);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Str(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Str);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_CirP1(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_CirP1);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_CirP2(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_CirP2);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SimP(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SimP);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BLS(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BLS);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ESCSw(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ESCSw);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_HDCSw(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_HDCSw);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_AVHSw(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_AVHSw);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BrakeLampRelay(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BrakeLampRelay);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_EssRelay(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_EssRelay);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_GearR(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_GearR);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_Clutch(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_Clutch);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_ParkBrake(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_ParkBrake);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_PedalPDT(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_PedalPDT);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_PedalPDF(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_PedalPDF);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_BrakeFluid(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_BrakeFluid);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_TCSTemp(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_TCSTemp);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_HDCTemp(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_HDCTemp);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SCCTemp(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SCCTemp);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_TVBBTemp(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_TVBBTemp);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MainCanLine(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MainCanLine);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_SubCanLine(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_SubCanLine);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_EMSTimeOut(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_EMSTimeOut);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_FWDTimeOut(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_FWDTimeOut);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_TCUTimeOut(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_TCUTimeOut);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_HCUTimeOut(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_HCUTimeOut);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_MCUTimeOut(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_MCUTimeOut);
    Arbitrator_Vlv_Read_Arbitrator_VlvEemFailData_Eem_Fail_VariantCoding(&Arbitrator_VlvBus.Arbitrator_VlvEemFailData.Eem_Fail_VariantCoding);

    Arbitrator_Vlv_Read_Arbitrator_VlvEemCtrlInhibitData(&Arbitrator_VlvBus.Arbitrator_VlvEemCtrlInhibitData);
    /*==============================================================================
    * Members of structure Arbitrator_VlvEemCtrlInhibitData 
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvDiagVlvActrInfo(&Arbitrator_VlvBus.Arbitrator_VlvDiagVlvActrInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvDiagVlvActrInfo 
     : Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty;
     : Arbitrator_VlvDiagVlvActrInfo.FlOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FlIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FrOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.FrIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RlOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RlIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RrOvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RrIvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData;
     : Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData;
     : Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvWhlSpdInfo(&Arbitrator_VlvBus.Arbitrator_VlvWhlSpdInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvWhlSpdInfo 
     : Arbitrator_VlvWhlSpdInfo.FlWhlSpd;
     : Arbitrator_VlvWhlSpdInfo.FrWhlSpd;
     : Arbitrator_VlvWhlSpdInfo.RlWhlSpd;
     : Arbitrator_VlvWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Arbitrator_Vlv_Read_Arbitrator_VlvEcuModeSts(&Arbitrator_VlvBus.Arbitrator_VlvEcuModeSts);
    Arbitrator_Vlv_Read_Arbitrator_VlvIgnOnOffSts(&Arbitrator_VlvBus.Arbitrator_VlvIgnOnOffSts);
    Arbitrator_Vlv_Read_Arbitrator_VlvIgnEdgeSts(&Arbitrator_VlvBus.Arbitrator_VlvIgnEdgeSts);
    Arbitrator_Vlv_Read_Arbitrator_VlvAcmAsicInitCompleteFlag(&Arbitrator_VlvBus.Arbitrator_VlvAcmAsicInitCompleteFlag);
    Arbitrator_Vlv_Read_Arbitrator_VlvDiagSci(&Arbitrator_VlvBus.Arbitrator_VlvDiagSci);
    Arbitrator_Vlv_Read_Arbitrator_VlvDiagAhbSci(&Arbitrator_VlvBus.Arbitrator_VlvDiagAhbSci);
    Arbitrator_Vlv_Read_Arbitrator_VlvFuncInhibitVlvSts(&Arbitrator_VlvBus.Arbitrator_VlvFuncInhibitVlvSts);

    /* Process */
	Arbitrator_Vlv_Processing(&Arbitrator_VlvBus);

    /* Output */
    Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrBbsDrvInfo(&Arbitrator_VlvBus.Arbitrator_VlvArbFsrBbsDrvInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbFsrBbsDrvInfo 
     : Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv;
     : Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff;
     =============================================================================*/
    
    Arbitrator_Vlv_Write_Arbitrator_VlvArbFsrAbsDrvInfo(&Arbitrator_VlvBus.Arbitrator_VlvArbFsrAbsDrvInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbFsrAbsDrvInfo 
     : Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv;
     : Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff;
     =============================================================================*/
    
    Arbitrator_Vlv_Write_Arbitrator_VlvArbWhlVlvReqInfo(&Arbitrator_VlvBus.Arbitrator_VlvArbWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbWhlVlvReqInfo 
     : Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData;
     : Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/
    
    Arbitrator_Vlv_Write_Arbitrator_VlvArbNormVlvReqInfo(&Arbitrator_VlvBus.Arbitrator_VlvArbNormVlvReqInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbNormVlvReqInfo 
     : Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData;
     : Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData;
     : Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData;
     : Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData;
     : Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData;
     : Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData;
     =============================================================================*/
    
    Arbitrator_Vlv_Write_Arbitrator_VlvArbBalVlvReqInfo(&Arbitrator_VlvBus.Arbitrator_VlvArbBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbBalVlvReqInfo 
     : Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/
    
    Arbitrator_Vlv_Write_Arbitrator_VlvArbResPVlvReqInfo(&Arbitrator_VlvBus.Arbitrator_VlvArbResPVlvReqInfo);
    /*==============================================================================
    * Members of structure Arbitrator_VlvArbResPVlvReqInfo 
     : Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData;
     =============================================================================*/
    
    Arbitrator_Vlv_Write_Arbitrator_VlvAsicEnDrDrv(&Arbitrator_VlvBus.Arbitrator_VlvAsicEnDrDrv);
    Arbitrator_Vlv_Write_Arbitrator_VlvArbVlvSync(&Arbitrator_VlvBus.Arbitrator_VlvArbVlvSync);
    Arbitrator_Vlv_Write_Arbitrator_VlvArbVlvDriveState(&Arbitrator_VlvBus.Arbitrator_VlvArbVlvDriveState);

    Arbitrator_Vlv_Timer_Elapsed = STM0_TIM0.U - Arbitrator_Vlv_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ARBITRATOR_VLV_STOP_SEC_CODE
#include "Arbitrator_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
