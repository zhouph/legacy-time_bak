/**
 * @defgroup Arbitrator_Vlv Arbitrator_Vlv
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Vlv.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_PROCESSING_H_
#define ARBITRATOR_PROCESSING_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Types.h"
#include "Arbitrator_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define ARB_VLV_NONE        (0)
#define ARB_VLV_RESIDUAL    (1)
#define ARB_VLV_DIAG        (2)
#define ARB_VLV_LOGIC       (3)
#define ARB_VLV_FAILSAFE    (4)

#define ARB_VLV_MAX_CURRENT (2250)
#define ARB_VLV_MAX_DUTY    (10000)

#define ARB_VLV_WHEEL_NO_VLVDRV_SCALE   (2.25)  /* resl. : duty --> I Valve_SetI : 1 mA = 0.001A */
#define ARB_VLV_WHEEL_NC_VLVDRV_SCALE   (10)    /* resl. : 0.1 --> 0.01  Valve_SetDuty : 0.01 % */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef uint8 Arb_Vlv5msBufIdx_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Arbitrator_Vlv_Processing(Arbitrator_Vlv_HdrBusType *pArbVlvInfo);
extern void Arbitrator_Vlv_Initialize(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_PROCESSING_H_ */
/** @} */

