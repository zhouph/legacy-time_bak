Arbitrator_VlvFSBbsVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_sim_time'
    'fs_on_cutp_time'
    'fs_on_cuts_time'
    'fs_on_rlv_time'
    'fs_on_cv_time'
    };
Arbitrator_VlvFSBbsVlvActrInfo = CreateBus(Arbitrator_VlvFSBbsVlvActrInfo, DeList);
clear DeList;

Arbitrator_VlvFSFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
Arbitrator_VlvFSFsrBbsDrvInfo = CreateBus(Arbitrator_VlvFSFsrBbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvFSEscVlvActrInfo = Simulink.Bus;
DeList={
    'fs_on_flno_time'
    'fs_on_frno_time'
    'fs_on_rlno_time'
    'fs_on_rrno_time'
    'fs_on_bal_time'
    'fs_on_pressdump_time'
    'fs_on_resp_time'
    'fs_on_flnc_time'
    'fs_on_frnc_time'
    'fs_on_rlnc_time'
    'fs_on_rrnc_time'
    };
Arbitrator_VlvFSEscVlvActrInfo = CreateBus(Arbitrator_VlvFSEscVlvActrInfo, DeList);
clear DeList;

Arbitrator_VlvFSFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
Arbitrator_VlvFSFsrAbsDrvInfo = CreateBus(Arbitrator_VlvFSFsrAbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    };
Arbitrator_VlvWhlVlvReqAbcInfo = CreateBus(Arbitrator_VlvWhlVlvReqAbcInfo, DeList);
clear DeList;

Arbitrator_VlvNormVlvReqSesInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    };
Arbitrator_VlvNormVlvReqSesInfo = CreateBus(Arbitrator_VlvNormVlvReqSesInfo, DeList);
clear DeList;

Arbitrator_VlvWhlVlvReqSesInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    };
Arbitrator_VlvWhlVlvReqSesInfo = CreateBus(Arbitrator_VlvWhlVlvReqSesInfo, DeList);
clear DeList;

Arbitrator_VlvNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'RelsVlvReq'
    'CircVlvReq'
    'PressDumpVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    'CircVlvDataLen'
    'RelsVlvDataLen'
    'PressDumpVlvDataLen'
    };
Arbitrator_VlvNormVlvReqVlvActInfo = CreateBus(Arbitrator_VlvNormVlvReqVlvActInfo, DeList);
clear DeList;

Arbitrator_VlvWhlVlvReqIdbInfo = Simulink.Bus;
DeList={
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    };
Arbitrator_VlvWhlVlvReqIdbInfo = CreateBus(Arbitrator_VlvWhlVlvReqIdbInfo, DeList);
clear DeList;

Arbitrator_VlvIdbBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'SecdBalVlvReqData_array_0'
    'SecdBalVlvReqData_array_1'
    'SecdBalVlvReqData_array_2'
    'SecdBalVlvReqData_array_3'
    'SecdBalVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    'PrimBalVlvReq'
    'SecdBalVlvReq'
    'ChmbBalVlvReq'
    'PrimBalVlvDataLen'
    'SecdBalVlvDataLen'
    'ChmbBalVlvDataLen'
    };
Arbitrator_VlvIdbBalVlvReqInfo = CreateBus(Arbitrator_VlvIdbBalVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    };
Arbitrator_VlvEemFailData = CreateBus(Arbitrator_VlvEemFailData, DeList);
clear DeList;

Arbitrator_VlvEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Arbitrator_VlvEemCtrlInhibitData = CreateBus(Arbitrator_VlvEemCtrlInhibitData, DeList);
clear DeList;

Arbitrator_VlvDiagVlvActrInfo = Simulink.Bus;
DeList={
    'SolenoidDrvDuty'
    'FlOvReqData'
    'FlIvReqData'
    'FrOvReqData'
    'FrIvReqData'
    'RlOvReqData'
    'RlIvReqData'
    'RrOvReqData'
    'RrIvReqData'
    'PrimCutVlvReqData'
    'SecdCutVlvReqData'
    'SimVlvReqData'
    'ResPVlvReqData'
    'BalVlvReqData'
    'CircVlvReqData'
    'PressDumpReqData'
    'RelsVlvReqData'
    };
Arbitrator_VlvDiagVlvActrInfo = CreateBus(Arbitrator_VlvDiagVlvActrInfo, DeList);
clear DeList;

Arbitrator_VlvWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Arbitrator_VlvWhlSpdInfo = CreateBus(Arbitrator_VlvWhlSpdInfo, DeList);
clear DeList;

Arbitrator_VlvEcuModeSts = Simulink.Bus;
DeList={'Arbitrator_VlvEcuModeSts'};
Arbitrator_VlvEcuModeSts = CreateBus(Arbitrator_VlvEcuModeSts, DeList);
clear DeList;

Arbitrator_VlvIgnOnOffSts = Simulink.Bus;
DeList={'Arbitrator_VlvIgnOnOffSts'};
Arbitrator_VlvIgnOnOffSts = CreateBus(Arbitrator_VlvIgnOnOffSts, DeList);
clear DeList;

Arbitrator_VlvIgnEdgeSts = Simulink.Bus;
DeList={'Arbitrator_VlvIgnEdgeSts'};
Arbitrator_VlvIgnEdgeSts = CreateBus(Arbitrator_VlvIgnEdgeSts, DeList);
clear DeList;

Arbitrator_VlvAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Arbitrator_VlvAcmAsicInitCompleteFlag'};
Arbitrator_VlvAcmAsicInitCompleteFlag = CreateBus(Arbitrator_VlvAcmAsicInitCompleteFlag, DeList);
clear DeList;

Arbitrator_VlvDiagSci = Simulink.Bus;
DeList={'Arbitrator_VlvDiagSci'};
Arbitrator_VlvDiagSci = CreateBus(Arbitrator_VlvDiagSci, DeList);
clear DeList;

Arbitrator_VlvDiagAhbSci = Simulink.Bus;
DeList={'Arbitrator_VlvDiagAhbSci'};
Arbitrator_VlvDiagAhbSci = CreateBus(Arbitrator_VlvDiagAhbSci, DeList);
clear DeList;

Arbitrator_VlvFuncInhibitVlvSts = Simulink.Bus;
DeList={'Arbitrator_VlvFuncInhibitVlvSts'};
Arbitrator_VlvFuncInhibitVlvSts = CreateBus(Arbitrator_VlvFuncInhibitVlvSts, DeList);
clear DeList;

Arbitrator_VlvArbFsrBbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    'FsrCbsOff'
    };
Arbitrator_VlvArbFsrBbsDrvInfo = CreateBus(Arbitrator_VlvArbFsrBbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvArbFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    'FsrAbsOff'
    };
Arbitrator_VlvArbFsrAbsDrvInfo = CreateBus(Arbitrator_VlvArbFsrAbsDrvInfo, DeList);
clear DeList;

Arbitrator_VlvArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Arbitrator_VlvArbWhlVlvReqInfo = CreateBus(Arbitrator_VlvArbWhlVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    };
Arbitrator_VlvArbNormVlvReqInfo = CreateBus(Arbitrator_VlvArbNormVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvArbBalVlvReqInfo = Simulink.Bus;
DeList={
    'BalVlvReqData_array_0'
    'BalVlvReqData_array_1'
    'BalVlvReqData_array_2'
    'BalVlvReqData_array_3'
    'BalVlvReqData_array_4'
    };
Arbitrator_VlvArbBalVlvReqInfo = CreateBus(Arbitrator_VlvArbBalVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvArbResPVlvReqInfo = Simulink.Bus;
DeList={
    'ResPVlvReqData_array_0'
    'ResPVlvReqData_array_1'
    'ResPVlvReqData_array_2'
    'ResPVlvReqData_array_3'
    'ResPVlvReqData_array_4'
    };
Arbitrator_VlvArbResPVlvReqInfo = CreateBus(Arbitrator_VlvArbResPVlvReqInfo, DeList);
clear DeList;

Arbitrator_VlvAsicEnDrDrv = Simulink.Bus;
DeList={'Arbitrator_VlvAsicEnDrDrv'};
Arbitrator_VlvAsicEnDrDrv = CreateBus(Arbitrator_VlvAsicEnDrDrv, DeList);
clear DeList;

Arbitrator_VlvArbVlvSync = Simulink.Bus;
DeList={'Arbitrator_VlvArbVlvSync'};
Arbitrator_VlvArbVlvSync = CreateBus(Arbitrator_VlvArbVlvSync, DeList);
clear DeList;

Arbitrator_VlvArbVlvDriveState = Simulink.Bus;
DeList={'Arbitrator_VlvArbVlvDriveState'};
Arbitrator_VlvArbVlvDriveState = CreateBus(Arbitrator_VlvArbVlvDriveState, DeList);
clear DeList;

