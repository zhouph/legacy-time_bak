Arbitrator_RlyEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    };
Arbitrator_RlyEemFailData = CreateBus(Arbitrator_RlyEemFailData, DeList);
clear DeList;

Arbitrator_RlyWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Arbitrator_RlyWhlSpdInfo = CreateBus(Arbitrator_RlyWhlSpdInfo, DeList);
clear DeList;

Arbitrator_RlyEcuModeSts = Simulink.Bus;
DeList={'Arbitrator_RlyEcuModeSts'};
Arbitrator_RlyEcuModeSts = CreateBus(Arbitrator_RlyEcuModeSts, DeList);
clear DeList;

Arbitrator_RlyIgnOnOffSts = Simulink.Bus;
DeList={'Arbitrator_RlyIgnOnOffSts'};
Arbitrator_RlyIgnOnOffSts = CreateBus(Arbitrator_RlyIgnOnOffSts, DeList);
clear DeList;

Arbitrator_RlyIgnEdgeSts = Simulink.Bus;
DeList={'Arbitrator_RlyIgnEdgeSts'};
Arbitrator_RlyIgnEdgeSts = CreateBus(Arbitrator_RlyIgnEdgeSts, DeList);
clear DeList;

Arbitrator_RlyDiagSci = Simulink.Bus;
DeList={'Arbitrator_RlyDiagSci'};
Arbitrator_RlyDiagSci = CreateBus(Arbitrator_RlyDiagSci, DeList);
clear DeList;

Arbitrator_RlyDiagAhbSci = Simulink.Bus;
DeList={'Arbitrator_RlyDiagAhbSci'};
Arbitrator_RlyDiagAhbSci = CreateBus(Arbitrator_RlyDiagAhbSci, DeList);
clear DeList;

