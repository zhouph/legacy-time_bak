Arbitrator_MtrEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    };
Arbitrator_MtrEemFailData = CreateBus(Arbitrator_MtrEemFailData, DeList);
clear DeList;

Arbitrator_MtrEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Arbitrator_MtrEemCtrlInhibitData = CreateBus(Arbitrator_MtrEemCtrlInhibitData, DeList);
clear DeList;

Arbitrator_MtrMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Arbitrator_MtrMotDqIRefMccInfo = CreateBus(Arbitrator_MtrMotDqIRefMccInfo, DeList);
clear DeList;

Arbitrator_MtrMtrProcessOutData = Simulink.Bus;
DeList={
    'MtrProIqFailsafe'
    'MtrProIdFailsafe'
    'MtrProUPhasePWM'
    'MtrProVPhasePWM'
    'MtrProWPhasePWM'
    };
Arbitrator_MtrMtrProcessOutData = CreateBus(Arbitrator_MtrMtrProcessOutData, DeList);
clear DeList;

Arbitrator_MtrMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    };
Arbitrator_MtrMtrProcessDataInf = CreateBus(Arbitrator_MtrMtrProcessDataInf, DeList);
clear DeList;

Arbitrator_MtrEcuModeSts = Simulink.Bus;
DeList={'Arbitrator_MtrEcuModeSts'};
Arbitrator_MtrEcuModeSts = CreateBus(Arbitrator_MtrEcuModeSts, DeList);
clear DeList;

Arbitrator_MtrIgnOnOffSts = Simulink.Bus;
DeList={'Arbitrator_MtrIgnOnOffSts'};
Arbitrator_MtrIgnOnOffSts = CreateBus(Arbitrator_MtrIgnOnOffSts, DeList);
clear DeList;

Arbitrator_MtrIgnEdgeSts = Simulink.Bus;
DeList={'Arbitrator_MtrIgnEdgeSts'};
Arbitrator_MtrIgnEdgeSts = CreateBus(Arbitrator_MtrIgnEdgeSts, DeList);
clear DeList;

Arbitrator_MtrDiagSci = Simulink.Bus;
DeList={'Arbitrator_MtrDiagSci'};
Arbitrator_MtrDiagSci = CreateBus(Arbitrator_MtrDiagSci, DeList);
clear DeList;

Arbitrator_MtrDiagAhbSci = Simulink.Bus;
DeList={'Arbitrator_MtrDiagAhbSci'};
Arbitrator_MtrDiagAhbSci = CreateBus(Arbitrator_MtrDiagAhbSci, DeList);
clear DeList;

Arbitrator_MtrVdcLinkFild = Simulink.Bus;
DeList={'Arbitrator_MtrVdcLinkFild'};
Arbitrator_MtrVdcLinkFild = CreateBus(Arbitrator_MtrVdcLinkFild, DeList);
clear DeList;

Arbitrator_MtrMotCtrlMode = Simulink.Bus;
DeList={'Arbitrator_MtrMotCtrlMode'};
Arbitrator_MtrMotCtrlMode = CreateBus(Arbitrator_MtrMotCtrlMode, DeList);
clear DeList;

Arbitrator_MtrMotCtrlState = Simulink.Bus;
DeList={'Arbitrator_MtrMotCtrlState'};
Arbitrator_MtrMotCtrlState = CreateBus(Arbitrator_MtrMotCtrlState, DeList);
clear DeList;

Arbitrator_MtrMtrArbtratorData = Simulink.Bus;
DeList={
    'IqSelected'
    'IdSelected'
    'UPhasePWMSelected'
    'VPhasePWMSelected'
    'WPhasePWMSelected'
    'MtrCtrlMode'
    };
Arbitrator_MtrMtrArbtratorData = CreateBus(Arbitrator_MtrMtrArbtratorData, DeList);
clear DeList;

Arbitrator_MtrMtrArbtratorInfo = Simulink.Bus;
DeList={
    'MtrArbCalMode'
    'MtrArbState'
    };
Arbitrator_MtrMtrArbtratorInfo = CreateBus(Arbitrator_MtrMtrArbtratorInfo, DeList);
clear DeList;

Arbitrator_MtrMtrArbDriveState = Simulink.Bus;
DeList={'Arbitrator_MtrMtrArbDriveState'};
Arbitrator_MtrMtrArbDriveState = CreateBus(Arbitrator_MtrMtrArbDriveState, DeList);
clear DeList;

