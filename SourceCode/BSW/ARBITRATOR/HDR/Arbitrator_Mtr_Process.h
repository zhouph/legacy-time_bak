/**
 * @defgroup Arbitrator_Mtr Arbitrator_Mtr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Mtr_Process.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/
#ifndef ARBITRATOR_MTR_PROCESS_H_
#define ARBITRATOR_MTR_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Types.h"
#include "Arbitrator_Cfg.h"
#include "Mtr_Processing_Diag_Process.h"

#define Salsint32Init	0
#define DEF_DIAG_STATE 9000
#define DEF_CURRENT_CONTROL 1
#define DEF_DUTY_CONTROL        0
#define DEF_HALF_DUTY           2499

extern void Arbitrator_Mtr_Process(Arbitrator_Mtr_HdrBusType *pArbitrator_Mtr_HdrBusType);
#endif