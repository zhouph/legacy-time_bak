/**
 * @defgroup Arbitrator_MemMap Arbitrator_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_MEMMAP_H_
#define ARBITRATOR_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_START_SEC_CODE)
  #undef ARBITRATOR_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_CODE)
  #undef ARBITRATOR_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_CODE_STARTED
    #error "ARBITRATOR_SEC_CODE not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_START_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_CONST_UNSPECIFIED_STARTED
    #error "ARBITRATOR_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_START_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_VAR_UNSPECIFIED_STARTED
    #error "ARBITRATOR_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_START_SEC_VAR_32BIT)
  #undef ARBITRATOR_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_VAR_32BIT)
  #undef ARBITRATOR_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_VAR_32BIT_STARTED
    #error "ARBITRATOR_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ARBITRATOR_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_START_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ARBITRATOR_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_START_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_SEC_STARTED
    #error "ARBITRATOR section not closed"
  #endif
  #define CHK_ARBITRATOR_SEC_STARTED
  #define CHK_ARBITRATOR_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ARBITRATOR_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_SEC_STARTED
  #undef CHK_ARBITRATOR_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_MTR_START_SEC_CODE)
  #undef ARBITRATOR_MTR_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_CODE)
  #undef ARBITRATOR_MTR_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_CODE_STARTED
    #error "ARBITRATOR_MTR_SEC_CODE not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_MTR_START_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_MTR_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_MTR_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_CONST_UNSPECIFIED_STARTED
    #error "ARBITRATOR_MTR_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_MTR_START_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_MTR_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_MTR_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_VAR_UNSPECIFIED_STARTED
    #error "ARBITRATOR_MTR_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_START_SEC_VAR_32BIT)
  #undef ARBITRATOR_MTR_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_VAR_32BIT)
  #undef ARBITRATOR_MTR_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_VAR_32BIT_STARTED
    #error "ARBITRATOR_MTR_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_MTR_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ARBITRATOR_MTR_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_START_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_MTR_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_MTR_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ARBITRATOR_MTR_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_MTR_START_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_MTR_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_MTR_SEC_STARTED
    #error "ARBITRATOR_MTR section not closed"
  #endif
  #define CHK_ARBITRATOR_MTR_SEC_STARTED
  #define CHK_ARBITRATOR_MTR_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_MTR_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_MTR_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_MTR_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ARBITRATOR_MTR_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_MTR_SEC_STARTED
  #undef CHK_ARBITRATOR_MTR_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_VLV_START_SEC_CODE)
  #undef ARBITRATOR_VLV_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_CODE)
  #undef ARBITRATOR_VLV_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_CODE_STARTED
    #error "ARBITRATOR_VLV_SEC_CODE not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_VLV_START_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_VLV_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_VLV_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_CONST_UNSPECIFIED_STARTED
    #error "ARBITRATOR_VLV_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_VLV_START_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_VLV_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_VLV_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_VAR_UNSPECIFIED_STARTED
    #error "ARBITRATOR_VLV_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_START_SEC_VAR_32BIT)
  #undef ARBITRATOR_VLV_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_VAR_32BIT)
  #undef ARBITRATOR_VLV_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_VAR_32BIT_STARTED
    #error "ARBITRATOR_VLV_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ARBITRATOR_VLV_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_START_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_VLV_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_VLV_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ARBITRATOR_VLV_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_VLV_START_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_VLV_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_VLV_SEC_STARTED
    #error "ARBITRATOR_VLV section not closed"
  #endif
  #define CHK_ARBITRATOR_VLV_SEC_STARTED
  #define CHK_ARBITRATOR_VLV_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_VLV_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_VLV_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_VLV_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ARBITRATOR_VLV_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_VLV_SEC_STARTED
  #undef CHK_ARBITRATOR_VLV_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_RLY_START_SEC_CODE)
  #undef ARBITRATOR_RLY_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_CODE)
  #undef ARBITRATOR_RLY_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_CODE_STARTED
    #error "ARBITRATOR_RLY_SEC_CODE not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_RLY_START_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_RLY_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_CONST_UNSPECIFIED)
  #undef ARBITRATOR_RLY_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_CONST_UNSPECIFIED_STARTED
    #error "ARBITRATOR_RLY_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_RLY_START_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_RLY_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_VAR_UNSPECIFIED)
  #undef ARBITRATOR_RLY_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_VAR_UNSPECIFIED_STARTED
    #error "ARBITRATOR_RLY_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_START_SEC_VAR_32BIT)
  #undef ARBITRATOR_RLY_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_VAR_32BIT)
  #undef ARBITRATOR_RLY_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_VAR_32BIT_STARTED
    #error "ARBITRATOR_RLY_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_RLY_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ARBITRATOR_RLY_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_START_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_RLY_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ARBITRATOR_RLY_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ARBITRATOR_RLY_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ARBITRATOR_RLY_START_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_RLY_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ARBITRATOR_RLY_SEC_STARTED
    #error "ARBITRATOR_RLY section not closed"
  #endif
  #define CHK_ARBITRATOR_RLY_SEC_STARTED
  #define CHK_ARBITRATOR_RLY_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ARBITRATOR_RLY_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ARBITRATOR_RLY_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ARBITRATOR_RLY_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ARBITRATOR_RLY_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ARBITRATOR_RLY_SEC_STARTED
  #undef CHK_ARBITRATOR_RLY_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_MEMMAP_H_ */
/** @} */
