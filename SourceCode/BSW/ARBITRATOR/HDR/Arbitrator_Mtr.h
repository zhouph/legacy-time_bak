/**
 * @defgroup Arbitrator_Mtr Arbitrator_Mtr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Mtr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_MTR_H_
#define ARBITRATOR_MTR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Types.h"
#include "Arbitrator_Cfg.h"
#include "Arbitrator_Cal.h"
#include "Arbitrator_Mtr_Process.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ARBITRATOR_MTR_MODULE_ID      (0)
 #define ARBITRATOR_MTR_MAJOR_VERSION  (2)
 #define ARBITRATOR_MTR_MINOR_VERSION  (0)
 #define ARBITRATOR_MTR_PATCH_VERSION  (0)
 #define ARBITRATOR_MTR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Arbitrator_Mtr_HdrBusType Arbitrator_MtrBus;

/* Version Info */
extern const SwcVersionInfo_t Arbitrator_MtrVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t Arbitrator_MtrEemFailData;
extern Eem_MainEemCtrlInhibitData_t Arbitrator_MtrEemCtrlInhibitData;
extern Mcc_1msCtrlMotDqIRefMccInfo_t Arbitrator_MtrMotDqIRefMccInfo;
extern Mtr_Processing_DiagMtrProcessOutData_t Arbitrator_MtrMtrProcessOutData;
extern Mtr_Processing_DiagMtrProcessDataInfo_t Arbitrator_MtrMtrProcessDataInf;
extern Mom_HndlrEcuModeSts_t Arbitrator_MtrEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Arbitrator_MtrIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Arbitrator_MtrIgnEdgeSts;
extern Diag_HndlrDiagSci_t Arbitrator_MtrDiagSci;
extern Diag_HndlrDiagAhbSci_t Arbitrator_MtrDiagAhbSci;
extern Msp_1msCtrlVdcLinkFild_t Arbitrator_MtrVdcLinkFild;
extern Mcc_1msCtrlMotCtrlMode_t Arbitrator_MtrMotCtrlMode;
extern Mcc_1msCtrlMotCtrlState_t Arbitrator_MtrMotCtrlState;

/* Output Data Element */
extern Arbitrator_MtrMtrArbitratorData_t Arbitrator_MtrMtrArbtratorData;
extern Arbitrator_MtrMtrArbitratorInfo_t Arbitrator_MtrMtrArbtratorInfo;
extern Arbitrator_MtrMtrArbDriveState_t Arbitrator_MtrMtrArbDriveState;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Arbitrator_Mtr_Init(void);
extern void Arbitrator_Mtr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_MTR_H_ */
/** @} */
