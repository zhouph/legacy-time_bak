/**
 * @defgroup Arbitrator_Vlv Arbitrator_Vlv
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Vlv.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_VLV_H_
#define ARBITRATOR_VLV_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Types.h"
#include "Arbitrator_Cfg.h"
#include "Arbitrator_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ARBITRATOR_VLV_MODULE_ID      (0)
 #define ARBITRATOR_VLV_MAJOR_VERSION  (2)
 #define ARBITRATOR_VLV_MINOR_VERSION  (0)
 #define ARBITRATOR_VLV_PATCH_VERSION  (0)
 #define ARBITRATOR_VLV_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Arbitrator_Vlv_HdrBusType Arbitrator_VlvBus;

/* Version Info */
extern const SwcVersionInfo_t Arbitrator_VlvVersionInfo;

/* Input Data Element */
extern BbsVlvM_MainFSBbsVlvActr_t Arbitrator_VlvFSBbsVlvActrInfo;
extern BbsVlvM_MainFSFsrBbsDrvInfo_t Arbitrator_VlvFSFsrBbsDrvInfo;
extern AbsVlvM_MainFSEscVlvActr_t Arbitrator_VlvFSEscVlvActrInfo;
extern AbsVlvM_MainFSFsrAbsDrvInfo_t Arbitrator_VlvFSFsrAbsDrvInfo;
extern Abc_CtrlWhlVlvReqAbcInfo_t Arbitrator_VlvWhlVlvReqAbcInfo;
extern Ses_CtrlNormVlvReqSesInfo_t Arbitrator_VlvNormVlvReqSesInfo;
extern Ses_CtrlWhlVlvReqSesInfo_t Arbitrator_VlvWhlVlvReqSesInfo;
extern Vat_CtrlNormVlvReqVlvActInfo_t Arbitrator_VlvNormVlvReqVlvActInfo;
extern Vat_CtrlWhlVlvReqIdbInfo_t Arbitrator_VlvWhlVlvReqIdbInfo;
extern Vat_CtrlIdbBalVlvReqInfo_t Arbitrator_VlvIdbBalVlvReqInfo;
extern Eem_MainEemFailData_t Arbitrator_VlvEemFailData;
extern Eem_MainEemCtrlInhibitData_t Arbitrator_VlvEemCtrlInhibitData;
extern Diag_HndlrDiagVlvActr_t Arbitrator_VlvDiagVlvActrInfo;
extern Wss_SenWhlSpdInfo_t Arbitrator_VlvWhlSpdInfo;
extern Mom_HndlrEcuModeSts_t Arbitrator_VlvEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Arbitrator_VlvIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Arbitrator_VlvIgnEdgeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Arbitrator_VlvAcmAsicInitCompleteFlag;
extern Diag_HndlrDiagSci_t Arbitrator_VlvDiagSci;
extern Diag_HndlrDiagAhbSci_t Arbitrator_VlvDiagAhbSci;
extern Eem_SuspcDetnFuncInhibitVlvSts_t Arbitrator_VlvFuncInhibitVlvSts;

/* Output Data Element */
extern Arbitrator_VlvArbFsrBbsDrvInfo_t Arbitrator_VlvArbFsrBbsDrvInfo;
extern Arbitrator_VlvArbFsrAbsDrvInfo_t Arbitrator_VlvArbFsrAbsDrvInfo;
extern Arbitrator_VlvArbWhlVlvReqInfo_t Arbitrator_VlvArbWhlVlvReqInfo;
extern Arbitrator_VlvArbNormVlvReqInfo_t Arbitrator_VlvArbNormVlvReqInfo;
extern Arbitrator_VlvArbBalVlvReqInfo_t Arbitrator_VlvArbBalVlvReqInfo;
extern Arbitrator_VlvArbResPVlvReqInfo_t Arbitrator_VlvArbResPVlvReqInfo;
extern Arbitrator_VlvArbAsicEnDrDrvInfo_t Arbitrator_VlvAsicEnDrDrv;
extern Arbitrator_VlvArbVlvSync_t Arbitrator_VlvArbVlvSync;
extern Arbitrator_VlvArbVlvDriveState_t Arbitrator_VlvArbVlvDriveState;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Arbitrator_Vlv_Init(void);
extern void Arbitrator_Vlv(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_VLV_H_ */
/** @} */
