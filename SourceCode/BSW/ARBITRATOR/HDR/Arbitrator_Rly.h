/**
 * @defgroup Arbitrator_Rly Arbitrator_Rly
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Rly.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_RLY_H_
#define ARBITRATOR_RLY_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arbitrator_Types.h"
#include "Arbitrator_Cfg.h"
#include "Arbitrator_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ARBITRATOR_RLY_MODULE_ID      (0)
 #define ARBITRATOR_RLY_MAJOR_VERSION  (2)
 #define ARBITRATOR_RLY_MINOR_VERSION  (0)
 #define ARBITRATOR_RLY_PATCH_VERSION  (0)
 #define ARBITRATOR_RLY_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Arbitrator_Rly_HdrBusType Arbitrator_RlyBus;

/* Version Info */
extern const SwcVersionInfo_t Arbitrator_RlyVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t Arbitrator_RlyEemFailData;
extern Wss_SenWhlSpdInfo_t Arbitrator_RlyWhlSpdInfo;
extern Mom_HndlrEcuModeSts_t Arbitrator_RlyEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Arbitrator_RlyIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Arbitrator_RlyIgnEdgeSts;
extern Diag_HndlrDiagSci_t Arbitrator_RlyDiagSci;
extern Diag_HndlrDiagAhbSci_t Arbitrator_RlyDiagAhbSci;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Arbitrator_Rly_Init(void);
extern void Arbitrator_Rly(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_RLY_H_ */
/** @} */
