/**
 * @defgroup Arbitrator_Types Arbitrator_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arbitrator_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARBITRATOR_TYPES_H_
#define ARBITRATOR_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t Arbitrator_MtrEemFailData;
    Eem_MainEemCtrlInhibitData_t Arbitrator_MtrEemCtrlInhibitData;
    Mcc_1msCtrlMotDqIRefMccInfo_t Arbitrator_MtrMotDqIRefMccInfo;
    Mtr_Processing_DiagMtrProcessOutData_t Arbitrator_MtrMtrProcessOutData;
    Mtr_Processing_DiagMtrProcessDataInfo_t Arbitrator_MtrMtrProcessDataInf;
    Mom_HndlrEcuModeSts_t Arbitrator_MtrEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Arbitrator_MtrIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Arbitrator_MtrIgnEdgeSts;
    Diag_HndlrDiagSci_t Arbitrator_MtrDiagSci;
    Diag_HndlrDiagAhbSci_t Arbitrator_MtrDiagAhbSci;
    Msp_1msCtrlVdcLinkFild_t Arbitrator_MtrVdcLinkFild;
    Mcc_1msCtrlMotCtrlMode_t Arbitrator_MtrMotCtrlMode;
    Mcc_1msCtrlMotCtrlState_t Arbitrator_MtrMotCtrlState;

/* Output Data Element */
    Arbitrator_MtrMtrArbitratorData_t Arbitrator_MtrMtrArbtratorData;
    Arbitrator_MtrMtrArbitratorInfo_t Arbitrator_MtrMtrArbtratorInfo;
    Arbitrator_MtrMtrArbDriveState_t Arbitrator_MtrMtrArbDriveState;
}Arbitrator_Mtr_HdrBusType;

typedef struct
{
/* Input Data Element */
    BbsVlvM_MainFSBbsVlvActr_t Arbitrator_VlvFSBbsVlvActrInfo;
    BbsVlvM_MainFSFsrBbsDrvInfo_t Arbitrator_VlvFSFsrBbsDrvInfo;
    AbsVlvM_MainFSEscVlvActr_t Arbitrator_VlvFSEscVlvActrInfo;
    AbsVlvM_MainFSFsrAbsDrvInfo_t Arbitrator_VlvFSFsrAbsDrvInfo;
    Abc_CtrlWhlVlvReqAbcInfo_t Arbitrator_VlvWhlVlvReqAbcInfo;
    Ses_CtrlNormVlvReqSesInfo_t Arbitrator_VlvNormVlvReqSesInfo;
    Ses_CtrlWhlVlvReqSesInfo_t Arbitrator_VlvWhlVlvReqSesInfo;
    Vat_CtrlNormVlvReqVlvActInfo_t Arbitrator_VlvNormVlvReqVlvActInfo;
    Vat_CtrlWhlVlvReqIdbInfo_t Arbitrator_VlvWhlVlvReqIdbInfo;
    Vat_CtrlIdbBalVlvReqInfo_t Arbitrator_VlvIdbBalVlvReqInfo;
    Eem_MainEemFailData_t Arbitrator_VlvEemFailData;
    Eem_MainEemCtrlInhibitData_t Arbitrator_VlvEemCtrlInhibitData;
    Diag_HndlrDiagVlvActr_t Arbitrator_VlvDiagVlvActrInfo;
    Wss_SenWhlSpdInfo_t Arbitrator_VlvWhlSpdInfo;
    Mom_HndlrEcuModeSts_t Arbitrator_VlvEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Arbitrator_VlvIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Arbitrator_VlvIgnEdgeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Arbitrator_VlvAcmAsicInitCompleteFlag;
    Diag_HndlrDiagSci_t Arbitrator_VlvDiagSci;
    Diag_HndlrDiagAhbSci_t Arbitrator_VlvDiagAhbSci;
    Eem_SuspcDetnFuncInhibitVlvSts_t Arbitrator_VlvFuncInhibitVlvSts;

/* Output Data Element */
    Arbitrator_VlvArbFsrBbsDrvInfo_t Arbitrator_VlvArbFsrBbsDrvInfo;
    Arbitrator_VlvArbFsrAbsDrvInfo_t Arbitrator_VlvArbFsrAbsDrvInfo;
    Arbitrator_VlvArbWhlVlvReqInfo_t Arbitrator_VlvArbWhlVlvReqInfo;
    Arbitrator_VlvArbNormVlvReqInfo_t Arbitrator_VlvArbNormVlvReqInfo;
    Arbitrator_VlvArbBalVlvReqInfo_t Arbitrator_VlvArbBalVlvReqInfo;
    Arbitrator_VlvArbResPVlvReqInfo_t Arbitrator_VlvArbResPVlvReqInfo;
    Arbitrator_VlvArbAsicEnDrDrvInfo_t Arbitrator_VlvAsicEnDrDrv;
    Arbitrator_VlvArbVlvSync_t Arbitrator_VlvArbVlvSync;
    Arbitrator_VlvArbVlvDriveState_t Arbitrator_VlvArbVlvDriveState;
}Arbitrator_Vlv_HdrBusType;

typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t Arbitrator_RlyEemFailData;
    Wss_SenWhlSpdInfo_t Arbitrator_RlyWhlSpdInfo;
    Mom_HndlrEcuModeSts_t Arbitrator_RlyEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Arbitrator_RlyIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Arbitrator_RlyIgnEdgeSts;
    Diag_HndlrDiagSci_t Arbitrator_RlyDiagSci;
    Diag_HndlrDiagAhbSci_t Arbitrator_RlyDiagAhbSci;

/* Output Data Element */
}Arbitrator_Rly_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARBITRATOR_TYPES_H_ */
/** @} */
