#define S_FUNCTION_NAME      Arbitrator_Rly_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          53
#define WidthOutputPort         0

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Arbitrator_Rly.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Arbitrator_RlyEemFailData.Eem_Fail_Motor = input[0];
    Arbitrator_RlyEemFailData.Eem_Fail_MPS = input[1];
    Arbitrator_RlyEemFailData.Eem_Fail_MGD = input[2];
    Arbitrator_RlyEemFailData.Eem_Fail_BBSValveRelay = input[3];
    Arbitrator_RlyEemFailData.Eem_Fail_ESCValveRelay = input[4];
    Arbitrator_RlyEemFailData.Eem_Fail_ECUHw = input[5];
    Arbitrator_RlyEemFailData.Eem_Fail_ASIC = input[6];
    Arbitrator_RlyEemFailData.Eem_Fail_OverVolt = input[7];
    Arbitrator_RlyEemFailData.Eem_Fail_UnderVolt = input[8];
    Arbitrator_RlyEemFailData.Eem_Fail_LowVolt = input[9];
    Arbitrator_RlyEemFailData.Eem_Fail_LowerVolt = input[10];
    Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_12V = input[11];
    Arbitrator_RlyEemFailData.Eem_Fail_SenPwr_5V = input[12];
    Arbitrator_RlyEemFailData.Eem_Fail_Yaw = input[13];
    Arbitrator_RlyEemFailData.Eem_Fail_Ay = input[14];
    Arbitrator_RlyEemFailData.Eem_Fail_Ax = input[15];
    Arbitrator_RlyEemFailData.Eem_Fail_Str = input[16];
    Arbitrator_RlyEemFailData.Eem_Fail_CirP1 = input[17];
    Arbitrator_RlyEemFailData.Eem_Fail_CirP2 = input[18];
    Arbitrator_RlyEemFailData.Eem_Fail_SimP = input[19];
    Arbitrator_RlyEemFailData.Eem_Fail_BLS = input[20];
    Arbitrator_RlyEemFailData.Eem_Fail_ESCSw = input[21];
    Arbitrator_RlyEemFailData.Eem_Fail_HDCSw = input[22];
    Arbitrator_RlyEemFailData.Eem_Fail_AVHSw = input[23];
    Arbitrator_RlyEemFailData.Eem_Fail_BrakeLampRelay = input[24];
    Arbitrator_RlyEemFailData.Eem_Fail_EssRelay = input[25];
    Arbitrator_RlyEemFailData.Eem_Fail_GearR = input[26];
    Arbitrator_RlyEemFailData.Eem_Fail_Clutch = input[27];
    Arbitrator_RlyEemFailData.Eem_Fail_ParkBrake = input[28];
    Arbitrator_RlyEemFailData.Eem_Fail_PedalPDT = input[29];
    Arbitrator_RlyEemFailData.Eem_Fail_PedalPDF = input[30];
    Arbitrator_RlyEemFailData.Eem_Fail_BrakeFluid = input[31];
    Arbitrator_RlyEemFailData.Eem_Fail_TCSTemp = input[32];
    Arbitrator_RlyEemFailData.Eem_Fail_HDCTemp = input[33];
    Arbitrator_RlyEemFailData.Eem_Fail_SCCTemp = input[34];
    Arbitrator_RlyEemFailData.Eem_Fail_TVBBTemp = input[35];
    Arbitrator_RlyEemFailData.Eem_Fail_MainCanLine = input[36];
    Arbitrator_RlyEemFailData.Eem_Fail_SubCanLine = input[37];
    Arbitrator_RlyEemFailData.Eem_Fail_EMSTimeOut = input[38];
    Arbitrator_RlyEemFailData.Eem_Fail_FWDTimeOut = input[39];
    Arbitrator_RlyEemFailData.Eem_Fail_TCUTimeOut = input[40];
    Arbitrator_RlyEemFailData.Eem_Fail_HCUTimeOut = input[41];
    Arbitrator_RlyEemFailData.Eem_Fail_MCUTimeOut = input[42];
    Arbitrator_RlyEemFailData.Eem_Fail_VariantCoding = input[43];
    Arbitrator_RlyWhlSpdInfo.FlWhlSpd = input[44];
    Arbitrator_RlyWhlSpdInfo.FrWhlSpd = input[45];
    Arbitrator_RlyWhlSpdInfo.RlWhlSpd = input[46];
    Arbitrator_RlyWhlSpdInfo.RrlWhlSpd = input[47];
    Arbitrator_RlyEcuModeSts = input[48];
    Arbitrator_RlyIgnOnOffSts = input[49];
    Arbitrator_RlyIgnEdgeSts = input[50];
    Arbitrator_RlyDiagSci = input[51];
    Arbitrator_RlyDiagAhbSci = input[52];

    Arbitrator_Rly();


    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Arbitrator_Rly_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
