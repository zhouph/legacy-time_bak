#define S_FUNCTION_NAME      Arbitrator_Vlv_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          405
#define WidthOutputPort         87

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Arbitrator_Vlv.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Arbitrator_VlvFSBbsVlvActrInfo.fs_on_sim_time = input[0];
    Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cutp_time = input[1];
    Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cuts_time = input[2];
    Arbitrator_VlvFSBbsVlvActrInfo.fs_on_rlv_time = input[3];
    Arbitrator_VlvFSBbsVlvActrInfo.fs_on_cv_time = input[4];
    Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsDrv = input[5];
    Arbitrator_VlvFSFsrBbsDrvInfo.FsrCbsOff = input[6];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_flno_time = input[7];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_frno_time = input[8];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlno_time = input[9];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrno_time = input[10];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_bal_time = input[11];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_pressdump_time = input[12];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_resp_time = input[13];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_flnc_time = input[14];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_frnc_time = input[15];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_rlnc_time = input[16];
    Arbitrator_VlvFSEscVlvActrInfo.fs_on_rrnc_time = input[17];
    Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsDrv = input[18];
    Arbitrator_VlvFSFsrAbsDrvInfo.FsrAbsOff = input[19];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[0] = input[20];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[1] = input[21];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[2] = input[22];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[3] = input[23];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[4] = input[24];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[5] = input[25];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[6] = input[26];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[7] = input[27];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[8] = input[28];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReqData[9] = input[29];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[0] = input[30];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[1] = input[31];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[2] = input[32];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[3] = input[33];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[4] = input[34];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[5] = input[35];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[6] = input[36];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[7] = input[37];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[8] = input[38];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReqData[9] = input[39];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[0] = input[40];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[1] = input[41];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[2] = input[42];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[3] = input[43];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[4] = input[44];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[5] = input[45];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[6] = input[46];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[7] = input[47];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[8] = input[48];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReqData[9] = input[49];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[0] = input[50];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[1] = input[51];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[2] = input[52];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[3] = input[53];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[4] = input[54];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[5] = input[55];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[6] = input[56];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[7] = input[57];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[8] = input[58];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReqData[9] = input[59];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[0] = input[60];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[1] = input[61];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[2] = input[62];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[3] = input[63];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[4] = input[64];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[5] = input[65];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[6] = input[66];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[7] = input[67];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[8] = input[68];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReqData[9] = input[69];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[0] = input[70];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[1] = input[71];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[2] = input[72];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[3] = input[73];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[4] = input[74];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[5] = input[75];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[6] = input[76];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[7] = input[77];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[8] = input[78];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReqData[9] = input[79];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[0] = input[80];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[1] = input[81];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[2] = input[82];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[3] = input[83];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[4] = input[84];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[5] = input[85];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[6] = input[86];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[7] = input[87];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[8] = input[88];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReqData[9] = input[89];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[0] = input[90];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[1] = input[91];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[2] = input[92];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[3] = input[93];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[4] = input[94];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[5] = input[95];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[6] = input[96];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[7] = input[97];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[8] = input[98];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReqData[9] = input[99];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvReq = input[100];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvReq = input[101];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvReq = input[102];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvReq = input[103];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvReq = input[104];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvReq = input[105];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvReq = input[106];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvReq = input[107];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlIvDataLen = input[108];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrIvDataLen = input[109];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlIvDataLen = input[110];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrIvDataLen = input[111];
    Arbitrator_VlvWhlVlvReqAbcInfo.FlOvDataLen = input[112];
    Arbitrator_VlvWhlVlvReqAbcInfo.FrOvDataLen = input[113];
    Arbitrator_VlvWhlVlvReqAbcInfo.RlOvDataLen = input[114];
    Arbitrator_VlvWhlVlvReqAbcInfo.RrOvDataLen = input[115];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[0] = input[116];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[1] = input[117];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[2] = input[118];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[3] = input[119];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReqData[4] = input[120];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[0] = input[121];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[1] = input[122];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[2] = input[123];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[3] = input[124];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReqData[4] = input[125];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[0] = input[126];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[1] = input[127];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[2] = input[128];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[3] = input[129];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReqData[4] = input[130];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[0] = input[131];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[1] = input[132];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[2] = input[133];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[3] = input[134];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReqData[4] = input[135];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[0] = input[136];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[1] = input[137];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[2] = input[138];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[3] = input[139];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvReqData[4] = input[140];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvReq = input[141];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvReq = input[142];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvReq = input[143];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvReq = input[144];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvReq = input[145];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCutVlvDataLen = input[146];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCutVlvDataLen = input[147];
    Arbitrator_VlvNormVlvReqSesInfo.PrimCircVlvDataLen = input[148];
    Arbitrator_VlvNormVlvReqSesInfo.SecdCircVlvDataLen = input[149];
    Arbitrator_VlvNormVlvReqSesInfo.SimVlvDataLen = input[150];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[0] = input[151];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[1] = input[152];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[2] = input[153];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[3] = input[154];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvReqData[4] = input[155];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[0] = input[156];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[1] = input[157];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[2] = input[158];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[3] = input[159];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvReqData[4] = input[160];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[0] = input[161];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[1] = input[162];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[2] = input[163];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[3] = input[164];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvReqData[4] = input[165];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[0] = input[166];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[1] = input[167];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[2] = input[168];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[3] = input[169];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvReqData[4] = input[170];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvReq = input[171];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvReq = input[172];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvReq = input[173];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvReq = input[174];
    Arbitrator_VlvWhlVlvReqSesInfo.FlIvDataLen = input[175];
    Arbitrator_VlvWhlVlvReqSesInfo.FrIvDataLen = input[176];
    Arbitrator_VlvWhlVlvReqSesInfo.RlIvDataLen = input[177];
    Arbitrator_VlvWhlVlvReqSesInfo.RrIvDataLen = input[178];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[0] = input[179];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[1] = input[180];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[2] = input[181];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[3] = input[182];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReqData[4] = input[183];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[0] = input[184];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[1] = input[185];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[2] = input[186];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[3] = input[187];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReqData[4] = input[188];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[0] = input[189];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[1] = input[190];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[2] = input[191];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[3] = input[192];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReqData[4] = input[193];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[0] = input[194];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[1] = input[195];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[2] = input[196];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[3] = input[197];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReqData[4] = input[198];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[0] = input[199];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[1] = input[200];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[2] = input[201];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[3] = input[202];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReqData[4] = input[203];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[0] = input[204];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[1] = input[205];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[2] = input[206];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[3] = input[207];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReqData[4] = input[208];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[0] = input[209];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[1] = input[210];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[2] = input[211];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[3] = input[212];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReqData[4] = input[213];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[0] = input[214];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[1] = input[215];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[2] = input[216];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[3] = input[217];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReqData[4] = input[218];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvReq = input[219];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvReq = input[220];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvReq = input[221];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvReq = input[222];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvReq = input[223];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvReq = input[224];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvReq = input[225];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvReq = input[226];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCutVlvDataLen = input[227];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCutVlvDataLen = input[228];
    Arbitrator_VlvNormVlvReqVlvActInfo.PrimCircVlvDataLen = input[229];
    Arbitrator_VlvNormVlvReqVlvActInfo.SecdCircVlvDataLen = input[230];
    Arbitrator_VlvNormVlvReqVlvActInfo.SimVlvDataLen = input[231];
    Arbitrator_VlvNormVlvReqVlvActInfo.CircVlvDataLen = input[232];
    Arbitrator_VlvNormVlvReqVlvActInfo.RelsVlvDataLen = input[233];
    Arbitrator_VlvNormVlvReqVlvActInfo.PressDumpVlvDataLen = input[234];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReq = input[235];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReq = input[236];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReq = input[237];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReq = input[238];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReq = input[239];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReq = input[240];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReq = input[241];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReq = input[242];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvDataLen = input[243];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvDataLen = input[244];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvDataLen = input[245];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvDataLen = input[246];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvDataLen = input[247];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvDataLen = input[248];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvDataLen = input[249];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvDataLen = input[250];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[0] = input[251];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[1] = input[252];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[2] = input[253];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[3] = input[254];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlIvReqData[4] = input[255];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[0] = input[256];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[1] = input[257];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[2] = input[258];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[3] = input[259];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrIvReqData[4] = input[260];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[0] = input[261];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[1] = input[262];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[2] = input[263];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[3] = input[264];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlIvReqData[4] = input[265];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[0] = input[266];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[1] = input[267];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[2] = input[268];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[3] = input[269];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrIvReqData[4] = input[270];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[0] = input[271];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[1] = input[272];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[2] = input[273];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[3] = input[274];
    Arbitrator_VlvWhlVlvReqIdbInfo.FlOvReqData[4] = input[275];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[0] = input[276];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[1] = input[277];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[2] = input[278];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[3] = input[279];
    Arbitrator_VlvWhlVlvReqIdbInfo.FrOvReqData[4] = input[280];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[0] = input[281];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[1] = input[282];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[2] = input[283];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[3] = input[284];
    Arbitrator_VlvWhlVlvReqIdbInfo.RlOvReqData[4] = input[285];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[0] = input[286];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[1] = input[287];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[2] = input[288];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[3] = input[289];
    Arbitrator_VlvWhlVlvReqIdbInfo.RrOvReqData[4] = input[290];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[0] = input[291];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[1] = input[292];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[2] = input[293];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[3] = input[294];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReqData[4] = input[295];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[0] = input[296];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[1] = input[297];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[2] = input[298];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[3] = input[299];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReqData[4] = input[300];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[0] = input[301];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[1] = input[302];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[2] = input[303];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[3] = input[304];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReqData[4] = input[305];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvReq = input[306];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvReq = input[307];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvReq = input[308];
    Arbitrator_VlvIdbBalVlvReqInfo.PrimBalVlvDataLen = input[309];
    Arbitrator_VlvIdbBalVlvReqInfo.SecdBalVlvDataLen = input[310];
    Arbitrator_VlvIdbBalVlvReqInfo.ChmbBalVlvDataLen = input[311];
    Arbitrator_VlvEemFailData.Eem_Fail_BBSSol = input[312];
    Arbitrator_VlvEemFailData.Eem_Fail_ESCSol = input[313];
    Arbitrator_VlvEemFailData.Eem_Fail_FrontSol = input[314];
    Arbitrator_VlvEemFailData.Eem_Fail_RearSol = input[315];
    Arbitrator_VlvEemFailData.Eem_Fail_Motor = input[316];
    Arbitrator_VlvEemFailData.Eem_Fail_MPS = input[317];
    Arbitrator_VlvEemFailData.Eem_Fail_MGD = input[318];
    Arbitrator_VlvEemFailData.Eem_Fail_BBSValveRelay = input[319];
    Arbitrator_VlvEemFailData.Eem_Fail_ESCValveRelay = input[320];
    Arbitrator_VlvEemFailData.Eem_Fail_ECUHw = input[321];
    Arbitrator_VlvEemFailData.Eem_Fail_ASIC = input[322];
    Arbitrator_VlvEemFailData.Eem_Fail_OverVolt = input[323];
    Arbitrator_VlvEemFailData.Eem_Fail_UnderVolt = input[324];
    Arbitrator_VlvEemFailData.Eem_Fail_LowVolt = input[325];
    Arbitrator_VlvEemFailData.Eem_Fail_LowerVolt = input[326];
    Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_12V = input[327];
    Arbitrator_VlvEemFailData.Eem_Fail_SenPwr_5V = input[328];
    Arbitrator_VlvEemFailData.Eem_Fail_Yaw = input[329];
    Arbitrator_VlvEemFailData.Eem_Fail_Ay = input[330];
    Arbitrator_VlvEemFailData.Eem_Fail_Ax = input[331];
    Arbitrator_VlvEemFailData.Eem_Fail_Str = input[332];
    Arbitrator_VlvEemFailData.Eem_Fail_CirP1 = input[333];
    Arbitrator_VlvEemFailData.Eem_Fail_CirP2 = input[334];
    Arbitrator_VlvEemFailData.Eem_Fail_SimP = input[335];
    Arbitrator_VlvEemFailData.Eem_Fail_BLS = input[336];
    Arbitrator_VlvEemFailData.Eem_Fail_ESCSw = input[337];
    Arbitrator_VlvEemFailData.Eem_Fail_HDCSw = input[338];
    Arbitrator_VlvEemFailData.Eem_Fail_AVHSw = input[339];
    Arbitrator_VlvEemFailData.Eem_Fail_BrakeLampRelay = input[340];
    Arbitrator_VlvEemFailData.Eem_Fail_EssRelay = input[341];
    Arbitrator_VlvEemFailData.Eem_Fail_GearR = input[342];
    Arbitrator_VlvEemFailData.Eem_Fail_Clutch = input[343];
    Arbitrator_VlvEemFailData.Eem_Fail_ParkBrake = input[344];
    Arbitrator_VlvEemFailData.Eem_Fail_PedalPDT = input[345];
    Arbitrator_VlvEemFailData.Eem_Fail_PedalPDF = input[346];
    Arbitrator_VlvEemFailData.Eem_Fail_BrakeFluid = input[347];
    Arbitrator_VlvEemFailData.Eem_Fail_TCSTemp = input[348];
    Arbitrator_VlvEemFailData.Eem_Fail_HDCTemp = input[349];
    Arbitrator_VlvEemFailData.Eem_Fail_SCCTemp = input[350];
    Arbitrator_VlvEemFailData.Eem_Fail_TVBBTemp = input[351];
    Arbitrator_VlvEemFailData.Eem_Fail_MainCanLine = input[352];
    Arbitrator_VlvEemFailData.Eem_Fail_SubCanLine = input[353];
    Arbitrator_VlvEemFailData.Eem_Fail_EMSTimeOut = input[354];
    Arbitrator_VlvEemFailData.Eem_Fail_FWDTimeOut = input[355];
    Arbitrator_VlvEemFailData.Eem_Fail_TCUTimeOut = input[356];
    Arbitrator_VlvEemFailData.Eem_Fail_HCUTimeOut = input[357];
    Arbitrator_VlvEemFailData.Eem_Fail_MCUTimeOut = input[358];
    Arbitrator_VlvEemFailData.Eem_Fail_VariantCoding = input[359];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Cbs = input[360];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Ebd = input[361];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Abs = input[362];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Edc = input[363];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Btcs = input[364];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Etcs = input[365];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Tcs = input[366];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Vdc = input[367];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hsa = input[368];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Hdc = input[369];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Pba = input[370];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Avh = input[371];
    Arbitrator_VlvEemCtrlInhibitData.Eem_CtrlIhb_Moc = input[372];
    Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_AllControlInhibit = input[373];
    Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = input[374];
    Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DegradeModeFail = input[375];
    Arbitrator_VlvEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = input[376];
    Arbitrator_VlvDiagVlvActrInfo.SolenoidDrvDuty = input[377];
    Arbitrator_VlvDiagVlvActrInfo.FlOvReqData = input[378];
    Arbitrator_VlvDiagVlvActrInfo.FlIvReqData = input[379];
    Arbitrator_VlvDiagVlvActrInfo.FrOvReqData = input[380];
    Arbitrator_VlvDiagVlvActrInfo.FrIvReqData = input[381];
    Arbitrator_VlvDiagVlvActrInfo.RlOvReqData = input[382];
    Arbitrator_VlvDiagVlvActrInfo.RlIvReqData = input[383];
    Arbitrator_VlvDiagVlvActrInfo.RrOvReqData = input[384];
    Arbitrator_VlvDiagVlvActrInfo.RrIvReqData = input[385];
    Arbitrator_VlvDiagVlvActrInfo.PrimCutVlvReqData = input[386];
    Arbitrator_VlvDiagVlvActrInfo.SecdCutVlvReqData = input[387];
    Arbitrator_VlvDiagVlvActrInfo.SimVlvReqData = input[388];
    Arbitrator_VlvDiagVlvActrInfo.ResPVlvReqData = input[389];
    Arbitrator_VlvDiagVlvActrInfo.BalVlvReqData = input[390];
    Arbitrator_VlvDiagVlvActrInfo.CircVlvReqData = input[391];
    Arbitrator_VlvDiagVlvActrInfo.PressDumpReqData = input[392];
    Arbitrator_VlvDiagVlvActrInfo.RelsVlvReqData = input[393];
    Arbitrator_VlvWhlSpdInfo.FlWhlSpd = input[394];
    Arbitrator_VlvWhlSpdInfo.FrWhlSpd = input[395];
    Arbitrator_VlvWhlSpdInfo.RlWhlSpd = input[396];
    Arbitrator_VlvWhlSpdInfo.RrlWhlSpd = input[397];
    Arbitrator_VlvEcuModeSts = input[398];
    Arbitrator_VlvIgnOnOffSts = input[399];
    Arbitrator_VlvIgnEdgeSts = input[400];
    Arbitrator_VlvAcmAsicInitCompleteFlag = input[401];
    Arbitrator_VlvDiagSci = input[402];
    Arbitrator_VlvDiagAhbSci = input[403];
    Arbitrator_VlvFuncInhibitVlvSts = input[404];

    Arbitrator_Vlv();


    output[0] = Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsDrv;
    output[1] = Arbitrator_VlvArbFsrBbsDrvInfo.FsrCbsOff;
    output[2] = Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsDrv;
    output[3] = Arbitrator_VlvArbFsrAbsDrvInfo.FsrAbsOff;
    output[4] = Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[0];
    output[5] = Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[1];
    output[6] = Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[2];
    output[7] = Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[3];
    output[8] = Arbitrator_VlvArbWhlVlvReqInfo.FlOvReqData[4];
    output[9] = Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[0];
    output[10] = Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[1];
    output[11] = Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[2];
    output[12] = Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[3];
    output[13] = Arbitrator_VlvArbWhlVlvReqInfo.FlIvReqData[4];
    output[14] = Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[0];
    output[15] = Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[1];
    output[16] = Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[2];
    output[17] = Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[3];
    output[18] = Arbitrator_VlvArbWhlVlvReqInfo.FrOvReqData[4];
    output[19] = Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[0];
    output[20] = Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[1];
    output[21] = Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[2];
    output[22] = Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[3];
    output[23] = Arbitrator_VlvArbWhlVlvReqInfo.FrIvReqData[4];
    output[24] = Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[0];
    output[25] = Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[1];
    output[26] = Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[2];
    output[27] = Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[3];
    output[28] = Arbitrator_VlvArbWhlVlvReqInfo.RlOvReqData[4];
    output[29] = Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[0];
    output[30] = Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[1];
    output[31] = Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[2];
    output[32] = Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[3];
    output[33] = Arbitrator_VlvArbWhlVlvReqInfo.RlIvReqData[4];
    output[34] = Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[0];
    output[35] = Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[1];
    output[36] = Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[2];
    output[37] = Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[3];
    output[38] = Arbitrator_VlvArbWhlVlvReqInfo.RrOvReqData[4];
    output[39] = Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[0];
    output[40] = Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[1];
    output[41] = Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[2];
    output[42] = Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[3];
    output[43] = Arbitrator_VlvArbWhlVlvReqInfo.RrIvReqData[4];
    output[44] = Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[0];
    output[45] = Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[1];
    output[46] = Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[2];
    output[47] = Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[3];
    output[48] = Arbitrator_VlvArbNormVlvReqInfo.PrimCutVlvReqData[4];
    output[49] = Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[0];
    output[50] = Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[1];
    output[51] = Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[2];
    output[52] = Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[3];
    output[53] = Arbitrator_VlvArbNormVlvReqInfo.SecdCutVlvReqData[4];
    output[54] = Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[0];
    output[55] = Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[1];
    output[56] = Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[2];
    output[57] = Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[3];
    output[58] = Arbitrator_VlvArbNormVlvReqInfo.SimVlvReqData[4];
    output[59] = Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[0];
    output[60] = Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[1];
    output[61] = Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[2];
    output[62] = Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[3];
    output[63] = Arbitrator_VlvArbNormVlvReqInfo.RelsVlvReqData[4];
    output[64] = Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[0];
    output[65] = Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[1];
    output[66] = Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[2];
    output[67] = Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[3];
    output[68] = Arbitrator_VlvArbNormVlvReqInfo.CircVlvReqData[4];
    output[69] = Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[0];
    output[70] = Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[1];
    output[71] = Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[2];
    output[72] = Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[3];
    output[73] = Arbitrator_VlvArbNormVlvReqInfo.PressDumpVlvReqData[4];
    output[74] = Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[0];
    output[75] = Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[1];
    output[76] = Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[2];
    output[77] = Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[3];
    output[78] = Arbitrator_VlvArbBalVlvReqInfo.BalVlvReqData[4];
    output[79] = Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[0];
    output[80] = Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[1];
    output[81] = Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[2];
    output[82] = Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[3];
    output[83] = Arbitrator_VlvArbResPVlvReqInfo.ResPVlvReqData[4];
    output[84] = Arbitrator_VlvAsicEnDrDrv;
    output[85] = Arbitrator_VlvArbVlvSync;
    output[86] = Arbitrator_VlvArbVlvDriveState;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Arbitrator_Vlv_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
