#define S_FUNCTION_NAME      Arbitrator_Mtr_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          81
#define WidthOutputPort         9

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Arbitrator_Mtr.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Arbitrator_MtrEemFailData.Eem_Fail_BBSSol = input[0];
    Arbitrator_MtrEemFailData.Eem_Fail_ESCSol = input[1];
    Arbitrator_MtrEemFailData.Eem_Fail_FrontSol = input[2];
    Arbitrator_MtrEemFailData.Eem_Fail_RearSol = input[3];
    Arbitrator_MtrEemFailData.Eem_Fail_Motor = input[4];
    Arbitrator_MtrEemFailData.Eem_Fail_MPS = input[5];
    Arbitrator_MtrEemFailData.Eem_Fail_MGD = input[6];
    Arbitrator_MtrEemFailData.Eem_Fail_BBSValveRelay = input[7];
    Arbitrator_MtrEemFailData.Eem_Fail_ESCValveRelay = input[8];
    Arbitrator_MtrEemFailData.Eem_Fail_ECUHw = input[9];
    Arbitrator_MtrEemFailData.Eem_Fail_ASIC = input[10];
    Arbitrator_MtrEemFailData.Eem_Fail_OverVolt = input[11];
    Arbitrator_MtrEemFailData.Eem_Fail_UnderVolt = input[12];
    Arbitrator_MtrEemFailData.Eem_Fail_LowVolt = input[13];
    Arbitrator_MtrEemFailData.Eem_Fail_LowerVolt = input[14];
    Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_12V = input[15];
    Arbitrator_MtrEemFailData.Eem_Fail_SenPwr_5V = input[16];
    Arbitrator_MtrEemFailData.Eem_Fail_Yaw = input[17];
    Arbitrator_MtrEemFailData.Eem_Fail_Ay = input[18];
    Arbitrator_MtrEemFailData.Eem_Fail_Ax = input[19];
    Arbitrator_MtrEemFailData.Eem_Fail_Str = input[20];
    Arbitrator_MtrEemFailData.Eem_Fail_CirP1 = input[21];
    Arbitrator_MtrEemFailData.Eem_Fail_CirP2 = input[22];
    Arbitrator_MtrEemFailData.Eem_Fail_SimP = input[23];
    Arbitrator_MtrEemFailData.Eem_Fail_BLS = input[24];
    Arbitrator_MtrEemFailData.Eem_Fail_ESCSw = input[25];
    Arbitrator_MtrEemFailData.Eem_Fail_HDCSw = input[26];
    Arbitrator_MtrEemFailData.Eem_Fail_AVHSw = input[27];
    Arbitrator_MtrEemFailData.Eem_Fail_BrakeLampRelay = input[28];
    Arbitrator_MtrEemFailData.Eem_Fail_EssRelay = input[29];
    Arbitrator_MtrEemFailData.Eem_Fail_GearR = input[30];
    Arbitrator_MtrEemFailData.Eem_Fail_Clutch = input[31];
    Arbitrator_MtrEemFailData.Eem_Fail_ParkBrake = input[32];
    Arbitrator_MtrEemFailData.Eem_Fail_PedalPDT = input[33];
    Arbitrator_MtrEemFailData.Eem_Fail_PedalPDF = input[34];
    Arbitrator_MtrEemFailData.Eem_Fail_BrakeFluid = input[35];
    Arbitrator_MtrEemFailData.Eem_Fail_TCSTemp = input[36];
    Arbitrator_MtrEemFailData.Eem_Fail_HDCTemp = input[37];
    Arbitrator_MtrEemFailData.Eem_Fail_SCCTemp = input[38];
    Arbitrator_MtrEemFailData.Eem_Fail_TVBBTemp = input[39];
    Arbitrator_MtrEemFailData.Eem_Fail_MainCanLine = input[40];
    Arbitrator_MtrEemFailData.Eem_Fail_SubCanLine = input[41];
    Arbitrator_MtrEemFailData.Eem_Fail_EMSTimeOut = input[42];
    Arbitrator_MtrEemFailData.Eem_Fail_FWDTimeOut = input[43];
    Arbitrator_MtrEemFailData.Eem_Fail_TCUTimeOut = input[44];
    Arbitrator_MtrEemFailData.Eem_Fail_HCUTimeOut = input[45];
    Arbitrator_MtrEemFailData.Eem_Fail_MCUTimeOut = input[46];
    Arbitrator_MtrEemFailData.Eem_Fail_VariantCoding = input[47];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Cbs = input[48];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Ebd = input[49];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Abs = input[50];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Edc = input[51];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Btcs = input[52];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Etcs = input[53];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Tcs = input[54];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Vdc = input[55];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hsa = input[56];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Hdc = input[57];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Pba = input[58];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Avh = input[59];
    Arbitrator_MtrEemCtrlInhibitData.Eem_CtrlIhb_Moc = input[60];
    Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_AllControlInhibit = input[61];
    Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = input[62];
    Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DegradeModeFail = input[63];
    Arbitrator_MtrEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = input[64];
    Arbitrator_MtrMotDqIRefMccInfo.IdRef = input[65];
    Arbitrator_MtrMotDqIRefMccInfo.IqRef = input[66];
    Arbitrator_MtrMtrProcessOutData.MtrProIqFailsafe = input[67];
    Arbitrator_MtrMtrProcessOutData.MtrProIdFailsafe = input[68];
    Arbitrator_MtrMtrProcessOutData.MtrProUPhasePWM = input[69];
    Arbitrator_MtrMtrProcessOutData.MtrProVPhasePWM = input[70];
    Arbitrator_MtrMtrProcessOutData.MtrProWPhasePWM = input[71];
    Arbitrator_MtrMtrProcessDataInf.MtrProCalibrationState = input[72];
    Arbitrator_MtrEcuModeSts = input[73];
    Arbitrator_MtrIgnOnOffSts = input[74];
    Arbitrator_MtrIgnEdgeSts = input[75];
    Arbitrator_MtrDiagSci = input[76];
    Arbitrator_MtrDiagAhbSci = input[77];
    Arbitrator_MtrVdcLinkFild = input[78];
    Arbitrator_MtrMotCtrlMode = input[79];
    Arbitrator_MtrMotCtrlState = input[80];

    Arbitrator_Mtr();


    output[0] = Arbitrator_MtrMtrArbtratorData.IqSelected;
    output[1] = Arbitrator_MtrMtrArbtratorData.IdSelected;
    output[2] = Arbitrator_MtrMtrArbtratorData.UPhasePWMSelected;
    output[3] = Arbitrator_MtrMtrArbtratorData.VPhasePWMSelected;
    output[4] = Arbitrator_MtrMtrArbtratorData.WPhasePWMSelected;
    output[5] = Arbitrator_MtrMtrArbtratorData.MtrCtrlMode;
    output[6] = Arbitrator_MtrMtrArbtratorInfo.MtrArbCalMode;
    output[7] = Arbitrator_MtrMtrArbtratorInfo.MtrArbState;
    output[8] = Arbitrator_MtrMtrArbDriveState;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Arbitrator_Mtr_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
