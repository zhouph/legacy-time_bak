# \file
#
# \brief Arbitrator
#
# This file contains the implementation of the SWC
# module Arbitrator.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Arbitrator_CORE_PATH     := $(MANDO_BSW_ROOT)\Arbitrator
Arbitrator_CAL_PATH      := $(Arbitrator_CORE_PATH)\CAL\$(Arbitrator_VARIANT)
Arbitrator_SRC_PATH      := $(Arbitrator_CORE_PATH)\SRC
Arbitrator_CFG_PATH      := $(Arbitrator_CORE_PATH)\CFG\$(Arbitrator_VARIANT)
Arbitrator_HDR_PATH      := $(Arbitrator_CORE_PATH)\HDR
Arbitrator_IFA_PATH      := $(Arbitrator_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Arbitrator_CMN_PATH      := $(Arbitrator_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Arbitrator_UT_PATH		:= $(Arbitrator_CORE_PATH)\UT
	Arbitrator_UNITY_PATH	:= $(Arbitrator_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Arbitrator_UT_PATH)
	CC_INCLUDE_PATH		+= $(Arbitrator_UNITY_PATH)
	Arbitrator_Mtr_PATH 	:= Arbitrator_UT_PATH\Arbitrator_Mtr
	Arbitrator_Vlv_PATH 	:= Arbitrator_UT_PATH\Arbitrator_Vlv
	Arbitrator_Rly_PATH 	:= Arbitrator_UT_PATH\Arbitrator_Rly
endif
CC_INCLUDE_PATH    += $(Arbitrator_CAL_PATH)
CC_INCLUDE_PATH    += $(Arbitrator_SRC_PATH)
CC_INCLUDE_PATH    += $(Arbitrator_CFG_PATH)
CC_INCLUDE_PATH    += $(Arbitrator_HDR_PATH)
CC_INCLUDE_PATH    += $(Arbitrator_IFA_PATH)
CC_INCLUDE_PATH    += $(Arbitrator_CMN_PATH)

