# \file
#
# \brief Arbitrator
#
# This file contains the implementation of the SWC
# module Arbitrator.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Arbitrator_src

Arbitrator_src_FILES        += $(Arbitrator_SRC_PATH)\Arbitrator_Mtr.c
Arbitrator_src_FILES        += $(Arbitrator_IFA_PATH)\Arbitrator_Mtr_Ifa.c
Arbitrator_src_FILES        += $(Arbitrator_SRC_PATH)\Arbitrator_Mtr_Process.c
Arbitrator_src_FILES        += $(Arbitrator_SRC_PATH)\Arbitrator_Vlv.c
Arbitrator_src_FILES        += $(Arbitrator_IFA_PATH)\Arbitrator_Vlv_Ifa.c
Arbitrator_src_FILES        += $(Arbitrator_SRC_PATH)\Arbitrator_Vlv_Processing.c
Arbitrator_src_FILES        += $(Arbitrator_SRC_PATH)\Arbitrator_Rly.c
Arbitrator_src_FILES        += $(Arbitrator_IFA_PATH)\Arbitrator_Rly_Ifa.c
Arbitrator_src_FILES        += $(Arbitrator_CFG_PATH)\Arbitrator_Cfg.c
Arbitrator_src_FILES        += $(Arbitrator_CAL_PATH)\Arbitrator_Cal.c

ifeq ($(ICE_COMPILE),true)
Arbitrator_src_FILES        += $(Arbitrator_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Arbitrator_src_FILES        += $(Arbitrator_UNITY_PATH)\unity.c
	Arbitrator_src_FILES        += $(Arbitrator_UNITY_PATH)\unity_fixture.c	
	Arbitrator_src_FILES        += $(Arbitrator_UT_PATH)\main.c
	Arbitrator_src_FILES        += $(Arbitrator_UT_PATH)\Arbitrator_Mtr\Arbitrator_Mtr_UtMain.c
	Arbitrator_src_FILES        += $(Arbitrator_UT_PATH)\Arbitrator_Vlv\Arbitrator_Vlv_UtMain.c
	Arbitrator_src_FILES        += $(Arbitrator_UT_PATH)\Arbitrator_Rly\Arbitrator_Rly_UtMain.c
endif