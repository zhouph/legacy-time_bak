#define S_FUNCTION_NAME      Mgd_TLE9180_Hndlr_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          1
#define WidthOutputPort         99

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Mgd_TLE9180_Hndlr.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Mgd_TLE9180_HndlrEcuModeSts = input[0];

    Mgd_TLE9180_Hndlr();


    output[0] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM;
    output[1] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM;
    output[2] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock;
    output[3] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM;
    output[4] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM;
    output[5] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM;
    output[6] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM;
    output[7] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM;
    output[8] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf;
    output[9] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp;
    output[10] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd;
    output[11] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd;
    output[12] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag;
    output[13] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp;
    output[14] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt;
    output[15] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12;
    output[16] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom;
    output[17] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn;
    output[18] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete;
    output[19] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct;
    output[20] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm;
    output[21] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid;
    output[22] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw;
    output[23] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW;
    output[24] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1;
    output[25] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom;
    output[26] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4;
    output[27] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6;
    output[28] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6;
    output[29] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5;
    output[30] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb;
    output[31] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim;
    output[32] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3;
    output[33] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2;
    output[34] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1;
    output[35] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2;
    output[36] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1;
    output[37] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3;
    output[38] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2;
    output[39] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1;
    output[40] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh;
    output[41] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh;
    output[42] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs;
    output[43] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs;
    output[44] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc;
    output[45] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc;
    output[46] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh;
    output[47] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck;
    output[48] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1;
    output[49] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp;
    output[50] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail;
    output[51] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb;
    output[52] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh;
    output[53] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs;
    output[54] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt;
    output[55] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3;
    output[56] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2;
    output[57] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1;
    output[58] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3;
    output[59] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2;
    output[60] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1;
    output[61] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3;
    output[62] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2;
    output[63] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1;
    output[64] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3;
    output[65] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2;
    output[66] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1;
    output[67] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1;
    output[68] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2;
    output[69] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3;
    output[70] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1;
    output[71] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2;
    output[72] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3;
    output[73] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame;
    output[74] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo;
    output[75] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd;
    output[76] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc;
    output[77] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid;
    output[78] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo;
    output[79] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid;
    output[80] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1;
    output[81] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv;
    output[82] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov;
    output[83] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib;
    output[84] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2;
    output[85] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv;
    output[86] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov;
    output[87] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib;
    output[88] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3;
    output[89] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv;
    output[90] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov;
    output[91] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib;
    output[92] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn;
    output[93] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso;
    output[94] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1;
    output[95] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2;
    output[96] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3;
    output[97] = Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort;
    output[98] = Mgd_TLE9180_HndlrMgdInvalid;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Mgd_TLE9180_Hndlr_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
