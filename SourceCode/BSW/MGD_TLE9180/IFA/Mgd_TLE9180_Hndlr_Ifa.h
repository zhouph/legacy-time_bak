/**
 * @defgroup Mgd_TLE9180_Hndlr_Ifa Mgd_TLE9180_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mgd_TLE9180_Hndlr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGD_TLE9180_HNDLR_IFA_H_
#define MGD_TLE9180_HNDLR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mgd_TLE9180_Hndlr_Read_Mgd_TLE9180_HndlrEcuModeSts(data) do \
{ \
    *data = Mgd_TLE9180_HndlrEcuModeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdIdleM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdConfM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdConfLock(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSelfTestM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSoffM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdRectM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdNormM(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOsf(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOp(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdScd(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSd(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdIndiag(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOutp(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdExt(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdInt12(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdRom(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdLimpOn(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdStIncomplete(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdApcAct(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdGtm(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdCtrlRegInvalid(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdLfw(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOtW(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvReg1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVccRom(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg4(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvReg6(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg6(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg5(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvCb(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrClkTrim(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrCp2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrCp1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVdh(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVdh(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVs(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVs(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVcc(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVcc(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvLdVdh(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdDdpStuck(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdCp1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvCp(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdClkfail(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdUvCb(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvVdh(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvVs(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOt(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiFrame(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiTo(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiWd(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiCrc(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEpiAddInvalid(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEonfTo(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEonfSigInvalid(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Uv(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Ov(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Calib(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Uv(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Ov(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Calib(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Uv(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Ov(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Calib(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpErrn(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpMiso(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB1(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB2(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB3(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3 = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo_mgdTle9180ErrPort(data) do \
{ \
    Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort = *data; \
}while(0);

#define Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdInvalid(data) do \
{ \
    Mgd_TLE9180_HndlrMgdInvalid = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGD_TLE9180_HNDLR_IFA_H_ */
/** @} */
