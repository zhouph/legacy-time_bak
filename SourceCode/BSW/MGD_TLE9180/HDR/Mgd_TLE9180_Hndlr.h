/**
 * @defgroup Mgd_TLE9180_Hndlr Mgd_TLE9180_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mgd_TLE9180_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGD_TLE9180_HNDLR_H_
#define MGD_TLE9180_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mgd_TLE9180_Types.h"
#include "Mgd_TLE9180_Cfg.h"
#include "Mgd_TLE9180_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MGD_TLE9180_HNDLR_MODULE_ID      (0)
 #define MGD_TLE9180_HNDLR_MAJOR_VERSION  (2)
 #define MGD_TLE9180_HNDLR_MINOR_VERSION  (0)
 #define MGD_TLE9180_HNDLR_PATCH_VERSION  (0)
 #define MGD_TLE9180_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mgd_TLE9180_Hndlr_HdrBusType Mgd_TLE9180_HndlrBus;

/* Version Info */
extern const SwcVersionInfo_t Mgd_TLE9180_HndlrVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Mgd_TLE9180_HndlrEcuModeSts;

/* Output Data Element */
extern Mgd_TLE9180_HndlrMgdDcdInfo_t Mgd_TLE9180_HndlrMgdDcdInfo;
extern Mgd_TLE9180_HndlrMgdInvalid_t Mgd_TLE9180_HndlrMgdInvalid;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mgd_TLE9180_Hndlr_Init(void);
extern void Mgd_TLE9180_Hndlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGD_TLE9180_HNDLR_H_ */
/** @} */
