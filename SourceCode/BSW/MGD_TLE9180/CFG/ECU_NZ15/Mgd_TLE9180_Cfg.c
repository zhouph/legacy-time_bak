/**
 * @defgroup Mgd_TLE9180_Cfg Mgd_TLE9180_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mgd_TLE9180_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mgd_TLE9180_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MGD_TLE9180_START_SEC_CONST_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MGD_TLE9180_STOP_SEC_CONST_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGD_TLE9180_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MGD_TLE9180_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_START_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGD_TLE9180_STOP_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_START_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGD_TLE9180_STOP_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_START_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (32BIT)**/


#define MGD_TLE9180_STOP_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGD_TLE9180_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MGD_TLE9180_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_START_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGD_TLE9180_STOP_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_START_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGD_TLE9180_STOP_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_START_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (32BIT)**/


#define MGD_TLE9180_STOP_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MGD_TLE9180_START_SEC_CODE
#include "Mgd_TLE9180_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MGD_TLE9180_STOP_SEC_CODE
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
