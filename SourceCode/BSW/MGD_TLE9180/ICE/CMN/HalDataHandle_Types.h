/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef HAL_DATAHANDLE_TYPES_H_
#define HAL_DATAHANDLE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef MpsD1SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t;
typedef AchWssRedncyAsicInfo_t Ach_InputAchWssRedncyAsicInfo_t;
typedef VBatt1Mon_t Ioc_InputSR1msVBatt1Mon_t;
typedef VddMon_t Ioc_InputSR1msVddMon_t;
typedef HwTrigVlvInfo_t AdcIf_Conv1msHwTrigVlvInfo_t;
typedef IocVlvEsv1Data_t Ioc_OutputSR1msIocVlvEsv1Data_t;
typedef Int5vMon_t Ioc_InputSR1msInt5vMon_t;
typedef WhlVlvFbMonInfo_t Ioc_InputSR1msWhlVlvFbMonInfo_t;
typedef RlyMonInfo_t Ioc_InputCS1msRlyMonInfo_t;
typedef AchWldShlsAsicInfo_t Ach_InputAchWldShlsAsicInfo_t;
typedef SwTrigFspInfo_t AdcIf_Conv1msSwTrigFspInfo_t;
typedef Vdd1Mon_t Ioc_InputSR1msVdd1Mon_t;
typedef MpsD2SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t;
typedef SwTrigMocInfo_t AdcIf_Conv1msSwTrigMocInfo_t;
typedef VlvdInvalid_t Vlvd_A3944_HndlrVlvdInvalid_t;
typedef MgdDcdInfo_t Mgd_TLE9180_HndlrMgdDcdInfo_t;
typedef VlvDrvEnRst_t Vlvd_A3944_HndlrVlvDrvEnRst_t;
typedef PedlSigMonInfo_t Ioc_InputSR1msPedlSigMonInfo_t;
typedef MotAngleMonInfo_t Ioc_InputSR50usMotAngleMonInfo_t;
typedef AchAsicInvalidInfo_t Ach_InputAchAsicInvalidInfo_t;
typedef PwrMonInfoEsc_t Ioc_InputSR1msPwrMonInfoEsc_t;
typedef IocAdcSelWriteData_t Ioc_OutputSR1msIocAdcSelWriteData_t;
typedef SwTrigPdtInfo_t AdcIf_Conv1msSwTrigPdtInfo_t;
typedef BFLMon_t Ioc_InputSR1msBFLMon_t;
typedef SwTrigPwrInfo_t AdcIf_Conv1msSwTrigPwrInfo_t;
typedef LineTestMon_t Ioc_InputSR1msLineTestMon_t;
typedef SwTrigEscInfo_t AdcIf_Conv1msSwTrigEscInfo_t;
typedef IocVlvNc1Data_t Ioc_OutputSR1msIocVlvNc1Data_t;
typedef VlvMonInfo_t Ioc_InputSR1msVlvMonInfo_t;
typedef FspCbsHMon_t Ioc_InputSR1msFspCbsHMon_t;
typedef Vdd2Mon_t Ioc_InputSR1msVdd2Mon_t;
typedef AchWssPort3AsicInfo_t Ach_InputAchWssPort3AsicInfo_t;
typedef IocVlvNo0Data_t Ioc_OutputSR1msIocVlvNo0Data_t;
typedef AchWssPort0AsicInfo_t Ach_InputAchWssPort0AsicInfo_t;
typedef WssMonInfo_t Ioc_InputSR5msWssMonInfo_t;
typedef AchWssPort2AsicInfo_t Ach_InputAchWssPort2AsicInfo_t;
typedef VlvdFF0DcdInfo_t Vlvd_A3944_HndlrVlvdFF0DcdInfo_t;
typedef RisngTiStampInfo_t Icu_InputCaptureRisngTiStampInfo_t;
typedef MainCanEn_t CanTrv_TLE6251_HndlrMainCanEn_t;
typedef CspMon_t Ioc_InputSR1msCspMon_t;
typedef Vdd3Mon_t Ioc_InputSR1msVdd3Mon_t;
typedef SwTrigMotInfo_t AdcIf_Conv1msSwTrigMotInfo_t;
typedef TempMon_t Ioc_InputSR1msTempMon_t;
typedef AchAdcData_t Ach_InputAchAdcData_t;
typedef HalPressureInfo_t Ioc_InputSR1msHalPressureInfo_t;
typedef SwTrigTmpInfo_t AdcIf_Conv1msSwTrigTmpInfo_t;
typedef FspAbsHMon_t Ioc_InputSR1msFspAbsHMon_t;
typedef PbcVdaAi_t Ioc_InputCS1msPbcVdaAi_t;
typedef SwLineMonInfo_t Ioc_InputCS1msSwLineMonInfo_t;
typedef IocDcMtrFreqData_t Ioc_OutputSR1msIocDcMtrFreqData_t;
typedef ReservedMonInfoEsc_t Ioc_InputSR1msReservedMonInfoEsc_t;
typedef IocVlvEsv0Data_t Ioc_OutputSR1msIocVlvEsv0Data_t;
typedef VBatt2Mon_t Ioc_InputSR1msVBatt2Mon_t;
typedef MpsD2IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t;
typedef FallIdxInfo_t Icu_InputCaptureFallIdxInfo_t;
typedef FallTiStampInfo_t Icu_InputCaptureFallTiStampInfo_t;
typedef AdcInvalid_t AdcIf_Conv1msAdcInvalid_t;
typedef MgdInvalid_t Mgd_TLE9180_HndlrMgdInvalid_t;
typedef MotCurrMonInfo_t Ioc_InputSR50usMotCurrMonInfo_t;
typedef AchWssPort1AsicInfo_t Ach_InputAchWssPort1AsicInfo_t;
typedef AcmAsicInitCompleteFlag_t Acm_MainAcmAsicInitCompleteFlag_t;
typedef IocVlvNc3Data_t Ioc_OutputSR1msIocVlvNc3Data_t;
typedef IocVlvTc0Data_t Ioc_OutputSR1msIocVlvTc0Data_t;
typedef HwTrigMotInfo_t AdcIf_Conv50usHwTrigMotInfo_t;
typedef FspAbsMon_t Ioc_InputSR1msFspAbsMon_t;
typedef IocVlvNc0Data_t Ioc_OutputSR1msIocVlvNc0Data_t;
typedef IocVlvRelayData_t Ioc_OutputSR1msIocVlvRelayData_t;
typedef AchMotorAsicInfo_t Ach_InputAchMotorAsicInfo_t;
typedef SwtMonInfoEsc_t Ioc_InputCS1msSwtMonInfoEsc_t;
typedef RsmDbcMon_t Ioc_InputCS1msRsmDbcMon_t;
typedef SentHPressureInfo_t SentH_MainSentHPressureInfo_t;
typedef MotMonInfo_t Ioc_InputSR1msMotMonInfo_t;
typedef MocMonInfo_t Ioc_InputSR1msMocMonInfo_t;
typedef RisngIdxInfo_t Icu_InputCaptureRisngIdxInfo_t;
typedef EpbMonInfoEsc_t Ioc_InputSR1msEpbMonInfoEsc_t;
typedef MpsD2SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_t;
typedef VlvMonInfoEsc_t Ioc_InputSR1msVlvMonInfoEsc_t;
typedef Ext5vMon_t Ioc_InputSR1msExt5vMon_t;
typedef IocVlvNo3Data_t Ioc_OutputSR1msIocVlvNo3Data_t;
typedef IocVlvNo2Data_t Ioc_OutputSR1msIocVlvNo2Data_t;
typedef VlvdFF1DcdInfo_t Vlvd_A3944_HndlrVlvdFF1DcdInfo_t;
typedef Vdd4Mon_t Ioc_InputSR1msVdd4Mon_t;
typedef MpsD1SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_t;
typedef FspCbsMon_t Ioc_InputSR1msFspCbsMon_t;
typedef Vdd5Mon_t Ioc_InputSR1msVdd5Mon_t;
typedef IocVlvTc1Data_t Ioc_OutputSR1msIocVlvTc1Data_t;
typedef IocVlvNc2Data_t Ioc_OutputSR1msIocVlvNc2Data_t;
typedef PedlPwrMonInfo_t Ioc_InputSR1msPedlPwrMonInfo_t;
typedef IocDcMtrDutyData_t Ioc_OutputSR1msIocDcMtrDutyData_t;
typedef MotMonInfoEsc_t Ioc_InputSR1msMotMonInfoEsc_t;
typedef MotVoltsMonInfo_t Ioc_InputSR1msMotVoltsMonInfo_t;
typedef MpsD1IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t;
typedef IocVlvNo1Data_t Ioc_OutputSR1msIocVlvNo1Data_t;
typedef AchSysPwrAsicInfo_t Ach_InputAchSysPwrAsicInfo_t;
typedef MpsInvalid_t Mps_TLE5012_Hndlr_1msMpsInvalid_t;
typedef AchValveAsicInfo_t Ach_InputAchValveAsicInfo_t;
typedef CEMon_t Ioc_InputSR1msCEMon_t;
typedef SwtMonInfo_t Ioc_InputCS1msSwtMonInfo_t;
typedef AchVsoSelAsicInfo_t Ach_InputAchVsoSelAsicInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* HAL_DATAHANDLE_TYPES_H_ */
/** @} */
