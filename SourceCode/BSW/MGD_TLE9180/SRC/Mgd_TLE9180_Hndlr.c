/**
 * @defgroup Mgd_TLE9180_Hndlr Mgd_TLE9180_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mgd_TLE9180_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mgd_TLE9180_Hndlr.h"
#include "Mgd_TLE9180_Hndlr_Ifa.h"
#include "IfxStm_reg.h"
#include "Mgd_TLE9180_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MGD_TLE9180_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MGD_TLE9180_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGD_TLE9180_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mgd_TLE9180_Hndlr_HdrBusType Mgd_TLE9180_HndlrBus;

/* Version Info */
const SwcVersionInfo_t Mgd_TLE9180_HndlrVersionInfo = 
{   
    MGD_TLE9180_HNDLR_MODULE_ID,           /* Mgd_TLE9180_HndlrVersionInfo.ModuleId */
    MGD_TLE9180_HNDLR_MAJOR_VERSION,       /* Mgd_TLE9180_HndlrVersionInfo.MajorVer */
    MGD_TLE9180_HNDLR_MINOR_VERSION,       /* Mgd_TLE9180_HndlrVersionInfo.MinorVer */
    MGD_TLE9180_HNDLR_PATCH_VERSION,       /* Mgd_TLE9180_HndlrVersionInfo.PatchVer */
    MGD_TLE9180_HNDLR_BRANCH_VERSION       /* Mgd_TLE9180_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Mgd_TLE9180_HndlrEcuModeSts;

/* Output Data Element */
Mgd_TLE9180_HndlrMgdDcdInfo_t Mgd_TLE9180_HndlrMgdDcdInfo;
Mgd_TLE9180_HndlrMgdInvalid_t Mgd_TLE9180_HndlrMgdInvalid;

uint32 Mgd_TLE9180_Hndlr_Timer_Start;
uint32 Mgd_TLE9180_Hndlr_Timer_Elapsed;

#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_HNDLR_START_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (32BIT)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MGD_TLE9180_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Mgd_TLE9180_MemMap.h"
#define MGD_TLE9180_HNDLR_START_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/** Variable Section (32BIT)**/


#define MGD_TLE9180_HNDLR_STOP_SEC_VAR_32BIT
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MGD_TLE9180_HNDLR_START_SEC_CODE
#include "Mgd_TLE9180_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mgd_TLE9180_Hndlr_Init(void)
{
    /* Initialize internal bus */
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrEcuModeSts = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3 = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort = 0;
    Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdInvalid = 0;
    Mgd_TLE9180_Process_Init();
}

void Mgd_TLE9180_Hndlr(void)
{
    Mgd_TLE9180_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mgd_TLE9180_Hndlr_Read_Mgd_TLE9180_HndlrEcuModeSts(&Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrEcuModeSts);

    /* Process */
	Mgd_TLE9180_Process(&Mgd_TLE9180_HndlrBus);

    /* Output */
    Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdDcdInfo(&Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdDcdInfo);
    /*==============================================================================
    * Members of structure Mgd_TLE9180_HndlrMgdDcdInfo 
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3;
     : Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/
    
    Mgd_TLE9180_Hndlr_Write_Mgd_TLE9180_HndlrMgdInvalid(&Mgd_TLE9180_HndlrBus.Mgd_TLE9180_HndlrMgdInvalid);

    Mgd_TLE9180_Hndlr_Timer_Elapsed = STM0_TIM0.U - Mgd_TLE9180_Hndlr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MGD_TLE9180_HNDLR_STOP_SEC_CODE
#include "Mgd_TLE9180_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
