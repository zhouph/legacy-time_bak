/**
 * @defgroup Mgd_TLE9180_Device Mgd_TLE9180_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mgd_TLE9180_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mgd_TLE9180_Process.h"
#include "Spim_Cdd_Cfg.h"
#include "dio.h"
#include "IfxPort_reg.h"
#include "IfxStm_reg.h"
#include "Common.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
								
uint8 mgd9180_configure_complete = 0;
uint32 TEMP_TEST_ERROR_COUNT =0;
uint32 u32Mgd9180_Normal_Spi_Check =0;

Mgd_TLE9180_RxMsg_Type  MGD_RxFrame[MGD9180_MAX_BUFFER_SIZE];
Mgd_TLE9180_TxMsg_Type  MGD_TxFrame[MGD9180_MAX_BUFFER_SIZE];

Mgd_TLE9180_MsgConfigType MGD_MsgConfig[MGD9180_ID_MAX];

MGD9180_RX_MSG_t MGD9180_RX_MSG;
MGD9180_TX_MSG_t MGD9180_TX_MSG;

uint8  Mgd8180_ErrPinStatus = 0;
uint8  u8Mgd9180_NormalSpiStatus = 0;
uint16	crc_temp	= 0;

MGD_TLE9180_ERR_MONITOR_t MGD9180_ERR_MON;

/* Internal Bus */


/* Version Info */


/* Input Data Element */


/* Output Data Element */

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/** Variable Section (NOINIT_UNSPECIFIED)**/


/** Variable Section (NOINIT_32BIT)**/

/** Variable Section (UNSPECIFIED)**/


/** Variable Section (32BIT)**/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static uint8 Mgd_TLE9180_CRC3_Gen(uint32 DATA);
static void Mgd_TLE9180_Init_Config(void);
static void Mgd_TLE9180_Error_Monitor_Cmd(void);
void Mgd_TLE9180_SpiCallback(Mgd_TLE9180_Hndlr_HdrBusType *Mgd_TLE9180_SpiCallBack);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mgd_TLE9180_Init_Transfer(void)
{
    uint32       MsgIdIndex           = 0;                                                                                       
    uint16       msg_num              = 0;

	Mgd8180_ErrPinStatus = P32_IN.B.P0;
	for(MsgIdIndex = 0u ; MsgIdIndex < MGD9180_ID_MAX ; MsgIdIndex++)                                                             
    {                                                                                                                            
        if((MGD_MsgConfig[MsgIdIndex].RD_REQ == MGD_REQ_SET)||(MGD_MsgConfig[MsgIdIndex].WRT_REQ == MGD_REQ_SET))                
        {                                                                                                                        
            if(MGD_MsgConfig[MsgIdIndex].WRT_REQ == MGD_REQ_SET)    /* Set Write Spi Data */                                     
            {                                                                                                                    
                MGD_TxFrame[msg_num].B.mgd_Tx_command   = MGD_SPI_RW_WRITE;                                                         
                MGD_TxFrame[msg_num].B.mgd_Tx_data  	= MGD_MsgConfig[MsgIdIndex].DATA;                                            
                MGD_MsgConfig[MsgIdIndex].WRT_REQ   	= MGD_REQ_NOT_SET;                                                           
            }                                                                                                                    
            MGD_TxFrame[msg_num].B.mgd_Tx_addr      = MGD_MsgConfig[MsgIdIndex].ADD;                                                                                                       
            MGD_TxFrame[msg_num].B.mgd_Tx_crc       = Mgd_TLE9180_CRC3_Gen(MGD_TxFrame[msg_num].R);                                   
            MGD_MsgConfig[MsgIdIndex].RD_REQ        = MGD_REQ_NOT_SET;                                                           
            MGD_MsgConfig[MsgIdIndex].STATUS        = MGD_STATUS_SENT;                                                                                                                                                            
            msg_num++;                                                                                                           
        }                                                                                                                        
    }
	if(msg_num>0)
	{
		/* Dummy MSG */
		{	
			MGD_TxFrame[msg_num].B.mgd_Tx_addr      = MGD9180_ADDR_nop;    
            MGD_TxFrame[msg_num].B.mgd_Tx_crc       = Mgd_TLE9180_CRC3_Gen(MGD_TxFrame[msg_num].R);
			msg_num++;
		}
		/* Send SPI via DMA */
		Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Mtr, (Spim_Cdd_DataType*)MGD_TxFrame, (Spim_Cdd_DataType*)MGD_RxFrame, msg_num);
		Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_Mtr_Vdr);
	}	
}

uint8 Mgd_TLE9180_Init_Decode(void)
{
    /* Decoding MGD(TLE9180) Message */                                                                                                          
    uint32  MsgIdIndex  = 0;                                                                                                             
    uint16  msg_num     = 1;                                                                                                             
	uint16  init_Check_Cnt   = 0;
    uint8   i           = 0;  
	uint8   decodeStatus     = 0;
	Mgd_SpiStatus mgdSpiStatus;
	
    for(MsgIdIndex = 0u ; MsgIdIndex < MGD9180_ID_MAX ; MsgIdIndex++)                                                                     
    {                                                                                                                                    
        if(MGD_MsgConfig[MsgIdIndex].STATUS == MGD_STATUS_SENT)                                                                          
        {                                                                                                                                
            crc_temp = Mgd_TLE9180_CRC3_Gen(MGD_RxFrame[msg_num].R);  
			mgdSpiStatus.R = (uint8)(MGD_RxFrame[msg_num].B.mgd_Rx_status);
            if(mgdSpiStatus.B.SpiError == 1)  /* SPI Error Flag */                                                         
            {                                                                                                                            
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_ERROR;                                                                     
                TEMP_TEST_ERROR_COUNT++;    /* TEST */ 
				init_Check_Cnt++;
            } 
            else if(MGD_RxFrame[msg_num].B.mgd_Rx_crc != crc_temp)  /* CRC Error */                                                      
            {                                                                                                                            
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_ERROR;
                TEMP_TEST_ERROR_COUNT++;    /* TEST */     
				init_Check_Cnt++;				
            }                                                                                                                            
            else if(MGD_RxFrame[msg_num].B.mgd_Rx_addr != MGD_MsgConfig[MsgIdIndex].ADD) /* Address Feedback Error */                 
            {                                                                                                                            
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_ERROR;                                                                     
                TEMP_TEST_ERROR_COUNT++;    /* TEST */  
				init_Check_Cnt++;				
            }                                                                                                                            
            else                                                                                                                         
            {                                                                                                                            
                MGD9180_RX_MSG.A.MGD9180_RxArray[MsgIdIndex] = MGD_RxFrame[msg_num].R;                                                     
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_IDLE;                                                                      
            }                                                                                                                            
            msg_num++; 
        }
    }                                                                                                                                    
    for(i = 0 ; i < MGD9180_MAX_BUFFER_SIZE ; i++)                                                                                               
    {                                                                                                                                    
                                                                                                                                         
        MGD_TxFrame[i].R = 0x0u;                                                                                                       
        MGD_RxFrame[i].R = 0x0u;                                                                                                       
    }                                                                                                                                    
	if(init_Check_Cnt>0)
	{
		decodeStatus = 1;
	}
	else
	{
	    decodeStatus = 0;
	}
return decodeStatus;	
}

volatile uint32 Mgd_TLE9180_InitWait(uint32 u32WaitTime)
{
	volatile uint32 u32Waitdelay=0;
	volatile uint32 u32CurrentTime=0;
	volatile uint32 u32WaitdelayCnt=0;

	u32CurrentTime = STM0_TIM0.U; 
	u32Waitdelay = u32CurrentTime + u32WaitTime;
	
	if(u32Waitdelay<u32CurrentTime)
	{
		u32Waitdelay = (0xFFFFFFFF - u32CurrentTime)+u32WaitTime;
	}
	
	while(u32WaitdelayCnt < u32Waitdelay)
	{
		u32WaitdelayCnt = STM0_TIM0.U;
	}
	return u32WaitdelayCnt;
}

void Mgd_TLE9180_Process_Init(void)
{
	uint32 initCnt = 0x0u;
	uint32 tempCnt = 0x0u;
	uint8  comStatus = 0;	

	/* Initialze I/O Control */
	P32_OMR.U = (uint32_t)(PORT_HIGH << 6);/*Inhibit : Enable*/
	P23_OMR.U = (uint32_t)(PORT_HIGH << 3);/*EN : Disable*/
	P23_OMR.U = (uint32_t)(PORT_HIGH << 2);/*SOFF : Disable*/
	
	Mgd_TLE9180_InitWait(2000000);/*5ns Base : 10ms Delay */

    /* Initialize Version Info */
	mgd9180_configure_complete = 0;
    for(initCnt=0;initCnt<MGD9180_ID_MAX;initCnt++)
	{
		MGD_MsgConfig[initCnt].ADD       = MGD_MsgAddArray[initCnt];
		MGD_MsgConfig[initCnt].WRT_REQ   = MGD_REQ_NOT_SET;
		MGD_MsgConfig[initCnt].RD_REQ    = MGD_REQ_NOT_SET;
		MGD_MsgConfig[initCnt].STATUS    = MGD_STATUS_IDLE;
		MGD_MsgConfig[initCnt].DATA      = 0;
	}
	for(initCnt=0;initCnt<MGD9180_MAX_BUFFER_SIZE;initCnt++)
	{	
		MGD_RxFrame[initCnt].R=0x0u;
		MGD_TxFrame[initCnt].R=0x0u;
	}
    /* Initialize internal bus */


	/* Initialize Send Configuration */
	P23_OMR.U = (uint32_t)(PORT_LOW << 3);/*EN : Disable*/

	tempCnt = Mgd_TLE9180_InitWait(2000000);/*5ns Base : 10ms Delay */

	Mgd_TLE9180_Error_Monitor_Cmd();
	Mgd_TLE9180_Init_Transfer();
	
	tempCnt = Mgd_TLE9180_InitWait(100000);/*5ns Base : 500us Delay */
	comStatus = Mgd_TLE9180_Init_Decode();

	if(Mgd8180_ErrPinStatus == 1)
	{
		Mgd_TLE9180_Init_Config();
		Mgd_TLE9180_Init_Transfer();	

		tempCnt = Mgd_TLE9180_InitWait(100000);/*5ns Base : 500us Delay */
		
		comStatus = Mgd_TLE9180_Init_Decode();
	}	

	if((Mgd8180_ErrPinStatus==1)&&(comStatus==0))
	{
		mgd9180_configure_complete = 1;		
		P23_OMR.U = (uint32_t)(PORT_HIGH << 3);	/*EN : Enable*/
	}
	else
	{
		mgd9180_configure_complete = 0;
	}
}

void Mgd_TLE9180_Process(Mgd_TLE9180_Hndlr_HdrBusType *Mgd_TLE9180_HndlProcess)
{
    uint32       MsgIdIndex           = 0;                                                                                       
    uint16       msg_num              = 0;

	Mgd_TLE9180_SpiCallback(Mgd_TLE9180_HndlProcess);
	Mgd8180_ErrPinStatus = P32_IN.B.P0;/* Error Status Check */
	if(mgd9180_configure_complete == 1)
	{
		Mgd_TLE9180_Error_Monitor_Cmd();
	}
	else if(mgd9180_configure_complete == 0)
	{
		/* To Do */
		/* Re-Configuration Concept */
	}
	else
	{
		;
	}

	for(MsgIdIndex = 0u ; MsgIdIndex < MGD9180_ID_MAX ; MsgIdIndex++)                                                             
    {                                                                                                                            
        if((MGD_MsgConfig[MsgIdIndex].RD_REQ == MGD_REQ_SET)||(MGD_MsgConfig[MsgIdIndex].WRT_REQ == MGD_REQ_SET))                
        {                                                                                                                        
            if(MGD_MsgConfig[MsgIdIndex].WRT_REQ == MGD_REQ_SET)    /* Set Write Spi Data */                                     
            {                                                                                                                    
                MGD_TxFrame[msg_num].B.mgd_Tx_command   = MGD_SPI_RW_WRITE;                                                         
                MGD_TxFrame[msg_num].B.mgd_Tx_data  	= MGD_MsgConfig[MsgIdIndex].DATA;                                            
                MGD_MsgConfig[MsgIdIndex].WRT_REQ   	= MGD_REQ_NOT_SET;                                                           
            }                                                                                                                    
            MGD_TxFrame[msg_num].B.mgd_Tx_addr      = MGD_MsgConfig[MsgIdIndex].ADD;                                                                                                       
            MGD_TxFrame[msg_num].B.mgd_Tx_crc       = Mgd_TLE9180_CRC3_Gen(MGD_TxFrame[msg_num].R);                                   
            MGD_MsgConfig[MsgIdIndex].RD_REQ        = MGD_REQ_NOT_SET;                                                           
            MGD_MsgConfig[MsgIdIndex].STATUS        = MGD_STATUS_SENT;                                                                                                                                                            
            msg_num++;                                                                                                           
        }                                                                                                                        
    }
	if(msg_num>0)
	{
		/* Dummy MSG */
		{	
			MGD_TxFrame[msg_num].B.mgd_Tx_addr      = MGD9180_ADDR_nop;    
            MGD_TxFrame[msg_num].B.mgd_Tx_crc       = Mgd_TLE9180_CRC3_Gen(MGD_TxFrame[msg_num].R);
			msg_num++;		
		}
		/* Send SPI via DMA */
		Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Mtr, (Spim_Cdd_DataType*)MGD_TxFrame, (Spim_Cdd_DataType*)MGD_RxFrame, msg_num);
		Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_Mtr_Vdr);

	}
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static uint8 Mgd_TLE9180_CRC3_Gen(uint32 DATA)
{
    uint32 l_u32CrcData = 0;
	uint32 tempCnt = 0;
    uint32 l_u32CrcPoly = 0xB00000;
    uint32 l_u32Data_MSB_check = 0;
    uint32 l_u32Data_MSB = 0x800000;
	
    l_u32CrcData =((DATA&0xFFFFF8)^0x800000);  
    l_u32Data_MSB_check = l_u32CrcData&l_u32Data_MSB;
    while(l_u32CrcData > 0x7)
    {
        if(l_u32Data_MSB_check > 0)
        {
            l_u32CrcData = l_u32CrcData^l_u32CrcPoly;
        }
        l_u32Data_MSB = l_u32Data_MSB >> 1;
        l_u32CrcPoly = l_u32CrcPoly >> 1;
        l_u32Data_MSB_check = l_u32CrcData&l_u32Data_MSB;
    }
    return (uint8)l_u32CrcData;

}

uint8_t Mgd_TLE9180_Crc8_Gen(const void *vptr, int len)
{	
	const uint8_t *data = vptr;	
	unsigned crc = 0;	
	int i, j;	
	for (j = len; j; j--, data++) 
	{		
		crc ^= (*data << 8);		
		for(i = 8; i; i--)
		{			
			if (crc & 0x8000)
			{
				crc ^= (0x14D0<<3);
			}
			crc <<= 1;		
		}	
	}	
	return (uint8_t)(crc >> 8);
}

static void Mgd_TLE9180_Init_Config(void)
{
	uint8_t mgdTle9180ConfigTable[32];
	uint8_t mgdTle9180Cnt = 0;
	uint8_t mgdTle9180DataCrc = 0;

	/* Sample */
	mgdTle9180ConfigTable[mgdTle9180Cnt] = 0x00;
	mgdTle9180Cnt++;	
	/* MSG #1 : General Config1*/
	/*
		[7:5] Overtemperature Detection Threshold (tl_ot_w) = 0x2 (160)
		[4] Input Pattern Supervision (in_diag_act) = 0x1 (Enable)
		[3] SPI Window Watchdog (spi_wwd_act) = 0x0 (disable (Default))
		[2] Limp Home Mode Activation (limp_act) = 0x0 (disable (Default))
		[1] VCC Supervision (vcc_sup_off) = 0x0 (Enalbe(Default))
		[0] VCC Monitoring Threshold (vcc_select) = 0x1 (5V)
	*/
    MGD_MsgConfig[MGD9180_ID1_Conf_Gen_1].DATA   = 0x10;
    MGD_MsgConfig[MGD9180_ID1_Conf_Gen_1].WRT_REQ = MGD_REQ_SET;
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID1_Conf_Gen_1].DATA;
	mgdTle9180Cnt++;
	/* MSG #2 : General Config2*/
	/*
		[7]Threshold Level Overcurrent Detection Current Sense Amplifiers
			0B 5V Overcurrent Detection Threshold (default)
		[6]Disable Overvoltage Detection of High-side Buffer Capacitors
			0B OV BSx enabled (default)
		[5]Disable Load Dump Overvoltage Detection at pin VDHP
			0B OV LD enabled (default)
		[4]Disable Shutdown at VDHP
			0B OV SD VDHP enabled (default)
		[3]Enable 3 VDHx sense pins
			1B 3 VDH sense pins and 1 VDHP power pin enabled
		[2]Enable Current Sense Amplifier 3
			0B Current Sense Amplifier 3 deactivated (default)
		[1]Enable Current Sense Amplifier 2
			0B Current Sense Amplifier 2 deactivated
		[0]Enable Current Sense Amplifier 1 and Reference Output Buffer
			0B Current Sense Amplifier 1 and Reference Output Buffer deactivated
	*/
    MGD_MsgConfig[MGD9180_ID2_Conf_Gen_2].DATA   = 0x08;                                                          
    MGD_MsgConfig[MGD9180_ID2_Conf_Gen_2].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID2_Conf_Gen_2].DATA;
	mgdTle9180Cnt++;

	/* MSG #3 : General Config3*/
	/*
		[7] none
		[6] rmw Enable ART pin
			1B ART pin enabled; APC disabled
		[5:3] rmw Timing Activation of Active Phase Cut Off (APC)
			011B 20ms (default)
		[2:1] rmw Active Phase Cut Off (APC) Output Signal Configuration
			10B Active stuck - oscillating in normal mode (default)
		[0] rmw Failure Behavior of Short Circuit Detection in ART Mode
			0B ARE - Short Circuit Detection only (default)
	*/
    MGD_MsgConfig[MGD9180_ID3_Conf_Gen_3].DATA   = 0x5C;                                                          
    MGD_MsgConfig[MGD9180_ID3_Conf_Gen_3].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID3_Conf_Gen_3].DATA;
	mgdTle9180Cnt++;

	/* MSG #4 : Window Watchdog*/
	/*
		[7:5] Loop Counter of Window Watchdog (increment 2; decrement 1)
			010B 6 (default)
		[4:2] Ratio between Locked and Evaluation Window
			101B 90% (default)
		[1:0] Period of Window Watchdog
			10B 5ms (default)
	*/
    MGD_MsgConfig[MGD9180_ID4_Conf_wwd].DATA   = 0x56;                                                          
    MGD_MsgConfig[MGD9180_ID4_Conf_wwd].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID4_Conf_wwd].DATA;
	mgdTle9180Cnt++;

	/* MSG #5 : Vs Over & Under voltage*/
	/*
		[7:4] Vs Overvoltage Threshold
			0000B 18.00V (default)
		[3:0] Vs Undervoltage Threshold
			0111B 7.01V
	*/
    MGD_MsgConfig[MGD9180_ID5_Tl_vs].DATA   = 0x07;                                                          
    MGD_MsgConfig[MGD9180_ID5_Tl_vs].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID5_Tl_vs].DATA;
	mgdTle9180Cnt++;

	/* MSG #6 : VDHP Over- and Undervoltage Thresholds*/
	/*
		[7:4] VDHP Overvoltage Threshold
		1000B 50.01V
		[3:0] VDHP Undervoltage Threshold
		0000B 3.96V (default)
	*/
    MGD_MsgConfig[MGD9180_ID6_Tl_vdh].DATA   = 0x80;                                                          
    MGD_MsgConfig[MGD9180_ID6_Tl_vdh].WRT_REQ = MGD_REQ_SET;
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID6_Tl_vdh].DATA;
	mgdTle9180Cnt++;

	/* MSG #7 : CB Under- and VCC Under- and Overvoltage Thresholds*/
	/*
		[7:4] rmw CB Undervoltage Threshold
		1001B 9.07V (default)
		
		[3:2] rmw VCC Overvoltage Threshold
		01B 4% of configured VCC supply voltage
		
		[1:0] rmw VCC Undervoltage Threshold
		01B 4% of configured VCC supply voltage
	*/
    MGD_MsgConfig[MGD9180_ID7_Tl_cbvcc].DATA   = 0x95;                                                          
    MGD_MsgConfig[MGD9180_ID7_Tl_cbvcc].WRT_REQ = MGD_REQ_SET;
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID7_Tl_cbvcc].DATA;
	mgdTle9180Cnt++;

	/* MSG #8 : Charge Pump/High-side Buffer Failure Modes*/
	/*
		[7:6] rmw CB Undervoltage Failure Behavior
		00B W - Warning (default)
		[5] none
		[4] rmw Overload Charge Pump 2 Failure Behavior
		1B Shutdown of output stages (default)
		[3:2] none
		[1:0] rmw Undervoltage High-side Buffer Capacitor Failure Behavior
		00B W - Warning (default)
	*/
    MGD_MsgConfig[MGD9180_ID8_Fm_1].DATA   = 0x10;                                                          
    MGD_MsgConfig[MGD9180_ID8_Fm_1].WRT_REQ = MGD_REQ_SET;
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID8_Fm_1].DATA;
	mgdTle9180Cnt++;

	/* MSG #9 : Miscellaneous Failure Modes*/
	/*
		[7:6] APC activation
		10B Triggered by VCC Overvoltage and SPI Window Watchdog Timeout
		[5:4] SPI Window Watchdog Time-out Failure Behavior
		11B LE - Latched Error (default)
		[3:2] none
		[1:0] Overtemperature Detection Failure Behavior
		00B W - Warning (default)
	*/
    MGD_MsgConfig[MGD9180_ID9_Fm_2].DATA   = 0x80;                                                          
    MGD_MsgConfig[MGD9180_ID9_Fm_2].WRT_REQ = MGD_REQ_SET;
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID9_Fm_2].DATA;
	mgdTle9180Cnt++;

	/* MSG #10 : Vs & VDHP & VCC Undervoltage Failure Modes*/
	/*
		[7:6] none
		[5:4] Vs Undervoltage Failure Behavior
		00B W - Warning
		[3:2] VDHP Undervoltage Failure Behavior
		00B W - Warning (default)
		[1:0] VCC Undervoltage Failure Behavior
		00B W - Warning (default)
	*/
    MGD_MsgConfig[MGD9180_ID10_Fm_3].DATA   = 0x00;                                                          
    MGD_MsgConfig[MGD9180_ID10_Fm_3].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID10_Fm_3].DATA;
	mgdTle9180Cnt++;

	/* MSG #11 : Vs & VDHP & VCC Overvoltage Failure Modes*/
	/*
		[7] none
		[6:5] Vs Overvoltage Failure Behavior
		00B W - Warning
		[4:2] VDHP Overvoltage Failure Behavior
		000B W - Warning (default)
		[1:0] VCC Overvoltage Failure Behavior
		00B W - Warning (default)
	*/
    MGD_MsgConfig[MGD9180_ID11_Fm_4].DATA   =0x00;                                                          
    MGD_MsgConfig[MGD9180_ID11_Fm_4].WRT_REQ = MGD_REQ_SET;
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID11_Fm_4].DATA;
	mgdTle9180Cnt++;

	/* MSG #12: Short Circuit Detection & Signal Path Supervision Failure Modes*/
	/*
		[7:5] Short Ciruit Detection Failure Behavior
		011B LE - Latched Error, all external FETs off (default)
		[4] Digital Output Pin Overload Failure Behavior
		0B No shutdown of output stages (default)
		[3:2] Output Stage Feedback Failure Behavior
		00B W - Warning (default)
		[1:0] Input Pattern Supervision Failure Behavior
		00B W - Warning (default)
	*/
    MGD_MsgConfig[MGD9180_ID12_Fm_5].DATA   = 0x60;                                                          
    MGD_MsgConfig[MGD9180_ID12_Fm_5].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID12_Fm_5].DATA;
	mgdTle9180Cnt++;

	/* MSG #13 : Dead Time High-side*/
	/*
		[7:0] Dead time for high-side output stages
		[formula: value*35.7+107; unit: ns]
		00000000B 107ns - minimum dead time
	*/
    MGD_MsgConfig[MGD9180_ID13_Dt_hs].DATA   = 0x0;                                                         
    MGD_MsgConfig[MGD9180_ID13_Dt_hs].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID13_Dt_hs].DATA;
	mgdTle9180Cnt++;

	/* MSG #14 : Dead Time Low-side*/
	/*
		[7:0] Dead time for low-side output stages
		[formula: value*35.7+107; unit: ns]
		00000000B 107ns - minimum dead time
	*/
    MGD_MsgConfig[MGD9180_ID14_Dt_ls].DATA   = 0x0;                                                         
    MGD_MsgConfig[MGD9180_ID14_Dt_ls].WRT_REQ = MGD_REQ_SET;		
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID14_Dt_ls].DATA;
	mgdTle9180Cnt++;

	/* MSG #15 : Undervoltage Filter Times*/
	/*
		[7:6] rmw High-side Buffer Capacitor Undervoltage Filter Time
		10B 5レs (default)
		[5:4] rmw CB Undervoltage Filter Time
		00B 10レs (default)
		[3:2] rmw VDHP Undervoltage Filter Time
		01B 25レs (default)
		[1:0] Vs Undervoltage Filter Time
		01B 25レs (default)
	*/
    MGD_MsgConfig[MGD9180_ID15_Ft_1].DATA   = 0x85;                                                          
    MGD_MsgConfig[MGD9180_ID15_Ft_1].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] =  MGD_MsgConfig[MGD9180_ID15_Ft_1].DATA;
	mgdTle9180Cnt++;

	/* MSG #16 : Overvoltage and VCC Filter Times*/
	/*
		[7:6] VCC Undervoltage Filter Time
		01B 25レs (default)
		[5:4] VCC Overvoltage Filter Time
		01B 25レs (default)
		[3:2] VDHP Overvoltage Filter Time
		00B 10レs (default)
		[1:0] Vs Overvoltage Filter Time
		00B 10レs (default)
	*/
    MGD_MsgConfig[MGD9180_ID16_Ft_2].DATA   = 0x50;                                                          
    MGD_MsgConfig[MGD9180_ID16_Ft_2].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID16_Ft_2].DATA;
	mgdTle9180Cnt++;

	/* MSG #17 : Overtemperature & Short Circuit Detection Filter Times*/
	/*
		[7] Overtemperature Shutdown Filter Time
		0B 10レs (default)
		[6:5] Overtemperature Detection Filter Time
		00B 10レs (default)
		[4:2] Short Circuit Detection Blanking Time
		000B 0.5レs
		[1:0] Short Circuit Detection Filter Time
		10B 4レs (default)
	*/
    MGD_MsgConfig[MGD9180_ID17_Ft_3].DATA   = 0x02;                                                          
    MGD_MsgConfig[MGD9180_ID17_Ft_3].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID17_Ft_3].DATA;
	mgdTle9180Cnt++;
	
	/* MSG #18 : Overcurrent Filter Time*/
	/*
		[7:2] none
		[1:0] Current Sense Amplifier Overcurrent Filter Time
		10B 5レs (default)
	*/
    MGD_MsgConfig[MGD9180_ID18_Ft_4].DATA   = 0x02;                                                          
    MGD_MsgConfig[MGD9180_ID18_Ft_4].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID18_Ft_4].DATA;
	mgdTle9180Cnt++;

	/* MSG #19*/
	/*
		[7:6] none
		[5:4] Current Sense Amplifier 3 Overcurrent Failure Behavior
		00B W - Warning (default)
		[3:2] Current Sense Amplifier 2 Overcurrent Failure Behavior
		00B W - Warning (default)
		[1:0] Current Sense Amplifier 1 Overcurrent Failure Behavior
		00B W - Warning (default)
	*/
    MGD_MsgConfig[MGD9180_ID19_Fm_6].DATA   = 0x0;                                                          
    MGD_MsgConfig[MGD9180_ID19_Fm_6].WRT_REQ = MGD_REQ_SET;	
	mgdTle9180ConfigTable[mgdTle9180Cnt] = MGD_MsgConfig[MGD9180_ID19_Fm_6].DATA;
	mgdTle9180Cnt++;

	/* Configuration Signature Set */
	mgdTle9180DataCrc = Mgd_TLE9180_Crc8_Gen(mgdTle9180ConfigTable,mgdTle9180Cnt);
	
	MGD_MsgConfig[MGD9180_ID0_Conf_Sig].DATA   =mgdTle9180DataCrc;															
	MGD_MsgConfig[MGD9180_ID0_Conf_Sig].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID25_Sc_ls_1].DATA   =0x2D;															
	MGD_MsgConfig[MGD9180_ID25_Sc_ls_1].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID26_Sc_ls_2].DATA   =0x2D;															
	MGD_MsgConfig[MGD9180_ID26_Sc_ls_2].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID27_Sc_ls_3].DATA   =0x2D;															
	MGD_MsgConfig[MGD9180_ID27_Sc_ls_3].WRT_REQ = MGD_REQ_SET;		

	MGD_MsgConfig[MGD9180_ID28_Sc_hs_1].DATA   =0x2D;															
	MGD_MsgConfig[MGD9180_ID28_Sc_hs_1].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID29_Sc_hs_2].DATA   =0x2D;															
	MGD_MsgConfig[MGD9180_ID29_Sc_hs_2].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID30_Sc_hs_3].DATA   =0x2D;															
	MGD_MsgConfig[MGD9180_ID30_Sc_hs_3].WRT_REQ = MGD_REQ_SET;		

	MGD_MsgConfig[MGD9180_ID31_Li_ctr].DATA   =0xE8;															
	MGD_MsgConfig[MGD9180_ID31_Li_ctr].WRT_REQ = MGD_REQ_SET;		

	MGD_MsgConfig[MGD9180_ID32_Misc_ctr].DATA   =0x00;															
	MGD_MsgConfig[MGD9180_ID32_Misc_ctr].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID37_art_entry].DATA   =0x80;															
	MGD_MsgConfig[MGD9180_ID37_art_entry].WRT_REQ = MGD_REQ_SET;	

	MGD_MsgConfig[MGD9180_ID40_Ds_mark].DATA   =0x3F;															
	MGD_MsgConfig[MGD9180_ID40_Ds_mark].WRT_REQ = MGD_REQ_SET;		
}

static void Mgd_TLE9180_Error_Monitor_Cmd(void)
{
	MGD_MsgConfig[MGD9180_ID44_Om_over].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID45_Err_overr].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID46_Ser].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID47_Err_i_1].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID48_Err_i_2].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID49_Err_e].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID50_Err_sd].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID51_Err_scd].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID52_Err_indiag].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID53_Err_osf].RD_REQ = MGD_REQ_SET;	
	MGD_MsgConfig[MGD9180_ID54_Err_spiconf].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID55_Err_op_12].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID56_Err_op_3].RD_REQ = MGD_REQ_SET;
	MGD_MsgConfig[MGD9180_ID57_Err_outp].RD_REQ = MGD_REQ_SET;		
}

void Mgh_TLE9180_CopySigData(Mgd_TLE9180_Hndlr_HdrBusType *Mgd_TLE9180_SpiRxCopy)
{
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_idle_m		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_conf_m		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_conf_lock	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_self_test_m	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_soff_m				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM            =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_err_m				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_rect_m				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Om_over.B.mgd_rx64_norm_m				   ;															                   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf             =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_osf				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp              =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_op 				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd             =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_scd				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd              =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_sd 				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_indiag 		       ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp            =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_outp				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt             =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_ext				   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_overr.B.mgd_rx65_int12			   ;															                   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom             =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_rom					   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_limp_on			     ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete    =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_st_incomplete	     ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_apc_act				 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm             =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_gtm					 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid  =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_ctrl_reg_invalid      ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw             =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_lfw					 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Ser.B.mgd_rx66_err_ot_w			     ;															   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_ov_reg1		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_uv_vcc_rom	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_uv_reg4		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_ov_reg6		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_uv_reg6		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_uv_reg5		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb         =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_uv_cb		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim      =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_1.B.mgd_rx67_err_clk_trim		 ;																 
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_uv_bs3 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_uv_bs2 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_uv_bs1 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_cp2			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_cp1			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_ov_bs3 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_ov_bs2 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_i_2.B.mgd_rx68_err_ov_bs1 		 ;															   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_ov_vdh			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_uv_vdh			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs         =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_ov_vs			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs         =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_uv_vs			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_uv_vcc			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_ov_vcc			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh      =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_e.B.mgd_rx69_err_ov_ld_vdh	     ;																 
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck      =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_ddp_stuck		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1           =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_cp1			   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_ov_cp			   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_clkfail		   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_uv_cb			   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh         =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_ov_vdh 	       ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_ov_vs			   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt            =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_sd.B.mgd_rx70_sd_ot 			   ;															   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_scd.B.mgd_rx71_err_scd_ls3		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_scd.B.mgd_rx71_err_scd_ls2		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_scd.B.mgd_rx71_err_scd_ls1		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_scd.B.mgd_rx71_err_scd_hs3		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_scd.B.mgd_rx71_err_scd_hs2		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_scd.B.mgd_rx71_err_scd_hs1		 ;																 
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_indiag.B.mgd_rx72_err_ind_ls3 	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_indiag.B.mgd_rx72_err_ind_ls2 	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_indiag.B.mgd_rx72_err_ind_ls1 	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_indiag.B.mgd_rx72_err_ind_hs3 	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_indiag.B.mgd_rx72_err_ind_hs2 	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_indiag.B.mgd_rx72_err_ind_hs1 	 ;																  
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_osf.B.mgd_rx73_err_osf_ls1		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_osf.B.mgd_rx73_err_osf_ls2		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_osf.B.mgd_rx73_err_osf_ls3		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_osf.B.mgd_rx73_err_osf_hs1		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_osf.B.mgd_rx73_err_osf_hs2		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_osf.B.mgd_rx73_err_osf_hs3		 ;							 								  
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_err_spi_frame	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_err_spi_to 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_err_spi_wd 		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc       =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_err_spi_crc		 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid   =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_spi_add_invalid	 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo          =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_conf_to			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid  =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_spiconf.B.mgd_rx74_conf_sig_invalid  ;																 
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_oc_op1			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_op1_uv			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_op1_ov			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_op1_calib	     ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_oc_op2			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_op2_uv			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_op2_ov			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_12.B.mgd_rx75_err_op2_calib	     ;															   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_3.B.mgd_rx76_err_oc_op3			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_3.B.mgd_rx76_err_op3_uv			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov        =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_3.B.mgd_rx76_err_op3_ov			 ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_op_3.B.mgd_rx76_err_op3_calib	   ;															   
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_outp.B.mgd_rx77_err_outp_errn	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_outp.B.mgd_rx77_err_outp_miso	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_outp.B.mgd_rx77_err_outp_PFB1	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_outp.B.mgd_rx77_err_outp_PFB2	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3     =(uint8)MGD9180_RX_MSG.A.M.mgd9180_Rx_Err_outp.B.mgd_rx77_err_outp_PFB3	   ;
	Mgd_TLE9180_SpiRxCopy->Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort  = Mgd8180_ErrPinStatus;
}

void Mgd_TLE9180_SpiCallback(Mgd_TLE9180_Hndlr_HdrBusType *Mgd_TLE9180_SpiCallBack)
{
    /* Decoding MGD(TLE9180) Message */                                                                                                          
    uint32  MsgIdIndex  = 0;                                                                                                             
    uint16  msg_num     = 1;                                                                                                             
	uint16  init_Check_Cnt   = 0;
    uint8   i           = 0;                                                                                                             
	Mgd_SpiStatus mgdSpiStatus;
	
    for(MsgIdIndex = 0u ; MsgIdIndex < MGD9180_ID_MAX ; MsgIdIndex++)                                                                     
    {                                                                                                                                    
        if(MGD_MsgConfig[MsgIdIndex].STATUS == MGD_STATUS_SENT)                                                                          
        {                                                                                                                                
            crc_temp = Mgd_TLE9180_CRC3_Gen(MGD_RxFrame[msg_num].R);  
			mgdSpiStatus.R = (uint8)((MGD_RxFrame[msg_num].B.mgd_Rx_status)|((uint8)0x0u));
			u8Mgd9180_NormalSpiStatus = mgdSpiStatus.R;
            if(mgdSpiStatus.B.SpiError == 1)  /* SPI Error Flag */                                                         
            {                                                                                                                            
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_ERROR;                                                                     
                u32Mgd9180_Normal_Spi_Check++;    /* TEST */
			} 
            else if(MGD_RxFrame[msg_num].B.mgd_Rx_crc != crc_temp)  /* CRC Error */                                                      
            {                                                                                                                            
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_ERROR;
                u32Mgd9180_Normal_Spi_Check++;    /* TEST */                                                                                   
            }                                                                                                                            
            else if(MGD_RxFrame[msg_num].B.mgd_Rx_addr != MGD_MsgConfig[MsgIdIndex].ADD) /* Address Feedback Error */                 
            {                                                                                                                            
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_ERROR;                                                                     
                u32Mgd9180_Normal_Spi_Check++;    /* TEST */                                                                                   
            }                                                                                                                            
            else                                                                                                                         
            {                                                                                                                            
                MGD9180_RX_MSG.A.MGD9180_RxArray[MsgIdIndex] = MGD_RxFrame[msg_num].R;                                                     
                MGD_MsgConfig[MsgIdIndex].STATUS = MGD_STATUS_IDLE;  
				u32Mgd9180_Normal_Spi_Check = 0;
            }                                                                                                                            
            msg_num++; 
			
        }
    }                                                                                                                                    
    for(i = 0 ; i < MGD9180_MAX_BUFFER_SIZE ; i++)                                                                                               
    {                                                                                                                                    
                                                                                                                                         
        MGD_TxFrame[i].R = 0x0u;                                                                                                       
        MGD_RxFrame[i].R = 0x0u;                                                                                                       
    }                                                                                                                                    

   Mgh_TLE9180_CopySigData(Mgd_TLE9180_SpiCallBack); 

}
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
