/**
 * @defgroup Mgd_TLE9180_Device Mgd_TLE9180_Device
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mgd_TLE9180_Device.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MGD_TLE9180_DEVICE_H_
#define MGD_TLE9180_DEVICE_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Mgd_TLE9180_Hndlr.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define MGD9180_ADDR_Conf_Sig          0x0 
#define MGD9180_ADDR_Conf_Gen_1        0x1 
#define MGD9180_ADDR_Conf_Gen_2        0x2 
#define MGD9180_ADDR_Conf_Gen_3        0x3 
#define MGD9180_ADDR_Conf_wwd          0x4 
#define MGD9180_ADDR_Tl_vs             0x5 
#define MGD9180_ADDR_Tl_vdh            0x6 
#define MGD9180_ADDR_Tl_cbvcc          0x7 
#define MGD9180_ADDR_Fm_1              0x8 
#define MGD9180_ADDR_Fm_2              0x9 
#define MGD9180_ADDR_Fm_3              0xA 
#define MGD9180_ADDR_Fm_4              0xB 
#define MGD9180_ADDR_Fm_5              0xC 
#define MGD9180_ADDR_Dt_hs             0xD 
#define MGD9180_ADDR_Dt_ls             0xE 
#define MGD9180_ADDR_Ft_1              0xF 
#define MGD9180_ADDR_Ft_2              0x10
#define MGD9180_ADDR_Ft_3              0x11
#define MGD9180_ADDR_Ft_4              0x12
#define MGD9180_ADDR_Fm_6              0x13
#define MGD9180_ADDR_Op_gai_1          0x20
#define MGD9180_ADDR_Op_gai_2          0x21
#define MGD9180_ADDR_Op_gai_3          0x22
#define MGD9180_ADDR_Op_0cl            0x23
#define MGD9180_ADDR_Op_con            0x24
#define MGD9180_ADDR_Sc_ls_1           0x25
#define MGD9180_ADDR_Sc_ls_2           0x26
#define MGD9180_ADDR_Sc_ls_3           0x27
#define MGD9180_ADDR_Sc_hs_1           0x28
#define MGD9180_ADDR_Sc_hs_2           0x29
#define MGD9180_ADDR_Sc_hs_3           0x2A
#define MGD9180_ADDR_Li_ctr            0x2B
#define MGD9180_ADDR_Misc_ctr          0x2C
#define MGD9180_ADDR_art_tlp           0x2D
#define MGD9180_ADDR_art_tla           0x2E
#define MGD9180_ADDR_art_fi            0x2F
#define MGD9180_ADDR_art_acc           0x30
#define MGD9180_ADDR_art_entry         0x31
#define MGD9180_ADDR_nop               0x32
#define MGD9180_ADDR_Drev_mark         0x33
#define MGD9180_ADDR_Ds_mark           0x34
#define MGD9180_ADDR_Sel_st_1          0x35
#define MGD9180_ADDR_Sel_st_2          0x36
#define MGD9180_ADDR_En_st             0x37
#define MGD9180_ADDR_Om_over           0x40
#define MGD9180_ADDR_Err_overr         0x41
#define MGD9180_ADDR_Ser               0x42
#define MGD9180_ADDR_Err_i_1           0x43
#define MGD9180_ADDR_Err_i_2           0x44
#define MGD9180_ADDR_Err_e             0x45
#define MGD9180_ADDR_Err_sd            0x46
#define MGD9180_ADDR_Err_scd           0x47
#define MGD9180_ADDR_Err_indiag        0x48
#define MGD9180_ADDR_Err_osf           0x49
#define MGD9180_ADDR_Err_spiconf       0x4A
#define MGD9180_ADDR_Err_op_12         0x4B
#define MGD9180_ADDR_Err_op_3          0x4C
#define MGD9180_ADDR_Err_outp          0x4D
#define MGD9180_ADDR_dsm_ls1           0x4E
#define MGD9180_ADDR_dsm_ls2           0x4F
#define MGD9180_ADDR_dsm_ls3           0x50
#define MGD9180_ADDR_dsm_hs1           0x51
#define MGD9180_ADDR_dsm_hs2           0x52
#define MGD9180_ADDR_dsm_hs3           0x53
#define MGD9180_ADDR_Rdm_ls1           0x54
#define MGD9180_ADDR_Rdm_ls2           0x55
#define MGD9180_ADDR_Rdm_ls3           0x56
#define MGD9180_ADDR_Rdm_hs1           0x57
#define MGD9180_ADDR_Rdm_hs2           0x58
#define MGD9180_ADDR_Rdm_hs3           0x59
#define MGD9180_ADDR_temp_ls1          0x5A
#define MGD9180_ADDR_temp_ls2          0x5B
#define MGD9180_ADDR_temp_ls3          0x5C
#define MGD9180_ADDR_temp_hs1          0x5D
#define MGD9180_ADDR_temp_hs2          0x5E
#define MGD9180_ADDR_temp_hs3          0x5F
#define MGD9180_ADDR_wwic              0x60
#define MGD9180_ADDR_res_cc1           0x61
#define MGD9180_ADDR_res_cc2           0x62
#define MGD9180_ADDR_res_cc3           0x63
#define MGD9180_ADDR_res_vcc           0x64
#define MGD9180_ADDR_res_cb            0x65
#define MGD9180_ADDR_res_vs            0x66
#define MGD9180_ADDR_res_vdh           0x67

#define MGD9180_WRITE_CMD	0x1
#define MGD9180_READ_CMD	0x0

#define MGD9180_MAX_BUFFER_SIZE     30u

#define MGD9180_CONFIG_SIGNATURE	0xBAu

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
enum
{
    MGD_DISABLE      = 0u,
    MGD_ENABLE       = 1u
};

enum
{
    MGD_SPI_RW_READ  = 0u,
    MGD_SPI_RW_WRITE = 1u
};

enum
{
    MGD_REQ_NOT_SET  = 0u,
    MGD_REQ_SET      = 1u
};

enum
{
    MGD_STATUS_IDLE  = 0u,
    MGD_STATUS_SENT  = 1u,
    MGD_STATUS_ERROR = 2u
};

typedef enum
{	
	MGD9180_ID1_Conf_Gen_1 = 0x0u,
	MGD9180_ID2_Conf_Gen_2	  ,
	MGD9180_ID3_Conf_Gen_3	  ,
	MGD9180_ID4_Conf_wwd	  ,
	MGD9180_ID5_Tl_vs		  ,
	MGD9180_ID6_Tl_vdh		  ,
	MGD9180_ID7_Tl_cbvcc	  ,
	MGD9180_ID8_Fm_1		  ,
	MGD9180_ID9_Fm_2		  ,
	MGD9180_ID10_Fm_3		  ,
	MGD9180_ID11_Fm_4		  ,
	MGD9180_ID12_Fm_5		  ,
	MGD9180_ID13_Dt_hs		  ,
	MGD9180_ID14_Dt_ls		  ,
	MGD9180_ID15_Ft_1		  ,
	MGD9180_ID16_Ft_2		  ,
	MGD9180_ID17_Ft_3		  ,
	MGD9180_ID18_Ft_4		  ,
	MGD9180_ID19_Fm_6		  ,
	MGD9180_ID0_Conf_Sig	  ,
	MGD9180_ID20_Op_gai_1	  ,
	MGD9180_ID21_Op_gai_2	  ,
	MGD9180_ID22_Op_gai_3	  ,
	MGD9180_ID23_Op_0cl 	  ,
	MGD9180_ID24_Op_con 	  ,
	MGD9180_ID25_Sc_ls_1	  ,
	MGD9180_ID26_Sc_ls_2	  ,
	MGD9180_ID27_Sc_ls_3	  ,
	MGD9180_ID28_Sc_hs_1	  ,
	MGD9180_ID29_Sc_hs_2	  ,
	MGD9180_ID30_Sc_hs_3	  ,
	MGD9180_ID31_Li_ctr 	  ,
	MGD9180_ID32_Misc_ctr	  ,
	MGD9180_ID33_art_tlp	  ,
	MGD9180_ID34_art_tla	  ,
	MGD9180_ID35_art_fi 	  ,
	MGD9180_ID36_art_acc	  ,
	MGD9180_ID37_art_entry	  ,
	MGD9180_ID38_nop		  ,
	MGD9180_ID39_Drev_mark	  ,
	MGD9180_ID40_Ds_mark	  ,
	MGD9180_ID41_Sel_st_1	  ,
	MGD9180_ID42_Sel_st_2	  ,
	MGD9180_ID43_En_st		  ,
	MGD9180_ID44_Om_over	  ,
	MGD9180_ID45_Err_overr	  ,
	MGD9180_ID46_Ser		  ,
	MGD9180_ID47_Err_i_1	  ,
	MGD9180_ID48_Err_i_2	  ,
	MGD9180_ID49_Err_e		  ,
	MGD9180_ID50_Err_sd 	  ,
	MGD9180_ID51_Err_scd	  ,
	MGD9180_ID52_Err_indiag   ,
	MGD9180_ID53_Err_osf	  ,
	MGD9180_ID54_Err_spiconf  ,
	MGD9180_ID55_Err_op_12	  ,
	MGD9180_ID56_Err_op_3	  ,
	MGD9180_ID57_Err_outp	  ,
	MGD9180_ID58_dsm_ls1	  ,
	MGD9180_ID59_dsm_ls2	  ,
	MGD9180_ID60_dsm_ls3	  ,
	MGD9180_ID61_dsm_hs1	  ,
	MGD9180_ID62_dsm_hs2	  ,
	MGD9180_ID63_dsm_hs3	  ,
	MGD9180_ID64_Rdm_ls1	  ,
	MGD9180_ID65_Rdm_ls2	  ,
	MGD9180_ID66_Rdm_ls3	  ,
	MGD9180_ID67_Rdm_hs1	  ,
	MGD9180_ID68_Rdm_hs2	  ,
	MGD9180_ID69_Rdm_hs3	  ,
	MGD9180_ID70_temp_ls1	  ,
	MGD9180_ID71_temp_ls2	  ,
	MGD9180_ID72_temp_ls3	  ,
	MGD9180_ID73_temp_hs1	  ,
	MGD9180_ID74_temp_hs2	  ,
	MGD9180_ID75_temp_hs3	  ,
	MGD9180_ID76_wwic		  ,
	MGD9180_ID77_res_cc1	  ,
	MGD9180_ID78_res_cc2	  ,
	MGD9180_ID79_res_cc3	  ,
	MGD9180_ID80_res_vcc	  ,
	MGD9180_ID81_res_cb 	  ,
	MGD9180_ID82_res_vs 	  ,
	MGD9180_ID83_res_vdh	  ,

	MGD9180_ID_MAX

}Mgd_TLE9180_IdConf; 

static const uint16 MGD_MsgAddArray[MGD9180_ID_MAX] =
{
	{MGD9180_ADDR_Conf_Gen_1  },
	{MGD9180_ADDR_Conf_Gen_2  },
	{MGD9180_ADDR_Conf_Gen_3  },
	{MGD9180_ADDR_Conf_wwd    },
	{MGD9180_ADDR_Tl_vs       },
	{MGD9180_ADDR_Tl_vdh      },
	{MGD9180_ADDR_Tl_cbvcc    },
	{MGD9180_ADDR_Fm_1        },
	{MGD9180_ADDR_Fm_2        },
	{MGD9180_ADDR_Fm_3        },
	{MGD9180_ADDR_Fm_4        },
	{MGD9180_ADDR_Fm_5        },
	{MGD9180_ADDR_Dt_hs       },
	{MGD9180_ADDR_Dt_ls       },
	{MGD9180_ADDR_Ft_1        },
	{MGD9180_ADDR_Ft_2        },
	{MGD9180_ADDR_Ft_3        },
	{MGD9180_ADDR_Ft_4        },
	{MGD9180_ADDR_Fm_6        },
	{MGD9180_ADDR_Conf_Sig    },	
	{MGD9180_ADDR_Op_gai_1    },
	{MGD9180_ADDR_Op_gai_2    },
	{MGD9180_ADDR_Op_gai_3    },
	{MGD9180_ADDR_Op_0cl      },
	{MGD9180_ADDR_Op_con      },
	{MGD9180_ADDR_Sc_ls_1     },
	{MGD9180_ADDR_Sc_ls_2     },
	{MGD9180_ADDR_Sc_ls_3     },
	{MGD9180_ADDR_Sc_hs_1     },
	{MGD9180_ADDR_Sc_hs_2     },
	{MGD9180_ADDR_Sc_hs_3     },
	{MGD9180_ADDR_Li_ctr      },
	{MGD9180_ADDR_Misc_ctr    },
	{MGD9180_ADDR_art_tlp     },
	{MGD9180_ADDR_art_tla     },
	{MGD9180_ADDR_art_fi      },
	{MGD9180_ADDR_art_acc     },
	{MGD9180_ADDR_art_entry   },
	{MGD9180_ADDR_nop         },
	{MGD9180_ADDR_Drev_mark   },
	{MGD9180_ADDR_Ds_mark     },
	{MGD9180_ADDR_Sel_st_1    },
	{MGD9180_ADDR_Sel_st_2    },
	{MGD9180_ADDR_En_st       },
	{MGD9180_ADDR_Om_over     },
	{MGD9180_ADDR_Err_overr   },
	{MGD9180_ADDR_Ser         },
	{MGD9180_ADDR_Err_i_1     },
	{MGD9180_ADDR_Err_i_2     },
	{MGD9180_ADDR_Err_e       },
	{MGD9180_ADDR_Err_sd      },
	{MGD9180_ADDR_Err_scd     },
	{MGD9180_ADDR_Err_indiag  },
	{MGD9180_ADDR_Err_osf     },
	{MGD9180_ADDR_Err_spiconf },
	{MGD9180_ADDR_Err_op_12   },
	{MGD9180_ADDR_Err_op_3    },
	{MGD9180_ADDR_Err_outp    },
	{MGD9180_ADDR_dsm_ls1     },
	{MGD9180_ADDR_dsm_ls2     },
	{MGD9180_ADDR_dsm_ls3     },
	{MGD9180_ADDR_dsm_hs1     },
	{MGD9180_ADDR_dsm_hs2     },
	{MGD9180_ADDR_dsm_hs3     },
	{MGD9180_ADDR_Rdm_ls1     },
	{MGD9180_ADDR_Rdm_ls2     },
	{MGD9180_ADDR_Rdm_ls3     },
	{MGD9180_ADDR_Rdm_hs1     },
	{MGD9180_ADDR_Rdm_hs2     },
	{MGD9180_ADDR_Rdm_hs3     },
	{MGD9180_ADDR_temp_ls1    },
	{MGD9180_ADDR_temp_ls2    },
	{MGD9180_ADDR_temp_ls3    },
	{MGD9180_ADDR_temp_hs1    },
	{MGD9180_ADDR_temp_hs2    },
	{MGD9180_ADDR_temp_hs3    },
	{MGD9180_ADDR_wwic        },
	{MGD9180_ADDR_res_cc1     },
	{MGD9180_ADDR_res_cc2     },
	{MGD9180_ADDR_res_cc3     },
	{MGD9180_ADDR_res_vcc     },
	{MGD9180_ADDR_res_cb      },
	{MGD9180_ADDR_res_vs      },
	{MGD9180_ADDR_res_vdh     }    

};
/* Tx Message Set */
                               
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		    uint32 mgd_tx0_crc_sig 	:8;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                         
}Mgd_TLE9180_Tx_Msg0;                                                            
                                                        
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx1_vcc_select 	:1;                                              
		uint32 mgd_tx1_vcc_sup_off	:1;                                              
		uint32 mgd_tx1_limp_act		:1;                                                
		uint32 mgd_tx1_spi_wwd_act	:1;                                              
		uint32 mgd_tx1_in_diag_act	:1;                                              
		uint32 mgd_tx1_tl_ot_w		:3;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                            
}Mgd_TLE9180_Tx_Msg1;                                                            

typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx2_en_op1	 		:1;                                                
		uint32 mgd_tx2_en_op2			:1;                                                
		uint32 mgd_tx2_en_op3			:1;                                                
		uint32 mgd_tx2_en_vdh3			:1;                                              
		uint32 mgd_tx2_dis_sd_vdh		:1;                                              
		uint32 mgd_tx2_dis_ov_ld_vdh	:1;                                            
		uint32 mgd_tx2_dis_ov_bh		:1;                                              
		uint32 mgd_tx2_tl_oc_op			:1;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg2;                                                            

typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx3_art_scd	 		:1;                                              
		uint32 mgd_tx3_apc_conf			:2;                                              
		uint32 mgd_tx3_apc_tact			:3;                                              
		uint32 mgd_tx3_en_ART			:1;                                                
		uint32 mgd_tx3_res0				:1;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg3;                                                            
                                                                                 
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx4_wwd_tp	 		:2;                                                
		uint32 mgd_tx4_wwd_ratio		:3;                                              
		uint32 mgd_tx4_wwd_count		:3; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg4;                                                            
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx5_tl_uv_vs	 		:4;                                              
		uint32 mgd_tx5_tl_ov_vs			:4;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                     
}Mgd_TLE9180_Tx_Msg5;                                                            
                                                                                 
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx6_tl_uv_vdh 		:4;                                              
		uint32 mgd_tx6_tl_ov_vdh		:4; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                       
}Mgd_TLE9180_Tx_Msg6;                                                            
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx7_tl_uv_vcc 		:2;                                              
		uint32 mgd_tx7_tl_ov_vcc		:2;                                              
		uint32 mgd_tx7_tl_uv_cb			:4; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg7;                                                            
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx8_fm_uv_bs 		:2;                                              
		uint32 mgd_tx8_res0				:2;                                                
		uint32 mgd_tx8_fm_cp2_off		:1;                                              
		uint32 mgd_tx8_res1				:1;                                                
		uint32 mgd_tx8_fm_uv_cb			:2; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg8;                                                            
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx9_fm_ot_w	 		:2;                                              
		uint32 mgd_tx9_res0				:2;                                                
		uint32 mgd_tx9_fm_spi_wwd		:2;                                              
		uint32 mgd_tx9_fm_act_apc		:2; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                            
}Mgd_TLE9180_Tx_Msg9;                                                            
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx10_fm_uv_vcc 		:2;                                            
		uint32 mgd_tx10_fm_uv_vdh		:2;                                              
		uint32 mgd_tx10_fm_uv_vs		:2;                                              
		uint32 mgd_tx10_res0			:2;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg10;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx11_fm_ov_vcc 		:2;                                            
		uint32 mgd_tx11_fm_ov_vdh		:3;                                              
		uint32 mgd_tx11_fm_ov_vs		:2;                                              
		uint32 mgd_tx11_res0			:1;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg11;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx12_fm_in_diag 		:2;                                            
		uint32 mgd_tx12_fm_osfb			:2;                                              
		uint32 mgd_tx12_fm_outp_ol		:1;                                            
		uint32 mgd_tx12_fm_scd			:3; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg12;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx13_dths			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg13;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx14_dtls			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg14;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx15_f_uv_vs			:2;                                              
		uint32 mgd_tx15_f_uv_vdh		:2;                                              
		uint32 mgd_tx15_f_uv_cb			:2;                                              
		uint32 mgd_tx15_f_uv_bs			:2; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                         
}Mgd_TLE9180_Tx_Msg15;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx16_f_uv_vs			:2;                                              
		uint32 mgd_tx16_f_ov_vdh		:2;                                              
		uint32 mgd_tx16_f_ov_vcc		:2;                                              
		uint32 mgd_tx16_f_uv_vcc		:2;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg16;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx17_f_fi_scd		:2;                                              
		uint32 mgd_tx17_f_bl_scd		:2;                                              
		uint32 mgd_tx17_f_ot_w			:2;                                              
		uint32 mgd_tx17_f_ot_sd			:2; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg17;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx18_f_oc_op			:2;                                              
		uint32 mgd_tx18_res0			:6; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg18;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx19_fm_oc_op1		:2;                                              
		uint32 mgd_tx19_fm_oc_op2		:2;                                              
		uint32 mgd_tx19_fm_oc_op3		:2;                                              
		uint32 mgd_tx19_res0			:2;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                            
}Mgd_TLE9180_Tx_Msg19;                                                           
                                                                                 
/*=============================================================================  
   Message : Control 0x20H-0x34H                                                 
   ============================================================================*/
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx32_op2_gai1		:3;                                              
		uint32 mgd_tx32_res0			:1;                                                
		uint32 mgd_tx32_op1_gai1		:3;                                              
		uint32 mgd_tx32_res1			:1;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg32;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx33_op2_gai2		:3;                                              
		uint32 mgd_tx33_res0			:1;                                                
		uint32 mgd_tx33_op1_gai2		:3;                                              
		uint32 mgd_tx33_res1			:1;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg33;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx34_op3_gai1		:3;                                              
		uint32 mgd_tx34_res0			:1;                                                
		uint32 mgd_tx34_op3_gai2		:3;                                              
		uint32 mgd_tx34_res1			:1;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg34;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx35_ofs				:6;                                                
		uint32 mgd_tx35_zerocl			:2;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg35;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx36_op3_cal_n		:1;                                              
		uint32 mgd_tx36_op2_cal_n		:1;                                              
		uint32 mgd_tx36_op1_cal_n		:1;                                              
		uint32 mgd_tx36_res0			:2;                                                
		uint32 mgd_tx36_op3_cal			:1;                                              
		uint32 mgd_tx36_op2_cal			:1;                                              
		uint32 mgd_tx36_op1_cal			:1; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg36;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx37_sc_ls_1			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                       
}Mgd_TLE9180_Tx_Msg37;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx38_sc_ls_2			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                        
}Mgd_TLE9180_Tx_Msg38;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx39_sc_ls_3			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg39;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx40_sc_hs_1			:8;    
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                       
}Mgd_TLE9180_Tx_Msg40;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx41_sc_hs_2			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                         
}Mgd_TLE9180_Tx_Msg41;                                                           
                                                                                
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx42_sc_hs_3			:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                            
}Mgd_TLE9180_Tx_Msg42;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx43_dis_hb1			:1;                                              
		uint32 mgd_tx43_dis_hb2			:1;                                              
		uint32 mgd_tx43_dis_hb3			:1;                                              
		uint32 mgd_tx43_ex_limp			:1;                                              
		uint32 mgd_tx43_en_limp			:1;                                              
		uint32 mgd_tx43_en_hb1			:1;                                              
		uint32 mgd_tx43_en_hb2			:1;                                              
		uint32 mgd_tx43_en_hb3			:1;   
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg43;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx44_pfd				:1;                                                
		uint32 mgd_tx44_res0			:1;                                                
		uint32 mgd_tx44_art				:2;                                                
		uint32 mgd_tx44_res1			:2;                                                
		uint32 mgd_tx44_sh_op3_gai		:1;                                            
		uint32 mgd_tx44_sh_op12_gai		:1;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg44;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx45_tl_art_p		:8;   
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                            
}Mgd_TLE9180_Tx_Msg45;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx46_tl_art_a		:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg46;                                                           

typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx47_fi_art_tla		:4;                                            
		uint32 mgd_tx47_fi_art_tlp		:4; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                         
}Mgd_TLE9180_Tx_Msg47;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx48_acc_art_tla		:3;                                            
		uint32 mgd_tx48_res0			:2;                                                
		uint32 mgd_tx48_acc_art_tlp		:3; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg48;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx49_art_en			:1;                                              
		uint32 mgd_tx49_res0			:6;                                                
		uint32 mgd_tx49_art_dis			:1; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                         
}Mgd_TLE9180_Tx_Msg49;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx50_nop				:8; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                           
}Mgd_TLE9180_Tx_Msg50;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx51_ls1				:1;                                                
		uint32 mgd_tx51_ls2				:1;                                                
		uint32 mgd_tx51_ls3				:1;                                                
		uint32 mgd_tx51_hs1				:1;                                                
		uint32 mgd_tx51_hs2				:1;                                                
		uint32 mgd_tx51_hs3				:1;                                                
		uint32 mgd_tx51_Drev_acc		:2; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg51;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx52_ls1				:1;                                                
		uint32 mgd_tx52_ls2				:1;                                                
		uint32 mgd_tx52_ls3				:1;                                                
		uint32 mgd_tx52_hs1				:1;                                                
		uint32 mgd_tx52_hs2				:1;                                                
		uint32 mgd_tx52_hs3				:1;                                                
		uint32 mgd_tx52_res0			:2;  
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                     
}Mgd_TLE9180_Tx_Msg52;                                                           
                                                                                 
/*=============================================================================  
   Message : Self Test 0x35H-0x37H                                               
   ============================================================================*/
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx53_en_clk_trim 	:1;                                            
		uint32 mgd_tx53_st_ls			:1;                                                
		uint32 mgd_tx53_st_hs			:1;                                                
		uint32 mgd_tx53_st_uv_bs		:1;                                              
		uint32 mgd_tx53_st_uv_cb		:1;                                              
		uint32 mgd_tx53_st_scd_ls		:1;                                              
		uint32 mgd_tx53_st_scd_hs		:1;                                              
		uint32 mgd_tx53_st_uv_vcc		:1; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                        
}Mgd_TLE9180_Tx_Msg53;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx54_en_vreg_op		:1;                                            
		uint32 mgd_tx54_en_op1_gt1		:1;                                            
		uint32 mgd_tx54_en_op1_gt2		:1;                                            
		uint32 mgd_tx54_en_op2_gt1		:1;                                            
		uint32 mgd_tx54_en_op2_gt2		:1;                                            
		uint32 mgd_tx54_en_op3_gt1		:1;                                            
		uint32 mgd_tx54_en_op3_gt2		:1;                                            
		uint32 mgd_tx54_res0			:1;   
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                    
}Mgd_TLE9180_Tx_Msg54;                                                           
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
		uint32 mgd_tx55_res0			:6;                                                
		uint32 mgd_tx55_dis_st			:1;                                              
		uint32 mgd_tx55_res1			:1; 
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                                          
}Mgd_TLE9180_Tx_Msg55;                                                           
                                                                                 
/*=============================================================================  
   Message :Read 0x40H-0x67H                                                     
   ============================================================================*/                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg64;                                                           
                                                                                                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg65;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg66;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg67;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                         
}Mgd_TLE9180_Tx_Msg68;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg69;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg70;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                         
}Mgd_TLE9180_Tx_Msg71;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg72;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg73;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                         
}Mgd_TLE9180_Tx_Msg74;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                         
}Mgd_TLE9180_Tx_Msg75;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg76;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg77;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg78;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg79;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg80;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg81;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg82;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg83;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg84;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg85;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg86;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;
}Mgd_TLE9180_Tx_Msg87;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg88;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg89;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg90;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg91;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg92;                                                           
                                                                                
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg93;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg94;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                            
}Mgd_TLE9180_Tx_Msg95;                                                           
                                                                                                                                                                  
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                         
}Mgd_TLE9180_Tx_Msg96;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg97;                                                           

typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg98;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                           
}Mgd_TLE9180_Tx_Msg99;                                                           
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                         
}Mgd_TLE9180_Tx_Msg100;                                                          
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                       
}Mgd_TLE9180_Tx_Msg101;                                                          
                                                                                 
typedef struct                                                                    
{                                                                                
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                          
}Mgd_TLE9180_Tx_Msg102;                                                          
                                                                                 
typedef struct                                                                    
{
	union
	{
		uint8 DATA;
		struct
		{
			uint32 mgd_tx103_data			:8;
		}B;
	}R;
	uint32 mgd_Reserved24					:24;                                                                                                                                         
}Mgd_TLE9180_Tx_Msg103;                                                   
                                                                                                                                                      
/* Rx Message Set */
/*=============================================================================
   Message : Configuration 0x00H-0x1FH
   ============================================================================*/
typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx0_crc		:3;
		uint32 mgd_rx0_res		:1;
		uint32 mgd_rx0_data 	:8;
		uint32 mgd_rx0_addr		:7;
		uint32 mgd_rx0_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg0;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx1_crc		:3;
		uint32 mgd_rx1_res		:1;
		uint32 mgd_rx1_data 	:8;
		uint32 mgd_rx1_addr		:7;
		uint32 mgd_rx1_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg1;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx2_crc		:3;
		uint32 mgd_rx2_res		:1;
		uint32 mgd_rx2_data 	:8;
		uint32 mgd_rx2_addr		:7;
		uint32 mgd_rx2_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg2;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx3_crc		:3;
		uint32 mgd_rx3_res		:1;
		uint32 mgd_rx3_data 	:8;
		uint32 mgd_rx3_addr		:7;
		uint32 mgd_rx3_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg3;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx4_crc		:3;
		uint32 mgd_rx4_res		:1;
		uint32 mgd_rx4_data 	:8;
		uint32 mgd_rx4_addr		:7;
		uint32 mgd_rx4_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg4;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx5_crc		:3;
		uint32 mgd_rx5_res		:1;
		uint32 mgd_rx5_data 	:8;
		uint32 mgd_rx5_addr		:7;
		uint32 mgd_rx5_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg5;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx6_crc		:3;
		uint32 mgd_rx6_res		:1;
		uint32 mgd_rx6_data 	:8;
		uint32 mgd_rx6_addr		:7;
		uint32 mgd_rx6_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg6;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx7_crc		:3;
		uint32 mgd_rx7_res		:1;
		uint32 mgd_rx7_data 	:8;
		uint32 mgd_rx7_addr		:7;
		uint32 mgd_rx7_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg7;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx8_crc		:3;
		uint32 mgd_rx8_res		:1;
		uint32 mgd_rx8_data 	:8;
		uint32 mgd_rx8_addr		:7;
		uint32 mgd_rx8_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg8;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx9_crc		:3;
		uint32 mgd_rx9_res		:1;
		uint32 mgd_rx9_data 	:8;
		uint32 mgd_rx9_addr		:7;
		uint32 mgd_rx9_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg9;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx10_crc		:3;
		uint32 mgd_rx10_res		:1;
		uint32 mgd_rx10_data 	:8;
		uint32 mgd_rx10_addr	:7;
		uint32 mgd_rx10_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg10;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx11_crc		:3;
		uint32 mgd_rx11_res		:1;
		uint32 mgd_rx11_data 	:8;
		uint32 mgd_rx11_addr	:7;
		uint32 mgd_rx11_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg11;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx12_crc		:3;
		uint32 mgd_rx12_res		:1;
		uint32 mgd_rx12_data 	:8;
		uint32 mgd_rx12_addr	:7;
		uint32 mgd_rx12_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg12;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx13_crc		:3;
		uint32 mgd_rx13_res		:1;
		uint32 mgd_rx13_data 	:8;
		uint32 mgd_rx13_addr	:7;
		uint32 mgd_rx13_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg13;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx14_crc		:3;
		uint32 mgd_rx14_res		:1;
		uint32 mgd_rx14_data 	:8;
		uint32 mgd_rx14_addr	:7;
		uint32 mgd_rx14_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg14;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx15_crc		:3;
		uint32 mgd_rx15_res		:1;
		uint32 mgd_rx15_data 	:8;
		uint32 mgd_rx15_addr	:7;
		uint32 mgd_rx15_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg15;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx16_crc		:3;
		uint32 mgd_rx16_res		:1;
		uint32 mgd_rx16_data 	:8;
		uint32 mgd_rx16_addr	:7;
		uint32 mgd_rx16_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg16;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx17_crc		:3;
		uint32 mgd_rx17_res		:1;
		uint32 mgd_rx17_data 	:8;
		uint32 mgd_rx17_addr	:7;
		uint32 mgd_rx17_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg17;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx18_crc		:3;
		uint32 mgd_rx18_res		:1;
		uint32 mgd_rx18_data 	:8;
		uint32 mgd_rx18_addr	:7;
		uint32 mgd_rx18_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg18;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx19_crc		:3;
		uint32 mgd_rx19_res		:1;
		uint32 mgd_rx19_data 	:8;
		uint32 mgd_rx19_addr	:7;
		uint32 mgd_rx19_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg19;

/*=============================================================================
   Message : Control 0x20H-0x34H
   ============================================================================*/
typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx32_crc 	:3;
		uint32 mgd_rx32_res 	:1;
		uint32 mgd_rx32_data	:8;
		uint32 mgd_rx32_addr	:7;
		uint32 mgd_rx32_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg32;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx33_crc 	:3;
		uint32 mgd_rx33_res 	:1;
		uint32 mgd_rx33_data	:8;
		uint32 mgd_rx33_addr	:7;
		uint32 mgd_rx33_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg33;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx34_crc 	:3;
		uint32 mgd_rx34_res 	:1;
		uint32 mgd_rx34_data	:8;
		uint32 mgd_rx34_addr	:7;
		uint32 mgd_rx34_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg34;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx35_crc 	:3;
		uint32 mgd_rx35_res 	:1;
		uint32 mgd_rx35_data	:8;
		uint32 mgd_rx35_addr	:7;
		uint32 mgd_rx35_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg35;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx36_crc 	:3;
		uint32 mgd_rx36_res 	:1;
		uint32 mgd_rx36_data	:8;
		uint32 mgd_rx36_addr	:7;
		uint32 mgd_rx36_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg36;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx37_crc 	:3;
		uint32 mgd_rx37_res 	:1;
		uint32 mgd_rx37_data	:8;
		uint32 mgd_rx37_addr	:7;
		uint32 mgd_rx37_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg37;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx38_crc 	:3;
		uint32 mgd_rx38_res 	:1;
		uint32 mgd_rx38_data	:8;
		uint32 mgd_rx38_addr	:7;
		uint32 mgd_rx38_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg38;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx39_crc 	:3;
		uint32 mgd_rx39_res 	:1;
		uint32 mgd_rx39_data	:8;
		uint32 mgd_rx39_addr	:7;
		uint32 mgd_rx39_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg39;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx40_crc 	:3;
		uint32 mgd_rx40_res 	:1;
		uint32 mgd_rx40_data	:8;
		uint32 mgd_rx40_addr	:7;
		uint32 mgd_rx40_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg40;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx41_crc 	:3;
		uint32 mgd_rx41_res 	:1;
		uint32 mgd_rx41_data	:8;
		uint32 mgd_rx41_addr	:7;
		uint32 mgd_rx41_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg41;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx42_crc 	:3;
		uint32 mgd_rx42_res 	:1;
		uint32 mgd_rx42_data	:8;
		uint32 mgd_rx42_addr	:7;
		uint32 mgd_rx42_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg42;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx43_crc 	:3;
		uint32 mgd_rx43_res 	:1;
		uint32 mgd_rx43_data	:8;
		uint32 mgd_rx43_addr	:7;
		uint32 mgd_rx43_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg43;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx44_crc 	:3;
		uint32 mgd_rx44_res 	:1;
		uint32 mgd_rx44_data	:8;
		uint32 mgd_rx44_addr	:7;
		uint32 mgd_rx44_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg44;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx45_crc 	:3;
		uint32 mgd_rx45_res 	:1;
		uint32 mgd_rx45_data	:8;
		uint32 mgd_rx45_addr	:7;
		uint32 mgd_rx45_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg45;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx46_crc 	:3;
		uint32 mgd_rx46_res 	:1;
		uint32 mgd_rx46_data	:8;
		uint32 mgd_rx46_addr	:7;
		uint32 mgd_rx46_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg46;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx47_crc 	:3;
		uint32 mgd_rx47_res 	:1;
		uint32 mgd_rx47_data	:8;
		uint32 mgd_rx47_addr	:7;
		uint32 mgd_rx47_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg47;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx48_crc 	:3;
		uint32 mgd_rx48_res 	:1;
		uint32 mgd_rx48_data	:8;
		uint32 mgd_rx48_addr	:7;
		uint32 mgd_rx48_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg48;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx49_crc 	:3;
		uint32 mgd_rx49_res 	:1;
		uint32 mgd_rx49_data	:8;
		uint32 mgd_rx49_addr	:7;
		uint32 mgd_rx49_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg49;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx50_crc 	:3;
		uint32 mgd_rx50_res 	:1;
		uint32 mgd_rx50_data	:8;
		uint32 mgd_rx50_addr	:7;
		uint32 mgd_rx50_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg50;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx51_crc 	:3;
		uint32 mgd_rx51_res 	:1;
		uint32 mgd_rx51_data	:8;
		uint32 mgd_rx51_addr	:7;
		uint32 mgd_rx51_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg51;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx52_crc 	:3;
		uint32 mgd_rx52_res 	:1;
		uint32 mgd_rx52_data	:8;
		uint32 mgd_rx52_addr	:7;
		uint32 mgd_rx52_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg52;

/*=============================================================================
   Message : Self Test 0x35H-0x37H
   ============================================================================*/
typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx53_crc 	:3;
		uint32 mgd_rx53_res 	:1;
		uint32 mgd_rx53_data	:8;
		uint32 mgd_rx53_addr	:7;
		uint32 mgd_rx53_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg53;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx54_crc 	:3;
		uint32 mgd_rx54_res 	:1;
		uint32 mgd_rx54_data	:8;
		uint32 mgd_rx54_addr	:7;
		uint32 mgd_rx54_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg54;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx55_crc 	:3;
		uint32 mgd_rx55_res 	:1;
		uint32 mgd_rx55_data	:8;
		uint32 mgd_rx55_addr	:7;
		uint32 mgd_rx55_status	:5;
		uint32 reserved_OF24	:8;
	}B;
}Mgd_TLE9180_Rx_Msg55;


/*=============================================================================
   Message :Read 0x40H-0x67H
   ============================================================================*/
typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx64_crc 			:3;
		uint32 mgd_rx64_res 			:1;
		uint32 mgd_rx64_idle_m			:1;
		uint32 mgd_rx64_conf_m			:1;
		uint32 mgd_rx64_conf_lock		:1;
		uint32 mgd_rx64_self_test_m		:1;
		uint32 mgd_rx64_soff_m			:1;
		uint32 mgd_rx64_err_m			:1;
		uint32 mgd_rx64_rect_m			:1;
		uint32 mgd_rx64_norm_m			:1;
		uint32 mgd_rx64_addr			:7;
		uint32 mgd_rx64_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg64;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx65_crc 			:3;
		uint32 mgd_rx65_res 			:1;
		uint32 mgd_rx65_osf				:1;
		uint32 mgd_rx65_op				:1;
		uint32 mgd_rx65_scd				:1;
		uint32 mgd_rx65_sd				:1;
		uint32 mgd_rx65_indiag			:1;
		uint32 mgd_rx65_outp			:1;
		uint32 mgd_rx65_ext				:1;
		uint32 mgd_rx65_int12			:1;
		uint32 mgd_rx65_addr			:7;
		uint32 mgd_rx65_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg65;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx66_crc 			:3;
		uint32 mgd_rx66_res 			:1;
		uint32 mgd_rx66_rom				:1;
		uint32 mgd_rx66_limp_on			:1;
		uint32 mgd_rx66_st_incomplete	:1;
		uint32 mgd_rx66_apc_act			:1;
		uint32 mgd_rx66_gtm				:1;
		uint32 mgd_rx66_ctrl_reg_invalid:1;
		uint32 mgd_rx66_lfw				:1;
		uint32 mgd_rx66_err_ot_w		:1;
		uint32 mgd_rx66_addr			:7;
		uint32 mgd_rx66_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg66;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx67_crc 			:3;
		uint32 mgd_rx67_res 			:1;
		uint32 mgd_rx67_err_ov_reg1		:1;
		uint32 mgd_rx67_err_uv_vcc_rom	:1;
		uint32 mgd_rx67_err_uv_reg4		:1;
		uint32 mgd_rx67_err_ov_reg6		:1;
		uint32 mgd_rx67_err_uv_reg6		:1;
		uint32 mgd_rx67_err_uv_reg5		:1;
		uint32 mgd_rx67_err_uv_cb		:1;
		uint32 mgd_rx67_err_clk_trim	:1;
		uint32 mgd_rx67_addr			:7;
		uint32 mgd_rx67_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg67;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx68_crc 			:3;
		uint32 mgd_rx68_res 			:1;
		uint32 mgd_rx68_err_uv_bs3		:1;
		uint32 mgd_rx68_err_uv_bs2		:1;
		uint32 mgd_rx68_err_uv_bs1		:1;
		uint32 mgd_rx68_err_cp2			:1;
		uint32 mgd_rx68_err_cp1			:1;
		uint32 mgd_rx68_err_ov_bs3		:1;
		uint32 mgd_rx68_err_ov_bs2		:1;
		uint32 mgd_rx68_err_ov_bs1		:1;
		uint32 mgd_rx68_addr			:7;
		uint32 mgd_rx68_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg68;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx69_crc 			:3;
		uint32 mgd_rx69_res 			:1;
		uint32 mgd_rx69_err_ov_vdh		:1;
		uint32 mgd_rx69_err_uv_vdh		:1;
		uint32 mgd_rx69_err_ov_vs		:1;
		uint32 mgd_rx69_err_uv_vs		:1;
		uint32 mgd_rx69_err_uv_vcc		:1;
		uint32 mgd_rx69_err_ov_vcc		:1;
		uint32 mgd_rx69_err_ov_ld_vdh	:1;
		uint32 mgd_rx69_res0			:1;
		uint32 mgd_rx69_addr			:7;
		uint32 mgd_rx69_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg69;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx70_crc 			:3;
		uint32 mgd_rx70_res 			:1;
		uint32 mgd_rx70_sd_ddp_stuck	:1;
		uint32 mgd_rx70_sd_cp1			:1;
		uint32 mgd_rx70_sd_ov_cp		:1;
		uint32 mgd_rx70_sd_clkfail		:1;
		uint32 mgd_rx70_sd_uv_cb		:1;
		uint32 mgd_rx70_sd_ov_vdh		:1;
		uint32 mgd_rx70_sd_ov_vs		:1;
		uint32 mgd_rx70_sd_ot			:1;
		uint32 mgd_rx70_addr			:7;
		uint32 mgd_rx70_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg70;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx71_crc 			:3;
		uint32 mgd_rx71_res 			:1;
		uint32 mgd_rx71_res0			:2;
		uint32 mgd_rx71_err_scd_ls3		:1;
		uint32 mgd_rx71_err_scd_ls2		:1;
		uint32 mgd_rx71_err_scd_ls1		:1;
		uint32 mgd_rx71_err_scd_hs3		:1;
		uint32 mgd_rx71_err_scd_hs2		:1;
		uint32 mgd_rx71_err_scd_hs1		:1;
		uint32 mgd_rx71_addr			:7;
		uint32 mgd_rx71_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg71;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx72_crc 			:3;
		uint32 mgd_rx72_res 			:1;
		uint32 mgd_rx72_res0			:2;
		uint32 mgd_rx72_err_ind_ls3		:1;
		uint32 mgd_rx72_err_ind_ls2		:1;
		uint32 mgd_rx72_err_ind_ls1		:1;
		uint32 mgd_rx72_err_ind_hs3		:1;
		uint32 mgd_rx72_err_ind_hs2		:1;
		uint32 mgd_rx72_err_ind_hs1		:1;
		uint32 mgd_rx72_addr			:7;
		uint32 mgd_rx72_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg72;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx73_crc 			:3;
		uint32 mgd_rx73_res 			:1;
		uint32 mgd_rx73_res0			:2;
		uint32 mgd_rx73_err_osf_ls1		:1;
		uint32 mgd_rx73_err_osf_ls2		:1;
		uint32 mgd_rx73_err_osf_ls3		:1;
		uint32 mgd_rx73_err_osf_hs1		:1;
		uint32 mgd_rx73_err_osf_hs2		:1;
		uint32 mgd_rx73_err_osf_hs3		:1;
		uint32 mgd_rx73_addr			:7;
		uint32 mgd_rx73_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg73;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx74_crc 			:3;
		uint32 mgd_rx74_res 			:1;
		uint32 mgd_rx74_err_spi_frame	:1;
		uint32 mgd_rx74_err_spi_to		:1;
		uint32 mgd_rx74_err_spi_wd		:1;
		uint32 mgd_rx74_err_spi_crc		:1;
		uint32 mgd_rx74_spi_add_invalid	:1;
		uint32 mgd_rx74_conf_to			:1;
		uint32 mgd_rx74_conf_sig_invalid:1;
		uint32 mgd_rx74_res0			:1;
		uint32 mgd_rx74_addr			:7;
		uint32 mgd_rx74_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg74;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx75_crc 			:3;
		uint32 mgd_rx75_res 			:1;
		uint32 mgd_rx75_err_oc_op1		:1;
		uint32 mgd_rx75_err_op1_uv		:1;
		uint32 mgd_rx75_err_op1_ov		:1;
		uint32 mgd_rx75_err_op1_calib	:1;
		uint32 mgd_rx75_err_oc_op2		:1;
		uint32 mgd_rx75_err_op2_uv		:1;
		uint32 mgd_rx75_err_op2_ov		:1;
		uint32 mgd_rx75_err_op2_calib	:1;
		uint32 mgd_rx75_addr			:7;
		uint32 mgd_rx75_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg75;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx76_crc 			:3;
		uint32 mgd_rx76_res 			:1;
		uint32 mgd_rx76_err_oc_op3		:1;
		uint32 mgd_rx76_err_op3_uv		:1;
		uint32 mgd_rx76_err_op3_ov		:1;
		uint32 mgd_rx76_err_op3_calib	:1;
		uint32 mgd_rx76_res0			:4;
		uint32 mgd_rx76_addr			:7;
		uint32 mgd_rx76_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg76;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx77_crc 			:3;
		uint32 mgd_rx77_res 			:1;
		uint32 mgd_rx77_err_outp_errn	:1;
		uint32 mgd_rx77_err_outp_miso	:1;
		uint32 mgd_rx77_err_outp_PFB1	:1;
		uint32 mgd_rx77_err_outp_PFB2	:1;
		uint32 mgd_rx77_err_outp_PFB3	:1;
		uint32 mgd_rx77_res0			:3;
		uint32 mgd_rx77_addr			:7;
		uint32 mgd_rx77_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg77;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx78_crc 			:3;
		uint32 mgd_rx78_res 			:1;
		uint32 mgd_rx78_dsm_ls1			:8;
		uint32 mgd_rx78_addr			:7;
		uint32 mgd_rx78_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg78;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx79_crc 			:3;
		uint32 mgd_rx79_res 			:1;
		uint32 mgd_rx79_dsm_ls2			:8;
		uint32 mgd_rx79_addr			:7;
		uint32 mgd_rx79_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg79;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx80_crc 			:3;
		uint32 mgd_rx80_res 			:1;
		uint32 mgd_rx80_dsm_ls3			:8;
		uint32 mgd_rx80_addr			:7;
		uint32 mgd_rx80_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg80;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx81_crc 			:3;
		uint32 mgd_rx81_res 			:1;
		uint32 mgd_rx81_dsm_hs1			:8;
		uint32 mgd_rx81_addr			:7;
		uint32 mgd_rx81_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg81;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx82_crc 			:3;
		uint32 mgd_rx82_res 			:1;
		uint32 mgd_rx82_dsm_hs2			:8;
		uint32 mgd_rx82_addr			:7;
		uint32 mgd_rx82_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg82;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx83_crc 			:3;
		uint32 mgd_rx83_res 			:1;
		uint32 mgd_rx83_dsm_h32			:8;
		uint32 mgd_rx83_addr			:7;
		uint32 mgd_rx83_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg83;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx84_crc 			:3;
		uint32 mgd_rx84_res 			:1;
		uint32 mgd_rx84_rdm_ls1			:8;
		uint32 mgd_rx84_addr			:7;
		uint32 mgd_rx84_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg84;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx85_crc 			:3;
		uint32 mgd_rx85_res 			:1;
		uint32 mgd_rx85_rdm_ls2			:8;
		uint32 mgd_rx85_addr			:7;
		uint32 mgd_rx85_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg85;


typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx86_crc 			:3;
		uint32 mgd_rx86_res 			:1;
		uint32 mgd_rx86_rdm_ls3			:8;
		uint32 mgd_rx86_addr			:7;
		uint32 mgd_rx86_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg86;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx87_crc 			:3;
		uint32 mgd_rx87_res 			:1;
		uint32 mgd_rx87_rdm_hs1			:8;
		uint32 mgd_rx87_addr			:7;
		uint32 mgd_rx87_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg87;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx88_crc 			:3;
		uint32 mgd_rx88_res 			:1;
		uint32 mgd_rx88_rdm_hs2			:8;
		uint32 mgd_rx88_addr			:7;
		uint32 mgd_rx88_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg88;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx89_crc 			:3;
		uint32 mgd_rx89_res 			:1;
		uint32 mgd_rx89_rdm_hs3			:8;
		uint32 mgd_rx89_addr			:7;
		uint32 mgd_rx89_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg89;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx90_crc 			:3;
		uint32 mgd_rx90_res 			:1;
		uint32 mgd_rx90_temp_ls1		:8;
		uint32 mgd_rx90_addr			:7;
		uint32 mgd_rx90_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg90;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx91_crc 			:3;
		uint32 mgd_rx91_res 			:1;
		uint32 mgd_rx91_temp_ls2		:8;
		uint32 mgd_rx91_addr			:7;
		uint32 mgd_rx91_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg91;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx92_crc 			:3;
		uint32 mgd_rx92_res 			:1;
		uint32 mgd_rx92_temp_ls3		:8;
		uint32 mgd_rx92_addr			:7;
		uint32 mgd_rx92_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg92;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx93_crc 			:3;
		uint32 mgd_rx93_res 			:1;
		uint32 mgd_rx93_temp_hs1		:8;
		uint32 mgd_rx93_addr			:7;
		uint32 mgd_rx93_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg93;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx94_crc 			:3;
		uint32 mgd_rx94_res 			:1;
		uint32 mgd_rx94_temp_hs2		:8;
		uint32 mgd_rx94_addr			:7;
		uint32 mgd_rx94_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg94;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx95_crc 			:3;
		uint32 mgd_rx95_res 			:1;
		uint32 mgd_rx95_temp_hs3		:8;
		uint32 mgd_rx95_addr			:7;
		uint32 mgd_rx95_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg95;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx96_crc 			:3;
		uint32 mgd_rx96_res 			:1;
		uint32 mgd_rx96_wwlc			:5;
		uint32 mgd_rx96_res0			:3;
		uint32 mgd_rx96_addr			:7;
		uint32 mgd_rx96_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg96;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx97_crc 			:3;
		uint32 mgd_rx97_res 			:1;
		uint32 mgd_rx97_cc_lowb			:8;
		uint32 mgd_rx97_addr			:7;
		uint32 mgd_rx97_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg97;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx98_crc 			:3;
		uint32 mgd_rx98_res 			:1;
		uint32 mgd_rx98_cc_medb			:8;
		uint32 mgd_rx98_addr			:7;
		uint32 mgd_rx98_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg98;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx99_crc 			:3;
		uint32 mgd_rx99_res 			:1;
		uint32 mgd_rx99_cc_highb		:8;
		uint32 mgd_rx99_addr			:7;
		uint32 mgd_rx99_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg99;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx100_crc 			:3;
		uint32 mgd_rx100_res 			:1;
		uint32 mgd_rx100_res_vcc		:8;
		uint32 mgd_rx100_addr			:7;
		uint32 mgd_rx100_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg100;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx101_crc 			:3;
		uint32 mgd_rx101_res 			:1;
		uint32 mgd_rx101_res_cb			:8;
		uint32 mgd_rx101_addr			:7;
		uint32 mgd_rx101_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg101;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx102_crc 			:3;
		uint32 mgd_rx102_res 			:1;
		uint32 mgd_rx102_res_vs			:8;
		uint32 mgd_rx102_addr			:7;
		uint32 mgd_rx102_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg102;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_rx103_crc 			:3;
		uint32 mgd_rx103_res 			:1;
		uint32 mgd_rx103_res_vdh		:8;
		uint32 mgd_rx103_addr			:7;
		uint32 mgd_rx103_status			:5;
		uint32 reserved_OF24			:8;
	}B;
}Mgd_TLE9180_Rx_Msg103;

/*End of Msg */

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_Tx_crc 			:3;
		uint32 mgd_Tx_res 			:5;
		uint32 mgd_Tx_data			:8;
		uint32 mgd_Tx_addr			:7;
		uint32 mgd_Tx_command 		:1;
		uint32 reserved_OF24			:8;
	}B;

}Mgd_TLE9180_TxMsg_Type;

typedef union
{
	uint32 R;
	struct
	{
		uint32 mgd_Rx_crc		:3;
		uint32 mgd_Rx_res		:1;
		uint32 mgd_Rx_data 	:8;
		uint32 mgd_Rx_addr		:7;
		uint32 mgd_Rx_status	:5;
		uint32 reserved_OF24	:8;
	}B;

}Mgd_TLE9180_RxMsg_Type;

typedef struct
{
    uint32 RESERVED     :13;
    uint32 STATUS       :2;
    uint32 RD_REQ       :1;
    uint32 WRT_REQ      :1;
    uint32 DATA         :8;
    uint32 ADD          :7;
}Mgd_TLE9180_MsgConfigType;

typedef union
{
	uint8 R;
	struct
	{
	uint8	SpecialEvent :1;
	uint8	SpiError	 :1;
	uint8	ConfigValid	 :1;
	uint8	Warnning	 :1;
	uint8	Error 		 :1;
	uint8	Reserved 	 :3;
	}B;
}Mgd_SpiStatus;

typedef struct
{
	union
	{
		uint32 MGD9180_RxArray[MGD9180_ID_MAX];
		struct
		{
			Mgd_TLE9180_Rx_Msg1 	 mgd9180_Rx_Conf_Gen_1;
			Mgd_TLE9180_Rx_Msg2 	 mgd9180_Rx_Conf_Gen_2;
			Mgd_TLE9180_Rx_Msg3 	 mgd9180_Rx_Conf_Gen_3;
			Mgd_TLE9180_Rx_Msg4 	 mgd9180_Rx_Conf_wwd;
			Mgd_TLE9180_Rx_Msg5 	 mgd9180_Rx_Tl_vs;
			Mgd_TLE9180_Rx_Msg6 	 mgd9180_Rx_Tl_vdh;
			Mgd_TLE9180_Rx_Msg7 	 mgd9180_Rx_Tl_cbvcc;
			Mgd_TLE9180_Rx_Msg8 	 mgd9180_Rx_Fm_1;
			Mgd_TLE9180_Rx_Msg9 	 mgd9180_Rx_Fm_2;
			Mgd_TLE9180_Rx_Msg10	 mgd9180_Rx_Fm_3;
			Mgd_TLE9180_Rx_Msg11	 mgd9180_Rx_Fm_4;
			Mgd_TLE9180_Rx_Msg12	 mgd9180_Rx_Fm_5;
			Mgd_TLE9180_Rx_Msg13	 mgd9180_Rx_Dt_hs;
			Mgd_TLE9180_Rx_Msg14	 mgd9180_Rx_Dt_ls;
			Mgd_TLE9180_Rx_Msg15	 mgd9180_Rx_Ft_1;
			Mgd_TLE9180_Rx_Msg16	 mgd9180_Rx_Ft_2;
			Mgd_TLE9180_Rx_Msg17	 mgd9180_Rx_Ft_3;
			Mgd_TLE9180_Rx_Msg18	 mgd9180_Rx_Ft_4;
			Mgd_TLE9180_Rx_Msg19	 mgd9180_Rx_Fm_6;
			Mgd_TLE9180_Rx_Msg0 	 mgd9180_Rx_Conf_Sig;			
			Mgd_TLE9180_Rx_Msg32	 mgd9180_Rx_Op_gai_1;
			Mgd_TLE9180_Rx_Msg33	 mgd9180_Rx_Op_gai_2;
			Mgd_TLE9180_Rx_Msg34	 mgd9180_Rx_Op_gai_3;
			Mgd_TLE9180_Rx_Msg35	 mgd9180_Rx_Op_0cl;
			Mgd_TLE9180_Rx_Msg36	 mgd9180_Rx_Op_con;
			Mgd_TLE9180_Rx_Msg37	 mgd9180_Rx_Sc_ls_1;
			Mgd_TLE9180_Rx_Msg38	 mgd9180_Rx_Sc_ls_2;
			Mgd_TLE9180_Rx_Msg39	 mgd9180_Rx_Sc_ls_3;
			Mgd_TLE9180_Rx_Msg40	 mgd9180_Rx_Sc_hs_1;
			Mgd_TLE9180_Rx_Msg41	 mgd9180_Rx_Sc_hs_2;
			Mgd_TLE9180_Rx_Msg42	 mgd9180_Rx_Sc_hs_3;
			Mgd_TLE9180_Rx_Msg43	 mgd9180_Rx_Li_ctr;
			Mgd_TLE9180_Rx_Msg44	 mgd9180_Rx_Misc_ctr;
			Mgd_TLE9180_Rx_Msg45	 mgd9180_Rx_art_tlp;
			Mgd_TLE9180_Rx_Msg46	 mgd9180_Rx_art_tla;
			Mgd_TLE9180_Rx_Msg47	 mgd9180_Rx_art_fi;
			Mgd_TLE9180_Rx_Msg48	 mgd9180_Rx_art_acc;
			Mgd_TLE9180_Rx_Msg49	 mgd9180_Rx_art_entry;
			Mgd_TLE9180_Rx_Msg50	 mgd9180_Rx_nop;
			Mgd_TLE9180_Rx_Msg51	 mgd9180_Rx_Drev_mark;
			Mgd_TLE9180_Rx_Msg52	 mgd9180_Rx_Ds_mark;
			Mgd_TLE9180_Rx_Msg53	 mgd9180_Rx_Sel_st_1;
			Mgd_TLE9180_Rx_Msg54	 mgd9180_Rx_Sel_st_2;
			Mgd_TLE9180_Rx_Msg55	 mgd9180_Rx_En_st;
			Mgd_TLE9180_Rx_Msg64	 mgd9180_Rx_Om_over;
			Mgd_TLE9180_Rx_Msg65	 mgd9180_Rx_Err_overr;
			Mgd_TLE9180_Rx_Msg66	 mgd9180_Rx_Ser;
			Mgd_TLE9180_Rx_Msg67	 mgd9180_Rx_Err_i_1;
			Mgd_TLE9180_Rx_Msg68	 mgd9180_Rx_Err_i_2;
			Mgd_TLE9180_Rx_Msg69	 mgd9180_Rx_Err_e;
			Mgd_TLE9180_Rx_Msg70	 mgd9180_Rx_Err_sd;
			Mgd_TLE9180_Rx_Msg71	 mgd9180_Rx_Err_scd;
			Mgd_TLE9180_Rx_Msg72	 mgd9180_Rx_Err_indiag;
			Mgd_TLE9180_Rx_Msg73	 mgd9180_Rx_Err_osf;
			Mgd_TLE9180_Rx_Msg74	 mgd9180_Rx_Err_spiconf;
			Mgd_TLE9180_Rx_Msg75	 mgd9180_Rx_Err_op_12;
			Mgd_TLE9180_Rx_Msg76	 mgd9180_Rx_Err_op_3;
			Mgd_TLE9180_Rx_Msg77	 mgd9180_Rx_Err_outp;
			Mgd_TLE9180_Rx_Msg78	 mgd9180_Rx_dsm_ls1;
			Mgd_TLE9180_Rx_Msg79	 mgd9180_Rx_dsm_ls2;
			Mgd_TLE9180_Rx_Msg80	 mgd9180_Rx_dsm_ls3;
			Mgd_TLE9180_Rx_Msg81	 mgd9180_Rx_dsm_hs1;
			Mgd_TLE9180_Rx_Msg82	 mgd9180_Rx_dsm_hs2;
			Mgd_TLE9180_Rx_Msg83	 mgd9180_Rx_dsm_hs3;
			Mgd_TLE9180_Rx_Msg84	 mgd9180_Rx_Rdm_ls1;
			Mgd_TLE9180_Rx_Msg85	 mgd9180_Rx_Rdm_ls2;
			Mgd_TLE9180_Rx_Msg86	 mgd9180_Rx_Rdm_ls3;
			Mgd_TLE9180_Rx_Msg87	 mgd9180_Rx_Rdm_hs1;
			Mgd_TLE9180_Rx_Msg88	 mgd9180_Rx_Rdm_hs2;
			Mgd_TLE9180_Rx_Msg89	 mgd9180_Rx_Rdm_hs3;
			Mgd_TLE9180_Rx_Msg90	 mgd9180_Rx_temp_ls1;
			Mgd_TLE9180_Rx_Msg91	 mgd9180_Rx_temp_ls2;
			Mgd_TLE9180_Rx_Msg92	 mgd9180_Rx_temp_ls3;
			Mgd_TLE9180_Rx_Msg93	 mgd9180_Rx_temp_hs1;
			Mgd_TLE9180_Rx_Msg94	 mgd9180_Rx_temp_hs2;
			Mgd_TLE9180_Rx_Msg95	 mgd9180_Rx_temp_hs3;
			Mgd_TLE9180_Rx_Msg96	 mgd9180_Rx_wwic;
			Mgd_TLE9180_Rx_Msg97	 mgd9180_Rx_res_cc1;
			Mgd_TLE9180_Rx_Msg98	 mgd9180_Rx_res_cc2;
			Mgd_TLE9180_Rx_Msg99	 mgd9180_Rx_res_cc3;
			Mgd_TLE9180_Rx_Msg100	 mgd9180_Rx_res_vcc;
			Mgd_TLE9180_Rx_Msg101	 mgd9180_Rx_res_cb;
			Mgd_TLE9180_Rx_Msg102	 mgd9180_Rx_res_vs;
			Mgd_TLE9180_Rx_Msg103	 mgd9180_Rx_res_vdh;
		}M;
	}A;
}MGD9180_RX_MSG_t;

typedef struct
{
	Mgd_TLE9180_Tx_Msg0 	 mgd9180_Tx_Conf_Sig;
	Mgd_TLE9180_Tx_Msg1 	 mgd9180_Tx_Conf_Gen_1;
	Mgd_TLE9180_Tx_Msg2 	 mgd9180_Tx_Conf_Gen_2;
	Mgd_TLE9180_Tx_Msg3 	 mgd9180_Tx_Conf_Gen_3;
	Mgd_TLE9180_Tx_Msg4 	 mgd9180_Tx_Conf_wwd;
	Mgd_TLE9180_Tx_Msg5 	 mgd9180_Tx_Tl_vs;
	Mgd_TLE9180_Tx_Msg6 	 mgd9180_Tx_Tl_vdh;
	Mgd_TLE9180_Tx_Msg7 	 mgd9180_Tx_Tl_cbvcc;
	Mgd_TLE9180_Tx_Msg8 	 mgd9180_Tx_Fm_1;
	Mgd_TLE9180_Tx_Msg9 	 mgd9180_Tx_Fm_2;
	Mgd_TLE9180_Tx_Msg10	 mgd9180_Tx_Fm_3;
	Mgd_TLE9180_Tx_Msg11	 mgd9180_Tx_Fm_4;
	Mgd_TLE9180_Tx_Msg12	 mgd9180_Tx_Fm_5;
	Mgd_TLE9180_Tx_Msg13	 mgd9180_Tx_Dt_hs;
	Mgd_TLE9180_Tx_Msg14	 mgd9180_Tx_Dt_ls;
	Mgd_TLE9180_Tx_Msg15	 mgd9180_Tx_Ft_1;
	Mgd_TLE9180_Tx_Msg16	 mgd9180_Tx_Ft_2;
	Mgd_TLE9180_Tx_Msg17	 mgd9180_Tx_Ft_3;
	Mgd_TLE9180_Tx_Msg18	 mgd9180_Tx_Ft_4;
	Mgd_TLE9180_Tx_Msg19	 mgd9180_Tx_Fm_6;
	Mgd_TLE9180_Tx_Msg32	 mgd9180_Tx_Op_gai_1;
	Mgd_TLE9180_Tx_Msg33	 mgd9180_Tx_Op_gai_2;
	Mgd_TLE9180_Tx_Msg34	 mgd9180_Tx_Op_gai_3;
	Mgd_TLE9180_Tx_Msg35	 mgd9180_Tx_Op_0cl;
	Mgd_TLE9180_Tx_Msg36	 mgd9180_Tx_Op_con;
	Mgd_TLE9180_Tx_Msg37	 mgd9180_Tx_Sc_ls_1;
	Mgd_TLE9180_Tx_Msg38	 mgd9180_Tx_Sc_ls_2;
	Mgd_TLE9180_Tx_Msg39	 mgd9180_Tx_Sc_ls_3;
	Mgd_TLE9180_Tx_Msg40	 mgd9180_Tx_Sc_hs_1;
	Mgd_TLE9180_Tx_Msg41	 mgd9180_Tx_Sc_hs_2;
	Mgd_TLE9180_Tx_Msg42	 mgd9180_Tx_Sc_hs_3;
	Mgd_TLE9180_Tx_Msg43	 mgd9180_Tx_Li_ctr;
	Mgd_TLE9180_Tx_Msg44	 mgd9180_Tx_Misc_ctr;
	Mgd_TLE9180_Tx_Msg45	 mgd9180_Tx_art_tlp;
	Mgd_TLE9180_Tx_Msg46	 mgd9180_Tx_art_tla;
	Mgd_TLE9180_Tx_Msg47	 mgd9180_Tx_art_fi;
	Mgd_TLE9180_Tx_Msg48	 mgd9180_Tx_art_acc;
	Mgd_TLE9180_Tx_Msg49	 mgd9180_Tx_art_entry;
	Mgd_TLE9180_Tx_Msg50	 mgd9180_Tx_nop;
	Mgd_TLE9180_Tx_Msg51	 mgd9180_Tx_Drev_mark;
	Mgd_TLE9180_Tx_Msg52	 mgd9180_Tx_Ds_mark;
	Mgd_TLE9180_Tx_Msg53	 mgd9180_Tx_Sel_st_1;
	Mgd_TLE9180_Tx_Msg54	 mgd9180_Tx_Sel_st_2;
	Mgd_TLE9180_Tx_Msg55	 mgd9180_Tx_En_st;
	Mgd_TLE9180_Tx_Msg64	 mgd9180_Tx_Om_over;
	Mgd_TLE9180_Tx_Msg65	 mgd9180_Tx_Err_overr;
	Mgd_TLE9180_Tx_Msg66	 mgd9180_Tx_Ser;
	Mgd_TLE9180_Tx_Msg67	 mgd9180_Tx_Err_i_1;
	Mgd_TLE9180_Tx_Msg68	 mgd9180_Tx_Err_i_2;
	Mgd_TLE9180_Tx_Msg69	 mgd9180_Tx_Err_e;
	Mgd_TLE9180_Tx_Msg70	 mgd9180_Tx_Err_sd;
	Mgd_TLE9180_Tx_Msg71	 mgd9180_Tx_Err_scd;
	Mgd_TLE9180_Tx_Msg72	 mgd9180_Tx_Err_indiag;
	Mgd_TLE9180_Tx_Msg73	 mgd9180_Tx_Err_osf;
	Mgd_TLE9180_Tx_Msg74	 mgd9180_Tx_Err_spiconf;
	Mgd_TLE9180_Tx_Msg75	 mgd9180_Tx_Err_op_12;
	Mgd_TLE9180_Tx_Msg76	 mgd9180_Tx_Err_op_3;
	Mgd_TLE9180_Tx_Msg77	 mgd9180_Tx_Err_outp;
	Mgd_TLE9180_Tx_Msg78	 mgd9180_Tx_dsm_ls1;
	Mgd_TLE9180_Tx_Msg79	 mgd9180_Tx_dsm_ls2;
	Mgd_TLE9180_Tx_Msg80	 mgd9180_Tx_dsm_ls3;
	Mgd_TLE9180_Tx_Msg81	 mgd9180_Tx_dsm_hs1;
	Mgd_TLE9180_Tx_Msg82	 mgd9180_Tx_dsm_hs2;
	Mgd_TLE9180_Tx_Msg83	 mgd9180_Tx_dsm_hs3;
	Mgd_TLE9180_Tx_Msg84	 mgd9180_Tx_Rdm_ls1;
	Mgd_TLE9180_Tx_Msg85	 mgd9180_Tx_Rdm_ls2;
	Mgd_TLE9180_Tx_Msg86	 mgd9180_Tx_Rdm_ls3;
	Mgd_TLE9180_Tx_Msg87	 mgd9180_Tx_Rdm_hs1;
	Mgd_TLE9180_Tx_Msg88	 mgd9180_Tx_Rdm_hs2;
	Mgd_TLE9180_Tx_Msg89	 mgd9180_Tx_Rdm_hs3;
	Mgd_TLE9180_Tx_Msg90	 mgd9180_Tx_temp_ls1;
	Mgd_TLE9180_Tx_Msg91	 mgd9180_Tx_temp_ls2;
	Mgd_TLE9180_Tx_Msg92	 mgd9180_Tx_temp_ls3;
	Mgd_TLE9180_Tx_Msg93	 mgd9180_Tx_temp_hs1;
	Mgd_TLE9180_Tx_Msg94	 mgd9180_Tx_temp_hs2;
	Mgd_TLE9180_Tx_Msg95	 mgd9180_Tx_temp_hs3;
	Mgd_TLE9180_Tx_Msg96	 mgd9180_Tx_wwic;
	Mgd_TLE9180_Tx_Msg97	 mgd9180_Tx_res_cc1;
	Mgd_TLE9180_Tx_Msg98	 mgd9180_Tx_res_cc2;
	Mgd_TLE9180_Tx_Msg99	 mgd9180_Tx_res_cc3;
	Mgd_TLE9180_Tx_Msg100	 mgd9180_Tx_res_vcc;
	Mgd_TLE9180_Tx_Msg101	 mgd9180_Tx_res_cb;
	Mgd_TLE9180_Tx_Msg102	 mgd9180_Tx_res_vs;
	Mgd_TLE9180_Tx_Msg103	 mgd9180_Tx_res_vdh;
}MGD9180_TX_MSG_t;

typedef struct
{
	uint8 mgdOutSig_rx64_idle_m;
	uint8 mgdOutSig_rx64_conf_m;
	uint8 mgdOutSig_rx64_conf_lock;
	uint8 mgdOutSig_rx64_self_test_m;
	uint8 mgdOutSig_rx64_soff_m;
	uint8 mgdOutSig_rx64_err_m;
	uint8 mgdOutSig_rx64_rect_m;
	uint8 mgdOutSig_rx64_norm_m;
											
	uint8 mgdOutSig_rx65_osf;
	uint8 mgdOutSig_rx65_op;
	uint8 mgdOutSig_rx65_scd;
	uint8 mgdOutSig_rx65_sd;
	uint8 mgdOutSig_rx65_indiag;
	uint8 mgdOutSig_rx65_outp;
	uint8 mgdOutSig_rx65_ext;
	uint8 mgdOutSig_rx65_int12;
											
	uint8 mgdOutSig_rx66_rom;
	uint8 mgdOutSig_rx66_limp_on;
	uint8 mgdOutSig_rx66_st_incomplete;
	uint8 mgdOutSig_rx66_apc_act;
	uint8 mgdOutSig_rx66_gtm;
	uint8 mgdOutSig_rx66_ctrl_reg_invalid;
	uint8 mgdOutSig_rx66_lfw;
	uint8 mgdOutSig_rx66_err_ot_w;
											
	uint8 mgdOutSig_rx67_err_ov_reg1;
	uint8 mgdOutSig_rx67_err_uv_vcc_rom;
	uint8 mgdOutSig_rx67_err_uv_reg4;
	uint8 mgdOutSig_rx67_err_ov_reg6;
	uint8 mgdOutSig_rx67_err_uv_reg6;
	uint8 mgdOutSig_rx67_err_uv_reg5;
	uint8 mgdOutSig_rx67_err_uv_cb;
	uint8 mgdOutSig_rx67_err_clk_trim;
											
	uint8 mgdOutSig_rx68_err_uv_bs3;
	uint8 mgdOutSig_rx68_err_uv_bs2;
	uint8 mgdOutSig_rx68_err_uv_bs1;
	uint8 mgdOutSig_rx68_err_cp2;
	uint8 mgdOutSig_rx68_err_cp1;
	uint8 mgdOutSig_rx68_err_ov_bs3;
	uint8 mgdOutSig_rx68_err_ov_bs2;
	uint8 mgdOutSig_rx68_err_ov_bs1;
											
	uint8 mgdOutSig_rx69_err_ov_vdh;
	uint8 mgdOutSig_rx69_err_uv_vdh;
	uint8 mgdOutSig_rx69_err_ov_vs;
	uint8 mgdOutSig_rx69_err_uv_vs;
	uint8 mgdOutSig_rx69_err_uv_vcc;
	uint8 mgdOutSig_rx69_err_ov_vcc;
	uint8 mgdOutSig_rx69_err_ov_ld_vdh;
											
	uint8 mgdOutSig_rx70_sd_ddp_stuck;
	uint8 mgdOutSig_rx70_sd_cp1;
	uint8 mgdOutSig_rx70_sd_ov_cp;
	uint8 mgdOutSig_rx70_sd_clkfail;
	uint8 mgdOutSig_rx70_sd_uv_cb;
	uint8 mgdOutSig_rx70_sd_ov_vdh;
	uint8 mgdOutSig_rx70_sd_ov_vs;
	uint8 mgdOutSig_rx70_sd_ot;
											
	uint8 mgdOutSig_rx71_err_scd_ls3;
	uint8 mgdOutSig_rx71_err_scd_ls2;
	uint8 mgdOutSig_rx71_err_scd_ls1;
	uint8 mgdOutSig_rx71_err_scd_hs3;
	uint8 mgdOutSig_rx71_err_scd_hs2;
	uint8 mgdOutSig_rx71_err_scd_hs1;
											
	uint8 mgdOutSig_rx72_err_ind_ls3;
	uint8 mgdOutSig_rx72_err_ind_ls2;
	uint8 mgdOutSig_rx72_err_ind_ls1;
	uint8 mgdOutSig_rx72_err_ind_hs3;
	uint8 mgdOutSig_rx72_err_ind_hs2;
	uint8 mgdOutSig_rx72_err_ind_hs1;
											
	uint8 mgdOutSig_rx73_err_osf_ls1;
	uint8 mgdOutSig_rx73_err_osf_ls2;
	uint8 mgdOutSig_rx73_err_osf_ls3;
	uint8 mgdOutSig_rx73_err_osf_hs1;
	uint8 mgdOutSig_rx73_err_osf_hs2;
	uint8 mgdOutSig_rx73_err_osf_hs3;
											
	uint8 mgdOutSig_rx74_err_spi_frame;
	uint8 mgdOutSig_rx74_err_spi_to;
	uint8 mgdOutSig_rx74_err_spi_wd;
	uint8 mgdOutSig_rx74_err_spi_crc;
	uint8 mgdOutSig_rx74_spi_add_invalid;
	uint8 mgdOutSig_rx74_conf_to;
	uint8 mgdOutSig_rx74_conf_sig_invalid;
											
	uint8 mgdOutSig_rx75_err_oc_op1;
	uint8 mgdOutSig_rx75_err_op1_uv;
	uint8 mgdOutSig_rx75_err_op1_ov;
	uint8 mgdOutSig_rx75_err_op1_calib;
	uint8 mgdOutSig_rx75_err_oc_op2;
	uint8 mgdOutSig_rx75_err_op2_uv;
	uint8 mgdOutSig_rx75_err_op2_ov;
	uint8 mgdOutSig_rx75_err_op2_calib;
											
	uint8 mgdOutSig_rx76_err_oc_op3;
	uint8 mgdOutSig_rx76_err_op3_uv;
	uint8 mgdOutSig_rx76_err_op3_ov;
	uint8 mgdOutSig_rx76_err_op3_calib;
											
	uint8 mgdOutSig_rx77_err_outp_errn;
	uint8 mgdOutSig_rx77_err_outp_miso;
	uint8 mgdOutSig_rx77_err_outp_PFB1;
	uint8 mgdOutSig_rx77_err_outp_PFB2;
	uint8 mgdOutSig_rx77_err_outp_PFB3;

	uint8 mgdOutSig_Tle1980_ErrPin_Status;

}MGD_TLE9180_ERR_MONITOR_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern MGD_TLE9180_ERR_MONITOR_t MGD9180_ERR_MON;
extern uint8 mgd9180_configure_complete;
extern uint8 u8Mgd9180_NormalSpiStatus;
extern uint32 u32Mgd9180_Normal_Spi_Check;
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mgd_TLE9180_Process(Mgd_TLE9180_Hndlr_HdrBusType *Mgd_TLE9180_HndlProcess);
extern void Mgd_TLE9180_Process_Init(void);
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MGD_TLE9180_DEVICE_H_ */
/** @} */
