#define UT_MAX_STEP 5

#define MGD_TLE9180_HNDLRECUMODESTS  {0,0,0,0,0}

#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDIDLEM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDCONFM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDCONFLOCK   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSELFTESTM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSOFFM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDRECTM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDNORMM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDOSF   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDOP   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSCD   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSD   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDINDIAG   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDOUTP   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDEXT   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDINT12   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDROM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDLIMPON   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSTINCOMPLETE   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDAPCACT   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDGTM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDCTRLREGINVALID   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDLFW   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROTW   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVREG1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVCCROM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVREG4   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVREG6   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVREG6   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVREG5   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVCB   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRCLKTRIM   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVBS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVBS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVBS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRCP2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRCP1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVBS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVBS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVBS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVVDH   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVDH   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVVS   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVS   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVCC   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVVCC   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVLDVDH   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDDDPSTUCK   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDCP1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOVCP   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDCLKFAIL   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDUVCB   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOVVDH   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOVVS   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOT   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDLS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDLS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDLS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDHS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDHS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDHS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDLS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDLS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDLS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDHS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDHS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDHS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFLS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFLS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFLS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFHS1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFHS2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFHS3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPIFRAME   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPITO   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPIWD   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPICRC   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDEPIADDINVALID   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDEONFTO   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDEONFSIGINVALID   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROCOP1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP1UV   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP1OV   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP1CALIB   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROCOP2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP2UV   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP2OV   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP2CALIB   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROCOP3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP3UV   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP3OV   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP3CALIB   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPERRN   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPMISO   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPPFB1   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPPFB2   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPPFB3   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDDCDINFO_MGDTLE9180ERRPORT   {0,0,0,0,0}
#define MGD_TLE9180_HNDLRMGDINVALID     {0,0,0,0,0}
