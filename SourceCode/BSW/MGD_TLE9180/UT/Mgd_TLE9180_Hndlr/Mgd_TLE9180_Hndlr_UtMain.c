#include "unity.h"
#include "unity_fixture.h"
#include "Mgd_TLE9180_Hndlr.h"
#include "Mgd_TLE9180_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Mgd_TLE9180_HndlrEcuModeSts[MAX_STEP] = MGD_TLE9180_HNDLRECUMODESTS;

const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdIdleM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDIDLEM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdConfM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDCONFM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdConfLock[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDCONFLOCK;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSelfTestM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSELFTESTM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSoffM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSOFFM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdRectM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDRECTM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdNormM[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDNORMM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOsf[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDOSF;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOp[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDOP;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdScd[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSCD;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSd[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSD;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdIndiag[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDINDIAG;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOutp[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDOUTP;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdExt[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDEXT;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdInt12[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDINT12;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdRom[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDROM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdLimpOn[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDLIMPON;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdStIncomplete[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSTINCOMPLETE;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdApcAct[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDAPCACT;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdGtm[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDGTM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdCtrlRegInvalid[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDCTRLREGINVALID;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdLfw[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDLFW;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOtW[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROTW;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvReg1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVREG1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVccRom[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVCCROM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg4[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVREG4;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvReg6[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVREG6;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg6[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVREG6;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg5[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVREG5;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvCb[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVCB;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrClkTrim[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRCLKTRIM;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVBS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVBS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVBS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrCp2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRCP2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrCp1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRCP1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVBS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVBS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVBS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVdh[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVVDH;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVdh[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVDH;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVs[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVVS;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVs[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVS;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVcc[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRUVVCC;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVcc[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVVCC;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvLdVdh[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROVLDVDH;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdDdpStuck[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDDDPSTUCK;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdCp1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDCP1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvCp[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOVCP;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdClkfail[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDCLKFAIL;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdUvCb[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDUVCB;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvVdh[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOVVDH;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvVs[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOVVS;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOt[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDSDOT;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDLS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDLS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDLS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDHS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDHS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSCDHS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDLS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDLS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDLS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDHS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDHS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRINDHS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFLS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFLS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFLS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFHS1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFHS2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROSFHS3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiFrame[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPIFRAME;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiTo[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPITO;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiWd[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPIWD;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiCrc[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERRSPICRC;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEpiAddInvalid[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDEPIADDINVALID;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEonfTo[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDEONFTO;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEonfSigInvalid[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDEONFSIGINVALID;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROCOP1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Uv[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP1UV;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Ov[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP1OV;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Calib[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP1CALIB;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROCOP2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Uv[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP2UV;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Ov[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP2OV;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Calib[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP2CALIB;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROCOP3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Uv[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP3UV;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Ov[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP3OV;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Calib[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROP3CALIB;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpErrn[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPERRN;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpMiso[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPMISO;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB1[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPPFB1;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB2[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPPFB2;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB3[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDERROUTPPFB3;
const Haluint8 UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdTle9180ErrPort[MAX_STEP] = MGD_TLE9180_HNDLRMGDDCDINFO_MGDTLE9180ERRPORT;
const Mgd_TLE9180_HndlrMgdInvalid_t UtExpected_Mgd_TLE9180_HndlrMgdInvalid[MAX_STEP] = MGD_TLE9180_HNDLRMGDINVALID;



TEST_GROUP(Mgd_TLE9180_Hndlr);
TEST_SETUP(Mgd_TLE9180_Hndlr)
{
    Mgd_TLE9180_Hndlr_Init();
}

TEST_TEAR_DOWN(Mgd_TLE9180_Hndlr)
{   /* Postcondition */

}

TEST(Mgd_TLE9180_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mgd_TLE9180_HndlrEcuModeSts = UtInput_Mgd_TLE9180_HndlrEcuModeSts[i];

        Mgd_TLE9180_Hndlr();

        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdIdleM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdIdleM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdConfM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdConfLock, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdConfLock[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSelfTestM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSelfTestM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSoffM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSoffM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdRectM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdRectM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdNormM, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdNormM[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdOsf, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOsf[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdOp, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOp[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdScd, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdScd[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSd, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSd[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdIndiag, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdIndiag[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdOutp, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdOutp[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdExt, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdExt[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdInt12, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdInt12[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdRom, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdRom[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdLimpOn, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdLimpOn[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdStIncomplete, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdStIncomplete[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdApcAct, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdApcAct[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdGtm, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdGtm[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdCtrlRegInvalid, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdCtrlRegInvalid[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdLfw, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdLfw[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOtW, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOtW[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvReg1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVccRom, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVccRom[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg4, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg4[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvReg6, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvReg6[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg6, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg6[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvReg5, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvReg5[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvCb, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvCb[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrClkTrim, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrClkTrim[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvBs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvBs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrCp2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrCp1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrCp1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvBs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvBs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVdh, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVdh[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVdh, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVdh[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVs, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVs[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVs, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVs[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrUvVcc, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrUvVcc[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvVcc, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvVcc[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOvLdVdh, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOvLdVdh[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdDdpStuck, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdDdpStuck[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdCp1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdCp1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvCp, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvCp[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdClkfail, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdClkfail[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdUvCb, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdUvCb[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVdh, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvVdh[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOvVs, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOvVs[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdSdOt, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdSdOt[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdLs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdLs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrScdHs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrScdHs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndLs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndLs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrIndHs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrIndHs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfLs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfLs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOsfHs3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOsfHs3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiFrame, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiFrame[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiTo, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiTo[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiWd, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiWd[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrSpiCrc, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrSpiCrc[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdEpiAddInvalid, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEpiAddInvalid[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfTo, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEonfTo[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdEonfSigInvalid, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdEonfSigInvalid[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Uv, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Uv[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Ov, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Ov[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp1Calib, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp1Calib[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Uv, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Uv[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Ov, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Ov[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp2Calib, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp2Calib[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOcOp3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOcOp3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Uv, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Uv[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Ov, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Ov[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOp3Calib, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOp3Calib[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpErrn, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpErrn[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpMiso, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpMiso[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB1, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB1[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB2, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB2[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdErrOutpPFB3, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdErrOutpPFB3[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdDcdInfo.mgdTle9180ErrPort, UtExpected_Mgd_TLE9180_HndlrMgdDcdInfo_mgdTle9180ErrPort[i]);
        TEST_ASSERT_EQUAL(Mgd_TLE9180_HndlrMgdInvalid, UtExpected_Mgd_TLE9180_HndlrMgdInvalid[i]);
    }
}

TEST_GROUP_RUNNER(Mgd_TLE9180_Hndlr)
{
    RUN_TEST_CASE(Mgd_TLE9180_Hndlr, All);
}
