# \file
#
# \brief Mgd_TLE9180
#
# This file contains the implementation of the SWC
# module Mgd_TLE9180.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Mgd_TLE9180_src

Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_SRC_PATH)\Mgd_TLE9180_Hndlr.c
Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_IFA_PATH)\Mgd_TLE9180_Hndlr_Ifa.c
Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_CFG_PATH)\Mgd_TLE9180_Cfg.c
Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_CAL_PATH)\Mgd_TLE9180_Cal.c
Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_SRC_PATH)\Mgd_TLE9180_Process.c

ifeq ($(ICE_COMPILE),true)
Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_UNITY_PATH)\unity.c
	Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_UNITY_PATH)\unity_fixture.c	
	Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_UT_PATH)\main.c
	Mgd_TLE9180_src_FILES        += $(Mgd_TLE9180_UT_PATH)\Mgd_TLE9180_Hndlr\Mgd_TLE9180_Hndlr_UtMain.c
endif