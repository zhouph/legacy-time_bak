#define S_FUNCTION_NAME      MtrM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          129
#define WidthOutputPort         21

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "MtrM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    MtrM_MainMotMonInfo.MotPwrVoltMon = input[0];
    MtrM_MainMotVoltsMonInfo.MotVoltPhUMon = input[1];
    MtrM_MainMotVoltsMonInfo.MotVoltPhVMon = input[2];
    MtrM_MainMotVoltsMonInfo.MotVoltPhWMon = input[3];
    MtrM_MainMpsD1SpiDcdInfo.S_MAGOL = input[4];
    MtrM_MainMpsD2SpiDcdInfo.S_MAGOL = input[5];
    MtrM_MainMgdDcdInfo.mgdIdleM = input[6];
    MtrM_MainMgdDcdInfo.mgdConfM = input[7];
    MtrM_MainMgdDcdInfo.mgdConfLock = input[8];
    MtrM_MainMgdDcdInfo.mgdSelfTestM = input[9];
    MtrM_MainMgdDcdInfo.mgdSoffM = input[10];
    MtrM_MainMgdDcdInfo.mgdErrM = input[11];
    MtrM_MainMgdDcdInfo.mgdRectM = input[12];
    MtrM_MainMgdDcdInfo.mgdNormM = input[13];
    MtrM_MainMgdDcdInfo.mgdOsf = input[14];
    MtrM_MainMgdDcdInfo.mgdOp = input[15];
    MtrM_MainMgdDcdInfo.mgdScd = input[16];
    MtrM_MainMgdDcdInfo.mgdSd = input[17];
    MtrM_MainMgdDcdInfo.mgdIndiag = input[18];
    MtrM_MainMgdDcdInfo.mgdOutp = input[19];
    MtrM_MainMgdDcdInfo.mgdExt = input[20];
    MtrM_MainMgdDcdInfo.mgdInt12 = input[21];
    MtrM_MainMgdDcdInfo.mgdRom = input[22];
    MtrM_MainMgdDcdInfo.mgdLimpOn = input[23];
    MtrM_MainMgdDcdInfo.mgdStIncomplete = input[24];
    MtrM_MainMgdDcdInfo.mgdApcAct = input[25];
    MtrM_MainMgdDcdInfo.mgdGtm = input[26];
    MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid = input[27];
    MtrM_MainMgdDcdInfo.mgdLfw = input[28];
    MtrM_MainMgdDcdInfo.mgdErrOtW = input[29];
    MtrM_MainMgdDcdInfo.mgdErrOvReg1 = input[30];
    MtrM_MainMgdDcdInfo.mgdErrUvVccRom = input[31];
    MtrM_MainMgdDcdInfo.mgdErrUvReg4 = input[32];
    MtrM_MainMgdDcdInfo.mgdErrOvReg6 = input[33];
    MtrM_MainMgdDcdInfo.mgdErrUvReg6 = input[34];
    MtrM_MainMgdDcdInfo.mgdErrUvReg5 = input[35];
    MtrM_MainMgdDcdInfo.mgdErrUvCb = input[36];
    MtrM_MainMgdDcdInfo.mgdErrClkTrim = input[37];
    MtrM_MainMgdDcdInfo.mgdErrUvBs3 = input[38];
    MtrM_MainMgdDcdInfo.mgdErrUvBs2 = input[39];
    MtrM_MainMgdDcdInfo.mgdErrUvBs1 = input[40];
    MtrM_MainMgdDcdInfo.mgdErrCp2 = input[41];
    MtrM_MainMgdDcdInfo.mgdErrCp1 = input[42];
    MtrM_MainMgdDcdInfo.mgdErrOvBs3 = input[43];
    MtrM_MainMgdDcdInfo.mgdErrOvBs2 = input[44];
    MtrM_MainMgdDcdInfo.mgdErrOvBs1 = input[45];
    MtrM_MainMgdDcdInfo.mgdErrOvVdh = input[46];
    MtrM_MainMgdDcdInfo.mgdErrUvVdh = input[47];
    MtrM_MainMgdDcdInfo.mgdErrOvVs = input[48];
    MtrM_MainMgdDcdInfo.mgdErrUvVs = input[49];
    MtrM_MainMgdDcdInfo.mgdErrUvVcc = input[50];
    MtrM_MainMgdDcdInfo.mgdErrOvVcc = input[51];
    MtrM_MainMgdDcdInfo.mgdErrOvLdVdh = input[52];
    MtrM_MainMgdDcdInfo.mgdSdDdpStuck = input[53];
    MtrM_MainMgdDcdInfo.mgdSdCp1 = input[54];
    MtrM_MainMgdDcdInfo.mgdSdOvCp = input[55];
    MtrM_MainMgdDcdInfo.mgdSdClkfail = input[56];
    MtrM_MainMgdDcdInfo.mgdSdUvCb = input[57];
    MtrM_MainMgdDcdInfo.mgdSdOvVdh = input[58];
    MtrM_MainMgdDcdInfo.mgdSdOvVs = input[59];
    MtrM_MainMgdDcdInfo.mgdSdOt = input[60];
    MtrM_MainMgdDcdInfo.mgdErrScdLs3 = input[61];
    MtrM_MainMgdDcdInfo.mgdErrScdLs2 = input[62];
    MtrM_MainMgdDcdInfo.mgdErrScdLs1 = input[63];
    MtrM_MainMgdDcdInfo.mgdErrScdHs3 = input[64];
    MtrM_MainMgdDcdInfo.mgdErrScdHs2 = input[65];
    MtrM_MainMgdDcdInfo.mgdErrScdHs1 = input[66];
    MtrM_MainMgdDcdInfo.mgdErrIndLs3 = input[67];
    MtrM_MainMgdDcdInfo.mgdErrIndLs2 = input[68];
    MtrM_MainMgdDcdInfo.mgdErrIndLs1 = input[69];
    MtrM_MainMgdDcdInfo.mgdErrIndHs3 = input[70];
    MtrM_MainMgdDcdInfo.mgdErrIndHs2 = input[71];
    MtrM_MainMgdDcdInfo.mgdErrIndHs1 = input[72];
    MtrM_MainMgdDcdInfo.mgdErrOsfLs1 = input[73];
    MtrM_MainMgdDcdInfo.mgdErrOsfLs2 = input[74];
    MtrM_MainMgdDcdInfo.mgdErrOsfLs3 = input[75];
    MtrM_MainMgdDcdInfo.mgdErrOsfHs1 = input[76];
    MtrM_MainMgdDcdInfo.mgdErrOsfHs2 = input[77];
    MtrM_MainMgdDcdInfo.mgdErrOsfHs3 = input[78];
    MtrM_MainMgdDcdInfo.mgdErrSpiFrame = input[79];
    MtrM_MainMgdDcdInfo.mgdErrSpiTo = input[80];
    MtrM_MainMgdDcdInfo.mgdErrSpiWd = input[81];
    MtrM_MainMgdDcdInfo.mgdErrSpiCrc = input[82];
    MtrM_MainMgdDcdInfo.mgdEpiAddInvalid = input[83];
    MtrM_MainMgdDcdInfo.mgdEonfTo = input[84];
    MtrM_MainMgdDcdInfo.mgdEonfSigInvalid = input[85];
    MtrM_MainMgdDcdInfo.mgdErrOcOp1 = input[86];
    MtrM_MainMgdDcdInfo.mgdErrOp1Uv = input[87];
    MtrM_MainMgdDcdInfo.mgdErrOp1Ov = input[88];
    MtrM_MainMgdDcdInfo.mgdErrOp1Calib = input[89];
    MtrM_MainMgdDcdInfo.mgdErrOcOp2 = input[90];
    MtrM_MainMgdDcdInfo.mgdErrOp2Uv = input[91];
    MtrM_MainMgdDcdInfo.mgdErrOp2Ov = input[92];
    MtrM_MainMgdDcdInfo.mgdErrOp2Calib = input[93];
    MtrM_MainMgdDcdInfo.mgdErrOcOp3 = input[94];
    MtrM_MainMgdDcdInfo.mgdErrOp3Uv = input[95];
    MtrM_MainMgdDcdInfo.mgdErrOp3Ov = input[96];
    MtrM_MainMgdDcdInfo.mgdErrOp3Calib = input[97];
    MtrM_MainMgdDcdInfo.mgdErrOutpErrn = input[98];
    MtrM_MainMgdDcdInfo.mgdErrOutpMiso = input[99];
    MtrM_MainMgdDcdInfo.mgdErrOutpPFB1 = input[100];
    MtrM_MainMgdDcdInfo.mgdErrOutpPFB2 = input[101];
    MtrM_MainMgdDcdInfo.mgdErrOutpPFB3 = input[102];
    MtrM_MainMgdDcdInfo.mgdTle9180ErrPort = input[103];
    MtrM_MainMotDqIRefMccInfo.IdRef = input[104];
    MtrM_MainMotDqIRefMccInfo.IqRef = input[105];
    MtrM_MainHwTrigMotInfo.Uphase0Mon = input[106];
    MtrM_MainHwTrigMotInfo.Uphase1Mon = input[107];
    MtrM_MainHwTrigMotInfo.Vphase0Mon = input[108];
    MtrM_MainHwTrigMotInfo.VPhase1Mon = input[109];
    MtrM_MainMotCurrMonInfo.MotCurrPhUMon0 = input[110];
    MtrM_MainMotCurrMonInfo.MotCurrPhUMon1 = input[111];
    MtrM_MainMotCurrMonInfo.MotCurrPhVMon0 = input[112];
    MtrM_MainMotCurrMonInfo.MotCurrPhVMon1 = input[113];
    MtrM_MainMotAngleMonInfo.MotPosiAngle1deg = input[114];
    MtrM_MainMotAngleMonInfo.MotPosiAngle2deg = input[115];
    MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd = input[116];
    MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd = input[117];
    MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd = input[118];
    MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg = input[119];
    MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg = input[120];
    MtrM_MainEcuModeSts = input[121];
    MtrM_MainIgnOnOffSts = input[122];
    MtrM_MainIgnEdgeSts = input[123];
    MtrM_MainVBatt1Mon = input[124];
    MtrM_MainVBatt2Mon = input[125];
    MtrM_MainDiagClrSrs = input[126];
    MtrM_MainMgdInvalid = input[127];
    MtrM_MainMtrArbDriveState = input[128];

    MtrM_Main();


    output[0] = MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err;
    output[1] = MtrM_MainMTRMOutInfo.MTRM_Open_Err;
    output[2] = MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err;
    output[3] = MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err;
    output[4] = MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err;
    output[5] = MtrM_MainMTRMOutInfo.MTRM_Calibration_Err;
    output[6] = MtrM_MainMTRMOutInfo.MTRM_Short_Err;
    output[7] = MtrM_MainMTRMOutInfo.MTRM_Driver_Err;
    output[8] = MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr;
    output[9] = MtrM_MainMgdErrInfo.MgdClkMonErr;
    output[10] = MtrM_MainMgdErrInfo.MgdBISTErr;
    output[11] = MtrM_MainMgdErrInfo.MgdFlashMemoryErr;
    output[12] = MtrM_MainMgdErrInfo.MgdRAMErr;
    output[13] = MtrM_MainMgdErrInfo.MgdConfigRegiErr;
    output[14] = MtrM_MainMgdErrInfo.MgdInputPattMonErr;
    output[15] = MtrM_MainMgdErrInfo.MgdOverTempErr;
    output[16] = MtrM_MainMgdErrInfo.MgdCPmpVMonErr;
    output[17] = MtrM_MainMgdErrInfo.MgdHBuffCapVErr;
    output[18] = MtrM_MainMgdErrInfo.MgdInOutPlauErr;
    output[19] = MtrM_MainMgdErrInfo.MgdCtrlSigMonErr;
    output[20] = MtrM_MainMTRInitEndFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    MtrM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
