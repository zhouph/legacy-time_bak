/**
 * @defgroup MtrM_Main_Ifa MtrM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTRM_MAIN_IFA_H_
#define MTRM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define MtrM_Main_Read_MtrM_MainMotMonInfo(data) do \
{ \
    *data = MtrM_MainMotMonInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotVoltsMonInfo(data) do \
{ \
    *data = MtrM_MainMotVoltsMonInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMpsD1SpiDcdInfo(data) do \
{ \
    *data = MtrM_MainMpsD1SpiDcdInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMpsD2SpiDcdInfo(data) do \
{ \
    *data = MtrM_MainMpsD2SpiDcdInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotDqIRefMccInfo(data) do \
{ \
    *data = MtrM_MainMotDqIRefMccInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainHwTrigMotInfo(data) do \
{ \
    *data = MtrM_MainHwTrigMotInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotCurrMonInfo(data) do \
{ \
    *data = MtrM_MainMotCurrMonInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotAngleMonInfo(data) do \
{ \
    *data = MtrM_MainMotAngleMonInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrProcessOutInfo(data) do \
{ \
    *data = MtrM_MainMtrProcessOutInfo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotMonInfo_MotPwrVoltMon(data) do \
{ \
    *data = MtrM_MainMotMonInfo.MotPwrVoltMon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotVoltsMonInfo_MotVoltPhUMon(data) do \
{ \
    *data = MtrM_MainMotVoltsMonInfo.MotVoltPhUMon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotVoltsMonInfo_MotVoltPhVMon(data) do \
{ \
    *data = MtrM_MainMotVoltsMonInfo.MotVoltPhVMon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotVoltsMonInfo_MotVoltPhWMon(data) do \
{ \
    *data = MtrM_MainMotVoltsMonInfo.MotVoltPhWMon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMpsD1SpiDcdInfo_S_MAGOL(data) do \
{ \
    *data = MtrM_MainMpsD1SpiDcdInfo.S_MAGOL; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMpsD2SpiDcdInfo_S_MAGOL(data) do \
{ \
    *data = MtrM_MainMpsD2SpiDcdInfo.S_MAGOL; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdIdleM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdIdleM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdConfM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdConfM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdConfLock(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdConfLock; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSelfTestM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSelfTestM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSoffM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSoffM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdRectM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdRectM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdNormM(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdNormM; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdOsf(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdOsf; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdOp(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdOp; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdScd(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdScd; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSd(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSd; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdIndiag(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdIndiag; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdOutp(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdOutp; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdExt(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdExt; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdInt12(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdInt12; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdRom(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdRom; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdLimpOn(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdLimpOn; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdStIncomplete(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdStIncomplete; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdApcAct(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdApcAct; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdGtm(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdGtm; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdCtrlRegInvalid(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdLfw(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdLfw; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOtW(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOtW; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvReg1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvReg1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvVccRom(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvVccRom; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvReg4(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvReg4; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvReg6(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvReg6; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvReg6(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvReg6; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvReg5(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvReg5; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvCb(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvCb; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrClkTrim(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrClkTrim; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvBs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvBs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvBs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvBs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvBs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvBs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrCp2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrCp2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrCp1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrCp1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvBs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvBs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvBs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvBs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvBs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvBs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvVdh(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvVdh; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvVdh(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvVdh; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvVs(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvVs; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvVs(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvVs; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrUvVcc(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrUvVcc; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvVcc(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvVcc; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOvLdVdh(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOvLdVdh; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdDdpStuck(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdDdpStuck; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdCp1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdCp1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdOvCp(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdOvCp; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdClkfail(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdClkfail; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdUvCb(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdUvCb; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdOvVdh(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdOvVdh; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdOvVs(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdOvVs; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdSdOt(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdSdOt; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrScdLs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrScdLs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrScdLs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrScdLs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrScdLs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrScdLs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrScdHs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrScdHs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrScdHs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrScdHs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrScdHs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrScdHs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrIndLs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrIndLs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrIndLs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrIndLs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrIndLs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrIndLs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrIndHs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrIndHs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrIndHs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrIndHs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrIndHs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrIndHs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOsfLs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOsfLs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOsfLs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOsfLs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOsfLs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOsfLs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOsfHs1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOsfHs1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOsfHs2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOsfHs2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOsfHs3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOsfHs3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrSpiFrame(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrSpiFrame; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrSpiTo(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrSpiTo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrSpiWd(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrSpiWd; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrSpiCrc(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrSpiCrc; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdEpiAddInvalid(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdEpiAddInvalid; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdEonfTo(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdEonfTo; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdEonfSigInvalid(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdEonfSigInvalid; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOcOp1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOcOp1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp1Uv(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp1Uv; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp1Ov(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp1Ov; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp1Calib(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp1Calib; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOcOp2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOcOp2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp2Uv(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp2Uv; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp2Ov(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp2Ov; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp2Calib(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp2Calib; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOcOp3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOcOp3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp3Uv(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp3Uv; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp3Ov(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp3Ov; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOp3Calib(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOp3Calib; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOutpErrn(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOutpErrn; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOutpMiso(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOutpMiso; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOutpPFB1(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOutpPFB1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOutpPFB2(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOutpPFB2; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdErrOutpPFB3(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdErrOutpPFB3; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdDcdInfo_mgdTle9180ErrPort(data) do \
{ \
    *data = MtrM_MainMgdDcdInfo.mgdTle9180ErrPort; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotDqIRefMccInfo_IdRef(data) do \
{ \
    *data = MtrM_MainMotDqIRefMccInfo.IdRef; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotDqIRefMccInfo_IqRef(data) do \
{ \
    *data = MtrM_MainMotDqIRefMccInfo.IqRef; \
}while(0);

#define MtrM_Main_Read_MtrM_MainHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    *data = MtrM_MainHwTrigMotInfo.Uphase0Mon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    *data = MtrM_MainHwTrigMotInfo.Uphase1Mon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    *data = MtrM_MainHwTrigMotInfo.Vphase0Mon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    *data = MtrM_MainHwTrigMotInfo.VPhase1Mon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    *data = MtrM_MainMotCurrMonInfo.MotCurrPhUMon0; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    *data = MtrM_MainMotCurrMonInfo.MotCurrPhUMon1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    *data = MtrM_MainMotCurrMonInfo.MotCurrPhVMon0; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    *data = MtrM_MainMotCurrMonInfo.MotCurrPhVMon1; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    *data = MtrM_MainMotAngleMonInfo.MotPosiAngle1deg; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    *data = MtrM_MainMotAngleMonInfo.MotPosiAngle2deg; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    *data = MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    *data = MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    *data = MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    *data = MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    *data = MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg; \
}while(0);

#define MtrM_Main_Read_MtrM_MainEcuModeSts(data) do \
{ \
    *data = MtrM_MainEcuModeSts; \
}while(0);

#define MtrM_Main_Read_MtrM_MainIgnOnOffSts(data) do \
{ \
    *data = MtrM_MainIgnOnOffSts; \
}while(0);

#define MtrM_Main_Read_MtrM_MainIgnEdgeSts(data) do \
{ \
    *data = MtrM_MainIgnEdgeSts; \
}while(0);

#define MtrM_Main_Read_MtrM_MainVBatt1Mon(data) do \
{ \
    *data = MtrM_MainVBatt1Mon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainVBatt2Mon(data) do \
{ \
    *data = MtrM_MainVBatt2Mon; \
}while(0);

#define MtrM_Main_Read_MtrM_MainDiagClrSrs(data) do \
{ \
    *data = MtrM_MainDiagClrSrs; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMgdInvalid(data) do \
{ \
    *data = MtrM_MainMgdInvalid; \
}while(0);

#define MtrM_Main_Read_MtrM_MainMtrArbDriveState(data) do \
{ \
    *data = MtrM_MainMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define MtrM_Main_Write_MtrM_MainMTRMOutInfo(data) do \
{ \
    MtrM_MainMTRMOutInfo = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo(data) do \
{ \
    MtrM_MainMgdErrInfo = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Power_Open_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Open_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Open_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Phase_U_OverCurret_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Phase_V_OverCurret_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Phase_W_OverCurret_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Calibration_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Calibration_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Short_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Short_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRMOutInfo_MTRM_Driver_Err(data) do \
{ \
    MtrM_MainMTRMOutInfo.MTRM_Driver_Err = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdInterPwrSuppMonErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdClkMonErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdClkMonErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdBISTErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdBISTErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdFlashMemoryErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdFlashMemoryErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdRAMErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdRAMErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdConfigRegiErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdConfigRegiErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdInputPattMonErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdInputPattMonErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdOverTempErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdOverTempErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdCPmpVMonErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdCPmpVMonErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdHBuffCapVErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdHBuffCapVErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdInOutPlauErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdInOutPlauErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMgdErrInfo_MgdCtrlSigMonErr(data) do \
{ \
    MtrM_MainMgdErrInfo.MgdCtrlSigMonErr = *data; \
}while(0);

#define MtrM_Main_Write_MtrM_MainMTRInitEndFlg(data) do \
{ \
    MtrM_MainMTRInitEndFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTRM_MAIN_IFA_H_ */
/** @} */
