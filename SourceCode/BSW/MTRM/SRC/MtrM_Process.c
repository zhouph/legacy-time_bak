/**
 * @defgroup MtrM_Main MtrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MtrM_Main.h"
#include "MtrM_Main_Ifa.h"

#include <stdlib.h>

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SET 1u
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTRM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTRM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"

static boolean MtrM_CheckInhibit(MtrM_Main_HdrBusType *pMtrM_Info);
static void MtrM_PowerOpenCheck(MtrM_Main_HdrBusType *pMtrM_PowerOpenCheck,boolean bInhibit);
static void MtrM_MotorOpenCheck(MtrM_Main_HdrBusType *pMtrM_MotorOpenCheck,boolean bInhibit);
static void MtrM_MotorCalibrationCheck(MtrM_Main_HdrBusType *pMtrM_Cal,boolean bInhibit);
static void MtrM_MotorShortCheck(MtrM_Main_HdrBusType *pMtrM_InverterShortCheck,boolean bInhibit);
static void MtrM_MotorOverCurrentCheck(MtrM_Main_HdrBusType *pMtrM_OverCurrentCheck,boolean bInhibit);
static Saluint8 MtrM_MotorPhaseCurrentChk(Saluint32 Current);
static Saluint8 MtrM_MtrPwrMonitor(MtrM_Main_HdrBusType *pMtrM_MtrPwrMonitor);
static Saluint8 MtrM_MtrBat2Monotor(MtrM_Main_HdrBusType *pMtrM_MtrBat2Monotor);
static Saluint8 MtrM_MotorDrivingOpenChk(MtrM_Main_HdrBusType *pMtrM_MotorDrivingCheck);

static boolean MgdM_CheckInhibit(MtrM_Main_HdrBusType *pMtrM_Info);
static void MgdM_InterPwrSupply(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckClock(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckBistErr(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckFlashMemory(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckRamErr(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckConfigRegister(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckInputPattern(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckOverTemp(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckCPVoltage(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckHBuffCap(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckInOutPlau(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);
static void MgdM_CheckCtrlSig(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit);

void   MtrM_Process(MtrM_Main_HdrBusType *pMtrM_Info)
{
    boolean bInhibit = FALSE;
    /* Motor Inverter / Power / Cal */
    bInhibit = MtrM_CheckInhibit(pMtrM_Info);
    MtrM_PowerOpenCheck(pMtrM_Info,bInhibit);
    MtrM_MotorOpenCheck(pMtrM_Info,bInhibit);
    MtrM_MotorCalibrationCheck(pMtrM_Info,bInhibit);
    MtrM_MotorShortCheck(pMtrM_Info,bInhibit);
    MtrM_MotorOverCurrentCheck(pMtrM_Info,bInhibit);
    /* Motor Gate Driver */
    bInhibit = MgdM_CheckInhibit(pMtrM_Info);
    MgdM_InterPwrSupply(pMtrM_Info, bInhibit);
    MgdM_CheckClock(pMtrM_Info, bInhibit);
    MgdM_CheckBistErr(pMtrM_Info, bInhibit);
    MgdM_CheckFlashMemory(pMtrM_Info, bInhibit);
    MgdM_CheckRamErr(pMtrM_Info, bInhibit);
    MgdM_CheckConfigRegister(pMtrM_Info, bInhibit);
    MgdM_CheckInputPattern(pMtrM_Info, bInhibit);
    MgdM_CheckOverTemp(pMtrM_Info, bInhibit);
    MgdM_CheckCPVoltage(pMtrM_Info, bInhibit);
    MgdM_CheckHBuffCap(pMtrM_Info, bInhibit);
    MgdM_CheckInOutPlau(pMtrM_Info, bInhibit);
    MgdM_CheckCtrlSig(pMtrM_Info, bInhibit);

    /* Temp Init End Flg On */
    pMtrM_Info->MtrM_MainMTRInitEndFlg = TRUE;
}

static boolean MtrM_CheckInhibit(MtrM_Main_HdrBusType *pMtrM_Info)
{
    boolean bInhibit = FALSE;

    if(
        (pMtrM_Info->MtrM_MainVBatt1Mon < U16_CALCVOLT_7V0)
        ||(pMtrM_Info->MtrM_MainVBatt1Mon > U16_CALCVOLT_17V0)
      )
    {
        bInhibit = TRUE;
    }

    return bInhibit;
}
/*************************
	Motor Power Check 
	************************/
static void MtrM_PowerOpenCheck(MtrM_Main_HdrBusType *pMtrM_PowerOpenCheck,boolean bInhibit)
{
        Saluint8 MotPwrError	=MtrM_MtrPwrMonitor(pMtrM_PowerOpenCheck);
        Saluint8 MotBat2Error=MtrM_MtrBat2Monotor(pMtrM_PowerOpenCheck);

        if((MotPwrError ==MTRM_ERRDETECED)||(MotBat2Error ==MTRM_ERRDETECED))
        {
            pMtrM_PowerOpenCheck->MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err=ERR_PREFAILED;
        }
        else
        {
            pMtrM_PowerOpenCheck->MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err=ERR_PREPASSED;
        }

        if(bInhibit==TRUE)
        {
             pMtrM_PowerOpenCheck->MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err= ERR_INHIBIT;
        }
}

static Saluint8 MtrM_MtrPwrMonitor(MtrM_Main_HdrBusType *pMtrM_MtrPwrMonitor)
{
	Saluint16 DiffPower =0;
	static Saluint8   MtrErr =0;
	DiffPower =(Saluint16)abs(pMtrM_MtrPwrMonitor->MtrM_MainVBatt2Mon -pMtrM_MtrPwrMonitor->MtrM_MainMotMonInfo.MotPwrVoltMon);
	if(DiffPower>MTRM_VOLT_2V0) /* Threshold  = 2V */
	{
		MtrErr =MTRM_ERRDETECED;
	}
       else if(DiffPower<=MTRM_VOLT_1V0)
       {
              MtrErr =0;
       }
       else
       {
            ;
       }
	return MtrErr;
}

static Saluint8 MtrM_MtrBat2Monotor(MtrM_Main_HdrBusType *pMtrM_MtrBat2Monotor)
{
	static Saluint8   MtrErr =0;
    
	if(pMtrM_MtrBat2Monotor->MtrM_MainVBatt1Mon>MTRM_VOLT_7V5)
	{
		if(pMtrM_MtrBat2Monotor->MtrM_MainVBatt2Mon <=MTRM_VOLT_4V0)
		{
		 	MtrErr =MTRM_ERRDETECED;
		}
              else if(pMtrM_MtrBat2Monotor->MtrM_MainVBatt2Mon >MTRM_VOLT_5V0)
              {
                    MtrErr =0;
              }
              else
              {
                    ;
              }
	}
       
	return MtrErr;
}


/*************************
	Motor Open Check 
	************************/
static void MtrM_MotorOpenCheck(MtrM_Main_HdrBusType *pMtrM_MotorOpenCheck,boolean bInhibit)
{
        Saluint8 MtrM_MagmeticLoss = pMtrM_MotorOpenCheck->MtrM_MainMpsD1SpiDcdInfo.S_MAGOL; /* pMtrM_MotorOpenCheck->MtrM_MainMgdDcdInfo.S_MAGOL; */
        Saluint8 MtrM_MotorDrivingDetect = MtrM_MotorDrivingOpenChk(pMtrM_MotorOpenCheck);

        if((MtrM_MagmeticLoss ==MTRM_ERRDETECED)||(MtrM_MotorDrivingDetect==MTRM_ERRDETECED))
        {
            pMtrM_MotorOpenCheck->MtrM_MainMTRMOutInfo.MTRM_Open_Err=ERR_PREFAILED;
        }
        else
        {
            pMtrM_MotorOpenCheck->MtrM_MainMTRMOutInfo.MTRM_Open_Err=ERR_PREPASSED;
        }	

	 if(bInhibit==TRUE)
        {
             pMtrM_MotorOpenCheck->MtrM_MainMTRMOutInfo.MTRM_Open_Err=ERR_INHIBIT;
        }
}

static Saluint8 MtrM_MotorDrivingOpenChk(MtrM_Main_HdrBusType *pMtrM_MotorDrivingCheck)
{
	Saluint8   MtrErr=0;
	Salsint32 MtrM_Iu=0;
       Salsint32 MtrM_Iv=0;  

	MtrM_Iu = pMtrM_MotorDrivingCheck->MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd;
	MtrM_Iv = pMtrM_MotorDrivingCheck->MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd;

	if( pMtrM_MotorDrivingCheck->MtrM_MainMotDqIRefMccInfo.IqRef != 0)
	{
		if((MtrM_Iu==0)||(MtrM_Iv==0))
		{
			MtrErr =MTRM_ERRDETECED;
		}
	}

	return MtrErr;
}

/*************************
	Motor Calibration Check
	*************************/
static void MtrM_MotorCalibrationCheck(MtrM_Main_HdrBusType *pMtrM_Cal,boolean bInhibit)
{
        /* Need Memory Monitoring Module */
        if(bInhibit==TRUE)
        {
             pMtrM_Cal->MtrM_MainMTRMOutInfo.MTRM_Calibration_Err =ERR_INHIBIT;
        }
}
/*************************
	Motor Short Check 
	************************/
static void MtrM_MotorShortCheck(MtrM_Main_HdrBusType *pMtrM_InverterShortCheck,boolean bInhibit)
{
        Saluint8 MtrM_UHighSide	=pMtrM_InverterShortCheck->MtrM_MainMgdDcdInfo.mgdErrScdHs1;
        Saluint8 MtrM_VHighSide	=pMtrM_InverterShortCheck->MtrM_MainMgdDcdInfo.mgdErrScdHs2;
        Saluint8 MtrM_WHighSide	=pMtrM_InverterShortCheck->MtrM_MainMgdDcdInfo.mgdErrScdHs3;
        Saluint8 MtrM_ULowSide	=pMtrM_InverterShortCheck->MtrM_MainMgdDcdInfo.mgdErrScdLs1;
        Saluint8 MtrM_VLowSide	=pMtrM_InverterShortCheck->MtrM_MainMgdDcdInfo.mgdErrScdLs2;
        Saluint8 MtrM_WLowSide	=pMtrM_InverterShortCheck->MtrM_MainMgdDcdInfo.mgdErrScdLs3;
        Saluint8 MtrM_MotorShortErr = (Saluint8)(MtrM_UHighSide|MtrM_VHighSide|MtrM_WHighSide|MtrM_ULowSide|MtrM_VLowSide|MtrM_WLowSide);
        /* Short Circuit Error Detect */
        if(MtrM_MotorShortErr==MTRM_ERRDETECED)
        {
            pMtrM_InverterShortCheck->MtrM_MainMTRMOutInfo.MTRM_Short_Err=ERR_PREFAILED;
        }
        else
        {
            pMtrM_InverterShortCheck->MtrM_MainMTRMOutInfo.MTRM_Short_Err=ERR_PREPASSED;
        }

        if(bInhibit == TRUE)
        {
            pMtrM_InverterShortCheck->MtrM_MainMTRMOutInfo.MTRM_Short_Err=ERR_INHIBIT;
        }
}
/*************************
	Motor Over current Check 
	************************/
static void MtrM_MotorOverCurrentCheck(MtrM_Main_HdrBusType *pMtrM_OverCurrentCheck,boolean bInhibit)
{
        /* Calculation */
        Salsint32 MtrM_Iu=0; 
        Salsint32 MtrM_Iv=0;
        Salsint32 MtrM_Iw=0;
        Saluint32 MtrM_ABSIu=0;
        Saluint32 MtrM_ABSIv=0;
        Saluint32 MtrM_ABSIw=0;
        Saluint8   MtrM_UPhaseErr=0;
        Saluint8   MtrM_VPhaseErr=0;
        Saluint8   MtrM_WPhaseErr=0;

        MtrM_Iu = pMtrM_OverCurrentCheck->MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd;
        MtrM_Iv = pMtrM_OverCurrentCheck->MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd;
        MtrM_Iw = pMtrM_OverCurrentCheck->MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd;

        MtrM_ABSIu =(Saluint32)abs(MtrM_Iu);
        MtrM_ABSIv =(Saluint32)abs(MtrM_Iv);
        MtrM_ABSIw =(Saluint32)abs(MtrM_Iw);

        MtrM_UPhaseErr =MtrM_MotorPhaseCurrentChk(MtrM_ABSIu);
        MtrM_VPhaseErr =MtrM_MotorPhaseCurrentChk(MtrM_ABSIv);
        MtrM_WPhaseErr=MtrM_MotorPhaseCurrentChk(MtrM_ABSIw);

        /* U Phase */
        if(MtrM_UPhaseErr==MTRM_ERR)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err=ERR_FAILED;
        }
        else if(MtrM_UPhaseErr==MTRM_ERRDETECED)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err=ERR_PREFAILED;
        }
        else
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err=ERR_PREPASSED;
        }
        /* V Phase */
        if(MtrM_VPhaseErr==MTRM_ERR)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err=ERR_FAILED;
        }
        else if(MtrM_VPhaseErr==MTRM_ERRDETECED)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err=ERR_PREFAILED;
        }
        else
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err=ERR_PREPASSED;
        }
        /* W Phase */
        if(MtrM_WPhaseErr==MTRM_ERR)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err=ERR_FAILED;
        }
        else if(MtrM_WPhaseErr==MTRM_ERRDETECED)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err=ERR_PREFAILED;
        }
        else
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err=ERR_PREPASSED;
        }

        if(bInhibit==TRUE)
        {
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err=ERR_INHIBIT;
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err=ERR_INHIBIT;
            pMtrM_OverCurrentCheck->MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err=ERR_INHIBIT;
        }
}

static Saluint8 MtrM_MotorPhaseCurrentChk(Saluint32 Current)
{
	Saluint8 MtrM_Phase_Err=0;
       if((Current>=MTRM_CURRENT_120A))
	{
		MtrM_Phase_Err =MTRM_ERR;
	}
       else if((Current<MTRM_CURRENT_120A)&&(Current>=MTRM_CURRENT_100A))
       {
              MtrM_Phase_Err = MTRM_ERRDETECED;
       }
       else
       {
            ;
       }
        
	return MtrM_Phase_Err;
}

/*************************
	Motor Gate Driver 
	************************/
static boolean MgdM_CheckInhibit(MtrM_Main_HdrBusType *pMtrM_Info)
{
    boolean bInhibit = FALSE;

    if((pMtrM_Info->MtrM_MainMgdInvalid> 0) ||
       (pMtrM_Info->MtrM_MainVBatt1Mon < U16_CALCVOLT_7V0) ||
       (pMtrM_Info->MtrM_MainVBatt1Mon > U16_CALCVOLT_17V0)
      )
    {
        bInhibit = TRUE;
    }

    return bInhibit;
}

static void MgdM_InterPwrSupply(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOvReg1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvVccRom == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvReg4 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOvReg6 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvReg6 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvReg5 == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr = ERR_PREPASSED;
    }
    if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckClock(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrClkTrim == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdSdClkfail == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdClkMonErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdClkMonErr = ERR_PREPASSED;
    }

    if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdClkMonErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckBistErr(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if(pMtrM_Info->MtrM_MainMgdDcdInfo.mgdStIncomplete == SET)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdBISTErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdBISTErr = ERR_PREPASSED;
    }

    if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdBISTErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckFlashMemory(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    pMtrM_Info->MtrM_MainMgdErrInfo.MgdFlashMemoryErr = ERR_NONE;
}

static void MgdM_CheckRamErr(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    pMtrM_Info->MtrM_MainMgdErrInfo.MgdRAMErr = ERR_NONE;
}

static void MgdM_CheckConfigRegister(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdEonfTo == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdEonfSigInvalid == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdConfigRegiErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdConfigRegiErr = ERR_PREPASSED;
    }

     if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdConfigRegiErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckInputPattern(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdIndiag == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrIndLs1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrIndLs2 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrIndLs3 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrIndHs1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrIndHs2 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrIndHs3 == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInputPattMonErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInputPattMonErr = ERR_PREPASSED;
    }
    
     if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInputPattMonErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckOverTemp(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOtW == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdSdOt == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdOverTempErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdOverTempErr = ERR_PREPASSED;
    }

     if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdOverTempErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckCPVoltage(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvCb == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrCp1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrCp2 == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdCPmpVMonErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdCPmpVMonErr = ERR_PREPASSED;
    }

     if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdCPmpVMonErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckHBuffCap(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvBs1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvBs2 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrUvBs3 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOvBs1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOvBs2 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOvBs3 == SET)
      )
    {
       pMtrM_Info->MtrM_MainMgdErrInfo.MgdHBuffCapVErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdHBuffCapVErr = ERR_PREPASSED;
    }

    if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdHBuffCapVErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckInOutPlau(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if((pMtrM_Info->MtrM_MainMgdDcdInfo.mgdOsf == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOsfLs1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOsfLs2 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOsfLs3 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOsfHs1 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOsfHs2 == SET) ||
       (pMtrM_Info->MtrM_MainMgdDcdInfo.mgdErrOsfHs3 == SET)
      )
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInOutPlauErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInOutPlauErr = ERR_PREPASSED; 
    }
    
    if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdInOutPlauErr = ERR_INHIBIT;
    }
}

static void MgdM_CheckCtrlSig(MtrM_Main_HdrBusType *pMtrM_Info, boolean bInhibit)
{
    if(pMtrM_Info->MtrM_MainMgdDcdInfo.mgdSdDdpStuck == SET)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdCtrlSigMonErr = ERR_PREFAILED;
    }
    else
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdCtrlSigMonErr = ERR_PREPASSED;
    }
    
    if(bInhibit == TRUE)
    {
        pMtrM_Info->MtrM_MainMgdErrInfo.MgdCtrlSigMonErr = ERR_INHIBIT;
    }
}
