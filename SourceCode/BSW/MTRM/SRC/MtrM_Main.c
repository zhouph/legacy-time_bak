/**
 * @defgroup MtrM_Main MtrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MtrM_Main.h"
#include "MtrM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTRM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTRM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
MtrM_Main_HdrBusType MtrM_MainBus;

/* Version Info */
const SwcVersionInfo_t MtrM_MainVersionInfo = 
{   
    MTRM_MAIN_MODULE_ID,           /* MtrM_MainVersionInfo.ModuleId */
    MTRM_MAIN_MAJOR_VERSION,       /* MtrM_MainVersionInfo.MajorVer */
    MTRM_MAIN_MINOR_VERSION,       /* MtrM_MainVersionInfo.MinorVer */
    MTRM_MAIN_PATCH_VERSION,       /* MtrM_MainVersionInfo.PatchVer */
    MTRM_MAIN_BRANCH_VERSION       /* MtrM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputSR1msMotMonInfo_t MtrM_MainMotMonInfo;
Ioc_InputSR1msMotVoltsMonInfo_t MtrM_MainMotVoltsMonInfo;
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t MtrM_MainMpsD1SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t MtrM_MainMpsD2SpiDcdInfo;
Mgd_TLE9180_HndlrMgdDcdInfo_t MtrM_MainMgdDcdInfo;
Mcc_1msCtrlMotDqIRefMccInfo_t MtrM_MainMotDqIRefMccInfo;
AdcIf_Conv50usHwTrigMotInfo_t MtrM_MainHwTrigMotInfo;
Ioc_InputSR50usMotCurrMonInfo_t MtrM_MainMotCurrMonInfo;
Ioc_InputSR50usMotAngleMonInfo_t MtrM_MainMotAngleMonInfo;
Mtr_Processing_SigMtrProcessOutInfo_t MtrM_MainMtrProcessOutInfo;
Mom_HndlrEcuModeSts_t MtrM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t MtrM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t MtrM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t MtrM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t MtrM_MainVBatt2Mon;
Diag_HndlrDiagClr_t MtrM_MainDiagClrSrs;
Mgd_TLE9180_HndlrMgdInvalid_t MtrM_MainMgdInvalid;
Arbitrator_MtrMtrArbDriveState_t MtrM_MainMtrArbDriveState;

/* Output Data Element */
MtrM_MainMTRMOutInfo_t MtrM_MainMTRMOutInfo;
MtrM_MainMgdErrInfo_t MtrM_MainMgdErrInfo;
MtrM_MainMTRInitTest_t MtrM_MainMTRInitEndFlg;

uint32 MtrM_Main_Timer_Start;
uint32 MtrM_Main_Timer_Elapsed;

#define MTRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
#define MTRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_MAIN_START_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (32BIT)**/


#define MTRM_MAIN_STOP_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
#define MTRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_MAIN_START_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (32BIT)**/


#define MTRM_MAIN_STOP_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTRM_MAIN_START_SEC_CODE
#include "MtrM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void MtrM_Main_Init(void)
{
    /* Initialize internal bus */
    MtrM_MainBus.MtrM_MainMotMonInfo.MotPwrVoltMon = 0;
    MtrM_MainBus.MtrM_MainMotVoltsMonInfo.MotVoltPhUMon = 0;
    MtrM_MainBus.MtrM_MainMotVoltsMonInfo.MotVoltPhVMon = 0;
    MtrM_MainBus.MtrM_MainMotVoltsMonInfo.MotVoltPhWMon = 0;
    MtrM_MainBus.MtrM_MainMpsD1SpiDcdInfo.S_MAGOL = 0;
    MtrM_MainBus.MtrM_MainMpsD2SpiDcdInfo.S_MAGOL = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdIdleM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdConfM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdConfLock = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSelfTestM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSoffM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdRectM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdNormM = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdOsf = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdOp = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdScd = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSd = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdIndiag = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdOutp = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdExt = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdInt12 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdRom = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdLimpOn = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdStIncomplete = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdApcAct = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdGtm = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdLfw = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOtW = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvReg1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvVccRom = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvReg4 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvReg6 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvReg6 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvReg5 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvCb = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrClkTrim = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvBs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvBs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvBs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrCp2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrCp1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvBs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvBs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvBs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvVdh = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvVdh = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvVs = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvVs = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrUvVcc = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvVcc = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOvLdVdh = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdDdpStuck = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdCp1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdOvCp = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdClkfail = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdUvCb = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdOvVdh = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdOvVs = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdSdOt = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrScdLs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrScdLs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrScdLs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrScdHs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrScdHs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrScdHs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrIndLs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrIndLs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrIndLs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrIndHs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrIndHs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrIndHs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOsfLs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOsfLs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOsfLs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOsfHs1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOsfHs2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOsfHs3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrSpiFrame = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrSpiTo = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrSpiWd = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrSpiCrc = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdEpiAddInvalid = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdEonfTo = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdEonfSigInvalid = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOcOp1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp1Uv = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp1Ov = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp1Calib = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOcOp2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp2Uv = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp2Ov = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp2Calib = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOcOp3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp3Uv = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp3Ov = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOp3Calib = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOutpErrn = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOutpMiso = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOutpPFB1 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOutpPFB2 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdErrOutpPFB3 = 0;
    MtrM_MainBus.MtrM_MainMgdDcdInfo.mgdTle9180ErrPort = 0;
    MtrM_MainBus.MtrM_MainMotDqIRefMccInfo.IdRef = 0;
    MtrM_MainBus.MtrM_MainMotDqIRefMccInfo.IqRef = 0;
    MtrM_MainBus.MtrM_MainHwTrigMotInfo.Uphase0Mon = 0;
    MtrM_MainBus.MtrM_MainHwTrigMotInfo.Uphase1Mon = 0;
    MtrM_MainBus.MtrM_MainHwTrigMotInfo.Vphase0Mon = 0;
    MtrM_MainBus.MtrM_MainHwTrigMotInfo.VPhase1Mon = 0;
    MtrM_MainBus.MtrM_MainMotCurrMonInfo.MotCurrPhUMon0 = 0;
    MtrM_MainBus.MtrM_MainMotCurrMonInfo.MotCurrPhUMon1 = 0;
    MtrM_MainBus.MtrM_MainMotCurrMonInfo.MotCurrPhVMon0 = 0;
    MtrM_MainBus.MtrM_MainMotCurrMonInfo.MotCurrPhVMon1 = 0;
    MtrM_MainBus.MtrM_MainMotAngleMonInfo.MotPosiAngle1deg = 0;
    MtrM_MainBus.MtrM_MainMotAngleMonInfo.MotPosiAngle2deg = 0;
    MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd = 0;
    MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd = 0;
    MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd = 0;
    MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg = 0;
    MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg = 0;
    MtrM_MainBus.MtrM_MainEcuModeSts = 0;
    MtrM_MainBus.MtrM_MainIgnOnOffSts = 0;
    MtrM_MainBus.MtrM_MainIgnEdgeSts = 0;
    MtrM_MainBus.MtrM_MainVBatt1Mon = 0;
    MtrM_MainBus.MtrM_MainVBatt2Mon = 0;
    MtrM_MainBus.MtrM_MainDiagClrSrs = 0;
    MtrM_MainBus.MtrM_MainMgdInvalid = 0;
    MtrM_MainBus.MtrM_MainMtrArbDriveState = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Open_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Calibration_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Short_Err = 0;
    MtrM_MainBus.MtrM_MainMTRMOutInfo.MTRM_Driver_Err = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdClkMonErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdBISTErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdFlashMemoryErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdRAMErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdConfigRegiErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdInputPattMonErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdOverTempErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdCPmpVMonErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdHBuffCapVErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdInOutPlauErr = 0;
    MtrM_MainBus.MtrM_MainMgdErrInfo.MgdCtrlSigMonErr = 0;
    MtrM_MainBus.MtrM_MainMTRInitEndFlg = 0;
}

void MtrM_Main(void)
{
    MtrM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    MtrM_Main_Read_MtrM_MainMotMonInfo_MotPwrVoltMon(&MtrM_MainBus.MtrM_MainMotMonInfo.MotPwrVoltMon);

    MtrM_Main_Read_MtrM_MainMotVoltsMonInfo(&MtrM_MainBus.MtrM_MainMotVoltsMonInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMotVoltsMonInfo 
     : MtrM_MainMotVoltsMonInfo.MotVoltPhUMon;
     : MtrM_MainMotVoltsMonInfo.MotVoltPhVMon;
     : MtrM_MainMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/
    
    /* Decomposed structure interface */
    MtrM_Main_Read_MtrM_MainMpsD1SpiDcdInfo_S_MAGOL(&MtrM_MainBus.MtrM_MainMpsD1SpiDcdInfo.S_MAGOL);

    /* Decomposed structure interface */
    MtrM_Main_Read_MtrM_MainMpsD2SpiDcdInfo_S_MAGOL(&MtrM_MainBus.MtrM_MainMpsD2SpiDcdInfo.S_MAGOL);

    MtrM_Main_Read_MtrM_MainMgdDcdInfo(&MtrM_MainBus.MtrM_MainMgdDcdInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMgdDcdInfo 
     : MtrM_MainMgdDcdInfo.mgdIdleM;
     : MtrM_MainMgdDcdInfo.mgdConfM;
     : MtrM_MainMgdDcdInfo.mgdConfLock;
     : MtrM_MainMgdDcdInfo.mgdSelfTestM;
     : MtrM_MainMgdDcdInfo.mgdSoffM;
     : MtrM_MainMgdDcdInfo.mgdErrM;
     : MtrM_MainMgdDcdInfo.mgdRectM;
     : MtrM_MainMgdDcdInfo.mgdNormM;
     : MtrM_MainMgdDcdInfo.mgdOsf;
     : MtrM_MainMgdDcdInfo.mgdOp;
     : MtrM_MainMgdDcdInfo.mgdScd;
     : MtrM_MainMgdDcdInfo.mgdSd;
     : MtrM_MainMgdDcdInfo.mgdIndiag;
     : MtrM_MainMgdDcdInfo.mgdOutp;
     : MtrM_MainMgdDcdInfo.mgdExt;
     : MtrM_MainMgdDcdInfo.mgdInt12;
     : MtrM_MainMgdDcdInfo.mgdRom;
     : MtrM_MainMgdDcdInfo.mgdLimpOn;
     : MtrM_MainMgdDcdInfo.mgdStIncomplete;
     : MtrM_MainMgdDcdInfo.mgdApcAct;
     : MtrM_MainMgdDcdInfo.mgdGtm;
     : MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid;
     : MtrM_MainMgdDcdInfo.mgdLfw;
     : MtrM_MainMgdDcdInfo.mgdErrOtW;
     : MtrM_MainMgdDcdInfo.mgdErrOvReg1;
     : MtrM_MainMgdDcdInfo.mgdErrUvVccRom;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg4;
     : MtrM_MainMgdDcdInfo.mgdErrOvReg6;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg6;
     : MtrM_MainMgdDcdInfo.mgdErrUvReg5;
     : MtrM_MainMgdDcdInfo.mgdErrUvCb;
     : MtrM_MainMgdDcdInfo.mgdErrClkTrim;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs3;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs2;
     : MtrM_MainMgdDcdInfo.mgdErrUvBs1;
     : MtrM_MainMgdDcdInfo.mgdErrCp2;
     : MtrM_MainMgdDcdInfo.mgdErrCp1;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs3;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs2;
     : MtrM_MainMgdDcdInfo.mgdErrOvBs1;
     : MtrM_MainMgdDcdInfo.mgdErrOvVdh;
     : MtrM_MainMgdDcdInfo.mgdErrUvVdh;
     : MtrM_MainMgdDcdInfo.mgdErrOvVs;
     : MtrM_MainMgdDcdInfo.mgdErrUvVs;
     : MtrM_MainMgdDcdInfo.mgdErrUvVcc;
     : MtrM_MainMgdDcdInfo.mgdErrOvVcc;
     : MtrM_MainMgdDcdInfo.mgdErrOvLdVdh;
     : MtrM_MainMgdDcdInfo.mgdSdDdpStuck;
     : MtrM_MainMgdDcdInfo.mgdSdCp1;
     : MtrM_MainMgdDcdInfo.mgdSdOvCp;
     : MtrM_MainMgdDcdInfo.mgdSdClkfail;
     : MtrM_MainMgdDcdInfo.mgdSdUvCb;
     : MtrM_MainMgdDcdInfo.mgdSdOvVdh;
     : MtrM_MainMgdDcdInfo.mgdSdOvVs;
     : MtrM_MainMgdDcdInfo.mgdSdOt;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs3;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs2;
     : MtrM_MainMgdDcdInfo.mgdErrScdLs1;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs3;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs2;
     : MtrM_MainMgdDcdInfo.mgdErrScdHs1;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs3;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs2;
     : MtrM_MainMgdDcdInfo.mgdErrIndLs1;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs3;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs2;
     : MtrM_MainMgdDcdInfo.mgdErrIndHs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs2;
     : MtrM_MainMgdDcdInfo.mgdErrOsfLs3;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs1;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs2;
     : MtrM_MainMgdDcdInfo.mgdErrOsfHs3;
     : MtrM_MainMgdDcdInfo.mgdErrSpiFrame;
     : MtrM_MainMgdDcdInfo.mgdErrSpiTo;
     : MtrM_MainMgdDcdInfo.mgdErrSpiWd;
     : MtrM_MainMgdDcdInfo.mgdErrSpiCrc;
     : MtrM_MainMgdDcdInfo.mgdEpiAddInvalid;
     : MtrM_MainMgdDcdInfo.mgdEonfTo;
     : MtrM_MainMgdDcdInfo.mgdEonfSigInvalid;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp1;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp1Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp2;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp2Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOcOp3;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Uv;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Ov;
     : MtrM_MainMgdDcdInfo.mgdErrOp3Calib;
     : MtrM_MainMgdDcdInfo.mgdErrOutpErrn;
     : MtrM_MainMgdDcdInfo.mgdErrOutpMiso;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB1;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB2;
     : MtrM_MainMgdDcdInfo.mgdErrOutpPFB3;
     : MtrM_MainMgdDcdInfo.mgdTle9180ErrPort;
     =============================================================================*/
    
    MtrM_Main_Read_MtrM_MainMotDqIRefMccInfo(&MtrM_MainBus.MtrM_MainMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMotDqIRefMccInfo 
     : MtrM_MainMotDqIRefMccInfo.IdRef;
     : MtrM_MainMotDqIRefMccInfo.IqRef;
     =============================================================================*/
    
    MtrM_Main_Read_MtrM_MainHwTrigMotInfo(&MtrM_MainBus.MtrM_MainHwTrigMotInfo);
    /*==============================================================================
    * Members of structure MtrM_MainHwTrigMotInfo 
     : MtrM_MainHwTrigMotInfo.Uphase0Mon;
     : MtrM_MainHwTrigMotInfo.Uphase1Mon;
     : MtrM_MainHwTrigMotInfo.Vphase0Mon;
     : MtrM_MainHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/
    
    MtrM_Main_Read_MtrM_MainMotCurrMonInfo(&MtrM_MainBus.MtrM_MainMotCurrMonInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMotCurrMonInfo 
     : MtrM_MainMotCurrMonInfo.MotCurrPhUMon0;
     : MtrM_MainMotCurrMonInfo.MotCurrPhUMon1;
     : MtrM_MainMotCurrMonInfo.MotCurrPhVMon0;
     : MtrM_MainMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/
    
    /* Decomposed structure interface */
    MtrM_Main_Read_MtrM_MainMotAngleMonInfo_MotPosiAngle1deg(&MtrM_MainBus.MtrM_MainMotAngleMonInfo.MotPosiAngle1deg);
    MtrM_Main_Read_MtrM_MainMotAngleMonInfo_MotPosiAngle2deg(&MtrM_MainBus.MtrM_MainMotAngleMonInfo.MotPosiAngle2deg);

    /* Decomposed structure interface */
    MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhUMeasd(&MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd);
    MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhVMeasd(&MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd);
    MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotCurrPhWMeasd(&MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd);
    MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotPosiAngle1_1_100Deg(&MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg);
    MtrM_Main_Read_MtrM_MainMtrProcessOutInfo_MotPosiAngle2_1_100Deg(&MtrM_MainBus.MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg);

    MtrM_Main_Read_MtrM_MainEcuModeSts(&MtrM_MainBus.MtrM_MainEcuModeSts);
    MtrM_Main_Read_MtrM_MainIgnOnOffSts(&MtrM_MainBus.MtrM_MainIgnOnOffSts);
    MtrM_Main_Read_MtrM_MainIgnEdgeSts(&MtrM_MainBus.MtrM_MainIgnEdgeSts);
    MtrM_Main_Read_MtrM_MainVBatt1Mon(&MtrM_MainBus.MtrM_MainVBatt1Mon);
    MtrM_Main_Read_MtrM_MainVBatt2Mon(&MtrM_MainBus.MtrM_MainVBatt2Mon);
    MtrM_Main_Read_MtrM_MainDiagClrSrs(&MtrM_MainBus.MtrM_MainDiagClrSrs);
    MtrM_Main_Read_MtrM_MainMgdInvalid(&MtrM_MainBus.MtrM_MainMgdInvalid);
    MtrM_Main_Read_MtrM_MainMtrArbDriveState(&MtrM_MainBus.MtrM_MainMtrArbDriveState);

    /* Process */
    MtrM_Process(&MtrM_MainBus);

    /* Output */
    MtrM_Main_Write_MtrM_MainMTRMOutInfo(&MtrM_MainBus.MtrM_MainMTRMOutInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMTRMOutInfo 
     : MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Open_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Calibration_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Short_Err;
     : MtrM_MainMTRMOutInfo.MTRM_Driver_Err;
     =============================================================================*/
    
    MtrM_Main_Write_MtrM_MainMgdErrInfo(&MtrM_MainBus.MtrM_MainMgdErrInfo);
    /*==============================================================================
    * Members of structure MtrM_MainMgdErrInfo 
     : MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr;
     : MtrM_MainMgdErrInfo.MgdClkMonErr;
     : MtrM_MainMgdErrInfo.MgdBISTErr;
     : MtrM_MainMgdErrInfo.MgdFlashMemoryErr;
     : MtrM_MainMgdErrInfo.MgdRAMErr;
     : MtrM_MainMgdErrInfo.MgdConfigRegiErr;
     : MtrM_MainMgdErrInfo.MgdInputPattMonErr;
     : MtrM_MainMgdErrInfo.MgdOverTempErr;
     : MtrM_MainMgdErrInfo.MgdCPmpVMonErr;
     : MtrM_MainMgdErrInfo.MgdHBuffCapVErr;
     : MtrM_MainMgdErrInfo.MgdInOutPlauErr;
     : MtrM_MainMgdErrInfo.MgdCtrlSigMonErr;
     =============================================================================*/
    
    MtrM_Main_Write_MtrM_MainMTRInitEndFlg(&MtrM_MainBus.MtrM_MainMTRInitEndFlg);

    MtrM_Main_Timer_Elapsed = STM0_TIM0.U - MtrM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTRM_MAIN_STOP_SEC_CODE
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
