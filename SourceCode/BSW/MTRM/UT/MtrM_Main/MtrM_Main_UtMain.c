#include "unity.h"
#include "unity_fixture.h"
#include "MtrM_Main.h"
#include "MtrM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint16 UtInput_MtrM_MainMotMonInfo_MotPwrVoltMon[MAX_STEP] = MTRM_MAINMOTMONINFO_MOTPWRVOLTMON;
const Haluint16 UtInput_MtrM_MainMotVoltsMonInfo_MotVoltPhUMon[MAX_STEP] = MTRM_MAINMOTVOLTSMONINFO_MOTVOLTPHUMON;
const Haluint16 UtInput_MtrM_MainMotVoltsMonInfo_MotVoltPhVMon[MAX_STEP] = MTRM_MAINMOTVOLTSMONINFO_MOTVOLTPHVMON;
const Haluint16 UtInput_MtrM_MainMotVoltsMonInfo_MotVoltPhWMon[MAX_STEP] = MTRM_MAINMOTVOLTSMONINFO_MOTVOLTPHWMON;
const Haluint8 UtInput_MtrM_MainMpsD1SpiDcdInfo_S_MAGOL[MAX_STEP] = MTRM_MAINMPSD1SPIDCDINFO_S_MAGOL;
const Haluint8 UtInput_MtrM_MainMpsD2SpiDcdInfo_S_MAGOL[MAX_STEP] = MTRM_MAINMPSD2SPIDCDINFO_S_MAGOL;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdIdleM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDIDLEM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdConfM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDCONFM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdConfLock[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDCONFLOCK;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSelfTestM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSELFTESTM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSoffM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSOFFM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdRectM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDRECTM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdNormM[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDNORMM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdOsf[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDOSF;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdOp[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDOP;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdScd[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSCD;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSd[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSD;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdIndiag[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDINDIAG;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdOutp[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDOUTP;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdExt[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDEXT;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdInt12[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDINT12;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdRom[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDROM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdLimpOn[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDLIMPON;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdStIncomplete[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSTINCOMPLETE;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdApcAct[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDAPCACT;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdGtm[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDGTM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdCtrlRegInvalid[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDCTRLREGINVALID;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdLfw[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDLFW;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOtW[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROTW;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvReg1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVREG1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVccRom[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVVCCROM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvReg4[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVREG4;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvReg6[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVREG6;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvReg6[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVREG6;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvReg5[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVREG5;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvCb[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVCB;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrClkTrim[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRCLKTRIM;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvBs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVBS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvBs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVBS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvBs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVBS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrCp2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRCP2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrCp1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRCP1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvBs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVBS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvBs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVBS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvBs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVBS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvVdh[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVVDH;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVdh[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVVDH;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvVs[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVVS;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVs[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVVS;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVcc[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRUVVCC;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvVcc[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVVCC;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOvLdVdh[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROVLDVDH;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdDdpStuck[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDDDPSTUCK;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdCp1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDCP1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdOvCp[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDOVCP;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdClkfail[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDCLKFAIL;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdUvCb[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDUVCB;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdOvVdh[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDOVVDH;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdOvVs[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDOVVS;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdSdOt[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDSDOT;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrScdLs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSCDLS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrScdLs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSCDLS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrScdLs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSCDLS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrScdHs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSCDHS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrScdHs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSCDHS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrScdHs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSCDHS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrIndLs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRINDLS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrIndLs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRINDLS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrIndLs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRINDLS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrIndHs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRINDHS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrIndHs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRINDHS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrIndHs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRINDHS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfLs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROSFLS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfLs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROSFLS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfLs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROSFLS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfHs1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROSFHS1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfHs2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROSFHS2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfHs3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROSFHS3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiFrame[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSPIFRAME;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiTo[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSPITO;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiWd[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSPIWD;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiCrc[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERRSPICRC;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdEpiAddInvalid[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDEPIADDINVALID;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdEonfTo[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDEONFTO;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdEonfSigInvalid[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDEONFSIGINVALID;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOcOp1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROCOP1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp1Uv[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP1UV;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp1Ov[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP1OV;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp1Calib[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP1CALIB;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOcOp2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROCOP2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp2Uv[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP2UV;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp2Ov[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP2OV;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp2Calib[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP2CALIB;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOcOp3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROCOP3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp3Uv[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP3UV;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp3Ov[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP3OV;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOp3Calib[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROP3CALIB;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpErrn[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROUTPERRN;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpMiso[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROUTPMISO;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpPFB1[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROUTPPFB1;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpPFB2[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROUTPPFB2;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpPFB3[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDERROUTPPFB3;
const Haluint8 UtInput_MtrM_MainMgdDcdInfo_mgdTle9180ErrPort[MAX_STEP] = MTRM_MAINMGDDCDINFO_MGDTLE9180ERRPORT;
const Rtesint32 UtInput_MtrM_MainMotDqIRefMccInfo_IdRef[MAX_STEP] = MTRM_MAINMOTDQIREFMCCINFO_IDREF;
const Rtesint32 UtInput_MtrM_MainMotDqIRefMccInfo_IqRef[MAX_STEP] = MTRM_MAINMOTDQIREFMCCINFO_IQREF;
const Haluint16 UtInput_MtrM_MainHwTrigMotInfo_Uphase0Mon[MAX_STEP] = MTRM_MAINHWTRIGMOTINFO_UPHASE0MON;
const Haluint16 UtInput_MtrM_MainHwTrigMotInfo_Uphase1Mon[MAX_STEP] = MTRM_MAINHWTRIGMOTINFO_UPHASE1MON;
const Haluint16 UtInput_MtrM_MainHwTrigMotInfo_Vphase0Mon[MAX_STEP] = MTRM_MAINHWTRIGMOTINFO_VPHASE0MON;
const Haluint16 UtInput_MtrM_MainHwTrigMotInfo_VPhase1Mon[MAX_STEP] = MTRM_MAINHWTRIGMOTINFO_VPHASE1MON;
const Haluint16 UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhUMon0[MAX_STEP] = MTRM_MAINMOTCURRMONINFO_MOTCURRPHUMON0;
const Haluint16 UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhUMon1[MAX_STEP] = MTRM_MAINMOTCURRMONINFO_MOTCURRPHUMON1;
const Haluint16 UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhVMon0[MAX_STEP] = MTRM_MAINMOTCURRMONINFO_MOTCURRPHVMON0;
const Haluint16 UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhVMon1[MAX_STEP] = MTRM_MAINMOTCURRMONINFO_MOTCURRPHVMON1;
const Haluint16 UtInput_MtrM_MainMotAngleMonInfo_MotPosiAngle1deg[MAX_STEP] = MTRM_MAINMOTANGLEMONINFO_MOTPOSIANGLE1DEG;
const Haluint16 UtInput_MtrM_MainMotAngleMonInfo_MotPosiAngle2deg[MAX_STEP] = MTRM_MAINMOTANGLEMONINFO_MOTPOSIANGLE2DEG;
const Salsint32 UtInput_MtrM_MainMtrProcessOutInfo_MotCurrPhUMeasd[MAX_STEP] = MTRM_MAINMTRPROCESSOUTINFO_MOTCURRPHUMEASD;
const Salsint32 UtInput_MtrM_MainMtrProcessOutInfo_MotCurrPhVMeasd[MAX_STEP] = MTRM_MAINMTRPROCESSOUTINFO_MOTCURRPHVMEASD;
const Salsint32 UtInput_MtrM_MainMtrProcessOutInfo_MotCurrPhWMeasd[MAX_STEP] = MTRM_MAINMTRPROCESSOUTINFO_MOTCURRPHWMEASD;
const Salsint32 UtInput_MtrM_MainMtrProcessOutInfo_MotPosiAngle1_1_100Deg[MAX_STEP] = MTRM_MAINMTRPROCESSOUTINFO_MOTPOSIANGLE1_1_100DEG;
const Salsint32 UtInput_MtrM_MainMtrProcessOutInfo_MotPosiAngle2_1_100Deg[MAX_STEP] = MTRM_MAINMTRPROCESSOUTINFO_MOTPOSIANGLE2_1_100DEG;
const Mom_HndlrEcuModeSts_t UtInput_MtrM_MainEcuModeSts[MAX_STEP] = MTRM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_MtrM_MainIgnOnOffSts[MAX_STEP] = MTRM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_MtrM_MainIgnEdgeSts[MAX_STEP] = MTRM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_MtrM_MainVBatt1Mon[MAX_STEP] = MTRM_MAINVBATT1MON;
const Ioc_InputSR1msVBatt2Mon_t UtInput_MtrM_MainVBatt2Mon[MAX_STEP] = MTRM_MAINVBATT2MON;
const Diag_HndlrDiagClr_t UtInput_MtrM_MainDiagClrSrs[MAX_STEP] = MTRM_MAINDIAGCLRSRS;
const Mgd_TLE9180_HndlrMgdInvalid_t UtInput_MtrM_MainMgdInvalid[MAX_STEP] = MTRM_MAINMGDINVALID;
const Arbitrator_MtrMtrArbDriveState_t UtInput_MtrM_MainMtrArbDriveState[MAX_STEP] = MTRM_MAINMTRARBDRIVESTATE;

const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Power_Open_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_POWER_OPEN_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Open_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_OPEN_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Phase_U_OverCurret_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_PHASE_U_OVERCURRET_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Phase_V_OverCurret_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_PHASE_V_OVERCURRET_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Phase_W_OverCurret_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_PHASE_W_OVERCURRET_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Calibration_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_CALIBRATION_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Short_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_SHORT_ERR;
const Saluint8 UtExpected_MtrM_MainMTRMOutInfo_MTRM_Driver_Err[MAX_STEP] = MTRM_MAINMTRMOUTINFO_MTRM_DRIVER_ERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdInterPwrSuppMonErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDINTERPWRSUPPMONERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdClkMonErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDCLKMONERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdBISTErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDBISTERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdFlashMemoryErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDFLASHMEMORYERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdRAMErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDRAMERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdConfigRegiErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDCONFIGREGIERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdInputPattMonErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDINPUTPATTMONERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdOverTempErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDOVERTEMPERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdCPmpVMonErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDCPMPVMONERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdHBuffCapVErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDHBUFFCAPVERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdInOutPlauErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDINOUTPLAUERR;
const Saluint8 UtExpected_MtrM_MainMgdErrInfo_MgdCtrlSigMonErr[MAX_STEP] = MTRM_MAINMGDERRINFO_MGDCTRLSIGMONERR;
const MtrM_MainMTRInitTest_t UtExpected_MtrM_MainMTRInitEndFlg[MAX_STEP] = MTRM_MAINMTRINITENDFLG;



TEST_GROUP(MtrM_Main);
TEST_SETUP(MtrM_Main)
{
    MtrM_Main_Init();
}

TEST_TEAR_DOWN(MtrM_Main)
{   /* Postcondition */

}

TEST(MtrM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        MtrM_MainMotMonInfo.MotPwrVoltMon = UtInput_MtrM_MainMotMonInfo_MotPwrVoltMon[i];
        MtrM_MainMotVoltsMonInfo.MotVoltPhUMon = UtInput_MtrM_MainMotVoltsMonInfo_MotVoltPhUMon[i];
        MtrM_MainMotVoltsMonInfo.MotVoltPhVMon = UtInput_MtrM_MainMotVoltsMonInfo_MotVoltPhVMon[i];
        MtrM_MainMotVoltsMonInfo.MotVoltPhWMon = UtInput_MtrM_MainMotVoltsMonInfo_MotVoltPhWMon[i];
        MtrM_MainMpsD1SpiDcdInfo.S_MAGOL = UtInput_MtrM_MainMpsD1SpiDcdInfo_S_MAGOL[i];
        MtrM_MainMpsD2SpiDcdInfo.S_MAGOL = UtInput_MtrM_MainMpsD2SpiDcdInfo_S_MAGOL[i];
        MtrM_MainMgdDcdInfo.mgdIdleM = UtInput_MtrM_MainMgdDcdInfo_mgdIdleM[i];
        MtrM_MainMgdDcdInfo.mgdConfM = UtInput_MtrM_MainMgdDcdInfo_mgdConfM[i];
        MtrM_MainMgdDcdInfo.mgdConfLock = UtInput_MtrM_MainMgdDcdInfo_mgdConfLock[i];
        MtrM_MainMgdDcdInfo.mgdSelfTestM = UtInput_MtrM_MainMgdDcdInfo_mgdSelfTestM[i];
        MtrM_MainMgdDcdInfo.mgdSoffM = UtInput_MtrM_MainMgdDcdInfo_mgdSoffM[i];
        MtrM_MainMgdDcdInfo.mgdErrM = UtInput_MtrM_MainMgdDcdInfo_mgdErrM[i];
        MtrM_MainMgdDcdInfo.mgdRectM = UtInput_MtrM_MainMgdDcdInfo_mgdRectM[i];
        MtrM_MainMgdDcdInfo.mgdNormM = UtInput_MtrM_MainMgdDcdInfo_mgdNormM[i];
        MtrM_MainMgdDcdInfo.mgdOsf = UtInput_MtrM_MainMgdDcdInfo_mgdOsf[i];
        MtrM_MainMgdDcdInfo.mgdOp = UtInput_MtrM_MainMgdDcdInfo_mgdOp[i];
        MtrM_MainMgdDcdInfo.mgdScd = UtInput_MtrM_MainMgdDcdInfo_mgdScd[i];
        MtrM_MainMgdDcdInfo.mgdSd = UtInput_MtrM_MainMgdDcdInfo_mgdSd[i];
        MtrM_MainMgdDcdInfo.mgdIndiag = UtInput_MtrM_MainMgdDcdInfo_mgdIndiag[i];
        MtrM_MainMgdDcdInfo.mgdOutp = UtInput_MtrM_MainMgdDcdInfo_mgdOutp[i];
        MtrM_MainMgdDcdInfo.mgdExt = UtInput_MtrM_MainMgdDcdInfo_mgdExt[i];
        MtrM_MainMgdDcdInfo.mgdInt12 = UtInput_MtrM_MainMgdDcdInfo_mgdInt12[i];
        MtrM_MainMgdDcdInfo.mgdRom = UtInput_MtrM_MainMgdDcdInfo_mgdRom[i];
        MtrM_MainMgdDcdInfo.mgdLimpOn = UtInput_MtrM_MainMgdDcdInfo_mgdLimpOn[i];
        MtrM_MainMgdDcdInfo.mgdStIncomplete = UtInput_MtrM_MainMgdDcdInfo_mgdStIncomplete[i];
        MtrM_MainMgdDcdInfo.mgdApcAct = UtInput_MtrM_MainMgdDcdInfo_mgdApcAct[i];
        MtrM_MainMgdDcdInfo.mgdGtm = UtInput_MtrM_MainMgdDcdInfo_mgdGtm[i];
        MtrM_MainMgdDcdInfo.mgdCtrlRegInvalid = UtInput_MtrM_MainMgdDcdInfo_mgdCtrlRegInvalid[i];
        MtrM_MainMgdDcdInfo.mgdLfw = UtInput_MtrM_MainMgdDcdInfo_mgdLfw[i];
        MtrM_MainMgdDcdInfo.mgdErrOtW = UtInput_MtrM_MainMgdDcdInfo_mgdErrOtW[i];
        MtrM_MainMgdDcdInfo.mgdErrOvReg1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvReg1[i];
        MtrM_MainMgdDcdInfo.mgdErrUvVccRom = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVccRom[i];
        MtrM_MainMgdDcdInfo.mgdErrUvReg4 = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvReg4[i];
        MtrM_MainMgdDcdInfo.mgdErrOvReg6 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvReg6[i];
        MtrM_MainMgdDcdInfo.mgdErrUvReg6 = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvReg6[i];
        MtrM_MainMgdDcdInfo.mgdErrUvReg5 = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvReg5[i];
        MtrM_MainMgdDcdInfo.mgdErrUvCb = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvCb[i];
        MtrM_MainMgdDcdInfo.mgdErrClkTrim = UtInput_MtrM_MainMgdDcdInfo_mgdErrClkTrim[i];
        MtrM_MainMgdDcdInfo.mgdErrUvBs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvBs3[i];
        MtrM_MainMgdDcdInfo.mgdErrUvBs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvBs2[i];
        MtrM_MainMgdDcdInfo.mgdErrUvBs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvBs1[i];
        MtrM_MainMgdDcdInfo.mgdErrCp2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrCp2[i];
        MtrM_MainMgdDcdInfo.mgdErrCp1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrCp1[i];
        MtrM_MainMgdDcdInfo.mgdErrOvBs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvBs3[i];
        MtrM_MainMgdDcdInfo.mgdErrOvBs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvBs2[i];
        MtrM_MainMgdDcdInfo.mgdErrOvBs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvBs1[i];
        MtrM_MainMgdDcdInfo.mgdErrOvVdh = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvVdh[i];
        MtrM_MainMgdDcdInfo.mgdErrUvVdh = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVdh[i];
        MtrM_MainMgdDcdInfo.mgdErrOvVs = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvVs[i];
        MtrM_MainMgdDcdInfo.mgdErrUvVs = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVs[i];
        MtrM_MainMgdDcdInfo.mgdErrUvVcc = UtInput_MtrM_MainMgdDcdInfo_mgdErrUvVcc[i];
        MtrM_MainMgdDcdInfo.mgdErrOvVcc = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvVcc[i];
        MtrM_MainMgdDcdInfo.mgdErrOvLdVdh = UtInput_MtrM_MainMgdDcdInfo_mgdErrOvLdVdh[i];
        MtrM_MainMgdDcdInfo.mgdSdDdpStuck = UtInput_MtrM_MainMgdDcdInfo_mgdSdDdpStuck[i];
        MtrM_MainMgdDcdInfo.mgdSdCp1 = UtInput_MtrM_MainMgdDcdInfo_mgdSdCp1[i];
        MtrM_MainMgdDcdInfo.mgdSdOvCp = UtInput_MtrM_MainMgdDcdInfo_mgdSdOvCp[i];
        MtrM_MainMgdDcdInfo.mgdSdClkfail = UtInput_MtrM_MainMgdDcdInfo_mgdSdClkfail[i];
        MtrM_MainMgdDcdInfo.mgdSdUvCb = UtInput_MtrM_MainMgdDcdInfo_mgdSdUvCb[i];
        MtrM_MainMgdDcdInfo.mgdSdOvVdh = UtInput_MtrM_MainMgdDcdInfo_mgdSdOvVdh[i];
        MtrM_MainMgdDcdInfo.mgdSdOvVs = UtInput_MtrM_MainMgdDcdInfo_mgdSdOvVs[i];
        MtrM_MainMgdDcdInfo.mgdSdOt = UtInput_MtrM_MainMgdDcdInfo_mgdSdOt[i];
        MtrM_MainMgdDcdInfo.mgdErrScdLs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrScdLs3[i];
        MtrM_MainMgdDcdInfo.mgdErrScdLs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrScdLs2[i];
        MtrM_MainMgdDcdInfo.mgdErrScdLs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrScdLs1[i];
        MtrM_MainMgdDcdInfo.mgdErrScdHs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrScdHs3[i];
        MtrM_MainMgdDcdInfo.mgdErrScdHs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrScdHs2[i];
        MtrM_MainMgdDcdInfo.mgdErrScdHs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrScdHs1[i];
        MtrM_MainMgdDcdInfo.mgdErrIndLs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrIndLs3[i];
        MtrM_MainMgdDcdInfo.mgdErrIndLs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrIndLs2[i];
        MtrM_MainMgdDcdInfo.mgdErrIndLs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrIndLs1[i];
        MtrM_MainMgdDcdInfo.mgdErrIndHs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrIndHs3[i];
        MtrM_MainMgdDcdInfo.mgdErrIndHs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrIndHs2[i];
        MtrM_MainMgdDcdInfo.mgdErrIndHs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrIndHs1[i];
        MtrM_MainMgdDcdInfo.mgdErrOsfLs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfLs1[i];
        MtrM_MainMgdDcdInfo.mgdErrOsfLs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfLs2[i];
        MtrM_MainMgdDcdInfo.mgdErrOsfLs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfLs3[i];
        MtrM_MainMgdDcdInfo.mgdErrOsfHs1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfHs1[i];
        MtrM_MainMgdDcdInfo.mgdErrOsfHs2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfHs2[i];
        MtrM_MainMgdDcdInfo.mgdErrOsfHs3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOsfHs3[i];
        MtrM_MainMgdDcdInfo.mgdErrSpiFrame = UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiFrame[i];
        MtrM_MainMgdDcdInfo.mgdErrSpiTo = UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiTo[i];
        MtrM_MainMgdDcdInfo.mgdErrSpiWd = UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiWd[i];
        MtrM_MainMgdDcdInfo.mgdErrSpiCrc = UtInput_MtrM_MainMgdDcdInfo_mgdErrSpiCrc[i];
        MtrM_MainMgdDcdInfo.mgdEpiAddInvalid = UtInput_MtrM_MainMgdDcdInfo_mgdEpiAddInvalid[i];
        MtrM_MainMgdDcdInfo.mgdEonfTo = UtInput_MtrM_MainMgdDcdInfo_mgdEonfTo[i];
        MtrM_MainMgdDcdInfo.mgdEonfSigInvalid = UtInput_MtrM_MainMgdDcdInfo_mgdEonfSigInvalid[i];
        MtrM_MainMgdDcdInfo.mgdErrOcOp1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOcOp1[i];
        MtrM_MainMgdDcdInfo.mgdErrOp1Uv = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp1Uv[i];
        MtrM_MainMgdDcdInfo.mgdErrOp1Ov = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp1Ov[i];
        MtrM_MainMgdDcdInfo.mgdErrOp1Calib = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp1Calib[i];
        MtrM_MainMgdDcdInfo.mgdErrOcOp2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOcOp2[i];
        MtrM_MainMgdDcdInfo.mgdErrOp2Uv = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp2Uv[i];
        MtrM_MainMgdDcdInfo.mgdErrOp2Ov = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp2Ov[i];
        MtrM_MainMgdDcdInfo.mgdErrOp2Calib = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp2Calib[i];
        MtrM_MainMgdDcdInfo.mgdErrOcOp3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOcOp3[i];
        MtrM_MainMgdDcdInfo.mgdErrOp3Uv = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp3Uv[i];
        MtrM_MainMgdDcdInfo.mgdErrOp3Ov = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp3Ov[i];
        MtrM_MainMgdDcdInfo.mgdErrOp3Calib = UtInput_MtrM_MainMgdDcdInfo_mgdErrOp3Calib[i];
        MtrM_MainMgdDcdInfo.mgdErrOutpErrn = UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpErrn[i];
        MtrM_MainMgdDcdInfo.mgdErrOutpMiso = UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpMiso[i];
        MtrM_MainMgdDcdInfo.mgdErrOutpPFB1 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpPFB1[i];
        MtrM_MainMgdDcdInfo.mgdErrOutpPFB2 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpPFB2[i];
        MtrM_MainMgdDcdInfo.mgdErrOutpPFB3 = UtInput_MtrM_MainMgdDcdInfo_mgdErrOutpPFB3[i];
        MtrM_MainMgdDcdInfo.mgdTle9180ErrPort = UtInput_MtrM_MainMgdDcdInfo_mgdTle9180ErrPort[i];
        MtrM_MainMotDqIRefMccInfo.IdRef = UtInput_MtrM_MainMotDqIRefMccInfo_IdRef[i];
        MtrM_MainMotDqIRefMccInfo.IqRef = UtInput_MtrM_MainMotDqIRefMccInfo_IqRef[i];
        MtrM_MainHwTrigMotInfo.Uphase0Mon = UtInput_MtrM_MainHwTrigMotInfo_Uphase0Mon[i];
        MtrM_MainHwTrigMotInfo.Uphase1Mon = UtInput_MtrM_MainHwTrigMotInfo_Uphase1Mon[i];
        MtrM_MainHwTrigMotInfo.Vphase0Mon = UtInput_MtrM_MainHwTrigMotInfo_Vphase0Mon[i];
        MtrM_MainHwTrigMotInfo.VPhase1Mon = UtInput_MtrM_MainHwTrigMotInfo_VPhase1Mon[i];
        MtrM_MainMotCurrMonInfo.MotCurrPhUMon0 = UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhUMon0[i];
        MtrM_MainMotCurrMonInfo.MotCurrPhUMon1 = UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhUMon1[i];
        MtrM_MainMotCurrMonInfo.MotCurrPhVMon0 = UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhVMon0[i];
        MtrM_MainMotCurrMonInfo.MotCurrPhVMon1 = UtInput_MtrM_MainMotCurrMonInfo_MotCurrPhVMon1[i];
        MtrM_MainMotAngleMonInfo.MotPosiAngle1deg = UtInput_MtrM_MainMotAngleMonInfo_MotPosiAngle1deg[i];
        MtrM_MainMotAngleMonInfo.MotPosiAngle2deg = UtInput_MtrM_MainMotAngleMonInfo_MotPosiAngle2deg[i];
        MtrM_MainMtrProcessOutInfo.MotCurrPhUMeasd = UtInput_MtrM_MainMtrProcessOutInfo_MotCurrPhUMeasd[i];
        MtrM_MainMtrProcessOutInfo.MotCurrPhVMeasd = UtInput_MtrM_MainMtrProcessOutInfo_MotCurrPhVMeasd[i];
        MtrM_MainMtrProcessOutInfo.MotCurrPhWMeasd = UtInput_MtrM_MainMtrProcessOutInfo_MotCurrPhWMeasd[i];
        MtrM_MainMtrProcessOutInfo.MotPosiAngle1_1_100Deg = UtInput_MtrM_MainMtrProcessOutInfo_MotPosiAngle1_1_100Deg[i];
        MtrM_MainMtrProcessOutInfo.MotPosiAngle2_1_100Deg = UtInput_MtrM_MainMtrProcessOutInfo_MotPosiAngle2_1_100Deg[i];
        MtrM_MainEcuModeSts = UtInput_MtrM_MainEcuModeSts[i];
        MtrM_MainIgnOnOffSts = UtInput_MtrM_MainIgnOnOffSts[i];
        MtrM_MainIgnEdgeSts = UtInput_MtrM_MainIgnEdgeSts[i];
        MtrM_MainVBatt1Mon = UtInput_MtrM_MainVBatt1Mon[i];
        MtrM_MainVBatt2Mon = UtInput_MtrM_MainVBatt2Mon[i];
        MtrM_MainDiagClrSrs = UtInput_MtrM_MainDiagClrSrs[i];
        MtrM_MainMgdInvalid = UtInput_MtrM_MainMgdInvalid[i];
        MtrM_MainMtrArbDriveState = UtInput_MtrM_MainMtrArbDriveState[i];

        MtrM_Main();

        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Power_Open_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Power_Open_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Open_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Open_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Phase_U_OverCurret_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Phase_U_OverCurret_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Phase_V_OverCurret_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Phase_V_OverCurret_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Phase_W_OverCurret_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Phase_W_OverCurret_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Calibration_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Calibration_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Short_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Short_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRMOutInfo.MTRM_Driver_Err, UtExpected_MtrM_MainMTRMOutInfo_MTRM_Driver_Err[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdInterPwrSuppMonErr, UtExpected_MtrM_MainMgdErrInfo_MgdInterPwrSuppMonErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdClkMonErr, UtExpected_MtrM_MainMgdErrInfo_MgdClkMonErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdBISTErr, UtExpected_MtrM_MainMgdErrInfo_MgdBISTErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdFlashMemoryErr, UtExpected_MtrM_MainMgdErrInfo_MgdFlashMemoryErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdRAMErr, UtExpected_MtrM_MainMgdErrInfo_MgdRAMErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdConfigRegiErr, UtExpected_MtrM_MainMgdErrInfo_MgdConfigRegiErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdInputPattMonErr, UtExpected_MtrM_MainMgdErrInfo_MgdInputPattMonErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdOverTempErr, UtExpected_MtrM_MainMgdErrInfo_MgdOverTempErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdCPmpVMonErr, UtExpected_MtrM_MainMgdErrInfo_MgdCPmpVMonErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdHBuffCapVErr, UtExpected_MtrM_MainMgdErrInfo_MgdHBuffCapVErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdInOutPlauErr, UtExpected_MtrM_MainMgdErrInfo_MgdInOutPlauErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMgdErrInfo.MgdCtrlSigMonErr, UtExpected_MtrM_MainMgdErrInfo_MgdCtrlSigMonErr[i]);
        TEST_ASSERT_EQUAL(MtrM_MainMTRInitEndFlg, UtExpected_MtrM_MainMTRInitEndFlg[i]);
    }
}

TEST_GROUP_RUNNER(MtrM_Main)
{
    RUN_TEST_CASE(MtrM_Main, All);
}
