MtrM_MainMotMonInfo = Simulink.Bus;
DeList={
    'MotPwrVoltMon'
    };
MtrM_MainMotMonInfo = CreateBus(MtrM_MainMotMonInfo, DeList);
clear DeList;

MtrM_MainMotVoltsMonInfo = Simulink.Bus;
DeList={
    'MotVoltPhUMon'
    'MotVoltPhVMon'
    'MotVoltPhWMon'
    };
MtrM_MainMotVoltsMonInfo = CreateBus(MtrM_MainMotVoltsMonInfo, DeList);
clear DeList;

MtrM_MainMpsD1SpiDcdInfo = Simulink.Bus;
DeList={
    'S_MAGOL'
    };
MtrM_MainMpsD1SpiDcdInfo = CreateBus(MtrM_MainMpsD1SpiDcdInfo, DeList);
clear DeList;

MtrM_MainMpsD2SpiDcdInfo = Simulink.Bus;
DeList={
    'S_MAGOL'
    };
MtrM_MainMpsD2SpiDcdInfo = CreateBus(MtrM_MainMpsD2SpiDcdInfo, DeList);
clear DeList;

MtrM_MainMgdDcdInfo = Simulink.Bus;
DeList={
    'mgdIdleM'
    'mgdConfM'
    'mgdConfLock'
    'mgdSelfTestM'
    'mgdSoffM'
    'mgdErrM'
    'mgdRectM'
    'mgdNormM'
    'mgdOsf'
    'mgdOp'
    'mgdScd'
    'mgdSd'
    'mgdIndiag'
    'mgdOutp'
    'mgdExt'
    'mgdInt12'
    'mgdRom'
    'mgdLimpOn'
    'mgdStIncomplete'
    'mgdApcAct'
    'mgdGtm'
    'mgdCtrlRegInvalid'
    'mgdLfw'
    'mgdErrOtW'
    'mgdErrOvReg1'
    'mgdErrUvVccRom'
    'mgdErrUvReg4'
    'mgdErrOvReg6'
    'mgdErrUvReg6'
    'mgdErrUvReg5'
    'mgdErrUvCb'
    'mgdErrClkTrim'
    'mgdErrUvBs3'
    'mgdErrUvBs2'
    'mgdErrUvBs1'
    'mgdErrCp2'
    'mgdErrCp1'
    'mgdErrOvBs3'
    'mgdErrOvBs2'
    'mgdErrOvBs1'
    'mgdErrOvVdh'
    'mgdErrUvVdh'
    'mgdErrOvVs'
    'mgdErrUvVs'
    'mgdErrUvVcc'
    'mgdErrOvVcc'
    'mgdErrOvLdVdh'
    'mgdSdDdpStuck'
    'mgdSdCp1'
    'mgdSdOvCp'
    'mgdSdClkfail'
    'mgdSdUvCb'
    'mgdSdOvVdh'
    'mgdSdOvVs'
    'mgdSdOt'
    'mgdErrScdLs3'
    'mgdErrScdLs2'
    'mgdErrScdLs1'
    'mgdErrScdHs3'
    'mgdErrScdHs2'
    'mgdErrScdHs1'
    'mgdErrIndLs3'
    'mgdErrIndLs2'
    'mgdErrIndLs1'
    'mgdErrIndHs3'
    'mgdErrIndHs2'
    'mgdErrIndHs1'
    'mgdErrOsfLs1'
    'mgdErrOsfLs2'
    'mgdErrOsfLs3'
    'mgdErrOsfHs1'
    'mgdErrOsfHs2'
    'mgdErrOsfHs3'
    'mgdErrSpiFrame'
    'mgdErrSpiTo'
    'mgdErrSpiWd'
    'mgdErrSpiCrc'
    'mgdEpiAddInvalid'
    'mgdEonfTo'
    'mgdEonfSigInvalid'
    'mgdErrOcOp1'
    'mgdErrOp1Uv'
    'mgdErrOp1Ov'
    'mgdErrOp1Calib'
    'mgdErrOcOp2'
    'mgdErrOp2Uv'
    'mgdErrOp2Ov'
    'mgdErrOp2Calib'
    'mgdErrOcOp3'
    'mgdErrOp3Uv'
    'mgdErrOp3Ov'
    'mgdErrOp3Calib'
    'mgdErrOutpErrn'
    'mgdErrOutpMiso'
    'mgdErrOutpPFB1'
    'mgdErrOutpPFB2'
    'mgdErrOutpPFB3'
    'mgdTle9180ErrPort'
    };
MtrM_MainMgdDcdInfo = CreateBus(MtrM_MainMgdDcdInfo, DeList);
clear DeList;

MtrM_MainMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
MtrM_MainMotDqIRefMccInfo = CreateBus(MtrM_MainMotDqIRefMccInfo, DeList);
clear DeList;

MtrM_MainHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
MtrM_MainHwTrigMotInfo = CreateBus(MtrM_MainHwTrigMotInfo, DeList);
clear DeList;

MtrM_MainMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
MtrM_MainMotCurrMonInfo = CreateBus(MtrM_MainMotCurrMonInfo, DeList);
clear DeList;

MtrM_MainMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    };
MtrM_MainMotAngleMonInfo = CreateBus(MtrM_MainMotAngleMonInfo, DeList);
clear DeList;

MtrM_MainMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
MtrM_MainMtrProcessOutInfo = CreateBus(MtrM_MainMtrProcessOutInfo, DeList);
clear DeList;

MtrM_MainEcuModeSts = Simulink.Bus;
DeList={'MtrM_MainEcuModeSts'};
MtrM_MainEcuModeSts = CreateBus(MtrM_MainEcuModeSts, DeList);
clear DeList;

MtrM_MainIgnOnOffSts = Simulink.Bus;
DeList={'MtrM_MainIgnOnOffSts'};
MtrM_MainIgnOnOffSts = CreateBus(MtrM_MainIgnOnOffSts, DeList);
clear DeList;

MtrM_MainIgnEdgeSts = Simulink.Bus;
DeList={'MtrM_MainIgnEdgeSts'};
MtrM_MainIgnEdgeSts = CreateBus(MtrM_MainIgnEdgeSts, DeList);
clear DeList;

MtrM_MainVBatt1Mon = Simulink.Bus;
DeList={'MtrM_MainVBatt1Mon'};
MtrM_MainVBatt1Mon = CreateBus(MtrM_MainVBatt1Mon, DeList);
clear DeList;

MtrM_MainVBatt2Mon = Simulink.Bus;
DeList={'MtrM_MainVBatt2Mon'};
MtrM_MainVBatt2Mon = CreateBus(MtrM_MainVBatt2Mon, DeList);
clear DeList;

MtrM_MainDiagClrSrs = Simulink.Bus;
DeList={'MtrM_MainDiagClrSrs'};
MtrM_MainDiagClrSrs = CreateBus(MtrM_MainDiagClrSrs, DeList);
clear DeList;

MtrM_MainMgdInvalid = Simulink.Bus;
DeList={'MtrM_MainMgdInvalid'};
MtrM_MainMgdInvalid = CreateBus(MtrM_MainMgdInvalid, DeList);
clear DeList;

MtrM_MainMtrArbDriveState = Simulink.Bus;
DeList={'MtrM_MainMtrArbDriveState'};
MtrM_MainMtrArbDriveState = CreateBus(MtrM_MainMtrArbDriveState, DeList);
clear DeList;

MtrM_MainMTRMOutInfo = Simulink.Bus;
DeList={
    'MTRM_Power_Open_Err'
    'MTRM_Open_Err'
    'MTRM_Phase_U_OverCurret_Err'
    'MTRM_Phase_V_OverCurret_Err'
    'MTRM_Phase_W_OverCurret_Err'
    'MTRM_Calibration_Err'
    'MTRM_Short_Err'
    'MTRM_Driver_Err'
    };
MtrM_MainMTRMOutInfo = CreateBus(MtrM_MainMTRMOutInfo, DeList);
clear DeList;

MtrM_MainMgdErrInfo = Simulink.Bus;
DeList={
    'MgdInterPwrSuppMonErr'
    'MgdClkMonErr'
    'MgdBISTErr'
    'MgdFlashMemoryErr'
    'MgdRAMErr'
    'MgdConfigRegiErr'
    'MgdInputPattMonErr'
    'MgdOverTempErr'
    'MgdCPmpVMonErr'
    'MgdHBuffCapVErr'
    'MgdInOutPlauErr'
    'MgdCtrlSigMonErr'
    };
MtrM_MainMgdErrInfo = CreateBus(MtrM_MainMgdErrInfo, DeList);
clear DeList;

MtrM_MainMTRInitEndFlg = Simulink.Bus;
DeList={'MtrM_MainMTRInitEndFlg'};
MtrM_MainMTRInitEndFlg = CreateBus(MtrM_MainMTRInitEndFlg, DeList);
clear DeList;

