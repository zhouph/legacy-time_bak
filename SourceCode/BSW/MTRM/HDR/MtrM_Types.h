/**
 * @defgroup MtrM_Types MtrM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTRM_TYPES_H_
#define MTRM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputSR1msMotMonInfo_t MtrM_MainMotMonInfo;
    Ioc_InputSR1msMotVoltsMonInfo_t MtrM_MainMotVoltsMonInfo;
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t MtrM_MainMpsD1SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t MtrM_MainMpsD2SpiDcdInfo;
    Mgd_TLE9180_HndlrMgdDcdInfo_t MtrM_MainMgdDcdInfo;
    Mcc_1msCtrlMotDqIRefMccInfo_t MtrM_MainMotDqIRefMccInfo;
    AdcIf_Conv50usHwTrigMotInfo_t MtrM_MainHwTrigMotInfo;
    Ioc_InputSR50usMotCurrMonInfo_t MtrM_MainMotCurrMonInfo;
    Ioc_InputSR50usMotAngleMonInfo_t MtrM_MainMotAngleMonInfo;
    Mtr_Processing_SigMtrProcessOutInfo_t MtrM_MainMtrProcessOutInfo;
    Mom_HndlrEcuModeSts_t MtrM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t MtrM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t MtrM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t MtrM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t MtrM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t MtrM_MainDiagClrSrs;
    Mgd_TLE9180_HndlrMgdInvalid_t MtrM_MainMgdInvalid;
    Arbitrator_MtrMtrArbDriveState_t MtrM_MainMtrArbDriveState;

/* Output Data Element */
    MtrM_MainMTRMOutInfo_t MtrM_MainMTRMOutInfo;
    MtrM_MainMgdErrInfo_t MtrM_MainMgdErrInfo;
    MtrM_MainMTRInitTest_t MtrM_MainMTRInitEndFlg;
}MtrM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTRM_TYPES_H_ */
/** @} */
