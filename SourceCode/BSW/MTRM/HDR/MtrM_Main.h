/**
 * @defgroup MtrM_Main MtrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTRM_MAIN_H_
#define MTRM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MtrM_Types.h"
#include "MtrM_Cfg.h"
#include "MtrM_Cal.h"
#include "MtrM_Process.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MTRM_MAIN_MODULE_ID      (0)
 #define MTRM_MAIN_MAJOR_VERSION  (2)
 #define MTRM_MAIN_MINOR_VERSION  (0)
 #define MTRM_MAIN_PATCH_VERSION  (0)
 #define MTRM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern MtrM_Main_HdrBusType MtrM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t MtrM_MainVersionInfo;

/* Input Data Element */
extern Ioc_InputSR1msMotMonInfo_t MtrM_MainMotMonInfo;
extern Ioc_InputSR1msMotVoltsMonInfo_t MtrM_MainMotVoltsMonInfo;
extern Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t MtrM_MainMpsD1SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t MtrM_MainMpsD2SpiDcdInfo;
extern Mgd_TLE9180_HndlrMgdDcdInfo_t MtrM_MainMgdDcdInfo;
extern Mcc_1msCtrlMotDqIRefMccInfo_t MtrM_MainMotDqIRefMccInfo;
extern AdcIf_Conv50usHwTrigMotInfo_t MtrM_MainHwTrigMotInfo;
extern Ioc_InputSR50usMotCurrMonInfo_t MtrM_MainMotCurrMonInfo;
extern Ioc_InputSR50usMotAngleMonInfo_t MtrM_MainMotAngleMonInfo;
extern Mtr_Processing_SigMtrProcessOutInfo_t MtrM_MainMtrProcessOutInfo;
extern Mom_HndlrEcuModeSts_t MtrM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t MtrM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t MtrM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t MtrM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t MtrM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t MtrM_MainDiagClrSrs;
extern Mgd_TLE9180_HndlrMgdInvalid_t MtrM_MainMgdInvalid;
extern Arbitrator_MtrMtrArbDriveState_t MtrM_MainMtrArbDriveState;

/* Output Data Element */
extern MtrM_MainMTRMOutInfo_t MtrM_MainMTRMOutInfo;
extern MtrM_MainMgdErrInfo_t MtrM_MainMgdErrInfo;
extern MtrM_MainMTRInitTest_t MtrM_MainMTRInitEndFlg;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void MtrM_Main_Init(void);
extern void MtrM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MTRM_MAIN_H_ */
/** @} */
