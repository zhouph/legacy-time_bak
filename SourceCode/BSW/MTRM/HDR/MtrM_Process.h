/**
 * @defgroup MtrM_Main MtrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MTRM_PROCESS_H_
#define MTRM_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MtrM_Types.h"
#include "MtrM_Cfg.h"
#include "Common.h"

/*****************************
	Voltage Resolution : 1V  /  1000 
	**************************************/
#define	MTRM_VOLT_0V0		(Saluint16)0
#define   MTRM_VOLT_1V0        (Saluint16)1000
#define	MTRM_VOLT_2V0		(Saluint16)2000
#define   MTRM_VOLT_4V0        (Saluint16)4000
#define   MTRM_VOLT_5V0        (Saluint16)5000
#define	MTRM_VOLT_7V0		(Saluint16)7000
#define	MTRM_VOLT_7V5		(Saluint16)7500
#define	MTRM_VOLT_17V0	(Saluint16)17000
/*******************************
	Current Resolution: 1A / 100
	**************************************/
#define MTRM_PHASE_ERR		(Saluint8)0xAA
#define MTRM_CURRENT_80A	(Saluint32)8000
#define MTRM_CURRENT_100A	(Saluint32)10000
#define MTRM_CURRENT_110A (Saluint32)11000
#define MTRM_CURRENT_120A	(Saluint32)12000
#define MTRM_CURRENT_130A (Saluint32)13000
#define MTRM_CURRENT_140A (Saluint32)14000
#define MTRM_CURRENT_150A (Saluint32)15000
/******************************
	Error Detect Define
	**************************************/
#define 	MTRM_ERRDETECED	(Saluint8)1
#define   MTRM_ERR                  (Saluint8)2

extern void   MtrM_Process(MtrM_Main_HdrBusType *pMtrM_Info);
#endif
