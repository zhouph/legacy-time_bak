# \file
#
# \brief MtrM
#
# This file contains the implementation of the SWC
# module MtrM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

MtrM_CORE_PATH     := $(MANDO_BSW_ROOT)\MtrM
MtrM_CAL_PATH      := $(MtrM_CORE_PATH)\CAL\$(MtrM_VARIANT)
MtrM_SRC_PATH      := $(MtrM_CORE_PATH)\SRC
MtrM_CFG_PATH      := $(MtrM_CORE_PATH)\CFG\$(MtrM_VARIANT)
MtrM_HDR_PATH      := $(MtrM_CORE_PATH)\HDR
MtrM_IFA_PATH      := $(MtrM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    MtrM_CMN_PATH      := $(MtrM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	MtrM_UT_PATH		:= $(MtrM_CORE_PATH)\UT
	MtrM_UNITY_PATH	:= $(MtrM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(MtrM_UT_PATH)
	CC_INCLUDE_PATH		+= $(MtrM_UNITY_PATH)
	MtrM_Main_PATH 	:= MtrM_UT_PATH\MtrM_Main
endif
CC_INCLUDE_PATH    += $(MtrM_CAL_PATH)
CC_INCLUDE_PATH    += $(MtrM_SRC_PATH)
CC_INCLUDE_PATH    += $(MtrM_CFG_PATH)
CC_INCLUDE_PATH    += $(MtrM_HDR_PATH)
CC_INCLUDE_PATH    += $(MtrM_IFA_PATH)
CC_INCLUDE_PATH    += $(MtrM_CMN_PATH)

