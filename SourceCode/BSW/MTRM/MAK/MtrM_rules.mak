# \file
#
# \brief MtrM
#
# This file contains the implementation of the SWC
# module MtrM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += MtrM_src

MtrM_src_FILES        += $(MtrM_SRC_PATH)\MtrM_Main.c
MtrM_src_FILES        += $(MtrM_SRC_PATH)\MtrM_Process.c
MtrM_src_FILES        += $(MtrM_IFA_PATH)\MtrM_Main_Ifa.c
MtrM_src_FILES        += $(MtrM_CFG_PATH)\MtrM_Cfg.c
MtrM_src_FILES        += $(MtrM_CAL_PATH)\MtrM_Cal.c

ifeq ($(ICE_COMPILE),true)
MtrM_src_FILES        += $(MtrM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	MtrM_src_FILES        += $(MtrM_UNITY_PATH)\unity.c
	MtrM_src_FILES        += $(MtrM_UNITY_PATH)\unity_fixture.c	
	MtrM_src_FILES        += $(MtrM_UT_PATH)\main.c
	MtrM_src_FILES        += $(MtrM_UT_PATH)\MtrM_Main\MtrM_Main_UtMain.c
endif