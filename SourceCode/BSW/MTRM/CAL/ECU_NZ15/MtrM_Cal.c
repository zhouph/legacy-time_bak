/**
 * @defgroup MtrM_Cal MtrM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MtrM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "MtrM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MTRM_START_SEC_CALIB_UNSPECIFIED
#include "MtrM_MemMap.h"
/* Global Calibration Section */


#define MTRM_STOP_SEC_CALIB_UNSPECIFIED
#include "MtrM_MemMap.h"

#define MTRM_START_SEC_CONST_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MTRM_STOP_SEC_CONST_UNSPECIFIED
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTRM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTRM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_START_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTRM_STOP_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
#define MTRM_START_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTRM_STOP_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_START_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (32BIT)**/


#define MTRM_STOP_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MTRM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MTRM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_START_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MTRM_STOP_SEC_VAR_NOINIT_32BIT
#include "MtrM_MemMap.h"
#define MTRM_START_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MTRM_STOP_SEC_VAR_UNSPECIFIED
#include "MtrM_MemMap.h"
#define MTRM_START_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/** Variable Section (32BIT)**/


#define MTRM_STOP_SEC_VAR_32BIT
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MTRM_START_SEC_CODE
#include "MtrM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MTRM_STOP_SEC_CODE
#include "MtrM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
