/**
 * @defgroup RlyM_Cal RlyM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RlyM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "RlyM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RLYM_START_SEC_CALIB_UNSPECIFIED
#include "RlyM_MemMap.h"
/* Global Calibration Section */


#define RLYM_STOP_SEC_CALIB_UNSPECIFIED
#include "RlyM_MemMap.h"

#define RLYM_START_SEC_CONST_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RLYM_STOP_SEC_CONST_UNSPECIFIED
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLYM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RLYM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_START_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLYM_STOP_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
#define RLYM_START_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLYM_STOP_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_START_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (32BIT)**/


#define RLYM_STOP_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLYM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RLYM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_START_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLYM_STOP_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
#define RLYM_START_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLYM_STOP_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_START_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (32BIT)**/


#define RLYM_STOP_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RLYM_START_SEC_CODE
#include "RlyM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define RLYM_STOP_SEC_CODE
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
