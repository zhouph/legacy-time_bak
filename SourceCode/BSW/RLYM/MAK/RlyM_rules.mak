# \file
#
# \brief RlyM
#
# This file contains the implementation of the SWC
# module RlyM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += RlyM_src

RlyM_src_FILES        += $(RlyM_SRC_PATH)\RlyM_Main.c
RlyM_src_FILES        += $(RlyM_SRC_PATH)\RlyM_Process.c
RlyM_src_FILES        += $(RlyM_IFA_PATH)\RlyM_Main_Ifa.c
RlyM_src_FILES        += $(RlyM_CFG_PATH)\RlyM_Cfg.c
RlyM_src_FILES        += $(RlyM_CAL_PATH)\RlyM_Cal.c

ifeq ($(ICE_COMPILE),true)
RlyM_src_FILES        += $(RlyM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	RlyM_src_FILES        += $(RlyM_UNITY_PATH)\unity.c
	RlyM_src_FILES        += $(RlyM_UNITY_PATH)\unity_fixture.c	
	RlyM_src_FILES        += $(RlyM_UT_PATH)\main.c
	RlyM_src_FILES        += $(RlyM_UT_PATH)\RlyM_Main\RlyM_Main_UtMain.c
endif