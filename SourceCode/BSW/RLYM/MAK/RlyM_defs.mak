# \file
#
# \brief RlyM
#
# This file contains the implementation of the SWC
# module RlyM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

RlyM_CORE_PATH     := $(MANDO_BSW_ROOT)\RlyM
RlyM_CAL_PATH      := $(RlyM_CORE_PATH)\CAL\$(RlyM_VARIANT)
RlyM_SRC_PATH      := $(RlyM_CORE_PATH)\SRC
RlyM_CFG_PATH      := $(RlyM_CORE_PATH)\CFG\$(RlyM_VARIANT)
RlyM_HDR_PATH      := $(RlyM_CORE_PATH)\HDR
RlyM_IFA_PATH      := $(RlyM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    RlyM_CMN_PATH      := $(RlyM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	RlyM_UT_PATH		:= $(RlyM_CORE_PATH)\UT
	RlyM_UNITY_PATH	:= $(RlyM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(RlyM_UT_PATH)
	CC_INCLUDE_PATH		+= $(RlyM_UNITY_PATH)
	RlyM_Main_PATH 	:= RlyM_UT_PATH\RlyM_Main
endif
CC_INCLUDE_PATH    += $(RlyM_CAL_PATH)
CC_INCLUDE_PATH    += $(RlyM_SRC_PATH)
CC_INCLUDE_PATH    += $(RlyM_CFG_PATH)
CC_INCLUDE_PATH    += $(RlyM_HDR_PATH)
CC_INCLUDE_PATH    += $(RlyM_IFA_PATH)
CC_INCLUDE_PATH    += $(RlyM_CMN_PATH)

