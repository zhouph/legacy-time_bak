#include "unity.h"
#include "unity_fixture.h"
#include "RlyM_Main.h"
#include "RlyM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_RlyM_MainRlyDrvInfo_RlyDbcDrv[MAX_STEP] = RLYM_MAINRLYDRVINFO_RLYDBCDRV;
const Saluint8 UtInput_RlyM_MainRlyDrvInfo_RlyEssDrv[MAX_STEP] = RLYM_MAINRLYDRVINFO_RLYESSDRV;
const Haluint8 UtInput_RlyM_MainRlyMonInfo_RlyDbcMon[MAX_STEP] = RLYM_MAINRLYMONINFO_RLYDBCMON;
const Haluint8 UtInput_RlyM_MainRlyMonInfo_RlyEssMon[MAX_STEP] = RLYM_MAINRLYMONINFO_RLYESSMON;
const Haluint8 UtInput_RlyM_MainRlyMonInfo_RlyFault[MAX_STEP] = RLYM_MAINRLYMONINFO_RLYFAULT;
const Mom_HndlrEcuModeSts_t UtInput_RlyM_MainEcuModeSts[MAX_STEP] = RLYM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_RlyM_MainIgnOnOffSts[MAX_STEP] = RLYM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_RlyM_MainIgnEdgeSts[MAX_STEP] = RLYM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_RlyM_MainVBatt1Mon[MAX_STEP] = RLYM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_RlyM_MainDiagClrSrs[MAX_STEP] = RLYM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err[MAX_STEP] = RLYM_MAINRELAYMONITORDATA_RLYM_HDCRELAY_OPEN_ERR;
const Saluint8 UtExpected_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err[MAX_STEP] = RLYM_MAINRELAYMONITORDATA_RLYM_HDCRELAY_SHORT_ERR;
const Saluint8 UtExpected_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err[MAX_STEP] = RLYM_MAINRELAYMONITORDATA_RLYM_ESSRELAY_OPEN_ERR;
const Saluint8 UtExpected_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err[MAX_STEP] = RLYM_MAINRELAYMONITORDATA_RLYM_ESSRELAY_SHORT_ERR;



TEST_GROUP(RlyM_Main);
TEST_SETUP(RlyM_Main)
{
    RlyM_Main_Init();
}

TEST_TEAR_DOWN(RlyM_Main)
{   /* Postcondition */

}

TEST(RlyM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        RlyM_MainRlyDrvInfo.RlyDbcDrv = UtInput_RlyM_MainRlyDrvInfo_RlyDbcDrv[i];
        RlyM_MainRlyDrvInfo.RlyEssDrv = UtInput_RlyM_MainRlyDrvInfo_RlyEssDrv[i];
        RlyM_MainRlyMonInfo.RlyDbcMon = UtInput_RlyM_MainRlyMonInfo_RlyDbcMon[i];
        RlyM_MainRlyMonInfo.RlyEssMon = UtInput_RlyM_MainRlyMonInfo_RlyEssMon[i];
        RlyM_MainRlyMonInfo.RlyFault = UtInput_RlyM_MainRlyMonInfo_RlyFault[i];
        RlyM_MainEcuModeSts = UtInput_RlyM_MainEcuModeSts[i];
        RlyM_MainIgnOnOffSts = UtInput_RlyM_MainIgnOnOffSts[i];
        RlyM_MainIgnEdgeSts = UtInput_RlyM_MainIgnEdgeSts[i];
        RlyM_MainVBatt1Mon = UtInput_RlyM_MainVBatt1Mon[i];
        RlyM_MainDiagClrSrs = UtInput_RlyM_MainDiagClrSrs[i];

        RlyM_Main();

        TEST_ASSERT_EQUAL(RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err, UtExpected_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err[i]);
        TEST_ASSERT_EQUAL(RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err, UtExpected_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err[i]);
        TEST_ASSERT_EQUAL(RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err, UtExpected_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err[i]);
        TEST_ASSERT_EQUAL(RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err, UtExpected_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err[i]);
    }
}

TEST_GROUP_RUNNER(RlyM_Main)
{
    RUN_TEST_CASE(RlyM_Main, All);
}
