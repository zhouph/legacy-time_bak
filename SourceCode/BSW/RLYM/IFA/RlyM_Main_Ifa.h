/**
 * @defgroup RlyM_Main_Ifa RlyM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RlyM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RLYM_MAIN_IFA_H_
#define RLYM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define RlyM_Main_Read_RlyM_MainRlyDrvInfo(data) do \
{ \
    *data = RlyM_MainRlyDrvInfo; \
}while(0);

#define RlyM_Main_Read_RlyM_MainRlyMonInfo(data) do \
{ \
    *data = RlyM_MainRlyMonInfo; \
}while(0);

#define RlyM_Main_Read_RlyM_MainRlyDrvInfo_RlyDbcDrv(data) do \
{ \
    *data = RlyM_MainRlyDrvInfo.RlyDbcDrv; \
}while(0);

#define RlyM_Main_Read_RlyM_MainRlyDrvInfo_RlyEssDrv(data) do \
{ \
    *data = RlyM_MainRlyDrvInfo.RlyEssDrv; \
}while(0);

#define RlyM_Main_Read_RlyM_MainRlyMonInfo_RlyDbcMon(data) do \
{ \
    *data = RlyM_MainRlyMonInfo.RlyDbcMon; \
}while(0);

#define RlyM_Main_Read_RlyM_MainRlyMonInfo_RlyEssMon(data) do \
{ \
    *data = RlyM_MainRlyMonInfo.RlyEssMon; \
}while(0);

#define RlyM_Main_Read_RlyM_MainRlyMonInfo_RlyFault(data) do \
{ \
    *data = RlyM_MainRlyMonInfo.RlyFault; \
}while(0);

#define RlyM_Main_Read_RlyM_MainEcuModeSts(data) do \
{ \
    *data = RlyM_MainEcuModeSts; \
}while(0);

#define RlyM_Main_Read_RlyM_MainIgnOnOffSts(data) do \
{ \
    *data = RlyM_MainIgnOnOffSts; \
}while(0);

#define RlyM_Main_Read_RlyM_MainIgnEdgeSts(data) do \
{ \
    *data = RlyM_MainIgnEdgeSts; \
}while(0);

#define RlyM_Main_Read_RlyM_MainVBatt1Mon(data) do \
{ \
    *data = RlyM_MainVBatt1Mon; \
}while(0);

#define RlyM_Main_Read_RlyM_MainDiagClrSrs(data) do \
{ \
    *data = RlyM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define RlyM_Main_Write_RlyM_MainRelayMonitorData(data) do \
{ \
    RlyM_MainRelayMonitorData = *data; \
}while(0);

#define RlyM_Main_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Open_Err(data) do \
{ \
    RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = *data; \
}while(0);

#define RlyM_Main_Write_RlyM_MainRelayMonitorData_RlyM_HDCRelay_Short_Err(data) do \
{ \
    RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = *data; \
}while(0);

#define RlyM_Main_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Open_Err(data) do \
{ \
    RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = *data; \
}while(0);

#define RlyM_Main_Write_RlyM_MainRelayMonitorData_RlyM_ESSRelay_Short_Err(data) do \
{ \
    RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RLYM_MAIN_IFA_H_ */
/** @} */
