
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Common.h"
#include "RlyM_Main.h"
#include "RlyM_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/


/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

static RlyMCfg* pRlyM_Cfg;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void RlyM_HDCRelay_Check(void);
static void RlyM_ESSRelay_Check(void);


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void RlyM_Process(void)
{
	pRlyM_Cfg = RlyM_GetCfg();
	if(RlyM_MainBus.RlyM_MainVBatt1Mon < RLYM_7V0 || RlyM_MainBus.RlyM_MainVBatt1Mon > RLYM_17V0)
	{
        RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err   = ERR_INHIBIT;
        RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err  = ERR_INHIBIT; 
        RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err   = ERR_INHIBIT;
        RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err  = ERR_INHIBIT;

	}
	else
	{
		RlyM_HDCRelay_Check();
		RlyM_ESSRelay_Check();
	}	
}



void RlyM_Init(void)
{
	RlyM_InitCfg();

    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err 	= ERR_NONE;
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err 	= ERR_NONE;	
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err 	= ERR_NONE;
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err 	= ERR_NONE;

}




/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void RlyM_HDCRelay_Check(void)
{
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err 	= ERR_NONE;
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err	= ERR_NONE;

	if(pRlyM_Cfg->RlyM_HDCrly_type == RLYM_ANALOG_TYPE)
	{
		if(RlyM_MainBus.RlyM_MainRlyDrvInfo.RlyDbcDrv == RLYM_ON)
		{
			if(RlyM_MainBus.RlyM_MainRlyMonInfo.RlyDbcMon > RLYM_ON_LEVLE)
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = ERR_PREFAILED;		
			}
			else
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = ERR_PREPASSED;		
			}
		}
		else	
		{
			if(RlyM_MainBus.RlyM_MainRlyMonInfo.RlyDbcMon < RLYM_ON_LEVLE)
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = ERR_PREFAILED;		
			}
			else
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = ERR_PREPASSED;		
			}
		}
	}
	else
	{
	    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err   = ERR_INHIBIT;
        RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err  = ERR_INHIBIT;
	}

	/*Relay Monitoring module is inhibited because Relay doesn't connect during 2015 NZ WNT TEST*/
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err 	= ERR_PREPASSED;
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err	= ERR_PREPASSED;
}

static void RlyM_ESSRelay_Check(void)
{
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err	= ERR_NONE;
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err	= ERR_NONE;

	if(pRlyM_Cfg->RlyM_ESSrly_type == RLYM_ANALOG_TYPE)
	{
		if(RlyM_MainBus.RlyM_MainRlyDrvInfo.RlyEssDrv == RLYM_ON)
		{
			if(RlyM_MainBus.RlyM_MainRlyMonInfo.RlyEssMon > RLYM_ON_LEVLE)
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = ERR_PREFAILED;		
			}
			else
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = ERR_PREPASSED;		
			}
		}
		else	
		{
			if(RlyM_MainBus.RlyM_MainRlyMonInfo.RlyEssMon < RLYM_ON_LEVLE)
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = ERR_PREFAILED;		
			}
			else
			{
				RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = ERR_PREPASSED;		
			}
		}
	}
	else
	{
		RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err	= ERR_INHIBIT;
		RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err	= ERR_INHIBIT;		
	}

	/*Relay Monitoring module is inhibited because Relay doesn't connect during 2015 NZ WNT TEST*/
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err	= ERR_PREPASSED;
	RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err	= ERR_PREPASSED;
	
}
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
