/**
 * @defgroup RlyM_Main RlyM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RlyM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "RlyM_Main.h"
#include "RlyM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RLYM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RLYM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLYM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
RlyM_Main_HdrBusType RlyM_MainBus;

/* Version Info */
const SwcVersionInfo_t RlyM_MainVersionInfo = 
{   
    RLYM_MAIN_MODULE_ID,           /* RlyM_MainVersionInfo.ModuleId */
    RLYM_MAIN_MAJOR_VERSION,       /* RlyM_MainVersionInfo.MajorVer */
    RLYM_MAIN_MINOR_VERSION,       /* RlyM_MainVersionInfo.MinorVer */
    RLYM_MAIN_PATCH_VERSION,       /* RlyM_MainVersionInfo.PatchVer */
    RLYM_MAIN_BRANCH_VERSION       /* RlyM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Rly_ActrRlyDrvInfo_t RlyM_MainRlyDrvInfo;
Ioc_InputCS1msRlyMonInfo_t RlyM_MainRlyMonInfo;
Mom_HndlrEcuModeSts_t RlyM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t RlyM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t RlyM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t RlyM_MainVBatt1Mon;
Diag_HndlrDiagClr_t RlyM_MainDiagClrSrs;

/* Output Data Element */
RlyM_MainRelayMonitor_t RlyM_MainRelayMonitorData;

uint32 RlyM_Main_Timer_Start;
uint32 RlyM_Main_Timer_Elapsed;

#define RLYM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLYM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
#define RLYM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLYM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_MAIN_START_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (32BIT)**/


#define RLYM_MAIN_STOP_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RLYM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RLYM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RLYM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "RlyM_MemMap.h"
#define RLYM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RLYM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "RlyM_MemMap.h"
#define RLYM_MAIN_START_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/** Variable Section (32BIT)**/


#define RLYM_MAIN_STOP_SEC_VAR_32BIT
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RLYM_MAIN_START_SEC_CODE
#include "RlyM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void RlyM_Main_Init(void)
{
    /* Initialize internal bus */
    RlyM_MainBus.RlyM_MainRlyDrvInfo.RlyDbcDrv = 0;
    RlyM_MainBus.RlyM_MainRlyDrvInfo.RlyEssDrv = 0;
    RlyM_MainBus.RlyM_MainRlyMonInfo.RlyDbcMon = 0;
    RlyM_MainBus.RlyM_MainRlyMonInfo.RlyEssMon = 0;
    RlyM_MainBus.RlyM_MainRlyMonInfo.RlyFault = 0;
    RlyM_MainBus.RlyM_MainEcuModeSts = 0;
    RlyM_MainBus.RlyM_MainIgnOnOffSts = 0;
    RlyM_MainBus.RlyM_MainIgnEdgeSts = 0;
    RlyM_MainBus.RlyM_MainVBatt1Mon = 0;
    RlyM_MainBus.RlyM_MainDiagClrSrs = 0;
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err = 0;
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err = 0;
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err = 0;
    RlyM_MainBus.RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err = 0;
}

void RlyM_Main(void)
{
    RlyM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    RlyM_Main_Read_RlyM_MainRlyDrvInfo(&RlyM_MainBus.RlyM_MainRlyDrvInfo);
    /*==============================================================================
    * Members of structure RlyM_MainRlyDrvInfo 
     : RlyM_MainRlyDrvInfo.RlyDbcDrv;
     : RlyM_MainRlyDrvInfo.RlyEssDrv;
     =============================================================================*/
    
    RlyM_Main_Read_RlyM_MainRlyMonInfo(&RlyM_MainBus.RlyM_MainRlyMonInfo);
    /*==============================================================================
    * Members of structure RlyM_MainRlyMonInfo 
     : RlyM_MainRlyMonInfo.RlyDbcMon;
     : RlyM_MainRlyMonInfo.RlyEssMon;
     : RlyM_MainRlyMonInfo.RlyFault;
     =============================================================================*/
    
    RlyM_Main_Read_RlyM_MainEcuModeSts(&RlyM_MainBus.RlyM_MainEcuModeSts);
    RlyM_Main_Read_RlyM_MainIgnOnOffSts(&RlyM_MainBus.RlyM_MainIgnOnOffSts);
    RlyM_Main_Read_RlyM_MainIgnEdgeSts(&RlyM_MainBus.RlyM_MainIgnEdgeSts);
    RlyM_Main_Read_RlyM_MainVBatt1Mon(&RlyM_MainBus.RlyM_MainVBatt1Mon);
    RlyM_Main_Read_RlyM_MainDiagClrSrs(&RlyM_MainBus.RlyM_MainDiagClrSrs);

    /* Process */
	if(RlyM_MainBus.RlyM_MainDiagClrSrs == 1)
	{
		RlyM_Main_Init();
	}
	RlyM_Process();

    /* Output */
    RlyM_Main_Write_RlyM_MainRelayMonitorData(&RlyM_MainBus.RlyM_MainRelayMonitorData);
    /*==============================================================================
    * Members of structure RlyM_MainRelayMonitorData 
     : RlyM_MainRelayMonitorData.RlyM_HDCRelay_Open_Err;
     : RlyM_MainRelayMonitorData.RlyM_HDCRelay_Short_Err;
     : RlyM_MainRelayMonitorData.RlyM_ESSRelay_Open_Err;
     : RlyM_MainRelayMonitorData.RlyM_ESSRelay_Short_Err;
     =============================================================================*/
    

    RlyM_Main_Timer_Elapsed = STM0_TIM0.U - RlyM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define RLYM_MAIN_STOP_SEC_CODE
#include "RlyM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
