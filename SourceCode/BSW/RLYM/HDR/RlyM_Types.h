/**
 * @defgroup RlyM_Types RlyM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RlyM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RLYM_TYPES_H_
#define RLYM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Rly_ActrRlyDrvInfo_t RlyM_MainRlyDrvInfo;
    Ioc_InputCS1msRlyMonInfo_t RlyM_MainRlyMonInfo;
    Mom_HndlrEcuModeSts_t RlyM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t RlyM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t RlyM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t RlyM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t RlyM_MainDiagClrSrs;

/* Output Data Element */
    RlyM_MainRelayMonitor_t RlyM_MainRelayMonitorData;
}RlyM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RLYM_TYPES_H_ */
/** @} */
