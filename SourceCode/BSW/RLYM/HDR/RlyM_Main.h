/**
 * @defgroup RlyM_Main RlyM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RlyM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RLYM_MAIN_H_
#define RLYM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "RlyM_Types.h"
#include "RlyM_Cfg.h"
#include "RlyM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define RLYM_MAIN_MODULE_ID      (0)
 #define RLYM_MAIN_MAJOR_VERSION  (2)
 #define RLYM_MAIN_MINOR_VERSION  (0)
 #define RLYM_MAIN_PATCH_VERSION  (0)
 #define RLYM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern RlyM_Main_HdrBusType RlyM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t RlyM_MainVersionInfo;

/* Input Data Element */
extern Rly_ActrRlyDrvInfo_t RlyM_MainRlyDrvInfo;
extern Ioc_InputCS1msRlyMonInfo_t RlyM_MainRlyMonInfo;
extern Mom_HndlrEcuModeSts_t RlyM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t RlyM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t RlyM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t RlyM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t RlyM_MainDiagClrSrs;

/* Output Data Element */
extern RlyM_MainRelayMonitor_t RlyM_MainRelayMonitorData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void RlyM_Main_Init(void);
extern void RlyM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RLYM_MAIN_H_ */
/** @} */
