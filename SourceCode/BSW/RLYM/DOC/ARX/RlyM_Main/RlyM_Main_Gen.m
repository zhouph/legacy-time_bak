RlyM_MainRlyDrvInfo = Simulink.Bus;
DeList={
    'RlyDbcDrv'
    'RlyEssDrv'
    };
RlyM_MainRlyDrvInfo = CreateBus(RlyM_MainRlyDrvInfo, DeList);
clear DeList;

RlyM_MainRlyMonInfo = Simulink.Bus;
DeList={
    'RlyDbcMon'
    'RlyEssMon'
    'RlyFault'
    };
RlyM_MainRlyMonInfo = CreateBus(RlyM_MainRlyMonInfo, DeList);
clear DeList;

RlyM_MainEcuModeSts = Simulink.Bus;
DeList={'RlyM_MainEcuModeSts'};
RlyM_MainEcuModeSts = CreateBus(RlyM_MainEcuModeSts, DeList);
clear DeList;

RlyM_MainIgnOnOffSts = Simulink.Bus;
DeList={'RlyM_MainIgnOnOffSts'};
RlyM_MainIgnOnOffSts = CreateBus(RlyM_MainIgnOnOffSts, DeList);
clear DeList;

RlyM_MainIgnEdgeSts = Simulink.Bus;
DeList={'RlyM_MainIgnEdgeSts'};
RlyM_MainIgnEdgeSts = CreateBus(RlyM_MainIgnEdgeSts, DeList);
clear DeList;

RlyM_MainVBatt1Mon = Simulink.Bus;
DeList={'RlyM_MainVBatt1Mon'};
RlyM_MainVBatt1Mon = CreateBus(RlyM_MainVBatt1Mon, DeList);
clear DeList;

RlyM_MainDiagClrSrs = Simulink.Bus;
DeList={'RlyM_MainDiagClrSrs'};
RlyM_MainDiagClrSrs = CreateBus(RlyM_MainDiagClrSrs, DeList);
clear DeList;

RlyM_MainRelayMonitorData = Simulink.Bus;
DeList={
    'RlyM_HDCRelay_Open_Err'
    'RlyM_HDCRelay_Short_Err'
    'RlyM_ESSRelay_Open_Err'
    'RlyM_ESSRelay_Short_Err'
    };
RlyM_MainRelayMonitorData = CreateBus(RlyM_MainRelayMonitorData, DeList);
clear DeList;

