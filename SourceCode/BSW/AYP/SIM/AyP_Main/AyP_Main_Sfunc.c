#define S_FUNCTION_NAME      AyP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          37
#define WidthOutputPort         5

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "AyP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    AyP_MainEemFailData.Eem_Fail_SenPwr_12V = input[0];
    AyP_MainEemFailData.Eem_Fail_Yaw = input[1];
    AyP_MainEemFailData.Eem_Fail_Ay = input[2];
    AyP_MainEemFailData.Eem_Fail_Str = input[3];
    AyP_MainEemFailData.Eem_Fail_WssFL = input[4];
    AyP_MainEemFailData.Eem_Fail_WssFR = input[5];
    AyP_MainEemFailData.Eem_Fail_WssRL = input[6];
    AyP_MainEemFailData.Eem_Fail_WssRR = input[7];
    AyP_MainCanRxEscInfo.Ay = input[8];
    AyP_MainEscSwtStInfo.EscDisabledBySwt = input[9];
    AyP_MainWhlSpdInfo.FlWhlSpd = input[10];
    AyP_MainWhlSpdInfo.FrWhlSpd = input[11];
    AyP_MainWhlSpdInfo.RlWhlSpd = input[12];
    AyP_MainWhlSpdInfo.RrlWhlSpd = input[13];
    AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = input[14];
    AyP_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[15];
    AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[16];
    AyP_MainEemSuspectData.Eem_Suspect_WssFL = input[17];
    AyP_MainEemSuspectData.Eem_Suspect_WssFR = input[18];
    AyP_MainEemSuspectData.Eem_Suspect_WssRL = input[19];
    AyP_MainEemSuspectData.Eem_Suspect_WssRR = input[20];
    AyP_MainEemSuspectData.Eem_Suspect_FrontWss = input[21];
    AyP_MainEemSuspectData.Eem_Suspect_RearWss = input[22];
    AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = input[23];
    AyP_MainEemEceData.Eem_Ece_Ay = input[24];
    AyP_MainCanMonData.CanM_SubBusOff_Err = input[25];
    AyP_MainIMUCalcInfo.Reverse_Gear_flg = input[26];
    AyP_MainIMUCalcInfo.Reverse_Judge_Time = input[27];
    AyP_MainWssSpeedOut.WssMax = input[28];
    AyP_MainWssSpeedOut.WssMin = input[29];
    AyP_MainWssCalcInfo.Rough_Sus_Flg = input[30];
    AyP_MainEcuModeSts = input[31];
    AyP_MainIgnOnOffSts = input[32];
    AyP_MainIgnEdgeSts = input[33];
    AyP_MainVBatt1Mon = input[34];
    AyP_MainDiagClrSrs = input[35];
    AyP_MainVehSpd = input[36];

    AyP_Main();


    output[0] = AyP_MainAyPlauOutput.AyPlauNoiselErr;
    output[1] = AyP_MainAyPlauOutput.AyPlauModelErr;
    output[2] = AyP_MainAyPlauOutput.AyPlauShockErr;
    output[3] = AyP_MainAyPlauOutput.AyPlauRangeErr;
    output[4] = AyP_MainAyPlauOutput.AyPlauStandStillErr;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    AyP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
