# \file
#
# \brief AyP
#
# This file contains the implementation of the SWC
# module AyP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += AyP_src

AyP_src_FILES        += $(AyP_SRC_PATH)\AyP_Main.c
AyP_src_FILES        += $(AyP_SRC_PATH)\AyP_Process.c
AyP_src_FILES        += $(AyP_IFA_PATH)\AyP_Main_Ifa.c
AyP_src_FILES        += $(AyP_CFG_PATH)\AyP_Cfg.c
AyP_src_FILES        += $(AyP_CAL_PATH)\AyP_Cal.c

ifeq ($(ICE_COMPILE),true)
AyP_src_FILES        += $(AyP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	AyP_src_FILES        += $(AyP_UNITY_PATH)\unity.c
	AyP_src_FILES        += $(AyP_UNITY_PATH)\unity_fixture.c	
	AyP_src_FILES        += $(AyP_UT_PATH)\main.c
	AyP_src_FILES        += $(AyP_UT_PATH)\AyP_Main\AyP_Main_UtMain.c
endif