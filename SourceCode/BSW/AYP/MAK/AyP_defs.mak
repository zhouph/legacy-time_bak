# \file
#
# \brief AyP
#
# This file contains the implementation of the SWC
# module AyP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

AyP_CORE_PATH     := $(MANDO_BSW_ROOT)\AyP
AyP_CAL_PATH      := $(AyP_CORE_PATH)\CAL\$(AyP_VARIANT)
AyP_SRC_PATH      := $(AyP_CORE_PATH)\SRC
AyP_CFG_PATH      := $(AyP_CORE_PATH)\CFG\$(AyP_VARIANT)
AyP_HDR_PATH      := $(AyP_CORE_PATH)\HDR
AyP_IFA_PATH      := $(AyP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    AyP_CMN_PATH      := $(AyP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	AyP_UT_PATH		:= $(AyP_CORE_PATH)\UT
	AyP_UNITY_PATH	:= $(AyP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(AyP_UT_PATH)
	CC_INCLUDE_PATH		+= $(AyP_UNITY_PATH)
	AyP_Main_PATH 	:= AyP_UT_PATH\AyP_Main
endif
CC_INCLUDE_PATH    += $(AyP_CAL_PATH)
CC_INCLUDE_PATH    += $(AyP_SRC_PATH)
CC_INCLUDE_PATH    += $(AyP_CFG_PATH)
CC_INCLUDE_PATH    += $(AyP_HDR_PATH)
CC_INCLUDE_PATH    += $(AyP_IFA_PATH)
CC_INCLUDE_PATH    += $(AyP_CMN_PATH)

