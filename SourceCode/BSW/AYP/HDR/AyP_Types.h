/**
 * @defgroup AyP_Types AyP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYP_TYPES_H_
#define AYP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t AyP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t AyP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t AyP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t AyP_MainWhlSpdInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t AyP_MainBaseBrkCtrlModInfo;
    SenPwrM_MainSenPwrMonitor_t AyP_MainSenPwrMonitorData;
    Eem_MainEemSuspectData_t AyP_MainEemSuspectData;
    Eem_MainEemEceData_t AyP_MainEemEceData;
    CanM_MainCanMonData_t AyP_MainCanMonData;
    Proxy_RxIMUCalc_t AyP_MainIMUCalcInfo;
    Wss_SenWssSpeedOut_t AyP_MainWssSpeedOut;
    Wss_SenWssCalc_t AyP_MainWssCalcInfo;
    Mom_HndlrEcuModeSts_t AyP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AyP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AyP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AyP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t AyP_MainDiagClrSrs;
    Abc_CtrlVehSpd_t AyP_MainVehSpd;

/* Output Data Element */
    AyP_MainAyPlauOutput_t AyP_MainAyPlauOutput;
}AyP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AYP_TYPES_H_ */
/** @} */
