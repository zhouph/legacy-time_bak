/**
 * @defgroup AyP_Main AyP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYP_PROCESS_H_
#define AYP_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyP_Types.h"
#include "AyP_Cfg.h"
#include "Common.h"

#define AY_ALLOW  0
#define AY_INHIBIT 1

#define DEF_2SEC_TIME 200
#define DEF_5SEC_TIME 500

#define DEF_STANDSTILL          1
#define DEF_NON_STANDSTILL 0

#define Lat_0_3G 300
#define Lat_0_7G 700
#define Lat_0_8G 800
#define Lat_1_0G 1000
#define Lat_1_5G 1500
extern void AyP_Process(AyP_Main_HdrBusType *pAyP_Process);
#endif
