/**
 * @defgroup AyP_Main AyP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYP_MAIN_H_
#define AYP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyP_Types.h"
#include "AyP_Cfg.h"
#include "AyP_Cal.h"
#include "AyP_Process.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define AYP_MAIN_MODULE_ID      (0)
 #define AYP_MAIN_MAJOR_VERSION  (2)
 #define AYP_MAIN_MINOR_VERSION  (0)
 #define AYP_MAIN_PATCH_VERSION  (0)
 #define AYP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AyP_Main_HdrBusType AyP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t AyP_MainVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t AyP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t AyP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t AyP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t AyP_MainWhlSpdInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t AyP_MainBaseBrkCtrlModInfo;
extern SenPwrM_MainSenPwrMonitor_t AyP_MainSenPwrMonitorData;
extern Eem_MainEemSuspectData_t AyP_MainEemSuspectData;
extern Eem_MainEemEceData_t AyP_MainEemEceData;
extern CanM_MainCanMonData_t AyP_MainCanMonData;
extern Proxy_RxIMUCalc_t AyP_MainIMUCalcInfo;
extern Wss_SenWssSpeedOut_t AyP_MainWssSpeedOut;
extern Wss_SenWssCalc_t AyP_MainWssCalcInfo;
extern Mom_HndlrEcuModeSts_t AyP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t AyP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t AyP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t AyP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t AyP_MainDiagClrSrs;
extern Abc_CtrlVehSpd_t AyP_MainVehSpd;

/* Output Data Element */
extern AyP_MainAyPlauOutput_t AyP_MainAyPlauOutput;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AyP_Main_Init(void);
extern void AyP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AYP_MAIN_H_ */
/** @} */
