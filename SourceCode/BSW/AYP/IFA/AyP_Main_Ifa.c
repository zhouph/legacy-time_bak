/**
 * @defgroup AyP_Main_Ifa AyP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyP_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AyP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AYP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AyP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/** Variable Section (32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/** Variable Section (32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AYP_MAIN_START_SEC_CODE
#include "AyP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AYP_MAIN_STOP_SEC_CODE
#include "AyP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
