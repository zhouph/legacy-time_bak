/**
 * @defgroup AyP_Main_Ifa AyP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AYP_MAIN_IFA_H_
#define AYP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AyP_Main_Read_AyP_MainEemFailData(data) do \
{ \
    *data = AyP_MainEemFailData; \
}while(0);

#define AyP_Main_Read_AyP_MainCanRxEscInfo(data) do \
{ \
    *data = AyP_MainCanRxEscInfo; \
}while(0);

#define AyP_Main_Read_AyP_MainEscSwtStInfo(data) do \
{ \
    *data = AyP_MainEscSwtStInfo; \
}while(0);

#define AyP_Main_Read_AyP_MainWhlSpdInfo(data) do \
{ \
    *data = AyP_MainWhlSpdInfo; \
}while(0);

#define AyP_Main_Read_AyP_MainBaseBrkCtrlModInfo(data) do \
{ \
    *data = AyP_MainBaseBrkCtrlModInfo; \
}while(0);

#define AyP_Main_Read_AyP_MainSenPwrMonitorData(data) do \
{ \
    *data = AyP_MainSenPwrMonitorData; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData(data) do \
{ \
    *data = AyP_MainEemSuspectData; \
}while(0);

#define AyP_Main_Read_AyP_MainEemEceData(data) do \
{ \
    *data = AyP_MainEemEceData; \
}while(0);

#define AyP_Main_Read_AyP_MainCanMonData(data) do \
{ \
    *data = AyP_MainCanMonData; \
}while(0);

#define AyP_Main_Read_AyP_MainIMUCalcInfo(data) do \
{ \
    *data = AyP_MainIMUCalcInfo; \
}while(0);

#define AyP_Main_Read_AyP_MainWssSpeedOut(data) do \
{ \
    *data = AyP_MainWssSpeedOut; \
}while(0);

#define AyP_Main_Read_AyP_MainWssCalcInfo(data) do \
{ \
    *data = AyP_MainWssCalcInfo; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_Yaw; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_Ay; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_Str; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = AyP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define AyP_Main_Read_AyP_MainCanRxEscInfo_Ay(data) do \
{ \
    *data = AyP_MainCanRxEscInfo.Ay; \
}while(0);

#define AyP_Main_Read_AyP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = AyP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define AyP_Main_Read_AyP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = AyP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define AyP_Main_Read_AyP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = AyP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define AyP_Main_Read_AyP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = AyP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define AyP_Main_Read_AyP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = AyP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define AyP_Main_Read_AyP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define AyP_Main_Read_AyP_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = AyP_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define AyP_Main_Read_AyP_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define AyP_Main_Read_AyP_MainEemEceData_Eem_Ece_Ay(data) do \
{ \
    *data = AyP_MainEemEceData.Eem_Ece_Ay; \
}while(0);

#define AyP_Main_Read_AyP_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    *data = AyP_MainCanMonData.CanM_SubBusOff_Err; \
}while(0);

#define AyP_Main_Read_AyP_MainIMUCalcInfo_Reverse_Gear_flg(data) do \
{ \
    *data = AyP_MainIMUCalcInfo.Reverse_Gear_flg; \
}while(0);

#define AyP_Main_Read_AyP_MainIMUCalcInfo_Reverse_Judge_Time(data) do \
{ \
    *data = AyP_MainIMUCalcInfo.Reverse_Judge_Time; \
}while(0);

#define AyP_Main_Read_AyP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = AyP_MainWssSpeedOut.WssMax; \
}while(0);

#define AyP_Main_Read_AyP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = AyP_MainWssSpeedOut.WssMin; \
}while(0);

#define AyP_Main_Read_AyP_MainWssCalcInfo_Rough_Sus_Flg(data) do \
{ \
    *data = AyP_MainWssCalcInfo.Rough_Sus_Flg; \
}while(0);

#define AyP_Main_Read_AyP_MainEcuModeSts(data) do \
{ \
    *data = AyP_MainEcuModeSts; \
}while(0);

#define AyP_Main_Read_AyP_MainIgnOnOffSts(data) do \
{ \
    *data = AyP_MainIgnOnOffSts; \
}while(0);

#define AyP_Main_Read_AyP_MainIgnEdgeSts(data) do \
{ \
    *data = AyP_MainIgnEdgeSts; \
}while(0);

#define AyP_Main_Read_AyP_MainVBatt1Mon(data) do \
{ \
    *data = AyP_MainVBatt1Mon; \
}while(0);

#define AyP_Main_Read_AyP_MainDiagClrSrs(data) do \
{ \
    *data = AyP_MainDiagClrSrs; \
}while(0);

#define AyP_Main_Read_AyP_MainVehSpd(data) do \
{ \
    *data = AyP_MainVehSpd; \
}while(0);


/* Set Output DE MAcro Function */
#define AyP_Main_Write_AyP_MainAyPlauOutput(data) do \
{ \
    AyP_MainAyPlauOutput = *data; \
}while(0);

#define AyP_Main_Write_AyP_MainAyPlauOutput_AyPlauNoiselErr(data) do \
{ \
    AyP_MainAyPlauOutput.AyPlauNoiselErr = *data; \
}while(0);

#define AyP_Main_Write_AyP_MainAyPlauOutput_AyPlauModelErr(data) do \
{ \
    AyP_MainAyPlauOutput.AyPlauModelErr = *data; \
}while(0);

#define AyP_Main_Write_AyP_MainAyPlauOutput_AyPlauShockErr(data) do \
{ \
    AyP_MainAyPlauOutput.AyPlauShockErr = *data; \
}while(0);

#define AyP_Main_Write_AyP_MainAyPlauOutput_AyPlauRangeErr(data) do \
{ \
    AyP_MainAyPlauOutput.AyPlauRangeErr = *data; \
}while(0);

#define AyP_Main_Write_AyP_MainAyPlauOutput_AyPlauStandStillErr(data) do \
{ \
    AyP_MainAyPlauOutput.AyPlauStandStillErr = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AYP_MAIN_IFA_H_ */
/** @} */
