/**
 * @defgroup AyP_Process AyP_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyP_Main.h"
#include "AyP_Main_Ifa.h"
#include "AyP_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AyP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AYP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AyP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"

extern signed int Proxy_fys16EstimatedAY; /* TODO */

static Saluint8 AyP_Inhibit_Define(AyP_Main_HdrBusType *pAyInhibitDefine);
static Saluint8 AyP_StandStillState(AyP_Main_HdrBusType *pStandStillState);
static Saluint16 AyP_Threhsold_Calc(AyP_Main_HdrBusType *AyThreshold);
static void AyP_Range(AyP_Main_HdrBusType *pAyPRange, boolean inhibit);
static void AyP_StandstillOffsetChk(AyP_Main_HdrBusType *pAyPStandstillChk, boolean inhibit);
static void AyP_NoiseChk(AyP_Main_HdrBusType *pAyPNoiseChk, boolean inhibit);

void AyP_Process(AyP_Main_HdrBusType *pAyP_Process)
{
    boolean Inhibit  = AyP_Inhibit_Define(pAyP_Process);
    
    AyP_Range(pAyP_Process,Inhibit);
    AyP_StandstillOffsetChk(pAyP_Process,Inhibit);
    AyP_NoiseChk(pAyP_Process,Inhibit);
}

static Saluint8 AyP_Inhibit_Define(AyP_Main_HdrBusType *pAyInhibitDefine)
{
    boolean inhibit =AY_ALLOW;
    if((pAyInhibitDefine->AyP_MainSenPwrMonitorData.SenPwrM_12V_Open_Err==1)
        ||(pAyInhibitDefine->AyP_MainSenPwrMonitorData.SenPwrM_12V_Short_Err==1)
        ||(pAyInhibitDefine->AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable==1)
        ||(pAyInhibitDefine->AyP_MainCanMonData.CanM_SubBusOff_Err==1)
        ||(pAyInhibitDefine->AyP_MainEemEceData.Eem_Ece_Ay==1)
        ||(pAyInhibitDefine->AyP_MainEemFailData.Eem_Fail_Ay==1)
     )
    {
        inhibit =AY_INHIBIT;
    }

    return inhibit;
}

/***************
     Ay Range Error 
     **************/
static void AyP_Range(AyP_Main_HdrBusType *pAyPRange, boolean inhibit)
{
    Saluint16 abs_u16EstimatedAy = 0;

    abs_u16EstimatedAy = abs(Proxy_fys16EstimatedAY);

    /* TODO : New Concept
                    if((rev_for_judge_time<=0)&&(fu1CLSTEEROK==1)&&(Temp_Abs_STR_Mdl_0_<U16_STEER_300DEG))
    */
    
    if(abs_u16EstimatedAy >= Lat_1_5G)
    {
        pAyPRange->AyP_MainAyPlauOutput.AyPlauRangeErr = ERR_PREFAILED;
    }
    else
    {
        pAyPRange->AyP_MainAyPlauOutput.AyPlauRangeErr = ERR_PREPASSED;
    }

    if(inhibit==TRUE)
    {
        pAyPRange->AyP_MainAyPlauOutput.AyPlauRangeErr = ERR_INHIBIT;
    }
}

/***************
     Ay Standstill Error 
     **************/
static void AyP_StandstillOffsetChk(AyP_Main_HdrBusType *pAyPStandstillChk, boolean inhibit)
{
    Saluint16 ABS_Ay_1_1000g = 0;
    Saluint8 Ay_Standstill =AyP_StandStillState(pAyPStandstillChk);
    ABS_Ay_1_1000g =(Saluint16)Proxy_fys16EstimatedAY;
    if(Ay_Standstill==DEF_STANDSTILL)
    {
        if (Ay_Standstill >Lat_0_7G)
        {
            pAyPStandstillChk->AyP_MainAyPlauOutput.AyPlauStandStillErr =ERR_PREFAILED;
        }
        else
        {
            pAyPStandstillChk->AyP_MainAyPlauOutput.AyPlauStandStillErr =ERR_PREPASSED;
        }
    }

    if(inhibit==TRUE)
    {
        pAyPStandstillChk->AyP_MainAyPlauOutput.AyPlauStandStillErr= ERR_INHIBIT;
    }
}

static Saluint8 AyP_StandStillState(AyP_Main_HdrBusType *pStandStillState)
{
    Salsint32 Vehicle_Speed = U8_F64SPEED_0KPH25;
    Saluint32 Vehicle_Max_Wheel_Speed =U8_F64SPEED_0KPH25;
    static Saluint16 Standstill_Time =0;
    static Saluint8 StandStill_State =DEF_NON_STANDSTILL;   

    Vehicle_Speed =pStandStillState->AyP_MainVehSpd;
    Vehicle_Max_Wheel_Speed   =pStandStillState->AyP_MainWssSpeedOut.WssMax;
       

    if((Vehicle_Speed <=U8_F64SPEED_2KPH25)&&(Vehicle_Max_Wheel_Speed<U8_F64SPEED_0KPH25))
    {
        if(Standstill_Time<DEF_2SEC_TIME)
        {
            Standstill_Time ++;
        }
        else
        {
            StandStill_State =DEF_STANDSTILL;
        }
    }
    else
    {
        StandStill_State =DEF_NON_STANDSTILL;
        Standstill_Time =0;
    }

    return StandStill_State;
}


/***************
     Ay Noise Error 
     **************/
static void AyP_NoiseChk(AyP_Main_HdrBusType *pAyPNoiseChk, boolean inhibit)
{
    static Salsint16 Ay_1_1000g =0;
    Salsint16 Ay_1_1000g_old = 0;
    Saluint16 Diff_Ay_Value  =0;
    static Saluint16 AyP_Noise_Clear_Cnt =0;
    Saluint16 Ay_noise_threshold =0;
    boolean AyP_Noise_Inhibit=0;
    
    Ay_1_1000g_old =Ay_1_1000g;
    Ay_1_1000g =Proxy_fys16EstimatedAY;
    Diff_Ay_Value =(Saluint16)(abs(Ay_1_1000g-Ay_1_1000g_old));
    Ay_noise_threshold = AyP_Threhsold_Calc(pAyPNoiseChk);
    if(AyP_Noise_Clear_Cnt>DEF_5SEC_TIME)
    {
        pAyPNoiseChk->AyP_MainAyPlauOutput.AyPlauNoiselErr =ERR_PASSED;
        AyP_Noise_Clear_Cnt =0;
    }
    else
    {
        AyP_Noise_Clear_Cnt++;
    }
    
    if(Diff_Ay_Value>Ay_noise_threshold)
    {
        pAyPNoiseChk->AyP_MainAyPlauOutput.AyPlauNoiselErr =ERR_PREFAILED;
    }
    else
    {
        pAyPNoiseChk->AyP_MainAyPlauOutput.AyPlauNoiselErr =ERR_PREPASSED;
    }

    if( (pAyPNoiseChk->AyP_MainWssCalcInfo.Rough_Sus_Flg==1)
    )
    {
        AyP_Noise_Inhibit =1;
    }


    if((inhibit==TRUE)||(AyP_Noise_Inhibit==TRUE))
    {
        pAyPNoiseChk->AyP_MainAyPlauOutput.AyPlauNoiselErr = ERR_INHIBIT;
    }
}

static Saluint16 AyP_Threhsold_Calc(AyP_Main_HdrBusType *AyThreshold)
{
    Saluint16 AyP_Threshold=0;
    Saluint8    AyP_Stable =0; /* Need Stable define */
    if(AyThreshold->AyP_MainVehSpd<U8_F64SPEED_2KPH)
    {
        AyP_Threshold =Lat_0_3G;
    }
    else if(AyP_Stable ==1)
    {
        AyP_Threshold =Lat_0_8G;
    }
    else
    {
        AyP_Threshold =Lat_1_0G;
    }
    return AyP_Threshold;
}