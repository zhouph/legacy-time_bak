/**
 * @defgroup AyP_Main AyP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AyP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AyP_Main.h"
#include "AyP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AyP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AYP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AyP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AyP_Main_HdrBusType AyP_MainBus;

/* Version Info */
const SwcVersionInfo_t AyP_MainVersionInfo = 
{   
    AYP_MAIN_MODULE_ID,           /* AyP_MainVersionInfo.ModuleId */
    AYP_MAIN_MAJOR_VERSION,       /* AyP_MainVersionInfo.MajorVer */
    AYP_MAIN_MINOR_VERSION,       /* AyP_MainVersionInfo.MinorVer */
    AYP_MAIN_PATCH_VERSION,       /* AyP_MainVersionInfo.PatchVer */
    AYP_MAIN_BRANCH_VERSION       /* AyP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t AyP_MainEemFailData;
Proxy_RxCanRxEscInfo_t AyP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t AyP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t AyP_MainWhlSpdInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t AyP_MainBaseBrkCtrlModInfo;
SenPwrM_MainSenPwrMonitor_t AyP_MainSenPwrMonitorData;
Eem_MainEemSuspectData_t AyP_MainEemSuspectData;
Eem_MainEemEceData_t AyP_MainEemEceData;
CanM_MainCanMonData_t AyP_MainCanMonData;
Proxy_RxIMUCalc_t AyP_MainIMUCalcInfo;
Wss_SenWssSpeedOut_t AyP_MainWssSpeedOut;
Wss_SenWssCalc_t AyP_MainWssCalcInfo;
Mom_HndlrEcuModeSts_t AyP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t AyP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t AyP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t AyP_MainVBatt1Mon;
Diag_HndlrDiagClr_t AyP_MainDiagClrSrs;
Abc_CtrlVehSpd_t AyP_MainVehSpd;

/* Output Data Element */
AyP_MainAyPlauOutput_t AyP_MainAyPlauOutput;

uint32 AyP_Main_Timer_Start;
uint32 AyP_Main_Timer_Elapsed;

#define AYP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/** Variable Section (32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AYP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AYP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AyP_MemMap.h"
#define AYP_MAIN_START_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/** Variable Section (32BIT)**/


#define AYP_MAIN_STOP_SEC_VAR_32BIT
#include "AyP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AYP_MAIN_START_SEC_CODE
#include "AyP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AyP_Main_Init(void)
{
    /* Initialize internal bus */
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_SenPwr_12V = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_Yaw = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_Ay = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_Str = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssFL = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssFR = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssRL = 0;
    AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssRR = 0;
    AyP_MainBus.AyP_MainCanRxEscInfo.Ay = 0;
    AyP_MainBus.AyP_MainEscSwtStInfo.EscDisabledBySwt = 0;
    AyP_MainBus.AyP_MainWhlSpdInfo.FlWhlSpd = 0;
    AyP_MainBus.AyP_MainWhlSpdInfo.FrWhlSpd = 0;
    AyP_MainBus.AyP_MainWhlSpdInfo.RlWhlSpd = 0;
    AyP_MainBus.AyP_MainWhlSpdInfo.RrlWhlSpd = 0;
    AyP_MainBus.AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = 0;
    AyP_MainBus.AyP_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    AyP_MainBus.AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssFL = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssFR = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssRL = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssRR = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_FrontWss = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_RearWss = 0;
    AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = 0;
    AyP_MainBus.AyP_MainEemEceData.Eem_Ece_Ay = 0;
    AyP_MainBus.AyP_MainCanMonData.CanM_SubBusOff_Err = 0;
    AyP_MainBus.AyP_MainIMUCalcInfo.Reverse_Gear_flg = 0;
    AyP_MainBus.AyP_MainIMUCalcInfo.Reverse_Judge_Time = 0;
    AyP_MainBus.AyP_MainWssSpeedOut.WssMax = 0;
    AyP_MainBus.AyP_MainWssSpeedOut.WssMin = 0;
    AyP_MainBus.AyP_MainWssCalcInfo.Rough_Sus_Flg = 0;
    AyP_MainBus.AyP_MainEcuModeSts = 0;
    AyP_MainBus.AyP_MainIgnOnOffSts = 0;
    AyP_MainBus.AyP_MainIgnEdgeSts = 0;
    AyP_MainBus.AyP_MainVBatt1Mon = 0;
    AyP_MainBus.AyP_MainDiagClrSrs = 0;
    AyP_MainBus.AyP_MainVehSpd = 0;
    AyP_MainBus.AyP_MainAyPlauOutput.AyPlauNoiselErr = 0;
    AyP_MainBus.AyP_MainAyPlauOutput.AyPlauModelErr = 0;
    AyP_MainBus.AyP_MainAyPlauOutput.AyPlauShockErr = 0;
    AyP_MainBus.AyP_MainAyPlauOutput.AyPlauRangeErr = 0;
    AyP_MainBus.AyP_MainAyPlauOutput.AyPlauStandStillErr = 0;
}

void AyP_Main(void)
{
    AyP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_SenPwr_12V(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_SenPwr_12V);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_Yaw(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_Yaw);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_Ay(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_Ay);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_Str(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_Str);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssFL(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssFL);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssFR(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssFR);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssRL(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssRL);
    AyP_Main_Read_AyP_MainEemFailData_Eem_Fail_WssRR(&AyP_MainBus.AyP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainCanRxEscInfo_Ay(&AyP_MainBus.AyP_MainCanRxEscInfo.Ay);

    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainEscSwtStInfo_EscDisabledBySwt(&AyP_MainBus.AyP_MainEscSwtStInfo.EscDisabledBySwt);

    AyP_Main_Read_AyP_MainWhlSpdInfo(&AyP_MainBus.AyP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure AyP_MainWhlSpdInfo 
     : AyP_MainWhlSpdInfo.FlWhlSpd;
     : AyP_MainWhlSpdInfo.FrWhlSpd;
     : AyP_MainWhlSpdInfo.RlWhlSpd;
     : AyP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(&AyP_MainBus.AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainSenPwrMonitorData_SenPwrM_5V_Stable(&AyP_MainBus.AyP_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    AyP_Main_Read_AyP_MainSenPwrMonitorData_SenPwrM_12V_Stable(&AyP_MainBus.AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFL(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssFL);
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssFR(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssFR);
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRL(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssRL);
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_WssRR(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_WssRR);
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_FrontWss(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_FrontWss);
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_RearWss(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_RearWss);
    AyP_Main_Read_AyP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(&AyP_MainBus.AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V);

    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainEemEceData_Eem_Ece_Ay(&AyP_MainBus.AyP_MainEemEceData.Eem_Ece_Ay);

    /* Decomposed structure interface */
    AyP_Main_Read_AyP_MainCanMonData_CanM_SubBusOff_Err(&AyP_MainBus.AyP_MainCanMonData.CanM_SubBusOff_Err);

    AyP_Main_Read_AyP_MainIMUCalcInfo(&AyP_MainBus.AyP_MainIMUCalcInfo);
    /*==============================================================================
    * Members of structure AyP_MainIMUCalcInfo 
     : AyP_MainIMUCalcInfo.Reverse_Gear_flg;
     : AyP_MainIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/
    
    AyP_Main_Read_AyP_MainWssSpeedOut(&AyP_MainBus.AyP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure AyP_MainWssSpeedOut 
     : AyP_MainWssSpeedOut.WssMax;
     : AyP_MainWssSpeedOut.WssMin;
     =============================================================================*/
    
    AyP_Main_Read_AyP_MainWssCalcInfo(&AyP_MainBus.AyP_MainWssCalcInfo);
    /*==============================================================================
    * Members of structure AyP_MainWssCalcInfo 
     : AyP_MainWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/
    
    AyP_Main_Read_AyP_MainEcuModeSts(&AyP_MainBus.AyP_MainEcuModeSts);
    AyP_Main_Read_AyP_MainIgnOnOffSts(&AyP_MainBus.AyP_MainIgnOnOffSts);
    AyP_Main_Read_AyP_MainIgnEdgeSts(&AyP_MainBus.AyP_MainIgnEdgeSts);
    AyP_Main_Read_AyP_MainVBatt1Mon(&AyP_MainBus.AyP_MainVBatt1Mon);
    AyP_Main_Read_AyP_MainDiagClrSrs(&AyP_MainBus.AyP_MainDiagClrSrs);
    AyP_Main_Read_AyP_MainVehSpd(&AyP_MainBus.AyP_MainVehSpd);

    /* Process */
    AyP_Process(&AyP_MainBus);
    /* Output */
    AyP_Main_Write_AyP_MainAyPlauOutput(&AyP_MainBus.AyP_MainAyPlauOutput);
    /*==============================================================================
    * Members of structure AyP_MainAyPlauOutput 
     : AyP_MainAyPlauOutput.AyPlauNoiselErr;
     : AyP_MainAyPlauOutput.AyPlauModelErr;
     : AyP_MainAyPlauOutput.AyPlauShockErr;
     : AyP_MainAyPlauOutput.AyPlauRangeErr;
     : AyP_MainAyPlauOutput.AyPlauStandStillErr;
     =============================================================================*/
    

    AyP_Main_Timer_Elapsed = STM0_TIM0.U - AyP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AYP_MAIN_STOP_SEC_CODE
#include "AyP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
