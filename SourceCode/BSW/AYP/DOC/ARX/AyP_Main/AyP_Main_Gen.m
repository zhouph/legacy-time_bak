AyP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Str'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
AyP_MainEemFailData = CreateBus(AyP_MainEemFailData, DeList);
clear DeList;

AyP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ay'
    };
AyP_MainCanRxEscInfo = CreateBus(AyP_MainCanRxEscInfo, DeList);
clear DeList;

AyP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    };
AyP_MainEscSwtStInfo = CreateBus(AyP_MainEscSwtStInfo, DeList);
clear DeList;

AyP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
AyP_MainWhlSpdInfo = CreateBus(AyP_MainWhlSpdInfo, DeList);
clear DeList;

AyP_MainBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
AyP_MainBaseBrkCtrlModInfo = CreateBus(AyP_MainBaseBrkCtrlModInfo, DeList);
clear DeList;

AyP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AyP_MainSenPwrMonitorData = CreateBus(AyP_MainSenPwrMonitorData, DeList);
clear DeList;

AyP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_SenPwr_12V'
    };
AyP_MainEemSuspectData = CreateBus(AyP_MainEemSuspectData, DeList);
clear DeList;

AyP_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Ay'
    };
AyP_MainEemEceData = CreateBus(AyP_MainEemEceData, DeList);
clear DeList;

AyP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_SubBusOff_Err'
    };
AyP_MainCanMonData = CreateBus(AyP_MainCanMonData, DeList);
clear DeList;

AyP_MainIMUCalcInfo = Simulink.Bus;
DeList={
    'Reverse_Gear_flg'
    'Reverse_Judge_Time'
    };
AyP_MainIMUCalcInfo = CreateBus(AyP_MainIMUCalcInfo, DeList);
clear DeList;

AyP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
AyP_MainWssSpeedOut = CreateBus(AyP_MainWssSpeedOut, DeList);
clear DeList;

AyP_MainWssCalcInfo = Simulink.Bus;
DeList={
    'Rough_Sus_Flg'
    };
AyP_MainWssCalcInfo = CreateBus(AyP_MainWssCalcInfo, DeList);
clear DeList;

AyP_MainEcuModeSts = Simulink.Bus;
DeList={'AyP_MainEcuModeSts'};
AyP_MainEcuModeSts = CreateBus(AyP_MainEcuModeSts, DeList);
clear DeList;

AyP_MainIgnOnOffSts = Simulink.Bus;
DeList={'AyP_MainIgnOnOffSts'};
AyP_MainIgnOnOffSts = CreateBus(AyP_MainIgnOnOffSts, DeList);
clear DeList;

AyP_MainIgnEdgeSts = Simulink.Bus;
DeList={'AyP_MainIgnEdgeSts'};
AyP_MainIgnEdgeSts = CreateBus(AyP_MainIgnEdgeSts, DeList);
clear DeList;

AyP_MainVBatt1Mon = Simulink.Bus;
DeList={'AyP_MainVBatt1Mon'};
AyP_MainVBatt1Mon = CreateBus(AyP_MainVBatt1Mon, DeList);
clear DeList;

AyP_MainDiagClrSrs = Simulink.Bus;
DeList={'AyP_MainDiagClrSrs'};
AyP_MainDiagClrSrs = CreateBus(AyP_MainDiagClrSrs, DeList);
clear DeList;

AyP_MainVehSpd = Simulink.Bus;
DeList={'AyP_MainVehSpd'};
AyP_MainVehSpd = CreateBus(AyP_MainVehSpd, DeList);
clear DeList;

AyP_MainAyPlauOutput = Simulink.Bus;
DeList={
    'AyPlauNoiselErr'
    'AyPlauModelErr'
    'AyPlauShockErr'
    'AyPlauRangeErr'
    'AyPlauStandStillErr'
    };
AyP_MainAyPlauOutput = CreateBus(AyP_MainAyPlauOutput, DeList);
clear DeList;

