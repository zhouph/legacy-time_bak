#include "unity.h"
#include "unity_fixture.h"
#include "AyP_Main.h"
#include "AyP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_Yaw[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_YAW;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_Ay[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_AY;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_Str[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_STR;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_AyP_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = AYP_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Salsint16 UtInput_AyP_MainCanRxEscInfo_Ay[MAX_STEP] = AYP_MAINCANRXESCINFO_AY;
const Saluint8 UtInput_AyP_MainEscSwtStInfo_EscDisabledBySwt[MAX_STEP] = AYP_MAINESCSWTSTINFO_ESCDISABLEDBYSWT;
const Saluint16 UtInput_AyP_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = AYP_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_AyP_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = AYP_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_AyP_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = AYP_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_AyP_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = AYP_MAINWHLSPDINFO_RRLWHLSPD;
const Rtesint32 UtInput_AyP_MainBaseBrkCtrlModInfo_VehStandStillStFlg[MAX_STEP] = AYP_MAINBASEBRKCTRLMODINFO_VEHSTANDSTILLSTFLG;
const Saluint8 UtInput_AyP_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = AYP_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_AyP_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = AYP_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssFL[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFL;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssFR[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFR;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssRL[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRL;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssRR[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRR;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_FrontWss[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_RearWss[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_REARWSS;
const Saluint8 UtInput_AyP_MainEemSuspectData_Eem_Suspect_SenPwr_12V[MAX_STEP] = AYP_MAINEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_12V;
const Saluint8 UtInput_AyP_MainEemEceData_Eem_Ece_Ay[MAX_STEP] = AYP_MAINEEMECEDATA_EEM_ECE_AY;
const Saluint8 UtInput_AyP_MainCanMonData_CanM_SubBusOff_Err[MAX_STEP] = AYP_MAINCANMONDATA_CANM_SUBBUSOFF_ERR;
const Saluint8 UtInput_AyP_MainIMUCalcInfo_Reverse_Gear_flg[MAX_STEP] = AYP_MAINIMUCALCINFO_REVERSE_GEAR_FLG;
const Saluint8 UtInput_AyP_MainIMUCalcInfo_Reverse_Judge_Time[MAX_STEP] = AYP_MAINIMUCALCINFO_REVERSE_JUDGE_TIME;
const Saluint32 UtInput_AyP_MainWssSpeedOut_WssMax[MAX_STEP] = AYP_MAINWSSSPEEDOUT_WSSMAX;
const Saluint32 UtInput_AyP_MainWssSpeedOut_WssMin[MAX_STEP] = AYP_MAINWSSSPEEDOUT_WSSMIN;
const Saluint8 UtInput_AyP_MainWssCalcInfo_Rough_Sus_Flg[MAX_STEP] = AYP_MAINWSSCALCINFO_ROUGH_SUS_FLG;
const Mom_HndlrEcuModeSts_t UtInput_AyP_MainEcuModeSts[MAX_STEP] = AYP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_AyP_MainIgnOnOffSts[MAX_STEP] = AYP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_AyP_MainIgnEdgeSts[MAX_STEP] = AYP_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_AyP_MainVBatt1Mon[MAX_STEP] = AYP_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_AyP_MainDiagClrSrs[MAX_STEP] = AYP_MAINDIAGCLRSRS;
const Abc_CtrlVehSpd_t UtInput_AyP_MainVehSpd[MAX_STEP] = AYP_MAINVEHSPD;

const Saluint8 UtExpected_AyP_MainAyPlauOutput_AyPlauNoiselErr[MAX_STEP] = AYP_MAINAYPLAUOUTPUT_AYPLAUNOISELERR;
const Saluint8 UtExpected_AyP_MainAyPlauOutput_AyPlauModelErr[MAX_STEP] = AYP_MAINAYPLAUOUTPUT_AYPLAUMODELERR;
const Saluint8 UtExpected_AyP_MainAyPlauOutput_AyPlauShockErr[MAX_STEP] = AYP_MAINAYPLAUOUTPUT_AYPLAUSHOCKERR;
const Saluint8 UtExpected_AyP_MainAyPlauOutput_AyPlauRangeErr[MAX_STEP] = AYP_MAINAYPLAUOUTPUT_AYPLAURANGEERR;
const Saluint8 UtExpected_AyP_MainAyPlauOutput_AyPlauStandStillErr[MAX_STEP] = AYP_MAINAYPLAUOUTPUT_AYPLAUSTANDSTILLERR;



TEST_GROUP(AyP_Main);
TEST_SETUP(AyP_Main)
{
    AyP_Main_Init();
}

TEST_TEAR_DOWN(AyP_Main)
{   /* Postcondition */

}

TEST(AyP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        AyP_MainEemFailData.Eem_Fail_SenPwr_12V = UtInput_AyP_MainEemFailData_Eem_Fail_SenPwr_12V[i];
        AyP_MainEemFailData.Eem_Fail_Yaw = UtInput_AyP_MainEemFailData_Eem_Fail_Yaw[i];
        AyP_MainEemFailData.Eem_Fail_Ay = UtInput_AyP_MainEemFailData_Eem_Fail_Ay[i];
        AyP_MainEemFailData.Eem_Fail_Str = UtInput_AyP_MainEemFailData_Eem_Fail_Str[i];
        AyP_MainEemFailData.Eem_Fail_WssFL = UtInput_AyP_MainEemFailData_Eem_Fail_WssFL[i];
        AyP_MainEemFailData.Eem_Fail_WssFR = UtInput_AyP_MainEemFailData_Eem_Fail_WssFR[i];
        AyP_MainEemFailData.Eem_Fail_WssRL = UtInput_AyP_MainEemFailData_Eem_Fail_WssRL[i];
        AyP_MainEemFailData.Eem_Fail_WssRR = UtInput_AyP_MainEemFailData_Eem_Fail_WssRR[i];
        AyP_MainCanRxEscInfo.Ay = UtInput_AyP_MainCanRxEscInfo_Ay[i];
        AyP_MainEscSwtStInfo.EscDisabledBySwt = UtInput_AyP_MainEscSwtStInfo_EscDisabledBySwt[i];
        AyP_MainWhlSpdInfo.FlWhlSpd = UtInput_AyP_MainWhlSpdInfo_FlWhlSpd[i];
        AyP_MainWhlSpdInfo.FrWhlSpd = UtInput_AyP_MainWhlSpdInfo_FrWhlSpd[i];
        AyP_MainWhlSpdInfo.RlWhlSpd = UtInput_AyP_MainWhlSpdInfo_RlWhlSpd[i];
        AyP_MainWhlSpdInfo.RrlWhlSpd = UtInput_AyP_MainWhlSpdInfo_RrlWhlSpd[i];
        AyP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = UtInput_AyP_MainBaseBrkCtrlModInfo_VehStandStillStFlg[i];
        AyP_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_AyP_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        AyP_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_AyP_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        AyP_MainEemSuspectData.Eem_Suspect_WssFL = UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssFL[i];
        AyP_MainEemSuspectData.Eem_Suspect_WssFR = UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssFR[i];
        AyP_MainEemSuspectData.Eem_Suspect_WssRL = UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssRL[i];
        AyP_MainEemSuspectData.Eem_Suspect_WssRR = UtInput_AyP_MainEemSuspectData_Eem_Suspect_WssRR[i];
        AyP_MainEemSuspectData.Eem_Suspect_FrontWss = UtInput_AyP_MainEemSuspectData_Eem_Suspect_FrontWss[i];
        AyP_MainEemSuspectData.Eem_Suspect_RearWss = UtInput_AyP_MainEemSuspectData_Eem_Suspect_RearWss[i];
        AyP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = UtInput_AyP_MainEemSuspectData_Eem_Suspect_SenPwr_12V[i];
        AyP_MainEemEceData.Eem_Ece_Ay = UtInput_AyP_MainEemEceData_Eem_Ece_Ay[i];
        AyP_MainCanMonData.CanM_SubBusOff_Err = UtInput_AyP_MainCanMonData_CanM_SubBusOff_Err[i];
        AyP_MainIMUCalcInfo.Reverse_Gear_flg = UtInput_AyP_MainIMUCalcInfo_Reverse_Gear_flg[i];
        AyP_MainIMUCalcInfo.Reverse_Judge_Time = UtInput_AyP_MainIMUCalcInfo_Reverse_Judge_Time[i];
        AyP_MainWssSpeedOut.WssMax = UtInput_AyP_MainWssSpeedOut_WssMax[i];
        AyP_MainWssSpeedOut.WssMin = UtInput_AyP_MainWssSpeedOut_WssMin[i];
        AyP_MainWssCalcInfo.Rough_Sus_Flg = UtInput_AyP_MainWssCalcInfo_Rough_Sus_Flg[i];
        AyP_MainEcuModeSts = UtInput_AyP_MainEcuModeSts[i];
        AyP_MainIgnOnOffSts = UtInput_AyP_MainIgnOnOffSts[i];
        AyP_MainIgnEdgeSts = UtInput_AyP_MainIgnEdgeSts[i];
        AyP_MainVBatt1Mon = UtInput_AyP_MainVBatt1Mon[i];
        AyP_MainDiagClrSrs = UtInput_AyP_MainDiagClrSrs[i];
        AyP_MainVehSpd = UtInput_AyP_MainVehSpd[i];

        AyP_Main();

        TEST_ASSERT_EQUAL(AyP_MainAyPlauOutput.AyPlauNoiselErr, UtExpected_AyP_MainAyPlauOutput_AyPlauNoiselErr[i]);
        TEST_ASSERT_EQUAL(AyP_MainAyPlauOutput.AyPlauModelErr, UtExpected_AyP_MainAyPlauOutput_AyPlauModelErr[i]);
        TEST_ASSERT_EQUAL(AyP_MainAyPlauOutput.AyPlauShockErr, UtExpected_AyP_MainAyPlauOutput_AyPlauShockErr[i]);
        TEST_ASSERT_EQUAL(AyP_MainAyPlauOutput.AyPlauRangeErr, UtExpected_AyP_MainAyPlauOutput_AyPlauRangeErr[i]);
        TEST_ASSERT_EQUAL(AyP_MainAyPlauOutput.AyPlauStandStillErr, UtExpected_AyP_MainAyPlauOutput_AyPlauStandStillErr[i]);
    }
}

TEST_GROUP_RUNNER(AyP_Main)
{
    RUN_TEST_CASE(AyP_Main, All);
}
