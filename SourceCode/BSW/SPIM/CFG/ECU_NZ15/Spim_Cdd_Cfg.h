/**
 * @defgroup Spim_Cdd_Cfg Spim_Cdd_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Cdd_Cfg.h
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

#ifndef SPIM_CDD_CFG_H_
#define SPIM_CDD_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Cdd_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
  /* Spi Driver Channel Symbolic names */
  #define Spim_Cdd_SpiChannel_Vdr_A3944     (Spim_Cdd_ChannelType)0
  #define Spim_Cdd_SpiChannel_100Asic       (Spim_Cdd_ChannelType)1
  #define Spim_Cdd_SpiChannel_Mps_Tle5012_1 (Spim_Cdd_ChannelType)2
  #define Spim_Cdd_SpiChannel_Mps_Tle5012_2 (Spim_Cdd_ChannelType)3
  #define Spim_Cdd_SpiChannel_Mtr           (Spim_Cdd_ChannelType)4
  #define Spim_Cdd_SpiChannel_MaxNum        (Spim_Cdd_ChannelType)5
  #define Spim_Cdd_SpiChannel_Null          (Spim_Cdd_ChannelType)6

  /* Spi Driver Sequence Symbolic names */
  #define Spim_Cdd_SpiSequence_100Asic     (Spim_Cdd_SequenceType)0
  #define Spim_Cdd_SpiSequence_Mps_Tle5012 (Spim_Cdd_SequenceType)1
  #define Spim_Cdd_SpiSequence_Mtr_Vdr     (Spim_Cdd_SequenceType)2
  #define Spim_Cdd_SpiSequence_MaxNum      (Spim_Cdd_SequenceType)3

  /* Max number of channel in a sequence */
  #define Spim_Cdd_ChannelAssign_MaxNum       5
  #define SPI_EB_SIZE   128
 
  /* Det ID */
  #define SPIM_CDD_MODULE_ID              ((uint16) 1015U)
  #define SPIM_CDD_API_INIT               ((uint8) 0U)
  #define SPIM_CDD_API_SETUP_EB           ((uint8) 1U)
  #define SPIM_CDD_API_SYNC_TX            ((uint8) 2U)
  #define SPIM_CDD_API_ASYNC_TX           ((uint8) 3U)
  #define SPIM_CDD_ERROR_INPUT_RANGE      ((uint8) 0U)
  #define SPIM_CDD_ERROR_CONFIG_PAR       ((uint8) 1U)
  #define SPIM_CDD_ERROR_BUFFER           ((uint8) 2U)
  #define SPIM_CDD_ERROR_UNSUPPORTED      ((uint8) 3U)
  
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
  /* Spi Driver Config Parameters */
  extern const Spim_Cdd_ChannelConfigType Spim_Cdd_ChannelConfig[];
  extern const Spim_Cdd_ChannelType \
    Spim_Cdd_SeqChAssign[Spim_Cdd_SpiSequence_MaxNum][Spim_Cdd_ChannelAssign_MaxNum];
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_CDD_CFG_H_ */
/** @} */
