/**
 * @defgroup Spim_Sch_Cfg Spim_Sch_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Sch_Cfg.c
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Sch_Cfg.h"
#include "Spim_If_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Spi Schedule Table for Asic Initialization Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableAsicInit[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence Entry 0ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_0,            /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_1,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_0             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_2,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_1             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_3,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_2             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-4 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_4,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_3             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-5 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_7,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_4             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-6 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_9,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_7             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-7 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_18,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_9             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-8 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_19,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_18            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-Dummy */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_8,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_19            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-Dummy */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_5,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_8             /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 0ms-Dummy */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_6,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_5             /* Rx Frame ID */ 
     },
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }     
 };

 /* Spi Schedule Table for Asic Default Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableAsicDefault[] = 
 {  
    {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence Entry 0ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_0,            /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_3,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_0             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_10,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_3             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_11,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_10            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-4 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_12,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_11            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-5 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_13,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_12            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-6 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_14,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_13            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-7 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_15,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_14            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-8 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_16,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_15            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-9 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_17,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_16            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-10 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_24,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_17            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-11 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_25,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_24            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-12 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_26,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_25            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0ms-13 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_0,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_9,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_26            /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_0,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence Entry 1ms */ 
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_2,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_9             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_4,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_2             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_10,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_4             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_11,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_10            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-4 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_12,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_11            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-5 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_13,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_12            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-6 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_14,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_13            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-7 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_15,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_14            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-8 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_16,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_15            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-9 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_17,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_16            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-10 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_19,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_17            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-11 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_20,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_19            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-12 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_21,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_20            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-13 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_22,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_21            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-14 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_23,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_22            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1ms-15 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_1,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_9,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_23            /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 1ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_1,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence Entry 2ms */ 
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_5,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_9             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_8,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_5             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_10,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_8             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_11,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_10            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-4 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_12,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_11            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-5 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_13,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_12            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-6 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_14,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_13            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-7 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_15,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_14            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-8 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_16,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_15            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-9 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_17,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_16            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2ms-10 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_2,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_9,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_17            /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 2ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_2,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence Entry 3ms */ 
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_3,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_9             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_5,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_3             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_10,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_5             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_11,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_10            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-4 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_12,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_11            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-5 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_13,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_12            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-6 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_14,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_13            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-7 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_15,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_14            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-8 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_16,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_15            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-9 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_17,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_16            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-10 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_18,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_17            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3ms-11 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_3,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_9,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_18            /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 3ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_3,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence Entry 4ms */ 
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     },          
     {  /* Entry Transmit 4ms-0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_5,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_9             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_6,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_5             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_10,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_6             /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_11,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_10            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-4 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_12,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_11            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-5 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_13,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_12            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-6 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_14,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_13            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-7 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_15,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_14            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-8 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_16,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_15            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-9 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_17,           /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_16            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-10 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_9,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_17            /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 4ms-11 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Asic_4,           /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_ASIC_MSG_TX_8,            /* Tx Frame ID */ 
        SPI_FRAME_ASIC_MSG_RX_9             /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 4ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Asic_4,          /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }    
 };

 /* Address Table for Asic Schedule Table */
 const Spim_Sch_AddressTableType  Spim_Sch_AddrTableAsic[] = 
 {          
    (Spim_Sch_AddressTableType)&Spim_Sch_TableAsicInit[0],    /* Asic Initial Schedule Table Address */
    (Spim_Sch_AddressTableType)&Spim_Sch_TableAsicDefault[0]  /* Asic Default Schedule Table Address */
 };
 
 /* Spi Schedule Table for Mps1 Initialization Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableMps1_Init[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_ANGLE,        /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS1_MSG_RX_ANGLE         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_CONTROL,      /* Tx Frame ID */
        SPI_FRAME_MPS1_MSG_RX_ERROR         /* Rx Frame ID */
     }, 
     {  /* Entry Transmit 3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS1_MSG_RX_CONTROL       /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }    
 };
 
 /* Spi Schedule Table for Mps1 Default Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableMps1_Default[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_ANGLE,        /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS1_MSG_RX_ANGLE         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_CONTROL,      /* Tx Frame ID */ 
        SPI_FRAME_MPS1_MSG_RX_ERROR         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_MPS1_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS1_MSG_RX_CONTROL       /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }   
 };

 /* Address Table for Mps1 Schedule Table */
 const Spim_Sch_AddressTableType  Spim_Sch_AddrTableMps_1[] = \
 {  
    (Spim_Sch_AddressTableType)&Spim_Sch_TableMps1_Init[0],    /* Mps1 Initial Schedule Table Address */
    (Spim_Sch_AddressTableType)&Spim_Sch_TableMps1_Default[0]  /* Mps1 Default Schedule Table Address */
 };

  /* Spi Schedule Table for Mps2 Initialization Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableMps2_Init[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_ANGLE,        /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS2_MSG_RX_ANGLE         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_CONTROL,      /* Tx Frame ID */ 
        SPI_FRAME_MPS2_MSG_RX_ERROR         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS2_MSG_RX_CONTROL       /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }    
 };
 
 /* Spi Schedule Table for Mps2 Default Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableMps2_Default[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_ANGLE,        /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS2_MSG_RX_ANGLE         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_CONTROL,      /* Tx Frame ID */ 
        SPI_FRAME_MPS2_MSG_RX_ERROR         /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 3 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Mps_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_MPS2_MSG_TX_ERROR,        /* Tx Frame ID */ 
        SPI_FRAME_MPS2_MSG_RX_CONTROL       /* Rx Frame ID */ 
     }, 
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Mps_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }   
 };

 /* Address Table for Mps2 Schedule Table */
 const Spim_Sch_AddressTableType  Spim_Sch_AddrTableMps_2[] = \
 {  
    (Spim_Sch_AddressTableType)&Spim_Sch_TableMps2_Init[0],    /* Mps2 Initial Schedule Table Address */
    (Spim_Sch_AddressTableType)&Spim_Sch_TableMps2_Default[0]  /* Mps2 Default Schedule Table Address */
 };

 /* Spi Schedule Table for VDR Init Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableVdr_Init[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_0,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence 0ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_0,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0 - 0*/  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_0,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,      /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 0 - 1 */  
        ScheduleEntry_AsyncTransmit,            /* Entry Type */  
        Spim_Cdd_SpiChannel_Reg_0,                /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,               /* Spi Sequence */  
        SPI_FRAME_REG_MSG_TX_WR_SENS_CTRL,      /* Tx Frame ID */ 
        SPI_FRAME_NULL                          /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 0 - 2*/  
        ScheduleEntry_AsyncTransmit,            /* Entry Type */  
        Spim_Cdd_SpiChannel_Reg_0,                /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,               /* Spi Sequence */  
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,  /* Tx Frame ID */ 
        SPI_FRAME_NULL                          /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 0 - 1 */  
        ScheduleEntry_AsyncTransmit,            /* Entry Type */  
        Spim_Cdd_SpiChannel_Reg_0,                /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,               /* Spi Sequence */  
        SPI_FRAME_REG_MSG_TX_RD_SENS_CTRL,      /* Tx Frame ID */ 
        SPI_FRAME_REG_MSG_RX_RD_SENS_CTRL       /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 0 - 2*/  
        ScheduleEntry_AsyncTransmit,            /* Entry Type */  
        Spim_Cdd_SpiChannel_Reg_0,                /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,               /* Spi Sequence */  
        SPI_FRAME_REG_MSG_TX_RD_DIAG_CFG_CTRL,  /* Tx Frame ID */ 
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL   /* Rx Frame ID */ 
     },
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence 1ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_1,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK,     /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* End of Sequence Entry 1ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence 2ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* Tx Frame ID */
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },     
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },     
     {  /* End of Sequence Entry 2ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }   
 };

 /* Spi Schedule Table for VDR Default Schedule */
 const Spim_Sch_ScheduleTableType Spim_Sch_TableVdr_Default[] = 
 {  
     {  /* Start of Schedule Entry */ 
        ScheduleEntry_StartOfSchedule,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_0,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence 0ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_0,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 0 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_0,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,      /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* End of Sequence Entry 0ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_0,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence 1ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 1 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_1,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_1,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK,     /* Tx Frame ID */ 
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* End of Sequence Entry 1ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_1,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Start of Sequence 2ms */
        ScheduleEntry_StartOfSequence,      /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* Entry Transmit 2 */  
        ScheduleEntry_AsyncTransmit,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Vdr_2,            /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* Tx Frame ID */
        SPI_FRAME_VDR_MSG_RX_FAULT          /* Rx Frame ID */ 
     },
     {  /* End of Sequence Entry 2ms */ 
        ScheduleEntry_EndOfSequence,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }, 
     {  /* End of Schedule Entry */ 
        ScheduleEntry_EndOfSchedule,        /* Entry Type */  
        Spim_Cdd_SpiChannel_Null,             /* Spi Channel */ 
        Spim_Cdd_SpiSequence_Vdr_2,           /* Spi Sequence */  
        SPI_FRAME_NULL,                     /* Tx Frame ID */ 
        SPI_FRAME_NULL                      /* Rx Frame ID */ 
     }     
 };

 /* Address Table for Vdr Schedule Table */
 const Spim_Sch_AddressTableType  Spim_Sch_AddrTableVdr[] = \
 {  
    (Spim_Sch_AddressTableType)&Spim_Sch_TableVdr_Init[0],    /* Mps2 Initial Schedule Table Address */
    (Spim_Sch_AddressTableType)&Spim_Sch_TableVdr_Default[0]  /* Mps2 Default Schedule Table Address */
 };

 /* Entity Table */
 const Spim_Sch_EntityTableType   Spim_Sch_EntityTable[] = \
 {
    (Spim_Sch_EntityTableType)&Spim_Sch_AddrTableAsic[0],
    (Spim_Sch_EntityTableType)&Spim_Sch_AddrTableMps_1[0],
    (Spim_Sch_EntityTableType)&Spim_Sch_AddrTableMps_2[0],
    (Spim_Sch_EntityTableType)&Spim_Sch_AddrTableVdr[0]
 };



/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
 #define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"


/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
