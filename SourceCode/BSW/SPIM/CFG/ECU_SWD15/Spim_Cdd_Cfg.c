/**
 * @defgroup Spim_Cdd_Cfg Spim_Cdd_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Cdd_Cfg.c
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Cdd.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Spi Driver Config Parameters */
 const Spim_Cdd_ChannelConfigType Spim_Cdd_ChannelConfig[] =
 {
    /* Spi Configuration for ASIC Channel0 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Disabled,                /* Pha */
        HwChannelType_QspiCh0,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch2,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH19,                           /* DmaChTx */
        DMA_CH20                            /* DmaChRx */
    },
    /* Spi Configuration for ASIC Channel1 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Disabled,                /* Pha */
        HwChannelType_QspiCh0,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch2,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH19,                           /* DmaChTx */
        DMA_CH20                            /* DmaChRx */
    },
    /* Spi Configuration for ASIC Channel2 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Disabled,                /* Pha */
        HwChannelType_QspiCh0,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch2,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH19,                           /* DmaChTx */
        DMA_CH20                            /* DmaChRx */
    },
    /* Spi Configuration for ASIC Channel3 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Disabled,                /* Pha */
        HwChannelType_QspiCh0,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch2,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH19,                           /* DmaChTx */
        DMA_CH20                            /* DmaChRx */
    },
    /* Spi Configuration for ASIC Channel4 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Disabled,                /* Pha */
        HwChannelType_QspiCh0,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch2,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH19,                           /* DmaChTx */
        DMA_CH20                            /* DmaChRx */
    },
    /* Spi Configuration for MPS Channel 1 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh1,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch4,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH21,                           /* DmaChTx */
        DMA_CH22                            /* DmaChRx */
    },
    /* Spi Configuration for MPS Channel 2 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh2,              /* HwChannel */
        PortInputType_B,                    /* PortInput */
        HwCs_Ch0,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH23,                           /* DmaChTx */
        DMA_CH24                            /* DmaChRx */
    },
    /* Spi Configuration for Vlave driver Channel 0 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch10,                          /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
    /* Spi Configuration for Vlave driver Channel 1 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch10,                          /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
    /* Spi Configuration for Vlave driver Channel 2 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch10,                          /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
    /* Spi Configuration for Vlave driver Channel 3 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch10,                          /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
    /* Spi Configuration for Vlave driver Channel 4 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch10,                          /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
    /* Spi Configuration for Regulator Channel 0 */
    {
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch6,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x2U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x1U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x1U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
 };

 const Spim_Cdd_ChannelType \
    Spim_Cdd_SeqChAssign[Spim_Cdd_SpiSequence_MaxNum][Spim_Cdd_ChannelAssign_MaxNum] =
 {
    /* Channel asignment for Sequence 0 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Asic_0,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 1 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Asic_1,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 2 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Asic_2,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 3 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Asic_3,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 4 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Asic_4,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 5 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Mps_1,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 6 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Mps_2,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 7 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Vdr_0,
        Spim_Cdd_SpiChannel_Reg_0,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 8 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Vdr_1,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 9 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Vdr_2,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 10 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Vdr_3,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 11 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Vdr_4,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
 };


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
