/**
 * @defgroup Spim_Sch_Cfg Spim_Sch_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Sch_Cfg.h
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

#ifndef SPIM_SCH_CFG_H_
#define SPIM_SCH_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Sch_Types.h"
#include "Spim_Cdd_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 /* Spi Schedule Entity Number Type */
 #define ScheduleEntity_Asic     (Spim_Sch_ScheduleEntityNumType)0
 #define ScheduleEntity_Mps_1    (Spim_Sch_ScheduleEntityNumType)1
 #define ScheduleEntity_Mps_2    (Spim_Sch_ScheduleEntityNumType)2
 #define ScheduleEntity_Vdr      (Spim_Sch_ScheduleEntityNumType)3
 #define ScheduleEntity_Max      (Spim_Sch_ScheduleEntityNumType)4
 #define ScheduleEntity_Null     (Spim_Sch_ScheduleEntityNumType)5

 /* Spi Schedule Table Number Type */
 #define ScheduleTable_Init      (Spim_Sch_ScheduleTableNumType)0
 #define ScheduleTable_Default   (Spim_Sch_ScheduleTableNumType)1
 #define ScheduleTable_Max       (Spim_Sch_ScheduleTableNumType)2
 #define ScheduleTable_Null      (Spim_Sch_ScheduleTableNumType)3

 /* Configuration for Schedule Tables */
 #define SPI_SCHEDULE_MAX_SEQ    5      /* Max Number of Sequence */
 #define SPI_SCHEDULE_MAX_EP     20     /* Max Number of Entry point inside a sequence */

 /* Det ID */
 #define SPIM_SCH_MODULE_ID           ((uint16) 1014U)
 #define SPIM_SCH_API_INIT            ((uint8) 0U)
 #define SPIM_SCH_API_PARSE           ((uint8) 1U)
 #define SPIM_SCH_API_START           ((uint8) 2U)
 #define SPIM_SCH_API_STOP            ((uint8) 3U)
 #define SPIM_SCH_API_RUN             ((uint8) 4U)
 #define SPIM_SCH_API_SET_ACTIVE      ((uint8) 5U)
 #define SPIM_SCH_API_GET_ACTIVE      ((uint8) 6U)
 #define SPIM_SCH_ERROR_INPUT_RANGE   ((uint8) 7U)
 #define SPIM_SCH_ERROR_TIMING        ((uint8) 8U)
 #define SPIM_SCH_ERROR_CALL_LLD      ((uint8) 9U)
 #define SPIM_SCH_ERROR_CONFIG_PAR    ((uint8) 10U)

 /* Initialization */
 #define Spim_Sch_Init()  \
 {  \
    Spim_Sch_InitScheduleEntity(ScheduleEntity_Asic);     \
    Spim_Sch_InitScheduleEntity(ScheduleEntity_Mps_1);    \
    Spim_Sch_InitScheduleEntity(ScheduleEntity_Mps_2);    \
    Spim_Sch_InitScheduleEntity(ScheduleEntity_Vdr);      \
    Spim_Sch_StartPeriodic(ScheduleEntity_Mps_1);         \
    Spim_Sch_StartPeriodic(ScheduleEntity_Mps_2);         \
 }

 /* Main function */
 #define Spim_Sch_MainFunction_50us() \
 {  \
    Spim_Sch_RunSchedule(ScheduleEntity_Mps_1);   \
    Spim_Sch_RunSchedule(ScheduleEntity_Mps_2);   \
 }
 #define Spim_Sch_MainFunction_1ms()  \
 {  \
    Spim_Sch_RunSchedule(ScheduleEntity_Asic);    \
    Spim_Sch_RunSchedule(ScheduleEntity_Vdr);     \
 }
 #define Spim_Sch_MainFunction_5ms()  \
 {  \
    Spim_Sch_StartOneShot(ScheduleEntity_Asic);   \
    Spim_Sch_StartOneShot(ScheduleEntity_Vdr);    \
 }
 
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 /* Entity Table */
 extern const Spim_Sch_EntityTableType   Spim_Sch_EntityTable[];

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_SCH_CFG_H_ */
/** @} */
