/**
 * @defgroup Spim_If_Cfg Spim_If_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_If_Cfg.c
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_If.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

 const Spim_If_SignalConfigType Spim_If_SignalConfig[SPI_SIG_MAX_NUM] =
 {
    /* SPI_SIG_ASIC_TX_PD_MT_CFG */
    {
        SPI_FRAME_ASIC_MSG_TX_0,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_HPD_SR */
    {
        SPI_FRAME_ASIC_MSG_TX_0,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_SED */
    {
        SPI_FRAME_ASIC_MSG_TX_0,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_E5VS_OC_CFG */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LDACT_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD_SINK_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ICHAR */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PCHAR */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 4         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WSCFG4 */
    {
        SPI_FRAME_ASIC_MSG_TX_2,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WSCFG3 */
    {
        SPI_FRAME_ASIC_MSG_TX_2,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WSCFG2 */
    {
        SPI_FRAME_ASIC_MSG_TX_2,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WSCFG1 */
    {
        SPI_FRAME_ASIC_MSG_TX_2,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_OCF_PD */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ADIN1_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ADIN2_EN */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_STOPCLK2 */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_FM_EN */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_FM_AMP */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ISO_DET */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ISO_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 9,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VSO_SEL */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VSO_S */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_OCF_WS */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_DF_WS */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_TRK_DIS1 */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_TRK_DIS2 */
    {
        SPI_FRAME_ASIC_MSG_TX_4 ,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_TRK_DIS3 */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_TRK_DIS4 */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_FVPWR_ACT */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 9,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WLD_ON */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VPO1_ON */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VPO2_ON */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_E5VE_ON */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_HD_ON */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PD_ON */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS1_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS2_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS3_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS4_DIS */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 9,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PD_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WLD_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_E5VS_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_HD_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ISOK_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VSO_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VPO2_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_VPO1_CLR_FLT */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 9,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_CR_DIS34 */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_CR_DIS12 */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_CR_FB */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LLC */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_FDCL */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_DIDT */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_ICLAMP */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LF_PWM_58 */
    {
        SPI_FRAME_ASIC_MSG_TX_9,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LF_PWM_14 */
    {
        SPI_FRAME_ASIC_MSG_TX_9,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD1DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_10,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD2DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_11,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD3DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_12,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD4DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_13,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD5DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_14,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD6DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_15,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD7DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_16,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_LSD8DUTYCYCLE */
    {
        SPI_FRAME_ASIC_MSG_TX_17,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MR */
    {
        SPI_FRAME_ASIC_MSG_TX_18,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_S */
    {
        SPI_FRAME_ASIC_MSG_TX_19,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WSAI_S */
    {
        SPI_FRAME_ASIC_MSG_TX_19,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_CNT_EN */
    {
        SPI_FRAME_ASIC_MSG_TX_19,       /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_WS_CNT_RST */
    {
        SPI_FRAME_ASIC_MSG_TX_19,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_0 */
    {
        SPI_FRAME_ASIC_MSG_TX_0,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_1 */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_2 */
    {
        SPI_FRAME_ASIC_MSG_TX_2,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_3 */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_4 */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_5 */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_6 */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_7 */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_8 */
    {
        SPI_FRAME_ASIC_MSG_TX_8,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_9 */
    {
        SPI_FRAME_ASIC_MSG_TX_9,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_10 */
    {
        SPI_FRAME_ASIC_MSG_TX_10,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_11 */
    {
        SPI_FRAME_ASIC_MSG_TX_11,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_12 */
    {
        SPI_FRAME_ASIC_MSG_TX_12,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_13 */
    {
        SPI_FRAME_ASIC_MSG_TX_13,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_14 */
    {
        SPI_FRAME_ASIC_MSG_TX_14,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_15 */
    {
        SPI_FRAME_ASIC_MSG_TX_15,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_16 */
    {
        SPI_FRAME_ASIC_MSG_TX_16,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_17 */
    {
        SPI_FRAME_ASIC_MSG_TX_17,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_18 */
    {
        SPI_FRAME_ASIC_MSG_TX_18,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_19 */
    {
        SPI_FRAME_ASIC_MSG_TX_19,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_20 */
    {
        SPI_FRAME_ASIC_MSG_TX_20,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_21 */
    {
        SPI_FRAME_ASIC_MSG_TX_21,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_22 */
    {
        SPI_FRAME_ASIC_MSG_TX_22,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_23 */
    {
        SPI_FRAME_ASIC_MSG_TX_23,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_24 */
    {
        SPI_FRAME_ASIC_MSG_TX_24,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_25 */
    {
        SPI_FRAME_ASIC_MSG_TX_25,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_MSG_ID_26 */
    {
        SPI_FRAME_ASIC_MSG_TX_26,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_0 */
    {
        SPI_FRAME_ASIC_MSG_TX_0,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_1 */
    {
        SPI_FRAME_ASIC_MSG_TX_1,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_2 */
    {
        SPI_FRAME_ASIC_MSG_TX_2,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_3 */
    {
        SPI_FRAME_ASIC_MSG_TX_3,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_4 */
    {
        SPI_FRAME_ASIC_MSG_TX_4,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_5 */
    {
        SPI_FRAME_ASIC_MSG_TX_5,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_6 */
    {
        SPI_FRAME_ASIC_MSG_TX_6,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_7 */
    {
        SPI_FRAME_ASIC_MSG_TX_7,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_8 */
    {
        SPI_FRAME_ASIC_MSG_TX_8,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_9 */
    {
        SPI_FRAME_ASIC_MSG_TX_9,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_10 */
    {
        SPI_FRAME_ASIC_MSG_TX_10,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_11 */
    {
        SPI_FRAME_ASIC_MSG_TX_11,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_12 */
    {
        SPI_FRAME_ASIC_MSG_TX_12,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_13 */
    {
        SPI_FRAME_ASIC_MSG_TX_13,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_14 */
    {
        SPI_FRAME_ASIC_MSG_TX_14,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_15 */
    {
        SPI_FRAME_ASIC_MSG_TX_15,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_16 */
    {
        SPI_FRAME_ASIC_MSG_TX_16,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_17 */
    {
        SPI_FRAME_ASIC_MSG_TX_17,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_18 */
    {
        SPI_FRAME_ASIC_MSG_TX_18,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_19 */
    {
        SPI_FRAME_ASIC_MSG_TX_19,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_20 */
    {
        SPI_FRAME_ASIC_MSG_TX_20,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_21 */
    {
        SPI_FRAME_ASIC_MSG_TX_21,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_22 */
    {
        SPI_FRAME_ASIC_MSG_TX_22,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_23 */
    {
        SPI_FRAME_ASIC_MSG_TX_23,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_24 */
    {
        SPI_FRAME_ASIC_MSG_TX_24,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_25 */
    {
        SPI_FRAME_ASIC_MSG_TX_25,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_TX_PARITY_26 */
    {
        SPI_FRAME_ASIC_MSG_TX_26,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PD_MT_CFG */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_HPD_SR */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_RST_WD */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_RST_ALU */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_RST_EXT */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_RST_CLK */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VINT_UV */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VCC5_UV */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_DOSV_UV */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 9,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VERSION */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 4         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_E5VS_OC_CFG */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LDACT_DIS */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD_SINK_DIS */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ICHAR */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PCHAR */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 4         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_MANUFACTURING_DATA */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 4         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 9,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_OTW_OV */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FGND */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPWR_UV */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LD */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPWR_OV */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ASIC_CLK_CNT */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_TEMP */
    {
        SPI_FRAME_ASIC_MSG_RX_4,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VSO_SEL */
    {
        SPI_FRAME_ASIC_MSG_RX_4,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VSO_S */
    {
        SPI_FRAME_ASIC_MSG_RX_4,        /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FVPWR_ACT */
    {
        SPI_FRAME_ASIC_MSG_RX_4,        /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VINT_D */
    {
        SPI_FRAME_ASIC_MSG_RX_5,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_HD_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_5,        /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PD_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_5,        /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPRE10 */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_WLD */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WLD_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WLD_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WLD_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPRE12 */
    {
        SPI_FRAME_ASIC_MSG_RX_7,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_CR_DIS34 */
    {
        SPI_FRAME_ASIC_MSG_RX_7,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_CR_DIS12 */
    {
        SPI_FRAME_ASIC_MSG_RX_7,        /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_CR_FB */
    {
        SPI_FRAME_ASIC_MSG_RX_7,        /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VCP_VPWR */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD4_CRER */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD3_CRER */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD2_CRER */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD1_CRER */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VINT_A */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISOPOS4 */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISOPOS3 */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISOPOS2 */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISOPOS1 */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD1_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD1 */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD1_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD1_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD1_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD2_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD2 */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD2_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD2_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD2_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD3_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD3 */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD3_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD3_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD3_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD4_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD4 */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD4_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD4_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD4_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISOK_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISOK_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD5_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD5 */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD5_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD5_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD5_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISO_NEG2 */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISO_NEG1 */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD6_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD6 */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD6_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD6_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD6_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISO_NEG4 */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ISO_NEG3 */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD7_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD7 */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD7_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD7_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD7_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD8_DUTY */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_LSD8 */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD8_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD8_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_LSD8_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_AR */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_ERR_CNT */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_HD_LKG */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_E5VS_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_E5VS_OUV */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS_S */
    {
        SPI_FRAME_ASIC_MSG_RX_19,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WSAI_S */
    {
        SPI_FRAME_ASIC_MSG_RX_19,       /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 2         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS_CNT_OV */
    {
        SPI_FRAME_ASIC_MSG_RX_19,       /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS_CNT */
    {
        SPI_FRAME_ASIC_MSG_RX_19,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_NOTLEGAL */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_FAIL */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_STOP */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_DATA */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_LKG */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_NOTLEGALBITS */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1_CODEERROR */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS1MANCHESTERDECODINGRESULT */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 9         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_NOTLEGAL */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_FAIL */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_STOP */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_DATA */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_LKG */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_NOTLEGALBITS */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2_CODEERROR */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS2MANCHESTERDECODINGRESULT */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 9         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_NOTLEGAL */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_FAIL */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_STOP */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_DATA */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_LKG */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_NOTLEGALBITS */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3_CODEERROR */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS3MANCHESTERDECODINGRESULT */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 9         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_NOTLEGAL */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_FAIL */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_STOP */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_DATA */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 5         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_LKG */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_NOTLEGALBITS */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 3         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4_CODEERROR */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_WS4MANCHESTERDECODINGRESULT */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 9         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_AD_RST1 */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_VPO1 */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPO1_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPO1_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPO1_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_AD_RST2 */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_VPO2 */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPO2_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPO2_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VPO2_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_AD_RST3 */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 10        /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VDS_VSO */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 10,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VSO_OT */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VSO_OP */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_VSO_OC */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_0 */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_1 */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_2 */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_3 */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_4 */
    {
        SPI_FRAME_ASIC_MSG_RX_4,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_5 */
    {
        SPI_FRAME_ASIC_MSG_RX_5,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_6 */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_7 */
    {
        SPI_FRAME_ASIC_MSG_RX_7,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_8 */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_9 */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_10 */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_11 */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_12 */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_13 */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_14 */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_15 */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_16 */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_17 */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_18 */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_19 */
    {
        SPI_FRAME_ASIC_MSG_RX_19,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_20 */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_21 */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_22 */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_23 */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_24 */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_25 */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_FMSG_26 */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_0 */
    {
        SPI_FRAME_ASIC_MSG_RX_0,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_1 */
    {
        SPI_FRAME_ASIC_MSG_RX_1,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_2 */
    {
        SPI_FRAME_ASIC_MSG_RX_2,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_3 */
    {
        SPI_FRAME_ASIC_MSG_RX_3,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_4 */
    {
        SPI_FRAME_ASIC_MSG_RX_4,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_5 */
    {
        SPI_FRAME_ASIC_MSG_RX_5,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_6 */
    {
        SPI_FRAME_ASIC_MSG_RX_6,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_7 */
    {
        SPI_FRAME_ASIC_MSG_RX_7,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_8 */
    {
        SPI_FRAME_ASIC_MSG_RX_8,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_9 */
    {
        SPI_FRAME_ASIC_MSG_RX_9,        /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_10 */
    {
        SPI_FRAME_ASIC_MSG_RX_10,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_11 */
    {
        SPI_FRAME_ASIC_MSG_RX_11,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_12 */
    {
        SPI_FRAME_ASIC_MSG_RX_12,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_13 */
    {
        SPI_FRAME_ASIC_MSG_RX_13,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_14 */
    {
        SPI_FRAME_ASIC_MSG_RX_14,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_15 */
    {
        SPI_FRAME_ASIC_MSG_RX_15,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_16 */
    {
        SPI_FRAME_ASIC_MSG_RX_16,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_17 */
    {
        SPI_FRAME_ASIC_MSG_RX_17,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_18 */
    {
        SPI_FRAME_ASIC_MSG_RX_18,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_19 */
    {
        SPI_FRAME_ASIC_MSG_RX_19,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_20 */
    {
        SPI_FRAME_ASIC_MSG_RX_20,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_21 */
    {
        SPI_FRAME_ASIC_MSG_RX_21,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_22 */
    {
        SPI_FRAME_ASIC_MSG_RX_22,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_23 */
    {
        SPI_FRAME_ASIC_MSG_RX_23,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_24 */
    {
        SPI_FRAME_ASIC_MSG_RX_24,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_25 */
    {
        SPI_FRAME_ASIC_MSG_RX_25,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_ASIC_RX_PARITY_26 */
    {
        SPI_FRAME_ASIC_MSG_RX_26,       /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_ANGLE_DATA */
    {
        SPI_FRAME_MPS1_MSG_TX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_ANGLE_ADDR */
    {
        SPI_FRAME_MPS1_MSG_TX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 6         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_ANGLE_RW */
    {
        SPI_FRAME_MPS1_MSG_TX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_ERROR_DATA */
    {
        SPI_FRAME_MPS1_MSG_TX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_ERROR_ADDR */
    {
        SPI_FRAME_MPS1_MSG_TX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 6         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_ERROR_RW */
    {
        SPI_FRAME_MPS1_MSG_TX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_CONTROL_ERST */
    {
        SPI_FRAME_MPS1_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_CONTROL_PWR */
    {
        SPI_FRAME_MPS1_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_CONTROL_RPM */
    {
        SPI_FRAME_MPS1_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_CONTROL_ADDR */
    {
        SPI_FRAME_MPS1_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 6         /* BitLength */
    },
    /* SPI_SIG_MPS1_TX_CONTROL_RW */
    {
        SPI_FRAME_MPS1_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },

    /* SPI_SIG_MPS2_TX_ANGLE_DATA */
    {
        SPI_FRAME_MPS2_MSG_TX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_ANGLE_ADDR */
    {
        SPI_FRAME_MPS2_MSG_TX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 6         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_ANGLE_RW */
    {
        SPI_FRAME_MPS2_MSG_TX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_ERROR_DATA */
    {
        SPI_FRAME_MPS2_MSG_TX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 8         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_ERROR_ADDR */
    {
        SPI_FRAME_MPS2_MSG_TX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 6         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_ERROR_RW */
    {
        SPI_FRAME_MPS2_MSG_TX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_CONTROL_ERST */
    {
        SPI_FRAME_MPS2_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_CONTROL_PWR */
    {
        SPI_FRAME_MPS2_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_CONTROL_RPM */
    {
        SPI_FRAME_MPS2_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_CONTROL_ADDR */
    {
        SPI_FRAME_MPS2_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 6         /* BitLength */
    },
    /* SPI_SIG_MPS2_TX_CONTROL_RW */
    {
        SPI_FRAME_MPS2_MSG_TX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_ANGLE_P */
    {
        SPI_FRAME_MPS1_MSG_RX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_ANGLE */
    {
        SPI_FRAME_MPS1_MSG_RX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 13        /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_ANGLE_EF */
    {
        SPI_FRAME_MPS1_MSG_RX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_ERROR_P */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_BATD */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_MAGM */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_IERR */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_TRNO */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_TMP */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_EEP1 */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_EEP2 */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_ERROR_EF */
    {
        SPI_FRAME_MPS1_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_CONTROL_P */
    {
        SPI_FRAME_MPS1_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_ERST */
    {
        SPI_FRAME_MPS1_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_PWR */
    {
        SPI_FRAME_MPS1_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_RPM */
    {
        SPI_FRAME_MPS1_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS1_RX_CONTROL_EF */
    {
        SPI_FRAME_MPS1_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_ANGLE_P */
    {
        SPI_FRAME_MPS2_MSG_RX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_ANGLE */
    {
        SPI_FRAME_MPS2_MSG_RX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 13        /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_ANGLE_EF */
    {
        SPI_FRAME_MPS2_MSG_RX_ANGLE,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_ERROR_P */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_BATD */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_MAGM */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_IERR */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_TRNO */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_TMP */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_EEP1 */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_EEP2 */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_ERROR_EF */
    {
        SPI_FRAME_MPS2_MSG_RX_ERROR,    /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_CONTROL_P */
    {
        SPI_FRAME_MPS2_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_ERST */
    {
        SPI_FRAME_MPS2_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_PWR */
    {
        SPI_FRAME_MPS2_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_RPM */
    {
        SPI_FRAME_MPS2_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_MPS2_RX_CONTROL_EF */
    {
        SPI_FRAME_MPS2_MSG_RX_CONTROL,  /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_G0 */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_G1 */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_G2 */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_G3 */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_G4 */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_G5 */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_GATE_SEL_ID */
    {
        SPI_FRAME_VDR_MSG_TX_GATE_SEL,  /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 4         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_K0 */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_K1 */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_K2 */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_K3 */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_K4 */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_K5 */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_OLM */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_FAULT_MSK_ID */
    {
        SPI_FRAME_VDR_MSG_TX_FAULT_MSK, /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 4         /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_SG */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_SB0 */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_TON */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_TOF */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_NPD */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 8,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_RT */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 10,           /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_ADDR */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 12,           /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH0_CFCFG_ID */
    {
        SPI_FRAME_VDR_MSG_TX_CH0_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 15,           /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_SG */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_SB0 */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_TON */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_TOF */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_NPD */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 8,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_RT */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 10,           /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_ADDR */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 12,           /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH1_CFCFG_ID */
    {
        SPI_FRAME_VDR_MSG_TX_CH1_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 15,           /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_SG */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_SB0 */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_TON */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_TOF */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_NPD */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 8,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_RT */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 10,           /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_ADDR */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 12,           /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH2_CFCFG_ID */
    {
        SPI_FRAME_VDR_MSG_TX_CH2_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 15,           /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_SG */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_SB0 */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_TON */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_TOF */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_NPD */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 8,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_RT */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 10,           /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_ADDR */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 12,           /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH3_CFCFG_ID */
    {
        SPI_FRAME_VDR_MSG_TX_CH3_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 15,           /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_SG */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_SB0 */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_TON */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_TOF */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_NPD */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 8,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_RT */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 10,           /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_ADDR */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 12,           /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH4_CFCFG_ID */
    {
        SPI_FRAME_VDR_MSG_TX_CH4_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 15,           /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_SG */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_SB0 */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_TON */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_TOF */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_NPD */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 8,            /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_RT */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 10,           /* StartBit */
        (Spim_If_SignalBitType) 2             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_ADDR */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 12,           /* StartBit */
        (Spim_If_SignalBitType) 3             /* BitLength */
    },
    /* SPI_SIG_VDR_TX_CH5_CFCFG_ID */
    {
        SPI_FRAME_VDR_MSG_TX_CH5_FAULT_CFG,  /* FrameId */
        (Spim_If_SignalBitType) 15,           /* StartBit */
        (Spim_If_SignalBitType) 1             /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C0OL */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 0,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C0SB */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 1,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C0SG */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 2,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C1OL */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 3,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C1SB */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 4,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C1SG */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 5,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C2OL */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 6,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C2SB */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 7,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_C2SG */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 8,        /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_FR */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 11,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_OT */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 12,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_LR */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 13,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_UV */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 14,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_VDR_RX_FF */
    {
        SPI_FRAME_VDR_MSG_RX_FAULT,     /* FrameId */
        (Spim_If_SignalBitType) 15,       /* StartBit */
        (Spim_If_SignalBitType) 1         /* BitLength */
    },
    /* SPI_SIG_REG_RX_REV_MINOR */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_REV,    /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 4,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_REV_MAJOR */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_REV,    /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 4,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_ID */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_ID,     /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 8,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_IGN */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_STAT,   /* FrameId */
        (Spim_If_SignalBitType) 0,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_CANWU_L */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_STAT,   /* FrameId */
        (Spim_If_SignalBitType) 1,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_NMASK_VDD1_UV_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG1,   /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD_3_5_SEL */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG1,   /* FrameId */
        (Spim_If_SignalBitType) 7,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_TX_NMASK_VDD1_UV_OV */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG1,   /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_TX_VDD_3_5_SEL */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG1,   /* FrameId */
        (Spim_If_SignalBitType) 7,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_POST_RUN_RST */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_MASK_VBATP_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 5,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_EN_VDD5_OT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_EN_VDD3_5_OT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 7,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_TX_POST_RUN_RST */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 4,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_TX_MASK_VBATP_OV */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 5,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_TX_EN_VDD5_OT */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 6,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_TX_EN_VDD3_5_OT */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG2,   /* FrameId */
        (Spim_If_SignalBitType) 7,            /* StartBit */
        (Spim_If_SignalBitType) 1,            /* BitLength */
    },
    /* SPI_SIG_REG_RX_BG_ERR1 */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_BG_ERR2 */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_AVDD_VMON_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 2,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VCP12_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 3,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VCP12_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 4,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VCP17_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VPATP_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 6,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VBATP_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,    /* FrameId */
        (Spim_If_SignalBitType) 7,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD1_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD1_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD3_5_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 2,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD3_5_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 3,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD5_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 4,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD5_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD6_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 6,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD6_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,    /* FrameId */
        (Spim_If_SignalBitType) 7,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD_3_5_OT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD5_OT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VSOUT1_OT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 2,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VSOUT1_ILIM */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 3,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VSOUT1_OV */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 4,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VSOUT1_UV */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD3_5_ILIM */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 6,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD5_ILIM */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,  /* FrameId */
        (Spim_If_SignalBitType) 7,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_FAIL_CNT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_2,  /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 3,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_EE_CRC_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_2,  /* FrameId */
        (Spim_If_SignalBitType) 4,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_CFG_CRC_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_2,  /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_ABIST_RUN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,  /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_LBIST_RUN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,  /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_ABIST_UVOV_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,  /* FrameId */
        (Spim_If_SignalBitType) 2,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_ABIST_UVOV_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,  /* FrameId */
        (Spim_If_SignalBitType) 3,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_LBIST_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,  /* FrameId */
        (Spim_If_SignalBitType) 4,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_NRES_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,  /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_TRIM_ERR_VMON */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,  /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_ENDRV_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,  /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,  /* FrameId */
        (Spim_If_SignalBitType) 2,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_MCU_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,  /* FrameId */
        (Spim_If_SignalBitType) 3,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_LOCLK */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,  /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_SPI_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,  /* FrameId */
        (Spim_If_SignalBitType) 6,                /* StartBit */
        (Spim_If_SignalBitType) 2,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_FSM */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_5,  /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 3,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_CFG_LOCK */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_SAFE_LOCK_THR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 4,                /* BitLength */
    },
    /* SPI_SIG_REG_RX_SAFE_TO */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 5,                /* StartBit */
        (Spim_If_SignalBitType) 3,                /* BitLength */
    },
    /* SPI_SIG_REG_TX_CFG_LOCK */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 0,                /* StartBit */
        (Spim_If_SignalBitType) 1,                /* BitLength */
    },
    /* SPI_SIG_REG_TX_SAFE_LOCK_THR */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 1,                /* StartBit */
        (Spim_If_SignalBitType) 4,                /* BitLength */
    },
    /* SPI_SIG_REG_TX_SAFE_TO */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_CFG,     /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 3,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_ABIST_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_LBIST_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_EE_CRC_CHK */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_AUTO_BIST_DIS */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_BIST_DEG_CNT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 2,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_ABIST_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_LBIST_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_EE_CRC_CHK */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_AUTO_BIST_DIS */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_BIST_DEG_CNT */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_BIST_CTRL,   /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 2,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_EXIT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_EXIT_MASK */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 1,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_NO_ERROR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_ENABLE_DRV */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_CFG_CRC_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_EXIT */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_EXIT_MASK */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 1,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_NO_ERROR */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_ENABLE_DRV */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_CFG_CRC_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CHECK_CTRL,  /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD_3_5_SEL */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIS_NRES_MON */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_RST_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 3,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_IGN_PWRL */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_CFG */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_ERROR_CFG */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_NO_SAFE_TO */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_VDD_3_5_SEL */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIS_NRES_MON */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WD_RST_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 3,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_IGN_PWRL */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WD_CFG */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_ERROR_CFG */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_NO_SAFE_TO */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,    /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DEV_ERR_CNT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_STAT,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_FAIL */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_STAT,    /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_ERROR_PIN_FAIL */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_STAT,    /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DEV_ERR_CNT */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_STAT,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WD_FAIL */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_STAT,    /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_ERROR_PIN_FAIL */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_STAT,    /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_PWMH */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_PWM_H,   /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_PWMH */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_PWM_H,   /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_PWML */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_PWM_L,   /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_PWML */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_PWM_L,   /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_PWD_THR */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_PWD_THR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_PWD_THR */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_PWD_THR_CFG, /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_CFG_CRC */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CFG_CRC,     /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_CFG_CRC */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CFG_CRC,     /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_MUX_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_SPI_SDO */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_MUX_OUT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_INT_CON */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 3,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_MUX_CFG */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 2,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_MUX_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_SPI_SDO */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_MUX_OUT */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_INT_CON */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 3,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_MUX_CFG */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,      /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 2,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_DIAG_MUX_SEL */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_MUX_SEL,       /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_DIAG_MUX_SEL */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_MUX_SEL,       /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_TOKEN_SEED */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_TOKEN_FDBCK,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_FDBK */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_TOKEN_FDBCK,    /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WDT_TOKEN_SEED */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_TOKEN_FDBCK,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WDT_FDBK */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_TOKEN_FDBCK,    /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_RT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_WIN1_CFG,       /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WDT_RT */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_WIN1_CFG,       /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_RW */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_WIN2_CFG,       /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 5,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WDT_RW */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_WIN2_CFG,       /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 5,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_TOKEN */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_TOKEN_VALUE,    /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 4,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDFAIL_TH */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_TOKEN_VALUE,    /* FrameId */
        (Spim_If_SignalBitType) 7,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_TOKEN_EARLY */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_TIME_OUT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 1,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_SEQ_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 2,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_CFG_CHG */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 3,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WD_WRONG_CFG */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_TOKEN_ERR */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 5,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_ANSW_CNT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 6,                    /* StartBit */
        (Spim_If_SignalBitType) 2,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WDT_ANSW */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_ANSWER,         /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_VSOUT1_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_VDD5_EN */
    {
        SPI_FRAME_REG_MSG_RX_RD_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_VSOUT1_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 0,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_VDD5_EN */
    {
        SPI_FRAME_REG_MSG_TX_WR_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 4,                    /* StartBit */
        (Spim_If_SignalBitType) 1,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_SW_LOCK_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_SW_LOCK,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_SW_UNLOCK_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_SW_UNLOCK,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DEV_ID_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DEV_ID,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DEV_REV_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DEV_REV,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_DEV_CFG1_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG1,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DEV_CFG1_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DEV_CFG1,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_DEV_CFG2_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_DEV_CFG2,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DEV_CFG2_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DEV_CFG2,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_CAN_STBY_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_CAN_STBY,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_STAT_1_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_STAT_1,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_STAT_2_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_STAT_2,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_STAT_3_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_STAT_3,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_STAT_4_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_STAT_4,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_STAT_5_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_STAT_5,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_ERR_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_ERR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_ERR_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_ERR_STAT_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_STAT,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_ERR_STAT_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_ERR_STAT,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_PWD_THR_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_PWD_THR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_PWD_THR_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_PWD_THR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_CHECK_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_CHECK_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_CHECK_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CHECK_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_BIST_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_BIST_CTRL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_BIST_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_BIST_CTRL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_WDT_WIN1_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_WDT_WIN1_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_WDT_WIN1_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_WIN1_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_WDT_WIN2_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_WDT_WIN2_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_WDT_WIN2_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_WIN2_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_WDT_TOKEN_VALUE_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_WDT_TOKEN_VALUE,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_WDT_STATUS_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_WDT_ANSWER_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_ANSWER,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DEV_STAT_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DEV_STAT,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_VMON_STAT_1_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_VMON_STAT_1,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_VMON_STAT_2_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_VMON_STAT_2,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SENS_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SENS_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_FUNC_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_FUNC_CFG,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_FUNC_CFG_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_FUNC_CFG,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_CFG_CRC_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_CFG_CRC,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_CFG_CRC_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_CFG_CRC,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DIAG_CFG_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DIAG_CFG_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_DIAG_CFG_CTRL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_CFG_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_DIAG_MUX_SEL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_DIAG_MUX_SEL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_DIAG_MUX_SEL_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_DIAG_MUX_SEL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_ERR_PWM_H_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_ERR_PWM_H,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_ERR_PWM_H_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_PWM_H,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_SAFETY_ERR_PWM_L_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_SAFETY_ERR_PWM_L,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_SAFETY_ERR_PWM_L_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_SAFETY_ERR_PWM_L,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_RD_WDT_TOKEN_FDBCK_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_RD_WDT_TOKEN_FDBCK,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_TX_WR_WDT_TOKEN_FDBCK_CMD_P */
    {
        SPI_FRAME_REG_MSG_TX_WR_WDT_TOKEN_FDBCK,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_SW_LOCK_STAT */
    {
        SPI_FRAME_REG_MSG_RX_SW_LOCK,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_SW_UNLOCK_STAT */
    {
        SPI_FRAME_REG_MSG_RX_SW_UNLOCK,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DEV_ID_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_ID,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DEV_REV_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_REV,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_DEV_CFG1_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_DEV_CFG1,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DEV_CFG1_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG1,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_DEV_CFG2_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_DEV_CFG2,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DEV_CFG2_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_CFG2,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_CAN_STBY_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_CAN_STBY,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_STAT_1_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_1,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_STAT_2_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_2,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_STAT_3_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_3,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_STAT_4_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_4,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_STAT_5_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_STAT_5,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_ERR_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_ERR_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_ERR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_ERR_STAT_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_ERR_STAT,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_ERR_STAT_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_STAT,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_PWD_THR_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_PWD_THR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_PWD_THR_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_PWD_THR_CFG,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_CHECK_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CHECK_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_CHECK_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_CHECK_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_BIST_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_BIST_CTRL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_BIST_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_BIST_CTRL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_WDT_WIN1_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_WIN1_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_WDT_WIN1_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_WDT_WIN1_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_WDT_WIN2_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_WIN2_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_WDT_WIN2_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_WDT_WIN2_CFG,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_WDT_TOKEN_VALUE_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_TOKEN_VALUE,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_WDT_STATUS_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_STATUS,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_WDT_ANSWER_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_WDT_ANSWER,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DEV_STAT_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DEV_STAT,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_VMON_STAT_1_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_1,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_VMON_STAT_2_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_VMON_STAT_2,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SENS_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SENS_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SENS_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_FUNC_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_FUNC_CFG,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_FUNC_CFG_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_FUNC_CFG,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_CFG_CRC_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_CFG_CRC,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_CFG_CRC_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_CFG_CRC,         /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DIAG_CFG_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_CFG_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_DIAG_CFG_CTRL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_DIAG_CFG_CTRL,          /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_DIAG_MUX_SEL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_DIAG_MUX_SEL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_DIAG_MUX_SEL_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_DIAG_MUX_SEL,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_ERR_PWM_H_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_PWM_H,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_ERR_PWM_H_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_ERR_PWM_H,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_SAFETY_ERR_PWM_L_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_SAFETY_ERR_PWM_L,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_SAFETY_ERR_PWM_L_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_SAFETY_ERR_PWM_L,           /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_RD_WDT_TOKEN_FDBCK_STAT */
    {
        SPI_FRAME_REG_MSG_RX_RD_WDT_TOKEN_FDBCK,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
    /* SPI_SIG_REG_RX_WR_WDT_TOKEN_FDBCK_STAT */
    {
        SPI_FRAME_REG_MSG_RX_WR_WDT_TOKEN_FDBCK,            /* FrameId */
        (Spim_If_SignalBitType) 8,                    /* StartBit */
        (Spim_If_SignalBitType) 8,                    /* BitLength */
    },
 };


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
 Spim_If_FrameDataType Spim_If_FrameBuf[SPI_FRAME_MAX_NUM];

#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
