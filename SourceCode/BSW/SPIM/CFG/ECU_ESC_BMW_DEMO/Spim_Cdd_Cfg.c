/**
 * @defgroup Spim_Cdd_Cfg Spim_Cdd_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Cdd_Cfg.c
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Cdd.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Spi Driver Config Parameters */
 const Spim_Cdd_ChannelConfigType Spim_Cdd_ChannelConfig[] =
 {
    /* Spi Configuration for Vlave driver Channel 0 */
    {
        CsDrvType_ShortData,                /* Cs Drive Type */
        (Spim_Cdd_DataWidthType)16,         /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_High,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch10,                          /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x0U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x0U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x4U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
    /* Spi Configuration for 100 ASIC */
    {
        CsDrvType_ShortData,                /* Cs Drive Type */
        (Spim_Cdd_DataWidthType)32,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Enabled,                /* Pha */
        HwChannelType_QspiCh0,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch2,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x2U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* C */
        (Spim_Cdd_DelayParamType) 0x2U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x5U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x2U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* TRAIL */
        DMA_CH19,                           /* DmaChTx */
        DMA_CH20                            /* DmaChRx */
    },
    /* Spi Configuration for Motor Position Sensor TLE 5012 - 1 */
    {
        CsDrvType_ContinuousData,            /* Cs Drive Type */
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh1,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch10,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x4U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x1U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x1U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x3U,       /* TRAIL */
        DMA_CH27,                           /* DmaChTx */
        DMA_CH28                            /* DmaChRx */
    },
    /* Spi Configuration for Motor Position Sensor TLE 5012 - 2 */
    {
        CsDrvType_ContinuousData,            /* Cs Drive Type */
        (Spim_Cdd_DataWidthType)16,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                  /* Pol */
        ClockPhase_Enabled,                 /* Pha */
        HwChannelType_QspiCh1,              /* HwChannel */
        PortInputType_A,                    /* PortInput */
        HwCs_Ch5,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x4U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x3U,    /* C */
        (Spim_Cdd_DelayParamType) 0x1U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x1U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x7U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x1U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x3U,       /* TRAIL */
        DMA_CH27,                           /* DmaChTx */
        DMA_CH28                            /* DmaChRx */
    },
    /* Spi Configuration for Motor Gate Driver */
    {
        CsDrvType_ShortData,                /* Cs Drive Type */
        (Spim_Cdd_DataWidthType)24,           /* DataWidth */
        TransferStart_MSB,                  /* TransferStart */
        CsPol_LowActive,                    /* CsPol */
        ClockIdlePol_Low,                   /* Pol */
        ClockPhase_Disabled,                /* Pha */
        HwChannelType_QspiCh3,              /* HwChannel */
        PortInputType_C,                    /* PortInput */
        HwCs_Ch13,                           /* CsPin */
        (Spim_Cdd_BaudRateParamType) 0x0U,    /* TQ */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* Q */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* A */
        (Spim_Cdd_BaudRateParamType) 0x2U,    /* B */
        (Spim_Cdd_BaudRateParamType) 0x1U,    /* C */
        (Spim_Cdd_DelayParamType) 0x2U,       /* IPRE */
        (Spim_Cdd_DelayParamType) 0x5U,       /* IDLE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* LEAD */
        (Spim_Cdd_DelayParamType) 0x2U,       /* TPRE */
        (Spim_Cdd_DelayParamType) 0x2U,       /* TRAIL */
        DMA_CH25,                           /* DmaChTx */
        DMA_CH26                            /* DmaChRx */
    },
 };

 const Spim_Cdd_ChannelType \
    Spim_Cdd_SeqChAssign[Spim_Cdd_SpiSequence_MaxNum][Spim_Cdd_ChannelAssign_MaxNum] =
 {
    /* Channel asignment for Sequence 0 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_100Asic,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 1 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Mps_Tle5012_1,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
    /* Channel asignment for Sequence 2 */
    /* Channels must be listed in a scheduled order */
    {
        Spim_Cdd_SpiChannel_Mtr,
        Spim_Cdd_SpiChannel_Vdr_A3944,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null,
        Spim_Cdd_SpiChannel_Null
    },
 };


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
