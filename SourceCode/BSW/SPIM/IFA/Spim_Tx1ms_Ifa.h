/**
 * @defgroup Spim_Tx1ms_Ifa Spim_Tx1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Tx1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_TX1MS_IFA_H_
#define SPIM_TX1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicSeedKeyInfo(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicSeedKeyInfo; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_HpdSr(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.HpdSr; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_PdMtCfg(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.PdMtCfg; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Pchar(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Pchar; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Ichar(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Ichar; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_LsdSinkDis(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.LsdSinkDis; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_LdactDis(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.LdactDis; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_E5vsOcCfg(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.E5vsOcCfg; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_IsoFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.IsoFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_IsoDet(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.IsoDet; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_FmAmp(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.FmAmp; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_FmEn(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.FmEn; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Stopclk2(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Stopclk2; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Adin2En(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Adin2En; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Adin1Dis(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Adin1Dis; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_OcfPd(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.OcfPd; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_FvpwrAct(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.FvpwrAct; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_DfWs(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.DfWs; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_OcfWs(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.OcfWs; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Iclamp(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Iclamp; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Didt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Didt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Fdcl(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Fdcl; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Llc(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Llc; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_CrFb(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.CrFb; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_CrDis12(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.CrDis12; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_CrDis34(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.CrDis34; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_LfPwm14(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.LfPwm14; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_LfPwm58(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.LfPwm58; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsaiS(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsaiS; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsS(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsS; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Wscfg1(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Wscfg1; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Wscfg2(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Wscfg2; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Wscfg3(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Wscfg3; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Wscfg4(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Wscfg4; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsTrkDis1(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsTrkDis1; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsTrkDis2(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsTrkDis2; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsTrkDis3(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsTrkDis3; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsTrkDis4(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsTrkDis4; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsCntRst(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsCntRst; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsCntEn(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsCntEn; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_VsoSel(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.VsoSel; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_VsoS(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.VsoS; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Vpo1On(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Vpo1On; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_PdClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.PdClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WldClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WldClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_E5vsClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.E5vsClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_WsClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.WsClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_HdClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.HdClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_IsokClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.IsokClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_VsoClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.VsoClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Vpo2ClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Vpo2ClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_Vpo1ClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.Vpo1ClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo_LsdClrFlt(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicInfo.LsdClrFlt; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicSeedKeyInfo_Sed(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicSeedKeyInfo_Mr(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicSeedKeyInfo.Mr; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_PdOn(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.PdOn; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd5dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd5dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd1dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd1dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd6dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd6dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd2dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd2dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd7dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd7dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd3dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd3dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd8dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd8dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_Lsd4dutycycle(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.Lsd4dutycycle; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo_HdOn(data) \
{ \
    *data = Spim_Tx1msSpiTxAsicActInfo.HdOn; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msEcuModeSts(data) \
{ \
    *data = Spim_Tx1msEcuModeSts; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msAsicSchTblNum(data) \
{ \
    *data = Spim_Tx1msAsicSchTblNum; \
}
#define Spim_Tx1ms_Read_Spim_Tx1msFuncInhibitSpimSts(data) \
{ \
    *data = Spim_Tx1msFuncInhibitSpimSts; \
}

/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_TX1MS_IFA_H_ */
/** @} */
