/**
 * @defgroup SpiCdd_Ifa SpiCdd_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SpiCdd_Ifa.h
 * @brief       Template file
 * @date        2014. 11. 17.
 ******************************************************************************/

#ifndef SPICDD_IFA_H_
#define SPICDD_IFA_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "IfxQspi_reg.h"
#include "IfxSrc_reg.h"
#include "Mcal_DmaLib.h"
#include "Irq_Cfg.h"
#include "DetErr.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define Ifa_Mcal_ResetENDINIT()			Mcal_ResetENDINIT()
#define Ifa_Mcal_SetENDINIT()			Mcal_SetENDINIT()
#define Ifa_Mcal_DmaEnableHwTransfer(Channel)	\
							Mcal_DmaEnableHwTransfer(Channel)
#define Ifa_Mcal_SetAtomic(Address, Value, Offset, Bits)	\
							Mcal_SetAtomic(Address, Value, Offset, Bits)
#define Ifa_Dma_DisableHwTransaction(Channel)	\
							Dma_DisableHwTransaction(Channel)
#define Ifa_DetErr_Report(ModuleId, InstanceId, ApiId, ErrorId)	\
							DetErr_Report(ModuleId, InstanceId, ApiId, ErrorId)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPICDD_IFA_H_ */
/** @} */
