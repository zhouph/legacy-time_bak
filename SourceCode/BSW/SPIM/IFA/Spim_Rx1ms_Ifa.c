/**
 * @defgroup Spim_Rx1ms_Ifa Spim_Rx1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx1ms_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Rx1ms_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPIM_RX1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_CODE
#include "Spim_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SPIM_RX1MS_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
