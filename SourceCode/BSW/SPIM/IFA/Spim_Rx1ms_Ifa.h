/**
 * @defgroup Spim_Rx1ms_Ifa Spim_Rx1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_RX1MS_IFA_H_
#define SPIM_RX1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Spim_Rx1ms_Read_Spim_Rx1msEcuModeSts(data) \
{ \
    *data = Spim_Rx1msEcuModeSts; \
}
#define Spim_Rx1ms_Read_Spim_Rx1msFuncInhibitSpimSts(data) \
{ \
    *data = Spim_Rx1msFuncInhibitSpimSts; \
}

/* Set Output DE MAcro Function */
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo(data) \
{ \
    Spim_Rx1msRxAsicInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo(data) \
{ \
    Spim_Rx1msRxMpsInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo(data) \
{ \
    Spim_Rx1msRxVlvdInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo(data) \
{ \
    Spim_Rx1msRxRegInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo(data) \
{ \
    Spim_Rx1msRxMgdInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1AngInfo(data) \
{ \
    Spim_Rx1msRxMpsD1AngInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1SafeWrdInfo(data) \
{ \
    Spim_Rx1msRxMpsD1SafeWrdInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1StatusInfo(data) \
{ \
    Spim_Rx1msRxMpsD1StatusInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2AngInfo(data) \
{ \
    Spim_Rx1msRxMpsD2AngInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2SafeWrdInfo(data) \
{ \
    Spim_Rx1msRxMpsD2SafeWrdInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2StatusInfo(data) \
{ \
    Spim_Rx1msRxMpsD2StatusInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdFFInfo(data) \
{ \
    Spim_Rx1msRxVlvdFFInfo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_PdMtCfg(data) \
{ \
    Spim_Rx1msRxAsicInfo.PdMtCfg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_HpdSr(data) \
{ \
    Spim_Rx1msRxAsicInfo.HpdSr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_RstWd(data) \
{ \
    Spim_Rx1msRxAsicInfo.RstWd = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_RstAlu(data) \
{ \
    Spim_Rx1msRxAsicInfo.RstAlu = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_RstExt(data) \
{ \
    Spim_Rx1msRxAsicInfo.RstExt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_RstClk(data) \
{ \
    Spim_Rx1msRxAsicInfo.RstClk = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VintUv(data) \
{ \
    Spim_Rx1msRxAsicInfo.VintUv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vcc5Uv(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vcc5Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_DosvUv(data) \
{ \
    Spim_Rx1msRxAsicInfo.DosvUv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Version(data) \
{ \
    Spim_Rx1msRxAsicInfo.Version = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_E5vsOcCfg(data) \
{ \
    Spim_Rx1msRxAsicInfo.E5vsOcCfg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_LdactDis(data) \
{ \
    Spim_Rx1msRxAsicInfo.LdactDis = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_LsdSinkDis(data) \
{ \
    Spim_Rx1msRxAsicInfo.LsdSinkDis = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ichar(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ichar = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Pchar(data) \
{ \
    Spim_Rx1msRxAsicInfo.Pchar = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_ManufacturingData(data) \
{ \
    Spim_Rx1msRxAsicInfo.ManufacturingData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_OtwOv(data) \
{ \
    Spim_Rx1msRxAsicInfo.OtwOv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fgnd(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fgnd = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VpwrUv(data) \
{ \
    Spim_Rx1msRxAsicInfo.VpwrUv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ld(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ld = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VpwrOv(data) \
{ \
    Spim_Rx1msRxAsicInfo.VpwrOv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_AsicClkCnt(data) \
{ \
    Spim_Rx1msRxAsicInfo.AsicClkCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Temp(data) \
{ \
    Spim_Rx1msRxAsicInfo.Temp = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VsoSel(data) \
{ \
    Spim_Rx1msRxAsicInfo.VsoSel = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VsoS(data) \
{ \
    Spim_Rx1msRxAsicInfo.VsoS = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_FvpwrAct(data) \
{ \
    Spim_Rx1msRxAsicInfo.FvpwrAct = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VintD(data) \
{ \
    Spim_Rx1msRxAsicInfo.VintD = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_HdOc(data) \
{ \
    Spim_Rx1msRxAsicInfo.HdOc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_PdOc(data) \
{ \
    Spim_Rx1msRxAsicInfo.PdOc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpre10(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpre10 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsWld(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsWld = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WldOt(data) \
{ \
    Spim_Rx1msRxAsicInfo.WldOt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WldOp(data) \
{ \
    Spim_Rx1msRxAsicInfo.WldOp = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WldOc(data) \
{ \
    Spim_Rx1msRxAsicInfo.WldOc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpre12(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpre12 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_CrDis34(data) \
{ \
    Spim_Rx1msRxAsicInfo.CrDis34 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_CrDis12(data) \
{ \
    Spim_Rx1msRxAsicInfo.CrDis12 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_CrFb(data) \
{ \
    Spim_Rx1msRxAsicInfo.CrFb = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VcpVpwr(data) \
{ \
    Spim_Rx1msRxAsicInfo.VcpVpwr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd4Crer(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd4Crer = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd3Crer(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd3Crer = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd2Crer(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd2Crer = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd1Crer(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd1Crer = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VintA(data) \
{ \
    Spim_Rx1msRxAsicInfo.VintA = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Isopos4(data) \
{ \
    Spim_Rx1msRxAsicInfo.Isopos4 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Isopos3(data) \
{ \
    Spim_Rx1msRxAsicInfo.Isopos3 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Isopos2(data) \
{ \
    Spim_Rx1msRxAsicInfo.Isopos2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Isopos1(data) \
{ \
    Spim_Rx1msRxAsicInfo.Isopos1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd1Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd1Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd1(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd1Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd1Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd1Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd1Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd1Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd1Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd2Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd2Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd2(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd2Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd2Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd2Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd2Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd2Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd2Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd3Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd3Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd3(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd3 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd3Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd3Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd3Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd3Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd3Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd3Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd4Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd4Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd4(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd4 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd4Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd4Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd4Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd4Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd4Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd4Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_IsokOt(data) \
{ \
    Spim_Rx1msRxAsicInfo.IsokOt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_IsokOc(data) \
{ \
    Spim_Rx1msRxAsicInfo.IsokOc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd5Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd5Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd5(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd5 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd5Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd5Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd5Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd5Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd5Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd5Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_IsoNeg2(data) \
{ \
    Spim_Rx1msRxAsicInfo.IsoNeg2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_IsoNeg1(data) \
{ \
    Spim_Rx1msRxAsicInfo.IsoNeg1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd6Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd6Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd6(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd6 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd6Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd6Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd6Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd6Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd6Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd6Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_IsoNeg4(data) \
{ \
    Spim_Rx1msRxAsicInfo.IsoNeg4 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_IsoNeg3(data) \
{ \
    Spim_Rx1msRxAsicInfo.IsoNeg3 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd7Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd7Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd7(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd7 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd7Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd7Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd7Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd7Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd7Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd7Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd8Duty(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd8Duty = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsLsd8(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsLsd8 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd8Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd8Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd8Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd8Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Lsd8Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Lsd8Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ar(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ar = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_ErrCnt(data) \
{ \
    Spim_Rx1msRxAsicInfo.ErrCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_HdLkg(data) \
{ \
    Spim_Rx1msRxAsicInfo.HdLkg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_E5vsOc(data) \
{ \
    Spim_Rx1msRxAsicInfo.E5vsOc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_E5vsOuv(data) \
{ \
    Spim_Rx1msRxAsicInfo.E5vsOuv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WsS(data) \
{ \
    Spim_Rx1msRxAsicInfo.WsS = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WsaiS(data) \
{ \
    Spim_Rx1msRxAsicInfo.WsaiS = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WsCntOv(data) \
{ \
    Spim_Rx1msRxAsicInfo.WsCntOv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_WsCnt(data) \
{ \
    Spim_Rx1msRxAsicInfo.WsCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Notlegal(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Notlegal = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Fail(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Fail = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Stop(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Stop = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Data(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Data = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Lkg(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Lkg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Notlegalbits(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Notlegalbits = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1Codeerror(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1Codeerror = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws1manchesterdecodingresult(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws1manchesterdecodingresult = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Notlegal(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Notlegal = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Fail(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Fail = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Stop(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Stop = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Data(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Data = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Lkg(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Lkg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Notlegalbits(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Notlegalbits = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2Codeerror(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2Codeerror = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws2manchesterdecodingresult(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws2manchesterdecodingresult = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Notlegal(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Notlegal = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Fail(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Fail = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Stop(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Stop = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Data(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Data = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Lkg(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Lkg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Notlegalbits(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Notlegalbits = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3Codeerror(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3Codeerror = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws3manchesterdecodingresult(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws3manchesterdecodingresult = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Notlegal(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Notlegal = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Fail(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Fail = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Stop(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Stop = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Data(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Data = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Lkg(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Lkg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Notlegalbits(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Notlegalbits = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4Codeerror(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4Codeerror = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Ws4manchesterdecodingresult(data) \
{ \
    Spim_Rx1msRxAsicInfo.Ws4manchesterdecodingresult = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_AdRst1(data) \
{ \
    Spim_Rx1msRxAsicInfo.AdRst1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsVpo1(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsVpo1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpo1Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpo1Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpo1Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpo1Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpo1Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpo1Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_AdRst2(data) \
{ \
    Spim_Rx1msRxAsicInfo.AdRst2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsVpo2(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsVpo2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpo2Ot(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpo2Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpo2Op(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpo2Op = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Vpo2Oc(data) \
{ \
    Spim_Rx1msRxAsicInfo.Vpo2Oc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_AdRst3(data) \
{ \
    Spim_Rx1msRxAsicInfo.AdRst3 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VdsVso(data) \
{ \
    Spim_Rx1msRxAsicInfo.VdsVso = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VsoOt(data) \
{ \
    Spim_Rx1msRxAsicInfo.VsoOt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VsoOp(data) \
{ \
    Spim_Rx1msRxAsicInfo.VsoOp = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_VsoOc(data) \
{ \
    Spim_Rx1msRxAsicInfo.VsoOc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg0(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg0 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg1(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg2(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg3(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg3 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg4(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg4 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg5(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg5 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg6(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg6 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg7(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg7 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg8(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg8 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg9(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg9 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg10(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg10 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg11(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg11 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg12(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg12 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg13(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg13 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg14(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg14 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg15(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg15 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg16(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg16 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg17(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg17 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg18(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg18 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg19(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg19 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg20(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg20 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg21(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg21 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg22(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg22 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg23(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg23 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg24(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg24 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg25(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg25 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo_Fmsg26(data) \
{ \
    Spim_Rx1msRxAsicInfo.Fmsg26 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1AngleP(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1AngleP = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1AngleEf(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1AngleEf = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1ErrorP(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1ErrorP = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Batd(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Batd = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Magm(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Magm = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Ierr(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Ierr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Trno(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Trno = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Tmp(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Tmp = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Eep1(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Eep1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Eep2(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Eep2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1ErrorEf(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1ErrorEf = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1ControlP(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1ControlP = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Erst(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Erst = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Pwr(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Pwr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1Rpm(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1Rpm = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps1ControlEf(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps1ControlEf = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2AngleP(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2AngleP = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2AngleEf(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2AngleEf = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2ErrorP(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2ErrorP = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Batd(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Batd = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Magm(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Magm = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Ierr(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Ierr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Trno(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Trno = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Tmp(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Tmp = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Eep1(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Eep1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Eep2(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Eep2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2ErrorEf(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2ErrorEf = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2ControlP(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2ControlP = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Erst(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Erst = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Pwr(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Pwr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2Rpm(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2Rpm = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo_Mps2ControlEf(data) \
{ \
    Spim_Rx1msRxMpsInfo.Mps2ControlEf = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C0ol(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C0ol = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C0sb(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C0sb = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C0sg(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C0sg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C1ol(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C1ol = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C1sb(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C1sb = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C1sg(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C1sg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C2ol(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C2ol = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C2sb(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C2sb = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_C2sg(data) \
{ \
    Spim_Rx1msRxVlvdInfo.C2sg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_Fr(data) \
{ \
    Spim_Rx1msRxVlvdInfo.Fr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_Ot(data) \
{ \
    Spim_Rx1msRxVlvdInfo.Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_Lr(data) \
{ \
    Spim_Rx1msRxVlvdInfo.Lr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_Uv(data) \
{ \
    Spim_Rx1msRxVlvdInfo.Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo_Ff(data) \
{ \
    Spim_Rx1msRxVlvdInfo.Ff = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RevMinor(data) \
{ \
    Spim_Rx1msRxRegInfo.RevMinor = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RevMajor(data) \
{ \
    Spim_Rx1msRxRegInfo.RevMajor = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Id(data) \
{ \
    Spim_Rx1msRxRegInfo.Id = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Ign(data) \
{ \
    Spim_Rx1msRxRegInfo.Ign = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_CanwuL(data) \
{ \
    Spim_Rx1msRxRegInfo.CanwuL = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_NmaskVdd1UvOv(data) \
{ \
    Spim_Rx1msRxRegInfo.NmaskVdd1UvOv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd35Sel(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd35Sel = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_PostRunRst(data) \
{ \
    Spim_Rx1msRxRegInfo.PostRunRst = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_MaskVbatpOv(data) \
{ \
    Spim_Rx1msRxRegInfo.MaskVbatpOv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_EnVdd5Ot(data) \
{ \
    Spim_Rx1msRxRegInfo.EnVdd5Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_EnVdd35Ot(data) \
{ \
    Spim_Rx1msRxRegInfo.EnVdd35Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_BgErr1(data) \
{ \
    Spim_Rx1msRxRegInfo.BgErr1 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_BgErr2(data) \
{ \
    Spim_Rx1msRxRegInfo.BgErr2 = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_AvddVmonErr(data) \
{ \
    Spim_Rx1msRxRegInfo.AvddVmonErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vcp12Uv(data) \
{ \
    Spim_Rx1msRxRegInfo.Vcp12Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vcp12Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vcp12Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vcp17Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vcp17Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_VpatpUv(data) \
{ \
    Spim_Rx1msRxRegInfo.VpatpUv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_VbatpOv(data) \
{ \
    Spim_Rx1msRxRegInfo.VbatpOv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd1Uv(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd1Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd1Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd1Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd35Uv(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd35Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd35Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd35Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd5Uv(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd5Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd5Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd5Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd6Uv(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd6Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd6Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd6Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd35Ot(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd35Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd5Ot(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd5Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vsout1Ot(data) \
{ \
    Spim_Rx1msRxRegInfo.Vsout1Ot = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vsout1Ilim(data) \
{ \
    Spim_Rx1msRxRegInfo.Vsout1Ilim = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vsout1Ov(data) \
{ \
    Spim_Rx1msRxRegInfo.Vsout1Ov = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vsout1Uv(data) \
{ \
    Spim_Rx1msRxRegInfo.Vsout1Uv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd35Ilim(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd35Ilim = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd5Ilim(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd5Ilim = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdFailCnt(data) \
{ \
    Spim_Rx1msRxRegInfo.WdFailCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_EeCrcErr(data) \
{ \
    Spim_Rx1msRxRegInfo.EeCrcErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_CfgCrcErr(data) \
{ \
    Spim_Rx1msRxRegInfo.CfgCrcErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_AbistRun(data) \
{ \
    Spim_Rx1msRxRegInfo.AbistRun = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_LbistRun(data) \
{ \
    Spim_Rx1msRxRegInfo.LbistRun = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_AbistUvovErr(data) \
{ \
    Spim_Rx1msRxRegInfo.AbistUvovErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_LbistErr(data) \
{ \
    Spim_Rx1msRxRegInfo.LbistErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_NresErr(data) \
{ \
    Spim_Rx1msRxRegInfo.NresErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_TrimErrVmon(data) \
{ \
    Spim_Rx1msRxRegInfo.TrimErrVmon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_EndrvErr(data) \
{ \
    Spim_Rx1msRxRegInfo.EndrvErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdErr(data) \
{ \
    Spim_Rx1msRxRegInfo.WdErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_McuErr(data) \
{ \
    Spim_Rx1msRxRegInfo.McuErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Loclk(data) \
{ \
    Spim_Rx1msRxRegInfo.Loclk = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_SpiErr(data) \
{ \
    Spim_Rx1msRxRegInfo.SpiErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Fsm(data) \
{ \
    Spim_Rx1msRxRegInfo.Fsm = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_CfgLock(data) \
{ \
    Spim_Rx1msRxRegInfo.CfgLock = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_SafeLockThr(data) \
{ \
    Spim_Rx1msRxRegInfo.SafeLockThr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_SafeTo(data) \
{ \
    Spim_Rx1msRxRegInfo.SafeTo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_AbistEn(data) \
{ \
    Spim_Rx1msRxRegInfo.AbistEn = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_LbistEn(data) \
{ \
    Spim_Rx1msRxRegInfo.LbistEn = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_EeCrcChk(data) \
{ \
    Spim_Rx1msRxRegInfo.EeCrcChk = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_AutoBistDis(data) \
{ \
    Spim_Rx1msRxRegInfo.AutoBistDis = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_BistDegCnt(data) \
{ \
    Spim_Rx1msRxRegInfo.BistDegCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagExit(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagExit = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagExitMask(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagExitMask = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_NoError(data) \
{ \
    Spim_Rx1msRxRegInfo.NoError = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_EnableDrv(data) \
{ \
    Spim_Rx1msRxRegInfo.EnableDrv = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_CfgCrcEn(data) \
{ \
    Spim_Rx1msRxRegInfo.CfgCrcEn = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DisNresMon(data) \
{ \
    Spim_Rx1msRxRegInfo.DisNresMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdRstEn(data) \
{ \
    Spim_Rx1msRxRegInfo.WdRstEn = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_IgnPwrl(data) \
{ \
    Spim_Rx1msRxRegInfo.IgnPwrl = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdCfg(data) \
{ \
    Spim_Rx1msRxRegInfo.WdCfg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_ErrorCfg(data) \
{ \
    Spim_Rx1msRxRegInfo.ErrorCfg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_NoSafeTo(data) \
{ \
    Spim_Rx1msRxRegInfo.NoSafeTo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DevErrCnt(data) \
{ \
    Spim_Rx1msRxRegInfo.DevErrCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdFail(data) \
{ \
    Spim_Rx1msRxRegInfo.WdFail = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_ErrorPinFail(data) \
{ \
    Spim_Rx1msRxRegInfo.ErrorPinFail = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Pwmh(data) \
{ \
    Spim_Rx1msRxRegInfo.Pwmh = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Pwml(data) \
{ \
    Spim_Rx1msRxRegInfo.Pwml = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_PwdThr(data) \
{ \
    Spim_Rx1msRxRegInfo.PwdThr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_CfgCrc(data) \
{ \
    Spim_Rx1msRxRegInfo.CfgCrc = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagMuxEn(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagMuxEn = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagSpiSdo(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagSpiSdo = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagMuxOut(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagMuxOut = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagIntCon(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagIntCon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagMuxCfg(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagMuxCfg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_DiagMuxSel(data) \
{ \
    Spim_Rx1msRxRegInfo.DiagMuxSel = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdtTokenSeed(data) \
{ \
    Spim_Rx1msRxRegInfo.WdtTokenSeed = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdtFdbk(data) \
{ \
    Spim_Rx1msRxRegInfo.WdtFdbk = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdtRt(data) \
{ \
    Spim_Rx1msRxRegInfo.WdtRt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdtRw(data) \
{ \
    Spim_Rx1msRxRegInfo.WdtRw = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdtToken(data) \
{ \
    Spim_Rx1msRxRegInfo.WdtToken = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdfailTh(data) \
{ \
    Spim_Rx1msRxRegInfo.WdfailTh = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_TokenEarly(data) \
{ \
    Spim_Rx1msRxRegInfo.TokenEarly = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_TimeOut(data) \
{ \
    Spim_Rx1msRxRegInfo.TimeOut = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_SeqErr(data) \
{ \
    Spim_Rx1msRxRegInfo.SeqErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdCfgChg(data) \
{ \
    Spim_Rx1msRxRegInfo.WdCfgChg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdWrongCfg(data) \
{ \
    Spim_Rx1msRxRegInfo.WdWrongCfg = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_TokenErr(data) \
{ \
    Spim_Rx1msRxRegInfo.TokenErr = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WdtAnswCnt(data) \
{ \
    Spim_Rx1msRxRegInfo.WdtAnswCnt = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vsout1En(data) \
{ \
    Spim_Rx1msRxRegInfo.Vsout1En = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_Vdd5En(data) \
{ \
    Spim_Rx1msRxRegInfo.Vdd5En = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_SwLockStat(data) \
{ \
    Spim_Rx1msRxRegInfo.SwLockStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_SwUnlockStat(data) \
{ \
    Spim_Rx1msRxRegInfo.SwUnlockStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDevIdStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDevIdStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDevRevStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDevRevStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrDevCfg1Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrDevCfg1Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDevCfg1Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDevCfg1Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrDevCfg2Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrDevCfg2Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDevCfg2Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDevCfg2Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrCanStbyStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrCanStbyStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyStat1Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyStat1Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyStat2Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyStat2Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyStat3Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyStat3Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyStat4Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyStat4Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyStat5Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyStat5Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyErrCfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyErrCfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyErrCfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyErrCfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyErrStatStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyErrStatStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyErrStatStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyErrStatStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyPwdThrCfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyPwdThrCfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyPwdThrCfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyPwdThrCfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyCheckCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyCheckCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyCheckCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyCheckCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyBistCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyBistCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyBistCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyBistCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdWdtWin1CfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdWdtWin1CfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrWdtWin1CfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrWdtWin1CfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdWdtWin2CfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdWdtWin2CfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrWdtWin2CfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrWdtWin2CfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdWdtTokenValueStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdWdtTokenValueStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdWdtStatusStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdWdtStatusStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrWdtAnswerStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrWdtAnswerStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDevStatStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDevStatStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdVmonStat1Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdVmonStat1Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdVmonStat2Stat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdVmonStat2Stat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSensCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSensCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSensCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSensCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyFuncCfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyFuncCfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyFuncCfgStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyFuncCfgStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyCfgCrcStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyCfgCrcStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyCfgCrcStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyCfgCrcStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDiagCfgCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDiagCfgCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrDiagCfgCtrlStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrDiagCfgCtrlStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdDiagMuxSelStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdDiagMuxSelStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrDiagMuxSelStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrDiagMuxSelStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyErrPwmHStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyErrPwmHStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyErrPwmHStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyErrPwmHStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdSafetyErrPwmLStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdSafetyErrPwmLStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrSafetyErrPwmLStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrSafetyErrPwmLStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_RdWdtTokenFdbckStat(data) \
{ \
    Spim_Rx1msRxRegInfo.RdWdtTokenFdbckStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo_WrWdtTokenFdbckStat(data) \
{ \
    Spim_Rx1msRxRegInfo.WrWdtTokenFdbckStat = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_CspMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.CspMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_FlOvCurrMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.FlOvCurrMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_FlIvDutyMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.FlIvDutyMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_FrOvCurrMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.FrOvCurrMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_FrIvDutyMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.FrIvDutyMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_RlOvCurrMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.RlOvCurrMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_RlIvDutyMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.RlIvDutyMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_RrOvCurrMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.RrOvCurrMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_RrIvDutyMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.RrIvDutyMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo_Pdf5vMon(data) \
{ \
    Spim_Rx1msRxAsicPhyInfo.Pdf5vMon = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_ErrOverViewRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.ErrOverViewRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_SpecialEventRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.SpecialEventRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_InterErr1RawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.InterErr1RawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_InterErr2RawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.InterErr2RawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_ShutDownErrRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.ShutDownErrRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_InputPatternViolateRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.InputPatternViolateRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_OutStageFeedErrRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.OutStageFeedErrRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_SpiCommConfigErrRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.SpiCommConfigErrRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo_SpiSTATRawData(data) \
{ \
    Spim_Rx1msRxMgdInfo.SpiSTATRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1AngInfo_D1SpiAngRawData(data) \
{ \
    Spim_Rx1msRxMpsD1AngInfo.D1SpiAngRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1SafeWrdInfo_D1SafeWrdRawData(data) \
{ \
    Spim_Rx1msRxMpsD1SafeWrdInfo.D1SafeWrdRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1StatusInfo_D1StatusRawData(data) \
{ \
    Spim_Rx1msRxMpsD1StatusInfo.D1StatusRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2AngInfo_D2SpiAngRawData(data) \
{ \
    Spim_Rx1msRxMpsD2AngInfo.D2SpiAngRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2SafeWrdInfo_D2SafeWrdRawData(data) \
{ \
    Spim_Rx1msRxMpsD2SafeWrdInfo.D2SafeWrdRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2StatusInfo_D2StatusRawData(data) \
{ \
    Spim_Rx1msRxMpsD2StatusInfo.D2StatusRawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdFFInfo_FF0RawData(data) \
{ \
    Spim_Rx1msRxVlvdFFInfo.FF0RawData = *data; \
}
#define Spim_Rx1ms_Write_Spim_Rx1msRxVlvdFFInfo_FF1RawData(data) \
{ \
    Spim_Rx1msRxVlvdFFInfo.FF1RawData = *data; \
}
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_RX1MS_IFA_H_ */
/** @} */
