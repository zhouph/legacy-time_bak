/**
 * @defgroup SpiSch_Ifa SpiSch_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SpiSch_Ifa.h
 * @brief       Template file
 * @date        2014. 11. 17.
 ******************************************************************************/

#ifndef SPISCH_IFA_H_
#define SPISCH_IFA_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Cdd.h"
#include "Spim_If.h"
#include "DetErr.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define Ifa_SpiCdd_SyncTransmit(Sequence)	SpiCdd_SyncTransmit(Sequence)
 #define Ifa_SpiCdd_AsyncTransmit(Sequence)	SpiCdd_AsyncTransmit(Sequence)
 #define Ifa_SpiCdd_SetupEB(Channel, SrcDataBufferPtr, DesDataBufferPtr, Length) \
 				SpiCdd_SetupEB(Channel, SrcDataBufferPtr, DesDataBufferPtr, Length)
 #define Ifa_DetErr_Report(ModuleId, InstanceId, ApiId, ErrorId)	\
 				DetErr_Report(ModuleId, InstanceId, ApiId, ErrorId)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPISCH_IFA_H_ */
/** @} */
