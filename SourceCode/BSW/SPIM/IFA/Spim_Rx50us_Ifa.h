/**
 * @defgroup Spim_Rx50us_Ifa Spim_Rx50us_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx50us_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_RX50US_IFA_H_
#define SPIM_RX50US_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Spim_Rx50us_Read_Spim_Rx50usEcuModeSts(data) \
{ \
    *data = Spim_Rx50usEcuModeSts; \
}
#define Spim_Rx50us_Read_Spim_Rx50usFuncInhibitSpimSts(data) \
{ \
    *data = Spim_Rx50usFuncInhibitSpimSts; \
}

/* Set Output DE MAcro Function */
#define Spim_Rx50us_Write_Spim_Rx50usRxMpsAngleInfo(data) \
{ \
    Spim_Rx50usRxMpsAngleInfo = *data; \
}
#define Spim_Rx50us_Write_Spim_Rx50usRxMpsAngleInfo_Mps1Angle(data) \
{ \
    Spim_Rx50usRxMpsAngleInfo.Mps1Angle = *data; \
}
#define Spim_Rx50us_Write_Spim_Rx50usRxMpsAngleInfo_Mps2Angle(data) \
{ \
    Spim_Rx50usRxMpsAngleInfo.Mps2Angle = *data; \
}
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_RX50US_IFA_H_ */
/** @} */
