/**
 * @defgroup Spim_Sch Spim_Sch
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Sch.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef SPIM_SCH_H_
#define SPIM_SCH_H_

/*==============================================================================
 *                  INCLUDE FILES
 ==============================================================================*/
#include "Spim_Sch_Types.h"
#include "Spim_Sch_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
 /* Initialize Schedule Entity Parameters */
 extern void Spim_Sch_InitScheduleEntity(Spim_Sch_ScheduleEntityNumType entity);

 /* Start Schedule Entity */
 extern void Spim_Sch_StartOneShot(Spim_Sch_ScheduleEntityNumType entity);
 extern void Spim_Sch_StartPeriodic(Spim_Sch_ScheduleEntityNumType entity);

 /* Stop Schedule Entity */
 extern void Spim_Sch_StopScheduleEntity(Spim_Sch_ScheduleEntityNumType entity);

 /* Run Schedule */
 extern void Spim_Sch_RunSchedule(Spim_Sch_ScheduleEntityNumType entity);

 /* Set Active Schedule Table Service */
 extern void Spim_Sch_SetActiveScheduleTable(Spim_Sch_ScheduleEntityNumType entity, \
                                           Spim_Sch_ScheduleTableNumType  scheduleTable);
 /* Get Active Schedule Table Service */
 extern Spim_Sch_ScheduleTableNumType Spim_Sch_GetActiveScheduleTable(\
                                           Spim_Sch_ScheduleEntityNumType entity);
/*==============================================================================
 *                  END OF FILE
 ==============================================================================*/
#endif /* SPIM_SCH_H_ */
/** @} */
