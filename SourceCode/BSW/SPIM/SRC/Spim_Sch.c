/**
 * @defgroup Spim_Sch Spim_Sch
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Sch.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Sch.h"
#include "Spim_Sch_Ifa.h"
#include "Spim_Cdd.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 /* Macro Functions to Access Schedule Table Items */
 #define Spim_Sch_GetScheduleTable(entity, table)  \
                       Spim_Sch_EntityTable[entity][table]
 #define Spim_Sch_GetEntryType(entity, table, idx) \
                       Spim_Sch_EntityTable[entity][table][idx].EntryType
 #define Spim_Sch_GetSpiChannel(entity, table, idx) \
                       Spim_Sch_EntityTable[entity][table][idx].SpiChannel
 #define Spim_Sch_GetSpiSequence(entity, table, idx) \
                       Spim_Sch_EntityTable[entity][table][idx].SpiSequence
 #define Spim_Sch_GetTxFrameId(entity, table, idx) \
                       Spim_Sch_EntityTable[entity][table][idx].TxFrameId
 #define Spim_Sch_GetRxFrameId(entity, table, idx) \
                       Spim_Sch_EntityTable[entity][table][idx].RxFrameId

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
 /* Spi Schedule Entity Type */
 typedef struct
 {
     Spim_Sch_ScheduleTableNumType     runningTable;
     Spim_Sch_ScheduleTableNumType     readyTable;
     Spim_Sch_SequenceNumType          activeSeq;
     Spim_Sch_SequenceNumType          lastSeq;
     Spim_Sch_SequenceType             seqId[SPI_SCHEDULE_MAX_SEQ];          /* Lower Level sequence ID */
     Spim_Sch_LldTxMode                transmitMode[SPI_SCHEDULE_MAX_SEQ];   /* Lower Level Driver Transmit Mode */
     Spim_Sch_EntityEnabledType        enable;
     Spim_Sch_ModeType                 mode;
 }Spim_Sch_ScheduleEntityType;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Spi Schedule Entities */

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/
 static Spim_Sch_ScheduleEntityType Spim_Sch_Entity[ScheduleEntity_Max];
#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#pragma align 4
/* Low Level Buffer to Transmit SPI */
static Spim_Sch_SpiBufType Spim_Sch_LldTxBuffer\
                     [ScheduleEntity_Max][SPI_SCHEDULE_MAX_SEQ][SPI_SCHEDULE_MAX_EP];

/* Low Level Buffer to Receive SPI */
static Spim_Sch_SpiBufType Spim_Sch_LldRxBuffer\
                     [ScheduleEntity_Max][SPI_SCHEDULE_MAX_SEQ][SPI_SCHEDULE_MAX_EP];
/* Restore alignment option */
/* Required to be replaced by Compiler Abstraction in the future */
#pragma align restore

/* Tx Frame ID List */
static Spim_If_FrameIdType Spim_Sch_TxFrameIdList\
                     [ScheduleEntity_Max][SPI_SCHEDULE_MAX_SEQ][SPI_SCHEDULE_MAX_EP];

/* Rx Frame ID List */
static Spim_If_FrameIdType Spim_Sch_RxFrameIdList\
                     [ScheduleEntity_Max][SPI_SCHEDULE_MAX_SEQ][SPI_SCHEDULE_MAX_EP];


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"
_INLINE_ static void Spim_Sch_CopyIfData(Spim_Sch_SequenceNumType seq, Spim_Sch_ScheduleEntityNumType entity);
_INLINE_ static void Spim_Sch_ParseScheduleTable(Spim_Sch_ScheduleTableNumType runningTable, Spim_Sch_ScheduleEntityNumType entity);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/* Initialize Schedule Entity Parameters */
void Spim_Sch_InitScheduleEntity(Spim_Sch_ScheduleEntityNumType entity);

/* Start Schedule Entity */
void Spim_Sch_StartOneShot(Spim_Sch_ScheduleEntityNumType entity);
void Spim_Sch_StartPeriodic(Spim_Sch_ScheduleEntityNumType entity);

/* Stop Schedule Entity */
void Spim_Sch_StopScheduleEntity(Spim_Sch_ScheduleEntityNumType entity);

/* Run Schedule */
void Spim_Sch_RunSchedule(Spim_Sch_ScheduleEntityNumType entity);

/* Set Active Schedule Table Service */
void Spim_Sch_SetActiveScheduleTable(Spim_Sch_ScheduleEntityNumType  entity,
                                   Spim_Sch_ScheduleTableNumType   scheduleTable);
/* Get Active Schedule Table Service */
Spim_Sch_ScheduleTableNumType Spim_Sch_GetActiveScheduleTable(\
                                           Spim_Sch_ScheduleEntityNumType entity);


/****************************************************************************
| NAME:             Spim_Sch_InitScheduleEntity
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Initialize selected Schedule Entity Parameters
****************************************************************************/
 void Spim_Sch_InitScheduleEntity(Spim_Sch_ScheduleEntityNumType entity)
 {
      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_INIT,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }

      /* Initialize Entity */
      Spim_Sch_Entity[entity].runningTable = 0;
      Spim_Sch_Entity[entity].readyTable = 0;
      Spim_Sch_Entity[entity].activeSeq = 0;
      Spim_Sch_Entity[entity].lastSeq = 0;
      Spim_Sch_Entity[entity].enable = EntityEnable_Disabled;

      /* Initially Parse Schedule Table */
      Spim_Sch_ParseScheduleTable(entity, Spim_Sch_Entity[entity].runningTable);
 }

/****************************************************************************
| NAME:             Spim_Sch_StartOneShot
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Start selected Schedule Entity
****************************************************************************/
 void Spim_Sch_StartOneShot(Spim_Sch_ScheduleEntityNumType entity)
 {
      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_START,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }

      /* if previous schedule has not completed */
      if(Spim_Sch_Entity[entity].enable == EntityEnable_Enabled)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_START,
                          SPIM_SCH_ERROR_TIMING);
          return;
      }

      /* if Schedule Table Change Requested, parse new Schedule Table */
      if(Spim_Sch_Entity[entity].runningTable != Spim_Sch_Entity[entity].readyTable)
      {
          Spim_Sch_Entity[entity].runningTable = Spim_Sch_Entity[entity].readyTable;
          Spim_Sch_ParseScheduleTable(entity, Spim_Sch_Entity[entity].runningTable);
      }

      Spim_Sch_Entity[entity].activeSeq = 0;
      Spim_Sch_Entity[entity].mode = Mode_OneShot;
      Spim_Sch_Entity[entity].enable = EntityEnable_Enabled;
 }

/****************************************************************************
| NAME:             Spim_Sch_StartPeriodic
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Start selected Schedule Entity
****************************************************************************/
 void Spim_Sch_StartPeriodic(Spim_Sch_ScheduleEntityNumType entity)
 {
      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_START,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }

      /* if previous schedule has not completed */
      if(Spim_Sch_Entity[entity].enable == EntityEnable_Enabled)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_START,
                          SPIM_SCH_ERROR_TIMING);
          return;
      }

      /* if Schedule Table Change Requested, parse new Schedule Table */
      if(Spim_Sch_Entity[entity].runningTable != Spim_Sch_Entity[entity].readyTable)
      {
          Spim_Sch_Entity[entity].runningTable = Spim_Sch_Entity[entity].readyTable;
          Spim_Sch_ParseScheduleTable(entity, Spim_Sch_Entity[entity].runningTable);
      }

      Spim_Sch_Entity[entity].activeSeq = 0;
      Spim_Sch_Entity[entity].mode = Mode_Periodic;
      Spim_Sch_Entity[entity].enable = EntityEnable_Enabled;
 }

/****************************************************************************
| NAME:             Spim_Sch_StopScheduleEntity
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Stop selected Schedule Entity
****************************************************************************/
 void Spim_Sch_StopScheduleEntity(Spim_Sch_ScheduleEntityNumType entity)
 {
      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_STOP,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }
      Spim_Sch_Entity[entity].enable = EntityEnable_Disabled;
 }

/****************************************************************************
| NAME:             Spim_Sch_RunSchedule
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Execute selected Schedule Entity
****************************************************************************/
 void Spim_Sch_RunSchedule(Spim_Sch_ScheduleEntityNumType entity)
 {
      Spim_Sch_SequenceNumType activeSeq = Spim_Sch_Entity[entity].activeSeq;
      Std_ReturnType ReturnStatus;

      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          Det_ReportError(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_RUN,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }

      if(Spim_Sch_Entity[entity].enable == EntityEnable_Enabled)
      {
          /* Exechange between Interaction Layer Data and Scheduler Ll Buffer */
          Spim_Sch_CopyIfData(entity,
                            Spim_Sch_Entity[entity].activeSeq);

          /* Call low level Spi transmission function */
          if(Spim_Sch_Entity[entity].transmitMode[activeSeq] == LldTxMode_Sync)
          {
              ReturnStatus = Spim_Cdd_SyncTransmit(Spim_Sch_Entity[entity].seqId[activeSeq]);
              if(ReturnStatus == E_NOT_OK)
              {
                  DetErr_Report(SPIM_SCH_MODULE_ID,
                                  entity,
                                  SPIM_SCH_API_RUN,
                                  SPIM_SCH_ERROR_CALL_LLD);
                  return;
              }
          }
          else if(Spim_Sch_Entity[entity].transmitMode[activeSeq] == LldTxMode_Async)
          {
              ReturnStatus = Spim_Cdd_AsyncTransmit(Spim_Sch_Entity[entity].seqId[activeSeq]);
              if(ReturnStatus == E_NOT_OK)
              {
                  DetErr_Report(SPIM_SCH_MODULE_ID,
                                  entity,
                                  SPIM_SCH_API_RUN,
                                  SPIM_SCH_ERROR_CALL_LLD);
                  return;
              }
          }
          /* End of Schedule Function */
          if(Spim_Sch_Entity[entity].activeSeq == Spim_Sch_Entity[entity].lastSeq)
          {
              /* if Schedule Table Change Requested, parse new Schedule Table */
              if(Spim_Sch_Entity[entity].runningTable != Spim_Sch_Entity[entity].readyTable)
              {
                  Spim_Sch_Entity[entity].runningTable = Spim_Sch_Entity[entity].readyTable;
                  Spim_Sch_ParseScheduleTable(entity, Spim_Sch_Entity[entity].runningTable);
              }

              if(Spim_Sch_Entity[entity].mode == Mode_Periodic)
              {
                  Spim_Sch_Entity[entity].activeSeq = 0;
                  return;
              }
              else
              {
                  Spim_Sch_StopScheduleEntity(entity);
              }
          }
          Spim_Sch_Entity[entity].activeSeq++;
      }
 }

/****************************************************************************
| NAME:             Spim_Sch_SetActiveScheduleTable
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|                   Spim_Sch_ScheduleTableNumType  scheduleTable | selected table
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Activate selected Schedule Table
****************************************************************************/
 void Spim_Sch_SetActiveScheduleTable(Spim_Sch_ScheduleEntityNumType  entity,
                                    Spim_Sch_ScheduleTableNumType   scheduleTable)
 {
      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_SET_ACTIVE,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }
      else if(scheduleTable >= ScheduleTable_Max)
      {          
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_SET_ACTIVE,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return;
      }
      Spim_Sch_Entity[entity].readyTable = scheduleTable;
 }

/****************************************************************************
| NAME:             Spim_Sch_GetActiveScheduleTable
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
|
| RETURN VALUES:    Active Schedule Table Number
|
| DESCRIPTION:      Return Active Schedule Table
****************************************************************************/
 Spim_Sch_ScheduleTableNumType Spim_Sch_GetActiveScheduleTable(\
                                            Spim_Sch_ScheduleEntityNumType entity)
 {
      /* Input Range Check */
      if(entity >= ScheduleEntity_Max)
      {
          DetErr_Report(SPIM_SCH_MODULE_ID,
                          entity,
                          SPIM_SCH_API_GET_ACTIVE,
                          SPIM_SCH_ERROR_INPUT_RANGE);
          return ScheduleEntity_Null;
      }
      return Spim_Sch_Entity[entity].readyTable;
 }

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
 /****************************************************************************
 | NAME:             Spim_Sch_ParseScheduleTable
 | CALLED BY:        Spim_Sch_InitScheduleEntity
 |                   Spim_Sch_StartOneShot
 | PRECONDITIONS:    none
 |
 | INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
 |                   Spim_Sch_ScheduleTableNumType  scheduleTable | selected table
 |
 | RETURN VALUES:    none
 |
 | DESCRIPTION:      Setup Low Level Driver's EB Buffers
 ****************************************************************************/
  static void Spim_Sch_ParseScheduleTable(Spim_Sch_ScheduleEntityNumType entity,
                                        Spim_Sch_ScheduleTableNumType  runningTable)
  {
       Spim_Sch_SequenceNumType seqNum = 0;
       Spim_Sch_EntryNumType chStartNum, entryNum = 0;
       Spim_Sch_TrueFalseType flowControl = TrueFalse_True;
       Spim_Sch_ChannelType spiChannel;
       uint16 searchCnt, i;
       Spim_If_FrameIdType txFrameId;
       Spim_If_FrameIdType rxFrameId;
#if 0 /* BECK_TEST */
       searchCnt = 0;
       while(flowControl == TrueFalse_True)
       {
           switch(Spim_Sch_GetEntryType(entity, runningTable, searchCnt))
           {
               case ScheduleEntry_StartOfSchedule:
                   /* Initialize local variables */
                   seqNum = 0;
                   entryNum = 0;
               break;

               case ScheduleEntry_EndOfSchedule:
                   /* Terminate While Loop */
                   flowControl = TrueFalse_False;
               break;

               case ScheduleEntry_StartOfSequence:
                   /* register low level driver sequence id */
                   Spim_Sch_Entity[entity].seqId[seqNum] = \
                                   Spim_Sch_GetSpiSequence(entity, runningTable, searchCnt);
                   chStartNum = 0;
                   entryNum = 0;
               break;

               case ScheduleEntry_EndOfSequence:
                   for(i=entryNum; i<SPI_SCHEDULE_MAX_EP; i++)
                   {
                       Spim_Sch_TxFrameIdList[entity][seqNum][i] = SPI_FRAME_NULL;
                       Spim_Sch_RxFrameIdList[entity][seqNum][i] = SPI_FRAME_NULL;
                   }
                   Spim_Sch_Entity[entity].lastSeq = seqNum;
                   seqNum++;
                   entryNum = 0;
               break;

               case ScheduleEntry_SyncTransmit:
                   /* Get Spi Driver Channel and determine the size of transmit */
                   spiChannel = Spim_Sch_GetSpiChannel(entity, runningTable, searchCnt);
                   Spim_Sch_Entity[entity].transmitMode[seqNum] = LldTxMode_Sync;

                   /* Register Tx Frame ID to the List*/
                   txFrameId = Spim_Sch_GetTxFrameId(entity, runningTable, searchCnt);
                   if(txFrameId != SPI_FRAME_NULL)
                   {
                       Spim_Sch_TxFrameIdList[entity][seqNum][entryNum] = txFrameId;
                   }
                   else
                   {
                       /* Schedule Table Parsing Error */
                       DetErr_Report(SPIM_SCH_MODULE_ID,
                                       entity,
                                       SPIM_SCH_API_PARSE,
                                       SPIM_SCH_ERROR_CONFIG_PAR);
                   }

                   rxFrameId = Spim_Sch_GetRxFrameId(entity, runningTable, searchCnt);
                   Spim_Sch_RxFrameIdList[entity][seqNum][entryNum] = rxFrameId;
                   entryNum++;
                   if(spiChannel != Spim_Sch_GetSpiChannel(entity, runningTable,searchCnt+1))
                   {
                       /* if next entry Spi Channel is different from current one */
                       Spim_Cdd_SetupEB(spiChannel,
                                     (uint8*)&Spim_Sch_LldTxBuffer[entity][seqNum][chStartNum],
                                     (uint8*)&Spim_Sch_LldRxBuffer[entity][seqNum][chStartNum],
                                     entryNum-chStartNum);
                       chStartNum = entryNum;
                   }
               break;

               case ScheduleEntry_AsyncTransmit:
                   /* Get Spi Driver Channel and determine the size of transmit */
                   spiChannel = Spim_Sch_GetSpiChannel(entity, runningTable, searchCnt);
                   Spim_Sch_Entity[entity].transmitMode[seqNum] = LldTxMode_Async;

                   /* Register Tx Frame ID to the List*/
                   txFrameId = Spim_Sch_GetTxFrameId(entity, runningTable, searchCnt);
                   if(txFrameId != SPI_FRAME_NULL)
                   {
                       Spim_Sch_TxFrameIdList[entity][seqNum][entryNum] = txFrameId;
                   }
                   else
                   {
                       /* Schedule Table Parsing Error */
                       DetErr_Report(SPIM_SCH_MODULE_ID,
                                       entity,
                                       SPIM_SCH_API_PARSE,
                                       SPIM_SCH_ERROR_CONFIG_PAR);
                   }

                   rxFrameId = Spim_Sch_GetRxFrameId(entity, runningTable, searchCnt);
                   Spim_Sch_RxFrameIdList[entity][seqNum][entryNum] = rxFrameId;
                   entryNum++;
                   if(spiChannel != Spim_Sch_GetSpiChannel(entity, runningTable,searchCnt+1))
                   {
                       /* if next entry Spi Channel is different from current one */
                	   Spim_Cdd_SetupEB(spiChannel,
                                     (uint8*)&Spim_Sch_LldTxBuffer[entity][seqNum][chStartNum],
                                     (uint8*)&Spim_Sch_LldRxBuffer[entity][seqNum][chStartNum],
                                     entryNum-chStartNum);
                       chStartNum = entryNum;
                   }
               break;
               /* Schedule Table Parsing Error */
               default :
                   DetErr_Report(SPIM_SCH_MODULE_ID,
                                   entity,
                                   SPIM_SCH_API_PARSE,
                                   SPIM_SCH_ERROR_CONFIG_PAR);
               break;
           }
           searchCnt++;
       }
#endif
  }

 /****************************************************************************
 | NAME:             Spim_Sch_CopyIfData
 | CALLED BY:        Spim_Sch_RunSchedule
 |                   Spim_Sch_StartOneShot
 | PRECONDITIONS:    none
 |
 | INPUT PARAMETERS: Spim_Sch_ScheduleEntityNumType  entity | selected entity
 |                   Spim_Sch_SequenceNumType        seq    | sequence
 |
 | RETURN VALUES:    none
 |
 | DESCRIPTION:      Exechange between Interface Layer Data and Scheduler
 |                   Ll Buffer
 ****************************************************************************/
 _INLINE_ static void Spim_Sch_CopyIfData(Spim_Sch_ScheduleEntityNumType entity,
                                        Spim_Sch_SequenceNumType       seq)
 {
     Spim_Sch_EntryNumType entry = 0;

     while(Spim_Sch_TxFrameIdList[entity][seq][entry] != SPI_FRAME_NULL)
     {
         if(Spim_Sch_RxFrameIdList[entity][seq][entry] != SPI_FRAME_NULL)
         {
             Spim_If_FrameBuf[Spim_Sch_RxFrameIdList[entity][seq][entry]] = \
                                   Spim_Sch_LldRxBuffer[entity][seq][entry];
         }
         Spim_Sch_LldTxBuffer[entity][seq][entry] = \
                       Spim_If_FrameBuf[Spim_Sch_TxFrameIdList[entity][seq][entry]];
         entry++;
     }
 }

#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
