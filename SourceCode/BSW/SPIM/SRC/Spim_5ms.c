/**
 * @defgroup Spim_1ms Spim_1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_5ms.h"
#include "Spim_Cdd.h"
#include "Spim_Sch.h"
#include "Spim_If.h"
#include "Mps_A1334_Hndlr.h"
#include "Vlvd_A3944_Hndlr.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Spim_5ms_Init(void)
{
    Spim_Cdd_Init();
    /*Spim_Sch_Init();*/    /* BECK_TEST */
    #if 0   /* 80ASIC, Old MPS Not Used */
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_0, 0);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_1, 1);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_2, 2);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_3, 3);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_4, 4);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_5, 5);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_6, 6);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_7, 7);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_8, 8);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_9, 9);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_10, 10);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_11, 11);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_12, 12);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_13, 13);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_14, 14);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_15, 15);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_16, 16);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_17, 17);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_18, 18);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_19, 19);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_20, 20);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_21, 21);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_22, 22);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_23, 23);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_24, 24);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_25, 25);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MSG_ID_26, 26);

	//Mps_a1334 Init
	/* Address setup for each spi data frame */
	Spim_If_SendSignal(SPI_SIG_MPS1_TX_ANGLE_ADDR, 0);
	Spim_If_SendSignal(SPI_SIG_MPS1_TX_ERROR_ADDR, 4);
	Spim_If_SendSignal(SPI_SIG_MPS1_TX_CONTROL_ADDR, 8);
	Spim_If_SendSignal(SPI_SIG_MPS2_TX_ANGLE_ADDR, 0);
	Spim_If_SendSignal(SPI_SIG_MPS2_TX_ERROR_ADDR, 4);
	Spim_If_SendSignal(SPI_SIG_MPS2_TX_CONTROL_ADDR, 8);

    /* MPS 1 Control Data Setup */
	Spim_If_SendSignal(SPI_SIG_MPS1_TX_CONTROL_RW, WriteSpiTrans);
	Spim_If_SendSignal(SPI_SIG_MPS1_TX_CONTROL_ERST, ClearErrFlag);
	Spim_If_SendSignal(SPI_SIG_MPS1_TX_CONTROL_PWR, MpsM_Config[MpsM_Dualdie_Mps_1].LPM);
    Spim_If_SendSignal(SPI_SIG_MPS1_TX_CONTROL_RPM, MpsM_Config[MpsM_Dualdie_Mps_1].RpmMode);

    /* MPS 2 Control Data Setup */
    Spim_If_SendSignal(SPI_SIG_MPS2_TX_CONTROL_RW, WriteSpiTrans);
    Spim_If_SendSignal(SPI_SIG_MPS2_TX_CONTROL_ERST, ClearErrFlag);
    Spim_If_SendSignal(SPI_SIG_MPS2_TX_CONTROL_PWR, MpsM_Config[MpsM_Dualdie_Mps_2].LPM);
    Spim_If_SendSignal(SPI_SIG_MPS2_TX_CONTROL_RPM, MpsM_Config[MpsM_Dualdie_Mps_2].RpmMode);

	//VLVD_A3944 Init
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_GATE_SEL_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_FAULT_MSK_ID, 5);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_FAULT_MSK_ID, 5);

	  /* Channel 0 Fault Config Spi Tx Signal Set */
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_CFCFG_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_ADDR, VdrCh0);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_RT, VdrM_ChannelConfig[VdrCh0].RT);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_NPD, VdrM_ChannelConfig[VdrCh0].NPD);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_TOF, VdrM_ChannelConfig[VdrCh0].TOF);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_TON, VdrM_ChannelConfig[VdrCh0].TON);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_SB, VdrM_ChannelConfig[VdrCh0].SB);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH0_SG, VdrM_ChannelConfig[VdrCh0].SG);

	  /* Channel 1 Fault Config Spi Tx Signal Set */
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_CFCFG_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_ADDR, VdrCh1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_RT, VdrM_ChannelConfig[VdrCh1].RT);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_NPD, VdrM_ChannelConfig[VdrCh1].NPD);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_TOF, VdrM_ChannelConfig[VdrCh1].TOF);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_TON, VdrM_ChannelConfig[VdrCh1].TON);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_SB, VdrM_ChannelConfig[VdrCh1].SB);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH1_SG, VdrM_ChannelConfig[VdrCh1].SG);

	  /* Channel 2 Fault Config Spi Tx Signal Set */
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_CFCFG_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_ADDR, VdrCh2);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_RT, VdrM_ChannelConfig[VdrCh2].RT);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_NPD, VdrM_ChannelConfig[VdrCh2].NPD);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_TOF, VdrM_ChannelConfig[VdrCh2].TOF);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_TON, VdrM_ChannelConfig[VdrCh2].TON);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_SB, VdrM_ChannelConfig[VdrCh2].SB);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH2_SG, VdrM_ChannelConfig[VdrCh2].SG);

	  /* Channel 3 Fault Config Spi Tx Signal Set */
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_CFCFG_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_ADDR, VdrCh3);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_RT, VdrM_ChannelConfig[VdrCh3].RT);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_NPD, VdrM_ChannelConfig[VdrCh3].NPD);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_TOF, VdrM_ChannelConfig[VdrCh3].TOF);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_TON, VdrM_ChannelConfig[VdrCh3].TON);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_SB, VdrM_ChannelConfig[VdrCh3].SB);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH3_SG, VdrM_ChannelConfig[VdrCh3].SG);

	  /* Channel 4 Fault Config Spi Tx Signal Set */
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_CFCFG_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_ADDR, VdrCh4);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_RT, VdrM_ChannelConfig[VdrCh4].RT);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_NPD, VdrM_ChannelConfig[VdrCh4].NPD);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_TOF, VdrM_ChannelConfig[VdrCh4].TOF);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_TON, VdrM_ChannelConfig[VdrCh4].TON);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_SB, VdrM_ChannelConfig[VdrCh4].SB);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH4_SG, VdrM_ChannelConfig[VdrCh4].SG);

	  /* Channel 5 Fault Config Spi Tx Signal Set */
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_CFCFG_ID, 1);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_ADDR, VdrCh5);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_RT, VdrM_ChannelConfig[VdrCh5].RT);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_NPD, VdrM_ChannelConfig[VdrCh5].NPD);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_TOF, VdrM_ChannelConfig[VdrCh5].TOF);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_TON, VdrM_ChannelConfig[VdrCh5].TON);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_SB, VdrM_ChannelConfig[VdrCh5].SB);
	  Spim_If_SendSignal(SPI_SIG_VDR_TX_CH5_SG, VdrM_ChannelConfig[VdrCh5].SG);	
    #endif
}

void Spim_5ms_MainFunction(void)
{
    //SpiSch_MainFunction_5ms();
	/*Spim_Sch_MainFunction_5ms();*/    /* BECK_TEST */
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
