/**
 * @defgroup Spim_Rx50us Spim_Rx50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx50us.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Rx50us.h"
#include "Spim_Rx50us_Ifa.h"
#include "Spim_Sch.h"
#include "Spim_If.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_RX50US_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPIM_RX50US_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_RX50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Spim_Rx50us_HdrBusType Spim_Rx50usBus;
Spim_Rx50us_HdrBusType Spim_Rx50usBusOld;

/* Version Info */
SwcVersionInfo_t Spim_Rx50usVersionInfo;

/* Input Data Element */
Mom_HndlrEcuModeSts_t Spim_Rx50usEcuModeSts;
Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Rx50usFuncInhibitSpimSts;

/* Output Data Element */
Spim_Rx50usRxMpsAngleInfo_t Spim_Rx50usRxMpsAngleInfo;

#define SPIM_RX50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX50US_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_RX50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_RX50US_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_RX50US_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX50US_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_RX50US_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_RX50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_RX50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX50US_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_RX50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_RX50US_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_RX50US_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX50US_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_RX50US_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
_INLINE_ static void Spim_50us_CopyRxData(void);
 
#define SPIM_RX50US_START_SEC_CODE
#include "Spim_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Spim_Rx50us_Init(void)
{
    /* Initialize Version Info */
    Spim_Rx50usVersionInfo.ModuleId = SPIM_RX50US_MODULE_ID;
    Spim_Rx50usVersionInfo.MajorVer = SPIM_RX50US_MAJOR_VERSION;
    Spim_Rx50usVersionInfo.MinorVer = SPIM_RX50US_MINOR_VERSION;
    Spim_Rx50usVersionInfo.PatchVer = SPIM_RX50US_PATCH_VERSION;
    Spim_Rx50usVersionInfo.BranchVer = SPIM_RX50US_BRANCH_VERSION;
    
    /* Initialize internal bus */
    Spim_Rx50usBus.Spim_Rx50usEcuModeSts = 0;
    Spim_Rx50usBus.Spim_Rx50usFuncInhibitSpimSts = 0;
    Spim_Rx50usBus.Spim_Rx50usRxMpsAngleInfo.Mps1Angle = 0;
    Spim_Rx50usBus.Spim_Rx50usRxMpsAngleInfo.Mps2Angle = 0;
}

void Spim_Rx50us(void)
{
    /* Input */
    Spim_Rx50us_Read_Spim_Rx50usEcuModeSts(&Spim_Rx50usBus.Spim_Rx50usEcuModeSts);
    Spim_Rx50us_Read_Spim_Rx50usFuncInhibitSpimSts(&Spim_Rx50usBus.Spim_Rx50usFuncInhibitSpimSts);

    /* Process */
    /*Spim_Sch_MainFunction_50us();*/ /* BECK_TEST */
    /*Spim_50us_CopyRxData();*/ /* BECK_TEST */

    /* Output */
    Spim_Rx50us_Write_Spim_Rx50usRxMpsAngleInfo(&Spim_Rx50usBus.Spim_Rx50usRxMpsAngleInfo);
    /*==============================================================================
    * Members of structure Spim_Rx50usRxMpsAngleInfo 
     : Spim_Rx50usRxMpsAngleInfo.Mps1Angle;
     : Spim_Rx50usRxMpsAngleInfo.Mps2Angle;
     =============================================================================*/
    
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
_INLINE_ static void Spim_50us_CopyRxData(void)
{
    Spim_Rx50usBus.Spim_Rx50usRxMpsAngleInfo.Mps1Angle = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_ANGLE);
    Spim_Rx50usBus.Spim_Rx50usRxMpsAngleInfo.Mps2Angle = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_ANGLE);
}

#define SPIM_RX50US_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
