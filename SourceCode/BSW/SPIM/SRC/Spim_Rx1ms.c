/**
 * @defgroup Spim_Rx1ms Spim_Rx1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Rx1ms.h"
#include "Spim_Rx1ms_Ifa.h"
#include "Spim_Sch.h"
#include "Spim_If.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/
 static const uint8 ParityTable[16] = 
 { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
#define SPIM_RX1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Spim_Rx1ms_HdrBusType Spim_Rx1msBus;
Spim_Rx1ms_HdrBusType Spim_Rx1msBusOld;

/* Version Info */
SwcVersionInfo_t Spim_Rx1msVersionInfo;

/* Input Data Element */
Mom_HndlrEcuModeSts_t Spim_Rx1msEcuModeSts;
Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Rx1msFuncInhibitSpimSts;

/* Output Data Element */
Spim_Rx1msRxAsicInfo_t Spim_Rx1msRxAsicInfo;
Spim_Rx1msRxMpsInfo_t Spim_Rx1msRxMpsInfo;
Spim_Rx1msRxVlvdInfo_t Spim_Rx1msRxVlvdInfo;
Spim_Rx1msRxRegInfo_t Spim_Rx1msRxRegInfo;
Spim_Rx1msRxAsicPhyInfo_t Spim_Rx1msRxAsicPhyInfo;
Spim_Rx1msRxMgdInfo_t Spim_Rx1msRxMgdInfo;
Spim_Rx1msRxMpsD1AngInfo_t Spim_Rx1msRxMpsD1AngInfo;
Spim_Rx1msRxMpsD1SafeWrdInfo_t Spim_Rx1msRxMpsD1SafeWrdInfo;
Spim_Rx1msRxMpsD1StatusInfo_t Spim_Rx1msRxMpsD1StatusInfo;
Spim_Rx1msRxMpsD2AngInfo_t Spim_Rx1msRxMpsD2AngInfo;
Spim_Rx1msRxMpsD2SafeWrdInfo_t Spim_Rx1msRxMpsD2SafeWrdInfo;
Spim_Rx1msRxMpsD2StatusInfo_t Spim_Rx1msRxMpsD2StatusInfo;
Spim_Rx1msRxVlvdFFInfo_t Spim_Rx1msRxVlvdFFInfo;

#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_RX1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_RX1MS_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_RX1MS_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_RX1MS_START_SEC_CODE
#include "Spim_MemMap.h"
_INLINE_ static void Spim_1ms_CopyTxData(void);
_INLINE_ static void Spim_1ms_CopyRxData(void);
_INLINE_ static uint8 Spim_1ms_AsicGetParityBit(uint32 data);
_INLINE_ static void Spim_1ms_AsicSetParitySig(uint16  frameId, uint16 signalId);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Spim_Rx1ms_Init(void)
{
    /* Initialize Version Info */
    Spim_Rx1msVersionInfo.ModuleId = SPIM_RX1MS_MODULE_ID;
    Spim_Rx1msVersionInfo.MajorVer = SPIM_RX1MS_MAJOR_VERSION;
    Spim_Rx1msVersionInfo.MinorVer = SPIM_RX1MS_MINOR_VERSION;
    Spim_Rx1msVersionInfo.PatchVer = SPIM_RX1MS_PATCH_VERSION;
    Spim_Rx1msVersionInfo.BranchVer = SPIM_RX1MS_BRANCH_VERSION;
    
    /* Initialize internal bus */
    Spim_Rx1msBus.Spim_Rx1msEcuModeSts = 0;
    Spim_Rx1msBus.Spim_Rx1msFuncInhibitSpimSts = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.PdMtCfg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.HpdSr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstWd = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstAlu = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstExt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstClk = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VintUv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vcc5Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.DosvUv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Version = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.E5vsOcCfg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.LdactDis = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.LsdSinkDis = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ichar = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Pchar = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.ManufacturingData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.OtwOv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fgnd = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VpwrUv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ld = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VpwrOv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AsicClkCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Temp = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoSel = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoS = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.FvpwrAct = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VintD = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.HdOc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.PdOc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpre10 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsWld = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WldOt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WldOp = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WldOc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpre12 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.CrDis34 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.CrDis12 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.CrFb = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VcpVpwr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Crer = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Crer = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Crer = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Crer = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VintA = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos4 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos3 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd3 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd4 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsokOt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsokOc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd5 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd6 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg4 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg3 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd7 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Duty = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd8 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ar = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.ErrCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.HdLkg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.E5vsOc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.E5vsOuv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsS = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsaiS = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsCntOv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Notlegal = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Fail = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Stop = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Data = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Lkg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Notlegalbits = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Codeerror = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1manchesterdecodingresult = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Notlegal = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Fail = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Stop = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Data = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Lkg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Notlegalbits = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Codeerror = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2manchesterdecodingresult = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Notlegal = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Fail = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Stop = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Data = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Lkg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Notlegalbits = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Codeerror = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3manchesterdecodingresult = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Notlegal = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Fail = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Stop = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Data = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Lkg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Notlegalbits = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Codeerror = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4manchesterdecodingresult = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AdRst1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsVpo1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo1Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo1Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo1Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AdRst2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsVpo2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo2Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo2Op = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo2Oc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AdRst3 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsVso = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoOt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoOp = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoOc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg0 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg3 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg4 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg5 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg6 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg7 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg8 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg9 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg10 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg11 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg12 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg13 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg14 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg15 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg16 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg17 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg18 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg19 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg20 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg21 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg22 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg23 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg24 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg25 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg26 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1AngleP = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1AngleEf = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ErrorP = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Batd = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Magm = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Ierr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Trno = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Tmp = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Eep1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Eep2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ErrorEf = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ControlP = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Erst = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Pwr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Rpm = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ControlEf = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2AngleP = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2AngleEf = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ErrorP = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Batd = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Magm = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Ierr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Trno = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Tmp = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Eep1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Eep2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ErrorEf = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ControlP = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Erst = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Pwr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Rpm = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ControlEf = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C0ol = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C0sb = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C0sg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C1ol = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C1sb = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C1sg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C2ol = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C2sb = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C2sg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Fr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Lr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Ff = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RevMinor = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RevMajor = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Id = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Ign = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CanwuL = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NmaskVdd1UvOv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Sel = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.PostRunRst = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.MaskVbatpOv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EnVdd5Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EnVdd35Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.BgErr1 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.BgErr2 = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AvddVmonErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vcp12Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vcp12Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vcp17Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.VpatpUv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.VbatpOv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd1Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd1Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd6Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd6Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Ot = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Ilim = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Ov = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Uv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Ilim = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Ilim = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdFailCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EeCrcErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgCrcErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AbistRun = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.LbistRun = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AbistUvovErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.LbistErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NresErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TrimErrVmon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EndrvErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.McuErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Loclk = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SpiErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Fsm = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgLock = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SafeLockThr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SafeTo = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AbistEn = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.LbistEn = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EeCrcChk = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AutoBistDis = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.BistDegCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagExit = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagExitMask = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NoError = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EnableDrv = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgCrcEn = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DisNresMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdRstEn = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.IgnPwrl = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdCfg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.ErrorCfg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NoSafeTo = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DevErrCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdFail = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.ErrorPinFail = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Pwmh = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Pwml = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.PwdThr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgCrc = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxEn = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagSpiSdo = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxOut = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagIntCon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxCfg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxSel = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtTokenSeed = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtFdbk = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtRt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtRw = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtToken = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdfailTh = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TokenEarly = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TimeOut = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SeqErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdCfgChg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdWrongCfg = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TokenErr = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtAnswCnt = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1En = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5En = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SwLockStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SwUnlockStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevIdStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevRevStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDevCfg1Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevCfg1Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDevCfg2Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevCfg2Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrCanStbyStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat1Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat2Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat3Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat4Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat5Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrCfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrCfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrStatStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrStatStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyPwdThrCfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyPwdThrCfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyCheckCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyCheckCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyBistCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyBistCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtWin1CfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtWin1CfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtWin2CfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtWin2CfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtTokenValueStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtStatusStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtAnswerStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevStatStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdVmonStat1Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdVmonStat2Stat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSensCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSensCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyFuncCfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyFuncCfgStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyCfgCrcStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyCfgCrcStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDiagCfgCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDiagCfgCtrlStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDiagMuxSelStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDiagMuxSelStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrPwmHStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrPwmHStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrPwmLStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrPwmLStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtTokenFdbckStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtTokenFdbckStat = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.CspMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FlOvCurrMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FlIvDutyMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FrOvCurrMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FrIvDutyMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RlOvCurrMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RlIvDutyMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RrOvCurrMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RrIvDutyMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.Pdf5vMon = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.ErrOverViewRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.SpecialEventRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.InterErr1RawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.InterErr2RawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.ShutDownErrRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.InputPatternViolateRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.OutStageFeedErrRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.SpiCommConfigErrRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.SpiSTATRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsD1AngInfo.D1SpiAngRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsD1SafeWrdInfo.D1SafeWrdRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsD1StatusInfo.D1StatusRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsD2AngInfo.D2SpiAngRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsD2SafeWrdInfo.D2SafeWrdRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxMpsD2StatusInfo.D2StatusRawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdFFInfo.FF0RawData = 0;
    Spim_Rx1msBus.Spim_Rx1msRxVlvdFFInfo.FF1RawData = 0;
}

void Spim_Rx1ms(void)
{
    /* Input */
    Spim_Rx1ms_Read_Spim_Rx1msEcuModeSts(&Spim_Rx1msBus.Spim_Rx1msEcuModeSts);
    Spim_Rx1ms_Read_Spim_Rx1msFuncInhibitSpimSts(&Spim_Rx1msBus.Spim_Rx1msFuncInhibitSpimSts);

    /* Process */
    /*Spim_Sch_MainFunction_1ms();*/ /* BECK_TEST */
    /*Spim_1ms_CopyRxData();*/ /* BECK_TEST */

/* ToDo_ECU_NZ15_InterfaceAdded_0514

Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.ErrOverViewRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.SpecialEventRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.InterErr1RawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.InterErr2RawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.ShutDownErrRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.InputPatternViolateRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.OutStageFeedErrRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.SpiCommConfigErrRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMgdInfo.SpiSTATRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMpsD1AngInfo.D1SpiAngRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMpsD1SafeWrdInfo.D1SafeWrdRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMpsD1StatusInfo.D1StatusRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMpsD2AngInfo.D2SpiAngRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMpsD2SafeWrdInfo.D2SafeWrdRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxMpsD2StatusInfo.D2StatusRawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxVlvdFFInfo.FF0RawData = 0;
   Spim_Rx1msBus.Spim_Rx1msRxVlvdFFInfo.FF1RawData = 0;



*/


    /* Output */
    Spim_Rx1ms_Write_Spim_Rx1msRxAsicInfo(&Spim_Rx1msBus.Spim_Rx1msRxAsicInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxAsicInfo 
     : Spim_Rx1msRxAsicInfo.PdMtCfg;
     : Spim_Rx1msRxAsicInfo.HpdSr;
     : Spim_Rx1msRxAsicInfo.RstWd;
     : Spim_Rx1msRxAsicInfo.RstAlu;
     : Spim_Rx1msRxAsicInfo.RstExt;
     : Spim_Rx1msRxAsicInfo.RstClk;
     : Spim_Rx1msRxAsicInfo.VintUv;
     : Spim_Rx1msRxAsicInfo.Vcc5Uv;
     : Spim_Rx1msRxAsicInfo.DosvUv;
     : Spim_Rx1msRxAsicInfo.Version;
     : Spim_Rx1msRxAsicInfo.E5vsOcCfg;
     : Spim_Rx1msRxAsicInfo.LdactDis;
     : Spim_Rx1msRxAsicInfo.LsdSinkDis;
     : Spim_Rx1msRxAsicInfo.Ichar;
     : Spim_Rx1msRxAsicInfo.Pchar;
     : Spim_Rx1msRxAsicInfo.ManufacturingData;
     : Spim_Rx1msRxAsicInfo.Ws4Ot;
     : Spim_Rx1msRxAsicInfo.Ws4Op;
     : Spim_Rx1msRxAsicInfo.Ws4Oc;
     : Spim_Rx1msRxAsicInfo.Ws3Ot;
     : Spim_Rx1msRxAsicInfo.Ws3Op;
     : Spim_Rx1msRxAsicInfo.Ws3Oc;
     : Spim_Rx1msRxAsicInfo.Ws2Ot;
     : Spim_Rx1msRxAsicInfo.Ws2Op;
     : Spim_Rx1msRxAsicInfo.Ws2Oc;
     : Spim_Rx1msRxAsicInfo.Ws1Ot;
     : Spim_Rx1msRxAsicInfo.Ws1Op;
     : Spim_Rx1msRxAsicInfo.Ws1Oc;
     : Spim_Rx1msRxAsicInfo.OtwOv;
     : Spim_Rx1msRxAsicInfo.Fgnd;
     : Spim_Rx1msRxAsicInfo.VpwrUv;
     : Spim_Rx1msRxAsicInfo.Ld;
     : Spim_Rx1msRxAsicInfo.VpwrOv;
     : Spim_Rx1msRxAsicInfo.AsicClkCnt;
     : Spim_Rx1msRxAsicInfo.Temp;
     : Spim_Rx1msRxAsicInfo.VsoSel;
     : Spim_Rx1msRxAsicInfo.VsoS;
     : Spim_Rx1msRxAsicInfo.FvpwrAct;
     : Spim_Rx1msRxAsicInfo.VintD;
     : Spim_Rx1msRxAsicInfo.HdOc;
     : Spim_Rx1msRxAsicInfo.PdOc;
     : Spim_Rx1msRxAsicInfo.Vpre10;
     : Spim_Rx1msRxAsicInfo.VdsWld;
     : Spim_Rx1msRxAsicInfo.WldOt;
     : Spim_Rx1msRxAsicInfo.WldOp;
     : Spim_Rx1msRxAsicInfo.WldOc;
     : Spim_Rx1msRxAsicInfo.Vpre12;
     : Spim_Rx1msRxAsicInfo.CrDis34;
     : Spim_Rx1msRxAsicInfo.CrDis12;
     : Spim_Rx1msRxAsicInfo.CrFb;
     : Spim_Rx1msRxAsicInfo.VcpVpwr;
     : Spim_Rx1msRxAsicInfo.Lsd4Crer;
     : Spim_Rx1msRxAsicInfo.Lsd3Crer;
     : Spim_Rx1msRxAsicInfo.Lsd2Crer;
     : Spim_Rx1msRxAsicInfo.Lsd1Crer;
     : Spim_Rx1msRxAsicInfo.VintA;
     : Spim_Rx1msRxAsicInfo.Isopos4;
     : Spim_Rx1msRxAsicInfo.Isopos3;
     : Spim_Rx1msRxAsicInfo.Isopos2;
     : Spim_Rx1msRxAsicInfo.Isopos1;
     : Spim_Rx1msRxAsicInfo.Lsd1Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd1;
     : Spim_Rx1msRxAsicInfo.Lsd1Ot;
     : Spim_Rx1msRxAsicInfo.Lsd1Op;
     : Spim_Rx1msRxAsicInfo.Lsd1Oc;
     : Spim_Rx1msRxAsicInfo.Lsd2Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd2;
     : Spim_Rx1msRxAsicInfo.Lsd2Ot;
     : Spim_Rx1msRxAsicInfo.Lsd2Op;
     : Spim_Rx1msRxAsicInfo.Lsd2Oc;
     : Spim_Rx1msRxAsicInfo.Lsd3Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd3;
     : Spim_Rx1msRxAsicInfo.Lsd3Ot;
     : Spim_Rx1msRxAsicInfo.Lsd3Op;
     : Spim_Rx1msRxAsicInfo.Lsd3Oc;
     : Spim_Rx1msRxAsicInfo.Lsd4Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd4;
     : Spim_Rx1msRxAsicInfo.Lsd4Ot;
     : Spim_Rx1msRxAsicInfo.Lsd4Op;
     : Spim_Rx1msRxAsicInfo.Lsd4Oc;
     : Spim_Rx1msRxAsicInfo.IsokOt;
     : Spim_Rx1msRxAsicInfo.IsokOc;
     : Spim_Rx1msRxAsicInfo.Lsd5Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd5;
     : Spim_Rx1msRxAsicInfo.Lsd5Ot;
     : Spim_Rx1msRxAsicInfo.Lsd5Op;
     : Spim_Rx1msRxAsicInfo.Lsd5Oc;
     : Spim_Rx1msRxAsicInfo.IsoNeg2;
     : Spim_Rx1msRxAsicInfo.IsoNeg1;
     : Spim_Rx1msRxAsicInfo.Lsd6Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd6;
     : Spim_Rx1msRxAsicInfo.Lsd6Ot;
     : Spim_Rx1msRxAsicInfo.Lsd6Op;
     : Spim_Rx1msRxAsicInfo.Lsd6Oc;
     : Spim_Rx1msRxAsicInfo.IsoNeg4;
     : Spim_Rx1msRxAsicInfo.IsoNeg3;
     : Spim_Rx1msRxAsicInfo.Lsd7Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd7;
     : Spim_Rx1msRxAsicInfo.Lsd7Ot;
     : Spim_Rx1msRxAsicInfo.Lsd7Op;
     : Spim_Rx1msRxAsicInfo.Lsd7Oc;
     : Spim_Rx1msRxAsicInfo.Lsd8Duty;
     : Spim_Rx1msRxAsicInfo.VdsLsd8;
     : Spim_Rx1msRxAsicInfo.Lsd8Ot;
     : Spim_Rx1msRxAsicInfo.Lsd8Op;
     : Spim_Rx1msRxAsicInfo.Lsd8Oc;
     : Spim_Rx1msRxAsicInfo.Ar;
     : Spim_Rx1msRxAsicInfo.ErrCnt;
     : Spim_Rx1msRxAsicInfo.HdLkg;
     : Spim_Rx1msRxAsicInfo.E5vsOc;
     : Spim_Rx1msRxAsicInfo.E5vsOuv;
     : Spim_Rx1msRxAsicInfo.WsS;
     : Spim_Rx1msRxAsicInfo.WsaiS;
     : Spim_Rx1msRxAsicInfo.WsCntOv;
     : Spim_Rx1msRxAsicInfo.WsCnt;
     : Spim_Rx1msRxAsicInfo.Ws1Notlegal;
     : Spim_Rx1msRxAsicInfo.Ws1Fail;
     : Spim_Rx1msRxAsicInfo.Ws1Stop;
     : Spim_Rx1msRxAsicInfo.Ws1Data;
     : Spim_Rx1msRxAsicInfo.Ws1Lkg;
     : Spim_Rx1msRxAsicInfo.Ws1Notlegalbits;
     : Spim_Rx1msRxAsicInfo.Ws1Codeerror;
     : Spim_Rx1msRxAsicInfo.Ws1manchesterdecodingresult;
     : Spim_Rx1msRxAsicInfo.Ws2Notlegal;
     : Spim_Rx1msRxAsicInfo.Ws2Fail;
     : Spim_Rx1msRxAsicInfo.Ws2Stop;
     : Spim_Rx1msRxAsicInfo.Ws2Data;
     : Spim_Rx1msRxAsicInfo.Ws2Lkg;
     : Spim_Rx1msRxAsicInfo.Ws2Notlegalbits;
     : Spim_Rx1msRxAsicInfo.Ws2Codeerror;
     : Spim_Rx1msRxAsicInfo.Ws2manchesterdecodingresult;
     : Spim_Rx1msRxAsicInfo.Ws3Notlegal;
     : Spim_Rx1msRxAsicInfo.Ws3Fail;
     : Spim_Rx1msRxAsicInfo.Ws3Stop;
     : Spim_Rx1msRxAsicInfo.Ws3Data;
     : Spim_Rx1msRxAsicInfo.Ws3Lkg;
     : Spim_Rx1msRxAsicInfo.Ws3Notlegalbits;
     : Spim_Rx1msRxAsicInfo.Ws3Codeerror;
     : Spim_Rx1msRxAsicInfo.Ws3manchesterdecodingresult;
     : Spim_Rx1msRxAsicInfo.Ws4Notlegal;
     : Spim_Rx1msRxAsicInfo.Ws4Fail;
     : Spim_Rx1msRxAsicInfo.Ws4Stop;
     : Spim_Rx1msRxAsicInfo.Ws4Data;
     : Spim_Rx1msRxAsicInfo.Ws4Lkg;
     : Spim_Rx1msRxAsicInfo.Ws4Notlegalbits;
     : Spim_Rx1msRxAsicInfo.Ws4Codeerror;
     : Spim_Rx1msRxAsicInfo.Ws4manchesterdecodingresult;
     : Spim_Rx1msRxAsicInfo.AdRst1;
     : Spim_Rx1msRxAsicInfo.VdsVpo1;
     : Spim_Rx1msRxAsicInfo.Vpo1Ot;
     : Spim_Rx1msRxAsicInfo.Vpo1Op;
     : Spim_Rx1msRxAsicInfo.Vpo1Oc;
     : Spim_Rx1msRxAsicInfo.AdRst2;
     : Spim_Rx1msRxAsicInfo.VdsVpo2;
     : Spim_Rx1msRxAsicInfo.Vpo2Ot;
     : Spim_Rx1msRxAsicInfo.Vpo2Op;
     : Spim_Rx1msRxAsicInfo.Vpo2Oc;
     : Spim_Rx1msRxAsicInfo.AdRst3;
     : Spim_Rx1msRxAsicInfo.VdsVso;
     : Spim_Rx1msRxAsicInfo.VsoOt;
     : Spim_Rx1msRxAsicInfo.VsoOp;
     : Spim_Rx1msRxAsicInfo.VsoOc;
     : Spim_Rx1msRxAsicInfo.Fmsg0;
     : Spim_Rx1msRxAsicInfo.Fmsg1;
     : Spim_Rx1msRxAsicInfo.Fmsg2;
     : Spim_Rx1msRxAsicInfo.Fmsg3;
     : Spim_Rx1msRxAsicInfo.Fmsg4;
     : Spim_Rx1msRxAsicInfo.Fmsg5;
     : Spim_Rx1msRxAsicInfo.Fmsg6;
     : Spim_Rx1msRxAsicInfo.Fmsg7;
     : Spim_Rx1msRxAsicInfo.Fmsg8;
     : Spim_Rx1msRxAsicInfo.Fmsg9;
     : Spim_Rx1msRxAsicInfo.Fmsg10;
     : Spim_Rx1msRxAsicInfo.Fmsg11;
     : Spim_Rx1msRxAsicInfo.Fmsg12;
     : Spim_Rx1msRxAsicInfo.Fmsg13;
     : Spim_Rx1msRxAsicInfo.Fmsg14;
     : Spim_Rx1msRxAsicInfo.Fmsg15;
     : Spim_Rx1msRxAsicInfo.Fmsg16;
     : Spim_Rx1msRxAsicInfo.Fmsg17;
     : Spim_Rx1msRxAsicInfo.Fmsg18;
     : Spim_Rx1msRxAsicInfo.Fmsg19;
     : Spim_Rx1msRxAsicInfo.Fmsg20;
     : Spim_Rx1msRxAsicInfo.Fmsg21;
     : Spim_Rx1msRxAsicInfo.Fmsg22;
     : Spim_Rx1msRxAsicInfo.Fmsg23;
     : Spim_Rx1msRxAsicInfo.Fmsg24;
     : Spim_Rx1msRxAsicInfo.Fmsg25;
     : Spim_Rx1msRxAsicInfo.Fmsg26;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsInfo 
     : Spim_Rx1msRxMpsInfo.Mps1AngleP;
     : Spim_Rx1msRxMpsInfo.Mps1AngleEf;
     : Spim_Rx1msRxMpsInfo.Mps1ErrorP;
     : Spim_Rx1msRxMpsInfo.Mps1Batd;
     : Spim_Rx1msRxMpsInfo.Mps1Magm;
     : Spim_Rx1msRxMpsInfo.Mps1Ierr;
     : Spim_Rx1msRxMpsInfo.Mps1Trno;
     : Spim_Rx1msRxMpsInfo.Mps1Tmp;
     : Spim_Rx1msRxMpsInfo.Mps1Eep1;
     : Spim_Rx1msRxMpsInfo.Mps1Eep2;
     : Spim_Rx1msRxMpsInfo.Mps1ErrorEf;
     : Spim_Rx1msRxMpsInfo.Mps1ControlP;
     : Spim_Rx1msRxMpsInfo.Mps1Erst;
     : Spim_Rx1msRxMpsInfo.Mps1Pwr;
     : Spim_Rx1msRxMpsInfo.Mps1Rpm;
     : Spim_Rx1msRxMpsInfo.Mps1ControlEf;
     : Spim_Rx1msRxMpsInfo.Mps2AngleP;
     : Spim_Rx1msRxMpsInfo.Mps2AngleEf;
     : Spim_Rx1msRxMpsInfo.Mps2ErrorP;
     : Spim_Rx1msRxMpsInfo.Mps2Batd;
     : Spim_Rx1msRxMpsInfo.Mps2Magm;
     : Spim_Rx1msRxMpsInfo.Mps2Ierr;
     : Spim_Rx1msRxMpsInfo.Mps2Trno;
     : Spim_Rx1msRxMpsInfo.Mps2Tmp;
     : Spim_Rx1msRxMpsInfo.Mps2Eep1;
     : Spim_Rx1msRxMpsInfo.Mps2Eep2;
     : Spim_Rx1msRxMpsInfo.Mps2ErrorEf;
     : Spim_Rx1msRxMpsInfo.Mps2ControlP;
     : Spim_Rx1msRxMpsInfo.Mps2Erst;
     : Spim_Rx1msRxMpsInfo.Mps2Pwr;
     : Spim_Rx1msRxMpsInfo.Mps2Rpm;
     : Spim_Rx1msRxMpsInfo.Mps2ControlEf;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxVlvdInfo(&Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxVlvdInfo 
     : Spim_Rx1msRxVlvdInfo.C0ol;
     : Spim_Rx1msRxVlvdInfo.C0sb;
     : Spim_Rx1msRxVlvdInfo.C0sg;
     : Spim_Rx1msRxVlvdInfo.C1ol;
     : Spim_Rx1msRxVlvdInfo.C1sb;
     : Spim_Rx1msRxVlvdInfo.C1sg;
     : Spim_Rx1msRxVlvdInfo.C2ol;
     : Spim_Rx1msRxVlvdInfo.C2sb;
     : Spim_Rx1msRxVlvdInfo.C2sg;
     : Spim_Rx1msRxVlvdInfo.Fr;
     : Spim_Rx1msRxVlvdInfo.Ot;
     : Spim_Rx1msRxVlvdInfo.Lr;
     : Spim_Rx1msRxVlvdInfo.Uv;
     : Spim_Rx1msRxVlvdInfo.Ff;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxRegInfo(&Spim_Rx1msBus.Spim_Rx1msRxRegInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxRegInfo 
     : Spim_Rx1msRxRegInfo.RevMinor;
     : Spim_Rx1msRxRegInfo.RevMajor;
     : Spim_Rx1msRxRegInfo.Id;
     : Spim_Rx1msRxRegInfo.Ign;
     : Spim_Rx1msRxRegInfo.CanwuL;
     : Spim_Rx1msRxRegInfo.NmaskVdd1UvOv;
     : Spim_Rx1msRxRegInfo.Vdd35Sel;
     : Spim_Rx1msRxRegInfo.PostRunRst;
     : Spim_Rx1msRxRegInfo.MaskVbatpOv;
     : Spim_Rx1msRxRegInfo.EnVdd5Ot;
     : Spim_Rx1msRxRegInfo.EnVdd35Ot;
     : Spim_Rx1msRxRegInfo.BgErr1;
     : Spim_Rx1msRxRegInfo.BgErr2;
     : Spim_Rx1msRxRegInfo.AvddVmonErr;
     : Spim_Rx1msRxRegInfo.Vcp12Uv;
     : Spim_Rx1msRxRegInfo.Vcp12Ov;
     : Spim_Rx1msRxRegInfo.Vcp17Ov;
     : Spim_Rx1msRxRegInfo.VpatpUv;
     : Spim_Rx1msRxRegInfo.VbatpOv;
     : Spim_Rx1msRxRegInfo.Vdd1Uv;
     : Spim_Rx1msRxRegInfo.Vdd1Ov;
     : Spim_Rx1msRxRegInfo.Vdd35Uv;
     : Spim_Rx1msRxRegInfo.Vdd35Ov;
     : Spim_Rx1msRxRegInfo.Vdd5Uv;
     : Spim_Rx1msRxRegInfo.Vdd5Ov;
     : Spim_Rx1msRxRegInfo.Vdd6Uv;
     : Spim_Rx1msRxRegInfo.Vdd6Ov;
     : Spim_Rx1msRxRegInfo.Vdd35Ot;
     : Spim_Rx1msRxRegInfo.Vdd5Ot;
     : Spim_Rx1msRxRegInfo.Vsout1Ot;
     : Spim_Rx1msRxRegInfo.Vsout1Ilim;
     : Spim_Rx1msRxRegInfo.Vsout1Ov;
     : Spim_Rx1msRxRegInfo.Vsout1Uv;
     : Spim_Rx1msRxRegInfo.Vdd35Ilim;
     : Spim_Rx1msRxRegInfo.Vdd5Ilim;
     : Spim_Rx1msRxRegInfo.WdFailCnt;
     : Spim_Rx1msRxRegInfo.EeCrcErr;
     : Spim_Rx1msRxRegInfo.CfgCrcErr;
     : Spim_Rx1msRxRegInfo.AbistRun;
     : Spim_Rx1msRxRegInfo.LbistRun;
     : Spim_Rx1msRxRegInfo.AbistUvovErr;
     : Spim_Rx1msRxRegInfo.LbistErr;
     : Spim_Rx1msRxRegInfo.NresErr;
     : Spim_Rx1msRxRegInfo.TrimErrVmon;
     : Spim_Rx1msRxRegInfo.EndrvErr;
     : Spim_Rx1msRxRegInfo.WdErr;
     : Spim_Rx1msRxRegInfo.McuErr;
     : Spim_Rx1msRxRegInfo.Loclk;
     : Spim_Rx1msRxRegInfo.SpiErr;
     : Spim_Rx1msRxRegInfo.Fsm;
     : Spim_Rx1msRxRegInfo.CfgLock;
     : Spim_Rx1msRxRegInfo.SafeLockThr;
     : Spim_Rx1msRxRegInfo.SafeTo;
     : Spim_Rx1msRxRegInfo.AbistEn;
     : Spim_Rx1msRxRegInfo.LbistEn;
     : Spim_Rx1msRxRegInfo.EeCrcChk;
     : Spim_Rx1msRxRegInfo.AutoBistDis;
     : Spim_Rx1msRxRegInfo.BistDegCnt;
     : Spim_Rx1msRxRegInfo.DiagExit;
     : Spim_Rx1msRxRegInfo.DiagExitMask;
     : Spim_Rx1msRxRegInfo.NoError;
     : Spim_Rx1msRxRegInfo.EnableDrv;
     : Spim_Rx1msRxRegInfo.CfgCrcEn;
     : Spim_Rx1msRxRegInfo.DisNresMon;
     : Spim_Rx1msRxRegInfo.WdRstEn;
     : Spim_Rx1msRxRegInfo.IgnPwrl;
     : Spim_Rx1msRxRegInfo.WdCfg;
     : Spim_Rx1msRxRegInfo.ErrorCfg;
     : Spim_Rx1msRxRegInfo.NoSafeTo;
     : Spim_Rx1msRxRegInfo.DevErrCnt;
     : Spim_Rx1msRxRegInfo.WdFail;
     : Spim_Rx1msRxRegInfo.ErrorPinFail;
     : Spim_Rx1msRxRegInfo.Pwmh;
     : Spim_Rx1msRxRegInfo.Pwml;
     : Spim_Rx1msRxRegInfo.PwdThr;
     : Spim_Rx1msRxRegInfo.CfgCrc;
     : Spim_Rx1msRxRegInfo.DiagMuxEn;
     : Spim_Rx1msRxRegInfo.DiagSpiSdo;
     : Spim_Rx1msRxRegInfo.DiagMuxOut;
     : Spim_Rx1msRxRegInfo.DiagIntCon;
     : Spim_Rx1msRxRegInfo.DiagMuxCfg;
     : Spim_Rx1msRxRegInfo.DiagMuxSel;
     : Spim_Rx1msRxRegInfo.WdtTokenSeed;
     : Spim_Rx1msRxRegInfo.WdtFdbk;
     : Spim_Rx1msRxRegInfo.WdtRt;
     : Spim_Rx1msRxRegInfo.WdtRw;
     : Spim_Rx1msRxRegInfo.WdtToken;
     : Spim_Rx1msRxRegInfo.WdfailTh;
     : Spim_Rx1msRxRegInfo.TokenEarly;
     : Spim_Rx1msRxRegInfo.TimeOut;
     : Spim_Rx1msRxRegInfo.SeqErr;
     : Spim_Rx1msRxRegInfo.WdCfgChg;
     : Spim_Rx1msRxRegInfo.WdWrongCfg;
     : Spim_Rx1msRxRegInfo.TokenErr;
     : Spim_Rx1msRxRegInfo.WdtAnswCnt;
     : Spim_Rx1msRxRegInfo.Vsout1En;
     : Spim_Rx1msRxRegInfo.Vdd5En;
     : Spim_Rx1msRxRegInfo.SwLockStat;
     : Spim_Rx1msRxRegInfo.SwUnlockStat;
     : Spim_Rx1msRxRegInfo.RdDevIdStat;
     : Spim_Rx1msRxRegInfo.RdDevRevStat;
     : Spim_Rx1msRxRegInfo.WrDevCfg1Stat;
     : Spim_Rx1msRxRegInfo.RdDevCfg1Stat;
     : Spim_Rx1msRxRegInfo.WrDevCfg2Stat;
     : Spim_Rx1msRxRegInfo.RdDevCfg2Stat;
     : Spim_Rx1msRxRegInfo.WrCanStbyStat;
     : Spim_Rx1msRxRegInfo.RdSafetyStat1Stat;
     : Spim_Rx1msRxRegInfo.RdSafetyStat2Stat;
     : Spim_Rx1msRxRegInfo.RdSafetyStat3Stat;
     : Spim_Rx1msRxRegInfo.RdSafetyStat4Stat;
     : Spim_Rx1msRxRegInfo.RdSafetyStat5Stat;
     : Spim_Rx1msRxRegInfo.RdSafetyErrCfgStat;
     : Spim_Rx1msRxRegInfo.WrSafetyErrCfgStat;
     : Spim_Rx1msRxRegInfo.WrSafetyErrStatStat;
     : Spim_Rx1msRxRegInfo.RdSafetyErrStatStat;
     : Spim_Rx1msRxRegInfo.RdSafetyPwdThrCfgStat;
     : Spim_Rx1msRxRegInfo.WrSafetyPwdThrCfgStat;
     : Spim_Rx1msRxRegInfo.RdSafetyCheckCtrlStat;
     : Spim_Rx1msRxRegInfo.WrSafetyCheckCtrlStat;
     : Spim_Rx1msRxRegInfo.RdSafetyBistCtrlStat;
     : Spim_Rx1msRxRegInfo.WrSafetyBistCtrlStat;
     : Spim_Rx1msRxRegInfo.RdWdtWin1CfgStat;
     : Spim_Rx1msRxRegInfo.WrWdtWin1CfgStat;
     : Spim_Rx1msRxRegInfo.RdWdtWin2CfgStat;
     : Spim_Rx1msRxRegInfo.WrWdtWin2CfgStat;
     : Spim_Rx1msRxRegInfo.RdWdtTokenValueStat;
     : Spim_Rx1msRxRegInfo.RdWdtStatusStat;
     : Spim_Rx1msRxRegInfo.WrWdtAnswerStat;
     : Spim_Rx1msRxRegInfo.RdDevStatStat;
     : Spim_Rx1msRxRegInfo.RdVmonStat1Stat;
     : Spim_Rx1msRxRegInfo.RdVmonStat2Stat;
     : Spim_Rx1msRxRegInfo.RdSensCtrlStat;
     : Spim_Rx1msRxRegInfo.WrSensCtrlStat;
     : Spim_Rx1msRxRegInfo.RdSafetyFuncCfgStat;
     : Spim_Rx1msRxRegInfo.WrSafetyFuncCfgStat;
     : Spim_Rx1msRxRegInfo.RdSafetyCfgCrcStat;
     : Spim_Rx1msRxRegInfo.WrSafetyCfgCrcStat;
     : Spim_Rx1msRxRegInfo.RdDiagCfgCtrlStat;
     : Spim_Rx1msRxRegInfo.WrDiagCfgCtrlStat;
     : Spim_Rx1msRxRegInfo.RdDiagMuxSelStat;
     : Spim_Rx1msRxRegInfo.WrDiagMuxSelStat;
     : Spim_Rx1msRxRegInfo.RdSafetyErrPwmHStat;
     : Spim_Rx1msRxRegInfo.WrSafetyErrPwmHStat;
     : Spim_Rx1msRxRegInfo.RdSafetyErrPwmLStat;
     : Spim_Rx1msRxRegInfo.WrSafetyErrPwmLStat;
     : Spim_Rx1msRxRegInfo.RdWdtTokenFdbckStat;
     : Spim_Rx1msRxRegInfo.WrWdtTokenFdbckStat;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxAsicPhyInfo(&Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxAsicPhyInfo 
     : Spim_Rx1msRxAsicPhyInfo.CspMon;
     : Spim_Rx1msRxAsicPhyInfo.FlOvCurrMon;
     : Spim_Rx1msRxAsicPhyInfo.FlIvDutyMon;
     : Spim_Rx1msRxAsicPhyInfo.FrOvCurrMon;
     : Spim_Rx1msRxAsicPhyInfo.FrIvDutyMon;
     : Spim_Rx1msRxAsicPhyInfo.RlOvCurrMon;
     : Spim_Rx1msRxAsicPhyInfo.RlIvDutyMon;
     : Spim_Rx1msRxAsicPhyInfo.RrOvCurrMon;
     : Spim_Rx1msRxAsicPhyInfo.RrIvDutyMon;
     : Spim_Rx1msRxAsicPhyInfo.Pdf5vMon;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMgdInfo(&Spim_Rx1msBus.Spim_Rx1msRxMgdInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMgdInfo 
     : Spim_Rx1msRxMgdInfo.ErrOverViewRawData;
     : Spim_Rx1msRxMgdInfo.SpecialEventRawData;
     : Spim_Rx1msRxMgdInfo.InterErr1RawData;
     : Spim_Rx1msRxMgdInfo.InterErr2RawData;
     : Spim_Rx1msRxMgdInfo.ShutDownErrRawData;
     : Spim_Rx1msRxMgdInfo.InputPatternViolateRawData;
     : Spim_Rx1msRxMgdInfo.OutStageFeedErrRawData;
     : Spim_Rx1msRxMgdInfo.SpiCommConfigErrRawData;
     : Spim_Rx1msRxMgdInfo.SpiSTATRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1AngInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsD1AngInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsD1AngInfo 
     : Spim_Rx1msRxMpsD1AngInfo.D1SpiAngRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1SafeWrdInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsD1SafeWrdInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsD1SafeWrdInfo 
     : Spim_Rx1msRxMpsD1SafeWrdInfo.D1SafeWrdRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsD1StatusInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsD1StatusInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsD1StatusInfo 
     : Spim_Rx1msRxMpsD1StatusInfo.D1StatusRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2AngInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsD2AngInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsD2AngInfo 
     : Spim_Rx1msRxMpsD2AngInfo.D2SpiAngRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2SafeWrdInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsD2SafeWrdInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsD2SafeWrdInfo 
     : Spim_Rx1msRxMpsD2SafeWrdInfo.D2SafeWrdRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxMpsD2StatusInfo(&Spim_Rx1msBus.Spim_Rx1msRxMpsD2StatusInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxMpsD2StatusInfo 
     : Spim_Rx1msRxMpsD2StatusInfo.D2StatusRawData;
     =============================================================================*/
    
    Spim_Rx1ms_Write_Spim_Rx1msRxVlvdFFInfo(&Spim_Rx1msBus.Spim_Rx1msRxVlvdFFInfo);
    /*==============================================================================
    * Members of structure Spim_Rx1msRxVlvdFFInfo 
     : Spim_Rx1msRxVlvdFFInfo.FF0RawData;
     : Spim_Rx1msRxVlvdFFInfo.FF1RawData;
     =============================================================================*/
    
    
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
_INLINE_ static uint8 Spim_1ms_AsicGetParityBit(uint32 data)
 {
    uint8 ret_val;
    uint8 temp;

    temp  = ParityTable[(data&0xF)];
    temp += ParityTable[((data>>4)&0xF)];
    temp += ParityTable[((data>>8)&0xF)];
    temp += ParityTable[((data>>12)&0x7)];
    ret_val = temp % 2;

    return ret_val;
 }

_INLINE_ static void Spim_1ms_AsicSetParitySig(uint16  frameId, uint16 signalId)
 {
    uint8 parityTx;

    /* Generate parity bit for each spi data frame */
    parityTx = Spim_1ms_AsicGetParityBit(Spim_If_FrameBuf[frameId]);    
    
    /* Send parity bit for each spi data frame */
    Spim_If_SendSignal(signalId, parityTx);
 }

_INLINE_ static void Spim_1ms_CopyRxData(void)
{
  /* BECK_TEST */ /*
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.PdMtCfg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_PD_MT_CFG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.HpdSr = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_HPD_SR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstWd = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_RST_WD);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstAlu = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_RST_ALU);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstExt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_RST_EXT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.RstClk = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_RST_CLK);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VintUv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VINT_UV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vcc5Uv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VCC5_UV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.DosvUv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_DOSV_UV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Version = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VERSION);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.E5vsOcCfg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_E5VS_OC_CFG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.LdactDis = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LDACT_DIS);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.LsdSinkDis = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD_SINK_DIS);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ichar = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ICHAR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Pchar = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_PCHAR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.ManufacturingData = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_MANUFACTURING_DATA);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.OtwOv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_OTW_OV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fgnd = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FGND);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VpwrUv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPWR_UV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ld = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LD);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VpwrOv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPWR_OV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AsicClkCnt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ASIC_CLK_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Temp = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_TEMP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoSel = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VSO_SEL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoS = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VSO_S);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.FvpwrAct = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FVPWR_ACT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VintD = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VINT_D);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.HdOc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_HD_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.PdOc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_PD_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpre10 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPRE10);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsWld = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_WLD);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WldOt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WLD_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WldOp = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WLD_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WldOc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WLD_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpre12 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPRE12);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.CrDis34 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_CR_DIS34);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.CrDis12 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_CR_DIS12);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.CrFb = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_CR_FB);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VcpVpwr = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VCP_VPWR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Crer = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD4_CRER);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Crer = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD3_CRER);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Crer = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD2_CRER);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Crer = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD1_CRER);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VintA = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VINT_A);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos4 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISOPOS4);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos3 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISOPOS3);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos2 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISOPOS2);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Isopos1 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISOPOS1);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD1_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd1 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD1);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD1_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD1_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd1Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD1_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD2_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd2 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD2);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD2_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD2_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd2Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD2_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD3_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd3 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD3);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD3_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD3_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd3Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD3_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD4_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd4 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD4);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD4_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD4_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd4Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD4_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsokOt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISOK_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsokOc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISOK_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD5_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd5 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD5);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD5_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD5_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd5Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD5_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg2 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISO_NEG2);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg1 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISO_NEG1);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD6_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd6 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD6);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD6_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD6_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd6Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD6_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg4 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISO_NEG4);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.IsoNeg3 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ISO_NEG3);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD7_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd7 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD7);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD7_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD7_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd7Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD7_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Duty = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD8_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsLsd8 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_LSD8);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD8_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD8_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Lsd8Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD8_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ar = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_AR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.ErrCnt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_ERR_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.HdLkg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_HD_LKG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.E5vsOc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_E5VS_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.E5vsOuv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_E5VS_OUV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsS = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS_S);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsaiS = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WSAI_S);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsCntOv = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS_CNT_OV);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.WsCnt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Notlegal = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_NOTLEGAL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Fail = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_FAIL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Stop = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_STOP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Data = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_DATA);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Lkg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_LKG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Notlegalbits = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_NOTLEGALBITS);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1Codeerror = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1_CODEERROR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws1manchesterdecodingresult = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS1MANCHESTERDECODINGRESULT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Notlegal = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_NOTLEGAL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Fail = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_FAIL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Stop = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_STOP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Data = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_DATA);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Lkg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_LKG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Notlegalbits = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_NOTLEGALBITS);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2Codeerror = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2_CODEERROR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws2manchesterdecodingresult = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS2MANCHESTERDECODINGRESULT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Notlegal = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_NOTLEGAL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Fail = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_FAIL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Stop = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_STOP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Data = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_DATA);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Lkg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_LKG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Notlegalbits = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_NOTLEGALBITS);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3Codeerror = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3_CODEERROR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws3manchesterdecodingresult = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS3MANCHESTERDECODINGRESULT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Notlegal = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_NOTLEGAL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Fail = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_FAIL);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Stop = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_STOP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Data = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_DATA);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Lkg = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_LKG);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Notlegalbits = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_NOTLEGALBITS);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4Codeerror = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4_CODEERROR);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Ws4manchesterdecodingresult = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_WS4MANCHESTERDECODINGRESULT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AdRst1 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_AD_RST1);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsVpo1 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_VPO1);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo1Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPO1_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo1Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPO1_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo1Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPO1_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AdRst2 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_AD_RST2);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsVpo2 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_VPO2);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo2Ot = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPO2_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo2Op = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPO2_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Vpo2Oc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VPO2_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.AdRst3 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_AD_RST3);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VdsVso = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VDS_VSO);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoOt = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VSO_OT);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoOp = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VSO_OP);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.VsoOc = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_VSO_OC);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg0 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_0);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg1 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_1);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg2 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_2);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg3 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_3);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg4 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_4);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg5 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_5);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg6 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_6);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg7 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_7);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg8 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_8);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg9 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_9);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg10 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_10);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg11 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_11);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg12 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_12);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg13 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_13);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg14 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_14);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg15 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_15);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg16 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_16);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg17 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_17);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg18 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_18);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg19 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_19);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg20 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_20);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg21 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_21);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg22 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_22);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg23 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_23);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg24 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_24);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg25 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_25);
    Spim_Rx1msBus.Spim_Rx1msRxAsicInfo.Fmsg26 = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_FMSG_26);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.CspMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_AD_RST3);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FlOvCurrMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD5_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FlIvDutyMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD1_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FrOvCurrMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD6_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.FrIvDutyMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD2_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RlOvCurrMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD7_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RlIvDutyMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD3_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RrOvCurrMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD8_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.RrIvDutyMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_LSD4_DUTY);
    Spim_Rx1msBus.Spim_Rx1msRxAsicPhyInfo.Pdf5vMon = Spim_If_ReceiveSignal(SPI_SIG_ASIC_RX_AD_RST1);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1AngleP = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_ANGLE_P);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1AngleEf = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_ANGLE_EF);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ErrorP = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_ERROR_P);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Batd = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_BATD);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Magm = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_MAGM);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Ierr = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_IERR);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Trno = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_TRNO);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Tmp = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_TMP);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Eep1 = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_EEP1);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Eep2 = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_EEP2);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ErrorEf = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_ERROR_EF);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ControlP = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_CONTROL_P);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Erst = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_ERST);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Pwr = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_PWR);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1Rpm = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_RPM);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps1ControlEf = Spim_If_ReceiveSignal(SPI_SIG_MPS1_RX_CONTROL_EF);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2AngleP = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_ANGLE_P);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2AngleEf = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_ANGLE_EF);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ErrorP = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_ERROR_P);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Batd = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_BATD);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Magm = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_MAGM);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Ierr = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_IERR);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Trno = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_TRNO);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Tmp = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_TMP);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Eep1 = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_EEP1);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Eep2 = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_EEP2);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ErrorEf = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_ERROR_EF);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ControlP = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_CONTROL_P);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Erst = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_ERST);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Pwr = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_PWR);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2Rpm = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_RPM);
    Spim_Rx1msBus.Spim_Rx1msRxMpsInfo.Mps2ControlEf = Spim_If_ReceiveSignal(SPI_SIG_MPS2_RX_CONTROL_EF);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C0ol = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C0OL);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C0sb = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C0SB);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C0sg = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C0SG);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C1ol = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C1OL);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C1sb = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C1SB);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C1sg = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C1SG);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C2ol = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C2OL);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C2sb = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C2SB);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.C2sg = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_C2SG);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Fr = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_FR);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Ot = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_OT);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Lr = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_LR);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Uv = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_UV);
    Spim_Rx1msBus.Spim_Rx1msRxVlvdInfo.Ff = Spim_If_ReceiveSignal(SPI_SIG_VDR_RX_FF);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RevMinor = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_REV_MINOR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RevMajor = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_REV_MAJOR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Id = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ID);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Ign = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_IGN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CanwuL = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_CANWU_L);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NmaskVdd1UvOv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_NMASK_VDD1_UV_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Sel = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD_3_5_SEL);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.PostRunRst = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_POST_RUN_RST);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.MaskVbatpOv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_MASK_VBATP_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EnVdd5Ot = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_EN_VDD5_OT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EnVdd35Ot = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_EN_VDD3_5_OT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.BgErr1 = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_BG_ERR1);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.BgErr2 = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_BG_ERR2);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AvddVmonErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_AVDD_VMON_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vcp12Uv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VCP12_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vcp12Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VCP12_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vcp17Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VCP17_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.VpatpUv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VPATP_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.VbatpOv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VBATP_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd1Uv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD1_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd1Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD1_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Uv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD3_5_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD3_5_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Uv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD5_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD5_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd6Uv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD6_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd6Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD6_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Ot = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD_3_5_OT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Ot = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD5_OT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Ot = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VSOUT1_OT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Ilim = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VSOUT1_ILIM);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Ov = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VSOUT1_OV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1Uv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VSOUT1_UV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd35Ilim = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD3_5_ILIM);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5Ilim = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD5_ILIM);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdFailCnt = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_FAIL_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EeCrcErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_EE_CRC_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgCrcErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_CFG_CRC_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AbistRun = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ABIST_RUN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.LbistRun = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_LBIST_RUN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AbistUvovErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ABIST_UVOV_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.LbistErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_LBIST_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NresErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_NRES_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TrimErrVmon = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_TRIM_ERR_VMON);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EndrvErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ENDRV_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.McuErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_MCU_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Loclk = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_LOCLK);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SpiErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_SPI_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Fsm = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_FSM);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgLock = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_CFG_LOCK);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SafeLockThr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_SAFE_LOCK_THR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SafeTo = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_SAFE_TO);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AbistEn = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ABIST_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.LbistEn = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_LBIST_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EeCrcChk = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_EE_CRC_CHK);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.AutoBistDis = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_AUTO_BIST_DIS);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.BistDegCnt = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_BIST_DEG_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagExit = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_EXIT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagExitMask = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_EXIT_MASK);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NoError = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_NO_ERROR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.EnableDrv = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ENABLE_DRV);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgCrcEn = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_CFG_CRC_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DisNresMon = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIS_NRES_MON);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdRstEn = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_RST_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.IgnPwrl = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_IGN_PWRL);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdCfg = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_CFG);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.ErrorCfg = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ERROR_CFG);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.NoSafeTo = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_NO_SAFE_TO);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DevErrCnt = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DEV_ERR_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdFail = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_FAIL);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.ErrorPinFail = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_ERROR_PIN_FAIL);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Pwmh = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_PWMH);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Pwml = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_PWML);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.PwdThr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_PWD_THR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.CfgCrc = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_CFG_CRC);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxEn = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_MUX_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagSpiSdo = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_SPI_SDO);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxOut = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_MUX_OUT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagIntCon = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_INT_CON);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxCfg = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_MUX_CFG);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.DiagMuxSel = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_DIAG_MUX_SEL);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtTokenSeed = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDT_TOKEN_SEED);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtFdbk = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDT_FDBK);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtRt = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDT_RT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtRw = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDT_RW);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtToken = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDT_TOKEN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdfailTh = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDFAIL_TH);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TokenEarly = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_TOKEN_EARLY);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TimeOut = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_TIME_OUT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SeqErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_SEQ_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdCfgChg = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_CFG_CHG);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdWrongCfg = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WD_WRONG_CFG);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.TokenErr = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_TOKEN_ERR);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WdtAnswCnt = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WDT_ANSW_CNT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vsout1En = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VSOUT1_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.Vdd5En = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_VDD5_EN);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SwLockStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_SW_LOCK_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.SwUnlockStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_SW_UNLOCK_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevIdStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DEV_ID_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevRevStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DEV_REV_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDevCfg1Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_DEV_CFG1_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevCfg1Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DEV_CFG1_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDevCfg2Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_DEV_CFG2_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevCfg2Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DEV_CFG2_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrCanStbyStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_CAN_STBY_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat1Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_STAT_1_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat2Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_STAT_2_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat3Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_STAT_3_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat4Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_STAT_4_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyStat5Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_STAT_5_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrCfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_ERR_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrCfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_ERR_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrStatStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_ERR_STAT_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrStatStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_ERR_STAT_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyPwdThrCfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_PWD_THR_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyPwdThrCfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_PWD_THR_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyCheckCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_CHECK_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyCheckCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_CHECK_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyBistCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_BIST_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyBistCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_BIST_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtWin1CfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_WDT_WIN1_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtWin1CfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_WDT_WIN1_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtWin2CfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_WDT_WIN2_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtWin2CfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_WDT_WIN2_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtTokenValueStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_WDT_TOKEN_VALUE_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtStatusStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_WDT_STATUS_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtAnswerStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_WDT_ANSWER_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDevStatStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DEV_STAT_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdVmonStat1Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_VMON_STAT_1_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdVmonStat2Stat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_VMON_STAT_2_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSensCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SENS_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSensCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SENS_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyFuncCfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_FUNC_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyFuncCfgStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_FUNC_CFG_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyCfgCrcStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_CFG_CRC_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyCfgCrcStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_CFG_CRC_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDiagCfgCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DIAG_CFG_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDiagCfgCtrlStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_DIAG_CFG_CTRL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdDiagMuxSelStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_DIAG_MUX_SEL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrDiagMuxSelStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_DIAG_MUX_SEL_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrPwmHStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_ERR_PWM_H_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrPwmHStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_ERR_PWM_H_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdSafetyErrPwmLStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_SAFETY_ERR_PWM_L_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrSafetyErrPwmLStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_SAFETY_ERR_PWM_L_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.RdWdtTokenFdbckStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_RD_WDT_TOKEN_FDBCK_STAT);
    Spim_Rx1msBus.Spim_Rx1msRxRegInfo.WrWdtTokenFdbckStat = Spim_If_ReceiveSignal(SPI_SIG_REG_RX_WR_WDT_TOKEN_FDBCK_STAT);
    */
}

#define SPIM_RX1MS_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
