/**
 * @defgroup Spim_Tx1ms Spim_Tx1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Tx1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Tx1ms.h"
#include "Spim_Tx1ms_Ifa.h"
#include "Spim_Sch.h"
#include "Spim_If.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 /* Initial Seed Value */
 #define INITIAL_SEED_VALUE   53

 /* Logical Macro Function */
 #define XNOR(x,y)  (~(x^y))
 #define LFSR(s)    ((uint8)(s<<1)|\
                    (uint8)((XNOR(XNOR(XNOR(s>>7,s>>5),s>>4),s>>3)&0x01)))
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_TX1MS_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/
 static const uint8 ParityTable[16] = 
 { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
 
#define SPIM_TX1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_TX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Spim_Tx1ms_HdrBusType Spim_Tx1msBus;
Spim_Tx1ms_HdrBusType Spim_Tx1msBusOld;

uint8 seed_count;
/* Version Info */
SwcVersionInfo_t Spim_Tx1msVersionInfo;

/* Input Data Element */
Asic_Mgh80Com_Hndlr1msSpiTxAsicInfo_t Spim_Tx1msSpiTxAsicInfo;
Asic_Mgh80Com_Hndlr5msSpiTxAsicSeedKeyInfo_t Spim_Tx1msSpiTxAsicSeedKeyInfo;
Ioc_OutputSR1msSpiTxAsicActInfo_t Spim_Tx1msSpiTxAsicActInfo;
Mom_HndlrEcuModeSts_t Spim_Tx1msEcuModeSts;
Asic_Mgh80Com_Hndlr1msAsicSchTblNum_t Spim_Tx1msAsicSchTblNum;
Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Tx1msFuncInhibitSpimSts;

/* Output Data Element */

#define SPIM_TX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_TX1MS_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_TX1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_TX1MS_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_TX1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_TX1MS_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_TX1MS_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPIM_TX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_TX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_TX1MS_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_TX1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_TX1MS_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_TX1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_TX1MS_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_TX1MS_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
_INLINE_ static void Spim_1ms_CopyTxData(void);
 
#define SPIM_TX1MS_START_SEC_CODE
#include "Spim_MemMap.h"
_INLINE_ static uint8 Asic_mgh80_com_5ms_GenerateSeed(void);
_INLINE_ static uint8 Asic_mgh80_5ms_CalcAluResult(uint8 seed);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Spim_Tx1ms_Init(void)
{
    /* Initialize Version Info */
    Spim_Tx1msVersionInfo.ModuleId = SPIM_TX1MS_MODULE_ID;
    Spim_Tx1msVersionInfo.MajorVer = SPIM_TX1MS_MAJOR_VERSION;
    Spim_Tx1msVersionInfo.MinorVer = SPIM_TX1MS_MINOR_VERSION;
    Spim_Tx1msVersionInfo.PatchVer = SPIM_TX1MS_PATCH_VERSION;
    Spim_Tx1msVersionInfo.BranchVer = SPIM_TX1MS_BRANCH_VERSION;
    
    /* Initialize internal bus */
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.HpdSr = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.PdMtCfg = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Pchar = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Ichar = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LsdSinkDis = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LdactDis = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.E5vsOcCfg = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.IsoFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.IsoDet = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.FmAmp = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.FmEn = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Stopclk2 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Adin2En = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Adin1Dis = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.OcfPd = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.FvpwrAct = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.DfWs = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.OcfWs = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Iclamp = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Didt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Fdcl = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Llc = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.CrFb = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.CrDis12 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.CrDis34 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LfPwm14 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LfPwm58 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsaiS = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsS = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg1 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg2 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg3 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg4 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis1 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis2 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis3 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis4 = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsCntRst = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsCntEn = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.VsoSel = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.VsoS = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Vpo1On = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.PdClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WldClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.E5vsClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.HdClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.IsokClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.VsoClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Vpo2ClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Vpo1ClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LsdClrFlt = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Mr = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.PdOn = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd5dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd1dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd6dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd2dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd7dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd3dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd8dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd4dutycycle = 0;
    Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.HdOn = 0;
    Spim_Tx1msBus.Spim_Tx1msEcuModeSts = 0;
    Spim_Tx1msBus.Spim_Tx1msAsicSchTblNum = 0;
    Spim_Tx1msBus.Spim_Tx1msFuncInhibitSpimSts = 0;
}

void Spim_Tx1ms(void)
{
    /* Input */
    Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicInfo(&Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo);
    /*==============================================================================
    * Members of structure Spim_Tx1msSpiTxAsicInfo 
     : Spim_Tx1msSpiTxAsicInfo.HpdSr;
     : Spim_Tx1msSpiTxAsicInfo.PdMtCfg;
     : Spim_Tx1msSpiTxAsicInfo.Pchar;
     : Spim_Tx1msSpiTxAsicInfo.Ichar;
     : Spim_Tx1msSpiTxAsicInfo.LsdSinkDis;
     : Spim_Tx1msSpiTxAsicInfo.LdactDis;
     : Spim_Tx1msSpiTxAsicInfo.E5vsOcCfg;
     : Spim_Tx1msSpiTxAsicInfo.IsoFlt;
     : Spim_Tx1msSpiTxAsicInfo.IsoDet;
     : Spim_Tx1msSpiTxAsicInfo.FmAmp;
     : Spim_Tx1msSpiTxAsicInfo.FmEn;
     : Spim_Tx1msSpiTxAsicInfo.Stopclk2;
     : Spim_Tx1msSpiTxAsicInfo.Adin2En;
     : Spim_Tx1msSpiTxAsicInfo.Adin1Dis;
     : Spim_Tx1msSpiTxAsicInfo.OcfPd;
     : Spim_Tx1msSpiTxAsicInfo.FvpwrAct;
     : Spim_Tx1msSpiTxAsicInfo.DfWs;
     : Spim_Tx1msSpiTxAsicInfo.OcfWs;
     : Spim_Tx1msSpiTxAsicInfo.Iclamp;
     : Spim_Tx1msSpiTxAsicInfo.Didt;
     : Spim_Tx1msSpiTxAsicInfo.Fdcl;
     : Spim_Tx1msSpiTxAsicInfo.Llc;
     : Spim_Tx1msSpiTxAsicInfo.CrFb;
     : Spim_Tx1msSpiTxAsicInfo.CrDis12;
     : Spim_Tx1msSpiTxAsicInfo.CrDis34;
     : Spim_Tx1msSpiTxAsicInfo.LfPwm14;
     : Spim_Tx1msSpiTxAsicInfo.LfPwm58;
     : Spim_Tx1msSpiTxAsicInfo.WsaiS;
     : Spim_Tx1msSpiTxAsicInfo.WsS;
     : Spim_Tx1msSpiTxAsicInfo.Wscfg1;
     : Spim_Tx1msSpiTxAsicInfo.Wscfg2;
     : Spim_Tx1msSpiTxAsicInfo.Wscfg3;
     : Spim_Tx1msSpiTxAsicInfo.Wscfg4;
     : Spim_Tx1msSpiTxAsicInfo.WsTrkDis1;
     : Spim_Tx1msSpiTxAsicInfo.WsTrkDis2;
     : Spim_Tx1msSpiTxAsicInfo.WsTrkDis3;
     : Spim_Tx1msSpiTxAsicInfo.WsTrkDis4;
     : Spim_Tx1msSpiTxAsicInfo.WsCntRst;
     : Spim_Tx1msSpiTxAsicInfo.WsCntEn;
     : Spim_Tx1msSpiTxAsicInfo.VsoSel;
     : Spim_Tx1msSpiTxAsicInfo.VsoS;
     : Spim_Tx1msSpiTxAsicInfo.Vpo1On;
     : Spim_Tx1msSpiTxAsicInfo.PdClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.WldClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.E5vsClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.WsClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.HdClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.IsokClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.VsoClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.Vpo2ClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.Vpo1ClrFlt;
     : Spim_Tx1msSpiTxAsicInfo.LsdClrFlt;
     =============================================================================*/
    
    //Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicSeedKeyInfo(&Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo);
    /*==============================================================================
    * Members of structure Spim_Tx1msSpiTxAsicSeedKeyInfo 
     : Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed;
     : Spim_Tx1msSpiTxAsicSeedKeyInfo.Mr;
     =============================================================================*/
    
    Spim_Tx1ms_Read_Spim_Tx1msSpiTxAsicActInfo(&Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo);
    /*==============================================================================
    * Members of structure Spim_Tx1msSpiTxAsicActInfo 
     : Spim_Tx1msSpiTxAsicActInfo.PdOn;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd5dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd1dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd6dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd2dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd7dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd3dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd8dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.Lsd4dutycycle;
     : Spim_Tx1msSpiTxAsicActInfo.HdOn;
     =============================================================================*/
    
    Spim_Tx1ms_Read_Spim_Tx1msEcuModeSts(&Spim_Tx1msBus.Spim_Tx1msEcuModeSts);
    Spim_Tx1ms_Read_Spim_Tx1msAsicSchTblNum(&Spim_Tx1msBus.Spim_Tx1msAsicSchTblNum);
    Spim_Tx1ms_Read_Spim_Tx1msFuncInhibitSpimSts(&Spim_Tx1msBus.Spim_Tx1msFuncInhibitSpimSts);

    /* Process */
	seed_count = (seed_count + 1) % 5;
	if(seed_count == 0)
	{
		Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed = Asic_mgh80_com_5ms_GenerateSeed();
		Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Mr = Asic_mgh80_5ms_CalcAluResult(Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed);
	}
    /*Spim_Sch_SetActiveScheduleTable(ScheduleEntity_Asic, Spim_Tx1msBus.Spim_Tx1msAsicSchTblNum);*/    /* BECK_TEST */
    /*Spim_1ms_CopyTxData();*/  /* BECK_TEST */
    
    /* Output */
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
 static uint8 Spim_1ms_AsicGetParityBit(uint32 data)
 {
    uint8 ret_val;
    uint8 temp;

    temp  = ParityTable[(data&0xF)];
    temp += ParityTable[((data>>4)&0xF)];
    temp += ParityTable[((data>>8)&0xF)];
    temp += ParityTable[((data>>12)&0x7)];
    ret_val = temp % 2;

    return ret_val;
 }

 static void Spim_1ms_AsicSetParitySig(uint16  frameId, uint16 signalId)
 {
    uint8 parityTx;

    /* Generate parity bit for each spi data frame */
    parityTx = Spim_1ms_AsicGetParityBit(Spim_If_FrameBuf[frameId]);    
    
    /* Send parity bit for each spi data frame */
    Spim_If_SendSignal(signalId, parityTx);
 }

_INLINE_ static void Spim_1ms_CopyTxData(void)
{
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_HPD_SR, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.HpdSr);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_PD_MT_CFG, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.PdMtCfg);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_PCHAR, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Pchar);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ICHAR, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Ichar);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD_SINK_DIS, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LsdSinkDis);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LDACT_DIS, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LdactDis);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_E5VS_OC_CFG, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.E5vsOcCfg);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ISO_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.IsoFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ISO_DET, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.IsoDet);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_FM_AMP, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.FmAmp);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_FM_EN, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.FmEn);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_STOPCLK2, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Stopclk2);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ADIN2_EN, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Adin2En);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ADIN1_DIS, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Adin1Dis);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_OCF_PD, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.OcfPd);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_FVPWR_ACT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.FvpwrAct);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_DF_WS, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.DfWs);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_OCF_WS, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.OcfWs);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ICLAMP, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Iclamp);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_DIDT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Didt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_FDCL, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Fdcl);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LLC, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Llc);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_CR_FB, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.CrFb);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_CR_DIS12, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.CrDis12);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_CR_DIS34, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.CrDis34);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LF_PWM_14, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LfPwm14);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LF_PWM_58, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LfPwm58);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WSAI_S, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsaiS);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_S, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsS);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WSCFG1, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg1);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WSCFG2, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg2);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WSCFG3, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg3);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WSCFG4, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Wscfg4);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_TRK_DIS1, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis1);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_TRK_DIS2, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis2);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_TRK_DIS3, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis3);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_TRK_DIS4, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsTrkDis4);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_CNT_RST, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsCntRst);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_CNT_EN, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsCntEn);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_VSO_SEL, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.VsoSel);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_VSO_S, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.VsoS);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_VPO1_ON, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Vpo1On);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_PD_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.PdClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WLD_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WldClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_E5VS_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.E5vsClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_WS_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.WsClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_HD_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.HdClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_ISOK_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.IsokClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_VSO_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.VsoClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_VPO2_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Vpo2ClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_VPO1_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.Vpo1ClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD_CLR_FLT, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicInfo.LsdClrFlt);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_SED, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_MR, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicSeedKeyInfo.Mr);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_PD_ON, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.PdOn);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD5DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd5dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD1DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd1dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD6DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd6dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD2DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd2dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD7DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd7dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD3DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd3dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD8DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd8dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_LSD4DUTYCYCLE, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.Lsd4dutycycle);
    Spim_If_SendSignal(SPI_SIG_ASIC_TX_HD_ON, Spim_Tx1msBus.Spim_Tx1msSpiTxAsicActInfo.HdOn);

    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_0, SPI_SIG_ASIC_TX_PARITY_0);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_1, SPI_SIG_ASIC_TX_PARITY_1);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_2, SPI_SIG_ASIC_TX_PARITY_2);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_3, SPI_SIG_ASIC_TX_PARITY_3);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_4, SPI_SIG_ASIC_TX_PARITY_4);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_5, SPI_SIG_ASIC_TX_PARITY_5);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_6, SPI_SIG_ASIC_TX_PARITY_6);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_7, SPI_SIG_ASIC_TX_PARITY_7);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_8, SPI_SIG_ASIC_TX_PARITY_8);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_9, SPI_SIG_ASIC_TX_PARITY_9);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_10, SPI_SIG_ASIC_TX_PARITY_10);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_11, SPI_SIG_ASIC_TX_PARITY_11);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_12, SPI_SIG_ASIC_TX_PARITY_12);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_13, SPI_SIG_ASIC_TX_PARITY_13);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_14, SPI_SIG_ASIC_TX_PARITY_14);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_15, SPI_SIG_ASIC_TX_PARITY_15);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_16, SPI_SIG_ASIC_TX_PARITY_16);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_17, SPI_SIG_ASIC_TX_PARITY_17);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_18, SPI_SIG_ASIC_TX_PARITY_18);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_19, SPI_SIG_ASIC_TX_PARITY_19);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_20, SPI_SIG_ASIC_TX_PARITY_20);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_21, SPI_SIG_ASIC_TX_PARITY_21);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_22, SPI_SIG_ASIC_TX_PARITY_22);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_23, SPI_SIG_ASIC_TX_PARITY_23);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_24, SPI_SIG_ASIC_TX_PARITY_24);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_25, SPI_SIG_ASIC_TX_PARITY_25);
    Spim_1ms_AsicSetParitySig(SPI_FRAME_ASIC_MSG_TX_26, SPI_SIG_ASIC_TX_PARITY_26);
}

_INLINE_ static uint8 Asic_mgh80_com_5ms_GenerateSeed(void)
{
    static uint8 seed = INITIAL_SEED_VALUE;
 
    /* Generate Seed by LFSR */
    seed = LFSR(seed);
    return seed;
}

_INLINE_ static uint8 Asic_mgh80_5ms_CalcAluResult(uint8 seed)
{
    uint8 result;
    uint32 temp;

    /* Calculate LFSR */
    temp = LFSR(seed);

    /* Calculate Alu Check Result */
    temp = (~(((temp << 2) + 6) - 4)+2);
    result = (uint8)(temp >> 2);

    return result;
}
#define SPIM_TX1MS_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
