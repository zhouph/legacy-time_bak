/**
 * @defgroup Spim_Cdd Spim_Cdd
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Cdd.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef SPIM_CDD_H_
#define SPIM_CDD_H_

/*==============================================================================
 *                  INCLUDE FILES
 ==============================================================================*/
#include "Spim_Cdd_Types.h"
#include "Spim_Cdd_Cfg.h"


/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
 /* Initialize Spi driver */
 extern void Spim_Cdd_Init(void);

 /* EB Buffer Setup Function */
 extern Std_ReturnType Spim_Cdd_SetupEB(Spim_Cdd_ChannelType Channel,\
                                      const Spim_Cdd_DataType* SrcDataBufferPtr,\
                                      Spim_Cdd_DataType* DesDataBufferPtr,\
                                      Spim_Cdd_NumberOfDataType Length);

 /* Spi Synchronous Transmission Function */
 extern Std_ReturnType Spim_Cdd_SyncTransmit(Spim_Cdd_SequenceType Sequence);

 /* Spi Asynchronous Transmission Function */
 extern Std_ReturnType Spim_Cdd_AsyncTransmit(Spim_Cdd_SequenceType Sequence);

/*==============================================================================
 *                  END OF FILE
 ==============================================================================*/
#endif /* SPIM_CDD_H_ */
/** @} */
