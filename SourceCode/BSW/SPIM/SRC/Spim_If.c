/**
 * @defgroup Spim_If Spim_If
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_If.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_If.h"
#include "Spim_If_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
 /* Signal tx-interface */
 void Spim_If_SendSignal(Spim_If_SignalIdType signalId, Spim_If_DataType data);

 /* Signal rx-interface */
 Spim_If_DataType Spim_If_ReceiveSignal(Spim_If_SignalIdType signalId);


 /****************************************************************************
 | NAME:             Spim_If_SendSignal
 | CALLED BY:
 | PRECONDITIONS:    none
 |
 | INPUT PARAMETERS: Spim_If_SignalIdType	signalId 	| selected signal ID
 |                   Spim_If_DataType      data      | data to send
 |
 | RETURN VALUES:    none
 |
 | DESCRIPTION:      Send SPI signal
 ****************************************************************************/
  void Spim_If_SendSignal(Spim_If_SignalIdType signalId, Spim_If_DataType data)
  {
     Spim_If_FrameIdType frameId = Spim_If_SignalConfig[signalId].FrameId;
     Spim_If_SignalBitType startBit = Spim_If_SignalConfig[signalId].StartBit;
     Spim_If_SignalBitType bitLength = Spim_If_SignalConfig[signalId].BitLength;
     Spim_If_SignalBitType endBit = startBit + bitLength;
     Spim_If_FrameDataType* frameDataPtr = &Spim_If_FrameBuf[frameId];
     const uint8 dataBitSize = sizeof(Spim_If_FrameDataType) * 8;
     Spim_If_SignalBitType leftShift = dataBitSize - startBit;
     Spim_If_SignalBitType dataLeftShift = dataBitSize - bitLength;

     *frameDataPtr = ((*frameDataPtr >> endBit) << endBit) +
                     (((data << dataLeftShift) >> dataLeftShift) << startBit) +
                     ((*frameDataPtr << leftShift) >> leftShift);
  }

 /****************************************************************************
 | NAME:             Spim_If_ReceiveSignal
 | CALLED BY:
 | PRECONDITIONS:    none
 |
 | INPUT PARAMETERS: Spim_If_SignalIdType	signalId 	| selected signal ID
 |
 | RETURN VALUES:    Received SPI signal
 |
 | DESCRIPTION:      Receive SPI signal
 ****************************************************************************/
  Spim_If_DataType Spim_If_ReceiveSignal(Spim_If_SignalIdType signalId)
  {
     Spim_If_FrameIdType frameId = Spim_If_SignalConfig[signalId].FrameId;
     Spim_If_SignalBitType startBit = Spim_If_SignalConfig[signalId].StartBit;
     Spim_If_SignalBitType bitLength = Spim_If_SignalConfig[signalId].BitLength;
     Spim_If_FrameDataType* frameDataPtr = &Spim_If_FrameBuf[frameId];
     const uint8 dataBitSize = sizeof(Spim_If_FrameDataType) * 8;
     uint8 leftShift = dataBitSize - (bitLength + startBit);

     /* Signal unpacking */
     return (*frameDataPtr << leftShift) >> (leftShift + startBit);
  }

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
