/**
 * @defgroup Spim_Cdd Spim_Cdd
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Cdd.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Cdd.h"
#include "Spim_Cdd_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
  /* Define to reset the QSPI module by setting GLOBALCON.RESETS bits */
  #define SPI_QSPI_RESET_MODULE (0xF0000000U)
  #define SPI_QSPI_CLC_ENABLE (0x8U)
  
  /* SPI Register Start Address Mapping */
  #define SPI_HW_MODULE  ((volatile Ifx_QSPI *)(void *) &(MODULE_QSPI0))
  
  /* Defines for QSPI GLOBALCON Register */
  #define SPI_QSPI_MASTER_SELECT (0x0U)
  #define SPI_QSPI_LOOPBACK_DISABLE (0x0U)
  #define SPI_QSPI_EXPECT_VALUE (15U)
  #define SPI_QSPI_SI_DISABLE (0U)
  #define SPI_QSPI_SRF_DISABLE (0U)
  #define SPI_QSPI_MODULE_ENABLE (1U)
  #define SPI_QSPI_RXTXFIFO_RESET (6U)
  
  /* Defines for QSPI ECON Register */
  #define SPI_QSPI_CLOCKPHASE_MASK (0x01U)
  #define SPI_QSPI_CLOCKPOL_MASK (0x01U)
  #define SPI_QSPI_CLOCKPOL_SHIFT (0x01U)
  #define SPI_QSPI_PARITY_DISABLE (0U)
  
  /* Defines for QSPI GLOBALCON1 Register */
  //#define SPI_QSPI_ERRORENS_VALUE (0x17FU)
  #define SPI_QSPI_ERRORENS_VALUE (0x0)
  #define SPI_QSPI_USREN_DISABLED (0x0)
  #define SPI_QSPI_TXFM_SINGLE_MOVE (0x1)
  #define SPI_QSPI_RXFM_SINGLE_MOVE (0x1)

  /* If the TXFIFO filling level is equal or less than this
  threshold, than each move of an element from the
  TXFIFO to the shift register triggers a transmit interrupt */
  #define SPI_QSPI_TXFIFOINT_VALUE (0x1U)
  /* If the RXFIFO filling level is equal or greater than this
  threshold, than each move of an element from the shift
  register to the FIFO triggers a receive interrupt */
  #define SPI_QSPI_RXFIFOINT_VALUE (0x1U)
  /* Enable the Interrupt */
  #define SPI_QSPI_INT_ENABLE (0x1U)
  /* Disable the interrupt */
  #define SPI_QSPI_INT_DISABLE (0x0U)
  
  /* Defines for QSPI BACONENTRY Register */
  #define SPI_QSPI_TIMEDELAY_MASK (0x0007FFFEU)
  #define SPI_QSPI_DL_BIT_SELECT (0U)
  #define SPI_QSPI_UINT_DISABLED (0U)
  #define SPI_QSPI_BYTE_BIT     (0U)
  #define SPI_QSPI_BYTE_BYTE    (1U)
  
  /* Defines for Interrupt Enable  */
  #define SPI_QSPI_RXF_INT_ENABLE_VALUE (0x00000400U)
  #define SPI_QSPI_TXF_INT_ENABLE_VALUE (0x00000200U)

  /* Defines for FLAGSCLEAR register */
  #define SPI_QSPI_FLAGSCLEAR_VALUE (0x0FFFU)
  #define SPI_QSPI_RXC_FLAGSCLEAR_VALUE (0x00000400U)
  #define SPI_QSPI_TXC_FLAGSCLEAR_VALUE (0x00000200U)
  #define SPI_QSPI_ERR_FLAGSCLEAR_VALUE (0x000001FFU)
  
  /* SRC TOS values for DMA Move Engines */
  /* TOS is One bit in EP and 2 Bits in HE. delta managed using bitwise OR */
  #define SPI_SRCREG_TOS_DMA (3U)

  /* Macros for bit operation */
  #define SPI_BIT_POSISION(x)   (0x1<<(x))
  #define SPI_16BIT_TRANSFER    16
  #define SPI_32BIT_TRANSFER    32

  /* Macros for DMA TCS */
  #define DMA_CHCFG_32BIT_CONTINUOUS      0x00500000
  #define DMA_ADIC_SCBE_DCBE_LINKED_LIST  0x003C0088
  #define DMA_ADIC_DCBE_LINKED_LIST       0x002C0088
  #define DMA_ADIC_SCBE_LINKED_LIST       0x001C0088
  #define NULL (void*)0

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
  /* Spi Hw Register Data Entry and Exit Types */
  typedef uint32 Spim_Cdd_DataEntryRegType;
  typedef uint32 Spim_Cdd_DataExitRegType;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPIM_STOP_SEC_CONST_UNSPECIFIED
#include "Spim_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
 /* Buffer to store Hw Configuration Parameter for each channel */
 /* 32bit Alignment in case of DMA Transmission */
 /* Required to be replaced by Compiler Abstraction in the future */
 #pragma align 4
 static Ifx_QSPI_BACON Spim_Cdd_QspiBacon[Spim_Cdd_SpiChannel_MaxNum];
 static Ifx_QSPI_BACON Spim_Cdd_QspiBaconLast[Spim_Cdd_SpiChannel_MaxNum];
 static Ifx_QSPI_ECON  Spim_Cdd_QspiEcon[Spim_Cdd_SpiChannel_MaxNum];
 static Ifx_QSPI_SSOC  Spim_Cdd_QspiSSOC[Spim_Cdd_SpiChannel_MaxNum];
 static uint32 dummy1, dummy2;
 /* Restore alignment option */
 /* Required to be replaced by Compiler Abstraction in the future */   
 #pragma align restore

 #pragma align 32
 static Dma_FullTxCtrlSetType Spim_Cdd_DmaTxBaconTcs[Spim_Cdd_SpiChannel_MaxNum];
 static Dma_FullTxCtrlSetType Spim_Cdd_DmaTxBaconTcsLast[Spim_Cdd_SpiChannel_MaxNum];
 static Dma_FullTxCtrlSetType Spim_Cdd_DmaTxDataTcs[Spim_Cdd_SpiChannel_MaxNum];
 static Dma_FullTxCtrlSetType Spim_Cdd_DmaTxDataTcsLast[Spim_Cdd_SpiChannel_MaxNum];
 static Dma_FullTxCtrlSetType Spim_Cdd_DmaRxTcs[Spim_Cdd_SpiChannel_MaxNum];
 static Dma_FullTxCtrlSetType Spim_Cdd_LastTxTcs[Spim_Cdd_SpiSequence_MaxNum];
 static Dma_FullTxCtrlSetType Spim_Cdd_LastRxTcs[Spim_Cdd_SpiSequence_MaxNum];
 #pragma align restore
 
 /* Buffer to store External Buffer information for each channel */
 static Spim_Cdd_ChannelEBConfigType Spim_Cdd_EBConfig[Spim_Cdd_SpiChannel_MaxNum];

#define SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPIM_STOP_SEC_VAR_NOINIT_32BIT
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPIM_STOP_SEC_VAR_UNSPECIFIED
#include "Spim_MemMap.h"
#define SPIM_START_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/** Variable Section (32BIT)**/


#define SPIM_STOP_SEC_VAR_32BIT
#include "Spim_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPIM_START_SEC_CODE
#include "Spim_MemMap.h"
_INLINE_ static void Spim_Cdd_lHwDisableQspiRxIntr(uint32 Module);
_INLINE_ static void Spim_Cdd_lHwEnableQspiRxIntr(uint32 Module);
_INLINE_ static void Spim_Cdd_lHwEnableQspiTxIntr(uint32 Module);
_INLINE_ static void Spim_Cdd_lHwDisableQspiTxIntr(uint32 Module);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
 /* Initialize Spi driver */
 void Spim_Cdd_Init(void);

 /* EB Buffer Setup Function */
 Std_ReturnType Spim_Cdd_SetupEB(Spim_Cdd_ChannelType Channel,\
                              const Spim_Cdd_DataType* SrcDataBufferPtr,\
                              Spim_Cdd_DataType* DesDataBufferPtr,\
                              Spim_Cdd_NumberOfDataType Length);

 /* Spi Synchronous Transmission Function */
 Std_ReturnType Spim_Cdd_SyncTransmit(Spim_Cdd_SequenceType Sequence);

 /* Spi Asynchronous Transmission Function */
 Std_ReturnType Spim_Cdd_AsyncTransmit(Spim_Cdd_SequenceType Sequence);
/****************************************************************************
| NAME:             Spim_Cdd_Init
| CALLED BY:
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Initialize Spi driver
 ****************************************************************************/
 void Spim_Cdd_Init(void)
 {
    Spim_Cdd_ChannelType channel;
    Spim_Cdd_HwChannelType hwChannel;
    Spim_Cdd_HwCsType hwCs;

    /* Reset End Init Protection to access regsiters */
    Mcal_ResetENDINIT();
    
    /* Enable QSPI Module */
    SPI_HW_MODULE[0].CLC.U = SPI_QSPI_CLC_ENABLE;
    SPI_HW_MODULE[1].CLC.U = SPI_QSPI_CLC_ENABLE;
    SPI_HW_MODULE[2].CLC.U = SPI_QSPI_CLC_ENABLE;
    SPI_HW_MODULE[3].CLC.U = SPI_QSPI_CLC_ENABLE;
    /* Reset QSPI Module */    
    SPI_HW_MODULE[0].GLOBALCON.U = SPI_QSPI_RESET_MODULE;
    SPI_HW_MODULE[1].GLOBALCON.U = SPI_QSPI_RESET_MODULE;
    SPI_HW_MODULE[2].GLOBALCON.U = SPI_QSPI_RESET_MODULE;
    SPI_HW_MODULE[3].GLOBALCON.U = SPI_QSPI_RESET_MODULE;

    for(channel = 0; channel < Spim_Cdd_SpiChannel_MaxNum; channel++)
    {
        hwChannel = Spim_Cdd_ChannelConfig[channel].HwChannel;
        hwCs = Spim_Cdd_ChannelConfig[channel].CsPin;
        
        /* Initialize Port Input Selection */
        SPI_HW_MODULE[hwChannel].PISEL.B.MRIS = Spim_Cdd_ChannelConfig[channel].PortInput;

        /* Initialize GLOBALCON Register */        
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.EN = SPI_QSPI_MODULE_ENABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.MS = SPI_QSPI_MASTER_SELECT;
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.TQ = Spim_Cdd_ChannelConfig[channel].TQ;
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.SI = SPI_QSPI_SI_DISABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.EXPECT = SPI_QSPI_EXPECT_VALUE;
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.LB = SPI_QSPI_LOOPBACK_DISABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON.B.SRF = SPI_QSPI_SRF_DISABLE;

        /* Initialize GLOBALCON1 Register */
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.ERRORENS = SPI_QSPI_ERRORENS_VALUE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.TXEN = SPI_QSPI_INT_ENABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.RXEN = SPI_QSPI_INT_ENABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.PT1 = SPI_QSPI_INT_DISABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.PT2 = SPI_QSPI_INT_DISABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.USREN = SPI_QSPI_INT_DISABLE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.TXFIFOINT = SPI_QSPI_TXFIFOINT_VALUE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.RXFIFOINT = SPI_QSPI_RXFIFOINT_VALUE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.TXFM = SPI_QSPI_TXFM_SINGLE_MOVE;
        SPI_HW_MODULE[hwChannel].GLOBALCON1.B.RXFM = SPI_QSPI_RXFM_SINGLE_MOVE;

        /* Initialize BACON Data Structure for each channel */
        Spim_Cdd_QspiBacon[channel].B.LAST = Spim_Cdd_ChannelConfig[channel].CsDrv;
        Spim_Cdd_QspiBacon[channel].B.IPRE = Spim_Cdd_ChannelConfig[channel].IPRE;
        Spim_Cdd_QspiBacon[channel].B.IDLE = Spim_Cdd_ChannelConfig[channel].IDLE;
        Spim_Cdd_QspiBacon[channel].B.LPRE = Spim_Cdd_ChannelConfig[channel].LPRE;
        Spim_Cdd_QspiBacon[channel].B.LEAD = Spim_Cdd_ChannelConfig[channel].LEAD;
        Spim_Cdd_QspiBacon[channel].B.TPRE = Spim_Cdd_ChannelConfig[channel].TPRE;
        Spim_Cdd_QspiBacon[channel].B.TRAIL = Spim_Cdd_ChannelConfig[channel].TRAIL;
        Spim_Cdd_QspiBacon[channel].B.UINT = SPI_QSPI_UINT_DISABLED;
        Spim_Cdd_QspiBacon[channel].B.MSB = Spim_Cdd_ChannelConfig[channel].TransferStart;
        Spim_Cdd_QspiBacon[channel].B.BYTE = SPI_QSPI_BYTE_BIT;
        Spim_Cdd_QspiBacon[channel].B.DL = Spim_Cdd_ChannelConfig[channel].DataWidth-1;
        Spim_Cdd_QspiBacon[channel].B.CS = Spim_Cdd_ChannelConfig[channel].CsPin;

        /* Make Last BACON Data for Countinous Data mode with LAST = 1 */
        if(Spim_Cdd_ChannelConfig[channel].CsDrv == CsDrvType_ContinuousData)
        {
            Spim_Cdd_QspiBaconLast[channel].U = Spim_Cdd_QspiBacon[channel].U;
            Spim_Cdd_QspiBaconLast[channel].B.LAST = 0x1;
        }

        /* Initialize ECON Data Structure for each channel */
        Spim_Cdd_QspiEcon[channel].B.Q = Spim_Cdd_ChannelConfig[channel].Q;
        Spim_Cdd_QspiEcon[channel].B.A = Spim_Cdd_ChannelConfig[channel].A;
        Spim_Cdd_QspiEcon[channel].B.B = Spim_Cdd_ChannelConfig[channel].B;
        Spim_Cdd_QspiEcon[channel].B.C = Spim_Cdd_ChannelConfig[channel].C;
        Spim_Cdd_QspiEcon[channel].B.CPH = Spim_Cdd_ChannelConfig[channel].Pha;
        Spim_Cdd_QspiEcon[channel].B.CPOL = Spim_Cdd_ChannelConfig[channel].Pol;
        Spim_Cdd_QspiEcon[channel].B.PAREN = SPI_QSPI_PARITY_DISABLE;

        SPI_HW_MODULE[hwChannel].ECON[hwCs%8].U = Spim_Cdd_QspiEcon[channel].U;

        /* Initialize SSOC Data Structure for each channel */        
        Spim_Cdd_QspiSSOC[channel].U = SPI_BIT_POSISION(hwCs+16);
        if(Spim_Cdd_ChannelConfig[channel].CsPol == CsPol_LowActive)
        {
            SPI_HW_MODULE[hwChannel].SSOC.U |= SPI_BIT_POSISION(hwCs+16);
            SPI_HW_MODULE[hwChannel].SSOC.U &= SPI_BIT_POSISION(hwCs) ^ 0xFFFFFFFF;
        }
        else if(Spim_Cdd_ChannelConfig[channel].CsPol == CsPol_HighActive)
        {
            SPI_HW_MODULE[hwChannel].SSOC.U |= \
                      (SPI_BIT_POSISION(hwCs+16) | SPI_BIT_POSISION(hwCs));
        }
        else
        {
            DetErr_Report(SPIM_CDD_MODULE_ID,
                            channel,
                            SPIM_CDD_API_INIT,
                            SPIM_CDD_ERROR_CONFIG_PAR);
        }        

        /* IRQ Setup */
        MODULE_SRC.QSPI.QSPI[hwChannel].TX.U |= \
                          (IRQ_TOS_DMA | Spim_Cdd_ChannelConfig[channel].DmaChTx);
        MODULE_SRC.QSPI.QSPI[hwChannel].RX.U |= \
                          (IRQ_TOS_DMA | Spim_Cdd_ChannelConfig[channel].DmaChRx);        
     }
    /* Set End Init Protection */
    Mcal_SetENDINIT();
  }

  /****************************************************************************
  | NAME:             Spim_Cdd_SetupEB
  | CALLED BY:
  | PRECONDITIONS:    none
  |
  | INPUT PARAMETERS: Spim_Cdd_ChannelType  Channel   | selected Spi SW Channel
  |                   Spim_Cdd_DataType*    SrcDataBufferPtr | source address
  |                   Spim_Cdd_DataType*    DesDataBufferPtr | destination address
  |                   Spim_Cdd_NumberOfDataType   Length | Transmission Length
  |
  | RETURN VALUES:    E_OK:     EB is Set Correctly
  |                   E_NOT_OK: EB is not Set because of error
  |
  | DESCRIPTION:      Setup EB Buffer
  ****************************************************************************/
   Std_ReturnType Spim_Cdd_SetupEB(Spim_Cdd_ChannelType Channel,
                                const Spim_Cdd_DataType* SrcDataBufferPtr,
                                Spim_Cdd_DataType* DesDataBufferPtr,
                                Spim_Cdd_NumberOfDataType Length)
   {
      Spim_Cdd_HwChannelType hwChannel = Spim_Cdd_ChannelConfig[Channel].HwChannel;
      Spim_Cdd_HwCsType hwCs = Spim_Cdd_ChannelConfig[Channel].CsPin;
      Dma_ChType dmaTxChannel = Spim_Cdd_ChannelConfig[Channel].DmaChTx;
      Dma_ChType dmaRxChannel = Spim_Cdd_ChannelConfig[Channel].DmaChRx;
      Spim_Cdd_CsDrvType CsDrvType = Spim_Cdd_ChannelConfig[Channel].CsDrv;
      
      /* Input Range Check */
      if(Channel >= Spim_Cdd_SpiChannel_MaxNum)
      {
          DetErr_Report(SPIM_CDD_MODULE_ID,
                          Channel,
                          SPIM_CDD_API_SETUP_EB,
                          SPIM_CDD_ERROR_INPUT_RANGE);
          return E_NOT_OK;
      }
      /* Setup External Buffer Config */
      Spim_Cdd_EBConfig[Channel].srcAddr = SrcDataBufferPtr;
      Spim_Cdd_EBConfig[Channel].desAddr = DesDataBufferPtr;
      Spim_Cdd_EBConfig[Channel].len = Length;

      /* DMA Linked List Setup */
      /* TCS for Spi Tx BACON Setup */
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaChControlStatus = 0x0;
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaChannelConfig = DMA_CHCFG_32BIT_CONTINUOUS + 1;
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaAddressIntrControl = DMA_ADIC_SCBE_DCBE_LINKED_LIST;
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaDestinationAddress = (uint32)&SPI_HW_MODULE[hwChannel].BACONENTRY.U;
      if((CsDrvType == CsDrvType_ContinuousData)&&(Length <=1))    /* In case of ContinouseData mode and Length == 1 */
      {
          Spim_Cdd_DmaTxBaconTcs[Channel].DmaSourceAddress = (uint32)&Spim_Cdd_QspiBaconLast[Channel];
          CsDrvType = CsDrvType_ShortData; /* Acts like ShortData mode except BACON Last */
      }
      else
      {
          Spim_Cdd_DmaTxBaconTcs[Channel].DmaSourceAddress = (uint32)&Spim_Cdd_QspiBacon[Channel];
      }
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaSrcDestCrc = 0x0;
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaReadDataCrc = 0x0;
      Spim_Cdd_DmaTxBaconTcs[Channel].DmaShadowAddress = &Spim_Cdd_DmaTxDataTcs[Channel];

      /* TCS for Spi Tx DATAENTRY Setup */
      Spim_Cdd_DmaTxDataTcs[Channel].DmaChControlStatus = 0x0;
      Spim_Cdd_DmaTxDataTcs[Channel].DmaAddressIntrControl = DMA_ADIC_DCBE_LINKED_LIST;
      Spim_Cdd_DmaTxDataTcs[Channel].DmaDestinationAddress = (uint32)&SPI_HW_MODULE[hwChannel].DATAENTRY[0].U;
      Spim_Cdd_DmaTxDataTcs[Channel].DmaSourceAddress = (uint32)SrcDataBufferPtr;
      Spim_Cdd_DmaTxDataTcs[Channel].DmaSrcDestCrc = 0x0;
      Spim_Cdd_DmaTxDataTcs[Channel].DmaReadDataCrc = 0x0;
      if(CsDrvType == CsDrvType_ShortData)
      {
          Spim_Cdd_DmaTxDataTcs[Channel].DmaChannelConfig = DMA_CHCFG_32BIT_CONTINUOUS + Length;
          Spim_Cdd_DmaTxDataTcs[Channel].DmaShadowAddress = NULL;
      }
      else if(CsDrvType == CsDrvType_ContinuousData)
      {
          Spim_Cdd_DmaTxDataTcs[Channel].DmaChannelConfig = DMA_CHCFG_32BIT_CONTINUOUS + Length - 1;
          Spim_Cdd_DmaTxDataTcs[Channel].DmaShadowAddress = &Spim_Cdd_DmaTxBaconTcsLast[Channel];
      }
      
      /* TCS for Spi Tx BACON Setup for Last frame */
      if(CsDrvType == CsDrvType_ContinuousData)
      {
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaChControlStatus = 0x0;
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaChannelConfig = DMA_CHCFG_32BIT_CONTINUOUS + 1;
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaAddressIntrControl = DMA_ADIC_SCBE_DCBE_LINKED_LIST;
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaDestinationAddress = (uint32)&SPI_HW_MODULE[hwChannel].BACONENTRY.U;
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaSourceAddress = (uint32)&Spim_Cdd_QspiBaconLast[Channel];
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaSrcDestCrc = 0x0;
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaReadDataCrc = 0x0;
          Spim_Cdd_DmaTxBaconTcsLast[Channel].DmaShadowAddress = &Spim_Cdd_DmaTxDataTcsLast[Channel];
          
          /* TCS for Spi Tx DATAENTRY Setup */
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaChControlStatus = 0x0;
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaChannelConfig = DMA_CHCFG_32BIT_CONTINUOUS + 1;
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaAddressIntrControl = DMA_ADIC_DCBE_LINKED_LIST;
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaDestinationAddress = (uint32)&SPI_HW_MODULE[hwChannel].DATAENTRY[0].U;
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaSourceAddress = (uint32)SrcDataBufferPtr + (4 * (Length - 1));
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaSrcDestCrc = 0x0;
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaReadDataCrc = 0x0;
          Spim_Cdd_DmaTxDataTcsLast[Channel].DmaShadowAddress = NULL;
      }
      
      /* TCS for Spi Rx DATAEXIT Setup */
      Spim_Cdd_DmaRxTcs[Channel].DmaChControlStatus = 0x0;
      Spim_Cdd_DmaRxTcs[Channel].DmaChannelConfig = DMA_CHCFG_32BIT_CONTINUOUS + Length;
      Spim_Cdd_DmaRxTcs[Channel].DmaAddressIntrControl = DMA_ADIC_SCBE_LINKED_LIST;
      Spim_Cdd_DmaRxTcs[Channel].DmaDestinationAddress = (uint32)DesDataBufferPtr;
      Spim_Cdd_DmaRxTcs[Channel].DmaSourceAddress = (uint32)&SPI_HW_MODULE[hwChannel].RXEXIT.U;
      Spim_Cdd_DmaRxTcs[Channel].DmaSrcDestCrc = 0x0;
      Spim_Cdd_DmaRxTcs[Channel].DmaReadDataCrc = 0x0;
      Spim_Cdd_DmaRxTcs[Channel].DmaShadowAddress = NULL;

      /* Find if this channel TCS has to be linked to next channel TCS */
      Spim_Cdd_SequenceType seq;
      Spim_Cdd_ChannelType ch, nextCh;
      for(seq = 0; seq < Spim_Cdd_SpiSequence_MaxNum; seq++)
      {
          Spim_Cdd_ChannelType firstChannel = Spim_Cdd_SeqChAssign[seq][0];
          for(ch = 0; ch < Spim_Cdd_ChannelAssign_MaxNum; ch++)
          {
            if(Spim_Cdd_SeqChAssign[seq][ch] == Channel)
            {
              nextCh = Spim_Cdd_SeqChAssign[seq][ch+1];
              if(nextCh != Spim_Cdd_SpiChannel_Null)
              {
                /* If next channel exist, link to next channel TCS */
                  if(CsDrvType == CsDrvType_ShortData)
                  {
                      Spim_Cdd_DmaTxDataTcs[Channel].DmaShadowAddress = &Spim_Cdd_DmaTxBaconTcs[nextCh];
                      Spim_Cdd_DmaRxTcs[Channel].DmaShadowAddress = &Spim_Cdd_DmaRxTcs[nextCh];
                  }
                  else if(CsDrvType == CsDrvType_ContinuousData)
                  {
                      Spim_Cdd_DmaTxDataTcsLast[Channel].DmaShadowAddress = &Spim_Cdd_DmaTxBaconTcs[nextCh];
                      Spim_Cdd_DmaRxTcs[Channel].DmaShadowAddress = &Spim_Cdd_DmaRxTcs[nextCh];
                  }
              }
              else
              {
                /* if this is the last channel in the sequence */
                /* Dummy Copy */
                if(CsDrvType == CsDrvType_ShortData)
                {
                    Spim_Cdd_DmaTxDataTcs[Channel].DmaShadowAddress = &Spim_Cdd_LastTxTcs[seq];
                }
                else if(CsDrvType == CsDrvType_ContinuousData)
                {
                    Spim_Cdd_DmaTxDataTcsLast[Channel].DmaShadowAddress = &Spim_Cdd_LastTxTcs[seq];
                }                    
                Spim_Cdd_LastTxTcs[seq].DmaChControlStatus = Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaChControlStatus;
                Spim_Cdd_LastTxTcs[seq].DmaChannelConfig = 0x0;
                Spim_Cdd_LastTxTcs[seq].DmaAddressIntrControl = 0x0;
                Spim_Cdd_LastTxTcs[seq].DmaDestinationAddress = (uint32)&dummy1;
                Spim_Cdd_LastTxTcs[seq].DmaSourceAddress = (uint32)&dummy2;
                Spim_Cdd_LastTxTcs[seq].DmaSrcDestCrc = Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaSrcDestCrc;
                Spim_Cdd_LastTxTcs[seq].DmaReadDataCrc = Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaReadDataCrc;
                Spim_Cdd_LastTxTcs[seq].DmaShadowAddress = (uint32)Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaShadowAddress;
                Spim_Cdd_DmaRxTcs[Channel].DmaShadowAddress = &Spim_Cdd_LastRxTcs[seq];
                Spim_Cdd_LastRxTcs[seq].DmaChControlStatus = Spim_Cdd_DmaRxTcs[firstChannel].DmaChControlStatus;
                Spim_Cdd_LastRxTcs[seq].DmaChannelConfig = 0x0;
                Spim_Cdd_LastRxTcs[seq].DmaAddressIntrControl = 0x0;
                Spim_Cdd_LastRxTcs[seq].DmaDestinationAddress = (uint32)&dummy1;
                Spim_Cdd_LastRxTcs[seq].DmaSourceAddress = (uint32)&dummy2;
                Spim_Cdd_LastRxTcs[seq].DmaSrcDestCrc = Spim_Cdd_DmaRxTcs[firstChannel].DmaSrcDestCrc;
                Spim_Cdd_LastRxTcs[seq].DmaReadDataCrc = Spim_Cdd_DmaRxTcs[firstChannel].DmaReadDataCrc;
                Spim_Cdd_LastRxTcs[seq].DmaShadowAddress = (uint32)Spim_Cdd_DmaRxTcs[firstChannel].DmaShadowAddress;
                }
            }
          }
      }
      return E_OK;
   }

/****************************************************************************
| NAME:             Spim_Cdd_SyncTransmit
| CALLED BY:        
| PRECONDITIONS:    EB Buffer for selected channel must be set up
|
| INPUT PARAMETERS: Spim_Cdd_SequenceType  Sequence   | selected Spi Sequence
|
| RETURN VALUES:    E_OK:     SPI data has been transmitted Correctly
|                   E_NOT_OK: SPI data has not been transmitted because of error
|
| DESCRIPTION:      Spi Synchronous Transmission Function
|                   No Mechanism for Resource Synchronization
|                   Need to be scheduled carefully
****************************************************************************/
 Std_ReturnType Spim_Cdd_SyncTransmit(Spim_Cdd_SequenceType Sequence)
 {
    Spim_Cdd_ChannelType channel = Sequence;
    Spim_Cdd_HwChannelType hwChannel = Spim_Cdd_ChannelConfig[channel].HwChannel;
    Spim_Cdd_HwCsType hwCs = Spim_Cdd_ChannelConfig[channel].CsPin;
    Spim_Cdd_NumberOfDataType i;   
    uint16 *srcAddr16 = (uint16*)&Spim_Cdd_EBConfig[channel].srcAddr[0];
    uint16 *desAddr16 = (uint16*)&Spim_Cdd_EBConfig[channel].desAddr[0];
    uint32 *srcAddr32 = (uint32*)&Spim_Cdd_EBConfig[channel].srcAddr[0];
    uint32 *desAddr32 = (uint32*)&Spim_Cdd_EBConfig[channel].desAddr[0];
    uint32 LoopCounter;

    /* Sync transmission is not supported now */
    /* This service is only available for 1channel in 1sequence feature */
    DetErr_Report(SPIM_CDD_MODULE_ID,
                    channel,
                    SPIM_CDD_API_SYNC_TX,
                    SPIM_CDD_ERROR_UNSUPPORTED);
    return E_NOT_OK;

    /* Disable Qspi Channel Interrupt */
    Spim_Cdd_lHwDisableQspiTxIntr(hwChannel);
    Spim_Cdd_lHwDisableQspiRxIntr(hwChannel);

    /* Disable DMA Transaction */
    Dma_DisableHwTransaction(Spim_Cdd_ChannelConfig[channel].DmaChTx);
    Dma_DisableHwTransaction(Spim_Cdd_ChannelConfig[channel].DmaChRx);

    /* Input Range Check */
    if(Sequence >= Spim_Cdd_SpiChannel_MaxNum)
    {
        DetErr_Report(SPIM_CDD_MODULE_ID,
                        Sequence,
                        SPIM_CDD_API_SYNC_TX,
                        SPIM_CDD_ERROR_INPUT_RANGE);
        return E_NOT_OK;
    }
    /* Hw Buffer Check */
    if(SPI_HW_MODULE[hwChannel].STATUS.B.RXFIFOLEVEL != 0 || \
                            SPI_HW_MODULE[hwChannel].STATUS.B.TXFIFOLEVEL != 0)
    {
        DetErr_Report(SPIM_CDD_MODULE_ID,
                        Sequence,
                        SPIM_CDD_API_SYNC_TX,
                        SPIM_CDD_ERROR_BUFFER);
        return E_NOT_OK;   
    }

    SPI_HW_MODULE[hwChannel].BACONENTRY.U = Spim_Cdd_QspiBacon[channel].U;
    SPI_HW_MODULE[hwChannel].FLAGSCLEAR.U = SPI_QSPI_RXC_FLAGSCLEAR_VALUE;

    for(i=0; i<Spim_Cdd_EBConfig[channel].len; i++)
    {
        /* Push to Data Entry */
        LoopCounter = 0xFFFFFFFF;
        while((SPI_HW_MODULE[hwChannel].STATUS.B.TXF == 0) && (--LoopCounter != 0));        
        SPI_HW_MODULE[hwChannel].FLAGSCLEAR.U = SPI_QSPI_TXC_FLAGSCLEAR_VALUE;       
        SPI_HW_MODULE[hwChannel].DATAENTRY[0].U = srcAddr32[i];
        
        /* Pop from Data Exit */
        LoopCounter = 0xFFFFFFFF;
        while((SPI_HW_MODULE[hwChannel].STATUS.B.RXF == 0) && (--LoopCounter != 0));
        SPI_HW_MODULE[hwChannel].FLAGSCLEAR.U = SPI_QSPI_RXC_FLAGSCLEAR_VALUE;
        desAddr32[i] = (uint16)SPI_HW_MODULE[hwChannel].RXEXIT.U;           
    }
    return E_OK;
 }

   /****************************************************************************
   | NAME:             Spim_Cdd_AsyncTransmit
   | CALLED BY:
   | PRECONDITIONS:    EB Buffer for selected channel must be set up
   |
   | INPUT PARAMETERS: Spim_Cdd_SequenceType  Sequence   | selected Spi Sequence
   |
   | RETURN VALUES:    E_OK:     SPI data will be transmitted Correctly
   |                   E_NOT_OK: SPI data will not be transmitted because of error
   |
   | DESCRIPTION:      Spi Asynchronous Transmission Function
   |                   No Mechanism for Resource Synchronization
   |                   Need to be scheduled carefully
   ****************************************************************************/
    Std_ReturnType Spim_Cdd_AsyncTransmit(Spim_Cdd_SequenceType Sequence)
    {
       Spim_Cdd_ChannelType firstChannel = Spim_Cdd_SeqChAssign[Sequence][0];
       Spim_Cdd_HwChannelType hwChannel = Spim_Cdd_ChannelConfig[firstChannel].HwChannel;
       Dma_ChType dmaTxChannel = Spim_Cdd_ChannelConfig[firstChannel].DmaChTx;
       Dma_ChType dmaRxChannel = Spim_Cdd_ChannelConfig[firstChannel].DmaChRx;
       uint16 dummy;

       /* Input Range Check */
       if(Sequence >= Spim_Cdd_SpiChannel_MaxNum)
       {
           DetErr_Report(SPIM_CDD_MODULE_ID,
                           Sequence,
                           SPIM_CDD_API_ASYNC_TX,
                           SPIM_CDD_ERROR_INPUT_RANGE);
           return E_NOT_OK;
       }
       /* Hw Buffer Check */
       if(SPI_HW_MODULE[hwChannel].STATUS.B.RXFIFOLEVEL != 0 || \
                               SPI_HW_MODULE[hwChannel].STATUS.B.TXFIFOLEVEL != 0)
       {
          while(SPI_HW_MODULE[hwChannel].STATUS.B.RXFIFOLEVEL != 0 || \
                              SPI_HW_MODULE[hwChannel].STATUS.B.TXFIFOLEVEL != 0)
          {
             dummy = SPI_HW_MODULE[hwChannel].RXEXIT.U;
          }
          DetErr_Report(SPIM_CDD_MODULE_ID,
                          Sequence,
                          SPIM_CDD_API_ASYNC_TX,
                          SPIM_CDD_ERROR_BUFFER);
          return E_NOT_OK;
       }

       /* Enable Qspi Channel Interrupt */
       Spim_Cdd_lHwEnableQspiTxIntr(hwChannel);
       Spim_Cdd_lHwEnableQspiRxIntr(hwChannel);

       /* Need to setup CHCFG & ADICR */
       /* Src and des address also need to be set because of unknown corruption */
       MODULE_DMA.CH[dmaTxChannel].CHCFGR.U = \
                               Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaChannelConfig;
       MODULE_DMA.CH[dmaTxChannel].ADICR.U = \
                               Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaAddressIntrControl;
       MODULE_DMA.CH[dmaTxChannel].DADR.U = \
                               Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaDestinationAddress;
       MODULE_DMA.CH[dmaTxChannel].SADR.U = \
                               Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaSourceAddress;
       MODULE_DMA.CH[dmaTxChannel].SHADR.U = \
                               (uint32)Spim_Cdd_DmaTxBaconTcs[firstChannel].DmaShadowAddress;

       MODULE_DMA.CH[dmaRxChannel].CHCFGR.U = \
                               Spim_Cdd_DmaRxTcs[firstChannel].DmaChannelConfig;
       MODULE_DMA.CH[dmaRxChannel].ADICR.U = \
                               Spim_Cdd_DmaRxTcs[firstChannel].DmaAddressIntrControl;
       MODULE_DMA.CH[dmaRxChannel].DADR.U = \
                               Spim_Cdd_DmaRxTcs[firstChannel].DmaDestinationAddress;
       MODULE_DMA.CH[dmaRxChannel].SADR.U = \
                               Spim_Cdd_DmaRxTcs[firstChannel].DmaSourceAddress;
       MODULE_DMA.CH[dmaRxChannel].SHADR.U = \
                               (uint32)Spim_Cdd_DmaRxTcs[firstChannel].DmaShadowAddress;

       Mcal_DmaEnableHwTransfer((Dma_ChannelType)dmaTxChannel);
       Mcal_DmaEnableHwTransfer((Dma_ChannelType)dmaRxChannel);

       /* Clear Rx Interrupt flag before starting Transmission */
       SPI_HW_MODULE[hwChannel].FLAGSCLEAR.U = SPI_QSPI_RXC_FLAGSCLEAR_VALUE;

        /* Request Interrupt */
       SPI_HW_MODULE[hwChannel].STATUS.U = SPI_QSPI_TXF_INT_ENABLE_VALUE;

       return E_OK;
    }

/*==============================================================================
 *                  PRIVATE FUNCTION DEFINITIONS
 =============================================================================*/

     /****************************************************************************
     | NAME:             Spim_Cdd_lHwEnableQspiTxIntr
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: uint32  Module   | selected Spi HW Channel
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Enable Interrupt Router Tx Interrupt Src
     ****************************************************************************/
      _INLINE_ static void  Spim_Cdd_lHwEnableQspiTxIntr(uint32 Module)
      {
         MODULE_SRC.QSPI.QSPI[Module].TX.U |= 0x52000400;
      }

     /****************************************************************************
     | NAME:             Spim_Cdd_lHwDisableQspiTxIntr
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: uint32  Module   | selected Spi HW Channel
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Disable Interrupt Router Tx Interrupt Src
     ****************************************************************************/
      _INLINE_ static void  Spim_Cdd_lHwDisableQspiTxIntr(uint32 Module)
      {
         MODULE_SRC.QSPI.QSPI[Module].TX.B.SRE = 0U; /*  Disable intr */
      }

     /****************************************************************************
     | NAME:             Spim_Cdd_lHwEnableQspiRxIntr
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: uint32  Module   | selected Spi HW Channel
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Enable Interrupt Router Rx Interrupt Src
     ****************************************************************************/
      _INLINE_ static void  Spim_Cdd_lHwEnableQspiRxIntr(uint32 Module)
      {
         MODULE_SRC.QSPI.QSPI[Module].RX.U |= 0x52000400;
      }

     /****************************************************************************
     | NAME:             Spim_Cdd_lHwDisableQspiRxIntr
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: uint32  Module   | selected Spi HW Channel
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Disable Interrupt Router Rx Interrupt Src
     ****************************************************************************/
      _INLINE_ static void  Spim_Cdd_lHwDisableQspiRxIntr(uint32 Module)
      {
         MODULE_SRC.QSPI.QSPI[Module].RX.B.SRE = 0U; /*  Disable intr */
      }

     /****************************************************************************
     | NAME:             Spim_Cdd_DmaSetSourceAddr
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: Dma_ChannelType  Channel   | selected DMA HW Channel
     |                   uint32  Address   | source address
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Setup DMA Source Address
     ****************************************************************************/
      _INLINE_ void Spim_Cdd_DmaSetSourceAddr(Dma_ChannelType Channel, uint32 Address)
      {
         MODULE_DMA.CH[Channel].SADR.U = (uint32)Address;
      }

     /****************************************************************************
     | NAME:             Spim_Cdd_DmaSetDestAddr
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: Dma_ChannelType  Channel   | selected DMA HW Channel
     |                   uint32  Address   | destination address
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Setup DMA Destination Address
     ****************************************************************************/
      _INLINE_ void Spim_Cdd_DmaSetDestAddr(Dma_ChannelType Channel , uint32 Address)
      {
         MODULE_DMA.CH[Channel].DADR.U  = (uint32)Address;
      }

     /****************************************************************************
     | NAME:             Spim_Cdd_DmaSetTxCount
     | CALLED BY:        Spim_Cdd_AsyncTransmit
     | PRECONDITIONS:
     |
     | INPUT PARAMETERS: uint32  Module   | selected DMA HW Channel
     |                   uint32  ReloadValue   | Transfer counter reload value
     |
     | RETURN VALUES:    none
     |
     | DESCRIPTION:      Setup DMA Transfer Count
     ****************************************************************************/
      _INLINE_ void Spim_Cdd_DmaSetTxCount(Dma_ChannelType Channel, uint32 ReloadValue)
      {
         Mcal_SetAtomic(&MODULE_DMA.CH[Channel].CHCFGR.U, ReloadValue, 0,14);
      }

#define SPIM_STOP_SEC_CODE
#include "Spim_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
