/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef SAL_TYPES_H_
#define SAL_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef sint8 Salsint8;
typedef uint8 Saluint8;
typedef sint16 Salsint16;
typedef uint16 Saluint16;
typedef sint32 Salsint32;
typedef uint32 Saluint32;
typedef float32 Salfloat32;
typedef float64 Salfloat64;

typedef struct
{
    Saluint8 HcuRegenEna;
    Saluint16 HcuRegenBrkTq;
}CanRxRegenInfo_t;

typedef struct
{
    Salsint16 AccelPedlVal;
    Saluint8 AccelPedlValErr;
}CanRxAccelPedlInfo_t;

typedef struct
{
    Saluint8 EngMsgFault;
    Saluint8 TarGearPosi;
    Saluint8 GearSelDisp;
    Saluint8 HcuServiceMod;
    Saluint8 SubCanBusSigFault;
    Salsint16 CoolantTemp;
    Saluint8 CoolantTempErr;
}CanRxIdbInfo_t;

typedef struct
{
    Salsint16 EngTemp;
    Saluint8 EngTempErr;
}CanRxEngTempInfo_t;

typedef struct
{
    Salsint16 Ax;
    Salsint16 YawRate;
    Salsint16 SteeringAngle;
    Salsint16 Ay;
    Saluint8 PbSwt;
    Saluint8 ClutchSwt;
    Saluint8 GearRSwt;
    Salsint16 EngActIndTq;
    Salsint16 EngRpm;
    Salsint16 EngIndTq;
    Salsint16 EngFrictionLossTq;
    Salsint16 EngStdTq;
    Saluint16 TurbineRpm;
    Salsint16 ThrottleAngle;
    Salsint16 TpsResol1000;
    Salsint16 PvAvCanResol1000;
    Saluint8 EngChr;
    Saluint8 EngVol;
    Saluint8 GearType;
    Saluint8 EngClutchState;
    Salsint16 EngTqCmdBeforeIntv;
    Salsint16 MotEstTq;
    Salsint16 MotTqCmdBeforeIntv;
    Salsint16 TqIntvTCU;
    Salsint16 TqIntvSlowTCU;
    Salsint16 TqIncReq;
    Salsint16 DecelReq;
    Saluint8 RainSnsStat;
    Saluint8 EngSpdErr;
    Saluint8 AtType;
    Saluint8 MtType;
    Saluint8 CvtType;
    Saluint8 TurbineRpmErr;
    Saluint8 ThrottleAngleErr;
    Saluint8 TopTrvlCltchSwtAct;
    Saluint8 TopTrvlCltchSwtActV;
    Saluint8 HillDesCtrlMdSwtAct;
    Saluint8 HillDesCtrlMdSwtActV;
    Saluint8 WiperIntSW;
    Saluint8 WiperLow;
    Saluint8 WiperHigh;
    Saluint8 WiperValid;
    Saluint8 WiperAuto;
    Saluint8 TcuFaultSts;
}CanRxEscInfo_t;

typedef struct
{
    Salsint32 PtcReqData[5];
    Salsint32 StcReqData[5];
    Salsint32 PesvReqData[5];
    Salsint32 SesvReqData[5];
    Salsint32 PtcReq;
    Salsint32 StcReq;
    Salsint32 PesvReq;
    Salsint32 SesvReq;
}EscVlvReqAswInfo_t;

typedef struct
{
    Salsint32 MotPwmPhUData;
    Salsint32 MotPwmPhVData;
    Salsint32 MotPwmPhWData;
    Saluint8 MotReq;
}MotReqDataDiagInfo_t;

typedef struct
{
    Salsint32 MotPwmPhUData;
    Salsint32 MotPwmPhVData;
    Salsint32 MotPwmPhWData;
    Saluint8 MotReq;
}MotReqDataEemInfo_t;

typedef struct
{
    Salsint32 MotPwmPhUData;
    Salsint32 MotPwmPhVData;
    Salsint32 MotPwmPhWData;
    Saluint8 MotReq;
}MotReqDataAcmctlInfo_t;

typedef struct
{
    Saluint8 PrimCircPErr;
    Saluint8 SecdCircPErr;
    Saluint8 PedlSimPErr;
}ErrCircPInfo_t;

typedef struct
{
    Saluint8 BlsErr;
}ErrInfo_t;

typedef struct
{
    Saluint8 FlWhlSpdInvld;
    Saluint8 FrWhlSpdInvld;
    Saluint8 RlWhlSpdInvld;
    Saluint8 RrWhlSpdInvld;
    Saluint8 FlWhlSpdErr;
    Saluint8 FrWhlSpdErr;
    Saluint8 RlWhlSpdErr;
    Saluint8 RrWhlSpdErr;
    Saluint8 FrontWhlSpdErr;
    Saluint8 RearWhlSpdErr;
}ErrWhlSpdInfo_t;

typedef struct
{
    Saluint8 PdtErr;
    Saluint8 PdfErr;
}ErrPedlInfo_t;

typedef struct
{
    Saluint8 RearSolErr;
    Saluint8 FrontSolErr;
    Saluint8 EscSolErr;
}ErrVlvInfo_t;

typedef struct
{
    Saluint8 MainCanLineErr;
    Saluint8 EmsTiOutErr;
    Saluint8 TcuTiOutErr;
}CanBusErrInfo_t;

typedef struct
{
    Saluint8 EbdFailure;
    Saluint8 AbsFailure;
    Saluint8 TcsFailure;
    Saluint8 VdcFailure;
}CtrlErrInfo_t;

typedef struct
{
    Saluint8 EcuHwFailure;
    Saluint8 EscEcuHWFailure;
}EcuErrInfo_t;

typedef struct
{
    Saluint8 HigherVoltErr;
    Saluint8 HighVoltErr;
    Saluint8 LowVoltErr;
    Saluint8 LowerVoltErr;
}VoltErrInfo_t;

typedef struct
{
    Saluint8 EscSnsrErr;
}SnsrErrInfo_t;

typedef struct
{
    Saluint8 PedlErr;
}PedErrInfo_t;

typedef struct
{
    Saluint8 McpErr;
}McpErrInfo_t;

typedef struct
{
    Saluint8 YawErr;
    Saluint8 StrErr;
    Saluint8 AyErr;
    Saluint8 AxErr;
}CanErrInfo_t;

typedef struct
{
    Saluint8 MotErr;
}MotErrInfo_t;

typedef struct
{
    Salsint32 MotCurrPhUMeasd;
    Salsint32 MotCurrPhVMeasd;
    Salsint32 MotCurrPhWMeasd;
}MotCurrInfo_t;

typedef struct
{
    Saluint16 MotElecAngle1;
    Saluint16 MotMechAngle1;
}MotAngle1Info_t;

typedef struct
{
    Saluint16 MotElecAngle2;
    Saluint16 MotMechAngle2;
}MotAngle2Info_t;

typedef struct
{
    Salsint32 MotIqMeasd;
    Salsint32 MotIdMeasd;
}MotDqIMeasdInfo_t;

typedef struct
{
    Salsint32 StkPosnMeasd;
    Salsint32 MotElecAngleFild;
    Salsint32 MotMechAngleSpdFild;
}MotRotgAgSigInfo_t;

typedef struct
{
    Salsint16 PdtSig;
}Pdt5msRawInfo_t;

typedef struct
{
    Salsint16 PdfSig;
}Pdf5msRawInfo_t;

typedef struct
{
    Saluint8 MotOrgSetErr;
    Saluint8 MotOrgSet;
}MotOrgSetStInfo_t;

typedef struct
{
    Salsint32 IdRef;
    Salsint32 IqRef;
}MotDqIRefSesInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvReqData[5];
    Saluint16 SecdCutVlvReqData[5];
    Saluint16 PrimCircVlvReqData[5];
    Saluint16 SecdCircVlvReqData[5];
    Saluint16 SimVlvReqData[5];
    Saluint8 PrimCutVlvReq;
    Saluint8 SecdCutVlvReq;
    Saluint8 PrimCircVlvReq;
    Saluint8 SecdCircVlvReq;
    Saluint8 SimVlvReq;
    Saluint8 PrimCutVlvDataLen;
    Saluint8 SecdCutVlvDataLen;
    Saluint8 PrimCircVlvDataLen;
    Saluint8 SecdCircVlvDataLen;
    Saluint8 SimVlvDataLen;
}NormVlvReqSesInfo_t;

typedef struct
{
    Saluint8 FlexBrkASwt;
    Saluint8 FlexBrkBSwt;
}SwtStsFlexBrkInfo_t;

typedef struct
{
    Saluint8 BlsSwt_Esc;
    Saluint8 AvhSwt_Esc;
    Saluint8 EscSwt_Esc;
    Saluint8 HdcSwt_Esc;
    Saluint8 PbSwt_Esc;
    Saluint8 GearRSwt_Esc;
    Saluint8 BlfSwt_Esc;
    Saluint8 ClutchSwt_Esc;
    Saluint8 ItpmsSwt_Esc;
}SwtInfoEsc_t;

typedef struct
{
    Saluint8 EscDisabledBySwt;
    Saluint8 TcsDisabledBySwt;
}EscSwtStInfo_t;

typedef struct
{
    Saluint8 FlWhlPulseCnt;
    Saluint8 FrWhlPulseCnt;
    Saluint8 RlWhlPulseCnt;
    Saluint8 RrWhlPulseCnt;
}WhlPulseCntInfo_t;

typedef struct
{
    Saluint16 FlWhlSpd;
    Saluint16 FrWhlSpd;
    Saluint16 RlWhlSpd;
    Saluint16 RrlWhlSpd;
}WhlSpdInfo_t;

typedef struct
{
    Saluint16 FlWhlEdgeCnt;
    Saluint16 FrWhlEdgeCnt;
    Saluint16 RlWhlEdgeCnt;
    Saluint16 RrWhlEdgeCnt;
}WhlEdgeCntInfo_t;

typedef struct
{
    Saluint8 FlWhlSnsrType;
    Saluint8 FrWhlSnsrType;
    Saluint8 RlWhlSnsrType;
    Saluint8 RrWhlSnsrType;
}WhlSnsrTypeInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvDrvData;
    Saluint16 PrimCircVlvDrvData;
    Saluint16 SecdCutVlvDrvData;
    Saluint16 SecdCircVlvDrvData;
    Saluint16 SimVlvDrvData;
}NormVlvDrvInfo_t;

typedef struct
{
    Saluint16 FlOvDrvData;
    Saluint16 FlIvDrvData;
    Saluint16 FrOvDrvData;
    Saluint16 FrIvDrvData;
    Saluint16 RlOvDrvData;
    Saluint16 RlIvDrvData;
    Saluint16 RrOvDrvData;
    Saluint16 RrIvDrvData;
}WhlVlvDrvInfo_t;

typedef struct
{
    Saluint16 PtcDrvData;
    Saluint16 StcDrvData;
    Saluint16 PesvDrvData;
    Saluint16 SesvDrvData;
}EscVlvDrvInfo_t;

typedef struct
{
    Saluint8 RlyDbcDrv;
    Saluint8 RlyEssDrv;
}RlyDrvInfo_t;

typedef struct
{
    Salsint16 PrimCircPSig;
    Salsint16 SecdCircPSig;
}CircP5msRawInfo_t;

typedef struct
{
    Salsint16 PistPSig;
}PistP5msRawInfo_t;

typedef struct
{
    Salsint16 Pres_MCP_1_100_bar_Esc;
    Salsint16 Pres_FLP_1_100_bar_Esc;
    Salsint16 Pres_FRP_1_100_bar_Esc;
    Salsint16 Pres_RLP_1_100_bar_Esc;
    Salsint16 Pres_RRP_1_100_bar_Esc;
    Salsint16 Pres_MCP_1_100_temp_Esc;
    Salsint16 Pres_FLP_1_100_temp_Esc;
    Salsint16 Pres_FRP_1_100_temp_Esc;
    Salsint16 Pres_RLP_1_100_temp_Esc;
    Salsint16 Pres_RRP_1_100_temp_Esc;
}PresCalcEsc_t;

typedef struct
{
    Salsint32 PdtRaw[5];
}PdtBufInfo_t;

typedef struct
{
    Salsint32 PdfRaw[5];
}PdfBufInfo_t;

typedef struct
{
    Salsint32 KPdtOffsEolReadVal;
    Salsint32 KPdfOffsEolReadVal;
    Salsint32 KPdtOffsDrvgReadVal;
    Salsint32 KPdfOffsDrvgReadVal;
    Salsint32 KPedlSimPOffsEolReadVal;
    Salsint32 KPedlSimPOffsDrvgReadVal;
    Salsint32 KPistPOffsEolReadVal;
    Salsint32 KPistPOffsDrvgReadVal;
    Salsint32 KPrimCircPOffsEolReadVal;
    Salsint32 KPrimCircPOffsDrvgReadVal;
    Salsint32 KSecdCircPOffsEolReadVal;
    Salsint32 KSecdCircPOffsDrvgReadVal;
    Salsint32 KSteerEepOffs;
    Salsint32 KYawEepOffs;
    Salsint32 KLatEepOffs;
    Salsint32 KFsYawEepOffs;
    Salsint32 KYawEepMax;
    Salsint32 KYawEepMin;
    Salsint32 KSteerEepMax;
    Salsint32 KSteerEepMin;
    Salsint32 KLatEepMax;
    Salsint32 KLatEepMin;
    Salsint32 KYawStillEepMax;
    Salsint32 KYawStillEepMin;
    Salsint32 KYawStandEepOffs;
    Salsint32 KYawTmpEepMap[31];
    Salsint32 KAdpvVehMdlVchEep;
    Salsint32 KAdpvVehMdlVcrtRatEep;
    Salsint32 KFlBtcTmpEep;
    Salsint32 KFrBtcTmpEep;
    Salsint32 KRlBtcTmpEep;
    Salsint32 KRrBtcTmpEep;
    Salsint32 KEeBtcsDataEep;
    Salsint32 KLgtSnsrEolEepOffs;
    Salsint32 KLgtSnsrDrvgEepOffs;
    Salsint32 ReadInvld;
    Salsint32 WrReqCmpld;
}LogicEepDataInfo_t;

typedef struct
{
    Saluint16 MAI_MTPVoltage;
}MotorAbsInputEsc_t;

typedef struct
{
    Saluint8 YawRateInvld;
    Saluint8 AyInvld;
    Saluint8 SteeringAngleRxOk;
    Saluint8 AxInvldData;
    Saluint8 Ems1RxErr;
    Saluint8 Ems2RxErr;
    Saluint8 Tcu1RxErr;
    Saluint8 Tcu5RxErr;
    Saluint8 Hcu1RxErr;
    Saluint8 Hcu2RxErr;
    Saluint8 Hcu3RxErr;
}CanRxEemInfo_t;

typedef struct
{
    Saluint8 ImuHwLineErr;
    Saluint8 AxHwLineErr;
    Saluint8 SasHwLineErr;
}SenHwErrInfo_t;

typedef struct
{
    Saluint8 FsrCbsDrv;
    Saluint8 FsrCbsOff;
    Saluint8 FsrCbsReq;
}FsrCbsDrvEemInfo_t;

typedef struct
{
    Saluint8 FsrAbsDrv;
    Saluint8 FsrAbsOff;
    Saluint8 FsrAbsReq;
}FsrAbsDrvEemInfo_t;

typedef struct
{
    Saluint8 MotDrvEn;
    Saluint8 MotDrvRst;
    Saluint8 MotDrvReq;
}MotDrvEemInfo_t;

typedef struct
{
    Saluint8 VlvDrvEn;
    Saluint8 VlvDrvReq;
}VlvDrvEemInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvReqData[5];
    Saluint16 PrimCircVlvReqData[5];
    Saluint16 SecdCutVlvReqData[5];
    Saluint16 SecdCircVlvReqData[5];
    Saluint16 SimVlvReqData[5];
    Saluint8 PrimCutVlvReq;
    Saluint8 PrimCircVlvReq;
    Saluint8 SecdCutVlvReq;
    Saluint8 SecdCircVlvReq;
    Saluint8 SimVlvReq;
    Saluint8 PrimCutVlvDataLen;
    Saluint8 PrimCircVlvDataLen;
    Saluint8 SecdCutVlvDataLen;
    Saluint8 SecdCircVlvDataLen;
    Saluint8 SimVlvDataLen;
}NormVlvReqEemInfo_t;

typedef struct
{
    Saluint16 FlOvReqData[5];
    Saluint16 FlIvReqData[5];
    Saluint16 FrOvReqData[5];
    Saluint16 FrIvReqData[5];
    Saluint16 RlOvReqData[5];
    Saluint16 RlIvReqData[5];
    Saluint16 RrOvReqData[5];
    Saluint16 RrIvReqData[5];
    Saluint8 FlOvReq;
    Saluint8 FlIvReq;
    Saluint8 FrOvReq;
    Saluint8 FrIvReq;
    Saluint8 RlOvReq;
    Saluint8 RlIvReq;
    Saluint8 RrOvReq;
    Saluint8 RrIvReq;
    Saluint8 FlOvDataLen;
    Saluint8 FlIvDataLen;
    Saluint8 FrOvDataLen;
    Saluint8 FrIvDataLen;
    Saluint8 RlOvDataLen;
    Saluint8 RlIvDataLen;
    Saluint8 RrOvDataLen;
    Saluint8 RrIvDataLen;
}WhlVlvReqEemInfo_t;

typedef struct
{
    Saluint8 PtcReqData[5];
    Saluint8 StcReqData[5];
    Saluint8 PesvReqData[5];
    Saluint8 SesvReqData[5];
    Saluint8 PtcReq;
    Saluint8 StcReq;
    Saluint8 PesvReq;
    Saluint8 SesvReq;
}EsvVlvReqEemInfo_t;

typedef struct
{
    Saluint8 FsrAbsOff;
    Saluint8 FsrAbsDrv;
}FsrAbsDrvInfo_t;

typedef struct
{
    Saluint8 FsrCbsOff;
    Saluint8 FsrCbsDrv;
}FsrCbsDrvInfo_t;

typedef struct
{
    Saluint16 MotPwmPhUhighData;
    Saluint16 MotPwmPhVhighData;
    Saluint16 MotPwmPhWhighData;
    Saluint16 MotPwmPhUlowData;
    Saluint16 MotPwmPhVlowData;
    Saluint16 MotPwmPhWlowData;
}MotPwmDataInfo_t;

typedef struct
{
    Salsint16 PrimCircPRaw[5];
    Salsint16 SecdCircPRaw[5];
}CircPBufInfo_t;

typedef struct
{
    Salsint16 PistPRaw[5];
}PistPBufInfo_t;

typedef struct
{
    Salsint16 PedlSimPRaw[5];
}PspBufInfo_t;

typedef struct
{
    Saluint16 FlOvReqData[5];
    Saluint16 FlIvReqData[5];
    Saluint16 FrOvReqData[5];
    Saluint16 FrIvReqData[5];
    Saluint16 RlOvReqData[5];
    Saluint16 RlIvReqData[5];
    Saluint16 RrOvReqData[5];
    Saluint16 RrIvReqData[5];
}WhlVlvReqInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvReqData[5];
    Saluint16 PrimCircVlvReqData[5];
    Saluint16 SecdCutVlvReqData[5];
    Saluint16 SecdCircVlvReqData[5];
    Saluint16 SimVlvReqData[5];
}NormVlvReqInfo_t;

typedef struct
{
    Saluint16 PtcReqData[5];
    Saluint16 StcReqData[5];
    Saluint16 PesvReqData[5];
    Saluint16 SesvReqData[5];
}EscVlvReqInfo_t;

typedef struct
{
    Saluint8 PrimCutPErr;
    Saluint8 SecdCutPErr;
    Saluint8 BbsVlvErr;
}NormVlvSts_t;

typedef struct
{
    Saluint8 FlOvErr;
    Saluint8 FlIvErr;
    Saluint8 FrOvErr;
    Saluint8 FrIvErr;
    Saluint8 RlOvErr;
    Saluint8 RlIvErr;
    Saluint8 RrOvErr;
    Saluint8 RrIvErr;
}WhlVlvSts_t;

typedef struct
{
    Saluint8 Ems1TiOutErr;
    Saluint8 Ems2TiOutErr;
    Saluint8 HcuTiOutErr;
}TiOut_t;

typedef struct
{
    Saluint8 AbsLampDrv;
    Saluint8 EbdLampDrv;
    Saluint8 VdcOffLampDrv;
    Saluint8 VdcLampDrv;
    Saluint8 FnLampDrv;
    Saluint8 AhbWLampOnReq;
    Saluint8 SrvLampOnReq;
}LampInfo_t;

typedef struct
{
    Saluint8 AhbDfctvModFailure;
    Saluint8 AhbDegradedModFailure;
}AhbErrInfo_t;

typedef struct
{
    Salsint32 EbdCtrlFail;
    Salsint32 AbsCtrlFail;
    Salsint32 CbcCtrlFail;
    Salsint32 EdcCtrlFail;
    Salsint32 TcsCtrlFail;
    Salsint32 EscCtrlFail;
    Salsint32 HbbCtrlFail;
    Salsint32 HdcCtrlFail;
    Salsint32 HsaCtrlFail;
    Salsint32 VsmCtrlFail;
    Salsint32 SccCtrlFail;
    Salsint32 CdmCtrlFail;
    Salsint32 AvhCtrlFail;
    Salsint32 PbaCtrlFail;
    Salsint32 BdwCtrlFail;
    Salsint32 EssCtrlFail;
    Salsint32 TvbbCtrlFail;
    Salsint32 TspCtrlFail;
    Salsint32 RopCtrlFail;
    Salsint32 EcdCtrlFail;
    Salsint32 TodCtrlFail;
    Salsint32 AebCtrlFail;
    Salsint32 AdcCtrlFail;
    Salsint32 TcsDiscTempErrFlg;
    Salsint32 HdcDiscTempErrFlg;
    Salsint32 SccDiscTempErrFlg;
    Salsint32 TvbbDiscTempErrFlg;
    Salsint32 SlsCtrlFail;
}ActvBrkCtrlFailFlgsforNewFMInfo_t;

typedef struct
{
    Salsint32 PrkgBrkSusDet;
    Salsint32 GearRSwtInv;
    Salsint32 InitEndFlg;
    Salsint32 CalUIntMot;
    Salsint32 CalUVDD;
    Salsint32 MotInitOnFlg;
    Salsint32 DrvModToLogic;
    Salsint32 CalUMot;
    Salsint32 StandstillFlg;
    Salsint32 MotMonAd;
    Salsint32 AlatSnsr100;
    Salsint32 TurnTblCntr;
    Salsint32 YawSerlNOKFlg;
    Salsint32 YawSNUnmatchFlg;
    Salsint32 YawUnstableFlg;
    Salsint32 YawCorrn;
}ActvBrkCtrlFSIfInfo_t;

typedef struct
{
    Salsint32 DiagSasCaltoAppl;
}SasCalInfo_t;

typedef struct
{
    Salsint32 SenPwr1secOk;
    Salsint32 CbitProcStep;
    Salsint32 SenRasterSus;
    Salsint32 McpHwSusFlg;
    Salsint32 McpSusDet;
    Salsint32 McpSenPwr1sOk;
    Salsint32 YawSenPwr1sOk;
    Salsint32 YawSusDet;
    Salsint32 SteerSenPwr1sOk;
    Salsint32 StrSusDet;
    Salsint32 AySusDet;
    Salsint32 AySenPwr1sOk;
    Salsint32 AxSenPwr1sOk;
    Salsint32 AxSusDet;
}ActvBrkCtrlSnsrStInfo_t;

typedef struct
{
    Salsint32 BlsSigSuspcFlg;
}BlsInvldInfo_t;

typedef struct
{
    Salsint32 FlexBrkSwtFaultDet;
}SwtStsFSInfo_t;

typedef struct
{
    Salsint32 MaiCanSusDet;
    Salsint32 EmsTiOutSusDet;
    Salsint32 TcuTiOutSusDet;
}CanTimeOutStInfo_t;

typedef struct
{
    Salsint32 PrimCircPSigSuspcFlg;
    Salsint32 SecdCircPSigSuspcFlg;
}CircPInvldInfo_t;

typedef struct
{
    Salsint32 PedlTrvlOffsEolCalcReqFlg;
    Salsint32 PSnsrOffsEolCalcReqFlg;
}IdbSnsrOffsEolCalcReqByDiagInfo_t;

typedef struct
{
    Salsint32 PedlSimrPSigSuspcFlg;
}PedlSimrPInvldInfo_t;

typedef struct
{
    Salsint32 PdfSigSuspcFlg;
    Salsint32 PdtSigSuspcFlg;
}PedlTrvlSigInvldInfo_t;

typedef struct
{
    Salsint32 PistPSigFaultFlg;
    Salsint32 PistPSigSuspcFlg;
}PistPInvldInfo_t;

typedef struct
{
    Salsint32 WhlSpdSuspcFrntLe;
    Salsint32 WhlSpdSuspcFrntRi;
    Salsint32 WhlSpdSuspcReLe;
    Salsint32 WhlSpdSuspcReRi;
    Salsint32 SameSideWSSErrDet;
    Salsint32 DiagonalWSSErrDet;
}WhlSpdInvldInfo_t;

typedef struct
{
    Saluint8 MainBusOffFlag;
    Saluint8 SenBusOffFlag;
}CanRxInfo_t;

typedef struct
{
    Salsint16 PrimCircPSig;
    Salsint16 SecdCircPSig;
}CircP1msRawInfo_t;

typedef struct
{
    Salsint16 PistPSig;
}PistP1msRawInfo_t;

typedef struct
{
    Salsint16 PdtSig;
}Pdt1msRawInfo_t;

typedef struct
{
    Saluint8 Bms1SocPc;
}RxBms1Info_t;

typedef struct
{
    Saluint8 YawSerialNum_0;
    Saluint8 YawSerialNum_1;
    Saluint8 YawSerialNum_2;
}RxYawSerialInfo_t;

typedef struct
{
    Saluint8 YawRateValidData;
    Saluint8 YawRateSelfTestStatus;
    Saluint8 YawRateSignal_0;
    Saluint8 YawRateSignal_1;
    Saluint8 SensorOscFreqDev;
    Saluint8 Gyro_Fail;
    Saluint8 Raster_Fail;
    Saluint8 Eep_Fail;
    Saluint8 Batt_Range_Err;
    Saluint8 Asic_Fail;
    Saluint8 Accel_Fail;
    Saluint8 Ram_Fail;
    Saluint8 Rom_Fail;
    Saluint8 Ad_Fail;
    Saluint8 Osc_Fail;
    Saluint8 Watchdog_Rst;
    Saluint8 Plaus_Err_Pst;
    Saluint8 RollingCounter;
    Saluint8 Can_Func_Err;
    Saluint8 AccelEratorRateSig_0;
    Saluint8 AccelEratorRateSig_1;
    Saluint8 LatAccValidData;
    Saluint8 LatAccSelfTestStatus;
}RxYawAccInfo_t;

typedef struct
{
    Saluint8 ShiftClass_Ccan;
}RxTcu6Info_t;

typedef struct
{
    Saluint8 Typ;
    Saluint8 GearTyp;
}RxTcu5Info_t;

typedef struct
{
    Saluint8 Targe;
    Saluint8 GarChange;
    Saluint8 Flt;
    Saluint8 GarSelDisp;
    Saluint16 TQRedReq_PC;
    Saluint16 TQRedReqSlw_PC;
    Saluint8 TQIncReq_PC;
}RxTcu1Info_t;

typedef struct
{
    Saluint16 Angle;
    Saluint8 Speed;
    Saluint8 Ok;
    Saluint8 Cal;
    Saluint8 Trim;
    Saluint8 CheckSum;
    Saluint8 MsgCount;
}RxSasInfo_t;

typedef struct
{
    Saluint8 Flt;
}RxMcu2Info_t;

typedef struct
{
    Saluint16 MoTestTQ_PC;
    Saluint16 MotActRotSpd_RPM;
}RxMcu1Info_t;

typedef struct
{
    Saluint8 IntSenFltSymtmActive;
    Saluint8 IntSenFaultPresent;
    Saluint8 LongAccSenCirErrPre;
    Saluint8 LonACSenRanChkErrPre;
    Saluint8 LongRollingCounter;
    Saluint8 IntTempSensorFault;
    Saluint8 LongAccInvalidData;
    Saluint8 LongAccSelfTstStatus;
    Saluint8 LongAccRateSignal_0;
    Saluint8 LongAccRateSignal_1;
}RxLongAccInfo_t;

typedef struct
{
    Saluint8 HevMod;
}RxHcu5Info_t;

typedef struct
{
    Saluint16 TmIntQcMDBINV_PC;
    Saluint16 MotTQCMC_PC;
    Saluint16 MotTQCMDBINV_PC;
}RxHcu3Info_t;

typedef struct
{
    Saluint8 ServiceMod;
    Saluint8 RegenENA;
    Saluint16 RegenBRKTQ_NM;
    Saluint16 CrpTQ_NM;
    Saluint16 WhlDEMTQ_NM;
}RxHcu2Info_t;

typedef struct
{
    Saluint8 EngCltStat;
    Saluint8 HEVRDY;
    Saluint8 EngTQCmdBinV_PC;
    Saluint8 EngTQCmd_PC;
}RxHcu1Info_t;

typedef struct
{
    Saluint8 OutTemp_SNR_C;
}RxFact1Info_t;

typedef struct
{
    Saluint8 EngColTemp_C;
}RxEms3Info_t;

typedef struct
{
    Saluint8 EngSpdErr;
    Saluint8 AccPedDep_PC;
    Saluint8 Tps_PC;
}RxEms2Info_t;

typedef struct
{
    Saluint8 TqStd_NM;
    Saluint8 ActINDTQ_PC;
    Saluint16 EngSpd_RPM;
    Saluint8 IndTQ_PC;
    Saluint8 FrictTQ_PC;
}RxEms1Info_t;

typedef struct
{
    Saluint8 IGN_SW;
}RxClu2Info_t;

typedef struct
{
    Saluint8 P_Brake_Act;
    Saluint8 Cf_Clu_BrakeFluIDSW;
}RxClu1Info_t;

typedef struct
{
    Saluint8 Bms1MsgOkFlg;
    Saluint8 YawSerialMsgOkFlg;
    Saluint8 YawAccMsgOkFlg;
    Saluint8 Tcu6MsgOkFlg;
    Saluint8 Tcu5MsgOkFlg;
    Saluint8 Tcu1MsgOkFlg;
    Saluint8 SasMsgOkFlg;
    Saluint8 Mcu2MsgOkFlg;
    Saluint8 Mcu1MsgOkFlg;
    Saluint8 LongAccMsgOkFlg;
    Saluint8 Hcu5MsgOkFlg;
    Saluint8 Hcu3MsgOkFlg;
    Saluint8 Hcu2MsgOkFlg;
    Saluint8 Hcu1MsgOkFlg;
    Saluint8 Fatc1MsgOkFlg;
    Saluint8 Ems3MsgOkFlg;
    Saluint8 Ems2MsgOkFlg;
    Saluint8 Ems1MsgOkFlg;
    Saluint8 Clu2MsgOkFlg;
    Saluint8 Clu1MsgOkFlg;
}RxMsgOkFlgInfo_t;

typedef struct
{
    Saluint32 TestYawRateSensor;
    Saluint32 TestAcclSensor;
    Saluint32 YawSerialNumberReq;
}TxYawCbitInfo_t;

typedef struct
{
    Saluint32 WhlSpdFL;
    Saluint32 WhlSpdFR;
    Saluint32 WhlSpdRL;
    Saluint32 WhlSpdRR;
}TxWhlSpdInfo_t;

typedef struct
{
    Saluint32 WhlPulFL;
    Saluint32 WhlPulFR;
    Saluint32 WhlPulRL;
    Saluint32 WhlPulRR;
}TxWhlPulInfo_t;

typedef struct
{
    Saluint32 CfBrkAbsWLMP;
    Saluint32 CfBrkEbdWLMP;
    Saluint32 CfBrkTcsWLMP;
    Saluint32 CfBrkTcsFLMP;
    Saluint32 CfBrkAbsDIAG;
    Saluint32 CrBrkWheelFL_KMH;
    Saluint32 CrBrkWheelFR_KMH;
    Saluint32 CrBrkWheelRL_KMH;
    Saluint32 CrBrkWheelRR_KMH;
}TxTcs5Info_t;

typedef struct
{
    Saluint32 CfBrkTcsREQ;
    Saluint32 CfBrkTcsPAS;
    Saluint32 CfBrkTcsDEF;
    Saluint32 CfBrkTcsCTL;
    Saluint32 CfBrkAbsACT;
    Saluint32 CfBrkAbsDEF;
    Saluint32 CfBrkEbdDEF;
    Saluint32 CfBrkTcsGSC;
    Saluint32 CfBrkEspPAS;
    Saluint32 CfBrkEspDEF;
    Saluint32 CfBrkEspCTL;
    Saluint32 CfBrkMsrREQ;
    Saluint32 CfBrkTcsMFRN;
    Saluint32 CfBrkMinGEAR;
    Saluint32 CfBrkMaxGEAR;
    Saluint32 BrakeLight;
    Saluint32 HacCtl;
    Saluint32 HacPas;
    Saluint32 HacDef;
    Saluint32 CrBrkTQI_PC;
    Saluint32 CrBrkTQIMSR_PC;
    Saluint32 CrBrkTQISLW_PC;
    Saluint32 CrEbsMSGCHKSUM;
}TxTcs1Info_t;

typedef struct
{
    Saluint32 CalSas_CCW;
    Saluint32 CalSas_Lws_CID;
}TxSasCalInfo_t;

typedef struct
{
    Saluint32 LatAccel;
    Saluint32 LatAccelStat;
    Saluint32 LatAccelDiag;
    Saluint32 LongAccel;
    Saluint32 LongAccelStat;
    Saluint32 LongAccelDiag;
    Saluint32 YawRate;
    Saluint32 YawRateStat;
    Saluint32 YawRateDiag;
    Saluint32 CylPres;
    Saluint32 CylPresStat;
    Saluint32 CylPresDiag;
}TxEsp2Info_t;

typedef struct
{
    Saluint32 CfBrkEbsStat;
    Saluint32 CrBrkRegenTQLimit_NM;
    Saluint32 CrBrkEstTot_NM;
    Saluint32 CfBrkSnsFail;
    Saluint32 CfBrkRbsWLamp;
    Saluint32 CfBrkVacuumSysDef;
    Saluint32 CrBrkEstHyd_NM;
    Saluint32 CrBrkStkDep_PC;
    Saluint8 CfBrkPgmRun1;
}TxEbs1Info_t;

typedef struct
{
    Saluint32 CfAhbWLMP;
    Saluint32 CfAhbDiag;
    Saluint32 CfAhbAct;
    Saluint32 CfAhbDef;
    Saluint32 CfAhbSLMP;
    Saluint32 CfBrkAhbSTDep_PC;
    Saluint32 CfBrkBuzzR;
    Saluint32 CfBrkPedalCalStatus;
    Saluint32 CfBrkAhbSnsFail;
    Saluint32 WhlSpdFLAhb;
    Saluint32 WhlSpdFRAhb;
    Saluint32 CrAhbMsgChkSum;
}TxAhb1Info_t;

typedef struct
{
    Saluint8 MotorDutyDataFlg;
    Saluint16 MotorDuty;
}DcMtrDutyData_t;

typedef struct
{
    Saluint8 MotorFreqDataFlg;
    Saluint16 MotorFreq;
}DcMtrFreqData_t;


typedef Saluint8 CanRxGearSelDispErrInfo_t;
typedef Saluint8 DiagSci_t;
typedef Saluint16 VdcLink_t;
typedef Saluint8 BlsSwt_t;
typedef Saluint8 EscSwt_t;
typedef Saluint8 AvhSwt_t;
typedef Saluint8 BsSwt_t;
typedef Saluint8 DoorSwt_t;
typedef Saluint8 HzrdSwt_t;
typedef Saluint8 HdcSwt_t;
typedef Saluint8 PbSwt_t;
typedef Saluint8 IgnOnOffSts_t;
typedef Saluint8 IgnEdgeSts_t;
typedef Saluint8 MomEcuInhibit_t;
typedef Saluint8 PrlyEcuInhibit_t;
typedef Salsint16 PedlSimPRaw_t;
typedef Saluint8 DiagAhbSci_t;
typedef Saluint8 FuncInhibitProxySts_t;
typedef Saluint8 FuncInhibitDiagSts_t;
typedef Saluint8 FuncInhibitFsrSts_t;
typedef Saluint8 FuncInhibitAcmioSts_t;
typedef Saluint8 FuncInhibitAcmctlSts_t;
typedef Saluint8 FuncInhibitMspSts_t;
typedef Saluint8 FuncInhibitNvmSts_t;
typedef Saluint8 FuncInhibitWssSts_t;
typedef Saluint8 FuncInhibitPedalSts_t;
typedef Saluint8 FuncInhibitPressSts_t;
typedef Saluint8 FuncInhibitRlySts_t;
typedef Saluint8 FuncInhibitSesSts_t;
typedef Saluint8 FuncInhibitSwtSts_t;
typedef Saluint8 FuncInhibitVlvSts_t;
typedef Saluint8 FuncInhibitPrlySts_t;
typedef Saluint8 FuncInhibitMomSts_t;
typedef Saluint8 FuncInhibitVlvdSts_t;
typedef Saluint8 FuncInhibitSpimSts_t;
typedef Saluint8 FuncInhibitAdcifSts_t;
typedef Saluint8 FuncInhibitIcuSts_t;
typedef Saluint8 FuncInhibitMpsSts_t;
typedef Saluint8 FuncInhibitRegSts_t;
typedef Saluint8 FuncInhibitAsicSts_t;
typedef Saluint8 FuncInhibitCanTrcvSts_t;
typedef Saluint8 FuncInhibitGdSts_t;
typedef Saluint8 FuncInhibitIocSts_t;
typedef Saluint8 FuncInhibitSigProcSts_t;
typedef Saluint8 FuncInhibitDetSts_t;
typedef Saluint8 FuncInhibitBbcSts_t;
typedef Saluint8 FuncInhibitRbcSts_t;
typedef Saluint8 FuncInhibitArbiSts_t;
typedef Saluint8 FuncInhibitPctrlSts_t;
typedef Saluint8 FuncInhibitMccSts_t;
typedef Saluint8 FuncInhibitVlvActSts_t;
typedef Saluint8 FuncInhibitAbcSts_t;
typedef Saluint8 EcuModeSts_t;
typedef Saluint8 VlvSync_t;
typedef Saluint8 EscSwtFail_t;
typedef Saluint8 AsicErr_t;
typedef Saluint8 VddIcVoltErr_t;
typedef Saluint8 ResidualModeEntryFlg_t;
typedef Saluint8 BuzzerInfo_t;
typedef Saluint8 DiagClr_t;
typedef Saluint8 FsrDcMtrShutDwn_t;
typedef Saluint8 FsrEnDrDrv_t;
typedef Salsint16 VacuumCalcType_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SAL_TYPES_H_ */
/** @} */
