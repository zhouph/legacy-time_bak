/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef SAL_DATAHANDLE_TYPES_H_
#define SAL_DATAHANDLE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef FuncInhibitPedalSts_t Eem_SuspcDetnFuncInhibitPedalSts_t;
typedef TiOut_t Eem_CanMsgTiOutDetnTiOut_t;
typedef FsrEnDrDrv_t Fsr_ActrFsrEnDrDrv_t;
typedef MotDqIMeasdInfo_t Acmctl_CtrlMotDqIMeasdInfo_t;
typedef TxAhb1Info_t Proxy_TxTxAhb1Info_t;
typedef ErrPedlInfo_t Eem_PdlSigHwChkErrPedlInfo_t;
typedef PistP1msRawInfo_t Press_SenPistP1msRawInfo_t;
typedef FuncInhibitAsicSts_t Eem_SuspcDetnFuncInhibitAsicSts_t;
typedef RxMcu1Info_t Proxy_RxByComRxMcu1Info_t;
typedef MotDqIRefSesInfo_t Ses_CtrlMotDqIRefSesInfo_t;
typedef CanTimeOutStInfo_t Eem_SuspcDetnCanTimeOutStInfo_t;
typedef WhlSpdInvldInfo_t Eem_SuspcDetnWhlSpdInvldInfo_t;
typedef PistPInvldInfo_t Eem_SuspcDetnPistPInvldInfo_t;
typedef VddIcVoltErr_t Eem_VoltFaultDetnVddIcVoltErr_t;
typedef MotPwmDataInfo_t Acmio_ActrMotPwmDataInfo_t;
typedef SnsrErrInfo_t Eem_EscSnrFaultDetnSnsrErrInfo_t;
typedef WhlVlvReqEemInfo_t Eem_ResidualModReqWhlVlvReqEemInfo_t;
typedef CircP1msRawInfo_t Press_SenCircP1msRawInfo_t;
typedef FuncInhibitDiagSts_t Eem_SuspcDetnFuncInhibitDiagSts_t;
typedef EcuErrInfo_t Eem_EcuErrDetnEcuErrInfo_t;
typedef RxHcu1Info_t Proxy_RxByComRxHcu1Info_t;
typedef BlsSwt_t Swt_SenBlsSwt_t;
typedef CanRxEngTempInfo_t Proxy_RxCanRxEngTempInfo_t;
typedef CanRxRegenInfo_t Proxy_RxCanRxRegenInfo_t;
typedef TxEbs1Info_t Proxy_TxTxEbs1Info_t;
typedef DoorSwt_t Swt_SenDoorSwt_t;
typedef CircP5msRawInfo_t Press_SenSyncCircP5msRawInfo_t;
typedef TxWhlPulInfo_t Proxy_TxTxWhlPulInfo_t;
typedef PedlTrvlSigInvldInfo_t Eem_SuspcDetnPedlTrvlSigInvldInfo_t;
typedef FuncInhibitPctrlSts_t Eem_SuspcDetnFuncInhibitPctrlSts_t;
typedef MotDrvEemInfo_t Eem_ResidualModReqMotDrvEemInfo_t;
typedef ResidualModeEntryFlg_t Eem_ResidualModReqResidualModeEntryFlg_t;
typedef SwtStsFlexBrkInfo_t Swt_SenSwtStsFlexBrkInfo_t;
typedef WhlPulseCntInfo_t Wss_SenWhlPulseCntInfo_t;
typedef EscSwtFail_t Eem_DisbySwDetnEscSwtFail_t;
typedef Pdt5msRawInfo_t Pedal_SenSyncPdt5msRawInfo_t;
typedef RxHcu2Info_t Proxy_RxByComRxHcu2Info_t;
typedef FuncInhibitNvmSts_t Eem_SuspcDetnFuncInhibitNvmSts_t;
typedef FsrCbsDrvEemInfo_t Eem_ResidualModReqFsrCbsDrvEemInfo_t;
typedef DiagSci_t Diag_HndlrDiagSci_t;
typedef BsSwt_t Swt_SenBsSwt_t;
typedef BuzzerInfo_t Eem_BuzReqBuzzerInfo_t;
typedef CircPBufInfo_t Press_SenCircPBufInfo_t;
typedef DiagClr_t Diag_HndlrDiagClr_t;
typedef LogicEepDataInfo_t Nvm_HndlrLogicEepDataInfo_t;
typedef IgnEdgeSts_t Prly_HndlrIgnEdgeSts_t;
typedef CanRxEemInfo_t Proxy_RxCanRxEemInfo_t;
typedef ErrVlvInfo_t Eem_AsicErrDetnErrVlvInfo_t;
typedef FuncInhibitCanTrcvSts_t Eem_SuspcDetnFuncInhibitCanTrcvSts_t;
typedef VacuumCalcType_t Vacuum_MainVacuumCalcType_t;
typedef CanRxGearSelDispErrInfo_t Proxy_RxCanRxGearSelDispErrInfo_t;
typedef FuncInhibitDetSts_t Eem_SuspcDetnFuncInhibitDetSts_t;
typedef FuncInhibitAcmioSts_t Eem_SuspcDetnFuncInhibitAcmioSts_t;
typedef CanBusErrInfo_t Eem_CanMsgTiOutDetnCanBusErrInfo_t;
typedef FuncInhibitVlvSts_t Eem_SuspcDetnFuncInhibitVlvSts_t;
typedef FuncInhibitRbcSts_t Eem_SuspcDetnFuncInhibitRbcSts_t;
typedef MotAngle2Info_t Acmio_SenMotAngle2Info_t;
typedef FsrDcMtrShutDwn_t Fsr_ActrFsrDcMtrShutDwn_t;
typedef FsrAbsDrvEemInfo_t Eem_ResidualModReqFsrAbsDrvEemInfo_t;
typedef RxTcu6Info_t Proxy_RxByComRxTcu6Info_t;
typedef DcMtrFreqData_t Dcmio_ActrDcMtrFreqData_t;
typedef SwtInfoEsc_t Swt_SenSwtInfoEsc_t;
typedef AhbErrInfo_t Eem_AhbModFailDetnAhbErrInfo_t;
typedef MotOrgSetStInfo_t Ses_CtrlMotOrgSetStInfo_t;
typedef FuncInhibitPrlySts_t Eem_SuspcDetnFuncInhibitPrlySts_t;
typedef FsrCbsDrvInfo_t Fsr_ActrFsrCbsDrvInfo_t;
typedef FuncInhibitAdcifSts_t Eem_SuspcDetnFuncInhibitAdcifSts_t;
typedef PistP5msRawInfo_t Press_SenSyncPistP5msRawInfo_t;
typedef FuncInhibitFsrSts_t Eem_SuspcDetnFuncInhibitFsrSts_t;
typedef RxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo_t;
typedef WhlSpdInfo_t Wss_SenWhlSpdInfo_t;
typedef PedlSimrPInvldInfo_t Eem_SuspcDetnPedlSimrPInvldInfo_t;
typedef NormVlvDrvInfo_t Vlv_ActrNormVlvDrvInfo_t;
typedef PedlSimPRaw_t Press_SenSyncPedlSimPRaw_t;
typedef IdbSnsrOffsEolCalcReqByDiagInfo_t Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t;
typedef FuncInhibitRegSts_t Eem_SuspcDetnFuncInhibitRegSts_t;
typedef FuncInhibitSesSts_t Eem_SuspcDetnFuncInhibitSesSts_t;
typedef RxMcu2Info_t Proxy_RxByComRxMcu2Info_t;
typedef RxEms3Info_t Proxy_RxByComRxEms3Info_t;
typedef FuncInhibitIocSts_t Eem_SuspcDetnFuncInhibitIocSts_t;
typedef CanRxEscInfo_t Proxy_RxCanRxEscInfo_t;
typedef FsrAbsDrvInfo_t Fsr_ActrFsrAbsDrvInfo_t;
typedef EcuModeSts_t Mom_HndlrEcuModeSts_t;
typedef FuncInhibitSigProcSts_t Eem_SuspcDetnFuncInhibitSigProcSts_t;
typedef FuncInhibitMspSts_t Eem_SuspcDetnFuncInhibitMspSts_t;
typedef MotErrInfo_t Eem_MotErrtDetnMotErrInfo_t;
typedef PbSwt_t Swt_SenPbSwt_t;
typedef FuncInhibitVlvActSts_t Eem_SuspcDetnFuncInhibitVlvActSts_t;
typedef RxEms1Info_t Proxy_RxByComRxEms1Info_t;
typedef RxEms2Info_t Proxy_RxByComRxEms2Info_t;
typedef FuncInhibitPressSts_t Eem_SuspcDetnFuncInhibitPressSts_t;
typedef MomEcuInhibit_t Mom_HndlrMomEcuInhibit_t;
typedef MotorAbsInputEsc_t Dcmio_SenMotorAbsInputEsc_t;
typedef RxClu1Info_t Proxy_RxByComRxClu1Info_t;
typedef FuncInhibitGdSts_t Eem_SuspcDetnFuncInhibitGdSts_t;
typedef FuncInhibitSpimSts_t Eem_SuspcDetnFuncInhibitSpimSts_t;
typedef McpErrInfo_t Eem_SuspcDetnMcpErrInfo_t;
typedef TxYawCbitInfo_t Proxy_TxTxYawCbitInfo_t;
typedef FuncInhibitArbiSts_t Eem_SuspcDetnFuncInhibitArbiSts_t;
typedef LampInfo_t Eem_LampReqLampInfo_t;
typedef EscVlvReqInfo_t Vlv_ActrSyncEscVlvReqInfo_t;
typedef Pdf5msRawInfo_t Pedal_SenSyncPdf5msRawInfo_t;
typedef RxClu2Info_t Proxy_RxByComRxClu2Info_t;
typedef CtrlErrInfo_t Eem_ErrFlagSetCtrlErrInfo_t;
typedef NormVlvReqInfo_t Vlv_ActrSyncNormVlvReqInfo_t;
typedef AsicErr_t Eem_AsicErrDetnAsicErr_t;
typedef EscSwt_t Swt_SenEscSwt_t;
typedef CanRxIdbInfo_t Proxy_RxCanRxIdbInfo_t;
typedef FuncInhibitWssSts_t Eem_SuspcDetnFuncInhibitWssSts_t;
typedef PspBufInfo_t Press_SenPspBufInfo_t;
typedef PdtBufInfo_t Pedal_SenPdtBufInfo_t;
typedef TxEsp2Info_t Proxy_TxTxEsp2Info_t;
typedef FuncInhibitSwtSts_t Eem_SuspcDetnFuncInhibitSwtSts_t;
typedef PistPBufInfo_t Press_SenPistPBufInfo_t;
typedef MotCurrInfo_t Acmio_SenMotCurrInfo_t;
typedef NormVlvReqEemInfo_t Eem_ResidualModReqNormVlvReqEemInfo_t;
typedef FuncInhibitAcmctlSts_t Eem_SuspcDetnFuncInhibitAcmctlSts_t;
typedef VlvDrvEemInfo_t Eem_ResidualModReqVlvDrvEemInfo_t;
typedef RxYawSerialInfo_t Proxy_RxByComRxYawSerialInfo_t;
typedef MotReqDataEemInfo_t Eem_ResidualModReqMotReqDataEemInfo_t;
typedef MotAngle1Info_t Acmio_SenMotAngle1Info_t;
typedef DiagAhbSci_t Diag_HndlrDiagAhbSci_t;
typedef PedErrInfo_t Eem_PdlSigHwChkPedErrInfo_t;
typedef FuncInhibitMomSts_t Eem_SuspcDetnFuncInhibitMomSts_t;
typedef SasCalInfo_t Diag_HndlrSasCalInfo_t;
typedef ActvBrkCtrlSnsrStInfo_t Eem_SuspcDetnActvBrkCtrlSnsrStInfo_t;
typedef CanRxAccelPedlInfo_t Proxy_RxCanRxAccelPedlInfo_t;
typedef ActvBrkCtrlFSIfInfo_t Eem_SuspcDetnActvBrkCtrlFSIfInfo_t;
typedef PresCalcEsc_t Press_SenPresCalcEsc_t;
typedef EscVlvReqAswInfo_t Vlv_DummyEscVlvReqAswInfo_t;
typedef VdcLink_t Acmio_SenVdcLink_t;
typedef RxHcu3Info_t Proxy_RxByComRxHcu3Info_t;
typedef PdfBufInfo_t Pedal_SenPdfBufInfo_t;
typedef MotReqDataDiagInfo_t Diag_HndlrMotReqDataDiagInfo_t;
typedef ErrInfo_t Eem_SuspcDetnErrInfo_t;
typedef NormVlvReqSesInfo_t Ses_CtrlNormVlvReqSesInfo_t;
typedef RxFact1Info_t Proxy_RxByComRxFact1Info_t;
typedef WhlVlvReqInfo_t Vlv_ActrSyncWhlVlvReqInfo_t;
typedef TxTcs5Info_t Proxy_TxTxTcs5Info_t;
typedef FuncInhibitIcuSts_t Eem_SuspcDetnFuncInhibitIcuSts_t;
typedef RxLongAccInfo_t Proxy_RxByComRxLongAccInfo_t;
typedef SwtStsFSInfo_t Eem_SuspcDetnSwtStsFSInfo_t;
typedef MotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo_t;
typedef HzrdSwt_t Swt_SenHzrdSwt_t;
typedef WhlEdgeCntInfo_t Wss_SenWhlEdgeCntInfo_t;
typedef ActvBrkCtrlFailFlgsforNewFMInfo_t Eem_SuspcDetnActvBrkCtrlFailFlgsforNewFMInfo_t;
typedef SenHwErrInfo_t Eem_EscSnrFaultDetnSenHwErrInfo_t;
typedef TxTcs1Info_t Proxy_TxTxTcs1Info_t;
typedef CanRxInfo_t Proxy_RxCanRxInfo_t;
typedef WhlVlvDrvInfo_t Vlv_ActrWhlVlvDrvInfo_t;
typedef FuncInhibitAbcSts_t Eem_SuspcDetnFuncInhibitAbcSts_t;
typedef RxTcu5Info_t Proxy_RxByComRxTcu5Info_t;
typedef DcMtrDutyData_t Dcmio_ActrDcMtrDutyData_t;
typedef FuncInhibitMccSts_t Eem_SuspcDetnFuncInhibitMccSts_t;
typedef EscVlvDrvInfo_t Vlv_ActrEscVlvDrvInfo_t;
typedef VlvSync_t Vlv_ActrSyncVlvSync_t;
typedef WhlVlvSts_t Eem_AsicErrDetnWhlVlvSts_t;
typedef FuncInhibitVlvdSts_t Eem_SuspcDetnFuncInhibitVlvdSts_t;
typedef ErrWhlSpdInfo_t Eem_AsicErrDetnErrWhlSpdInfo_t;
typedef MotReqDataAcmctlInfo_t Acmctl_CtrlMotReqDataAcmctlInfo_t;
typedef TxSasCalInfo_t Proxy_TxTxSasCalInfo_t;
typedef FuncInhibitRlySts_t Eem_SuspcDetnFuncInhibitRlySts_t;
typedef RlyDrvInfo_t Rly_ActrRlyDrvInfo_t;
typedef RxTcu1Info_t Proxy_RxByComRxTcu1Info_t;
typedef FuncInhibitMpsSts_t Eem_SuspcDetnFuncInhibitMpsSts_t;
typedef RxSasInfo_t Proxy_RxByComRxSasInfo_t;
typedef HdcSwt_t Swt_SenHdcSwt_t;
typedef TxWhlSpdInfo_t Proxy_TxTxWhlSpdInfo_t;
typedef IgnOnOffSts_t Prly_HndlrIgnOnOffSts_t;
typedef EscSwtStInfo_t Eem_DisbySwDetnEscSwtStInfo_t;
typedef RxBms1Info_t Proxy_RxByComRxBms1Info_t;
typedef BlsInvldInfo_t Eem_SuspcDetnBlsInvldInfo_t;
typedef WhlSnsrTypeInfo_t Wss_SenWhlSnsrTypeInfo_t;
typedef AvhSwt_t Swt_SenAvhSwt_t;
typedef CanErrInfo_t Eem_EscSnrFaultDetnCanErrInfo_t;
typedef VoltErrInfo_t Eem_VoltFaultDetnVoltErrInfo_t;
typedef FuncInhibitBbcSts_t Eem_SuspcDetnFuncInhibitBbcSts_t;
typedef ErrCircPInfo_t Eem_BbsVlvErrDetnErrCircPInfo_t;
typedef RxYawAccInfo_t Proxy_RxByComRxYawAccInfo_t;
typedef EsvVlvReqEemInfo_t Eem_ResidualModReqEsvVlvReqEemInfo_t;
typedef FuncInhibitProxySts_t Eem_SuspcDetnFuncInhibitProxySts_t;
typedef Pdt1msRawInfo_t Pedal_SenPdt1msRawInfo_t;
typedef RxHcu5Info_t Proxy_RxByComRxHcu5Info_t;
typedef CircPInvldInfo_t Eem_SuspcDetnCircPInvldInfo_t;
typedef NormVlvSts_t Eem_BbsVlvErrDetnNormVlvSts_t;
typedef PrlyEcuInhibit_t Prly_HndlrPrlyEcuInhibit_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SAL_DATAHANDLE_TYPES_H_ */
/** @} */
