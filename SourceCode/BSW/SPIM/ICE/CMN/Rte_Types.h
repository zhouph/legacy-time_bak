/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef RTE_TYPES_H_
#define RTE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef sint8 Rtesint8;
typedef uint8 Rteuint8;
typedef sint16 Rtesint16;
typedef uint16 Rteuint16;
typedef sint32 Rtesint32;
typedef uint32 Rteuint32;
typedef float32 Rtefloat32;
typedef float64 Rtefloat64;

typedef struct
{
    Rtesint32 TqIntvTCS;
    Rtesint32 TqIntvMsr;
    Rtesint32 TqIntvSlowTCS;
    Rtesint32 MinGear;
    Rtesint32 MaxGear;
    Rtesint32 TcsReq;
    Rtesint32 TcsCtrl;
    Rtesint32 AbsAct;
    Rtesint32 TcsGearShiftChr;
    Rtesint32 EspCtrl;
    Rtesint32 MsrReq;
    Rtesint32 TcsProductInfo;
}CanTxInfo_t;

typedef struct
{
    Rtesint32 FlIvReqData[10];
    Rtesint32 FrIvReqData[10];
    Rtesint32 RlIvReqData[10];
    Rtesint32 RrIvReqData[10];
    Rtesint32 FlOvReqData[10];
    Rtesint32 FrOvReqData[10];
    Rtesint32 RlOvReqData[10];
    Rtesint32 RrOvReqData[10];
    Rtesint32 FlIvReq;
    Rtesint32 FrIvReq;
    Rtesint32 RlIvReq;
    Rtesint32 RrIvReq;
    Rtesint32 FlOvReq;
    Rtesint32 FrOvReq;
    Rtesint32 RlOvReq;
    Rtesint32 RrOvReq;
    Rteuint8 FlIvDataLen;
    Rteuint8 FrIvDataLen;
    Rteuint8 RlIvDataLen;
    Rteuint8 RrIvDataLen;
    Rteuint8 FlOvDataLen;
    Rteuint8 FrOvDataLen;
    Rteuint8 RlOvDataLen;
    Rteuint8 RrOvDataLen;
}WhlVlvReqAbcInfo_t;

typedef struct
{
    Rtesint32 IdRef;
    Rtesint32 IqRef;
}MotDqIRefMccInfo_t;

typedef struct
{
    Rtesint32 TarRgnBrkTq;
    Rtesint32 VirtStkDep;
    Rtesint32 VirtStkValid;
    Rtesint32 EstTotBrkForce;
    Rtesint32 EstHydBrkForce;
    Rtesint32 EhbStat;
}TarRgnBrkTqInfo_t;

typedef struct
{
    Rteuint16 PrimCutVlvReqData[5];
    Rteuint16 SecdCutVlvReqData[5];
    Rteuint16 PrimCircVlvReqData[5];
    Rteuint16 SecdCircVlvReqData[5];
    Rteuint16 SimVlvReqData[5];
    Rteuint8 PrimCutVlvReq;
    Rteuint8 SecdCutVlvReq;
    Rteuint8 PrimCircVlvReq;
    Rteuint8 SecdCircVlvReq;
    Rteuint8 SimVlvReq;
    Rteuint8 PrimCutVlvDataLen;
    Rteuint8 SecdCutVlvDataLen;
    Rteuint8 PrimCircVlvDataLen;
    Rteuint8 SecdCircVlvDataLen;
    Rteuint8 SimVlvDataLen;
}NormVlvReqVlvActInfo_t;

typedef struct
{
    Rtesint32 AbsActFlg;
    Rtesint32 AbsDefectFlg;
    Rtesint32 AbsTarPFrntLe;
    Rtesint32 AbsTarPFrntRi;
    Rtesint32 AbsTarPReLe;
    Rtesint32 AbsTarPReRi;
    Rtesint32 FrntWhlSlip;
    Rtesint32 AbsDesTarP;
    Rtesint32 AbsDesTarPReqFlg;
    Rtesint32 AbsCtrlFadeOutFlg;
}AbsCtrlInfo_t;

typedef struct
{
    Rtesint32 AvhActFlg;
    Rtesint32 AvhDefectFlg;
    Rtesint32 AvhTarP;
}AvhCtrlInfo_t;

typedef struct
{
    Rtesint32 BaActFlg;
    Rtesint32 BaCDefectFlg;
    Rtesint32 BaTarP;
}BaCtrlInfo_t;

typedef struct
{
    Rtesint32 EbdActFlg;
    Rtesint32 EbdDefectFlg;
    Rtesint32 EbdTarPFrntLe;
    Rtesint32 EbdTarPFrntRi;
    Rtesint32 EbdTarPReLe;
    Rtesint32 EbdTarPReRi;
}EbdCtrlInfo_t;

typedef struct
{
    Rtesint32 EbpActFlg;
    Rtesint32 EbpDefectFlg;
    Rtesint32 EbpTarP;
}EbpCtrlInfo_t;

typedef struct
{
    Rtesint32 EpbiActFlg;
    Rtesint32 EpbiDefectFlg;
    Rtesint32 EpbiTarP;
}EpbiCtrlInfo_t;

typedef struct
{
    Rtesint32 EscActFlg;
    Rtesint32 EscDefectFlg;
    Rtesint32 EscTarPFrntLe;
    Rtesint32 EscTarPFrntRi;
    Rtesint32 EscTarPReLe;
    Rtesint32 EscTarPReRi;
}EscCtrlInfo_t;

typedef struct
{
    Rtesint32 HdcActFlg;
    Rtesint32 HdcDefectFlg;
    Rtesint32 HdcTarP;
}HdcCtrlInfo_t;

typedef struct
{
    Rtesint32 HsaActFlg;
    Rtesint32 HsaDefectFlg;
    Rtesint32 HsaTarP;
}HsaCtrlInfo_t;

typedef struct
{
    Rtesint32 SccActFlg;
    Rtesint32 SccDefectFlg;
    Rtesint32 SccTarP;
}SccCtrlInfo_t;

typedef struct
{
    Rtesint32 StkRecvryCtrlState;
}StkRecvryCtrlIfInfo_t;

typedef struct
{
    Rtesint32 TcsActFlg;
    Rtesint32 TcsDefectFlg;
    Rtesint32 TcsTarPFrntLe;
    Rtesint32 TcsTarPFrntRi;
    Rtesint32 TcsTarPReLe;
    Rtesint32 TcsTarPReRi;
    Rtesint32 BothDrvgWhlBrkCtlReqFlg;
}TcsCtrlInfo_t;

typedef struct
{
    Rtesint32 TvbbActFlg;
    Rtesint32 TvbbDefectFlg;
    Rtesint32 TvbbTarP;
}TvbbCtrlInfo_t;

typedef struct
{
    Rtesint32 LowSpdCtrlInhibitFlg;
    Rtesint32 StopdVehCtrlModFlg;
    Rtesint32 VehStandStillStFlg;
    Rtesint32 InhibitFlg;
}BaseBrkCtrlModInfo_t;

typedef struct
{
    Rtesint32 BrkPedlStatus;
    Rtesint32 DrvrIntendToRelsBrk;
    Rtesint32 PanicBrkStFlg;
    Rtesint32 PedlReldStFlg1;
    Rtesint32 PedlReldStFlg2;
    Rtesint32 PedlReldStUsingPedlSimrPFlg;
}BrkPedlStatusInfo_t;

typedef struct
{
    Rtesint32 PanicBrkSt1msFlg;
}BrkPedlStatus1msInfo_t;

typedef struct
{
    Rtesint32 PrimCircPFild;
    Rtesint32 PrimCircPFild_1_100Bar;
    Rtesint32 SecdCircPFild;
    Rtesint32 SecdCircPFild_1_100Bar;
}CircPFildInfo_t;

typedef struct
{
    Rtesint32 PrimCircPFild1ms;
    Rtesint32 SecdCircPFild1ms;
}CircPFild1msInfo_t;

typedef struct
{
    Rtesint32 PrimCircPOffsCorrd;
    Rtesint32 PrimCircPOffsCorrdStOkFlg;
    Rtesint32 SecdCircPOffsCorrd;
    Rtesint32 SecdCircPOffsCorrdStOkFlg;
}CircPOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PrimCircPChgDurg10ms;
    Rtesint32 PrimCircPChgDurg5ms;
    Rtesint32 PrimCircPRate;
    Rtesint32 PrimCircPRateBarPerSec;
    Rtesint32 SecdCircPChgDurg10ms;
    Rtesint32 SecdCircPChgDurg5ms;
    Rtesint32 SecdCircPRate;
    Rtesint32 SecdCircPRateBarPerSec;
}CircPRateInfo_t;

typedef struct
{
    Rtesint32 PrimCircPRate1msAvg_1_100Bar;
    Rtesint32 SecdCircPRate1msAvg_1_100Bar;
}CircPRate1msInfo_t;

typedef struct
{
    Rtesint32 FrntLeEstimdWhlP;
    Rtesint32 FrntRiEstimdWhlP;
    Rtesint32 ReLeEstimdWhlP;
    Rtesint32 ReRiEstimdWhlP;
}EstimdWhlPInfo_t;

typedef struct
{
    Rtesint32 NeedFastRespForAvh;
    Rtesint32 NeedFastRespForEsc;
}FastBrkPRespRqrdModInfo_t;

typedef struct
{
    Rtesint32 FinalTarPOfPrim;
    Rtesint32 FinalTarPOfSecdy;
    Rtesint32 FinalTarPOfWhlFrntLe;
    Rtesint32 FinalTarPOfWhlFrntRi;
    Rtesint32 FinalTarPOfWhlReLe;
    Rtesint32 FinalTarPOfWhlReRi;
    Rtesint32 FinalTarP;
    Rtesint32 FinalTarPDelta;
    Rtesint32 FinalTarPResetFlg;
    Rtesint32 WhlVolumeChgFlg;
    Rtesint32 PrimCircPBoostReqFlg;
    Rtesint32 SecdyCircPBoostReqFlg;
}FinalTarPInfo_t;

typedef struct
{
    Rtesint32 FinalTarPRateOfPrim5ms;
    Rtesint32 FinalTarPRateOfSecd5ms;
    Rtesint32 FinalTarPRateOfPrim10ms;
    Rtesint32 FinalTarPRateOfSecdy10ms;
}FinalTarPRateInfo_t;

typedef struct
{
    Rtesint32 VlvActFlg;
    Rtesint32 VlvCtrlTarI;
    Rtesint32 VlvOnTi;
}IdbCircVlv1CtrlStInfo_t;

typedef struct
{
    Rtesint32 VlvActFlg;
    Rtesint32 VlvCtrlTarI;
    Rtesint32 VlvOnTi;
}IdbCircVlv2CtrlStInfo_t;

typedef struct
{
    Rtesint32 VlvActFlg;
    Rtesint32 VlvCtrlTarI;
    Rtesint32 VlvOnTi;
}IdbCutVlv1CtrlStInfo_t;

typedef struct
{
    Rtesint32 VlvActFlg;
    Rtesint32 VlvCtrlTarI;
    Rtesint32 VlvOnTi;
}IdbCutVlv2CtrlStInfo_t;

typedef struct
{
    Rtesint32 MotTarPosn;
}IdbMotPosnInfo_t;

typedef struct
{
    Rtesint32 VlvActFlg;
    Rtesint32 VlvCtrlTarI;
    Rtesint32 VlvOnTi;
}IdbSimrVlvCtrlStInfo_t;

typedef struct
{
    Rtesint32 PedlSimrPFild;
    Rtesint32 PedlSimrPFild_1_100Bar;
}PedlSimrPFildInfo_t;

typedef struct
{
    Rtesint32 PedlSimrPOffsCorrd;
    Rtesint32 PedlSimrPOffsCorrdStOkFlg;
}PedlSimrPOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PdfFild;
    Rtesint32 PdtFild;
}PedlTrvlFildInfo_t;

typedef struct
{
    Rtesint32 PdtFild1ms;
}PedlTrvlFild1msInfo_t;

typedef struct
{
    Rtesint32 PdfOffsCorrdStOkFlg;
    Rtesint32 PdfRawOffsCorrd;
    Rtesint32 PdtOffsCorrdStOkFlg;
    Rtesint32 PdtRawOffsCorrd;
}PedlTrvlOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PedlTrvlRate;
    Rtesint32 PedlTrvlRateMilliMtrPerSec;
}PedlTrvlRateInfo_t;

typedef struct
{
    Rtesint32 PdfSigNoiseSuspcFlgH;
    Rtesint32 PdfSigNoiseSuspcFlgL;
    Rtesint32 PdfSigTag;
    Rtesint32 PdtSigNoiseSuspcFlgH;
    Rtesint32 PdtSigNoiseSuspcFlgL;
    Rtesint32 PdtSigTag;
    Rtesint32 PedlSigTag;
    Rtesint32 PedlSimrPSigNoiseSuspcFlgH;
    Rtesint32 PedlSimrPSigNoiseSuspcFlgL;
}PedlTrvlTagInfo_t;

typedef struct
{
    Rtesint32 PistPFild;
    Rtesint32 PistPFild_1_100Bar;
}PistPFildInfo_t;

typedef struct
{
    Rtesint32 PistPFild1ms;
}PistPFild1msInfo_t;

typedef struct
{
    Rtesint32 PistPOffsCorrd;
    Rtesint32 PistPOffsCorrdStOkFlg;
}PistPOffsCorrdInfo_t;

typedef struct
{
    Rtesint32 PistPChgDurg10ms;
    Rtesint32 PistPChgDurg5ms;
    Rtesint32 PistPRate;
    Rtesint32 PistPRateBarPerSec;
}PistPRateInfo_t;

typedef struct
{
    Rtesint32 PistPRate1msAvg_1_100Bar;
}PistPRate1msInfo_t;

typedef struct
{
    Rtesint32 FrntSlipDetThdSlip;
    Rtesint32 FrntSlipDetThdUDiff;
}RgnBrkCoopWithAbsInfo_t;

typedef struct
{
    Rtesint32 AllowedRcvrStrk_mm;
    Rtesint32 FwdRcvrMaxPosi_mm;
    Rtesint32 BackwRcvrMinPosi_mm;
}StkRecvryActnIfInfo_t;

typedef struct
{
    Rtesint32 VehStandStillStFlg;
    Rtesint32 VehStopStFlg;
}VehStopStInfo_t;

typedef struct
{
    Rtesint32 PedlSimrPGendOnFlg;
}VlvActtnReqByBaseBrkCtrlrInfo_t;

typedef struct
{
    Rtesint32 WhlSpdFildFrntLe;
    Rtesint32 WhlSpdFildFrntRi;
    Rtesint32 WhlSpdFildReLe;
    Rtesint32 WhlSpdFildReRi;
}WhlSpdFildInfo_t;


typedef Rtesint32 FunctionLamp_t;
typedef Rtesint32 PCtrlAct_t;
typedef Rtesint32 VdcLinkFild_t;
typedef Rtesint32 MotCtrlMode_t;
typedef Rtesint32 ActvBrkCtrlrActFlg_t;
typedef Rtesint32 Ay_t;
typedef Rtesint32 VehSpd_t;
typedef Rtesint32 BaseBrkCtrlrActFlg_t;
typedef Rtesint32 BlsFild_t;
typedef Rtesint32 BrkPRednForBaseBrkCtrlr_t;
typedef Rtesint32 FlexBrkSwtFild_t;
typedef Rtesint32 InitQuickBrkDctFlg_t;
typedef Rtesint32 MotICtrlFadeOutState_t;
typedef Rtesint32 PCtrlBoostMod_t;
typedef Rtesint32 PCtrlFadeoutSt_t;
typedef Rtesint32 PCtrlSt_t;
typedef Rtesint32 PedlTrvlFinal_t;
typedef Rtesint32 RgnBrkCtlrBlendgFlg_t;
typedef Rtesint32 RgnBrkCtrlrActStFlg_t;
typedef Rtesint32 SeldMapOfBrkPedlToBrkP_t;
typedef Rtesint32 TarDeltaStk_t;
typedef Rtesint32 TarPFromBaseBrkCtrlr_t;
typedef Rtesint32 TarPFromBrkPedl_t;
typedef Rtesint32 VehSpdFild_t;
typedef Rtesint32 StkRecvryStabnEndOK_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RTE_TYPES_H_ */
/** @} */
