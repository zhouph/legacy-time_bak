/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef HAL_TYPES_H_
#define HAL_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef sint8 Halsint8;
typedef uint8 Haluint8;
typedef sint16 Halsint16;
typedef uint16 Haluint16;
typedef sint32 Halsint32;
typedef uint32 Haluint32;
typedef float32 Halfloat32;
typedef float64 Halfloat64;

typedef struct
{
    Haluint8 PrimCutVlvLowVolt;
    Haluint8 PrimCutVlvLogicalRst;
    Haluint8 PrimCutVlvOverTemp;
    Haluint8 PrimCutVlvShortToGnd;
    Haluint8 PrimCutVlvShortToBatt;
    Haluint8 PrimCutVlvOpenLoad;
    Haluint8 SecdCutVlvLowVolt;
    Haluint8 SecdCutVlvLogicalRst;
    Haluint8 SecdCutVlvOverTemp;
    Haluint8 SecdCutVlvShortToGnd;
    Haluint8 SecdCutVlvShortToBatt;
    Haluint8 SecdCutVlvOpenLoad;
    Haluint8 PrimCircVlvLowVolt;
    Haluint8 PrimCircVlvLogicalRst;
    Haluint8 PrimCircVlvOverTemp;
    Haluint8 PrimCircVlvShortToGnd;
    Haluint8 PrimCircVlvShortToBatt;
    Haluint8 PrimCircVlvOpenLoad;
    Haluint8 SecdCircVlvLowVolt;
    Haluint8 SecdCircVlvLogicalRst;
    Haluint8 SecdCircVlvOverTemp;
    Haluint8 SecdCircVlvShortToGnd;
    Haluint8 SecdCircVlvShortToBatt;
    Haluint8 SecdCircVlvOpenLoad;
    Haluint8 SimVlvLowVolt;
    Haluint8 SimVlvLogicalRst;
    Haluint8 SimVlvOverTemp;
    Haluint8 SimVlvShortToGnd;
    Haluint8 SimVlvShortToBatt;
    Haluint8 SimVlvOpenLoad;
}FaultStsVlvdInfo_t;

typedef struct
{
    Haluint16 Sup3p3vMon;
    Haluint16 Ext5vMon;
    Haluint16 CEMon;
    Haluint16 Vbatt01Mon;
    Haluint16 Vbatt02Mon;
    Haluint16 VddMon;
}SwTrigPwrInfo_t;

typedef struct
{
    Haluint16 AsicTempMon;
    Haluint16 DiagOutMon;
    Haluint16 LineTestMon;
    Haluint16 TempMon;
}SwTrigTmpInfo_t;

typedef struct
{
    Haluint16 FlIvMon;
    Haluint16 FrIvMon;
}SwTrigVlvInfo_t;

typedef struct
{
    Haluint16 FspAbsHMon;
    Haluint16 FspAbsMon;
    Haluint16 FspCbsHMon;
    Haluint16 FspCbsMon;
}SwTrigFspInfo_t;

typedef struct
{
    Haluint16 FsrAbsMon;
    Haluint16 FsrCbsMon;
    Haluint16 VbattFsrMon;
}SwTrigFsrInfo_t;

typedef struct
{
    Haluint16 PrimCircPSenNegMon;
    Haluint16 PrimCircPSenPosMon;
    Haluint16 SecdCircPSenNegMon;
    Haluint16 SecdCircPSenPosMon;
    Haluint16 PspSenNegMon;
    Haluint16 PspSenPosMon;
}SwTrigPressInfo_t;

typedef struct
{
    Haluint16 MotPwrMon;
    Haluint16 StarMon;
    Haluint16 UoutMon;
    Haluint16 VoutMon;
    Haluint16 WoutMon;
}SwTrigMotInfo_t;

typedef struct
{
    Haluint16 Uphase0Mon;
    Haluint16 Uphase1Mon;
    Haluint16 Vphase0Mon;
    Haluint16 VPhase1Mon;
}HwTrigMotInfo_t;

typedef struct
{
    Haluint16 PdfSigMon;
    Haluint16 Pdt5vMon;
    Haluint16 PdtSigMon;
}SwTrigPdtInfo_t;

typedef struct
{
    Haluint16 RlIMainMon;
    Haluint16 RlISubMonFs;
    Haluint16 RlMocNegMon;
    Haluint16 RlMocPosMon;
    Haluint16 RlNoMon;
    Haluint16 RrIMainMon;
    Haluint16 RrISubMonFs;
    Haluint16 RrMocNegMon;
    Haluint16 RrMocPosMon;
    Haluint16 RrNoMon;
}SwTrigMocInfo_t;

typedef struct
{
    Haluint16 VlvCircPMon;
    Haluint16 VlvCircSMon;
    Haluint16 VlvCutPMon;
    Haluint16 VlvCutSMon;
    Haluint16 VlvSimMon;
}HwTrigVlvInfo_t;

typedef struct
{
    Haluint16 AdcVBatt01Mon_Esc;
    Haluint16 AdcVBatt02Mon_Esc;
    Haluint16 AdcGio2Mon_Esc;
    Haluint16 AdcExt5VSupplyMon_Esc;
    Haluint16 AdcExt12VSupplyMon_Esc;
    Haluint16 AdcSwVpwrMon_Esc;
    Haluint16 AdcVpwrMon_Esc;
    Haluint16 AdcGio1Mon_Esc;
    Haluint16 AdcIgnMon_Esc;
}SwTrigPwrInfoEsc_t;

typedef struct
{
    Haluint16 AdcFspMon_Esc;
}SwTrigVlvInfoEsc_t;

typedef struct
{
    Haluint16 AdcMtpMon_Esc;
    Haluint16 AdcMtrFwSMon_Esc;
}SwTrigMotInfoEsc_t;

typedef struct
{
    Haluint16 AdcEpbPwrMon_Esc;
    Haluint16 AdcEpbI1LMon_Esc;
    Haluint16 AdcEpbI1RMon_Esc;
    Haluint16 AdcEpbI2LMon_Esc;
    Haluint16 AdcEpbI2RMon_Esc;
    Haluint16 AdcEpbMonLPlus_Esc;
    Haluint16 AdcEpbMonLMinus_Esc;
    Haluint16 AdcEpbMonRPlus_Esc;
    Haluint16 AdcEpbMonRMinus_Esc;
}SwTrigEpbInfoEsc_t;

typedef struct
{
    Haluint16 AdcMcuAdc1_Esc;
    Haluint16 AdcMcuAdc2_Esc;
    Haluint16 AdcMcuAdc3_Esc;
    Haluint16 AdcMcuAdc4_Esc;
    Haluint16 AdcMcuAdc5_Esc;
}SwTrigReservedEsc_t;

typedef struct
{
    Haluint16 VoltVBatt01Mon_Esc;
    Haluint16 VoltVBatt02Mon_Esc;
    Haluint16 VoltGio2Mon_Esc;
    Haluint16 VoltExt5VSupplyMon_Esc;
    Haluint16 VoltExt12VSupplyMon_Esc;
    Haluint16 VoltSwVpwrMon_Esc;
    Haluint16 VoltVpwrMon_Esc;
    Haluint16 VoltGio1Mon_Esc;
    Haluint16 VoltIgnMon_Esc;
}PwrMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltFspMon_Esc;
}VlvMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltMtpMon_Esc;
    Haluint16 VoltMtrFwSMon_Esc;
}MotMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltEpbPwrMon_Esc;
    Haluint16 VoltEpbI1LMon_Esc;
    Haluint16 VoltEpbI1RMon_Esc;
    Haluint16 VoltEpbI2LMon_Esc;
    Haluint16 VoltEpbI2RMon_Esc;
    Haluint16 VoltEpbMonLPlus_Esc;
    Haluint16 VoltEpbMonLMinus_Esc;
    Haluint16 VoltEpbMonRPlus_Esc;
    Haluint16 VoltEpbMonRMinus_Esc;
}EpbMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltMcuAdc1_Esc;
    Haluint16 VoltMcuAdc2_Esc;
    Haluint16 VoltMcuAdc3_Esc;
    Haluint16 VoltMcuAdc4_Esc;
    Haluint16 VoltMcuAdc5_Esc;
}ReservedMonInfoEsc_t;

typedef struct
{
    Haluint16 HpdSr;
    Haluint16 PdMtCfg;
    Haluint16 Pchar;
    Haluint16 Ichar;
    Haluint16 LsdSinkDis;
    Haluint16 LdactDis;
    Haluint16 E5vsOcCfg;
    Haluint16 IsoFlt;
    Haluint16 IsoDet;
    Haluint16 FmAmp;
    Haluint16 FmEn;
    Haluint16 Stopclk2;
    Haluint16 Adin2En;
    Haluint16 Adin1Dis;
    Haluint16 OcfPd;
    Haluint16 FvpwrAct;
    Haluint16 DfWs;
    Haluint16 OcfWs;
    Haluint16 Iclamp;
    Haluint16 Didt;
    Haluint16 Fdcl;
    Haluint16 Llc;
    Haluint16 CrFb;
    Haluint16 CrDis12;
    Haluint16 CrDis34;
    Haluint16 LfPwm14;
    Haluint16 LfPwm58;
    Haluint16 WsaiS;
    Haluint16 WsS;
    Haluint16 Wscfg1;
    Haluint16 Wscfg2;
    Haluint16 Wscfg3;
    Haluint16 Wscfg4;
    Haluint16 WsTrkDis1;
    Haluint16 WsTrkDis2;
    Haluint16 WsTrkDis3;
    Haluint16 WsTrkDis4;
    Haluint16 WsCntRst;
    Haluint16 WsCntEn;
    Haluint16 VsoSel;
    Haluint16 VsoS;
    Haluint16 Vpo1On;
    Haluint16 PdClrFlt;
    Haluint16 WldClrFlt;
    Haluint16 E5vsClrFlt;
    Haluint16 WsClrFlt;
    Haluint16 HdClrFlt;
    Haluint16 IsokClrFlt;
    Haluint16 VsoClrFlt;
    Haluint16 Vpo2ClrFlt;
    Haluint16 Vpo1ClrFlt;
    Haluint16 LsdClrFlt;
}SpiTxAsicInfo_t;

typedef struct
{
    Haluint16 Sed;
    Haluint16 Mr;
}SpiTxAsicSeedKeyInfo_t;

typedef struct
{
    Haluint8 Ws1Oc;
    Haluint8 Ws1Op;
    Haluint8 Ws1Ot;
    Haluint8 Ws2Oc;
    Haluint8 Ws2Op;
    Haluint8 Ws2Ot;
    Haluint8 Ws3Oc;
    Haluint8 Ws3Op;
    Haluint8 Ws3Ot;
    Haluint8 Ws4Oc;
    Haluint8 Ws4Op;
    Haluint8 Ws4Ot;
    Haluint8 VpwrOv;
    Haluint8 VpwrUv;
    Haluint8 OtwOv;
    Haluint8 Ld;
    Haluint8 PdOc;
    Haluint8 HdOc;
    Haluint8 WldOc;
    Haluint8 WldOp;
    Haluint8 WldOt;
    Haluint8 VdsWld;
    Haluint8 Lsd1Crer;
    Haluint8 Lsd2Crer;
    Haluint8 Lsd3Crer;
    Haluint8 Lsd4Crer;
    Haluint8 Lsd1Oc;
    Haluint8 Lsd1Op;
    Haluint8 Lsd1Ot;
    Haluint8 VdsLsd1;
    Haluint8 Lsd2Oc;
    Haluint8 Lsd2Op;
    Haluint8 Lsd2Ot;
    Haluint8 VdsLsd2;
    Haluint8 Lsd3Oc;
    Haluint8 Lsd3Op;
    Haluint8 Lsd3Ot;
    Haluint8 VdsLsd3;
    Haluint8 Lsd4Oc;
    Haluint8 Lsd4Op;
    Haluint8 Lsd4Ot;
    Haluint8 VdsLsd4;
    Haluint8 Lsd5Oc;
    Haluint8 Lsd5Op;
    Haluint8 Lsd5Ot;
    Haluint8 VdsLsd5;
    Haluint8 Lsd6Oc;
    Haluint8 Lsd6Op;
    Haluint8 Lsd6Ot;
    Haluint8 VdsLsd6;
    Haluint8 Lsd7Oc;
    Haluint8 Lsd7Op;
    Haluint8 Lsd7Ot;
    Haluint8 VdsLsd7;
    Haluint8 Lsd8Oc;
    Haluint8 Lsd8Op;
    Haluint8 Lsd8Ot;
    Haluint8 VdsLsd8;
    Haluint8 IsOkOc;
    Haluint8 IsOkOt;
    Haluint8 E5vsOuv;
    Haluint8 E5vsOc;
    Haluint8 HdLkg;
    Haluint8 ArErrCnt;
    Haluint8 Ws1Lkg;
    Haluint8 Ws2Lkg;
    Haluint8 Ws3Lkg;
    Haluint8 Ws4Lkg;
    Haluint8 Vpo1Oc;
    Haluint8 Vpo1Op;
    Haluint8 Vpo1Ot;
    Haluint8 VdsVpo1;
    Haluint8 Vpo2Oc;
    Haluint8 Vpo2Op;
    Haluint8 Vpo2Ot;
    Haluint8 VdsVpo2;
    Haluint8 VsoOc;
    Haluint8 VsoOp;
    Haluint8 VsoOt;
    Haluint8 VdsVso;
}FaultStsAsicInfo_t;

typedef struct
{
    Haluint8 OverTemp;
    Haluint8 ShortGndPhA;
    Haluint8 ShortGndPhB;
    Haluint8 ShortGndPhC;
    Haluint8 ShortSplyPhA;
    Haluint8 ShortSplyPhB;
    Haluint8 ShortSplyPhC;
    Haluint8 ShortLoadPhA;
    Haluint8 ShortLoadPhB;
    Haluint8 ShortLoadPhC;
    Haluint8 UnderVoltVreg;
    Haluint8 UnderVoltBootstrapCapPhA;
    Haluint8 UnderVoltBootstrapCapPhB;
    Haluint8 UnderVoltBootstrapCapPhC;
}FaultStsGdInfo_t;

typedef struct
{
    Haluint16 FlRisngIdx;
    Haluint16 FrRisngIdx;
    Haluint16 RlRisngIdx;
    Haluint16 RrRisngIdx;
}RisngIdxInfo_t;

typedef struct
{
    Haluint16 FlFallIdx;
    Haluint16 FrFallIdx;
    Haluint16 RlFallIdx;
    Haluint16 RrFallIdx;
}FallIdxInfo_t;

typedef struct
{
    Haluint32 FlRisngTiStamp[32];
    Haluint32 FrRisngTiStamp[32];
    Haluint32 RlRisngTiStamp[32];
    Haluint32 RrRisngTiStamp[32];
}RisngTiStampInfo_t;

typedef struct
{
    Haluint32 FlFallTiStamp[32];
    Haluint32 FrFallTiStamp[32];
    Haluint32 RlFallTiStamp[32];
    Haluint32 RrFallTiStamp[32];
}FallTiStampInfo_t;

typedef struct
{
    Haluint8 LpmSuplyLoss;
    Haluint8 TarMagLoss;
    Haluint8 InternalErr;
    Haluint8 TempOutOfRange;
    Haluint8 EepErr1;
    Haluint8 EepErr2;
}FaultStsMpsInfo_t;

typedef struct
{
    Haluint16 FlIvMon;
    Haluint16 FrIvMon;
    Haluint16 RlIvMon;
    Haluint16 RrIvMon;
}WhlVlvIvMonInfo_t;

typedef struct
{
    Haluint16 FsrAbsMon;
    Haluint16 FsrCbsMon;
    Haluint16 VBattFsrMon;
}FsrMonInfo_t;

typedef struct
{
    Haluint16 PrimCircPNegMon;
    Haluint16 SecdCircPPosMon;
    Haluint16 SecdCircPNegMon;
    Haluint16 PedlSimPNegMon;
    Haluint16 PedlSimPPosMon;
    Haluint16 PrimCircPPosMon;
}PressMonInfo_t;

typedef struct
{
    Haluint16 McpPresData1;
    Haluint16 McpPresData2;
    Haluint8 McpSentCrc;
    Haluint16 McpSentData;
    Haluint8 McpSentMsgId;
    Haluint8 McpSentSerialCrc;
    Haluint8 McpSentConfig;
    Haluint16 Wlp1PresData1;
    Haluint16 Wlp1PresData2;
    Haluint8 Wlp1SentCrc;
    Haluint16 Wlp1SentData;
    Haluint8 Wlp1SentMsgId;
    Haluint8 Wlp1SentSerialCrc;
    Haluint8 Wlp1SentConfig;
    Haluint16 Wlp2PresData1;
    Haluint16 Wlp2PresData2;
    Haluint8 Wlp2SentCrc;
    Haluint16 Wlp2SentData;
    Haluint8 Wlp2SentMsgId;
    Haluint8 Wlp2SentSerialCrc;
    Haluint8 Wlp2SentConfig;
}SentHPressureInfo_t;

typedef struct
{
    Haluint16 McpPresData1;
    Haluint16 McpPresData2;
    Haluint8 McpSentCrc;
    Haluint16 McpSentData;
    Haluint8 McpSentMsgId;
    Haluint8 McpSentSerialCrc;
    Haluint8 McpSentConfig;
    Haluint16 Wlp1PresData1;
    Haluint16 Wlp1PresData2;
    Haluint8 Wlp1SentCrc;
    Haluint16 Wlp1SentData;
    Haluint8 Wlp1SentMsgId;
    Haluint8 Wlp1SentSerialCrc;
    Haluint8 Wlp1SentConfig;
    Haluint16 Wlp2PresData1;
    Haluint16 Wlp2PresData2;
    Haluint8 Wlp2SentCrc;
    Haluint16 Wlp2SentData;
    Haluint8 Wlp2SentMsgId;
    Haluint8 Wlp2SentSerialCrc;
    Haluint8 Wlp2SentConfig;
}HalPressureInfo_t;

typedef struct
{
    Haluint16 MotPwrVoltMon;
    Haluint16 MotStarMon;
    Haluint16 MotVoltPhUMon;
}MotMonInfo_t;

typedef struct
{
    Haluint16 MotCurrPhUMon0;
    Haluint16 MotCurrPhUMon1;
    Haluint16 MotCurrPhVMon0;
    Haluint16 MotCurrPhVMon1;
}MotCurrMonInfo_t;

typedef struct
{
    Haluint16 MotVoltPhVMon;
    Haluint16 MotVoltPhWMon;
}MotVoltsMonInfo_t;

typedef struct
{
    Haluint16 PdfSigMon;
    Haluint16 PdtSigMon;
}PedlSigMonInfo_t;

typedef struct
{
    Haluint16 Pdt5vMon;
    Haluint16 Pdf5vMon;
}PedlPwrMonInfo_t;

typedef struct
{
    Haluint16 RlCurrMainMon;
    Haluint16 RlCurrSubMon;
    Haluint16 RlMocNegMon;
    Haluint16 RlMocPosMon;
    Haluint16 RrCurrMainMon;
    Haluint16 RrCurrSubMon;
    Haluint16 RrMocNegMon;
    Haluint16 RrMocPosMon;
}MocMonInfo_t;

typedef struct
{
    Haluint16 PrimCircVlvMon;
    Haluint16 SecdCirCVlvMon;
    Haluint16 PrimCutVlvMon;
    Haluint16 SecdCutVlvMon;
    Haluint16 SimVlvMon;
}NormVlvMonInfo_t;

typedef struct
{
    Haluint8 AvhSwtMon;
    Haluint8 BflSwtMon;
    Haluint8 BlsSwtMon;
    Haluint8 BsSwtMon;
    Haluint8 DoorSwtMon;
    Haluint8 EscSwtMon;
    Haluint8 FlexBrkASwtMon;
    Haluint8 FlexBrkBSwtMon;
    Haluint8 HzrdSwtMon;
    Haluint8 HdcSwtMon;
    Haluint8 PbSwtMon;
}SwtMonInfo_t;

typedef struct
{
    Haluint8 GdFaultFlg1;
    Haluint8 GdFaultFlg2;
}GdMonInfo_t;

typedef struct
{
    Haluint8 BlsSwtMon_Esc;
    Haluint8 AvhSwtMon_Esc;
    Haluint8 EscSwtMon_Esc;
    Haluint8 HdcSwtMon_Esc;
    Haluint8 PbSwtMon_Esc;
    Haluint8 GearRSwtMon_Esc;
    Haluint8 BlfSwtMon_Esc;
    Haluint8 ClutchSwtMon_Esc;
    Haluint8 ItpmsSwtMon_Esc;
}SwtMonInfoEsc_t;

typedef struct
{
    Haluint8 RlyDbcMon;
    Haluint8 RlyEssMon;
    Haluint8 RlyFault;
}RlyMonInfo_t;

typedef struct
{
    Haluint8 Sw1LineCMon;
    Haluint8 Sw2LineBMon;
    Haluint8 Sw3LineEMon;
    Haluint8 Sw4LineFMon;
}SwLineMonInfo_t;

typedef struct
{
    Haluint8 FlOvMon;
    Haluint8 FROvMon;
    Haluint8 RLOvMon;
    Haluint8 RROvMon;
}WhlVlvOvMonInfo_t;

typedef struct
{
    Haluint16 FlRisngIdx;
    Haluint16 FlFallIdx;
    Haluint32 FlRisngTiStamp[32];
    Haluint32 FlFallTiStamp[32];
    Haluint16 FrRisngIdx;
    Haluint16 FrFallIdx;
    Haluint32 FrRisngTiStamp[32];
    Haluint32 FrFallTiStamp[32];
    Haluint16 RlRisngIdx;
    Haluint16 RlFallIdx;
    Haluint32 RlRisngTiStamp[32];
    Haluint32 RlFallTiStamp[32];
    Haluint16 RrRisngIdx;
    Haluint16 RrFallIdx;
    Haluint32 RrRisngTiStamp[32];
    Haluint32 RrFallTiStamp[32];
}WssMonInfo_t;

typedef struct
{
    Haluint16 MotPosiAngle1;
    Haluint16 MotPosiAngle2;
}MotAngleMonInfo_t;

typedef struct
{
    Haluint16 FlOvCurrMon;
    Haluint16 FlIvDutyMon;
    Haluint16 FrOvCurrMon;
    Haluint16 FrIvDutyMon;
    Haluint16 RlOvCurrMon;
    Haluint16 RlIvDutyMon;
    Haluint16 RrOvCurrMon;
    Haluint16 RrIvDutyMon;
}WhlVlvFbMonInfo_t;

typedef struct
{
    Haluint32 PdOn;
    Haluint32 Lsd5dutycycle;
    Haluint32 Lsd1dutycycle;
    Haluint32 Lsd6dutycycle;
    Haluint32 Lsd2dutycycle;
    Haluint32 Lsd7dutycycle;
    Haluint32 Lsd3dutycycle;
    Haluint32 Lsd8dutycycle;
    Haluint32 Lsd4dutycycle;
    Haluint32 HdOn;
}SpiTxAsicActInfo_t;

typedef struct
{
    Haluint32 PdMtCfg;
    Haluint32 HpdSr;
    Haluint32 RstWd;
    Haluint32 RstAlu;
    Haluint32 RstExt;
    Haluint32 RstClk;
    Haluint32 VintUv;
    Haluint32 Vcc5Uv;
    Haluint32 DosvUv;
    Haluint32 Version;
    Haluint32 E5vsOcCfg;
    Haluint32 LdactDis;
    Haluint32 LsdSinkDis;
    Haluint32 Ichar;
    Haluint32 Pchar;
    Haluint32 ManufacturingData;
    Haluint32 Ws4Ot;
    Haluint32 Ws4Op;
    Haluint32 Ws4Oc;
    Haluint32 Ws3Ot;
    Haluint32 Ws3Op;
    Haluint32 Ws3Oc;
    Haluint32 Ws2Ot;
    Haluint32 Ws2Op;
    Haluint32 Ws2Oc;
    Haluint32 Ws1Ot;
    Haluint32 Ws1Op;
    Haluint32 Ws1Oc;
    Haluint32 OtwOv;
    Haluint32 Fgnd;
    Haluint32 VpwrUv;
    Haluint32 Ld;
    Haluint32 VpwrOv;
    Haluint32 AsicClkCnt;
    Haluint32 Temp;
    Haluint32 VsoSel;
    Haluint32 VsoS;
    Haluint32 FvpwrAct;
    Haluint32 VintD;
    Haluint32 HdOc;
    Haluint32 PdOc;
    Haluint32 Vpre10;
    Haluint32 VdsWld;
    Haluint32 WldOt;
    Haluint32 WldOp;
    Haluint32 WldOc;
    Haluint32 Vpre12;
    Haluint32 CrDis34;
    Haluint32 CrDis12;
    Haluint32 CrFb;
    Haluint32 VcpVpwr;
    Haluint32 Lsd4Crer;
    Haluint32 Lsd3Crer;
    Haluint32 Lsd2Crer;
    Haluint32 Lsd1Crer;
    Haluint32 VintA;
    Haluint32 Isopos4;
    Haluint32 Isopos3;
    Haluint32 Isopos2;
    Haluint32 Isopos1;
    Haluint32 Lsd1Duty;
    Haluint32 VdsLsd1;
    Haluint32 Lsd1Ot;
    Haluint32 Lsd1Op;
    Haluint32 Lsd1Oc;
    Haluint32 Lsd2Duty;
    Haluint32 VdsLsd2;
    Haluint32 Lsd2Ot;
    Haluint32 Lsd2Op;
    Haluint32 Lsd2Oc;
    Haluint32 Lsd3Duty;
    Haluint32 VdsLsd3;
    Haluint32 Lsd3Ot;
    Haluint32 Lsd3Op;
    Haluint32 Lsd3Oc;
    Haluint32 Lsd4Duty;
    Haluint32 VdsLsd4;
    Haluint32 Lsd4Ot;
    Haluint32 Lsd4Op;
    Haluint32 Lsd4Oc;
    Haluint32 IsokOt;
    Haluint32 IsokOc;
    Haluint32 Lsd5Duty;
    Haluint32 VdsLsd5;
    Haluint32 Lsd5Ot;
    Haluint32 Lsd5Op;
    Haluint32 Lsd5Oc;
    Haluint32 IsoNeg2;
    Haluint32 IsoNeg1;
    Haluint32 Lsd6Duty;
    Haluint32 VdsLsd6;
    Haluint32 Lsd6Ot;
    Haluint32 Lsd6Op;
    Haluint32 Lsd6Oc;
    Haluint32 IsoNeg4;
    Haluint32 IsoNeg3;
    Haluint32 Lsd7Duty;
    Haluint32 VdsLsd7;
    Haluint32 Lsd7Ot;
    Haluint32 Lsd7Op;
    Haluint32 Lsd7Oc;
    Haluint32 Lsd8Duty;
    Haluint32 VdsLsd8;
    Haluint32 Lsd8Ot;
    Haluint32 Lsd8Op;
    Haluint32 Lsd8Oc;
    Haluint32 Ar;
    Haluint32 ErrCnt;
    Haluint32 HdLkg;
    Haluint32 E5vsOc;
    Haluint32 E5vsOuv;
    Haluint32 WsS;
    Haluint32 WsaiS;
    Haluint32 WsCntOv;
    Haluint32 WsCnt;
    Haluint32 Ws1Notlegal;
    Haluint32 Ws1Fail;
    Haluint32 Ws1Stop;
    Haluint32 Ws1Data;
    Haluint32 Ws1Lkg;
    Haluint32 Ws1Notlegalbits;
    Haluint32 Ws1Codeerror;
    Haluint32 Ws1manchesterdecodingresult;
    Haluint32 Ws2Notlegal;
    Haluint32 Ws2Fail;
    Haluint32 Ws2Stop;
    Haluint32 Ws2Data;
    Haluint32 Ws2Lkg;
    Haluint32 Ws2Notlegalbits;
    Haluint32 Ws2Codeerror;
    Haluint32 Ws2manchesterdecodingresult;
    Haluint32 Ws3Notlegal;
    Haluint32 Ws3Fail;
    Haluint32 Ws3Stop;
    Haluint32 Ws3Data;
    Haluint32 Ws3Lkg;
    Haluint32 Ws3Notlegalbits;
    Haluint32 Ws3Codeerror;
    Haluint32 Ws3manchesterdecodingresult;
    Haluint32 Ws4Notlegal;
    Haluint32 Ws4Fail;
    Haluint32 Ws4Stop;
    Haluint32 Ws4Data;
    Haluint32 Ws4Lkg;
    Haluint32 Ws4Notlegalbits;
    Haluint32 Ws4Codeerror;
    Haluint32 Ws4manchesterdecodingresult;
    Haluint32 AdRst1;
    Haluint32 VdsVpo1;
    Haluint32 Vpo1Ot;
    Haluint32 Vpo1Op;
    Haluint32 Vpo1Oc;
    Haluint32 AdRst2;
    Haluint32 VdsVpo2;
    Haluint32 Vpo2Ot;
    Haluint32 Vpo2Op;
    Haluint32 Vpo2Oc;
    Haluint32 AdRst3;
    Haluint32 VdsVso;
    Haluint32 VsoOt;
    Haluint32 VsoOp;
    Haluint32 VsoOc;
    Haluint32 Fmsg0;
    Haluint32 Fmsg1;
    Haluint32 Fmsg2;
    Haluint32 Fmsg3;
    Haluint32 Fmsg4;
    Haluint32 Fmsg5;
    Haluint32 Fmsg6;
    Haluint32 Fmsg7;
    Haluint32 Fmsg8;
    Haluint32 Fmsg9;
    Haluint32 Fmsg10;
    Haluint32 Fmsg11;
    Haluint32 Fmsg12;
    Haluint32 Fmsg13;
    Haluint32 Fmsg14;
    Haluint32 Fmsg15;
    Haluint32 Fmsg16;
    Haluint32 Fmsg17;
    Haluint32 Fmsg18;
    Haluint32 Fmsg19;
    Haluint32 Fmsg20;
    Haluint32 Fmsg21;
    Haluint32 Fmsg22;
    Haluint32 Fmsg23;
    Haluint32 Fmsg24;
    Haluint32 Fmsg25;
    Haluint32 Fmsg26;
}RxAsicInfo_t;

typedef struct
{
    Haluint32 Mps1AngleP;
    Haluint32 Mps1AngleEf;
    Haluint32 Mps1ErrorP;
    Haluint32 Mps1Batd;
    Haluint32 Mps1Magm;
    Haluint32 Mps1Ierr;
    Haluint32 Mps1Trno;
    Haluint32 Mps1Tmp;
    Haluint32 Mps1Eep1;
    Haluint32 Mps1Eep2;
    Haluint32 Mps1ErrorEf;
    Haluint32 Mps1ControlP;
    Haluint32 Mps1Erst;
    Haluint32 Mps1Pwr;
    Haluint32 Mps1Rpm;
    Haluint32 Mps1ControlEf;
    Haluint32 Mps2AngleP;
    Haluint32 Mps2AngleEf;
    Haluint32 Mps2ErrorP;
    Haluint32 Mps2Batd;
    Haluint32 Mps2Magm;
    Haluint32 Mps2Ierr;
    Haluint32 Mps2Trno;
    Haluint32 Mps2Tmp;
    Haluint32 Mps2Eep1;
    Haluint32 Mps2Eep2;
    Haluint32 Mps2ErrorEf;
    Haluint32 Mps2ControlP;
    Haluint32 Mps2Erst;
    Haluint32 Mps2Pwr;
    Haluint32 Mps2Rpm;
    Haluint32 Mps2ControlEf;
}RxMpsInfo_t;

typedef struct
{
    Haluint32 Mps1Angle;
    Haluint32 Mps2Angle;
}RxMpsAngleInfo_t;

typedef struct
{
    Haluint32 C0ol;
    Haluint32 C0sb;
    Haluint32 C0sg;
    Haluint32 C1ol;
    Haluint32 C1sb;
    Haluint32 C1sg;
    Haluint32 C2ol;
    Haluint32 C2sb;
    Haluint32 C2sg;
    Haluint32 Fr;
    Haluint32 Ot;
    Haluint32 Lr;
    Haluint32 Uv;
    Haluint32 Ff;
}RxVlvdInfo_t;

typedef struct
{
    Haluint32 RevMinor;
    Haluint32 RevMajor;
    Haluint32 Id;
    Haluint32 Ign;
    Haluint32 CanwuL;
    Haluint32 NmaskVdd1UvOv;
    Haluint32 Vdd35Sel;
    Haluint32 PostRunRst;
    Haluint32 MaskVbatpOv;
    Haluint32 EnVdd5Ot;
    Haluint32 EnVdd35Ot;
    Haluint32 BgErr1;
    Haluint32 BgErr2;
    Haluint32 AvddVmonErr;
    Haluint32 Vcp12Uv;
    Haluint32 Vcp12Ov;
    Haluint32 Vcp17Ov;
    Haluint32 VpatpUv;
    Haluint32 VbatpOv;
    Haluint32 Vdd1Uv;
    Haluint32 Vdd1Ov;
    Haluint32 Vdd35Uv;
    Haluint32 Vdd35Ov;
    Haluint32 Vdd5Uv;
    Haluint32 Vdd5Ov;
    Haluint32 Vdd6Uv;
    Haluint32 Vdd6Ov;
    Haluint32 Vdd35Ot;
    Haluint32 Vdd5Ot;
    Haluint32 Vsout1Ot;
    Haluint32 Vsout1Ilim;
    Haluint32 Vsout1Ov;
    Haluint32 Vsout1Uv;
    Haluint32 Vdd35Ilim;
    Haluint32 Vdd5Ilim;
    Haluint32 WdFailCnt;
    Haluint32 EeCrcErr;
    Haluint32 CfgCrcErr;
    Haluint32 AbistRun;
    Haluint32 LbistRun;
    Haluint32 AbistUvovErr;
    Haluint32 LbistErr;
    Haluint32 NresErr;
    Haluint32 TrimErrVmon;
    Haluint32 EndrvErr;
    Haluint32 WdErr;
    Haluint32 McuErr;
    Haluint32 Loclk;
    Haluint32 SpiErr;
    Haluint32 Fsm;
    Haluint32 CfgLock;
    Haluint32 SafeLockThr;
    Haluint32 SafeTo;
    Haluint32 AbistEn;
    Haluint32 LbistEn;
    Haluint32 EeCrcChk;
    Haluint32 AutoBistDis;
    Haluint32 BistDegCnt;
    Haluint32 DiagExit;
    Haluint32 DiagExitMask;
    Haluint32 NoError;
    Haluint32 EnableDrv;
    Haluint32 CfgCrcEn;
    Haluint32 DisNresMon;
    Haluint32 WdRstEn;
    Haluint32 IgnPwrl;
    Haluint32 WdCfg;
    Haluint32 ErrorCfg;
    Haluint32 NoSafeTo;
    Haluint32 DevErrCnt;
    Haluint32 WdFail;
    Haluint32 ErrorPinFail;
    Haluint32 Pwmh;
    Haluint32 Pwml;
    Haluint32 PwdThr;
    Haluint32 CfgCrc;
    Haluint32 DiagMuxEn;
    Haluint32 DiagSpiSdo;
    Haluint32 DiagMuxOut;
    Haluint32 DiagIntCon;
    Haluint32 DiagMuxCfg;
    Haluint32 DiagMuxSel;
    Haluint32 WdtTokenSeed;
    Haluint32 WdtFdbk;
    Haluint32 WdtRt;
    Haluint32 WdtRw;
    Haluint32 WdtToken;
    Haluint32 WdfailTh;
    Haluint32 TokenEarly;
    Haluint32 TimeOut;
    Haluint32 SeqErr;
    Haluint32 WdCfgChg;
    Haluint32 WdWrongCfg;
    Haluint32 TokenErr;
    Haluint32 WdtAnswCnt;
    Haluint32 Vsout1En;
    Haluint32 Vdd5En;
    Haluint32 SwLockStat;
    Haluint32 SwUnlockStat;
    Haluint32 RdDevIdStat;
    Haluint32 RdDevRevStat;
    Haluint32 WrDevCfg1Stat;
    Haluint32 RdDevCfg1Stat;
    Haluint32 WrDevCfg2Stat;
    Haluint32 RdDevCfg2Stat;
    Haluint32 WrCanStbyStat;
    Haluint32 RdSafetyStat1Stat;
    Haluint32 RdSafetyStat2Stat;
    Haluint32 RdSafetyStat3Stat;
    Haluint32 RdSafetyStat4Stat;
    Haluint32 RdSafetyStat5Stat;
    Haluint32 RdSafetyErrCfgStat;
    Haluint32 WrSafetyErrCfgStat;
    Haluint32 WrSafetyErrStatStat;
    Haluint32 RdSafetyErrStatStat;
    Haluint32 RdSafetyPwdThrCfgStat;
    Haluint32 WrSafetyPwdThrCfgStat;
    Haluint32 RdSafetyCheckCtrlStat;
    Haluint32 WrSafetyCheckCtrlStat;
    Haluint32 RdSafetyBistCtrlStat;
    Haluint32 WrSafetyBistCtrlStat;
    Haluint32 RdWdtWin1CfgStat;
    Haluint32 WrWdtWin1CfgStat;
    Haluint32 RdWdtWin2CfgStat;
    Haluint32 WrWdtWin2CfgStat;
    Haluint32 RdWdtTokenValueStat;
    Haluint32 RdWdtStatusStat;
    Haluint32 WrWdtAnswerStat;
    Haluint32 RdDevStatStat;
    Haluint32 RdVmonStat1Stat;
    Haluint32 RdVmonStat2Stat;
    Haluint32 RdSensCtrlStat;
    Haluint32 WrSensCtrlStat;
    Haluint32 RdSafetyFuncCfgStat;
    Haluint32 WrSafetyFuncCfgStat;
    Haluint32 RdSafetyCfgCrcStat;
    Haluint32 WrSafetyCfgCrcStat;
    Haluint32 RdDiagCfgCtrlStat;
    Haluint32 WrDiagCfgCtrlStat;
    Haluint32 RdDiagMuxSelStat;
    Haluint32 WrDiagMuxSelStat;
    Haluint32 RdSafetyErrPwmHStat;
    Haluint32 WrSafetyErrPwmHStat;
    Haluint32 RdSafetyErrPwmLStat;
    Haluint32 WrSafetyErrPwmLStat;
    Haluint32 RdWdtTokenFdbckStat;
    Haluint32 WrWdtTokenFdbckStat;
}RxRegInfo_t;

typedef struct
{
    Haluint32 CspMon;
    Haluint32 FlOvCurrMon;
    Haluint32 FlIvDutyMon;
    Haluint32 FrOvCurrMon;
    Haluint32 FrIvDutyMon;
    Haluint32 RlOvCurrMon;
    Haluint32 RlIvDutyMon;
    Haluint32 RrOvCurrMon;
    Haluint32 RrIvDutyMon;
    Haluint32 Pdf5vMon;
}RxAsicPhyInfo_t;

typedef struct
{
    Haluint8 AsicInitDataFlg;
    Haluint8 ACH_Tx1_FS_VDS_TH;
    Haluint8 ACH_Tx1_FS_CMD;
    Haluint8 ACH_Tx1_FS_EN;
    Haluint8 ACH_Tx1_PMP_EN;
    Haluint8 ACH_Tx1_OSC_SprSpect_DIS;
    Haluint8 ACH_Tx1_VDD_4_OVC_SD_RESTART_TIME;
    Haluint8 ACH_Tx1_VDD4_OVC_FLT_TIME;
    Haluint8 ACH_Tx1_VDD_3_OVC_SD_RESTART_TIME;
    Haluint8 ACH_Tx1_VDD3_OVC_FLT_TIME;
    Haluint8 ACH_Tx2_WD_SW_DIS;
    Haluint8 ACH_Tx2_VDD_2d5_AUTO_SWITCH_OFF;
    Haluint8 ACH_Tx2_VDD1_DIODELOSS_FILT;
    Haluint8 ACH_Tx2_WD_DIS;
    Haluint8 ACH_Tx2_VDD5_DIS;
    Haluint8 ACH_Tx2_VDD2_DIS;
    Haluint8 ACH_Tx19_E_PVDS_GPO;
    Haluint8 ACH_Tx19_E_VDD2d5dWSx_OT_GPO;
    Haluint8 ACH_Tx19_E_FBO_GPO;
    Haluint8 ACH_Tx19_E_FSPI_GPO;
    Haluint8 ACH_Tx19_E_EN_GPO;
    Haluint8 ACH_Tx19_E_FS_VDS_GPO;
}AcmAsicInitWriteData_t;

typedef struct
{
    Haluint8 ValveInitDataFlg;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_0;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_1;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_2;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_3;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_4;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_5;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_6;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_7;
    Haluint8 ACH_Tx65_CH0_STATUS_L;
    Haluint8 ACH_Tx65_CH0_STATUS_H;
    Haluint8 ACH_Tx65_CH1_STATUS_L;
    Haluint8 ACH_Tx65_CH1_STATUS_H;
    Haluint8 ACH_Tx65_CH2_STATUS_L;
    Haluint8 ACH_Tx65_CH2_STATUS_H;
    Haluint8 ACH_Tx65_CH3_STATUS_L;
    Haluint8 ACH_Tx65_CH3_STATUS_H;
    Haluint8 ACH_Tx65_CH4_STATUS_L;
    Haluint8 ACH_Tx65_CH4_STATUS_H;
    Haluint8 ACH_Tx65_CH5_STATUS_L;
    Haluint8 ACH_Tx65_CH5_STATUS_H;
    Haluint8 ACH_Tx65_CH6_STATUS_L;
    Haluint8 ACH_Tx65_CH6_STATUS_H;
    Haluint8 ACH_Tx65_CH7_STATUS_L;
    Haluint8 ACH_Tx65_CH7_STATUS_H;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_0;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_1;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_2;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_3;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_4;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_5;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_6;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_7;
    Haluint8 ACH_Tx67_CH1_EN;
    Haluint8 ACH_Tx67_CH2_EN;
    Haluint8 ACH_Tx67_CH3_EN;
    Haluint8 ACH_Tx67_CH4_EN;
    Haluint8 ACH_Tx67_CH1_DIAG_EN;
    Haluint8 ACH_Tx67_CH2_DIAG_EN;
    Haluint8 ACH_Tx67_CH3_DIAG_EN;
    Haluint8 ACH_Tx67_CH4_DIAG_EN;
    Haluint8 ACH_Tx67_CH1_TD_BLANK;
    Haluint8 ACH_Tx67_CH2_TD_BLANK;
    Haluint8 ACH_Tx67_CH3_TD_BLANK;
    Haluint8 ACH_Tx67_CH4_TD_BLANK;
    Haluint8 ACH_Tx67_CH1_CMD;
    Haluint8 ACH_Tx67_CH2_CMD;
    Haluint8 ACH_Tx67_CH3_CMD;
    Haluint8 ACH_Tx67_CH4_CMD;
    Haluint8 ACH_TxNO0_2_SR_SEL;
    Haluint8 ACH_TxNO0_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO0_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO0_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO0_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO0_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO0_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO0_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO0_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO0_2_E_SOL_GPO;
    Haluint8 ACH_TxNO0_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO0_6_PWM_CODE;
    Haluint8 ACH_TxNO0_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO0_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO0_8_KI;
    Haluint8 ACH_TxNO0_8_KP;
    Haluint8 ACH_TxNO0_9_INTTIME;
    Haluint8 ACH_TxNO0_9_FILT_TIME;
    Haluint8 ACH_TxNO0_9_CHOP_TIME;
    Haluint8 ACH_TxNO0_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxNO1_2_SR_SEL;
    Haluint8 ACH_TxNO1_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO1_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO1_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO1_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO1_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO1_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO1_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO1_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO1_2_E_SOL_GPO;
    Haluint8 ACH_TxNO1_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO1_6_PWM_CODE;
    Haluint8 ACH_TxNO1_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO1_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO1_8_KI;
    Haluint8 ACH_TxNO1_8_KP;
    Haluint8 ACH_TxNO1_9_INTTIME;
    Haluint8 ACH_TxNO1_9_FILT_TIME;
    Haluint8 ACH_TxNO1_9_CHOP_TIME;
    Haluint8 ACH_TxNO1_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxNO2_2_SR_SEL;
    Haluint8 ACH_TxNO2_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO2_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO2_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO2_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO2_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO2_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO2_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO2_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO2_2_E_SOL_GPO;
    Haluint8 ACH_TxNO2_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO2_6_PWM_CODE;
    Haluint8 ACH_TxNO2_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO2_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO2_8_KI;
    Haluint8 ACH_TxNO2_8_KP;
    Haluint8 ACH_TxNO2_9_INTTIME;
    Haluint8 ACH_TxNO2_9_FILT_TIME;
    Haluint8 ACH_TxNO2_9_CHOP_TIME;
    Haluint8 ACH_TxNO2_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxNO3_2_SR_SEL;
    Haluint8 ACH_TxNO3_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO3_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO3_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO3_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO3_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO3_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO3_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO3_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO3_2_E_SOL_GPO;
    Haluint8 ACH_TxNO3_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO3_6_PWM_CODE;
    Haluint8 ACH_TxNO3_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO3_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO3_8_KI;
    Haluint8 ACH_TxNO3_8_KP;
    Haluint8 ACH_TxNO3_9_INTTIME;
    Haluint8 ACH_TxNO3_9_FILT_TIME;
    Haluint8 ACH_TxNO3_9_CHOP_TIME;
    Haluint8 ACH_TxNO3_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxTC0_2_SR_SEL;
    Haluint8 ACH_TxTC0_2_CALIBRATION_DIS;
    Haluint8 ACH_TxTC0_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxTC0_2_TD_BLANK_SEL;
    Haluint8 ACH_TxTC0_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxTC0_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxTC0_2_DIAG_BIST_EN;
    Haluint8 ACH_TxTC0_2_ADC_BIST_EN;
    Haluint8 ACH_TxTC0_2_OFF_DIAG_EN;
    Haluint8 ACH_TxTC0_2_E_SOL_GPO;
    Haluint8 ACH_TxTC0_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxTC0_6_PWM_CODE;
    Haluint8 ACH_TxTC0_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxTC0_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxTC0_8_KI;
    Haluint8 ACH_TxTC0_8_KP;
    Haluint8 ACH_TxTC0_9_INTTIME;
    Haluint8 ACH_TxTC0_9_FILT_TIME;
    Haluint8 ACH_TxTC0_9_CHOP_TIME;
    Haluint8 ACH_TxTC0_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxTC1_2_SR_SEL;
    Haluint8 ACH_TxTC1_2_CALIBRATION_DIS;
    Haluint8 ACH_TxTC1_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxTC1_2_TD_BLANK_SEL;
    Haluint8 ACH_TxTC1_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxTC1_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxTC1_2_DIAG_BIST_EN;
    Haluint8 ACH_TxTC1_2_ADC_BIST_EN;
    Haluint8 ACH_TxTC1_2_OFF_DIAG_EN;
    Haluint8 ACH_TxTC1_2_E_SOL_GPO;
    Haluint8 ACH_TxTC1_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxTC1_6_PWM_CODE;
    Haluint8 ACH_TxTC1_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxTC1_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxTC1_8_KI;
    Haluint8 ACH_TxTC1_8_KP;
    Haluint8 ACH_TxTC1_9_INTTIME;
    Haluint8 ACH_TxTC1_9_FILT_TIME;
    Haluint8 ACH_TxTC1_9_CHOP_TIME;
    Haluint8 ACH_TxTC1_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxESV0_2_SR_SEL;
    Haluint8 ACH_TxESV0_2_CALIBRATION_DIS;
    Haluint8 ACH_TxESV0_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxESV0_2_TD_BLANK_SEL;
    Haluint8 ACH_TxESV0_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxESV0_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxESV0_2_DIAG_BIST_EN;
    Haluint8 ACH_TxESV0_2_ADC_BIST_EN;
    Haluint8 ACH_TxESV0_2_OFF_DIAG_EN;
    Haluint8 ACH_TxESV0_2_E_SOL_GPO;
    Haluint8 ACH_TxESV0_2_E_LSCLAMP_GPO;
    Haluint8 ACH_TxESV0_6_PWM_CODE;
    Haluint8 ACH_TxESV0_6_FREQ_MOD_STEP;
    Haluint8 ACH_TxESV0_6_MAX_FREQ_DELTA;
    Haluint8 ACH_TxESV0_8_KI;
    Haluint8 ACH_TxESV0_8_KP;
    Haluint8 ACH_TxESV0_9_INTTIME;
    Haluint8 ACH_TxESV0_9_FILT_TIME;
    Haluint8 ACH_TxESV0_9_CHOP_TIME;
    Haluint8 ACH_TxESV1_2_SR_SEL;
    Haluint8 ACH_TxESV1_2_CALIBRATION_DIS;
    Haluint8 ACH_TxESV1_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxESV1_2_TD_BLANK_SEL;
    Haluint8 ACH_TxESV1_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxESV1_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxESV1_2_DIAG_BIST_EN;
    Haluint8 ACH_TxESV1_2_ADC_BIST_EN;
    Haluint8 ACH_TxESV1_2_OFF_DIAG_EN;
    Haluint8 ACH_TxESV1_2_E_SOL_GPO;
    Haluint8 ACH_TxESV1_2_E_LSCLAMP_GPO;
    Haluint8 ACH_TxESV1_6_PWM_CODE;
    Haluint8 ACH_TxESV1_6_FREQ_MOD_STEP;
    Haluint8 ACH_TxESV1_6_MAX_FREQ_DELTA;
    Haluint8 ACH_TxESV1_8_KI;
    Haluint8 ACH_TxESV1_8_KP;
    Haluint8 ACH_TxESV1_9_INTTIME;
    Haluint8 ACH_TxESV1_9_FILT_TIME;
    Haluint8 ACH_TxESV1_9_CHOP_TIME;
}AcmValveInitWriteData_t;

typedef struct
{
    Haluint8 MotorInitFlg;
    Haluint8 ACH_Tx8_PMP_TCK_PWM;
    Haluint8 ACH_Tx9_PMP_VDS_TH;
    Haluint8 ACH_Tx9_PMP2_DIS;
    Haluint8 ACH_Tx9_PMP_VDS_FIL;
    Haluint8 ACH_Tx9_LDACT_DIS;
    Haluint8 ACH_Tx9_PMP1_ISINC;
    Haluint8 ACH_Tx9_PMP_DT;
    Haluint8 ACH_Tx9_PMP_TEST;
    Haluint8 ACH_Tx9_PMP_BIST_EN;
}AcmMotorInitWriteData_t;

typedef struct
{
    Haluint8 VsoInitDataFlg;
    Haluint8 ACH_Tx15_VSO_CONF;
    Haluint8 ACH_Tx15_WSO_VSO_S;
    Haluint8 ACH_Tx15_SSIG_VSO;
    Haluint8 ACH_Tx15_VSO_PTEN;
    Haluint8 ACH_Tx15_VSO_OFF_DIAG_EN;
    Haluint8 ACH_Tx15_VSO_LS_CLAMP_DIS;
    Haluint8 ACH_Tx15_SEL_CONF;
    Haluint8 ACH_Tx15_WSO_SEL_S;
    Haluint8 ACH_Tx15_SSIG_SEL;
    Haluint8 ACH_Tx15_SEL_OUT_CMD;
    Haluint8 ACH_Tx15_SEL_OFF_DIAG_EN;
    Haluint8 ACH_Tx15_SEL_LS_CLAMP_DIS;
    Haluint8 ACH_Tx15_SEL_PTEN;
}AcmVsoInitWriteData_t;

typedef struct
{
    Haluint8 WssInitDataFlg;
    Haluint8 ACH_Tx32_CONFIG_RANGE;
    Haluint8 ACH_Tx32_WSO_TEST;
    Haluint8 ACH_Tx33_WSI_FIRST_THRESHOLD;
    Haluint8 ACH_Tx34_WSI_OFFSET_THRESHOLD;
    Haluint8 ACH_Tx35_FILTER_SELECTION;
    Haluint8 ACH_Tx35_PTEN;
    Haluint8 ACH_Tx35_SS_DIS;
    Haluint8 ACH_Tx35_FIX_TH;
    Haluint8 ACH_Tx35_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx36_FILTER_SELECTION;
    Haluint8 ACH_Tx36_PTEN;
    Haluint8 ACH_Tx36_SS_DIS;
    Haluint8 ACH_Tx36_FIX_TH;
    Haluint8 ACH_Tx36_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx37_FILTER_SELECTION;
    Haluint8 ACH_Tx37_PTEN;
    Haluint8 ACH_Tx37_SS_DIS;
    Haluint8 ACH_Tx37_FIX_TH;
    Haluint8 ACH_Tx37_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx38_FILTER_SELECTION;
    Haluint8 ACH_Tx38_PTEN;
    Haluint8 ACH_Tx38_SS_DIS;
    Haluint8 ACH_Tx38_FIX_TH;
    Haluint8 ACH_Tx38_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx39_FVPWR_ACT;
    Haluint8 ACH_Tx39_WSI_VDD4_UV;
    Haluint8 ACH_Tx39_INIT;
    Haluint8 ACH_Tx39_DIAG;
    Haluint8 ACH_Tx39_CH4_EN;
    Haluint8 ACH_Tx39_CH3_EN;
    Haluint8 ACH_Tx39_CH2_EN;
    Haluint8 ACH_Tx39_CH1_EN;
    Haluint8 ACH_Tx44_WSI1_LBIST_EN;
    Haluint8 ACH_Tx44_WSI2_LBIST_EN;
    Haluint8 ACH_Tx44_WSI3_LBIST_EN;
    Haluint8 ACH_Tx44_WSI4_LBIST_EN;
}AcmWssInitWriteData_t;

typedef struct
{
    Haluint8 LampShlsInitDataFlg;
    Haluint8 ACH_Tx17_WLD_CONF;
    Haluint8 ACH_Tx17_WLD_CMD;
    Haluint8 ACH_Tx17_WLD_DIAGOFF_EN;
    Haluint8 ACH_Tx17_WLD_LS_CLAMP_DIS;
    Haluint8 ACH_Tx17_SHLS_CMD;
    Haluint8 ACH_Tx17_SHLS_DIAGOFF_EN;
    Haluint8 ACH_Tx17_SHLS_CONFIG;
    Haluint8 ACH_Tx17_SHLS_LS_CLAMP_DIS;
}AcmLampShlsInitWriteData_t;

typedef struct
{
    Haluint8 WatchdogInitDataFlg;
    Haluint8 ACH_Tx2_WD_SW_DIS;
    Haluint8 ACH_Tx2_WD_DIS;
    Haluint8 ACH_Tx74_VALID_REQUEST_START;
    Haluint8 ACH_Tx74_VALID_ANSWER_START;
    Haluint8 ACH_Tx75_VALID_ANSWER_DELTA;
    Haluint8 ACH_Tx75_VALID_REQUEST_DELTA;
    Haluint8 ACH_Tx75_REQ_CHECK_ENABLE;
    Haluint8 ACH_Tx76_ANSWER_TIME_OUT_DELTA;
    Haluint8 ACH_Tx76_REQUEST_TIME_OUT_DELTA;
    Haluint8 ACH_Tx76_TO_RESET_ENABLE;
    Haluint8 ACH_Tx77_NUM_OF_GOOD_STEPS;
    Haluint8 ACH_Tx77_NUM_OF_BAD_STEPS;
    Haluint8 ACH_Tx77_HIGH_TH;
    Haluint8 ACH_Tx77_LOW_TH;
    Haluint8 ACH_Tx77_CLOCK_DIVISION;
    Haluint8 ACH_Tx77_RESET_ENABLE;
}AwdWatchdogInitWriteData_t;

typedef struct
{
    Haluint8 AwdWatchdogAnswerDataFlg;
    Haluint8 AwdACH_Tx78_ANSWER_LOW;
    Haluint8 AwdACH_Tx78_ANSWER_HIGH;
}AwdWatchdogAnswerData_t;

typedef struct
{
    Haluint8 AwdReadSeedFlag;
    Haluint8 AwdReadWatchdogStatus;
}AwdWatchdogSeedReadData_t;

typedef struct
{
    Haluint8 KeepAliveDataFlg;
    Haluint8 ACH_Tx1_PHOLD;
    Haluint8 ACH_Tx1_KA;
}AkaKeepAliveWriteData_t;

typedef struct
{
    Haluint8 ACH_Tx1_FS_VDS_TH;
    Haluint8 ACH_Tx1_FS_CMD;
    Haluint8 ACH_Tx1_FS_EN;
    Haluint8 ACH_Tx1_PMP_EN;
    Haluint8 ACH_Tx1_OSC_SprSpect_DIS;
    Haluint8 ACH_Tx1_VDD_4_OVC_SD_RESTART_TIME;
    Haluint8 ACH_Tx1_VDD4_OVC_FLT_TIME;
    Haluint8 ACH_Tx1_VDD_3_OVC_SD_RESTART_TIME;
    Haluint8 ACH_Tx1_VDD3_OVC_FLT_TIME;
    Haluint8 ACH_Tx2_WD_SW_DIS;
    Haluint8 ACH_Tx2_VDD_2d5_AUTO_SWITCH_OFF;
    Haluint8 ACH_Tx2_VDD1_DIODELOSS_FILT;
    Haluint8 ACH_Tx2_WD_DIS;
    Haluint8 ACH_Tx2_VDD5_DIS;
    Haluint8 ACH_Tx2_VDD2_DIS;
    Haluint8 ACH_Tx19_E_PVDS_GPO;
    Haluint8 ACH_Tx19_E_VDD2d5dWSx_OT_GPO;
    Haluint8 ACH_Tx19_E_FBO_GPO;
    Haluint8 ACH_Tx19_E_FSPI_GPO;
    Haluint8 ACH_Tx19_E_EN_GPO;
    Haluint8 ACH_Tx19_E_FS_VDS_GPO;
}AchAsicInitReadData_t;

typedef struct
{
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_0;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_1;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_2;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_3;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_4;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_5;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_6;
    Haluint8 ACH_Tx64_SOLENOID_ENABLE_7;
    Haluint8 ACH_Tx65_CH0_STATUS_L;
    Haluint8 ACH_Tx65_CH0_STATUS_H;
    Haluint8 ACH_Tx65_CH1_STATUS_L;
    Haluint8 ACH_Tx65_CH1_STATUS_H;
    Haluint8 ACH_Tx65_CH2_STATUS_L;
    Haluint8 ACH_Tx65_CH2_STATUS_H;
    Haluint8 ACH_Tx65_CH3_STATUS_L;
    Haluint8 ACH_Tx65_CH3_STATUS_H;
    Haluint8 ACH_Tx65_CH4_STATUS_L;
    Haluint8 ACH_Tx65_CH4_STATUS_H;
    Haluint8 ACH_Tx65_CH5_STATUS_L;
    Haluint8 ACH_Tx65_CH5_STATUS_H;
    Haluint8 ACH_Tx65_CH6_STATUS_L;
    Haluint8 ACH_Tx65_CH6_STATUS_H;
    Haluint8 ACH_Tx65_CH7_STATUS_L;
    Haluint8 ACH_Tx65_CH7_STATUS_H;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_0;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_1;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_2;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_3;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_4;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_5;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_6;
    Haluint8 ACH_Tx64_2_SOLENOID_ENABLE_7;
    Haluint8 ACH_Tx67_CH1_EN;
    Haluint8 ACH_Tx67_CH2_EN;
    Haluint8 ACH_Tx67_CH3_EN;
    Haluint8 ACH_Tx67_CH4_EN;
    Haluint8 ACH_Tx67_CH1_DIAG_EN;
    Haluint8 ACH_Tx67_CH2_DIAG_EN;
    Haluint8 ACH_Tx67_CH3_DIAG_EN;
    Haluint8 ACH_Tx67_CH4_DIAG_EN;
    Haluint8 ACH_Tx67_CH1_TD_BLANK;
    Haluint8 ACH_Tx67_CH2_TD_BLANK;
    Haluint8 ACH_Tx67_CH3_TD_BLANK;
    Haluint8 ACH_Tx67_CH4_TD_BLANK;
    Haluint8 ACH_Tx67_CH1_CMD;
    Haluint8 ACH_Tx67_CH2_CMD;
    Haluint8 ACH_Tx67_CH3_CMD;
    Haluint8 ACH_Tx67_CH4_CMD;
    Haluint8 ACH_TxNO0_2_SR_SEL;
    Haluint8 ACH_TxNO0_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO0_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO0_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO0_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO0_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO0_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO0_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO0_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO0_2_E_SOL_GPO;
    Haluint8 ACH_TxNO0_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO0_6_PWM_CODE;
    Haluint8 ACH_TxNO0_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO0_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO0_8_KI;
    Haluint8 ACH_TxNO0_8_KP;
    Haluint8 ACH_TxNO0_9_INTTIME;
    Haluint8 ACH_TxNO0_9_FILT_TIME;
    Haluint8 ACH_TxNO0_9_CHOP_TIME;
    Haluint8 ACH_TxNO0_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxNO1_2_SR_SEL;
    Haluint8 ACH_TxNO1_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO1_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO1_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO1_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO1_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO1_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO1_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO1_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO1_2_E_SOL_GPO;
    Haluint8 ACH_TxNO1_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO1_6_PWM_CODE;
    Haluint8 ACH_TxNO1_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO1_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO1_8_KI;
    Haluint8 ACH_TxNO1_8_KP;
    Haluint8 ACH_TxNO1_9_INTTIME;
    Haluint8 ACH_TxNO1_9_FILT_TIME;
    Haluint8 ACH_TxNO1_9_CHOP_TIME;
    Haluint8 ACH_TxNO1_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxNO2_2_SR_SEL;
    Haluint8 ACH_TxNO2_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO2_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO2_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO2_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO2_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO2_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO2_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO2_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO2_2_E_SOL_GPO;
    Haluint8 ACH_TxNO2_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO2_6_PWM_CODE;
    Haluint8 ACH_TxNO2_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO2_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO2_8_KI;
    Haluint8 ACH_TxNO2_8_KP;
    Haluint8 ACH_TxNO2_9_INTTIME;
    Haluint8 ACH_TxNO2_9_FILT_TIME;
    Haluint8 ACH_TxNO2_9_CHOP_TIME;
    Haluint8 ACH_TxNO2_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxNO3_2_SR_SEL;
    Haluint8 ACH_TxNO3_2_CALIBRATION_DIS;
    Haluint8 ACH_TxNO3_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxNO3_2_TD_BLANK_SEL;
    Haluint8 ACH_TxNO3_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxNO3_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxNO3_2_DIAG_BIST_EN;
    Haluint8 ACH_TxNO3_2_ADC_BIST_EN;
    Haluint8 ACH_TxNO3_2_OFF_DIAG_EN;
    Haluint8 ACH_TxNO3_2_E_SOL_GPO;
    Haluint8 ACH_TxNO3_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxNO3_6_PWM_CODE;
    Haluint8 ACH_TxNO3_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxNO3_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxNO3_8_KI;
    Haluint8 ACH_TxNO3_8_KP;
    Haluint8 ACH_TxNO3_9_INTTIME;
    Haluint8 ACH_TxNO3_9_FILT_TIME;
    Haluint8 ACH_TxNO3_9_CHOP_TIME;
    Haluint8 ACH_TxNO3_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxTC0_2_SR_SEL;
    Haluint8 ACH_TxTC0_2_CALIBRATION_DIS;
    Haluint8 ACH_TxTC0_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxTC0_2_TD_BLANK_SEL;
    Haluint8 ACH_TxTC0_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxTC0_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxTC0_2_DIAG_BIST_EN;
    Haluint8 ACH_TxTC0_2_ADC_BIST_EN;
    Haluint8 ACH_TxTC0_2_OFF_DIAG_EN;
    Haluint8 ACH_TxTC0_2_E_SOL_GPO;
    Haluint8 ACH_TxTC0_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxTC0_6_PWM_CODE;
    Haluint8 ACH_TxTC0_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxTC0_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxTC0_8_KI;
    Haluint8 ACH_TxTC0_8_KP;
    Haluint8 ACH_TxTC0_9_INTTIME;
    Haluint8 ACH_TxTC0_9_FILT_TIME;
    Haluint8 ACH_TxTC0_9_CHOP_TIME;
    Haluint8 ACH_TxTC0_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxTC1_2_SR_SEL;
    Haluint8 ACH_TxTC1_2_CALIBRATION_DIS;
    Haluint8 ACH_TxTC1_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxTC1_2_TD_BLANK_SEL;
    Haluint8 ACH_TxTC1_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxTC1_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxTC1_2_DIAG_BIST_EN;
    Haluint8 ACH_TxTC1_2_ADC_BIST_EN;
    Haluint8 ACH_TxTC1_2_OFF_DIAG_EN;
    Haluint8 ACH_TxTC1_2_E_SOL_GPO;
    Haluint8 ACH_TxTC1_2_E_LSCLAMP_GPO;
    Haluint16 ACH_TxTC1_6_PWM_CODE;
    Haluint8 ACH_TxTC1_7_FREQ_MOD_STEP;
    Haluint8 ACH_TxTC1_7_MAX_FREQ_DELTA;
    Haluint8 ACH_TxTC1_8_KI;
    Haluint8 ACH_TxTC1_8_KP;
    Haluint8 ACH_TxTC1_9_INTTIME;
    Haluint8 ACH_TxTC1_9_FILT_TIME;
    Haluint8 ACH_TxTC1_9_CHOP_TIME;
    Haluint8 ACH_TxTC1_13_BASE_DELTA_CURENT;
    Haluint8 ACH_TxESV0_2_SR_SEL;
    Haluint8 ACH_TxESV0_2_CALIBRATION_DIS;
    Haluint8 ACH_TxESV0_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxESV0_2_TD_BLANK_SEL;
    Haluint8 ACH_TxESV0_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxESV0_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxESV0_2_DIAG_BIST_EN;
    Haluint8 ACH_TxESV0_2_ADC_BIST_EN;
    Haluint8 ACH_TxESV0_2_OFF_DIAG_EN;
    Haluint8 ACH_TxESV0_2_E_SOL_GPO;
    Haluint8 ACH_TxESV0_2_E_LSCLAMP_GPO;
    Haluint8 ACH_TxESV0_6_PWM_CODE;
    Haluint8 ACH_TxESV0_6_FREQ_MOD_STEP;
    Haluint8 ACH_TxESV0_6_MAX_FREQ_DELTA;
    Haluint8 ACH_TxESV0_8_KI;
    Haluint8 ACH_TxESV0_8_KP;
    Haluint8 ACH_TxESV0_9_INTTIME;
    Haluint8 ACH_TxESV0_9_FILT_TIME;
    Haluint8 ACH_TxESV0_9_CHOP_TIME;
    Haluint8 ACH_TxESV1_2_SR_SEL;
    Haluint8 ACH_TxESV1_2_CALIBRATION_DIS;
    Haluint8 ACH_TxESV1_2_OFS_CHOP_DIS;
    Haluint8 ACH_TxESV1_2_TD_BLANK_SEL;
    Haluint8 ACH_TxESV1_2_LS_CLAMP_DIS;
    Haluint8 ACH_TxESV1_2_LOGIC_BIST_EN;
    Haluint8 ACH_TxESV1_2_DIAG_BIST_EN;
    Haluint8 ACH_TxESV1_2_ADC_BIST_EN;
    Haluint8 ACH_TxESV1_2_OFF_DIAG_EN;
    Haluint8 ACH_TxESV1_2_E_SOL_GPO;
    Haluint8 ACH_TxESV1_2_E_LSCLAMP_GPO;
    Haluint8 ACH_TxESV1_6_PWM_CODE;
    Haluint8 ACH_TxESV1_6_FREQ_MOD_STEP;
    Haluint8 ACH_TxESV1_6_MAX_FREQ_DELTA;
    Haluint8 ACH_TxESV1_8_KI;
    Haluint8 ACH_TxESV1_8_KP;
    Haluint8 ACH_TxESV1_9_INTTIME;
    Haluint8 ACH_TxESV1_9_FILT_TIME;
    Haluint8 ACH_TxESV1_9_CHOP_TIME;
}AchValveInitReadData_t;

typedef struct
{
    Haluint8 ACH_Tx8_PMP_TCK_PWM;
    Haluint8 ACH_Tx9_PMP_VDS_TH;
    Haluint8 ACH_Tx9_PMP2_DIS;
    Haluint8 ACH_Tx9_PMP_VDS_FIL;
    Haluint8 ACH_Tx9_LDACT_DIS;
    Haluint8 ACH_Tx9_PMP1_ISINC;
    Haluint8 ACH_Tx9_PMP_DT;
    Haluint8 ACH_Tx9_PMP_TEST;
    Haluint8 ACH_Tx9_PMP_BIST_EN;
}AchMotorInitReadData_t;

typedef struct
{
    Haluint8 ACH_Tx15_VSO_CONF;
    Haluint8 ACH_Tx15_WSO_VSO_S;
    Haluint8 ACH_Tx15_SSIG_VSO;
    Haluint8 ACH_Tx15_VSO_PTEN;
    Haluint8 ACH_Tx15_VSO_OFF_DIAG_EN;
    Haluint8 ACH_Tx15_VSO_LS_CLAMP_DIS;
    Haluint8 ACH_Tx15_SEL_CONF;
    Haluint8 ACH_Tx15_WSO_SEL_S;
    Haluint8 ACH_Tx15_SSIG_SEL;
    Haluint8 ACH_Tx15_SEL_OUT_CMD;
    Haluint8 ACH_Tx15_SEL_OFF_DIAG_EN;
    Haluint8 ACH_Tx15_SEL_LS_CLAMP_DIS;
    Haluint8 ACH_Tx15_SEL_PTEN;
}AchVsoInitReadData_t;

typedef struct
{
    Haluint8 ACH_Tx32_CONFIG_RANGE;
    Haluint8 ACH_Tx32_WSO_TEST;
    Haluint8 ACH_Tx33_WSI_FIRST_THRESHOLD;
    Haluint8 ACH_Tx34_WSI_OFFSET_THRESHOLD;
    Haluint8 ACH_Tx35_FILTER_SELECTION;
    Haluint8 ACH_Tx35_PTEN;
    Haluint8 ACH_Tx35_SS_DIS;
    Haluint8 ACH_Tx35_FIX_TH;
    Haluint8 ACH_Tx35_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx36_FILTER_SELECTION;
    Haluint8 ACH_Tx36_PTEN;
    Haluint8 ACH_Tx36_SS_DIS;
    Haluint8 ACH_Tx36_FIX_TH;
    Haluint8 ACH_Tx36_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx37_FILTER_SELECTION;
    Haluint8 ACH_Tx37_PTEN;
    Haluint8 ACH_Tx37_SS_DIS;
    Haluint8 ACH_Tx37_FIX_TH;
    Haluint8 ACH_Tx37_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx38_FILTER_SELECTION;
    Haluint8 ACH_Tx38_PTEN;
    Haluint8 ACH_Tx38_SS_DIS;
    Haluint8 ACH_Tx38_FIX_TH;
    Haluint8 ACH_Tx38_SENSOR_TYPE_SELECTION;
    Haluint8 ACH_Tx39_FVPWR_ACT;
    Haluint8 ACH_Tx39_WSI_VDD4_UV;
    Haluint8 ACH_Tx39_INIT;
    Haluint8 ACH_Tx39_DIAG;
    Haluint8 ACH_Tx39_CH4_EN;
    Haluint8 ACH_Tx39_CH3_EN;
    Haluint8 ACH_Tx39_CH2_EN;
    Haluint8 ACH_Tx39_CH1_EN;
    Haluint8 ACH_Tx44_WSI1_LBIST_EN;
    Haluint8 ACH_Tx44_WSI2_LBIST_EN;
    Haluint8 ACH_Tx44_WSI3_LBIST_EN;
    Haluint8 ACH_Tx44_WSI4_LBIST_EN;
}AchWssInitReadData_t;

typedef struct
{
    Haluint8 ACH_Tx17_WLD_CONF;
    Haluint8 ACH_Tx17_WLD_CMD;
    Haluint8 ACH_Tx17_WLD_DIAGOFF_EN;
    Haluint8 ACH_Tx17_WLD_LS_CLAMP_DIS;
    Haluint8 ACH_Tx17_SHLS_CMD;
    Haluint8 ACH_Tx17_SHLS_DIAGOFF_EN;
    Haluint8 ACH_Tx17_SHLS_CONFIG;
    Haluint8 ACH_Tx17_SHLS_LS_CLAMP_DIS;
}AchLampShlsInitReadData_t;

typedef struct
{
    Haluint8 ACH_Tx2_WD_SW_DIS;
    Haluint8 ACH_Tx2_WD_DIS;
    Haluint8 ACH_Tx74_VALID_REQUEST_START;
    Haluint8 ACH_Tx74_VALID_ANSWER_START;
    Haluint8 ACH_Tx75_VALID_ANSWER_DELTA;
    Haluint8 ACH_Tx75_VALID_REQUEST_DELTA;
    Haluint8 ACH_Tx75_REQ_CHECK_ENABLE;
    Haluint8 ACH_Tx76_ANSWER_TIME_OUT_DELTA;
    Haluint8 ACH_Tx76_REQUEST_TIME_OUT_DELTA;
    Haluint8 ACH_Tx76_TO_RESET_ENABLE;
    Haluint8 ACH_Tx77_NUM_OF_GOOD_STEPS;
    Haluint8 ACH_Tx77_NUM_OF_BAD_STEPS;
    Haluint8 ACH_Tx77_HIGH_TH;
    Haluint8 ACH_Tx77_LOW_TH;
    Haluint8 ACH_Tx77_CLOCK_DIVISION;
    Haluint8 ACH_Tx77_RESET_ENABLE;
}AchWatchdogInitReadData_t;

typedef struct
{
    Haluint8 HAL_RX72mu8_Rx_72_SEED;
    Haluint8 HAL_RX73mu4_Rx_73_WD_CNT_VALUE;
    Haluint8 HAL_RX73mu1_Rx_73_WD_EARLY_ANSW;
    Haluint8 HAL_RX73mu1_Rx_73_WD_LATE_ANSW;
    Haluint8 HAL_RX73mu1_Rx_73_WD_BAD_ANSW;
    Haluint8 HAL_RX73mu1_Rx_73_WD_EARLY_REQ;
    Haluint8 HAL_RX73mu1_Rx_73_WD_LATE_REQ;
    Haluint8 HAL_RX73mu1_Rx_73_WD_RST_TO_ANSW;
    Haluint8 HAL_RX73mu1_Rx_73_WD_RST_TO_REQ;
    Haluint8 HAL_RX73mu1_Rx_73_WD_RST_CNT;
    Haluint8 HAL_RX73mu4_Rx_73_WD_RST_EVENT_VALUE;
}AchWatchdogReadData_t;

typedef struct
{
    Haluint8 Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
}AchWssPort0AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
}AchWssPort1AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
}AchWssPort2AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
}AchWssPort3AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_VSO_T_SD;
    Haluint8 Ach_Asic_VSO_OPENLOAD;
    Haluint8 Ach_Asic_VSO_LVT;
    Haluint8 Ach_Asic_VSO_VGS_LS_FAULT;
    Haluint8 Ach_Asic_VSO_LS_CLAMP_ACT;
    Haluint8 Ach_Asic_VSO_LS_OVC;
    Haluint8 Ach_Asic_GND_LOSS;
    Haluint8 Ach_Asic_SEL_T_SD;
    Haluint8 Ach_Asic_SEL_OPENLOAD;
    Haluint8 Ach_Asic_SEL_LVT;
    Haluint8 Ach_Asic_SEL_VGS_LS_FAULT;
    Haluint8 Ach_Asic_SEL_LS_CLAMP_ACT;
    Haluint8 Ach_Asic_SEL_LS_OVC;
}AchVsoSelAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_WLD_T_SD;
    Haluint8 Ach_Asic_WLD_OPL;
    Haluint8 Ach_Asic_WLD_LVT;
    Haluint8 Ach_Asic_WLD_LS_OVC;
    Haluint8 Ach_Asic_WLD_VGS_LS_FAULT;
    Haluint8 Ach_Asic_WLD_LS_CLAMP;
    Haluint8 Ach_Asic_SHLS_T_SD;
    Haluint8 Ach_Asic_SHLS_OPL;
    Haluint8 Ach_Asic_SHLS_LVT;
    Haluint8 Ach_Asic_SHLS_OVC;
    Haluint8 Ach_Asic_SHLS_VGS_FAULT;
    Haluint8 Ach_Asic_SHLS_LS_CLAMP;
}AchWldShlsAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH0_WS1_CNT;
    Haluint8 Ach_Asic_CH1_WS1_CNT;
    Haluint8 Ach_Asic_CH2_WS1_CNT;
    Haluint8 Ach_Asic_CH3_WS1_CNT;
}AchWssRedncyAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_vdd1_ovc;
    Haluint8 Ach_Asic_vdd2_out_of_reg;
    Haluint8 Ach_Asic_vdd3_ovc;
    Haluint8 Ach_Asic_vdd4_ovc;
    Haluint8 Ach_Asic_vdd1_diode_loss;
    Haluint8 Ach_Asic_vdd2_rev_curr;
    Haluint8 Ach_Asic_vdd1_t_sd;
    Haluint8 Ach_Asic_vdd2_t_sd;
    Haluint8 Ach_Asic_vdd1_uv;
    Haluint8 Ach_Asic_vdd2_uv;
    Haluint8 Ach_Asic_vdd3_uv;
    Haluint8 Ach_Asic_vdd4_uv;
    Haluint8 Ach_Asic_vdd1_ov;
    Haluint8 Ach_Asic_vdd2_ov;
    Haluint8 Ach_Asic_vdd3_ov;
    Haluint8 Ach_Asic_vdd4_ov;
    Haluint8 Ach_Asic_vint_ov;
    Haluint8 Ach_Asic_vint_uv;
    Haluint8 Ach_Asic_dgndloss;
    Haluint8 Ach_Asic_vpwr_uv;
    Haluint8 Ach_Asic_vpwr_ov;
    Haluint8 Ach_Asic_comm_too_long;
    Haluint8 Ach_Asic_comm_too_short;
    Haluint8 Ach_Asic_comm_wrong_add;
    Haluint8 Ach_Asic_comm_wrong_fcnt;
    Haluint8 Ach_Asic_comm_wrong_crc;
}AchSysPwrAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_FS_TURN_ON;
    Haluint8 Ach_Asic_FS_TURN_OFF;
    Haluint8 Ach_Asic_FS_VDS_FAULT;
    Haluint8 Ach_Asic_FS_RVP_FAULT;
    Haluint8 Ach_Asic_VHD_OV;
    Haluint8 Ach_Asic_T_SD_INT;
    Haluint8 Ach_Asic_CP_OV;
    Haluint8 Ach_Asic_CP_UV;
    Haluint8 Ach_Asic_VPWR_UV;
    Haluint8 Ach_Asic_VPWR_OV;
    Haluint8 Ach_Asic_PMP_LD_ACT;
    Haluint16 Ach_Asic_No0_AVG_CURRENT;
    Haluint8 Ach_Asic_No0_PWM_FAULT;
    Haluint8 Ach_Asic_No0_CURR_SENSE;
    Haluint8 Ach_Asic_No0_ADC_FAULT;
    Haluint8 Ach_Asic_No0_T_SD;
    Haluint8 Ach_Asic_No0_OPEN_LOAD;
    Haluint8 Ach_Asic_No0_GND_LOSS;
    Haluint8 Ach_Asic_No0_LVT;
    Haluint8 Ach_Asic_No0_LS_OVC;
    Haluint8 Ach_Asic_No0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No0_HS_SHORT;
    Haluint8 Ach_Asic_No0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No0_UNDER_CURR;
    Haluint16 Ach_Asic_No1_AVG_CURRENT;
    Haluint8 Ach_Asic_No1_PWM_FAULT;
    Haluint8 Ach_Asic_No1_CURR_SENSE;
    Haluint8 Ach_Asic_No1_ADC_FAULT;
    Haluint8 Ach_Asic_No1_T_SD;
    Haluint8 Ach_Asic_No1_OPEN_LOAD;
    Haluint8 Ach_Asic_No1_GND_LOSS;
    Haluint8 Ach_Asic_No1_LVT;
    Haluint8 Ach_Asic_No1_LS_OVC;
    Haluint8 Ach_Asic_No1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No1_HS_SHORT;
    Haluint8 Ach_Asic_No1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No1_UNDER_CURR;
    Haluint16 Ach_Asic_No2_AVG_CURRENT;
    Haluint8 Ach_Asic_No2_PWM_FAULT;
    Haluint8 Ach_Asic_No2_CURR_SENSE;
    Haluint8 Ach_Asic_No2_ADC_FAULT;
    Haluint8 Ach_Asic_No2_T_SD;
    Haluint8 Ach_Asic_No2_OPEN_LOAD;
    Haluint8 Ach_Asic_No2_GND_LOSS;
    Haluint8 Ach_Asic_No2_LVT;
    Haluint8 Ach_Asic_No2_LS_OVC;
    Haluint8 Ach_Asic_No2_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No2_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No2_HS_SHORT;
    Haluint8 Ach_Asic_No2_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No2_UNDER_CURR;
    Haluint16 Ach_Asic_No3_AVG_CURRENT;
    Haluint8 Ach_Asic_No3_PWM_FAULT;
    Haluint8 Ach_Asic_No3_CURR_SENSE;
    Haluint8 Ach_Asic_No3_ADC_FAULT;
    Haluint8 Ach_Asic_No3_T_SD;
    Haluint8 Ach_Asic_No3_OPEN_LOAD;
    Haluint8 Ach_Asic_No3_GND_LOSS;
    Haluint8 Ach_Asic_No3_LVT;
    Haluint8 Ach_Asic_No3_LS_OVC;
    Haluint8 Ach_Asic_No3_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No3_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No3_HS_SHORT;
    Haluint8 Ach_Asic_No3_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No3_UNDER_CURR;
    Haluint16 Ach_Asic_Nc0_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc0_PWM_FAULT;
    Haluint8 Ach_Asic_Nc0_CURR_SENSE;
    Haluint8 Ach_Asic_Nc0_ADC_FAULT;
    Haluint8 Ach_Asic_Nc0_T_SD;
    Haluint8 Ach_Asic_Nc0_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc0_GND_LOSS;
    Haluint8 Ach_Asic_Nc0_LVT;
    Haluint8 Ach_Asic_Nc0_LS_OVC;
    Haluint8 Ach_Asic_Nc0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc0_HS_SHORT;
    Haluint8 Ach_Asic_Nc0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc0_UNDER_CURR;
    Haluint16 Ach_Asic_Nc1_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc1_PWM_FAULT;
    Haluint8 Ach_Asic_Nc1_CURR_SENSE;
    Haluint8 Ach_Asic_Nc1_ADC_FAULT;
    Haluint8 Ach_Asic_Nc1_T_SD;
    Haluint8 Ach_Asic_Nc1_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc1_GND_LOSS;
    Haluint8 Ach_Asic_Nc1_LVT;
    Haluint8 Ach_Asic_Nc1_LS_OVC;
    Haluint8 Ach_Asic_Nc1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc1_HS_SHORT;
    Haluint8 Ach_Asic_Nc1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc1_UNDER_CURR;
    Haluint16 Ach_Asic_Nc2_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc2_PWM_FAULT;
    Haluint8 Ach_Asic_Nc2_CURR_SENSE;
    Haluint8 Ach_Asic_Nc2_ADC_FAULT;
    Haluint8 Ach_Asic_Nc2_T_SD;
    Haluint8 Ach_Asic_Nc2_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc2_GND_LOSS;
    Haluint8 Ach_Asic_Nc2_LVT;
    Haluint8 Ach_Asic_Nc2_LS_OVC;
    Haluint8 Ach_Asic_Nc2_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc2_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc2_HS_SHORT;
    Haluint8 Ach_Asic_Nc2_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc2_UNDER_CURR;
    Haluint16 Ach_Asic_Nc3_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc3_PWM_FAULT;
    Haluint8 Ach_Asic_Nc3_CURR_SENSE;
    Haluint8 Ach_Asic_Nc3_ADC_FAULT;
    Haluint8 Ach_Asic_Nc3_T_SD;
    Haluint8 Ach_Asic_Nc3_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc3_GND_LOSS;
    Haluint8 Ach_Asic_Nc3_LVT;
    Haluint8 Ach_Asic_Nc3_LS_OVC;
    Haluint8 Ach_Asic_Nc3_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc3_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc3_HS_SHORT;
    Haluint8 Ach_Asic_Nc3_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc3_UNDER_CURR;
    Haluint16 Ach_Asic_Tc0_AVG_CURRENT;
    Haluint8 Ach_Asic_Tc0_PWM_FAULT;
    Haluint8 Ach_Asic_Tc0_CURR_SENSE;
    Haluint8 Ach_Asic_Tc0_ADC_FAULT;
    Haluint8 Ach_Asic_Tc0_T_SD;
    Haluint8 Ach_Asic_Tc0_OPEN_LOAD;
    Haluint8 Ach_Asic_Tc0_GND_LOSS;
    Haluint8 Ach_Asic_Tc0_LVT;
    Haluint8 Ach_Asic_Tc0_LS_OVC;
    Haluint8 Ach_Asic_Tc0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Tc0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Tc0_HS_SHORT;
    Haluint8 Ach_Asic_Tc0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Tc0_UNDER_CURR;
    Haluint16 Ach_Asic_Tc1_AVG_CURRENT;
    Haluint8 Ach_Asic_Tc1_PWM_FAULT;
    Haluint8 Ach_Asic_Tc1_CURR_SENSE;
    Haluint8 Ach_Asic_Tc1_ADC_FAULT;
    Haluint8 Ach_Asic_Tc1_T_SD;
    Haluint8 Ach_Asic_Tc1_OPEN_LOAD;
    Haluint8 Ach_Asic_Tc1_GND_LOSS;
    Haluint8 Ach_Asic_Tc1_LVT;
    Haluint8 Ach_Asic_Tc1_LS_OVC;
    Haluint8 Ach_Asic_Tc1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Tc1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Tc1_HS_SHORT;
    Haluint8 Ach_Asic_Tc1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Tc1_UNDER_CURR;
    Haluint16 Ach_Asic_Esv0_AVG_CURRENT;
    Haluint8 Ach_Asic_Esv0_PWM_FAULT;
    Haluint8 Ach_Asic_Esv0_CURR_SENSE;
    Haluint8 Ach_Asic_Esv0_ADC_FAULT;
    Haluint8 Ach_Asic_Esv0_T_SD;
    Haluint8 Ach_Asic_Esv0_OPEN_LOAD;
    Haluint8 Ach_Asic_Esv0_GND_LOSS;
    Haluint8 Ach_Asic_Esv0_LVT;
    Haluint8 Ach_Asic_Esv0_LS_OVC;
    Haluint8 Ach_Asic_Esv0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Esv0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Esv0_HS_SHORT;
    Haluint8 Ach_Asic_Esv0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Esv0_UNDER_CURR;
    Haluint16 Ach_Asic_Esv1_AVG_CURRENT;
    Haluint8 Ach_Asic_Esv1_PWM_FAULT;
    Haluint8 Ach_Asic_Esv1_CURR_SENSE;
    Haluint8 Ach_Asic_Esv1_ADC_FAULT;
    Haluint8 Ach_Asic_Esv1_T_SD;
    Haluint8 Ach_Asic_Esv1_OPEN_LOAD;
    Haluint8 Ach_Asic_Esv1_GND_LOSS;
    Haluint8 Ach_Asic_Esv1_LVT;
    Haluint8 Ach_Asic_Esv1_LS_OVC;
    Haluint8 Ach_Asic_Esv1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Esv1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Esv1_HS_SHORT;
    Haluint8 Ach_Asic_Esv1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Esv1_UNDER_CURR;
}AchValveAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_PMP_LD_ACT;
    Haluint8 Ach_Asic_PMP1_TURN_ON;
    Haluint8 Ach_Asic_PMP1_TURN_OFF;
    Haluint8 Ach_Asic_PMP2_TURN_ON;
    Haluint8 Ach_Asic_PMP2_TURN_OFF;
    Haluint8 Ach_Asic_PMP3_TURN_ON;
    Haluint8 Ach_Asic_PMP3_TURN_OFF;
    Haluint8 Ach_Asic_PMP_VDS_TURNOFF;
    Haluint8 Ach_Asic_PMP1_VDS_FAULT;
    Haluint8 Ach_Asic_PMP_FLYBACK;
    Haluint8 Ach_Asic_CP_OV;
    Haluint8 Ach_Asic_CP_UV;
}AchMotorAsicInfo_t;

typedef struct
{
    Haluint8 MtrDutyDataFlg;
    Haluint8 ACH_Tx8_PUMP_DTY_PWM;
}IocDcMtrDutyData_t;

typedef struct
{
    Haluint8 MtrFreqDataFlg;
    Haluint8 ACH_Tx8_PUMP_TCK_PWM;
}IocDcMtrFreqData_t;

typedef struct
{
    Haluint8 VlvNo0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo0Data_t;

typedef struct
{
    Haluint8 VlvNo1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo1Data_t;

typedef struct
{
    Haluint8 VlvNo2DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo2Data_t;

typedef struct
{
    Haluint8 VlvNo3DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo3Data_t;

typedef struct
{
    Haluint8 VlvTc0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvTc0Data_t;

typedef struct
{
    Haluint8 VlvTc1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvTc1Data_t;

typedef struct
{
    Haluint8 VlvEsv0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvEsv0Data_t;

typedef struct
{
    Haluint8 VlvEsv1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvEsv1Data_t;

typedef struct
{
    Haluint8 VlvNc0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc0Data_t;

typedef struct
{
    Haluint8 VlvNc1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc1Data_t;

typedef struct
{
    Haluint8 VlvNc2DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc2Data_t;

typedef struct
{
    Haluint8 VlvNc3DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc3Data_t;

typedef struct
{
    Haluint8 VlvRelayDataFlg;
    Haluint8 ACH_Tx1_FS_CMD;
}IocVlvRelayData_t;

typedef struct
{
    Haluint8 AdcSelDataFlg;
    Haluint8 ACH_Tx13_ADC_SEL;
}IocAdcSelWriteData_t;

typedef struct
{
    Haluint16 ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT;
    Haluint16 ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY;
}AchAdcData_t;


typedef Haluint8 AsicSchTblNum_t;
typedef Haluint8 GdFaultFlg2_t;
typedef Haluint8 GdCoast_t;
typedef Haluint8 GdEnStopOnFault_t;
typedef Haluint8 GdMotDrvEn_t;
typedef Haluint16 Sup3p3vMon_t;
typedef Haluint16 Ext5vMon_t;
typedef Haluint16 AsicTempMon_t;
typedef Haluint16 CEMon_t;
typedef Haluint16 DiagOutMon_t;
typedef Haluint16 FspAbsHMon_t;
typedef Haluint16 FspAbsMon_t;
typedef Haluint16 FspCbsHMon_t;
typedef Haluint16 FspCbsMon_t;
typedef Haluint16 LineTestMon_t;
typedef Haluint16 TempMon_t;
typedef Haluint16 VBatt1Mon_t;
typedef Haluint16 VBatt2Mon_t;
typedef Haluint16 VddMon_t;
typedef Haluint8 RsmDbcMon_t;
typedef Haluint8 PbcVdaAi_t;
typedef Haluint16 CspMon_t;
typedef Haluint8 MainCanEn_t;
typedef Haluint8 SetDirection_t;
typedef Haluint8 VlvDrvEnRst_t;
typedef Haluint8 GdMotDrvRst_t;
typedef Haluint8 AcmAsicInitCompleteFlag_t;
typedef Haluint8 AwdWatchdogSeedReqData_t;
typedef Haluint8 AwdPrnDrv_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* HAL_TYPES_H_ */
/** @} */
