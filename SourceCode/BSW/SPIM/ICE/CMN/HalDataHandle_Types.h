/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef HAL_DATAHANDLE_TYPES_H_
#define HAL_DATAHANDLE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef AchWssRedncyAsicInfo_t Ach_InputAchWssRedncyAsicInfo_t;
typedef MotMonInfo_t Ioc_InputSR1msMotMonInfo_t;
typedef Ext5vMon_t Ioc_InputSR1msExt5vMon_t;
typedef IocVlvEsv1Data_t Ioc_OutputSR1msIocVlvEsv1Data_t;
typedef SpiTxAsicInfo_t Asic_Mgh80Com_Hndlr1msSpiTxAsicInfo_t;
typedef WhlVlvFbMonInfo_t Ioc_InputSR1msWhlVlvFbMonInfo_t;
typedef RlyMonInfo_t Ioc_InputCS1msRlyMonInfo_t;
typedef SwTrigTmpInfo_t AdcIf_Conv1msSwTrigTmpInfo_t;
typedef SwTrigFspInfo_t AdcIf_Conv1msSwTrigFspInfo_t;
typedef PedlSigMonInfo_t Ioc_InputSR1msPedlSigMonInfo_t;
typedef IocVlvNo2Data_t Ioc_OutputSR1msIocVlvNo2Data_t;
typedef SwTrigMocInfo_t AdcIf_Conv1msSwTrigMocInfo_t;
typedef RisngIdxInfo_t Icu_InputCaptureRisngIdxInfo_t;
typedef SwTrigPwrInfoEsc_t AdcIfEsc_Conv1msSwTrigPwrInfoEsc_t;
typedef GdCoast_t Gd_A4935_HndlrGdCoast_t;
typedef VlvDrvEnRst_t Vlvd_A3944_HndlrVlvDrvEnRst_t;
typedef SwTrigPdtInfo_t AdcIf_Conv1msSwTrigPdtInfo_t;
typedef MotAngleMonInfo_t Ioc_InputSR50usMotAngleMonInfo_t;
typedef PwrMonInfoEsc_t Ioc_InputSR1msPwrMonInfoEsc_t;
typedef AchVsoInitReadData_t Ach_InputAchVsoInitReadData_t;
typedef IocDcMtrDutyData_t Ioc_OutputSR1msIocDcMtrDutyData_t;
typedef SwTrigEpbInfoEsc_t AdcIfEsc_Conv1msSwTrigEpbInfoEsc_t;
typedef PressMonInfo_t Ioc_InputSR1msPressMonInfo_t;
typedef FaultStsMpsInfo_t Mps_A1334_HndlrFaultStsMpsInfo_t;
typedef RxAsicPhyInfo_t Spim_Rx1msRxAsicPhyInfo_t;
typedef SwTrigPwrInfo_t AdcIf_Conv1msSwTrigPwrInfo_t;
typedef LineTestMon_t Ioc_InputSR1msLineTestMon_t;
typedef FaultStsAsicInfo_t Asic_Mgh80Com_Hndlr1msFaultStsAsicInfo_t;
typedef NormVlvMonInfo_t Ioc_InputSR1msNormVlvMonInfo_t;
typedef AsicSchTblNum_t Asic_Mgh80Com_Hndlr1msAsicSchTblNum_t;
typedef EpbMonInfoEsc_t Ioc_InputSR1msEpbMonInfoEsc_t;
typedef HwTrigVlvInfo_t AdcIf_Conv1msHwTrigVlvInfo_t;
typedef AwdWatchdogSeedReqData_t Awd_mainAwdWatchdogSeedReqData_t;
typedef AchWssPort3AsicInfo_t Ach_InputAchWssPort3AsicInfo_t;
typedef IocVlvNo0Data_t Ioc_OutputSR1msIocVlvNo0Data_t;
typedef AchWssPort0AsicInfo_t Ach_InputAchWssPort0AsicInfo_t;
typedef IocVlvNc3Data_t Ioc_OutputSR1msIocVlvNc3Data_t;
typedef WssMonInfo_t Ioc_InputSR5msWssMonInfo_t;
typedef AchWssPort2AsicInfo_t Ach_InputAchWssPort2AsicInfo_t;
typedef RxMpsAngleInfo_t Spim_Rx50usRxMpsAngleInfo_t;
typedef SwTrigReservedEsc_t AdcIfEsc_Conv1msSwTrigReservedEsc_t;
typedef MainCanEn_t CanTrv_TLE6251_HndlrMainCanEn_t;
typedef CspMon_t Ioc_InputSR1msCspMon_t;
typedef AchWatchdogReadData_t Ach_InputAchWatchdogReadData_t;
typedef SwTrigMotInfo_t AdcIf_Conv1msSwTrigMotInfo_t;
typedef TempMon_t Ioc_InputSR1msTempMon_t;
typedef GdMonInfo_t Ioc_InputCS1msGdMonInfo_t;
typedef AchAdcData_t Ach_InputAchAdcData_t;
typedef RxRegInfo_t Spim_Rx1msRxRegInfo_t;
typedef HalPressureInfo_t Ioc_InputSR1msHalPressureInfo_t;
typedef AchWldShlsAsicInfo_t Ach_InputAchWldShlsAsicInfo_t;
typedef GdEnStopOnFault_t Gd_A4935_HndlrGdEnStopOnFault_t;
typedef SwLineMonInfo_t Ioc_InputCS1msSwLineMonInfo_t;
typedef AwdWatchdogSeedReadData_t Awd_mainAwdWatchdogSeedReadData_t;
typedef SwTrigPressInfo_t AdcIf_Conv1msSwTrigPressInfo_t;
typedef AchAsicInitReadData_t Ach_InputAchAsicInitReadData_t;
typedef AcmValveInitWriteData_t Acm_MainAcmValveInitWriteData_t;
typedef Sup3p3vMon_t Ioc_InputSR1msSup3p3vMon_t;
typedef IocVlvEsv0Data_t Ioc_OutputSR1msIocVlvEsv0Data_t;
typedef VBatt2Mon_t Ioc_InputSR1msVBatt2Mon_t;
typedef FallIdxInfo_t Icu_InputCaptureFallIdxInfo_t;
typedef FaultStsGdInfo_t Gd_A4935_HndlrFaultStsGdInfo_t;
typedef FallTiStampInfo_t Icu_InputCaptureFallTiStampInfo_t;
typedef FspAbsHMon_t Ioc_InputSR1msFspAbsHMon_t;
typedef IocAdcSelWriteData_t Ioc_OutputSR1msIocAdcSelWriteData_t;
typedef MotCurrMonInfo_t Ioc_InputSR50usMotCurrMonInfo_t;
typedef GdFaultFlg2_t Gd_A4935_HndlrGdFaultFlg2_t;
typedef IocVlvNc0Data_t Ioc_OutputSR1msIocVlvNc0Data_t;
typedef VddMon_t Ioc_InputSR1msVddMon_t;
typedef AcmAsicInitCompleteFlag_t Acm_MainAcmAsicInitCompleteFlag_t;
typedef AchSysPwrAsicInfo_t Ach_InputAchSysPwrAsicInfo_t;
typedef AcmWssInitWriteData_t Acm_MainAcmWssInitWriteData_t;
typedef IocVlvTc0Data_t Ioc_OutputSR1msIocVlvTc0Data_t;
typedef RxAsicInfo_t Spim_Rx1msRxAsicInfo_t;
typedef IocDcMtrFreqData_t Ioc_OutputSR1msIocDcMtrFreqData_t;
typedef AwdWatchdogInitWriteData_t Awd_mainAwdWatchdogInitWriteData_t;
typedef FspAbsMon_t Ioc_InputSR1msFspAbsMon_t;
typedef IocVlvTc1Data_t Ioc_OutputSR1msIocVlvTc1Data_t;
typedef IocVlvRelayData_t Ioc_OutputSR1msIocVlvRelayData_t;
typedef AcmAsicInitWriteData_t Acm_MainAcmAsicInitWriteData_t;
typedef AkaKeepAliveWriteData_t Aka_MainAkaKeepAliveWriteData_t;
typedef AchMotorAsicInfo_t Ach_InputAchMotorAsicInfo_t;
typedef SwtMonInfoEsc_t Ioc_InputCS1msSwtMonInfoEsc_t;
typedef WhlVlvOvMonInfo_t Ioc_InputCS1msWhlVlvOvMonInfo_t;
typedef RsmDbcMon_t Ioc_InputCS1msRsmDbcMon_t;
typedef SentHPressureInfo_t SentH_MainSentHPressureInfo_t;
typedef VBatt1Mon_t Ioc_InputSR1msVBatt1Mon_t;
typedef MocMonInfo_t Ioc_InputSR1msMocMonInfo_t;
typedef AchWssInitReadData_t Ach_InputAchWssInitReadData_t;
typedef DiagOutMon_t Ioc_InputSR1msDiagOutMon_t;
typedef SpiTxAsicSeedKeyInfo_t Asic_Mgh80Com_Hndlr5msSpiTxAsicSeedKeyInfo_t;
typedef GdMotDrvRst_t Gd_A4935_HndlrGdMotDrvRst_t;
typedef AchLampShlsInitReadData_t Ach_InputAchLampShlsInitReadData_t;
typedef PbcVdaAi_t Ioc_InputCS1msPbcVdaAi_t;
typedef VlvMonInfoEsc_t Ioc_InputSR1msVlvMonInfoEsc_t;
typedef AwdPrnDrv_t Awd_mainAwdPrnDrv_t;
typedef IocVlvNo3Data_t Ioc_OutputSR1msIocVlvNo3Data_t;
typedef FsrMonInfo_t Ioc_InputSR1msFsrMonInfo_t;
typedef HwTrigMotInfo_t AdcIf_Conv50usHwTrigMotInfo_t;
typedef AcmVsoInitWriteData_t Acm_MainAcmVsoInitWriteData_t;
typedef AcmLampShlsInitWriteData_t Acm_MainAcmLampShlsInitWriteData_t;
typedef AsicTempMon_t Ioc_InputSR1msAsicTempMon_t;
typedef AcmMotorInitWriteData_t Acm_MainAcmMotorInitWriteData_t;
typedef FspCbsMon_t Ioc_InputSR1msFspCbsMon_t;
typedef FspCbsHMon_t Ioc_InputSR1msFspCbsHMon_t;
typedef FaultStsVlvdInfo_t Vlvd_A3944_HndlrFaultStsVlvdInfo_t;
typedef SwTrigVlvInfoEsc_t AdcIfEsc_Conv1msSwTrigVlvInfoEsc_t;
typedef RxVlvdInfo_t Spim_Rx1msRxVlvdInfo_t;
typedef IocVlvNc1Data_t Ioc_OutputSR1msIocVlvNc1Data_t;
typedef PedlPwrMonInfo_t Ioc_InputSR1msPedlPwrMonInfo_t;
typedef AchWatchdogInitReadData_t Ach_InputAchWatchdogInitReadData_t;
typedef IocVlvNc2Data_t Ioc_OutputSR1msIocVlvNc2Data_t;
typedef WhlVlvIvMonInfo_t Ioc_InputSR1msWhlVlvIvMonInfo_t;
typedef SpiTxAsicActInfo_t Ioc_OutputSR1msSpiTxAsicActInfo_t;
typedef AchWssPort1AsicInfo_t Ach_InputAchWssPort1AsicInfo_t;
typedef MotMonInfoEsc_t Ioc_InputSR1msMotMonInfoEsc_t;
typedef MotVoltsMonInfo_t Ioc_InputSR1msMotVoltsMonInfo_t;
typedef RxMpsInfo_t Spim_Rx1msRxMpsInfo_t;
typedef GdMotDrvEn_t Gd_A4935_HndlrGdMotDrvEn_t;
typedef IocVlvNo1Data_t Ioc_OutputSR1msIocVlvNo1Data_t;
typedef SetDirection_t Gd_A4935_HndlrSetDirection_t;
typedef AchValveInitReadData_t Ach_InputAchValveInitReadData_t;
typedef RisngTiStampInfo_t Icu_InputCaptureRisngTiStampInfo_t;
typedef SwTrigFsrInfo_t AdcIf_Conv1msSwTrigFsrInfo_t;
typedef ReservedMonInfoEsc_t Ioc_InputSR1msReservedMonInfoEsc_t;
typedef SwTrigVlvInfo_t AdcIf_Conv1msSwTrigVlvInfo_t;
typedef AchValveAsicInfo_t Ach_InputAchValveAsicInfo_t;
typedef SwTrigMotInfoEsc_t AdcIfEsc_Conv1msSwTrigMotInfoEsc_t;
typedef CEMon_t Ioc_InputSR1msCEMon_t;
typedef AchMotorInitReadData_t Ach_InputAchMotorInitReadData_t;
typedef SwtMonInfo_t Ioc_InputCS1msSwtMonInfo_t;
typedef AchVsoSelAsicInfo_t Ach_InputAchVsoSelAsicInfo_t;
typedef AwdWatchdogAnswerData_t Awd_mainAwdWatchdogAnswerData_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* HAL_DATAHANDLE_TYPES_H_ */
/** @} */
