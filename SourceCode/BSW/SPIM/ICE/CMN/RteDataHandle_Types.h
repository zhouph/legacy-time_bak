/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef RTE_DATAHANDLE_TYPES_H_
#define RTE_DATAHANDLE_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef PCtrlSt_t Pct_CtrlPCtrlSt_t;
typedef CircPOffsCorrdInfo_t Spc_5msCtrlCircPOffsCorrdInfo_t;
typedef MotICtrlFadeOutState_t Mcc_1msCtrlMotICtrlFadeOutState_t;
typedef IdbCircVlv1CtrlStInfo_t Pct_CtrlIdbCircVlv1CtrlStInfo_t;
typedef FinalTarPInfo_t Arb_CtrlFinalTarPInfo_t;
typedef PCtrlBoostMod_t Pct_CtrlPCtrlBoostMod_t;
typedef FinalTarPRateInfo_t Arb_CtrlFinalTarPRateInfo_t;
typedef CanTxInfo_t Abc_CtrlCanTxInfo_t;
typedef IdbMotPosnInfo_t Mcc_1msCtrlIdbMotPosnInfo_t;
typedef StkRecvryCtrlIfInfo_t Abc_CtrlStkRecvryCtrlIfInfo_t;
typedef VehSpd_t Abc_CtrlVehSpd_t;
typedef RgnBrkCoopWithAbsInfo_t Rbc_CtrlRgnBrkCoopWithAbsInfo_t;
typedef PedlTrvlRateInfo_t Det_5msCtrlPedlTrvlRateInfo_t;
typedef SccCtrlInfo_t Abc_CtrlSccCtrlInfo_t;
typedef BrkPedlStatusInfo_t Det_5msCtrlBrkPedlStatusInfo_t;
typedef PedlSimrPOffsCorrdInfo_t Spc_5msCtrlPedlSimrPOffsCorrdInfo_t;
typedef Ay_t Abc_CtrlAy_t;
typedef VlvActtnReqByBaseBrkCtrlrInfo_t Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t;
typedef PedlTrvlTagInfo_t Det_5msCtrlPedlTrvlTagInfo_t;
typedef RgnBrkCtrlrActStFlg_t Rbc_CtrlRgnBrkCtrlrActStFlg_t;
typedef PistPRate1msInfo_t Det_1msCtrlPistPRate1msInfo_t;
typedef IdbSimrVlvCtrlStInfo_t Pct_CtrlIdbSimrVlvCtrlStInfo_t;
typedef RgnBrkCtlrBlendgFlg_t Rbc_CtrlRgnBrkCtlrBlendgFlg_t;
typedef VehSpdFild_t Det_5msCtrlVehSpdFild_t;
typedef PistPFild1msInfo_t Spc_1msCtrlPistPFild1msInfo_t;
typedef TvbbCtrlInfo_t Abc_CtrlTvbbCtrlInfo_t;
typedef FunctionLamp_t Abc_CtrlFunctionLamp_t;
typedef BaseBrkCtrlrActFlg_t Bbc_CtrlBaseBrkCtrlrActFlg_t;
typedef WhlSpdFildInfo_t Spc_5msCtrlWhlSpdFildInfo_t;
typedef TarRgnBrkTqInfo_t Rbc_CtrlTarRgnBrkTqInfo_t;
typedef StkRecvryActnIfInfo_t Pct_CtrlStkRecvryActnIfInfo_t;
typedef VdcLinkFild_t Mcc_1msCtrlVdcLinkFild_t;
typedef PedlTrvlFildInfo_t Spc_5msCtrlPedlTrvlFildInfo_t;
typedef EbdCtrlInfo_t Abc_CtrlEbdCtrlInfo_t;
typedef WhlVlvReqAbcInfo_t Abc_CtrlWhlVlvReqAbcInfo_t;
typedef PistPFildInfo_t Spc_5msCtrlPistPFildInfo_t;
typedef BaCtrlInfo_t Abc_CtrlBaCtrlInfo_t;
typedef PedlTrvlFinal_t Det_5msCtrlPedlTrvlFinal_t;
typedef SeldMapOfBrkPedlToBrkP_t Arb_CtrlSeldMapOfBrkPedlToBrkP_t;
typedef BaseBrkCtrlModInfo_t Bbc_CtrlBaseBrkCtrlModInfo_t;
typedef ActvBrkCtrlrActFlg_t Abc_CtrlActvBrkCtrlrActFlg_t;
typedef BrkPRednForBaseBrkCtrlr_t Rbc_CtrlBrkPRednForBaseBrkCtrlr_t;
typedef PedlTrvlOffsCorrdInfo_t Spc_5msCtrlPedlTrvlOffsCorrdInfo_t;
typedef TarPFromBrkPedl_t Bbc_CtrlTarPFromBrkPedl_t;
typedef AbsCtrlInfo_t Abc_CtrlAbsCtrlInfo_t;
typedef IdbCutVlv1CtrlStInfo_t Pct_CtrlIdbCutVlv1CtrlStInfo_t;
typedef TarDeltaStk_t Pct_CtrlTarDeltaStk_t;
typedef PCtrlFadeoutSt_t Pct_CtrlPCtrlFadeoutSt_t;
typedef VehStopStInfo_t Det_5msCtrlVehStopStInfo_t;
typedef IdbCircVlv2CtrlStInfo_t Pct_CtrlIdbCircVlv2CtrlStInfo_t;
typedef IdbCutVlv2CtrlStInfo_t Pct_CtrlIdbCutVlv2CtrlStInfo_t;
typedef NormVlvReqVlvActInfo_t Vat_CtrlNormVlvReqVlvActInfo_t;
typedef TarPFromBaseBrkCtrlr_t Bbc_CtrlTarPFromBaseBrkCtrlr_t;
typedef PistPRateInfo_t Det_5msCtrlPistPRateInfo_t;
typedef PistPOffsCorrdInfo_t Spc_5msCtrlPistPOffsCorrdInfo_t;
typedef CircPFild1msInfo_t Spc_1msCtrlCircPFild1msInfo_t;
typedef AvhCtrlInfo_t Abc_CtrlAvhCtrlInfo_t;
typedef TcsCtrlInfo_t Abc_CtrlTcsCtrlInfo_t;
typedef BrkPedlStatus1msInfo_t Det_1msCtrlBrkPedlStatus1msInfo_t;
typedef CircPFildInfo_t Spc_5msCtrlCircPFildInfo_t;
typedef FastBrkPRespRqrdModInfo_t Arb_CtrlFastBrkPRespRqrdModInfo_t;
typedef EbpCtrlInfo_t Abc_CtrlEbpCtrlInfo_t;
typedef EscCtrlInfo_t Abc_CtrlEscCtrlInfo_t;
typedef MotDqIRefMccInfo_t Mcc_1msCtrlMotDqIRefMccInfo_t;
typedef PedlTrvlFild1msInfo_t Spc_1msCtrlPedlTrvlFild1msInfo_t;
typedef PCtrlAct_t Arb_CtrlPCtrlAct_t;
typedef CircPRate1msInfo_t Det_1msCtrlCircPRate1msInfo_t;
typedef FlexBrkSwtFild_t Spc_5msCtrlFlexBrkSwtFild_t;
typedef InitQuickBrkDctFlg_t Pct_CtrlInitQuickBrkDctFlg_t;
typedef HsaCtrlInfo_t Abc_CtrlHsaCtrlInfo_t;
typedef CircPRateInfo_t Det_5msCtrlCircPRateInfo_t;
typedef HdcCtrlInfo_t Abc_CtrlHdcCtrlInfo_t;
typedef StkRecvryStabnEndOK_t Mcc_1msCtrlStkRecvryStabnEndOK_t;
typedef EpbiCtrlInfo_t Abc_CtrlEpbiCtrlInfo_t;
typedef EstimdWhlPInfo_t Det_5msCtrlEstimdWhlPInfo_t;
typedef BlsFild_t Spc_5msCtrlBlsFild_t;
typedef MotCtrlMode_t Mcc_5msCtrlMotCtrlMode_t;
typedef PedlSimrPFildInfo_t Spc_5msCtrlPedlSimrPFildInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RTE_DATAHANDLE_TYPES_H_ */
/** @} */
