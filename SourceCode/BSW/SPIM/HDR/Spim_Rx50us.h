/**
 * @defgroup Spim_Rx50us Spim_Rx50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx50us.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_RX50US_H_
#define SPIM_RX50US_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Types.h"
#include "Spim_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SPIM_RX50US_MODULE_ID      (0)
 #define SPIM_RX50US_MAJOR_VERSION  (2)
 #define SPIM_RX50US_MINOR_VERSION  (0)
 #define SPIM_RX50US_PATCH_VERSION  (0)
 #define SPIM_RX50US_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Spim_Rx50us_HdrBusType Spim_Rx50usBus;
extern Spim_Rx50us_HdrBusType Spim_Rx50usBusOld;

/* Version Info */
extern SwcVersionInfo_t Spim_Rx50usVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Spim_Rx50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Rx50usFuncInhibitSpimSts;

/* Output Data Element */
extern Spim_Rx50usRxMpsAngleInfo_t Spim_Rx50usRxMpsAngleInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Spim_Rx50us_Init(void);
extern void Spim_Rx50us(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_RX50US_H_ */
/** @} */
