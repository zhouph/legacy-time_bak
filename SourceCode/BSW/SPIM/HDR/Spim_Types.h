/**
 * @defgroup Spim_Types Spim_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_TYPES_H_
#define SPIM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Spim_Rx50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Rx50usFuncInhibitSpimSts;

/* Output Data Element */
    Spim_Rx50usRxMpsAngleInfo_t Spim_Rx50usRxMpsAngleInfo;
}Spim_Rx50us_HdrBusType;

typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Spim_Rx1msEcuModeSts;
    Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Rx1msFuncInhibitSpimSts;

/* Output Data Element */
    Spim_Rx1msRxAsicInfo_t Spim_Rx1msRxAsicInfo;
    Spim_Rx1msRxMpsInfo_t Spim_Rx1msRxMpsInfo;
    Spim_Rx1msRxVlvdInfo_t Spim_Rx1msRxVlvdInfo;
    Spim_Rx1msRxRegInfo_t Spim_Rx1msRxRegInfo;
    Spim_Rx1msRxAsicPhyInfo_t Spim_Rx1msRxAsicPhyInfo;
    Spim_Rx1msRxMgdInfo_t Spim_Rx1msRxMgdInfo;
    Spim_Rx1msRxMpsD1AngInfo_t Spim_Rx1msRxMpsD1AngInfo;
    Spim_Rx1msRxMpsD1SafeWrdInfo_t Spim_Rx1msRxMpsD1SafeWrdInfo;
    Spim_Rx1msRxMpsD1StatusInfo_t Spim_Rx1msRxMpsD1StatusInfo;
    Spim_Rx1msRxMpsD2AngInfo_t Spim_Rx1msRxMpsD2AngInfo;
    Spim_Rx1msRxMpsD2SafeWrdInfo_t Spim_Rx1msRxMpsD2SafeWrdInfo;
    Spim_Rx1msRxMpsD2StatusInfo_t Spim_Rx1msRxMpsD2StatusInfo;
    Spim_Rx1msRxVlvdFFInfo_t Spim_Rx1msRxVlvdFFInfo;
}Spim_Rx1ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Asic_Mgh80Com_Hndlr1msSpiTxAsicInfo_t Spim_Tx1msSpiTxAsicInfo;
    Asic_Mgh80Com_Hndlr5msSpiTxAsicSeedKeyInfo_t Spim_Tx1msSpiTxAsicSeedKeyInfo;
    Ioc_OutputSR1msSpiTxAsicActInfo_t Spim_Tx1msSpiTxAsicActInfo;
    Mom_HndlrEcuModeSts_t Spim_Tx1msEcuModeSts;
    Asic_Mgh80Com_Hndlr1msAsicSchTblNum_t Spim_Tx1msAsicSchTblNum;
    Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Tx1msFuncInhibitSpimSts;

/* Output Data Element */
}Spim_Tx1ms_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_TYPES_H_ */
/** @} */
