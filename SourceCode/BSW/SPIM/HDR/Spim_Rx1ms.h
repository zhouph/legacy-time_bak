/**
 * @defgroup Spim_Rx1ms Spim_Rx1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Rx1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_RX1MS_H_
#define SPIM_RX1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Types.h"
#include "Spim_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SPIM_RX1MS_MODULE_ID      (0)
 #define SPIM_RX1MS_MAJOR_VERSION  (2)
 #define SPIM_RX1MS_MINOR_VERSION  (0)
 #define SPIM_RX1MS_PATCH_VERSION  (0)
 #define SPIM_RX1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Spim_Rx1ms_HdrBusType Spim_Rx1msBus;
extern Spim_Rx1ms_HdrBusType Spim_Rx1msBusOld;

/* Version Info */
extern SwcVersionInfo_t Spim_Rx1msVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Spim_Rx1msEcuModeSts;
extern Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Rx1msFuncInhibitSpimSts;

/* Output Data Element */
extern Spim_Rx1msRxAsicInfo_t Spim_Rx1msRxAsicInfo;
extern Spim_Rx1msRxMpsInfo_t Spim_Rx1msRxMpsInfo;
extern Spim_Rx1msRxVlvdInfo_t Spim_Rx1msRxVlvdInfo;
extern Spim_Rx1msRxRegInfo_t Spim_Rx1msRxRegInfo;
extern Spim_Rx1msRxAsicPhyInfo_t Spim_Rx1msRxAsicPhyInfo;
extern Spim_Rx1msRxMgdInfo_t Spim_Rx1msRxMgdInfo;
extern Spim_Rx1msRxMpsD1AngInfo_t Spim_Rx1msRxMpsD1AngInfo;
extern Spim_Rx1msRxMpsD1SafeWrdInfo_t Spim_Rx1msRxMpsD1SafeWrdInfo;
extern Spim_Rx1msRxMpsD1StatusInfo_t Spim_Rx1msRxMpsD1StatusInfo;
extern Spim_Rx1msRxMpsD2AngInfo_t Spim_Rx1msRxMpsD2AngInfo;
extern Spim_Rx1msRxMpsD2SafeWrdInfo_t Spim_Rx1msRxMpsD2SafeWrdInfo;
extern Spim_Rx1msRxMpsD2StatusInfo_t Spim_Rx1msRxMpsD2StatusInfo;
extern Spim_Rx1msRxVlvdFFInfo_t Spim_Rx1msRxVlvdFFInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Spim_Rx1ms_Init(void);
extern void Spim_Rx1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_RX1MS_H_ */
/** @} */
