/**
 * @defgroup Spim_Tx1ms Spim_Tx1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Tx1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_TX1MS_H_
#define SPIM_TX1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Types.h"
#include "Spim_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SPIM_TX1MS_MODULE_ID      (0)
 #define SPIM_TX1MS_MAJOR_VERSION  (2)
 #define SPIM_TX1MS_MINOR_VERSION  (0)
 #define SPIM_TX1MS_PATCH_VERSION  (0)
 #define SPIM_TX1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Spim_Tx1ms_HdrBusType Spim_Tx1msBus;
extern Spim_Tx1ms_HdrBusType Spim_Tx1msBusOld;

/* Version Info */
extern SwcVersionInfo_t Spim_Tx1msVersionInfo;

/* Input Data Element */
extern Asic_Mgh80Com_Hndlr1msSpiTxAsicInfo_t Spim_Tx1msSpiTxAsicInfo;
extern Asic_Mgh80Com_Hndlr5msSpiTxAsicSeedKeyInfo_t Spim_Tx1msSpiTxAsicSeedKeyInfo;
extern Ioc_OutputSR1msSpiTxAsicActInfo_t Spim_Tx1msSpiTxAsicActInfo;
extern Mom_HndlrEcuModeSts_t Spim_Tx1msEcuModeSts;
extern Asic_Mgh80Com_Hndlr1msAsicSchTblNum_t Spim_Tx1msAsicSchTblNum;
extern Eem_SuspcDetnFuncInhibitSpimSts_t Spim_Tx1msFuncInhibitSpimSts;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Spim_Tx1ms_Init(void);
extern void Spim_Tx1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_TX1MS_H_ */
/** @} */
