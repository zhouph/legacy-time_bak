/**
 * @defgroup Spim_Sch_Types Spim_Sch_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Sch_Types.h
 * @brief       Template file
 * @date        2014. 11. 17.
 ******************************************************************************/

#ifndef SPIM_SCH_TYPES_H_
#define SPIM_SCH_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define SPIM_SCH_PATH_BSWINCLUDE( _FILENAME_ ) STRINGIFY(../../include/_FILENAME_)
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
/* Temprary */
 #include "Spim_Cdd_Types.h"
 #include "Spim_If_Types.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #ifndef NULL
  #define NULL 0L
 #endif /* NULL */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
  /* Schedule Entity Number Type */
  typedef unsigned char Spim_Sch_ScheduleEntityNumType;

  /* Schedule Table Number Type */
  typedef unsigned char Spim_Sch_ScheduleTableNumType;

  /* Schedule Table Sequence and Entry Types */
  typedef uint8 Spim_Sch_SequenceNumType;
  typedef uint16 Spim_Sch_EntryNumType;

  /* Spi Buffer Type */
  typedef uint32 Spim_Sch_SpiBufType; 

  /* Spi Schedule Table Entry Point Type */
  typedef enum
  {
      ScheduleEntry_StartOfSchedule = 0,
      ScheduleEntry_EndOfSchedule,
      ScheduleEntry_StartOfSequence,
      ScheduleEntry_EndOfSequence,
      ScheduleEntry_SyncTransmit,
      ScheduleEntry_AsyncTransmit    
  }Spim_Sch_EntryType;

  /* Entity Enable Type */
  typedef enum
  {
      EntityEnable_Disabled = 0,
      EntityEnable_Enabled
  }Spim_Sch_EntityEnabledType;

  /* True-False Type */
  typedef enum
  {
      TrueFalse_False = 0,
      TrueFalse_True
  }Spim_Sch_TrueFalseType;

  typedef enum 
  {
      LldTxMode_Sync = 0,
      LldTxMode_Async
  }Spim_Sch_LldTxMode;  

  typedef enum 
  {
      Mode_Periodic = 0,
      Mode_OneShot
  }Spim_Sch_ModeType;
  /* Spi Schedule Table Type */
  typedef struct
  {
      Spim_Sch_EntryType      EntryType;
      Spim_Cdd_ChannelType    SpiChannel;
      Spim_Cdd_SequenceType   SpiSequence;
      Spim_If_FrameIdType     TxFrameId;  
      Spim_If_FrameIdType     RxFrameId;  
  }Spim_Sch_ScheduleTableType;

  /* Spi Address Table Data Type */
  typedef Spim_Sch_ScheduleTableType* Spim_Sch_AddressTableType;
  typedef Spim_Sch_AddressTableType* Spim_Sch_EntityTableType;

  typedef uint8 Spim_Sch_SequenceType;
  typedef uint8 Spim_Sch_ChannelType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_SCH_TYPES_H_ */
/** @} */
