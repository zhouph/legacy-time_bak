/**
 * @defgroup Spim_Cdd_Types Spim_Cdd_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_Cdd_Types.h
 * @brief       Template file
 * @date        2014. 11. 14.
 ******************************************************************************/

#ifndef SPIM_CDD_TYPES_H_
#define SPIM_CDD_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define SPIM_CDD_PATH_BSWINCLUDE( _FILENAME_ ) STRINGIFY(../../include/_FILENAME_)
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Dma.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
 /* Spi Channel Type */
  typedef uint8 Spim_Cdd_ChannelType;

  /* Spi Sequence Type */
  typedef uint8 Spim_Cdd_SequenceType;

  /* Baudrate Parameter Type */
  typedef uint8 Spim_Cdd_BaudRateParamType;

  /* Delay Parameter Type */
  typedef uint8 Spim_Cdd_DelayParamType;

  /* Spi Transmission Buffer Data Type */
  typedef uint8 Spim_Cdd_DataType;

  /* Spi Tranmission Length Data Type */
  typedef uint16 Spim_Cdd_NumberOfDataType;

  /* Data Width(bit) Type */
  typedef uint8 Spim_Cdd_DataWidthType;

  /* Spi Cs Drive Type */
  typedef enum
  {
      CsDrvType_ContinuousData = 0,
      CsDrvType_ShortData,
      CsDrvType_MaxNum
  }Spim_Cdd_CsDrvType;

  /* Transfer Start Type */
  typedef enum
  {
      TransferStart_LSB = 0,
      TransferStart_MSB,
      TransferStart_MaxNum
  }Spim_Cdd_TransferStartType;

  /* Chip Select Polarity Type */
  typedef enum
  {
      CsPol_LowActive = 0,
      CsPol_HighActive,
      CsPol_MaxNum
  }Spim_Cdd_CsPolType;

  /* Clock Idle Polarity Type */
  typedef enum
  {
      ClockIdlePol_Low = 0,
      ClockIdlePol_High,
      ClockIdlePol_MaxNum
  }Spim_Cdd_ClockIdlePolType;

  /* Clock Phase Type */
  typedef enum
  {
      ClockPhase_Disabled = 0,
      ClockPhase_Enabled,
      ClockPhase_MaxNum
  }Spim_Cdd_ClockPhaseType;

  /* QSPI Hardware Channel Type */
  typedef enum
  {
      HwChannelType_QspiCh0 = 0,
      HwChannelType_QspiCh1,
      HwChannelType_QspiCh2,
      HwChannelType_QspiCh3,
      HwChannelType_MaxNum
  }Spim_Cdd_HwChannelType;

  typedef enum
  {
      PortInputType_A = 0,
      PortInputType_B,
      PortInputType_C,
      PortInputType_D,
      PortInputType_E,
      PortInputType_F,
      PortInputType_G,
      PortInputType_H
  }Spim_Cdd_PortInputType;

  /* Hardware Chip Select Type */
  typedef enum
  {
      HwCs_Ch0 = 0,
      HwCs_Ch1,
      HwCs_Ch2,
      HwCs_Ch3,
      HwCs_Ch4,
      HwCs_Ch5,
      HwCs_Ch6,
      HwCs_Ch7,
      HwCs_Ch8,
      HwCs_Ch9,
      HwCs_Ch10,
      HwCs_Ch11,
      HwCs_Ch12,
      HwCs_Ch13,
      HwCs_Ch14,
      HwCs_Ch15,
      HwCs_MaxNum
  }Spim_Cdd_HwCsType;

  /* Spi Driver Channel Configuration Type */
  typedef struct
  {
      Spim_Cdd_CsDrvType            CsDrv;
      Spim_Cdd_DataWidthType        DataWidth;
      Spim_Cdd_TransferStartType    TransferStart;
      Spim_Cdd_CsPolType            CsPol;
      Spim_Cdd_ClockIdlePolType     Pol;
      Spim_Cdd_ClockPhaseType       Pha;
      Spim_Cdd_HwChannelType        HwChannel;
      Spim_Cdd_PortInputType        PortInput;
      Spim_Cdd_HwCsType             CsPin;
      Spim_Cdd_BaudRateParamType    TQ;
      Spim_Cdd_BaudRateParamType    Q;
      Spim_Cdd_BaudRateParamType    A;
      Spim_Cdd_BaudRateParamType    B;
      Spim_Cdd_BaudRateParamType    C;
      Spim_Cdd_DelayParamType       IPRE;
      Spim_Cdd_DelayParamType       IDLE;
      Spim_Cdd_DelayParamType       LPRE;
      Spim_Cdd_DelayParamType       LEAD;
      Spim_Cdd_DelayParamType       TPRE;
      Spim_Cdd_DelayParamType       TRAIL;
      Dma_ChType                  DmaChTx;
      Dma_ChType                  DmaChRx;
  }Spim_Cdd_ChannelConfigType;

  /* Spi Driver Channel EB Config Type */
  typedef struct
  {
      Spim_Cdd_DataType           *srcAddr;
      Spim_Cdd_DataType           *desAddr;
      Spim_Cdd_NumberOfDataType    len;
  }Spim_Cdd_ChannelEBConfigType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_CDD_TYPES_H_ */
/** @} */
