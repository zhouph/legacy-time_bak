/**
 * @defgroup Spim_MemMap Spim_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPIM_MEMMAP_H_
#define SPIM_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (SPIM_START_SEC_CODE)
  #undef SPIM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_CODE)
  #undef SPIM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_CODE_STARTED
    #error "SPIM_SEC_CODE not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (SPIM_START_SEC_CONST_UNSPECIFIED)
  #undef SPIM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_CONST_UNSPECIFIED)
  #undef SPIM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_CONST_UNSPECIFIED_STARTED
    #error "SPIM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (SPIM_START_SEC_VAR_UNSPECIFIED)
  #undef SPIM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_VAR_UNSPECIFIED)
  #undef SPIM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_VAR_UNSPECIFIED_STARTED
    #error "SPIM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_START_SEC_VAR_32BIT)
  #undef SPIM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_VAR_32BIT)
  #undef SPIM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_VAR_32BIT_STARTED
    #error "SPIM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "SPIM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_START_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "SPIM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (SPIM_START_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_SEC_STARTED
    #error "SPIM section not closed"
  #endif
  #define CHK_SPIM_SEC_STARTED
  #define CHK_SPIM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "SPIM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_SEC_STARTED
  #undef CHK_SPIM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (SPIM_RX50US_START_SEC_CODE)
  #undef SPIM_RX50US_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_CODE)
  #undef SPIM_RX50US_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_CODE_STARTED
    #error "SPIM_RX50US_SEC_CODE not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (SPIM_RX50US_START_SEC_CONST_UNSPECIFIED)
  #undef SPIM_RX50US_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_CONST_UNSPECIFIED)
  #undef SPIM_RX50US_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_CONST_UNSPECIFIED_STARTED
    #error "SPIM_RX50US_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (SPIM_RX50US_START_SEC_VAR_UNSPECIFIED)
  #undef SPIM_RX50US_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_VAR_UNSPECIFIED)
  #undef SPIM_RX50US_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_VAR_UNSPECIFIED_STARTED
    #error "SPIM_RX50US_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_START_SEC_VAR_32BIT)
  #undef SPIM_RX50US_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_VAR_32BIT)
  #undef SPIM_RX50US_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_VAR_32BIT_STARTED
    #error "SPIM_RX50US_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_RX50US_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_RX50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "SPIM_RX50US_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_START_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_RX50US_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_RX50US_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_VAR_NOINIT_32BIT_STARTED
    #error "SPIM_RX50US_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (SPIM_RX50US_START_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_RX50US_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX50US_SEC_STARTED
    #error "SPIM_RX50US section not closed"
  #endif
  #define CHK_SPIM_RX50US_SEC_STARTED
  #define CHK_SPIM_RX50US_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX50US_STOP_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_RX50US_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX50US_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "SPIM_RX50US_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX50US_SEC_STARTED
  #undef CHK_SPIM_RX50US_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (SPIM_RX1MS_START_SEC_CODE)
  #undef SPIM_RX1MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_CODE)
  #undef SPIM_RX1MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_CODE_STARTED
    #error "SPIM_RX1MS_SEC_CODE not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (SPIM_RX1MS_START_SEC_CONST_UNSPECIFIED)
  #undef SPIM_RX1MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef SPIM_RX1MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "SPIM_RX1MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (SPIM_RX1MS_START_SEC_VAR_UNSPECIFIED)
  #undef SPIM_RX1MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef SPIM_RX1MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "SPIM_RX1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_START_SEC_VAR_32BIT)
  #undef SPIM_RX1MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_VAR_32BIT)
  #undef SPIM_RX1MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_VAR_32BIT_STARTED
    #error "SPIM_RX1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_RX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_RX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "SPIM_RX1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_START_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_RX1MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_RX1MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "SPIM_RX1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (SPIM_RX1MS_START_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_RX1MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_RX1MS_SEC_STARTED
    #error "SPIM_RX1MS section not closed"
  #endif
  #define CHK_SPIM_RX1MS_SEC_STARTED
  #define CHK_SPIM_RX1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_RX1MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_RX1MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_RX1MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "SPIM_RX1MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_RX1MS_SEC_STARTED
  #undef CHK_SPIM_RX1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (SPIM_TX1MS_START_SEC_CODE)
  #undef SPIM_TX1MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_CODE)
  #undef SPIM_TX1MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_CODE_STARTED
    #error "SPIM_TX1MS_SEC_CODE not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (SPIM_TX1MS_START_SEC_CONST_UNSPECIFIED)
  #undef SPIM_TX1MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef SPIM_TX1MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "SPIM_TX1MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (SPIM_TX1MS_START_SEC_VAR_UNSPECIFIED)
  #undef SPIM_TX1MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef SPIM_TX1MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "SPIM_TX1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_START_SEC_VAR_32BIT)
  #undef SPIM_TX1MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_VAR_32BIT)
  #undef SPIM_TX1MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_VAR_32BIT_STARTED
    #error "SPIM_TX1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_TX1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef SPIM_TX1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "SPIM_TX1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_START_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_TX1MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef SPIM_TX1MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "SPIM_TX1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (SPIM_TX1MS_START_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_TX1MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_SPIM_TX1MS_SEC_STARTED
    #error "SPIM_TX1MS section not closed"
  #endif
  #define CHK_SPIM_TX1MS_SEC_STARTED
  #define CHK_SPIM_TX1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (SPIM_TX1MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef SPIM_TX1MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_SPIM_TX1MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "SPIM_TX1MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_SPIM_TX1MS_SEC_STARTED
  #undef CHK_SPIM_TX1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_MEMMAP_H_ */
/** @} */
