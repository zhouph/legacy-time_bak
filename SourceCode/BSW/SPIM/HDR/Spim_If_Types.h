/**
 * @defgroup Spim_If_Types Spim_If_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spim_If_Types.h
 * @brief       Template file
 * @date        2014. 11. 17.
 ******************************************************************************/

#ifndef SPIM_IF_TYPES_H_
#define SPIM_IF_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define SPIM_IF_PATH_BSWINCLUDE( _FILENAME_ ) STRINGIFY(../../include/_FILENAME_)
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
 typedef uint16 Spim_If_SignalIdType;
 typedef uint32 Spim_If_DataType;
 typedef uint16 Spim_If_FrameIdType;
 typedef uint32 Spim_If_FrameDataType;
 typedef uint8  Spim_If_SignalBitType;

 /* Signal config type structure */
 typedef struct
 {
    Spim_If_FrameIdType     FrameId;
    Spim_If_SignalBitType   StartBit;
    Spim_If_SignalBitType   BitLength;
 }Spim_If_SignalConfigType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPIM_IF_TYPES_H_ */
/** @} */
