/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  $FILENAME   : Can_17_MCanP_Cfg.h $                                       **
**                                                                            **
**  $CC VERSION : \main\dev_tc23x_as4.0.3\2 $                                **
**                                                                            **
**  DATE, TIME: 2015-01-20, 19:02:26                                      **
**                                                                            **
**  GENERATOR : Build b120625-0327                                          **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : CAN configuration generated out of ECU configuration       ** 
**                   file (Can_17_MCanP.bmd)                                  **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: No                                       **
**                                                                            **
*******************************************************************************/

/**  TRACEABILITY: [cover parentID=DS_AS_CAN047,
                   DS_NAS_PR446,DS_AS403_PR2849]                              **
**                                                                            **
**  [/cover]                                                                 **/


#ifndef CAN_17_MCANP_CFG_H
#define CAN_17_MCANP_CFG_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/*******************************************************************************
**                      Common Published Information                          **
*******************************************************************************/

/* Autosar specification version */

#define CAN_17_MCANP_AS_VERSION (403)
#define CAN_17_MCANP_AR_RELEASE_MAJOR_VERSION  (4U)
#define CAN_17_MCANP_AR_RELEASE_MINOR_VERSION  (0U)
#define CAN_17_MCANP_AR_RELEASE_REVISION_VERSION  (3U)

/* Vendor specific implementation version information */
#define CAN_17_MCANP_SW_MAJOR_VERSION  (2U)
#define CAN_17_MCANP_SW_MINOR_VERSION  (3U)
#define CAN_17_MCANP_SW_PATCH_VERSION  (0U)


/*******************************************************************************
**                     Configuration options                                  **
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/



/* Is FD enabled ? */
#define CAN_FD_ENABLE           (STD_OFF)

/* Number of Kernels available in the device */
#define CAN_NUM_KERNELS_IN_DEVICE  (1U)

/* Number of CAN Controllers available in the device */
#define CAN_NUM_CONTROLLERS_IN_DEVICE  (4U)

/* Number of CAN Controllers available in the First Kernel */
#define CAN_NUM_CONTROLLERS_IN_KERNEL1  (4U)

/* Maximum number of CAN Controllers configured in a ConfigSet */
#define CAN_NUM_CONTROLLER_MAX         (2U)

/* Maximum number of transmit hardware objects configured in a ConfigSet */
#define CAN_TX_HARDWARE_OBJECTS_MAX    (38U)

/* Maximum number of receive hardware objects configured in a ConfigSet */
#define CAN_RX_HARDWARE_OBJECTS_MAX    (27U)

/* Maximum number of Rx FIFO configurations in a ConfigSet */
#define CAN_NUM_RX_FIFO_MAX            (0U)

/* Configured Message Identifier Type */
/* STD_ON  : Only Standard Message Identifiers are configured */
/* STD_OFF : At least one Extended/Mixed Message Identifier is configured */
#define CAN_STANDARD_ID_ONLY           (STD_ON) 

/*******************************************************************************
**                      CAN General Information                               **
*******************************************************************************/

/* Configuration: CAN_17_MCANP_INSTANCE_ID
 - Specifies the InstanceId of this module instance
*/
#define CAN_17_MCANP_INSTANCE_ID                (0U)

/* Configuration: CAN_DEV_ERROR_DETECT
 - STD_ON  - DET is enabled
 - STD_OFF - DET is disabled
*/      
#define CAN_DEV_ERROR_DETECT           (STD_OFF)

/* Configuration: CAN_VERSION_INFO_API
 - STD_ON  - Can_17_MCanP_GetVersionInfo API is enabled
 - STD_OFF - Can_17_MCanP_GetVersionInfo API is disabled
*/      
#define CAN_VERSION_INFO_API           (STD_OFF)

/* Configuration: CAN_MULTIPLEXED_TRANSMISSION
 - STD_ON  - Multiplexed transmission feature is enabled
 - STD_OFF - Multiplexed transmission feature is disabled
*/      
#define CAN_MULTIPLEXED_TRANSMISSION   (STD_OFF)

/* Configuration: CAN_TIMEOUT_DURATION
 - Specifies the maximum number of loops for blocking function until 
   a timeout is raised in short term wait loops
*/
#define CAN_TIMEOUT_DURATION    (400U)

/* Configuration: CAN_PB_FIXEDADDR
 - STD_ON  - PB fixed address optimization is enabled 
 - STD_OFF - PB fixed address optimization is disabled 
*/
#define CAN_PB_FIXEDADDR               (STD_OFF)

/* Configuration: CAN_DEBUG_SUPPORT
 - STD_ON  - Debug support is enabled 
 - STD_OFF - Debug support is disabled 
*/
#define CAN_DEBUG_SUPPORT              (STD_OFF)

/* Configuration: CAN_CHANGE_BAUDRATE_API
 - STD_ON  - Can_17_MCanP_CheckBaudrate and Can_17_MCanP_ChangeBaudrate 
             APIs are enabled
 - STD_OFF - Can_17_MCanP_CheckBaudrate and Can_17_MCanP_ChangeBaudrate 
             APIs are disabled
*/
#define CAN_CHANGE_BAUDRATE_API              (STD_OFF)

/* CAN Hardware Timeout DEM */
#define CAN_E_TIMEOUT_DEM_REPORT   (CAN_DISABLE_DEM_REPORT)


/******************************************************************************/
                    /* CAN Controller Configurations */
/******************************************************************************/

/******************************************************************************/
              /* CONTROLLER_0_MULTICANPLUS_NODE0 Configuration */
/******************************************************************************/

/* CONTROLLER_0_MULTICANPLUS_NODE0 Activation 
 - STD_ON  - Controller is used
 - STD_OFF - Controller is NOT used
*/      
#define CAN_HWCONTROLLER0_ACTIVATION           (STD_ON)

/* CONTROLLER_0_MULTICANPLUS_NODE0 Transmit Confirmation Event Processing
 - CAN_INTERRUPT - Transmission is notified through interrupt mechanism
 - CAN_POLLING   - Transmission is notified through polling mechanism  
*/      
#define CAN_TX_PROCESSING_HWCONTROLLER0        (CAN_POLLING)

/* CONTROLLER_0_MULTICANPLUS_NODE0 Receive Indication Event Processing
 - CAN_INTERRUPT - Reception is notified through interrupt mechanism
 - CAN_POLLING   - Reception is notified through polling mechanism  
*/      
#define CAN_RX_PROCESSING_HWCONTROLLER0        (CAN_INTERRUPT)

/* CONTROLLER_0_MULTICANPLUS_NODE0 Wakeup Event Processing
 - CAN_INTERRUPT - Wakeup event is  notified through interrupt mechanism
 - CAN_POLLING   - Wakeup event is notified through polling mechanism  
*/      
#define CAN_WU_PROCESSING_HWCONTROLLER0        (CAN_NO_PROCESSING)

/* CONTROLLER_0_MULTICANPLUS_NODE0 Bus-Off Event Processing
 - CAN_INTERRUPT - Bus-off event notified through interrupt mechanism
 - CAN_POLLING   - Bus-off event notified through polling mechanism  
*/
#define CAN_BO_PROCESSING_HWCONTROLLER0        (CAN_INTERRUPT)

/******************************************************************************/
              /* CONTROLLER_1_MULTICANPLUS_NODE1 Configuration */
/******************************************************************************/

#define CAN_HWCONTROLLER1_ACTIVATION           (STD_OFF)

/******************************************************************************/
              /* CONTROLLER_2_MULTICANPLUS_NODE2 Configuration */
/******************************************************************************/

/* CONTROLLER_2_MULTICANPLUS_NODE2 Activation 
 - STD_ON  - Controller is used
 - STD_OFF - Controller is NOT used
*/      
#define CAN_HWCONTROLLER2_ACTIVATION           (STD_ON)

/* CONTROLLER_2_MULTICANPLUS_NODE2 Transmit Confirmation Event Processing
 - CAN_INTERRUPT - Transmission is notified through interrupt mechanism
 - CAN_POLLING   - Transmission is notified through polling mechanism  
*/      
#define CAN_TX_PROCESSING_HWCONTROLLER2        (CAN_POLLING)

/* CONTROLLER_2_MULTICANPLUS_NODE2 Receive Indication Event Processing
 - CAN_INTERRUPT - Reception is notified through interrupt mechanism
 - CAN_POLLING   - Reception is notified through polling mechanism  
*/      
#define CAN_RX_PROCESSING_HWCONTROLLER2        (CAN_INTERRUPT)

/* CONTROLLER_2_MULTICANPLUS_NODE2 Wakeup Event Processing
 - CAN_INTERRUPT - Wakeup event is  notified through interrupt mechanism
 - CAN_POLLING   - Wakeup event is notified through polling mechanism  
*/      
#define CAN_WU_PROCESSING_HWCONTROLLER2        (CAN_NO_PROCESSING)

/* CONTROLLER_2_MULTICANPLUS_NODE2 Bus-Off Event Processing
 - CAN_INTERRUPT - Bus-off event notified through interrupt mechanism
 - CAN_POLLING   - Bus-off event notified through polling mechanism  
*/
#define CAN_BO_PROCESSING_HWCONTROLLER2        (CAN_INTERRUPT)

/******************************************************************************/
              /* CONTROLLER_3_MULTICANPLUS_NODE3 Configuration */
/******************************************************************************/

#define CAN_HWCONTROLLER3_ACTIVATION           (STD_OFF)

/****************** End Of CAN Controller Configurations **********************/

/* Configuration: CAN_WAKEUP_CONFIGURED
 - STD_ON  - At least one of the CAN controllers supports wakeup
 - STD_OFF - None of the CAN controllers supports wakeup
*/      
#define CAN_WAKEUP_CONFIGURED                  (STD_OFF)

/* Configuration: CAN_LPDU_RX_CALLOUT
 - STD_ON  - L-PDU receive callout support enabled
 - STD_OFF - L-PDU receive callout support disabled
*/      
#define CAN_LPDU_RX_CALLOUT                    (STD_ON)

/*******************************************************************************
    Symbolic Name Defintions of CAN Controllers and CAN Hardware Objects
*******************************************************************************/

/*******************************************************************************
    CanConfigSet_0 -> Symbolic Name Defintions of CAN Controllers 
*******************************************************************************/

#ifdef Can_17_MCanPConf_CanController_CanController_0 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanController_CanController_0 != 0U)
    #error Can_17_MCanPConf_CanController_CanController_0 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanController_CanController_0   (0U) 
#endif /* #ifdef Can_17_MCanPConf_CanController_CanController_0 */
#ifdef Can_17_MCanPConf_CanController_CanController_1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanController_CanController_1 != 1U)
    #error Can_17_MCanPConf_CanController_CanController_1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanController_CanController_1   (1U) 
#endif /* #ifdef Can_17_MCanPConf_CanController_CanController_1 */

/*******************************************************************************
    CanConfigSet_0 -> Symbolic Name Defintions of CAN Hardware Objects 
*******************************************************************************/

#ifdef Can_17_MCanPConf_CanHardwareObject_EMS1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_EMS1 != 0U)
    #error Can_17_MCanPConf_CanHardwareObject_EMS1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_EMS1   (0U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_EMS1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_EMS2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_EMS2 != 1U)
    #error Can_17_MCanPConf_CanHardwareObject_EMS2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_EMS2   (1U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_EMS2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_EMS6 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_EMS6 != 2U)
    #error Can_17_MCanPConf_CanHardwareObject_EMS6 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_EMS6   (2U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_EMS6 */
#ifdef Can_17_MCanPConf_CanHardwareObject_TCU1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_TCU1 != 3U)
    #error Can_17_MCanPConf_CanHardwareObject_TCU1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_TCU1   (3U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_TCU1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_TCU2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_TCU2 != 4U)
    #error Can_17_MCanPConf_CanHardwareObject_TCU2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_TCU2   (4U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_TCU2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_TCU5 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_TCU5 != 5U)
    #error Can_17_MCanPConf_CanHardwareObject_TCU5 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_TCU5   (5U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_TCU5 */
#ifdef Can_17_MCanPConf_CanHardwareObject_TCU6 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_TCU6 != 6U)
    #error Can_17_MCanPConf_CanHardwareObject_TCU6 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_TCU6   (6U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_TCU6 */
#ifdef Can_17_MCanPConf_CanHardwareObject_HCU1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_HCU1 != 7U)
    #error Can_17_MCanPConf_CanHardwareObject_HCU1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_HCU1   (7U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_HCU1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_HCU2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_HCU2 != 8U)
    #error Can_17_MCanPConf_CanHardwareObject_HCU2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_HCU2   (8U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_HCU2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_HCU3 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_HCU3 != 9U)
    #error Can_17_MCanPConf_CanHardwareObject_HCU3 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_HCU3   (9U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_HCU3 */
#ifdef Can_17_MCanPConf_CanHardwareObject_HCU5 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_HCU5 != 10U)
    #error Can_17_MCanPConf_CanHardwareObject_HCU5 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_HCU5   (10U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_HCU5 */
#ifdef Can_17_MCanPConf_CanHardwareObject_MCU1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_MCU1 != 11U)
    #error Can_17_MCanPConf_CanHardwareObject_MCU1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_MCU1   (11U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_MCU1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_MCU2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_MCU2 != 12U)
    #error Can_17_MCanPConf_CanHardwareObject_MCU2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_MCU2   (12U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_MCU2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_BMS1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_BMS1 != 13U)
    #error Can_17_MCanPConf_CanHardwareObject_BMS1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_BMS1   (13U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_BMS1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_VSM2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_VSM2 != 14U)
    #error Can_17_MCanPConf_CanHardwareObject_VSM2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_VSM2   (14U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_VSM2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_SAS /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_SAS != 15U)
    #error Can_17_MCanPConf_CanHardwareObject_SAS is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_SAS   (15U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_SAS */
#ifdef Can_17_MCanPConf_CanHardwareObject_FATC1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_FATC1 != 16U)
    #error Can_17_MCanPConf_CanHardwareObject_FATC1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_FATC1   (16U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_FATC1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_CLU1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_CLU1 != 17U)
    #error Can_17_MCanPConf_CanHardwareObject_CLU1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_CLU1   (17U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_CLU1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_CGW1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_CGW1 != 18U)
    #error Can_17_MCanPConf_CanHardwareObject_CGW1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_CGW1   (18U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_CGW1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_EMS3 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_EMS3 != 19U)
    #error Can_17_MCanPConf_CanHardwareObject_EMS3 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_EMS3   (19U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_EMS3 */
#ifdef Can_17_MCanPConf_CanHardwareObject_DIAG_RX1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_DIAG_RX1 != 20U)
    #error Can_17_MCanPConf_CanHardwareObject_DIAG_RX1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_DIAG_RX1   (20U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_DIAG_RX1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_DIAG_RX2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_DIAG_RX2 != 21U)
    #error Can_17_MCanPConf_CanHardwareObject_DIAG_RX2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_DIAG_RX2   (21U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_DIAG_RX2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_CLU2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_CLU2 != 22U)
    #error Can_17_MCanPConf_CanHardwareObject_CLU2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_CLU2   (22U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_CLU2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_EPB1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_EPB1 != 23U)
    #error Can_17_MCanPConf_CanHardwareObject_EPB1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_EPB1   (23U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_EPB1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_YAW_ACC /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_YAW_ACC != 24U)
    #error Can_17_MCanPConf_CanHardwareObject_YAW_ACC is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_YAW_ACC   (24U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_YAW_ACC */
#ifdef Can_17_MCanPConf_CanHardwareObject_LONG_ACC /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LONG_ACC != 25U)
    #error Can_17_MCanPConf_CanHardwareObject_LONG_ACC is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LONG_ACC   (25U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LONG_ACC */
#ifdef Can_17_MCanPConf_CanHardwareObject_YAW_SERIAL /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_YAW_SERIAL != 26U)
    #error Can_17_MCanPConf_CanHardwareObject_YAW_SERIAL is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_YAW_SERIAL   (26U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_YAW_SERIAL */
#ifdef Can_17_MCanPConf_CanHardwareObject_TCS1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_TCS1 != 27U)
    #error Can_17_MCanPConf_CanHardwareObject_TCS1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_TCS1   (27U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_TCS1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_TCS5 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_TCS5 != 28U)
    #error Can_17_MCanPConf_CanHardwareObject_TCS5 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_TCS5   (28U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_TCS5 */
#ifdef Can_17_MCanPConf_CanHardwareObject_ESP2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_ESP2 != 29U)
    #error Can_17_MCanPConf_CanHardwareObject_ESP2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_ESP2   (29U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_ESP2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_SAS_CAL /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_SAS_CAL != 30U)
    #error Can_17_MCanPConf_CanHardwareObject_SAS_CAL is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_SAS_CAL   (30U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_SAS_CAL */
#ifdef Can_17_MCanPConf_CanHardwareObject_ESP1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_ESP1 != 31U)
    #error Can_17_MCanPConf_CanHardwareObject_ESP1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_ESP1   (31U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_ESP1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_VSM1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_VSM1 != 32U)
    #error Can_17_MCanPConf_CanHardwareObject_VSM1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_VSM1   (32U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_VSM1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_WHL_PUL /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_WHL_PUL != 33U)
    #error Can_17_MCanPConf_CanHardwareObject_WHL_PUL is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_WHL_PUL   (33U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_WHL_PUL */
#ifdef Can_17_MCanPConf_CanHardwareObject_AHB1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_AHB1 != 34U)
    #error Can_17_MCanPConf_CanHardwareObject_AHB1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_AHB1   (34U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_AHB1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_EBS1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_EBS1 != 35U)
    #error Can_17_MCanPConf_CanHardwareObject_EBS1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_EBS1   (35U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_EBS1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_WHL_SPD /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_WHL_SPD != 36U)
    #error Can_17_MCanPConf_CanHardwareObject_WHL_SPD is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_WHL_SPD   (36U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_WHL_SPD */
#ifdef Can_17_MCanPConf_CanHardwareObject_WHL_SPD_HEV /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_WHL_SPD_HEV != 37U)
    #error Can_17_MCanPConf_CanHardwareObject_WHL_SPD_HEV is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_WHL_SPD_HEV   (37U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_WHL_SPD_HEV */
#ifdef Can_17_MCanPConf_CanHardwareObject_DIAG_TX1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_DIAG_TX1 != 38U)
    #error Can_17_MCanPConf_CanHardwareObject_DIAG_TX1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_DIAG_TX1   (38U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_DIAG_TX1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_YAW_CBIT /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_YAW_CBIT != 39U)
    #error Can_17_MCanPConf_CanHardwareObject_YAW_CBIT is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_YAW_CBIT   (39U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_YAW_CBIT */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA0 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA0 != 40U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA0 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA0   (40U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA0 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA1 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA1 != 41U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA1 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA1   (41U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA1 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA2 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA2 != 42U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA2 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA2   (42U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA2 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA3 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA3 != 43U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA3 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA3   (43U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA3 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA4 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA4 != 44U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA4 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA4   (44U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA4 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA5 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA5 != 45U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA5 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA5   (45U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA5 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA6 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA6 != 46U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA6 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA6   (46U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA6 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA7 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA7 != 47U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA7 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA7   (47U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA7 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA8 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA8 != 48U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA8 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA8   (48U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA8 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA9 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA9 != 49U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA9 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA9   (49U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA9 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA10 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA10 != 50U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA10 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA10   (50U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA10 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA11 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA11 != 51U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA11 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA11   (51U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA11 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA12 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA12 != 52U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA12 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA12   (52U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA12 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA13 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA13 != 53U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA13 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA13   (53U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA13 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA14 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA14 != 54U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA14 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA14   (54U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA14 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA15 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA15 != 55U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA15 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA15   (55U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA15 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA16 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA16 != 56U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA16 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA16   (56U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA16 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA17 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA17 != 57U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA17 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA17   (57U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA17 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA18 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA18 != 58U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA18 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA18   (58U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA18 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA19 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA19 != 59U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA19 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA19   (59U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA19 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA20 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA20 != 60U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA20 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA20   (60U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA20 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA21 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA21 != 61U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA21 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA21   (61U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA21 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA22 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA22 != 62U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA22 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA22   (62U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA22 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA23 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA23 != 63U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA23 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA23   (63U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA23 */
#ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA24 /* to prevent double declaration */
  #if (Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA24 != 64U)
    #error Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA24 is already defined with a different value
  #endif  
#else
  #define Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA24   (64U) 
#endif /* #ifdef Can_17_MCanPConf_CanHardwareObject_LOGGER_DATA24 */

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/

#endif  /* CAN_17_MCANP_CFG_H */
