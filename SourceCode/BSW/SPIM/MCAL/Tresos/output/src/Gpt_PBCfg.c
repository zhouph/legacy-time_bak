/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Gpt_PBCfg.c                                                   **
**                                                                            **
**  $CC VERSION : \main\20 $                                                 **
**                                                                            **
**  DATE, TIME: 2015-01-20, 19:02:19                                         **
**                                                                            **
**  GENERATOR : Build b120625-0327                                            **
**                                                                            **
**  BSW MODULE DECRIPTION : Gpt.bmd                                           **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking                                                       **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : GPT configuration generated out of ECU configuration       **
**                 file                                                       **
**                                                                            **
**  SPECIFICATION(S) : AUTOSAR_SWS_GPT_Driver, Release AS4.0.3                **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
/* Include module header File */ 
#include "Gpt.h"

/*******************************************************************************
**                      Inclusion File Check                                  **
*******************************************************************************/


/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/



/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/


/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

#define GPT_START_SEC_CODE
#include "MemMap.h"




/* 
   Channel Notification Function Declaration 
*/
/* System1msTick_Core1 channel notification function declaration */
extern void Gpt_Notification_1msSystemISR_Core1(void);



/* System5msTick_Core1 channel notification function declaration */
extern void Gpt_Notification_5msSystemISR_Core1(void);




#define GPT_STOP_SEC_CODE
#include "MemMap.h"

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/* MISRA RULE 87 VIOLATION: Inclusion of MemMap.h in between the code can't 
   be avoided as it is required for mapping global variables, constants 
   and code
*/ 


/* Memory Mapping the configuration constant */
#define GPT_START_SEC_POSTBUILDCFG
#include "MemMap.h"
/* 
   Channel Configuration Instance GptChannelConfigSet_0
*/ 
static const Gpt_ChannelConfigType Gpt_kChannelConfig0[ ] =
{

  /* Channel Symbolic Name(ChannelId) : System1msTick_Core1 
     GTM TOM/ATOM Channel : GTM_TOM2_CHANNEL0 in GPT_MODE_CONTINUOUS
  */ 
  {
    #if (GPT_ENABLE_DISABLE_NOTIFICATION_API == STD_ON)
    &Gpt_Notification_1msSystemISR_Core1, /* Notification Function */
    #endif
    #if ( (GPT_WAKEUP_FUNCTIONALITY_API == STD_ON) \
        && (GPT_REPORT_WAKEUP_SOURCE == STD_ON) )
    0U, /* Wakeup Info */
    #endif
    /* Assigned Hardware Unit */
    GTM_TOM2_CHANNEL0,
    #if (GPT_WAKEUP_FUNCTIONALITY_API == STD_ON)
    (boolean)FALSE, /* Wakeup Capability */
    #endif
    GPT_MODE_CONTINUOUS, /* Channel Mode */
    #if (GPT_SAFETY_ENABLE == STD_ON)
    GPT_QM_SIGNAL, /* Channel Signal Type */
    #endif
    GTM_FIXED_CLOCK_1 /* Channel Clock frequency */
  },

  /* Channel Symbolic Name(ChannelId) : System_FreeRunCnt 
     GTM TOM/ATOM Channel : GTM_TOM2_CHANNEL2 in GPT_MODE_CONTINUOUS
  */ 
  {
    #if (GPT_ENABLE_DISABLE_NOTIFICATION_API == STD_ON)
    NULL_PTR, /* Notification Function */
    #endif
    #if ( (GPT_WAKEUP_FUNCTIONALITY_API == STD_ON) \
        && (GPT_REPORT_WAKEUP_SOURCE == STD_ON) )
    0U, /* Wakeup Info */
    #endif
    /* Assigned Hardware Unit */
    GTM_TOM2_CHANNEL2,
    #if (GPT_WAKEUP_FUNCTIONALITY_API == STD_ON)
    (boolean)FALSE, /* Wakeup Capability */
    #endif
    GPT_MODE_CONTINUOUS, /* Channel Mode */
    #if (GPT_SAFETY_ENABLE == STD_ON)
    GPT_QM_SIGNAL, /* Channel Signal Type */
    #endif
    GTM_FIXED_CLOCK_1 /* Channel Clock frequency */
  },

  /* Channel Symbolic Name(ChannelId) : System5msTick_Core1 
     GTM TOM/ATOM Channel : GTM_TOM2_CHANNEL15 in GPT_MODE_CONTINUOUS
  */ 
  {
    #if (GPT_ENABLE_DISABLE_NOTIFICATION_API == STD_ON)
    &Gpt_Notification_5msSystemISR_Core1, /* Notification Function */
    #endif
    #if ( (GPT_WAKEUP_FUNCTIONALITY_API == STD_ON) \
        && (GPT_REPORT_WAKEUP_SOURCE == STD_ON) )
    0U, /* Wakeup Info */
    #endif
    /* Assigned Hardware Unit */
    GTM_TOM2_CHANNEL15,
    #if (GPT_WAKEUP_FUNCTIONALITY_API == STD_ON)
    (boolean)FALSE, /* Wakeup Capability */
    #endif
    GPT_MODE_CONTINUOUS, /* Channel Mode */
    #if (GPT_SAFETY_ENABLE == STD_ON)
    GPT_QM_SIGNAL, /* Channel Signal Type */
    #endif
    GTM_FIXED_CLOCK_1 /* Channel Clock frequency */
  }
};




/* 
Configuration: Configuration  
 Definition of Gpt_ConfigType structure
*/
const Gpt_ConfigType Gpt_ConfigRoot[1] = 
{

  {  
    #if (GPT_SAFETY_ENABLE == STD_ON)
    (uint32)GPT_MODULE_ID << 16U,  
    #endif
    Gpt_kChannelConfig0,

    3U,
}
};

#define GPT_STOP_SEC_POSTBUILDCFG
#include "MemMap.h"


/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/


/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/


/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/


/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/
