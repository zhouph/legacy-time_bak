# \file
#
# \brief Mando make configuration module
#
# This file contains the implementation of the Mando make 
# configuration module.
#
# \author Mando, Advanced R&D, Korea

#################################################################


MANDO_MCAL_ROOT     ?= $(PROJECT_ROOT)\MCAL\MCAL_RC1
MANDO_TRESOS_ROOT   ?= $(PROJECT_ROOT)\MCAL\Tresos

MANDO_MCAL_MODULES  := MCAL

include $(PROJECT_ROOT)\MCAL\MCAL_RC1\MCAL\MAK\MCAL_defs.mak
include $(PROJECT_ROOT)\MCAL\MCAL_RC1\MCAL\MAK\MCAL_rules.mak

#################################################################
# Dirty glues
# To be removed
#################################################################

CC_OPT += -D_TASKING_C_TRICORE_=1
CC_OPT += -D__AHB_GEN3=1
CC_OPT += -D__ECU=2
CC_OPT += -D__ESP_PLUSE=1
CC_OPT += -D__ESP_ACC=1
CC_OPT += -D__MGH80_MOCi=0
CC_OPT += -D__MCU=2
CC_OPT += --no-warnings

CPP_OPTS += -D__AHB_GEN3=1
CPP_OPTS += -D__ECU=2
CPP_OPTS += -D__ESP_PLUSE=1
CPP_OPTS += -D__ESP_ACC=1
CPP_OPTS += -D__MGH80_MOCi=0
CPP_OPTS += -D__MCU=2


CPP_OPTS += -D_TASKING_C_TRICORE_=1

LINK_OPT += -D__CPU__=TC27X
LINK_OPT += -D__PROC_TC27X__
LINK_OPT += --core=mpe:vtc