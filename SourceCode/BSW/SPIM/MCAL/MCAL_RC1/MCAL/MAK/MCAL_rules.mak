# \file
#
# \brief MCAL
#
# This file contains the implementation of the BSW
# module MCAL.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += MCAL_src

##Stub
MCAL_src_FILES       += $(MCAL_Stub_PATH)\CanIf.c
MCAL_src_FILES       += $(MCAL_Stub_PATH)\CanIf_Cbk.c
MCAL_src_FILES       += $(MCAL_Stub_PATH)\Fee_Fcpt.c
##Adc
MCAL_src_FILES       += $(MCAL_Adc_PATH)\Ssc\Adc.c
MCAL_src_FILES       += $(MCAL_Adc_PATH)\Ssc\Adc_Calibration.c
MCAL_src_FILES       += $(MCAL_Adc_PATH)\Ssc\Adc_ConvHandle.c
MCAL_src_FILES       += $(MCAL_Adc_PATH)\Ssc\Adc_HwHandle.c
MCAL_src_FILES       += $(MCAL_Adc_PATH)\Ssc\Adc_Ver.c
##Can
MCAL_src_FILES       += $(MCAL_Can_PATH)\src\Can_17_MCanP.c
##Crc
MCAL_src_FILES       += $(MCAL_Crc_PATH)\src\Crc.c
##Dio
MCAL_src_FILES       += $(MCAL_Dio_PATH)\src\Dio.c
MCAL_src_FILES       += $(MCAL_Dio_PATH)\src\Dio_Ver.c
##Dma
MCAL_src_FILES       += $(MCAL_Dma_PATH)\src\Dma.c
##Fee
MCAL_src_FILES       += $(MCAL_Fee_PATH)\src\Fee.c
MCAL_src_FILES       += $(MCAL_Fee_PATH)\src\Fee_Ver.c
##Fls
MCAL_src_FILES       += $(MCAL_Fls_PATH)\src\Fls_17_Pmu.c
MCAL_src_FILES       += $(MCAL_Fls_PATH)\src\Fls_17_Pmu_ac.c
MCAL_src_FILES       += $(MCAL_Fls_PATH)\src\Fls_17_Pmu_Ver.c
##Gpt
MCAL_src_FILES       += $(MCAL_Gpt_PATH)\src\Gpt.c
MCAL_src_FILES       += $(MCAL_Gpt_PATH)\src\Gpt_Ver.c
##Irq
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Adc_Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Can_Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Dma_Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Fls_Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Gtm_Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Mcal_Trap.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Spi_Irq.c
MCAL_src_FILES       += $(MCAL_Irq_PATH)\src\Stm_Irq.c
##Mcu
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Mcu.c
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Gtm.c
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Gtm_Platform.c
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Mcu_Crc.c
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Mcu_Dma.c
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Mcu_Platform.c
MCAL_src_FILES       += $(MCAL_Mcu_PATH)\src\Mcu_Ver.c
##Port
MCAL_src_FILES       += $(MCAL_Port_PATH)\src\Port.c
##Pwm
MCAL_src_FILES       += $(MCAL_Pwm_PATH)\src\Pwm_17_Gtm.c
##Spi
MCAL_src_FILES       += $(MCAL_Spi_PATH)\src\Spi.c
MCAL_src_FILES       += $(MCAL_Spi_PATH)\src\Spi_Ver.c
MCAL_src_FILES       += $(MCAL_Spi_PATH)\src\SpiSlave.c
##Tricore_general
MCAL_src_FILES       += $(MANDO_MCAL_ROOT)\ssc\Tricore_general\src\Mcal.c
MCAL_src_FILES       += $(MANDO_MCAL_ROOT)\ssc\Tricore_general\src\Mcal_DmaLib.c
MCAL_src_FILES       += $(MANDO_MCAL_ROOT)\ssc\Tricore_general\src\Mcal_TcLib.c
MCAL_src_FILES       += $(MANDO_MCAL_ROOT)\ssc\Tricore_general\src\Mcal_WdgLib.c
##Tresos
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Adc_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Can_17_MCanP_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Dio_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Dma_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\EcuM_LCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\EcuM_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Fls_17_Pmu_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Gpt_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Gtm_LCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Gtm_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Mcu_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Port_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Pwm_17_Gtm_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Spi_PBCfg.c
MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\Fee_PBCfg.c
#MCAL_src_FILES       += $(MCAL_Tresos_PATH)\output\src\CanIf_Cbk.c


#################################################################
