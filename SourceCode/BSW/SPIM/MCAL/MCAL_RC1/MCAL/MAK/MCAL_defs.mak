# \file
#
# \brief MCAL
#
# This file contains the implementation of the BSW
# module MCAL.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

MCAL_CORE_PATH    := $(MANDO_MCAL_ROOT)
MCAL_Stub_PATH    := $(MCAL_CORE_PATH)\stub
MCAL_Adc_PATH     := $(MCAL_CORE_PATH)\ssc\Adc
MCAL_Can_PATH     := $(MCAL_CORE_PATH)\ssc\Can
MCAL_Crc_PATH     := $(MCAL_CORE_PATH)\ssc\Crc
MCAL_Dio_PATH     := $(MCAL_CORE_PATH)\ssc\Dio
MCAL_Dma_PATH     := $(MCAL_CORE_PATH)\ssc\Dma
MCAL_Fee_PATH     := $(MCAL_CORE_PATH)\ssc\Fee
MCAL_Fls_PATH     := $(MCAL_CORE_PATH)\ssc\Fls
MCAL_Gpt_PATH     := $(MCAL_CORE_PATH)\ssc\Gpt
MCAL_Irq_PATH     := $(MCAL_CORE_PATH)\ssc\Irq
MCAL_Mcu_PATH     := $(MCAL_CORE_PATH)\ssc\Mcu
MCAL_Port_PATH    := $(MCAL_CORE_PATH)\ssc\Port
MCAL_Pwm_PATH     := $(MCAL_CORE_PATH)\ssc\Pwm
MCAL_Spi_PATH     := $(MCAL_CORE_PATH)\ssc\Spi
MCAL_General_PATH := $(MCAL_CORE_PATH)\ssc\General
MCAL_Tresos_PATH := $(MANDO_TRESOS_ROOT)

## MCAL INCLUDE

CC_INCLUDE_PATH    += $(MCAL_Stub_PATH)
CC_INCLUDE_PATH    += $(MCAL_Adc_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Can_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Crc_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Dio_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Dma_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Fee_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Fls_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Gpt_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Irq_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Mcu_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Port_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Pwm_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_Spi_PATH)\Inc
CC_INCLUDE_PATH    += $(MCAL_General_PATH)\inc
CC_INCLUDE_PATH    += $(MCAL_General_PATH)\tricore\compiler
CC_INCLUDE_PATH    += $(MCAL_General_PATH)\tricore\inc
CC_INCLUDE_PATH    += $(MCAL_General_PATH)\tricore\inc\TC27xB
CC_INCLUDE_PATH    += $(MCAL_CORE_PATH)\ssc\Tricore_general\Inc

## TRESOS INCLUDE

CC_INCLUDE_PATH    += $(MCAL_Tresos_PATH)\output\inc
CC_INCLUDE_PATH    += $(MCAL_Tresos_PATH)\output\Demo_Aurix
