/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Irq.h $                                                    **
**                                                                           **
**  $CC VERSION : \main\48 $                                                 **
**                                                                           **
**  $DATE       : 2014-03-17 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains initisalization of interrupt priority   **
**                and interrupt category.                                    **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/

#ifndef  _IRQ_H
#define  _IRQ_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
/* Inclusion of Platform_Types.h and Compiler.h SPI070: */
#include "Std_Types.h"

/* Include Irq Configuration */
#include "Irq_Cfg.h"
#include "Mcal_Options.h"

#if (IFX_MCAL_USED == STD_ON)
#include "EcuM.h"
#endif /* (IFX_MCAL_USED == STD_ON) */

#if defined (ECUM_USES_SPI)
#include "Spi.h"
#endif

#if defined (ECUM_USES_ADC)
#include "Adc.h"
#endif

#if defined (ECUM_USES_LIN)
#include "Lin_17_AscLin.h"
#endif

#if defined (ECUM_USES_UART)
#include "Uart.h"
#endif

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
#define IRQ_DMA_AVAILABLE          (0U)
#define IRQ_DMA_USED_MCALADC       (1U)
#define IRQ_DMA_USED_MCALSPI_TX    (2U)
#define IRQ_DMA_USED_MCALSPI_RX    (3U)
#define IRQ_ASCLIN_AVAILABLE       (0U)
#define IRQ_ASCLIN_USED_MCALLIN    (1U)    
#define IRQ_ASCLIN_USED_MCALSTDLIN (2U)
#define IRQ_ASCLIN_USED_MCALUART   (3U)

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/
#if (IFX_MCAL_USED == STD_ON)
#define IRQ_START_SEC_VAR_32BIT
#include "MemMap.h"
#else
#define IFX_IRQ_START_SEC_VAR_32BIT_ASIL_B
#include "Ifx_MemMap.h"
#endif


extern uint32 Irq_InterruptHandlerVectorTable[256];

#if (IFX_MCAL_USED == STD_ON)
#define IRQ_STOP_SEC_VAR_32BIT
#include "MemMap.h"
#else
#define IFX_IRQ_STOP_SEC_VAR_32BIT_ASIL_B
#include "Ifx_MemMap.h"
#endif

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/
#if (IFX_MCAL_USED == STD_ON)
#define IRQ_START_SEC_CODE
#include "MemMap.h"
#else
#define IFX_IRQ_START_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
#endif

extern void Irq_InstallInterruptHandler (uint32 srpn, uint32 addr);

#if (IFX_MCAL_USED == STD_ON)
/*******************************************************************************
** Syntax :  void IrqAscLin_Init(void)                                        **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqAscLin_Init(void);

#if (IRQ_ASCLIN0_EXIST == STD_ON)
#if((LIN_ASCLIN0_USED == STD_ON)||(UART_ASCLIN0_USED == STD_ON) || \
    (IRQ_ASCLIN_CHANNEL0_USED == IRQ_ASCLIN_USED_MCALSTDLIN))
#if((IRQ_ASCLIN0_TX_PRIO > 0) && (IRQ_ASCLIN0_TX_CAT == IRQ_CAT1))
extern void ASCLIN0TX_ISR(void);
#endif
#if((IRQ_ASCLIN0_RX_PRIO > 0) && (IRQ_ASCLIN0_RX_CAT == IRQ_CAT1))
extern void ASCLIN0RX_ISR(void);
#endif
#if((IRQ_ASCLIN0_ERR_PRIO > 0) && (IRQ_ASCLIN0_ERR_CAT == IRQ_CAT1))
extern void ASCLIN0EX_ISR(void);
#endif
#endif
#endif /* (IRQ_ASCLIN0_EXIST == STD_ON) */

#if (IRQ_ASCLIN1_EXIST == STD_ON)
#if((LIN_ASCLIN1_USED == STD_ON)||(UART_ASCLIN1_USED == STD_ON) || \
    (IRQ_ASCLIN_CHANNEL1_USED == IRQ_ASCLIN_USED_MCALSTDLIN))
#if((IRQ_ASCLIN1_TX_PRIO > 0) && (IRQ_ASCLIN1_TX_CAT == IRQ_CAT1))
extern void ASCLIN1TX_ISR(void);
#endif
#if((IRQ_ASCLIN1_RX_PRIO > 0) && (IRQ_ASCLIN1_RX_CAT == IRQ_CAT1))
extern void ASCLIN1RX_ISR(void);
#endif
#if((IRQ_ASCLIN1_ERR_PRIO > 0) && (IRQ_ASCLIN1_ERR_CAT == IRQ_CAT1))
extern void ASCLIN1EX_ISR(void);
#endif
#endif
#endif /* (IRQ_ASCLIN1_EXIST == STD_ON) */

#if (IRQ_ASCLIN2_EXIST == STD_ON)
#if((LIN_ASCLIN2_USED == STD_ON)||(UART_ASCLIN2_USED == STD_ON) || \
    (IRQ_ASCLIN_CHANNEL2_USED == IRQ_ASCLIN_USED_MCALSTDLIN))
#if((IRQ_ASCLIN2_TX_PRIO > 0) && (IRQ_ASCLIN2_TX_CAT == IRQ_CAT1))
extern void ASCLIN2TX_ISR(void);
#endif
#if((IRQ_ASCLIN2_RX_PRIO > 0) && (IRQ_ASCLIN2_RX_CAT == IRQ_CAT1))
extern void ASCLIN2RX_ISR(void);
#endif
#if((IRQ_ASCLIN2_ERR_PRIO > 0) && (IRQ_ASCLIN2_ERR_CAT == IRQ_CAT1))
extern void ASCLIN2EX_ISR(void);
#endif
#endif
#endif /* (IRQ_ASCLIN2_EXIST == STD_ON) */

#if (IRQ_ASCLIN3_EXIST == STD_ON)
#if((LIN_ASCLIN3_USED == STD_ON)||(UART_ASCLIN3_USED == STD_ON) || \
    (IRQ_ASCLIN_CHANNEL3_USED == IRQ_ASCLIN_USED_MCALSTDLIN))
#if((IRQ_ASCLIN3_TX_PRIO > 0) && (IRQ_ASCLIN3_TX_CAT == IRQ_CAT1))
extern void ASCLIN3TX_ISR(void);
#endif
#if((IRQ_ASCLIN3_RX_PRIO > 0) && (IRQ_ASCLIN3_RX_CAT == IRQ_CAT1))
extern void ASCLIN3RX_ISR(void);
#endif
#if((IRQ_ASCLIN3_ERR_PRIO > 0) && (IRQ_ASCLIN3_ERR_CAT == IRQ_CAT1))
extern void ASCLIN3EX_ISR(void);
#endif
#endif
#endif /* (IRQ_ASCLIN3_EXIST == STD_ON) */

/*******************************************************************************
** Syntax :  void IrqCcu6_Init(void)                                          **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqCcu6_Init(void);

/*******************************************************************************
** Syntax :  void IrqCan_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqCan_Init(void);


/*******************************************************************************
** Syntax :  void IrqHsm_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
#if ((IRQ_HSM0_EXIST == STD_ON) || (IRQ_HSM1_EXIST == STD_ON))
extern void IrqHsm_Init(void);
#endif

#if (IRQ_HSM0_EXIST == STD_ON)
#if((IRQ_HSM_SR0_PRIO > 0) && (IRQ_HSM_SR0_CAT == IRQ_CAT1))
extern void HSMSR0_ISR(void);
#endif
#endif

#if (IRQ_HSM1_EXIST == STD_ON)
#if((IRQ_HSM_SR1_PRIO > 0) && (IRQ_HSM_SR1_CAT == IRQ_CAT1))
extern void HSMSR1_ISR(void);
#endif
#endif

/*******************************************************************************
** Syntax :  void IrqGpt_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqGpt_Init(void);

/*******************************************************************************
** Syntax :  void IrqGtm_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqGtm_Init(void);
#endif /* (IFX_MCAL_USED == STD_ON) */

/*******************************************************************************
** Syntax :  void IrqStm_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for STM                        **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqStm_Init(void);

#if (IFX_MCAL_USED == STD_ON)
/*******************************************************************************
** Syntax :  void IrqGpsrGroup_Init(void)                                     **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqGpsrGroup_Init(void);

/*******************************************************************************
** Syntax :  void IrqFlexray_Init(void)                                       **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for ERay                       **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqFlexray_Init(void);

/*******************************************************************************
** Syntax :  void IrqSpi_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/

extern void IrqSpi_Init(void);

/******************************************************************************
** Syntax : void QSPI4ERR_ISR(void)                                        **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for Error interrupt of QSPIx                        **
**                                                                           **
*****************************************************************************/
/******************************************************************************
** Syntax : void DMA_ISR_QSPIxTX(void)                                       **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for Transmit Dma channel interrupt used by QSPIx    **
**                                                                           **
*****************************************************************************/


/******************************************************************************
** Syntax : void DMA_ISR_QSPI0RX(void)                                       **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for Receive Dma channel interrupt used by QSPIx    **
**                                                                           **
*****************************************************************************/

#if (IRQ_QSPI0_EXIST == STD_ON)
#if ((SPI_QSPI0_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))

#if ((IRQ_QSPI0_ERR_PRIO > 0) && (IRQ_QSPI0_ERR_CAT == IRQ_CAT1)  )
extern void QSPI0ERR_ISR(void);
extern void DMA_ISR_QSPI0TX(void);
extern void DMA_ISR_QSPI0RX(void);
#endif
#endif
#endif /* (IRQ_QSPI0_EXIST == STD_ON) */

#if (IRQ_QSPI1_EXIST == STD_ON)
#if ((SPI_QSPI1_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI1_ERR_PRIO > 0) && (IRQ_QSPI1_ERR_CAT == IRQ_CAT1) )
extern void QSPI1ERR_ISR(void);
extern void DMA_ISR_QSPI1TX(void);
extern void DMA_ISR_QSPI1RX(void);
#endif
#endif
#endif /* (IRQ_QSPI1_EXIST == STD_ON) */

#if (IRQ_QSPI2_EXIST == STD_ON)
#if ((SPI_QSPI2_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI2_ERR_PRIO > 0) && (IRQ_QSPI2_ERR_CAT == IRQ_CAT1) )
extern void QSPI2ERR_ISR(void);
extern void DMA_ISR_QSPI2TX(void);
extern void DMA_ISR_QSPI2RX(void);
#endif
#endif
#endif /* (IRQ_QSPI2_EXIST == STD_ON) */

#if (IRQ_QSPI3_EXIST == STD_ON)
#if ((SPI_QSPI3_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI3_ERR_PRIO > 0) && (IRQ_QSPI3_ERR_CAT == IRQ_CAT1) )
extern void QSPI3ERR_ISR(void);
extern void DMA_ISR_QSPI3TX(void);
extern void DMA_ISR_QSPI3RX(void);
#endif
#endif
#endif /* (IRQ_QSPI3_EXIST == STD_ON) */

#if (IRQ_QSPI4_EXIST == STD_ON)
#if ((SPI_QSPI4_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI4_ERR_PRIO > 0) && (IRQ_QSPI4_ERR_CAT == IRQ_CAT1) )
extern void QSPIXERR_ISR(void);
extern void DMA_ISR_QSPI4TX(void);
extern void DMA_ISR_QSPI4RX(void);
#endif
#endif
#endif /* (IRQ_QSPI4_EXIST == STD_ON) */

#if (IRQ_QSPI5_EXIST == STD_ON)
#if ((SPI_QSPI5_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI5_ERR_PRIO > 0) && (IRQ_QSPI5_ERR_CAT == IRQ_CAT1) )
extern void QSPI5ERR_ISR(void);
extern void DMA_ISR_QSPI5TX(void);
extern void DMA_ISR_QSPI5RX(void);
#endif
#endif
#endif /* (IRQ_QSPI0_EXIST == STD_ON) */

/******************************************************************************
** Syntax : void QSPIXPT_ISR(void)                                       **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for Phase transition interrupt (PT2) used by QSPIx    **
**                                                                           **
*****************************************************************************/
#if (IRQ_QSPI0_EXIST == STD_ON)
#if ((IRQ_QSPI0_PT_PRIO > 0) && \
     (IRQ_QSPI0_PT_CAT == IRQ_CAT1) )
extern void QSPI0PT_ISR(void);
#endif
#endif /* (IRQ_QSPI0_EXIST == STD_ON) */

#if (IRQ_QSPI1_EXIST == STD_ON)
#if ((IRQ_QSPI1_PT_PRIO > 0) && \
     (IRQ_QSPI1_PT_CAT == IRQ_CAT1) )
extern void QSPI1PT_ISR(void);
#endif
#endif /* (IRQ_QSPI1_EXIST == STD_ON) */

#if (IRQ_QSPI2_EXIST == STD_ON)
#if ((IRQ_QSPI2_PT_PRIO > 0) && \
     (IRQ_QSPI2_PT_CAT == IRQ_CAT1) )
extern void QSPI2PT_ISR(void);
#endif
#endif /* (IRQ_QSPI2_EXIST == STD_ON) */

#if (IRQ_QSPI3_EXIST == STD_ON)
#if ((IRQ_QSPI3_PT_PRIO > 0) && \
     (IRQ_QSPI3_PT_CAT == IRQ_CAT1) )
extern void QSPI3PT_ISR(void);
#endif
#endif /* (IRQ_QSPI3_EXIST == STD_ON) */

#if (IRQ_QSPI4_EXIST == STD_ON)
#if ((IRQ_QSPI4_PT_PRIO > 0) && \
     (IRQ_QSPI4_PT_CAT == IRQ_CAT1) )
extern void QSPI4PT_ISR(void);
#endif
#endif /* (IRQ_QSPI4_EXIST == STD_ON) */

#if (IRQ_QSPI5_EXIST == STD_ON)
#if ((IRQ_QSPI5_PT_PRIO > 0) && \
     (IRQ_QSPI5_PT_CAT == IRQ_CAT1) && (SPI_QSPI15_USED == STD_ON))
extern void QSPI5PT_ISR(void);
#endif
#endif /* (IRQ_QSPI5_EXIST == STD_ON) */


/******************************************************************************
** Syntax : void QSPIXUD_ISR(void)                                       **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for User interrupt (PT1) used by QSPIx    **
**                                                                           **
*****************************************************************************/
#if (IRQ_QSPI0_EXIST == STD_ON)
#if ((IRQ_QSPI0_UD_PRIO > 0) && \
     (IRQ_QSPI0_UD_CAT == IRQ_CAT1) )
  extern void QSPI0UD_ISR(void);
#endif
#endif /* (IRQ_QSPI0_EXIST == STD_ON) */

#if (IRQ_QSPI1_EXIST == STD_ON)
#if ((IRQ_QSPI1_UD_PRIO > 0) && \
     (IRQ_QSPI1_UD_CAT == IRQ_CAT1) )
  extern void QSPI1UD_ISR(void);
#endif
#endif /*(IRQ_QSPI1_EXIST == STD_ON) */

#if (IRQ_QSPI2_EXIST == STD_ON)
#if ((IRQ_QSPI2_UD_PRIO > 0) && \
     (IRQ_QSPI2_UD_CAT == IRQ_CAT1) )
  extern void QSPI2UD_ISR(void);
#endif
#endif /* (IRQ_QSPI2_EXIST == STD_ON) */

#if (IRQ_QSPI3_EXIST == STD_ON)
#if ((IRQ_QSPI3_UD_PRIO > 0) && \
     (IRQ_QSPI3_UD_CAT == IRQ_CAT1) )
  extern void QSPI3UD_ISR(void);
#endif
#endif /* (IRQ_QSPI3_EXIST == STD_ON) */

#if (IRQ_QSPI4_EXIST == STD_ON)
#if ((IRQ_QSPI4_UD_PRIO > 0) && \
     (IRQ_QSPI4_UD_CAT == IRQ_CAT1) )
  extern void QSPI4UD_ISR(void);
#endif
#endif /* (IRQ_QSPI4_EXIST == STD_ON) */

#if (IRQ_QSPI5_EXIST == STD_ON)
#if ((IRQ_QSPI5_UD_PRIO > 0) && \
     (IRQ_QSPI5_UD_CAT == IRQ_CAT1) )
  extern void QSPI5UD_ISR(void);
#endif
#endif /* (IRQ_QSPI5_EXIST == STD_ON) */

/******************************************************************************
** Syntax : void GTM_ISR_<MOD>[x]_SRy(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for GTM Service request for Sub Modules             **
**                                                                           **
*****************************************************************************/
#if (IRQ_GTM_TOM0_EXIST == STD_ON)

#if((IRQ_GTM_TOM0_SR0_PRIO > 0) && (IRQ_GTM_TOM0_SR0_CAT == IRQ_CAT1))
extern void GTMTOM0SR0_ISR(void);
#endif

#if((IRQ_GTM_TOM0_SR1_PRIO > 0) && (IRQ_GTM_TOM0_SR1_CAT == IRQ_CAT1))
extern void GTMTOM0SR1_ISR(void);
#endif


#if((IRQ_GTM_TOM0_SR2_PRIO > 0) && (IRQ_GTM_TOM0_SR2_CAT == IRQ_CAT1))
extern void GTMTOM0SR2_ISR(void);
#endif

#if((IRQ_GTM_TOM0_SR3_PRIO > 0) && (IRQ_GTM_TOM0_SR3_CAT == IRQ_CAT1))
extern void GTMTOM0SR3_ISR(void);
#endif

#if((IRQ_GTM_TOM0_SR4_PRIO > 0) && (IRQ_GTM_TOM0_SR4_CAT == IRQ_CAT1))
extern void GTMTOM0SR4_ISR(void);
#endif

#if((IRQ_GTM_TOM0_SR5_PRIO > 0) && (IRQ_GTM_TOM0_SR5_CAT == IRQ_CAT1))
extern void GTMTOM0SR5_ISR(void);
#endif

#if((IRQ_GTM_TOM0_SR6_PRIO > 0) && (IRQ_GTM_TOM0_SR6_CAT == IRQ_CAT1))
extern void GTMTOM0SR6_ISR(void);
#endif

#if((IRQ_GTM_TOM0_SR7_PRIO > 0) && (IRQ_GTM_TOM0_SR7_CAT == IRQ_CAT1))
extern void GTMTOM0SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TOM0_EXIST == STD_ON) */

#if (IRQ_GTM_TOM1_EXIST == STD_ON)

#if((IRQ_GTM_TOM1_SR0_PRIO > 0) && (IRQ_GTM_TOM1_SR0_CAT == IRQ_CAT1))
extern void GTMTOM1SR0_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR1_PRIO > 0) && (IRQ_GTM_TOM1_SR1_CAT == IRQ_CAT1))
extern void GTMTOM1SR1_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR2_PRIO > 0) && (IRQ_GTM_TOM1_SR2_CAT == IRQ_CAT1))
extern void GTMTOM1SR2_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR3_PRIO > 0) && (IRQ_GTM_TOM1_SR3_CAT == IRQ_CAT1))
extern void GTMTOM1SR3_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR4_PRIO > 0) && (IRQ_GTM_TOM1_SR4_CAT == IRQ_CAT1))
extern void GTMTOM1SR4_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR5_PRIO > 0) && (IRQ_GTM_TOM1_SR5_CAT == IRQ_CAT1))
extern void GTMTOM1SR5_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR6_PRIO > 0) && (IRQ_GTM_TOM1_SR6_CAT == IRQ_CAT1))
extern void GTMTOM1SR6_ISR(void);
#endif

#if((IRQ_GTM_TOM1_SR7_PRIO > 0) && (IRQ_GTM_TOM1_SR7_CAT == IRQ_CAT1))
extern void GTMTOM1SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TOM1_EXIST == STD_ON) */

#if (IRQ_GTM_TOM2_EXIST == STD_ON)

#if((IRQ_GTM_TOM2_SR0_PRIO > 0) && (IRQ_GTM_TOM2_SR0_CAT == IRQ_CAT1))
extern void GTMTOM2SR0_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR1_PRIO > 0) && (IRQ_GTM_TOM2_SR1_CAT == IRQ_CAT1))
extern void GTMTOM2SR1_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR2_PRIO > 0) && (IRQ_GTM_TOM2_SR2_CAT == IRQ_CAT1))
extern void GTMTOM2SR2_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR3_PRIO > 0) && (IRQ_GTM_TOM2_SR3_CAT == IRQ_CAT1))
extern void GTMTOM2SR3_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR4_PRIO > 0) && (IRQ_GTM_TOM2_SR4_CAT == IRQ_CAT1))
extern void GTMTOM2SR4_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR5_PRIO > 0) && (IRQ_GTM_TOM2_SR5_CAT == IRQ_CAT1))
extern void GTMTOM2SR5_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR6_PRIO > 0) && (IRQ_GTM_TOM2_SR6_CAT == IRQ_CAT1))
extern void GTMTOM2SR6_ISR(void);
#endif

#if((IRQ_GTM_TOM2_SR7_PRIO > 0) && (IRQ_GTM_TOM2_SR7_CAT == IRQ_CAT1))
extern void GTMTOM2SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TOM2_EXIST == STD_ON)*/

#if (IRQ_GTM_TOM3_EXIST == STD_ON)

#if((IRQ_GTM_TOM3_SR0_PRIO > 0) && (IRQ_GTM_TOM3_SR0_CAT == IRQ_CAT1))
extern void GTMTOM3SR0_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR1_PRIO > 0) && (IRQ_GTM_TOM3_SR1_CAT == IRQ_CAT1))
extern void GTMTOM3SR1_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR2_PRIO > 0) && (IRQ_GTM_TOM3_SR2_CAT == IRQ_CAT1))
extern void GTMTOM3SR2_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR3_PRIO > 0) && (IRQ_GTM_TOM3_SR3_CAT == IRQ_CAT1))
extern void GTMTOM3SR3_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR4_PRIO > 0) && (IRQ_GTM_TOM3_SR4_CAT == IRQ_CAT1))
extern void GTMTOM3SR4_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR5_PRIO > 0) && (IRQ_GTM_TOM3_SR5_CAT == IRQ_CAT1))
extern void GTMTOM3SR5_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR6_PRIO > 0) && (IRQ_GTM_TOM3_SR6_CAT == IRQ_CAT1))
extern void GTMTOM3SR6_ISR(void);
#endif

#if((IRQ_GTM_TOM3_SR7_PRIO > 0) && (IRQ_GTM_TOM3_SR7_CAT == IRQ_CAT1))
extern void GTMTOM3SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TOM3_EXIST == STD_ON) */

#if (IRQ_GTM_TOM4_EXIST == STD_ON)

#if((IRQ_GTM_TOM4_SR0_PRIO > 0) && (IRQ_GTM_TOM4_SR0_CAT == IRQ_CAT1))
extern void GTMTOM4SR0_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR1_PRIO > 0) && (IRQ_GTM_TOM4_SR1_CAT == IRQ_CAT1))
extern void GTMTOM4SR1_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR2_PRIO > 0) && (IRQ_GTM_TOM4_SR2_CAT == IRQ_CAT1))
extern void GTMTOM4SR2_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR3_PRIO > 0) && (IRQ_GTM_TOM4_SR3_CAT == IRQ_CAT1))
extern void GTMTOM4SR3_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR4_PRIO > 0) && (IRQ_GTM_TOM4_SR4_CAT == IRQ_CAT1))
extern void GTMTOM4SR4_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR5_PRIO > 0) && (IRQ_GTM_TOM4_SR5_CAT == IRQ_CAT1))
extern void GTMTOM4SR5_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR6_PRIO > 0) && (IRQ_GTM_TOM4_SR6_CAT == IRQ_CAT1))
extern void GTMTOM4SR6_ISR(void);
#endif

#if((IRQ_GTM_TOM4_SR7_PRIO > 0) && (IRQ_GTM_TOM4_SR7_CAT == IRQ_CAT1))
extern void GTMTOM4SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TOM4_EXIST == STD_ON) */


#if (IRQ_GTM_ATOM0_EXIST == STD_ON)

#if((IRQ_GTM_ATOM0_SR0_PRIO > 0) && (IRQ_GTM_ATOM0_SR0_CAT == IRQ_CAT1))
extern void GTMATOM0SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM0_SR1_PRIO > 0) && (IRQ_GTM_ATOM0_SR1_CAT == IRQ_CAT1))
extern void GTMATOM0SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM0_SR2_PRIO > 0) && (IRQ_GTM_ATOM0_SR2_CAT == IRQ_CAT1))
extern void GTMATOM0SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM0_SR3_PRIO > 0) && (IRQ_GTM_ATOM0_SR3_CAT == IRQ_CAT1))
extern void GTMATOM0SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM0_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM1_EXIST == STD_ON)

#if((IRQ_GTM_ATOM1_SR0_PRIO > 0) && (IRQ_GTM_ATOM1_SR0_CAT == IRQ_CAT1))
extern void GTMATOM1SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM1_SR1_PRIO > 0) && (IRQ_GTM_ATOM1_SR1_CAT == IRQ_CAT1))
extern void GTMATOM1SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM1_SR2_PRIO > 0) && (IRQ_GTM_ATOM1_SR2_CAT == IRQ_CAT1))
extern void GTMATOM1SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM1_SR3_PRIO > 0) && (IRQ_GTM_ATOM1_SR3_CAT == IRQ_CAT1))
extern void GTMATOM1SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM1_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM2_EXIST == STD_ON)

#if((IRQ_GTM_ATOM2_SR0_PRIO > 0) && (IRQ_GTM_ATOM2_SR0_CAT == IRQ_CAT1))
extern void GTMATOM2SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM2_SR1_PRIO > 0) && (IRQ_GTM_ATOM2_SR1_CAT == IRQ_CAT1))
extern void GTMATOM2SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM2_SR2_PRIO > 0) && (IRQ_GTM_ATOM2_SR2_CAT == IRQ_CAT1))
extern void GTMATOM2SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM2_SR3_PRIO > 0) && (IRQ_GTM_ATOM2_SR3_CAT == IRQ_CAT1))
extern void GTMATOM2SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM2_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM3_EXIST == STD_ON)

#if((IRQ_GTM_ATOM3_SR0_PRIO > 0) && (IRQ_GTM_ATOM3_SR0_CAT == IRQ_CAT1))
extern void GTMATOM3SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM3_SR1_PRIO > 0) && (IRQ_GTM_ATOM3_SR1_CAT == IRQ_CAT1))
extern void GTMATOM3SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM3_SR2_PRIO > 0) && (IRQ_GTM_ATOM3_SR2_CAT == IRQ_CAT1))
extern void GTMATOM3SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM3_SR3_PRIO > 0) && (IRQ_GTM_ATOM3_SR3_CAT == IRQ_CAT1))
extern void GTMATOM3SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM3_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM4_EXIST == STD_ON)

#if((IRQ_GTM_ATOM4_SR0_PRIO > 0) && (IRQ_GTM_ATOM4_SR0_CAT == IRQ_CAT1))
extern void GTMATOM4SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM4_SR1_PRIO > 0) && (IRQ_GTM_ATOM4_SR1_CAT == IRQ_CAT1))
extern void GTMATOM4SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM4_SR2_PRIO > 0) && (IRQ_GTM_ATOM4_SR2_CAT == IRQ_CAT1))
extern void GTMATOM4SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM4_SR3_PRIO > 0) && (IRQ_GTM_ATOM4_SR3_CAT == IRQ_CAT1))
extern void GTMATOM4SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM4_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM5_EXIST == STD_ON)

#if((IRQ_GTM_ATOM5_SR0_PRIO > 0) && (IRQ_GTM_ATOM5_SR0_CAT == IRQ_CAT1))
extern void GTMATOM5SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM5_SR1_PRIO > 0) && (IRQ_GTM_ATOM5_SR1_CAT == IRQ_CAT1))
extern void GTMATOM5SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM5_SR2_PRIO > 0) && (IRQ_GTM_ATOM5_SR2_CAT == IRQ_CAT1))
extern void GTMATOM5SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM5_SR3_PRIO > 0) && (IRQ_GTM_ATOM5_SR3_CAT == IRQ_CAT1))
extern void GTMATOM5SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM5_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM6_EXIST == STD_ON)

#if((IRQ_GTM_ATOM6_SR0_PRIO > 0) && (IRQ_GTM_ATOM6_SR0_CAT == IRQ_CAT1))
extern void GTMATOM6SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM6_SR1_PRIO > 0) && (IRQ_GTM_ATOM6_SR1_CAT == IRQ_CAT1))
extern void GTMATOM6SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM6_SR2_PRIO > 0) && (IRQ_GTM_ATOM6_SR2_CAT == IRQ_CAT1))
extern void GTMATOM6SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM6_SR3_PRIO > 0) && (IRQ_GTM_ATOM6_SR3_CAT == IRQ_CAT1))
extern void GTMATOM6SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM6_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM7_EXIST == STD_ON)

#if((IRQ_GTM_ATOM7_SR0_PRIO > 0) && (IRQ_GTM_ATOM7_SR0_CAT == IRQ_CAT1))
extern void GTMATOM7SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM7_SR1_PRIO > 0) && (IRQ_GTM_ATOM7_SR1_CAT == IRQ_CAT1))
extern void GTMATOM7SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM7_SR2_PRIO > 0) && (IRQ_GTM_ATOM7_SR2_CAT == IRQ_CAT1))
extern void GTMATOM7SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM7_SR3_PRIO > 0) && (IRQ_GTM_ATOM7_SR3_CAT == IRQ_CAT1))
extern void GTMATOM7SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM7_EXIST == STD_ON) */

#if (IRQ_GTM_ATOM8_EXIST == STD_ON)

#if((IRQ_GTM_ATOM8_SR0_PRIO > 0) && (IRQ_GTM_ATOM8_SR0_CAT == IRQ_CAT1))
extern void GTMATOM8SR0_ISR(void);
#endif

#if((IRQ_GTM_ATOM8_SR1_PRIO > 0) && (IRQ_GTM_ATOM8_SR1_CAT == IRQ_CAT1))
extern void GTMATOM8SR1_ISR(void);
#endif

#if((IRQ_GTM_ATOM8_SR2_PRIO > 0) && (IRQ_GTM_ATOM8_SR2_CAT == IRQ_CAT1))
extern void GTMATOM8SR2_ISR(void);
#endif

#if((IRQ_GTM_ATOM8_SR3_PRIO > 0) && (IRQ_GTM_ATOM8_SR3_CAT == IRQ_CAT1))
extern void GTMATOM8SR3_ISR(void);
#endif

#endif /* (IRQ_GTM_ATOM8_EXIST == STD_ON) */

#if (IRQ_GTM_TIM0_EXIST == STD_ON)

#if((IRQ_GTM_TIM0_SR0_PRIO > 0) && (IRQ_GTM_TIM0_SR0_CAT == IRQ_CAT1))
extern void GTMTIM0SR0_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR1_PRIO > 0) && (IRQ_GTM_TIM0_SR1_CAT == IRQ_CAT1))
extern void GTMTIM0SR1_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR2_PRIO > 0) && (IRQ_GTM_TIM0_SR2_CAT == IRQ_CAT1))
extern void GTMTIM0SR2_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR3_PRIO > 0) && (IRQ_GTM_TIM0_SR3_CAT == IRQ_CAT1))
extern void GTMTIM0SR3_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR4_PRIO > 0) && (IRQ_GTM_TIM0_SR4_CAT == IRQ_CAT1))
extern void GTMTIM0SR4_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR5_PRIO > 0) && (IRQ_GTM_TIM0_SR5_CAT == IRQ_CAT1))
extern void GTMTIM0SR5_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR6_PRIO > 0) && (IRQ_GTM_TIM0_SR6_CAT == IRQ_CAT1))
extern void GTMTIM0SR6_ISR(void);
#endif

#if((IRQ_GTM_TIM0_SR7_PRIO > 0) && (IRQ_GTM_TIM0_SR7_CAT == IRQ_CAT1))
extern void GTMTIM0SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TIM0_EXIST == STD_ON) */

#if (IRQ_GTM_TIM1_EXIST == STD_ON)

#if((IRQ_GTM_TIM1_SR0_PRIO > 0) && (IRQ_GTM_TIM1_SR0_CAT == IRQ_CAT1))
extern void GTMTIM1SR0_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR1_PRIO > 0) && (IRQ_GTM_TIM1_SR1_CAT == IRQ_CAT1))
extern void GTMTIM1SR1_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR2_PRIO > 0) && (IRQ_GTM_TIM1_SR2_CAT == IRQ_CAT1))
extern void GTMTIM1SR2_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR3_PRIO > 0) && (IRQ_GTM_TIM1_SR3_CAT == IRQ_CAT1))
extern void GTMTIM1SR3_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR4_PRIO > 0) && (IRQ_GTM_TIM1_SR4_CAT == IRQ_CAT1))
extern void GTMTIM1SR4_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR5_PRIO > 0) && (IRQ_GTM_TIM1_SR5_CAT == IRQ_CAT1))
extern void GTMTIM1SR5_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR6_PRIO > 0) && (IRQ_GTM_TIM1_SR6_CAT == IRQ_CAT1))
extern void GTMTIM1SR6_ISR(void);
#endif

#if((IRQ_GTM_TIM1_SR7_PRIO > 0) && (IRQ_GTM_TIM1_SR7_CAT == IRQ_CAT1))
extern void GTMTIM1SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TIM1_EXIST == STD_ON) */

#if (IRQ_GTM_TIM2_EXIST == STD_ON)

#if((IRQ_GTM_TIM2_SR0_PRIO > 0) && (IRQ_GTM_TIM2_SR0_CAT == IRQ_CAT1))
extern void GTMTIM2SR0_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR1_PRIO > 0) && (IRQ_GTM_TIM2_SR1_CAT == IRQ_CAT1))
extern void GTMTIM2SR1_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR2_PRIO > 0) && (IRQ_GTM_TIM2_SR2_CAT == IRQ_CAT1))
extern void GTMTIM2SR2_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR3_PRIO > 0) && (IRQ_GTM_TIM2_SR3_CAT == IRQ_CAT1))
extern void GTMTIM2SR3_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR4_PRIO > 0) && (IRQ_GTM_TIM2_SR4_CAT == IRQ_CAT1))
extern void GTMTIM2SR4_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR5_PRIO > 0) && (IRQ_GTM_TIM2_SR5_CAT == IRQ_CAT1))
extern void GTMTIM2SR5_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR6_PRIO > 0) && (IRQ_GTM_TIM2_SR6_CAT == IRQ_CAT1))
extern void GTMTIM2SR6_ISR(void);
#endif

#if((IRQ_GTM_TIM2_SR7_PRIO > 0) && (IRQ_GTM_TIM2_SR7_CAT == IRQ_CAT1))
extern void GTMTIM2SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TIM2_EXIST == STD_ON) */

#if (IRQ_GTM_TIM3_EXIST == STD_ON)

#if((IRQ_GTM_TIM3_SR0_PRIO > 0) && (IRQ_GTM_TIM3_SR0_CAT == IRQ_CAT1))
extern void GTMTIM3SR0_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR1_PRIO > 0) && (IRQ_GTM_TIM3_SR1_CAT == IRQ_CAT1))
extern void GTMTIM3SR1_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR2_PRIO > 0) && (IRQ_GTM_TIM3_SR2_CAT == IRQ_CAT1))
extern void GTMTIM3SR2_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR3_PRIO > 0) && (IRQ_GTM_TIM3_SR3_CAT == IRQ_CAT1))
extern void GTMTIM3SR3_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR4_PRIO > 0) && (IRQ_GTM_TIM3_SR4_CAT == IRQ_CAT1))
extern void GTMTIM3SR4_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR5_PRIO > 0) && (IRQ_GTM_TIM3_SR5_CAT == IRQ_CAT1))
extern void GTMTIM3SR5_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR6_PRIO > 0) && (IRQ_GTM_TIM3_SR6_CAT == IRQ_CAT1))
extern void GTMTIM3SR6_ISR(void);
#endif

#if((IRQ_GTM_TIM3_SR7_PRIO > 0) && (IRQ_GTM_TIM3_SR7_CAT == IRQ_CAT1))
extern void GTMTIM3SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TIM3_EXIST == STD_ON) */

#if (IRQ_GTM_TIM4_EXIST == STD_ON)

#if((IRQ_GTM_TIM4_SR0_PRIO > 0) && (IRQ_GTM_TIM4_SR0_CAT == IRQ_CAT1))
extern void GTMTIM4SR0_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR1_PRIO > 0) && (IRQ_GTM_TIM4_SR1_CAT == IRQ_CAT1))
extern void GTMTIM4SR1_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR2_PRIO > 0) && (IRQ_GTM_TIM4_SR2_CAT == IRQ_CAT1))
extern void GTMTIM4SR2_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR3_PRIO > 0) && (IRQ_GTM_TIM4_SR3_CAT == IRQ_CAT1))
extern void GTMTIM4SR3_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR4_PRIO > 0) && (IRQ_GTM_TIM4_SR4_CAT == IRQ_CAT1))
extern void GTMTIM4SR4_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR5_PRIO > 0) && (IRQ_GTM_TIM4_SR5_CAT == IRQ_CAT1))
extern void GTMTIM4SR5_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR6_PRIO > 0) && (IRQ_GTM_TIM4_SR6_CAT == IRQ_CAT1))
extern void GTMTIM4SR6_ISR(void);
#endif

#if((IRQ_GTM_TIM4_SR7_PRIO > 0) && (IRQ_GTM_TIM4_SR7_CAT == IRQ_CAT1))
extern void GTMTIM4SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TIM4_EXIST == STD_ON) */

#if (IRQ_GTM_TIM5_EXIST == STD_ON)

#if((IRQ_GTM_TIM5_SR0_PRIO > 0) && (IRQ_GTM_TIM5_SR0_CAT == IRQ_CAT1))
extern void GTMTIM5SR0_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR1_PRIO > 0) && (IRQ_GTM_TIM5_SR1_CAT == IRQ_CAT1))
extern void GTMTIM5SR1_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR2_PRIO > 0) && (IRQ_GTM_TIM5_SR2_CAT == IRQ_CAT1))
extern void GTMTIM5SR2_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR3_PRIO > 0) && (IRQ_GTM_TIM5_SR3_CAT == IRQ_CAT1))
extern void GTMTIM5SR3_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR4_PRIO > 0) && (IRQ_GTM_TIM5_SR4_CAT == IRQ_CAT1))
extern void GTMTIM5SR4_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR5_PRIO > 0) && (IRQ_GTM_TIM5_SR5_CAT == IRQ_CAT1))
extern void GTMTIM5SR5_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR6_PRIO > 0) && (IRQ_GTM_TIM5_SR6_CAT == IRQ_CAT1))
extern void GTMTIM5SR6_ISR(void);
#endif

#if((IRQ_GTM_TIM5_SR7_PRIO > 0) && (IRQ_GTM_TIM5_SR7_CAT == IRQ_CAT1))
extern void GTMTIM5SR7_ISR(void);
#endif

#endif /* (IRQ_GTM_TIM5_EXIST == STD_ON) */

#if (IRQ_GTM_AEI_EXIST == STD_ON)
#if((IRQ_GTM_AEI_PRIO > 0) && (IRQ_GTM_AEI_CAT == IRQ_CAT1))
extern void GTMAEI_ISR(void);
#endif
#endif /* (IRQ_GTM_AEI_EXIST == STD_ON) */

#if (IRQ_GTM_ARU_EXIST == STD_ON)
#if((IRQ_GTM_ARU_SR0_PRIO > 0) && (IRQ_GTM_ARU_SR0_CAT == IRQ_CAT1))
extern void GTMARUSR0_ISR(void);
#endif

#if((IRQ_GTM_ARU_SR1_PRIO > 0) && (IRQ_GTM_ARU_SR1_CAT == IRQ_CAT1))
extern void GTMARUSR1_ISR(void);
#endif

#if((IRQ_GTM_ARU_SR2_PRIO > 0) && (IRQ_GTM_ARU_SR2_CAT == IRQ_CAT1))
extern void GTMARUSR2_ISR(void);
#endif
#endif /* (IRQ_GTM_ARU_EXIST == STD_ON) */

#if (IRQ_GTM_BRC_EXIST == STD_ON)
#if((IRQ_GTM_BRC_PRIO > 0) && (IRQ_GTM_BRC_CAT == IRQ_CAT1))
extern void GTMBRC_ISR(void);
#endif
#endif /* (IRQ_GTM_BRC_EXIST == STD_ON) */

#if (IRQ_GTM_CMP_EXIST == STD_ON)
#if((IRQ_GTM_CMP_PRIO > 0) && (IRQ_GTM_CMP_CAT == IRQ_CAT1))
extern void GTMCMP_ISR(void);
#endif
#endif /* (IRQ_GTM_CMP_EXIST == STD_ON) */

#if (IRQ_GTM_SPE0_EXIST == STD_ON)
#if((IRQ_GTM_SPE0_PRIO > 0) && (IRQ_GTM_SPE0_CAT == IRQ_CAT1))
extern void GTMSPE0_ISR(void);
#endif
#endif /* (IRQ_GTM_SPE0_EXIST == STD_ON) */

#if (IRQ_GTM_SPE1_EXIST == STD_ON)
#if((IRQ_GTM_SPE1_PRIO > 0) && (IRQ_GTM_SPE1_CAT == IRQ_CAT1))
extern void GTMSPE1_ISR(void);
#endif
#endif /* (IRQ_GTM_SPE1_EXIST == STD_ON) */

#if (IRQ_GTM_PSM0_EXIST == STD_ON)
#if((IRQ_GTM_PSM0_SR0_PRIO > 0) && (IRQ_GTM_PSM0_SR0_CAT == IRQ_CAT1))
extern void GTMPSM0SR0_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR1_PRIO > 0) && (IRQ_GTM_PSM0_SR1_CAT == IRQ_CAT1))
extern void GTMPSM0SR1_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR2_PRIO > 0) && (IRQ_GTM_PSM0_SR2_CAT == IRQ_CAT1))
extern void GTMPSM0SR2_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR3_PRIO > 0) && (IRQ_GTM_PSM0_SR3_CAT == IRQ_CAT1))
extern void GTMPSM0SR3_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR4_PRIO > 0) && (IRQ_GTM_PSM0_SR4_CAT == IRQ_CAT1))
extern void GTMPSM0SR4_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR5_PRIO > 0) && (IRQ_GTM_PSM0_SR5_CAT == IRQ_CAT1))
extern void GTMPSM0SR5_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR6_PRIO > 0) && (IRQ_GTM_PSM0_SR6_CAT == IRQ_CAT1))
extern void GTMPSM0SR6_ISR(void);
#endif

#if((IRQ_GTM_PSM0_SR7_PRIO > 0) && (IRQ_GTM_PSM0_SR7_CAT == IRQ_CAT1))
extern void GTMPSM0SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_PSM0_EXIST == STD_ON) */

#if (IRQ_GTM_DPLL_EXIST == STD_ON)
#if((IRQ_GTM_DPLL_SR0_PRIO > 0) && (IRQ_GTM_DPLL_SR0_CAT == IRQ_CAT1))
extern void GTMDPLLSR0_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR1_PRIO > 0) && (IRQ_GTM_DPLL_SR1_CAT == IRQ_CAT1))
extern void GTMDPLLSR1_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR2_PRIO > 0) && (IRQ_GTM_DPLL_SR2_CAT == IRQ_CAT1))
extern void GTMDPLLSR2_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR3_PRIO > 0) && (IRQ_GTM_DPLL_SR3_CAT == IRQ_CAT1))
extern void GTMDPLLSR3_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR4_PRIO > 0) && (IRQ_GTM_DPLL_SR4_CAT == IRQ_CAT1))
extern void GTMDPLLSR4_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR5_PRIO > 0) && (IRQ_GTM_DPLL_SR5_CAT == IRQ_CAT1))
extern void GTMDPLLSR5_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR6_PRIO > 0) && (IRQ_GTM_DPLL_SR6_CAT == IRQ_CAT1))
extern void GTMDPLLSR6_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR7_PRIO > 0) && (IRQ_GTM_DPLL_SR7_CAT == IRQ_CAT1))
extern void GTMDPLLSR7_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR8_PRIO > 0) && (IRQ_GTM_DPLL_SR8_CAT == IRQ_CAT1))
extern void GTMDPLLSR8_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR9_PRIO > 0) && (IRQ_GTM_DPLL_SR9_CAT == IRQ_CAT1))
extern void GTMDPLLSR9_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR10_PRIO > 0) && (IRQ_GTM_DPLL_SR10_CAT == IRQ_CAT1))
extern void GTMDPLLSR10_ISR(void);
#endif


#if((IRQ_GTM_DPLL_SR11_PRIO > 0) && (IRQ_GTM_DPLL_SR11_CAT == IRQ_CAT1))
extern void GTMDPLLSR11_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR12_PRIO > 0) && (IRQ_GTM_DPLL_SR12_CAT == IRQ_CAT1))
extern void GTMDPLLSR12_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR13_PRIO > 0) && (IRQ_GTM_DPLL_SR13_CAT == IRQ_CAT1))
extern void GTMDPLLSR13_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR14_PRIO > 0) && (IRQ_GTM_DPLL_SR14_CAT == IRQ_CAT1))
extern void GTMDPLLSR14_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR15_PRIO > 0) && (IRQ_GTM_DPLL_SR15_CAT == IRQ_CAT1))
extern void GTMDPLLSR15_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR16_PRIO > 0) && (IRQ_GTM_DPLL_SR16_CAT == IRQ_CAT1))
extern void GTMDPLLSR16_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR17_PRIO > 0) && (IRQ_GTM_DPLL_SR17_CAT == IRQ_CAT1))
extern void GTMDPLLSR17_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR18_PRIO > 0) && (IRQ_GTM_DPLL_SR18_CAT == IRQ_CAT1))
extern void GTMDPLLSR18_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR19_PRIO > 0) && (IRQ_GTM_DPLL_SR19_CAT == IRQ_CAT1))
extern void GTMDPLLSR19_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR20_PRIO > 0) && (IRQ_GTM_DPLL_SR20_CAT == IRQ_CAT1))
extern void GTMDPLLSR20_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR21_PRIO > 0) && (IRQ_GTM_DPLL_SR21_CAT == IRQ_CAT1))
extern void GTMDPLLSR21_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR22_PRIO > 0) && (IRQ_GTM_DPLL_SR22_CAT == IRQ_CAT1))
extern void GTMDPLLSR22_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR23_PRIO > 0) && (IRQ_GTM_DPLL_SR23_CAT == IRQ_CAT1))
extern void GTMDPLLSR23_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR24_PRIO > 0) && (IRQ_GTM_DPLL_SR24_CAT == IRQ_CAT1))
extern void GTMDPLLSR24_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR25_PRIO > 0) && (IRQ_GTM_DPLL_SR25_CAT == IRQ_CAT1))
extern void GTMDPLLSR25_ISR(void);
#endif

#if((IRQ_GTM_DPLL_SR26_PRIO > 0) && (IRQ_GTM_DPLL_SR26_CAT == IRQ_CAT1))
extern void GTMDPLLSR26_ISR(void);
#endif
#endif /* (IRQ_GTM_DPLL_EXIST == STD_ON) */

#if (IRQ_GTM_MCS0_EXIST == STD_ON)
#if((IRQ_GTM_MCS0_SR0_PRIO > 0) && (IRQ_GTM_MCS0_SR0_CAT == IRQ_CAT1))
extern void GTMMCS0SR0_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR1_PRIO > 0) && (IRQ_GTM_MCS0_SR1_CAT == IRQ_CAT1))
extern void GTMMCS0SR1_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR2_PRIO > 0) && (IRQ_GTM_MCS0_SR2_CAT == IRQ_CAT1))
extern void GTMMCS0SR2_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR3_PRIO > 0) && (IRQ_GTM_MCS0_SR3_CAT == IRQ_CAT1))
extern void GTMMCS0SR3_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR4_PRIO > 0) && (IRQ_GTM_MCS0_SR4_CAT == IRQ_CAT1))
extern void GTMMCS0SR4_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR5_PRIO > 0) && (IRQ_GTM_MCS0_SR5_CAT == IRQ_CAT1))
extern void GTMMCS0SR5_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR6_PRIO > 0) && (IRQ_GTM_MCS0_SR6_CAT == IRQ_CAT1))
extern void GTMMCS0SR6_ISR(void);
#endif

#if((IRQ_GTM_MCS0_SR7_PRIO > 0) && (IRQ_GTM_MCS0_SR7_CAT == IRQ_CAT1))
extern void GTMMCS0SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_MCS0_EXIST == STD_ON) */

#if (IRQ_GTM_MCS1_EXIST == STD_ON)
#if((IRQ_GTM_MCS1_SR0_PRIO > 0) && (IRQ_GTM_MCS1_SR0_CAT == IRQ_CAT1))
extern void GTMMCS1SR0_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR1_PRIO > 0) && (IRQ_GTM_MCS1_SR1_CAT == IRQ_CAT1))
extern void GTMMCS1SR1_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR2_PRIO > 0) && (IRQ_GTM_MCS1_SR2_CAT == IRQ_CAT1))
extern void GTMMCS1SR2_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR3_PRIO > 0) && (IRQ_GTM_MCS1_SR3_CAT == IRQ_CAT1))
extern void GTMMCS1SR3_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR4_PRIO > 0) && (IRQ_GTM_MCS1_SR4_CAT == IRQ_CAT1))
extern void GTMMCS1SR4_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR5_PRIO > 0) && (IRQ_GTM_MCS1_SR5_CAT == IRQ_CAT1))
extern void GTMMCS1SR5_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR6_PRIO > 0) && (IRQ_GTM_MCS1_SR6_CAT == IRQ_CAT1))
extern void GTMMCS1SR6_ISR(void);
#endif

#if((IRQ_GTM_MCS1_SR7_PRIO > 0) && (IRQ_GTM_MCS1_SR7_CAT == IRQ_CAT1))
extern void GTMMCS1SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_MCS1_EXIST == STD_ON) */

#if (IRQ_GTM_MCS2_EXIST == STD_ON)
#if((IRQ_GTM_MCS2_SR0_PRIO > 0) && (IRQ_GTM_MCS2_SR0_CAT == IRQ_CAT1))
extern void GTMMCS2SR0_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR1_PRIO > 0) && (IRQ_GTM_MCS2_SR1_CAT == IRQ_CAT1))
extern void GTMMCS2SR1_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR2_PRIO > 0) && (IRQ_GTM_MCS2_SR2_CAT == IRQ_CAT1))
extern void GTMMCS2SR2_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR3_PRIO > 0) && (IRQ_GTM_MCS2_SR3_CAT == IRQ_CAT1))
extern void GTMMCS2SR3_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR4_PRIO > 0) && (IRQ_GTM_MCS2_SR4_CAT == IRQ_CAT1))
extern void GTMMCS2SR4_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR5_PRIO > 0) && (IRQ_GTM_MCS2_SR5_CAT == IRQ_CAT1))
extern void GTMMCS2SR5_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR6_PRIO > 0) && (IRQ_GTM_MCS2_SR6_CAT == IRQ_CAT1))
extern void GTMMCS2SR6_ISR(void);
#endif

#if((IRQ_GTM_MCS2_SR7_PRIO > 0) && (IRQ_GTM_MCS2_SR7_CAT == IRQ_CAT1))
extern void GTMMCS2SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_MCS2_EXIST == STD_ON) */

#if (IRQ_GTM_MCS3_EXIST == STD_ON)
#if((IRQ_GTM_MCS3_SR0_PRIO > 0) && (IRQ_GTM_MCS3_SR0_CAT == IRQ_CAT1))
extern void GTMMCS3SR0_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR1_PRIO > 0) && (IRQ_GTM_MCS3_SR1_CAT == IRQ_CAT1))
extern void GTMMCS3SR1_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR2_PRIO > 0) && (IRQ_GTM_MCS3_SR2_CAT == IRQ_CAT1))
extern void GTMMCS3SR2_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR3_PRIO > 0) && (IRQ_GTM_MCS3_SR3_CAT == IRQ_CAT1))
extern void GTMMCS3SR3_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR4_PRIO > 0) && (IRQ_GTM_MCS3_SR4_CAT == IRQ_CAT1))
extern void GTMMCS3SR4_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR5_PRIO > 0) && (IRQ_GTM_MCS3_SR5_CAT == IRQ_CAT1))
extern void GTMMCS3SR5_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR6_PRIO > 0) && (IRQ_GTM_MCS3_SR6_CAT == IRQ_CAT1))
extern void GTMMCS3SR6_ISR(void);
#endif

#if((IRQ_GTM_MCS3_SR7_PRIO > 0) && (IRQ_GTM_MCS3_SR7_CAT == IRQ_CAT1))
extern void GTMMCS3SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_MCS0_EXIST == STD_ON) */

#if (IRQ_GTM_MCS4_EXIST == STD_ON)
#if((IRQ_GTM_MCS4_SR0_PRIO > 0) && (IRQ_GTM_MCS4_SR0_CAT == IRQ_CAT1))
extern void GTMMCS4SR0_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR1_PRIO > 0) && (IRQ_GTM_MCS4_SR1_CAT == IRQ_CAT1))
extern void GTMMCS4SR1_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR2_PRIO > 0) && (IRQ_GTM_MCS4_SR2_CAT == IRQ_CAT1))
extern void GTMMCS4SR2_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR3_PRIO > 0) && (IRQ_GTM_MCS4_SR3_CAT == IRQ_CAT1))
extern void GTMMCS4SR3_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR4_PRIO > 0) && (IRQ_GTM_MCS4_SR4_CAT == IRQ_CAT1))
extern void GTMMCS4SR4_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR5_PRIO > 0) && (IRQ_GTM_MCS4_SR5_CAT == IRQ_CAT1))
extern void GTMMCS4SR5_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR6_PRIO > 0) && (IRQ_GTM_MCS4_SR6_CAT == IRQ_CAT1))
extern void GTMMCS4SR6_ISR(void);
#endif

#if((IRQ_GTM_MCS4_SR7_PRIO > 0) && (IRQ_GTM_MCS4_SR7_CAT == IRQ_CAT1))
extern void GTMMCS4SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_MCS4_EXIST == STD_ON) */

#if (IRQ_GTM_MCS5_EXIST == STD_ON)
#if((IRQ_GTM_MCS5_SR0_PRIO > 0) && (IRQ_GTM_MCS5_SR0_CAT == IRQ_CAT1))
extern void GTMMCS5SR0_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR1_PRIO > 0) && (IRQ_GTM_MCS5_SR1_CAT == IRQ_CAT1))
extern void GTMMCS5SR1_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR2_PRIO > 0) && (IRQ_GTM_MCS5_SR2_CAT == IRQ_CAT1))
extern void GTMMCS5SR2_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR3_PRIO > 0) && (IRQ_GTM_MCS5_SR3_CAT == IRQ_CAT1))
extern void GTMMCS5SR3_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR4_PRIO > 0) && (IRQ_GTM_MCS5_SR4_CAT == IRQ_CAT1))
extern void GTMMCS5SR4_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR5_PRIO > 0) && (IRQ_GTM_MCS5_SR5_CAT == IRQ_CAT1))
extern void GTMMCS5SR5_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR6_PRIO > 0) && (IRQ_GTM_MCS5_SR6_CAT == IRQ_CAT1))
extern void GTMMCS5SR6_ISR(void);
#endif

#if((IRQ_GTM_MCS5_SR7_PRIO > 0) && (IRQ_GTM_MCS5_SR7_CAT == IRQ_CAT1))
extern void GTMMCS5SR7_ISR(void);
#endif
#endif /* (IRQ_GTM_MCS5_EXIST == STD_ON) */

/******************************************************************************
** Syntax : void ERU_ISR_SRN00(void)                                         **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for ERU Service request                             **
**                                                                           **
*****************************************************************************/

#if((IRQ_SCU_ERU_SR0_PRIO > 0) && (IRQ_SCU_ERU_SR0_CAT == IRQ_CAT1))
extern void SCUERUSR0_ISR(void);
#endif

/******************************************************************************
** Syntax : void CCU6x_ISR_SRN00(void)                                       **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for ERU Service request                             **
**                                                                           **
*****************************************************************************/

#if((IRQ_CCU60_SR0_PRIO > 0) && (IRQ_CCU60_SR0_CAT == IRQ_CAT1))
extern void CCU60SR0_ISR(void);
#endif

#if((IRQ_CCU61_SR0_PRIO > 0) && (IRQ_CCU61_SR0_CAT == IRQ_CAT1))
extern void CCU61SR0_ISR(void);
#endif

/*******************************************************************************
** Syntax :  void IrqAdc_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for various                    **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqAdc_Init(void);

/*******************************************************************************
** Syntax :  void IrqEthernet_Init(void)                                      **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for Ethernet                   **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqEthernet_Init(void);
#if((IRQ_ETH_SR_PRIO > 0) && (IRQ_ETH_SR_CAT == IRQ_CAT1))
extern void ETHSR_ISR(void);
#endif


/*******************************************************************************
** Syntax :  void IrqMsc_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for MSC                        **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqMsc_Init(void);

#if (IRQ_MSC_EXIST == STD_ON)
#if (IRQ_MSC0_EXIST == STD_ON)
/******************************************************************************
** Syntax : void MSC0SR0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC0 Service request 0                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC0_SR0_PRIO > 0) && (IRQ_MSC0_SR0_CAT == IRQ_CAT1))
extern void MSC0SR0_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC0SR1_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC0 Service request 1                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC0_SR1_PRIO > 0) && (IRQ_MSC0_SR1_CAT == IRQ_CAT1))
extern void MSC0SR1_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC0SR2_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC0 Service request 2                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC0_SR2_PRIO > 0) && (IRQ_MSC0_SR2_CAT == IRQ_CAT1))
extern void MSC0SR2_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC0SR3_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC0 Service request 3                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC0_SR3_PRIO > 0) && (IRQ_MSC0_SR3_CAT == IRQ_CAT1))
extern void MSC0SR3_ISR(void);
#endif
#endif/* End for IRQ_MSC0_EXIST == STD_ON*/

#if (IRQ_MSC1_EXIST == STD_ON)
/******************************************************************************
** Syntax : void MSC1SR0_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC1 Service request 0                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC1_SR0_PRIO > 0) && (IRQ_MSC1_SR0_CAT == IRQ_CAT1))
extern void MSC1SR0_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC1SR1_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC1 Service request 1                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC1_SR1_PRIO > 0) && (IRQ_MSC1_SR1_CAT == IRQ_CAT1))
extern void MSC1SR1_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC1SR2_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC1 Service request 2                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC1_SR2_PRIO > 0) && (IRQ_MSC1_SR2_CAT == IRQ_CAT1))
extern void MSC1SR2_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC1SR3_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC1 Service request 3                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC1_SR3_PRIO > 0) && (IRQ_MSC1_SR3_CAT == IRQ_CAT1))
extern void MSC1SR3_ISR(void);
#endif
#endif /* End for IRQ_MSC1_EXIST == STD_ON*/


#if (IRQ_MSC2_EXIST == STD_ON)
/******************************************************************************
** Syntax : void MSC2SR0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC2 Service request 0                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC2_SR0_PRIO > 0) && (IRQ_MSC2_SR0_CAT == IRQ_CAT1))
extern void MSC2SR0_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC2SR1_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC2 Service request 1                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC2_SR1_PRIO > 0) && (IRQ_MSC2_SR1_CAT == IRQ_CAT1))
extern void MSC2SR1_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC2SR2_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC2 Service request 2                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC2_SR2_PRIO > 0) && (IRQ_MSC2_SR2_CAT == IRQ_CAT1))
extern void MSC2SR2_ISR(void);
#endif

/******************************************************************************
** Syntax : void MSC2SR3_ISR (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for MSC2 Service request 3                          **
**               for handling Tx/Rx frames                                   **
**                                                                           **
*****************************************************************************/
#if((IRQ_MSC2_SR3_PRIO > 0) && (IRQ_MSC2_SR3_CAT == IRQ_CAT1))
extern void MSC2SR3_ISR(void);
#endif
#endif/* End for IRQ_MSC2_EXIST == STD_ON*/
#endif /*End for IRQ_MSC_EXIST == STD_ON*/

/*******************************************************************************
** Syntax :  void IrqDma_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for DMA                        **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqDma_Init(void);
#if (IRQ_DMA_EXIST == STD_ON)
/******************************************************************************
** Syntax : void DMAERRSR_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for DMA error Service request for various possible  **
**               errors generated by move engine or DMA itself               **
**                                                                           **
******************************************************************************/
#if((IRQ_DMA_ERR_SR_PRIO > 0U) && (IRQ_DMA_ERR_SR_CAT == IRQ_CAT1))
extern void DMAERRSR_ISR(void);
#endif

/******************************************************************************
** Syntax : void DMACHzSR_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for DMA Channel z service request                  **
**                                                                           **
******************************************************************************/
#if (IRQ_DMA_CH0TO47_EXIST == STD_ON)

#if((IRQ_DMA_CHANNEL0_SR_PRIO > 0) && (IRQ_DMA_CHANNEL0_SR_CAT == IRQ_CAT1))
extern void DMACH0SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL1_SR_PRIO > 0) && (IRQ_DMA_CHANNEL1_SR_CAT == IRQ_CAT1))
extern void DMACH1SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL2_SR_PRIO > 0) && (IRQ_DMA_CHANNEL2_SR_CAT == IRQ_CAT1))
extern void DMACH2SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL3_SR_PRIO > 0) && (IRQ_DMA_CHANNEL3_SR_CAT == IRQ_CAT1))
extern void DMACH3SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL4_SR_PRIO > 0) && (IRQ_DMA_CHANNEL4_SR_CAT == IRQ_CAT1))
extern void DMACH4SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL5_SR_PRIO > 0) && (IRQ_DMA_CHANNEL5_SR_CAT == IRQ_CAT1))
extern void DMACH5SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL6_SR_PRIO > 0) && (IRQ_DMA_CHANNEL6_SR_CAT == IRQ_CAT1))
extern void DMACH6SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL7_SR_PRIO > 0) && (IRQ_DMA_CHANNEL7_SR_CAT == IRQ_CAT1))
extern void DMACH7SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL8_SR_PRIO > 0) && (IRQ_DMA_CHANNEL8_SR_CAT == IRQ_CAT1))
extern void DMACH8SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL9_SR_PRIO > 0) && (IRQ_DMA_CHANNEL9_SR_CAT == IRQ_CAT1))
extern void DMACH9SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL10_SR_PRIO > 0) && (IRQ_DMA_CHANNEL10_SR_CAT == IRQ_CAT1))
extern void DMACH10SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL11_SR_PRIO > 0) && (IRQ_DMA_CHANNEL11_SR_CAT == IRQ_CAT1))
extern void DMACH11SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL12_SR_PRIO > 0) && (IRQ_DMA_CHANNEL12_SR_CAT == IRQ_CAT1))
extern void DMACH12SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL13_SR_PRIO > 0) && (IRQ_DMA_CHANNEL13_SR_CAT == IRQ_CAT1))
extern void DMACH13SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL14_SR_PRIO > 0) && (IRQ_DMA_CHANNEL14_SR_CAT == IRQ_CAT1))
extern void DMACH14SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL15_SR_PRIO > 0) && (IRQ_DMA_CHANNEL15_SR_CAT == IRQ_CAT1))
extern void DMACH15SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL16_SR_PRIO > 0) && (IRQ_DMA_CHANNEL16_SR_CAT == IRQ_CAT1))
extern void DMACH16SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL17_SR_PRIO > 0) && (IRQ_DMA_CHANNEL17_SR_CAT == IRQ_CAT1))
extern void DMACH17SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL18_SR_PRIO > 0) && (IRQ_DMA_CHANNEL18_SR_CAT == IRQ_CAT1))
extern void DMACH18SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL19_SR_PRIO > 0) && (IRQ_DMA_CHANNEL19_SR_CAT == IRQ_CAT1))
extern void DMACH19SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL20_SR_PRIO > 0) && (IRQ_DMA_CHANNEL20_SR_CAT == IRQ_CAT1))
extern void DMACH20SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL21_SR_PRIO > 0) && (IRQ_DMA_CHANNEL21_SR_CAT == IRQ_CAT1))
extern void DMACH21SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL22_SR_PRIO > 0) && (IRQ_DMA_CHANNEL22_SR_CAT == IRQ_CAT1))
extern void DMACH22SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL23_SR_PRIO > 0) && (IRQ_DMA_CHANNEL23_SR_CAT == IRQ_CAT1))
extern void DMACH23SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL24_SR_PRIO > 0) && (IRQ_DMA_CHANNEL24_SR_CAT == IRQ_CAT1))
extern void DMACH24SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL25_SR_PRIO > 0) && (IRQ_DMA_CHANNEL25_SR_CAT == IRQ_CAT1))
extern void DMACH25SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL26_SR_PRIO > 0) && (IRQ_DMA_CHANNEL26_SR_CAT == IRQ_CAT1))
extern void DMACH26SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL27_SR_PRIO > 0) && (IRQ_DMA_CHANNEL27_SR_CAT == IRQ_CAT1))
extern void DMACH27SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL28_SR_PRIO > 0) && (IRQ_DMA_CHANNEL28_SR_CAT == IRQ_CAT1))
extern void DMACH28SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL29_SR_PRIO > 0) && (IRQ_DMA_CHANNEL29_SR_CAT == IRQ_CAT1))
extern void DMACH29SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL30_SR_PRIO > 0) && (IRQ_DMA_CHANNEL30_SR_CAT == IRQ_CAT1))
extern void DMACH30SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL31_SR_PRIO > 0) && (IRQ_DMA_CHANNEL31_SR_CAT == IRQ_CAT1))
extern void DMACH31SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL32_SR_PRIO > 0) && (IRQ_DMA_CHANNEL32_SR_CAT == IRQ_CAT1))
extern void DMACH32SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL33_SR_PRIO > 0) && (IRQ_DMA_CHANNEL33_SR_CAT == IRQ_CAT1))
extern void DMACH33SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL34_SR_PRIO > 0) && (IRQ_DMA_CHANNEL34_SR_CAT == IRQ_CAT1))
extern void DMACH34SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL35_SR_PRIO > 0) && (IRQ_DMA_CHANNEL35_SR_CAT == IRQ_CAT1))
extern void DMACH35SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL36_SR_PRIO > 0) && (IRQ_DMA_CHANNEL36_SR_CAT == IRQ_CAT1))
extern void DMACH36SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL37_SR_PRIO > 0) && (IRQ_DMA_CHANNEL37_SR_CAT == IRQ_CAT1))
extern void DMACH37SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL38_SR_PRIO > 0) && (IRQ_DMA_CHANNEL38_SR_CAT == IRQ_CAT1))
extern void DMACH38SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL39_SR_PRIO > 0) && (IRQ_DMA_CHANNEL39_SR_CAT == IRQ_CAT1))
extern void DMACH39SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL40_SR_PRIO > 0) && (IRQ_DMA_CHANNEL40_SR_CAT == IRQ_CAT1))
extern void DMACH40SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL41_SR_PRIO > 0) && (IRQ_DMA_CHANNEL41_SR_CAT == IRQ_CAT1))
extern void DMACH41SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL42_SR_PRIO > 0) && (IRQ_DMA_CHANNEL42_SR_CAT == IRQ_CAT1))
extern void DMACH42SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL43_SR_PRIO > 0) && (IRQ_DMA_CHANNEL43_SR_CAT == IRQ_CAT1))
extern void DMACH43SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL44_SR_PRIO > 0) && (IRQ_DMA_CHANNEL44_SR_CAT == IRQ_CAT1))
extern void DMACH44SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL45_SR_PRIO > 0) && (IRQ_DMA_CHANNEL45_SR_CAT == IRQ_CAT1))
extern void DMACH45SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL46_SR_PRIO > 0) && (IRQ_DMA_CHANNEL46_SR_CAT == IRQ_CAT1))
extern void DMACH46SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL47_SR_PRIO > 0) && (IRQ_DMA_CHANNEL47_SR_CAT == IRQ_CAT1))
extern void DMACH47SR_ISR(void);
#endif
#endif/*End of IRQ_DMA_CH0TO47_EXIST*/

#if (IRQ_DMA_CH48TO63_EXIST == STD_ON)
#if((IRQ_DMA_CHANNEL48_SR_PRIO > 0) && (IRQ_DMA_CHANNEL48_SR_CAT == IRQ_CAT1))
extern void DMACH48SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL49_SR_PRIO > 0) && (IRQ_DMA_CHANNEL49_SR_CAT == IRQ_CAT1))
extern void DMACH49SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL50_SR_PRIO > 0) && (IRQ_DMA_CHANNEL50_SR_CAT == IRQ_CAT1))
extern void DMACH50SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL51_SR_PRIO > 0) && (IRQ_DMA_CHANNEL51_SR_CAT == IRQ_CAT1))
extern void DMACH51SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL52_SR_PRIO > 0) && (IRQ_DMA_CHANNEL52_SR_CAT == IRQ_CAT1))
extern void DMACH52SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL53_SR_PRIO > 0) && (IRQ_DMA_CHANNEL53_SR_CAT == IRQ_CAT1))
extern void DMACH53SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL54_SR_PRIO > 0) && (IRQ_DMA_CHANNEL54_SR_CAT == IRQ_CAT1))
extern void DMACH54SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL55_SR_PRIO > 0) && (IRQ_DMA_CHANNEL55_SR_CAT == IRQ_CAT1))
extern void DMACH55SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL56_SR_PRIO > 0) && (IRQ_DMA_CHANNEL56_SR_CAT == IRQ_CAT1))
extern void DMACH56SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL57_SR_PRIO > 0) && (IRQ_DMA_CHANNEL57_SR_CAT == IRQ_CAT1))
extern void DMACH57SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL58_SR_PRIO > 0) && (IRQ_DMA_CHANNEL58_SR_CAT == IRQ_CAT1))
extern void DMACH58SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL59_SR_PRIO > 0) && (IRQ_DMA_CHANNEL59_SR_CAT == IRQ_CAT1))
extern void DMACH59SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL60_SR_PRIO > 0) && (IRQ_DMA_CHANNEL60_SR_CAT == IRQ_CAT1))
extern void DMACH60SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL61_SR_PRIO > 0) && (IRQ_DMA_CHANNEL61_SR_CAT == IRQ_CAT1))
extern void DMACH61SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL62_SR_PRIO > 0) && (IRQ_DMA_CHANNEL62_SR_CAT == IRQ_CAT1))
extern void DMACH62SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL63_SR_PRIO > 0) && (IRQ_DMA_CHANNEL63_SR_CAT == IRQ_CAT1))
extern void DMACH63SR_ISR(void);
#endif
#endif/*End of IRQ_DMA_CH48TO63_EXIST*/

#if (IRQ_DMA_CH64TO127_EXIST == STD_ON)
#if((IRQ_DMA_CHANNEL64_SR_PRIO > 0) && (IRQ_DMA_CHANNEL64_SR_CAT == IRQ_CAT1))
extern void DMACH64SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL65_SR_PRIO > 0) && (IRQ_DMA_CHANNEL65_SR_CAT == IRQ_CAT1))
extern void DMACH65SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL66_SR_PRIO > 0) && (IRQ_DMA_CHANNEL66_SR_CAT == IRQ_CAT1))
extern void DMACH66SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL67_SR_PRIO > 0) && (IRQ_DMA_CHANNEL67_SR_CAT == IRQ_CAT1))
extern void DMACH67SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL68_SR_PRIO > 0) && (IRQ_DMA_CHANNEL68_SR_CAT == IRQ_CAT1))
extern void DMACH68SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL69_SR_PRIO > 0) && (IRQ_DMA_CHANNEL69_SR_CAT == IRQ_CAT1))
extern void DMACH69SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL70_SR_PRIO > 0) && (IRQ_DMA_CHANNEL70_SR_CAT == IRQ_CAT1))
extern void DMACH70SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL71_SR_PRIO > 0) && (IRQ_DMA_CHANNEL71_SR_CAT == IRQ_CAT1))
extern void DMACH71SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL72_SR_PRIO > 0) && (IRQ_DMA_CHANNEL72_SR_CAT == IRQ_CAT1))
extern void DMACH72SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL73_SR_PRIO > 0) && (IRQ_DMA_CHANNEL73_SR_CAT == IRQ_CAT1))
extern void DMACH73SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL74_SR_PRIO > 0) && (IRQ_DMA_CHANNEL74_SR_CAT == IRQ_CAT1))
extern void DMACH74SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL75_SR_PRIO > 0) && (IRQ_DMA_CHANNEL75_SR_CAT == IRQ_CAT1))
extern void DMACH75SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL76_SR_PRIO > 0) && (IRQ_DMA_CHANNEL76_SR_CAT == IRQ_CAT1))
extern void DMACH76SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL77_SR_PRIO > 0) && (IRQ_DMA_CHANNEL77_SR_CAT == IRQ_CAT1))
extern void DMACH77SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL78_SR_PRIO > 0) && (IRQ_DMA_CHANNEL78_SR_CAT == IRQ_CAT1))
extern void DMACH78SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL79_SR_PRIO > 0) && (IRQ_DMA_CHANNEL79_SR_CAT == IRQ_CAT1))
extern void DMACH79SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL80_SR_PRIO > 0) && (IRQ_DMA_CHANNEL80_SR_CAT == IRQ_CAT1))
extern void DMACH80SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL81_SR_PRIO > 0) && (IRQ_DMA_CHANNEL81_SR_CAT == IRQ_CAT1))
extern void DMACH81SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL82_SR_PRIO > 0) && (IRQ_DMA_CHANNEL82_SR_CAT == IRQ_CAT1))
extern void DMACH82SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL83_SR_PRIO > 0) && (IRQ_DMA_CHANNEL83_SR_CAT == IRQ_CAT1))
extern void DMACH83SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL84_SR_PRIO > 0) && (IRQ_DMA_CHANNEL84_SR_CAT == IRQ_CAT1))
extern void DMACH84SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL85_SR_PRIO > 0) && (IRQ_DMA_CHANNEL85_SR_CAT == IRQ_CAT1))
extern void DMACH85SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL86_SR_PRIO > 0) && (IRQ_DMA_CHANNEL86_SR_CAT == IRQ_CAT1))
extern void DMACH86SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL87_SR_PRIO > 0) && (IRQ_DMA_CHANNEL87_SR_CAT == IRQ_CAT1))
extern void DMACH87SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL88_SR_PRIO > 0) && (IRQ_DMA_CHANNEL88_SR_CAT == IRQ_CAT1))
extern void DMACH88SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL89_SR_PRIO > 0) && (IRQ_DMA_CHANNEL89_SR_CAT == IRQ_CAT1))
extern void DMACH89SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL90_SR_PRIO > 0) && (IRQ_DMA_CHANNEL90_SR_CAT == IRQ_CAT1))
extern void DMACH90SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL91_SR_PRIO > 0) && (IRQ_DMA_CHANNEL91_SR_CAT == IRQ_CAT1))
extern void DMACH91SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL92_SR_PRIO > 0) && (IRQ_DMA_CHANNEL92_SR_CAT == IRQ_CAT1))
extern void DMACH92SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL93_SR_PRIO > 0) && (IRQ_DMA_CHANNEL93_SR_CAT == IRQ_CAT1))
extern void DMACH93SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL94_SR_PRIO > 0) && (IRQ_DMA_CHANNEL94_SR_CAT == IRQ_CAT1))
extern void DMACH94SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL95_SR_PRIO > 0) && (IRQ_DMA_CHANNEL95_SR_CAT == IRQ_CAT1))
extern void DMACH95SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL96_SR_PRIO > 0) && (IRQ_DMA_CHANNEL96_SR_CAT == IRQ_CAT1))
extern void DMACH96SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL97_SR_PRIO > 0) && (IRQ_DMA_CHANNEL97_SR_CAT == IRQ_CAT1))
extern void DMACH97SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL98_SR_PRIO > 0) && (IRQ_DMA_CHANNEL98_SR_CAT == IRQ_CAT1))
extern void DMACH98SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL99_SR_PRIO > 0) && (IRQ_DMA_CHANNEL99_SR_CAT == IRQ_CAT1))
extern void DMACH99SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL100_SR_PRIO > 0) && (IRQ_DMA_CHANNEL100_SR_CAT == IRQ_CAT1))
extern void DMACH100SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL101_SR_PRIO > 0) && (IRQ_DMA_CHANNEL101_SR_CAT == IRQ_CAT1))
extern void DMACH101SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL102_SR_PRIO > 0) && (IRQ_DMA_CHANNEL102_SR_CAT == IRQ_CAT1))
extern void DMACH102SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL103_SR_PRIO > 0) && (IRQ_DMA_CHANNEL103_SR_CAT == IRQ_CAT1))
extern void DMACH103SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL104_SR_PRIO > 0) && (IRQ_DMA_CHANNEL104_SR_CAT == IRQ_CAT1))
extern void DMACH104SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL105_SR_PRIO > 0) && (IRQ_DMA_CHANNEL105_SR_CAT == IRQ_CAT1))
extern void DMACH105SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL106_SR_PRIO > 0) && (IRQ_DMA_CHANNEL106_SR_CAT == IRQ_CAT1))
extern void DMACH106SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL107_SR_PRIO > 0) && (IRQ_DMA_CHANNEL107_SR_CAT == IRQ_CAT1))
extern void DMACH107SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL108_SR_PRIO > 0) && (IRQ_DMA_CHANNEL108_SR_CAT == IRQ_CAT1))
extern void DMACH108SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL109_SR_PRIO > 0) && (IRQ_DMA_CHANNEL109_SR_CAT == IRQ_CAT1))
extern void DMACH109SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL110_SR_PRIO > 0) && (IRQ_DMA_CHANNEL110_SR_CAT == IRQ_CAT1))
extern void DMACH110SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL111_SR_PRIO > 0) && (IRQ_DMA_CHANNEL111_SR_CAT == IRQ_CAT1))
extern void DMACH111SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL112_SR_PRIO > 0) && (IRQ_DMA_CHANNEL112_SR_CAT == IRQ_CAT1))
extern void DMACH112SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL113_SR_PRIO > 0) && (IRQ_DMA_CHANNEL113_SR_CAT == IRQ_CAT1))
extern void DMACH113SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL114_SR_PRIO > 0) && (IRQ_DMA_CHANNEL114_SR_CAT == IRQ_CAT1))
extern void DMACH114SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL115_SR_PRIO > 0) && (IRQ_DMA_CHANNEL115_SR_CAT == IRQ_CAT1))
extern void DMACH115SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL116_SR_PRIO > 0) && (IRQ_DMA_CHANNEL116_SR_CAT == IRQ_CAT1))
extern void DMACH116SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL117_SR_PRIO > 0) && (IRQ_DMA_CHANNEL117_SR_CAT == IRQ_CAT1))
extern void DMACH117SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL118_SR_PRIO > 0) && (IRQ_DMA_CHANNEL118_SR_CAT == IRQ_CAT1))
extern void DMACH118SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL119_SR_PRIO > 0) && (IRQ_DMA_CHANNEL119_SR_CAT == IRQ_CAT1))
extern void DMACH119SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL120_SR_PRIO > 0) && (IRQ_DMA_CHANNEL120_SR_CAT == IRQ_CAT1))
extern void DMACH120SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL121_SR_PRIO > 0) && (IRQ_DMA_CHANNEL121_SR_CAT == IRQ_CAT1))
extern void DMACH121SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL122_SR_PRIO > 0) && (IRQ_DMA_CHANNEL122_SR_CAT == IRQ_CAT1))
extern void DMACH122SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL123_SR_PRIO > 0) && (IRQ_DMA_CHANNEL123_SR_CAT == IRQ_CAT1))
extern void DMACH123SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL124_SR_PRIO > 0) && (IRQ_DMA_CHANNEL124_SR_CAT == IRQ_CAT1))
extern void DMACH124SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL125_SR_PRIO > 0) && (IRQ_DMA_CHANNEL125_SR_CAT == IRQ_CAT1))
extern void DMACH125SR_ISR(void);
#endif
#if((IRQ_DMA_CHANNEL126_SR_PRIO > 0) && (IRQ_DMA_CHANNEL126_SR_CAT == IRQ_CAT1))
extern void DMACH126SR_ISR(void);
#endif
#endif/*End of IRQ_DMA_CH64TO127_EXIST*/
#endif/*End of IRQ_DMA_EXIST*/

/*******************************************************************************
** Syntax :  void IrqDsadc_Init(void)                                         **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for DSADC                      **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqDsadc_Init(void);

#if (IRQ_DSADC_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void DSADCSRM0_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M0_EXIST == STD_ON)
#if((IRQ_DSADC_SRM0_PRIO > 0) && (IRQ_DSADC_SRM0_CAT == IRQ_CAT1))
extern void DSADCSRM0_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRA0_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A0_EXIST == STD_ON)
#if((IRQ_DSADC_SRA0_PRIO > 0) && (IRQ_DSADC_SRA0_CAT == IRQ_CAT1))
extern void DSADCSRA0_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRM1_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M1_EXIST == STD_ON)
#if((IRQ_DSADC_SRM1_PRIO > 0) && (IRQ_DSADC_SRM1_CAT == IRQ_CAT1))
extern void DSADCSRM1_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA1_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A1_EXIST == STD_ON)
#if((IRQ_DSADC_SRA1_PRIO > 0) && (IRQ_DSADC_SRA1_CAT == IRQ_CAT1))
extern void DSADCSRA1_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRM2_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M2_EXIST == STD_ON)
#if((IRQ_DSADC_SRM2_PRIO > 0) && (IRQ_DSADC_SRM2_CAT == IRQ_CAT1))
extern void DSADCSRM2_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA2_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A2_EXIST == STD_ON)
#if((IRQ_DSADC_SRA2_PRIO > 0) && (IRQ_DSADC_SRA2_CAT == IRQ_CAT1))
extern void DSADCSRA2_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM3_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M3_EXIST == STD_ON)
#if((IRQ_DSADC_SRM3_PRIO > 0) && (IRQ_DSADC_SRM3_CAT == IRQ_CAT1))
extern void DSADCSRM3_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA3_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A3_EXIST == STD_ON)
#if((IRQ_DSADC_SRA3_PRIO > 0) && (IRQ_DSADC_SRA3_CAT == IRQ_CAT1))
extern void DSADCSRA3_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM4_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M4_EXIST == STD_ON)
#if((IRQ_DSADC_SRM4_PRIO > 0) && (IRQ_DSADC_SRM4_CAT == IRQ_CAT1))
extern void DSADCSRM4_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA4_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A4_EXIST == STD_ON)
#if((IRQ_DSADC_SRA4_PRIO > 0) && (IRQ_DSADC_SRA4_CAT == IRQ_CAT1))
extern void DSADCSRA4_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM5_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M5_EXIST == STD_ON)
#if((IRQ_DSADC_SRM5_PRIO > 0) && (IRQ_DSADC_SRM5_CAT == IRQ_CAT1))
extern void DSADCSRM5_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA5_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A5_EXIST == STD_ON)
#if((IRQ_DSADC_SRA5_PRIO > 0) && (IRQ_DSADC_SRA5_CAT == IRQ_CAT1))
extern void DSADCSRA5_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM6_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M6_EXIST == STD_ON)
#if((IRQ_DSADC_SRM6_PRIO > 0) && (IRQ_DSADC_SRM6_CAT == IRQ_CAT1))
extern void DSADCSRM6_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA6_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A6_EXIST == STD_ON)
#if((IRQ_DSADC_SRA6_PRIO > 0) && (IRQ_DSADC_SRA6_CAT == IRQ_CAT1))
extern void DSADCSRA6_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM7_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M7_EXIST == STD_ON)
#if((IRQ_DSADC_SRM7_PRIO > 0) && (IRQ_DSADC_SRM7_CAT == IRQ_CAT1))
extern void DSADCSRM7_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA7_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A7_EXIST == STD_ON)
#if((IRQ_DSADC_SRA7_PRIO > 0) && (IRQ_DSADC_SRA7_CAT == IRQ_CAT1))
extern void DSADCSRA7_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM8_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M8_EXIST == STD_ON)
#if((IRQ_DSADC_SRM8_PRIO > 0) && (IRQ_DSADC_SRM8_CAT == IRQ_CAT1))
extern void DSADCSRM8_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA8_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A8_EXIST == STD_ON)
#if((IRQ_DSADC_SRA8_PRIO > 0) && (IRQ_DSADC_SRA8_CAT == IRQ_CAT1))
extern void DSADCSRA8_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax :          void DSADCSRM9_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_M9_EXIST == STD_ON)
#if((IRQ_DSADC_SRM9_PRIO > 0) && (IRQ_DSADC_SRM9_CAT == IRQ_CAT1))
extern void DSADCSRM9_ISR(void);
#endif
#endif
/******************************************************************************
** Syntax :          void DSADCSRA9_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on DSADC Request source conversion complete     **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_DSADC_A9_EXIST == STD_ON)
#if((IRQ_DSADC_SRA9_PRIO > 0) && (IRQ_DSADC_SRA9_CAT == IRQ_CAT1))
extern void DSADCSRA9_ISR(void);
#endif
#endif

#endif /* (IRQ_DSADC_EXIST == STD_ON) */

/*******************************************************************************
** Syntax :  void IrqScu_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for                            **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/
extern void IrqScu_Init(void);

/*******************************************************************************
** Syntax :  void IrqPmu_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for                            **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/

extern void IrqPmu_Init(void);

#if((IRQ_PMU0_SR0_PRIO > 0) && (IRQ_PMU0_SR0_CAT == IRQ_CAT1))
extern void PMU0SR0_ISR(void);
#endif

#if((IRQ_PMU0_SR1_PRIO > 0) && (IRQ_PMU0_SR1_CAT == IRQ_CAT1))
extern void PMU0SR1_ISR(void);
#endif

/*******************************************************************************
** Syntax :  void IrqSent_Init(void)                                          **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for                            **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/

extern void IrqSent_Init(void);


#if (IRQ_ADC_EXIST == STD_ON)

#if (IRQ_ADC0_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC0SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC0_SR0_PRIO > 0) && (IRQ_ADC0_SR0_CAT == IRQ_CAT1))
extern void ADC0SR0_ISR(void);
#endif /*((IRQ_ADC0_SR0_PRIO > 0) && (IRQ_ADC0_SR0_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC0SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC0_SR1_PRIO > 0) && (IRQ_ADC0_SR1_CAT == IRQ_CAT1))
extern void ADC0SR1_ISR(void);
#endif /*((IRQ_ADC0_SR1_PRIO > 0) && (IRQ_ADC0_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC0SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC0_SR2_PRIO > 0) && (IRQ_ADC0_SR2_CAT == IRQ_CAT1))
extern void ADC0SR2_ISR(void);
#endif /*((IRQ_ADC0_SR2_PRIO > 0) && (IRQ_ADC0_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC0_EXIST == STD_ON) */

#if (IRQ_ADC1_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC1SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC1_SR0_PRIO > 0) && (IRQ_ADC1_SR0_CAT == IRQ_CAT1))
extern void ADC1SR0_ISR(void);
#endif /*((IRQ_ADC1_SR0_PRIO > 0) && (IRQ_ADC1_SR0_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC1SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC1_SR1_PRIO > 0) && (IRQ_ADC1_SR1_CAT == IRQ_CAT1))
extern void ADC1SR1_ISR(void);
#endif /*((IRQ_ADC1_SR1_PRIO > 0) && (IRQ_ADC1_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC1SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC1_SR2_PRIO > 0) && (IRQ_ADC1_SR2_CAT == IRQ_CAT1))
extern void ADC1SR2_ISR(void);
#endif /*((IRQ_ADC1_SR2_PRIO > 0) && (IRQ_ADC1_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC1_EXIST == STD_ON) */

#if (IRQ_ADC2_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC2SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC2_SR0_PRIO > 0) && (IRQ_ADC2_SR0_CAT == IRQ_CAT1))
extern void ADC2SR0_ISR(void);
#endif /*((IRQ_ADC2_SR0_PRIO > 0) && (IRQ_ADC2_SR0_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC2SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC2_SR1_PRIO > 0) && (IRQ_ADC2_SR1_CAT == IRQ_CAT1))
extern void ADC2SR1_ISR(void);
#endif /*((IRQ_ADC2_SR1_PRIO > 0) && (IRQ_ADC2_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC2SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC2_SR2_PRIO > 0) && (IRQ_ADC2_SR2_CAT == IRQ_CAT1))
extern void ADC2SR2_ISR(void);
#endif /*((IRQ_ADC2_SR2_PRIO > 0) && (IRQ_ADC2_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC2_EXIST == STD_ON) */

#if (IRQ_ADC3_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC3SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC3_SR0_PRIO > 0) && (IRQ_ADC3_SR0_CAT == IRQ_CAT1))
extern void ADC3SR0_ISR(void);
#endif /*((IRQ_ADC3_SR0_PRIO > 0) && (IRQ_ADC3_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC3SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC3_SR1_PRIO > 0) && (IRQ_ADC3_SR1_CAT == IRQ_CAT1))
extern void ADC3SR1_ISR(void);
#endif /*((IRQ_ADC3_SR1_PRIO > 0) && (IRQ_ADC3_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC3SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC3_SR2_PRIO > 0) && (IRQ_ADC3_SR2_CAT == IRQ_CAT1))
extern void ADC3SR2_ISR(void);
#endif /*((IRQ_ADC3_SR2_PRIO > 0) && (IRQ_ADC3_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC3_EXIST == STD_ON) */

#if (IRQ_ADC4_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC4SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC4_SR0_PRIO > 0) && (IRQ_ADC4_SR0_CAT == IRQ_CAT1))
extern void ADC4SR0_ISR(void);
#endif /*((IRQ_ADC4_SR0_PRIO > 0) && (IRQ_ADC4_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC4SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC4_SR1_PRIO > 0) && (IRQ_ADC4_SR1_CAT == IRQ_CAT1))
extern void ADC4SR1_ISR(void);
#endif /*((IRQ_ADC4_SR1_PRIO > 0) && (IRQ_ADC4_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC4SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC4_SR2_PRIO > 0) && (IRQ_ADC4_SR2_CAT == IRQ_CAT1))
extern void ADC4SR2_ISR(void);
#endif /*((IRQ_ADC4_SR2_PRIO > 0) && (IRQ_ADC4_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC4_EXIST == STD_ON) */

#if (IRQ_ADC5_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC5SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC5_SR0_PRIO > 0) && (IRQ_ADC5_SR0_CAT == IRQ_CAT1))
extern void ADC5SR0_ISR(void);
#endif /*((IRQ_ADC5_SR0_PRIO > 0) && (IRQ_ADC5_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC5SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC5_SR1_PRIO > 0) && (IRQ_ADC5_SR1_CAT == IRQ_CAT1))
extern void ADC5SR1_ISR(void);
#endif /*((IRQ_ADC5_SR1_PRIO > 0) && (IRQ_ADC5_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC5SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC5_SR2_PRIO > 0) && (IRQ_ADC5_SR2_CAT == IRQ_CAT1))
extern void ADC5SR2_ISR(void);
#endif /*((IRQ_ADC5_SR2_PRIO > 0) && (IRQ_ADC5_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC5_EXIST == STD_ON) */

#if (IRQ_ADC6_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC6SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC6_SR0_PRIO > 0) && (IRQ_ADC6_SR0_CAT == IRQ_CAT1))
extern void ADC6SR0_ISR(void);
#endif /*((IRQ_ADC6_SR0_PRIO > 0) && (IRQ_ADC6_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC6SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC6_SR1_PRIO > 0) && (IRQ_ADC6_SR1_CAT == IRQ_CAT1))
extern void ADC6SR1_ISR(void);
#endif /*((IRQ_ADC6_SR1_PRIO > 0) && (IRQ_ADC6_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC6SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC6_SR2_PRIO > 0) && (IRQ_ADC6_SR2_CAT == IRQ_CAT1))
extern void ADC6SR2_ISR(void);
#endif /*((IRQ_ADC6_SR2_PRIO > 0) && (IRQ_ADC6_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC6_EXIST == STD_ON) */

#if (IRQ_ADC7_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC7SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC7_SR0_PRIO > 0) && (IRQ_ADC7_SR0_CAT == IRQ_CAT1))
extern void ADC7SR0_ISR(void);
#endif /*((IRQ_ADC7_SR0_PRIO > 0) && (IRQ_ADC7_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC7SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC7_SR1_PRIO > 0) && (IRQ_ADC7_SR1_CAT == IRQ_CAT1))
extern void ADC7SR1_ISR(void);
#endif /*((IRQ_ADC7_SR1_PRIO > 0) && (IRQ_ADC7_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC7SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC7_SR2_PRIO > 0) && (IRQ_ADC7_SR2_CAT == IRQ_CAT1))
extern void ADC7SR2_ISR(void);
#endif /*((IRQ_ADC7_SR2_PRIO > 0) && (IRQ_ADC7_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC7_EXIST == STD_ON) */

#if (IRQ_ADC8_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC8SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC8_SR0_PRIO > 0) && (IRQ_ADC8_SR0_CAT == IRQ_CAT1))
extern void ADC8SR0_ISR(void);
#endif /*((IRQ_ADC8_SR0_PRIO > 0) && (IRQ_ADC8_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC8SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC8_SR1_PRIO > 0) && (IRQ_ADC8_SR1_CAT == IRQ_CAT1))
extern void ADC8SR1_ISR(void);
#endif /*((IRQ_ADC8_SR1_PRIO > 0) && (IRQ_ADC8_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC8SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC8_SR2_PRIO > 0) && (IRQ_ADC8_SR2_CAT == IRQ_CAT1))
extern void ADC8SR2_ISR(void);
#endif /*((IRQ_ADC8_SR2_PRIO > 0) && (IRQ_ADC8_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC8_EXIST == STD_ON) */

#if (IRQ_ADC9_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC9SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC9_SR0_PRIO > 0) && (IRQ_ADC9_SR0_CAT == IRQ_CAT1))
extern void ADC9SR0_ISR(void);
#endif /*((IRQ_ADC9_SR0_PRIO > 0) && (IRQ_ADC9_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC9SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC9_SR1_PRIO > 0) && (IRQ_ADC9_SR1_CAT == IRQ_CAT1))
extern void ADC9SR1_ISR(void);
#endif /*((IRQ_ADC9_SR1_PRIO > 0) && (IRQ_ADC9_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC9SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC9_SR2_PRIO > 0) && (IRQ_ADC9_SR2_CAT == IRQ_CAT1))
extern void ADC9SR2_ISR(void);
#endif /*((IRQ_ADC9_SR2_PRIO > 0) && (IRQ_ADC9_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC9_EXIST == STD_ON) */

#if (IRQ_ADC10_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void ADC10SR0_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC10_SR0_PRIO > 0) && (IRQ_ADC10_SR0_CAT == IRQ_CAT1))
extern void ADC10SR0_ISR(void);
#endif /*((IRQ_ADC10_SR0_PRIO > 0) && (IRQ_ADC10_SR0_CAT == IRQ_CAT1)) */
/******************************************************************************
** Syntax :          void ADC10SR1_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC10_SR1_PRIO > 0) && (IRQ_ADC10_SR1_CAT == IRQ_CAT1))
extern void ADC10SR1_ISR(void);
#endif /*((IRQ_ADC10_SR1_PRIO > 0) && (IRQ_ADC10_SR1_CAT == IRQ_CAT1))*/
/******************************************************************************
** Syntax :          void ADC10SR2_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADC10_SR2_PRIO > 0) && (IRQ_ADC10_SR2_CAT == IRQ_CAT1))
extern void ADC10SR2_ISR(void);
#endif /*((IRQ_ADC10_SR2_PRIO > 0) && (IRQ_ADC10_SR2_CAT == IRQ_CAT1))*/

#endif /* (IRQ_ADC10_EXIST == STD_ON) */

#if (IRQ_ADCCG0_EXIST == STD_ON)
#if (IRQ_ADC_CG0SR0_USED_FOR_BGNDRS == STD_ON)
/******************************************************************************
** Syntax :          void ADCCG0SR0_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADCCG0_SR0_PRIO > 0) && (IRQ_ADCCG0_SR0_CAT == IRQ_CAT1))
extern void ADCCG0SR0_ISR(void);
#endif /*((IRQ_ADCCG0_SR0_PRIO > 0) && (IRQ_ADCCG0_SR0_CAT == IRQ_CAT1)) */

#else
/******************************************************************************
** Syntax :          void ADCCG0SR2_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_ADCCG0_SR2_PRIO > 0) && (IRQ_ADCCG0_SR2_CAT == IRQ_CAT1))
extern void ADCCG0SR2_ISR(void);
#endif /*((IRQ_ADCCG0_SR2_PRIO > 0) && (IRQ_ADCCG0_SR2_CAT == IRQ_CAT1)) */

#endif /* (IRQ_ADC_CG0SR0_USED_FOR_BGNDRS == STD_ON) */

#endif /* (IRQ_ADCCG0_EXIST == STD_ON) */

#endif /* (IRQ_ADC_EXIST == STD_ON) */

#if (IRQ_SENT_EXIST == STD_ON)
/******************************************************************************
** Syntax : void SENTSR0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 0                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR0_ISR(void);

/******************************************************************************
** Syntax : void SENTSR1_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 1                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR1_ISR(void);

/******************************************************************************
** Syntax : void SENTSR2_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 2                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR2_ISR(void);

/******************************************************************************
** Syntax : void SENTSR3_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 3                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR3_ISR(void);

/******************************************************************************
** Syntax : void SENTSR4_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 4                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR4_ISR(void);

/******************************************************************************
** Syntax : void SENTSR5_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 5                          **
**                                                                           **
*****************************************************************************/
extern void SENTSR5_ISR(void);

/******************************************************************************
** Syntax : void SENTSR6_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 6                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR6_ISR(void);

/******************************************************************************
** Syntax : void SENTSR7_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 7                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR7_ISR(void);

/******************************************************************************
** Syntax : void SENTSR8_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 8                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR8_ISR(void);

/******************************************************************************
** Syntax : void SENTSR9_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 9                           **
**                                                                           **
*****************************************************************************/
extern void SENTSR9_ISR(void);

/******************************************************************************
** Syntax : void SENTSR10_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 10                          **
**                                                                           **
*****************************************************************************/
extern void SENTSR10_ISR(void);

/******************************************************************************
** Syntax : void SENTSR11_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 11                          **
**                                                                           **
*****************************************************************************/
extern void SENTSR11_ISR(void);

/******************************************************************************
** Syntax : void SENTSR12_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 12                          **
**                                                                           **
*****************************************************************************/
extern void SENTSR12_ISR(void);

/******************************************************************************
** Syntax : void SENTSR13_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 13                          **
**                                                                           **
*****************************************************************************/
extern void SENTSR13_ISR(void);

/******************************************************************************
** Syntax : void SENTSR14_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 14                          **
**                                                                           **
*****************************************************************************/
extern void SENTSR14_ISR(void);
#endif /* (IRQ_SENT_EXIST == STD_ON) */
#endif /* (IFX_MCAL_USED == STD_ON) */

#if (IRQ_STM_EXIST == STD_ON)
#if (IRQ_STM0_EXIST == STD_ON)
/******************************************************************************
** Syntax : void STM0SRN0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for STM0 Interrupt Node 0                           **
**                                                                           **
*****************************************************************************/
extern void STM0SRN0_ISR(void);

/******************************************************************************
** Syntax : void STM0SRN1_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for STM0 Interrupt Node 1                           **
**                                                                           **
*****************************************************************************/
extern void STM0SRN1_ISR(void);
#endif /* (IRQ_STM0_EXIST == STD_ON)  */

#if (IRQ_STM1_EXIST == STD_ON)
/******************************************************************************
** Syntax : void STM1SRN0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for STM1 Interrupt Node 0                           **
**                                                                           **
*****************************************************************************/
extern void STM1SRN0_ISR(void);

/******************************************************************************
** Syntax : void STM1SRN1_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for STM1 Interrupt Node 1                           **
**                                                                           **
*****************************************************************************/
extern void STM1SRN1_ISR(void);
#endif /* (IRQ_STM1_EXIST == STD_ON)  */

#if (IRQ_STM2_EXIST == STD_ON)
/******************************************************************************
** Syntax : void STM2SRN0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for STM2 Interrupt Node 0                           **
**                                                                           **
*****************************************************************************/
extern void STM2SRN0_ISR(void);

/******************************************************************************
** Syntax : void STM2SRN1_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for STM2 Interrupt Node 1                           **
**                                                                           **
*****************************************************************************/
extern void STM2SRN1_ISR(void);
#endif /* (IRQ_STM2_EXIST == STD_ON)  */

#endif /* (IRQ_STM_EXIST == STD_ON)  */

#if (IFX_MCAL_USED == STD_ON)
#if (IRQ_CAN_EXIST == STD_ON)
/******************************************************************************
** Syntax : void CANSR0_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 0 Busoff event                   **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN0_EXIST == STD_ON)
#if((IRQ_CAN_SR0_PRIO > 0) && (IRQ_CAN_SR0_CAT == IRQ_CAT1))
extern void CANSR0_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR1_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 1 Busoff event                   **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN1_EXIST == STD_ON)
#if((IRQ_CAN_SR1_PRIO > 0) && (IRQ_CAN_SR1_CAT == IRQ_CAT1))
extern void CANSR1_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR2_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 2 Busoff event                   **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN2_EXIST == STD_ON)
#if((IRQ_CAN_SR2_PRIO > 0) && (IRQ_CAN_SR2_CAT == IRQ_CAT1))
extern void CANSR2_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR3_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 3 Busoff event                   **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN3_EXIST == STD_ON)
#if((IRQ_CAN_SR3_PRIO > 0) && (IRQ_CAN_SR3_CAT == IRQ_CAT1))
extern void CANSR3_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR4_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 0 Reception/Wakeup event         **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN4_EXIST == STD_ON)
#if((IRQ_CAN_SR4_PRIO > 0) && (IRQ_CAN_SR4_CAT == IRQ_CAT1))
extern void CANSR4_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR5_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 1 Reception/Wakeup event         **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN5_EXIST == STD_ON)
#if((IRQ_CAN_SR5_PRIO > 0) && (IRQ_CAN_SR5_CAT == IRQ_CAT1))
extern void CANSR5_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR6_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 2 Reception/Wakeup event         **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN6_EXIST == STD_ON)
#if((IRQ_CAN_SR6_PRIO > 0) && (IRQ_CAN_SR6_CAT == IRQ_CAT1))
extern void CANSR6_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR7_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 3 Reception/Wakeup event         **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN7_EXIST == STD_ON)
#if((IRQ_CAN_SR7_PRIO > 0) && (IRQ_CAN_SR7_CAT == IRQ_CAT1))
extern void CANSR7_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR8_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 0 Transmission event             **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN8_EXIST == STD_ON)
#if((IRQ_CAN_SR8_PRIO > 0) && (IRQ_CAN_SR8_CAT == IRQ_CAT1))
extern void CANSR8_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR9_ISR(void)                                            **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 1 Transmission event             **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN9_EXIST == STD_ON)
#if((IRQ_CAN_SR9_PRIO > 0) && (IRQ_CAN_SR9_CAT == IRQ_CAT1))
extern void CANSR9_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR10_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 2 Transmission event             **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN10_EXIST == STD_ON)
#if((IRQ_CAN_SR10_PRIO > 0) && (IRQ_CAN_SR10_CAT == IRQ_CAT1))
extern void CANSR10_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR11_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 3 Transmission event             **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN11_EXIST == STD_ON)
#if((IRQ_CAN_SR11_PRIO > 0) && (IRQ_CAN_SR11_CAT == IRQ_CAT1))
extern void CANSR11_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR16_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 4 Busoff event                   **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN16_EXIST == STD_ON)
#if((IRQ_CAN_SR16_PRIO > 0) && (IRQ_CAN_SR16_CAT == IRQ_CAT1))
extern void CANSR16_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR17_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 5 Busoff event                   **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN17_EXIST == STD_ON)
#if((IRQ_CAN_SR17_PRIO > 0) && (IRQ_CAN_SR17_CAT == IRQ_CAT1))
extern void CANSR17_ISR(void);
#endif
#endif


/******************************************************************************
** Syntax : void CANSR18_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 4 Reception/Wakeup event         **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN18_EXIST == STD_ON)
#if((IRQ_CAN_SR18_PRIO > 0) && (IRQ_CAN_SR18_CAT == IRQ_CAT1))
extern void CANSR18_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR19_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 5 Reception/Wakeup event         **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN19_EXIST == STD_ON)
#if((IRQ_CAN_SR19_PRIO > 0) && (IRQ_CAN_SR19_CAT == IRQ_CAT1))
extern void CANSR19_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR20_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 4 Transmission event             **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN20_EXIST == STD_ON)
#if((IRQ_CAN_SR20_PRIO > 0) && (IRQ_CAN_SR20_CAT == IRQ_CAT1))
extern void CANSR20_ISR(void);
#endif
#endif

/******************************************************************************
** Syntax : void CANSR21_ISR(void)                                           **
**                                                                           **
** Service ID: NA                                                            **
**                                                                           **
** Sync/Async: Synchronous                                                   **
**                                                                           **
** Reentrancy: Reentrant                                                     **
**                                                                           **
** Parameters (in): None                                                     **
**                                                                           **
** Parameters (out): None                                                    **
**                                                                           **
** Return value: None                                                        **
**                                                                           **
** Description : Service for CAN Controller 5 Transmission event             **
**                                                                           **
******************************************************************************/
#if (IRQ_CAN21_EXIST == STD_ON)
#if((IRQ_CAN_SR21_PRIO > 0) && (IRQ_CAN_SR21_CAT == IRQ_CAT1))
extern void CANSR21_ISR(void);
#endif
#endif

#endif /* (IRQ_CAN_EXIST == STD_ON)  */

/*******************************************************************************
** Syntax :  void IrqI2c_Init(void)                                           **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To set the interrupt priority for                            **
**               service nodes according to priority configurartion.          **
**                                                                            **
*******************************************************************************/

extern void IrqI2c_Init(void);

#if (IRQ_I2C0_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void I2C0BREQ_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C0_BREQ_PRIO > 0) && (IRQ_I2C0_BREQ_CAT == IRQ_CAT1))
extern void I2C0BREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C0LBREQ_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C0_LBREQ_PRIO > 0) && (IRQ_I2C0_LBREQ_CAT == IRQ_CAT1))
extern void I2C0LBREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C0SREQ_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C0_SREQ_PRIO > 0) && (IRQ_I2C0_SREQ_CAT == IRQ_CAT1))
extern void I2C0SREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C0LSREQ_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C0_LSREQ_PRIO > 0) && (IRQ_I2C0_LSREQ_CAT == IRQ_CAT1))
extern void I2C0LSREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C0ERR_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C0_ERR_PRIO > 0) && (IRQ_I2C0_ERR_CAT == IRQ_CAT1))
extern void I2C0ERR_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C0P_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C0_P_PRIO > 0) && (IRQ_I2C0_P_CAT == IRQ_CAT1))
extern void I2C0P_ISR(void);
#endif

#endif /* (IRQ_I2C0_EXIST == STD_ON) */

#if (IRQ_I2C1_EXIST == STD_ON)
/******************************************************************************
** Syntax :          void I2C1BREQ_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C1_BREQ_PRIO > 0) && (IRQ_I2C1_BREQ_CAT == IRQ_CAT1))
extern void I2C1BREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C1LBREQ_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C1_LBREQ_PRIO > 0) && (IRQ_I2C1_LBREQ_CAT == IRQ_CAT1))
extern void I2C1LBREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C1SREQ_ISR(void)                                 **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C1_SREQ_PRIO > 0) && (IRQ_I2C1_SREQ_CAT == IRQ_CAT1))
extern void I2C1SREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C1LSREQ_ISR(void)                                **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C1_LSREQ_PRIO > 0) && (IRQ_I2C1_LSREQ_CAT == IRQ_CAT1))
extern void I2C1LSREQ_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C1ERR_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C1_ERR_PRIO > 0) && (IRQ_I2C1_ERR_CAT == IRQ_CAT1))
extern void I2C1ERR_ISR(void);
#endif

/******************************************************************************
** Syntax :          void I2C1P_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on I2C Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if((IRQ_I2C1_P_PRIO > 0) && (IRQ_I2C1_P_CAT == IRQ_CAT1))
extern void I2C1P_ISR(void);
#endif

#endif /* (IRQ_I2C1_EXIST == STD_ON) */

/*******************************************************************************
** Syntax :  void Irq_ClearAllInterruptFlags(void)                            **
**                                                                            **
** Service ID:  none                                                          **
**                                                                            **
** Sync/Async:  Synchronous                                                   **
**                                                                            **
** Reentrancy:  non reentrant                                                 **
**                                                                            **
** Parameters (in): none                                                      **
**                                                                            **
** Parameters (out): none                                                     **
**                                                                            **
** Return value: none                                                         **
**                                                                            **
** Description : To clear all SRR and corresponding SRE bits.This ensures     **
** Standby mode can be entered if no pending interrupts are available.        **
**                                                                            **
*******************************************************************************/
extern void Irq_ClearAllInterruptFlags(void);

#endif /* (IFX_MCAL_USED == STD_ON) */

#if (IFX_MCAL_USED == STD_ON)
#define IRQ_STOP_SEC_CODE
#include "MemMap.h"
#else
#define IFX_IRQ_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
#endif

/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/

#endif /* IRQ_H */
