/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : CanIf_Cbk.c $                                              **
**                                                                           **
**  $CC VERSION : \main\9 $                                                  **
**                                                                           **
**  $DATE       : 2013-06-20 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : Implementation of CAN interface callback functions for     **
**                module testing                                             **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Mcal.h"
//#include "../../LIB/Common.h"
#include "CanIf_Cbk.h"
//#include "../../BSW/Com/SRC/CanChk.h"
#include "Can_17_MCanP_Cfg.h"
#include "Can_17_MCanP.h"
//#include "../../BSW/Com/SRC/CanDecoding.h"

#ifdef ECUM_USES_CAN
/* Macro to set MSB for Extended messages */
#define CAN_EXTENDED_MSB_SET            (0x80000000U)

void CanIf_TxConfirmation (PduIdType CanTxPduId);

uint8 Test_RxConfirmCount;
uint8 Test_TxConfirmCount;

uint8 Can_Main_BusOff_cnt;
uint8 Can_Sub_BusOff_cnt;

/*******************************************************************************
                      CanIf_RxIndication
*******************************************************************************/
#if 0 //PJS
void CanIf_RxIndication(Can_HwHandleType Hrh,
                        Can_IdType CanId,
                        uint8 CanDlc, 
                        const uint8 *CanSduPtr)
{
  //Test_RxConfirmCount++ ;
	;
}
#endif //PJS

/*******************************************************************************
                      CanIf_TxConfirmation
*******************************************************************************/
void CanIf_TxConfirmation (PduIdType CanTxPduId)
{
  Test_TxConfirmCount++ ;
}

/*******************************************************************************
                      CanIf_ControllerBusOff
*******************************************************************************/
void CanIf_ControllerBusOff(uint8 ControllerId)
{

	if (ControllerId == Can_17_MCanPConf_CanController_CanController_0)
	{
		//ccu1MainBusOffFlag = 1;
		//Can_Main_BusOff_cnt++;
		Can_17_MCanP_SetControllerMode(Can_17_MCanPConf_CanController_CanController_0, CAN_T_START); // �߰� 0813 PJS
	}

	else if (ControllerId == Can_17_MCanPConf_CanController_CanController_1)
	{
		//ccu1SenBusOffFlag = 1;
		//Can_Sub_BusOff_cnt++;
		Can_17_MCanP_SetControllerMode(Can_17_MCanPConf_CanController_CanController_1, CAN_T_START); // �߰� 0813 PJS
	}

	else
	{
	;
	}

  ;
}

/*******************************************************************************
                    CanIf_ControllerModeIndication
*******************************************************************************/
void CanIf_ControllerModeIndication( uint8 ControllerId,
                                     CanIf_ControllerModeType ControllerMode )
{
	;
}

/*******************************************************************************
                  CAN L-PDU Rx Callout Function Definition
*******************************************************************************/
boolean Appl_LPduRxCalloutFunction(uint8 Hrh,
                                   Can_IdType CanId,
                                   uint8 CanDlc,
                                   const uint8 *CanSduPtr)
{
  UNUSED_PARAMETER (Hrh)
  UNUSED_PARAMETER (CanId)
  UNUSED_PARAMETER (CanDlc)
  UNUSED_PARAMETER (CanSduPtr)

  return(TRUE);
}
#endif
