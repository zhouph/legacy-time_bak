#define S_FUNCTION_NAME      Spim_Rx1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          2
#define WidthOutputPort         392

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Spim_Rx1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Spim_Rx1msFuncInhibitSpimSts = input[0];
    Spim_Rx1msEcuModeSts = input[1];

    Spim_Rx1ms();


    output[0] = Spim_Rx1msRxAsicInfo.PdMtCfg;
    output[1] = Spim_Rx1msRxAsicInfo.HpdSr;
    output[2] = Spim_Rx1msRxAsicInfo.RstWd;
    output[3] = Spim_Rx1msRxAsicInfo.RstAlu;
    output[4] = Spim_Rx1msRxAsicInfo.RstExt;
    output[5] = Spim_Rx1msRxAsicInfo.RstClk;
    output[6] = Spim_Rx1msRxAsicInfo.VintUv;
    output[7] = Spim_Rx1msRxAsicInfo.Vcc5Uv;
    output[8] = Spim_Rx1msRxAsicInfo.DosvUv;
    output[9] = Spim_Rx1msRxAsicInfo.Version;
    output[10] = Spim_Rx1msRxAsicInfo.E5vsOcCfg;
    output[11] = Spim_Rx1msRxAsicInfo.LdactDis;
    output[12] = Spim_Rx1msRxAsicInfo.LsdSinkDis;
    output[13] = Spim_Rx1msRxAsicInfo.Ichar;
    output[14] = Spim_Rx1msRxAsicInfo.Pchar;
    output[15] = Spim_Rx1msRxAsicInfo.ManufacturingData;
    output[16] = Spim_Rx1msRxAsicInfo.Ws4Ot;
    output[17] = Spim_Rx1msRxAsicInfo.Ws4Op;
    output[18] = Spim_Rx1msRxAsicInfo.Ws4Oc;
    output[19] = Spim_Rx1msRxAsicInfo.Ws3Ot;
    output[20] = Spim_Rx1msRxAsicInfo.Ws3Op;
    output[21] = Spim_Rx1msRxAsicInfo.Ws3Oc;
    output[22] = Spim_Rx1msRxAsicInfo.Ws2Ot;
    output[23] = Spim_Rx1msRxAsicInfo.Ws2Op;
    output[24] = Spim_Rx1msRxAsicInfo.Ws2Oc;
    output[25] = Spim_Rx1msRxAsicInfo.Ws1Ot;
    output[26] = Spim_Rx1msRxAsicInfo.Ws1Op;
    output[27] = Spim_Rx1msRxAsicInfo.Ws1Oc;
    output[28] = Spim_Rx1msRxAsicInfo.OtwOv;
    output[29] = Spim_Rx1msRxAsicInfo.Fgnd;
    output[30] = Spim_Rx1msRxAsicInfo.VpwrUv;
    output[31] = Spim_Rx1msRxAsicInfo.Ld;
    output[32] = Spim_Rx1msRxAsicInfo.VpwrOv;
    output[33] = Spim_Rx1msRxAsicInfo.AsicClkCnt;
    output[34] = Spim_Rx1msRxAsicInfo.Temp;
    output[35] = Spim_Rx1msRxAsicInfo.VsoSel;
    output[36] = Spim_Rx1msRxAsicInfo.VsoS;
    output[37] = Spim_Rx1msRxAsicInfo.FvpwrAct;
    output[38] = Spim_Rx1msRxAsicInfo.VintD;
    output[39] = Spim_Rx1msRxAsicInfo.HdOc;
    output[40] = Spim_Rx1msRxAsicInfo.PdOc;
    output[41] = Spim_Rx1msRxAsicInfo.Vpre10;
    output[42] = Spim_Rx1msRxAsicInfo.VdsWld;
    output[43] = Spim_Rx1msRxAsicInfo.WldOt;
    output[44] = Spim_Rx1msRxAsicInfo.WldOp;
    output[45] = Spim_Rx1msRxAsicInfo.WldOc;
    output[46] = Spim_Rx1msRxAsicInfo.Vpre12;
    output[47] = Spim_Rx1msRxAsicInfo.CrDis34;
    output[48] = Spim_Rx1msRxAsicInfo.CrDis12;
    output[49] = Spim_Rx1msRxAsicInfo.CrFb;
    output[50] = Spim_Rx1msRxAsicInfo.VcpVpwr;
    output[51] = Spim_Rx1msRxAsicInfo.Lsd4Crer;
    output[52] = Spim_Rx1msRxAsicInfo.Lsd3Crer;
    output[53] = Spim_Rx1msRxAsicInfo.Lsd2Crer;
    output[54] = Spim_Rx1msRxAsicInfo.Lsd1Crer;
    output[55] = Spim_Rx1msRxAsicInfo.VintA;
    output[56] = Spim_Rx1msRxAsicInfo.Isopos4;
    output[57] = Spim_Rx1msRxAsicInfo.Isopos3;
    output[58] = Spim_Rx1msRxAsicInfo.Isopos2;
    output[59] = Spim_Rx1msRxAsicInfo.Isopos1;
    output[60] = Spim_Rx1msRxAsicInfo.Lsd1Duty;
    output[61] = Spim_Rx1msRxAsicInfo.VdsLsd1;
    output[62] = Spim_Rx1msRxAsicInfo.Lsd1Ot;
    output[63] = Spim_Rx1msRxAsicInfo.Lsd1Op;
    output[64] = Spim_Rx1msRxAsicInfo.Lsd1Oc;
    output[65] = Spim_Rx1msRxAsicInfo.Lsd2Duty;
    output[66] = Spim_Rx1msRxAsicInfo.VdsLsd2;
    output[67] = Spim_Rx1msRxAsicInfo.Lsd2Ot;
    output[68] = Spim_Rx1msRxAsicInfo.Lsd2Op;
    output[69] = Spim_Rx1msRxAsicInfo.Lsd2Oc;
    output[70] = Spim_Rx1msRxAsicInfo.Lsd3Duty;
    output[71] = Spim_Rx1msRxAsicInfo.VdsLsd3;
    output[72] = Spim_Rx1msRxAsicInfo.Lsd3Ot;
    output[73] = Spim_Rx1msRxAsicInfo.Lsd3Op;
    output[74] = Spim_Rx1msRxAsicInfo.Lsd3Oc;
    output[75] = Spim_Rx1msRxAsicInfo.Lsd4Duty;
    output[76] = Spim_Rx1msRxAsicInfo.VdsLsd4;
    output[77] = Spim_Rx1msRxAsicInfo.Lsd4Ot;
    output[78] = Spim_Rx1msRxAsicInfo.Lsd4Op;
    output[79] = Spim_Rx1msRxAsicInfo.Lsd4Oc;
    output[80] = Spim_Rx1msRxAsicInfo.IsokOt;
    output[81] = Spim_Rx1msRxAsicInfo.IsokOc;
    output[82] = Spim_Rx1msRxAsicInfo.Lsd5Duty;
    output[83] = Spim_Rx1msRxAsicInfo.VdsLsd5;
    output[84] = Spim_Rx1msRxAsicInfo.Lsd5Ot;
    output[85] = Spim_Rx1msRxAsicInfo.Lsd5Op;
    output[86] = Spim_Rx1msRxAsicInfo.Lsd5Oc;
    output[87] = Spim_Rx1msRxAsicInfo.IsoNeg2;
    output[88] = Spim_Rx1msRxAsicInfo.IsoNeg1;
    output[89] = Spim_Rx1msRxAsicInfo.Lsd6Duty;
    output[90] = Spim_Rx1msRxAsicInfo.VdsLsd6;
    output[91] = Spim_Rx1msRxAsicInfo.Lsd6Ot;
    output[92] = Spim_Rx1msRxAsicInfo.Lsd6Op;
    output[93] = Spim_Rx1msRxAsicInfo.Lsd6Oc;
    output[94] = Spim_Rx1msRxAsicInfo.IsoNeg4;
    output[95] = Spim_Rx1msRxAsicInfo.IsoNeg3;
    output[96] = Spim_Rx1msRxAsicInfo.Lsd7Duty;
    output[97] = Spim_Rx1msRxAsicInfo.VdsLsd7;
    output[98] = Spim_Rx1msRxAsicInfo.Lsd7Ot;
    output[99] = Spim_Rx1msRxAsicInfo.Lsd7Op;
    output[100] = Spim_Rx1msRxAsicInfo.Lsd7Oc;
    output[101] = Spim_Rx1msRxAsicInfo.Lsd8Duty;
    output[102] = Spim_Rx1msRxAsicInfo.VdsLsd8;
    output[103] = Spim_Rx1msRxAsicInfo.Lsd8Ot;
    output[104] = Spim_Rx1msRxAsicInfo.Lsd8Op;
    output[105] = Spim_Rx1msRxAsicInfo.Lsd8Oc;
    output[106] = Spim_Rx1msRxAsicInfo.Ar;
    output[107] = Spim_Rx1msRxAsicInfo.ErrCnt;
    output[108] = Spim_Rx1msRxAsicInfo.HdLkg;
    output[109] = Spim_Rx1msRxAsicInfo.E5vsOc;
    output[110] = Spim_Rx1msRxAsicInfo.E5vsOuv;
    output[111] = Spim_Rx1msRxAsicInfo.WsS;
    output[112] = Spim_Rx1msRxAsicInfo.WsaiS;
    output[113] = Spim_Rx1msRxAsicInfo.WsCntOv;
    output[114] = Spim_Rx1msRxAsicInfo.WsCnt;
    output[115] = Spim_Rx1msRxAsicInfo.Ws1Notlegal;
    output[116] = Spim_Rx1msRxAsicInfo.Ws1Fail;
    output[117] = Spim_Rx1msRxAsicInfo.Ws1Stop;
    output[118] = Spim_Rx1msRxAsicInfo.Ws1Data;
    output[119] = Spim_Rx1msRxAsicInfo.Ws1Lkg;
    output[120] = Spim_Rx1msRxAsicInfo.Ws1Notlegalbits;
    output[121] = Spim_Rx1msRxAsicInfo.Ws1Codeerror;
    output[122] = Spim_Rx1msRxAsicInfo.Ws1manchesterdecodingresult;
    output[123] = Spim_Rx1msRxAsicInfo.Ws2Notlegal;
    output[124] = Spim_Rx1msRxAsicInfo.Ws2Fail;
    output[125] = Spim_Rx1msRxAsicInfo.Ws2Stop;
    output[126] = Spim_Rx1msRxAsicInfo.Ws2Data;
    output[127] = Spim_Rx1msRxAsicInfo.Ws2Lkg;
    output[128] = Spim_Rx1msRxAsicInfo.Ws2Notlegalbits;
    output[129] = Spim_Rx1msRxAsicInfo.Ws2Codeerror;
    output[130] = Spim_Rx1msRxAsicInfo.Ws2manchesterdecodingresult;
    output[131] = Spim_Rx1msRxAsicInfo.Ws3Notlegal;
    output[132] = Spim_Rx1msRxAsicInfo.Ws3Fail;
    output[133] = Spim_Rx1msRxAsicInfo.Ws3Stop;
    output[134] = Spim_Rx1msRxAsicInfo.Ws3Data;
    output[135] = Spim_Rx1msRxAsicInfo.Ws3Lkg;
    output[136] = Spim_Rx1msRxAsicInfo.Ws3Notlegalbits;
    output[137] = Spim_Rx1msRxAsicInfo.Ws3Codeerror;
    output[138] = Spim_Rx1msRxAsicInfo.Ws3manchesterdecodingresult;
    output[139] = Spim_Rx1msRxAsicInfo.Ws4Notlegal;
    output[140] = Spim_Rx1msRxAsicInfo.Ws4Fail;
    output[141] = Spim_Rx1msRxAsicInfo.Ws4Stop;
    output[142] = Spim_Rx1msRxAsicInfo.Ws4Data;
    output[143] = Spim_Rx1msRxAsicInfo.Ws4Lkg;
    output[144] = Spim_Rx1msRxAsicInfo.Ws4Notlegalbits;
    output[145] = Spim_Rx1msRxAsicInfo.Ws4Codeerror;
    output[146] = Spim_Rx1msRxAsicInfo.Ws4manchesterdecodingresult;
    output[147] = Spim_Rx1msRxAsicInfo.AdRst1;
    output[148] = Spim_Rx1msRxAsicInfo.VdsVpo1;
    output[149] = Spim_Rx1msRxAsicInfo.Vpo1Ot;
    output[150] = Spim_Rx1msRxAsicInfo.Vpo1Op;
    output[151] = Spim_Rx1msRxAsicInfo.Vpo1Oc;
    output[152] = Spim_Rx1msRxAsicInfo.AdRst2;
    output[153] = Spim_Rx1msRxAsicInfo.VdsVpo2;
    output[154] = Spim_Rx1msRxAsicInfo.Vpo2Ot;
    output[155] = Spim_Rx1msRxAsicInfo.Vpo2Op;
    output[156] = Spim_Rx1msRxAsicInfo.Vpo2Oc;
    output[157] = Spim_Rx1msRxAsicInfo.AdRst3;
    output[158] = Spim_Rx1msRxAsicInfo.VdsVso;
    output[159] = Spim_Rx1msRxAsicInfo.VsoOt;
    output[160] = Spim_Rx1msRxAsicInfo.VsoOp;
    output[161] = Spim_Rx1msRxAsicInfo.VsoOc;
    output[162] = Spim_Rx1msRxAsicInfo.Fmsg0;
    output[163] = Spim_Rx1msRxAsicInfo.Fmsg1;
    output[164] = Spim_Rx1msRxAsicInfo.Fmsg2;
    output[165] = Spim_Rx1msRxAsicInfo.Fmsg3;
    output[166] = Spim_Rx1msRxAsicInfo.Fmsg4;
    output[167] = Spim_Rx1msRxAsicInfo.Fmsg5;
    output[168] = Spim_Rx1msRxAsicInfo.Fmsg6;
    output[169] = Spim_Rx1msRxAsicInfo.Fmsg7;
    output[170] = Spim_Rx1msRxAsicInfo.Fmsg8;
    output[171] = Spim_Rx1msRxAsicInfo.Fmsg9;
    output[172] = Spim_Rx1msRxAsicInfo.Fmsg10;
    output[173] = Spim_Rx1msRxAsicInfo.Fmsg11;
    output[174] = Spim_Rx1msRxAsicInfo.Fmsg12;
    output[175] = Spim_Rx1msRxAsicInfo.Fmsg13;
    output[176] = Spim_Rx1msRxAsicInfo.Fmsg14;
    output[177] = Spim_Rx1msRxAsicInfo.Fmsg15;
    output[178] = Spim_Rx1msRxAsicInfo.Fmsg16;
    output[179] = Spim_Rx1msRxAsicInfo.Fmsg17;
    output[180] = Spim_Rx1msRxAsicInfo.Fmsg18;
    output[181] = Spim_Rx1msRxAsicInfo.Fmsg19;
    output[182] = Spim_Rx1msRxAsicInfo.Fmsg20;
    output[183] = Spim_Rx1msRxAsicInfo.Fmsg21;
    output[184] = Spim_Rx1msRxAsicInfo.Fmsg22;
    output[185] = Spim_Rx1msRxAsicInfo.Fmsg23;
    output[186] = Spim_Rx1msRxAsicInfo.Fmsg24;
    output[187] = Spim_Rx1msRxAsicInfo.Fmsg25;
    output[188] = Spim_Rx1msRxAsicInfo.Fmsg26;
    output[189] = Spim_Rx1msRxMpsInfo.Mps1AngleP;
    output[190] = Spim_Rx1msRxMpsInfo.Mps1AngleEf;
    output[191] = Spim_Rx1msRxMpsInfo.Mps1ErrorP;
    output[192] = Spim_Rx1msRxMpsInfo.Mps1Batd;
    output[193] = Spim_Rx1msRxMpsInfo.Mps1Magm;
    output[194] = Spim_Rx1msRxMpsInfo.Mps1Ierr;
    output[195] = Spim_Rx1msRxMpsInfo.Mps1Trno;
    output[196] = Spim_Rx1msRxMpsInfo.Mps1Tmp;
    output[197] = Spim_Rx1msRxMpsInfo.Mps1Eep1;
    output[198] = Spim_Rx1msRxMpsInfo.Mps1Eep2;
    output[199] = Spim_Rx1msRxMpsInfo.Mps1ErrorEf;
    output[200] = Spim_Rx1msRxMpsInfo.Mps1ControlP;
    output[201] = Spim_Rx1msRxMpsInfo.Mps1Erst;
    output[202] = Spim_Rx1msRxMpsInfo.Mps1Pwr;
    output[203] = Spim_Rx1msRxMpsInfo.Mps1Rpm;
    output[204] = Spim_Rx1msRxMpsInfo.Mps1ControlEf;
    output[205] = Spim_Rx1msRxMpsInfo.Mps2AngleP;
    output[206] = Spim_Rx1msRxMpsInfo.Mps2AngleEf;
    output[207] = Spim_Rx1msRxMpsInfo.Mps2ErrorP;
    output[208] = Spim_Rx1msRxMpsInfo.Mps2Batd;
    output[209] = Spim_Rx1msRxMpsInfo.Mps2Magm;
    output[210] = Spim_Rx1msRxMpsInfo.Mps2Ierr;
    output[211] = Spim_Rx1msRxMpsInfo.Mps2Trno;
    output[212] = Spim_Rx1msRxMpsInfo.Mps2Tmp;
    output[213] = Spim_Rx1msRxMpsInfo.Mps2Eep1;
    output[214] = Spim_Rx1msRxMpsInfo.Mps2Eep2;
    output[215] = Spim_Rx1msRxMpsInfo.Mps2ErrorEf;
    output[216] = Spim_Rx1msRxMpsInfo.Mps2ControlP;
    output[217] = Spim_Rx1msRxMpsInfo.Mps2Erst;
    output[218] = Spim_Rx1msRxMpsInfo.Mps2Pwr;
    output[219] = Spim_Rx1msRxMpsInfo.Mps2Rpm;
    output[220] = Spim_Rx1msRxMpsInfo.Mps2ControlEf;
    output[221] = Spim_Rx1msRxVlvdInfo.C0ol;
    output[222] = Spim_Rx1msRxVlvdInfo.C0sb;
    output[223] = Spim_Rx1msRxVlvdInfo.C0sg;
    output[224] = Spim_Rx1msRxVlvdInfo.C1ol;
    output[225] = Spim_Rx1msRxVlvdInfo.C1sb;
    output[226] = Spim_Rx1msRxVlvdInfo.C1sg;
    output[227] = Spim_Rx1msRxVlvdInfo.C2ol;
    output[228] = Spim_Rx1msRxVlvdInfo.C2sb;
    output[229] = Spim_Rx1msRxVlvdInfo.C2sg;
    output[230] = Spim_Rx1msRxVlvdInfo.Fr;
    output[231] = Spim_Rx1msRxVlvdInfo.Ot;
    output[232] = Spim_Rx1msRxVlvdInfo.Lr;
    output[233] = Spim_Rx1msRxVlvdInfo.Uv;
    output[234] = Spim_Rx1msRxVlvdInfo.Ff;
    output[235] = Spim_Rx1msRxRegInfo.RevMinor;
    output[236] = Spim_Rx1msRxRegInfo.RevMajor;
    output[237] = Spim_Rx1msRxRegInfo.Id;
    output[238] = Spim_Rx1msRxRegInfo.Ign;
    output[239] = Spim_Rx1msRxRegInfo.CanwuL;
    output[240] = Spim_Rx1msRxRegInfo.NmaskVdd1UvOv;
    output[241] = Spim_Rx1msRxRegInfo.Vdd35Sel;
    output[242] = Spim_Rx1msRxRegInfo.PostRunRst;
    output[243] = Spim_Rx1msRxRegInfo.MaskVbatpOv;
    output[244] = Spim_Rx1msRxRegInfo.EnVdd5Ot;
    output[245] = Spim_Rx1msRxRegInfo.EnVdd35Ot;
    output[246] = Spim_Rx1msRxRegInfo.BgErr1;
    output[247] = Spim_Rx1msRxRegInfo.BgErr2;
    output[248] = Spim_Rx1msRxRegInfo.AvddVmonErr;
    output[249] = Spim_Rx1msRxRegInfo.Vcp12Uv;
    output[250] = Spim_Rx1msRxRegInfo.Vcp12Ov;
    output[251] = Spim_Rx1msRxRegInfo.Vcp17Ov;
    output[252] = Spim_Rx1msRxRegInfo.VpatpUv;
    output[253] = Spim_Rx1msRxRegInfo.VbatpOv;
    output[254] = Spim_Rx1msRxRegInfo.Vdd1Uv;
    output[255] = Spim_Rx1msRxRegInfo.Vdd1Ov;
    output[256] = Spim_Rx1msRxRegInfo.Vdd35Uv;
    output[257] = Spim_Rx1msRxRegInfo.Vdd35Ov;
    output[258] = Spim_Rx1msRxRegInfo.Vdd5Uv;
    output[259] = Spim_Rx1msRxRegInfo.Vdd5Ov;
    output[260] = Spim_Rx1msRxRegInfo.Vdd6Uv;
    output[261] = Spim_Rx1msRxRegInfo.Vdd6Ov;
    output[262] = Spim_Rx1msRxRegInfo.Vdd35Ot;
    output[263] = Spim_Rx1msRxRegInfo.Vdd5Ot;
    output[264] = Spim_Rx1msRxRegInfo.Vsout1Ot;
    output[265] = Spim_Rx1msRxRegInfo.Vsout1Ilim;
    output[266] = Spim_Rx1msRxRegInfo.Vsout1Ov;
    output[267] = Spim_Rx1msRxRegInfo.Vsout1Uv;
    output[268] = Spim_Rx1msRxRegInfo.Vdd35Ilim;
    output[269] = Spim_Rx1msRxRegInfo.Vdd5Ilim;
    output[270] = Spim_Rx1msRxRegInfo.WdFailCnt;
    output[271] = Spim_Rx1msRxRegInfo.EeCrcErr;
    output[272] = Spim_Rx1msRxRegInfo.CfgCrcErr;
    output[273] = Spim_Rx1msRxRegInfo.AbistRun;
    output[274] = Spim_Rx1msRxRegInfo.LbistRun;
    output[275] = Spim_Rx1msRxRegInfo.AbistUvovErr;
    output[276] = Spim_Rx1msRxRegInfo.LbistErr;
    output[277] = Spim_Rx1msRxRegInfo.NresErr;
    output[278] = Spim_Rx1msRxRegInfo.TrimErrVmon;
    output[279] = Spim_Rx1msRxRegInfo.EndrvErr;
    output[280] = Spim_Rx1msRxRegInfo.WdErr;
    output[281] = Spim_Rx1msRxRegInfo.McuErr;
    output[282] = Spim_Rx1msRxRegInfo.Loclk;
    output[283] = Spim_Rx1msRxRegInfo.SpiErr;
    output[284] = Spim_Rx1msRxRegInfo.Fsm;
    output[285] = Spim_Rx1msRxRegInfo.CfgLock;
    output[286] = Spim_Rx1msRxRegInfo.SafeLockThr;
    output[287] = Spim_Rx1msRxRegInfo.SafeTo;
    output[288] = Spim_Rx1msRxRegInfo.AbistEn;
    output[289] = Spim_Rx1msRxRegInfo.LbistEn;
    output[290] = Spim_Rx1msRxRegInfo.EeCrcChk;
    output[291] = Spim_Rx1msRxRegInfo.AutoBistDis;
    output[292] = Spim_Rx1msRxRegInfo.BistDegCnt;
    output[293] = Spim_Rx1msRxRegInfo.DiagExit;
    output[294] = Spim_Rx1msRxRegInfo.DiagExitMask;
    output[295] = Spim_Rx1msRxRegInfo.NoError;
    output[296] = Spim_Rx1msRxRegInfo.EnableDrv;
    output[297] = Spim_Rx1msRxRegInfo.CfgCrcEn;
    output[298] = Spim_Rx1msRxRegInfo.DisNresMon;
    output[299] = Spim_Rx1msRxRegInfo.WdRstEn;
    output[300] = Spim_Rx1msRxRegInfo.IgnPwrl;
    output[301] = Spim_Rx1msRxRegInfo.WdCfg;
    output[302] = Spim_Rx1msRxRegInfo.ErrorCfg;
    output[303] = Spim_Rx1msRxRegInfo.NoSafeTo;
    output[304] = Spim_Rx1msRxRegInfo.DevErrCnt;
    output[305] = Spim_Rx1msRxRegInfo.WdFail;
    output[306] = Spim_Rx1msRxRegInfo.ErrorPinFail;
    output[307] = Spim_Rx1msRxRegInfo.Pwmh;
    output[308] = Spim_Rx1msRxRegInfo.Pwml;
    output[309] = Spim_Rx1msRxRegInfo.PwdThr;
    output[310] = Spim_Rx1msRxRegInfo.CfgCrc;
    output[311] = Spim_Rx1msRxRegInfo.DiagMuxEn;
    output[312] = Spim_Rx1msRxRegInfo.DiagSpiSdo;
    output[313] = Spim_Rx1msRxRegInfo.DiagMuxOut;
    output[314] = Spim_Rx1msRxRegInfo.DiagIntCon;
    output[315] = Spim_Rx1msRxRegInfo.DiagMuxCfg;
    output[316] = Spim_Rx1msRxRegInfo.DiagMuxSel;
    output[317] = Spim_Rx1msRxRegInfo.WdtTokenSeed;
    output[318] = Spim_Rx1msRxRegInfo.WdtFdbk;
    output[319] = Spim_Rx1msRxRegInfo.WdtRt;
    output[320] = Spim_Rx1msRxRegInfo.WdtRw;
    output[321] = Spim_Rx1msRxRegInfo.WdtToken;
    output[322] = Spim_Rx1msRxRegInfo.WdfailTh;
    output[323] = Spim_Rx1msRxRegInfo.TokenEarly;
    output[324] = Spim_Rx1msRxRegInfo.TimeOut;
    output[325] = Spim_Rx1msRxRegInfo.SeqErr;
    output[326] = Spim_Rx1msRxRegInfo.WdCfgChg;
    output[327] = Spim_Rx1msRxRegInfo.WdWrongCfg;
    output[328] = Spim_Rx1msRxRegInfo.TokenErr;
    output[329] = Spim_Rx1msRxRegInfo.WdtAnswCnt;
    output[330] = Spim_Rx1msRxRegInfo.Vsout1En;
    output[331] = Spim_Rx1msRxRegInfo.Vdd5En;
    output[332] = Spim_Rx1msRxRegInfo.SwLockStat;
    output[333] = Spim_Rx1msRxRegInfo.SwUnlockStat;
    output[334] = Spim_Rx1msRxRegInfo.RdDevIdStat;
    output[335] = Spim_Rx1msRxRegInfo.RdDevRevStat;
    output[336] = Spim_Rx1msRxRegInfo.WrDevCfg1Stat;
    output[337] = Spim_Rx1msRxRegInfo.RdDevCfg1Stat;
    output[338] = Spim_Rx1msRxRegInfo.WrDevCfg2Stat;
    output[339] = Spim_Rx1msRxRegInfo.RdDevCfg2Stat;
    output[340] = Spim_Rx1msRxRegInfo.WrCanStbyStat;
    output[341] = Spim_Rx1msRxRegInfo.RdSafetyStat1Stat;
    output[342] = Spim_Rx1msRxRegInfo.RdSafetyStat2Stat;
    output[343] = Spim_Rx1msRxRegInfo.RdSafetyStat3Stat;
    output[344] = Spim_Rx1msRxRegInfo.RdSafetyStat4Stat;
    output[345] = Spim_Rx1msRxRegInfo.RdSafetyStat5Stat;
    output[346] = Spim_Rx1msRxRegInfo.RdSafetyErrCfgStat;
    output[347] = Spim_Rx1msRxRegInfo.WrSafetyErrCfgStat;
    output[348] = Spim_Rx1msRxRegInfo.WrSafetyErrStatStat;
    output[349] = Spim_Rx1msRxRegInfo.RdSafetyErrStatStat;
    output[350] = Spim_Rx1msRxRegInfo.RdSafetyPwdThrCfgStat;
    output[351] = Spim_Rx1msRxRegInfo.WrSafetyPwdThrCfgStat;
    output[352] = Spim_Rx1msRxRegInfo.RdSafetyCheckCtrlStat;
    output[353] = Spim_Rx1msRxRegInfo.WrSafetyCheckCtrlStat;
    output[354] = Spim_Rx1msRxRegInfo.RdSafetyBistCtrlStat;
    output[355] = Spim_Rx1msRxRegInfo.WrSafetyBistCtrlStat;
    output[356] = Spim_Rx1msRxRegInfo.RdWdtWin1CfgStat;
    output[357] = Spim_Rx1msRxRegInfo.WrWdtWin1CfgStat;
    output[358] = Spim_Rx1msRxRegInfo.RdWdtWin2CfgStat;
    output[359] = Spim_Rx1msRxRegInfo.WrWdtWin2CfgStat;
    output[360] = Spim_Rx1msRxRegInfo.RdWdtTokenValueStat;
    output[361] = Spim_Rx1msRxRegInfo.RdWdtStatusStat;
    output[362] = Spim_Rx1msRxRegInfo.WrWdtAnswerStat;
    output[363] = Spim_Rx1msRxRegInfo.RdDevStatStat;
    output[364] = Spim_Rx1msRxRegInfo.RdVmonStat1Stat;
    output[365] = Spim_Rx1msRxRegInfo.RdVmonStat2Stat;
    output[366] = Spim_Rx1msRxRegInfo.RdSensCtrlStat;
    output[367] = Spim_Rx1msRxRegInfo.WrSensCtrlStat;
    output[368] = Spim_Rx1msRxRegInfo.RdSafetyFuncCfgStat;
    output[369] = Spim_Rx1msRxRegInfo.WrSafetyFuncCfgStat;
    output[370] = Spim_Rx1msRxRegInfo.RdSafetyCfgCrcStat;
    output[371] = Spim_Rx1msRxRegInfo.WrSafetyCfgCrcStat;
    output[372] = Spim_Rx1msRxRegInfo.RdDiagCfgCtrlStat;
    output[373] = Spim_Rx1msRxRegInfo.WrDiagCfgCtrlStat;
    output[374] = Spim_Rx1msRxRegInfo.RdDiagMuxSelStat;
    output[375] = Spim_Rx1msRxRegInfo.WrDiagMuxSelStat;
    output[376] = Spim_Rx1msRxRegInfo.RdSafetyErrPwmHStat;
    output[377] = Spim_Rx1msRxRegInfo.WrSafetyErrPwmHStat;
    output[378] = Spim_Rx1msRxRegInfo.RdSafetyErrPwmLStat;
    output[379] = Spim_Rx1msRxRegInfo.WrSafetyErrPwmLStat;
    output[380] = Spim_Rx1msRxRegInfo.RdWdtTokenFdbckStat;
    output[381] = Spim_Rx1msRxRegInfo.WrWdtTokenFdbckStat;
    output[382] = Spim_Rx1msRxAsicPhyInfo.CspMon;
    output[383] = Spim_Rx1msRxAsicPhyInfo.FlOvCurrMon;
    output[384] = Spim_Rx1msRxAsicPhyInfo.FlIvDutyMon;
    output[385] = Spim_Rx1msRxAsicPhyInfo.FrOvCurrMon;
    output[386] = Spim_Rx1msRxAsicPhyInfo.FrIvDutyMon;
    output[387] = Spim_Rx1msRxAsicPhyInfo.RlOvCurrMon;
    output[388] = Spim_Rx1msRxAsicPhyInfo.RlIvDutyMon;
    output[389] = Spim_Rx1msRxAsicPhyInfo.RrOvCurrMon;
    output[390] = Spim_Rx1msRxAsicPhyInfo.RrIvDutyMon;
    output[391] = Spim_Rx1msRxAsicPhyInfo.Pdf5vMon;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Spim_Rx1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
