#define S_FUNCTION_NAME      Spim_Tx1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          67
#define WidthOutputPort         0

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Spim_Tx1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Spim_Tx1msSpiTxAsicInfo.HpdSr = input[0];
    Spim_Tx1msSpiTxAsicInfo.PdMtCfg = input[1];
    Spim_Tx1msSpiTxAsicInfo.Pchar = input[2];
    Spim_Tx1msSpiTxAsicInfo.Ichar = input[3];
    Spim_Tx1msSpiTxAsicInfo.LsdSinkDis = input[4];
    Spim_Tx1msSpiTxAsicInfo.LdactDis = input[5];
    Spim_Tx1msSpiTxAsicInfo.E5vsOcCfg = input[6];
    Spim_Tx1msSpiTxAsicInfo.IsoFlt = input[7];
    Spim_Tx1msSpiTxAsicInfo.IsoDet = input[8];
    Spim_Tx1msSpiTxAsicInfo.FmAmp = input[9];
    Spim_Tx1msSpiTxAsicInfo.FmEn = input[10];
    Spim_Tx1msSpiTxAsicInfo.Stopclk2 = input[11];
    Spim_Tx1msSpiTxAsicInfo.Adin2En = input[12];
    Spim_Tx1msSpiTxAsicInfo.Adin1Dis = input[13];
    Spim_Tx1msSpiTxAsicInfo.OcfPd = input[14];
    Spim_Tx1msSpiTxAsicInfo.FvpwrAct = input[15];
    Spim_Tx1msSpiTxAsicInfo.DfWs = input[16];
    Spim_Tx1msSpiTxAsicInfo.OcfWs = input[17];
    Spim_Tx1msSpiTxAsicInfo.Iclamp = input[18];
    Spim_Tx1msSpiTxAsicInfo.Didt = input[19];
    Spim_Tx1msSpiTxAsicInfo.Fdcl = input[20];
    Spim_Tx1msSpiTxAsicInfo.Llc = input[21];
    Spim_Tx1msSpiTxAsicInfo.CrFb = input[22];
    Spim_Tx1msSpiTxAsicInfo.CrDis12 = input[23];
    Spim_Tx1msSpiTxAsicInfo.CrDis34 = input[24];
    Spim_Tx1msSpiTxAsicInfo.LfPwm14 = input[25];
    Spim_Tx1msSpiTxAsicInfo.LfPwm58 = input[26];
    Spim_Tx1msSpiTxAsicInfo.WsaiS = input[27];
    Spim_Tx1msSpiTxAsicInfo.WsS = input[28];
    Spim_Tx1msSpiTxAsicInfo.Wscfg1 = input[29];
    Spim_Tx1msSpiTxAsicInfo.Wscfg2 = input[30];
    Spim_Tx1msSpiTxAsicInfo.Wscfg3 = input[31];
    Spim_Tx1msSpiTxAsicInfo.Wscfg4 = input[32];
    Spim_Tx1msSpiTxAsicInfo.WsTrkDis1 = input[33];
    Spim_Tx1msSpiTxAsicInfo.WsTrkDis2 = input[34];
    Spim_Tx1msSpiTxAsicInfo.WsTrkDis3 = input[35];
    Spim_Tx1msSpiTxAsicInfo.WsTrkDis4 = input[36];
    Spim_Tx1msSpiTxAsicInfo.WsCntRst = input[37];
    Spim_Tx1msSpiTxAsicInfo.WsCntEn = input[38];
    Spim_Tx1msSpiTxAsicInfo.VsoSel = input[39];
    Spim_Tx1msSpiTxAsicInfo.VsoS = input[40];
    Spim_Tx1msSpiTxAsicInfo.Vpo1On = input[41];
    Spim_Tx1msSpiTxAsicInfo.PdClrFlt = input[42];
    Spim_Tx1msSpiTxAsicInfo.WldClrFlt = input[43];
    Spim_Tx1msSpiTxAsicInfo.E5vsClrFlt = input[44];
    Spim_Tx1msSpiTxAsicInfo.WsClrFlt = input[45];
    Spim_Tx1msSpiTxAsicInfo.HdClrFlt = input[46];
    Spim_Tx1msSpiTxAsicInfo.IsokClrFlt = input[47];
    Spim_Tx1msSpiTxAsicInfo.VsoClrFlt = input[48];
    Spim_Tx1msSpiTxAsicInfo.Vpo2ClrFlt = input[49];
    Spim_Tx1msSpiTxAsicInfo.Vpo1ClrFlt = input[50];
    Spim_Tx1msSpiTxAsicInfo.LsdClrFlt = input[51];
    Spim_Tx1msSpiTxAsicSeedKeyInfo.Sed = input[52];
    Spim_Tx1msSpiTxAsicSeedKeyInfo.Mr = input[53];
    Spim_Tx1msSpiTxAsicActInfo.PdOn = input[54];
    Spim_Tx1msSpiTxAsicActInfo.Lsd5dutycycle = input[55];
    Spim_Tx1msSpiTxAsicActInfo.Lsd1dutycycle = input[56];
    Spim_Tx1msSpiTxAsicActInfo.Lsd6dutycycle = input[57];
    Spim_Tx1msSpiTxAsicActInfo.Lsd2dutycycle = input[58];
    Spim_Tx1msSpiTxAsicActInfo.Lsd7dutycycle = input[59];
    Spim_Tx1msSpiTxAsicActInfo.Lsd3dutycycle = input[60];
    Spim_Tx1msSpiTxAsicActInfo.Lsd8dutycycle = input[61];
    Spim_Tx1msSpiTxAsicActInfo.Lsd4dutycycle = input[62];
    Spim_Tx1msSpiTxAsicActInfo.HdOn = input[63];
    Spim_Tx1msAsicSchTblNum = input[64];
    Spim_Tx1msFuncInhibitSpimSts = input[65];
    Spim_Tx1msEcuModeSts = input[66];

    Spim_Tx1ms();


    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Spim_Tx1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
