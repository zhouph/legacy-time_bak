Spim_Rx50usFuncInhibitSpimSts = Simulink.Bus;
DeList={'Spim_Rx50usFuncInhibitSpimSts'};
Spim_Rx50usFuncInhibitSpimSts = CreateBus(Spim_Rx50usFuncInhibitSpimSts, DeList);
clear DeList;

Spim_Rx50usEcuModeSts = Simulink.Bus;
DeList={'Spim_Rx50usEcuModeSts'};
Spim_Rx50usEcuModeSts = CreateBus(Spim_Rx50usEcuModeSts, DeList);
clear DeList;

Spim_Rx50usRxMpsAngleInfo = Simulink.Bus;
DeList={
    'Mps1Angle'
    'Mps2Angle'
    };
Spim_Rx50usRxMpsAngleInfo = CreateBus(Spim_Rx50usRxMpsAngleInfo, DeList);
clear DeList;

