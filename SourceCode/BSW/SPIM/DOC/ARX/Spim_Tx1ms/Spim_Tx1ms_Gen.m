Spim_Tx1msSpiTxAsicInfo = Simulink.Bus;
DeList={
    'HpdSr'
    'PdMtCfg'
    'Pchar'
    'Ichar'
    'LsdSinkDis'
    'LdactDis'
    'E5vsOcCfg'
    'IsoFlt'
    'IsoDet'
    'FmAmp'
    'FmEn'
    'Stopclk2'
    'Adin2En'
    'Adin1Dis'
    'OcfPd'
    'FvpwrAct'
    'DfWs'
    'OcfWs'
    'Iclamp'
    'Didt'
    'Fdcl'
    'Llc'
    'CrFb'
    'CrDis12'
    'CrDis34'
    'LfPwm14'
    'LfPwm58'
    'WsaiS'
    'WsS'
    'Wscfg1'
    'Wscfg2'
    'Wscfg3'
    'Wscfg4'
    'WsTrkDis1'
    'WsTrkDis2'
    'WsTrkDis3'
    'WsTrkDis4'
    'WsCntRst'
    'WsCntEn'
    'VsoSel'
    'VsoS'
    'Vpo1On'
    'PdClrFlt'
    'WldClrFlt'
    'E5vsClrFlt'
    'WsClrFlt'
    'HdClrFlt'
    'IsokClrFlt'
    'VsoClrFlt'
    'Vpo2ClrFlt'
    'Vpo1ClrFlt'
    'LsdClrFlt'
    };
Spim_Tx1msSpiTxAsicInfo = CreateBus(Spim_Tx1msSpiTxAsicInfo, DeList);
clear DeList;

Spim_Tx1msSpiTxAsicSeedKeyInfo = Simulink.Bus;
DeList={
    'Sed'
    'Mr'
    };
Spim_Tx1msSpiTxAsicSeedKeyInfo = CreateBus(Spim_Tx1msSpiTxAsicSeedKeyInfo, DeList);
clear DeList;

Spim_Tx1msSpiTxAsicActInfo = Simulink.Bus;
DeList={
    'PdOn'
    'Lsd5dutycycle'
    'Lsd1dutycycle'
    'Lsd6dutycycle'
    'Lsd2dutycycle'
    'Lsd7dutycycle'
    'Lsd3dutycycle'
    'Lsd8dutycycle'
    'Lsd4dutycycle'
    'HdOn'
    };
Spim_Tx1msSpiTxAsicActInfo = CreateBus(Spim_Tx1msSpiTxAsicActInfo, DeList);
clear DeList;

Spim_Tx1msAsicSchTblNum = Simulink.Bus;
DeList={'Spim_Tx1msAsicSchTblNum'};
Spim_Tx1msAsicSchTblNum = CreateBus(Spim_Tx1msAsicSchTblNum, DeList);
clear DeList;

Spim_Tx1msFuncInhibitSpimSts = Simulink.Bus;
DeList={'Spim_Tx1msFuncInhibitSpimSts'};
Spim_Tx1msFuncInhibitSpimSts = CreateBus(Spim_Tx1msFuncInhibitSpimSts, DeList);
clear DeList;

Spim_Tx1msEcuModeSts = Simulink.Bus;
DeList={'Spim_Tx1msEcuModeSts'};
Spim_Tx1msEcuModeSts = CreateBus(Spim_Tx1msEcuModeSts, DeList);
clear DeList;

