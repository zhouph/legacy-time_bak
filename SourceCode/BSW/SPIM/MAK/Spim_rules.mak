# \file
#
# \brief Spim
#
# This file contains the implementation of the SWC
# module Spim.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Spim_src

#Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_Rx50us.c
#Spim_src_FILES        += $(Spim_IFA_PATH)\Spim_Rx50us_Ifa.c
#Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_Rx1ms.c
#Spim_src_FILES        += $(Spim_IFA_PATH)\Spim_Rx1ms_Ifa.c
#Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_Tx1ms.c
#Spim_src_FILES        += $(Spim_IFA_PATH)\Spim_Tx1ms_Ifa.c
#Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_5ms.c
#Spim_src_FILES        += $(Spim_CFG_PATH)\Spim_Cfg.c
#Spim_src_FILES        += $(Spim_CAL_PATH)\Spim_Cal.c
Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_Cdd.c
Spim_src_FILES        += $(Spim_CFG_PATH)\Spim_Cdd_Cfg.c
#Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_Sch.c
#Spim_src_FILES        += $(Spim_CFG_PATH)\Spim_Sch_Cfg.c
#Spim_src_FILES        += $(Spim_SRC_PATH)\Spim_If.c
#Spim_src_FILES        += $(Spim_CFG_PATH)\Spim_If_Cfg.c