# \file
#
# \brief Spim
#
# This file contains the implementation of the SWC
# module Spim.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Spim_CORE_PATH     := $(MANDO_BSW_ROOT)\Spim
Spim_CAL_PATH      := $(Spim_CORE_PATH)\CAL
Spim_SRC_PATH      := $(Spim_CORE_PATH)\SRC
Spim_CFG_PATH      := $(Spim_CORE_PATH)\CFG\$(Spim_VARIANT)
Spim_HDR_PATH      := $(Spim_CORE_PATH)\HDR
Spim_IFA_PATH      := $(Spim_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Spim_CMN_PATH      := $(Spim_CORE_PATH)\ICE\CMN
endif

CC_INCLUDE_PATH    += $(Spim_CAL_PATH)
CC_INCLUDE_PATH    += $(Spim_SRC_PATH)
CC_INCLUDE_PATH    += $(Spim_CFG_PATH)
CC_INCLUDE_PATH    += $(Spim_HDR_PATH)
CC_INCLUDE_PATH    += $(Spim_IFA_PATH)
CC_INCLUDE_PATH    += $(Spim_CMN_PATH)

