#include "unity.h"
#include "unity_fixture.h"
#include "Mps_TLE5012M_Main.h"
#include "Mps_TLE5012M_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_VR[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_VR;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_WD[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_WD;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_ROM[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_ROM;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_ADCT[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_ADCT;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_DSPU[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_DSPU;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_FUSE[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_FUSE;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_MAGOL[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_MAGOL;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_OV[MAX_STEP] = MPS_TLE5012M_MAINMPSD1SPIDCDINFO_S_OV;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_VR[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_VR;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_WD[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_WD;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_ROM[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_ROM;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_ADCT[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_ADCT;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_DSPU[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_DSPU;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_FUSE[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_FUSE;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_MAGOL[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_MAGOL;
const Haluint8 UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_OV[MAX_STEP] = MPS_TLE5012M_MAINMPSD2SPIDCDINFO_S_OV;
const Mom_HndlrEcuModeSts_t UtInput_Mps_TLE5012M_MainEcuModeSts[MAX_STEP] = MPS_TLE5012M_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Mps_TLE5012M_MainIgnOnOffSts[MAX_STEP] = MPS_TLE5012M_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_Mps_TLE5012M_MainIgnEdgeSts[MAX_STEP] = MPS_TLE5012M_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_Mps_TLE5012M_MainVBatt1Mon[MAX_STEP] = MPS_TLE5012M_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_Mps_TLE5012M_MainDiagClrSrs[MAX_STEP] = MPS_TLE5012M_MAINDIAGCLRSRS;
const Mps_TLE5012_Hndlr_1msMpsInvalid_t UtInput_Mps_TLE5012M_MainMpsInvalid[MAX_STEP] = MPS_TLE5012M_MAINMPSINVALID;
const Arbitrator_MtrMtrArbDriveState_t UtInput_Mps_TLE5012M_MainMtrArbDriveState[MAX_STEP] = MPS_TLE5012M_MAINMTRARBDRIVESTATE;

const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_ExtPWRSuppMonErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_EXTPWRSUPPMONERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_IntPWRSuppMonErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_INTPWRSUPPMONERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_WatchDogErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_WATCHDOGERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_FlashMemoryErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_FLASHMEMORYERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_StartUpTestErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_STARTUPTESTERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_ConfigRegiTestErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_CONFIGREGITESTERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_HWIntegConsisErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_HWINTEGCONSISERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_MagnetLossErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD1ERRINFO_MAGNETLOSSERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_ExtPWRSuppMonErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_EXTPWRSUPPMONERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_IntPWRSuppMonErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_INTPWRSUPPMONERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_WatchDogErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_WATCHDOGERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_FlashMemoryErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_FLASHMEMORYERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_StartUpTestErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_STARTUPTESTERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_ConfigRegiTestErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_CONFIGREGITESTERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_HWIntegConsisErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_HWINTEGCONSISERR;
const Saluint8 UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_MagnetLossErr[MAX_STEP] = MPS_TLE5012M_MAINMPSD2ERRINFO_MAGNETLOSSERR;



TEST_GROUP(Mps_TLE5012M_Main);
TEST_SETUP(Mps_TLE5012M_Main)
{
    Mps_TLE5012M_Main_Init();
}

TEST_TEAR_DOWN(Mps_TLE5012M_Main)
{   /* Postcondition */

}

TEST(Mps_TLE5012M_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_VR[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_WD[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_ROM[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_ADCT[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_DSPU[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_FUSE[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_MAGOL[i];
        Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV = UtInput_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_OV[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_VR[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_WD[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_ROM[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_ADCT[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_DSPU[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_FUSE[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_MAGOL[i];
        Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV = UtInput_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_OV[i];
        Mps_TLE5012M_MainEcuModeSts = UtInput_Mps_TLE5012M_MainEcuModeSts[i];
        Mps_TLE5012M_MainIgnOnOffSts = UtInput_Mps_TLE5012M_MainIgnOnOffSts[i];
        Mps_TLE5012M_MainIgnEdgeSts = UtInput_Mps_TLE5012M_MainIgnEdgeSts[i];
        Mps_TLE5012M_MainVBatt1Mon = UtInput_Mps_TLE5012M_MainVBatt1Mon[i];
        Mps_TLE5012M_MainDiagClrSrs = UtInput_Mps_TLE5012M_MainDiagClrSrs[i];
        Mps_TLE5012M_MainMpsInvalid = UtInput_Mps_TLE5012M_MainMpsInvalid[i];
        Mps_TLE5012M_MainMtrArbDriveState = UtInput_Mps_TLE5012M_MainMtrArbDriveState[i];

        Mps_TLE5012M_Main();

        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.ExtPWRSuppMonErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_ExtPWRSuppMonErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.IntPWRSuppMonErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_IntPWRSuppMonErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.WatchDogErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_WatchDogErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.FlashMemoryErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_FlashMemoryErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.StartUpTestErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_StartUpTestErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.ConfigRegiTestErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_ConfigRegiTestErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.HWIntegConsisErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_HWIntegConsisErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD1ErrInfo.MagnetLossErr, UtExpected_Mps_TLE5012M_MainMpsD1ErrInfo_MagnetLossErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.ExtPWRSuppMonErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_ExtPWRSuppMonErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.IntPWRSuppMonErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_IntPWRSuppMonErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.WatchDogErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_WatchDogErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.FlashMemoryErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_FlashMemoryErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.StartUpTestErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_StartUpTestErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.ConfigRegiTestErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_ConfigRegiTestErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.HWIntegConsisErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_HWIntegConsisErr[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012M_MainMpsD2ErrInfo.MagnetLossErr, UtExpected_Mps_TLE5012M_MainMpsD2ErrInfo_MagnetLossErr[i]);
    }
}

TEST_GROUP_RUNNER(Mps_TLE5012M_Main)
{
    RUN_TEST_CASE(Mps_TLE5012M_Main, All);
}
