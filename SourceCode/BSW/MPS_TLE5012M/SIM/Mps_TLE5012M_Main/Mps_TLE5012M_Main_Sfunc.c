#define S_FUNCTION_NAME      Mps_TLE5012M_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          23
#define WidthOutputPort         16

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Mps_TLE5012M_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR = input[0];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD = input[1];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM = input[2];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT = input[3];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU = input[4];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE = input[5];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL = input[6];
    Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV = input[7];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR = input[8];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD = input[9];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM = input[10];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT = input[11];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU = input[12];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE = input[13];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL = input[14];
    Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV = input[15];
    Mps_TLE5012M_MainEcuModeSts = input[16];
    Mps_TLE5012M_MainIgnOnOffSts = input[17];
    Mps_TLE5012M_MainIgnEdgeSts = input[18];
    Mps_TLE5012M_MainVBatt1Mon = input[19];
    Mps_TLE5012M_MainDiagClrSrs = input[20];
    Mps_TLE5012M_MainMpsInvalid = input[21];
    Mps_TLE5012M_MainMtrArbDriveState = input[22];

    Mps_TLE5012M_Main();


    output[0] = Mps_TLE5012M_MainMpsD1ErrInfo.ExtPWRSuppMonErr;
    output[1] = Mps_TLE5012M_MainMpsD1ErrInfo.IntPWRSuppMonErr;
    output[2] = Mps_TLE5012M_MainMpsD1ErrInfo.WatchDogErr;
    output[3] = Mps_TLE5012M_MainMpsD1ErrInfo.FlashMemoryErr;
    output[4] = Mps_TLE5012M_MainMpsD1ErrInfo.StartUpTestErr;
    output[5] = Mps_TLE5012M_MainMpsD1ErrInfo.ConfigRegiTestErr;
    output[6] = Mps_TLE5012M_MainMpsD1ErrInfo.HWIntegConsisErr;
    output[7] = Mps_TLE5012M_MainMpsD1ErrInfo.MagnetLossErr;
    output[8] = Mps_TLE5012M_MainMpsD2ErrInfo.ExtPWRSuppMonErr;
    output[9] = Mps_TLE5012M_MainMpsD2ErrInfo.IntPWRSuppMonErr;
    output[10] = Mps_TLE5012M_MainMpsD2ErrInfo.WatchDogErr;
    output[11] = Mps_TLE5012M_MainMpsD2ErrInfo.FlashMemoryErr;
    output[12] = Mps_TLE5012M_MainMpsD2ErrInfo.StartUpTestErr;
    output[13] = Mps_TLE5012M_MainMpsD2ErrInfo.ConfigRegiTestErr;
    output[14] = Mps_TLE5012M_MainMpsD2ErrInfo.HWIntegConsisErr;
    output[15] = Mps_TLE5012M_MainMpsD2ErrInfo.MagnetLossErr;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Mps_TLE5012M_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
