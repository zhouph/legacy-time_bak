/**
 * @defgroup Mps_TLE5012M_Main Mps_TLE5012M_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012M_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012M_Main.h"
#include "Mps_TLE5012M_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012M_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MPS_TLE5012M_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012M_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mps_TLE5012M_Main_HdrBusType Mps_TLE5012M_MainBus;

/* Version Info */
const SwcVersionInfo_t Mps_TLE5012M_MainVersionInfo = 
{   
    MPS_TLE5012M_MAIN_MODULE_ID,           /* Mps_TLE5012M_MainVersionInfo.ModuleId */
    MPS_TLE5012M_MAIN_MAJOR_VERSION,       /* Mps_TLE5012M_MainVersionInfo.MajorVer */
    MPS_TLE5012M_MAIN_MINOR_VERSION,       /* Mps_TLE5012M_MainVersionInfo.MinorVer */
    MPS_TLE5012M_MAIN_PATCH_VERSION,       /* Mps_TLE5012M_MainVersionInfo.PatchVer */
    MPS_TLE5012M_MAIN_BRANCH_VERSION       /* Mps_TLE5012M_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012M_MainMpsD1SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012M_MainMpsD2SpiDcdInfo;
Mom_HndlrEcuModeSts_t Mps_TLE5012M_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t Mps_TLE5012M_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Mps_TLE5012M_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t Mps_TLE5012M_MainVBatt1Mon;
Diag_HndlrDiagClr_t Mps_TLE5012M_MainDiagClrSrs;
Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012M_MainMpsInvalid;
Arbitrator_MtrMtrArbDriveState_t Mps_TLE5012M_MainMtrArbDriveState;

/* Output Data Element */
Mps_TLE5012M_MainMpsD1ErrInfo_t Mps_TLE5012M_MainMpsD1ErrInfo;
Mps_TLE5012M_MainMpsD2ErrInfo_t Mps_TLE5012M_MainMpsD2ErrInfo;

uint32 Mps_TLE5012M_Main_Timer_Start;
uint32 Mps_TLE5012M_Main_Timer_Elapsed;

#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_MAIN_START_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012M_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_MAIN_START_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012M_MAIN_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MPS_TLE5012M_MAIN_START_SEC_CODE
#include "Mps_TLE5012M_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mps_TLE5012M_Main_Init(void)
{
    /* Initialize internal bus */
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainEcuModeSts = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainIgnOnOffSts = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainIgnEdgeSts = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainVBatt1Mon = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainDiagClrSrs = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsInvalid = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMtrArbDriveState = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.ExtPWRSuppMonErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.IntPWRSuppMonErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.WatchDogErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.FlashMemoryErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.StartUpTestErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.ConfigRegiTestErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.HWIntegConsisErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo.MagnetLossErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.ExtPWRSuppMonErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.IntPWRSuppMonErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.WatchDogErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.FlashMemoryErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.StartUpTestErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.ConfigRegiTestErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.HWIntegConsisErr = 0;
    Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo.MagnetLossErr = 0;
}

void Mps_TLE5012M_Main(void)
{
    Mps_TLE5012M_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD1SpiDcdInfo 
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV;
     =============================================================================*/
    
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD2SpiDcdInfo 
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV;
     =============================================================================*/
    
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainEcuModeSts(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainEcuModeSts);
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainIgnOnOffSts(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainIgnOnOffSts);
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainIgnEdgeSts(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainIgnEdgeSts);
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainVBatt1Mon(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainVBatt1Mon);
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainDiagClrSrs(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainDiagClrSrs);
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsInvalid(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsInvalid);
    Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMtrArbDriveState(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMtrArbDriveState);

    /* Process */

    /* Output */
    Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD1ErrInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD1ErrInfo 
     : Mps_TLE5012M_MainMpsD1ErrInfo.ExtPWRSuppMonErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.IntPWRSuppMonErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.WatchDogErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.FlashMemoryErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.StartUpTestErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.ConfigRegiTestErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.HWIntegConsisErr;
     : Mps_TLE5012M_MainMpsD1ErrInfo.MagnetLossErr;
     =============================================================================*/
    
    Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo(&Mps_TLE5012M_MainBus.Mps_TLE5012M_MainMpsD2ErrInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012M_MainMpsD2ErrInfo 
     : Mps_TLE5012M_MainMpsD2ErrInfo.ExtPWRSuppMonErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.IntPWRSuppMonErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.WatchDogErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.FlashMemoryErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.StartUpTestErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.ConfigRegiTestErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.HWIntegConsisErr;
     : Mps_TLE5012M_MainMpsD2ErrInfo.MagnetLossErr;
     =============================================================================*/
    

    Mps_TLE5012M_Main_Timer_Elapsed = STM0_TIM0.U - Mps_TLE5012M_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MPS_TLE5012M_MAIN_STOP_SEC_CODE
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
