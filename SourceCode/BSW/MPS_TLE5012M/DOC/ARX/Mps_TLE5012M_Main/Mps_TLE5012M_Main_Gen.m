Mps_TLE5012M_MainMpsD1SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012M_MainMpsD1SpiDcdInfo = CreateBus(Mps_TLE5012M_MainMpsD1SpiDcdInfo, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD2SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012M_MainMpsD2SpiDcdInfo = CreateBus(Mps_TLE5012M_MainMpsD2SpiDcdInfo, DeList);
clear DeList;

Mps_TLE5012M_MainEcuModeSts = Simulink.Bus;
DeList={'Mps_TLE5012M_MainEcuModeSts'};
Mps_TLE5012M_MainEcuModeSts = CreateBus(Mps_TLE5012M_MainEcuModeSts, DeList);
clear DeList;

Mps_TLE5012M_MainIgnOnOffSts = Simulink.Bus;
DeList={'Mps_TLE5012M_MainIgnOnOffSts'};
Mps_TLE5012M_MainIgnOnOffSts = CreateBus(Mps_TLE5012M_MainIgnOnOffSts, DeList);
clear DeList;

Mps_TLE5012M_MainIgnEdgeSts = Simulink.Bus;
DeList={'Mps_TLE5012M_MainIgnEdgeSts'};
Mps_TLE5012M_MainIgnEdgeSts = CreateBus(Mps_TLE5012M_MainIgnEdgeSts, DeList);
clear DeList;

Mps_TLE5012M_MainVBatt1Mon = Simulink.Bus;
DeList={'Mps_TLE5012M_MainVBatt1Mon'};
Mps_TLE5012M_MainVBatt1Mon = CreateBus(Mps_TLE5012M_MainVBatt1Mon, DeList);
clear DeList;

Mps_TLE5012M_MainDiagClrSrs = Simulink.Bus;
DeList={'Mps_TLE5012M_MainDiagClrSrs'};
Mps_TLE5012M_MainDiagClrSrs = CreateBus(Mps_TLE5012M_MainDiagClrSrs, DeList);
clear DeList;

Mps_TLE5012M_MainMpsInvalid = Simulink.Bus;
DeList={'Mps_TLE5012M_MainMpsInvalid'};
Mps_TLE5012M_MainMpsInvalid = CreateBus(Mps_TLE5012M_MainMpsInvalid, DeList);
clear DeList;

Mps_TLE5012M_MainMtrArbDriveState = Simulink.Bus;
DeList={'Mps_TLE5012M_MainMtrArbDriveState'};
Mps_TLE5012M_MainMtrArbDriveState = CreateBus(Mps_TLE5012M_MainMtrArbDriveState, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD1ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Mps_TLE5012M_MainMpsD1ErrInfo = CreateBus(Mps_TLE5012M_MainMpsD1ErrInfo, DeList);
clear DeList;

Mps_TLE5012M_MainMpsD2ErrInfo = Simulink.Bus;
DeList={
    'ExtPWRSuppMonErr'
    'IntPWRSuppMonErr'
    'WatchDogErr'
    'FlashMemoryErr'
    'StartUpTestErr'
    'ConfigRegiTestErr'
    'HWIntegConsisErr'
    'MagnetLossErr'
    };
Mps_TLE5012M_MainMpsD2ErrInfo = CreateBus(Mps_TLE5012M_MainMpsD2ErrInfo, DeList);
clear DeList;

