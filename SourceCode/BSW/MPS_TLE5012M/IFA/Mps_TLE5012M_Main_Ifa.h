/**
 * @defgroup Mps_TLE5012M_Main_Ifa Mps_TLE5012M_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012M_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012M_MAIN_IFA_H_
#define MPS_TLE5012M_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_VR(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_VR; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_WD(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_WD; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_ROM(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ROM; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_ADCT(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_ADCT; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_DSPU(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_DSPU; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_FUSE(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_FUSE; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_MAGOL(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_MAGOL; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD1SpiDcdInfo_S_OV(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD1SpiDcdInfo.S_OV; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_VR(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_VR; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_WD(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_WD; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_ROM(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ROM; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_ADCT(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_ADCT; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_DSPU(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_DSPU; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_FUSE(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_FUSE; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_MAGOL(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_MAGOL; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsD2SpiDcdInfo_S_OV(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsD2SpiDcdInfo.S_OV; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainEcuModeSts(data) do \
{ \
    *data = Mps_TLE5012M_MainEcuModeSts; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainIgnOnOffSts(data) do \
{ \
    *data = Mps_TLE5012M_MainIgnOnOffSts; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainIgnEdgeSts(data) do \
{ \
    *data = Mps_TLE5012M_MainIgnEdgeSts; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainVBatt1Mon(data) do \
{ \
    *data = Mps_TLE5012M_MainVBatt1Mon; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainDiagClrSrs(data) do \
{ \
    *data = Mps_TLE5012M_MainDiagClrSrs; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMpsInvalid(data) do \
{ \
    *data = Mps_TLE5012M_MainMpsInvalid; \
}while(0);

#define Mps_TLE5012M_Main_Read_Mps_TLE5012M_MainMtrArbDriveState(data) do \
{ \
    *data = Mps_TLE5012M_MainMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_ExtPWRSuppMonErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.ExtPWRSuppMonErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_IntPWRSuppMonErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.IntPWRSuppMonErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_WatchDogErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.WatchDogErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_FlashMemoryErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.FlashMemoryErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_StartUpTestErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.StartUpTestErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_ConfigRegiTestErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.ConfigRegiTestErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_HWIntegConsisErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.HWIntegConsisErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD1ErrInfo_MagnetLossErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD1ErrInfo.MagnetLossErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_ExtPWRSuppMonErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.ExtPWRSuppMonErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_IntPWRSuppMonErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.IntPWRSuppMonErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_WatchDogErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.WatchDogErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_FlashMemoryErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.FlashMemoryErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_StartUpTestErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.StartUpTestErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_ConfigRegiTestErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.ConfigRegiTestErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_HWIntegConsisErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.HWIntegConsisErr = *data; \
}while(0);

#define Mps_TLE5012M_Main_Write_Mps_TLE5012M_MainMpsD2ErrInfo_MagnetLossErr(data) do \
{ \
    Mps_TLE5012M_MainMpsD2ErrInfo.MagnetLossErr = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012M_MAIN_IFA_H_ */
/** @} */
