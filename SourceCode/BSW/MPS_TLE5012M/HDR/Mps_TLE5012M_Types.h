/**
 * @defgroup Mps_TLE5012M_Types Mps_TLE5012M_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012M_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012M_TYPES_H_
#define MPS_TLE5012M_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012M_MainMpsD1SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012M_MainMpsD2SpiDcdInfo;
    Mom_HndlrEcuModeSts_t Mps_TLE5012M_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Mps_TLE5012M_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Mps_TLE5012M_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t Mps_TLE5012M_MainVBatt1Mon;
    Diag_HndlrDiagClr_t Mps_TLE5012M_MainDiagClrSrs;
    Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012M_MainMpsInvalid;
    Arbitrator_MtrMtrArbDriveState_t Mps_TLE5012M_MainMtrArbDriveState;

/* Output Data Element */
    Mps_TLE5012M_MainMpsD1ErrInfo_t Mps_TLE5012M_MainMpsD1ErrInfo;
    Mps_TLE5012M_MainMpsD2ErrInfo_t Mps_TLE5012M_MainMpsD2ErrInfo;
}Mps_TLE5012M_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012M_TYPES_H_ */
/** @} */
