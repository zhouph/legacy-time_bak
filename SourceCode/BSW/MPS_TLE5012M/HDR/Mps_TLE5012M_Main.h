/**
 * @defgroup Mps_TLE5012M_Main Mps_TLE5012M_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012M_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012M_MAIN_H_
#define MPS_TLE5012M_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012M_Types.h"
#include "Mps_TLE5012M_Cfg.h"
#include "Mps_TLE5012M_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MPS_TLE5012M_MAIN_MODULE_ID      (0)
 #define MPS_TLE5012M_MAIN_MAJOR_VERSION  (2)
 #define MPS_TLE5012M_MAIN_MINOR_VERSION  (0)
 #define MPS_TLE5012M_MAIN_PATCH_VERSION  (0)
 #define MPS_TLE5012M_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mps_TLE5012M_Main_HdrBusType Mps_TLE5012M_MainBus;

/* Version Info */
extern const SwcVersionInfo_t Mps_TLE5012M_MainVersionInfo;

/* Input Data Element */
extern Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012M_MainMpsD1SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012M_MainMpsD2SpiDcdInfo;
extern Mom_HndlrEcuModeSts_t Mps_TLE5012M_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Mps_TLE5012M_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Mps_TLE5012M_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t Mps_TLE5012M_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t Mps_TLE5012M_MainDiagClrSrs;
extern Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012M_MainMpsInvalid;
extern Arbitrator_MtrMtrArbDriveState_t Mps_TLE5012M_MainMtrArbDriveState;

/* Output Data Element */
extern Mps_TLE5012M_MainMpsD1ErrInfo_t Mps_TLE5012M_MainMpsD1ErrInfo;
extern Mps_TLE5012M_MainMpsD2ErrInfo_t Mps_TLE5012M_MainMpsD2ErrInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mps_TLE5012M_Main_Init(void);
extern void Mps_TLE5012M_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012M_MAIN_H_ */
/** @} */
