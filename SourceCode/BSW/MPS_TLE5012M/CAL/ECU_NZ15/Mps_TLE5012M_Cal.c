/**
 * @defgroup Mps_TLE5012M_Cal Mps_TLE5012M_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012M_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012M_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012M_START_SEC_CALIB_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/* Global Calibration Section */


#define MPS_TLE5012M_STOP_SEC_CALIB_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"

#define MPS_TLE5012M_START_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MPS_TLE5012M_STOP_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012M_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MPS_TLE5012M_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012M_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012M_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_START_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012M_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012M_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MPS_TLE5012M_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012M_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012M_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012M_MemMap.h"
#define MPS_TLE5012M_START_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012M_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MPS_TLE5012M_START_SEC_CODE
#include "Mps_TLE5012M_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MPS_TLE5012M_STOP_SEC_CODE
#include "Mps_TLE5012M_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
