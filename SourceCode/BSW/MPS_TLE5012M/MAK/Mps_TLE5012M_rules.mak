# \file
#
# \brief Mps_TLE5012M
#
# This file contains the implementation of the SWC
# module Mps_TLE5012M.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Mps_TLE5012M_src

Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_SRC_PATH)\Mps_TLE5012M_Main.c
Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_IFA_PATH)\Mps_TLE5012M_Main_Ifa.c
Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_CFG_PATH)\Mps_TLE5012M_Cfg.c
Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_CAL_PATH)\Mps_TLE5012M_Cal.c

ifeq ($(ICE_COMPILE),true)
Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_UNITY_PATH)\unity.c
	Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_UNITY_PATH)\unity_fixture.c	
	Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_UT_PATH)\main.c
	Mps_TLE5012M_src_FILES        += $(Mps_TLE5012M_UT_PATH)\Mps_TLE5012M_Main\Mps_TLE5012M_Main_UtMain.c
endif