Mtr_Processing_SigEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Mtr_Processing_SigEemFailData = CreateBus(Mtr_Processing_SigEemFailData, DeList);
clear DeList;

Mtr_Processing_SigSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
Mtr_Processing_SigSwTrigMotInfo = CreateBus(Mtr_Processing_SigSwTrigMotInfo, DeList);
clear DeList;

Mtr_Processing_SigWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Mtr_Processing_SigWhlSpdInfo = CreateBus(Mtr_Processing_SigWhlSpdInfo, DeList);
clear DeList;

Mtr_Processing_SigEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
Mtr_Processing_SigEemEceData = CreateBus(Mtr_Processing_SigEemEceData, DeList);
clear DeList;

Mtr_Processing_SigMtrProcessDataInf = Simulink.Bus;
DeList={
    'MtrProCalibrationState'
    'MtrCaliResult'
    };
Mtr_Processing_SigMtrProcessDataInf = CreateBus(Mtr_Processing_SigMtrProcessDataInf, DeList);
clear DeList;

Acmio_SenMotMonInfo = Simulink.Bus;
DeList={
    'MotPwrVoltMon'
    'MotStarMon'
    };
Acmio_SenMotMonInfo = CreateBus(Acmio_SenMotMonInfo, DeList);
clear DeList;

Acmio_SenMotVoltsMonInfo = Simulink.Bus;
DeList={
    'MotVoltPhUMon'
    'MotVoltPhVMon'
    'MotVoltPhWMon'
    };
Acmio_SenMotVoltsMonInfo = CreateBus(Acmio_SenMotVoltsMonInfo, DeList);
clear DeList;

Msp_CtrlMotOrgSetStInfo = Simulink.Bus;
DeList={
    'MotOrgSetErr'
    'MotOrgSet'
    };
Msp_CtrlMotOrgSetStInfo = CreateBus(Msp_CtrlMotOrgSetStInfo, DeList);
clear DeList;

Msp_CtrlPwrPistStBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'PosiWallTemp'
    'WallDetectedFlg'
    };
Msp_CtrlPwrPistStBfMotOrgSetInfo = CreateBus(Msp_CtrlPwrPistStBfMotOrgSetInfo, DeList);
clear DeList;

Acmctl_CtrlMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Acmctl_CtrlMotDqIRefMccInfo = CreateBus(Acmctl_CtrlMotDqIRefMccInfo, DeList);
clear DeList;

Acmctl_CtrlMotOrgSetStInfo = Simulink.Bus;
DeList={
    'MotOrgSetErr'
    'MotOrgSet'
    };
Acmctl_CtrlMotOrgSetStInfo = CreateBus(Acmctl_CtrlMotOrgSetStInfo, DeList);
clear DeList;

Acmctl_CtrlMotDqIRefSesInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Acmctl_CtrlMotDqIRefSesInfo = CreateBus(Acmctl_CtrlMotDqIRefSesInfo, DeList);
clear DeList;

Acmctl_CtrlFluxWeakengStInfo = Simulink.Bus;
DeList={
    'FluxWeakengFlg'
    'FluxWeakengFlgOld'
    };
Acmctl_CtrlFluxWeakengStInfo = CreateBus(Acmctl_CtrlFluxWeakengStInfo, DeList);
clear DeList;

Acmctl_CtrlMtrArbtratorData = Simulink.Bus;
DeList={
    'IqSelected'
    'IdSelected'
    'UPhasePWMSelected'
    'VPhasePWMSelected'
    'WPhasePWMSelected'
    'MtrCtrlMode'
    };
Acmctl_CtrlMtrArbtratorData = CreateBus(Acmctl_CtrlMtrArbtratorData, DeList);
clear DeList;

Acmctl_CtrlMtrArbtratorInfo = Simulink.Bus;
DeList={
    'MtrArbCalMode'
    'MtrArbState'
    };
Acmctl_CtrlMtrArbtratorInfo = CreateBus(Acmctl_CtrlMtrArbtratorInfo, DeList);
clear DeList;

Acmio_ActrMotReqDataDiagInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Acmio_ActrMotReqDataDiagInfo = CreateBus(Acmio_ActrMotReqDataDiagInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_50usEcuModeSts = Simulink.Bus;
DeList={'Mps_TLE5012_Hndlr_50usEcuModeSts'};
Mps_TLE5012_Hndlr_50usEcuModeSts = CreateBus(Mps_TLE5012_Hndlr_50usEcuModeSts, DeList);
clear DeList;

AdcIf_Conv50usEcuModeSts = Simulink.Bus;
DeList={'AdcIf_Conv50usEcuModeSts'};
AdcIf_Conv50usEcuModeSts = CreateBus(AdcIf_Conv50usEcuModeSts, DeList);
clear DeList;

AdcIf_Conv50usFuncInhibitAdcifSts = Simulink.Bus;
DeList={'AdcIf_Conv50usFuncInhibitAdcifSts'};
AdcIf_Conv50usFuncInhibitAdcifSts = CreateBus(AdcIf_Conv50usFuncInhibitAdcifSts, DeList);
clear DeList;

Ioc_InputSR50usEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputSR50usEcuModeSts'};
Ioc_InputSR50usEcuModeSts = CreateBus(Ioc_InputSR50usEcuModeSts, DeList);
clear DeList;

Ioc_InputSR50usFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputSR50usFuncInhibitIocSts'};
Ioc_InputSR50usFuncInhibitIocSts = CreateBus(Ioc_InputSR50usFuncInhibitIocSts, DeList);
clear DeList;

Mtr_Processing_SigEcuModeSts = Simulink.Bus;
DeList={'Mtr_Processing_SigEcuModeSts'};
Mtr_Processing_SigEcuModeSts = CreateBus(Mtr_Processing_SigEcuModeSts, DeList);
clear DeList;

Mtr_Processing_SigIgnOnOffSts = Simulink.Bus;
DeList={'Mtr_Processing_SigIgnOnOffSts'};
Mtr_Processing_SigIgnOnOffSts = CreateBus(Mtr_Processing_SigIgnOnOffSts, DeList);
clear DeList;

Mtr_Processing_SigIgnEdgeSts = Simulink.Bus;
DeList={'Mtr_Processing_SigIgnEdgeSts'};
Mtr_Processing_SigIgnEdgeSts = CreateBus(Mtr_Processing_SigIgnEdgeSts, DeList);
clear DeList;

Acmio_SenEcuModeSts = Simulink.Bus;
DeList={'Acmio_SenEcuModeSts'};
Acmio_SenEcuModeSts = CreateBus(Acmio_SenEcuModeSts, DeList);
clear DeList;

Acmio_SenFuncInhibitAcmioSts = Simulink.Bus;
DeList={'Acmio_SenFuncInhibitAcmioSts'};
Acmio_SenFuncInhibitAcmioSts = CreateBus(Acmio_SenFuncInhibitAcmioSts, DeList);
clear DeList;

Msp_CtrlEcuModeSts = Simulink.Bus;
DeList={'Msp_CtrlEcuModeSts'};
Msp_CtrlEcuModeSts = CreateBus(Msp_CtrlEcuModeSts, DeList);
clear DeList;

Msp_CtrlFuncInhibitMspSts = Simulink.Bus;
DeList={'Msp_CtrlFuncInhibitMspSts'};
Msp_CtrlFuncInhibitMspSts = CreateBus(Msp_CtrlFuncInhibitMspSts, DeList);
clear DeList;

Acmctl_CtrlEcuModeSts = Simulink.Bus;
DeList={'Acmctl_CtrlEcuModeSts'};
Acmctl_CtrlEcuModeSts = CreateBus(Acmctl_CtrlEcuModeSts, DeList);
clear DeList;

Acmctl_CtrlVdcLinkFild = Simulink.Bus;
DeList={'Acmctl_CtrlVdcLinkFild'};
Acmctl_CtrlVdcLinkFild = CreateBus(Acmctl_CtrlVdcLinkFild, DeList);
clear DeList;

Acmctl_CtrlMotCtrlMode = Simulink.Bus;
DeList={'Acmctl_CtrlMotCtrlMode'};
Acmctl_CtrlMotCtrlMode = CreateBus(Acmctl_CtrlMotCtrlMode, DeList);
clear DeList;

Acmctl_CtrlMotCtrlState = Simulink.Bus;
DeList={'Acmctl_CtrlMotCtrlState'};
Acmctl_CtrlMotCtrlState = CreateBus(Acmctl_CtrlMotCtrlState, DeList);
clear DeList;

Acmctl_CtrlFuncInhibitAcmctlSts = Simulink.Bus;
DeList={'Acmctl_CtrlFuncInhibitAcmctlSts'};
Acmctl_CtrlFuncInhibitAcmctlSts = CreateBus(Acmctl_CtrlFuncInhibitAcmctlSts, DeList);
clear DeList;

Acmctl_CtrlMtrArbDriveState = Simulink.Bus;
DeList={'Acmctl_CtrlMtrArbDriveState'};
Acmctl_CtrlMtrArbDriveState = CreateBus(Acmctl_CtrlMtrArbDriveState, DeList);
clear DeList;

Acmio_ActrEcuModeSts = Simulink.Bus;
DeList={'Acmio_ActrEcuModeSts'};
Acmio_ActrEcuModeSts = CreateBus(Acmio_ActrEcuModeSts, DeList);
clear DeList;

Acmio_ActrFuncInhibitAcmioSts = Simulink.Bus;
DeList={'Acmio_ActrFuncInhibitAcmioSts'};
Acmio_ActrFuncInhibitAcmioSts = CreateBus(Acmio_ActrFuncInhibitAcmioSts, DeList);
clear DeList;

Ioc_OutputCS50usEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputCS50usEcuModeSts'};
Ioc_OutputCS50usEcuModeSts = CreateBus(Ioc_OutputCS50usEcuModeSts, DeList);
clear DeList;

Ioc_OutputCS50usFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputCS50usFuncInhibitIocSts'};
Ioc_OutputCS50usFuncInhibitIocSts = CreateBus(Ioc_OutputCS50usFuncInhibitIocSts, DeList);
clear DeList;

AdcIf_Conv50usHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
AdcIf_Conv50usHwTrigMotInfo = CreateBus(AdcIf_Conv50usHwTrigMotInfo, DeList);
clear DeList;

Ioc_InputSR50usMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Ioc_InputSR50usMotCurrMonInfo = CreateBus(Ioc_InputSR50usMotCurrMonInfo, DeList);
clear DeList;

Ioc_InputSR50usMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Ioc_InputSR50usMotAngleMonInfo = CreateBus(Ioc_InputSR50usMotAngleMonInfo, DeList);
clear DeList;

Mtr_Processing_SigMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Mtr_Processing_SigMtrProcessOutInfo = CreateBus(Mtr_Processing_SigMtrProcessOutInfo, DeList);
clear DeList;

Msp_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Msp_CtrlMotRotgAgSigInfo = CreateBus(Msp_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'StkPosnMeasdBfMotOrgSet'
    'MotMechAngleSpdBfMotOrgSet'
    };
Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = CreateBus(Msp_CtrlMotRotgAgSigBfMotOrgSetInfo, DeList);
clear DeList;

Acmctl_CtrlMotDqIMeasdInfo = Simulink.Bus;
DeList={
    'MotIqMeasd'
    'MotIdMeasd'
    };
Acmctl_CtrlMotDqIMeasdInfo = CreateBus(Acmctl_CtrlMotDqIMeasdInfo, DeList);
clear DeList;

Acmio_SenVdcLink = Simulink.Bus;
DeList={'Acmio_SenVdcLink'};
Acmio_SenVdcLink = CreateBus(Acmio_SenVdcLink, DeList);
clear DeList;




Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo = Simulink.Bus;
DeList={
    'D1IIFAngRawData'
    'D1IIFAngDegree'
    };
Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo = CreateBus(Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo = Simulink.Bus;
DeList={
    'D2IIFAngRawData'
    'D2IIFAngDegree'
    };
Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo = CreateBus(Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo, DeList);
clear DeList;


AdcIf_Conv50usHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
AdcIf_Conv50usHwTrigMotInfo = CreateBus(AdcIf_Conv50usHwTrigMotInfo, DeList);
clear DeList;


Ioc_InputSR50usMpsD1IIFAngInfo = Simulink.Bus;
DeList={
    'D1IIFAngRawData'
    'D1IIFAngDegree'
    };
Ioc_InputSR50usMpsD1IIFAngInfo = CreateBus(Ioc_InputSR50usMpsD1IIFAngInfo, DeList);
clear DeList;

Ioc_InputSR50usMpsD2IIFAngInfo = Simulink.Bus;
DeList={
    'D2IIFAngRawData'
    'D2IIFAngDegree'
    };
Ioc_InputSR50usMpsD2IIFAngInfo = CreateBus(Ioc_InputSR50usMpsD2IIFAngInfo, DeList);
clear DeList;

Ioc_InputSR50usHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
Ioc_InputSR50usHwTrigMotInfo = CreateBus(Ioc_InputSR50usHwTrigMotInfo, DeList);
clear DeList;

Ioc_InputSR50usMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Ioc_InputSR50usMotCurrMonInfo = CreateBus(Ioc_InputSR50usMotCurrMonInfo, DeList);
clear DeList;

Ioc_InputSR50usMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Ioc_InputSR50usMotAngleMonInfo = CreateBus(Ioc_InputSR50usMotAngleMonInfo, DeList);
clear DeList;


Mtr_Processing_SigHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
Mtr_Processing_SigHwTrigMotInfo = CreateBus(Mtr_Processing_SigHwTrigMotInfo, DeList);
clear DeList;

Mtr_Processing_SigMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Mtr_Processing_SigMotCurrMonInfo = CreateBus(Mtr_Processing_SigMotCurrMonInfo, DeList);
clear DeList;

Mtr_Processing_SigMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Mtr_Processing_SigMotAngleMonInfo = CreateBus(Mtr_Processing_SigMotAngleMonInfo, DeList);
clear DeList;

Mtr_Processing_SigMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Mtr_Processing_SigMtrProcessOutInfo = CreateBus(Mtr_Processing_SigMtrProcessOutInfo, DeList);
clear DeList;


Acmio_SenMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Acmio_SenMotCurrMonInfo = CreateBus(Acmio_SenMotCurrMonInfo, DeList);
clear DeList;

Acmio_SenMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Acmio_SenMotAngleMonInfo = CreateBus(Acmio_SenMotAngleMonInfo, DeList);
clear DeList;

Acmio_SenMotCurrInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    };
Acmio_SenMotCurrInfo = CreateBus(Acmio_SenMotCurrInfo, DeList);
clear DeList;

Acmio_SenMotAngle1Info = Simulink.Bus;
DeList={
    'MotElecAngle1'
    'MotMechAngle1'
    };
Acmio_SenMotAngle1Info = CreateBus(Acmio_SenMotAngle1Info, DeList);
clear DeList;


Msp_CtrlMotAngle1Info = Simulink.Bus;
DeList={
    'MotElecAngle1'
    'MotMechAngle1'
    };
Msp_CtrlMotAngle1Info = CreateBus(Msp_CtrlMotAngle1Info, DeList);
clear DeList;

Msp_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Msp_CtrlMotRotgAgSigInfo = CreateBus(Msp_CtrlMotRotgAgSigInfo, DeList);
clear DeList;


Acmctl_CtrlMotCurrInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    };
Acmctl_CtrlMotCurrInfo = CreateBus(Acmctl_CtrlMotCurrInfo, DeList);
clear DeList;

Acmctl_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'MotElecAngleFild'
    };
Acmctl_CtrlMotRotgAgSigInfo = CreateBus(Acmctl_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Acmctl_CtrlMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Acmctl_CtrlMtrProcessOutInfo = CreateBus(Acmctl_CtrlMtrProcessOutInfo, DeList);
clear DeList;

Acmctl_CtrlMotReqDataAcmctlInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Acmctl_CtrlMotReqDataAcmctlInfo = CreateBus(Acmctl_CtrlMotReqDataAcmctlInfo, DeList);
clear DeList;


Acmio_ActrMotReqDataAcmctlInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Acmio_ActrMotReqDataAcmctlInfo = CreateBus(Acmio_ActrMotReqDataAcmctlInfo, DeList);
clear DeList;

Acmio_ActrMotPwmDataInfo = Simulink.Bus;
DeList={
    'MotPwmPhUhighData'
    'MotPwmPhVhighData'
    'MotPwmPhWhighData'
    'MotPwmPhUlowData'
    'MotPwmPhVlowData'
    'MotPwmPhWlowData'
    };
Acmio_ActrMotPwmDataInfo = CreateBus(Acmio_ActrMotPwmDataInfo, DeList);
clear DeList;


Ioc_OutputCS50usMotPwmDataInfo = Simulink.Bus;
DeList={
    'MotPwmPhUhighData'
    'MotPwmPhVhighData'
    'MotPwmPhWhighData'
    'MotPwmPhUlowData'
    'MotPwmPhVlowData'
    'MotPwmPhWlowData'
    };
Ioc_OutputCS50usMotPwmDataInfo = CreateBus(Ioc_OutputCS50usMotPwmDataInfo, DeList);
clear DeList;


