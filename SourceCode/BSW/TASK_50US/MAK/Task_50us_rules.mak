# \file
#
# \brief Task_50us
#
# This file contains the implementation of the SWC
# module Task_50us.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Task_50us_src

Task_50us_src_FILES        += $(Task_50us_SRC_PATH)\Task_50us.c
Task_50us_src_FILES        += $(Task_50us_IFA_PATH)\Task_50us_Ifa.c
Task_50us_src_FILES        += $(Task_50us_CFG_PATH)\Task_50us_Cfg.c
Task_50us_src_FILES        += $(Task_50us_CAL_PATH)\Task_50us_Cal.c
