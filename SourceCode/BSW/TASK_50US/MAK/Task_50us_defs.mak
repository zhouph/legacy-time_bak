# \file
#
# \brief Task_50us
#
# This file contains the implementation of the SWC
# module Task_50us.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Task_50us_CORE_PATH     := $(MANDO_BSW_ROOT)\Task_50us
Task_50us_CAL_PATH      := $(Task_50us_CORE_PATH)\CAL
Task_50us_SRC_PATH      := $(Task_50us_CORE_PATH)\SRC
Task_50us_CFG_PATH      := $(Task_50us_CORE_PATH)\CFG\$(Task_50us_VARIANT)
Task_50us_HDR_PATH      := $(Task_50us_CORE_PATH)\HDR
Task_50us_IFA_PATH      := $(Task_50us_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Task_50us_CMN_PATH      := $(Task_50us_CORE_PATH)\ICE\CMN
endif

CC_INCLUDE_PATH    += $(Task_50us_CAL_PATH)
CC_INCLUDE_PATH    += $(Task_50us_SRC_PATH)
CC_INCLUDE_PATH    += $(Task_50us_CFG_PATH)
CC_INCLUDE_PATH    += $(Task_50us_HDR_PATH)
CC_INCLUDE_PATH    += $(Task_50us_IFA_PATH)
CC_INCLUDE_PATH    += $(Task_50us_CMN_PATH)

