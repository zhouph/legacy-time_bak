/**
 * @defgroup Task_50us_Ifa Task_50us_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_50us_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_50US_IFA_H_
#define TASK_50US_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Task_50us_Read_Mtr_Processing_SigEemFailData(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigSwTrigMotInfo; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigWhlSpdInfo(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigWhlSpdInfo; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemEceData(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemEceData; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigMtrProcessDataInf(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigMtrProcessDataInf; \
}while(0);

#define Task_50us_Read_Acmio_SenMotMonInfo(data) do \
{ \
    *data = Task_50us_Acmio_SenMotMonInfo; \
}while(0);

#define Task_50us_Read_Acmio_SenMotVoltsMonInfo(data) do \
{ \
    *data = Task_50us_Acmio_SenMotVoltsMonInfo; \
}while(0);

#define Task_50us_Read_Msp_CtrlMotOrgSetStInfo(data) do \
{ \
    *data = Task_50us_Msp_CtrlMotOrgSetStInfo; \
}while(0);

#define Task_50us_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo(data) do \
{ \
    *data = Task_50us_Msp_CtrlPwrPistStBfMotOrgSetInfo; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotDqIRefMccInfo(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotDqIRefMccInfo; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotOrgSetStInfo(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotOrgSetStInfo; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotDqIRefSesInfo(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotDqIRefSesInfo; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlFluxWeakengStInfo(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlFluxWeakengStInfo; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorInfo(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorInfo; \
}while(0);

#define Task_50us_Read_Acmio_ActrMotReqDataDiagInfo(data) do \
{ \
    *data = Task_50us_Acmio_ActrMotReqDataDiagInfo; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_BBSSol(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_ESCSol(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_FrontSol(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_RearSol(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_RearSol; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_Motor; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MPS(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_MPS; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MGD(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_MGD; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo_MotPwrMon(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigSwTrigMotInfo.MotPwrMon; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo_StarMon(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigSwTrigMotInfo.StarMon; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo_UoutMon(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigSwTrigMotInfo.UoutMon; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo_VoutMon(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigSwTrigMotInfo.VoutMon; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo_WoutMon(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigSwTrigMotInfo.WoutMon; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEemEceData_Eem_Ece_Motor(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEemEceData.Eem_Ece_Motor; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigMtrProcessDataInf_MtrProCalibrationState(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigMtrProcessDataInf_MtrCaliResult(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult; \
}while(0);

#define Task_50us_Read_Acmio_SenMotMonInfo_MotPwrVoltMon(data) do \
{ \
    *data = Task_50us_Acmio_SenMotMonInfo.MotPwrVoltMon; \
}while(0);

#define Task_50us_Read_Acmio_SenMotVoltsMonInfo_MotVoltPhUMon(data) do \
{ \
    *data = Task_50us_Acmio_SenMotVoltsMonInfo.MotVoltPhUMon; \
}while(0);

#define Task_50us_Read_Acmio_SenMotVoltsMonInfo_MotVoltPhVMon(data) do \
{ \
    *data = Task_50us_Acmio_SenMotVoltsMonInfo.MotVoltPhVMon; \
}while(0);

#define Task_50us_Read_Acmio_SenMotVoltsMonInfo_MotVoltPhWMon(data) do \
{ \
    *data = Task_50us_Acmio_SenMotVoltsMonInfo.MotVoltPhWMon; \
}while(0);

#define Task_50us_Read_Msp_CtrlMotOrgSetStInfo_MotOrgSet(data) do \
{ \
    *data = Task_50us_Msp_CtrlMotOrgSetStInfo.MotOrgSet; \
}while(0);

#define Task_50us_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp(data) do \
{ \
    *data = Task_50us_Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp; \
}while(0);

#define Task_50us_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg(data) do \
{ \
    *data = Task_50us_Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotDqIRefMccInfo_IdRef(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotDqIRefMccInfo.IdRef; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotDqIRefMccInfo_IqRef(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotDqIRefMccInfo.IqRef; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotOrgSetStInfo_MotOrgSet(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotOrgSetStInfo.MotOrgSet; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotDqIRefSesInfo_IdRef(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotDqIRefSesInfo.IdRef; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotDqIRefSesInfo_IqRef(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotDqIRefSesInfo.IqRef; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlg(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlgOld(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData_IqSelected(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData.IqSelected; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData_IdSelected(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData.IdSelected; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData_UPhasePWMSelected(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData_VPhasePWMSelected(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData_WPhasePWMSelected(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorData_MtrCtrlMode(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorData.MtrCtrlMode; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorInfo_MtrArbCalMode(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbtratorInfo_MtrArbState(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbtratorInfo.MtrArbState; \
}while(0);

#define Task_50us_Read_Acmio_ActrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    *data = Task_50us_Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData; \
}while(0);

#define Task_50us_Read_Acmio_ActrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    *data = Task_50us_Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData; \
}while(0);

#define Task_50us_Read_Acmio_ActrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    *data = Task_50us_Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData; \
}while(0);

#define Task_50us_Read_Acmio_ActrMotReqDataDiagInfo_MotReq(data) do \
{ \
    *data = Task_50us_Acmio_ActrMotReqDataDiagInfo.MotReq; \
}while(0);

#define Task_50us_Read_Mps_TLE5012_Hndlr_50usEcuModeSts(data) do \
{ \
    *data = Task_50us_Mps_TLE5012_Hndlr_50usEcuModeSts; \
}while(0);

#define Task_50us_Read_AdcIf_Conv50usEcuModeSts(data) do \
{ \
    *data = Task_50us_AdcIf_Conv50usEcuModeSts; \
}while(0);

#define Task_50us_Read_AdcIf_Conv50usFuncInhibitAdcifSts(data) do \
{ \
    *data = Task_50us_AdcIf_Conv50usFuncInhibitAdcifSts; \
}while(0);

#define Task_50us_Read_Ioc_InputSR50usEcuModeSts(data) do \
{ \
    *data = Task_50us_Ioc_InputSR50usEcuModeSts; \
}while(0);

#define Task_50us_Read_Ioc_InputSR50usFuncInhibitIocSts(data) do \
{ \
    *data = Task_50us_Ioc_InputSR50usFuncInhibitIocSts; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigEcuModeSts(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigEcuModeSts; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigIgnOnOffSts(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigIgnOnOffSts; \
}while(0);

#define Task_50us_Read_Mtr_Processing_SigIgnEdgeSts(data) do \
{ \
    *data = Task_50us_Mtr_Processing_SigIgnEdgeSts; \
}while(0);

#define Task_50us_Read_Acmio_SenEcuModeSts(data) do \
{ \
    *data = Task_50us_Acmio_SenEcuModeSts; \
}while(0);

#define Task_50us_Read_Acmio_SenFuncInhibitAcmioSts(data) do \
{ \
    *data = Task_50us_Acmio_SenFuncInhibitAcmioSts; \
}while(0);

#define Task_50us_Read_Msp_CtrlEcuModeSts(data) do \
{ \
    *data = Task_50us_Msp_CtrlEcuModeSts; \
}while(0);

#define Task_50us_Read_Msp_CtrlFuncInhibitMspSts(data) do \
{ \
    *data = Task_50us_Msp_CtrlFuncInhibitMspSts; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlEcuModeSts(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlEcuModeSts; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlVdcLinkFild(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlVdcLinkFild; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotCtrlMode(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotCtrlMode; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMotCtrlState(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMotCtrlState; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlFuncInhibitAcmctlSts(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlFuncInhibitAcmctlSts; \
}while(0);

#define Task_50us_Read_Acmctl_CtrlMtrArbDriveState(data) do \
{ \
    *data = Task_50us_Acmctl_CtrlMtrArbDriveState; \
}while(0);

#define Task_50us_Read_Acmio_ActrEcuModeSts(data) do \
{ \
    *data = Task_50us_Acmio_ActrEcuModeSts; \
}while(0);

#define Task_50us_Read_Acmio_ActrFuncInhibitAcmioSts(data) do \
{ \
    *data = Task_50us_Acmio_ActrFuncInhibitAcmioSts; \
}while(0);

#define Task_50us_Read_Ioc_OutputCS50usEcuModeSts(data) do \
{ \
    *data = Task_50us_Ioc_OutputCS50usEcuModeSts; \
}while(0);

#define Task_50us_Read_Ioc_OutputCS50usFuncInhibitIocSts(data) do \
{ \
    *data = Task_50us_Ioc_OutputCS50usFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Uphase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_Vphase0Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon = *data; \
}while(0);

#define Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo_VPhase1Mon(data) do \
{ \
    Task_50us_AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    Task_50us_Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Task_50us_Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

#define Task_50us_Write_Acmio_SenVdcLink(data) do \
{ \
    Task_50us_Acmio_SenVdcLink = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_50US_IFA_H_ */
/** @} */
