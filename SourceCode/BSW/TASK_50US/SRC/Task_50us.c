/**
 * @defgroup Task_50us Task_50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_50us.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_50us.h"
#include "Task_50us_Ifa.h"
#include "Task_1ms.h"
#include "Task_5ms.h"
#include "Task_10ms.h"
/* Include runnables mapped to this task */
#include "Mps_TLE5012_Hndlr_50us.h"
#include "AdcIf_Conv50us.h"
#include "Ioc_InputSR50us.h"
#include "Mtr_Processing_Sig.h"
#include "Acmio_Sen.h"
#include "Msp_Ctrl.h"
#include "Acmctl_Ctrl.h"
#include "Acmio_Actr.h"
#include "Ioc_OutputCS50us.h"

#include "Gptm_Cdd.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define TASK_50US_START_SEC_CONST_UNSPECIFIED
#include "Task_50us_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define TASK_50US_STOP_SEC_CONST_UNSPECIFIED
#include "Task_50us_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_50us_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Task_50us_HdrBusType Task_50usBus;

/* Version Info */
const SwcVersionInfo_t Task_50usVersionInfo = 
{
    TASK_50US_MODULE_ID,       /* Task_50usVersionInfo.ModuleId */
    TASK_50US_MAJOR_VERSION,   /* Task_50usVersionInfo.MajorVer */
    TASK_50US_MINOR_VERSION,   /* Task_50usVersionInfo.MinorVer */
    TASK_50US_PATCH_VERSION,   /* Task_50usVersionInfo.PatchVer */
    TASK_50US_BRANCH_VERSION   /* Task_50usVersionInfo.BranchVer */
};

    
/* Input Data Element */
Eem_MainEemFailData_t Task_50us_Mtr_Processing_SigEemFailData;
AdcIf_Conv1msSwTrigMotInfo_t Task_50us_Mtr_Processing_SigSwTrigMotInfo;
Wss_SenWhlSpdInfo_t Task_50us_Mtr_Processing_SigWhlSpdInfo;
Eem_MainEemEceData_t Task_50us_Mtr_Processing_SigEemEceData;
Mtr_Processing_DiagMtrProcessDataInfo_t Task_50us_Mtr_Processing_SigMtrProcessDataInf;
Ioc_InputSR1msMotMonInfo_t Task_50us_Acmio_SenMotMonInfo;
Ioc_InputSR1msMotVoltsMonInfo_t Task_50us_Acmio_SenMotVoltsMonInfo;
Ses_CtrlMotOrgSetStInfo_t Task_50us_Msp_CtrlMotOrgSetStInfo;
Ses_CtrlPwrPistStBfMotOrgSetInfo_t Task_50us_Msp_CtrlPwrPistStBfMotOrgSetInfo;
Mcc_1msCtrlMotDqIRefMccInfo_t Task_50us_Acmctl_CtrlMotDqIRefMccInfo;
Ses_CtrlMotOrgSetStInfo_t Task_50us_Acmctl_CtrlMotOrgSetStInfo;
Ses_CtrlMotDqIRefSesInfo_t Task_50us_Acmctl_CtrlMotDqIRefSesInfo;
Mcc_1msCtrlFluxWeakengStInfo_t Task_50us_Acmctl_CtrlFluxWeakengStInfo;
Arbitrator_MtrMtrArbitratorData_t Task_50us_Acmctl_CtrlMtrArbtratorData;
Arbitrator_MtrMtrArbitratorInfo_t Task_50us_Acmctl_CtrlMtrArbtratorInfo;
Diag_HndlrMotReqDataDiagInfo_t Task_50us_Acmio_ActrMotReqDataDiagInfo;
Mom_HndlrEcuModeSts_t Task_50us_Mps_TLE5012_Hndlr_50usEcuModeSts;
Mom_HndlrEcuModeSts_t Task_50us_AdcIf_Conv50usEcuModeSts;
Eem_SuspcDetnFuncInhibitAdcifSts_t Task_50us_AdcIf_Conv50usFuncInhibitAdcifSts;
Mom_HndlrEcuModeSts_t Task_50us_Ioc_InputSR50usEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_50us_Ioc_InputSR50usFuncInhibitIocSts;
Mom_HndlrEcuModeSts_t Task_50us_Mtr_Processing_SigEcuModeSts;
Prly_HndlrIgnOnOffSts_t Task_50us_Mtr_Processing_SigIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t Task_50us_Mtr_Processing_SigIgnEdgeSts;
Mom_HndlrEcuModeSts_t Task_50us_Acmio_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitAcmioSts_t Task_50us_Acmio_SenFuncInhibitAcmioSts;
Mom_HndlrEcuModeSts_t Task_50us_Msp_CtrlEcuModeSts;
Eem_SuspcDetnFuncInhibitMspSts_t Task_50us_Msp_CtrlFuncInhibitMspSts;
Mom_HndlrEcuModeSts_t Task_50us_Acmctl_CtrlEcuModeSts;
Msp_1msCtrlVdcLinkFild_t Task_50us_Acmctl_CtrlVdcLinkFild;
Mcc_1msCtrlMotCtrlMode_t Task_50us_Acmctl_CtrlMotCtrlMode;
Mcc_1msCtrlMotCtrlState_t Task_50us_Acmctl_CtrlMotCtrlState;
Eem_SuspcDetnFuncInhibitAcmctlSts_t Task_50us_Acmctl_CtrlFuncInhibitAcmctlSts;
Arbitrator_MtrMtrArbDriveState_t Task_50us_Acmctl_CtrlMtrArbDriveState;
Mom_HndlrEcuModeSts_t Task_50us_Acmio_ActrEcuModeSts;
Eem_SuspcDetnFuncInhibitAcmioSts_t Task_50us_Acmio_ActrFuncInhibitAcmioSts;
Mom_HndlrEcuModeSts_t Task_50us_Ioc_OutputCS50usEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Task_50us_Ioc_OutputCS50usFuncInhibitIocSts;

/* Output Data Element */
AdcIf_Conv50usHwTrigMotInfo_t Task_50us_AdcIf_Conv50usHwTrigMotInfo;
Ioc_InputSR50usMotCurrMonInfo_t Task_50us_Ioc_InputSR50usMotCurrMonInfo;
Ioc_InputSR50usMotAngleMonInfo_t Task_50us_Ioc_InputSR50usMotAngleMonInfo;
Mtr_Processing_SigMtrProcessOutInfo_t Task_50us_Mtr_Processing_SigMtrProcessOutInfo;
Msp_CtrlMotRotgAgSigInfo_t Task_50us_Msp_CtrlMotRotgAgSigInfo;
Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;
Acmctl_CtrlMotDqIMeasdInfo_t Task_50us_Acmctl_CtrlMotDqIMeasdInfo;
Acmio_SenVdcLink_t Task_50us_Acmio_SenVdcLink;

uint8 Task_50us_toggle;

#define TASK_50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_50us_MemMap.h"
#define TASK_50US_START_SEC_VAR_NOINIT_32BIT
#include "Task_50us_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_50us_MemMap.h"
#define TASK_50US_START_SEC_VAR_UNSPECIFIED
#include "Task_50us_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_50US_STOP_SEC_VAR_UNSPECIFIED
#include "Task_50us_MemMap.h"
#define TASK_50US_START_SEC_VAR_32BIT
#include "Task_50us_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_50US_STOP_SEC_VAR_32BIT
#include "Task_50us_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define TASK_50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_50us_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define TASK_50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Task_50us_MemMap.h"
#define TASK_50US_START_SEC_VAR_NOINIT_32BIT
#include "Task_50us_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define TASK_50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Task_50us_MemMap.h"
#define TASK_50US_START_SEC_VAR_UNSPECIFIED
#include "Task_50us_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define TASK_50US_STOP_SEC_VAR_UNSPECIFIED
#include "Task_50us_MemMap.h"
#define TASK_50US_START_SEC_VAR_32BIT
#include "Task_50us_MemMap.h"
/** Variable Section (32BIT)**/


#define TASK_50US_STOP_SEC_VAR_32BIT
#include "Task_50us_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define TASK_50US_START_SEC_CODE
#include "Task_50us_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Task_50us_Init(void)
{    
    Gpt12IIF_Init();
    Mps_TLE5012_Hndlr_50us_Init();
    AdcIf_Conv50us_Init();
    Ioc_InputSR50us_Init();
    Mtr_Processing_Sig_Init();
    Acmio_Sen_Init();
    Msp_Ctrl_Init();
    Acmctl_Ctrl_Init();
    Acmio_Actr_Init();
    Ioc_OutputCS50us_Init();
    
    /* Initialize internal bus */
}

void Task_50us(void)
{
    uint16 i;
    /* "Read Interfaces" from <other Task> or <ISR Event>
    Assembly connection event between Tasks or Task and ISR
    for example #1) Task1msInterface1 = Task10msInterface1;
    for example #2) Task1msInterface1.Signal1 = Task10msInterface1.Signal1;
    for example #3) In case of Queue at <Shared RAM>, Task1msInterface1 = deQueue(&q);
    Therefore, it's possible to avoid memory violation.*/
    /* Sample interface */
    /* Structure interface */
    Task_50us_Mtr_Processing_SigEemFailData = Task_5ms_Eem_MainEemFailData;
    Task_50us_Mtr_Processing_SigSwTrigMotInfo = Task_1ms_AdcIf_Conv1msSwTrigMotInfo;
    Task_50us_Mtr_Processing_SigWhlSpdInfo = Task_5ms_Wss_SenWhlSpdInfo;
    Task_50us_Mtr_Processing_SigEemEceData = Task_5ms_Eem_MainEemEceData;
    Task_50us_Mtr_Processing_SigMtrProcessDataInf = Task_1ms_Mtr_Processing_DiagMtrProcessDataInf;
    Task_50us_Acmio_SenMotMonInfo = Task_1ms_Ioc_InputSR1msMotMonInfo;
    Task_50us_Acmio_SenMotVoltsMonInfo = Task_1ms_Ioc_InputSR1msMotVoltsMonInfo;
    Task_50us_Msp_CtrlMotOrgSetStInfo = Task_1ms_Ses_CtrlMotOrgSetStInfo;
    Task_50us_Msp_CtrlPwrPistStBfMotOrgSetInfo = Task_1ms_Ses_CtrlPwrPistStBfMotOrgSetInfo;
    Task_50us_Acmctl_CtrlMotDqIRefMccInfo = Task_1ms_Mcc_1msCtrlMotDqIRefMccInfo;
    Task_50us_Acmctl_CtrlMotOrgSetStInfo = Task_1ms_Ses_CtrlMotOrgSetStInfo;
    Task_50us_Acmctl_CtrlMotDqIRefSesInfo = Task_1ms_Ses_CtrlMotDqIRefSesInfo;
    Task_50us_Acmctl_CtrlFluxWeakengStInfo = Task_1ms_Mcc_1msCtrlFluxWeakengStInfo;
    Task_50us_Acmctl_CtrlMtrArbtratorData = Task_1ms_Arbitrator_MtrMtrArbtratorData;
    Task_50us_Acmctl_CtrlMtrArbtratorInfo = Task_1ms_Arbitrator_MtrMtrArbtratorInfo;
    Task_50us_Acmio_ActrMotReqDataDiagInfo = Task_10ms_Diag_HndlrMotReqDataDiagInfo;

    /* Single interface */
	/*
    Task_50us_Mps_TLE5012_Hndlr_50usEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_AdcIf_Conv50usEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_AdcIf_Conv50usFuncInhibitAdcifSts = Task_5ms_Eem_SuspcDetnFuncInhibitAdcifSts;
    Task_50us_Ioc_InputSR50usEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_Ioc_InputSR50usFuncInhibitIocSts = Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
    Task_50us_Mtr_Processing_SigEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    */
    Task_50us_Mtr_Processing_SigIgnOnOffSts = Task_5ms_Prly_HndlrIgnOnOffSts;
    Task_50us_Mtr_Processing_SigIgnEdgeSts = Task_5ms_Prly_HndlrIgnEdgeSts;
    /*
    Task_50us_Acmio_SenEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_Acmio_SenFuncInhibitAcmioSts = Task_5ms_Eem_SuspcDetnFuncInhibitAcmioSts;
    Task_50us_Msp_CtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_Msp_CtrlFuncInhibitMspSts = Task_5ms_Eem_SuspcDetnFuncInhibitMspSts;
    Task_50us_Acmctl_CtrlEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    */
    Task_50us_Acmctl_CtrlVdcLinkFild = Task_1ms_Msp_1msCtrlVdcLinkFild;
    Task_50us_Acmctl_CtrlMotCtrlMode = Task_1ms_Mcc_1msCtrlMotCtrlMode;
    Task_50us_Acmctl_CtrlMotCtrlState = Task_1ms_Mcc_1msCtrlMotCtrlState;
    /*
    Task_50us_Acmctl_CtrlFuncInhibitAcmctlSts = Task_5ms_Eem_SuspcDetnFuncInhibitAcmctlSts;
    Task_50us_Acmio_ActrEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_Acmio_ActrFuncInhibitAcmioSts = Task_5ms_Eem_SuspcDetnFuncInhibitAcmioSts;
    Task_50us_Ioc_OutputCS50usEcuModeSts = Task_5ms_Mom_HndlrEcuModeSts;
    Task_50us_Ioc_OutputCS50usFuncInhibitIocSts = Task_5ms_Eem_SuspcDetnFuncInhibitIocSts;
    */
    /* End of Sample interface

    /* Input */
    /* Structure interface */
    /* Decomposed structure interface */
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_BBSSol(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_ESCSol(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_FrontSol(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_RearSol(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_RearSol);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_Motor(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_Motor);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MPS(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_MPS);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_MGD(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_MGD);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_OverVolt(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_UnderVolt(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowVolt(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt);
    Task_50us_Read_Mtr_Processing_SigEemFailData_Eem_Fail_LowerVolt(&Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt);

    Task_50us_Read_Mtr_Processing_SigSwTrigMotInfo(&Task_50usBus.Mtr_Processing_SigSwTrigMotInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigSwTrigMotInfo 
     : Mtr_Processing_SigSwTrigMotInfo.MotPwrMon;
     : Mtr_Processing_SigSwTrigMotInfo.StarMon;
     : Mtr_Processing_SigSwTrigMotInfo.UoutMon;
     : Mtr_Processing_SigSwTrigMotInfo.VoutMon;
     : Mtr_Processing_SigSwTrigMotInfo.WoutMon;
     =============================================================================*/

    Task_50us_Read_Mtr_Processing_SigWhlSpdInfo(&Task_50usBus.Mtr_Processing_SigWhlSpdInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigWhlSpdInfo 
     : Mtr_Processing_SigWhlSpdInfo.FlWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.FrWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.RlWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_50us_Read_Mtr_Processing_SigEemEceData_Eem_Ece_Motor(&Task_50usBus.Mtr_Processing_SigEemEceData.Eem_Ece_Motor);

    Task_50us_Read_Mtr_Processing_SigMtrProcessDataInf(&Task_50usBus.Mtr_Processing_SigMtrProcessDataInf);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMtrProcessDataInf 
     : Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState;
     : Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_50us_Read_Acmio_SenMotMonInfo_MotPwrVoltMon(&Task_50usBus.Acmio_SenMotMonInfo.MotPwrVoltMon);

    Task_50us_Read_Acmio_SenMotVoltsMonInfo(&Task_50usBus.Acmio_SenMotVoltsMonInfo);
    /*==============================================================================
    * Members of structure Acmio_SenMotVoltsMonInfo 
     : Acmio_SenMotVoltsMonInfo.MotVoltPhUMon;
     : Acmio_SenMotVoltsMonInfo.MotVoltPhVMon;
     : Acmio_SenMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_50us_Read_Msp_CtrlMotOrgSetStInfo_MotOrgSet(&Task_50usBus.Msp_CtrlMotOrgSetStInfo.MotOrgSet);

    Task_50us_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo(&Task_50usBus.Msp_CtrlPwrPistStBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Msp_CtrlPwrPistStBfMotOrgSetInfo 
     : Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
     : Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
     =============================================================================*/

    Task_50us_Read_Acmctl_CtrlMotDqIRefMccInfo(&Task_50usBus.Acmctl_CtrlMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotDqIRefMccInfo 
     : Acmctl_CtrlMotDqIRefMccInfo.IdRef;
     : Acmctl_CtrlMotDqIRefMccInfo.IqRef;
     =============================================================================*/

    /* Decomposed structure interface */
    Task_50us_Read_Acmctl_CtrlMotOrgSetStInfo_MotOrgSet(&Task_50usBus.Acmctl_CtrlMotOrgSetStInfo.MotOrgSet);

    Task_50us_Read_Acmctl_CtrlMotDqIRefSesInfo(&Task_50usBus.Acmctl_CtrlMotDqIRefSesInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotDqIRefSesInfo 
     : Acmctl_CtrlMotDqIRefSesInfo.IdRef;
     : Acmctl_CtrlMotDqIRefSesInfo.IqRef;
     =============================================================================*/

    Task_50us_Read_Acmctl_CtrlFluxWeakengStInfo(&Task_50usBus.Acmctl_CtrlFluxWeakengStInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlFluxWeakengStInfo 
     : Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg;
     : Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld;
     =============================================================================*/

    Task_50us_Read_Acmctl_CtrlMtrArbtratorData(&Task_50usBus.Acmctl_CtrlMtrArbtratorData);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMtrArbtratorData 
     : Acmctl_CtrlMtrArbtratorData.IqSelected;
     : Acmctl_CtrlMtrArbtratorData.IdSelected;
     : Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected;
     : Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected;
     : Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected;
     : Acmctl_CtrlMtrArbtratorData.MtrCtrlMode;
     =============================================================================*/

    Task_50us_Read_Acmctl_CtrlMtrArbtratorInfo(&Task_50usBus.Acmctl_CtrlMtrArbtratorInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMtrArbtratorInfo 
     : Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode;
     : Acmctl_CtrlMtrArbtratorInfo.MtrArbState;
     =============================================================================*/

    Task_50us_Read_Acmio_ActrMotReqDataDiagInfo(&Task_50usBus.Acmio_ActrMotReqDataDiagInfo);
    /*==============================================================================
    * Members of structure Acmio_ActrMotReqDataDiagInfo 
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData;
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData;
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData;
     : Acmio_ActrMotReqDataDiagInfo.MotReq;
     =============================================================================*/

    /* Single interface */
     /*
    Task_50us_Read_Mps_TLE5012_Hndlr_50usEcuModeSts(&Task_50usBus.Mps_TLE5012_Hndlr_50usEcuModeSts);
    Task_50us_Read_AdcIf_Conv50usEcuModeSts(&Task_50usBus.AdcIf_Conv50usEcuModeSts);
    Task_50us_Read_AdcIf_Conv50usFuncInhibitAdcifSts(&Task_50usBus.AdcIf_Conv50usFuncInhibitAdcifSts);
    Task_50us_Read_Ioc_InputSR50usEcuModeSts(&Task_50usBus.Ioc_InputSR50usEcuModeSts);
    Task_50us_Read_Ioc_InputSR50usFuncInhibitIocSts(&Task_50usBus.Ioc_InputSR50usFuncInhibitIocSts);
    Task_50us_Read_Mtr_Processing_SigEcuModeSts(&Task_50usBus.Mtr_Processing_SigEcuModeSts);
    */
    Task_50us_Read_Mtr_Processing_SigIgnOnOffSts(&Task_50usBus.Mtr_Processing_SigIgnOnOffSts);
    Task_50us_Read_Mtr_Processing_SigIgnEdgeSts(&Task_50usBus.Mtr_Processing_SigIgnEdgeSts);
    /*
    Task_50us_Read_Acmio_SenEcuModeSts(&Task_50usBus.Acmio_SenEcuModeSts);
    Task_50us_Read_Acmio_SenFuncInhibitAcmioSts(&Task_50usBus.Acmio_SenFuncInhibitAcmioSts);
    Task_50us_Read_Msp_CtrlEcuModeSts(&Task_50usBus.Msp_CtrlEcuModeSts);
    Task_50us_Read_Msp_CtrlFuncInhibitMspSts(&Task_50usBus.Msp_CtrlFuncInhibitMspSts);
    Task_50us_Read_Acmctl_CtrlEcuModeSts(&Task_50usBus.Acmctl_CtrlEcuModeSts);
    */
    Task_50us_Read_Acmctl_CtrlVdcLinkFild(&Task_50usBus.Acmctl_CtrlVdcLinkFild);
    Task_50us_Read_Acmctl_CtrlMotCtrlMode(&Task_50usBus.Acmctl_CtrlMotCtrlMode);
    Task_50us_Read_Acmctl_CtrlMotCtrlState(&Task_50usBus.Acmctl_CtrlMotCtrlState);
    /*
    Task_50us_Read_Acmctl_CtrlFuncInhibitAcmctlSts(&Task_50usBus.Acmctl_CtrlFuncInhibitAcmctlSts);
    Task_50us_Read_Acmio_ActrEcuModeSts(&Task_50usBus.Acmio_ActrEcuModeSts);
    Task_50us_Read_Acmio_ActrFuncInhibitAcmioSts(&Task_50usBus.Acmio_ActrFuncInhibitAcmioSts);
    Task_50us_Read_Ioc_OutputCS50usEcuModeSts(&Task_50usBus.Ioc_OutputCS50usEcuModeSts);
    Task_50us_Read_Ioc_OutputCS50usFuncInhibitIocSts(&Task_50usBus.Ioc_OutputCS50usFuncInhibitIocSts);
    */
    /* Process */
    /**********************************************************************************************
     Mps_TLE5012_Hndlr_50us start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Mps_TLE5012_Hndlr_50usEcuModeSts = Task_50usBus.Mps_TLE5012_Hndlr_50usEcuModeSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Mps_TLE5012_Hndlr_50us();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Mps_TLE5012_Hndlr_50us end
     **********************************************************************************************/

    /**********************************************************************************************
     AdcIf_Conv50us start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //AdcIf_Conv50usEcuModeSts = Task_50usBus.AdcIf_Conv50usEcuModeSts;
    //AdcIf_Conv50usFuncInhibitAdcifSts = Task_50usBus.AdcIf_Conv50usFuncInhibitAdcifSts;

    /* Inter-Runnable structure interface */
    /* Inter-Runnable single interface */

    /* Execute  runnable */
    AdcIf_Conv50us();

    /* Structure interface */
    Task_50usBus.AdcIf_Conv50usHwTrigMotInfo = AdcIf_Conv50usHwTrigMotInfo;
    /*==============================================================================
    * Members of structure AdcIf_Conv50usHwTrigMotInfo 
     : AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     AdcIf_Conv50us end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_InputSR50us start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Ioc_InputSR50usEcuModeSts = Task_50usBus.Ioc_InputSR50usEcuModeSts;
    //Ioc_InputSR50usFuncInhibitIocSts = Task_50usBus.Ioc_InputSR50usFuncInhibitIocSts;

    /* Inter-Runnable structure interface */
    Ioc_InputSR50usMpsD1IIFAngInfo = Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMpsD1IIFAngInfo 
     : Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngRawData;
     : Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngDegree;
     =============================================================================*/

    Ioc_InputSR50usMpsD2IIFAngInfo = Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMpsD2IIFAngInfo 
     : Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngRawData;
     : Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngDegree;
     =============================================================================*/

    Ioc_InputSR50usHwTrigMotInfo = AdcIf_Conv50usHwTrigMotInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR50usHwTrigMotInfo 
     : Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon;
     : Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon;
     : Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon;
     : Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Ioc_InputSR50us();

    /* Structure interface */
    Task_50usBus.Ioc_InputSR50usMotCurrMonInfo = Ioc_InputSR50usMotCurrMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMotCurrMonInfo 
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/

    Task_50usBus.Ioc_InputSR50usMotAngleMonInfo = Ioc_InputSR50usMotAngleMonInfo;
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMotAngleMonInfo 
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Ioc_InputSR50us end
     **********************************************************************************************/

    /**********************************************************************************************
     Mtr_Processing_Sig start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_BBSSol;
    Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_ESCSol;
    Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_FrontSol;
    Mtr_Processing_SigEemFailData.Eem_Fail_RearSol = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_RearSol;
    Mtr_Processing_SigEemFailData.Eem_Fail_Motor = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_Motor;
    Mtr_Processing_SigEemFailData.Eem_Fail_MPS = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_MPS;
    Mtr_Processing_SigEemFailData.Eem_Fail_MGD = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_MGD;
    Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_OverVolt;
    Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_UnderVolt;
    Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowVolt;
    Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt = Task_50usBus.Mtr_Processing_SigEemFailData.Eem_Fail_LowerVolt;

    Mtr_Processing_SigSwTrigMotInfo = Task_50usBus.Mtr_Processing_SigSwTrigMotInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigSwTrigMotInfo 
     : Mtr_Processing_SigSwTrigMotInfo.MotPwrMon;
     : Mtr_Processing_SigSwTrigMotInfo.StarMon;
     : Mtr_Processing_SigSwTrigMotInfo.UoutMon;
     : Mtr_Processing_SigSwTrigMotInfo.VoutMon;
     : Mtr_Processing_SigSwTrigMotInfo.WoutMon;
     =============================================================================*/

    Mtr_Processing_SigWhlSpdInfo = Task_50usBus.Mtr_Processing_SigWhlSpdInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigWhlSpdInfo 
     : Mtr_Processing_SigWhlSpdInfo.FlWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.FrWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.RlWhlSpd;
     : Mtr_Processing_SigWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/

    /* Decomposed structure interface */
    Mtr_Processing_SigEemEceData.Eem_Ece_Motor = Task_50usBus.Mtr_Processing_SigEemEceData.Eem_Ece_Motor;

    Mtr_Processing_SigMtrProcessDataInf = Task_50usBus.Mtr_Processing_SigMtrProcessDataInf;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMtrProcessDataInf 
     : Mtr_Processing_SigMtrProcessDataInf.MtrProCalibrationState;
     : Mtr_Processing_SigMtrProcessDataInf.MtrCaliResult;
     =============================================================================*/

    /* Single interface */
    //Mtr_Processing_SigEcuModeSts = Task_50usBus.Mtr_Processing_SigEcuModeSts;
    Mtr_Processing_SigIgnOnOffSts = Task_50usBus.Mtr_Processing_SigIgnOnOffSts;
    Mtr_Processing_SigIgnEdgeSts = Task_50usBus.Mtr_Processing_SigIgnEdgeSts;

    /* Inter-Runnable structure interface */
    Mtr_Processing_SigHwTrigMotInfo = AdcIf_Conv50usHwTrigMotInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigHwTrigMotInfo 
     : Mtr_Processing_SigHwTrigMotInfo.Uphase0Mon;
     : Mtr_Processing_SigHwTrigMotInfo.Uphase1Mon;
     : Mtr_Processing_SigHwTrigMotInfo.Vphase0Mon;
     : Mtr_Processing_SigHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/

    Mtr_Processing_SigMotCurrMonInfo = Ioc_InputSR50usMotCurrMonInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMotCurrMonInfo 
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon0;
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhUMon1;
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon0;
     : Mtr_Processing_SigMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/

    Mtr_Processing_SigMotAngleMonInfo = Ioc_InputSR50usMotAngleMonInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMotAngleMonInfo 
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1deg;
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2deg;
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle1raw;
     : Mtr_Processing_SigMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Mtr_Processing_Sig();

    /* Structure interface */
    Task_50usBus.Mtr_Processing_SigMtrProcessOutInfo = Mtr_Processing_SigMtrProcessOutInfo;
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMtrProcessOutInfo 
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg;
     =============================================================================*/

    /* Single interface */

    /**********************************************************************************************
     Mtr_Processing_Sig end
     **********************************************************************************************/

    /**********************************************************************************************
     Acmio_Sen start
     **********************************************************************************************/

     /* Structure interface */
    /* Decomposed structure interface */
    Acmio_SenMotMonInfo.MotPwrVoltMon = Task_50usBus.Acmio_SenMotMonInfo.MotPwrVoltMon;

    Acmio_SenMotVoltsMonInfo = Task_50usBus.Acmio_SenMotVoltsMonInfo;
    /*==============================================================================
    * Members of structure Acmio_SenMotVoltsMonInfo 
     : Acmio_SenMotVoltsMonInfo.MotVoltPhUMon;
     : Acmio_SenMotVoltsMonInfo.MotVoltPhVMon;
     : Acmio_SenMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/

    /* Single interface */
    //Acmio_SenEcuModeSts = Task_50usBus.Acmio_SenEcuModeSts;
    //Acmio_SenFuncInhibitAcmioSts = Task_50usBus.Acmio_SenFuncInhibitAcmioSts;

    /* Inter-Runnable structure interface */
    Acmio_SenMotCurrMonInfo = Ioc_InputSR50usMotCurrMonInfo;
    /*==============================================================================
    * Members of structure Acmio_SenMotCurrMonInfo 
     : Acmio_SenMotCurrMonInfo.MotCurrPhUMon0;
     : Acmio_SenMotCurrMonInfo.MotCurrPhUMon1;
     : Acmio_SenMotCurrMonInfo.MotCurrPhVMon0;
     : Acmio_SenMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/

    Acmio_SenMotAngleMonInfo = Ioc_InputSR50usMotAngleMonInfo;
    /*==============================================================================
    * Members of structure Acmio_SenMotAngleMonInfo 
     : Acmio_SenMotAngleMonInfo.MotPosiAngle1deg;
     : Acmio_SenMotAngleMonInfo.MotPosiAngle2deg;
     : Acmio_SenMotAngleMonInfo.MotPosiAngle1raw;
     : Acmio_SenMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Acmio_Sen();

    /* Structure interface */
    /* Single interface */
    Task_50usBus.Acmio_SenVdcLink = Acmio_SenVdcLink;

    /**********************************************************************************************
     Acmio_Sen end
     **********************************************************************************************/

    /**********************************************************************************************
     Msp_Ctrl start
     **********************************************************************************************/
	Task_50us_toggle ^= 1;
	if(Task_50us_toggle == 0)
	{
	     /* Structure interface */
	    /* Decomposed structure interface */
	    Msp_CtrlMotOrgSetStInfo.MotOrgSet = Task_50usBus.Msp_CtrlMotOrgSetStInfo.MotOrgSet;

	    Msp_CtrlPwrPistStBfMotOrgSetInfo = Task_50usBus.Msp_CtrlPwrPistStBfMotOrgSetInfo;
	    /*==============================================================================
	    * Members of structure Msp_CtrlPwrPistStBfMotOrgSetInfo 
	     : Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
	     : Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
	     =============================================================================*/

	    /* Single interface */
	    //Msp_CtrlEcuModeSts = Task_50usBus.Msp_CtrlEcuModeSts;
	    //Msp_CtrlFuncInhibitMspSts = Task_50usBus.Msp_CtrlFuncInhibitMspSts;

	    /* Inter-Runnable structure interface */
	    Msp_CtrlMotAngle1Info = Acmio_SenMotAngle1Info;
	    /*==============================================================================
	    * Members of structure Msp_CtrlMotAngle1Info 
	     : Msp_CtrlMotAngle1Info.MotElecAngle1;
	     : Msp_CtrlMotAngle1Info.MotMechAngle1;
	     =============================================================================*/

	    /* Inter-Runnable single interface */

	    /* Execute  runnable */
	    Msp_Ctrl();

	    /* Structure interface */
	    Task_50usBus.Msp_CtrlMotRotgAgSigInfo = Msp_CtrlMotRotgAgSigInfo;
	    /*==============================================================================
	    * Members of structure Msp_CtrlMotRotgAgSigInfo 
	     : Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd;
	     : Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild;
	     : Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild;
	     =============================================================================*/

	    Task_50usBus.Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;
	    /*==============================================================================
	    * Members of structure Msp_CtrlMotRotgAgSigBfMotOrgSetInfo 
	     : Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
	     : Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
	     =============================================================================*/

	    /* Single interface */

	    /**********************************************************************************************
	     Msp_Ctrl end
	     **********************************************************************************************/

	    /**********************************************************************************************
	     Acmctl_Ctrl start
	     **********************************************************************************************/

	     /* Structure interface */
	    Acmctl_CtrlMotDqIRefMccInfo = Task_50usBus.Acmctl_CtrlMotDqIRefMccInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMotDqIRefMccInfo 
	     : Acmctl_CtrlMotDqIRefMccInfo.IdRef;
	     : Acmctl_CtrlMotDqIRefMccInfo.IqRef;
	     =============================================================================*/

	    /* Decomposed structure interface */
	    Acmctl_CtrlMotOrgSetStInfo.MotOrgSet = Task_50usBus.Acmctl_CtrlMotOrgSetStInfo.MotOrgSet;

	    Acmctl_CtrlMotDqIRefSesInfo = Task_50usBus.Acmctl_CtrlMotDqIRefSesInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMotDqIRefSesInfo 
	     : Acmctl_CtrlMotDqIRefSesInfo.IdRef;
	     : Acmctl_CtrlMotDqIRefSesInfo.IqRef;
	     =============================================================================*/

	    Acmctl_CtrlFluxWeakengStInfo = Task_50usBus.Acmctl_CtrlFluxWeakengStInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlFluxWeakengStInfo 
	     : Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg;
	     : Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld;
	     =============================================================================*/

	    Acmctl_CtrlMtrArbtratorData = Task_50usBus.Acmctl_CtrlMtrArbtratorData;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMtrArbtratorData 
	     : Acmctl_CtrlMtrArbtratorData.IqSelected;
	     : Acmctl_CtrlMtrArbtratorData.IdSelected;
	     : Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected;
	     : Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected;
	     : Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected;
	     : Acmctl_CtrlMtrArbtratorData.MtrCtrlMode;
	     =============================================================================*/

	    Acmctl_CtrlMtrArbtratorInfo = Task_50usBus.Acmctl_CtrlMtrArbtratorInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMtrArbtratorInfo 
	     : Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode;
	     : Acmctl_CtrlMtrArbtratorInfo.MtrArbState;
	     =============================================================================*/

	    /* Single interface */
	    //Acmctl_CtrlEcuModeSts = Task_50usBus.Acmctl_CtrlEcuModeSts;
	    Acmctl_CtrlVdcLinkFild = Task_50usBus.Acmctl_CtrlVdcLinkFild;
	    Acmctl_CtrlMotCtrlMode = Task_50usBus.Acmctl_CtrlMotCtrlMode;
	    Acmctl_CtrlMotCtrlState = Task_50usBus.Acmctl_CtrlMotCtrlState;
	    //Acmctl_CtrlFuncInhibitAcmctlSts = Task_50usBus.Acmctl_CtrlFuncInhibitAcmctlSts;
	    Acmctl_CtrlMtrArbDriveState = Task_50usBus.Acmctl_CtrlMtrArbDriveState;

	    /* Inter-Runnable structure interface */
	    Acmctl_CtrlMotCurrInfo = Acmio_SenMotCurrInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMotCurrInfo 
	     : Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd;
	     : Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd;
	     : Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd;
	     =============================================================================*/

	    /* Decomposed structure interface */
	    Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild = Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild;

	    Acmctl_CtrlMtrProcessOutInfo = Mtr_Processing_SigMtrProcessOutInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMtrProcessOutInfo 
	     : Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd;
	     : Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd;
	     : Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd;
	     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1raw;
	     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2raw;
	     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
	     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2_1_100Deg;
	     =============================================================================*/

	    /* Inter-Runnable single interface */

	    /* Execute  runnable */
	    Acmctl_Ctrl();

	    /* Structure interface */
	    Task_50usBus.Acmctl_CtrlMotDqIMeasdInfo = Acmctl_CtrlMotDqIMeasdInfo;
	    /*==============================================================================
	    * Members of structure Acmctl_CtrlMotDqIMeasdInfo 
	     : Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd;
	     : Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd;
	     =============================================================================*/

	    /* Single interface */
	}
    /**********************************************************************************************
     Acmctl_Ctrl end
     **********************************************************************************************/

    /**********************************************************************************************
     Acmio_Actr start
     **********************************************************************************************/

     /* Structure interface */
    Acmio_ActrMotReqDataDiagInfo = Task_50usBus.Acmio_ActrMotReqDataDiagInfo;
    /*==============================================================================
    * Members of structure Acmio_ActrMotReqDataDiagInfo 
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData;
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData;
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData;
     : Acmio_ActrMotReqDataDiagInfo.MotReq;
     =============================================================================*/

    /* Single interface */
    //Acmio_ActrEcuModeSts = Task_50usBus.Acmio_ActrEcuModeSts;
    //Acmio_ActrFuncInhibitAcmioSts = Task_50usBus.Acmio_ActrFuncInhibitAcmioSts;

    /* Inter-Runnable structure interface */
    Acmio_ActrMotReqDataAcmctlInfo = Acmctl_CtrlMotReqDataAcmctlInfo;
    /*==============================================================================
    * Members of structure Acmio_ActrMotReqDataAcmctlInfo 
     : Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData;
     : Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData;
     : Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData;
     : Acmio_ActrMotReqDataAcmctlInfo.MotReq;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Acmio_Actr();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Acmio_Actr end
     **********************************************************************************************/

    /**********************************************************************************************
     Ioc_OutputCS50us start
     **********************************************************************************************/

     /* Structure interface */
    /* Single interface */
    //Ioc_OutputCS50usEcuModeSts = Task_50usBus.Ioc_OutputCS50usEcuModeSts;
    //Ioc_OutputCS50usFuncInhibitIocSts = Task_50usBus.Ioc_OutputCS50usFuncInhibitIocSts;

    /* Inter-Runnable structure interface */
    Ioc_OutputCS50usMotPwmDataInfo = Acmio_ActrMotPwmDataInfo;
    /*==============================================================================
    * Members of structure Ioc_OutputCS50usMotPwmDataInfo 
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData;
     =============================================================================*/

    /* Inter-Runnable single interface */

    /* Execute  runnable */
    Ioc_OutputCS50us();

    /* Structure interface */
    /* Single interface */

    /**********************************************************************************************
     Ioc_OutputCS50us end
     **********************************************************************************************/


    /* Output */
    Task_50us_Write_AdcIf_Conv50usHwTrigMotInfo(&Task_50usBus.AdcIf_Conv50usHwTrigMotInfo);
    /*==============================================================================
    * Members of structure AdcIf_Conv50usHwTrigMotInfo 
     : AdcIf_Conv50usHwTrigMotInfo.Uphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Uphase1Mon;
     : AdcIf_Conv50usHwTrigMotInfo.Vphase0Mon;
     : AdcIf_Conv50usHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/

    Task_50us_Write_Ioc_InputSR50usMotCurrMonInfo(&Task_50usBus.Ioc_InputSR50usMotCurrMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMotCurrMonInfo 
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/

    Task_50us_Write_Ioc_InputSR50usMotAngleMonInfo(&Task_50usBus.Ioc_InputSR50usMotAngleMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMotAngleMonInfo 
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/

    Task_50us_Write_Mtr_Processing_SigMtrProcessOutInfo(&Task_50usBus.Mtr_Processing_SigMtrProcessOutInfo);
    /*==============================================================================
    * Members of structure Mtr_Processing_SigMtrProcessOutInfo 
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhWMeasd;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1raw;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2raw;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
     : Mtr_Processing_SigMtrProcessOutInfo.MotPosiAngle2_1_100Deg;
     =============================================================================*/

    Task_50us_Write_Msp_CtrlMotRotgAgSigInfo(&Task_50usBus.Msp_CtrlMotRotgAgSigInfo);
    /*==============================================================================
    * Members of structure Msp_CtrlMotRotgAgSigInfo 
     : Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd;
     : Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild;
     : Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild;
     =============================================================================*/

    Task_50us_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo(&Task_50usBus.Msp_CtrlMotRotgAgSigBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Msp_CtrlMotRotgAgSigBfMotOrgSetInfo 
     : Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
     : Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
     =============================================================================*/

    Task_50us_Write_Acmctl_CtrlMotDqIMeasdInfo(&Task_50usBus.Acmctl_CtrlMotDqIMeasdInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotDqIMeasdInfo 
     : Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd;
     : Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd;
     =============================================================================*/

    Task_50us_Write_Acmio_SenVdcLink(&Task_50usBus.Acmio_SenVdcLink);
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define TASK_50US_STOP_SEC_CODE
#include "Task_50us_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
