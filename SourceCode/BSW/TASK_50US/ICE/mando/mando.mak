# \file
#
# \brief Mando make base module
#
# This file contains the implementation of the Mando make 
# base module.
#
# \author Mando, Advanced R&D, Korea


include mando/mando_cfg.mak
include $(wildcard $(PROJECT_ROOT)/MAK/*.mak)
CC_INCLUDE_PATH += EXT_HEADER

#MAK_FILE_SUFFIX    :=mak
#DEFS_FILE_SUFFIX   :=_defs.$(MAK_FILE_SUFFIX)
#RULES_FILE_SUFFIX  :=_rules.$(MAK_FILE_SUFFIX)
#CHECK_FILE_SUFFIX  :=_check.$(MAK_FILE_SUFFIX)
#CFG_FILE_SUFFIX    :=_cfg.$(MAK_FILE_SUFFIX)
#
#include $(MANDO_DIR)\mando_cfg.mak
#
#include $(foreach PLUGIN,$(MANDO_BSW_MODULES),        #$(MANDO_BSW_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(RULES_FILE_SUFFIX))      
#include $(foreach PLUGIN,$(MANDO_BSW_MODULES),        #$(MANDO_BSW_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(DEFS_FILE_SUFFIX))
#
#include $(foreach PLUGIN,$(MANDO_ASW_MODULES),        #$(MANDO_ASW_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(RULES_FILE_SUFFIX))      
#include $(foreach PLUGIN,$(MANDO_ASW_MODULES),        #$(MANDO_ASW_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(DEFS_FILE_SUFFIX))
#
#
#
#LOC_FILE = $(PROJECT_ROOT)\IDB_Project.lsl
#
#CC_OPT += -D_TASKING_C_TRICORE_=1
#
#CPP_OPTS += -D_TASKING_C_TRICORE_=1
#
#LINK_OPT += -D__CPU__=TC27X
#LINK_OPT += -D__PROC_TC27X__
#LINK_OPT += --core=mpe:vtc
