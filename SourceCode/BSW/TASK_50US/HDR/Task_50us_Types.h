/**
 * @defgroup Task_50us_Types Task_50us_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_50us_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_50US_TYPES_H_
#define TASK_50US_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t Mtr_Processing_SigEemFailData;
    AdcIf_Conv1msSwTrigMotInfo_t Mtr_Processing_SigSwTrigMotInfo;
    Wss_SenWhlSpdInfo_t Mtr_Processing_SigWhlSpdInfo;
    Eem_MainEemEceData_t Mtr_Processing_SigEemEceData;
    Mtr_Processing_DiagMtrProcessDataInfo_t Mtr_Processing_SigMtrProcessDataInf;
    Ioc_InputSR1msMotMonInfo_t Acmio_SenMotMonInfo;
    Ioc_InputSR1msMotVoltsMonInfo_t Acmio_SenMotVoltsMonInfo;
    Ses_CtrlMotOrgSetStInfo_t Msp_CtrlMotOrgSetStInfo;
    Ses_CtrlPwrPistStBfMotOrgSetInfo_t Msp_CtrlPwrPistStBfMotOrgSetInfo;
    Mcc_1msCtrlMotDqIRefMccInfo_t Acmctl_CtrlMotDqIRefMccInfo;
    Ses_CtrlMotOrgSetStInfo_t Acmctl_CtrlMotOrgSetStInfo;
    Ses_CtrlMotDqIRefSesInfo_t Acmctl_CtrlMotDqIRefSesInfo;
    Mcc_1msCtrlFluxWeakengStInfo_t Acmctl_CtrlFluxWeakengStInfo;
    Arbitrator_MtrMtrArbitratorData_t Acmctl_CtrlMtrArbtratorData;
    Arbitrator_MtrMtrArbitratorInfo_t Acmctl_CtrlMtrArbtratorInfo;
    Diag_HndlrMotReqDataDiagInfo_t Acmio_ActrMotReqDataDiagInfo;
    Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_50usEcuModeSts;
    Mom_HndlrEcuModeSts_t AdcIf_Conv50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitAdcifSts_t AdcIf_Conv50usFuncInhibitAdcifSts;
    Mom_HndlrEcuModeSts_t Ioc_InputSR50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR50usFuncInhibitIocSts;
    Mom_HndlrEcuModeSts_t Mtr_Processing_SigEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Mtr_Processing_SigIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t Mtr_Processing_SigIgnEdgeSts;
    Mom_HndlrEcuModeSts_t Acmio_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_SenFuncInhibitAcmioSts;
    Mom_HndlrEcuModeSts_t Msp_CtrlEcuModeSts;
    Eem_SuspcDetnFuncInhibitMspSts_t Msp_CtrlFuncInhibitMspSts;
    Mom_HndlrEcuModeSts_t Acmctl_CtrlEcuModeSts;
    Msp_1msCtrlVdcLinkFild_t Acmctl_CtrlVdcLinkFild;
    Mcc_1msCtrlMotCtrlMode_t Acmctl_CtrlMotCtrlMode;
    Mcc_1msCtrlMotCtrlState_t Acmctl_CtrlMotCtrlState;
    Eem_SuspcDetnFuncInhibitAcmctlSts_t Acmctl_CtrlFuncInhibitAcmctlSts;
    Arbitrator_MtrMtrArbDriveState_t Acmctl_CtrlMtrArbDriveState;
    Mom_HndlrEcuModeSts_t Acmio_ActrEcuModeSts;
    Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_ActrFuncInhibitAcmioSts;
    Mom_HndlrEcuModeSts_t Ioc_OutputCS50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS50usFuncInhibitIocSts;

/* Output Data Element */
    AdcIf_Conv50usHwTrigMotInfo_t AdcIf_Conv50usHwTrigMotInfo;
    Ioc_InputSR50usMotCurrMonInfo_t Ioc_InputSR50usMotCurrMonInfo;
    Ioc_InputSR50usMotAngleMonInfo_t Ioc_InputSR50usMotAngleMonInfo;
    Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_SigMtrProcessOutInfo;
    Msp_CtrlMotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo;
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;
    Acmctl_CtrlMotDqIMeasdInfo_t Acmctl_CtrlMotDqIMeasdInfo;
    Acmio_SenVdcLink_t Acmio_SenVdcLink;
}Task_50us_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_50US_TYPES_H_ */
/** @} */
