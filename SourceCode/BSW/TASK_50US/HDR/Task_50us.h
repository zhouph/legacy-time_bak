/**
 * @defgroup Task_50us Task_50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Task_50us.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef TASK_50US_H_
#define TASK_50US_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Task_50us_Types.h"
#include "Task_50us_Cfg.h"
#include "Task_50us_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define TASK_50US_MODULE_ID      (0)
 #define TASK_50US_MAJOR_VERSION  (2)
 #define TASK_50US_MINOR_VERSION  (0)
 #define TASK_50US_PATCH_VERSION  (0)
 #define TASK_50US_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Task_50us_HdrBusType Task_50usBus;

/* Version Info */
extern const SwcVersionInfo_t Task_50usVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t Task_50us_Mtr_Processing_SigEemFailData;
extern AdcIf_Conv1msSwTrigMotInfo_t Task_50us_Mtr_Processing_SigSwTrigMotInfo;
extern Wss_SenWhlSpdInfo_t Task_50us_Mtr_Processing_SigWhlSpdInfo;
extern Eem_MainEemEceData_t Task_50us_Mtr_Processing_SigEemEceData;
extern Mtr_Processing_DiagMtrProcessDataInfo_t Task_50us_Mtr_Processing_SigMtrProcessDataInf;
extern Ioc_InputSR1msMotMonInfo_t Task_50us_Acmio_SenMotMonInfo;
extern Ioc_InputSR1msMotVoltsMonInfo_t Task_50us_Acmio_SenMotVoltsMonInfo;
extern Ses_CtrlMotOrgSetStInfo_t Task_50us_Msp_CtrlMotOrgSetStInfo;
extern Ses_CtrlPwrPistStBfMotOrgSetInfo_t Task_50us_Msp_CtrlPwrPistStBfMotOrgSetInfo;
extern Mcc_1msCtrlMotDqIRefMccInfo_t Task_50us_Acmctl_CtrlMotDqIRefMccInfo;
extern Ses_CtrlMotOrgSetStInfo_t Task_50us_Acmctl_CtrlMotOrgSetStInfo;
extern Ses_CtrlMotDqIRefSesInfo_t Task_50us_Acmctl_CtrlMotDqIRefSesInfo;
extern Mcc_1msCtrlFluxWeakengStInfo_t Task_50us_Acmctl_CtrlFluxWeakengStInfo;
extern Arbitrator_MtrMtrArbitratorData_t Task_50us_Acmctl_CtrlMtrArbtratorData;
extern Arbitrator_MtrMtrArbitratorInfo_t Task_50us_Acmctl_CtrlMtrArbtratorInfo;
extern Diag_HndlrMotReqDataDiagInfo_t Task_50us_Acmio_ActrMotReqDataDiagInfo;
extern Mom_HndlrEcuModeSts_t Task_50us_Mps_TLE5012_Hndlr_50usEcuModeSts;
extern Mom_HndlrEcuModeSts_t Task_50us_AdcIf_Conv50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAdcifSts_t Task_50us_AdcIf_Conv50usFuncInhibitAdcifSts;
extern Mom_HndlrEcuModeSts_t Task_50us_Ioc_InputSR50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_50us_Ioc_InputSR50usFuncInhibitIocSts;
extern Mom_HndlrEcuModeSts_t Task_50us_Mtr_Processing_SigEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Task_50us_Mtr_Processing_SigIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t Task_50us_Mtr_Processing_SigIgnEdgeSts;
extern Mom_HndlrEcuModeSts_t Task_50us_Acmio_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAcmioSts_t Task_50us_Acmio_SenFuncInhibitAcmioSts;
extern Mom_HndlrEcuModeSts_t Task_50us_Msp_CtrlEcuModeSts;
extern Eem_SuspcDetnFuncInhibitMspSts_t Task_50us_Msp_CtrlFuncInhibitMspSts;
extern Mom_HndlrEcuModeSts_t Task_50us_Acmctl_CtrlEcuModeSts;
extern Msp_1msCtrlVdcLinkFild_t Task_50us_Acmctl_CtrlVdcLinkFild;
extern Mcc_1msCtrlMotCtrlMode_t Task_50us_Acmctl_CtrlMotCtrlMode;
extern Mcc_1msCtrlMotCtrlState_t Task_50us_Acmctl_CtrlMotCtrlState;
extern Eem_SuspcDetnFuncInhibitAcmctlSts_t Task_50us_Acmctl_CtrlFuncInhibitAcmctlSts;
extern Arbitrator_MtrMtrArbDriveState_t Task_50us_Acmctl_CtrlMtrArbDriveState;
extern Mom_HndlrEcuModeSts_t Task_50us_Acmio_ActrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAcmioSts_t Task_50us_Acmio_ActrFuncInhibitAcmioSts;
extern Mom_HndlrEcuModeSts_t Task_50us_Ioc_OutputCS50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Task_50us_Ioc_OutputCS50usFuncInhibitIocSts;

/* Output Data Element */
extern AdcIf_Conv50usHwTrigMotInfo_t Task_50us_AdcIf_Conv50usHwTrigMotInfo;
extern Ioc_InputSR50usMotCurrMonInfo_t Task_50us_Ioc_InputSR50usMotCurrMonInfo;
extern Ioc_InputSR50usMotAngleMonInfo_t Task_50us_Ioc_InputSR50usMotAngleMonInfo;
extern Mtr_Processing_SigMtrProcessOutInfo_t Task_50us_Mtr_Processing_SigMtrProcessOutInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Task_50us_Msp_CtrlMotRotgAgSigInfo;
extern Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Task_50us_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;
extern Acmctl_CtrlMotDqIMeasdInfo_t Task_50us_Acmctl_CtrlMotDqIMeasdInfo;
extern Acmio_SenVdcLink_t Task_50us_Acmio_SenVdcLink;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Task_50us_Init(void);
extern void Task_50us(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TASK_50US_H_ */
/** @} */
