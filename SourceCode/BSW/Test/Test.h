/**
 * @defgroup Test
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Test.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
#ifndef TEST_H_
#define TEST_H_
/*==============================================================================
 *                  INCLUDE FILES
==============================================================================*/
 /*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
==============================================================================*/
//#define TEST_ENABLE

#ifdef TEST_ENABLE
    #define Test_MainFunction_1ms()     Test_1ms()
    #define Test_MainFunction_5ms()     Test_5ms()
    #define Test_MainFunction_50us()    Test_50us()
#else
    #define Test_MainFunction_1ms()
    #define Test_MainFunction_5ms()
    #define Test_MainFunction_50us()
#endif
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
==============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
==============================================================================*/

extern void Test_1ms();
extern void Test_5ms();
extern void Test_50us();
/*==============================================================================
 *                  END OF FILE
==============================================================================*/
#endif /* TEST_H_ */
/** @} */