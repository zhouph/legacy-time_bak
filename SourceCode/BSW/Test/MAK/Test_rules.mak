# \file
#
# \brief Test
#
# This file contains the implementation of the BSW
# module Test.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Test_src

Test_src_FILES       += $(Test_CORE_PATH)\Test.c

#################################################################
