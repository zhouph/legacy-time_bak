# \file
#
# \brief Test
#
# This file contains the implementation of the BSW
# module Test.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

Test_CORE_PATH     := $(MANDO_BSW_ROOT)\Test

CC_INCLUDE_PATH    += $(Test_CORE_PATH)

