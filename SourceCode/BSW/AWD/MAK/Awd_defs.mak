# \file
#
# \brief Awd
#
# This file contains the implementation of the SWC
# module Awd.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Awd_CORE_PATH     := $(MANDO_BSW_ROOT)\Awd
Awd_CAL_PATH      := $(Awd_CORE_PATH)\CAL\$(Awd_VARIANT)
Awd_SRC_PATH      := $(Awd_CORE_PATH)\SRC
Awd_CFG_PATH      := $(Awd_CORE_PATH)\CFG\$(Awd_VARIANT)
Awd_HDR_PATH      := $(Awd_CORE_PATH)\HDR
Awd_IFA_PATH      := $(Awd_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Awd_CMN_PATH      := $(Awd_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Awd_UT_PATH		:= $(Awd_CORE_PATH)\UT
	Awd_UNITY_PATH	:= $(Awd_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Awd_UT_PATH)
	CC_INCLUDE_PATH		+= $(Awd_UNITY_PATH)
	Awd_main_PATH 	:= Awd_UT_PATH\Awd_main
endif
CC_INCLUDE_PATH    += $(Awd_CAL_PATH)
CC_INCLUDE_PATH    += $(Awd_SRC_PATH)
CC_INCLUDE_PATH    += $(Awd_CFG_PATH)
CC_INCLUDE_PATH    += $(Awd_HDR_PATH)
CC_INCLUDE_PATH    += $(Awd_IFA_PATH)
CC_INCLUDE_PATH    += $(Awd_CMN_PATH)

