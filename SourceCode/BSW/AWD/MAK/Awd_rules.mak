# \file
#
# \brief Awd
#
# This file contains the implementation of the SWC
# module Awd.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Awd_src

Awd_src_FILES        += $(Awd_SRC_PATH)\Awd_main.c
Awd_src_FILES        += $(Awd_SRC_PATH)\Awd_Process.c
Awd_src_FILES        += $(Awd_IFA_PATH)\Awd_main_Ifa.c
Awd_src_FILES        += $(Awd_CFG_PATH)\Awd_Cfg.c
Awd_src_FILES        += $(Awd_CAL_PATH)\Awd_Cal.c

ifeq ($(ICE_COMPILE),true)
Awd_src_FILES        += $(Awd_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Awd_src_FILES        += $(Awd_UNITY_PATH)\unity.c
	Awd_src_FILES        += $(Awd_UNITY_PATH)\unity_fixture.c	
	Awd_src_FILES        += $(Awd_UT_PATH)\main.c
	Awd_src_FILES        += $(Awd_UT_PATH)\Awd_main\Awd_main_UtMain.c
endif