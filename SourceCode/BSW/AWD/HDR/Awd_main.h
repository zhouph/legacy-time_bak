/**
 * @defgroup Awd_main Awd_main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Awd_main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AWD_MAIN_H_
#define AWD_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Awd_Types.h"
#include "Awd_Cfg.h"
#include "Awd_Cal.h"
#include "Ach_Input.h"
#include "Ach_Output.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define AWD_MAIN_MODULE_ID      (0)
 #define AWD_MAIN_MAJOR_VERSION  (2)
 #define AWD_MAIN_MINOR_VERSION  (0)
 #define AWD_MAIN_PATCH_VERSION  (0)
 #define AWD_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Awd_main_HdrBusType Awd_mainBus;

/* Version Info */
extern const SwcVersionInfo_t Awd_mainVersionInfo;

/* Input Data Element */
extern Acm_MainAcmAsicInitCompleteFlag_t Awd_mainAcmAsicInitCompleteFlag;

/* Output Data Element */
extern Awd_mainAwdPrnDrv_t Awd_mainAwdPrnDrv;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Awd_main_Init(void);
extern void Awd_main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AWD_MAIN_H_ */
/** @} */
