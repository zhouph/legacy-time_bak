/**
 * @defgroup Awd_main Awd_main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Awd_main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Awd_main.h"
#include "Awd_main_Ifa.h"
#include "Awd_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AWD_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Awd_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AWD_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Awd_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AWD_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AWD_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AWD_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/** Variable Section (32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AWD_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

static boolean Awd_Switching = 0;
ACH_WdAnswerDataType Awd_WdAnswerData;
static uint16 Awd_ResetEventValue;
static uint16 Awd_ResetCount;
static uint16 Awd_ResetTimeoutReq;
static uint16 Awd_ResetTimoutAnsw;
static uint16 Awd_LateReq;
static uint16 Awd_EarlyReq;
static uint16 Awd_BadAnsw;
static uint16 Awd_LateAnsw;
static uint16 Awd_EarlyAnsw;
static uint16 Awd_CountValue;

static boolean Awd_WatchdogInitState;
ACH_WatchdogInitDataType Awd_InitData;

#define AWD_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AWD_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/** Variable Section (32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AWD_MAIN_START_SEC_CODE
#include "Awd_MemMap.h"

static void Awd_SetWdInitData(void);
static void Awd_CheckWdInitData(void);
static void Awd_WatchdogFunc(void);
static uint8 Awd_CompareData(uint16 Data1, uint16 Data2);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Awd_Process(void)
{
    uint8 temp = 0;
    uint8 TimeIndex_5msBase = 0;
    static uint8 TimeIndex = 0;

    if(TimeIndex >= Awd_TimeIndex_5ms)
    {
        TimeIndex_5msBase = TimeIndex - 5; /* To compatible with 10ms Loop base system */
    }
    else
    {
        TimeIndex_5msBase = TimeIndex;
    }
    
    if(Awd_mainBus.Awd_mainAcmAsicInitCompleteFlag == 1)
    {
        if(Awd_WatchdogInitState == 0)
        {
            switch(TimeIndex_5msBase)
            {
                case Awd_TimeIndex_1ms:
                {
                    Awd_SetWdInitData();
                    Ach_SetWriteSpiData_CS(ACH_WatchdogInitMsgSet, &Awd_InitData);
                    break;
                }
                case Awd_TimeIndex_4ms:
                {
                    Awd_CheckWdInitData();
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
        else
        {
            switch(TimeIndex_5msBase)
            {
                case Awd_TimeIndex_0ms:
                {
                    break;
                }
                case Awd_TimeIndex_1ms:
                {
                    
                    break;
                }
                case Awd_TimeIndex_2ms:
                {
                    
                    break;
                }
                case Awd_TimeIndex_3ms:
                {
                    if(Awd_Switching == 1)
                    {
                        Awd_WatchdogFunc();
                        Ach_SetWriteSpiData_CS(ACH_WatchdogAnswerSet, &Awd_WdAnswerData);
                    }
                    else
                    {
                        Ach_SetWriteSpiData_CS(ACH_WatchdogSeedRequest, &temp);
                    }
                    break;
                }
                case Awd_TimeIndex_4ms:
                {
                    if(Awd_Switching == 0)
                    {
                        Ach_SetReadSpiData_CS(ACH_MSG72_ID_WDG2SEED);
                        Ach_SetReadSpiData_CS(ACH_MSG73_ID_WDG2STATUS);
                    }
                    Awd_Switching^=1;
                    break;
                }
                default:
                {
                    break;
                }
            }

        }
    }
    
    TimeIndex++;
    if(TimeIndex >= LOOP_TIME)
    {
        TimeIndex = 0;
    }
}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/


static void Awd_SetWdInitData(void)
{
    Awd_InitData.ACH_Tx2_WD_SW_DIS                 = 1;      /* DISABLE DRIVER INHIBIT */ /* TODO */
    Awd_InitData.ACH_Tx2_WD_DIS                    = 1;      /* ASIC HW WATCHDOG 0:ENABLE, 1:DISABLE*/  /* TODO */      
    Awd_InitData.ACH_Tx74_VALID_REQUEST_START      = 10;     /* 2.56ms */
    Awd_InitData.ACH_Tx74_VALID_ANSWER_START       = 10;     /* 2.56ms */
    Awd_InitData.ACH_Tx75_VALID_ANSWER_DELTA       = 20;     /* 5.12ms */
    Awd_InitData.ACH_Tx75_VALID_REQUEST_DELTA      = 20;     /* 5.12ms */
    Awd_InitData.ACH_Tx75_REQ_CHECK_ENABLE         = 1;
    Awd_InitData.ACH_Tx76_ANSWER_TIME_OUT_DELTA    = 10;     /* 2.56ms */
    Awd_InitData.ACH_Tx76_REQUEST_TIME_OUT_DELTA   = 10;     /* 2.56ms */
    Awd_InitData.ACH_Tx76_TO_RESET_ENABLE          = 0;
    Awd_InitData.ACH_Tx77_NUM_OF_GOOD_STEPS        = 1;      /* 2.56ms */
    Awd_InitData.ACH_Tx77_NUM_OF_BAD_STEPS         = 3;      /* 2.56ms */
    Awd_InitData.ACH_Tx77_HIGH_TH                  = 15;
    Awd_InitData.ACH_Tx77_LOW_TH                   = 7;
    Awd_InitData.ACH_Tx77_CLOCK_DIVISION           = 1;      /* 256us */
    Awd_InitData.ACH_Tx77_RESET_ENABLE             = 0;
}

static void Awd_CheckWdInitData(void)
{
    uint8  err      = 0;
    uint16 DataTemp = 0;

    ACH_GetSigData_CS(ASIC_RX_MSG_2mu2_Rx_2_WD_SW_DIS, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx2_WD_SW_DIS);
    ACH_GetSigData_CS(ASIC_RX_MSG_2mu1_Rx_2_WD_DIS, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx2_WD_DIS);
    
    ACH_GetSigData_CS(ASIC_RX_MSG_74mu8_Rx_74_VALID_ANSWER_START, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx74_VALID_ANSWER_START);
    ACH_GetSigData_CS(ASIC_RX_MSG_74mu8_Rx_74_VALID_REQUEST_START, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx74_VALID_REQUEST_START);
    
    ACH_GetSigData_CS(ASIC_RX_MSG_75mu6_Rx_75_VALID_ANSWER_DELTA, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx75_VALID_ANSWER_DELTA);
    ACH_GetSigData_CS(ASIC_RX_MSG_75mu6_Rx_75_VALID_REQUEST_DELTA, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx75_VALID_REQUEST_DELTA);
    ACH_GetSigData_CS(ASIC_RX_MSG_75mu1_Rx_75_REQ_CHECK_ENABLE, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx75_REQ_CHECK_ENABLE);

    /*   NOTICE : ASIC ERRATA, MSG 76 ADDRESS RETURN is ORed with MSG 12
    Hal_GetSigData(ASIC_RX_MSG_76mu6_Rx_76_ANSWER_TIME_OUT_DELTA, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx76_ANSWER_TIME_OUT_DELTA);
    Hal_GetSigData(ASIC_RX_MSG_76mu6_Rx_76_REQUEST_TIME_OUT_DELTA, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx76_REQUEST_TIME_OUT_DELTA);
    Hal_GetSigData(ASIC_RX_MSG_76mu1_Rx_76_TO_RESET_ENABLE, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx76_TO_RESET_ENABLE);
    */
    
    ACH_GetSigData_CS(ASIC_RX_MSG_77mu3_Rx_77_NUM_OF_GOOD_STEPS, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx77_NUM_OF_GOOD_STEPS);
    ACH_GetSigData_CS(ASIC_RX_MSG_77mu3_Rx_77_NUM_OF_BAD_STEPS, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx77_NUM_OF_BAD_STEPS);
    ACH_GetSigData_CS(ASIC_RX_MSG_77mu4_Rx_77_HIGH_TH, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx77_HIGH_TH);
    ACH_GetSigData_CS(ASIC_RX_MSG_77mu4_Rx_77_LOW_TH, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx77_LOW_TH);
    ACH_GetSigData_CS(ASIC_RX_MSG_77mu1_Rx_77_CLOCK_DIVISION, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx77_CLOCK_DIVISION);
    ACH_GetSigData_CS(ASIC_RX_MSG_77mu1_Rx_77_RESET_ENABLE, &DataTemp);
    err |= Awd_CompareData(DataTemp, Awd_InitData.ACH_Tx77_RESET_ENABLE);

    if(err == 0)
    {
        Awd_WatchdogInitState = 1;
    }
}

static void Awd_WatchdogFunc(void)
{
    uint16 SEED = 0;
    
    ACH_GetSigData_CS(ASIC_RX_MSG_72mu8_Rx_72_SEED, &SEED);
    Awd_WdAnswerData.ACH_Tx78_ANSWER_LOW  = ((uint8)SEED^0xFF) + 1; /* 2's complement */
    Awd_WdAnswerData.ACH_Tx78_ANSWER_HIGH = (uint8)SEED;

    /*
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu4_Rx_73_WD_CNT_VALUE,&Awd_CountValue);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_EARLY_ANSW,&Awd_EarlyAnsw);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_LATE_ANSW,&Awd_LateAnsw);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_BAD_ANSW,&Awd_BadAnsw);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_EARLY_REQ,&Awd_EarlyReq);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_LATE_REQ,&Awd_LateReq);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_RST_TO_ANSW,&Awd_ResetTimoutAnsw);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_RST_TO_REQ,&Awd_ResetTimeoutReq);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu1_Rx_73_WD_RST_CNT,&Awd_ResetCount);
    ACH_GetSigData_CS(ASIC_RX_MSG_73mu4_Rx_73_WD_RST_EVENT_VALUE,&Awd_ResetEventValue);
    */
}

static uint8 Awd_CompareData(uint16 Data1, uint16 Data2)
{
    uint8 rtn = 0;

    if(Data1 == Data2)
    {
        rtn = 0;
    }
    else
    {
        rtn = 1;
    }
    
    return rtn;
}

#define AWD_MAIN_STOP_SEC_CODE
#include "Awd_MemMap.h"

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
