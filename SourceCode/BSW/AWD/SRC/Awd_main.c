/**
 * @defgroup Awd_main Awd_main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Awd_main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Awd_main.h"
#include "Awd_main_Ifa.h"
#include "Awd_Process.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AWD_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Awd_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AWD_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Awd_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AWD_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Awd_main_HdrBusType Awd_mainBus;

/* Version Info */
const SwcVersionInfo_t Awd_mainVersionInfo = 
{   
    AWD_MAIN_MODULE_ID,           /* Awd_mainVersionInfo.ModuleId */
    AWD_MAIN_MAJOR_VERSION,       /* Awd_mainVersionInfo.MajorVer */
    AWD_MAIN_MINOR_VERSION,       /* Awd_mainVersionInfo.MinorVer */
    AWD_MAIN_PATCH_VERSION,       /* Awd_mainVersionInfo.PatchVer */
    AWD_MAIN_BRANCH_VERSION       /* Awd_mainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Acm_MainAcmAsicInitCompleteFlag_t Awd_mainAcmAsicInitCompleteFlag;

/* Output Data Element */
Awd_mainAwdPrnDrv_t Awd_mainAwdPrnDrv;

uint32 Awd_main_Timer_Start;
uint32 Awd_main_Timer_Elapsed;

#define AWD_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AWD_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/** Variable Section (32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AWD_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AWD_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AWD_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_MAIN_START_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/** Variable Section (32BIT)**/


#define AWD_MAIN_STOP_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AWD_MAIN_START_SEC_CODE
#include "Awd_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Awd_main_Init(void)
{
    /* Initialize internal bus */
    Awd_mainBus.Awd_mainAcmAsicInitCompleteFlag = 0;
    Awd_mainBus.Awd_mainAwdPrnDrv = 0;
}

void Awd_main(void)
{
    Awd_main_Timer_Start = STM0_TIM0.U;

    /* Input */
    Awd_main_Read_Awd_mainAcmAsicInitCompleteFlag(&Awd_mainBus.Awd_mainAcmAsicInitCompleteFlag);

    /* Process */
    Awd_Process();

    /* Output */
    Awd_main_Write_Awd_mainAwdPrnDrv(&Awd_mainBus.Awd_mainAwdPrnDrv);

    Awd_main_Timer_Elapsed = STM0_TIM0.U - Awd_main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AWD_MAIN_STOP_SEC_CODE
#include "Awd_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
