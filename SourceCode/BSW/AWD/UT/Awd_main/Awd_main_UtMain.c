#include "unity.h"
#include "unity_fixture.h"
#include "Awd_main.h"
#include "Awd_main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Awd_mainAcmAsicInitCompleteFlag[MAX_STEP] = AWD_MAINACMASICINITCOMPLETEFLAG;

const Awd_mainAwdPrnDrv_t UtExpected_Awd_mainAwdPrnDrv[MAX_STEP] = AWD_MAINAWDPRNDRV;



TEST_GROUP(Awd_main);
TEST_SETUP(Awd_main)
{
    Awd_main_Init();
}

TEST_TEAR_DOWN(Awd_main)
{   /* Postcondition */

}

TEST(Awd_main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Awd_mainAcmAsicInitCompleteFlag = UtInput_Awd_mainAcmAsicInitCompleteFlag[i];

        Awd_main();

        TEST_ASSERT_EQUAL(Awd_mainAwdPrnDrv, UtExpected_Awd_mainAwdPrnDrv[i]);
    }
}

TEST_GROUP_RUNNER(Awd_main)
{
    RUN_TEST_CASE(Awd_main, All);
}
