/**
 * @defgroup Awd_Cfg Awd_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Awd_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Awd_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AWD_START_SEC_CONST_UNSPECIFIED
#include "Awd_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AWD_STOP_SEC_CONST_UNSPECIFIED
#include "Awd_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AWD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AWD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_START_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AWD_STOP_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
#define AWD_START_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AWD_STOP_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_START_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/** Variable Section (32BIT)**/


#define AWD_STOP_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AWD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AWD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_START_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AWD_STOP_SEC_VAR_NOINIT_32BIT
#include "Awd_MemMap.h"
#define AWD_START_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AWD_STOP_SEC_VAR_UNSPECIFIED
#include "Awd_MemMap.h"
#define AWD_START_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/** Variable Section (32BIT)**/


#define AWD_STOP_SEC_VAR_32BIT
#include "Awd_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AWD_START_SEC_CODE
#include "Awd_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AWD_STOP_SEC_CODE
#include "Awd_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
