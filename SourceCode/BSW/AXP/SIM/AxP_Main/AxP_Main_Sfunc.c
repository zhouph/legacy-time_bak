#define S_FUNCTION_NAME      AxP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          35
#define WidthOutputPort         4

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "AxP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    AxP_MainEemFailData.Eem_Fail_SenPwr_12V = input[0];
    AxP_MainEemFailData.Eem_Fail_SenPwr_5V = input[1];
    AxP_MainEemFailData.Eem_Fail_Ax = input[2];
    AxP_MainEemFailData.Eem_Fail_WssFL = input[3];
    AxP_MainEemFailData.Eem_Fail_WssFR = input[4];
    AxP_MainEemFailData.Eem_Fail_WssRL = input[5];
    AxP_MainEemFailData.Eem_Fail_WssRR = input[6];
    AxP_MainCanRxEscInfo.Ax = input[7];
    AxP_MainEscSwtStInfo.EscDisabledBySwt = input[8];
    AxP_MainWhlSpdInfo.FlWhlSpd = input[9];
    AxP_MainWhlSpdInfo.FrWhlSpd = input[10];
    AxP_MainWhlSpdInfo.RlWhlSpd = input[11];
    AxP_MainWhlSpdInfo.RrlWhlSpd = input[12];
    AxP_MainAbsCtrlInfo.AbsActFlg = input[13];
    AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = input[14];
    AxP_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[15];
    AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[16];
    AxP_MainEemSuspectData.Eem_Suspect_WssFL = input[17];
    AxP_MainEemSuspectData.Eem_Suspect_WssFR = input[18];
    AxP_MainEemSuspectData.Eem_Suspect_WssRL = input[19];
    AxP_MainEemSuspectData.Eem_Suspect_WssRR = input[20];
    AxP_MainEemSuspectData.Eem_Suspect_FrontWss = input[21];
    AxP_MainEemSuspectData.Eem_Suspect_RearWss = input[22];
    AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = input[23];
    AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V = input[24];
    AxP_MainEemEceData.Eem_Ece_Ax = input[25];
    AxP_MainCanMonData.CanM_SubBusOff_Err = input[26];
    AxP_MainWssSpeedOut.WssMax = input[27];
    AxP_MainWssSpeedOut.WssMin = input[28];
    AxP_MainEcuModeSts = input[29];
    AxP_MainIgnOnOffSts = input[30];
    AxP_MainIgnEdgeSts = input[31];
    AxP_MainVBatt1Mon = input[32];
    AxP_MainDiagClrSrs = input[33];
    AxP_MainVehSpd = input[34];

    AxP_Main();


    output[0] = AxP_MainAxPlauOutput.AxPlauOffsetErr;
    output[1] = AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr;
    output[2] = AxP_MainAxPlauOutput.AxPlauStickErr;
    output[3] = AxP_MainAxPlauOutput.AxPlauNoiseErr;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    AxP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
