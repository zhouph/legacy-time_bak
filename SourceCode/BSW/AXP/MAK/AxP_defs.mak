# \file
#
# \brief AxP
#
# This file contains the implementation of the SWC
# module AxP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

AxP_CORE_PATH     := $(MANDO_BSW_ROOT)\AxP
AxP_CAL_PATH      := $(AxP_CORE_PATH)\CAL\$(AxP_VARIANT)
AxP_SRC_PATH      := $(AxP_CORE_PATH)\SRC
AxP_CFG_PATH      := $(AxP_CORE_PATH)\CFG\$(AxP_VARIANT)
AxP_HDR_PATH      := $(AxP_CORE_PATH)\HDR
AxP_IFA_PATH      := $(AxP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    AxP_CMN_PATH      := $(AxP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	AxP_UT_PATH		:= $(AxP_CORE_PATH)\UT
	AxP_UNITY_PATH	:= $(AxP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(AxP_UT_PATH)
	CC_INCLUDE_PATH		+= $(AxP_UNITY_PATH)
	AxP_Main_PATH 	:= AxP_UT_PATH\AxP_Main
endif
CC_INCLUDE_PATH    += $(AxP_CAL_PATH)
CC_INCLUDE_PATH    += $(AxP_SRC_PATH)
CC_INCLUDE_PATH    += $(AxP_CFG_PATH)
CC_INCLUDE_PATH    += $(AxP_HDR_PATH)
CC_INCLUDE_PATH    += $(AxP_IFA_PATH)
CC_INCLUDE_PATH    += $(AxP_CMN_PATH)

