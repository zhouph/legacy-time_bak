# \file
#
# \brief AxP
#
# This file contains the implementation of the SWC
# module AxP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += AxP_src

AxP_src_FILES        += $(AxP_SRC_PATH)\AxP_Main.c
AxP_src_FILES        += $(AxP_SRC_PATH)\AxP_Process.c
AxP_src_FILES        += $(AxP_IFA_PATH)\AxP_Main_Ifa.c
AxP_src_FILES        += $(AxP_CFG_PATH)\AxP_Cfg.c
AxP_src_FILES        += $(AxP_CAL_PATH)\AxP_Cal.c

ifeq ($(ICE_COMPILE),true)
AxP_src_FILES        += $(AxP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	AxP_src_FILES        += $(AxP_UNITY_PATH)\unity.c
	AxP_src_FILES        += $(AxP_UNITY_PATH)\unity_fixture.c	
	AxP_src_FILES        += $(AxP_UT_PATH)\main.c
	AxP_src_FILES        += $(AxP_UT_PATH)\AxP_Main\AxP_Main_UtMain.c
endif