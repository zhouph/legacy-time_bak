/**
 * @defgroup AxP_Main AxP_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Process.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXP_PROCESS_H_
#define AXP_PROCESS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxP_Types.h"
#include "AxP_Cfg.h"
#include "Common.h"
/************
    Standstill offset 
    ******************/
#define DEF_STANDSTILL     1
#define DEF_NON_STANDSTILL 0
/************
    Vehicle Stable  
    ******************/
#define DEF_VEHICLE_SPEED_STABLE 1
#define DEF_VEHICLE_SPEED_UNSTABLE 0

#define DEF_CHECK_INIT          0
#define DEF_CHECK_ENABLE     1

#define  DEF_2SEC_TIME      200

#define DEF_0_05G  50
#define DEF_0_5G    500
#define DEF_0_7G    700
/******************************
       Inhibit Define
       **************************************/
#define	AX_INHIBIT		(Saluint8)1
#define	AX_ALLOW		(Saluint8)0


extern void AxP_Process(AxP_Main_HdrBusType *pAxP_Process);

#endif