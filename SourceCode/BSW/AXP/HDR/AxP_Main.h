/**
 * @defgroup AxP_Main AxP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXP_MAIN_H_
#define AXP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxP_Types.h"
#include "AxP_Cfg.h"
#include "AxP_Cal.h"
#include "AxP_Process.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define AXP_MAIN_MODULE_ID      (0)
 #define AXP_MAIN_MAJOR_VERSION  (2)
 #define AXP_MAIN_MINOR_VERSION  (0)
 #define AXP_MAIN_PATCH_VERSION  (0)
 #define AXP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern AxP_Main_HdrBusType AxP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t AxP_MainVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t AxP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t AxP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t AxP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t AxP_MainWhlSpdInfo;
extern Abc_CtrlAbsCtrlInfo_t AxP_MainAbsCtrlInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t AxP_MainBaseBrkCtrlModInfo;
extern SenPwrM_MainSenPwrMonitor_t AxP_MainSenPwrMonitorData;
extern Eem_MainEemSuspectData_t AxP_MainEemSuspectData;
extern Eem_MainEemEceData_t AxP_MainEemEceData;
extern CanM_MainCanMonData_t AxP_MainCanMonData;
extern Wss_SenWssSpeedOut_t AxP_MainWssSpeedOut;
extern Mom_HndlrEcuModeSts_t AxP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t AxP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t AxP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t AxP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t AxP_MainDiagClrSrs;
extern Abc_CtrlVehSpd_t AxP_MainVehSpd;

/* Output Data Element */
extern AxP_MainAxPlauOutput_t AxP_MainAxPlauOutput;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void AxP_Main_Init(void);
extern void AxP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AXP_MAIN_H_ */
/** @} */
