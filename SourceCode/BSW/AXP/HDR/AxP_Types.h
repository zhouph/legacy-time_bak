/**
 * @defgroup AxP_Types AxP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXP_TYPES_H_
#define AXP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t AxP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t AxP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t AxP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t AxP_MainWhlSpdInfo;
    Abc_CtrlAbsCtrlInfo_t AxP_MainAbsCtrlInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t AxP_MainBaseBrkCtrlModInfo;
    SenPwrM_MainSenPwrMonitor_t AxP_MainSenPwrMonitorData;
    Eem_MainEemSuspectData_t AxP_MainEemSuspectData;
    Eem_MainEemEceData_t AxP_MainEemEceData;
    CanM_MainCanMonData_t AxP_MainCanMonData;
    Wss_SenWssSpeedOut_t AxP_MainWssSpeedOut;
    Mom_HndlrEcuModeSts_t AxP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t AxP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t AxP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t AxP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t AxP_MainDiagClrSrs;
    Abc_CtrlVehSpd_t AxP_MainVehSpd;

/* Output Data Element */
    AxP_MainAxPlauOutput_t AxP_MainAxPlauOutput;
}AxP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AXP_TYPES_H_ */
/** @} */
