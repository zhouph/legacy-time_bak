/**
 * @defgroup AxP_Main AxP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxP_Main.h"
#include "AxP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AxP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AXP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AxP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
AxP_Main_HdrBusType AxP_MainBus;

/* Version Info */
const SwcVersionInfo_t AxP_MainVersionInfo = 
{   
    AXP_MAIN_MODULE_ID,           /* AxP_MainVersionInfo.ModuleId */
    AXP_MAIN_MAJOR_VERSION,       /* AxP_MainVersionInfo.MajorVer */
    AXP_MAIN_MINOR_VERSION,       /* AxP_MainVersionInfo.MinorVer */
    AXP_MAIN_PATCH_VERSION,       /* AxP_MainVersionInfo.PatchVer */
    AXP_MAIN_BRANCH_VERSION       /* AxP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t AxP_MainEemFailData;
Proxy_RxCanRxEscInfo_t AxP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t AxP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t AxP_MainWhlSpdInfo;
Abc_CtrlAbsCtrlInfo_t AxP_MainAbsCtrlInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t AxP_MainBaseBrkCtrlModInfo;
SenPwrM_MainSenPwrMonitor_t AxP_MainSenPwrMonitorData;
Eem_MainEemSuspectData_t AxP_MainEemSuspectData;
Eem_MainEemEceData_t AxP_MainEemEceData;
CanM_MainCanMonData_t AxP_MainCanMonData;
Wss_SenWssSpeedOut_t AxP_MainWssSpeedOut;
Mom_HndlrEcuModeSts_t AxP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t AxP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t AxP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t AxP_MainVBatt1Mon;
Diag_HndlrDiagClr_t AxP_MainDiagClrSrs;
Abc_CtrlVehSpd_t AxP_MainVehSpd;

/* Output Data Element */
AxP_MainAxPlauOutput_t AxP_MainAxPlauOutput;

uint32 AxP_Main_Timer_Start;
uint32 AxP_Main_Timer_Elapsed;

#define AXP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/** Variable Section (32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/** Variable Section (32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AXP_MAIN_START_SEC_CODE
#include "AxP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void AxP_Main_Init(void)
{
    /* Initialize internal bus */
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_SenPwr_12V = 0;
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_SenPwr_5V = 0;
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_Ax = 0;
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssFL = 0;
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssFR = 0;
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssRL = 0;
    AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssRR = 0;
    AxP_MainBus.AxP_MainCanRxEscInfo.Ax = 0;
    AxP_MainBus.AxP_MainEscSwtStInfo.EscDisabledBySwt = 0;
    AxP_MainBus.AxP_MainWhlSpdInfo.FlWhlSpd = 0;
    AxP_MainBus.AxP_MainWhlSpdInfo.FrWhlSpd = 0;
    AxP_MainBus.AxP_MainWhlSpdInfo.RlWhlSpd = 0;
    AxP_MainBus.AxP_MainWhlSpdInfo.RrlWhlSpd = 0;
    AxP_MainBus.AxP_MainAbsCtrlInfo.AbsActFlg = 0;
    AxP_MainBus.AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = 0;
    AxP_MainBus.AxP_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    AxP_MainBus.AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssFL = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssFR = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssRL = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssRR = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_FrontWss = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_RearWss = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = 0;
    AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V = 0;
    AxP_MainBus.AxP_MainEemEceData.Eem_Ece_Ax = 0;
    AxP_MainBus.AxP_MainCanMonData.CanM_SubBusOff_Err = 0;
    AxP_MainBus.AxP_MainWssSpeedOut.WssMax = 0;
    AxP_MainBus.AxP_MainWssSpeedOut.WssMin = 0;
    AxP_MainBus.AxP_MainEcuModeSts = 0;
    AxP_MainBus.AxP_MainIgnOnOffSts = 0;
    AxP_MainBus.AxP_MainIgnEdgeSts = 0;
    AxP_MainBus.AxP_MainVBatt1Mon = 0;
    AxP_MainBus.AxP_MainDiagClrSrs = 0;
    AxP_MainBus.AxP_MainVehSpd = 0;
    AxP_MainBus.AxP_MainAxPlauOutput.AxPlauOffsetErr = 0;
    AxP_MainBus.AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = 0;
    AxP_MainBus.AxP_MainAxPlauOutput.AxPlauStickErr = 0;
    AxP_MainBus.AxP_MainAxPlauOutput.AxPlauNoiseErr = 0;
}

void AxP_Main(void)
{
    AxP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_12V(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_SenPwr_12V);
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_5V(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_SenPwr_5V);
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_Ax(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_Ax);
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssFL(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssFL);
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssFR(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssFR);
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssRL(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssRL);
    AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssRR(&AxP_MainBus.AxP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainCanRxEscInfo_Ax(&AxP_MainBus.AxP_MainCanRxEscInfo.Ax);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainEscSwtStInfo_EscDisabledBySwt(&AxP_MainBus.AxP_MainEscSwtStInfo.EscDisabledBySwt);

    AxP_Main_Read_AxP_MainWhlSpdInfo(&AxP_MainBus.AxP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure AxP_MainWhlSpdInfo 
     : AxP_MainWhlSpdInfo.FlWhlSpd;
     : AxP_MainWhlSpdInfo.FrWhlSpd;
     : AxP_MainWhlSpdInfo.RlWhlSpd;
     : AxP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainAbsCtrlInfo_AbsActFlg(&AxP_MainBus.AxP_MainAbsCtrlInfo.AbsActFlg);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(&AxP_MainBus.AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainSenPwrMonitorData_SenPwrM_5V_Stable(&AxP_MainBus.AxP_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    AxP_Main_Read_AxP_MainSenPwrMonitorData_SenPwrM_12V_Stable(&AxP_MainBus.AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFL(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssFL);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFR(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssFR);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRL(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssRL);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRR(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_WssRR);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_FrontWss(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_FrontWss);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_RearWss(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_RearWss);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V);
    AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_5V(&AxP_MainBus.AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainEemEceData_Eem_Ece_Ax(&AxP_MainBus.AxP_MainEemEceData.Eem_Ece_Ax);

    /* Decomposed structure interface */
    AxP_Main_Read_AxP_MainCanMonData_CanM_SubBusOff_Err(&AxP_MainBus.AxP_MainCanMonData.CanM_SubBusOff_Err);

    AxP_Main_Read_AxP_MainWssSpeedOut(&AxP_MainBus.AxP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure AxP_MainWssSpeedOut 
     : AxP_MainWssSpeedOut.WssMax;
     : AxP_MainWssSpeedOut.WssMin;
     =============================================================================*/
    
    AxP_Main_Read_AxP_MainEcuModeSts(&AxP_MainBus.AxP_MainEcuModeSts);
    AxP_Main_Read_AxP_MainIgnOnOffSts(&AxP_MainBus.AxP_MainIgnOnOffSts);
    AxP_Main_Read_AxP_MainIgnEdgeSts(&AxP_MainBus.AxP_MainIgnEdgeSts);
    AxP_Main_Read_AxP_MainVBatt1Mon(&AxP_MainBus.AxP_MainVBatt1Mon);
    AxP_Main_Read_AxP_MainDiagClrSrs(&AxP_MainBus.AxP_MainDiagClrSrs);
    AxP_Main_Read_AxP_MainVehSpd(&AxP_MainBus.AxP_MainVehSpd);

    /* Process */
    AxP_Process(&AxP_MainBus);    
    /* Output */
    AxP_Main_Write_AxP_MainAxPlauOutput(&AxP_MainBus.AxP_MainAxPlauOutput);
    /*==============================================================================
    * Members of structure AxP_MainAxPlauOutput 
     : AxP_MainAxPlauOutput.AxPlauOffsetErr;
     : AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr;
     : AxP_MainAxPlauOutput.AxPlauStickErr;
     : AxP_MainAxPlauOutput.AxPlauNoiseErr;
     =============================================================================*/
    

    AxP_Main_Timer_Elapsed = STM0_TIM0.U - AxP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AXP_MAIN_STOP_SEC_CODE
#include "AxP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
