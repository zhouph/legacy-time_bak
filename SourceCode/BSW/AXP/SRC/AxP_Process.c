/**
 * @defgroup AxP_Main AxP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxP_Main.h"
#include "AxP_Main_Ifa.h"
#include "AxP_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AxP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AXP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AxP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"

/************
   To Do
   ************/
extern signed int Proxy_a_long_1_1000g;
extern Swt_SenSwtInfoEsc_t Swt_SenSwtInfoEsc;
/******************************************/

static Saluint8 Ax_Inhibit_Define(AxP_Main_HdrBusType *pAxInhibitDefine);
static Saluint8 AxP_StandStillState(AxP_Main_HdrBusType *pStandStillState);
static Saluint8 AxP_Vehicle_Speed_State(AxP_Main_HdrBusType *pAxPVehicleSpeedState);
static Salsint32 AxP_Vehicle_Over_Speed_Calc(Salsint32 speed);
static Salsint32 AxP_Vehicle_Under_Speed_Calc(Salsint32 speed);
static void AxP_NoiseChk(AxP_Main_HdrBusType *pAxP_NoiseChk, boolean inhibit);
static void AxP_DrivingOffset(AxP_Main_HdrBusType *pAxPDrivingOffset, boolean inhibit);
static void AxP_StickChk(AxP_Main_HdrBusType *pAxP_StickChk, boolean inhibit);
static void AxP_StandStillOffset(AxP_Main_HdrBusType *pAxP_StandStill, boolean inhibit);

void AxP_Process(AxP_Main_HdrBusType *pAxP_Process)
{
    boolean Inhibit =Ax_Inhibit_Define(pAxP_Process);
    
    AxP_StandStillOffset(pAxP_Process,Inhibit);
    AxP_DrivingOffset(pAxP_Process,Inhibit);
    AxP_StickChk(pAxP_Process,Inhibit);
    AxP_NoiseChk(pAxP_Process,Inhibit);
}

static Saluint8 Ax_Inhibit_Define(AxP_Main_HdrBusType *pAxInhibitDefine)
{
    boolean inhibit =AX_ALLOW;
    if((pAxInhibitDefine->AxP_MainSenPwrMonitorData.SenPwrM_12V_Open_Err==1)
       ||(pAxInhibitDefine->AxP_MainSenPwrMonitorData.SenPwrM_12V_Short_Err==1)
       ||(pAxInhibitDefine->AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable==1)
       ||(pAxInhibitDefine->AxP_MainCanMonData.CanM_SubBusOff_Err==1)
       ||(pAxInhibitDefine->AxP_MainEemEceData.Eem_Ece_Ax==1)
       ||(pAxInhibitDefine->AxP_MainEemFailData.Eem_Fail_Ax==1)
     )
    {
        inhibit =AX_INHIBIT;
    }

    return inhibit;
}

/**************************
     Stand-still Ax Offset Error detect 
     *******************************/
static void AxP_StandStillOffset(AxP_Main_HdrBusType *pAxP_StandStill, boolean inhibit)
{
    Saluint8 StandState =DEF_NON_STANDSTILL;
    Saluint16 ABSLongG_1_1000g =0;
    Saluint8 inhibit_offset =0; 
    
    StandState = AxP_StandStillState(pAxP_StandStill);
    ABSLongG_1_1000g= (Saluint16)(abs(Proxy_a_long_1_1000g));

    if(StandState ==DEF_STANDSTILL)
    {
        if(ABSLongG_1_1000g>DEF_0_7G) 
        {
            pAxP_StandStill->AxP_MainAxPlauOutput.AxPlauOffsetErr =ERR_PREFAILED;
        }
        else
        {
            pAxP_StandStill->AxP_MainAxPlauOutput.AxPlauOffsetErr =ERR_PREPASSED;
        }
    }

    if(inhibit==TRUE)
    {
         pAxP_StandStill->AxP_MainAxPlauOutput.AxPlauOffsetErr==ERR_INHIBIT;
    }
}

static Saluint8 AxP_StandStillState(AxP_Main_HdrBusType *pStandStillState)
{
    Salsint32 Vehicle_Speed = U8_F64SPEED_0KPH25;
    Saluint32 Vehicle_Max_Wheel_Speed =U8_F64SPEED_0KPH25;
    static Saluint16 Standstill_Time =0;
    static Saluint8 StandStill_State =DEF_NON_STANDSTILL;   

    Vehicle_Speed =pStandStillState->AxP_MainVehSpd;
    Vehicle_Max_Wheel_Speed   =pStandStillState->AxP_MainWssSpeedOut.WssMax;
       

    if((Vehicle_Speed <=U8_F64SPEED_2KPH25)&&(Vehicle_Max_Wheel_Speed<U8_F64SPEED_0KPH25))
    {
        if(Standstill_Time<DEF_2SEC_TIME)
        {
            Standstill_Time ++;
        }
        else
        {
            StandStill_State =DEF_STANDSTILL;
        }
    }
    else
    {
        StandStill_State =DEF_NON_STANDSTILL;
        Standstill_Time =0;
    }

    return StandStill_State;
}

/**************************
     Driving  Ax Offset Error detect 
     *******************************/
static void AxP_DrivingOffset(AxP_Main_HdrBusType *pAxPDrivingOffset, boolean inhibit)
{
    Salsint32 Vehicle_Speed = U8_F64SPEED_0KPH25;
    Saluint32 Vehicle_Min_Wheel_Speed =U8_F64SPEED_0KPH25;
    Saluint32 Vehicle_ABS_CONTROL =0;
    Saluint16 ABSLongG_1_1000g =0;
    Saluint8 Vehicle_BLS_ON =0;
    
    Vehicle_Speed =pAxPDrivingOffset->AxP_MainVehSpd;
    Vehicle_Min_Wheel_Speed=pAxPDrivingOffset->AxP_MainWssSpeedOut.WssMin;
    Vehicle_ABS_CONTROL =pAxPDrivingOffset->AxP_MainAbsCtrlInfo.AbsActFlg;
    Vehicle_BLS_ON =Swt_SenSwtInfoEsc.BlsSwt_Esc;
    ABSLongG_1_1000g=(Saluint16)(abs(Proxy_a_long_1_1000g));
    
    if((Vehicle_ABS_CONTROL==0)
        &&(Vehicle_Min_Wheel_Speed>U16_F64SPEED_10KPH)
        &&(Vehicle_BLS_ON==0)
        &&(ABSLongG_1_1000g>DEF_0_5G)
        &&(Vehicle_Speed>U16_F64SPEED_15KPH)
    )
    {
        pAxPDrivingOffset->AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr =ERR_PREFAILED;
    }
    else
    {
        pAxPDrivingOffset->AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr =ERR_PREPASSED;
    }

    if(inhibit==TRUE)
    {
        pAxPDrivingOffset->AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = ERR_INHIBIT;
    }
}

/**************************
     Driving  Ax Stick Error detect 
     *******************************/
static void AxP_StickChk(AxP_Main_HdrBusType *pAxP_StickChk, boolean inhibit)
{
    static Saluint32 Vehicle_Speed_Standard=0;
    static Saluint32 Vehicle_Speed_Over_Threshold =0;
    static Saluint32 Vehicle_Speed_Under_Threshold =0;
    static Saluint8 AxP_Stick_State =DEF_CHECK_INIT;
    static Salsint16 Ax_Max =0;
    static Salsint16 Ax_Min  =0;

    Saluint16 ABS_Diff_Max_Min_Ax =0;
    Saluint8 Vehicle_Speed_Stable =0;
    Salsint16 LongG_1_1000g=Proxy_a_long_1_1000g;
    Salsint32 Vehicle_Speed =pAxP_StickChk->AxP_MainVehSpd;
    
    switch(AxP_Stick_State)
    {
        case DEF_CHECK_INIT:
            Vehicle_Speed_Stable =AxP_Vehicle_Speed_State(pAxP_StickChk);
            if(Vehicle_Speed_Stable ==DEF_VEHICLE_SPEED_STABLE)
            {
                AxP_Stick_State =DEF_CHECK_ENABLE;
                Vehicle_Speed_Standard =Vehicle_Speed;
                Vehicle_Speed_Over_Threshold =AxP_Vehicle_Over_Speed_Calc(Vehicle_Speed_Standard);
                Vehicle_Speed_Under_Threshold =AxP_Vehicle_Under_Speed_Calc(Vehicle_Speed_Standard);
                Ax_Max =    LongG_1_1000g;
                Ax_Min =     LongG_1_1000g;
            }
            break;
         case DEF_CHECK_ENABLE:
                if(Ax_Max<LongG_1_1000g)
                {
                    Ax_Max=LongG_1_1000g;    
                }
                else if(Ax_Min>LongG_1_1000g)
                {
                    Ax_Min=LongG_1_1000g;    
                }
                else
                {
                    ;
                }

                ABS_Diff_Max_Min_Ax =(Saluint16 )(abs(Ax_Max-Ax_Min));
            
                if((Vehicle_Speed>Vehicle_Speed_Over_Threshold)
                    ||(Vehicle_Speed<Vehicle_Speed_Under_Threshold)
                    )
                {
                    if(ABS_Diff_Max_Min_Ax<DEF_0_05G)
                    {
                        pAxP_StickChk->AxP_MainAxPlauOutput.AxPlauStickErr=ERR_PREFAILED;
                    }
                    else
                    {
                        pAxP_StickChk->AxP_MainAxPlauOutput.AxPlauStickErr=ERR_PREPASSED;
                    }
                    AxP_Stick_State =DEF_CHECK_INIT;
                }      
            break;
         default:
            break;
    }

    if(inhibit==TRUE)
    {
        pAxP_StickChk->AxP_MainAxPlauOutput.AxPlauStickErr=ERR_INHIBIT;
    }
}

static Saluint8 AxP_Vehicle_Speed_State(AxP_Main_HdrBusType *pAxPVehicleSpeedState)
{
    Salsint32 Vehicle_Speed = U8_F64SPEED_0KPH25;
    Saluint32 Vehicle_Min_Wheel_Speed =U8_F64SPEED_0KPH25;
    Saluint32 SpeedDiff =U8_F64SPEED_0KPH25;
    Saluint8  Speed_State = DEF_VEHICLE_SPEED_UNSTABLE;

    Vehicle_Speed =pAxPVehicleSpeedState->AxP_MainVehSpd;
    Vehicle_Min_Wheel_Speed =pAxPVehicleSpeedState->AxP_MainWssSpeedOut.WssMin;

    if(Vehicle_Speed>=0)
    {
        SpeedDiff =(Saluint32 ) (abs(Vehicle_Speed-Vehicle_Min_Wheel_Speed));
    }

    if(SpeedDiff < U8_F64SPEED_3KPH)
    {
        Speed_State = DEF_VEHICLE_SPEED_STABLE;
    }

    return Speed_State;
}

static Salsint32 AxP_Vehicle_Over_Speed_Calc(Salsint32 speed)
{
    Salsint32 OverSpeed =0;
    OverSpeed=speed+U16_F64SPEED_20KPH;
    return OverSpeed;
}
Salsint32 AxP_Vehicle_Under_Speed_Calc(Salsint32 speed)
{
    Salsint32 UnderSpeed =0;
    if(speed>U16_F64SPEED_20KPH)
    {
        UnderSpeed =speed-U16_F64SPEED_20KPH;
    }
    return UnderSpeed;
}

/**************************
     Ax Noise Error detect 
     *******************************/
static void AxP_NoiseChk(AxP_Main_HdrBusType *pAxP_NoiseChk, boolean inhibit)
{
    Saluint16 TempDiffAxValue =0;
    static Salsint16 LongG_1_1000g =0;
    Salsint16 LongG_1_1000g_old=0;
    boolean Inhibit_Speed =0;

    LongG_1_1000g_old =LongG_1_1000g;
    LongG_1_1000g =Proxy_a_long_1_1000g;

    TempDiffAxValue =(Saluint16)abs(LongG_1_1000g-LongG_1_1000g_old);

    if(TempDiffAxValue>DEF_0_5G)
    {
        pAxP_NoiseChk->AxP_MainAxPlauOutput.AxPlauNoiseErr =ERR_PREFAILED;
    }
    else
    {
         pAxP_NoiseChk->AxP_MainAxPlauOutput.AxPlauNoiseErr =ERR_PREPASSED;
    }
    if(pAxP_NoiseChk->AxP_MainWssSpeedOut.WssMax >U8_F64SPEED_2KPH)
    {
        Inhibit_Speed =1;
    }
    if((inhibit==TRUE)||(Inhibit_Speed==TRUE))
    {
         pAxP_NoiseChk->AxP_MainAxPlauOutput.AxPlauNoiseErr=ERR_INHIBIT;
    }
}
