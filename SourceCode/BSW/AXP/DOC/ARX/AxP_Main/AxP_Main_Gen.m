AxP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Ax'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
AxP_MainEemFailData = CreateBus(AxP_MainEemFailData, DeList);
clear DeList;

AxP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    };
AxP_MainCanRxEscInfo = CreateBus(AxP_MainCanRxEscInfo, DeList);
clear DeList;

AxP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    };
AxP_MainEscSwtStInfo = CreateBus(AxP_MainEscSwtStInfo, DeList);
clear DeList;

AxP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
AxP_MainWhlSpdInfo = CreateBus(AxP_MainWhlSpdInfo, DeList);
clear DeList;

AxP_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    };
AxP_MainAbsCtrlInfo = CreateBus(AxP_MainAbsCtrlInfo, DeList);
clear DeList;

AxP_MainBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
AxP_MainBaseBrkCtrlModInfo = CreateBus(AxP_MainBaseBrkCtrlModInfo, DeList);
clear DeList;

AxP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
AxP_MainSenPwrMonitorData = CreateBus(AxP_MainSenPwrMonitorData, DeList);
clear DeList;

AxP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    };
AxP_MainEemSuspectData = CreateBus(AxP_MainEemSuspectData, DeList);
clear DeList;

AxP_MainEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Ax'
    };
AxP_MainEemEceData = CreateBus(AxP_MainEemEceData, DeList);
clear DeList;

AxP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_SubBusOff_Err'
    };
AxP_MainCanMonData = CreateBus(AxP_MainCanMonData, DeList);
clear DeList;

AxP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
AxP_MainWssSpeedOut = CreateBus(AxP_MainWssSpeedOut, DeList);
clear DeList;

AxP_MainEcuModeSts = Simulink.Bus;
DeList={'AxP_MainEcuModeSts'};
AxP_MainEcuModeSts = CreateBus(AxP_MainEcuModeSts, DeList);
clear DeList;

AxP_MainIgnOnOffSts = Simulink.Bus;
DeList={'AxP_MainIgnOnOffSts'};
AxP_MainIgnOnOffSts = CreateBus(AxP_MainIgnOnOffSts, DeList);
clear DeList;

AxP_MainIgnEdgeSts = Simulink.Bus;
DeList={'AxP_MainIgnEdgeSts'};
AxP_MainIgnEdgeSts = CreateBus(AxP_MainIgnEdgeSts, DeList);
clear DeList;

AxP_MainVBatt1Mon = Simulink.Bus;
DeList={'AxP_MainVBatt1Mon'};
AxP_MainVBatt1Mon = CreateBus(AxP_MainVBatt1Mon, DeList);
clear DeList;

AxP_MainDiagClrSrs = Simulink.Bus;
DeList={'AxP_MainDiagClrSrs'};
AxP_MainDiagClrSrs = CreateBus(AxP_MainDiagClrSrs, DeList);
clear DeList;

AxP_MainVehSpd = Simulink.Bus;
DeList={'AxP_MainVehSpd'};
AxP_MainVehSpd = CreateBus(AxP_MainVehSpd, DeList);
clear DeList;

AxP_MainAxPlauOutput = Simulink.Bus;
DeList={
    'AxPlauOffsetErr'
    'AxPlauDrivingOffsetErr'
    'AxPlauStickErr'
    'AxPlauNoiseErr'
    };
AxP_MainAxPlauOutput = CreateBus(AxP_MainAxPlauOutput, DeList);
clear DeList;

