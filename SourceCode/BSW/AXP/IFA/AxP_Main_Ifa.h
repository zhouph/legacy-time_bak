/**
 * @defgroup AxP_Main_Ifa AxP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AXP_MAIN_IFA_H_
#define AXP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define AxP_Main_Read_AxP_MainEemFailData(data) do \
{ \
    *data = AxP_MainEemFailData; \
}while(0);

#define AxP_Main_Read_AxP_MainCanRxEscInfo(data) do \
{ \
    *data = AxP_MainCanRxEscInfo; \
}while(0);

#define AxP_Main_Read_AxP_MainEscSwtStInfo(data) do \
{ \
    *data = AxP_MainEscSwtStInfo; \
}while(0);

#define AxP_Main_Read_AxP_MainWhlSpdInfo(data) do \
{ \
    *data = AxP_MainWhlSpdInfo; \
}while(0);

#define AxP_Main_Read_AxP_MainAbsCtrlInfo(data) do \
{ \
    *data = AxP_MainAbsCtrlInfo; \
}while(0);

#define AxP_Main_Read_AxP_MainBaseBrkCtrlModInfo(data) do \
{ \
    *data = AxP_MainBaseBrkCtrlModInfo; \
}while(0);

#define AxP_Main_Read_AxP_MainSenPwrMonitorData(data) do \
{ \
    *data = AxP_MainSenPwrMonitorData; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData(data) do \
{ \
    *data = AxP_MainEemSuspectData; \
}while(0);

#define AxP_Main_Read_AxP_MainEemEceData(data) do \
{ \
    *data = AxP_MainEemEceData; \
}while(0);

#define AxP_Main_Read_AxP_MainCanMonData(data) do \
{ \
    *data = AxP_MainCanMonData; \
}while(0);

#define AxP_Main_Read_AxP_MainWssSpeedOut(data) do \
{ \
    *data = AxP_MainWssSpeedOut; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_Ax(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_Ax; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define AxP_Main_Read_AxP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = AxP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define AxP_Main_Read_AxP_MainCanRxEscInfo_Ax(data) do \
{ \
    *data = AxP_MainCanRxEscInfo.Ax; \
}while(0);

#define AxP_Main_Read_AxP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = AxP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define AxP_Main_Read_AxP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = AxP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define AxP_Main_Read_AxP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = AxP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define AxP_Main_Read_AxP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = AxP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define AxP_Main_Read_AxP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = AxP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define AxP_Main_Read_AxP_MainAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = AxP_MainAbsCtrlInfo.AbsActFlg; \
}while(0);

#define AxP_Main_Read_AxP_MainBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define AxP_Main_Read_AxP_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = AxP_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define AxP_Main_Read_AxP_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define AxP_Main_Read_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_5V(data) do \
{ \
    *data = AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V; \
}while(0);

#define AxP_Main_Read_AxP_MainEemEceData_Eem_Ece_Ax(data) do \
{ \
    *data = AxP_MainEemEceData.Eem_Ece_Ax; \
}while(0);

#define AxP_Main_Read_AxP_MainCanMonData_CanM_SubBusOff_Err(data) do \
{ \
    *data = AxP_MainCanMonData.CanM_SubBusOff_Err; \
}while(0);

#define AxP_Main_Read_AxP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = AxP_MainWssSpeedOut.WssMax; \
}while(0);

#define AxP_Main_Read_AxP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = AxP_MainWssSpeedOut.WssMin; \
}while(0);

#define AxP_Main_Read_AxP_MainEcuModeSts(data) do \
{ \
    *data = AxP_MainEcuModeSts; \
}while(0);

#define AxP_Main_Read_AxP_MainIgnOnOffSts(data) do \
{ \
    *data = AxP_MainIgnOnOffSts; \
}while(0);

#define AxP_Main_Read_AxP_MainIgnEdgeSts(data) do \
{ \
    *data = AxP_MainIgnEdgeSts; \
}while(0);

#define AxP_Main_Read_AxP_MainVBatt1Mon(data) do \
{ \
    *data = AxP_MainVBatt1Mon; \
}while(0);

#define AxP_Main_Read_AxP_MainDiagClrSrs(data) do \
{ \
    *data = AxP_MainDiagClrSrs; \
}while(0);

#define AxP_Main_Read_AxP_MainVehSpd(data) do \
{ \
    *data = AxP_MainVehSpd; \
}while(0);


/* Set Output DE MAcro Function */
#define AxP_Main_Write_AxP_MainAxPlauOutput(data) do \
{ \
    AxP_MainAxPlauOutput = *data; \
}while(0);

#define AxP_Main_Write_AxP_MainAxPlauOutput_AxPlauOffsetErr(data) do \
{ \
    AxP_MainAxPlauOutput.AxPlauOffsetErr = *data; \
}while(0);

#define AxP_Main_Write_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr(data) do \
{ \
    AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr = *data; \
}while(0);

#define AxP_Main_Write_AxP_MainAxPlauOutput_AxPlauStickErr(data) do \
{ \
    AxP_MainAxPlauOutput.AxPlauStickErr = *data; \
}while(0);

#define AxP_Main_Write_AxP_MainAxPlauOutput_AxPlauNoiseErr(data) do \
{ \
    AxP_MainAxPlauOutput.AxPlauNoiseErr = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AXP_MAIN_IFA_H_ */
/** @} */
