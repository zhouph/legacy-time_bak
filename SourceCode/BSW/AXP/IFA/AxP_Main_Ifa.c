/**
 * @defgroup AxP_Main_Ifa AxP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        AxP_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "AxP_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "AxP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AXP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "AxP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/** Variable Section (32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AXP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AXP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "AxP_MemMap.h"
#define AXP_MAIN_START_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/** Variable Section (32BIT)**/


#define AXP_MAIN_STOP_SEC_VAR_32BIT
#include "AxP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AXP_MAIN_START_SEC_CODE
#include "AxP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AXP_MAIN_STOP_SEC_CODE
#include "AxP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
