#include "unity.h"
#include "unity_fixture.h"
#include "AxP_Main.h"
#include "AxP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_SenPwr_5V[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_5V;
const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_Ax[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_AX;
const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_AxP_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = AXP_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Salsint16 UtInput_AxP_MainCanRxEscInfo_Ax[MAX_STEP] = AXP_MAINCANRXESCINFO_AX;
const Saluint8 UtInput_AxP_MainEscSwtStInfo_EscDisabledBySwt[MAX_STEP] = AXP_MAINESCSWTSTINFO_ESCDISABLEDBYSWT;
const Saluint16 UtInput_AxP_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = AXP_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_AxP_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = AXP_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_AxP_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = AXP_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_AxP_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = AXP_MAINWHLSPDINFO_RRLWHLSPD;
const Rtesint32 UtInput_AxP_MainAbsCtrlInfo_AbsActFlg[MAX_STEP] = AXP_MAINABSCTRLINFO_ABSACTFLG;
const Rtesint32 UtInput_AxP_MainBaseBrkCtrlModInfo_VehStandStillStFlg[MAX_STEP] = AXP_MAINBASEBRKCTRLMODINFO_VEHSTANDSTILLSTFLG;
const Saluint8 UtInput_AxP_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = AXP_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_AxP_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = AXP_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssFL[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFL;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssFR[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFR;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssRL[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRL;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssRR[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRR;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_FrontWss[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_RearWss[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_REARWSS;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_12V[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_12V;
const Saluint8 UtInput_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_5V[MAX_STEP] = AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_5V;
const Saluint8 UtInput_AxP_MainEemEceData_Eem_Ece_Ax[MAX_STEP] = AXP_MAINEEMECEDATA_EEM_ECE_AX;
const Saluint8 UtInput_AxP_MainCanMonData_CanM_SubBusOff_Err[MAX_STEP] = AXP_MAINCANMONDATA_CANM_SUBBUSOFF_ERR;
const Saluint32 UtInput_AxP_MainWssSpeedOut_WssMax[MAX_STEP] = AXP_MAINWSSSPEEDOUT_WSSMAX;
const Saluint32 UtInput_AxP_MainWssSpeedOut_WssMin[MAX_STEP] = AXP_MAINWSSSPEEDOUT_WSSMIN;
const Mom_HndlrEcuModeSts_t UtInput_AxP_MainEcuModeSts[MAX_STEP] = AXP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_AxP_MainIgnOnOffSts[MAX_STEP] = AXP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_AxP_MainIgnEdgeSts[MAX_STEP] = AXP_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_AxP_MainVBatt1Mon[MAX_STEP] = AXP_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_AxP_MainDiagClrSrs[MAX_STEP] = AXP_MAINDIAGCLRSRS;
const Abc_CtrlVehSpd_t UtInput_AxP_MainVehSpd[MAX_STEP] = AXP_MAINVEHSPD;

const Saluint8 UtExpected_AxP_MainAxPlauOutput_AxPlauOffsetErr[MAX_STEP] = AXP_MAINAXPLAUOUTPUT_AXPLAUOFFSETERR;
const Saluint8 UtExpected_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr[MAX_STEP] = AXP_MAINAXPLAUOUTPUT_AXPLAUDRIVINGOFFSETERR;
const Saluint8 UtExpected_AxP_MainAxPlauOutput_AxPlauStickErr[MAX_STEP] = AXP_MAINAXPLAUOUTPUT_AXPLAUSTICKERR;
const Saluint8 UtExpected_AxP_MainAxPlauOutput_AxPlauNoiseErr[MAX_STEP] = AXP_MAINAXPLAUOUTPUT_AXPLAUNOISEERR;



TEST_GROUP(AxP_Main);
TEST_SETUP(AxP_Main)
{
    AxP_Main_Init();
}

TEST_TEAR_DOWN(AxP_Main)
{   /* Postcondition */

}

TEST(AxP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        AxP_MainEemFailData.Eem_Fail_SenPwr_12V = UtInput_AxP_MainEemFailData_Eem_Fail_SenPwr_12V[i];
        AxP_MainEemFailData.Eem_Fail_SenPwr_5V = UtInput_AxP_MainEemFailData_Eem_Fail_SenPwr_5V[i];
        AxP_MainEemFailData.Eem_Fail_Ax = UtInput_AxP_MainEemFailData_Eem_Fail_Ax[i];
        AxP_MainEemFailData.Eem_Fail_WssFL = UtInput_AxP_MainEemFailData_Eem_Fail_WssFL[i];
        AxP_MainEemFailData.Eem_Fail_WssFR = UtInput_AxP_MainEemFailData_Eem_Fail_WssFR[i];
        AxP_MainEemFailData.Eem_Fail_WssRL = UtInput_AxP_MainEemFailData_Eem_Fail_WssRL[i];
        AxP_MainEemFailData.Eem_Fail_WssRR = UtInput_AxP_MainEemFailData_Eem_Fail_WssRR[i];
        AxP_MainCanRxEscInfo.Ax = UtInput_AxP_MainCanRxEscInfo_Ax[i];
        AxP_MainEscSwtStInfo.EscDisabledBySwt = UtInput_AxP_MainEscSwtStInfo_EscDisabledBySwt[i];
        AxP_MainWhlSpdInfo.FlWhlSpd = UtInput_AxP_MainWhlSpdInfo_FlWhlSpd[i];
        AxP_MainWhlSpdInfo.FrWhlSpd = UtInput_AxP_MainWhlSpdInfo_FrWhlSpd[i];
        AxP_MainWhlSpdInfo.RlWhlSpd = UtInput_AxP_MainWhlSpdInfo_RlWhlSpd[i];
        AxP_MainWhlSpdInfo.RrlWhlSpd = UtInput_AxP_MainWhlSpdInfo_RrlWhlSpd[i];
        AxP_MainAbsCtrlInfo.AbsActFlg = UtInput_AxP_MainAbsCtrlInfo_AbsActFlg[i];
        AxP_MainBaseBrkCtrlModInfo.VehStandStillStFlg = UtInput_AxP_MainBaseBrkCtrlModInfo_VehStandStillStFlg[i];
        AxP_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_AxP_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        AxP_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_AxP_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        AxP_MainEemSuspectData.Eem_Suspect_WssFL = UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssFL[i];
        AxP_MainEemSuspectData.Eem_Suspect_WssFR = UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssFR[i];
        AxP_MainEemSuspectData.Eem_Suspect_WssRL = UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssRL[i];
        AxP_MainEemSuspectData.Eem_Suspect_WssRR = UtInput_AxP_MainEemSuspectData_Eem_Suspect_WssRR[i];
        AxP_MainEemSuspectData.Eem_Suspect_FrontWss = UtInput_AxP_MainEemSuspectData_Eem_Suspect_FrontWss[i];
        AxP_MainEemSuspectData.Eem_Suspect_RearWss = UtInput_AxP_MainEemSuspectData_Eem_Suspect_RearWss[i];
        AxP_MainEemSuspectData.Eem_Suspect_SenPwr_12V = UtInput_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_12V[i];
        AxP_MainEemSuspectData.Eem_Suspect_SenPwr_5V = UtInput_AxP_MainEemSuspectData_Eem_Suspect_SenPwr_5V[i];
        AxP_MainEemEceData.Eem_Ece_Ax = UtInput_AxP_MainEemEceData_Eem_Ece_Ax[i];
        AxP_MainCanMonData.CanM_SubBusOff_Err = UtInput_AxP_MainCanMonData_CanM_SubBusOff_Err[i];
        AxP_MainWssSpeedOut.WssMax = UtInput_AxP_MainWssSpeedOut_WssMax[i];
        AxP_MainWssSpeedOut.WssMin = UtInput_AxP_MainWssSpeedOut_WssMin[i];
        AxP_MainEcuModeSts = UtInput_AxP_MainEcuModeSts[i];
        AxP_MainIgnOnOffSts = UtInput_AxP_MainIgnOnOffSts[i];
        AxP_MainIgnEdgeSts = UtInput_AxP_MainIgnEdgeSts[i];
        AxP_MainVBatt1Mon = UtInput_AxP_MainVBatt1Mon[i];
        AxP_MainDiagClrSrs = UtInput_AxP_MainDiagClrSrs[i];
        AxP_MainVehSpd = UtInput_AxP_MainVehSpd[i];

        AxP_Main();

        TEST_ASSERT_EQUAL(AxP_MainAxPlauOutput.AxPlauOffsetErr, UtExpected_AxP_MainAxPlauOutput_AxPlauOffsetErr[i]);
        TEST_ASSERT_EQUAL(AxP_MainAxPlauOutput.AxPlauDrivingOffsetErr, UtExpected_AxP_MainAxPlauOutput_AxPlauDrivingOffsetErr[i]);
        TEST_ASSERT_EQUAL(AxP_MainAxPlauOutput.AxPlauStickErr, UtExpected_AxP_MainAxPlauOutput_AxPlauStickErr[i]);
        TEST_ASSERT_EQUAL(AxP_MainAxPlauOutput.AxPlauNoiseErr, UtExpected_AxP_MainAxPlauOutput_AxPlauNoiseErr[i]);
    }
}

TEST_GROUP_RUNNER(AxP_Main)
{
    RUN_TEST_CASE(AxP_Main, All);
}
