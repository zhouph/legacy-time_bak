#define UT_MAX_STEP 5

#define AXP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V {0,0,0,0,0}
#define AXP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_5V {0,0,0,0,0}
#define AXP_MAINEEMFAILDATA_EEM_FAIL_AX {0,0,0,0,0}
#define AXP_MAINEEMFAILDATA_EEM_FAIL_WSSFL {0,0,0,0,0}
#define AXP_MAINEEMFAILDATA_EEM_FAIL_WSSFR {0,0,0,0,0}
#define AXP_MAINEEMFAILDATA_EEM_FAIL_WSSRL {0,0,0,0,0}
#define AXP_MAINEEMFAILDATA_EEM_FAIL_WSSRR {0,0,0,0,0}
#define AXP_MAINCANRXESCINFO_AX {0,0,0,0,0}
#define AXP_MAINESCSWTSTINFO_ESCDISABLEDBYSWT {0,0,0,0,0}
#define AXP_MAINWHLSPDINFO_FLWHLSPD {0,0,0,0,0}
#define AXP_MAINWHLSPDINFO_FRWHLSPD {0,0,0,0,0}
#define AXP_MAINWHLSPDINFO_RLWHLSPD {0,0,0,0,0}
#define AXP_MAINWHLSPDINFO_RRLWHLSPD {0,0,0,0,0}
#define AXP_MAINABSCTRLINFO_ABSACTFLG {0,0,0,0,0}
#define AXP_MAINBASEBRKCTRLMODINFO_VEHSTANDSTILLSTFLG {0,0,0,0,0}
#define AXP_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE {0,0,0,0,0}
#define AXP_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFL {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFR {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRL {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRR {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_REARWSS {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_12V {0,0,0,0,0}
#define AXP_MAINEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_5V {0,0,0,0,0}
#define AXP_MAINEEMECEDATA_EEM_ECE_AX {0,0,0,0,0}
#define AXP_MAINCANMONDATA_CANM_SUBBUSOFF_ERR {0,0,0,0,0}
#define AXP_MAINWSSSPEEDOUT_WSSMAX {0,0,0,0,0}
#define AXP_MAINWSSSPEEDOUT_WSSMIN {0,0,0,0,0}
#define AXP_MAINECUMODESTS  {0,0,0,0,0}
#define AXP_MAINIGNONOFFSTS  {0,0,0,0,0}
#define AXP_MAINIGNEDGESTS  {0,0,0,0,0}
#define AXP_MAINVBATT1MON  {0,0,0,0,0}
#define AXP_MAINDIAGCLRSRS  {0,0,0,0,0}
#define AXP_MAINVEHSPD  {0,0,0,0,0}

#define AXP_MAINAXPLAUOUTPUT_AXPLAUOFFSETERR   {0,0,0,0,0}
#define AXP_MAINAXPLAUOUTPUT_AXPLAUDRIVINGOFFSETERR   {0,0,0,0,0}
#define AXP_MAINAXPLAUOUTPUT_AXPLAUSTICKERR   {0,0,0,0,0}
#define AXP_MAINAXPLAUOUTPUT_AXPLAUNOISEERR   {0,0,0,0,0}
