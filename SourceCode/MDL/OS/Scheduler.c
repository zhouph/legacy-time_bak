#ifndef __SCHEDULER_C__
#define __SCHEDULER_C__

#include "fifo.h"
#include "LibScheduler.h"
#include "Scheduler.h"

unsigned char PutOsState(OS_t *os, OsState_t mode);
unsigned char PutOsRunTaskId(OS_t *os, unsigned char id);
unsigned char TaskRegisterInit(TASK_t *task, TASK_t *task_info, unsigned char number_of_task, unsigned int tick);
unsigned char AdjustTaskRegister(TASK_t *a, unsigned int tick_period);
void Sch_PreProcCoreTask(unsigned char number_of_task, TASK_t *task, FIFO_BUFFER  *queue);
unsigned char GetRunningTaskId(TASK_t *a, OS_t *os, unsigned char number_of_task);
char Sch_ProcCoreTask(unsigned char number_of_queue, FIFO_BUFFER  *queue, TASK_t *task);
unsigned char CountCorePeriod(CORE_t *s);
void CountCorePeriod_Init(CORE_t *s);
unsigned LookCorePeriodCounter(CORE_t *s);
unsigned char Sch_CoreTickTok(CORE_t *core, TASK_t *task, unsigned char number_of_task);
unsigned int TasksMaxPeriod_Init(CORE_t *core, TASK_t *task, unsigned char NumberOfTask);

unsigned char PutOsState(OS_t *os, OsState_t mode)
{
	os->OsState = mode;
	return os->OsState;
}
unsigned char PutOsRunTaskId(OS_t *os, unsigned char id)
{
	os->RunTaskId=id;
	return os->RunTaskId;
}

unsigned char TaskRegisterInit(TASK_t *task, TASK_t *task_info, unsigned char number_of_task, unsigned int tick)
{
	unsigned char em_i, i;
	
	for(i=1; i<=number_of_task; i++)
	{
		em_i = i-1;
		task[em_i] = task_info[em_i];
		AdjustTaskRegister(&task[em_i], tick);
	}
	
	return 0;
}


unsigned char AdjustTaskRegister(TASK_t *a, unsigned int tick_period)
{
	a->TASK_EXEC_TIME = a->TASK_EXEC_TIME/tick_period - 1;
	a->TASK_RUN_START_POINT = a->TASK_RUN_START_POINT/tick_period;
	a->TASK_PERIOD_TIME = a->TASK_PERIOD_TIME/tick_period - 1;
	return 0;
}


void Sch_PreProcCoreTask(unsigned char number_of_task, TASK_t *task, FIFO_BUFFER  *queue)
{
	#define FIRST_TIME      2
	#define EXPIRED			1
	#define	OK				1
	#define	RUN_READY		1

	unsigned char i;

	for(i=0;i<number_of_task;i++)
	{
		if(CountTaskPeriod(&task[i])==EXPIRED)
		{
			CountTaskPeriod_Init(&task[i]);
			ExecuteTaskRunCounter_Init(&task[i]);
			PutTaskRunMode(&task[i],NOT_RUN);
		}

		if(GetTaskState(&task[i]) == RUNNING)
		{
			PutTaskState(&task[i], READY);
		}
		
		if(GetTaskRunOffsetState(&task[i]) == RUN_READY)
		{
			if(AddTask(&task[i],queue) == OK)
			{
				PutTaskRunMode(&task[i],RUN);
				PutTaskState(&task[i], READY);
			}
		}
	}
}

unsigned char GetRunningTaskId(TASK_t *a, OS_t *os, unsigned char number_of_task)
{
	unsigned char ok, semaphore_err, em_i, cur_taskstate, prev_taskstate;
//	unsigned char runmode;  TODO : warning [unused variable]
	int i;
	ok=0;
	semaphore_err=0;
		
	os->RunTaskId = PutOsState(os, IDLE);
	
	for(i=1; i<=number_of_task; i++)
	{
		em_i = i-1;
		if(semaphore_err==0)
		{
			cur_taskstate=GetTaskState(&a[em_i]);
			prev_taskstate=GetOldTaskState(&a[em_i]);
			if((cur_taskstate==RUNNING)||((cur_taskstate==SUSPENDED && prev_taskstate==RUNNING)))
			{
				PutOsRunTaskId(os, GetTaskNickName(&a[em_i]));
				PutOsState(os, SINGLE_TASK_RUN);
				break;
			}
		}
		else
		{
			PutOsState(os, SEMAPHORE_ERR);
		}
	}
	
	return ok;
}
	
char Sch_ProcCoreTask(unsigned char number_of_queue, FIFO_BUFFER  *queue, TASK_t *task)
{
	#define FIRST_TIME      2
	#define EXPIRED			1
	#define	OK				1
	#define	RUN_READY		1
	
	unsigned char h_TASK;
	int q;

	for(q=number_of_queue-1; q>-1; q--)
	{
		if(FIFO_Count(&queue[q])>0)
		{
			h_TASK = FIFO_Look(&queue[q]);
		
			if(GetTaskState(&task[h_TASK]) == READY)
			{
				ExecuteTaskRunCounter(&task[h_TASK]);
				
				if(LookTaskRunCounter(&task[h_TASK]) == EXPIRED) 
				{
					PutTaskState(&task[h_TASK], RUNNING);
					
					RunTask(&task[h_TASK]);
					RunRteWrite(&task[h_TASK]);
					RemoveTask(&task[h_TASK], queue);

					PutTaskState(&task[h_TASK], SUSPENDED);
					break;
				}
				else
				{
					PutTaskState(&task[h_TASK], RUNNING);
					
					if(LookTaskRunCounter(&task[h_TASK]) == FIRST_EXECUTED)
					{
						RunRteRead(&task[h_TASK]);
					}
					
					break;
				}
			}
		}
	}
	return h_TASK;
}

unsigned char Sch_CoreTickTok(CORE_t *core, TASK_t *task, unsigned char number_of_task)
{
	if(core->FIRST_RUN == 0)
	{
		if(TasksMaxPeriod_Init(core, task, number_of_task) > 0)
		{
			core->FIRST_RUN = 1;
		}
	}
	else
	{
		if(CountCorePeriod(core) == EXPIRED)
		{
			CountCorePeriod_Init(core);
		}
	}
	
	return core->FIRST_RUN;
}

unsigned char CountCorePeriod(CORE_t *core)
{
	unsigned char expired;

	if(core->period_counter > core->MAX_PERIOD)
	{
		expired=1;
	}
	else
	{
		core->period_counter++;
		expired = 0;
	}

	return expired;
}

void CountCorePeriod_Init(CORE_t *core)
{
	core->period_counter = 0;
}

unsigned int LookCorePeriodCounter(CORE_t *core)
{
	return core->period_counter;
}

unsigned int TasksMaxPeriod_Init(CORE_t *core, TASK_t *task, unsigned char NumberOfTask)
{
	unsigned a,b;
	unsigned int i;
	
	a = 0;
	
	if(NumberOfTask >= 1)
	{
		a = GetTaskPeriod(&task[0])-1;
	}
	
	if(NumberOfTask > 1)
	{
		for(i=1;i<NumberOfTask;i++)
		{
			b = GetTaskPeriod(&task[i])-1;
			a = MaxCommonNumber(a, b);
		}
	}
	
	core->MAX_PERIOD = a;
	
	return core->MAX_PERIOD;
}
#endif
