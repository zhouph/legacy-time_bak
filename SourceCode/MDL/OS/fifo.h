#ifndef FIFO_H
#define FIFO_H

#define FIFO_BUFFER_SIZE	16

struct fifo_buffer_t {
	unsigned id;
    unsigned head;         /* first byte of data */
    unsigned tail;         /* last byte of data */
    unsigned char *buffer; /* block of memory or array of data */
    unsigned buffer_len;  			/* length of the data */
};
typedef struct fifo_buffer_t FIFO_BUFFER;

unsigned FIFO_Count(FIFO_BUFFER  *b);
unsigned char FIFO_Full(FIFO_BUFFER *b);
unsigned char FIFO_Empty( FIFO_BUFFER *b);
unsigned char FIFO_Look(FIFO_BUFFER const *b);
unsigned char FIFO_Get(FIFO_BUFFER *b);
unsigned char FIFO_Put(FIFO_BUFFER *b, unsigned char data_byte);
unsigned char FIFO_Init(FIFO_BUFFER *b,unsigned char *buffer, unsigned buffer_len);
/* note: 버퍼 길이는 2의 배수여야 한다. */

#endif
