#ifndef __LIBSCHEDULER_H__
#define __LIBSCHEDULER_H__

#include "fifo.h"   /* TODO : GH, Errors(unknown type name 'FIFO_BUUFFER') */

typedef enum      
{
	DORMANT,        
	READY,          
	RUNNING,        
	SUSPENDED       
} TaskState_t;     

typedef enum      
{
	NOT_RUN,      
	RUN_READY,   
	RUN,         
	RUNNED       
} TaskRunModeState_t; 

typedef enum   
{
	IDLE,
	SINGLE_TASK_RUN,
	SEMAPHORE_ERR,
} OsState_t; 

typedef enum     
{
	NOT_EXCUTED,   
	EXPIRED,
	EXECUTING,
	FIRST_EXECUTED,       
} TaskExecState_t;

typedef void (*SCH_TASK_POINTER)(void);
typedef void (*SCH_RTE_READ_EVENT_POINTER)(void);
typedef void (*SCH_RTE_WRITE_EVENT_POINTER)(void);

typedef struct NewTask_type
{
	unsigned char ID;
	TaskRunModeState_t	RunMode;
	TaskState_t TaskState;
	TaskState_t OldTaskState;
	unsigned int PeriodCounter;
	unsigned int TaskExecTimer;
	unsigned char TaskPrio;
	
	unsigned char TASK_NICKNAME;
	unsigned int TASK_PERIOD_TIME;
	unsigned int TASK_RUN_START_POINT;
	unsigned int TASK_EXEC_TIME;
	
	SCH_TASK_POINTER	pFcnTask;
	SCH_RTE_READ_EVENT_POINTER pFcnRteRead;
	SCH_RTE_WRITE_EVENT_POINTER pFcnRteWrite;
} TASK_t;

unsigned char GetTaskID(TASK_t *a);
unsigned char GetTaskState(TASK_t *a);
unsigned char GetOldTaskState(TASK_t *a);
unsigned int GetTaskPeriod(TASK_t *a);
unsigned char GetTaskPrio(TASK_t *a);
unsigned char PutTaskState(TASK_t *a, TaskRunModeState_t state);
void CountTaskPeriod_Init(TASK_t *a);
unsigned int LookTaskPeriodCounter(TASK_t *a);
void RunTask(TASK_t *a);
void RunRteRead(TASK_t *a);
void RunRteWrite(TASK_t *a);
unsigned char GetTaskRunMode(TASK_t *a);
void PutTaskRunMode(TASK_t *a, unsigned char mode);
void ExecuteTaskRunCounter_Init(TASK_t *a);
unsigned char AddTask(TASK_t *a, FIFO_BUFFER  *b);
unsigned char RemoveTask(TASK_t *a, FIFO_BUFFER  *b);
unsigned char GetTaskNickName(TASK_t *a);
unsigned MaxCommonNumber(unsigned a, unsigned b);
unsigned char CountTaskPeriod(TASK_t *a);
TaskRunModeState_t GetTaskRunOffsetState(TASK_t *a);
TaskExecState_t ExecuteTaskRunCounter(TASK_t *a);
TaskExecState_t LookTaskRunCounter(TASK_t *a);


#endif

