#ifndef __LIBSCHEDULER_C__
#define __LIBSCHEDULER_C__

#include "LibScheduler.h"

unsigned char GetTaskID(TASK_t *a)
{
	return a->ID;
}

unsigned char GetTaskState(TASK_t *a)
{
	return a->TaskState;
}

unsigned char GetOldTaskState(TASK_t *a)
{
	return a->OldTaskState;
}

unsigned int GetTaskPeriod(TASK_t *a)
{
	return a->TASK_PERIOD_TIME;
}
unsigned char GetTaskPrio(TASK_t *a)
{
	return a->TaskPrio;
}
unsigned char PutTaskState(TASK_t *a, TaskRunModeState_t state)
{
	a->TaskState = a->OldTaskState;
	a->TaskState = state;
	return a->TaskState;
}

void CountTaskPeriod_Init(TASK_t *a)
{
	a->PeriodCounter = 1;
}

unsigned int LookTaskPeriodCounter(TASK_t *a)
{
	return a->PeriodCounter;
}

void RunTask(TASK_t *a)
{
	a->pFcnTask();
}

void RunRteRead(TASK_t *a)
{
	a->pFcnRteRead();
}

void RunRteWrite(TASK_t *a)
{
	a->pFcnRteWrite();
}

unsigned char GetTaskRunMode(TASK_t *a)
{
	return a->RunMode;
}

void PutTaskRunMode(TASK_t *a, unsigned char mode)
{
	a->RunMode=mode;
}

void ExecuteTaskRunCounter_Init(TASK_t *a)
{
	a->TaskExecTimer = 0;
}

unsigned char AddTask(TASK_t *a, FIFO_BUFFER  *b)
{
	unsigned char ok, prio, data, task_id;
	
	ok = 0;
	prio = GetTaskPrio(a);
	task_id = GetTaskID(a);
	data = FIFO_Put((b+prio), task_id);
	
	if(task_id==data) ok=1;
	
	return ok;
}

unsigned char RemoveTask(TASK_t *a, FIFO_BUFFER  *b)
{
	unsigned char prio, ok, task_id, removed_task_id;
	
	ok = 0;
	prio = GetTaskPrio(a);
	task_id = GetTaskID(a);
	removed_task_id = FIFO_Get((b+prio));
	if(removed_task_id==task_id) ok =1;

	return ok;
}

unsigned char GetTaskNickName(TASK_t *a)
{
	return a->TASK_NICKNAME;
}

unsigned MaxCommonNumber(unsigned a, unsigned b)
{
	unsigned x,y,g,tmp,c,d,i;
	
	if(a>b)
	{
		c=a;
		d=b;
	}
	
	if(a<b)
	{
		c=b;
		d=a;
	}
	
	for(i=1; i<= d; i++)
	{
		if(c%i == 0)
		{
			if(d%i == 0)
			{
				tmp = i;
			}
		}
	}
	
	x = c/tmp;
	y = d/tmp;
	g = x*y*tmp;

	return g;
}


unsigned char CountTaskPeriod(TASK_t *a)
{
	unsigned char expired;

	if(LookTaskPeriodCounter(a) > GetTaskPeriod(a))
	{
		expired=1;
	}
	else
	{
		a->PeriodCounter++;
		expired = 0;
	}

	return expired;
}

TaskRunModeState_t GetTaskRunOffsetState(TASK_t *a)
{
	switch(GetTaskRunMode(a))
	{
		case NOT_RUN:
			if((LookTaskPeriodCounter(a) > a->TASK_RUN_START_POINT))
			{
				PutTaskRunMode(a,RUN_READY);
			}
			
			break;
			
		case RUN:
			
			if(GetTaskState(a)==SUSPENDED)
			{
				PutTaskRunMode(a,RUNNED);
			}
			break;
			
		default:
			break;
	}
	
	return GetTaskRunMode(a);
}

TaskExecState_t ExecuteTaskRunCounter(TASK_t *a)
{
	TaskExecState_t mode;
	
	if(a->TaskExecTimer > a->TASK_EXEC_TIME)
	{
		mode = EXPIRED;
	}
	else
	{
		a->TaskExecTimer++;
		
		if(a->TaskExecTimer==0)
		{
			mode = NOT_EXCUTED;
		}
		else
		{
			if(a->TaskExecTimer>1)
			{
				mode = EXECUTING;
			}
			else
			{
				mode = FIRST_EXECUTED;
			}
		}
	}
	return mode;
}



TaskExecState_t LookTaskRunCounter(TASK_t *a)
{
	TaskExecState_t mode;
	if(a->TaskExecTimer > a->TASK_EXEC_TIME)
	{
		mode = EXPIRED;
	}
	else
	{
		
		if(a->TaskExecTimer==0)
		{
			mode = NOT_EXCUTED;
		}
		else
		{
			if(a->TaskExecTimer>1)
			{
				mode = EXECUTING;
			}
			else
			{
				mode = FIRST_EXECUTED;
			}
		}
	}
	return mode;
}

#endif
