#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

typedef struct NewCore_type
{
	unsigned int period_counter;
	unsigned int MAX_PERIOD;
	unsigned char FIRST_RUN; 
} CORE_t;

typedef struct NewOs_type
{
	OsState_t OsState;
	unsigned char RunTaskId;
}OS_t;

extern unsigned char TaskRegisterInit(TASK_t *task, TASK_t *task_info, unsigned char number_of_task, unsigned int tick);
extern void Sch_PreProcCoreTask(unsigned char number_of_task, TASK_t *task, FIFO_BUFFER  *queue);
extern unsigned char GetRunningTaskId(TASK_t *a, OS_t *os, unsigned char number_of_task);
extern char Sch_ProcCoreTask(unsigned char number_of_queue, FIFO_BUFFER  *queue, TASK_t *task);
extern unsigned char Sch_CoreTickTok(CORE_t *core, TASK_t *task, unsigned char number_of_task);
#endif
