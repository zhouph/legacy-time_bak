#include "fifo.h"

unsigned FIFO_Count(FIFO_BUFFER  *b)
{
	return (b->head - b->tail);
}

unsigned char FIFO_Full(FIFO_BUFFER *b)
{
	return (b ? (FIFO_Count(b) == b->buffer_len) : 1);
}

unsigned char FIFO_Empty( FIFO_BUFFER *b)
{
	if(FIFO_Count(b)==0)	return 1;
	else					return 0;
}

unsigned char FIFO_Look(FIFO_BUFFER const *b)
{
	if(b)
	{
		return (b->buffer[b->tail % b->buffer_len]);
	}
	
	return 0;
}

unsigned char FIFO_Get(FIFO_BUFFER *b)
{
    unsigned char data_byte = 0;

    if (!FIFO_Empty(b)) 
    {
        data_byte = b->buffer[b->tail % b->buffer_len];
        b->tail++;
    }
    return data_byte;
}

unsigned char FIFO_Put(FIFO_BUFFER *b, unsigned char data_byte)
{
    unsigned char status = 0;        /* return value */
	
	if(b)
	{
		/* limit the ring to prevent overwriting */
		if (!FIFO_Full(b)) 
		{
			b->buffer[b->head % b->buffer_len] = data_byte;
			b->head++;
			status = data_byte;
		}
	}

    return data_byte;
}

unsigned char FIFO_Init(FIFO_BUFFER *b,unsigned char *buffer, unsigned buffer_len)
{
	unsigned char err;
	err = 0;
	
	if (b){
		b->head = 0;
		b->tail = 0;
		b->buffer = buffer;
		b->buffer_len = buffer_len;
	}
	else
	{
		err=1;
	}
	return err;
	
}
