/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Mcal_Trap.c $                                              **
**                                                                           **
**  $CC VERSION : \main\16 $                                                 **
**                                                                           **
**  $DATE       : 2014-01-31 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains                                         **
**                - Trap functionality (only for illustration purpose)       **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Inclusion of Platform_Types.h and Compiler.h */
#include "Std_Types.h"
/* Inclusion of Tasking sfr file */
#include "IfxScu_reg.h"
#include "Mcal.h"
//#include "Test_Print.h"
#include "Mcal_Options.h"

/********************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/

/********************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
#ifdef _TASKING_C_TRICORE_
#if (_TASKING_C_TRICORE_ == 1U)
#define DEBUG()  __debug()
#endif /* #if (_TASKING_C_TRICORE_ == 1U) */
#endif

#ifdef _GNU_C_TRICORE_
#if (_GNU_C_TRICORE_ == 1U)
#define DEBUG() __asm__ volatile ("debug")
#endif /* #if (_GNU_C_TRICORE_ == 1U) */
#endif

#ifdef _DIABDATA_C_TRICORE_
#if (_DIABDATA_C_TRICORE_ == 1U)
#define DEBUG() __debug()
#define __debug _debug
#endif /* #if (_DIABDATA_C_TRICORE_ == 1U) */
#endif

/********************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/********************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
void check_cpu0_trap_1_tin6_num(void);
void check_cpu1_trap_1_tin6_num(void);
void check_cpu2_trap_1_tin6_num(void);

/********************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/********************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
uint32 wmu32_cpu0_trap1_tin6_CNT;
uint32 wmu32_cpu1_trap1_tin6_CNT;
uint32 wmu32_cpu2_trap1_tin6_CNT;

/********************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/

/********************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_VAR_32BIT
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_VAR_32BIT_ASIL_B
#include "Ifx_MemMap.h"
#endif
static uint32 UVALCON0;
static uint32 TrapIdentification[8][8];
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_VAR_32BIT
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_VAR_32BIT_ASIL_B
#include "Ifx_MemMap.h"
#endif

/********************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE_CPU0_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_CPU0_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif

void cpu0_trap_0(void) __attribute__((protect));
void cpu0_trap_1 (void) __attribute__((protect));
void cpu0_trap_2 (void) __attribute__((protect));
void cpu0_trap_3 (void) __attribute__((protect));
void cpu0_trap_4 (void) __attribute__((protect));
void cpu0_trap_5 (void) __attribute__((protect));
void cpu0_trap_6 (void) __attribute__((protect));
void cpu0_trap_7 (void) __attribute__((protect));
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE_CPU0_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_CPU0_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE_CPU1_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_CPU1_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif
void cpu1_trap_0 (void) __attribute__((protect));
void cpu1_trap_1 (void) __attribute__((protect));
void cpu1_trap_2 (void) __attribute__((protect));
void cpu1_trap_3 (void) __attribute__((protect));
void cpu1_trap_4 (void) __attribute__((protect));
void cpu1_trap_5 (void) __attribute__((protect));
void cpu1_trap_6 (void) __attribute__((protect));
void cpu1_trap_7 (void) __attribute__((protect));
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE_CPU1_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_CPU1_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif

#if ( MCAL_NO_OF_CORES == 3U )
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE_CPU2_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_CPU2_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif
void cpu2_trap_0 (void) __attribute__((protect));
void cpu2_trap_1 (void) __attribute__((protect));
void cpu2_trap_2 (void) __attribute__((protect));
void cpu2_trap_3 (void) __attribute__((protect));
void cpu2_trap_4 (void) __attribute__((protect));
void cpu2_trap_5 (void) __attribute__((protect));
void cpu2_trap_6 (void) __attribute__((protect));
void cpu2_trap_7 (void) __attribute__((protect));
#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE_CPU2_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_CPU2_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif
#endif

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
#endif

void _trap_0( void );
void _trap_1( void );
void _trap_2( void );
void _trap_3( void );
void _trap_4( void );
void _trap_5( void );
void _trap_6( void );
void _trap_7( void );


/********************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/

/******************************************************************************
** Syntax : void _trap_0( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample Service for  class 0 trap                            **
**                                                                           **
*****************************************************************************/
/* Trap class 0 handler. */
void _trap_0( void )
{
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  TrapIdentification[0][tin] = 1;
  switch(tin)
   {
     case 0:
          //print_f("\nClass 0: Virtual Address Fill Trap occurred\n");
          //get_char();
       
          break;

     case 1:
          //print_f("\nClass 0: Virtual Address Protection Trap occurred\n");
          //get_char();
                 
          break;

     default:
	 	    check_cpu1_trap_1_tin6_num();
			__asm ("rslcx\n");
			__asm("add.a a11, #4"); // Increase return address
			__asm("rfe");
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
   }
  __asm ("rslcx \n");
  __asm ("rfe \n");
}
/******************************************************************************
** Syntax : void _trap_1( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 1 trap                            **
**                                                                           **
*****************************************************************************/
/* Trap class 1 handler. */
void _trap_1( void )
{
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  TrapIdentification[1][tin] = 1;
  switch(tin)
   {
     case 1:
          //print_f("\nClass 1: Privilege Instruction Trap occurred\n");
          //get_char();

          break;

     case 2:
          //print_f("\nClass 1: Memory Protection Read Trap occurred\n");
          //get_char();

          break;

     case 3:
          //print_f("\nClass 1: Memory Protection Write Trap occurred\n");
          //get_char();
          
          break;

     case 4:
          //print_f("\nClass 1: Memory Protection Execution Trap occurred\n");
          //get_char();

          break;

     case 5:
          //print_f("\nClass 1: Memory Protection Peripheral Access Trap occurred\n");
          //get_char();
          
          break;

     case 6:
          //print_f("\nClass 1: Memory Protection Null Address Trap occurred\n");
          //get_char();
		  	  check_cpu1_trap_1_tin6_num();
		  	__asm ("rslcx\n");
			__asm("add.a a11, #4"); // Increase return address
			__asm("rfe");
          break;

     case 7:
          //print_f("\nClass 1: Global Register Write Protection Trap occurred\n");
          //get_char();
          
          break;


     default:
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
   }
  __asm ("rslcx\n");
  __asm ("rfe \n");
}
/******************************************************************************
** Syntax : void _trap_2( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 2 trap                            **
**                                                                           **
*****************************************************************************/
/* Trap class 2 handler. */
void _trap_2( void )
{
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  TrapIdentification[2][tin] = 1;
  switch(tin)
   {
     case 1:
          //print_f("\nClass 2: Illegal Opcode Trap occurred\n");
          //get_char();

          break;

     case 2:
          //print_f("\nClass 2: Unimplemented Opcode Trap occurred\n");
          //get_char();

          break;

     case 3:
          //print_f("\nClass 2: Invalid Operand Specification Trap occurred\n");
          //get_char();

          break;

     case 4:
          //print_f("\nClass 2: Data Address Alignment Trap occurred\n");
          //get_char();
          
          break;

     case 5:
          //print_f("\nClass 2: Invalid Local Memory Address Trap occurred\n");
          //get_char();
          
          break;

     default:
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
   }
  __asm ("rslcx \n");
  __asm ("rfe \n");
}
/******************************************************************************
** Syntax : void _trap_3( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 3 trap                            **
**                                                                           **
*****************************************************************************/

/* Trap class 3 handler. */
void _trap_3( void )
{
  /* No local variables used in this function */
  /* No function calls to be performed */
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  TrapIdentification[3][tin] = 1;

  /* The following steps are done to
   * perform Mcal_ResetENDINIT. The function call is avoided
   * as the trap has occurred due to contest overflow / underflow
   * or stack issue.
   */
  /* Mcal_ResetENDINIT starts........*/

    UVALCON0  = (uint32) SCU_WDTCPU0_CON0.U; 
    SCU_WDTCPU0_CON0.U = SCU_WDTCPU0_CON0.U & ~0x00000001 ;

    UVALCON0 |=  0x000000F1;       /*  set HWPW1 = 1111b */
    UVALCON0 &= ~0x00000002;       /*  set WDTLCK = 0 */
    UVALCON0 &= ~0x0000000C;       /*  set HWPW0 = 00b */

    SCU_WDTCPU0_CON0.U =  UVALCON0;          /*  unlock access */

    SCU_WDTCPU0_CON0.U  |=  0x000000F2;      /*  set HWPW1 = 1111b and WDTLCK = 1 */
    SCU_WDTCPU0_CON0.U  &= ~0x0000000C;      /*  set HWPW0 = 00b */
  switch(tin)
  {
     case 1:
         /* Free context list depletion trap */
     case 2:
         /* Call depth overflow trap occurred */
     case 3:
         /* Call depth underflow trap occurred */
     case 4:
         /* Free context list underflow trap */
     case 5:
         /* call stack underflow trap */
     case 6:
         /* context type trap */
     case 7:
         /* Nesting error */
         /* Perform reset */
         SCU_SWRSTCON.B.SWRSTREQ = 1U;
         break;
     default:
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
  }
  __asm ("rslcx \n");
  __asm ("rfe \n");
 
/* The return statement here intentionally removed because compiler will 
   generate SVLCX instruction in the begining of trap3 handler which again 
   results in trap3 because there is no free CSA, so the warning generated by 
   compiler here should be ignored */
}

/******************************************************************************
** Syntax : void _trap_4( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 4 trap                            **
**                                                                           **
*****************************************************************************/
/* Trap class 4 handler. */
void _trap_4( void )
{
  uint32 tin;
  
  __asm ("svlcx\n");

  __GETTIN (tin);

  TrapIdentification[4][tin] = 1;
  switch(tin)
   {
     case 1:
          //print_f("\nClass 4: Program Fetch Synchronous Error Trap occurred\n");
          //get_char();
          
          break;

     case 2:
          //print_f("\nClass 4: Data Access Synchronous Error Trap occurred\n");
          //get_char();

          break;

     case 3:
          //print_f("\nClass 4: Data Access Asynchronous Error Trap occurred\n");
          //get_char();

          break;

     default:
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
   }
  __asm ("rslcx \n");
  __asm ("rfe \n");
}
/******************************************************************************
** Syntax : void _trap_5( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 5 trap                            **
**                                                                           **
*****************************************************************************/
/* Trap class 5 handler. */
void _trap_5( void )
{
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  TrapIdentification[5][tin] = 1;
  switch(tin)
   {
     case 1:
          //print_f("\nClass 5: Arithemetic Overflow Trap occurred\n");
          //get_char();

          break;

     case 2:
          //print_f("\nClass 5: Sticky Arithemetic Overflow Trap occurred\n");
          //get_char();
          
          break;

     default:
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
   }
  __asm ("rslcx \n");
  __asm ("rfe \n");
}

/******************************************************************************
** Syntax : void _trap_6( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 6 trap                            **
**                                                                           **
*****************************************************************************/

/* Trap class 6 handler. */
void _trap_6( void )
{
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  /* TIN can be any number between 0 and 255.
   * storing the tin number in
   * TrapIdentification[6][0]
   */
  TrapIdentification[6][0] = tin;
  //print_f("\nClass 6: System Call Trap occurred\n");
  //get_char();
        

  __asm ("rslcx \n");
  __asm ("rfe \n");
}

/******************************************************************************
** Syntax : void _trap_7( void )                                             **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  class 7 trap                            **
**                                                                           **
*****************************************************************************/

/* Trap class 7 handler. */
void _trap_7( void )
{
  uint32 tin;
  
  __asm ("svlcx");

  __GETTIN (tin);

  TrapIdentification[7][tin] = 1;
  switch(tin)
   {
     case 0:
          //print_f("\nClass 7: Non-Maskable Interrupt Trap occurred\n");
          //get_char();

          break;

     default:
          /* Halt the execution if debug mode enabled.*/
          DEBUG();
          break;
   }
  __asm ("rslcx \n");
  __asm ("rfe \n");
}

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
#endif

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE_CPU0_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_CPU0_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif
/******************************************************************************
** Syntax : void cpu0_trap_x (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  Cpu0 trap                               **
**                                                                           **
*****************************************************************************/
void cpu0_trap_0 (void)
{
  __trap_handler (_trap_0);
}

void cpu0_trap_1 (void)
{
  __trap_handler (_trap_1);
}

void cpu0_trap_2 (void)
{
  __trap_handler (_trap_2);
}

void cpu0_trap_3 (void)
{
  __trap_handler (_trap_3);
}

void cpu0_trap_4 (void)
{
  __trap_handler (_trap_4);
}

void cpu0_trap_5 (void)
{
  __trap_handler (_trap_5);
}

void cpu0_trap_6 (void)
{
  __trap_handler (_trap_6);
}

void cpu0_trap_7 (void)
{
  __trap_handler (_trap_7);
}

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE_CPU0_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_CPU0_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE_CPU1_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_CPU1_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif
/******************************************************************************
** Syntax : void cpu1_trap_x (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  Cpu1 trap                               **
**                                                                           **
*****************************************************************************/
void cpu1_trap_0 (void)
{
  __trap_handler (_trap_0);
}

void cpu1_trap_1 (void)
{
  __trap_handler (_trap_1);
}

void cpu1_trap_2 (void)
{
  __trap_handler (_trap_2);
}

void cpu1_trap_3 (void)
{
  __trap_handler (_trap_3);
}

void cpu1_trap_4 (void)
{
  __trap_handler (_trap_4);
}

void cpu1_trap_5 (void)
{
  __trap_handler (_trap_5);
}

void cpu1_trap_6 (void)
{
  __trap_handler (_trap_6);
}

void cpu1_trap_7 (void)
{
  __trap_handler (_trap_7);
}

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE_CPU1_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_CPU1_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif

#if ( MCAL_NO_OF_CORES == 3U )

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_START_SEC_CODE_CPU2_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_START_SEC_CODE_CPU2_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif
/******************************************************************************
** Syntax : void cpu2_trap_x (void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Sample handler for  Cpu2 trap                               **
**                                                                           **
*****************************************************************************/
void cpu2_trap_0 (void)
{
  __trap_handler (_trap_0);
}

void cpu2_trap_1 (void)
{
  __trap_handler (_trap_1);
}

void cpu2_trap_2 (void)
{
  __trap_handler (_trap_2);
}

void cpu2_trap_3 (void)
{
  __trap_handler (_trap_3);
}

void cpu2_trap_4 (void)
{
  __trap_handler (_trap_4);
}

void cpu2_trap_5 (void)
{
  __trap_handler (_trap_5);
}

void cpu2_trap_6 (void)
{
  __trap_handler (_trap_6);
}

void cpu2_trap_7 (void)
{
  __trap_handler (_trap_7);
}

void check_cpu0_trap_1_tin6_num(void)
{
	wmu32_cpu0_trap1_tin6_CNT++;
}

void check_cpu1_trap_1_tin6_num(void)
{
	wmu32_cpu1_trap1_tin6_CNT++;
}

void check_cpu2_trap_1_tin6_num(void)
{
	wmu32_cpu2_trap1_tin6_CNT++;
}

#if (IFX_MCAL_USED == STD_ON)
#define MCAL_TRAP_STOP_SEC_CODE_CPU2_TRAP
#include "MemMap.h"
#else
#define IFX_MCAL_TRAP_STOP_SEC_CODE_CPU2_TRAP_ASIL_B
#include "Ifx_MemMap.h"
#endif

#endif /* ( MCAL_NO_OF_CORES == 3U ) */

