/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Sent_Irq.c $                                               **
**                                                                           **
**  $CC VERSION : \main\8 $                                                  **
**                                                                           **
**  $DATE       : 2013-09-20 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains the interrupt frames for the SENT. This **
**                file is given for evaluation purpose only.                 **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
/* Inclusion of Tasking sfr file */


/*Include Irq Module*/
#include "Irq.h"

/* Include Mcal.h to import the library functions */ 
#include "Mcal.h"

/* Include SENT header file  */
#include "Sent.h"
/*******************************************************************************
**                      Imported Compiler Switch Checks                       **
*******************************************************************************/

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
#define IRQ_START_SEC_CODE
#include "MemMap.h"

#if (IRQ_SENT_EXIST == STD_ON)

/******************************************************************************
** Syntax : void SENTSR0_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 0                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR0_PRIO > 0) || (IRQ_SENT_SR0_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR0_PRIO > 0) && (IRQ_SENT_SR0_CAT == IRQ_CAT1))
void SENTSR0_ISR(void)
#elif IRQ_SENT_SR0_CAT == IRQ_CAT23
ISR(SENTSR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[0]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR1_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 1                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR1_PRIO > 0) || (IRQ_SENT_SR1_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR1_PRIO > 0) && (IRQ_SENT_SR1_CAT == IRQ_CAT1))
void SENTSR1_ISR(void)
#elif IRQ_SENT_SR1_CAT == IRQ_CAT23
ISR(SENTSR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[1]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR2_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 2                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR2_PRIO > 0) || (IRQ_SENT_SR2_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR2_PRIO > 0) && (IRQ_SENT_SR2_CAT == IRQ_CAT1))
void SENTSR2_ISR(void)
#elif IRQ_SENT_SR2_CAT == IRQ_CAT23
ISR(SENTSR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[2]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR3_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 3                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR3_PRIO > 0) || (IRQ_SENT_SR3_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR3_PRIO > 0) && (IRQ_SENT_SR3_CAT == IRQ_CAT1))
void SENTSR3_ISR(void)
#elif IRQ_SENT_SR3_CAT == IRQ_CAT23
ISR(SENTSR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[3]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR4_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 4                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR4_PRIO > 0) || (IRQ_SENT_SR4_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR4_PRIO > 0) && (IRQ_SENT_SR4_CAT == IRQ_CAT1))
void SENTSR4_ISR(void)
#elif IRQ_SENT_SR4_CAT == IRQ_CAT23
ISR(SENTSR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[4]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR5_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 5                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR5_PRIO > 0) || (IRQ_SENT_SR5_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR5_PRIO > 0) && (IRQ_SENT_SR5_CAT == IRQ_CAT1))
void SENTSR5_ISR(void)
#elif IRQ_SENT_SR5_CAT == IRQ_CAT23
ISR(SENTSR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[5]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR6_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 6                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR6_PRIO > 0) || (IRQ_SENT_SR6_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR6_PRIO > 0) && (IRQ_SENT_SR6_CAT == IRQ_CAT1))
void SENTSR6_ISR(void)
#elif IRQ_SENT_SR6_CAT == IRQ_CAT23
ISR(SENTSR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[6]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR7_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 7                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR7_PRIO > 0) || (IRQ_SENT_SR7_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR7_PRIO > 0) && (IRQ_SENT_SR7_CAT == IRQ_CAT1))
void SENTSR7_ISR(void)
#elif IRQ_SENT_SR7_CAT == IRQ_CAT23
ISR(SENTSR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[7]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR8_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 8                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR8_PRIO > 0) || (IRQ_SENT_SR8_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR8_PRIO > 0) && (IRQ_SENT_SR8_CAT == IRQ_CAT1))
void SENTSR8_ISR(void)
#elif IRQ_SENT_SR8_CAT == IRQ_CAT23
ISR(SENTSR8_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[8]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR9_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 9                           **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR9_PRIO > 0) || (IRQ_SENT_SR9_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR9_PRIO > 0) && (IRQ_SENT_SR9_CAT == IRQ_CAT1))
void SENTSR9_ISR(void)
#elif IRQ_SENT_SR9_CAT == IRQ_CAT23
ISR(SENTSR9_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[9]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR10_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 10                          **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR10_PRIO > 0) || (IRQ_SENT_SR10_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR10_PRIO > 0) && (IRQ_SENT_SR10_CAT == IRQ_CAT1))
void SENTSR10_ISR(void)
#elif IRQ_SENT_SR10_CAT == IRQ_CAT23
ISR(SENTSR10_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[10]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR11_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 11                          **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR11_PRIO > 0) || (IRQ_SENT_SR11_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR11_PRIO > 0) && (IRQ_SENT_SR11_CAT == IRQ_CAT1))
void SENTSR11_ISR(void)
#elif IRQ_SENT_SR11_CAT == IRQ_CAT23
ISR(SENTSR11_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[11]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR12_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 12                          **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR12_PRIO > 0) || (IRQ_SENT_SR12_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR12_PRIO > 0) && (IRQ_SENT_SR12_CAT == IRQ_CAT1))
void SENTSR12_ISR(void)
#elif IRQ_SENT_SR12_CAT == IRQ_CAT23
ISR(SENTSR12_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[12]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR13_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 13                          **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR13_PRIO > 0) || (IRQ_SENT_SR13_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR13_PRIO > 0) && (IRQ_SENT_SR13_CAT == IRQ_CAT1))
void SENTSR13_ISR(void)
#elif IRQ_SENT_SR13_CAT == IRQ_CAT23
ISR(SENTSR13_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[13]);
}
#endif

/******************************************************************************
** Syntax : void SENTSR14_ISR(void)                                          **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for SENT Interrupt Node 14                          **
**                                                                           **
*****************************************************************************/
#if((IRQ_SENT_SR14_PRIO > 0) || (IRQ_SENT_SR14_CAT == IRQ_CAT23))
#if((IRQ_SENT_SR14_PRIO > 0) && (IRQ_SENT_SR14_CAT == IRQ_CAT1))
void SENTSR14_ISR(void)
#elif IRQ_SENT_SR14_CAT == IRQ_CAT23
ISR(SENTSR14_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  Sent_Isr(Sent_kConfigPtr->SrnToChanMapPtr[14]);
}
#endif

#endif /*IRQ_SENT_EXIST == STD_ON */

#define IRQ_STOP_SEC_CODE
#include "MemMap.h"
