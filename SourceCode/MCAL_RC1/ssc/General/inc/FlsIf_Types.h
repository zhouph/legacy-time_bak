/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : FlsIf_Types.h $                                            **
**                                                                           **
**  $CC VERSION : \main\2 $                                                  **
**                                                                           **
**  $DATE       : 2013-06-19 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION  : This header file exports                                  **
**                 - Standard typesfor  FLS driver.                          **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: yes                                     **
**                                                                           **
******************************************************************************/
/* This is a Dummy file created for compilation */
#ifndef FLSIF_TYPES_H_
#define FLSIF_TYPES_H_
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/* Mode of flash operation */


typedef uint32 FlsIf_AddressType;
typedef uint32 FlsIf_LengthType;
typedef uint8  FlsIf_DataType;


typedef enum 
{
  FLSIF_UNINIT,
  FLSIF_IDLE,
  FLSIF_BUSY
}FlsIf_StatusType;


typedef enum
{
  FLSIF_JOB_OK,
  FLSIF_JOB_PENDING,
  FLSIF_JOB_CANCELED,
  FLSIF_READ_JOB_FAILED,
  FLSIF_WRITE_JOB_FAILED,
  FLSIF_ERASE_JOB_FAILED,
  FLSIF_ERASE_WARN_VER,
  FLSIF_READ_WARN_SBER,
  FLSIF_WRITE_WARN_VER
}FlsIf_JobResultType;


/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/


/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/


/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/


/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/


#endif /* FLSIF_TYPES_H_ */
