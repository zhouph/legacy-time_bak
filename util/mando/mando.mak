# \file
#
# \brief Mando make base module
#
# This file contains the implementation of the Mando make 
# base module.
#
# \author Mando, Advanced R&D, Korea

#################################################################

MAK_FILE_SUFFIX    :=mak
DEFS_FILE_SUFFIX   :=_defs.$(MAK_FILE_SUFFIX)
RULES_FILE_SUFFIX  :=_rules.$(MAK_FILE_SUFFIX)
CHECK_FILE_SUFFIX  :=_check.$(MAK_FILE_SUFFIX)
CFG_FILE_SUFFIX    :=_cfg.$(MAK_FILE_SUFFIX)

include $(MANDO_DIR)\mando_bsw_cfg.mak
include $(MANDO_DIR)\mando_mcal_cfg.mak
include $(MANDO_DIR)\mando_asw_cfg.mak
include $(MANDO_DIR)\mando_common_cfg.mak

include $(foreach PLUGIN,$(MANDO_BSW_MODULES),\
        $(MANDO_BSW_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(RULES_FILE_SUFFIX))      
include $(foreach PLUGIN,$(MANDO_BSW_MODULES),\
        $(MANDO_BSW_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(DEFS_FILE_SUFFIX))

include $(foreach PLUGIN,$(MANDO_MCAL_MODULES),\
        $(MANDO_MCAL_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(RULES_FILE_SUFFIX))      
include $(foreach PLUGIN,$(MANDO_MCAL_MODULES),\
        $(MANDO_MCAL_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(DEFS_FILE_SUFFIX))

include $(foreach PLUGIN,$(MANDO_FAL_MODULES),\
        $(MANDO_FAL_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(RULES_FILE_SUFFIX))      
include $(foreach PLUGIN,$(MANDO_FAL_MODULES),\
        $(MANDO_FAL_ROOT)\$(PLUGIN)\MAK\$(PLUGIN)$(DEFS_FILE_SUFFIX))


