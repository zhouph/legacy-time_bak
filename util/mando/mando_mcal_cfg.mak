# \file
#
# \brief Mando make configuration module
#
# This file contains the implementation of the Mando make 
# configuration module.
#
# \author Mando, Advanced R&D, Korea

#################################################################


MANDO_MCAL_ROOT     ?= $(PROJECT_ROOT)\SourceCode\MCAL_RC1
MANDO_TRESOS_ROOT   ?= $(PROJECT_ROOT)\SourceCode\Tresos

MANDO_MCAL_MODULES  := MCAL

