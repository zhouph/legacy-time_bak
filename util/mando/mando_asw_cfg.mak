# \file
#
# \brief Mando make configuration module
#
# This file contains the implementation of the Mando make 
# configuration module.
#
# \author Mando, Advanced R&D, Korea

#################################################################

MANDO_ASW_ROOT     ?= $(PROJECT_ROOT)\SourceCode\ASW

MANDO_FAL_ROOT     ?= $(PROJECT_ROOT)\SourceCode\ASW\FAL

MANDO_FAL_MODULES  := IdbTestMode Abc Bbc Det Spc Arb Vat Mcc Pct Rbc

CC_INCLUDE_PATH    += $(MANDO_ASW_ROOT)\include

Abc_VARIANT     				:= VariantName
Arb_VARIANT     				:= VariantName
Bbc_VARIANT     				:= VariantName
Det_VARIANT     				:= VariantName
Mcc_VARIANT     				:= VariantName
Pct_VARIANT     				:= VariantName
Rbc_VARIANT     				:= VariantName
Vat_VARIANT     				:= VariantName
Spc_VARIANT     				:= VariantName