# \file
#
# \brief Mando make configuration module
#
# This file contains the implementation of the Mando make 
# configuration module.
#
# \author Mando, Advanced R&D, Korea

#################################################################

MANDO_BSW_ROOT     ?= $(PROJECT_ROOT)\SourceCode\BSW

MANDO_BSW_MODULES  := Ach Acm Acmctl Acmio AdcIf Aka Awd Cantrv_tle6251 Com Diag Eem Fsr Icu Ioc Mom Msp Nvm Pedal Press Prly Proxy Rly Scheduler SentH Ses Spim Swt SystemServices Test TimE TomTimerHandler Vlv Vlvd_A3944 Wss XCP Task_1ms Task_5ms Task_10ms Task_50us AbsVlvM AxM AxP AyM AyP BbsVlvM CanM MgdM Mgd_TLE9180 Mps_TLE5012_Hndlr Mps_TLE5012M MtrM PedalM PedalP PressM PressP RlyM SasM SasP SenPwrM SwtM SwtP SysPwrM Watchdog WssM WssP YawM YawP GptM Mtr_Processing Arbitrator EcuHwCtrlM

Ach_VARIANT                     := ECU_NZ15
Acm_VARIANT                     := ECU_NZ15
Acmctl_VARIANT                  := ECU_NZ15
Acmio_VARIANT                   := ECU_NZ15
AdcIf_VARIANT                   := ECU_NZ15
Aka_VARIANT                     := ECU_NZ15
Awd_VARIANT                     := ECU_NZ15
CanTrv_TLE6251_VARIANT          := ECU_NZ15
#Dcmio_VARIANT                   := ECU_NZ15
Diag_VARIANT                    := ECU_NZ15
Eem_VARIANT                     := ECU_NZ15
Fsr_VARIANT                     := ECU_NZ15
Icu_VARIANT                     := ECU_NZ15
Ioc_VARIANT                     := ECU_NZ15
Mom_VARIANT                     := ECU_NZ15

Msp_VARIANT                     := ECU_NZ15
Nvm_VARIANT                     := ECU_NZ15
Pedal_VARIANT                   := ECU_NZ15
Press_VARIANT                   := ECU_NZ15
Prly_VARIANT                    := ECU_NZ15
Rly_VARIANT                     := ECU_NZ15
Proxy_VARIANT                   := YFE
Com_VARIANT                     := YFE
Ses_VARIANT                     := ECU_NZ15
Spim_VARIANT                    := ECU_NZ15
Swt_VARIANT                     := ECU_NZ15
#Vacuum_VARIANT                  := ECU_NZ15
Vlv_VARIANT                     := ECU_NZ15
Vlvd_A3944_VARIANT              := ECU_NZ15
Wss_VARIANT                     := ECU_NZ15
Task_1ms_VARIANT                := ECU_NZ15
Task_5ms_VARIANT                := ECU_NZ15
Task_10ms_VARIANT               := ECU_NZ15
Task_50us_VARIANT               := ECU_NZ15
SentH_VARIANT                   := ECU_NZ15
## IDB Electronics Development Team
AbsVlvM_VARIANT                 := ECU_NZ15
#Asic_Mgh100M_VARIANT            := ECU_NZ15
EcuHwCtrlM_VARIANT            := ECU_NZ15
AxM_VARIANT                     := ECU_NZ15
AxP_VARIANT                     := ECU_NZ15
AyM_VARIANT                     := ECU_NZ15
AyP_VARIANT                     := ECU_NZ15
BbsVlvM_VARIANT                 := ECU_NZ15
CanM_VARIANT                    := ECU_NZ15
MgdM_VARIANT                    := ECU_NZ15
Mgd_TLE9180_VARIANT             := ECU_NZ15
Mps_TLE5012_Hndlr_VARIANT       := ECU_NZ15
Mps_TLE5012M_VARIANT            := ECU_NZ15
MtrM_VARIANT                    := ECU_NZ15
PedalM_VARIANT                  := ECU_NZ15

PedalP_VARIANT                  := ECU_NZ15
PressM_VARIANT                  := ECU_NZ15
PressP_VARIANT                  := ECU_NZ15

RlyM_VARIANT                    := ECU_NZ15
SasM_VARIANT                    := ECU_NZ15
SasP_VARIANT                    := ECU_NZ15
SenPwrM_VARIANT                 := ECU_NZ15
SwtM_VARIANT                    := ECU_NZ15
SwtP_VARIANT                    := ECU_NZ15
SysPwrM_VARIANT                 := ECU_NZ15
Watchdog_VARIANT                := ECU_NZ15
WssM_VARIANT                    := ECU_NZ15
WssP_VARIANT                    := ECU_NZ15
YawM_VARIANT                    := ECU_NZ15
YawP_VARIANT                    := ECU_NZ15
GptM_VARIANT                    := ECU_NZ15
Arbitrator_VARIANT              := ECU_NZ15
Mtr_Processing_VARIANT              := ECU_NZ15
TimE_VARIANT                    := IDB_NZ_SW
UNIT_TEST          ?= false

## Non-Architecture Module INCUDE
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\BSW\PWM