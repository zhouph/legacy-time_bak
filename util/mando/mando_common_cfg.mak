# \file
#
# \brief Mando make configuration module
#
# This file contains the implementation of the Mando make 
# configuration module.
#
# \author Mando, Advanced R&D, Korea

#################################################################

MANDO_COMMON_ROOT     ?= $(PROJECT_ROOT)\SourceCode\

## Library INCLUDE
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\LIB

## VIL INCLUDE
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\_PAR
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\_PAR\car
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\_PAR\version
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\BH\CAL
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\BH\Car_var
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\BH\Model
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\TD\CAL
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\TD\Car_var
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\TD\Model
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\YFE\CAL
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\YFE\Car_var
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\APPL\YFE\Model
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\VIL\HDR
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\VIL\IDBLogDataSet.c

CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\LIB
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\LIB\DetErr.c
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\LIB\WContlSubFunc.c
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\LIB\SwcCommonFunction.c
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\LIB\L_CommonFunction.c
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\LIB\L_CommonFunction_1ms.c

CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\FBL_HDR\FBL_HMC\Fbl_Header.c
CC_FILES_TO_BUILD  += $(PROJECT_ROOT)\SourceCode\FBL_HDR\FBL_HMC\Fbl_Mtab.c
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\FBL_HDR\FBL_HMC\

CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\HDR\CMN
CC_INCLUDE_PATH    += $(PROJECT_ROOT)\SourceCode\HDR\EXT

#################################################################
# Dirty glues
# To be removed
#################################################################

LOC_FILE = $(PROJECT_ROOT)\IDB_Project.lsl

CC_OPT += -D_TASKING_C_TRICORE_=1
CC_OPT += -D__AHB_GEN3=1
CC_OPT += -D__ECU=2
CC_OPT += -D__ESP_PLUSE=1
CC_OPT += -D__ESP_ACC=1
CC_OPT += -D__MGH80_MOCi=0
CC_OPT += -D__MCU=2
CC_OPT += --no-warnings

CPP_OPTS += -D__AHB_GEN3=1
CPP_OPTS += -D__ECU=2
CPP_OPTS += -D__ESP_PLUSE=1
CPP_OPTS += -D__ESP_ACC=1
CPP_OPTS += -D__MGH80_MOCi=0
CPP_OPTS += -D__MCU=2


CPP_OPTS += -D_TASKING_C_TRICORE_=1

LINK_OPT += -D__CPU__=TC27X
LINK_OPT += -D__PROC_TC27X__
LINK_OPT += --core=mpe:vtc