# \file
#
# \brief AUTOSAR ApplTemplates
#
# This file contains the implementation of the AUTOSAR
# module ApplTemplates.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.
#

#################################################################
# For GNU Make 3.82 we must explicitely set SHELL to cmd.exe, since
# otherwise it would try to use sh.exe.
# 3.80 produces error if SHELL is set this way, so we need the ifeq.
ifeq ($(MAKE_VERSION),3.82)
SHELL := cmd.exe
endif

#################################################################
# include merged makefile if it exists
-include $(PROJECT_ROOT)\util\Merged_Makefile.mak

#################################################################
# additonal C source files to be built
#################################################################

CC_FILES_TO_BUILD += 

#################################################################
# tresos settings
#################################################################
TRESOS2_WORKSPACE_DIR = $(PROJECT_ROOT)\..
TRESOS2_USER_OPTIONS += -data $(TRESOS2_WORKSPACE_DIR)
TRESOS2_PROJECT_NAME = $(PROJECT_NAME)
TRESOS2_USER_OPTIONS += -Dinfo=false
TRESOS2_USER_OPTIONS += -DmergeConfigs=true

# We want project mode, not legacy.
USE_LEGACY_MAKECFG := false

-include common.mak
